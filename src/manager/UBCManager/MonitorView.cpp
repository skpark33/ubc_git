// MonitorView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "MonitorView.h"
#include "Enviroment.h"
#include "MainFrm.h"
#include "MonitorDetailDlg.h"
#include "common\ubcdefine.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "ubccopcommon/SiteSelectDlg.h"
#include "UBCCopCommon\UbcMenuAuth.h"

#define STR_ADMIN_ON			LoadStringById(IDS_HOSTVIEW_LIST001)
#define STR_ADMIN_OFF			LoadStringById(IDS_HOSTVIEW_LIST002)
#define STR_OPERA_ON			LoadStringById(IDS_HOSTVIEW_LIST003)
#define STR_OPERA_OFF			LoadStringById(IDS_HOSTVIEW_LIST004)


#define STR_MONITOR_SITEID		LoadStringById(IDS_SITEVIEW_LST001)
#define STR_MONITOR_ID			LoadStringById(IDS_MONITORVIEW_STR002)
#define STR_MONITOR_NAME			LoadStringById(IDS_MONITORVIEW_STR003)
#define STR_MONITOR_DESC			LoadStringById(IDS_HOSTVIEW_LIST016)//
#define STR_MONITOR_TYPE   		LoadStringById(IDS_MONITORVIEW_STR004)
#define STR_MONITOR_STARTUPTIME		LoadStringById(IDS_MONITORVIEW_STR005)
#define STR_MONITOR_SHUTDOWNTIME		LoadStringById(IDS_MONITORVIEW_STR006)
#define STR_MONITOR_MONITORUSETIMENESS		LoadStringById(IDS_MONITORVIEW_STR008) // 
#define STR_MONITOR_WEEKSHUTDOWNTIME	LoadStringById(IDS_MONITORVIEW_STR007) 
#define STR_MONITOR_HOLIDAY			LoadStringById(IDS_HOLLIDAYDLG_LST001) //

#define STR_SEP				_T(",")

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(0,128,255)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)


// CMonitorView

IMPLEMENT_DYNCREATE(CMonitorView, CFormView)

CMonitorView::CMonitorView()
	: CFormView(CMonitorView::IDD)
,	m_Reposition(this)
{
	m_pMainFrame = (CMainFrame*)AfxGetMainWnd();

}

CMonitorView::~CMonitorView()
{
}

void CMonitorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_MONITOR, m_lscMonitor);

	DDX_Control(pDX, IDC_BUTTON_MONITOR_CREATE, m_btnAddMonitor);
	DDX_Control(pDX, IDC_BUTTON_MONITOR_DEL, m_btnDelMonitor);
	DDX_Control(pDX, IDC_BUTTON_MONITOR_MOD, m_btnModMonitor);
	DDX_Control(pDX, IDC_BUTTON_MONITOR_REF, m_btnRefMonitor);
	DDX_Control(pDX, IDC_BUTTON_MONITOR_OFF, m_btnMonitorOff);
	DDX_Control(pDX, IDC_BUTTON_MONITOR_ON, m_btnMonitorOn);

	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_COMBO_FILTER_OPR, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_FILTER_MONITORTYPE, m_cbMonitorType);

	DDX_Control(pDX, IDC_EDIT_FILTER_MONITORNAME, m_editMonitorName);
	DDX_Control(pDX, IDC_EDIT_FILTER_MONITORID, m_editMonitorId);
}

BEGIN_MESSAGE_MAP(CMonitorView, CFormView)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR_REF, &CMonitorView::OnBnClickedButtonMonitorRef)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR_CREATE, &CMonitorView::OnBnClickedButtonMonitorCreate)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR_DEL, &CMonitorView::OnBnClickedButtonMonitorDel)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR_MOD, &CMonitorView::OnBnClickedButtonMonitorMod)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR_OFF, &CMonitorView::OnBnClickedButtonMonitorOff)
	ON_BN_CLICKED(IDC_BUTTON_MONITOR_ON, &CMonitorView::OnBnClickedButtonMonitorOn)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SITE, &CMonitorView::OnBnClickedButtonFilterSite)
	ON_WM_DESTROY()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_MONITOR, &CMonitorView::OnNMDblclkListMonitor)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_MONITOR, &CMonitorView::OnNMRclickListMonitor)
	ON_COMMAND(ID_DEVICEMENU_POWERON, &CMonitorView::OnDevicemenuPoweron)
	ON_COMMAND(ID_DEVICEMENU_POWEROFF, &CMonitorView::OnDevicemenuPoweroff)
	ON_COMMAND(ID_DEVICEMENU_DETAIL, &CMonitorView::OnDevicemenuDetail)
	ON_COMMAND(ID_DEVICEMENU_REMOVE, &CMonitorView::OnDevicemenuRemove)
END_MESSAGE_MAP()


// CMonitorView 진단입니다.

#ifdef _DEBUG
void CMonitorView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMonitorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CMonitorView 메시지 처리기입니다.

void CMonitorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_HOSTVIEW_STR001));

	CodeItemList listMonitorType;
	CUbcCode::GetInstance()->GetCategoryInfo(_T("SubMonitorType"), listMonitorType);

	POSITION pos = listMonitorType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listMonitorType.GetNext(pos);
		m_cbMonitorType.AddString(info.strEnumString, info.nEnumNumber);
	}

	CRect rc;
	m_cbMonitorType.GetWindowRect(&rc);
	m_cbMonitorType.SetDroppedWidth(rc.Width() * 1.5);

	m_cbAdmin.AddString("All");
	m_cbAdmin.AddString(STR_ADMIN_ON);
	m_cbAdmin.AddString(STR_ADMIN_OFF);
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(STR_OPERA_ON);
	m_cbOperation.AddString(STR_OPERA_OFF);
	m_cbOperation.SetCurSel(0);

	m_btnDelMonitor.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btnModMonitor.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_btnRefMonitor.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnAddMonitor.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btnMonitorOff.LoadBitmap(IDB_BUTTON_SHUTDOWN, RGB(255,255,255));
	m_btnMonitorOn.LoadBitmap(IDB_BITMAP_POWERON, RGB(255,255,255));
/*
	m_btnAddMonitor.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN001));
	m_btnDelMonitor.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN002));
	m_btnModMonitor.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN003));
	m_btnRefMonitor.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN004));
	m_btnMonitorOn.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN007));
	m_btnMonitorOff.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN008));
*/
	InitPosition(m_rcClient);

	UpdateData(FALSE);
	InitAuthCtrl();
	InitMonitorList();

	SetScrollSizes(MM_TEXT, CSize(1,1));

}
void CMonitorView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscMonitor, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}


// 권한에 따라 활성화 한다.
void CMonitorView::InitAuthCtrl()
{

	m_btnDelMonitor.EnableWindow(IsAuth(_T("HDEL")));
	m_btnModMonitor.EnableWindow(IsAuth(_T("HMOD")));
	m_btnRefMonitor.EnableWindow(IsAuth(_T("HQRY")));
	m_btnAddMonitor.EnableWindow(IsAuth(_T("HREG")));
	m_btnMonitorOff.EnableWindow(IsAuth(_T("HOFF")));
	m_btnMonitorOn.EnableWindow(IsAuth(_T("HRBT")));
}

void CMonitorView::InitMonitorList()
{
	m_szMonitorColum[eCheck] = _T("");	
	m_szMonitorColum[eSiteId] = STR_MONITOR_SITEID;
	m_szMonitorColum[eMonitorType] = STR_MONITOR_TYPE;
	m_szMonitorColum[eMonitorName] = STR_MONITOR_NAME;
	m_szMonitorColum[eMonitorID] = STR_MONITOR_ID;
	m_szMonitorColum[eMonitorDesc] = STR_MONITOR_DESC;
	m_szMonitorColum[eStartupTime] = STR_MONITOR_STARTUPTIME;
	m_szMonitorColum[eShutdownTime] = STR_MONITOR_SHUTDOWNTIME;
	m_szMonitorColum[eMonitorUseTime] = STR_MONITOR_MONITORUSETIMENESS;
	m_szMonitorColum[eWeekShutdownTime] = STR_MONITOR_WEEKSHUTDOWNTIME;
	m_szMonitorColum[eHoliday] = STR_MONITOR_HOLIDAY;


	m_lscMonitor.SetExtendedStyle(m_lscMonitor.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_INFOTIP |m_lscMonitor.GetExtendedStyle());

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilMonitorList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilMonitorList.Add(&bmCheck, RGB(255, 255, 255));
	m_lscMonitor.SetImageList(&m_ilMonitorList, LVSIL_SMALL);

	m_lscMonitor.InsertColumn(eCheck,				m_szMonitorColum[eCheck], LVCFMT_LEFT, 22);
	m_lscMonitor.InsertColumn(eSiteId,			m_szMonitorColum[eSiteId], LVCFMT_LEFT, 60);
	m_lscMonitor.InsertColumn(eMonitorType,			m_szMonitorColum[eMonitorType], LVCFMT_LEFT, 60);
	m_lscMonitor.InsertColumn(eMonitorName,			m_szMonitorColum[eMonitorName], LVCFMT_LEFT, 100);
	m_lscMonitor.InsertColumn(eMonitorID,			m_szMonitorColum[eMonitorID], LVCFMT_LEFT, 80);
	m_lscMonitor.InsertColumn(eMonitorDesc,			m_szMonitorColum[eMonitorDesc], LVCFMT_LEFT, 100);
	m_lscMonitor.InsertColumn(eStartupTime,		m_szMonitorColum[eStartupTime], LVCFMT_LEFT, 80);
	m_lscMonitor.InsertColumn(eShutdownTime,		m_szMonitorColum[eShutdownTime], LVCFMT_LEFT, 80);
	m_lscMonitor.InsertColumn(eMonitorUseTime,		m_szMonitorColum[eMonitorUseTime], LVCFMT_LEFT, 40);
	m_lscMonitor.InsertColumn(eWeekShutdownTime,	m_szMonitorColum[eWeekShutdownTime], LVCFMT_LEFT, 190);
	m_lscMonitor.InsertColumn(eHoliday,			m_szMonitorColum[eHoliday], LVCFMT_LEFT, 190);

	m_lscMonitor.SetColumnWidth(eCheck,			22);
	m_lscMonitor.SetColumnWidth(eSiteId,			60);
	m_lscMonitor.SetColumnWidth(eMonitorType,		60);
	m_lscMonitor.SetColumnWidth(eMonitorName,		100);
	m_lscMonitor.SetColumnWidth(eMonitorID,			80);
	m_lscMonitor.SetColumnWidth(eMonitorDesc,		100);
	m_lscMonitor.SetColumnWidth(eStartupTime,		80);
	m_lscMonitor.SetColumnWidth(eShutdownTime,	80);
	m_lscMonitor.SetColumnWidth(eMonitorUseTime,		40);
	m_lscMonitor.SetColumnWidth(eWeekShutdownTime,190);
	m_lscMonitor.SetColumnWidth(eHoliday,			190);



	m_lscMonitor.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscMonitor.LoadColumnState("MONITOR-LIST", szPath);

}
BOOL CMonitorView::RefreshMonitor(bool bCompMsg)
{
	TraceLog(("RefreshMonitor begin"));
	DWORD dwStartTime =  ::GetTickCount() ;

	CWaitMessageBox wait;
	
	CString szSite = (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite);

	UpdateData();

	CString strMonitorName     ;
	CString strMonitorId       ;
	CString strMonitorType     ;
	CString strHostId	     ;
	CString strSiteId       ;
	CString strAdminStat    ;
	CString strOperaStat    ;

	m_editMonitorName.GetWindowText(strMonitorName);
	m_editMonitorId.GetWindowText(strMonitorId);
	
	strMonitorType = m_cbMonitorType.GetCheckedIDs();
	if(strMonitorType == _T("All")) strMonitorType = _T("");
	else if(strMonitorType.GetLength() >= 2) strMonitorType = strMonitorType.Mid(1, strMonitorType.GetLength()-2);
	
	strSiteId = m_strSiteId;

	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());

	CString strWhere;

	// 단말이름
	strMonitorName.Trim();
	if(!strMonitorName.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s monitorName like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strMonitorName
							);
	}

	// 단말ID
	strMonitorId.Trim();
	if(!strMonitorId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s monitorId like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strMonitorId
							);
	}

	// 단말타입
	if(!strMonitorType.IsEmpty())
	{
		strMonitorType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s monitorType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strMonitorType
							);
	}

	// 소속조직
	if(!strSiteId.IsEmpty())
	{
		strSiteId.Replace(_T(","), _T("','"));
		strWhere.AppendFormat(_T(" %s siteId in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strSiteId
							);
	}

	// 연결상태
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s operationalState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}

	// 사용상태
	if(_ttoi(strAdminStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s adminState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAdminStat)==1 ? 1:0)
							);
	}

	TraceLog(("m_lsMonitor.RemoveAll() begin"));
	m_lsMonitor.RemoveAll();
	TraceLog(("m_lsMonitor.RemoveAll() end"));

	BOOL bRet = CCopModule::GetObject()->GetMonitorList(szSite, "*", (strWhere.IsEmpty() ? NULL : strWhere),true,false,m_lsMonitor);

	RefreshMonitorList();

	TraceLog(("RefreshMonitor end"));

	if(!bRet)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR010), MB_ICONINFORMATION);
		return FALSE;
	}

	if(bCompMsg)
	{
		DWORD dwElapse = ::GetTickCount() - dwStartTime;
		DWORD dwElapseSec = dwElapse / 1000;
		CString strElapse;
		strElapse.Format("\nElapse time : %ld sec", dwElapseSec);

		CString strMsg;
		strMsg.Format(LoadStringById(IDS_ICONVIEW_STR011), m_lscMonitor.GetItemCount());
		strMsg.Append(strElapse);

		UbcMessageBox(strMsg, MB_ICONINFORMATION);
	}

	return bRet;
}

void CMonitorView::RefreshMonitorList(POSITION posStart)
{
	TraceLog(("RefreshMonitorList begin"));

	m_lscMonitor.SetRedraw(FALSE);

	int nRow = 0;
	LVITEM item;
	CString szBuf;
	POSITION pos = m_lsMonitor.GetHeadPosition();
	POSITION posTail = m_lsMonitor.GetTailPosition();

	if(posStart)
	{
		pos = posStart;
		m_lsMonitor.GetNext(pos);
	}
	else
	{
		m_lscMonitor.DeleteAllItems();
	}

	while(pos)
	{
		POSITION posOld = pos;
		SMonitorInfo info = m_lsMonitor.GetNext(pos);

		m_lscMonitor.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, &info);  //OK

		m_lscMonitor.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lscMonitor.GetCurSortCol();
	if(-1 != nCol)
		m_lscMonitor.SortList(nCol, m_lscMonitor.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscMonitor);
	else
		m_lscMonitor.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscMonitor);

	TraceLog(("RefreshMonitorList end"));

	m_lscMonitor.SetRedraw(TRUE);

	RefreshPaneText();
}


void CMonitorView::UpdateListRow(int nRow, SMonitorInfo* pInfo)
{
	if(!pInfo || m_lscMonitor.GetItemCount() < nRow)
		return;

	COLORREF crTextBk = COLOR_WHITE;
	if(pInfo->operationalState)
	{
		crTextBk = COLOR_YELLOW;
	}
	else
	{
		crTextBk = COLOR_GRAY;
	}
	m_lscMonitor.SetRowColor(nRow, crTextBk, m_lscMonitor.GetTextColor());

	CString szBuf;

	m_lscMonitor.SetItemText(nRow, eSiteId, pInfo->siteId);
	m_lscMonitor.SetItemText(nRow, eMonitorID, pInfo->monitorId);
	m_lscMonitor.SetItemText(nRow, eMonitorName, pInfo->monitorName);
	m_lscMonitor.SetItemText(nRow, eMonitorType, CUbcCode::GetInstance()->GetCodeName(_T("SubMonitorType"), pInfo->monitorType));
	m_lscMonitor.SetItemText(nRow, eMonitorDesc, pInfo->description);
	m_lscMonitor.SetItemText(nRow, eStartupTime, pInfo->startupTime);
	m_lscMonitor.SetItemText(nRow, eShutdownTime, pInfo->shutdownTime);
	m_lscMonitor.SetItemText(nRow, eWeekShutdownTime, pInfo->weekShutdownTime);
	m_lscMonitor.SetItemText(nRow, eHoliday, pInfo->holiday);

	// 밝기
	if(pInfo->monitorUseTime < 0)
	{
		m_lscMonitor.SetItemText(nRow, eMonitorUseTime, _T("Unknown"));
	}
	else
	{
		m_lscMonitor.SetItemText(nRow, eMonitorUseTime, ToString(pInfo->monitorUseTime));
	}

}

void CMonitorView::RefreshPaneText()
{
	int nActiveCnt = 0;
	POSITION pos = m_lsMonitor.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SMonitorInfo info = m_lsMonitor.GetNext(pos);
		if(info.operationalState)
		{
			nActiveCnt++;
		}
	}

	CString strPaneText;
	strPaneText.Format(_T("Monitor:%d Active:%d")
					 , m_lsMonitor.GetCount()
					 , nActiveCnt
					 );

	if(m_pMainFrame)
	{
		m_pMainFrame->SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_HOST, (LPARAM)(LPCTSTR)strPaneText);
	}
}

void CMonitorView::OnBnClickedButtonMonitorRef()
{
	RefreshMonitor();
}

void CMonitorView::OnBnClickedButtonMonitorCreate()
{
	CMonitorDetailDlg dlg;

	dlg.SetCreateMode();
	if(dlg.DoModal() != IDOK)
	{
		return;
	}
	SMonitorInfo Info;
	dlg.GetInfo(Info);

	MonitorInfoList dummyList;
	CCopModule::GetObject()->GetMonitorList(Info.siteId,Info.monitorId,NULL,true,true,dummyList); 
	if(dummyList.GetSize() > 0){
		CString msg;
		msg.Format(LoadStringById(IDS_ICONVIEW_STR024), Info.monitorId);
		UbcMessageBox(msg);
		return ;
	}

	if(!CCopModule::GetObject()->CreateMonitor(Info)){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_MONITORVIEW_STR021), Info.monitorId);
		UbcMessageBox(szMsg, MB_ICONERROR);
		return;
	}
	UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR022), MB_ICONINFORMATION);
	RefreshMonitor();

}

void CMonitorView::OnBnClickedButtonMonitorDel()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscMonitor.GetItemCount(); nRow++)
	{
		if(!m_lscMonitor.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	RemoveMonitor(arRow, true);
}

void CMonitorView::OnBnClickedButtonMonitorMod()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscMonitor.GetItemCount(); nRow++)
	{
		if(!m_lscMonitor.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	ModifyMonitor(arRow, true);
}


void CMonitorView::OnBnClickedButtonMonitorOff()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscMonitor.GetItemCount(); nRow++)
	{
		if(!m_lscMonitor.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	PowerOff(arRow, true);
}

void CMonitorView::OnBnClickedButtonMonitorOn()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscMonitor.GetItemCount(); nRow++)
	{
		if(!m_lscMonitor.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	PowerOn(arRow, true);
}

void CMonitorView::OnBnClickedButtonFilterSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() == IDOK)
	{
		SiteInfoList siteInfoList;
		if(dlg.GetSelectSiteInfoList(siteInfoList))
		{
			POSITION pos = siteInfoList.GetHeadPosition();
			while(pos)
			{
				SSiteInfo info = siteInfoList.GetNext(pos);
				strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
				strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
			}
		}
	}

	SetDlgItemText(IDC_EDIT_FILTER_SITENAME, strSiteName);
	m_strSiteId = strSiteId;
}

void CMonitorView::OnDestroy()
{
	CFormView::OnDestroy();

	if(m_pMainFrame)
	{
		m_pMainFrame->SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_HOST, (LPARAM)(LPCTSTR)_T(""));
	}

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscMonitor.SaveColumnState("MONITOR-LIST", szPath);
}

void CMonitorView::OnNMDblclkListMonitor(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifyMonitor(arRow, true);
}


void CMonitorView::ModifyMonitor(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscMonitor.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscMonitor.GetItemData(nRow);
		if(!pos) continue;

		SMonitorInfo Info = m_lsMonitor.GetAt(pos);
		CMonitorDetailDlg dlg;

		dlg.SetInfo(Info);
		if(dlg.DoModal() != IDOK)
		{
			dlg.GetInfo(Info);
			m_lsMonitor.GetAt(pos) = Info;
			UpdateListRow(nRow, &Info); //OK
			continue;
		}
		dlg.GetInfo(Info);

		if(!CCopModule::GetObject()->SetMonitor(Info, GetEnvPtr()->m_szLoginID))
		{
			m_szMsg.Format(LoadStringById(IDS_MONITORVIEW_STR010), Info.hostId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
		m_lsMonitor.GetAt(pos) = Info;

		TraceLog(("holiday=%s, shutdownTime=%s", Info.holiday, Info.shutdownTime));
	}

	if(bModified)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR009), MB_ICONINFORMATION);
		RefreshMonitorList();
	}
}

void CMonitorView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
	//InvalidateItems();
}

int CMonitorView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}


LRESULT CMonitorView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("CMonitorView::InvokeEvent begin"));

	HWND hWnd = GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));
	if(::IsWindow(hWnd) == FALSE) return 0;

	switch(wParam)
	{
/*
	case IEH_OPSTAT: 
		{
// cli >>> post PM=2/Site=SQI/Host=SQI-06112 operationalStateChanged siteId="SQI", hostId="SQI-06116", operationalState=true
			SOpStat* pOpStat = (SOpStat*)lParam;
			return this->m_picCtrl.ChangeOPStat((const char*)pOpStat->siteId, (const char*)pOpStat->hostId, pOpStat->operationalState);
		}
*/
	case IEH_DEVICE_OPSTAT: 
		{
			SDeviceOpStat* pOpStat = (SDeviceOpStat*)lParam;
			return this->ChangeOPStat(pOpStat);
		}
/*
	case IEH_ADSTAT:
		return ChangeAdStat((SAdStat*)lParam);

	case IEH_UPPACKAGE:
		return UpdatePackage((SUpPackage*)lParam);

	case IEH_CHNGVNC:
		return ChangeVncStat((cciEvent*)lParam);

		// 0000707: 콘텐츠 다운로드 상태 조회 기능
	case IEH_CHNGDOWN:
		return ChangeDownloadStat((SDownloadStateChange*)lParam);

		// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	case IEH_MOSTAT:
		return ChangeMonitorStat((SMonitorStat*)lParam);

	case IEH_HDDCHNG:
		return ChangeDiskAvail((SDiskAvailChanged*)lParam);
	*/
	case IEH_UNKOWN:
		//cciAttributeList* pList = (cciAttributeList*)lParam;

		//for(int i = 0; i < (int)pList->length(); i++){
		//	cciAttribute& attr = (*pList)[i];

		//	if(strcmp(attr.getName(),"siteId") == 0){
		//		attr.getValue(strSite);
		//	}
		//}
		break;
	}
	return 0;
}
int CMonitorView::ChangeOPStat(SDeviceOpStat* pOpStat)
{
	TraceLog(("Site=%s/Host=%s/%s=%s event arrived", pOpStat->siteId,pOpStat->hostId,
													pOpStat->objectClass,pOpStat->objectId));

	for(int nRow = 0; nRow < m_lscMonitor.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscMonitor.GetItemData(nRow);
		if(!pos) continue;

		SMonitorInfo Info = m_lsMonitor.GetAt(pos);

		//TraceLog(("Site=%s/Host=%s/Monitor=%s data founded", Info.siteId,Info.hostId,Info.monitorId));

		if(	strlen(pOpStat->siteId) > 0  && pOpStat->siteId[0] != '*' && Info.siteId != pOpStat->siteId || 
			strlen(pOpStat->hostId) > 0  && pOpStat->hostId[0] != '*' && Info.hostId != pOpStat->hostId ||
			strncmp("Monitor",pOpStat->objectClass,strlen("Monitor")) != 0 ||
			Info.monitorId != pOpStat->objectId)
		{
			continue;
		}
		TraceLog(("operationalState : %s", pOpStat->operationalState?"true":"false"));
//		m_lscMonitor.SetText(nRow, m_szMonitorColum[eMonitorOper], pOpStat->operationalState?STR_OPERA_ON:STR_OPERA_OFF);
		Info.operationalState = pOpStat->operationalState;
		m_lsMonitor.GetAt(pos) = Info;

//		RefreshMonitorList();
//		m_lscMonitor.Invalidate();
		UpdateListRow(nRow, &Info); //OK

		RefreshPaneText();
		return 0;
	}

	return 0;
}

void CMonitorView::OnNMRclickListMonitor(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscMonitor.GetItemData(nRow);
	if(!pos) return;
	SMonitorInfo Info = m_lsMonitor.GetAt(pos);

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);
//	ClientToScreen(&pt);

	pPopup = menu.GetSubMenu(8);


	pPopup->EnableMenuItem(ID_DEVICEMENU_POWERON, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_DEVICEMENU_POWEROFF, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_DEVICEMENU_DETAIL, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_DEVICEMENU_REMOVE, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));


	//pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_NONOTIFY,pt.x, pt.y, this);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CMonitorView::OnDevicemenuPoweron()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	PowerOn(arRow, true);
}

void CMonitorView::OnDevicemenuPoweroff()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	PowerOff(arRow, true);
}

void CMonitorView::OnDevicemenuDetail()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	ModifyMonitor(arRow, true);
}

void CMonitorView::OnDevicemenuRemove()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	RemoveMonitor(arRow, true);
}

void CMonitorView::RemoveMonitor(CArray<int>& arRow, bool bMsg)
{
	if(UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR012), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}

	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscMonitor.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscMonitor.GetItemData(nRow);
		if(!pos) continue;

		SMonitorInfo Info = m_lsMonitor.GetAt(pos);

		if(!CCopModule::GetObject()->DelMonitor(Info.siteId, Info.hostId, Info.monitorId))
		{
			m_szMsg.Format(LoadStringById(IDS_MONITORVIEW_STR013), Info.monitorId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;

		m_lsMonitor.RemoveAt(pos);
		//m_lscMonitor.DeleteItem(nRow);
	}

	if(bModified)
	{
		if(bMsg) 	UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR014), MB_ICONINFORMATION);
		RefreshMonitorList();
	}

}

void CMonitorView::PowerOn(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscMonitor.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscMonitor.GetItemData(nRow);
		if(!pos) continue;

		SMonitorInfo Info = m_lsMonitor.GetAt(pos);
		

		if(!CCopModule::GetObject()->DevicePowerOn(Info.siteId, Info.hostId, "Monitor", Info.monitorId, ::ToString(Info.monitorType)))
		{
			m_szMsg.Format(LoadStringById(IDS_MONITORVIEW_STR015), Info.monitorId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
	}

	if(bModified)
	{
		if(bMsg) 	UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR016), MB_ICONINFORMATION);
		//RefreshMonitorList();
	}

}

void CMonitorView::PowerOff(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscMonitor.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscMonitor.GetItemData(nRow);
		if(!pos) continue;

		SMonitorInfo Info = m_lsMonitor.GetAt(pos);
		

		if(!CCopModule::GetObject()->DevicePowerOff(Info.siteId, Info.hostId, "Monitor", Info.monitorId, ::ToString(Info.monitorType)))
		{
			m_szMsg.Format(LoadStringById(IDS_MONITORVIEW_STR017), Info.monitorId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
	}

	if(bModified)
	{
		if(bMsg) 	UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR018), MB_ICONINFORMATION);
		//RefreshMonitorList();
	}

}

