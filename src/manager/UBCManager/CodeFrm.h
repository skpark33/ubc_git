#pragma once


// CCodeFrm frame

class CCodeFrm : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CCodeFrm)
protected:
	CCodeFrm();           // protected constructor used by dynamic creation
	virtual ~CCodeFrm();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
