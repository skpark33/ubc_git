#pragma once

#define		TICKER_COUNT	10

#include "web_browser.h"
#include "ShockwaveFlash.h"
#include "ximage.h"
#include "PictureEx.h"
//#include "Contents.h"
//#include "PPTEventSink.h"
#include "OverlayInterface.h"
#include "VMRRender.h"

namespace Mng_Browser{ // 매니져용

//
// LOG 기록 후 casting 여부 등의 내용을 UTV_MGR 에게 보내기 위한 notification.
//void 	castingStateReport (...); -> 차후 구현
//

enum  E_TEXT_DIRECTION {
	TEXT_DIRECTION_HORIZONTAL = 0,		//가로
	TEXT_DIRECTION_VERTICAL				//세로
};

enum  E_TEXT_ALIGN {
	TEXT_ALIGN_LT = 1,					//left-top
	TEXT_ALIGN_CT,						//center-top
	TEXT_ALIGN_RT,						//right-top
	TEXT_ALIGN_LM,						//left-middle
	TEXT_ALIGN_CM,						//center-middle
	TEXT_ALIGN_RM,						//right-middle
	TEXT_ALIGN_LB,						//left-bottom
	TEXT_ALIGN_CB,						//center-bottom
	TEXT_ALIGN_RB						//right-bottom
};

enum E_TICKER_SPEED
{
	E_TICKER_FAST = 1001,
	E_TICKER_LITTLE_FAST,
	E_TICKER_NORMAL,
	E_TICKER_LITTLE_SLOW,
	E_TICKET_SLOW
};

// 매니져용
typedef  std::string	ciString;
typedef  CTime			ciTime;
typedef  int			ciShort;
typedef  int			ciLong;
typedef  bool			ciBoolean;
//#define		ciString		std::string
//#define		ciTime			CTime
//#define		ciShort			int
//#define		ciLong			int
//#define		ciBoolean		bool
#define		ciFalse			false
#define		ciTrue			true


enum CONTENTS_TYPE {
	CONTENTS_NOT_DEFINE = -1,
	CONTENTS_VIDEO = 0,
	CONTENTS_SMS,			//1. Horizontal Ticker
	CONTENTS_IMAGE,
	CONTENTS_PROMOTION,
	CONTENTS_TV,
	CONTENTS_TICKER,		//5. Vertical Ticker
	CONTENTS_PHONE,
	CONTENTS_WEBBROWSER,
	CONTENTS_FLASH,
	CONTENTS_WEBCAM,
	CONTENTS_RSS,			// 10.
	CONTENTS_CLOCK,
	CONTENTS_TEXT,			// 12. 이전 SMS
	CONTENTS_FLASH_TEMPLATE,
	CONTENTS_DYNAMIC_FLASH,
	CONTENTS_TYPING,
	CONTENTS_PPT,			//16
	CONTENTS_ETC,	// 2010.10.12 장광수 기타콘텐츠추가
	CONTENTS_WIZARD,	// 2010.10.21 정운형 마법사콘텐츠추가
	CONTENTS_CNT	// 2010.10.12 장광수 기타콘텐츠추가
};
static const char* CONTENTS_TYPE_TO_STRING[CONTENTS_CNT+1] = {
	"No Contents", "Video", "Ticker", "Image", "Promotion", "TV", "Ticker", "Phone", "Web", "Flash", "WebCam", "RSS", "Clock", "Text", "Flash", "Flash", "Typing", "PPT"
	, "Etc"		// 2010.10.12 장광수 기타콘텐츠추가
	, "Wizard"	// 2010.10.27 장광수 마법사콘텐츠추가
};

static const char* TIME_SCOPE_TO_STRING[8] = {
	"All Day", "07 ~ 09h", "09 ~ 12h", "12 ~ 13h", "13 ~ 18h", "18 ~ 22h", "22 ~ 24h", "00 ~ 07h"
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//class CTemplate;
class CFrame;
class CSchedule;

typedef	CArray<CSchedule*, CSchedule*>	CScheduleArray;
typedef	CMapStringToPtr					CScheduleMap;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSchedule
class CSchedule : public CWnd
{
	DECLARE_DYNAMIC(CSchedule)

public:
	static	CSchedule*	GetScheduleObject(CFrame* pParentWn, LPCSTR lpszID, CRect& rect, bool bDefaultSchedule, CString& strErrorMessage);

	CSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CSchedule();

protected:

	DECLARE_MESSAGE_MAP()

public:
	////////////////////////////////////////////////////////////////////////////
	// IDL defined schedule attributes
	ciString		m_siteId;					///<방송될 사이트
	ciString		m_hostId;					///<client host id		
	ciString		m_scheduleId;				///<schedule-id
	ciString		m_templateId;				///<템플릿 id
	ciString		m_frameId;					///<프레임 id
	//ciString		m_requestid;				///<광고신청 id
	ciString		m_contentsId;				///<contents id
	ciTime			m_startDate;				///<예정시작날짜
	ciTime			m_endDate;					///<예정끝날짜
	ciBoolean		m_openningFlag;				///<무조건 처음부터 시작할것인가, 상황에 따라 중간부터 시작할것인가
	ciShort			m_priority;					///<우선순위(0 : 최고,,,,99: 최하)	==> 나중에 부활 가능
	ciLong			m_castingState;				///<CastingStateEnum
	//ciTime		m_castingStateTime;			///<state 가 변경된 시각
	//ciString		m_castingStateHistory;		///<state 가 변경이력
	//ciString		m_result;					///<실패원인등...
	ciBoolean		m_operationalState;			///<고장여부
	ciBoolean		m_adminState;				///<동작여부
	//ciString		m_phoneNumber;				///<전화연결정보 ipaddress/port
	//ciString		m_comment1;					///<comment1
	//ciString		m_comment2;					///<comment2
	//ciString		m_comment3;					///<comment3

	////////////////////////////////////////////////////////////////////////////
	// default Schedule
	//ciString		m_tuchTime;
	ciString		m_parentScheduleId;			///<parentScheduleId
	ciShort			m_playOrder;				///<방송순서
	ciLong			m_timeScope;				///<TimeScopeEnum

	////////////////////////////////////////////////////////////////////////////
	// Schedule
	ciString		m_startTime;				///<예정시작시간
	ciString		m_endTime;					///<예정끝시간
	//ciTime		m_fromTime;					///<실제시작시간	
	//ciTime		m_toTime;					///<실제끝시간
	//ciBoolean		m_isOrign;					///<송수신여부 1:송신, 0:수신

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciString		m_filename;					///<contents file name
	ciString		m_location;					///<contents location
	ciLong			m_contentsType;				///<
	ciLong			m_contentsState;			///<
	ULONGLONG		m_volume; // filesize		///<
	ciLong			m_runningTime;				///<
	//ciString		m_bgColor;					///<
	//ciString		m_fgColor;					///<
	//ciString		m_font;						///<
	//ciInt			m_fontSize;					///<
	//ciInt			m_playSpeed;				///<
	//ciInt			m_soundVolume;				///<
	//ciStringList	m_promotionValueList;		///<
	ciString		m_contentsName;				///<
	ciString		m_comment[TICKER_COUNT];
	
	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	static	UINT			m_nScheduleID;		///<윈도우 생성시에 사용하는 윈도우 고유 ID
	static	CMapStringToPtr	m_mapSchedule;
	CTime					m_timeStartDate;
	CTime					m_timeEndDate;
	CString					m_strStartTime;		// only schedule
	CString					m_strEndTime;		// only schedule
	CString					m_strMediaFullPath;
	CString					m_strErrorMessage;
	CString					m_strLoadID;
	CFrame*					m_pParentWnd;
	CxImage					m_bgImage;
	int						m_nPlayCount;
	bool					m_bOpen;
	bool					m_bMute;
	bool					m_bPlay;
	bool					m_bLoadFromLocal;
	bool					m_bDefaultSchedule;
	DWORD					m_dwStartTick;

public:

	static CSchedule*		m_pPhoneSchedule;
	static CCriticalSection	m_csSchedulePhone;

	bool	IsDefaultSchedule() { return m_bDefaultSchedule; };
	LPCSTR	GetScheduleId()		{ return m_scheduleId.c_str(); };
	int		GetPlayOrder()		{ return m_playOrder; };
	int		GetTimeScope()		{ return m_timeScope; };
	LPCSTR	GetTemplateId()		{ return m_templateId.c_str(); };
	LPCSTR	GetLoadID()			{ return m_strLoadID; };
	void	ResetPlayCount()	{ m_nPlayCount = 0; };
	int		GetPlayCount()		{ return m_nPlayCount; };

	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime() = 0;
	virtual double	GetCurrentPlayTime() = 0;

	virtual bool	IsUseFile();
	virtual	bool	IsBetween();
	virtual bool	IsOpen() { return m_bOpen; };
	virtual bool	IsPlay() { return m_bPlay; };

	virtual	bool	OpenFile(int nGrade) = 0;
	virtual bool	CloseFile() = 0;
	virtual bool	DeleteFile();
	virtual bool	Play(bool bResume = false);
	virtual bool	Pause() = 0;
	virtual bool	Stop(bool bHide = true);
	virtual bool	Save();
	virtual void	SoundVolumeMute(bool bMute)	{ m_bMute = bMute; };
	virtual	bool	FileUpdate(LPCSTR lpszContentsId, LPCSTR lpszLocation, LPCSTR lpszFilename);


	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnHideContents(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//afx_msg LRESULT OnInvalidateFrame(WPARAM wParam, LPARAM lParam);
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CVideoSchedule
class CVideoSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CVideoSchedule)

public:
	CVideoSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CVideoSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//afx_msg LRESULT	OnCaptionShift(WPARAM wParam, LPARAM lParam);

protected:
	DECLARE_MESSAGE_MAP()

public:
	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciString		m_bgColor;		// 자막용
	ciString		m_fgColor;		// 자막용
	ciString		m_font;			// 자막용
	ciShort			m_fontSize;		// 자막용
	ciShort			m_playSpeed;	// 자막용
	ciShort			m_soundVolume;

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	COLORREF			m_rgbBgColor;
	COLORREF			m_rgbFgColor;
	CBasicInterface*	m_pInterface;
	bool				m_bUseCaption;
	bool				m_bPause;		///<Pause 여부

	int					m_nCaptionWidth;
	int					m_offset;
	int					m_nTimerId;
	int					m_nMS;
	int					m_nVedioRenderMode;					//VideoRender의 종류(1: Overlay, 2:VMR7,....)
	int					m_nVedioOpenType;					//Video open type(1: 초기화과정에 open, 2:매번 플레이시에 open)

	CString			GetCaptionFilename(CString strFilename);
	bool			OpenDSRender(void);						///<Directshow render를 옵션에 맞추어 설정하고 그래프 빌더를 구성한다.
	void			CloseDSRender(void);					///<Directshow render를 종료한다.


public:
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();
	virtual	void	SoundVolumeMute(bool bMute);

protected:
	/*
	// 멀티미디어 타이머
	static void CALLBACK timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2);
	*/
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPictureSchedule
class CPictureSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CPictureSchedule)

public:
	CPictureSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CPictureSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	
protected:
	DECLARE_MESSAGE_MAP()

public:
	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	CPictureEx	m_bgGIF;
	bool		m_bAnimatedGif;
	//DWORD		m_dwStartTick;

	int			m_nType;	// 0=fullscreen, 1=original, 2=fittowindow

public:
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPromotionSchedule
class CProgressBAR : public CWnd
{
	DECLARE_DYNAMIC(CProgressBAR)

public:
	static	CProgressBAR*	GetProgressBARObject(CSchedule* pParentWnd, LPCSTR lpszID);

	CProgressBAR(CSchedule* pParentWnd, LPCSTR lpszID);
	virtual ~CProgressBAR();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();

protected:

	DECLARE_MESSAGE_MAP()

public:
	////////////////////////////////////////////////////////////////////////////
	// IDL defined schedule attributes
	ciString	m_contentsId;

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	ciString	m_fgColor;
	ciString	m_bgColor;
	ciString	m_font;
	ciShort		m_fontSize;

	////////////////////////////////////////////////////////////////////////////
	// ProgressBar attributes
	ciString	m_progressBarId;
	ciString	m_itemName;
	ciShort		m_x;
	ciShort		m_y;
	ciShort		m_width;
	ciShort		m_height;
	ciString	m_borderColor;
	ciShort		m_borderThickness;
	ciBoolean	m_valueVisible;
	ciString	m_fontColor;
	ciLong		m_minValue;
	ciLong		m_maxValue;
	ciLong		m_direction;
	ciShort		m_valueX;
	ciShort		m_valueY;
	ciShort		m_valueWidth;
	ciShort		m_valueHeight;

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	static UINT	m_nProgressBARID;

	CSchedule*	m_pParentWnd;

	COLORREF	m_rgbFgColor;
	COLORREF	m_rgbBgColor;
	COLORREF	m_rgbBorderColor;
	COLORREF	m_rgbFontColor;

	double		m_fScaleX;
	double		m_fScaleY;

	int			m_value;

	CString		m_strErrorMessage;
	CString		m_strLoadID;

public:

	LPCSTR	GetProgressBarId() { return m_progressBarId.c_str(); };
	void	DrawBar(CDC* pDC);
	void	DrawText(CDC* pDC);
	void	SetValue(int value) { m_value=value; };
	void	SetScale(double fScaleX, double fScaleY) { m_fScaleX = fScaleX; m_fScaleY = fScaleY; };
	void	ReposWindow();

	virtual bool	Save();
	LPCSTR			GetLoadID() { return m_strLoadID; };

	CString	m_debugString;
};
typedef CMapStringToPtr	CProgressBARMap;

////////////////////////////////////////////
class CPromotionSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CPromotionSchedule)

public:
	CPromotionSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CPromotionSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();

protected:
	DECLARE_MESSAGE_MAP()

public:

	// attributes
	CStringArray	m_promotionValueList;

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	CProgressBARMap	m_mapProgressBAR;
	bool			LoadProgressBar();
	bool			UpdateProgressBARValue();

public:
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPhoneSchedule
class CPhoneSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CPhoneSchedule)

public:
	CPhoneSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CPhoneSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();

protected:
	DECLARE_MESSAGE_MAP()

public:
	// attributes
	ciString		m_phoneNumber;
	ciBoolean		m_isOrigin;		// only schedule

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	bool			m_bSendRejectMessage;

	CString			m_strPhone;
	CString			m_strPort;

	bool			m_bPlaying;

public:
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CWebSchedule
class CWebCamSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CWebCamSchedule)

public:
	CWebCamSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CWebCamSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();

protected:
	DECLARE_MESSAGE_MAP()

public:
	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	CString			m_strPhone;
	CString			m_strPort;

	bool			m_bPlaying;

public:
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CSMSSchedule
class CSMSSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CSMSSchedule)

public:
	CSMSSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CSMSSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();

	DECLARE_MESSAGE_MAP()

public:

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	//ciString		m_comment[TICKER_COUNT];	//상위 CScheduel 멤버로 올림
	ciString		m_bgColor;
	ciString		m_fgColor;
	ciString		m_font;
	ciShort			m_fontSize;

	ciLong			m_direction;	///<문자방향 가로 0, 세로 1
	ciLong			m_align;		///<정렬방향 1~9

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	
	COLORREF		m_rgbBgColor;
	COLORREF		m_rgbFgColor;
	CFont			m_fontSMS;
	CString			m_strSMS;
	int				m_nLineCount;			///<Text의 라인 수

	virtual bool	CreateFile();

public:
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHorizontalTickerSchedule
class CHorizontalTickerSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CHorizontalTickerSchedule)

public:
	CHorizontalTickerSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CHorizontalTickerSchedule();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	afx_msg LRESULT OnCaptionShift(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();

public:

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	//ciString		m_comment[TICKER_COUNT];		//상위 CScheduel 멤버로 올림
	ciString		m_bgColor;
	ciString		m_fgColor;
	ciString		m_font;
	ciShort			m_fontSize;
	ciShort			m_playSpeed;

	ciLong			m_direction;		///<문자방향 가로 0, 세로 1

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	COLORREF		m_rgbBgColor;
	COLORREF		m_rgbFgColor;

	int				m_offset;
	CFont			m_fontTicker;
	CString			m_strTicker;
	CStringW		m_strTickerW;
	int 			m_nTickerWidth;
	int				m_nWidth;
	int				m_nHeight;

	int 			m_nTimerId;
	int				m_nShiftStep;			///<티커를 움직이는 픽셀

	bool			m_bUseUnicode;
	CStringW		m_commentW[TICKER_COUNT];

	virtual bool	CreateFile();

	static void CALLBACK timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2);
	static UINT	CaptionShiftThread(LPVOID param);

public:
	int					m_nMS;
	bool				m_bExitThread;		///<Thread를 종료하는지 여부 플래그
	HANDLE				m_hevtShift;		///<Thread 종료 이벤트 핸들
	CWinThread*			m_pthreadShift;		///<Ticker를 움직이는 offest 조정 쓰레드
	CCriticalSection	m_csOffset;			///<Offeset값 동기화 객체
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();
	void			DecreaseOffset(void);					///<Offset 값을 줄인다.
	void			GetOffset(int* pnOffset);				///<설정된 offset 값을 반환한다.


};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CVerticalTickerSchedule
class CVerticalTickerSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CVerticalTickerSchedule)

public:
	CVerticalTickerSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CVerticalTickerSchedule();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	afx_msg LRESULT OnCaptionShift(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();

public:

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	//ciString		m_comment[TICKER_COUNT];		//상위 CScheduel 멤버로 올림
	ciString		m_bgColor;
	ciString		m_fgColor;
	ciString		m_font;
	ciShort			m_fontSize;
	ciShort			m_playSpeed;

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	COLORREF		m_rgbBgColor;
	COLORREF		m_rgbFgColor;

	int				m_nTickerIndex;
	int				m_nMaxTicker;
	int				m_nDelayCount;
	int				m_offset;
	CFont			m_fontTicker;
	CString			m_strTicker1;
	CString			m_strTicker2;
	int				m_nWidth;
	int				m_nHeight;

	int 			m_nTimerId;
	int				m_nMS;

	virtual bool	CreateFile();

	static void CALLBACK timeProc(UINT nTimerId, UINT msg, DWORD dwUser, DWORD dwParam1, DWORD dwParam2);

public:
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTypingTickerSchedule
class CTypingTickerSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CTypingTickerSchedule)

public:
	CTypingTickerSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CTypingTickerSchedule();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnCaptionShift(WPARAM wParam, LPARAM lParam);

public:

	////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	//ciString		m_comment[TICKER_COUNT];		//상위 CScheduel 멤버로 올림
	ciString		m_bgColor;
	ciString		m_fgColor;
	ciString		m_font;
	ciShort			m_fontSize;
	ciShort			m_playSpeed;

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	COLORREF		m_rgbBgColor;
	COLORREF		m_rgbFgColor;

	CFont			m_fontTicker;
	int				m_nCommentCount;
	int				m_nCommentIndex;
	CString			m_wstrTickerShow;
	CString			m_wstrTickerSource;
	int				m_nTickerIndex;
	CRect			m_rectTicker;

	int				m_nMS;

	virtual bool	CreateFile();

public:
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CURLSchedule
class CURLSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CURLSchedule)

public:
	CURLSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CURLSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	void DocumentCompleteWebBrowser(LPDISPATCH pDisp, VARIANT* URL);
	void NavigateComplete2(LPDISPATCH pDisp, VARIANT* URL);
	void NavigateError(LPDISPATCH pDisp, VARIANT* pvURL, VARIANT* pvFrame, VARIANT* pvStatusCode, VARIANT_BOOL* pvbCancel);

protected:
	DECLARE_MESSAGE_MAP()
	DECLARE_EVENTSINK_MAP()

public:
////////////////////////////////////////////////////////////////////////////
	// Contents attributes
	//ciString		m_comment[10];		//상위 CScheduel 멤버로 올림

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	CWeb_browser	m_WebBrowser;
	bool			m_bNavigateError;		///<Navigate error 여부
	int				m_nErrorCode;			///<Navigate error code

	virtual CString	GetFilePath();
	virtual bool	CreateFile();

public:
	virtual void	SetSchedule(LPCSTR lpszID);

	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CFlashSchedule
class FLASH_OBJECT
{
public:
	FLASH_OBJECT() {};
	virtual ~FLASH_OBJECT() {};

	CString		filename;
	CString		fullPath;
	CString		tempfullPath;
	int			prob_min;
	int			prob_max;
	int			max_count;
	int			cur_count;

	bool		CheckCount();
	bool		IncreaseCount();

	void		Save();
	void		Load();
};
typedef CArray<FLASH_OBJECT, FLASH_OBJECT&>		FLASH_OBJECT_LIST;

class CFlashSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CFlashSchedule)

public:
	CFlashSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CFlashSchedule();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();

protected:
	DECLARE_MESSAGE_MAP()

public:

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values

	bool				m_bEventMode;
	int					m_nMaxProbability;
	FLASH_OBJECT_LIST	m_flashList;

	bool				m_bPlaying;

	CShockwaveFlash*	m_pFlash;
	CCriticalSection	m_csFlash;

	virtual CString	GetFilePath();
	virtual bool	CreateFile();

public:

	static	CMapStringToString	g_mapFlashObject;
	virtual void	SetSchedule(LPCSTR lpszID);
	virtual double	GetTotalPlayTime();
	virtual double	GetCurrentPlayTime();

	virtual	bool	OpenFile(int nGrade);
	virtual bool	CloseFile();

	virtual bool	Play(bool bResume = false);
	virtual bool	Pause();
	virtual bool	Stop(bool bHide = true);

	virtual bool	Save();
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CRSSSchedule
class CRSSSchedule : public CFlashSchedule
{
	DECLARE_DYNAMIC(CRSSSchedule)

public:
	CRSSSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CRSSSchedule();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()

public:
	ciShort			m_width;			// contents width (pixel)
	ciShort			m_height;			// contents height (pixel)
	ciString		m_bgColor;			///<
	ciString		m_fgColor;			///<
	//ciString		m_font;				///<
	ciShort			m_fontSize;			///<
	//ciShort			m_currentComment;	// 현재 문자가 들어가야할 자리번호

	//CString			m_strRSS;
	CString			m_strRssFlashPath;	///<RSS 플래쉬 파일의 경로

public:
	virtual void	SetSchedule(LPCSTR lpszID);

	bool			CreateFile();

	virtual bool	Play();
	virtual bool	Save();
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CWzardchedule
class CWizardSchedule : public CFlashSchedule
{
	DECLARE_DYNAMIC(CWizardSchedule)

public:
	CWizardSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);
	virtual ~CWizardSchedule();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()

public:
	ciString wizardXML;		// wizard 용 XML string
	ciString wizardFiles;	// wizard 에 부속되는 파일의 리스트 (자유형식 : 어차피 Studio 에서만 참조함)

	CString			m_strWizardFlashPath;	///<Wizard 플래쉬 파일의 경로

	virtual LPCSTR	GetContentsTypeString() { return "Wizard"; };

	virtual void	SetSchedule(LPCSTR lpszID);

	bool			CreateFile();

	virtual bool	Play();
	virtual bool	Save();
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CPowerPointSchedule

//! MS-Office Power Point contents를 재생하는 클래스
/*!
*/
class CPowerPointSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CPowerPointSchedule)

public:
	CPowerPointSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);		///<로컬 모드에서의 생성자
	virtual ~CPowerPointSchedule();														///<소멸자

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);															///<WM_CREATE 이벤트 함수
	afx_msg void OnPaint();																							///<WM_PAINT 이벤트 함수
	afx_msg void OnDestroy();																						///<WM_DESTORY 이벤트 함수

protected:
	DECLARE_MESSAGE_MAP()

public:

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	bool		m_bSlideShowEnd;													///<슬라이드 쇼가 끝난는지 여부를 나타내는 플래그
	//DWORD		m_dwStartTick;														///<schedule의 시작 tick 값
	CString		m_strPPTViewPath;													///<PPTView 프로그램의 실제 경로
	CFont		m_font;																///<배경에 뿌려주는 글자 폰트
	CString		m_strLog;															///<배경에 뿌려지는 문자열

public:
	
	virtual LPCSTR	GetContentsTypeString() { return "MS-Office Power Point"; };	///<schedule의 contents type을 문자열로 반환하는 함수

	virtual void	SetSchedule(LPCSTR lpszID);										///<로컬 모드에서 schedule을 설정하는 함수
	virtual double	GetTotalPlayTime();												///<contents의 전체 재생시간을 반환하는 함수
	virtual double	GetCurrentPlayTime();											///<contens의 현재 재생경과 시간을 반환하는 함수

	virtual	bool	OpenFile(int nGrade);											///<contents 파일을 open하는 함수
	virtual bool	CloseFile();													///<contents 파일을 close 하는 함수

	virtual bool	Play(bool bResume = false);										///<schedule을 play하는 함수
	virtual bool	Pause();														///<schedule을 일시 정지 하는 함수
	virtual bool	Stop(bool bHide = true);										///<schedule을 정지하는 함수

	virtual bool	Save();															///<shcedule의 내용을 ini 파일로 저장하는 함수
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHwpSchedule

//! HWP contents를 재생하는 클래스
/*!
*/
class CHwpSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CHwpSchedule)

public:
	CHwpSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);			///<로컬 모드에서의 생성자
	virtual ~CHwpSchedule();														///<소멸자

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);							///<WM_CREATE 이벤트 함수
	afx_msg void OnPaint();															///<WM_PAINT 이벤트 함수
	afx_msg void OnDestroy();														///<WM_DESTORY 이벤트 함수

protected:
	DECLARE_MESSAGE_MAP()

public:

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	bool		m_bSlideShowEnd;													///<슬라이드 쇼가 끝난는지 여부를 나타내는 플래그
	//DWORD		m_dwStartTick;														///<schedule의 시작 tick 값
	CString		m_strHwpViewPath;													///<HwpView 프로그램의 실제 경로
	CFont		m_font;																///<배경에 뿌려주는 글자 폰트
	CString		m_strLog;															///<배경에 뿌려지는 문자열

public:
	
	virtual LPCSTR	GetContentsTypeString() { return "Hansoft Hangul"; };			///<schedule의 contents type을 문자열로 반환하는 함수

	virtual void	SetSchedule(LPCSTR lpszID);										///<로컬 모드에서 schedule을 설정하는 함수
	virtual double	GetTotalPlayTime();												///<contents의 전체 재생시간을 반환하는 함수
	virtual double	GetCurrentPlayTime();											///<contens의 현재 재생경과 시간을 반환하는 함수

	virtual	bool	OpenFile(int nGrade);											///<contents 파일을 open하는 함수
	virtual bool	CloseFile();													///<contents 파일을 close 하는 함수

	virtual bool	Play(bool bResume = false);										///<schedule을 play하는 함수
	virtual bool	Pause();														///<schedule을 일시 정지 하는 함수
	virtual bool	Stop(bool bHide = true);										///<schedule을 정지하는 함수

	virtual bool	Save();															///<shcedule의 내용을 ini 파일로 저장하는 함수

	//virtual CString	ToString();													///<schedule의 정보를 문자열로 반환하는 함수
	//virtual void ToGrid(CPropertyGrid* pGrid);									///<shcedule의 정보 문자열을 logdialog에 설정하는 함수 
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CExcelSchedule

//! Excel contents를 재생하는 클래스
/*!
*/
class CExcelSchedule : public CSchedule
{
	DECLARE_DYNAMIC(CExcelSchedule)

public:
	CExcelSchedule(CFrame* pParentWnd, LPCSTR lpszID, bool bDefaultSchedule);		///<로컬 모드에서의 생성자
	virtual ~CExcelSchedule();														///<소멸자

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);							///<WM_CREATE 이벤트 함수
	afx_msg void OnPaint();															///<WM_PAINT 이벤트 함수
	afx_msg void OnDestroy();														///<WM_DESTORY 이벤트 함수

protected:
	DECLARE_MESSAGE_MAP()

public:

	////////////////////////////////////////////////////////////////////////////
	// BRW define member values
	bool		m_bSlideShowEnd;													///<슬라이드 쇼가 끝난는지 여부를 나타내는 플래그
	//DWORD		m_dwStartTick;														///<schedule의 시작 tick 값
	CString		m_strExcelViewPath;													///<ExcelView 프로그램의 실제 경로
	CFont		m_font;																///<배경에 뿌려주는 글자 폰트
	CString		m_strLog;															///<배경에 뿌려지는 문자열

public:
	
	virtual LPCSTR	GetContentsTypeString() { return "MS-Office Excel/Hansoft Nexcel"; };	///<schedule의 contents type을 문자열로 반환하는 함수

	virtual void	SetSchedule(LPCSTR lpszID);										///<로컬 모드에서 schedule을 설정하는 함수
	virtual double	GetTotalPlayTime();												///<contents의 전체 재생시간을 반환하는 함수
	virtual double	GetCurrentPlayTime();											///<contens의 현재 재생경과 시간을 반환하는 함수

	virtual	bool	OpenFile(int nGrade);											///<contents 파일을 open하는 함수
	virtual bool	CloseFile();													///<contents 파일을 close 하는 함수

	virtual bool	Play(bool bResume = false);										///<schedule을 play하는 함수
	virtual bool	Pause();														///<schedule을 일시 정지 하는 함수
	virtual bool	Stop(bool bHide = true);										///<schedule을 정지하는 함수

	virtual bool	Save();															///<shcedule의 내용을 ini 파일로 저장하는 함수

	//virtual CString	ToString();													///<schedule의 정보를 문자열로 반환하는 함수
	//virtual void ToGrid(CPropertyGrid* pGrid);									///<shcedule의 정보 문자열을 logdialog에 설정하는 함수 
};

} // 매니져용

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define		SMS_HEADER		"<html><body background=\"%s\"><span style=\"font-family:%s; font-size:%dpt; color:%s; background-color:%s; \">\n"
#define		SMS_TAIL		"\n</span></body></html>"

#define		TICKER_HEADER	"\n\
<html><head>\n\
<style type=\"text/css\">\n\
#pscroller { width: %dpx; height: %dpx; border: 0px solid black; padding: 0px; }\n\
.someclass{ }\n\
</style>\n\
<script type=\"text/javascript\">\n\
var pausecontent=new Array()\n"

#define		TICKER_CONTENT	"pausecontent[%d]='<center><span style=\"font-family:%s; font-size:%dpt; color:%s; background-color:%s; \">%s</span></center>'\n"

#define		TICKER_TAIL		"\n\
</script>\n\
<script type=\"text/javascript\">\n\
function pausescroller(content, divId, divClass, delay){\n\
this.content=content\n\
this.tickerid=divId\n\
this.delay=delay\n\
this.mouseoverBol=0\n\
this.hiddendivpointer=1\n\
document.write('<div id=\"'+divId+'\" class=\"'+divClass+'\" style=\"position: relative; overflow: hidden\"><div class=\"innerDiv\" style=\"position: absolute; width: 100%%\" id=\"'+divId+'1\">'+content[0]+'</div><div class=\"innerDiv\" style=\"position: absolute; width: 100%%; visibility: hidden\" id=\"'+divId+'2\">'+content[1]+'</div></div>')\n\
var scrollerinstance=this\n\
if (window.addEventListener)\n\
window.addEventListener(\"load\", function(){scrollerinstance.initialize()}, false)\n\
else if (window.attachEvent)\n\
window.attachEvent(\"onload\", function(){scrollerinstance.initialize()})\n\
else if (document.getElementById)\n\
setTimeout(function(){scrollerinstance.initialize()}, 500)\n\
}\n\
\n\
pausescroller.prototype.initialize=function(){\n\
this.tickerdiv=document.getElementById(this.tickerid)\n\
this.visiblediv=document.getElementById(this.tickerid+\"1\")\n\
this.hiddendiv=document.getElementById(this.tickerid+\"2\")\n\
this.visibledivtop=parseInt(pausescroller.getCSSpadding(this.tickerdiv))\n\
this.visiblediv.style.width=this.hiddendiv.style.width=this.tickerdiv.offsetWidth-(this.visibledivtop*2)+\"px\"\n\
this.getinline(this.visiblediv, this.hiddendiv)\n\
this.hiddendiv.style.visibility=\"visible\"\n\
var scrollerinstance=this\n\
document.getElementById(this.tickerid).onmouseover=function(){scrollerinstance.mouseoverBol=1}\n\
document.getElementById(this.tickerid).onmouseout=function(){scrollerinstance.mouseoverBol=0}\n\
if (window.attachEvent)\n\
window.attachEvent(\"onunload\", function(){scrollerinstance.tickerdiv.onmouseover=scrollerinstance.tickerdiv.onmouseout=null})\n\
setTimeout(function(){scrollerinstance.animateup()}, this.delay)\n\
}\n\
\n\
pausescroller.prototype.animateup=function(){\n\
var scrollerinstance=this\n\
if (parseInt(this.hiddendiv.style.top)>(this.visibledivtop+5)){\n\
this.visiblediv.style.top=parseInt(this.visiblediv.style.top)-5+\"px\"\n\
this.hiddendiv.style.top=parseInt(this.hiddendiv.style.top)-5+\"px\"\n\
setTimeout(function(){scrollerinstance.animateup()}, 50)\n\
}\n\
else{\n\
this.getinline(this.hiddendiv, this.visiblediv)\n\
this.swapdivs()\n\
setTimeout(function(){scrollerinstance.setmessage()}, this.delay)\n\
}\n\
}\n\
\n\
pausescroller.prototype.swapdivs=function(){\n\
var tempcontainer=this.visiblediv\n\
this.visiblediv=this.hiddendiv\n\
this.hiddendiv=tempcontainer\n\
}\n\
\n\
pausescroller.prototype.getinline=function(div1, div2){\n\
div1.style.top=this.visibledivtop+\"px\"\n\
div2.style.top=Math.max(div1.parentNode.offsetHeight, div1.offsetHeight)+\"px\"\n\
}\n\
\n\
pausescroller.prototype.setmessage=function(){\n\
var scrollerinstance=this\n\
if (this.mouseoverBol==1)\n\
setTimeout(function(){scrollerinstance.setmessage()}, 100)\n\
else{\n\
var i=this.hiddendivpointer\n\
var ceiling=this.content.length\n\
this.hiddendivpointer=(i+1>ceiling-1)? 0 : i+1\n\
this.hiddendiv.innerHTML=this.content[this.hiddendivpointer]\n\
this.animateup()\n\
}\n\
}\n\
\n\
pausescroller.getCSSpadding=function(tickerobj){\n\
if (tickerobj.currentStyle)\n\
return tickerobj.currentStyle[\"paddingTop\"]\n\
else if (window.getComputedStyle)\n\
return window.getComputedStyle(tickerobj, \"\").getPropertyValue(\"padding-top\")\n\
else\n\
return 0\n\
}\n\
\n\
</script>\n\
</head>\n\
\n\
<body background=\"%s\">\n\
<script type=\"text/javascript\">\n\
new pausescroller(pausecontent, \"pscroller\", \"someclass\", %d)\n\
</script>\n\
</body>\n\
</html>"
