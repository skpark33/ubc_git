#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "UBCCopCommon\CopModule.h"


// CSiteUserView 폼 뷰입니다.

class CSiteUserView : public CFormView
{
	DECLARE_DYNCREATE(CSiteUserView)

protected:
	CSiteUserView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CSiteUserView();

	enum { eSiteCheck // 2010.02.17 by gwangsoo   // 2012.12.5 skpark insert eMgrID
		 , eSiteName, eSiteID, eMgrID, eSiteTel, eSiteAddr, eSiteMan, eSiteManTel, eSiteRemark, eSiteEnd };

	enum { eUserCheck // 2010.02.17 by gwangsoo
		 , eUserSite, eUserID, eUserName, eUserPerm, eRole, eValidDate, eUserTel, eUserMail
		 , eUseAlarm, eAlarmList, eSiteList, eHostList, eUserEnd };

	ChildSiteInfoList m_lsChildSite;
	SiteInfoList	m_lsSite;
	void			InitSiteList();
	void			RefreshSiteList(bool reget=false);
	void			UpdateSiteRow(int nRow, SSiteInfo& info);
	void			AddSite();
	void			DeleteSite();
	void			ModifySite();

	bool			_isMyLine(SChildSiteInfo& info);


	CMapStringToPtr	m_mapChildSite;
	SChildSiteInfo* GetSiteInfoFromMap(CString strSiteId);
	void			SetSiteInfoFromMap(CString strSiteId, SChildSiteInfo* pSiteInfo);
	HTREEITEM		FindTreeItem(HTREEITEM item, CString strSiteId);
	int				getMyChildren(CString parentId, SiteInfoList& children); // skpark 2012.11.13 m_lsSite  로부터 자신의 자식만 가져온다.

	UserInfoList	m_lsUser;
	void			InitUserList();
	void			RefreshUserList(bool reget=false);
	void			UpdateUserRow(int nRow, SUserInfo& info);
	void			AddUser();
	void			DeleteUser();
	void			ModifyUser();

	void			RefreshTree();

	CRect			m_rcClient;
	CReposControl	m_Reposition;
	void			InitPosition(CRect);

	RoleInfoList	m_lsRole;
	CString			GetRoleName(CString strRoleId);

	void			InitButtonState();

public:
	enum { IDD = IDD_SITEUSER_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	SChildSiteInfo  m_mySiteInfo;

	CTreeCtrl		m_treeSite;
	CTabCtrl		m_tabMain;
	CHoverButton	m_bnRefresh;
	CHoverButton	m_bnAdd;
	CHoverButton	m_bnDel;
	CHoverButton	m_bnMod;
	CHoverButton	m_btnExcelSave;
	CUTBListCtrlEx	m_lcSite;
	CUTBListCtrlEx	m_lcUser;
	CButton			m_kbIncludeChild;
	CImageList			m_ilTreeImage;

	virtual void OnInitialUpdate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnBnClickedButtonMod();
	afx_msg void OnNMDblclkLcSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkLcUser(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedKbIncludeChild();
	afx_msg void OnTcnSelchangeTabMain(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTvnSelchangedTreeSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBnToExcel();

	afx_msg void OnTvnBegindragTreeSite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

protected:

	CImageList*			m_pDragImage;		// 드래그시 생성된 이미지 사용
	HTREEITEM			m_hDragItem;		// 드래그시 처음 선택된 아이템 핸들 기억용
	BOOL				IsFitChildNode(HTREEITEM hSrcItem, HTREEITEM hChildItem);
	BOOL				IsFitChildNode(HTREEITEM hSrcItem, DWORD_PTR pInItemData);
	HTREEITEM			FindChild(HTREEITEM hParent, DWORD_PTR pFindData);
	void				ChangeParent(HTREEITEM hParentItem, HTREEITEM hItem, SChildSiteInfo* pChildSiteInfo);
	SSiteInfo*			GetTreeItemInfo(HTREEITEM hItem, int& nContentsIndex);

	bool	m_bSiteParentChanged;
public:
	CButton m_checkTreeEdit;
	CButton m_btSaveTree;
	CButton m_btCancelTree;
	afx_msg void OnBnClickedTreeEditCheck();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonCancel();

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};


