// AutoUpdateConfigDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AutoUpdateConfigDlg.h"

// CAutoUpdateConfigDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CAutoUpdateConfigDlg, CDialog)

CAutoUpdateConfigDlg::CAutoUpdateConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAutoUpdateConfigDlg::IDD, pParent)
	, m_bIsAutoUpdateFlag(FALSE)
{

}

CAutoUpdateConfigDlg::~CAutoUpdateConfigDlg()
{
}

void CAutoUpdateConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_KB_IS_AUTO_UPDATE, m_kbIsAutoUpdate);
	DDX_Control(pDX, IDC_DT_AUTO_UPDATE_TIME, m_dtAutoUpdateTime);
}


BEGIN_MESSAGE_MAP(CAutoUpdateConfigDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAutoUpdateConfigDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CAutoUpdateConfigDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CAutoUpdateConfigDlg 메시지 처리기입니다.

void CAutoUpdateConfigDlg::OnBnClickedOk()
{
	// BST_INDETERMINATE 리턴값이 이것이면 선택되지 않은것으로 본다
	m_bIsAutoUpdateFlag = m_kbIsAutoUpdate.GetCheck();

	m_strAutoUpdateTime = _T("NOSET");
	CTime tmAutoUpdateTime;
	if(m_dtAutoUpdateTime.GetTime(tmAutoUpdateTime) == GDT_VALID)
	{
		m_strAutoUpdateTime = tmAutoUpdateTime.Format(_T("%H:%M"));
	}

	OnOK();
}

void CAutoUpdateConfigDlg::OnBnClickedCancel()
{
	OnCancel();
}

BOOL CAutoUpdateConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 선택되지 않은 상태로 설정
	m_kbIsAutoUpdate.SetCheck(m_bIsAutoUpdateFlag);
	m_dtAutoUpdateTime.SetFormat(_T("HH:mm"));

	if(m_strAutoUpdateTime.IsEmpty() || m_strAutoUpdateTime == _T("NOSET") || m_strAutoUpdateTime.GetLength() < 5)
	{
		// 날짜를 선택하지 않은 상태로 설정
		m_dtAutoUpdateTime.SetTime();
	}
	else
	{
		CTime tmTmp = CTime::GetCurrentTime();
		CTime tmAutoUpdateTime(tmTmp.GetYear()
							 , tmTmp.GetMonth()
							 , tmTmp.GetDay()
							 , _ttoi(m_strAutoUpdateTime.Left(2))
							 , _ttoi(m_strAutoUpdateTime.Right(2))
							 , 0
							 );
		m_dtAutoUpdateTime.SetTime(&tmAutoUpdateTime);
	}

	InitDateRange(m_dtAutoUpdateTime);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
