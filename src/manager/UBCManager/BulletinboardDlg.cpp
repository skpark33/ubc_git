// BulletinboardDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "BulletinboardDlg.h"
#include "common\reposcontrol.h"
#include "common\libscratch\scratchUtil.h"


// CBulletinboardDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CBulletinboardDlg, CDialog)

CBulletinboardDlg::CBulletinboardDlg(OPEN_TYPE nOpenType, CWnd* pParent /*=NULL*/)
	: CDialog(CBulletinboardDlg::IDD, pParent)
,	m_Reposition(this)
,   m_nOpenType(nOpenType)
{

}

CBulletinboardDlg::~CBulletinboardDlg()
{
}

void CBulletinboardDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IE_BULLETINBOARD, m_ieBulletinBoard);
	DDX_Control(pDX, IDC_ABOUT_VERSION, m_txtVersion);
}


BEGIN_MESSAGE_MAP(CBulletinboardDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CBulletinboardDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CBulletinboardDlg::OnBnClickedCancel)
	ON_WM_SIZE()
	ON_WM_CREATE()
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CBulletinboardDlg, CDialog)
	ON_EVENT(CBulletinboardDlg, IDC_IE_BULLETINBOARD, 263, CBulletinboardDlg::WindowClosingIeBulletinboard, VTS_BOOL VTS_PBOOL)
END_EVENTSINK_MAP()


// CBulletinboardDlg 메시지 처리기입니다.

void CBulletinboardDlg::OnBnClickedOk()
{
	OnOK();
}

void CBulletinboardDlg::OnBnClickedCancel()
{
	OnCancel();
}

int CBulletinboardDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}

BOOL CBulletinboardDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitPosition(m_rcClient);

	CString strVersion;

	std::string sVer;
	scratchUtil::getInstance()->getVersion(sVer, false);

	if(CEnviroment::eSQISOFT == GetEnvPtr()->m_Customer)
	{
		strVersion.Format(" Manager Version %s  Copyright (C) 2009 SQI Soft Corporation.", sVer.c_str());
	}
	else
	{
		strVersion.Format(" Manager Version %s", sVer.c_str());
	}

	m_txtVersion.SetWindowText(strVersion);

	CString strEntry = _T("UNKNOWN");
	if     (CEnviroment::eKIA     == GetEnvPtr()->m_Customer) strEntry = _T("KIA");
	else if(CEnviroment::eHYUNDAI == GetEnvPtr()->m_Customer) strEntry = _T("HYUNDAI");
	else if(CEnviroment::eKMNL    == GetEnvPtr()->m_Customer) strEntry = _T("KMNL");
	else if(CEnviroment::eKMCL    == GetEnvPtr()->m_Customer) strEntry = _T("KMCL");
	else if(!GetEnvPtr()->m_strCustomer.IsEmpty()) strEntry = GetEnvPtr()->m_strCustomer;

	TCHAR szBuf[2048] = {0};
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, UBCURL_INI);
	GetPrivateProfileString(strEntry, _T("BASE"), "", szBuf, 2048, szPath); CString strBaseUrl(szBuf);

	CString strSubUrl;
	if(m_nOpenType == NOTICE_POPUP)
	{
		GetPrivateProfileString(strEntry, _T("NOTICE_POPUP"), "", szBuf, 2048, szPath); strSubUrl = szBuf;
	}
	else if(m_nOpenType == NOTICE_LIST)
	{
		GetPrivateProfileString(strEntry, _T("NOTICE_LIST"), "", szBuf, 2048, szPath); strSubUrl = szBuf;
	}

	CString strUrl;
	if(strBaseUrl.IsEmpty() || strSubUrl.IsEmpty())
	{
		strUrl = _T("about:blank");
		OnCancel();
	}
	else
	{
		strUrl.Format(_T("%s?GUBUN=%s&SITE=%s&ID=%s&PW=%s&PROGRAM=%s")
					, strBaseUrl
					, AsciiToBase64ForWeb(_T("UBC"))
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szSite)
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szLoginID)
					, AsciiToBase64ForWeb(GetEnvPtr()->m_szPassword)
					, AsciiToBase64ForWeb(strSubUrl)
					);
	}

	m_ieBulletinBoard.Navigate(strUrl, NULL, NULL, NULL, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CBulletinboardDlg::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_ieBulletinBoard, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_txtVersion, REPOS_FIX, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
}

void CBulletinboardDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CBulletinboardDlg::WindowClosingIeBulletinboard(BOOL IsChildWindow, BOOL* Cancel)
{
	// 브라우져의 종료확인 메세지를 안나오게 한다.
	*Cancel = TRUE;
	OnOK();
}
