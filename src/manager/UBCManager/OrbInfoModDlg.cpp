// OrbInfoModDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "OrbInfoModDlg.h"
#include "common\libscratch\orbconn.h"

// COrbInfoModDlg dialog

IMPLEMENT_DYNAMIC(COrbInfoModDlg, CDialog)

COrbInfoModDlg::COrbInfoModDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COrbInfoModDlg::IDD, pParent)
	, m_nPort(0)
{
	m_bModify = false;
	m_szName.Empty();
	m_szIP.Empty();
	m_szPort = _T("14109");
	m_plscCur = NULL;
}

COrbInfoModDlg::~COrbInfoModDlg()
{
}

void COrbInfoModDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_NAME, m_szName);
	DDX_Text(pDX, IDC_CI_IPADDR, m_szIP);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDV_MinMaxInt(pDX, m_nPort, 0, 65535);
}

BEGIN_MESSAGE_MAP(COrbInfoModDlg, CDialog)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &COrbInfoModDlg::OnBnClickedOk)
END_MESSAGE_MAP()
// COrbInfoModDlg message handlers
BOOL COrbInfoModDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_nPort = atoi(m_szPort);

	UpdateData(FALSE);

	((CEdit*)GetDlgItem(IDC_EDIT_NAME))->SetReadOnly(m_bModify);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void COrbInfoModDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void COrbInfoModDlg::OnBnClickedOk()
{
	UpdateData();

	m_szName.Trim();
	if(m_szName.IsEmpty())
	{
		GetDlgItem(IDC_EDIT_NAME)->SetFocus();
		UbcMessageBox(LoadStringById(IDS_ORBINFODLG_MSG005));
		return;
	}

	if(m_plscCur){
		for(int nRow = 0; nRow < m_plscCur->GetItemCount(); nRow++){
			SOrbConnInfo* pInfo = (SOrbConnInfo*)m_plscCur->GetItemData(nRow);
			if(!pInfo)	continue;
			if(m_szName == pInfo->name){
				CString szMsg;
				szMsg.Format(LoadStringById(IDS_ORBINFOMODDLG_MSG001), m_szName);
				UbcMessageBox(szMsg);
				return;
			}
		}
	}

	m_szPort.Format("%d", m_nPort);

	OnOK();
}

void COrbInfoModDlg::SetList(CUTBListCtrl* pList)
{
	m_plscCur = pList;
}

void COrbInfoModDlg::SetOrbInfo(LPCTSTR szName, LPCTSTR szIP, int nPort)
{
	m_szName = szName;
	m_szIP = szIP;
	m_nPort = nPort;
	m_szPort = ToString(m_nPort);

	if(IsWindow(m_hWnd)){
		UpdateData(FALSE);
	}
}

void COrbInfoModDlg::GetOrbInfo(CString& szName, CString& szIP, int& nPort)
{
	if(IsWindow(m_hWnd)){
		UpdateData();
	}

	szName = m_szName;
	szIP = m_szIP;
	nPort = m_nPort;
}