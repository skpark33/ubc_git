#pragma once

#include "common\UTBListCtrl.h"
#include "afxwin.h"

class CHsrCommandDlg;

// COrbInfoDlg dialog
class COrbInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(COrbInfoDlg)

public:
	COrbInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COrbInfoDlg();

// Dialog Data
	enum { IDD = IDD_CONN_INFO };
	enum { eColName, eColIP, eColPort, eColEnd };

	void SetOrbInfo(LPCTSTR szName, LPCTSTR szIP, int nPort);
	void GetOrbInfo(CString& szName, CString& szIP, int& nPort);
	void SelectCurInfo();

	bool m_bQuit;

protected:
	void InitList();
	void InsertData();
//	void SaveIni(SOrbInfo*);

	static int CALLBACK CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

	CImageList m_ilPackage;
	CUTBListCtrl m_lsctrOrbInfo;
	CString m_szColum[eColEnd];

	CString m_szName;
	CString m_szIP;
	int m_nPort;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonCiAdd();
	afx_msg void OnBnClickedButtonCiDel();
	afx_msg void OnBnClickedButtonCiMod();
	afx_msg void OnBnClickedOk();
	afx_msg void OnLvnColumnclickListOrbInfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawListOrbInfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListOrbInfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListOrbInfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonCiTest();
	afx_msg void OnBnClickedButtonServerRestart();

	CButton m_btServerRestart;
	CHsrCommandDlg*			m_pHsrCommand;

};
