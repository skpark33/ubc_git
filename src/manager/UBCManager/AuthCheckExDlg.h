#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "afxtempl.h"


#define		LICENSE_COMP_MGRID			0x0001
#define		LICENSE_COMP_SITEID			0x0002
#define		LICENSE_COMP_HOSTID			0x0004
#define		LICENSE_COMP_HOSTNAME		0x0008
#define		LICENSE_COMP_IPADDRESS		0x0010
#define		LICENSE_COMP_MACADDRESS		0x0020
#define		LICENSE_COMP_EDITION		0x0040
#define		LICENSE_COMP_AUTHDATE		0x0080
#define		LICENSE_COMP_ENTKEY			0x0100


// CAuthCheckExDlg 대화 상자입니다.

class CAuthCheckExDlg : public CDialog
{
protected:
	//
	enum LICENSE_PROC_TYPE { eNothing=0, eAdding, eUpdating, eDeleting };
	LPCSTR GetProcTypeString(LICENSE_PROC_TYPE type)
	{
		switch(type)
		{
		case eAdding:	return "신규 추가"; break;
		case eUpdating:	return "업데이트"; break;
		case eDeleting:	return "삭제"; break;
		default:
		case eNothing:	break;
		}
		return "없음";
	};

	//
	class LICENSE_INFO
	{
	public:
		static DWORD compareMask;

		LICENSE_INFO() { eProcType=eNothing; };
		~LICENSE_INFO() {};

		CString mgrId;
		CString siteId;
		CString hostId;
		CString hostName;
		CString ipAddress;
		CString macAddress;
		CString edition;
		CString authDate;
		CString enterpriseKey;
		LICENSE_PROC_TYPE eProcType;

		bool operator!= (const LICENSE_INFO& info)
		{
			if( compareMask & LICENSE_COMP_MGRID && mgrId != info.mgrId ) return true;
			if( compareMask & LICENSE_COMP_SITEID && siteId != info.siteId ) return true;
			if( compareMask & LICENSE_COMP_HOSTID && hostId != info.hostId ) return true;
			if( compareMask & LICENSE_COMP_HOSTNAME && hostName != info.hostName ) return true;
			if( compareMask & LICENSE_COMP_IPADDRESS && ipAddress != info.ipAddress ) return true;
			if( compareMask & LICENSE_COMP_MACADDRESS && macAddress != info.macAddress ) return true;
			if( compareMask & LICENSE_COMP_EDITION && edition != info.edition ) return true;
			if( compareMask & LICENSE_COMP_AUTHDATE && authDate != info.authDate ) return true;
			if( compareMask & LICENSE_COMP_ENTKEY && enterpriseKey != info.enterpriseKey ) return true;

			return false;
		}

		CString toString()
		{
			char buf[4096];
			sprintf(buf, "mgrId=%s,siteId=%s,hostId=%s,hostName=%s,ipAddress=%s,macAddress=%s,edition=%s,authDate=%s,enterpriseKey=%s,eProcType=%d"
					, mgrId
					, siteId
					, hostId
					, hostName
					, ipAddress
					, macAddress
					, edition
					, authDate
					, enterpriseKey
					, eProcType );

			return CString(buf);
		}
	};

	DECLARE_DYNAMIC(CAuthCheckExDlg)

public:
	CAuthCheckExDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAuthCheckExDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_AUTHCHECK_EX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedButtonFindLicenseFile();

	CEdit		m_editLicenseFile;
	CListCtrl	m_lcLicense;
	CButton		m_btnOK;

	CString		m_strDatFilename;

protected:
	CArray<LICENSE_INFO, LICENSE_INFO&>		m_arrProcessingHost;

	BOOL	GetLicenseInfo(LPCSTR lpszHostId, LPCSTR lpszIniPath, LICENSE_INFO& info);
};
