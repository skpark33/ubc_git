#pragma once
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "ubccopcommon\eventhandler.h"
#include "common\reposcontrol.h"
#include "common\CheckComboBox.h"
#include "UBCCopCommon\CopModule.h"
#include "afxwin.h"



// CLightView 폼 뷰입니다.
class CMainFrame;

class CLightView : public CFormView
{
	DECLARE_DYNCREATE(CLightView)

protected:
	CLightView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CLightView();

public:
	enum { IDD = IDD_LIGHTVIEW };
	enum { eCheck, 
			eSiteId,
			eLightType,
			eLightName,
			eLightID,
			eLightDesc,
			eStartupTime,
			eShutdownTime,
			eBrightness,
			eWeekShutdownTime,
			eHoliday,
			   eLightEnd };

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	CRect m_rcClient;
	CReposControl m_Reposition;

	CUTBListCtrlEx m_lscLight;
	CString		 m_szLightColum[eLightEnd];

	CHoverButton m_btnAddLight;
	CHoverButton m_btnDelLight;
	CHoverButton m_btnModLight;
	CHoverButton m_btnRefLight;
	CHoverButton m_btnLightOff;
	CHoverButton m_btnLightOn;

	CComboBox m_cbAdmin;
	CComboBox m_cbOperation;
	CCheckComboBox m_cbLightType;

	CEdit m_editLightName;
	CEdit m_editLightId;

	CImageList m_ilLightList;

	CString m_strSiteId;

	LightInfoList m_lsLight;

	CMainFrame*  m_pMainFrame;
	CString		 m_szMsg;
	CPoint m_ptSelList;

	void	InitAuthCtrl();
	void	InitLightList();
	BOOL	RefreshLight(bool bCompMsg=true);
	void	RefreshLightList(POSITION posStart=NULL);
	void	UpdateListRow(int nRow, SLightInfo* pInfo);
	void	RefreshPaneText();
	void	ModifyLight(CArray<int>& arRow, bool bMsg);
	void	RemoveLight(CArray<int>& arRow, bool bMsg);
	void	PowerOn(CArray<int>& arRow, bool bMsg);
	void	PowerOff(CArray<int>& arRow, bool bMsg);
	void	InitPosition(CRect rc);
	int		ChangeOPStat(SDeviceOpStat* pOpStat);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnBnClickedButtonLightRef();
	afx_msg void OnBnClickedButtonLightCreate();
	afx_msg void OnBnClickedButtonLightDel();
	afx_msg void OnBnClickedButtonLightMod();
	afx_msg void OnBnClickedButtonLightOff();
	afx_msg void OnBnClickedButtonLightOn();
	afx_msg void OnBnClickedButtonFilterSite();
	afx_msg void OnDestroy();
	afx_msg void OnNMDblclkListLight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	LRESULT InvokeEvent(WPARAM wParam, LPARAM lParam);
	afx_msg void OnNMRclickListLight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDevicemenuPoweron();
	afx_msg void OnDevicemenuPoweroff();
	afx_msg void OnDevicemenuDetail();
	afx_msg void OnDevicemenuRemove();
};


