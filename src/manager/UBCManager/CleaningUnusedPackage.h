#pragma once

#include "resource.h"
#include "common\HoverButton.h"
#include "common\reposcontrol.h"
#include "common\utblistctrlex.h"
#include "EditEx.h"

class CPackageView;

// CCleaningUnusedPackage 대화 상자입니다.

class CCleaningUnusedPackage : public CDialog
{
	DECLARE_DYNAMIC(CCleaningUnusedPackage)

public:
	enum { eCheck // 2010.02.17 by gwangsoo
		 , ePackage
		 , eEnd };

	CCleaningUnusedPackage(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCleaningUnusedPackage();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UNUSED_PACKAGE_DLG };

private:

	CReposControl	m_Reposition;
	bool			m_bInitialize;
	CRect			m_rcClient;
	CHoverButton	m_btnRef;
	CHoverButton	m_btnDel;
	CHoverButton	m_btnClose;
	CEditEx			m_ebUnusedDays;
	CUTBListCtrlEx	m_lcList;

	CPackageView*	m_parent;

	void InitList();
	void RefreshList();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonUnusedDel();
	afx_msg void OnBnClickedButtonRef();
};
