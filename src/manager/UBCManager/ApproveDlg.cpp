// ApproveDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "ApproveDlg.h"

#include "Enviroment.h"
//#include "WaitMessageBox.h"


#define	TIMER_REFRESH_ID		(WM_USER + 1025)
#define	TIMER_REFRESH_TIME		(100)


// CApproveDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CApproveDlg, CDialog)

CApproveDlg::CApproveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CApproveDlg::IDD, pParent)
	, m_Reposition(this)
{
	m_bInitSort = false;
}

CApproveDlg::~CApproveDlg()
{
	DeleteList();
}

void CApproveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_btnRefresh);
	DDX_Control(pDX, IDC_COMBO_APPROVE_TYPE, m_cbxApproveType);
	DDX_Control(pDX, IDC_COMBO_APPROVE_STATE, m_cbxApproveState);
	DDX_Control(pDX, IDC_DATE_FILTER_START, m_dtcFilterStart);
	DDX_Control(pDX, IDC_DATE_FILTER_END, m_dtcFilterEnd);
	DDX_Control(pDX, IDC_BUTTON_APPROVE, m_btnApprove);
	DDX_Control(pDX, IDC_BUTTON_REJECT, m_btnReject);
	DDX_Control(pDX, IDC_BUTTON_REMOVE, m_btnRemove);
	DDX_Control(pDX, IDC_LIST_APPROVE, m_lcApprove);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CApproveDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CApproveDlg::OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_APPROVE, &CApproveDlg::OnBnClickedButtonApprove)
	ON_BN_CLICKED(IDC_BUTTON_REJECT, &CApproveDlg::OnBnClickedButtonReject)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, &CApproveDlg::OnBnClickedButtonRemove)
	ON_WM_TIMER()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CApproveDlg 메시지 처리기입니다.

BOOL CApproveDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_Reposition.AddControl((CWnd*)&m_lcApprove, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl((CWnd*)&m_btnCancel, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);

	//
	m_btnRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	//m_btnApprove.LoadBitmap(IDB_BUTTON_APPROVE, RGB(255,255,255));
	//m_btnReject.LoadBitmap(IDB_BUTTON_REJECT, RGB(255,255,255));
	//m_btnRemove.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));

	//
	CTime tm_cur = CTime::GetCurrentTime();
	m_dtcFilterEnd.SetTime(&tm_cur);
	InitDateRange(m_dtcFilterEnd);

	CTime tm_start = tm_cur - CTimeSpan(365, 0, 0, 0);
	m_dtcFilterStart.SetTime(&tm_start);
	InitDateRange(m_dtcFilterStart);

	//
	InitList();
	InitControl();

	// init (refresh approve-items)
	SetTimer(TIMER_REFRESH_ID, TIMER_REFRESH_TIME, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CApproveDlg::OnOK()
{
	//CDialog::OnOK();
}

void CApproveDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnCancel();
}

void CApproveDlg::InitList()
{
	m_lcApprove.SetExtendedStyle(m_lcApprove.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES/*|LVS_EX_CHECKBOXES*/);

	m_strColumnName[eCheckBox		] = _T("");
	m_strColumnName[eApproveType	] = LoadStringById(IDS_APPROVEDLG_COL001);
	m_strColumnName[eMgrId			] = LoadStringById(IDS_APPROVEDLG_COL002);
	m_strColumnName[eApproveId		] = LoadStringById(IDS_APPROVEDLG_COL003);
	m_strColumnName[eApproveState	] = LoadStringById(IDS_APPROVEDLG_COL004);
	m_strColumnName[eAction			] = LoadStringById(IDS_APPROVEDLG_COL005);
	m_strColumnName[eTarget			] = LoadStringById(IDS_APPROVEDLG_COL006);
	m_strColumnName[eDetails		] = LoadStringById(IDS_APPROVEDLG_COL007);
	m_strColumnName[eRequestedId	] = LoadStringById(IDS_APPROVEDLG_COL008);
	m_strColumnName[eRequestedTime	] = LoadStringById(IDS_APPROVEDLG_COL009);
	m_strColumnName[eApprovedId		] = LoadStringById(IDS_APPROVEDLG_COL010);
	m_strColumnName[eApprovedTime	] = LoadStringById(IDS_APPROVEDLG_COL011);
	m_strColumnName[eDescription1	] = LoadStringById(IDS_APPROVEDLG_COL012);
	m_strColumnName[eDescription2	] = LoadStringById(IDS_APPROVEDLG_COL013);

	m_lcApprove.InsertColumn(eCheckBox,		m_strColumnName[eCheckBox],		LVCFMT_LEFT, 22);
	m_lcApprove.InsertColumn(eApproveType,	m_strColumnName[eApproveType],	LVCFMT_LEFT, 85);
	m_lcApprove.InsertColumn(eMgrId,		m_strColumnName[eMgrId],		LVCFMT_LEFT, 0);
	m_lcApprove.InsertColumn(eApproveId,	m_strColumnName[eApproveId],	LVCFMT_LEFT, 0);
	m_lcApprove.InsertColumn(eApproveState,	m_strColumnName[eApproveState],	LVCFMT_LEFT, 80);
	m_lcApprove.InsertColumn(eAction,		m_strColumnName[eAction],		LVCFMT_LEFT, 50);
	m_lcApprove.InsertColumn(eTarget,		m_strColumnName[eTarget],		LVCFMT_LEFT, 200);
	m_lcApprove.InsertColumn(eDetails,		m_strColumnName[eDetails],		LVCFMT_LEFT, 100);
	m_lcApprove.InsertColumn(eRequestedId,	m_strColumnName[eRequestedId],	LVCFMT_LEFT, 90);
	m_lcApprove.InsertColumn(eRequestedTime,m_strColumnName[eRequestedTime],LVCFMT_LEFT, 120);
	m_lcApprove.InsertColumn(eApprovedId,	m_strColumnName[eApprovedId],	LVCFMT_LEFT, 0);
	m_lcApprove.InsertColumn(eApprovedTime,	m_strColumnName[eApprovedTime],	LVCFMT_LEFT, 120);
	m_lcApprove.InsertColumn(eDescription1,	m_strColumnName[eDescription1],	LVCFMT_LEFT, 70);
	m_lcApprove.InsertColumn(eDescription2,	m_strColumnName[eDescription2],	LVCFMT_LEFT, 70);

	m_lcApprove.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CApproveDlg::InitControl()
{
	//
	m_cbxApproveType.ResetContent();
	m_cbxApproveType.AddString("All");
	for(int i=1; i<APPROVE_INFO::eTypeCount; i++)
		m_cbxApproveType.AddString(GetApproveTypeString((APPROVE_INFO::APPROVE_TYPE)i));
	m_cbxApproveType.SetCurSel(0);

	//
	m_cbxApproveState.ResetContent();
	m_cbxApproveState.AddString("All");
	for(int i=1; i<APPROVE_INFO::eStatusCount; i++)
		m_cbxApproveState.AddString(GetApproveStatusString((APPROVE_INFO::APPROVE_STATUS)i));
	m_cbxApproveState.SetCurSel(0);
}

CString CApproveDlg::GetApproveStatusString(APPROVE_INFO::APPROVE_STATUS eStatus)
{
	switch( eStatus )
	{
	case APPROVE_INFO::eStatusRequested:	return LoadStringById(IDS_APPROVEDLG_STATUS002);
	case APPROVE_INFO::eStatusAccepted:		return LoadStringById(IDS_APPROVEDLG_STATUS003);
	case APPROVE_INFO::eStatusRejected:		return LoadStringById(IDS_APPROVEDLG_STATUS004);
	case APPROVE_INFO::eStatusRemoved:		return LoadStringById(IDS_APPROVEDLG_STATUS005);
	default:								break;
	}

	//eApproveNoState
	return LoadStringById(IDS_APPROVEDLG_STATUS001);
}

CString CApproveDlg::GetApproveTypeString(APPROVE_INFO::APPROVE_TYPE eType)
{
	switch( eType )
	{
	case APPROVE_INFO::eTypeSite:		return LoadStringById(IDS_APPROVEDLG_TYPE002);
	case APPROVE_INFO::eTypeHost:		return LoadStringById(IDS_APPROVEDLG_TYPE003);
	case APPROVE_INFO::eTypeProgram:	return LoadStringById(IDS_APPROVEDLG_TYPE004);
	case APPROVE_INFO::eTypeBP:			return LoadStringById(IDS_APPROVEDLG_TYPE005);
	default:							break;
	}

	//eTypeNoType
	return LoadStringById(IDS_APPROVEDLG_TYPE001);
}

BOOL CApproveDlg::GetApproveInfoList(APPROVE_INFO_LIST& infoList, LPCSTR lpszApprovedId, bool bShowErrorMessage)
{
	// version check (server version must larger than 5.9x)
	double ver = atof(GetEnvPtr()->GetServerVersion());
	if( ver < 5.89f ) return FALSE;

	// make where-clause
	CString str_where = "";

	// approve state
	int idx = m_cbxApproveState.GetCurSel();
	if( idx > 0 )
	{
		str_where.Format("approveState = '%d' and ", idx);
	}

	// start ~ end time
	CTime tm_start, tm_end;
	if( m_dtcFilterStart.GetSafeHwnd() && m_dtcFilterStart.GetSafeHwnd() )
	{
		m_dtcFilterStart.GetTime(tm_start);
		m_dtcFilterEnd.GetTime(tm_end);
	}
	else
	{
		tm_end = CTime::GetCurrentTime();
		tm_start = tm_end - CTimeSpan(365, 0, 0, 0);
	}

	CString str_start = tm_start.Format("%Y-%m-%d") + _T(" 00:00:00");
	CString str_end = tm_end.Format("%Y-%m-%d") + _T(" 23:59:59");

	if( lpszApprovedId != NULL )
	{
		CString str_tmp;
		str_tmp.Format("approvedId = '%s' and approveState = '1' and requestedTime >= CAST('%s' as datetime) and requestedTime <= CAST('%s' as datetime)", 
							lpszApprovedId, str_start, str_end );
		str_where += str_tmp;
	}
	else
	{
		CString str_tmp;
		str_tmp.Format("requestedTime >= CAST('%s' as datetime) and requestedTime <= CAST('%s' as datetime)", 
							str_start, str_end );
		str_where += str_tmp;
	}

	if( !CCopModule::GetObject()->getApproveInfoList(str_where, infoList) )
	{
		// fail
		if( bShowErrorMessage )
			UbcMessageBox(LoadStringById(IDS_APPROVEDLG_MSG001), MB_ICONSTOP);
		return FALSE;
	}

	// approve type
	idx = m_cbxApproveType.GetCurSel();
	if( idx > 0 )
	{
		int cnt = infoList.GetCount();
		for(int i=cnt-1; i>=0; i--)
		{
			APPROVE_INFO* info = infoList.GetAt(i);
			if( info->eApproveType != idx )
			{
				delete info;
				infoList.RemoveAt(i);
			}
		}
	}

	return TRUE;
}

void CApproveDlg::RefreshList()
{
	m_btnApprove.EnableWindow(FALSE);
	m_btnReject.EnableWindow(FALSE);
	m_btnRemove.EnableWindow(FALSE);

	// get ApproveList
	APPROVE_INFO_LIST info_list;
	if( m_PreApproveInfoList.GetCount() > 0 )
	{
		info_list.Copy(m_PreApproveInfoList);
		m_PreApproveInfoList.RemoveAll();
	}
	else
	{
		// wait message box
		CWaitMessageBox wait;

		if( !GetApproveInfoList(info_list) ) return;
	}

	// refresh list-ctrl
	DeleteList();
	m_ApproveInfoList.Copy(info_list);

	for(int i=0; i<m_ApproveInfoList.GetCount(); i++)
	{
		APPROVE_INFO* info = m_ApproveInfoList.GetAt(i);

		m_lcApprove.InsertItem (i, "");
		m_lcApprove.SetItemText(i, eApproveType,	GetApproveTypeString(info->eApproveType));
		m_lcApprove.SetItemText(i, eMgrId,			info->strMgrId);
		m_lcApprove.SetItemText(i, eApproveId,		info->strApproveId);
		m_lcApprove.SetItemText(i, eApproveState,	GetApproveStatusString(info->eApproveState));
		m_lcApprove.SetItemText(i, eAction,			info->strAction);
		m_lcApprove.SetItemText(i, eTarget,			info->strTarget);
		m_lcApprove.SetItemText(i, eDetails,		info->strDetails);
		m_lcApprove.SetItemText(i, eRequestedId,	info->strRequestedId);
		m_lcApprove.SetItemText(i, eRequestedTime,	info->strRequestedTime);
		m_lcApprove.SetItemText(i, eApprovedId,		info->strApprovedId);
		m_lcApprove.SetItemText(i, eApprovedTime,	info->strApprovedTime);
		m_lcApprove.SetItemText(i, eDescription1,	info->strDescription1);
		m_lcApprove.SetItemText(i, eDescription2,	info->strDescription2);

		m_lcApprove.SetItemData(i, (DWORD_PTR)info);

		//
		if( GetEnvPtr()->m_szLoginID == info->strApprovedId )
		{
			m_btnApprove.EnableWindow(TRUE);
			m_btnReject.EnableWindow(TRUE);
			m_btnRemove.EnableWindow(TRUE);
		}
	}

	if( m_bInitSort == false )
	{
		m_bInitSort = true;
		//m_lcApprove.SetSortCol(eRequestedTime, false);
		m_lcApprove.SortList(eRequestedTime, false, (PFNLVCOMPARE)CUTBListCtrlEx::Compare, (DWORD_PTR)&m_lcApprove);
	}
	else
	{
		m_lcApprove.SortList(m_lcApprove.GetCurSortCol(), m_lcApprove.IsAscend(), (PFNLVCOMPARE)CUTBListCtrlEx::Compare, (DWORD_PTR)&m_lcApprove);
	}
}

void CApproveDlg::DeleteList()
{
	if( m_lcApprove.GetSafeHwnd() )
	{
		m_lcApprove.DeleteAllItems();
	}

	for(int i=0; i<m_ApproveInfoList.GetCount(); i++)
	{
		APPROVE_INFO* info = m_ApproveInfoList.GetAt(i);
		delete info;
	}
	m_ApproveInfoList.RemoveAll();
}

BOOL CApproveDlg::SetApproveState(int idx, APPROVE_INFO::APPROVE_STATUS eStatus)
{
	int count = m_lcApprove.GetItemCount();
	if( idx >= count ) return FALSE;

	APPROVE_INFO* info = (APPROVE_INFO*)m_lcApprove.GetItemData(idx);
	if( info == NULL ) return FALSE;

	return SetApproveState(info, eStatus);
}

BOOL CApproveDlg::SetApproveState(APPROVE_INFO* info, APPROVE_INFO::APPROVE_STATUS eStatus)
{
	if( info==NULL ) return FALSE;

	// set ApproveList
	return CCopModule::GetObject()->setApprove(info->strMgrId, info->strApproveId, eStatus);
}

BOOL CApproveDlg::RemoveApproveObject(APPROVE_INFO* info)
{
	if( info==NULL ) return FALSE;

	// set ApproveList
	return CCopModule::GetObject()->removeApprove(info->strMgrId, info->strApproveId);
}


BOOL CApproveDlg::GetSelectedItemList(CUIntArray& listSelected)
{
	listSelected.RemoveAll();

	int cnt = m_lcApprove.GetItemCount();
	for(int i=0; i<cnt; i++)
	{
		if( m_lcApprove.GetCheck(i)==0 ) continue;

		APPROVE_INFO* info = (APPROVE_INFO*)m_lcApprove.GetItemData(i);
		if( GetEnvPtr()->m_szLoginID != info->strApprovedId )
		{
			// not match approvedId
			UbcMessageBox(LoadStringById(IDS_APPROVEDLG_MSG005), MB_ICONSTOP);
			return FALSE;
		}

		listSelected.Add(i);
	}
	return TRUE;
}

void CApproveDlg::OnBnClickedButtonRefresh()
{
	RefreshList();
}

BOOL CApproveDlg::SetApproveStateOfSelectedItem(APPROVE_INFO::APPROVE_STATUS eStatus)
{
	// get selected-item
	CUIntArray list_selected;
	if( !GetSelectedItemList(list_selected) ) return FALSE;

	// wait message box
	CWaitMessageBox wait;

	if( list_selected.GetCount() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_APPROVEDLG_MSG002), MB_ICONSTOP);
		return FALSE;
	}

	// set new-approve-status
	int cnt_succ = 0;
	for(int i=0; i<list_selected.GetCount(); i++)
	{
		UINT idx = list_selected.GetAt(i);
		APPROVE_INFO* info = (APPROVE_INFO*)m_lcApprove.GetItemData(idx);

		if( info == NULL ) continue;
		if( SetApproveState(info, eStatus) == FALSE ) continue;

		// refresh list-ctrl
		cnt_succ++;
		info->eApproveState = eStatus;
		m_lcApprove.SetItemText(idx, eApproveState,	GetApproveStatusString(info->eApproveState));
		m_lcApprove.SetCheck(idx, FALSE);
	}

	if( cnt_succ != list_selected.GetCount() )
	{
		// someone fail
		UbcMessageBox(LoadStringById(IDS_APPROVEDLG_MSG003), MB_ICONSTOP);
		return FALSE;
	}

	// all success
	UbcMessageBox(LoadStringById(IDS_APPROVEDLG_MSG004), MB_ICONINFORMATION);
	return TRUE;
}
BOOL CApproveDlg::RemoveApproveObjectOfSelectedItem()
{
	// get selected-item
	CUIntArray list_selected;
	if( !GetSelectedItemList(list_selected) ) return FALSE;

	// wait message box
	CWaitMessageBox wait;

	if( list_selected.GetCount() == 0 )
	{
		UbcMessageBox(LoadStringById(IDS_APPROVEDLG_MSG002), MB_ICONSTOP);
		return FALSE;
	}

	// set new-approve-status
	int cnt_succ = 0;
	for(int i=0; i<list_selected.GetCount(); i++)
	{
		UINT idx = list_selected.GetAt(i);
		APPROVE_INFO* info = (APPROVE_INFO*)m_lcApprove.GetItemData(idx);

		if( info == NULL ) continue;
		if( RemoveApproveObject(info) == FALSE ) continue;

		// refresh list-ctrl
		cnt_succ++;
		m_lcApprove.SetCheck(idx, FALSE);
	}

	if( cnt_succ != list_selected.GetCount() )
	{
		// someone fail
		UbcMessageBox(LoadStringById(IDS_APPROVEDLG_MSG003), MB_ICONSTOP);
		return FALSE;
	}

	// all success
	UbcMessageBox(LoadStringById(IDS_APPROVEDLG_MSG004), MB_ICONINFORMATION);

	RefreshList();
	return TRUE;
}


void CApproveDlg::OnBnClickedButtonApprove()
{
	// set approve-status
	SetApproveStateOfSelectedItem(APPROVE_INFO::eStatusAccepted);
}

void CApproveDlg::OnBnClickedButtonReject()
{
	// set approve-status
	SetApproveStateOfSelectedItem(APPROVE_INFO::eStatusRejected);
}

void CApproveDlg::OnBnClickedButtonRemove()
{
	// set approve-status
	//SetApproveStateOfSelectedItem(APPROVE_INFO::eStatusRemoved);

	// remove Approve object
	RemoveApproveObjectOfSelectedItem();
}

void CApproveDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case TIMER_REFRESH_ID:
		KillTimer(TIMER_REFRESH_ID);
		RefreshList();
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CApproveDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )
	{
		m_Reposition.MoveControl(TRUE);
	}
}
