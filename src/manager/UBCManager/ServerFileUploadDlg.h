#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "UTBListCtrlEx.h"
#include "common/libFileServiceWrap/FileServiceWrapDll.h"


// CServerFileUploadDlg 대화 상자입니다.

class CServerFileUploadDlg
	: public CDialog
	, public CFileServiceWrap::IProgressHandler
{
protected:
	enum FILE_STATUS { eFSReady=0, eFSUploading, eFSCompleted, eFSAborted, eFSError };
	enum ERROR_CODE { eECNone=0, eECInternalError, eECFileNotFound, eECSendError };
	typedef struct {
		CString		strFilename;
		CString		strServerPath;
		CString		strLocalPath;
		ULONGLONG	ulFilesize;
		FILE_STATUS	eStatus;
		ERROR_CODE	eErrorCode;
	} FILE_ITEM;

	typedef CArray<FILE_ITEM*, FILE_ITEM*> FILE_LIST;

	class CAutoLock
	{
	public:
		CAutoLock(CCriticalSection& cs) { m_pCS=&cs; m_pCS->Lock(); };
		~CAutoLock() { m_pCS->Unlock(); };
	protected:
		CCriticalSection* m_pCS;
	};

	DECLARE_DYNAMIC(CServerFileUploadDlg)

public:
	CServerFileUploadDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CServerFileUploadDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SERVER_FILE_UPLOAD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CFont	m_font;
	CImageList	m_ilTab;

	enum TAB_COLUMN { eTCUnknown=-1, eTCUploading=0, eTCCompleted, eTCFailed, eTCMAXCOUNT };
	enum LC_COLUMN { eLCFilename=0, eLCServerPath, eLCStatus, eLCLocalPath, eLCMAXCOUNT };

	FILE_LIST	m_FileList;
	CStringArray	m_SelectedFileList;

	void	UpdateTabCtrl(TAB_COLUMN eShowTab=eTCUnknown);
	void	AddItem(TAB_COLUMN eTab, FILE_ITEM* pFileItem);

	LPCSTR	GetFileStatusString(FILE_STATUS eStatus);

	BOOL		m_bLogin;
	BOOL		m_bUploading;
	BOOL		m_bUserAbort;

	CFileServiceWrap m_objFileSvcClient;
	CWinThread*	m_pThread;
	FILE_ITEM*	m_pCurrentUploadingFile;
	CCriticalSection	m_cs;

public:
	CEdit		m_editServerPath;
	CEdit		m_editUploadFile;
	CButton		m_btnSelectFile;
	CButton		m_btnAddToUploadList;
	CTabCtrl	m_tc;
	CUTBListCtrlEx	m_lc[eTCMAXCOUNT];
	CButton		m_btnClose;

	afx_msg void OnBnClickedButtonSelectFile();
	afx_msg void OnBnClickedButtonAddToUploadList();
	afx_msg void OnTcnSelchangeTabCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusEditServerPath();
	afx_msg LRESULT OnCompleteItem(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTerminateUpload(WPARAM wParam, LPARAM lParam);

	BOOL	IsLoginValid() { return m_bLogin; };
	void	StartUpload();
	void	StopUpload();

	//
	static UINT	UploadThreadProc(LPVOID param);
	virtual void ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uSendBytes, ULONGLONG uTotalBytes, CString strLocalPath);

	BOOL	UploadFile();
};
