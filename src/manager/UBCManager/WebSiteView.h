#pragma once

#include "afxcmn.h"
#include "afxwin.h"
#include "Enviroment.h"
#include "ie_bulletinboard.h"
#include "common\reposcontrol.h"

// CWebSiteView 폼 뷰입니다.

class CWebSiteView : public CFormView
{
	DECLARE_DYNCREATE(CWebSiteView)

protected:
	CWebSiteView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CWebSiteView();

public:
	enum { IDD = IDD_WEB_SITEVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	CIe_bulletinboard m_ieMain;

	CRect m_rcClient;
	CReposControl m_Reposition;

	void InitPosition(CRect rc);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_EVENTSINK_MAP()
	void WindowClosingIeMain(BOOL IsChildWindow, BOOL* Cancel);
};


