#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "afxdtctl.h"

#include "common\hoverbutton.h"
#include "common\utblistctrl.h"
#include "common\progressctrlex.h"
#include "ubccopcommon\copmodule.h"

// CHostDetailDlg dialog
class CHostDetailDlg : public CDialog
{
	DECLARE_DYNAMIC(CHostDetailDlg)
public:
	void SetInfo(SHostInfo&);
	void GetInfo(SHostInfo&);
private:
public:
private:
	bool	m_bInit;
	CString m_edtTerminalId;
	CString m_edtTerminalName;
	CString m_edtSiteName;
	CString m_edtBootTime;
	CString m_edtPowerMngInfo;
	CString m_edtIpAddr;
	CString m_edtMacAddr;
	CString m_edtSerialNo;
	CString m_edtEdition;
	CString m_edtOs;
	CString m_edtVersion;
	CString m_edtRegstDate;
	CString m_edtServerId;
	CString m_edtVendor;
	CString m_edtModel;
//	CString m_edtPeriod;
//	CString m_edtPeriod2;
	CString m_edtPort;
	CString m_edtLocation;
	CString m_edtDesc;
	CString m_szLastUpTime;
	CString m_szCPUTemp;
//	CString m_szLogLimit;
	CString m_strShutdownDownTime;
	CString m_strPoweronTime;
	CString m_strWeekDays;
	CString m_strHolidays;
	BOOL m_bUseMonitorOffTime;
	std::list<std::string> m_listMonitorOffTime;
	CString m_strWeekShutdownTime;
	CString m_strProtocolType;

	BOOL m_bAutoUpdate;
	CBitmap m_bmTState;
	CStatic m_stcTState;

	int m_nLogLimit; // 로그보존기간 (1~356사이의 값을 갖는다)
	int m_nPeriod; // 감시주콅E(30~600)
	int m_nPeriod2; // 스크린샷주콅E(120~3600)
	CDateTimeCtrl m_dtDayBaseTime;	// 하루시작시간
	CComboBox m_cbVideoRendering;

	CSpinButtonCtrl m_spPeriod;
	CSpinButtonCtrl m_spPeriod2;
	CSpinButtonCtrl m_spLogLimit;
	CComboBox m_cbPowerControl;

	CProgressCtrlEx m_pgcCpu;
	CProgressCtrlEx m_pgcHdd1;
	CProgressCtrlEx m_pgcHdd2;
	CProgressCtrlEx m_pgcRMemory;
	CProgressCtrlEx m_pgcVMemory;

	CButton m_ckMute;
	CButton m_rdVolDis1;
	CButton m_rdVolDis2;
	CHoverButton m_btUState;
	CSliderCtrl m_slcVolume;

	CString m_strCategory;
	BOOL m_bDownloadTime;
	CDateTimeCtrl m_dtcDownloadTime;

	CComboBox m_cbType;
	CComboBox m_cbMonitorType;

	CArray<CWnd*>	m_arStaticCtr;
	CBrush			m_brushBG;
	SHostInfo		m_HostInfo;

	HostInfoList	m_cacheClientList;
public:
	CHostDetailDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CHostDetailDlg();

// Dialog Data
	enum { IDD = IDD_HOST_DETAILINFO };

private:
	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedButtonUstate();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCheckMute();
	afx_msg void OnCbnSelchangeComboPoweronmethod();
	afx_msg void OnBnClickedButtonHollsetting();
	afx_msg void OnEnKillfocusEditPeriod();
	afx_msg void OnEnKillfocusEditPeriod2();
	afx_msg void OnEnKillfocusEditLoglimit();
	afx_msg void OnBnClickedCheckDownloadtime();
	afx_msg void OnBnClickedButtonPowerOn();
	afx_msg void OnBnClickedKbUseMonitorOffTime();
	afx_msg LRESULT OnProgressExThresholdChanged(WPARAM wParam, LPARAM lParam);
	CButton m_packageInfo;
	CHoverButton m_btnSizeToggle;
	afx_msg void OnBnClickedButtonExt();
	afx_msg void OnBnClickedButtonPackageInfo();
	CString m_cacheServer;
	CEdit m_editDisplayCount;
	CButton m_btCacheSet;
	CButton m_btCacheUnset;
	afx_msg void OnBnClickedButtonCacheSet();
	afx_msg void OnBnClickedButtonCacheUnset();

protected:
	int		MIN_SCREENSHOT_PERIOD;
};
