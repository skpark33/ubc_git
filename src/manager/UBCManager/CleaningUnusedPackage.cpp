// CleaningUnusedPackage.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "PackageView.h"
#include "UBCCopCommon\CopModule.h"
#include "CleaningUnusedPackage.h"
#include "common/libCommon/ubcUtil.h"


// CCleaningUnusedPackage 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCleaningUnusedPackage, CDialog)

CCleaningUnusedPackage::CCleaningUnusedPackage(CWnd* pParent /*=NULL*/)
	: CDialog(CCleaningUnusedPackage::IDD, pParent)
	, m_Reposition(this)
	, m_bInitialize(false)
{
	m_parent = (CPackageView*)pParent;
}

CCleaningUnusedPackage::~CCleaningUnusedPackage()
{
}

void CCleaningUnusedPackage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PACKAGE, m_lcList);
	DDX_Control(pDX, IDC_BUTTON_REF, m_btnRef);
	DDX_Control(pDX, IDC_BUTTON_UNUSED_DEL, m_btnDel);
	DDX_Control(pDX, IDCANCEL, m_btnClose);
	DDX_Control(pDX, IDC_EB_UNUSED_DAYS, m_ebUnusedDays);
}


BEGIN_MESSAGE_MAP(CCleaningUnusedPackage, CDialog)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDOK, &CCleaningUnusedPackage::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCleaningUnusedPackage::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_UNUSED_DEL, &CCleaningUnusedPackage::OnBnClickedButtonUnusedDel)
	ON_BN_CLICKED(IDC_BUTTON_REF, &CCleaningUnusedPackage::OnBnClickedButtonRef)
END_MESSAGE_MAP()


// CCleaningUnusedPackage 메시지 처리기입니다.

void CCleaningUnusedPackage::OnBnClickedOk()
{
//	OnOK();
}

void CCleaningUnusedPackage::OnBnClickedCancel()
{
	OnCancel();
}

int CCleaningUnusedPackage::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(m_rcClient);

	m_Reposition.SetParentRect(m_rcClient);

	return 0;
}

void CCleaningUnusedPackage::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CCleaningUnusedPackage::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	CDialog::OnGetMinMaxInfo(lpMMI);

	if(m_bInitialize)
	{
		lpMMI->ptMinTrackSize.x = m_rcClient.Width() + ::GetSystemMetrics(SM_CXDLGFRAME);
		lpMMI->ptMinTrackSize.y = m_rcClient.Height() + ::GetSystemMetrics(SM_CXDLGFRAME);
	}
	else
	{
		m_bInitialize = true;
	}
}

BOOL CCleaningUnusedPackage::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_btnRef.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnDel.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
//	m_btnClose.LoadBitmap(IDB_BUTTON_CLOSE, RGB(255,255,255));

	m_btnRef.SetToolTipText(LoadStringById(IDS_PACKAGEVIEW_STR004));
	m_btnDel.SetToolTipText(LoadStringById(IDS_PACKAGEVIEW_STR003));

	m_Reposition.AddControl(&m_lcList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_ebUnusedDays.SetValue(90);

	InitList();
	RefreshList();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCleaningUnusedPackage::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);

	m_lcList.InsertColumn(eCheck  , _T(""), LVCFMT_CENTER, 22);
	m_lcList.InsertColumn(ePackage, LoadStringById(IDS_PACKAGEVIEW_LST002), LVCFMT_LEFT,  322);

	// 제일 마지막에 할것...
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CCleaningUnusedPackage::RefreshList()
{
	int nUnusedDays = m_ebUnusedDays.GetValueInt();

	if(nUnusedDays < 1)
	{
		UbcMessageBox(LoadStringById(IDS_PACKAGEVIEW_MSG003));
		return;
	}

	CWaitMessageBox wait;

	m_lcList.DeleteAllItems();
	CStringArray astrPackageList;

	if(!CCopModule::GetObject()->GetUnusedPackage((short)nUnusedDays, astrPackageList))
	{
		return;
	}

	m_lcList.SetRedraw(FALSE);

	for(int i=0; i<astrPackageList.GetCount() ;i++)
	{
		m_lcList.InsertItem(i, _T(""));
		m_lcList.SetItemText(i, ePackage, astrPackageList[i]);
	}

	int nCol = m_lcList.GetCurSortCol();
	if(-1 != nCol)
		m_lcList.SortList(nCol, m_lcList.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);
	else
		m_lcList.SortList(ePackage, m_lcList.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);

	m_lcList.SetRedraw(TRUE);
}

void CCleaningUnusedPackage::OnBnClickedButtonRef()
{
	RefreshList();
}

void CCleaningUnusedPackage::OnBnClickedButtonUnusedDel()
{
	CWaitMessageBox wait;

	int targetCount = 0;
	for(int nRow = m_lcList.GetItemCount()-1; nRow >= 0; nRow--) {
		if(!m_lcList.GetCheck(nRow)) continue;
		targetCount++;
	}
	if(targetCount > 1) {
		UbcMessageBox(IDS_HOSTVIEW_MSG024);
		return;
	}

	if(targetCount == 0)
	{
		UbcMessageBox(LoadStringById(IDS_PLANSCHEDULING_MSG002), MB_ICONINFORMATION);
		return;
	}


	for(int nRow = m_lcList.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;
	
		CString strMsg;
		// 0000560: 매니저에서 콘텐츠 패키지를 삭제시, 해당 콘텐츠 패키지가 사용중이라면, 사용중이라는 경고문을 넣는다. 
		CString strPackage = m_lcList.GetItemText(nRow, ePackage);
		ciStringSet hostSet;
		int nCnt = ubcUtil::getInstance()->getHostListUsingThisProgram(strPackage, hostSet);
		if(nCnt > 0)
		{
			strMsg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG001), strPackage);

			ciStringSet::iterator itr;
			for(itr=hostSet.begin();itr!=hostSet.end();itr++)
			{
				ciString hostId = (*itr);
				strMsg += _T("\n");
				strMsg += hostId.c_str();
			}

			if(UbcMessageBox(strMsg, MB_YESNO) != IDYES)
			{
				continue;
			}
		}

		// 방송계획으로 사용중인지 체크하여 지울지 판단한다.
		CString strBpList;

		if(!m_parent->GetBroadcastPlan(strPackage, strBpList)) continue;
		if(!strBpList.IsEmpty())
		{
			strMsg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG009), strPackage);
			UbcMessageBox(strMsg + strBpList);
			continue;
		}


		CString strSiteId = _T("*");

		if(strPackage.Find(_T("_")) > 0)
		{
			int pos = 0;
			strSiteId = strPackage.Tokenize(_T("_"), pos);
		}

		if(strSiteId.GetLength()==0 || strPackage.GetLength()==0) continue;
		if(CCopModule::GetObject()->RemovePackage(strSiteId, strPackage))
		{
			m_lcList.DeleteItem(nRow);
		}
	}
}
