#pragma once

#ifndef IDD_MULTI_SELECT_MONITOR
#define IDD_MULTI_SELECT_MONITOR	472
#endif//IDD_MULTI_SELECT_MONITOR

#include "afxwin.h"
#include "afxcmn.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\CheckComboBox.h"


// CMultiSelectMonitor 대화 상자입니다.

class CMultiSelectMonitor : public CDialog
{
	DECLARE_DYNAMIC(CMultiSelectMonitor)

public:
	CMultiSelectMonitor(LPCTSTR szCustomer, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMultiSelectMonitor();

	void SetTitle(CString& p) { m_title = p; }

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MULTI_SELECT_MONITOR };
	enum { eCheck, eMonitorName, eMonitorID, eGroup, eMaxCol };

	// In/Out Parameter
	CStringArray		m_astrSelMonitorList;    //monitorIdList
	CStringArray		m_astrSelMonitorNameList;  //monitorNameList

	int					m_nAuthority;
	CString				m_strSiteId;
	CString				m_strIniPath;

protected:
	CString				m_title;
	CString				m_strCustomer;

	CString				m_szColum[eMaxCol];
	MonitorInfoList		m_lsInfoList;

	CEdit				m_editFilterMonitorName;
	CEdit				m_editFilterMonitor;
	//CComboBox			m_cbxMonitorType;
	CCheckComboBox		m_cbxMonitorType;
	CComboBox			m_cbxSite;
	CComboBox			m_cbOnOff;
	CComboBox			m_cbOperation;
//	CEdit				m_editFilterDescription;
	CHoverButton		m_bnRefresh;
	CUTBListCtrlEx		m_lcMonitorList;
	CUTBListCtrlEx		m_lcSelMonitorList;
	CHoverButton		m_bnAdd;
	CHoverButton		m_bnDel;
	CHoverButton		m_bnOK;
	CHoverButton		m_bnCancel;

	CString				m_strSite;

	void				InitMonitorList();
	void				RefreshMonitorList();

	void				InitSelMonitorList();
	void				RefreshSelMonitorList();

	void				AddSelMonitor(int nMonitorIndex);
	void				DelSelMonitor(int nSelMonitorIndex);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButtonMonitorRefresh();
	afx_msg void OnBnClickedBnAdd();
	afx_msg void OnBnClickedBnDel();
	afx_msg void OnNMDblclkListMonitor(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkListSelMonitor(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeCbSite();
};
