// HostListDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Enviroment.h"
#include "ubccopcommon\copmodule.h"
#include "ubccopcommon\UBCCopCommon.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "ubccopcommon\MultiSelectHost.h"
#include "common\TraceLog.h"
#include "HostDetailDlg.h"
#include "HostListDlg.h"


// CHostListDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHostListDlg, CDialog)

CHostListDlg::CHostListDlg(CString& hostName, CString& cacheServer,CWnd* pParent /*=NULL*/)
	: CDialog(CHostListDlg::IDD, pParent)
	, m_cacheServer(cacheServer)
	, m_hostName(hostName)
{

}

CHostListDlg::~CHostListDlg()
{
}

void CHostListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcHostList);
	DDX_Control(pDX, IDC_EDIT_CACHE_HOST, m_editCacheServer);
	DDX_Control(pDX, IDC_EDIT_COUNTER, m_editCounter);
	DDX_Control(pDX, IDC_BUTTON_HOST_REFRESH, m_btRefresh);
	DDX_Control(pDX, IDC_BUTTON_HOST_ADD, m_btAdd);
	DDX_Control(pDX, IDC_BUTTON_HOST_DEL, m_btDel);
}


BEGIN_MESSAGE_MAP(CHostListDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CHostListDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_HOST_REFRESH, &CHostListDlg::OnBnClickedButtonHostRefresh)
	ON_BN_CLICKED(IDC_BUTTON_HOST_ADD, &CHostListDlg::OnBnClickedButtonHostAdd)
	ON_BN_CLICKED(IDC_BUTTON_HOST_DEL, &CHostListDlg::OnBnClickedButtonHostDel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, &CHostListDlg::OnNMDblclkListHost)
END_MESSAGE_MAP()


// CHostListDlg 메시지 처리기입니다.

BOOL CHostListDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	this->m_editCacheServer.SetWindowText(this->m_hostName);
	m_btRefresh.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btAdd.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btDel.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));

	m_btRefresh.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN004));
	m_btAdd.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN001));
	m_btDel.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN002));

	m_btAdd.EnableWindow(IsAuth(_T("HDEL")));
	m_btDel.EnableWindow(IsAuth(_T("HDEL")));


	InitHostList();
	RefreshHostList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CHostListDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CHostListDlg::InitHostList()
{
	m_lcHostList.SetExtendedStyle(m_lcHostList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	//m_szColum[eCheck] = _T("");	// 2010.02.17 by gwangsoo
	m_szColum[eHostName] = LoadStringById(IDS_HOSTVIEW_LIST009);
	m_szColum[eHostID] = LoadStringById(IDS_HOSTVIEW_LIST008);
	m_szColum[eGroup ] = LoadStringById(IDS_HOSTVIEW_LIST007);
	m_szColum[eHostIP ] = LoadStringById(IDS_HOSTVIEW_LIST016);
	m_szColum[eDescription ] = LoadStringById(IDS_HOSTVIEW_LIST015);
	m_szColum[eCategory ] = LoadStringById(IDS_HOSTVIEW_LIST036);
	m_szColum[eAddr1 ] = LoadStringById(IDS_HOSTVIEW_LIST055);

	//m_lcHostList.SetColumnWidth(eCheck, 22); 

	m_lcHostList.InsertColumn(eHostName , m_szColum[eHostName ], LVCFMT_LEFT  , 200);
	m_lcHostList.InsertColumn(eHostID , m_szColum[eHostID ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eGroup  , m_szColum[eGroup  ], LVCFMT_LEFT  ,  100);
	m_lcHostList.InsertColumn(eHostIP  , m_szColum[eHostIP  ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eDescription  , m_szColum[eDescription  ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eCategory  , m_szColum[eCategory  ], LVCFMT_LEFT  , 100);
	m_lcHostList.InsertColumn(eAddr1  , m_szColum[eAddr1  ], LVCFMT_LEFT  , 100);

	// 제일 마지막에 할것...
	m_lcHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

//	m_lcHostList.SetSortEnable(false);
//	m_lcHostList.SetFixedCol(eCheck);
}

void CHostListDlg::RefreshHostList()
{
	CWaitMessageBox wait;


	CString strWhere = "";
	strWhere += "(";
	strWhere += "domainName = '";
	strWhere += m_cacheServer;
	strWhere += "') ";

	m_lsInfoList.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForList(&aCall
														, _T("*")
														, _T("*")
														, strWhere
														);

	if(bRet)
	{
		CCopModule::GetObject()->GetHostData(&aCall
											, m_lsInfoList
											, 0
											);
	}

	m_lcHostList.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsInfoList.GetNext(pos);

		m_lcHostList.InsertItem(nRow, "");
		m_lcHostList.SetItemText(nRow, eHostName, info.hostName);
		m_lcHostList.SetItemText(nRow, eHostID, info.hostId);
		m_lcHostList.SetItemText(nRow, eGroup , info.siteId);
		m_lcHostList.SetItemText(nRow, eHostIP , info.ipAddress);
		m_lcHostList.SetItemText(nRow, eDescription , info.description);
		m_lcHostList.SetItemText(nRow, eCategory , info.category);
		m_lcHostList.SetItemText(nRow, eAddr1 , info.location);
	
		m_lcHostList.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}

	this->m_editCounter.SetWindowText(::ToString(nRow));

}

void CHostListDlg::OnBnClickedButtonHostRefresh()
{
	RefreshHostList();
}

void CHostListDlg::OnBnClickedButtonHostAdd()
{
	
	CMultiSelectHost dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	//dlg.SetTitle(LoadStringById(IDS_PLANOVIEW_STR007));
	dlg.m_exWhere = "(domainName != 'localhost' or domainName is null)";

	if(dlg.DoModal() != IDOK) return;

	CDirtyFlag aInfoDirty;
	aInfoDirty.SetDirty("domainName", true);

	int nRow=0;
	for(int i=0; i<dlg.m_astrSelHostList.GetCount() ;i++)
	{
		CString objectId = dlg.m_astrSelHostList[i];
		TraceLog(("Selected HostId = %s", objectId));

		
		SHostInfo Info;
		Info.siteId = dlg.m_astrSelSiteIdList[i];
		Info.hostId =  objectId;
		Info.domainName = m_cacheServer;

		if(CCopModule::GetObject()->SetHost(Info,GetEnvPtr()->m_szLoginID,&aInfoDirty)){
			nRow++;
		}else{
			CString errMsg;
			errMsg.Format("%s add on CacheServer(%s) failed", objectId, m_cacheServer);  
			TraceLog((errMsg));
			UbcMessageBox(errMsg);
		}
		
	}
	RefreshHostList();
}

void CHostListDlg::OnBnClickedButtonHostDel()
{
	bool bCheck = false;
	for(int nRow = 0; nRow < m_lcHostList.GetItemCount(); nRow++)
	{
		if(m_lcHostList.GetCheck(nRow))
		{
			bCheck = true;
			break;
		}
	}

	if(!bCheck)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}

	CWaitMessageBox wait;
	CDirtyFlag aInfoDirty;
	aInfoDirty.SetDirty("domainName", true);

	for(int nRow = m_lcHostList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		// 선택되어 있지 않으면 스킵
		if(!m_lcHostList.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lcHostList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsInfoList.GetAt(pos);
		Info.domainName = "";

		// 서버에 단말삭제명령 호출
		if(Info.siteId.GetLength()==0 || Info.hostId.GetLength()==0) continue;
		if(!CCopModule::GetObject()->SetHost(Info,GetEnvPtr()->m_szLoginID,&aInfoDirty)) continue;

		// 선택해제
		m_lcHostList.SetCheck(nRow, FALSE);

		m_lsInfoList.RemoveAt(pos);
		m_lcHostList.DeleteItem(nRow);
	}

	UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG003), MB_ICONINFORMATION);
}

void CHostListDlg::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	m_lcHostList.SetCheck(nRow, FALSE);

	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifyHost(arRow, true);

}
void CHostListDlg::ModifyHost(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lcHostList.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lcHostList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsInfoList.GetAt(pos);
		CHostDetailDlg dlg;

		dlg.SetInfo(Info);
		if(dlg.DoModal() != IDOK)
		{
			dlg.GetInfo(Info);
			m_lsInfoList.GetAt(pos) = Info;
			continue;
		}
		dlg.GetInfo(Info);

		if(!CCopModule::GetObject()->SetHost(Info, GetEnvPtr()->m_szLoginID))
		{
			CString m_szMsg;
			m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
		m_lsInfoList.GetAt(pos) = Info;
	}
	if(bModified)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG006), MB_ICONINFORMATION);
		RefreshHostList();
	}
}
