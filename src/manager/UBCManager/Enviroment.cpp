#include "stdafx.h"
#include "enviroment.h"
#include <fstream>
#include <iostream>
#include "common/libFTPClient\FTPClient.h"
#include "common/libCommon/ubcMuxRegacy.h"
//#include "common/libCommon/ubcHost.h"
//#include "CustomerInfoDlg.h"
#include "common/libFileServiceWrap/FileServiceWrapDll.h"
#ifdef _DEBUG
	#pragma comment(lib, "FileServiceWrap_d.lib")
#else
	#pragma comment(lib, "FileServiceWrap.lib")
#endif


CEnviroment* CEnviroment::m_pEnvPtr = NULL;

CEnviroment::CEnviroment()
{
	m_szSite.Empty();
	m_szLoginID.Empty();
	m_szPassword.Empty();
	m_Authority = CCopModule::eSiteUnkown;

	InitPath();
	InitLoginInfo();

	char buf[1024];
	CString szPath, szTemp;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, _UBCSTUDIO_EE_ENV_INI);

	::GetPrivateProfileString("STUDIO", "TYPE", "UBC", buf, 1024, szPath);
	szTemp = buf;
	if(szTemp == "USTB"){
		m_StudioType = eUSTBType;
	}else{
		m_StudioType = eUBCType;
	}

	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);

	::GetPrivateProfileString(_T("CUSTOMER_INFO"), _T("NAME"), _T(""), buf, 1024-1, szPath);
	szTemp = buf;

	// 0001408: Customer 정보 입력을 받는다.
	// 로그인과정으로 이동
	//if(szTemp.IsEmpty())
	//{
	//	CCustomerInfoDlg dlg;
	//	if(dlg.DoModal() == IDOK)
	//	{
	//		szTemp = dlg.GetSelectCustumerInfo();
	//		::WritePrivateProfileString(_T("CUSTOMER_INFO"), _T("NAME"), szTemp, szPath);
	//	}
	//}

	if     (szTemp == "NARSHA" ) m_Customer = eNARSHA;
	else if(szTemp == "LOTTE"  ) m_Customer = eLOTTE;
	else if(szTemp == "ADASSET") m_Customer = eADASSET; // 창일향 : 
	else if(szTemp == "KIA"    ) m_Customer = eKIA;
	else if(szTemp == "HYUNDAI") m_Customer = eHYUNDAI;
	else if(szTemp == "KMNL"   ) m_Customer = eKMNL;
	else if(szTemp == "KMCL"   ) m_Customer = eKMCL;
	else if(szTemp == "KPOST"  ) m_Customer = eKPOST; // 우정국
	else if(szTemp == "HYUNDAIQ") m_Customer = eHYUNDAIQ; //품확동
	else                         m_Customer = eSQISOFT;

	m_strCustomer = szTemp;

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	::GetPrivateProfileString(_T("ROOT"), _T("HTTP_ONLY"), _T(""), buf, 1024, szPath);
	m_strHttpOnly = buf;

	TraceLog(("Create CEnviroment : %s", m_strCustomer));
}

CEnviroment::~CEnviroment()
{
	TraceLog(("Destroy CEnviroment"));
}

CEnviroment* CEnviroment::GetObject()
{
	if(!m_pEnvPtr)
		m_pEnvPtr = new CEnviroment;
	return m_pEnvPtr;
}

void CEnviroment::Release()
{
	if(m_pEnvPtr)
		delete m_pEnvPtr;
	m_pEnvPtr = NULL;
}

void CEnviroment::InitPath()
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	m_szDrive = szDrive;
	m_szPath = szPath;
	m_szFile = m_szName = szFilename;
	m_szFile += m_szExt = szExt;
	m_szModule = szModule;

	CString ini, szTemp;
	ini.Format(_T("%s%sdata\\%s"), m_szDrive, m_szPath, UBCVARS_INI);
	char buf[1024];
	::GetPrivateProfileString(_T("STUDIO"), _T("PathOpenFile"), _T(""), buf, 1024, ini);
	m_szPathOpenFile = buf;

}

void CEnviroment::InitLoginInfo()
{
	CString szFile = m_szDrive + m_szPath + _T("data\\");
	szFile += LOGIN_DAT;

	std::ifstream ifLogin(szFile, ios::in | ios_base::binary);
	if( ifLogin.fail() ) {
		ifLogin.close();
		return;
	}

	SLoginInfo Info;
	ZeroMemory(&Info, sizeof(Info));
	ifLogin.read(reinterpret_cast<char*>(&Info),sizeof(SLoginInfo));
	if( ifLogin.fail() ) {
		ifLogin.close();
		return;
	}

	ifLogin.close();

	m_szSite = Info.szSite;
	m_szLoginID = Info.szID;
	m_szPassword = Encrypt(Info.szPW, Info.nPWSize);
}

void CEnviroment::SaveLoginInfo(bool bCheck)
{
	SLoginInfo Info;
	ZeroMemory(&Info, sizeof(Info));

	CString szFile = m_szDrive + m_szPath + _T("data\\");
	szFile += LOGIN_DAT;

	std::ofstream ofLogin(szFile, std::ios_base::binary);
	if( ofLogin.fail() ) {
		ofLogin.close();
		return;
	}

	if(bCheck){
		strcpy(Info.szSite, m_szSite);
		strcpy(Info.szID, m_szLoginID);
		strcpy(Info.szPW, m_szPassword);

		Info.nPWSize = strlen(Info.szPW);
		Encrypt(Info.szPW, Info.nPWSize);
	}

	ofLogin.write(reinterpret_cast<char*>(&Info),sizeof(SLoginInfo));
	if( ofLogin.fail() ) {
		ofLogin.close();
		return;
	}

	ofLogin.close();
}

char* CEnviroment::Encrypt(char* buf, int nSize)
{
	char cKey = 'A';
	for(int i = 0; i < nSize; i++){
		cKey++;
		if(cKey >= 'z')	cKey = '0';
		buf[i] ^= cKey;
	}
	return buf;
}

BOOL CEnviroment::InitUser()
{
	if(m_lsUser.GetCount() > 0)
		return FALSE;

	SUserInfo info;

	if(CCopModule::eSiteAdmin == m_Authority){
		info.siteId.Empty();
		info.userId.Empty();
	}
	else if(CCopModule::eSiteManager == m_Authority){
		info.siteId = m_szSite;
		info.userId.Empty();
	}
	else if(CCopModule::eSiteUser == m_Authority){
		info.siteId = m_szSite;
		info.userId = m_szLoginID;
	}
	else{
		info.siteId = m_szSite;
		info.userId = m_szLoginID;
	}

	return CCopModule::GetObject()->GetUser(info, m_lsUser);
}

BOOL CEnviroment::RefreshUser(SUserInfo& info)
{
	m_lsUser.RemoveAll();
	return CCopModule::GetObject()->GetUser(info, m_lsUser);
}

BOOL CEnviroment::InitSite()
{
	if(m_lsSite.GetCount() > 0)
		return FALSE;

	SSiteInfo info;
	if(CCopModule::eSiteAdmin != m_Authority){
		info.siteId = m_szSite;
	}

	return CCopModule::GetObject()->GetSite(info, m_lsSite);
}

BOOL CEnviroment::RefreshSite(SSiteInfo& info)
{
	m_lsSite.RemoveAll();

	return CCopModule::GetObject()->GetSite(info, m_lsSite);
}

//BOOL CEnviroment::InitHost(HostInfoList& lsHost, LPCTSTR szSite, LPCTSTR szWhere)
//{
//	if(lsHost.GetCount() > 0) return FALSE;
//
//	return RefreshHost(lsHost, szSite, szWhere);
//}
//
//BOOL CEnviroment::RefreshHost(HostInfoList& lsHost, LPCTSTR szSite, LPCTSTR szWhere)
//{
//	lsHost.RemoveAll();
//
//	CString szSiteID = m_szSite;
//	if(szSite){
//		szSiteID = szSite;
//	}
//	else if(CCopModule::eSiteAdmin == m_Authority){
//		szSiteID = _T("*");
//	}
//
//	cciCall aCall;
//	BOOL bRet = CCopModule::GetObject()->GetHost(&aCall, szSiteID, "*", szWhere);
//	if(bRet)
//	{
//		bRet = CCopModule::GetObject()->GetHostData(&aCall, lsHost);
//	}
//
//	return bRet;
//}

BOOL CEnviroment::RefreshPackageLog(LPCTSTR szWhere)
{
	m_lsPackageStateLog.RemoveAll();

	CString strSiteId = m_szSite;
	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority)
	{
		strSiteId = "*";
	}

	return CCopModule::GetObject()->GetPackageStateLog(strSiteId, m_lsPackageStateLog, szWhere);
}

BOOL CEnviroment::RefreshPowerLog(LPCTSTR szWhere)
{
	m_lsPowerStateLog.RemoveAll();

	CString strSiteId = m_szSite;
	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority)
	{
		strSiteId = "*";
	}

	return CCopModule::GetObject()->GetPowerStateLog(strSiteId, m_lsPowerStateLog, szWhere);
}

BOOL CEnviroment::RefreshConnectionLog(LPCTSTR szWhere)
{
	m_lsConnectionStateLog.RemoveAll();

	CString strSiteId = m_szSite;
	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority)
	{
		strSiteId = "*";
	}

	return CCopModule::GetObject()->GetConnectionStateLog(strSiteId, m_lsConnectionStateLog, szWhere);
}

BOOL CEnviroment::RefreshFault(LPCTSTR szWhere)
{
	m_lsFault.RemoveAll();

	CString strSiteId = m_szSite;
	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority)
	{
		strSiteId = "*";
	}

	return CCopModule::GetObject()->GetFault(strSiteId, m_lsFault, szWhere);
}

BOOL CEnviroment::RefreshFaultLog(LPCTSTR szWhere)
{
	m_lsFaultLog.RemoveAll();

	CString strSiteId = m_szSite;
	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority)
	{
		strSiteId = "*";
	}

	return CCopModule::GetObject()->GetFaultLog(strSiteId, m_lsFaultLog, szWhere);
}

bool CEnviroment::GetFile(LPCTSTR szFile, LPCTSTR szRemote, LPCTSTR szLocal, LPCTSTR szSite)
{
	bool bRet = false;

	ubcMuxData* aData = NULL;
	if(szSite)
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	else
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(m_szSite);

	if(!aData){
		return bRet;
	}

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	if(m_strHttpOnly.CompareNoCase("CLIENT") == 0)
	{
		CFileServiceWrap objFileSvcClient(aData->getFTPAddress_s().c_str(), aData->ftpPort);

		if(!objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str()))
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		bRet = objFileSvcClient.GetFile(CString(szRemote)+CString(szFile), CString(szLocal)+CString(szFile), 0, NULL);
		if(!bRet)
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		objFileSvcClient.Logout();
	}
	else
	{
		nsFTP::CFTPClient FtpClient;

		nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
		if (!FtpClient.Login(loginInfo)) {
			return bRet;
		}

		FtpClient.SetResumeMode(false);
		FtpClient.ChangeWorkingDirectory("/");

		bRet = FtpClient.DownloadFile(
					tstring(szRemote)+tstring(szFile), 
					tstring(szLocal)+tstring(szFile),
					nsFTP::CRepresentation(nsFTP::CType::Image()), true);

		FtpClient.Logout();
	}

	// 파일 다운로드가 안됐지만 파일이 생성된경우 파일을 삭제해야 함.
	if(!bRet){
		CString szBuf = szLocal;
		szBuf += szFile;

		CFileFind ff;
		if(ff.FindFile(szBuf)){
			::DeleteFile(szBuf);
		}
		ff.Close();
	}

	return bRet;
}

bool CEnviroment::PutFile(LPCTSTR szFile, LPCTSTR szLocal, LPCTSTR szRemote, LPCTSTR szSite)
{
	TraceLog(("PutFile(%s\\%s --> %s) with siteId %s", szLocal, szFile, szRemote, szSite));

	bool bRet = false;

	ubcMuxData* aData = NULL;
	if(szSite)
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	else
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(m_szSite);

	if(!aData) return bRet;

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	if(m_strHttpOnly.CompareNoCase("CLIENT") == 0)
	{
		CFileServiceWrap objFileSvcClient(aData->getFTPAddress_s().c_str(), aData->ftpPort);

		if(!objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str()))
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		bRet = objFileSvcClient.PutFile(CString(szLocal)+CString(szFile), CString(szRemote)+CString(szFile), 0, NULL);
		if(!bRet)
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		objFileSvcClient.Logout();
	}
	else
	{
		nsFTP::CFTPClient FtpClient;

		nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
		if (!FtpClient.Login(loginInfo)) return bRet;

		FtpClient.SetResumeMode(false);
		FtpClient.ChangeWorkingDirectory("/");

		FtpClient.MakeDirectory(tstring(szRemote));

		bRet = FtpClient.UploadFile(tstring(szLocal)+tstring(szFile)
								  , tstring(szRemote)+tstring(szFile)
								  , false
								  , nsFTP::CRepresentation(nsFTP::CType::Image())
								  , true
								  );

		FtpClient.Logout();
	}

	return bRet;
}

bool CEnviroment::RenameFile(LPCTSTR szOldFile, LPCTSTR szNewFile, LPCTSTR szRemote, LPCTSTR szSite)
{
	TraceLog(("RenameFile(%s\\%s --> %s\\%s) with siteId %s", szRemote, szOldFile, szRemote, szNewFile, szSite));

	bool bRet = false;

	ubcMuxData* aData = NULL;
	if(szSite)
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	else
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(m_szSite);

	if(!aData) return bRet;

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	if(m_strHttpOnly.CompareNoCase("CLIENT") == 0)
	{
		CFileServiceWrap objFileSvcClient(aData->getFTPAddress_s().c_str(), aData->ftpPort);

		if(!objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str()))
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		bRet = objFileSvcClient.MoveFile(CString(szRemote)+CString(szOldFile), CString(szRemote)+CString(szNewFile));
		if(!bRet)
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		objFileSvcClient.Logout();
	}
	else
	{
		nsFTP::CFTPClient FtpClient;

		nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
		if (!FtpClient.Login(loginInfo)) return bRet;

		FtpClient.SetResumeMode(false);
		FtpClient.ChangeWorkingDirectory("/");

		bRet = FtpClient.Rename(tstring(szRemote)+tstring(szOldFile), tstring(szRemote)+tstring(szNewFile));

		FtpClient.Logout();
	}

	return bRet;
}

bool CEnviroment::DeleteFile(LPCTSTR szFile, LPCTSTR szRemote, LPCTSTR szSite)
{
	TraceLog(("DeleteFile(%s\\%s) with siteId %s", szRemote, szFile, szSite));

	bool bRet = false;

	ubcMuxData* aData = NULL;
	if(szSite)
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(szSite);
	else
		aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(m_szSite);

	if(!aData) return bRet;

	// 2012.02.28 ftp대신 http로 파일전송을 할 수 있도록 한다.
	if(m_strHttpOnly.CompareNoCase("CLIENT") == 0)
	{
		CFileServiceWrap objFileSvcClient(aData->getFTPAddress_s().c_str(), aData->ftpPort);

		if(!objFileSvcClient.Login(aData->ftpId.c_str(), aData->ftpPasswd.c_str()))
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		bRet = objFileSvcClient.DeleteFile(CString(szRemote)+CString(szFile));
		if(!bRet)
		{
			UbcMessageBox(objFileSvcClient.GetErrorMsg());
			return bRet;
		}

		objFileSvcClient.Logout();
	}
	else
	{
		nsFTP::CFTPClient FtpClient;

		nsFTP::CLogonInfo loginInfo(aData->getFTPAddress_s(), aData->ftpPort, aData->ftpId, aData->ftpPasswd);
		if (!FtpClient.Login(loginInfo)) return bRet;

		FtpClient.SetResumeMode(false);
		FtpClient.ChangeWorkingDirectory("/");

		bRet = FtpClient.Delete(tstring(szRemote)+tstring(szFile));

		FtpClient.Logout();
	}

	return bRet;
}

bool CEnviroment::GetAutoUpdate()
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);

	::GetPrivateProfileString("ROOT", "AutoUpdateFlag", "1", buf, 1024, szPath);
	int nValue = atoi(buf);

	return (nValue != 0);
}

void CEnviroment::SetAutoUpdate(bool bValue)
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);

	::WritePrivateProfileString("ROOT", "AutoUpdateFlag", ToString((int)bValue), szPath);
}

bool CEnviroment::GetAlphaUpdate()
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, ORBCONN_INI);

	::GetPrivateProfileString("UBCCENTER", "FTPID", "", buf, 1024, szPath);

	CString ftpId = buf;
	if(ftpId.Find("pre_") >= 0){
		return true;
	}
	return false;
}

void CEnviroment::SetAlphaUpdate(bool bValue)
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, ORBCONN_INI);

	CString newId;

	::GetPrivateProfileString("UBCCENTER", "FTPID", "", buf, 1024, szPath);
	CString ftpId = buf;
	if(bValue) {
		if(ftpId.Find("pre_") >= 0){
			return;
		}
		int idx = ftpId.Find("ent_center");
		if(idx < 0){
			return;
		}
		newId = ftpId.Mid(0,idx);
		newId.Append("pre_");
		newId.Append(ftpId.Mid(idx));
	}else{
		int idx = ftpId.Find("pre_");
		if(idx < 0) {
			return;
		}
		newId = ftpId.Mid(0,idx);
		newId.Append(ftpId.Mid(idx+4));
	}
	::WritePrivateProfileString("UBCCENTER", "FTPID", newId, szPath);
	return;
}

bool CEnviroment::GetColumnNextSchedule()
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);

	::GetPrivateProfileString("ROOT", "SHOW_NEXTSCHEDULE", "0", buf, 1024, szPath);
	int nValue = atoi(buf);

	return (nValue != 0);
}

void CEnviroment::SetColumnNextSchedule(bool bValue)
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);

	::WritePrivateProfileString("ROOT", "SHOW_NEXTSCHEDULE", ToString((int)bValue), szPath);
	return;
}

bool CEnviroment::GetFileSize(CString szPath, ULONGLONG& ulSize)
{
	CFileFind ff;
	bool bRet = true;

	if(ff.FindFile(szPath)){
		ff.FindNextFile();
		ulSize = ff.GetLength();
	}else{
		ulSize = 0;
		bRet = false;
	}

	ff.Close();
	return bRet;
}
bool CEnviroment::IsValidOpenFolder()
{
	if(m_szPathOpenFile.IsEmpty())
		return false;

	CString szFolder = m_szPathOpenFile;
	if(szFolder.Right(1)=="\\")
		szFolder += "*.*";

	CFileFind ff;
	return ff.FindFile(szFolder);
}
void CEnviroment::SetPathOpenFile(LPCTSTR str)
{
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);
	::WritePrivateProfileString("STUDIO", "PathOpenFile", str, szPath);
	m_szPathOpenFile = str;
}

bool CEnviroment::GetServerOS()
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);

	::GetPrivateProfileString("ROOT", "USE_AXIS2", "0", buf, 1024, szPath);
	int nValue = atoi(buf);

	return nValue;
}

void CEnviroment::SetServerOS(bool bValue)
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", m_szDrive, m_szPath, UBCVARS_INI);

	::WritePrivateProfileString("ROOT", "USE_AXIS2", ToString((int)bValue), szPath);
}

CString
CEnviroment::GetServerVersion()
{
	return CCopModule::GetObjectA()->GetVersion();
}