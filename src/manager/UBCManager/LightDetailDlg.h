#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "afxdtctl.h"
#include "resource.h"
#include "enviroment.h"
#include "common/HoverButton.h"
#include "common\utblistctrl.h"
#include "ubccopcommon\copmodule.h"

// CLightDetailDlg 대화 상자입니다.

class CLightDetailDlg : public CDialog
{
	DECLARE_DYNAMIC(CLightDetailDlg)

public:
	CLightDetailDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLightDetailDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LIGHT_DETAIL };

protected:
	bool	m_bInit;

	SLightInfo	m_LightInfo;
	CString		m_strWeekShutdownTime;
	CString		m_strShutdownTime;
	CString		m_strStartupTime;

	BOOL			m_bShutdown;
	CTime			m_tmShutdown;

	BOOL			m_bStartup;
	CTime			m_tmStartup;

	void			InitHolidayInfo();
	void			AddHolidayItem(CString str);
	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	void SetInfo(SLightInfo&);
	void GetInfo(SLightInfo&);
	void			SetHolliday(LPCTSTR);
	LPCTSTR			GetHolliday(CString&);

	CEdit m_editLightName;
	CComboBox m_cbLightType;
	CEdit m_editLightId;
	CEdit m_editDesc;
	CSliderCtrl m_sliderBright;
	CMonthCalCtrl m_ctrCalendar;
	CUTBListCtrl m_lscHoliday;
	CButton m_ckShutdown;
	CButton m_ckStartup;
	CDateTimeCtrl m_dtShutdown;
	CDateTimeCtrl m_dtStartup;
	CButton			m_ckWeek[7];
	CHoverButton	m_bnAddHoliday;
	CHoverButton	m_bnDelHoliday;
	CHoverButton m_btUState;
	CBitmap m_bmTState;
	CStatic m_stcTState;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonDel();
	afx_msg void OnBnClickedBnPowerSetByDay();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedCheckShutdown();
	afx_msg void OnBnClickedCheckPoweron();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnBnClickedButtonUstate();
};
