#pragma once

#include "Enviroment.h"
#include "common/HoverButton.h"
#include "common/utblistctrlex.h"
#include "common\reposcontrol.h"


#define		WM_CHANGE_TAB		(WM_USER + 1025)


/////////////////////////////////
// CPrimaryLogView 폼 뷰입니다.

class CPrimaryLogView : public CFormView
{
	DECLARE_DYNCREATE(CPrimaryLogView)

protected:
	CPrimaryLogView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CPrimaryLogView();

public:
	enum { IDD = IDD_LOG_MAIN };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate();

	DECLARE_MESSAGE_MAP()

	CString		m_strHostId;
	CString		m_strHostName;

	CMapStringToPtr		m_mapLogBrowser;
	CReposControl		m_Reposition;
	CRect				m_rcClient;

	enum SUB_WIN   { eApplyLog, eScheduleStateLog, ePowerStateLog, 
					eConnectionStateLog, eDownloadSummaryLog, eUserLog, 
					eBPLog, ePlayLog, eFaultLog, eMaxTab };

	void		InitTab();
	void		InitList();
	void		RefreshList();

	void		GetFilter();

public:
	void		SetCurrentTab(int nIdx);

	CHoverButton	m_btnRefresh;

	CStatic			m_labelFilterHost;
	CEdit			m_editHostName;
	CButton			m_btFilterHost;

	CDateTimeCtrl	m_dtcFilterStart;
	CDateTimeCtrl	m_dtcFilterEnd;
	CHoverButton	m_btnExcelSave;

	CString		m_strPackageId;

	CStatic m_packageLabel;
	CEdit m_filterPackage;
	CButton m_btFilterPackage;

	CTabCtrl		m_tcLogTab;
	CUTBListCtrlEx	m_lcLog[eMaxTab];

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedBtnRefresh();
	afx_msg void OnBnClickedBtnFilterHostName();
	afx_msg void OnBnClickedBtnToExcel();
	afx_msg void OnTcnSelchangeHostlogTab(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonFilterPackage();

	afx_msg LRESULT OnChangeTab(WPARAM wParam, LPARAM lParam);
};


/////////////////////////////////
class CLogBrowser
{
public:
	CLogBrowser(CUTBListCtrlEx* pLC) : m_pLogListCtrl(pLC) {};
	virtual ~CLogBrowser() {};

protected:
	CUTBListCtrlEx*	m_pLogListCtrl;

	CString		m_strHostId;
	CString		m_strPackageId;
	CTime		m_tmStart;
	CTime		m_tmEnd;

public:
	void		SetFilter(LPCSTR lpszPackageId,LPCSTR lpszHostId, CTime& tmStart, CTime& tmEnd);

	virtual void InitList() = NULL;
	virtual void RefreshList() = NULL;
	virtual void VisibleFilter(CPrimaryLogView* parent) {
		parent->m_filterPackage.ShowWindow(false);
		parent->m_btFilterPackage.ShowWindow(false);
		parent->m_packageLabel.ShowWindow(false);

		parent->m_labelFilterHost.ShowWindow(true);
		parent->m_editHostName.ShowWindow(true);
		parent->m_btFilterHost.ShowWindow(true);
	}
};

/////////////////////////////////
// 컨텐츠 패키지 변경명령 로그
class CApplyLogBrowser: public virtual CLogBrowser
{
public:
	CApplyLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CApplyLogBrowser() {};

protected:
	enum { eSite, eHost, ePackage, eApplyTime, eHow, eWho, eWhy, eMaxCol };

	CString						m_LogColumn[eMaxCol];
	ApplyLogInfoList			m_LogDataList;

public:
	virtual void InitList();
	virtual void RefreshList();
};

/////////////////////////////////
// 컨텐츠 패키지 변경결과 로그
class CScheduleStateLogBrowser: public virtual CLogBrowser
{
public:
	CScheduleStateLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CScheduleStateLogBrowser() {};

protected:
	enum { eSite, eHost, eEventTime, eLastSchedule1, eAutoSchedule1, eCurrentSchedule1, eMaxCol };

	CString						m_LogColumn[eMaxCol];
	ScheduleStateLogInfoList	m_LogDataList;

public:
	virtual void InitList();
	virtual void RefreshList();
};

/////////////////////////////////
// 전원On/Off 로그
class CPowerStateLogBrowser: public virtual CLogBrowser
{
public:
	CPowerStateLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CPowerStateLogBrowser() {};

protected:
	enum { eSite, eHost, eEventTime, eBootUpTime, eBootDownTime, eMaxCol };

	CString						m_LogColumn[eMaxCol];
	PowerStateLogInfoList		m_LogDataList;

public:
	virtual void InitList();
	virtual void RefreshList();
};

/////////////////////////////////
// Connection 로그
class CConnectionStateLogBrowser: public virtual CLogBrowser
{
public:
	CConnectionStateLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CConnectionStateLogBrowser() {};

protected:
	enum { eSite, eHost, eEventTime, eOperationalState, eMaxCol };

	CString						m_LogColumn[eMaxCol];
	ConnectionStateLogInfoList	m_LogDataList;

public:
	virtual void InitList();
	virtual void RefreshList();
};

/////////////////////////////////
// DownloadSummary 로그
class CSubDownloadSummaryLogBrowser : public virtual CLogBrowser
{
public:
	CSubDownloadSummaryLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CSubDownloadSummaryLogBrowser() {}
protected:
	enum { eSiteName,eHostName,eProgramId,eBrwId,
		eProgramState,eProgramStartTime,eProgramEndTime,eProgress,eMaxCol };

	//다운로드 결과
	enum E_DOWNLOAD_STATE
	{
		E_STATE_INIT			,  //초기상태
		E_STATE_DOWNLOADING		,  //다운로드 상태
		E_STATE_PARTIAL_FAIL	,  //일부파일 실패
		E_STATE_COMPLETE_FAIL	,  //전체 실패(접속실패등...)
		E_STATE_COMPLETE_SUCCESS,  //전체 성공
		E_STATE_COMPLETE_LOCAL_EXIST	//패키지의 모든컨텐츠 파일이 로컬에 존재하여 다운로드하지 않음
	};

	CString						m_LogColumn[eMaxCol];
	DownloadSummaryInfoList		m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();
	CString GetStateString(int nValue);

	virtual void VisibleFilter(CPrimaryLogView* parent) {
		parent->m_filterPackage.ShowWindow(true);
		parent->m_btFilterPackage.ShowWindow(true);
		parent->m_packageLabel.ShowWindow(true);

		parent->m_labelFilterHost.ShowWindow(true);
		parent->m_editHostName.ShowWindow(true);
		parent->m_btFilterHost.ShowWindow(true);
	}

};


/////////////////////////////////
// UserLog 로그
class CSubUserLogBrowser : public virtual CLogBrowser
{
public:
	CSubUserLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CSubUserLogBrowser() {}
protected:
	enum { eSiteId,eUserId,eLoginTime,eVia,eResult,eMaxCol };


	CString				m_LogColumn[eMaxCol];
	UserLogInfoList		m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

	virtual void VisibleFilter(CPrimaryLogView* parent) {
		parent->m_labelFilterHost.ShowWindow(false);
		parent->m_editHostName.ShowWindow(false);
		parent->m_btFilterHost.ShowWindow(false);

		parent->m_filterPackage.ShowWindow(false);
		parent->m_btFilterPackage.ShowWindow(false);
		parent->m_packageLabel.ShowWindow(false);
	}

	CString m_strUserId;
};

/////////////////////////////////
// BP 로그
class CSubBPLogBrowser : public virtual CLogBrowser
{
public:
	CSubBPLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CSubBPLogBrowser() {}
protected:
	enum { eSiteId,eBpName,eTouchTime,eWho,eAction,eMaxCol };


	CString				m_LogColumn[eMaxCol];
	BPLogInfoList		m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

	virtual void VisibleFilter(CPrimaryLogView* parent) {
		parent->m_labelFilterHost.ShowWindow(false);
		parent->m_editHostName.ShowWindow(false);
		parent->m_btFilterHost.ShowWindow(false);

		parent->m_filterPackage.ShowWindow(false);
		parent->m_btFilterPackage.ShowWindow(false);
		parent->m_packageLabel.ShowWindow(false);
	}

	CString m_strSiteId;
};

/////////////////////////////////
// Play 로그
class CSubPlayLogBrowser : public virtual CLogBrowser
{
public:
	CSubPlayLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CSubPlayLogBrowser() {}
protected:
	enum { eSiteId,eHostId,ePlayDate,eProgramId,eContentsName,eFilename,
		ePlayCount, ePlayTime, eFailCount, eMaxCol };


	CString				m_LogColumn[eMaxCol];
	PlayLogInfoList		m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

	virtual void VisibleFilter(CPrimaryLogView* parent) {
		parent->m_labelFilterHost.ShowWindow(true);
		parent->m_editHostName.ShowWindow(true);
		parent->m_btFilterHost.ShowWindow(true);

		parent->m_filterPackage.ShowWindow(false);
		parent->m_btFilterPackage.ShowWindow(false);
		parent->m_packageLabel.ShowWindow(false);
	}

	CString m_strSiteId;
};

/////////////////////////////////
// Fault 로그
class CSubFaultLogBrowser : public virtual CLogBrowser
{
public:
	CSubFaultLogBrowser(CUTBListCtrlEx* pLC) : CLogBrowser(pLC) {};
	virtual ~CSubFaultLogBrowser() {}
protected:
	enum { eSiteId,eHostId,eEventTime,eUpdateTime,eSeverity,eProbableCause,eCounter,eAdditionalText,eMaxCol };


	CString				m_LogColumn[eMaxCol];
	FaultLogInfoList		m_LogDataList;

	virtual void InitList();
	virtual void RefreshList();

	virtual void VisibleFilter(CPrimaryLogView* parent) {
		parent->m_labelFilterHost.ShowWindow(true);
		parent->m_editHostName.ShowWindow(true);
		parent->m_btFilterHost.ShowWindow(true);

		parent->m_filterPackage.ShowWindow(false);
		parent->m_btFilterPackage.ShowWindow(false);
		parent->m_packageLabel.ShowWindow(false);
	}

	CString m_strSiteId;
};
