// HostDetailDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "HostDetailDlg.h"
#include "HostPackageInfoDlg.h"
#include "HollidayDlg.h"
#include "common\UbcCode.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "CacheServerOrClientDlg.h"
#include "CacheServerSelectDlg.h"

#define COLOR_STATIC	RGB(236,233,216)

// CHostDetailDlg dialog
IMPLEMENT_DYNAMIC(CHostDetailDlg, CDialog)

CHostDetailDlg::CHostDetailDlg(CWnd* pParent /*=NULL*/) : CDialog(CHostDetailDlg::IDD, pParent)
, m_nLogLimit(15)
, m_nPeriod(60)
, m_nPeriod2(120)
, m_bDownloadTime(FALSE)
, m_strCategory(_T(""))
, m_bUseMonitorOffTime(FALSE)
, m_cacheServer(_T(""))
{
	m_bInit = false;

	if(::GetEnableShortScreenshotPeriod())
	{
		MIN_SCREENSHOT_PERIOD = 1;
	}
	else
	{
		MIN_SCREENSHOT_PERIOD = 120;
	}
}

CHostDetailDlg::~CHostDetailDlg()
{
}

void CHostDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_TERID, m_edtTerminalId);
	DDX_Text(pDX, IDC_EDIT_SITEID, m_edtSiteName);
	DDX_Text(pDX, IDC_EDIT_TERNAME, m_edtTerminalName);
	DDX_Text(pDX, IDC_EDIT_LASTBOOTTIME, m_edtBootTime);
	DDX_Text(pDX, IDC_EDIT_POWER_MNG_INFO, m_edtPowerMngInfo);
	DDX_Text(pDX, IDC_EDIT_IPADDR, m_edtIpAddr);
	DDX_Text(pDX, IDC_EDIT_MACADDR, m_edtMacAddr);
	DDX_Text(pDX, IDC_EDIT_SERIALNO, m_edtSerialNo);
	DDX_Text(pDX, IDC_EDIT_EDITION, m_edtEdition);
	DDX_Text(pDX, IDC_EDIT_OS, m_edtOs);
	DDX_Text(pDX, IDC_EDIT_VERSION, m_edtVersion);
	DDX_Text(pDX, IDC_EDIT_REGISTDATE, m_edtRegstDate);
	DDX_Text(pDX, IDC_EDIT_SERVERID, m_edtServerId);
	DDX_Text(pDX, IDC_EDIT_VENDOR, m_edtVendor);
	DDX_Text(pDX, IDC_EDIT_MODEL, m_edtModel);
	//	DDX_Text(pDX, IDC_EDIT_PERIOD, m_edtPeriod);
	//	DDX_Text(pDX, IDC_EDIT_PERIOD2, m_edtPeriod2);
	DDX_Text(pDX, IDC_EDIT_PORT, m_edtPort);
	DDX_Text(pDX, IDC_EDIT_LOCATION, m_edtLocation);
	DDX_Text(pDX, IDC_EDIT_DESC, m_edtDesc);
	DDX_Text(pDX, IDC_EDIT_CPUTEMP, m_szCPUTemp);
	DDX_Text(pDX, IDC_STATIC_LASTUPTIME, m_szLastUpTime);
	DDX_Text(pDX, IDC_EB_SHUTDOWNTIME, m_strShutdownDownTime);
	DDX_Text(pDX, IDC_EB_POWERONTIME, m_strPoweronTime);
	DDX_Text(pDX, IDC_EB_WEEKDAYS, m_strWeekDays);
	DDX_Text(pDX, IDC_EB_HOLIDAYS, m_strHolidays);
	DDX_Check(pDX, IDC_KB_USE_MONITOR_OFF_TIME, m_bUseMonitorOffTime);

	DDX_Control(pDX, IDC_SPIN1, m_spPeriod);
	DDX_Control(pDX, IDC_SPIN2, m_spPeriod2);
	DDX_Control(pDX, IDC_COMBO_POWERONMETHOD, m_cbPowerControl);
	DDX_Control(pDX, IDC_PROG_CPU, m_pgcCpu);
	DDX_Control(pDX, IDC_PROG_HDD1, m_pgcHdd1);
	DDX_Control(pDX, IDC_PROG_HDD2, m_pgcHdd2);
	DDX_Control(pDX, IDC_PROG_RMEMORY, m_pgcRMemory);
	DDX_Control(pDX, IDC_PROG_VMEMORY, m_pgcVMemory);
	DDX_Control(pDX, IDC_SLIDER_VOLUME, m_slcVolume);
	DDX_Control(pDX, IDC_CHECK_MUTE, m_ckMute);
	DDX_Control(pDX, IDC_RADIO_DIS1, m_rdVolDis1);
	DDX_Control(pDX, IDC_RADIO_DIS2, m_rdVolDis2);
	DDX_Control(pDX, IDC_BUTTON_USTATE, m_btUState);
	DDX_Control(pDX, IDC_STATIC_TSTATE, m_stcTState);
	DDX_Control(pDX, IDC_SPIN_LOGLIMIT, m_spLogLimit);

	DDX_Check(pDX, IDC_CHECK_AUTOUPDATE, m_bAutoUpdate);
	DDX_Text(pDX, IDC_EDIT_LOGLIMIT, m_nLogLimit);
	DDV_MinMaxInt(pDX, m_nLogLimit, 1, 365);
	DDX_Text(pDX, IDC_EDIT_PERIOD, m_nPeriod);
	DDV_MinMaxInt(pDX, m_nPeriod, 1, 600);
	DDX_Text(pDX, IDC_EDIT_PERIOD2, m_nPeriod2);
	DDV_MinMaxInt(pDX, m_nPeriod2, MIN_SCREENSHOT_PERIOD, 3600);
	DDX_Control(pDX, IDC_DT_DAY_BASE_TIME, m_dtDayBaseTime);
	DDX_Control(pDX, IDC_COMBO_VIDEO_RENDERING, m_cbVideoRendering);

	DDX_Text(pDX, IDC_EDIT_CATEGORY, m_strCategory);
	DDV_MaxChars(pDX, m_strCategory, 255);
	DDX_Check(pDX, IDC_CHECK_DOWNLOADTIME, m_bDownloadTime);
	DDX_Control(pDX, IDC_DT_DOWNLOADTIME, m_dtcDownloadTime);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cbType);
	DDX_Control(pDX, IDC_COMBO_MONITORTYPE, m_cbMonitorType);
	DDX_Text(pDX, IDC_EDIT_PROTOCOL, m_strProtocolType);
	DDX_Control(pDX, IDC_BUTTON_PACKAGE_INFO, m_packageInfo);
	DDX_Control(pDX, IDC_BUTTON_EXT, m_btnSizeToggle);
	DDX_Text(pDX, IDC_EDIT_CACHE_SERVER_IP, m_cacheServer);
	DDX_Control(pDX, IDC_EDIT_DISPLAYCOUNT, m_editDisplayCount);
	DDX_Control(pDX, IDC_BUTTON_CACHE_SET, m_btCacheSet);
	DDX_Control(pDX, IDC_BUTTON_CACHE_UNSET, m_btCacheUnset);
}

BEGIN_MESSAGE_MAP(CHostDetailDlg, CDialog)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_USTATE, &CHostDetailDlg::OnBnClickedButtonUstate)
	ON_BN_CLICKED(IDOK, &CHostDetailDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECK_MUTE, &CHostDetailDlg::OnBnClickedCheckMute)
	ON_CBN_SELCHANGE(IDC_COMBO_POWERONMETHOD, &CHostDetailDlg::OnCbnSelchangeComboPoweronmethod)
	ON_BN_CLICKED(IDC_BUTTON_HOLLSETTING, &CHostDetailDlg::OnBnClickedButtonHollsetting)
	ON_EN_KILLFOCUS(IDC_EDIT_PERIOD, &CHostDetailDlg::OnEnKillfocusEditPeriod)
	ON_EN_KILLFOCUS(IDC_EDIT_PERIOD2, &CHostDetailDlg::OnEnKillfocusEditPeriod2)
	ON_EN_KILLFOCUS(IDC_EDIT_LOGLIMIT, &CHostDetailDlg::OnEnKillfocusEditLoglimit)
	ON_BN_CLICKED(IDC_CHECK_DOWNLOADTIME, &CHostDetailDlg::OnBnClickedCheckDownloadtime)
	ON_BN_CLICKED(IDC_BUTTON_POWER_ON, &CHostDetailDlg::OnBnClickedButtonPowerOn)
	ON_BN_CLICKED(IDC_KB_USE_MONITOR_OFF_TIME, &CHostDetailDlg::OnBnClickedKbUseMonitorOffTime)
	ON_REGISTERED_MESSAGE(WM_PROGRESSEX_THRESHOLD_CHANGE, OnProgressExThresholdChanged)
	ON_BN_CLICKED(IDC_BUTTON_EXT, &CHostDetailDlg::OnBnClickedButtonExt)
	ON_BN_CLICKED(IDC_BUTTON_PACKAGE_INFO, &CHostDetailDlg::OnBnClickedButtonPackageInfo)
	ON_BN_CLICKED(IDC_BUTTON_CACHE_SET, &CHostDetailDlg::OnBnClickedButtonCacheSet)
	ON_BN_CLICKED(IDC_BUTTON_CACHE_UNSET, &CHostDetailDlg::OnBnClickedButtonCacheUnset)
END_MESSAGE_MAP()

// CHostDetailDlg message handlers
BOOL CHostDetailDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	

	m_bInit = true;
	CString szBuf;

	//OnBnClickedButtonExt(); //skpark 처음부터 작게 나오도록 한다.
	//m_btnSizeToggle.LoadBitmap(IDB_BTN_SIZETOGGLE, RGB(255, 255, 255));//skpark

	//skpark 2014.03.03  WIA 만을 위한 특수코드
	if(GetEnvPtr()->m_strCustomer == "WIA"){
		GetDlgItem(IDC_KB_USE_MONITOR_OFF_TIME)->ShowWindow(SW_HIDE);
	}

	CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbType, _T("HostType"));
	m_cbType.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_cbType, m_HostInfo.hostType));

	CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbMonitorType, _T("MonitorType"));
	m_cbMonitorType.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_cbMonitorType, m_HostInfo.monitorType));

	m_spLogLimit.SetRange32(1, 365);
	m_spPeriod.SetRange32(30, 600);
	m_spPeriod2.SetRange32(MIN_SCREENSHOT_PERIOD, 3600);

	m_cbPowerControl.EnableWindow(TRUE);
	m_cbPowerControl.AddString("Not Support");
	m_cbPowerControl.AddString("AMT");
	m_cbPowerControl.AddString("WOL");
	m_cbPowerControl.AddString("WOL_REPEATER");
	m_cbPowerControl.AddString("WOR");
	m_cbPowerControl.AddString("WAKE-UP");

	m_pgcCpu.SetStyle(PROGRESS_TEXT);
	m_pgcCpu.SetRange(0, 100);
	szBuf.Format("%3.1f%%", m_HostInfo.cpuUsed);
	m_pgcCpu.SetText(szBuf);
	m_pgcCpu.SetPos(m_HostInfo.cpuUsed);

	m_pgcHdd1.SetStyle(PROGRESS_TEXT);
	m_pgcHdd1.SetRange(0, 100);
	m_pgcHdd1.EnableWindow(!m_HostInfo.filesystemInfo[0].IsEmpty());
	m_pgcHdd1.SetText(_T(" "));
	if(!m_HostInfo.filesystemInfo[0].IsEmpty() || m_HostInfo.filesystemCapacity[0] > 0)
	{
		szBuf.Format("%3.1f%%", m_HostInfo.filesystemCapacity[0]);
		m_pgcHdd1.SetText(m_HostInfo.filesystemInfo[0] + _T(" ") + szBuf);
		m_pgcHdd1.SetPos(m_HostInfo.filesystemCapacity[0]);
		m_pgcHdd1.VisibleThreshold(true);
		m_pgcHdd1.EnableThreshold(true);
		m_pgcHdd1.SetThresholdMinMax(50, 100);
		m_pgcHdd1.SetThreshold(m_HostInfo.hddThreshold);
		m_pgcHdd1.SetThresholdNotify(GetSafeHwnd(), WM_PROGRESSEX_THRESHOLD_CHANGE);

		if     (m_HostInfo.filesystemCapacity[0] >= 90) m_pgcHdd1.SetForeColor(RGB(255,128,128)); // RED
		else if(m_HostInfo.filesystemCapacity[0] >= 80) m_pgcHdd1.SetForeColor(RGB(255,128,0)); // ORANGE
		else if(m_HostInfo.filesystemCapacity[0] >= 70) m_pgcHdd1.SetForeColor(RGB(255,255,0)); // YELLOW
		else                                            m_pgcHdd1.SetForeColor(RGB(128,255,0)); // GREEN
	}

	m_pgcHdd2.SetStyle(PROGRESS_TEXT);
	m_pgcHdd2.SetRange(0, 100);
	m_pgcHdd2.EnableWindow(!m_HostInfo.filesystemInfo[1].IsEmpty());
	m_pgcHdd2.SetText(_T(" "));
	if(!m_HostInfo.filesystemInfo[1].IsEmpty())
	{
		szBuf.Format("%3.1f%%", m_HostInfo.filesystemCapacity[1]);
		m_pgcHdd2.SetText(m_HostInfo.filesystemInfo[1] + _T(" ") + szBuf);
		m_pgcHdd2.SetPos(m_HostInfo.filesystemCapacity[1]);
		// hdd임계값은 하나이므로 hdd 모두 동일한 임계치로 설정한다.
		m_pgcHdd2.VisibleThreshold(true);
		m_pgcHdd2.EnableThreshold(true);
		m_pgcHdd2.SetThresholdMinMax(50, 100);
		m_pgcHdd2.SetThreshold(m_HostInfo.hddThreshold);
		m_pgcHdd2.SetThresholdNotify(GetSafeHwnd(), WM_PROGRESSEX_THRESHOLD_CHANGE);

		if     (m_HostInfo.filesystemCapacity[1] >= 90) m_pgcHdd2.SetForeColor(RGB(255,128,128)); // RED
		else if(m_HostInfo.filesystemCapacity[1] >= 80) m_pgcHdd2.SetForeColor(RGB(255,128,0)); // ORANGE
		else if(m_HostInfo.filesystemCapacity[1] >= 70) m_pgcHdd2.SetForeColor(RGB(255,255,0)); // YELLOW
		else                                            m_pgcHdd2.SetForeColor(RGB(128,255,0)); // GREEN
	}

	m_pgcRMemory.SetStyle(PROGRESS_TEXT);
	m_pgcRMemory.SetRange(0, 100);
	float fRMem = 0.0f;
	if(m_HostInfo.realMemoryTotal)
		fRMem = (m_HostInfo.realMemoryUsed*100)/m_HostInfo.realMemoryTotal;
	szBuf.Format("%3.1f%%", fRMem);
	m_pgcRMemory.SetText(szBuf);
	m_pgcRMemory.SetPos(fRMem);

	m_pgcVMemory.SetStyle(PROGRESS_TEXT);
	m_pgcVMemory.SetRange(0, 100);
	float fVMem = 0.0f;
	if(m_HostInfo.virtualMemoryTotal)
		fVMem = (m_HostInfo.virtualMemoryUsed*100)/m_HostInfo.virtualMemoryTotal;
	szBuf.Format("%3.1f%%", fVMem);
	m_pgcVMemory.SetText(szBuf);
	m_pgcVMemory.SetPos(fVMem);

	m_edtTerminalId = m_HostInfo.hostId;
	m_edtTerminalName = m_HostInfo.hostName;
	m_edtSiteName = m_HostInfo.siteName;

	if(m_HostInfo.bootUpTime.GetTime())
		m_edtBootTime = m_HostInfo.bootUpTime.Format(STR_DEF_TIME);;

	m_edtIpAddr = m_HostInfo.ipAddress;
	m_edtMacAddr = m_HostInfo.macAddress;
	m_edtSerialNo.Format(_T("%d"), m_HostInfo.serialNo);
	CString dcounter;
	dcounter.Format(_T("%d"), m_HostInfo.displayCounter);
	m_editDisplayCount.SetWindowText(dcounter);
	m_edtEdition = m_HostInfo.edition;
	m_edtOs = m_HostInfo.os;
	m_edtVersion = m_HostInfo.version;

	if(m_HostInfo.monitorUseTime < 0)
	{
		SetDlgItemText(IDC_EDIT_MONITOR_USE_TIME, _T("Unknown"));
	}
	else
	{
		SetDlgItemText(IDC_EDIT_MONITOR_USE_TIME, ToString(m_HostInfo.monitorUseTime));
	}

	if(m_HostInfo.authDate.GetTime())
		m_edtRegstDate = m_HostInfo.authDate.Format(STR_DEF_TIME);

	m_edtServerId = m_HostInfo.mgrId;
	m_edtVendor = m_HostInfo.vendor;
	m_edtModel = m_HostInfo.model;
	m_nPeriod = GetValueRange((int)m_HostInfo.period, 1, 600);
	m_nPeriod2 = GetValueRange((int)m_HostInfo.screenshotPeriod, MIN_SCREENSHOT_PERIOD, 3600);
	m_edtPort = ToString(m_HostInfo.vncPort);
	m_edtLocation = m_HostInfo.location;

	m_edtPowerMngInfo = m_HostInfo.holiday;
	int nPos = 0;
	CString szToken;
	while(!(szToken = m_edtPowerMngInfo.Tokenize(EH_SEPARATOR, nPos)).IsEmpty())
	{
		if(szToken.Find(":") >= 0) {}
		else if(szToken.Find(".") >= 0)
		{
			m_strHolidays += szToken + " / ";
		}
		else if(szToken.GetLength() == 1)
		{
			switch(_ttoi(szToken))
			{
			case 0 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR001) + ","; break;
			case 1 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR002) + ","; break;
			case 2 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR003) + ","; break;
			case 3 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR004) + ","; break;
			case 4 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR005) + ","; break;
			case 5 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR006) + ","; break;
			case 6 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR007) + ","; break;
			}
		}
	}

	m_strHolidays.Trim(" / ");
	m_strWeekDays.Trim(",");

	m_cbPowerControl.SetCurSel(m_HostInfo.powerControl);
	GetDlgItem(IDC_BUTTON_POWER_ON)->EnableWindow((IsAuth(_T("HOFF")) || IsAuth(_T("HRBT"))) && m_cbPowerControl.GetCurSel()!=0 && !m_HostInfo.operationalState);
	m_nLogLimit = GetValueRange((int)m_HostInfo.playLogDayLimit, 1, 365);

	if(m_HostInfo.lastUpdateTime.GetTime())
		m_szLastUpTime = m_HostInfo.lastUpdateTime.Format(STR_DEF_TIME);

	m_edtDesc = m_HostInfo.description;

	//if(m_HostInfo.startupTime.GetLength() == 5)
	//{
	//	m_kbPowerOn.SetCheck(true);
	//	CTime tmStartUpTime(2000,1,1
	//					  , _ttoi(m_HostInfo.startupTime.Left(2))
	//					  , _ttoi(m_HostInfo.startupTime.Right(2))
	//					  , 0 );
	//	m_dtcPowerOn.SetTime(&tmStartUpTime);
	//}

	m_strShutdownDownTime = m_HostInfo.shutdownTime;
	m_strPoweronTime = m_HostInfo.startupTime;
	//if(m_HostInfo.shutdownTime.GetLength() == 5)
	//{
	//	m_kbPowerOff.SetCheck(true);
	//	CTime tmShutDownTime(2000,1,1
	//					   , _ttoi(m_HostInfo.shutdownTime.Left(2))
	//					   , _ttoi(m_HostInfo.shutdownTime.Right(2))
	//					   , 0 );
	//	m_dtcPowerOff.SetTime(&tmShutDownTime);
	//}

	m_bUseMonitorOffTime = m_HostInfo.monitorOff;
	m_listMonitorOffTime = m_HostInfo.monitorOffList;

	m_strWeekShutdownTime = m_HostInfo.weekShutdownTime;

	if(m_HostInfo.adminState)
		m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
	else
		m_btUState.LoadBitmap(IDB_BUTTON_NOTUSE, RGB(12,12,12));

	m_bmTState.DeleteObject();
	if(m_HostInfo.operationalState){
		m_bmTState.LoadBitmap(IDB_ON);
		m_stcTState.SetBitmap(m_bmTState);
	}else{
		m_bmTState.LoadBitmap(IDB_OFF);
		m_stcTState.SetBitmap(m_bmTState);
	}

	m_slcVolume.SetRange(0, 100);
	m_slcVolume.SetPos(m_HostInfo.soundVolume);

	if(m_HostInfo.displayCounter == 2){
		m_rdVolDis2.EnableWindow(true);
		
		if(m_HostInfo.soundDisplay == 1){
			m_rdVolDis1.SetCheck(true);
		}else{
			m_rdVolDis2.SetCheck(true);
		}
	}else{
		m_rdVolDis2.EnableWindow(false);
		m_rdVolDis1.SetCheck(true);
		m_rdVolDis2.SetCheck(false);
	}

	m_bAutoUpdate = m_HostInfo.autoUpdateFlag;

	m_ckMute.SetCheck(m_HostInfo.mute);
	if(m_HostInfo.mute)
		m_slcVolume.EnableWindow(false);

	switch(m_HostInfo.cpuTemp){
		case 1: m_szCPUTemp = "C";break;
		case 2: m_szCPUTemp = "D";break;
		default : m_szCPUTemp = "UNKNOWN";
	}
	//m_szCPUTemp = ToString(m_HostInfo.cpuTemp);

	if(m_HostInfo.playLogDayCriteria.GetLength() != 5)
	{
		m_HostInfo.playLogDayCriteria = "09:00";
	}

	m_dtDayBaseTime.SetFormat("tt HH:mm");
	CTime tmTmp = CTime::GetCurrentTime();
	CTime tmDayBaseTime(  tmTmp.GetYear()
						, tmTmp.GetMonth()
						, tmTmp.GetDay()
						, _ttoi(m_HostInfo.playLogDayCriteria.Left(2))
						, _ttoi(m_HostInfo.playLogDayCriteria.Right(2))
						, 0
						);
	m_dtDayBaseTime.SetTime(&tmDayBaseTime);
	InitDateRange(m_dtDayBaseTime);

	// 태그
	m_strCategory = m_HostInfo.category;

	// 콘텐츠 다운로드 시간 설정
	m_dtcDownloadTime.SetFormat("tt HH:mm");
	m_bDownloadTime = (m_HostInfo.contentsDownloadTime != "NOSET" && m_HostInfo.contentsDownloadTime.GetLength() == 5);
	m_dtcDownloadTime.EnableWindow(m_bDownloadTime);
	if(m_bDownloadTime)
	{
		CTime tmDayBaseTime(  tmTmp.GetYear()
							, tmTmp.GetMonth()
							, tmTmp.GetDay()
							, _ttoi(m_HostInfo.contentsDownloadTime.Left(2))
							, _ttoi(m_HostInfo.contentsDownloadTime.Right(2))
							, 0
							);
		m_dtcDownloadTime.SetTime(&tmDayBaseTime);
		InitDateRange(m_dtcDownloadTime);
	}

	// 타임존이 유효한지 확인 후 화면에 보이도록 한다.
	if(m_HostInfo.gmt >= -24*60 && m_HostInfo.gmt <= 24*60)
	{
		int nTimezone = m_HostInfo.gmt * (m_HostInfo.gmt>=0 ? 1 : -1);
		CString strTimezone;
		strTimezone.Format(_T("UTC%c%02d:%02d")
						, (m_HostInfo.gmt>=0 ? '-' : '+')
						, nTimezone/60
						, nTimezone-(nTimezone/60)*60
						);
		SetDlgItemText(IDC_EDIT_TIMEZONE, strTimezone);
	}
	else
	{
		SetDlgItemText(IDC_EDIT_TIMEZONE, _T(""));
	}

	m_strProtocolType = m_HostInfo.protocolType;

	m_cbVideoRendering.ResetContent();
	m_cbVideoRendering.AddString(_T("Overlay"));
	m_cbVideoRendering.AddString(_T("VMR Windowless"));
	m_cbVideoRendering.AddString(_T("EVR"));
	if(m_HostInfo.renderMode == 0) m_HostInfo.renderMode = 1;
	m_cbVideoRendering.SetCurSel(m_HostInfo.renderMode-1);

	m_brushBG.CreateSolidBrush(COLOR_STATIC);

	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_TERID));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_SITEID));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_CURSCHE1));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_CURSCHE2));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_LASTALLOCTIME1));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_LASTALLOCTIME2));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_LASTALLOCSCHE1));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_LASTALLOCSCHE2));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_AUTOSCHE1));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_AUTOSCHE2));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_LASTBOOTTIME));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_IPADDR));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_MACADDR));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_SERIALNO));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_EDITION));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_VERSION));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_SERVERID));
	//m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_MODEL));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_PORT));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_REGISTDATE));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_OS));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_CPUTEMP));
	m_arStaticCtr.Add(GetDlgItem(IDC_EDIT_PROTOCOL));

	m_cacheServer = m_HostInfo.domainName; //skpark


	CString packageInfo; //skpark
	if(!m_HostInfo.currentPackage1.IsEmpty()){
		packageInfo = m_HostInfo.currentPackage1;
		m_packageInfo.SetWindowText(packageInfo);
	}else if(!m_HostInfo.currentPackage2.IsEmpty()){
		packageInfo = m_HostInfo.currentPackage2;
		m_packageInfo.SetWindowText(packageInfo);
	}

	UpdateData(FALSE);

	OnCbnSelchangeComboPoweronmethod();	

	GetDlgItem(IDOK)->EnableWindow(IsAuth(_T("HMOD")));

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CHostDetailDlg::OnDestroy()
{
	CDialog::OnDestroy();

	m_bInit = false;
	m_bmTState.DeleteObject();
}

void CHostDetailDlg::OnClose()
{
	m_bInit = false;
	m_bmTState.DeleteObject();

	CDialog::OnClose();
}

HBRUSH CHostDetailDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	for(int i = 0; i < m_arStaticCtr.GetCount(); i++){
		CWnd* wnd = m_arStaticCtr.GetAt(i);
		if(wnd->GetSafeHwnd() == pWnd->GetSafeHwnd()){
			pDC->SetBkColor(COLOR_STATIC);
			hbr = (HBRUSH)m_brushBG;
			return hbr;
		}
	}

	return hbr;
}

void CHostDetailDlg::SetInfo(SHostInfo& info)
{
	HostInfoList lsHost;

	cciCall aCall;
	if(CCopModule::GetObject()->GetHostForAll(&aCall, info.siteId, info.hostId, NULL))
	{
		CCopModule::GetObject()->GetHostData(&aCall, lsHost);
	}
	
	POSITION pos = lsHost.GetHeadPosition();
	if(pos){
		m_HostInfo = lsHost.GetNext(pos);
		m_HostInfo.timeLineInfo = info.timeLineInfo;
	}else{
		m_HostInfo = info;
	}
}

void CHostDetailDlg::GetInfo(SHostInfo& info)
{
	info = m_HostInfo;
}



int CHostDetailDlg::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	LVFINDINFO lvFind;
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = ((CUTBListCtrl*)lParam)->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = ((CUTBListCtrl*)lParam)->FindItem(&lvFind);

	int nCol = 1;
	CString strItem1 = ((CUTBListCtrl*)lParam)->GetItemText(nIndex1, nCol);
	CString strItem2 = ((CUTBListCtrl*)lParam)->GetItemText(nIndex2, nCol);
	return strItem1.Compare(strItem2);
}

void CHostDetailDlg::OnBnClickedButtonUstate()
{
	m_HostInfo.adminState = !m_HostInfo.adminState;
	if(m_HostInfo.adminState)
		m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
	else
		m_btUState.LoadBitmap(IDB_BUTTON_NOTUSE, RGB(12,12,12));
}

void CHostDetailDlg::OnBnClickedCheckMute()
{
	UpdateData();
	if(m_ckMute.GetCheck())
		m_slcVolume.EnableWindow(false);
	else
		m_slcVolume.EnableWindow(true);
}

void CHostDetailDlg::OnBnClickedOk()
{
	UpdateData();

	if(m_cbType.GetCount() > 0 && m_cbType.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTDETAILDLG_MSG003));
		return;
	}

	m_HostInfo.hostType = (long)m_cbType.GetItemData(m_cbType.GetCurSel());
	m_HostInfo.vendor = m_edtVendor;
	m_HostInfo.monitorType = (long)m_cbMonitorType.GetItemData(m_cbMonitorType.GetCurSel());
	m_HostInfo.model = m_edtModel;
	m_HostInfo.domainName = m_cacheServer;//skpark
	m_HostInfo.period = m_nPeriod;
	m_HostInfo.screenshotPeriod = m_nPeriod2;
	m_HostInfo.vncPort = m_edtPort.IsEmpty()?0:_ttoi(m_edtPort);
	m_HostInfo.location = m_edtLocation;
	m_HostInfo.description = m_edtDesc;
	m_HostInfo.holiday = m_edtPowerMngInfo;
//	m_HostInfo.adminState;
	m_HostInfo.hostName = m_edtTerminalName;
	m_HostInfo.playLogDayLimit = m_nLogLimit;
	m_HostInfo.autoUpdateFlag = m_bAutoUpdate;

	m_HostInfo.mute = m_ckMute.GetCheck();
	m_HostInfo.soundVolume = m_slcVolume.GetPos();

	m_HostInfo.hddThreshold = m_pgcHdd1.GetThreshold();

	if(m_rdVolDis2.GetCheck()){
		m_HostInfo.soundDisplay = 2;
	}else{
		m_HostInfo.soundDisplay = 1;
	}

	m_HostInfo.powerControl = m_cbPowerControl.GetCurSel();
	//m_HostInfo.macAddress = this->m_edtMacAddr;

	//if(m_kbPowerOn.GetCheck())
	//{
	//	CTime tmStartUpTime;
	//	m_dtcPowerOn.GetTime(tmStartUpTime);
	//	m_HostInfo.startupTime = tmStartUpTime.Format("%H:%M");
	//}
	//else
	//{
	//	m_HostInfo.startupTime = "";
	//}

	m_HostInfo.shutdownTime = m_strShutdownDownTime;
	m_HostInfo.startupTime = m_strPoweronTime;
	//if(m_kbPowerOff.GetCheck())
	//{
	//	CTime tmShutDownTime;
	//	m_dtcPowerOff.GetTime(tmShutDownTime);
	//	m_HostInfo.shutdownTime = tmShutDownTime.Format("%H:%M");
	//}
	//else
	//{
	//	m_HostInfo.shutdownTime = "";
	//}

	m_HostInfo.monitorOff = (m_bUseMonitorOffTime == TRUE);
	m_HostInfo.monitorOffList = m_listMonitorOffTime;

	CTime tmTemp;
	m_dtDayBaseTime.GetTime(tmTemp);
	m_HostInfo.playLogDayCriteria = tmTemp.Format("%H:%M");

	m_HostInfo.renderMode = m_cbVideoRendering.GetCurSel() + 1;

	// 태그
	m_HostInfo.category = m_strCategory;

	// 콘텐츠 다운로드 시간 설정
	if(m_bDownloadTime)
	{
		m_dtcDownloadTime.GetTime(tmTemp);
		m_HostInfo.contentsDownloadTime = tmTemp.Format("%H:%M");
	}
	else
	{
		m_HostInfo.contentsDownloadTime.Empty();
	}

	m_HostInfo.weekShutdownTime = m_strWeekShutdownTime;

	{ // CacheServer 가 해제되었다면, 연결된 domainName을 모두 지워준다.
		CDirtyFlag aInfoDirty;
		aInfoDirty.SetDirty("domainName", true);

		int nRow = 0;
		POSITION pos = m_cacheClientList.GetHeadPosition();
		while(pos)
		{
			POSITION posOld = pos;
			SHostInfo Info = m_cacheClientList.GetNext(pos);

			Info.domainName = "";

			// 서버에 단말삭제명령 호출
			if(Info.siteId.GetLength()==0 || Info.hostId.GetLength()==0) continue;
			if(!CCopModule::GetObject()->SetHost(Info,GetEnvPtr()->m_szLoginID,&aInfoDirty)) continue;

			nRow++;
		}
		TraceLog(("%d host disconnected", nRow));
		m_cacheClientList.RemoveAll();
	}

	OnOK();
}

void CHostDetailDlg::OnCbnSelchangeComboPoweronmethod()
{
	// 단말이 켜져있지 않고 전원 켜기 방법이 선택되어 있는 경우 전원켜기버튼을 활성화 한다.
	GetDlgItem(IDC_BUTTON_POWER_ON)->EnableWindow((IsAuth(_T("HOFF")) || IsAuth(_T("HRBT"))) && m_cbPowerControl.GetCurSel()!=0 && !m_HostInfo.operationalState);
//	if(m_cbPowerControl.GetCurSel() == 0){
//		m_dtcPowerOn.EnableWindow(FALSE);
//		m_dtcPowerOff.EnableWindow(FALSE);
//	}else{
//		m_dtcPowerOn.EnableWindow(TRUE);
//		m_dtcPowerOff.EnableWindow(TRUE);
//	}
}

void CHostDetailDlg::OnBnClickedButtonHollsetting()
{
	UpdateData();

	CHollidayDlg dlg(this);
	dlg.SetHolliday(m_edtPowerMngInfo);
	dlg.SetPoweron(m_strPoweronTime);
	if(m_cbPowerControl.GetCurSel() != 0){
		dlg.ShowPoweron(true);
	}

    //skpark 2014.03.03  WIA 만을 위한 특수코드
	if(GetEnvPtr()->m_strCustomer == "WIA"){
		dlg.ShowMonitor(false);
	}

	// strHolliday 에 꺼짐시간이 있으며, 이 값으로 초기화 하므로 값을 넘길 필요없음.
	//dlg.m_bShutdown = m_kbPowerOff.GetCheck();
	//m_dtcPowerOff.GetTime(dlg.m_tmShutdown);

	dlg.m_bMonitorOff = m_bUseMonitorOffTime;
	dlg.m_listMonitorOffTime = m_listMonitorOffTime;

	dlg.m_strWeekShutdownTime = m_strWeekShutdownTime;


	if(dlg.DoModal() != IDOK) return;

	dlg.GetHolliday(m_edtPowerMngInfo);

	m_strShutdownDownTime.Empty();
	if(dlg.m_bShutdown)
	{
		m_strShutdownDownTime = dlg.m_tmShutdown.Format("%H:%M");
	}
	m_strPoweronTime.Empty();
	if(dlg.m_bPoweron)
	{
		m_strPoweronTime = dlg.m_tmPoweron.Format("%H:%M");
	}

	m_bUseMonitorOffTime = dlg.m_bMonitorOff;
	m_listMonitorOffTime = dlg.m_listMonitorOffTime;

	m_strWeekShutdownTime = dlg.m_strWeekShutdownTime;

	m_strWeekDays.Empty();
	m_strHolidays.Empty();
	int pos = 0;
	CString szToken;
	while(!(szToken = m_edtPowerMngInfo.Tokenize(EH_SEPARATOR, pos)).IsEmpty())
	{
		if(szToken.Find(":") >= 0) {}
		else if(szToken.Find(".") >= 0)
		{
			m_strHolidays += szToken + " / ";
		}
		else if(szToken.GetLength() == 1)
		{
			switch(_ttoi(szToken))
			{
			case 0 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR001) + ","; break;
			case 1 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR002) + ","; break;
			case 2 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR003) + ","; break;
			case 3 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR004) + ","; break;
			case 4 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR005) + ","; break;
			case 5 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR006) + ","; break;
			case 6 : m_strWeekDays += LoadStringById(IDS_HOSTDETAILDLG_STR007) + ","; break;
			}
		}
	}

	m_strHolidays.Trim(" / ");
	m_strWeekDays.Trim(",");

	UpdateData(FALSE);
}

void CHostDetailDlg::OnEnKillfocusEditPeriod()
{
	if(!m_bInit)	return;

	UpdateData(TRUE);

	CString strText;
	GetDlgItemText(IDC_EDIT_PERIOD, strText);
	if(strText.IsEmpty())
	{
		m_nPeriod = 60;
	}
	else
	{
		m_nPeriod = GetValueRange(m_nPeriod, 1, 600);
	}

	UpdateData(FALSE);
}

void CHostDetailDlg::OnEnKillfocusEditPeriod2()
{
	if(!m_bInit)	return;

	UpdateData(TRUE);
	
	CString strText;
	GetDlgItemText(IDC_EDIT_PERIOD2, strText);
	if(strText.IsEmpty())
	{
		m_nPeriod2 = MIN_SCREENSHOT_PERIOD;
	}
	else
	{
		m_nPeriod2 = GetValueRange(m_nPeriod2, MIN_SCREENSHOT_PERIOD, 3600);
	}

	UpdateData(FALSE);
}

void CHostDetailDlg::OnEnKillfocusEditLoglimit()
{
	if(!m_bInit)	return;

	UpdateData(TRUE);

	CString strText;
	GetDlgItemText(IDC_EDIT_LOGLIMIT, strText);
	if(strText.IsEmpty())
	{
		m_nLogLimit = 15;
	}
	else
	{
		m_nLogLimit = GetValueRange(m_nLogLimit, 1, 365);
	}

	UpdateData(FALSE);
}

void CHostDetailDlg::OnBnClickedCheckDownloadtime()
{
	UpdateData(TRUE);
	m_dtcDownloadTime.EnableWindow(m_bDownloadTime);
	UpdateData(FALSE);
}

// 단말전원켜기
void CHostDetailDlg::OnBnClickedButtonPowerOn()
{
	if(CCopModule::GetObject()->PowerOn(m_HostInfo.siteId, m_HostInfo.hostId))
	{
		// 전원켜기가 성공된 경우 다시 전원버튼을 누르지 못하게하기 위해 전원상태을 변경한다.
		m_HostInfo.operationalState = true;
		GetDlgItem(IDC_BUTTON_POWER_ON)->EnableWindow(FALSE);

		UbcMessageBox(LoadStringById(IDS_HOSTDETAILDLG_MSG001));
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_HOSTDETAILDLG_MSG002));
	}
}

void CHostDetailDlg::OnBnClickedKbUseMonitorOffTime()
{
	OnBnClickedButtonHollsetting();
}

LRESULT CHostDetailDlg::OnProgressExThresholdChanged(WPARAM wParam, LPARAM lParam)
{
	CProgressCtrlEx* pCtrl = (CProgressCtrlEx*)wParam;
	int nThreshold = (int)lParam;

	if(pCtrl == &m_pgcHdd1 && m_pgcHdd2.IsWindowEnabled())
	{
		m_pgcHdd2.SetThreshold(nThreshold);
	}

	if(pCtrl == &m_pgcHdd2 && m_pgcHdd1.IsWindowEnabled())
	{
		m_pgcHdd1.SetThreshold(nThreshold);
	}

	return 0;
}

void CHostDetailDlg::OnBnClickedButtonExt()
{
	//skpark
	CRect rect;
	GetWindowRect(&rect);

	int new_width = rect.Width();
	int new_left = rect.left;

	if(new_width > 950) {
		m_btnSizeToggle.SetWindowText("Detail >>");
		new_width -= 380;
		new_left += ((380/2)+10);
	}else{
		m_btnSizeToggle.SetWindowText("<< Simply");
		new_width += 380;
		new_left -= ((380/2)-10);
	}

	//CString debugMsg;
	//debugMsg.Format("current_width=%d, new_width=%d", current_width, new_width);
	//UbcMessageBox(debugMsg);

	MoveWindow(new_left,rect.top,new_width,rect.Height(),1);
}

void CHostDetailDlg::OnBnClickedButtonPackageInfo()
{
	//skpark
	CHostPackageInfoDlg aDlg(&m_HostInfo, this);
	aDlg.DoModal();
}

void CHostDetailDlg::OnBnClickedButtonCacheSet()
{
	CCacheServerOrClientDlg aDlg(this);
	if(aDlg.DoModal() == IDOK){
		if(aDlg.m_server_or_client == CCacheServerOrClientDlg::SERVER){
			m_cacheServer = "localhost";
			UpdateData(FALSE);
		}
		if(aDlg.m_server_or_client == CCacheServerOrClientDlg::CLIENT){

			CCacheServerSelectDlg dlg(GetEnvPtr()->m_strCustomer);
			dlg.m_currentHostID = this->m_edtTerminalId;
			dlg.m_bEditMode = true;
			dlg.m_nAuthority = GetEnvPtr()->m_Authority;
			dlg.m_strSiteId = GetEnvPtr()->m_szSite;
			if(dlg.DoModal() == IDOK){
				m_cacheServer = dlg.m_currentCacheServer;
				UpdateData(FALSE);
			}
		}
	}
}

void CHostDetailDlg::OnBnClickedButtonCacheUnset()
{
	m_cacheClientList.RemoveAll();

	if(m_cacheServer == "localhost"){
		CString strWhere = "domainName = '";
		strWhere += this->m_edtIpAddr;
		strWhere += ":8080'";

		cciCall aCall;
		BOOL bRet = CCopModule::GetObject()->GetHostForSelect(&aCall
															, _T("*")
															, _T("*")
															, strWhere
															);
		if(bRet)
		{
			CCopModule::GetObject()->GetHostData(&aCall
												, m_cacheClientList
												, 0
												);
			TraceLog(("%d data selected", m_cacheClientList.GetSize()));
		}else{
			TraceLog(("GetConnectedHostId failed"));
		}
	}
	m_cacheServer = "";
	UpdateData(FALSE);
}
