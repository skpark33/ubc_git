#pragma once

#include "ubccopcommon\eventhandler.h"


// CGeoMapFrm frame

class CGeoMapFrm : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CGeoMapFrm)
protected:
	CGeoMapFrm();           // protected constructor used by dynamic creation
	virtual ~CGeoMapFrm();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
