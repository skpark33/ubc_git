// HostView.cpp : implementation file
//
#include "stdafx.h"
#include "UBCManager.h"
#include "HostView.h"
#include "Enviroment.h"
#include "PackageChangeDlg.h"
#include "MainFrm.h"
#include "HostScreenView.h"
#include "HostDetailDlg.h"
#include "HostMultiModify.h"
#include "ChangeSiteDlg.h"
//#include "HddThresholdDlg.h"
#include "HddThresholdEx.h"
#include "ProcessView.h"

#include "common\ubcdefine.h"
//#include "ubccopcommon\HostInfoDlg.h"
#include "ubccopcommon\AuthCheckDlg.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libCommon\ubcMuxRegacy.h"
#include "common\libscratch\scratchUtil.h"
#include "common\libcommon\ubchost.h"
#include "common\libinstall\installutil.h"
#include "HostExcelSave.h"
//#include "DownloadStateDlg.h"
#include "HollidayDlg.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "ubccopcommon/SiteSelectDlg.h"
#include "ubccopcommon/PackageSelectDlg.h"
#include "ubccopcommon/ContentsSelectDlg.h"
#include "DownloadSummaryDlg.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "PackageChngLogDlg.h"
#include "MonitorPowerDlg.h"
#include "InputBox.h"
#include "IPInputDLG.h"
#include "PropertyDlg.h"
#include "AuthCheckExDlg.h"
#include "RemoteLoginSettingsDlg.h"
#include "DownloadTimeSetDlg.h"

//�ٿ��ε�E�ᰁE
//enum E_DOWNLOAD_REASON
//{
// E_REASON_INIT,    //�ʱ����
// E_REASON_CONNECTION_FAIL, //������ ������
// E_REASON_FILE_NOTFOUND,  //������ ������ ��������E����
// E_REASON_EXCEPTION_FTP,  //FTP ����E
// E_REASON_EXCEPTION_FILE, //���Ͼ׼��� ����E
// E_REASON_LOCAL_EXIST,  //���ÿ� �����Ͽ� �ٿ��ε�E����E����
// E_REASON_DOWNLOAD_SUCCESS, //�ٿ��ε�E����E
// E_REASON_UNKNOWN,   //�˼����� ����E�������E exception...)
//};
//
////�ٿ��ε�E����
//enum E_DOWNLOAD_STATE
//{
// E_STATE_INIT,    //�ʱ����
// E_STATE_DOWNLOADING,  //�ٿ��ε�E����
// E_STATE_PARTIAL_FAIL,  //�Ϻ����� ����
// E_STATE_COMPLETE_FAIL,  //��E?����(���ӽ��е�E..)
// E_STATE_COMPLETE_SUCCESS //��E?����E
//};

#define STR_ADMIN_ON			LoadStringById(IDS_HOSTVIEW_LIST001)
#define STR_ADMIN_OFF			LoadStringById(IDS_HOSTVIEW_LIST002)
#define STR_OPERA_ON			LoadStringById(IDS_HOSTVIEW_LIST003)
#define STR_OPERA_OFF			LoadStringById(IDS_HOSTVIEW_LIST004)
#define STR_NET_TRUE			LoadStringById(IDS_HOSTVIEW_LIST005)
#define STR_NET_FALSE			LoadStringById(IDS_HOSTVIEW_LIST006)

#define STR_HOST_SITE_NAME		LoadStringById(IDS_HOSTVIEW_LIST007)
#define STR_HOST_ID				LoadStringById(IDS_HOSTVIEW_LIST008)
#define STR_HOST_NAME			LoadStringById(IDS_HOSTVIEW_LIST009)
#define STR_HOST_VENDOR			LoadStringById(IDS_HOSTVIEW_LIST010)
#define STR_HOST_MONITORTYPE	LoadStringById(IDS_HOSTVIEW_LIST052)
#define STR_HOST_MODEL			LoadStringById(IDS_HOSTVIEW_LIST011)
#define STR_HOST_OS				LoadStringById(IDS_HOSTVIEW_LIST012)
#define STR_HOST_SERIAL			LoadStringById(IDS_HOSTVIEW_LIST013)
#define STR_HOST_PERIOD			LoadStringById(IDS_HOSTVIEW_LIST014)
#define STR_HOST_DESC			LoadStringById(IDS_HOSTVIEW_LIST015)
#define STR_HOST_IP				LoadStringById(IDS_HOSTVIEW_LIST016)
#define STR_HOST_ADMIN			LoadStringById(IDS_HOSTVIEW_LIST017)
#define STR_HOST_OPERA			LoadStringById(IDS_HOSTVIEW_LIST018)
#define STR_HOST_MAC			LoadStringById(IDS_HOSTVIEW_LIST019)
#define STR_HOST_EDIT			LoadStringById(IDS_HOSTVIEW_LIST020)
#define STR_HOST_BOOTUPTIME		LoadStringById(IDS_HOSTVIEW_LIST021)
#define STR_HOST_DISCNT			LoadStringById(IDS_HOSTVIEW_LIST022)
#define STR_HOST_AUTO1			LoadStringById(IDS_HOSTVIEW_LIST023)
#define STR_HOST_AUTO2			LoadStringById(IDS_HOSTVIEW_LIST024)
#define STR_HOST_CUR1			LoadStringById(IDS_HOSTVIEW_LIST025)
#define STR_HOST_CUR2			LoadStringById(IDS_HOSTVIEW_LIST026)
#define STR_HOST_LAST1			LoadStringById(IDS_HOSTVIEW_LIST027)
#define STR_HOST_LAST2			LoadStringById(IDS_HOSTVIEW_LIST028)
#define STR_HOST_NET1			LoadStringById(IDS_HOSTVIEW_LIST029)
//#define STR_HOST_NET2			LoadStringById(IDS_HOSTVIEW_LIST030)
#define STR_HOST_DOMAIN			LoadStringById(IDS_HOSTVIEW_LIST056)
#define STR_HOST_UDATE			LoadStringById(IDS_HOSTVIEW_LIST031)
#define STR_HOST_VNCPORT		LoadStringById(IDS_HOSTVIEW_LIST032)
#define STR_HOST_VNCSTAT		LoadStringById(IDS_HOSTVIEW_LIST033)
#define STR_HOST_VERSION		LoadStringById(IDS_HOSTVIEW_LIST034)
#define STR_HOST_DOWNLOADTIME	LoadStringById(IDS_HOSTVIEW_LIST035)
#define STR_HOST_TAG			LoadStringById(IDS_HOSTVIEW_LIST036)
#define STR_HOST_MONITOR_STAT	LoadStringById(IDS_HOSTVIEW_LIST041)
#define STR_HOST_TYPE   		LoadStringById(IDS_HOSTVIEW_LIST042)
#define STR_HOST_NEXT1			LoadStringById(IDS_HOSTVIEW_LIST053)
#define STR_HOST_NEXT2			LoadStringById(IDS_HOSTVIEW_LIST054)
#define STR_HOST_ADDR1			LoadStringById(IDS_HOSTVIEW_LIST055)


#define STR_SEP				_T(",")

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(0,128,255)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)

//�ٿ��ε�E����
static const char* DOWNLOAD_STATE[6] = { ""             // E_STATE_INIT,    //�ʱ����
									   , "Downloading"	// E_STATE_DOWNLOADING,  //�ٿ��ε�E����
									   , "Fail"         // E_STATE_PARTIAL_FAIL,  //�Ϻ����� ����
									   , "Fail"         // E_STATE_COMPLETE_FAIL,  //��E?����(���ӽ��е�E..)
									   , "Success"		// E_STATE_COMPLETE_SUCCESS //��E?����E
									   , "Success" };	// E_STATE_COMPLETE_LOCAL_EXIST //��Ű���� �������ÁE������ ���ÿ� �����Ͽ� �ٿ��ε�����E����

// CHostView
IMPLEMENT_DYNCREATE(CHostView, CFormView)


#define		FILTER_HEIGHT				150
#define		HOST_LIST_WIDTH				200

#define		TIME_OPEN_WIDTH				290
#define		TIME_CLOSE_WIDTH			0

CHostView::CHostView() : CFormView(CHostView::IDD)
,	m_Reposition(this)
//,	m_EventManager(this)
,	m_pMainFrame(NULL)
,	m_pCall(NULL)
,	m_showNextSchedule(false)
,	m_toggle(0)
,	m_time_width(0)
{
	m_pBackupHostList = NULL;

	m_pMainFrame = (CMainFrame*)AfxGetMainWnd();

}

CHostView::~CHostView()
{
	if(m_pCall)
	{
		delete m_pCall;
		m_pCall = NULL;
	}

	CEventHandler::SetIgnoreEvent(false);
	DeleteBackupHostList();
}

void CHostView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOST, m_lscHost);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_Filter);

	//	DDX_Control(pDX, IDC_BUTTON_HOST_ADD, m_btnAddHost);
	DDX_Control(pDX, IDC_BUTTON_HOST_DEL, m_btnDelHost);
	DDX_Control(pDX, IDC_BUTTON_HOST_MOD, m_btnModHost);
	DDX_Control(pDX, IDC_BUTTON_HOST_REF, m_btnRefHost);
	//	DDX_Control(pDX, IDC_BUTTON_HOST_OPT, m_btnOptHost);
	DDX_Control(pDX, IDC_BUTTON_HOST_UPDATE, m_btnUpdHost);
	DDX_Control(pDX, IDC_BUTTON_HOST_SHUTDOWN, m_btnShutdown);
	DDX_Control(pDX, IDC_BUTTON_HOST_REBOOT, m_btnReboot);
	DDX_Control(pDX, IDC_BUTTON_HOST_RESTART, m_btnRestart);
	DDX_Control(pDX, IDC_BUTTON_HOST_CNGSITE, m_btnChnSite);
	DDX_Control(pDX, IDC_BUTTON_HOST_HDDTHRESHOLD, m_btnHddThreshold);
	DDX_Control(pDX, IDC_BN_TO_EXCEL2, m_btnExcelSave);
	DDX_Control(pDX, IDC_BN_RETRANSMIT, m_btnRetransmit);
	DDX_Control(pDX, IDC_BN_POWER_MNG, m_btnPowerMng);
	DDX_Control(pDX, IDC_BUTTON_HOST_PACKAGE_CHNG, m_btnPackageChng);
	DDX_Control(pDX, IDC_BUTTON_HOST_MONITOR_POWER, m_btnMonitorPower);
	DDX_Control(pDX, IDC_BN_ADDTAG, m_btAddTags);
	DDX_Control(pDX, IDC_BN_DELTAG, m_btDelTags);

	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_COMBO_FILTER_OPR, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOSTTYPE, m_cbHostType);
	DDX_Control(pDX, IDC_COMBO_FILTER_TAG, m_cbFilterTag);
	DDX_Control(pDX, IDC_KB_ADVANCED_FILTER, m_kbAdvancedFilter);
	DDX_Control(pDX, IDC_COMBO_ADVANCED_FILTER, m_cbAdvancedFilter);
	DDX_Control(pDX, IDC_LIST_TIMELINE, m_lscTimeLine);
	DDX_Control(pDX, IDC_BUTTON_SIZE_TOGGLE, m_btnSizeToggle);
	DDX_Control(pDX, IDC_COMBO_FILTER_CACHE2, m_cbFilterCache);
	DDX_Control(pDX, IDC_BUTTON_HOST_START, m_btStart);
	DDX_Control(pDX, IDC_BUTTON_PROPERTY, m_btProperty);
	DDX_Control(pDX, IDC_BUTTON_CONFIG, m_btConfig);
	DDX_Control(pDX, IDC_BUTTON_CONVERTER_REBOOT, m_btnRebootConverter);
	DDX_Control(pDX, IDC_BUTTON_CONVERTER_UPDATE, m_btnUpdateConverter);
	DDX_Control(pDX, IDC_BUTTON_MEMO, m_btnMemo);
}

BEGIN_MESSAGE_MAP(CHostView, CFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_RBUTTONDOWN()

	ON_COMMAND(ID_HOSTLIST_PACKAGEAPPLY, OnPackageApply)
//	ON_COMMAND(ID_HOSTLIST_EDITPACKAGE, OnEditpackage)
	ON_COMMAND(ID_HOSTLIST_AUTOPACKAGE1, OnAutoPackage1)
	ON_COMMAND(ID_HOSTLIST_AUTOPACKAGE2, OnAutoPackage2)
	ON_COMMAND(ID_HOSTLIST_CURPACKAGE1, OnCurPackage1)
	ON_COMMAND(ID_HOSTLIST_CURPACKAGE2, OnCurPackage2)
	ON_COMMAND(ID_HOSTLIST_LASTPACKAGE1, OnLastPackage1)
	ON_COMMAND(ID_HOSTLIST_LASTPACKAGE2, OnLastPackage2)
	
	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN, OnRemotelogin)
	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN2, OnRemotelogin2) // 0001442: �������� ����� ������ ��������E�ʴ� vnc���������� �����.
	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN3, OnRemotelogin3) // 0001442: �������� ����� ������ ������ �����ϴ� vnc ���� ������ �����.
	ON_COMMAND(ID_HOSTLIST_REMOTE_DESKTOP, OnRemoteDesktop)
	ON_COMMAND(ID_HOSTLIST_OPENSCREENSHOTSLIDE, OnOpenscreenshotslide)
	ON_COMMAND(ID_HOSTLIST_USE, OnUse)
	ON_COMMAND(ID_HOSTLIST_SHOWRESERVATIONINFO, OnShowreservationinfo)
	ON_COMMAND(ID_HOSTLIST_PROPERTIES, OnProperties)

	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, OnNMDblclkListHost)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_HOST, OnNMRclickListHost)

	ON_BN_CLICKED(IDC_BUTTON_HOST_ADD, OnBnClickedButtonHostadd)
	ON_BN_CLICKED(IDC_BUTTON_HOST_DEL, OnBnClickedButtonHostdel)
	ON_BN_CLICKED(IDC_BUTTON_HOST_MOD, OnBnClickedButtonHostmod)
	ON_BN_CLICKED(IDC_BUTTON_HOST_REF, OnBnClickedButtonHostrfs)
	ON_BN_CLICKED(IDC_BUTTON_HOST_OPT, OnBnClickedButtonHostopt)
	ON_BN_CLICKED(IDC_BUTTON_HOST_UPDATE, OnBnClickedButtonHostupd)
	ON_BN_CLICKED(IDC_BUTTON_HOST_SHUTDOWN, OnBnClickedButtonHostShutdown)
	ON_BN_CLICKED(IDC_BUTTON_HOST_REBOOT, OnBnClickedButtonHostReboot)
	ON_BN_CLICKED(IDC_BUTTON_HOST_RESTART, OnBnClickedButtonHostRestart)
	ON_BN_CLICKED(IDC_BUTTON_HOST_CNGSITE, OnBnClickButtonHostChgSite)
	ON_COMMAND_RANGE(ID_HOSTLIST_CANCELPACKAGE, ID_HOSTLIST_CANCELPACKAGE_ALL, OnHostlistCancelPackage)
	ON_BN_CLICKED(IDC_BUTTON_HOST_HDDTHRESHOLD, &CHostView::OnBnClickedButtonHostHddthreshold)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL2, &CHostView::OnBnClickedBnToExcel2)
	ON_COMMAND(ID_HOSTLIST_DOWNLOAD_RESULT_VIEW, &CHostView::OnDownloadResultView)
	ON_BN_CLICKED(IDC_BN_RETRANSMIT, &CHostView::OnBnClickedBnRetransmit)
	ON_BN_CLICKED(IDC_BN_POWER_MNG, &CHostView::OnBnClickedBnPowerMng)
	ON_BN_CLICKED(IDC_BUTTON_HOST_PACKAGE_CHNG, OnBnClickedButtonHostPackageChng)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SITE, OnBnClickedButtonFilterSite)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_CONTENTS, OnBnClickedButtonFilterContents)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, OnBnClickedButtonFilterPackage)
	ON_REGISTERED_MESSAGE(WM_FILTER_HOST_CHANGE, OnFilterHostChanged)
	ON_COMMAND(ID_HOSTLIST_PKG_CHNG_LOG_VIEW, &CHostView::OnHostlistPkgChngLogView)
	ON_BN_CLICKED(IDC_BUTTON_HOST_MONITOR_POWER, &CHostView::OnBnClickedButtonHostMonitorPower)
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
	ON_BN_CLICKED(IDC_KB_ADVANCED_FILTER, &CHostView::OnBnClickedKbAdvancedFilter)
	ON_BN_CLICKED(IDC_BN_DELTAG, &CHostView::OnBnClickedBnDeltag)
	ON_BN_CLICKED(IDC_BN_ADDTAG, &CHostView::OnBnClickedBnAddtag)
	ON_BN_CLICKED(IDC_BUTTON_SIZE_TOGGLE, &CHostView::OnBnClickedButtonResizeToggle)

	ON_NOTIFY(NM_CLICK, IDC_LIST_HOST, &CHostView::OnNMClickListHost)
	ON_BN_CLICKED(IDC_BUTTON_HOST_START, &CHostView::OnBnClickedButtonHostStart)
	ON_BN_CLICKED(IDC_BUTTON_PROPERTY, &CHostView::OnBnClickedButtonHostProperty)
	ON_COMMAND(ID_REMOTE_LOGIN_SETTINGS, &CHostView::OnRemoteLoginSettings)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG, &CHostView::OnBnClickedButtonConfig)
	ON_BN_CLICKED(IDC_BUTTON_CONVERTER_REBOOT, &CHostView::OnBnClickedButtonConverterReboot)
	ON_BN_CLICKED(IDC_BUTTON_CONVERTER_UPDATE, &CHostView::OnBnClickedButtonConverterUpdate)
	ON_COMMAND(ID_HOSTLIST1_NETWORKTEST, &CHostView::OnHostlist1Networktest)
	ON_BN_CLICKED(IDC_BUTTON_MEMO, &CHostView::OnBnClickedButtonMemo)
END_MESSAGE_MAP()

// CHostView diagnostics
#ifdef _DEBUG
void CHostView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CHostView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CHostView message handlers
int CHostView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);
//	GetEnvPtr()->InitSite();

	return 0;
}

void CHostView::OnInitialUpdate()
{
	TraceLog(("OnInitialUpdate()"));
	CFormView::OnInitialUpdate();

	m_showNextSchedule = GetEnvPtr()->GetColumnNextSchedule();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_HOSTVIEW_STR001));

	CodeItemList listHostType;
	CUbcCode::GetInstance()->GetCategoryInfo(_T("HostType"), listHostType);

	POSITION pos = listHostType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listHostType.GetNext(pos);
		m_cbHostType.AddString(info.strEnumString, info.nEnumNumber);
	}

	CRect rc;
	m_cbHostType.GetWindowRect(&rc);
	m_cbHostType.SetDroppedWidth(rc.Width() * 1.5);

	m_cbAdmin.AddString("All");
	m_cbAdmin.AddString(STR_ADMIN_ON);
	m_cbAdmin.AddString(STR_ADMIN_OFF);
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(STR_OPERA_ON);
	m_cbOperation.AddString(STR_OPERA_OFF);
	m_cbOperation.SetCurSel(0);

	CStringArray aTagList;
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

//	m_btnAddHost.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btnDelHost.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btnModHost.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_btnRefHost.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnUpdHost.LoadBitmap(IDB_BUTTON_UPDATE, RGB(255,255,255));
	m_btProperty.LoadBitmap(IDB_BUTTON_PROPERTY, RGB(255,255,255));
	m_btConfig.LoadBitmap(IDB_BUTTON_CONFIG, RGB(255,255,255));
	m_btnShutdown.LoadBitmap(IDB_BUTTON_SHUTDOWN, RGB(255,255,255));
	m_btStart.LoadBitmap(IDB_BITMAP_POWERON, RGB(255,255,255));
	m_btnReboot.LoadBitmap(IDB_BUTTON_REBOOT, RGB(255,255,255));
	m_btnRestart.LoadBitmap(IDB_BUTTON_RSTBRW, RGB(255,255,255));
	m_btnChnSite.LoadBitmap(IDB_BUTTON_CNGSITE, RGB(255,255,255));
	m_btnHddThreshold.LoadBitmap(IDB_BUTTON_HDDTHRESHOLD, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));
	m_btnRetransmit.LoadBitmap(IDB_BUTTON_RETRANSMIT, RGB(255,255,255));
	m_btnPowerMng.LoadBitmap(IDB_BUTTON_POWER_MNG, RGB(255,255,255));
	m_btnPackageChng.LoadBitmap(IDB_BUTTON_HOST_PACKAGE_CHNG, RGB(255,255,255));
	m_btnMonitorPower.LoadBitmap(IDB_BUTTON_MONITOR_POWER, RGB(255,255,255));
	m_btAddTags.LoadBitmap(IDB_BUTTON_ADDTAGS, RGB(255,255,255));
	m_btDelTags.LoadBitmap(IDB_BUTTON_DELTAGS, RGB(255,255,255));

	m_btnRebootConverter.LoadBitmap(IDB_BUTTON_REBOOT_CONVERTER, RGB(255,255,255));
	m_btnUpdateConverter.LoadBitmap(IDB_BUTTON_AUTO_UPDATE_CONVERTER, RGB(255,255,255));
	m_btnMemo.LoadBitmap(IDB_BUTTON_MEMO, RGB(255,255,255));

//	m_btnAddHost.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN001));
	m_btnDelHost.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN002));
	m_btnModHost.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN003));
	m_btnRefHost.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN004));
//	m_btnOptHost.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN005));
	m_btnUpdHost.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN006));
	m_btProperty.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN021));
	m_btnShutdown.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN007));
	m_btStart.SetToolTipText(LoadStringById(IDS_PLANOVIEW_BTN001));
	m_btnReboot.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN008));
	m_btnRestart.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN009));
	m_btnChnSite.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN010));
	m_btnHddThreshold.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN011));
	m_btnExcelSave.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN012));
	m_btnRetransmit.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN013));
	m_btnPowerMng.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN014));
	m_btnPackageChng.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN015));
	m_btnMonitorPower.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN018));
	m_btAddTags.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN019));
	m_btDelTags.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN020));

	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR005));
	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR009));
	m_cbFilterCache.AddString(LoadStringById(IDS_PLANSELECTHOST_STR010));
	m_cbFilterCache.SetCurSel(0);


	// ���ѿ� ����EȰ��ȭ �Ѵ�.
	InitAuthCtrl();

	LoadFilterData();

	UpdateData(FALSE);

	InitPosition(m_rcClient);

//	InitSiteList();
	InitHostList();
	InitTimeLineList();

//	GetEnvPtr()->InitHost(m_lsHost, NULL, NULL);
	//m_lscHost.SetSortCol(m_lscHost.GetColPos(m_szHostColum[eHostID]));

//	RefreshHostList();
//	RefreshHost(false);

	SetScrollSizes(MM_TEXT, CSize(1,1));

	// �̺�Ʈ������ �����ɸ��� ������ �߻��Ǿ�E������� ����.
//	AfxBeginThread(AddEventThread, this);

	// 0000725: ���ȭ�鿡��, �ʵ���� �� �����÷� �����ϱ�E
	// ����� �÷����·� �ʱ�ȭ
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscHost.LoadColumnState("HOST-LIST", szPath, eHostID);

	TraceLog(("OnInitialUpdate() end"));

}

//UINT CHostView::AddEventThread(LPVOID pParam)
//{
//	CHostView* pView = (CHostView*)pParam;
//	if(!pView) return 0;
//	HWND hWnd = pView->GetSafeHwnd();
//
//	TraceLog(("CHostView::AddEventThread - Start"));
//
//	// 2010.05.13 �̺�Ʈ������ ���嶁E�߻����� OnCreate���� �������� �ű�E
//	CString szBuf;
//	szBuf.Format("PM=*/Site=%s/Host=* ", (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? "*" : GetEnvPtr()->m_szSite));
//
//	if(::IsWindow(hWnd)) pView->m_lsEvID.AddTail(pView->m_EventManager.AddEvent(szBuf + _T("downloadStateChanged")));
//	if(::IsWindow(hWnd)) pView->m_lsEvID.AddTail(pView->m_EventManager.AddEvent(szBuf + _T("operationalStateChanged")));
//	if(::IsWindow(hWnd)) pView->m_lsEvID.AddTail(pView->m_EventManager.AddEvent(szBuf + _T("adminStateChanged")));
//	if(::IsWindow(hWnd)) pView->m_lsEvID.AddTail(pView->m_EventManager.AddEvent(szBuf + _T("vncStateChanged")));
//	if(::IsWindow(hWnd)) pView->m_lsEvID.AddTail(pView->m_EventManager.AddEvent(_T("* hostScheduleUpdated")));
//	if(::IsWindow(hWnd)) pView->m_lsEvID.AddTail(pView->m_EventManager.AddEvent(szBuf + _T("monitorStateChanged"))); // 0001371: [RFP] ������ ������� ON/OFF ��Ȳ�� �����ִ� ��� (�Ŵ���E��Ʈ)
//
//	TraceLog(("CHostView::AddEventThread - End"));
//
//	return 0;
//}

// ���ѿ� ����EȰ��ȭ �Ѵ�.
void CHostView::InitAuthCtrl()
{
//HQRY	�ܸ�-�����ȸ
//HDEL	�ܸ�-����
//HMOD	�ܸ�-����
//HREG	�ܸ�-����
//HOFF	�ܸ�-����E
//HRBT	�ܸ�-�ٽý���
//HBRS	�ܸ�-��E�E��簡�?
//HGMD	�ܸ�-�׷�E?�E
//HCLR	�ܸ�-�ܸ�û��
//HEXL	�ܸ�-��� ExcelExport
//HCRS	�ܸ�-����������E?
//HVNC	�ܸ�-��������
//HDNV	��?E�ᰁE����E
//HUSE	�ܸ����-�̻翁E
//HDTL	�ܸ����-�Ӽ�
//HSCR	�ܸ���ũ������ȸ
//HPNO	�ܸ�-��Ű������
//PQRY	��Ű��E�����ȸ
//PREG	��Ű��E�߰�
//PDEL	��Ű��E����
//PMOD	��Ű��E����
//PCHG	��Ű��E��Eú?�E
//PRSV	��Ű��E����E
//PSAV	��Ű��E����E
//PSND	��Ű��E��E?
//CQRY	��������E?��?
//EQRY	��ް���E�����ȸ
//EREG	��ް���E�߰�
//EDEL	��ް���E����
//EMOD	��ް���E����
//BQRY	��۰�ȹ-�����ȸ
//BREG	��۰�ȹ-�߰�
//BDEL	��۰�ȹ-����
//BMOD	��۰�ȹ-����E
//MPQR	���μ�������-��ȸ
//LQRY	�α�-��ȸ
//FQRY	���-�����ȸ
//FDEL	���-����
//SQRY	ŁE�E��ȸ
//SBRD	�������װ�E?
//SGRP	������E?
//SUSR	�翁Eڰ�E?
//SCOD	�ڵ封E?

	m_btnDelHost.EnableWindow(IsAuth(_T("HDEL")));
	m_btnModHost.EnableWindow(IsAuth(_T("HMOD")));
	m_btnRefHost.EnableWindow(IsAuth(_T("HQRY")));
	m_btnUpdHost.EnableWindow(IsAuth(_T("HREG")));
	m_btProperty.EnableWindow(IsAuth(_T("HREG"))); 
	m_btnShutdown.EnableWindow(IsAuth(_T("HOFF")));
	m_btStart.EnableWindow(IsAuth(_T("HONN")));
	m_btnReboot.EnableWindow(IsAuth(_T("HRBT")));
	m_btnRestart.EnableWindow(IsAuth(_T("HBRS")));
	m_btnChnSite.EnableWindow(IsAuth(_T("HGMD")));
	m_btnHddThreshold.EnableWindow(IsAuth(_T("HCLR")));
	m_btnExcelSave.EnableWindow(IsAuth(_T("HEXL")));
	m_btnRetransmit.EnableWindow(IsAuth(_T("HCRS")));
	m_btnPowerMng.EnableWindow(IsAuth(_T("HMOD")));
	m_btnPackageChng.EnableWindow(IsAuth(_T("HVNC")));
	m_btnMonitorPower.EnableWindow(IsAuth(_T("HMPW")));
	m_btAddTags.EnableWindow(IsAuth(_T("HMOD")));
	m_btDelTags.EnableWindow(IsAuth(_T("HMOD")));
	if(CEnviroment::eKPOST == CEnviroment::GetObject()->m_Customer) {  // KPOST ONLY
		m_btConfig.EnableWindow(IsAuth(_T("HREG"))); 	
		m_btnUpdateConverter.EnableWindow(IsAuth(_T("HREG"))); 	
		m_btnMemo.EnableWindow(IsAuth(_T("HREG"))); 	
		m_btnRebootConverter.EnableWindow(IsAuth(_T("HREG"))); 	
	}else{
		m_btConfig.ShowWindow(SW_HIDE); 	
		m_btnUpdateConverter.ShowWindow(SW_HIDE); 	
		m_btnMemo.ShowWindow(SW_HIDE); 	
		m_btnRebootConverter.ShowWindow(SW_HIDE); 	
	}
}

void CHostView::OnClose()
{
	TraceLog(("CHostView::OnClose"));
	CFormView::OnClose();
}

void CHostView::OnDestroy()
{
	TraceLog(("CHostView::OnDestroy"));

	//// 0001414: �ܸ���ϵ�E�����κ��� �̺�Ʈ�� �޴� ȭ���� ������ ����������� ��E��?����.
	//// ���̻�E�̺�Ʈ�� ���ƿ͵� ó������E�ʵ��� �Ѵ�
	//if(!CEventHandler::CloseWaitAllEvent(GetSafeHwnd())) return;

	//m_EventManager.SetInterfacePtr(NULL);

	//POSITION pos = m_lsEvID.GetHeadPosition();
	//while(pos)
	//{
	//	int evId = m_lsEvID.GetNext(pos);
	//	m_EventManager.RemoveEvent(evId);
	//}
	//m_lsEvID.RemoveAll();

	if(m_pMainFrame)
	{
		m_pMainFrame->SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_HOST, (LPARAM)(LPCTSTR)_T(""));
	}

	// 0000725: ���ȭ�鿡��, �ʵ���� �� �����÷� �����ϱ�E
	// ����Ʈ�� �÷����� ����E
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscHost.SaveColumnState("HOST-LIST", szPath);

	CFormView::OnDestroy();
}

void CHostView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscHost, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_Filter, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_Reposition.AddControl(&m_btnSizeToggle, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_lscTimeLine, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}

void CHostView::OnSize(UINT nType, int cx, int cy)
{
	TraceLog(("OnSize(%d,%d,%d)", cx,cy,m_toggle));
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
	ToggleWindow(m_toggle); 
	InvalidateItems();
}

void CHostView::InitHostList()
{
	m_szHostColum[eCheck] = _T("");	// 2010.02.17 by gwangsoo
	m_szHostColum[eSiteName] = STR_HOST_SITE_NAME;
	m_szHostColum[eHostID] = STR_HOST_ID;
	m_szHostColum[eHostName] = STR_HOST_NAME;
	//m_szHostColum[eHostVendor] = STR_HOST_VENDOR;
	m_szHostColum[eMonitorType] = STR_HOST_MONITORTYPE;
	//m_szHostColum[eHostModel] = STR_HOST_MODEL;
	m_szHostColum[eHostOs] = STR_HOST_OS;
	//m_szHostColum[eHostSerial] = STR_HOST_SERIAL;
	m_szHostColum[eVersion] = STR_HOST_VERSION;
	//m_szHostColum[eHostPeriod] = STR_HOST_PERIOD;
	m_szHostColum[eHostAddr1] = STR_HOST_ADDR1;
	m_szHostColum[eHostDesc] = STR_HOST_DESC;
	m_szHostColum[eHostIP] = STR_HOST_IP;
//	m_szHostColum[eHostAdmin] = STR_HOST_ADMIN;
//	m_szHostColum[eHostOper] = STR_HOST_OPERA;
	m_szHostColum[eHostMacAddr] = STR_HOST_MAC;
	//m_szHostColum[eHostEdition] = STR_HOST_EDIT;
	m_szHostColum[eHostBootUpTime] = STR_HOST_BOOTUPTIME;
	m_szHostColum[eHostDisCnt] = STR_HOST_DISCNT;
	m_szHostColum[eHostAuto1] = STR_HOST_AUTO1;
	m_szHostColum[eHostAuto2] = STR_HOST_AUTO2;
	m_szHostColum[eHostCur1] = STR_HOST_CUR1;
	m_szHostColum[eHostCur2] = STR_HOST_CUR2;
	m_szHostColum[eHostLast1] = STR_HOST_LAST1;
	m_szHostColum[eHostLast2] = STR_HOST_LAST2;
	m_szHostColum[eHostNext1] = STR_HOST_NEXT1;
	m_szHostColum[eHostNext2] = STR_HOST_NEXT2;
//	m_szHostColum[eHostNet1] = STR_HOST_NET1;
//	m_szHostColum[eHostNet2] = STR_HOST_NET2;
	m_szHostColum[eDomainName] = STR_HOST_DOMAIN;
	m_szHostColum[eHostUpDate] = STR_HOST_UDATE;
	m_szHostColum[eHostVNCPort] = STR_HOST_VNCPORT;
	m_szHostColum[eHostVNCStat] = STR_HOST_VNCSTAT;
	m_szHostColum[eDownloadTime] = STR_HOST_DOWNLOADTIME;
	m_szHostColum[eTag] = STR_HOST_TAG;
	m_szHostColum[eMonitorStat] = STR_HOST_MONITOR_STAT;
	m_szHostColum[eHostType] = STR_HOST_TYPE;
	m_szHostColum[eGmt] = LoadStringById(IDS_HOSTVIEW_LIST046);
	m_szHostColum[eMonitorUseTime] = LoadStringById(IDS_HOSTVIEW_LIST047); // IDS_HOSTVIEW_LIST047
	m_szHostColum[eHostMsg] = LoadStringById(IDS_HOSTVIEW_LIST048);
	m_szHostColum[eDisk1Avail] = LoadStringById(IDS_HOSTVIEW_LIST049);
	m_szHostColum[eDisk2Avail] = LoadStringById(IDS_HOSTVIEW_LIST050);
	m_szHostColum[eShutdownTime] = LoadStringById(IDS_HOSTVIEW_LIST057);
	m_szHostColum[eStartupTime] = LoadStringById(IDS_HOSTVIEW_LIST058);
	m_szHostColum[eSosId] = _T("Property");

	//m_lscHost.SetExtendedStyle(m_lscHost.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);
	m_lscHost.SetExtendedStyle(m_lscHost.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_INFOTIP| m_lscHost.GetExtendedStyle());

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilHostList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilHostList.Add(&bmCheck, RGB(255, 255, 255));
	m_lscHost.SetImageList(&m_ilHostList, LVSIL_SMALL);

	for(int nCol = 0; nCol < eHostEnd; nCol++)
	{
		if(nCol == eDisk1Avail || nCol == eDisk2Avail)
		{
			m_lscHost.InsertColumn(nCol, m_szHostColum[nCol], LVCFMT_RIGHT, 100);
		}
		else
		{
			m_lscHost.InsertColumn(nCol, m_szHostColum[nCol], LVCFMT_LEFT, 100);
		}
	}

	m_lscHost.SetColumnWidth(eCheck, 22); // 2010.02.17 by gwangsoo
	m_lscHost.SetColumnWidth(eHostDisCnt, 20);
	m_lscHost.SetColumnWidth(eHostVNCStat, 15);
	m_lscHost.SetColumnWidth(eHostAuto1, 0);
	m_lscHost.SetColumnWidth(eHostAuto2, 0);
	m_lscHost.SetColumnWidth(eHostCur1, 200);
	m_lscHost.SetColumnWidth(eHostCur2, 0);
	m_lscHost.SetColumnWidth(eHostLast1, 200);
	m_lscHost.SetColumnWidth(eHostLast2, 0);
	
	if(m_showNextSchedule){
		m_lscHost.SetColumnWidth(eHostNext1, 280);
		m_lscHost.SetColumnWidth(eHostNext2, 0);
	}else{
		m_lscHost.SetColumnWidth(eHostNext1, 0);
		m_lscHost.SetColumnWidth(eHostNext2, 0);
	}
	m_lscHost.SetColumnWidth(eMonitorStat, 15);
	m_lscHost.SetColumnWidth(eHostMsg, 15);

//	m_lscHost.SetColumnWidth(m_lscHost.GetColPos(m_szHostColum[eHostAdmin]), 20);
//	m_lscHost.SetColumnWidth(m_lscHost.GetColPos(m_szHostColum[eHostOper]), 20);

	m_lscHost.SetProgressBarCol(eHostLast1, COLOR_DOWNLOAD, COLOR_GRAY, COLOR_BLACK);
	m_lscHost.SetProgressBarCol(eHostLast2, COLOR_DOWNLOAD, COLOR_GRAY, COLOR_BLACK);

	//m_lscHost.InitHeader(IDB_LIST_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
	m_lscHost.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CHostView::InitTimeLineList()
{
	m_btnSizeToggle.LoadBitmap(IDB_BTN_SIZETOGGLE, RGB(255, 255, 255));//skpark
	m_lscTimeLine.SetExtendedStyle(m_lscTimeLine.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);
	m_lscTimeLine.InsertColumn(1, LoadStringById(IDS_HOSTVIEW_PLANLIST), LVCFMT_LEFT, TIME_OPEN_WIDTH);
	m_lscTimeLine.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

bool CHostView::RefreshTimeLine(SHostInfo& hostInfo)
{
	TraceLog(("RefreshTimeLine begin(%s)", hostInfo.hostId));

	//CWaitMessageBox wait;

	CList<CString> timeLineList;
	int side = 1;
	int period = 10;
	bool bRet = CCopModule::GetObject()->GetTimeLine(hostInfo,timeLineList,period);
	if(!bRet){
		UbcMessageBox("Get TimeLine failed");
		return false;
	}

	m_lscTimeLine.SetRedraw(FALSE);

	m_lscTimeLine.DeleteAllItems();
	int nRow = 0;
	POSITION pos = timeLineList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		CString timeInfo = timeLineList.GetNext(pos);

		m_lscTimeLine.InsertItem(nRow, _T(""), -1);
		m_lscTimeLine.SetItemText(nRow, 0, timeInfo);
		m_lscTimeLine.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	m_lscTimeLine.SetRedraw(TRUE);

	TraceLog(("RefreshTimeLine end"));
	return true;
}
void CHostView::RefreshHostList(POSITION posStart)
{
	TraceLog(("RefreshHostList begin"));

	m_lscHost.SetRedraw(FALSE);

	int nRow = 0;
	LVITEM item;
	CString szBuf;
	POSITION pos = m_lsHost.GetHeadPosition();
	POSITION posTail = m_lsHost.GetTailPosition();

	if(posStart)
	{
		pos = posStart;
		m_lsHost.GetNext(pos);
	}
	else
	{
		m_lscHost.DeleteAllItems();
	}

	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsHost.GetNext(pos);

		m_lscHost.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, &info);  //OK

		m_lscHost.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lscHost.GetCurSortCol();
	if(-1 != nCol)
		m_lscHost.SortList(nCol, m_lscHost.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscHost);
	else
		m_lscHost.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscHost);

	TraceLog(("RefreshHostList end"));

	m_lscHost.SetRedraw(TRUE);

	RefreshPaneText();
}

void CHostView::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifyHost(arRow, true);
}

// �ܸ� ����Ʈ���� ������ ��ư Ŭ����
void CHostView::OnNMRclickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
	if(!pos) return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	CMapStringToPtr mspPackage;
	if(!Info.autoPackage1.IsEmpty()){
		mspPackage.SetAt(Info.autoPackage1, (void*)ID_HOSTLIST_AUTOPACKAGE1);
	}
	if(!Info.autoPackage2.IsEmpty()){
		mspPackage.SetAt(Info.autoPackage2, (void*)ID_HOSTLIST_AUTOPACKAGE2);
	}
	if(!Info.currentPackage1.IsEmpty()){
		mspPackage.SetAt(Info.currentPackage1, (void*)ID_HOSTLIST_CURPACKAGE1);
	}
	if(!Info.currentPackage2.IsEmpty()){
		mspPackage.SetAt(Info.currentPackage2, (void*)ID_HOSTLIST_CURPACKAGE2);
	}
	if(!Info.lastPackage1.IsEmpty()){
		mspPackage.SetAt(Info.lastPackage1, (void*)ID_HOSTLIST_LASTPACKAGE1);
	}
	if(!Info.lastPackage2.IsEmpty()){
		mspPackage.SetAt(Info.lastPackage2, (void*)ID_HOSTLIST_LASTPACKAGE2);
	}
	
	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);
//	ClientToScreen(&pt);

	pPopup = menu.GetSubMenu(0);

//  ��Ʃ��������� �ܸ��� ��?E��?�ȴѰ濁E��?����ÁE��Ű���� ��E��ϹǷ?�Ŵ��������� �����ϰ� ó���ϵ�����
//	if(!Info.operationalState)
//	{
//		pPopup->DeleteMenu(ID_HOSTLIST_PACKAGEAPPLY, MF_BYCOMMAND);
//	}

	if(IsAuth(_T("PMOD")))
	{
		CMenu menuPackage;
		if(mspPackage.GetCount() > 0 && menuPackage.CreatePopupMenu())
		{
			POSITION pos = mspPackage.GetStartPosition();

			while(pos)
			{
				CString szKey;
				int nMsgID;

				mspPackage.GetNextAssoc(pos, szKey, (void*&)nMsgID);
				menuPackage.AppendMenu(MF_BYCOMMAND|MF_STRING|(IsAuth(_T("PMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED), nMsgID, szKey);
			}
			pPopup->InsertMenu(ID_HOSTLIST_EDITPACKAGE, MF_BYCOMMAND|MF_STRING|MF_POPUP, (UINT)menuPackage.m_hMenu, LoadStringById(IDS_HOSTVIEW_STR002));
		}
	}
	pPopup->DeleteMenu(ID_HOSTLIST_EDITPACKAGE, MF_BYCOMMAND);

	// 2010.02.17 by gwangsoo
	if(!IsAuth(_T("HPNO")))
	{
		pPopup->DeleteMenu(ID_HOSTLIST_CANCELPACKAGE, MF_BYCOMMAND);
	}
	else if(Info.displayCounter > 1)
	{
		CMenu menuSelectDisplay;
		menuSelectDisplay.CreatePopupMenu();
		// �ո�E(Display A) / �޸�E(Display B) / ���
		CString strMenuName;
		strMenuName.Format("%s (Display 1)", LoadStringById(IDS_HOSTVIEW_STR007));
		menuSelectDisplay.AppendMenu(MF_BYCOMMAND|MF_STRING, ID_HOSTLIST_CANCELPACKAGE_A, strMenuName);

		strMenuName.Format("%s (Display 2)", LoadStringById(IDS_HOSTVIEW_STR008));
		menuSelectDisplay.AppendMenu(MF_BYCOMMAND|MF_STRING, ID_HOSTLIST_CANCELPACKAGE_B, strMenuName);

		menuSelectDisplay.AppendMenu(MF_BYCOMMAND|MF_STRING, ID_HOSTLIST_CANCELPACKAGE_ALL, LoadStringById(IDS_HOSTVIEW_STR009));

		pPopup->ModifyMenu(ID_HOSTLIST_CANCELPACKAGE, MF_BYCOMMAND|MF_STRING|MF_POPUP, (UINT)menuSelectDisplay.m_hMenu, LoadStringById(IDS_HOSTVIEW_STR006));
	}

	if(Info.adminState)
		pPopup->ModifyMenu(ID_HOSTLIST_USE, MF_BYCOMMAND, ID_HOSTLIST_USE, LoadStringById(IDS_HOSTVIEW_STR003));
	else
		pPopup->ModifyMenu(ID_HOSTLIST_USE, MF_BYCOMMAND, ID_HOSTLIST_USE, LoadStringById(IDS_HOSTVIEW_STR004));

//	if(!Info.vncState)
//		pPopup->DeleteMenu(ID_HOSTLIST_REMOTELOGIN, MF_BYCOMMAND);

//HQRY	�ܸ�-�����ȸ
//HDEL	�ܸ�-����
//HMOD	�ܸ�-����
//HREG	�ܸ�-����
//HOFF	�ܸ�-����E
//HRBT	�ܸ�-�ٽý���
//HBRS	�ܸ�-��E�E��簡�?
//HGMD	�ܸ�-�׷�E?�E
//HCLR	�ܸ�-�ܸ�û��
//HEXL	�ܸ�-��� ExcelExport
//HCRS	�ܸ�-����������E?
//HVNC	�ܸ�-��������
//HDNV	��?E�ᰁE����E
//HUSE	�ܸ����-�̻翁E
//HDTL	�ܸ����-�Ӽ�
//HSCR	�ܸ���ũ������ȸ
//HPNO	�ܸ�-��Ű������
//PQRY	��Ű��E�����ȸ
//PREG	��Ű��E�߰�
//PDEL	��Ű��E����
//PMOD	��Ű��E����
//PCHG	��Ű��E��Eú?�E
//PRSV	��Ű��E����E
//PSAV	��Ű��E����E
//PSND	��Ű��E��E?
//CQRY	��������E?��?
//EQRY	��ް���E�����ȸ
//EREG	��ް���E�߰�
//EDEL	��ް���E����
//EMOD	��ް���E����
//BQRY	��۰�ȹ-�����ȸ
//BREG	��۰�ȹ-�߰�
//BDEL	��۰�ȹ-����
//BMOD	��۰�ȹ-����E
//MPQR	���μ�������-��ȸ
//LQRY	�α�-��ȸ
//FQRY	���-�����ȸ
//FDEL	���-����
//SQRY	ŁE�E��ȸ
//SBRD	�������װ�E?
//SGRP	������E?
//SUSR	�翁Eڰ�E?
//SCOD	�ڵ封E?

	pPopup->EnableMenuItem(ID_HOSTLIST_PACKAGEAPPLY, (IsAuth(_T("PCHG"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
//	pPopup->EnableMenuItem(ID_HOSTLIST_EDITPACKAGE , (IsAuth(_T("PMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
//	pPopup->EnableMenuItem(ID_HOSTLIST_CANCELPACKAGE_A, (IsAuth(_T("HPNO"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
//	pPopup->EnableMenuItem(ID_HOSTLIST_CANCELPACKAGE_B, (IsAuth(_T("HPNO"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
//	pPopup->EnableMenuItem(ID_HOSTLIST_CANCELPACKAGE_ALL, (IsAuth(_T("HPNO"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_REMOTELOGIN, (IsAuth(_T("HVNC"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_REMOTE_DESKTOP, (IsAuth(_T("HVNC"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_OPENSCREENSHOTSLIDE, (IsAuth(_T("HSCR"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_DOWNLOAD_RESULT_VIEW, (IsAuth(_T("HDNV"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_PKG_CHNG_LOG_VIEW, (IsAuth(_T("HDNV"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_USE, (IsAuth(_T("HUSE"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_PROPERTIES, (IsAuth(_T("HDTL")) || IsAuth(_T("HMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));

	CString strOsName = Info.os;
	strOsName.Trim();
	strOsName.MakeLower();
	if(!strOsName.IsEmpty() && strOsName.Find(_T("win")) < 0)
	{
		pPopup->DeleteMenu(ID_HOSTLIST_REMOTE_DESKTOP, MF_BYCOMMAND);
	}

	//pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_NONOTIFY,pt.x, pt.y, this);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CHostView::OnBnClickedButtonHostadd()
{
}

// �ܸ�����
void CHostView::OnBnClickedButtonHostdel()
{
	bool bCheck = false;
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		if(m_lscHost.GetCheck(nRow))
		{
			bCheck = true;
			break;
		}
	}

	if(!bCheck)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}

	int targetCount = 0;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--) {
		if(!m_lscHost.GetCheck(nRow)) continue;
		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;
		targetCount++;
	}
	if(targetCount > 1) {
		UbcMessageBox(IDS_HOSTVIEW_MSG024);
		return;
	}


	if(UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG001), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG022), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG023), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}

	CWaitMessageBox wait;
	BOOL b_fail = false;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		// ���õǾ�E����E������E��ŵ
		if(!m_lscHost.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		// ������ �ܸ��������� ȣÁE
		if(Info.siteId.GetLength()==0 || Info.hostId.GetLength()==0) continue;
		if(!CCopModule::GetObject()->DelHost(Info.siteId, Info.hostId)) {
			b_fail = true;
			break;
		}

		// ��������
		m_lscHost.SetCheck(nRow, FALSE);

		m_lsHost.RemoveAt(pos);
		m_lscHost.DeleteItem(nRow);
	}
	if(!b_fail) {
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG003), MB_ICONINFORMATION);
	}
}

void CHostView::OnBnClickedButtonHostmod()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG004), MB_ICONINFORMATION);
		return;
	}
	// skpark 2012.12.20 ��Ƽ ������ ȣ��Ʈ �� ���� ������ ����� �޸��Ѵ�.
	//ModifyHost(arRow, true);
	CHostMultiModify dlg;
	if(dlg.DoModal() != IDOK)
	{
		return;
	}

	SHostInfo* aInfo = dlg.GetHostInfo();
	CDirtyFlag* aInfoDirty = dlg.GetHostInfoDirty();

	int nModified = 0;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscHost.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		aInfo->mgrId = Info.mgrId;
		aInfo->siteId = Info.siteId;
		aInfo->hostId = Info.hostId;

		if(!CCopModule::GetObject()->SetHost(*aInfo, GetEnvPtr()->m_szLoginID,aInfoDirty))
		{
			m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		if(m_showNextSchedule){
			if(!CCopModule::GetObject()->GetTimeLine(*aInfo))
			{
				TraceLog(("GetTimeLine(%s) failed", aInfo->hostId));;
			}
		}
		nModified++;
		m_lsHost.GetAt(pos) = Info;
	}
	if(nModified > 0)
	{
		char debugStr[256];
		sprintf(debugStr,"%d %s", nModified, LoadStringById(IDS_HOSTVIEW_MSG006));
		if(true) UbcMessageBox(debugStr, MB_ICONINFORMATION);
		RefreshHostList();
	}
}

void CHostView::ModifyHost(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscHost.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);
		CHostDetailDlg dlg;

		dlg.SetInfo(Info);
		if(dlg.DoModal() != IDOK)
		{
			dlg.GetInfo(Info);
			m_lsHost.GetAt(pos) = Info;
			UpdateListRow(nRow, &Info); //OK
			continue;
		}
		dlg.GetInfo(Info);

		if(!CCopModule::GetObject()->SetHost(Info, GetEnvPtr()->m_szLoginID))
		{
			m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
			UbcMessageBox(m_szMsg, MB_ICONERROR);
			continue;
		}
		if(m_showNextSchedule){
			if(!CCopModule::GetObject()->GetTimeLine(Info))
			{
				TraceLog(("GetTimeLine(%s) failed", Info.hostId));;
			}
		}
		bModified = true;
		m_lsHost.GetAt(pos) = Info;
	}

	if(bModified)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG006), MB_ICONINFORMATION);
		RefreshHostList();
	}
}

void CHostView::OnBnClickedButtonHostrfs()
{
	DeleteBackupHostList();
	RefreshHost();

	SaveFilterData();

	// ���� ���͸� �翁Eϴ?������E?ŁE�E
	((CMainFrame*)AfxGetMainWnd())->UpdateFilterData(this);
}




BOOL CHostView::RefreshHost(bool bCompMsg)
{
	TraceLog(("RefreshHost begin"));
	DWORD dwStartTime =  ::GetTickCount() ;

	CWaitMessageBox wait;
	
	m_showNextSchedule = GetEnvPtr()->GetColumnNextSchedule();

	CString szSite = (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite);

	UpdateData();

	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;
	CString strAdvancedFilter;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);
	if(m_kbAdvancedFilter.GetCheck()) m_cbAdvancedFilter.GetWindowText(strAdvancedFilter);

	// �������Ϳ� select,(,) ���ڻ翁E���E���ϵ�����.
	if(!strAdvancedFilter.IsEmpty())
	{
		CString strTmp = strAdvancedFilter;
		strTmp.MakeUpper();
		if(strTmp.Find("SELECT") >= 0 || strTmp.FindOneOf("()") >= 0)
		{
			UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG021));
			return FALSE;
		}
	}

	CString strWhere;

	// �ܸ��̸�
	strHostName.Trim();
	if(!strHostName.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostName like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostName
							);
	}

	// �ܸ�ID
	strHostId.Trim();
	if(!strHostId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostId like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostId
							);
	}

	// �ܸ�Ÿ��
	if(!strHostType.IsEmpty())
	{
		strHostType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s hostType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostType
							);
	}

	// �Ҽ�����E
	if(!strSiteId.IsEmpty())
	{
		strSiteId.Replace(_T(","), _T("','"));
		strWhere.AppendFormat(_T(" %s siteId in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strSiteId
							);
	}

	// ��������ÁE
	if(!strContentsId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 in (select programId from ubc_contents where lower(contentsId) = lower('%s'))"
								      " or lastSchedule2 in (select programId from ubc_contents where lower(contentsId) = lower('%s')))")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strContentsId
							, strContentsId
							);
	}

	// ������� ��Ű��E
	if(!strPackageId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (lastSchedule1 like '%%%s%%' or lastSchedule2 like '%%%s%%')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strPackageId
							, strPackageId
							);
	}

	// �������
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s operationalState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}

	// �翁E��?
	if(_ttoi(strAdminStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s adminState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAdminStat)==1 ? 1:0)
							);
	}

	// �˻���E
	strCategory.Trim();
	if(!strCategory.IsEmpty())
	{
		int pos = 0;
		CString strTag;
		while((strTag = strCategory.Tokenize("/", pos)) != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', category, '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s category like '%%%s%%'")
							 	 , (strWhere.IsEmpty() ? _T("") : _T("and"))
								 , strTag
								 );
		}
	}

	strAdvancedFilter.Trim();
	if(!strAdvancedFilter.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (%s) ")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strAdvancedFilter
							);
	}

	if(m_cbFilterCache.GetCurSel()==1) // localhost
	{
		strWhere.AppendFormat(_T(" %s domainName = 'localhost'")
							, (strWhere.IsEmpty() ? _T("") : _T("and")));
	}else if(m_cbFilterCache.GetCurSel()==2) {
		strWhere.AppendFormat(_T(" %s (domainName != 'localhost' or domainName is null)")
							, (strWhere.IsEmpty() ? _T("") : _T("and")));
	}


	//BOOL bRet = GetEnvPtr()->RefreshHost(m_lsHost, szSite, (strWhere.IsEmpty() ? NULL : strWhere));
	TraceLog(("m_lsHost.RemoveAll() begin"));
	m_lsHost.RemoveAll();
	TraceLog(("m_lsHost.RemoveAll() end"));

	if(m_pCall) delete m_pCall;
	m_pCall = new cciCall();
	BOOL bRet = CCopModule::GetObject()->GetHostForList(m_pCall, szSite, "*", (strWhere.IsEmpty() ? NULL : strWhere));
	if(bRet)
	{
		bRet = CCopModule::GetObject()->GetHostData(m_pCall, m_lsHost);
	}

	if(bRet)
	{
		if(m_showNextSchedule){
			bRet = CCopModule::GetObject()->GetTimeLine(m_lsHost);
		}
	}

	RefreshHostList();

	TraceLog(("RefreshHost end"));

	if(!bRet)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG007), MB_ICONINFORMATION);
		return FALSE;
	}

	if(bCompMsg)
	{
		DWORD dwElapse = ::GetTickCount() - dwStartTime;
		DWORD dwElapseSec = dwElapse / 1000;
		CString strElapse;
		strElapse.Format("\nElapse time : %ld sec", dwElapseSec);

		CString strMsg;
		strMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG008), m_lscHost.GetItemCount());
		strMsg.Append(strElapse);

		UbcMessageBox(strMsg, MB_ICONINFORMATION);
	}

	return bRet;
}

void CHostView::OnBnClickedButtonHostopt()
{
}

void CHostView::OnBnClickedButtonHostupd()
{
	//
	CString szSite, szKey, szPasswd, szHostId;

	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority){
		szSite = _T("*");
	}
	else if(CCopModule::eSiteManager == GetEnvPtr()->m_Authority){
		szSite = GetEnvPtr()->m_szSite;
	}
	else{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG017), MB_ICONINFORMATION);
		return;
	}

	// KPOST�� ���Ϸκ��� ���̼��� ������Ʈ ó��
	if((CEnviroment::eKPOST == CEnviroment::GetObject()->m_Customer)
	   || (CEnviroment::eHYUNDAIQ == CEnviroment::GetObject()->m_Customer)
	   || IsUseHostRegFromFile() )
	{
		HostupdEx();
		return;
	}

	// ��Ÿ �ܸ����
	CAuthCheckDlg dlg(GetEnvPtr()->m_strCustomer);
	if(dlg.DoModal() != IDOK)
		return;

	szKey = dlg.m_szKey;
	szPasswd = dlg.m_szPasswd;
	szHostId = dlg.m_szHostId;

	HCURSOR oldCursor = SetCursor(LoadCursor(NULL, IDC_WAIT)) ;
	int nRet = CCopModule::GetObject()->UpdateHost(szSite, szKey, szPasswd, szHostId, GetEnvPtr()->m_strCustomer);
	if(nRet < 0)
	{
//		UbcMessageBox("Failed to update site", MB_ICONERROR);
		return;
	}
	else if(nRet == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG009), MB_ICONINFORMATION);
		return;
	}

	SetCursor(oldCursor);

	BackupHostList();

	if(RefreshHost(false))
	{
		m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG010), nRet);
		UbcMessageBox(m_szMsg, MB_ICONINFORMATION);
	}
}

void CHostView::OnRButtonDown(UINT nFlags, CPoint point)
{
	CFormView::OnRButtonDown(nFlags, point);
}

void CHostView::OnBnClickedButtonHostShutdown()
{
	CList<int> lsRow;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG011), MB_ICONINFORMATION);
		return;
	}

	m_szMsg = LoadStringById(IDS_HOSTVIEW_MSG012);
	if(UbcMessageBox(m_szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow){
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost)	continue;

		m_lscHost.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);
		CCopModule::GetObject()->Shutdown(Info.siteId, Info.hostId);
	}
}

void CHostView::OnBnClickedButtonHostReboot()
{
	CList<int> lsRow;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	m_szMsg = LoadStringById(IDS_HOSTVIEW_MSG014);
	if(UbcMessageBox(m_szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow){
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost)	continue;

		m_lscHost.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);
		CCopModule::GetObject()->Reboot(Info.siteId, Info.hostId);
	}
}

void CHostView::OnBnClickedButtonHostRestart()
{
	CList<int> lsRow;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	m_szMsg = LoadStringById(IDS_HOSTVIEW_MSG016);
	if(UbcMessageBox(m_szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow){
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost)	continue;

		m_lscHost.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);
		CCopModule::GetObject()->Restart(Info.siteId, Info.hostId);
	}
}

void CHostView::OnBnClickButtonHostChgSite()
{
	CList<int> lsRow;
	CStringList lsHost;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost)	continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		CString szHost = _T("PM=*/Site=");
		szHost += Info.siteId;
		szHost += _T("/Host=");
		szHost += Info.hostId;

		lsHost.AddTail(szHost);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CChangeSiteDlg dlg;
	if(dlg.DoModal() != IDOK){
		return;
	}

	CWaitMessageBox wait;

	CCopModule::GetObject()->ChangeSite(dlg.m_szSelSiteId, lsHost, dlg.m_bReboot);

	RefreshHost(false);

	//POSITION posRow = lsRow.GetHeadPosition();
	//while(posRow){
	//	int nRow = lsRow.GetNext(posRow);

	//	POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
	//	if(!posHost)	continue;

	//	m_lscHost.SetCheck(nRow, FALSE);
	//}
}

LRESULT CHostView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	TraceLog(("CHostView::InvokeEvent begin"));

	HWND hWnd = GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));
	if(::IsWindow(hWnd) == FALSE) return 0;

	switch(wParam)
	{
	case IEH_OPSTAT:
// cli >>> post PM=2/Site=SQI/Host=SQI-06112 operationalStateChanged siteId="SQI", hostId="SQI-06116", operationalState=true
		return ChangeOPStat((SOpStat*)lParam);

	case IEH_ADSTAT:
		return ChangeAdStat((SAdStat*)lParam);

	case IEH_UPPACKAGE:
		return UpdatePackage((SUpPackage*)lParam);

	case IEH_CHNGVNC:
		return ChangeVncStat((cciEvent*)lParam);

		// 0000707: ����ÁE�ٿ��ε�E���� ��ȸ ���
	case IEH_CHNGDOWN:
		return ChangeDownloadStat((SDownloadStateChange*)lParam);

		// 0001371: [RFP] ������ ������� ON/OFF ��Ȳ�� �����ִ� ��� (�Ŵ���E��Ʈ)
	case IEH_MOSTAT:
		return ChangeMonitorStat((SMonitorStat*)lParam);

	case IEH_HDDCHNG:
		return ChangeDiskAvail((SDiskAvailChanged*)lParam);

	case IEH_UNKOWN:
		//cciAttributeList* pList = (cciAttributeList*)lParam;

		//for(int i = 0; i < (int)pList->length(); i++){
		//	cciAttribute& attr = (*pList)[i];

		//	if(strcmp(attr.getName(),"siteId") == 0){
		//		attr.getValue(strSite);
		//	}
		//}
		break;
	}
	return 0;
}

int CHostView::ChangeOPStat(SOpStat* pOpStat)
{
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pOpStat->siteId || Info.hostId != pOpStat->hostId)
			continue;

		TraceLog(("operationalState : %s", pOpStat->operationalState?"true":"false"));
//		m_lscHost.SetText(nRow, m_szHostColum[eHostOper], pOpStat->operationalState?STR_OPERA_ON:STR_OPERA_OFF);
		Info.operationalState = pOpStat->operationalState;
		m_lsHost.GetAt(pos) = Info;

//		RefreshHostList();
//		m_lscHost.Invalidate();
		UpdateListRow(nRow, &Info); //OK

		RefreshPaneText();
		return 0;
	}

	return 0;
}

int CHostView::ChangeAdStat(SAdStat* pAdStat)
{
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pAdStat->siteId || Info.hostId != pAdStat->hostId)
			continue;

		Info.adminState = pAdStat->adminState;
		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info); //OK

		//LVITEM item;
		//item.iItem = nRow;
		//item.iSubItem = m_lscHost.GetColPos(m_szHostColum[eSiteName]);
		//item.mask = LVIF_IMAGE;
		//item.iImage = (Info.adminState ? eMonitorOn : eMonitorOff);
		//m_lscHost.SetItem(&item);
//		m_lscHost.Invalidate();
		return 0;
	}

	return 0;
}

int CHostView::UpdatePackage(SUpPackage* pUpPackage)
{
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pUpPackage->siteId || Info.hostId != pUpPackage->hostId)
			continue;

		TraceLog(("autoPackage1 : %s", pUpPackage->autoPackage1));
		if(pUpPackage->autoPackage1 != "-")
			Info.autoPackage1 = pUpPackage->autoPackage1;

		TraceLog(("autoPackage2 : %s", pUpPackage->autoPackage2));
		if(pUpPackage->autoPackage2 != "-")
			Info.autoPackage2 = pUpPackage->autoPackage2;

		TraceLog(("currentPackage1 : %s", pUpPackage->currentPackage1));
		if(pUpPackage->currentPackage1 != "-")
			Info.currentPackage1 = pUpPackage->currentPackage1;

		TraceLog(("currentPackage2 : %s", pUpPackage->currentPackage2));
		if(pUpPackage->currentPackage2 != "-")
			Info.currentPackage2 = pUpPackage->currentPackage2;

		TraceLog(("lastPackage1 : %s", pUpPackage->lastPackage1));
		if(pUpPackage->lastPackage1 != "-")
			Info.lastPackage1 = pUpPackage->lastPackage1;

		TraceLog(("lastPackage2 : %s", pUpPackage->lastPackage2));
		if(pUpPackage->lastPackage2 != "-")
			Info.lastPackage2 = pUpPackage->lastPackage2;

		TraceLog(("networkUse1 : %d", pUpPackage->networkUse1));
		if(pUpPackage->networkUse1 != -1)
			Info.networkUse1 = pUpPackage->networkUse1;
		
		TraceLog(("networkUse2 : %d", pUpPackage->networkUse2));
		if(pUpPackage->networkUse2 != -1)
			Info.networkUse2 = pUpPackage->networkUse2;

		TraceLog(("hostMsg1 : %s", pUpPackage->hostMsg1));
		Info.hostMsg1 = pUpPackage->hostMsg1;

		TraceLog(("hostMsg2 : %s", pUpPackage->hostMsg2));
		Info.hostMsg2 = pUpPackage->hostMsg2;

		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info); //OK

		//CString szBuf;

		//szBuf.Format("%s %s %s", Info.autoPackage1, Info.autoPackage2.IsEmpty()?"":STR_SEP, Info.autoPackage2);
		//m_lscHost.SetText(nRow, m_szHostColum[eHostAuto1], szBuf);

		//szBuf.Format("%s %s %s", Info.currentPackage1, Info.currentPackage2.IsEmpty()?"":STR_SEP, Info.currentPackage2);
		//m_lscHost.SetText(nRow, m_szHostColum[eHostCur1], szBuf);

		//szBuf.Format("%s %s %s", Info.lastPackage1, Info.lastPackage2.IsEmpty()?"":STR_SEP, Info.lastPackage2);
		//m_lscHost.SetText(nRow, m_szHostColum[eHostLast1], szBuf);

		//szBuf.Format("%s %s %s", 
		//	Info.networkUse1?STR_NET_TRUE:STR_NET_FALSE, 
		//	(Info.displayCounter==1)?"":STR_SEP, 
		//	(Info.displayCounter==1)?"":(Info.networkUse2?STR_NET_TRUE:STR_NET_FALSE));
		//m_lscHost.SetText(nRow, m_szHostColum[eHostNet1], szBuf);

		return 0;
	}

	return 0;
}

void CHostView::OnPackageApply()
{
	// skpark 2013.3.29
	// ��Ű��E������ 2�ܰ��� ���̾�α׷� �����Ǵ� ���� �ϳ��� ���̾�α׷� ��ħ.
	
	//CPackageChangeDlg dlg;
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	dlg.SetEnvValue(GetEnvPtr()->m_szLoginID, szPath);


	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos) return;

	
	SHostInfo info = m_lsHost.GetAt(pos);
	for(int i=0; i<info.displayCounter ;i++)
	{
		SHostInfo infoDisp = info;
		infoDisp.displayCounter = i+1;
		dlg.m_arHostList.Add(infoDisp);
	}

	if(dlg.m_arHostList.GetCount() <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}
	
	if(dlg.DoModal() == IDOK)
	{
		//RefreshHostList();
	}
	
}

void CHostView::OnAutoPackage1()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("OnAutoPackage1 %d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	RunStudio(Info.autoPackage1);
}

void CHostView::OnAutoPackage2()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("OnAutoPackage2 %d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	RunStudio(Info.autoPackage2);
}

void CHostView::OnCurPackage1()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("OnCurPackage1 %d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	RunStudio(Info.currentPackage1);
}

void CHostView::OnCurPackage2()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("OnCurPackage2 %d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	RunStudio(Info.currentPackage2);
}

void CHostView::OnLastPackage1()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("OnLastPackage1 %d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	RunStudio(Info.lastPackage1);
}

void CHostView::OnLastPackage2()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	TRACE("OnLastPackage2 %d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	RunStudio(Info.lastPackage2);
}

void CHostView::OnRemotelogin()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	
	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // â��?E: ���� �α����� �ڳ����� ��E��Ѵ? (���� UKIPA version ó��)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(Info.siteId);
		if(!aData) return;
		TraceLog((aData->ipAddress.c_str()));

		CString szPath;
#ifdef _DEBUG
		szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
		szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
		TraceLog((szPath));

		int nPos = Info.hostId.Find(_T("-"));
		CString strId = (nPos < 0 ? _T("") : Info.hostId.Mid(nPos+1));

		CString szParam = " /proxy ";
		szParam += aData->ipAddress.c_str();
		szParam += ":5901 ID:";
		//szParam += ToString(Info.vncPort);
		szParam += strId;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);
*/
		CMainFrame::RemoteLogin_viaServer(Info.siteId, Info.hostId);
	}
	else if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
	{
		CString szParam = " ";
		szParam += Info.ipAddress;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
}

void CHostView::OnRemotelogin2()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	
	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // â��?E: ���� �α����� �ڳ����� ��E��Ѵ? (���� UKIPA version ó��)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		CString szPath;
#ifdef _DEBUG
		szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
		szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
		TraceLog((szPath));

		CString szParam;
		szParam.Format(_T("%s:%d"), Info.ipAddress, Info.vncPort);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);
*/
		CMainFrame::RemoteLogin_Direct(Info.ipAddress, Info.vncPort);
	}
	else if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType)
	{
		CString szParam = " ";
		szParam += Info.ipAddress;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
}

void CHostView::OnRemotelogin3()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	
	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // â��?E: ���� �α����� �ڳ����� ��E��Ѵ? (���� UKIPA version ó��)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(Info.siteId);
		if(!aData) return;
		TraceLog((aData->ipAddress.c_str()));

		CIPInputDLG		aDlg;

		aDlg.setIpAddress(aData->ipAddress.c_str());

		if(aDlg.DoModal() == IDOK){

			CString repeaterIp = aDlg.getIpAddress();

			if(repeaterIp.IsEmpty()){
				return;
			}

			CString szPath;
	#ifdef _DEBUG
			szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
	#else
			szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
	#endif
			TraceLog((szPath));

			int nPos = Info.hostId.Find(_T("-"));
			CString strId = (nPos < 0 ? _T("") : Info.hostId.Mid(nPos+1));

			CString szParam = " /proxy ";
			szParam += repeaterIp;
			szParam += ":5901 ID:";
			//szParam += ToString(Info.vncPort);
			szParam += strId;

			HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);

		}
*/
		CMainFrame::RemoteLogin_viaSpecified(Info.siteId, Info.hostId);
	}
}

void CHostView::OnRemoteDesktop()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos) return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	CString szParam;
	szParam.Format(_T(" /v %s"), Info.ipAddress);

	HINSTANCE nRet = ShellExecute(NULL, NULL, "mstsc.exe", szParam, "", SW_SHOWNORMAL);
}

void CHostView::OnOpenscreenshotslide()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	if(!scratchUtil::getInstance()->isFlashInstalled()){
		if(MessageBox(LoadStringById(IDS_HOSTVIEW_MSG015), LoadStringById(IDS_HOSTVIEW_STR005), MB_YESNO|MB_ICONINFORMATION) == IDYES){
			installUtil::getInstance()->installFlashPlayer();
		}
		//return;
	}

	SHostInfo Info = m_lsHost.GetAt(pos);

	RunFlashView(Info.siteId, Info.hostId);
}

void CHostView::OnUse()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	ubcHost::setAdminState(Info.siteId, Info.hostId, !Info.adminState);
	m_btnRefHost.SetFocus();
}

void CHostView::OnShowreservationinfo()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	CString strIniPath;
	strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	int nRet = 0;
	bool bModify = false;

	if(CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority){
		nRet = RunPackageRevDlg(
			eSAManager, 
			"*",
			Info.hostId,
			"", 
			"",
			GetEnvPtr()->m_Authority,
			GetEnvPtr()->m_szLoginID,
			GetEnvPtr()->m_szSite,
			strIniPath,
			bModify,
			"",
			GetEnvPtr()->m_strCustomer);
	}else{
		nRet = RunPackageRevDlg(
			eSAManager, 
			GetEnvPtr()->m_szSite, 
			Info.hostId,
			"", 
			"",
			GetEnvPtr()->m_Authority,
			GetEnvPtr()->m_szLoginID,
			GetEnvPtr()->m_szSite,
			strIniPath,
			bModify,
			"",
			GetEnvPtr()->m_strCustomer);
	}
}

void CHostView::OnProperties()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

	CArray<int> arRow;
	arRow.Add(m_ptSelList.y);
	ModifyHost(arRow, true);
}

void CHostView::RunStudio(LPCTSTR szPackage)
{
	if(szPackage == NULL) return;

	// ���� �����ϴ� ����ÁE��Ű������EȮ��
	BOOL bExist = FALSE;

	// 2010.08.06 by gwangsoo
	if(!CCopModule::GetObject()->IsExistPackage(szPackage, bExist))
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG019));
		return;
	}

	if(!bExist)
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG018), szPackage);
		UbcMessageBox(strMsg);
		return;
	}
/*
	unsigned long lPid = scratchUtil::getInstance()->getPid(UBCSTUDIOEEEXE_STR);
	HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);

	if(hWnd){
		CWnd* pWnd = CWnd::FromHandle(hWnd);
		pWnd->ActivateTopParent();

		if(pWnd->IsIconic()){
			pWnd->ShowWindow(SW_RESTORE);
		}

		COPYDATASTRUCT cds;
		cds.dwData = (ULONG_PTR)eCDSetPackage;
		cds.cbData = strlen(szPackage);
		cds.lpData = (PVOID)szPackage;

		::SendMessage(hWnd, WM_COPYDATA, (WPARAM)GetSafeHwnd(), (LPARAM)&cds);

		return;
	}
*/
	CString szPath = "";
	szPath.Format("\"%s%s\"", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	CString szParam = "";
	szParam += " +siteid=";
	szParam += GetEnvPtr()->m_szSite;
	szParam += " +userid=";
	szParam += GetEnvPtr()->m_szLoginID;
	szParam += " +passwd=";
	szParam += GetEnvPtr()->m_szPassword;
	szParam += " +package=";
	szParam += szPackage;

	HINSTANCE nRet = ShellExecute(NULL, NULL, UBCSTUDIOEEEXE_STR, szParam, szPath, SW_SHOWNORMAL);
}

int CHostView::ChangeVncStat(cciEvent* ev)
{
	TraceLog(("CHostView::ChangeVncStat begin"));

	ciString hostId;
	ciBoolean vncState;

	ev->getItem("hostId", hostId);
	TraceLog(("hostId [%s]", hostId.c_str()));

	ev->getItem("vncState", vncState);
	TraceLog(("vncState [%s]", (vncState ? "TRUE" : "FALSE")));

	HWND hWnd = m_lscHost.GetSafeHwnd();
	TraceLog(("hWnd [%X]", hWnd));

	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		if(::IsWindow(hWnd) == FALSE) return 0;

		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo info = m_lsHost.GetAt(pos);
		if(info.hostId != hostId.c_str()) continue;

		TraceLog(("info.hostId [%s]", info.hostId));

		m_lsHost.GetAt(pos).vncState = vncState;

		TraceLog(("m_lsHost.GetAt(pos).vncState [%s]", (m_lsHost.GetAt(pos).vncState ? "TRUE" : "FALSE")));

		LVITEM item;
		item.iItem = nRow;
		item.iSubItem = eHostVNCStat;//m_lscHost.GetColPos(m_szHostColum[eHostVNCStat]);
		item.mask = LVIF_IMAGE|LVIF_TEXT;
		item.pszText = (info.vncState || info.operationalState ? _T("On") : _T("Off"));
		item.iImage = (info.vncState || info.operationalState ? eVncOn : eVncOff);

		TraceLog(("SetItem"));

		m_lscHost.SetItem(&item);

		break;
	}

	TraceLog(("CHostView::ChangeVncStat end"));

	return 0;
}

//void CHostView::UpdateListRow(int nRow, SHostInfo* pInfo)
//{
//	if(!pInfo || m_lscHost.GetItemCount() < nRow)
//		return;
//
//	// 0000707: ����ÁE�ٿ��ε�E���� ��ȸ ���
//    COLORREF crTextBk = COLOR_WHITE;
//	if(IsJustNowHost(pInfo->hostId))
//	{
//		crTextBk = COLOR_LILAC;
//	}
//	else if(pInfo->operationalState)
//	{
//		crTextBk = COLOR_YELLOW;
//	}
//	else
//	{
//		crTextBk = COLOR_GRAY;
//	}
//	m_lscHost.SetRowColor(nRow, crTextBk, m_lscHost.GetTextColor());
//
//	CString szBuf;
//
//	m_lscHost.SetText(nRow, m_szHostColum[eSiteName], pInfo->siteName);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostID], pInfo->hostId);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostName], pInfo->hostName);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostType], CUbcCode::GetInstance()->GetCodeName(_T("HostType"), pInfo->hostType));
//	m_lscHost.SetText(nRow, m_szHostColum[eHostVendor], pInfo->vendor);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostModel], pInfo->model);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostOs], pInfo->os);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostSerial], pInfo->serialNo);
//	m_lscHost.SetText(nRow, m_szHostColum[eVersion], pInfo->version);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostPeriod], ToString(pInfo->period));
//	m_lscHost.SetText(nRow, m_szHostColum[eHostDesc], pInfo->description);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostIP], pInfo->ipAddress);
//
//	m_lscHost.SetText(nRow, m_szHostColum[eHostMacAddr], pInfo->macAddress);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostEdition], pInfo->edition);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostAuthDate], pInfo->authDate.Format(STR_ENV_TIME));
//	m_lscHost.SetText(nRow, m_szHostColum[eHostDisCnt], ToString(pInfo->displayCounter));
//
////	szBuf.Format("%s %s %s", pInfo->autoPackage1, pInfo->autoPackage2.IsEmpty()?"":STR_SEP, pInfo->autoPackage2);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostAuto1], pInfo->autoPackage1);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostAuto2], pInfo->autoPackage2);
//
////	szBuf.Format("%s %s %s", pInfo->currentPackage1, pInfo->currentPackage2.IsEmpty()?"":STR_SEP, pInfo->currentPackage2);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostCur1], pInfo->currentPackage1);
//	m_lscHost.SetText(nRow, m_szHostColum[eHostCur2], pInfo->currentPackage2);
//
////	szBuf.Format("%s %s %s", pInfo->lastPackage1, pInfo->lastPackage2.IsEmpty()?"":STR_SEP, pInfo->lastPackage2);
////	m_lscHost.SetText(nRow, m_szHostColum[eHostLast1], szBuf);
////		m_lscHost.SetText(nRow, m_szHostColum[eHostLast2], pInfo->lastPackage2);
//	
//	szBuf.Format("%s %s %s", 
//		pInfo->networkUse1?STR_NET_TRUE:STR_NET_FALSE, 
//		(pInfo->displayCounter==1)?"":STR_SEP, 
//		(pInfo->displayCounter==1)?"":(pInfo->networkUse2?STR_NET_TRUE:STR_NET_FALSE));
//	m_lscHost.SetText(nRow, m_szHostColum[eHostNet1], szBuf);
////		m_lscHost.SetText(nRow, m_szHostColum[eHostNet2], pInfo->networkUse2?STR_NET_TRUE:STR_NET_FALSE);
//	
//	m_lscHost.SetText(nRow, m_szHostColum[eHostUpDate], pInfo->lastUpdateTime.Format(STR_ENV_TIME));
//	m_lscHost.SetText(nRow, m_szHostColum[eHostVNCPort], ToString(pInfo->vncPort));
//
//	m_lscHost.SetText(nRow, m_szHostColum[eDownloadTime], pInfo->contentsDownloadTime);
//
//	CString strCategory;
//	GetDlgItemText(IDC_EDIT_FILTER_TAG, strCategory);
//	m_lscHost.SetText(nRow, m_szHostColum[eTag], (strCategory.IsEmpty() ? pInfo->category : strCategory));
//
//	CString strDownState;
//	if(pInfo->download1State == 0)
//	{
//		m_lscHost.SetProgressBarText( nRow, m_lscHost.GetColPos(m_szHostColum[eHostLast1])
//									, 0
//									, DT_LEFT
//									, pInfo->lastPackage1
//									, FALSE
//									, crTextBk
//									, crTextBk
//									, COLOR_BLACK
//									);
//	}
//	else if(pInfo->download1State == 4)
//	{
//		m_lscHost.SetProgressBarText( nRow, m_lscHost.GetColPos(m_szHostColum[eHostLast1])
//									, 0
//									, DT_LEFT
//									, pInfo->lastPackage1
//									, FALSE
//									, crTextBk
//									, crTextBk
//									, COLOR_BLACK
//									);
//	}
//	else
//	{
//		int nPercent = 0;
//		int nDelimPos = pInfo->progress1.Find(_T("/"));
//		if( nDelimPos > 0 && pInfo->progress1.GetLength() > nDelimPos+1 )
//		{
//			int nValue1 = _ttoi(pInfo->progress1.Left(nDelimPos));
//			int nValue2 = _ttoi(pInfo->progress1.Mid(nDelimPos+1));
//
//			// 0/0 ���� Default�̹Ƿ� 0���� ������ �濁E?�߻�����E�ʵ��� ��
//			if(nValue2 != 0)
//			{
//				nPercent = (nValue1 * 100) / nValue2;
//			}
//		}
//
//		strDownState.Format("%s %s : %s", pInfo->progress1, DOWNLOAD_STATE[pInfo->download1State], pInfo->lastPackage1);
//		m_lscHost.SetProgressBarText( nRow, m_lscHost.GetColPos(m_szHostColum[eHostLast1])
//									, nPercent
//									, DT_LEFT
//									, strDownState
//									, FALSE
//									, ((pInfo->download1State == 2 || pInfo->download1State == 3) ? COLOR_FAIL : COLOR_DOWNLOAD)
//									, COLOR_GRAY
//									, COLOR_BLACK
//									);
//	}
//
//	if(pInfo->download2State == 0)
//	{
//		m_lscHost.SetProgressBarText( nRow, m_lscHost.GetColPos(m_szHostColum[eHostLast2])
//									, 0
//									, DT_LEFT
//									, pInfo->lastPackage2
//									, FALSE
//									, crTextBk
//									, crTextBk
//									, COLOR_BLACK
//									);
//	}
//	else if(pInfo->download2State == 4)
//	{
//		m_lscHost.SetProgressBarText( nRow, m_lscHost.GetColPos(m_szHostColum[eHostLast2])
//									, 0
//									, DT_LEFT
//									, pInfo->lastPackage2
//									, FALSE
//									, crTextBk
//									, crTextBk
//									, COLOR_BLACK
//									);
//	}
//	else
//	{
//		int nPercent = 0;
//		int nDelimPos = pInfo->progress2.Find(_T("/"));
//		if( nDelimPos > 0 && pInfo->progress2.GetLength() > nDelimPos+1 )
//		{
//			int nValue1 = _ttoi(pInfo->progress2.Left(nDelimPos));
//			int nValue2 = _ttoi(pInfo->progress2.Mid(nDelimPos+1));
//
//			// 0/0 ���� Default�̹Ƿ� 0���� ������ �濁E?�߻�����E�ʵ��� ��
//			if(nValue2 != 0)
//			{
//				nPercent = (nValue1 * 100) / nValue2;
//			}
//		}
//
//		strDownState.Format("%s %s : %s", pInfo->progress2, DOWNLOAD_STATE[pInfo->download2State], pInfo->lastPackage2);
//		m_lscHost.SetProgressBarText( nRow, m_lscHost.GetColPos(m_szHostColum[eHostLast2])
//									, nPercent
//									, DT_LEFT
//									, strDownState
//									, FALSE
//									, ((pInfo->download2State == 2 || pInfo->download2State == 3) ? COLOR_FAIL : COLOR_DOWNLOAD)
//									, COLOR_GRAY
//									, COLOR_BLACK
//									);
//	}
//
//	bool bMonitorStat = utvUtil::getInstance()->monitorState(pInfo->monitorOff, pInfo->monitorOffList);
//	LVITEM itemMonitorStat;
//	itemMonitorStat.iItem = nRow;
//	itemMonitorStat.iSubItem = m_lscHost.GetColPos(m_szHostColum[eMonitorStat]);
//	itemMonitorStat.mask = LVIF_IMAGE|LVIF_TEXT;
//	itemMonitorStat.pszText = (bMonitorStat && pInfo->operationalState ? _T("On") : _T("Off"));
//	itemMonitorStat.iImage = (bMonitorStat && pInfo->operationalState ? eMonitorOn : eMonitorOff);
//	m_lscHost.SetItem(&itemMonitorStat);
//
//	LVITEM itemHostVNCStat;
//	itemHostVNCStat.iItem = nRow;
//	itemHostVNCStat.iSubItem = m_lscHost.GetColPos(m_szHostColum[eHostVNCStat]);
//	itemHostVNCStat.mask = LVIF_IMAGE|LVIF_TEXT;
//	itemHostVNCStat.pszText = (pInfo->vncState ? _T("On") : _T("Off"));
//	itemHostVNCStat.iImage = (pInfo->vncState ? eVncOn : eVncOff);
//	m_lscHost.SetItem(&itemHostVNCStat);
//}

void CHostView::UpdateListRow(int nRow, SHostInfo* pInfo)
{
	if(!pInfo || m_lscHost.GetItemCount() < nRow)
		return;

	// 0000707: ����ÁE�ٿ��ε�E���� ��ȸ ���
    COLORREF crTextBk = COLOR_WHITE;
	if(IsJustNowHost(pInfo->hostId))
	{
		crTextBk = COLOR_LILAC;
	}
	else if(pInfo->operationalState)
	{
		crTextBk = COLOR_YELLOW;
	}
	else
	{
		crTextBk = COLOR_GRAY;
	}
	m_lscHost.SetRowColor(nRow, crTextBk, m_lscHost.GetTextColor());

	CString szBuf;

	m_lscHost.SetItemText(nRow, eSiteName, pInfo->siteName);
	m_lscHost.SetItemText(nRow, eHostID, pInfo->hostId);
	m_lscHost.SetItemText(nRow, eHostName, pInfo->hostName);
	m_lscHost.SetItemText(nRow, eHostType, CUbcCode::GetInstance()->GetCodeName(_T("HostType"), pInfo->hostType));
	//m_lscHost.SetItemText(nRow, eHostVendor, pInfo->vendor);
	m_lscHost.SetItemText(nRow, eMonitorType, CUbcCode::GetInstance()->GetCodeName(_T("MonitorType"), pInfo->monitorType));
	//m_lscHost.SetItemText(nRow, eHostModel, pInfo->model);
	m_lscHost.SetItemText(nRow, eHostOs, pInfo->os);
	//m_lscHost.SetItemText(nRow, eHostSerial, ToString(pInfo->serialNo));
	m_lscHost.SetItemText(nRow, eVersion, pInfo->version);
	//m_lscHost.SetItemText(nRow, eHostPeriod, ToString(pInfo->period));
	m_lscHost.SetItemText(nRow, eHostAddr1, pInfo->location);
	m_lscHost.SetItemText(nRow, eHostDesc, pInfo->description);
	m_lscHost.SetItemText(nRow, eHostIP, pInfo->ipAddress);

	m_lscHost.SetItemText(nRow, eHostMacAddr, pInfo->macAddress);
	//m_lscHost.SetItemText(nRow, eHostEdition, pInfo->edition);
	m_lscHost.SetItemText(nRow, eHostBootUpTime, (pInfo->bootUpTime.GetTime()==0?_T(""):pInfo->bootUpTime.Format(STR_ENV_TIME)));
	m_lscHost.SetItemText(nRow, eHostDisCnt, ToString(pInfo->displayCounter));

//	szBuf.Format("%s %s %s", pInfo->autoPackage1, pInfo->autoPackage2.IsEmpty()?"":STR_SEP, pInfo->autoPackage2);
	m_lscHost.SetItemText(nRow, eHostAuto1, pInfo->autoPackage1);
	m_lscHost.SetItemText(nRow, eHostAuto2, pInfo->autoPackage2);

//	szBuf.Format("%s %s %s", pInfo->currentPackage1, pInfo->currentPackage2.IsEmpty()?"":STR_SEP, pInfo->currentPackage2);
	m_lscHost.SetItemText(nRow, eHostCur1, pInfo->currentPackage1);
	m_lscHost.SetItemText(nRow, eHostCur2, pInfo->currentPackage2);

	//TraceLog(("timeLineInfo.nextSchedule1=(%s)", pInfo->timeLineInfo.nextSchedule1));
	if(this->m_showNextSchedule){
		m_lscHost.SetItemText(nRow, eHostNext1, pInfo->timeLineInfo.nextSchedule1);
		m_lscHost.SetItemText(nRow, eHostNext2, pInfo->timeLineInfo.nextSchedule2);
	}
	//	szBuf.Format("%s %s %s", pInfo->lastPackage1, pInfo->lastPackage2.IsEmpty()?"":STR_SEP, pInfo->lastPackage2);
//	m_lscHost.SetItemText(nRow, eHostLast1, szBuf);
//		m_lscHost.SetItemText(nRow, eHostLast2, pInfo->lastPackage2);

	m_lscHost.SetItemText(nRow, eDomainName, pInfo->domainName);
	m_lscHost.SetItemText(nRow, eShutdownTime, pInfo->shutdownTime);
	m_lscHost.SetItemText(nRow, eStartupTime, pInfo->startupTime);
	m_lscHost.SetItemText(nRow, eSosId, pInfo->sosId);

//	szBuf.Format("%s %s %s", 
//		pInfo->networkUse1?STR_NET_TRUE:STR_NET_FALSE, 
//		(pInfo->displayCounter==1)?"":STR_SEP, 
//		(pInfo->displayCounter==1)?"":(pInfo->networkUse2?STR_NET_TRUE:STR_NET_FALSE));
//	m_lscHost.SetItemText(nRow, eHostNet1, szBuf);
//		m_lscHost.SetItemText(nRow, eHostNet2, pInfo->networkUse2?STR_NET_TRUE:STR_NET_FALSE);

	m_lscHost.SetItemText(nRow, eHostUpDate, pInfo->lastUpdateTime.Format(STR_ENV_TIME));
	m_lscHost.SetItemText(nRow, eHostVNCPort, ToString(pInfo->vncPort));

	m_lscHost.SetItemText(nRow, eDownloadTime, pInfo->contentsDownloadTime);

	//CString strCategory;
	//GetDlgItemText(IDC_EDIT_FILTER_TAG, strCategory);
	//m_lscHost.SetItemText(nRow, eTag, (strCategory.IsEmpty() ? pInfo->category : strCategory));
	m_lscHost.SetItemText(nRow, eTag, pInfo->category);

	CString strDownState;
	if(pInfo->download1State == 0)
	{
		m_lscHost.SetProgressBarText( nRow, eHostLast1//m_lscHost.GetColPos(m_szHostColum[eHostLast1])
									, 0
									, DT_LEFT
									, pInfo->lastPackage1
									, FALSE
									, crTextBk
									, crTextBk
									, COLOR_BLACK
									);
	}
	else if(pInfo->download1State == 4)
	{
		m_lscHost.SetProgressBarText( nRow, eHostLast1//m_lscHost.GetColPos(m_szHostColum[eHostLast1])
									, 0
									, DT_LEFT
									, pInfo->lastPackage1
									, FALSE
									, crTextBk
									, crTextBk
									, COLOR_BLACK
									);
	}
	else
	{
		int nPercent = 0;
		int nDelimPos = pInfo->progress1.Find(_T("/"));
		if( nDelimPos > 0 && pInfo->progress1.GetLength() > nDelimPos+1 )
		{
			int nValue1 = _ttoi(pInfo->progress1.Left(nDelimPos));
			int nValue2 = _ttoi(pInfo->progress1.Mid(nDelimPos+1));

			// 0/0 ���� Default�̹Ƿ� 0���� ������ �濁E?�߻�����E�ʵ��� ��
			if(nValue2 != 0)
			{
				nPercent = (nValue1 * 100) / nValue2;
			}
		}

		strDownState.Format("%s %s : %s", pInfo->progress1, DOWNLOAD_STATE[pInfo->download1State], pInfo->lastPackage1);
		m_lscHost.SetProgressBarText( nRow, eHostLast1//m_lscHost.GetColPos(m_szHostColum[eHostLast1])
									, nPercent
									, DT_LEFT
									, strDownState
									, FALSE
									, ((pInfo->download1State == 2 || pInfo->download1State == 3) ? COLOR_FAIL : COLOR_DOWNLOAD)
									, COLOR_GRAY
									, COLOR_BLACK
									);
	}

	if(pInfo->download2State == 0)
	{
		m_lscHost.SetProgressBarText( nRow, eHostLast2//m_lscHost.GetColPos(m_szHostColum[eHostLast2])
									, 0
									, DT_LEFT
									, pInfo->lastPackage2
									, FALSE
									, crTextBk
									, crTextBk
									, COLOR_BLACK
									);
	}
	else if(pInfo->download2State == 4)
	{
		m_lscHost.SetProgressBarText( nRow, eHostLast2//m_lscHost.GetColPos(m_szHostColum[eHostLast2])
									, 0
									, DT_LEFT
									, pInfo->lastPackage2
									, FALSE
									, crTextBk
									, crTextBk
									, COLOR_BLACK
									);
	}
	else
	{
		int nPercent = 0;
		int nDelimPos = pInfo->progress2.Find(_T("/"));
		if( nDelimPos > 0 && pInfo->progress2.GetLength() > nDelimPos+1 )
		{
			int nValue1 = _ttoi(pInfo->progress2.Left(nDelimPos));
			int nValue2 = _ttoi(pInfo->progress2.Mid(nDelimPos+1));

			// 0/0 ���� Default�̹Ƿ� 0���� ������ �濁E?�߻�����E�ʵ��� ��
			if(nValue2 != 0)
			{
				nPercent = (nValue1 * 100) / nValue2;
			}
		}

		strDownState.Format("%s %s : %s", pInfo->progress2, DOWNLOAD_STATE[pInfo->download2State], pInfo->lastPackage2);
		m_lscHost.SetProgressBarText( nRow, eHostLast2//m_lscHost.GetColPos(m_szHostColum[eHostLast2])
									, nPercent
									, DT_LEFT
									, strDownState
									, FALSE
									, ((pInfo->download2State == 2 || pInfo->download2State == 3) ? COLOR_FAIL : COLOR_DOWNLOAD)
									, COLOR_GRAY
									, COLOR_BLACK
									);
	}

	// Ÿ������ ��ȿ����EȮ�� �� ȭ�鿡 ���̵��� �Ѵ�.
	if(pInfo->gmt >= -24*60 && pInfo->gmt <= 24*60)
	{
		int nTimezone = pInfo->gmt * (pInfo->gmt>=0 ? 1 : -1);
		CString strTimezone;
		strTimezone.Format(_T("UTC%c%02d:%02d")
						, (pInfo->gmt>=0 ? '-' : '+')
						, nTimezone/60
						, nTimezone-(nTimezone/60)*60
						);
		m_lscHost.SetItemText(nRow, eGmt, strTimezone);
	}
	else
	{
		m_lscHost.SetItemText(nRow, eGmt, _T(""));
	}

	// �ŵ����� ��E�������� ��E?�翁Eð?
	if(pInfo->monitorUseTime < 0)
	{
		m_lscHost.SetItemText(nRow, eMonitorUseTime, _T("Unknown"));
	}
	else
	{
		m_lscHost.SetItemText(nRow, eMonitorUseTime, ToString(pInfo->monitorUseTime));
	}

	// 0001371: [RFP] ������ ������� ON/OFF ��Ȳ�� �����ִ� ��� (�Ŵ���E��Ʈ)
	//bool bMonitorStat = utvUtil::getInstance()->monitorState(pInfo->monitorOff, pInfo->monitorOffList);
	int nMonitorImgIdx = 2;
	if(pInfo->operationalState)
	{
		if(pInfo->monitorState > 0) nMonitorImgIdx = eMonitorOn;
		if(pInfo->monitorState == 0) nMonitorImgIdx = eMonitorOff;
		if(pInfo->monitorState < 0) nMonitorImgIdx = eMonitorUnknown;
	}
	m_lscHost.SetItemText(nRow, eDisk1Avail, (pInfo->disk1Avail < 0 ? GetDiskAvailError(pInfo->disk1Avail) : ToString((double)pInfo->disk1Avail, 2) + _T(" G")));
	m_lscHost.SetItemText(nRow, eDisk2Avail, (pInfo->disk2Avail < 0 ? GetDiskAvailError(pInfo->disk2Avail) : ToString((double)pInfo->disk2Avail, 2) + _T(" G")));

	LVITEM itemMonitorStat;
	itemMonitorStat.iItem = nRow;
	itemMonitorStat.iSubItem = eMonitorStat;//m_lscHost.GetColPos(m_szHostColum[eMonitorStat]);
	itemMonitorStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemMonitorStat.pszText = (nMonitorImgIdx == eMonitorUnknown ? _T("Unknown") : (nMonitorImgIdx == eMonitorOn ? _T("On") : _T("Off")));
	itemMonitorStat.iImage = nMonitorImgIdx;
	m_lscHost.SetItem(&itemMonitorStat);

	LVITEM itemHostVNCStat;
	itemHostVNCStat.iItem = nRow;
	itemHostVNCStat.iSubItem = eHostVNCStat;//m_lscHost.GetColPos(m_szHostColum[eHostVNCStat]);
	itemHostVNCStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemHostVNCStat.pszText = (pInfo->vncState || pInfo->operationalState ? _T("On") : _T("Off"));
	itemHostVNCStat.iImage = (pInfo->vncState || pInfo->operationalState ? eVncOn : eVncOff);
	m_lscHost.SetItem(&itemHostVNCStat);

	CString strHostMsg = pInfo->hostMsg1;
	if(!strHostMsg.IsEmpty() && pInfo->hostMsg2.IsEmpty())
	{
		strHostMsg += _T(" ");
	}
	strHostMsg += pInfo->hostMsg2;
	m_lscHost.SetItemText(nRow, eHostMsg, strHostMsg);

	LVITEM itemHostMsg;
	itemHostMsg.iItem = nRow;
	itemHostMsg.iSubItem = eHostMsg;//m_lscHost.GetColPos(m_szHostColum[eHostVNCStat]);
	itemHostMsg.mask = LVIF_IMAGE;
	itemHostMsg.iImage = (pInfo->hostMsg1.IsEmpty() && pInfo->hostMsg2.IsEmpty() ? -1 : eExclamation);
	m_lscHost.SetItem(&itemHostMsg);
}

CString CHostView::GetDiskAvailError(float errCode)
{
	if(int(errCode) == -2) {
		return "No Drive";
	}
	return "";
}

// 2010.02.17 by gwangsoo
void CHostView::OnHostlistCancelPackage(UINT nID)
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos) return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	CWaitMessageBox wait;

	int nTargetDisplay = eFront;

	if(nID == ID_HOSTLIST_CANCELPACKAGE)
	{
		nTargetDisplay = eFront;
	}
	else
	{
		nTargetDisplay = nID - ID_HOSTLIST_CANCELPACKAGE_A;
	}

	CString szMsg;
	szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG026));
	if(UbcMessageBox(szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}


	if(!CCopModule::GetObject()->CancelPackage(Info.siteId, Info.hostId, nTargetDisplay, GetEnvPtr()->m_szLoginID))
	{
		m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
		UbcMessageBox(m_szMsg, MB_ICONERROR);
	}
}

void CHostView::OnBnClickedButtonHostHddthreshold()
{
	/*CList<int> lsRow;

	bool bSetHddThreshold = true;
	int  nHddThreshold = -1;

	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		if(bSetHddThreshold)
		{
			if(nHddThreshold < 0) nHddThreshold = Info.hddThreshold;
			bSetHddThreshold = (nHddThreshold == Info.hddThreshold);
			nHddThreshold = Info.hddThreshold;
		}
	}

	if(lsRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CHddThresholdDlg dlg;
	if(bSetHddThreshold) dlg.m_nHddThreshold = nHddThreshold;
	else                 dlg.m_nHddThreshold = -1;

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow)
	{
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;

		SHostInfo Info = m_lsHost.GetAt(posHost);

		if(dlg.m_nSelect == 0)
		{
			Info.hddThreshold = dlg.m_nHddThreshold;
			CCopModule::GetObject()->SetHost(Info, GetEnvPtr()->m_szLoginID);
		}
		else
		{
			CCopModule::GetObject()->CleanContents(Info.siteId, Info.hostId);
		}

		m_lscHost.SetCheck(nRow, FALSE);
	}*/

	CHddThresholdEx dlg;

	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		dlg.m_aHostList.Add(Info.hostId);
	}

	if(dlg.m_aHostList.GetCount() <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	if(dlg.DoModal() != IDOK) return;

	RefreshHost(false);
}

void CHostView::BackupHostList()
{
	DeleteBackupHostList();

	m_pBackupHostList = new CMapStringToString();

	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo info = m_lsHost.GetAt(pos);
		m_pBackupHostList->SetAt(info.hostId, info.hostId);
	}
}

void CHostView::DeleteBackupHostList()
{
	if(m_pBackupHostList) delete m_pBackupHostList;
	m_pBackupHostList = NULL;
}

bool CHostView::IsJustNowHost(CString strHostId)
{
	if(!m_pBackupHostList) return false;

	CString strValue;
	m_pBackupHostList->Lookup(strHostId, strValue);

	return strValue.IsEmpty();
}

void CHostView::RefreshPaneText()
{
	int nActiveCnt = 0;
	POSITION pos = m_lsHost.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsHost.GetNext(pos);
		if(info.operationalState)
		{
			nActiveCnt++;
		}
	}

	CString strPaneText;
	strPaneText.Format(_T("Host:%d Active:%d")
					 , m_lsHost.GetCount()
					 , nActiveCnt
					 );

	if(m_pMainFrame)
	{
		m_pMainFrame->SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_HOST, (LPARAM)(LPCTSTR)strPaneText);
	}
}

void CHostView::OnBnClickedBnToExcel2()
{
	CHostExcelSave dlg;

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_HOSTVIEW_STR001)
									, m_lscHost
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}

int CHostView::ChangeDownloadStat(SDownloadStateChange* pData)
{
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pData->siteId || Info.hostId != pData->hostId)
			continue;

		// �����û�����ʰ�E�ٿ��ε尡 ����E�Ǵ� �濁E?�����Ƿ�
		// �ٿ��ε�E���º�ȭ�� �ش�E�ܸ��� �����û��Ű���� �ƴѰ濁E���÷����� �ʿ����.
		if(pData->brwId == 0 && Info.lastPackage1 != pData->programId) continue;
		if(pData->brwId == 1 && Info.lastPackage2 != pData->programId) continue;

		if(pData->brwId == 0)
		{
			Info.lastPackage1 = pData->programId;
			Info.progress1 = pData->progress;
			Info.download1State = pData->programState;
		}
		else if(pData->brwId == 1)
		{
			Info.lastPackage2 = pData->programId;
			Info.progress2 = pData->progress;
			Info.download2State = pData->programState;
		}

		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info); //OK
//		m_lscHost.Invalidate();
		return 0;
	}

	return 0;
}

// 0000751: ����ÁE�ٿ��ε�E���� ��ȸ ���
void CHostView::OnBnClickedBnRetransmit()
{
	CList<int> lsRow;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	m_szMsg = LoadStringById(IDS_HOSTVIEW_MSG020);
	if(UbcMessageBox(m_szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow){
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost)	continue;

		m_lscHost.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);

		// ����ÁE����E��?��ǻ�E��E�E??��⵿�ϴ� �������� ó����.
		CCopModule::GetObject()->Restart(Info.siteId, Info.hostId);
	}
}

// 0000751: ����ÁE�ٿ��ε�E���� ��ȸ ���
void CHostView::OnDownloadResultView()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos) return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	//CDownloadStateDlg dlg;
	//dlg.m_strSiteId = Info.siteId;
	//dlg.m_strHostId = Info.hostId;
	//dlg.m_strPackageId1 = Info.lastPackage1;
	//dlg.m_strPackageId2 = Info.lastPackage2;

	//dlg.DoModal();

	CString strPackage;
	//if     (eHostLast1 == m_ptSelList.x) strPackage = Info.lastPackage1;
	//else if(eHostLast2 == m_ptSelList.x) strPackage = Info.lastPackage2;
	//else if(eHostCur1  == m_ptSelList.x) strPackage = Info.currentPackage1;
	//else if(eHostCur2  == m_ptSelList.x) strPackage = Info.currentPackage2;
	//else if(eHostAuto1 == m_ptSelList.x) strPackage = Info.autoPackage1;
	//else if(eHostAuto2 == m_ptSelList.x) strPackage = Info.autoPackage2;

	CDownloadSummaryDlg dlg;
	dlg.SetParam(_T(""), _T(""), strPackage, Info.hostId, Info.hostName);
	dlg.DoModal();
}

void CHostView::OnBnClickedBnPowerMng()
{
	CList<int> lsRow;

	CString strHolliday;
	CString strShutdownTime;
	CString strPoweronTime;
	bool bMonitorOff = false;
	std::list<std::string> monitorOffList;
	CString strWeekShutdownTime;

	for(int nRow = 0; nRow<m_lscHost.GetItemCount(); nRow++)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;

		lsRow.AddTail(nRow);

		// ù��° ���� ÁEϿ?�ʱ�ȭ �Ѵ�
		if(!strHolliday.IsEmpty()) continue;

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;

		SHostInfo Info = m_lsHost.GetAt(posHost);

		strHolliday = Info.holiday;
		strShutdownTime = Info.shutdownTime;
		bMonitorOff = Info.monitorOff;
		monitorOffList = Info.monitorOffList;
		strWeekShutdownTime = Info.weekShutdownTime;
		strPoweronTime = Info.startupTime;
	}

	if(lsRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CHollidayDlg dlg(this);
	dlg.SetHolliday(strHolliday);
	dlg.SetPoweron(strPoweronTime);
	dlg.ShowPoweron(true);


	//skpark 2014.03.03  WIA ���� ���� Ư���ڵ�E
	if(GetEnvPtr()->m_strCustomer == "WIA"){
		dlg.ShowMonitor(false);
	}

	// strHolliday �� ����Eð��?������E �� ������ �ʱ�ȭ �ϹǷ� ���� �ѱ�E�ʿ����.
//	dlg.m_bShutdown = (!strShutdownTime.IsEmpty());
//	m_dtcPowerOff.GetTime(dlg.m_tmShutdown);

	dlg.m_bMonitorOff = bMonitorOff;
	dlg.m_listMonitorOffTime = monitorOffList;
	dlg.m_strWeekShutdownTime = strWeekShutdownTime;

	if(dlg.DoModal() != IDOK) return;

	dlg.GetHolliday(strHolliday);
	//dlg.GetPoweron(strPoweronTime);

	strShutdownTime.Empty();
	if(dlg.m_bShutdown)
	{
		strShutdownTime = dlg.m_tmShutdown.Format("%H:%M");
	}
	strPoweronTime.Empty();
	if(dlg.m_bPoweron)
	{
		strPoweronTime = dlg.m_tmPoweron.Format("%H:%M");
	}
	TraceLog(("STR_POWERON=%d,%s", dlg.m_bPoweron, strPoweronTime));

	bMonitorOff = dlg.m_bMonitorOff;
	monitorOffList = dlg.m_listMonitorOffTime;
	strWeekShutdownTime = dlg.m_strWeekShutdownTime;

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow)
	{
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;

		SHostInfo Info = m_lsHost.GetAt(posHost);

		Info.holiday = strHolliday;
		Info.shutdownTime = strShutdownTime;
		Info.startupTime = strPoweronTime;
		Info.monitorOff = bMonitorOff;
		Info.monitorOffList = monitorOffList;
		Info.weekShutdownTime = strWeekShutdownTime;

		CCopModule::GetObject()->SetPowerMng(Info, GetEnvPtr()->m_szLoginID);

		m_lscHost.SetCheck(nRow, FALSE);
	}

	if(GetEnvPtr()->m_strCustomer == "WIA"){
		RefreshHost(false);
	}
}

void CHostView::OnBnClickedButtonHostPackageChng()
{
	// skpark 2013.3.29
	// ��Ű��E������ 2�ܰ��� ���̾�α׷� �����Ǵ� ���� �ϳ��� ���̾�α׷� ��ħ.
	
	//CPackageChangeDlg dlg;
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	dlg.SetEnvValue(GetEnvPtr()->m_szLoginID, szPath);

	dlg.SetTestWarningMsg(LoadStringById(IDS_HDDTHRESHOLD_MSG005));

	for(int i=0; i<m_lscHost.GetItemCount() ;i++)
	{
		if(!m_lscHost.GetCheck(i)) continue;

		POSITION pos = (POSITION)m_lscHost.GetItemData(i);
		if(!pos) continue;

		SHostInfo info = m_lsHost.GetAt(pos);
		for(int i=0; i<info.displayCounter ;i++)
		{
			SHostInfo infoDisp = info;
			infoDisp.displayCounter = i+1;
			dlg.m_arHostList.Add(infoDisp);
		}
	}

	if(dlg.m_arHostList.GetCount() <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}

	if(dlg.DoModal() == IDOK)
	{
		//RefreshHostList();
	}
}

void CHostView::OnBnClickedButtonFilterSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // ��Ƽ������ �濁E�ּ�ó��
	if(dlg.DoModal() == IDOK)
	{
		SiteInfoList siteInfoList;
		if(dlg.GetSelectSiteInfoList(siteInfoList))
		{
			POSITION pos = siteInfoList.GetHeadPosition();
			while(pos)
			{
				SSiteInfo info = siteInfoList.GetNext(pos);
				strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
				strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
			}
		}
	}

	SetDlgItemText(IDC_EDIT_FILTER_SITENAME, strSiteName);
	m_strSiteId = strSiteId;
}

void CHostView::OnBnClickedButtonFilterContents()
{
	CContentsSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, _T(""));
		m_strContentsId = _T("");
		return;
	}

	CONTENTS_INFO_EX_LIST& list = dlg.GetSelectedContentsIdList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		CONTENTS_INFO_EX& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_CONTENTS, info.strContentsName);
		m_strContentsId = info.strContentsId;
	}
}

void CHostView::OnBnClickedButtonFilterPackage()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		m_strPackageId = _T("");
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
		m_strPackageId = info.szPackage;
	}
}

void CHostView::SaveFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;
	CString strAdvancedFilter;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	strContentsId = m_strContentsId;
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageName;
	strPackageId = m_strPackageId;
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);
	if(m_kbAdvancedFilter.GetCheck()) m_cbAdvancedFilter.GetWindowText(strAdvancedFilter);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	WritePrivateProfileString("HOST-FILTER", "HostName"         , strHostName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostID"           , strHostId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostType"         , strHostType     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteName"         , strSiteName     , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID"           , strSiteId       , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContents"  , strContentsName , szPath);
	WritePrivateProfileString("HOST-FILTER", "IncludeContentsID", strContentsId   , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackage"   , strPackageName  , szPath);
	WritePrivateProfileString("HOST-FILTER", "PlayingPackageID" , strPackageId    , szPath);
	WritePrivateProfileString("HOST-FILTER", "AdminStat"        , strAdminStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat"        , strOperaStat    , szPath);
	WritePrivateProfileString("HOST-FILTER", "Category"         , strCategory     , szPath);
	WritePrivateProfileString("HOST-FILTER", "AdvancedFilter"   , strAdvancedFilter, szPath);

	if(m_kbAdvancedFilter.GetCheck() && !strAdvancedFilter.IsEmpty() && m_cbAdvancedFilter.FindStringExact(0, strAdvancedFilter) < 0) 
	{
		m_cbAdvancedFilter.InsertString(0, strAdvancedFilter);
		for(int i=m_cbAdvancedFilter.GetCount(); i > 20 ; i--)
		{
			m_cbAdvancedFilter.DeleteString(i-1);
		}

		for(int i=0; i<m_cbAdvancedFilter.GetCount() ;i++)
		{
			CString strText;
			m_cbAdvancedFilter.GetLBText(i, strText);

			CString strKey;
			strKey.Format(_T("History%d"), i+1);
			WritePrivateProfileString("HOST-FILTER", strKey, strText, szPath);
		}
	}
}

void CHostView::LoadFilterData()
{
	CString strHostName     ;
	CString strHostId       ;
	CString strHostType     ;
	CString strSiteName     ;
	CString strSiteId       ;
	CString strContentsName ;
	CString strContentsId   ;
	CString strPackageName  ;
	CString strPackageId    ;
	CString strAdminStat    ;
	CString strOperaStat    ;
	CString strCategory     ;
	CString strAdvancedFilter;

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("HOST-FILTER", "HostName"         , "", buf, 1024, szPath); strHostName     = buf;
	GetPrivateProfileString("HOST-FILTER", "HostID"           , "", buf, 1024, szPath); strHostId       = buf;
	GetPrivateProfileString("HOST-FILTER", "HostType"         , "", buf, 1024, szPath); strHostType     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteName"         , "", buf, 1024, szPath); strSiteName     = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteID"           , "", buf, 1024, szPath); strSiteId       = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContents"  , "", buf, 1024, szPath); strContentsName = buf;
	GetPrivateProfileString("HOST-FILTER", "IncludeContentsID", "", buf, 1024, szPath); strContentsId   = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackage"   , "", buf, 1024, szPath); strPackageName  = buf;
	GetPrivateProfileString("HOST-FILTER", "PlayingPackageID" , "", buf, 1024, szPath); strPackageId    = buf;
	GetPrivateProfileString("HOST-FILTER", "AdminStat"        , "", buf, 1024, szPath); strAdminStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "OperaStat"        , "", buf, 1024, szPath); strOperaStat    = buf;
	GetPrivateProfileString("HOST-FILTER", "Category"         , "", buf, 1024, szPath); strCategory     = buf;
	GetPrivateProfileString("HOST-FILTER", "AdvancedFilter"   , "", buf, 1024, szPath); strAdvancedFilter = buf;

	SetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	SetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	if(strHostType.IsEmpty())
	{
		m_cbHostType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strHostType.Tokenize(",", pos)) != _T(""))
		{
			m_cbHostType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}
	SetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	m_strSiteId = strSiteId;
	SetDlgItemText(IDC_EDIT_FILTER_CONTENTS , strContentsName);
	m_strContentsId = strContentsId;
	SetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , strPackageName );
	m_strPackageId = strPackageId;
	m_cbAdmin.SetCurSel(_ttoi(strAdminStat));
	m_cbOperation.SetCurSel(_ttoi(strOperaStat));
	//SetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.SetWindowText(strCategory);
	m_kbAdvancedFilter.SetCheck(!strAdvancedFilter.IsEmpty());
	m_cbAdvancedFilter.EnableWindow(m_kbAdvancedFilter.GetCheck());
	m_cbAdvancedFilter.SetWindowText(strAdvancedFilter);

	for(int i=0; i<20 ;i++)
	{
		CString strKey;
		strKey.Format(_T("History%d"), i+1);
		GetPrivateProfileString("HOST-FILTER", strKey, "", buf, 1024, szPath);

		CString strValue = buf;
		if(!strValue.IsEmpty())
		{
			m_cbAdvancedFilter.AddString(strValue);
		}
	}
}

LRESULT CHostView::OnFilterHostChanged(WPARAM wParam, LPARAM lParam)
{
	LoadFilterData();
	UpdateData(FALSE);
	return 0;
}

void CHostView::OnHostlistPkgChngLogView()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos) return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	CPackageChngLogDlg dlg;
	dlg.SetParam(Info.hostId, Info.hostName);
	dlg.DoModal();
}

// 0001371: [RFP] ������ ������� ON/OFF ��Ȳ�� �����ִ� ��� (�Ŵ���E��Ʈ)
int CHostView::ChangeMonitorStat(SMonitorStat* pMonitorStat)
{
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pMonitorStat->siteId || Info.hostId != pMonitorStat->hostId)
			continue;

		Info.monitorState = pMonitorStat->monitorState;
		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info);  //OK

		//LVITEM item;
		//item.iItem = nRow;
		//item.iSubItem = m_lscHost.GetColPos(m_szHostColum[eSiteName]);
		//item.mask = LVIF_IMAGE;
		//item.iImage = (Info.adminState ? eMonitorOn : eMonitorOff);
		//m_lscHost.SetItem(&item);
//		m_lscHost.Invalidate();
		return 0;
	}

	return 0;
}

void CHostView::OnBnClickedButtonHostMonitorPower()
{
	CList<int> lsRow;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CMonitorPowerDlg dlg;
	if(dlg.DoModal() != IDOK) return;

	int nOnOffValue = dlg.GetMonitorState();

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow)
	{
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;

		m_lscHost.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);

		// ����E���¸� üũ����E����E�׳� ������ �������� �϶��.
		//if(nOnOffValue != Info.monitorState)
		{
			CCopModule::GetObject()->MonitorPower(Info.siteId, Info.hostId, nOnOffValue);
		}
	}
}

int CHostView::ChangeDiskAvail(SDiskAvailChanged* pData)
{
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pData->siteId || Info.hostId != pData->hostId)
			continue;

		if(pData->diskId == 1)
		{
			Info.disk1Avail = pData->diskAvail;
		}
		else if(pData->diskId == 2)
		{
			Info.disk2Avail = pData->diskAvail;
		}
		else
		{
			return 0;
		}

		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info); //OK

		return 0;
	}

	return 0;
}

void CHostView::OnBnClickedKbAdvancedFilter()
{
	m_cbAdvancedFilter.EnableWindow(m_kbAdvancedFilter.GetCheck());
}

void CHostView::OnBnClickedBnAddtag()
{
	int nSelCnt = 0;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;

		nSelCnt++;
	}

	if(nSelCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CInputBox dlg;
	dlg.m_strTitle = LoadStringById(IDS_HOSTVIEW_STR010);

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	// �˻���E
	CStringArray aTagList;

	int pos = 0;
	CString strTag;
	while((strTag = dlg.m_strInputText.Tokenize("/", pos)) != _T(""))
	{
		aTagList.Add(strTag);
	}

	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		CString strLowerCategory = "/" + Info.category + "/";
		strLowerCategory.MakeLower();
		for(int i=0; i<aTagList.GetCount(); i++)
		{
			CString strLowerTag = aTagList[i];
			strLowerTag.MakeLower();
			if(strLowerCategory.Find( "/" + strLowerTag + "/" ) < 0)
			{
				Info.category += "/" + aTagList[i];
			}
		}

		Info.category.Trim("/");

		CCopModule::GetObject()->SetCategory(Info, GetEnvPtr()->m_szLoginID, Info.category);
	}

	m_cbFilterTag.GetWindowText(strTag);

	// �˻���E�޺� �ʱ�ȭ
	aTagList.RemoveAll();
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_cbFilterTag.SetWindowText(strTag);

	RefreshHost(false);
}

void CHostView::OnBnClickedBnDeltag()
{
	int nSelCnt = 0;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;

		nSelCnt++;
	}

	if(nSelCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CInputBox dlg;
	dlg.m_strTitle = LoadStringById(IDS_HOSTVIEW_STR011);

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	// �˻���E
	CStringArray aTagList;

	int pos = 0;
	CString strTag;
	while((strTag = dlg.m_strInputText.Tokenize("/", pos)) != _T(""))
	{
		aTagList.Add(strTag);
	}

	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		CString strLowerCategory = "/" + Info.category + "/";
		strLowerCategory.MakeLower();
		for(int i=0; i<aTagList.GetCount(); i++)
		{
			CString strLowerTag = aTagList[i];
			strLowerTag.MakeLower();

			int nFind = strLowerCategory.Find( "/" + strLowerTag + "/" );
			if(nFind >= 0)
			{
				Info.category.Delete(nFind, strLowerTag.GetLength() + 1);
				strLowerCategory.Delete(nFind, strLowerTag.GetLength() + 1);
			}
		}

		Info.category.Trim("/");

		CCopModule::GetObject()->SetCategory(Info, GetEnvPtr()->m_szLoginID, Info.category);
	}

	m_cbFilterTag.GetWindowText(strTag);

	// �˻���E�޺� �ʱ�ȭ
	aTagList.RemoveAll();
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_cbFilterTag.SetWindowText(strTag);

	RefreshHost(false);
}

//void CHostView::OnBnClickedButtonHostNextRef()
//{
//	if(!m_pCall) return;
//
//	CWaitMessageBox wait;
//
//	POSITION pos = m_lsHost.FindIndex(m_lsHost.GetCount()-1);
//
//	BOOL bRet = CCopModule::GetObject()->GetHostData(m_pCall, m_lsHost, 500);
//
//	RefreshHostList(pos);
//
//	TraceLog(("RefreshHost end"));
//
//	if(!bRet)
//	{
//		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG007), MB_ICONINFORMATION);
//		return;
//	}
//
//	GetDlgItem(IDC_BUTTON_HOST_NEXT_REF)->EnableWindow(m_pCall->hasMore());
//
//	CString strMsg;
//	strMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG008), m_lscHost.GetItemCount());
//	UbcMessageBox(strMsg, MB_ICONINFORMATION);
//}


void CHostView::OnBnClickedButtonResizeToggle()
{
	TraceLog(("OnBnClickedButtonResizeToggle(%d)", m_toggle));
	ToggleWindow(++m_toggle);
	m_lscTimeLine.DeleteAllItems();
}

void CHostView::ToggleWindow(int on_off)
{
	CRect rcTimeLine;
	m_lscTimeLine.GetWindowRect(rcTimeLine);

	int current_time_width = rcTimeLine.Width();

	if(on_off %2 == 0){
		if(current_time_width == TIME_CLOSE_WIDTH){  // OnSize ���� ������Eȣ���ϴ� ���� ��������
			return;
		}
	}else{
		if(current_time_width == TIME_OPEN_WIDTH){
			return;
		}
	}
	TraceLog(("ToggleWindow(%d)", on_off));


	CRect rcWind;
	this->GetWindowRect(rcWind);

	CRect rcHost;
	m_lscHost.GetWindowRect(rcHost);

	CRect rcToggle;
	m_btnSizeToggle.GetWindowRect(rcToggle);


	// ������E�ʴ� ����E
	
	int y = rcHost.top - rcWind.top - 2; // btn ��Elist ��� y ��ǥ�� ������ �����ϴ�.
	int height = rcHost.Height();		 // btn ��Elist ��� height �� �����ϴ�.
	int offset = 2; // ��ü������ ������ 2 �̴�.
	int list_x = 2; // list �� x �� ������ 2 �̴�.
	int btn_width = rcToggle.Width(); // btn �� width �� �Һ��̴�.


	//���ϴ� ����,  btn �� x ��ǥ��,host list,time list �� width ���̴�. �̴� m_time_width �� ���� ���ǳ���.


	if(on_off %2==1){  // Open Case
		m_time_width = TIME_OPEN_WIDTH;
		m_Reposition.Move();
		//ShowItems(SW_SHOW);
	}else{				// Close Case
		m_time_width = TIME_CLOSE_WIDTH;
		//ShowItems(SW_HIDE);
	}

	int btn_x = rcWind.Width()-m_time_width-rcToggle.Width()-offset-offset;
	int time_x = rcWind.Width()-m_time_width-offset-offset;
	int list_width = btn_x-offset;

	m_btnSizeToggle.SetWindowPos(NULL,
								btn_x,
								y,
								btn_width,
								height,
								SWP_SHOWWINDOW);

	m_lscHost.SetWindowPos(NULL,
								list_x,
								y,
								list_width,
								height,
								SWP_SHOWWINDOW);
		
	m_lscTimeLine.SetWindowPos(NULL,
								time_x,
								y,
								m_time_width,
								height,
								SWP_SHOWWINDOW);
}


void CHostView::InvalidateItems()
{
	m_lscHost.Invalidate(false);
	m_lscTimeLine.Invalidate(false);
	m_btnSizeToggle.Invalidate(false);
}
void CHostView::OnNMClickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(m_time_width == TIME_CLOSE_WIDTH) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
	if(!pos) return;

	SHostInfo Info = m_lsHost.GetAt(pos);
	RefreshTimeLine(Info);
}

void CHostView::OnBnClickedButtonHostStart()
{
	CList<int> lsRow;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}
/*
	m_szMsg = LoadStringById(IDS_HOSTVIEW_MSG014);
	if(UbcMessageBox(m_szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}
*/
	CWaitMessageBox wait;

	int counter = 0;
	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow){
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost)	continue;

		m_lscHost.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);
		if(CCopModule::GetObject()->PowerOn(Info.siteId, Info.hostId))
		{
			// �����ѱⰡ ������ ��� �ٽ� ������ư�� ������ ���ϰ��ϱ� ���� ���������� �����Ѵ�.
			Info.operationalState = true;
			//m_btStart.EnableWindow(FALSE);
			counter++;
		}
		else
		{
		}	
	}
	if(counter > 0)	{
		UbcMessageBox(LoadStringById(IDS_HOSTDETAILDLG_MSG001));
	}else{
		UbcMessageBox(LoadStringById(IDS_HOSTDETAILDLG_MSG002));
	}
}

void CHostView::OnBnClickedButtonHostProperty()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG004), MB_ICONINFORMATION);
		return;
	}

	CPropertyDlg dlg(this);
	if(dlg.DoModal() != IDOK)
	{
		return;
	}

	CString name = dlg.getName();
	CString value = dlg.getValue();

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	WritePrivateProfileString("ROOT", "LAST_PROPERTY", name, szPath);

	int nModified =0;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscHost.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(name.Mid(0,3) == "GET"){
			TraceLog(("GetProperty(name=%s,value=%s)", name,value));
			if(!CCopModule::GetObjectA()->GetProperty((LPCTSTR)Info.siteId, (LPCTSTR)Info.hostId,(LPCTSTR)name))
			{
				m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(m_szMsg, MB_ICONERROR);
				continue;
			}
			nModified++;
		}else if(name.Mid(0,3) == "MSG"){
			TraceLog(("MSG(name=%s,value=%s)", name,value));

			if(!CCopModule::GetObjectA()->SetProperty((LPCTSTR)Info.siteId, (LPCTSTR)Info.hostId,(LPCTSTR)name,(LPCTSTR)value))
			{
				m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(m_szMsg, MB_ICONERROR);
				continue;
			}
			nModified++;
		}else{
			TraceLog(("SetProperty(name=%s,value=%s)", name,value));

			if(!CCopModule::GetObjectA()->SetProperty((LPCTSTR)Info.siteId, (LPCTSTR)Info.hostId,(LPCTSTR)name,(LPCTSTR)value))
			{
				m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(m_szMsg, MB_ICONERROR);
				continue;
			}
			nModified++;
		}
	}
	if(nModified > 0)
	{
		char debugStr[256];
		sprintf(debugStr,"%d %s", nModified, LoadStringById(IDS_HOSTVIEW_MSG006));
		UbcMessageBox(debugStr, MB_ICONINFORMATION);
	}
}

void CHostView::HostupdEx()
{
	CAuthCheckExDlg dlg;

	if( dlg.DoModal() == IDCANCEL ) return;

	int ret_add = CCopModule::GetObject()->CreateHost(dlg.m_strDatFilename);
	int ret_del = CCopModule::GetObject()->RemoveHost(dlg.m_strDatFilename);

	BackupHostList();

	if( RefreshHost(false) )
	{
		m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG010), ret_add);
		UbcMessageBox(m_szMsg, MB_ICONINFORMATION);
	}
}

void CHostView::OnRemoteLoginSettings()
{
	CRemoteLoginSettingsDlg dlg;
	dlg.DoModal();
}

void CHostView::OnBnClickedButtonConfig()
{
	CString contentsDownloadTime = "";
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscHost.GetItemCount(); nRow++)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);
		if(!Info.contentsDownloadTime.IsEmpty()) {
			contentsDownloadTime = Info.contentsDownloadTime;
		}
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG004), MB_ICONINFORMATION);
		return;
	}

	CDownloadTimeSetDlg dlg(this);
	dlg.SetContentsDownloadTime(contentsDownloadTime);

	if(dlg.DoModal() != IDOK)
	{
		return;
	}

	// 
	//SET.UBCVariables.Download.availabletime=00:00-11:11,

	CString name = dlg.getName();
	CString value = dlg.getValue();

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	WritePrivateProfileString("ROOT", "LAST_PROPERTY", name, szPath);

	int nModified =0;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscHost.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscHost.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(name.Mid(0,3) == "GET"){
			TraceLog(("GetProperty(name=%s,value=%s)", name,value));
			if(!CCopModule::GetObjectA()->GetProperty((LPCTSTR)Info.siteId, (LPCTSTR)Info.hostId,(LPCTSTR)name))
			{
				m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(m_szMsg, MB_ICONERROR);
				continue;
			}
			nModified++;
		}else if(name.Mid(0,3) == "MSG"){
			TraceLog(("MSG(name=%s,value=%s)", name,value));

			if(!CCopModule::GetObjectA()->SetProperty((LPCTSTR)Info.siteId, (LPCTSTR)Info.hostId,(LPCTSTR)name,(LPCTSTR)value))
			{
				m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(m_szMsg, MB_ICONERROR);
				continue;
			}
			nModified++;
		}else{
			TraceLog(("SetProperty(name=%s,value=%s)", name,value));

			if(!CCopModule::GetObjectA()->SetProperty((LPCTSTR)Info.siteId, (LPCTSTR)Info.hostId,(LPCTSTR)name,(LPCTSTR)value))
			{
				m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(m_szMsg, MB_ICONERROR);
				continue;
			}
			Info.contentsDownloadTime = value;
			CDirtyFlag aInfoDirty;
			aInfoDirty.SetDirty("contentsDownloadTime",true);
			if(!CCopModule::GetObjectA()->SetHost(Info, GetEnvPtr()->m_szLoginID,&aInfoDirty))
			{
				m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG005), Info.hostId);
				UbcMessageBox(m_szMsg, MB_ICONERROR);
				continue;
			}
			nModified++;
		}
	}
	if(nModified > 0)
	{
		char debugStr[256];
		sprintf(debugStr,"%d %s", nModified, LoadStringById(IDS_HOSTVIEW_MSG006));
		UbcMessageBox(debugStr, MB_ICONINFORMATION);
	}

}

void CHostView::OnBnClickedButtonConverterReboot()
{
	CList<int> lsRow;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	m_szMsg = LoadStringById(IDS_HOSTVIEW_MSG27);
	if(UbcMessageBox(m_szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow){
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost)	continue;

		m_lscHost.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);

		CString property_name = "GET.CONVERTER.0-0-0-0.REBOOT";
		CCopModule::GetObject()->GetProperty(std::string(Info.siteId), std::string(Info.hostId), std::string(property_name));

		TraceLog(("%s %s", property_name, Info.hostId));
	}
}

void CHostView::OnBnClickedButtonConverterUpdate()
{
	CList<int> lsRow;
	for(int nRow = m_lscHost.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lscHost.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	m_szMsg = LoadStringById(IDS_HOSTVIEW_MSG28);
	if(UbcMessageBox(m_szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES){
		return;
	}

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow){
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lscHost.GetItemData(nRow);
		if(!posHost)	continue;

		m_lscHost.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);
		
		CString property_name = "GET.CONVERTER.0-0-0-0.UPDATE";
		CCopModule::GetObject()->GetProperty(std::string(Info.siteId), std::string(Info.hostId), std::string(property_name));
		TraceLog(("%s %s", property_name, Info.hostId));
	}
}


void CHostView::OnHostlist1Networktest()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos) return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	CWaitMessageBox wait;

	if(!CCopModule::GetObject()->PingTest(std::string(Info.siteId), std::string(Info.hostId)))
	{
		m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG029), Info.hostId);
		UbcMessageBox(m_szMsg, MB_ICONERROR);
	}else{
		m_szMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG030), Info.hostId);
		UbcMessageBox(m_szMsg, MB_ICONERROR);
	}
}

void CHostView::OnBnClickedButtonMemo()
{
	HINSTANCE nRet = ShellExecute(NULL, NULL, "notepad.exe", " C:\\SQISoft\\memo.ini", "", SW_SHOWNORMAL);
}
