// WebPageView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "UBCManager.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "WebPageView.h"
#include "common\reposcontrol.h"
#include "DummyDoc.h"


// CWebPageView

IMPLEMENT_DYNCREATE(CWebPageView, CFormView)

CWebPageView::CWebPageView()
	: CFormView(CWebPageView::IDD)
,	m_Reposition(this)
{

}

CWebPageView::~CWebPageView()
{
}

void CWebPageView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IE_MAIN, m_ieMain);
}

BEGIN_MESSAGE_MAP(CWebPageView, CFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CWebPageView, CDialog)
	ON_EVENT(CWebPageView, IDC_IE_MAIN, 263, CWebPageView::WindowClosingIeMain, VTS_BOOL VTS_PBOOL)
END_EVENTSINK_MAP()


// CWebPageView 진단입니다.

#ifdef _DEBUG
void CWebPageView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CWebPageView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CWebPageView 메시지 처리기입니다.

int CWebPageView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}

void CWebPageView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	InitPosition(m_rcClient);

	CString strUrl = GetEnvPtr()->m_strDocParam;
	if(strUrl.IsEmpty()) strUrl = _T("about:blank");

	TraceLog(("URL : %s", strUrl));

    VARIANT vtUrl;
    vtUrl.vt = VT_BSTR;
    vtUrl.bstrVal = strUrl.AllocSysString();

	m_ieMain.Navigate2(&vtUrl, NULL, NULL, NULL, NULL);
}

void CWebPageView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CWebPageView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_ieMain, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}

void CWebPageView::WindowClosingIeMain(BOOL IsChildWindow, BOOL* Cancel)
{
	// 브라우져의 종료확인 메세지를 안나오게 한다.
	*Cancel = TRUE;
	GetParent()->PostMessage(WM_CLOSE);
}
