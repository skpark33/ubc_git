// MonitorDetailDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MonitorDetailDlg.h"
#include "ubccopcommon\MultiSelectHost.h"
#include "common\ubcdefine.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "PowerSettingByDay.h"

// CMonitorDetailDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMonitorDetailDlg, CDialog)

CMonitorDetailDlg::CMonitorDetailDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMonitorDetailDlg::IDD, pParent)
{
	m_bShutdown = false;
	m_strWeekShutdownTime.Empty();
	m_bInit = false;
	m_IsCreateMode = false;

}

CMonitorDetailDlg::~CMonitorDetailDlg()
{
}

void CMonitorDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_NAME, m_editMonitorName);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cbMonitorType);
	DDX_Control(pDX, IDC_EDIT_ID, m_editMonitorId);
	DDX_Control(pDX, IDC_EDIT_DESC, m_editDesc);
	DDX_Control(pDX, IDC_SLIDER_VALUE, m_sliderMonitorUseTime);
	DDX_Control(pDX, IDC_MONTHCALENDAR, m_ctrCalendar);
	DDX_Control(pDX, IDC_LIST_HOLIDAY, m_lscHoliday);
	DDX_Control(pDX, IDC_CHECK_SHUTDOWN, m_ckShutdown);
	DDX_Control(pDX, IDC_CHECK_POWERON, m_ckStartup);
	DDX_Control(pDX, IDC_TIME_SHUTDOWN, m_dtShutdown);
	DDX_Control(pDX, IDC_TIME_POWERON, m_dtStartup);
	DDX_Control(pDX, IDC_CHECK_WEEK0, m_ckWeek[0]);
	DDX_Control(pDX, IDC_CHECK_WEEK1, m_ckWeek[1]);
	DDX_Control(pDX, IDC_CHECK_WEEK2, m_ckWeek[2]);
	DDX_Control(pDX, IDC_CHECK_WEEK3, m_ckWeek[3]);
	DDX_Control(pDX, IDC_CHECK_WEEK4, m_ckWeek[4]);
	DDX_Control(pDX, IDC_CHECK_WEEK5, m_ckWeek[5]);
	DDX_Control(pDX, IDC_CHECK_WEEK6, m_ckWeek[6]);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_bnAddHoliday);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_bnDelHoliday);

	DDX_Control(pDX, IDC_BUTTON_USTATE, m_btUState);
	DDX_Control(pDX, IDC_STATIC_TSTATE, m_stcTState);
	DDX_Control(pDX, IDC_EDIT_HOSTID, m_editHostId);
	DDX_Control(pDX, ID_BUTTON_HOST, m_btHostSelect);
}


BEGIN_MESSAGE_MAP(CMonitorDetailDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CMonitorDetailDlg::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CMonitorDetailDlg::OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_BN_POWER_SET_BY_DAY, &CMonitorDetailDlg::OnBnClickedBnPowerSetByDay)
	ON_BN_CLICKED(IDOK, &CMonitorDetailDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMonitorDetailDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK_SHUTDOWN, &CMonitorDetailDlg::OnBnClickedCheckShutdown)
	ON_BN_CLICKED(IDC_CHECK_POWERON, &CMonitorDetailDlg::OnBnClickedCheckPoweron)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_USTATE, &CMonitorDetailDlg::OnBnClickedButtonUstate)
	ON_BN_CLICKED(ID_BUTTON_HOST, &CMonitorDetailDlg::OnBnClickedButtonHost)
	ON_EN_CHANGE(IDC_EDIT_ID, &CMonitorDetailDlg::OnEnChangeEditId)
END_MESSAGE_MAP()


// CMonitorDetailDlg 메시지 처리기입니다.

BOOL CMonitorDetailDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bInit = true;

	m_bnAddHoliday.LoadBitmap(IDB_BUTTON_NEXT2, RGB(255,255,255));
	m_bnDelHoliday.LoadBitmap(IDB_BUTTON_PREV2, RGB(255,255,255));

	m_lscHoliday.SetExtendedStyle(m_lscHoliday.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lscHoliday.InsertColumn(0, LoadStringById(IDS_HOLLIDAYDLG_LST001), LVCFMT_CENTER, 120);
	m_lscHoliday.InitHeader(IDB_LIST_HEADER);

	m_dtShutdown.SetFormat(" HH : mm");
	m_dtStartup.SetFormat(" HH : mm");

	CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbMonitorType, _T("SubMonitorType"));

	if(!m_IsCreateMode){

		m_editMonitorName.SetWindowText(m_MonitorInfo.monitorName);
		m_editMonitorId.SetWindowTextA(m_MonitorInfo.monitorId);

		CString entity;
		entity.Format("PM=*/Site=%s/Host=%s", m_MonitorInfo.siteId,m_MonitorInfo.hostId);

		m_editHostId.SetWindowText(entity);
		m_editDesc.SetWindowText(m_MonitorInfo.description);

		m_strShutdownTime = m_MonitorInfo.shutdownTime;
		m_strWeekShutdownTime = m_MonitorInfo.weekShutdownTime;

		m_sliderMonitorUseTime.SetRange(0,3000);
		m_sliderMonitorUseTime.SetPos(m_MonitorInfo.monitorUseTime);

		m_cbMonitorType.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_cbMonitorType, m_MonitorInfo.monitorType));

		if(m_MonitorInfo.adminState)
			m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
		else
			m_btUState.LoadBitmap(IDB_BUTTON_NOTUSE, RGB(12,12,12));

		m_bmTState.DeleteObject();
		if(m_MonitorInfo.operationalState){
			m_bmTState.LoadBitmap(IDB_ON);
			m_stcTState.SetBitmap(m_bmTState);
		}else{
			m_bmTState.LoadBitmap(IDB_OFF);
			m_stcTState.SetBitmap(m_bmTState);
		}

		if(m_MonitorInfo.startupTime.GetLength() != 5 || m_MonitorInfo.startupTime == "NOSET"){
			m_ckStartup.SetCheck(false);
		}else{
			CTime tmStartup(2000, 1, 1,  _ttoi(m_MonitorInfo.startupTime.Left(2)),  _ttoi(m_MonitorInfo.startupTime.Right(2)), 0);
			m_dtStartup.SetTime(&tmStartup);
			InitDateRange(m_dtStartup);
			m_ckStartup.SetCheck(true);
		}

		this->SetHolliday(m_MonitorInfo.holiday);
	}else{
		m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
		m_bmTState.LoadBitmap(IDB_OFF);
		m_stcTState.SetBitmap(m_bmTState);
		m_stcTState.EnableWindow(false);
		m_sliderMonitorUseTime.EnableWindow(false);
		m_ckStartup.SetCheck(false);
		m_editMonitorId.EnableWindow(true);
		m_btHostSelect.EnableWindow(true);
	}

	InitHolidayInfo();
	GetDlgItem(IDOK)->EnableWindow(IsAuth(_T("HMOD")));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMonitorDetailDlg::InitHolidayInfo()
{
	bool bShutdown = false;
	if(m_strShutdownTime != ""){
		int iStart = 0;
		CString szToken = m_strShutdownTime.Tokenize(EH_SEPARATOR, iStart);
		while(!szToken.IsEmpty()){
			if(szToken.Find(":") >= 0){
				int iPos = 0;
				CString szHour = szToken.Tokenize(":", iPos);
				CString szMin = szToken.Tokenize(":", iPos);
				CTime tmShut(2000, 1, 1, atoi(szHour), atoi(szMin), 0);
				m_dtShutdown.SetTime(&tmShut);
				InitDateRange(m_dtShutdown);
				bShutdown = true;
			}
			else if(szToken.Find(".") >=0){
				AddHolidayItem(szToken);
			}
			else if(szToken.GetLength() == 1){
				int n = atoi(szToken);
				if(0 <= n && n <= 6)
					m_ckWeek[n].SetCheck(true);
			}
			szToken = m_strShutdownTime.Tokenize(EH_SEPARATOR, iStart);
		}
	}

	m_ckShutdown.SetCheck(bShutdown);
	//if(!bShutdown){
	//	m_dtShutdown.EnableWindow(FALSE);
	//}
}


void CMonitorDetailDlg::OnBnClickedButtonAdd()
{
	SYSTEMTIME st;
	m_ctrCalendar.GetCurSel(&st);

	CString str;
	str.Format("%d.%02d", st.wMonth, st.wDay);

	for(int nRow = 0; nRow < m_lscHoliday.GetItemCount(); nRow++){
		CString szBuf = m_lscHoliday.GetItemText(nRow, 0);
		if(szBuf == str)	return;
	}

	AddHolidayItem(str);
}
void CMonitorDetailDlg::AddHolidayItem(CString str)
{
	int nRow = m_lscHoliday.GetItemCount();
	m_lscHoliday.InsertItem(nRow, str);
	m_lscHoliday.SortItems(CompareSite, (DWORD_PTR)&m_lscHoliday);
}

int CMonitorDetailDlg::CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	LVFINDINFO lvFind;
	lvFind.flags = LVFI_PARAM;

	CListCtrl* pListCtrl = (CListCtrl*)lParam;
	if(!pListCtrl) return 0;

	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	int nCol = 0;
	CString strItem1 = pListCtrl->GetItemText(nIndex1, nCol);
	CString strItem2 = pListCtrl->GetItemText(nIndex2, nCol);

	return strItem1.Compare(strItem2);
}

void CMonitorDetailDlg::SetHolliday(LPCTSTR szHolliday)
{
	m_strShutdownTime = szHolliday?szHolliday:"";
}

LPCTSTR CMonitorDetailDlg::GetHolliday(CString& szHolliday)
{
	return (szHolliday = m_strShutdownTime);
}

void CMonitorDetailDlg::OnBnClickedButtonDel()
{
	POSITION pos = m_lscHoliday.GetFirstSelectedItemPosition();
	while(pos){
		int nRow = m_lscHoliday.GetNextSelectedItem(pos);
		m_lscHoliday.DeleteItem(nRow);
	}
}

void CMonitorDetailDlg::OnBnClickedBnPowerSetByDay()
{
	CPowerSettingByDay dlg;
	dlg.m_strWeekShutdownTime = m_strWeekShutdownTime;
	if(dlg.DoModal() != IDOK) return;

	m_strWeekShutdownTime = dlg.m_strWeekShutdownTime;
}
void CMonitorDetailDlg::OnBnClickedCheckShutdown()
{
	m_dtShutdown.EnableWindow(m_ckShutdown.GetCheck());
}

void CMonitorDetailDlg::OnBnClickedCheckPoweron()
{
	m_dtStartup.EnableWindow(m_ckStartup.GetCheck());
}

void CMonitorDetailDlg::OnBnClickedButtonUstate()
{
	m_MonitorInfo.adminState = !m_MonitorInfo.adminState;
	if(m_MonitorInfo.adminState)
		m_btUState.LoadBitmap(IDB_BUTTON_USE, RGB(12,12,12));
	else
		m_btUState.LoadBitmap(IDB_BUTTON_NOTUSE, RGB(12,12,12));
}

void CMonitorDetailDlg::OnBnClickedOk()
{
	UpdateData();

	if(m_cbMonitorType.GetCount() > 0 && m_cbMonitorType.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR011));
		return;
	}

	CString buf;
	this->m_editMonitorId.GetWindowText(buf);
	if(buf.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_MONITORVIEW_STR023));
		return;
	}
	this->m_MonitorInfo.monitorId = buf;

	this->m_editHostId.GetWindowText(buf);
	if(buf.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG002));
		return;
	}
	cciEntity aHostEntity(buf);
	if(aHostEntity.length() != 3)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG002));
		return;
	}

	this->m_MonitorInfo.siteId = aHostEntity.getClassValue(1);
	this->m_MonitorInfo.hostId = aHostEntity.getClassValue(2);

	m_MonitorInfo.monitorType = (long)m_cbMonitorType.GetItemData(m_cbMonitorType.GetCurSel());

	this->m_editDesc.GetWindowText(m_MonitorInfo.description);
	this->m_editMonitorName.GetWindowText(m_MonitorInfo.monitorName);
	m_MonitorInfo.monitorUseTime = this->m_sliderMonitorUseTime.GetPos();

	m_bStartup = m_ckStartup.GetCheck();
	if(m_ckStartup.GetCheck()){
		m_dtStartup.GetTime(m_tmStartup);
		m_strStartupTime.Format("%02d:%02d", m_tmStartup.GetHour(), m_tmStartup.GetMinute());
	}else{
		m_strStartupTime = "NOSET";
	}
	m_MonitorInfo.startupTime = m_strStartupTime;

	m_bShutdown = m_ckShutdown.GetCheck();
	if(m_ckShutdown.GetCheck()){
		m_dtShutdown.GetTime(m_tmShutdown);
		m_strShutdownTime.Format("%02d:%02d", m_tmShutdown.GetHour(), m_tmShutdown.GetMinute());
	}else{
		m_strShutdownTime = "NOSET";
	}

	for(int nRow = 0; nRow < m_lscHoliday.GetItemCount(); nRow++){
		CString szBuf = m_lscHoliday.GetItemText(nRow, 0);
		m_strShutdownTime += EH_SEPARATOR;
		m_strShutdownTime += szBuf;
	}

	for(int i = 0; i < 7; i++){
		if(m_ckWeek[i].GetCheck()){
			m_strShutdownTime += EH_SEPARATOR;
			m_strShutdownTime += ToString(i);
		}
	}
	m_MonitorInfo.shutdownTime = m_strShutdownTime;

	m_MonitorInfo.weekShutdownTime = m_strWeekShutdownTime;
	this->GetHolliday(m_MonitorInfo.holiday);


	OnOK();
}

void CMonitorDetailDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
void CMonitorDetailDlg::SetInfo(SMonitorInfo& info)
{
	MonitorInfoList lsMonitor;
	CCopModule::GetObject()->GetMonitorList(info.siteId, info.monitorId, NULL, true, false,lsMonitor);
	POSITION pos = lsMonitor.GetHeadPosition();
	if(pos){
		m_MonitorInfo = lsMonitor.GetNext(pos);
	}else{
		m_MonitorInfo = info;
	}
}

void CMonitorDetailDlg::GetInfo(SMonitorInfo& info)
{
	info = m_MonitorInfo;
}


void CMonitorDetailDlg::OnDestroy()
{
	CDialog::OnDestroy();

	m_bInit = false;
	m_bmTState.DeleteObject();
}

void CMonitorDetailDlg::OnClose()
{
	m_bInit = false;
	m_bmTState.DeleteObject();

	CDialog::OnClose();
}

void CMonitorDetailDlg::OnBnClickedButtonHost()
{
	CMultiSelectHost dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	//dlg.SetTitle(LoadStringById(IDS_PLANOVIEW_STR007));

	if(dlg.DoModal() != IDOK) return;

	CString hostId ;
	for(int i=0; i<dlg.m_astrSelHostList.GetCount() ;i++)
	{
		hostId = dlg.m_astrSelHostList[i];
		TraceLog(("Selected HostId = %s", hostId));
		break;
	}
	CString siteId ;
	for(int i=0; i<dlg.m_astrSelSiteIdList.GetCount() ;i++)
	{
		siteId = dlg.m_astrSelSiteIdList[i];
		TraceLog(("Selected SiteId = %s", siteId));
		break;
	}

	if(hostId.IsEmpty() || siteId.IsEmpty()){
		UbcMessageBox(IDS_HOSTVIEW_MSG002);
		return;
	}

	CString entity;
	entity.Format("PM=*/Site=%s/Host=%s", siteId,hostId);

	this->m_editHostId.SetWindowTextA(entity);
	TraceLog(("%s host selected", entity));
}

void CMonitorDetailDlg::OnEnChangeEditId()
{
	CString buf, retval;
	m_editMonitorId.GetWindowText(buf);
	int llen = buf.GetLength();

	const char* special = "*,[]{}()!~`@#$%^&-+=/\\\"\':;?.| \t\n";
	int rlen = strlen(special);

	int counter=0;
	for(int i=0;i<llen;i++){
		int aCh = buf.GetAt(i);
		bool invalid = false;
		for(int j=0;j<rlen;j++){
			if(aCh == special[j]){
				TraceLog(("%s has special char %c", buf, aCh));
				invalid = true;
				counter++;
				break;
			}
		}
		if(invalid == false) {
			retval += (TCHAR)aCh;
		}
	}
	if(counter>0){
		m_editMonitorId.SetWindowText(retval);
	}
}
