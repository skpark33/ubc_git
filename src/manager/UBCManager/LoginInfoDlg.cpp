// LoginInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LoginInfoDlg.h"

// CLoginInfoDlg dialog
IMPLEMENT_DYNAMIC(CLoginInfoDlg, CDialog)

CLoginInfoDlg::CLoginInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoginInfoDlg::IDD, pParent)
{
}

CLoginInfoDlg::~CLoginInfoDlg()
{
}

void CLoginInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_STATIC_SITE, m_szSite);
	DDX_Text(pDX, IDC_STATIC_USER, m_szUser);
}

BEGIN_MESSAGE_MAP(CLoginInfoDlg, CDialog)
	ON_WM_DESTROY()
END_MESSAGE_MAP()

// CLoginInfoDlg message handlers

BOOL CLoginInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginInfoDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}
