#pragma once
#include "resource.h"
#include "afxwin.h"
#include "common\hoverbutton.h"
#include "common\utblistctrlex.h"
#include "common/libCLITransfer/CLITransfer.h"
#include "UBCCopCommon\CopModule.h"
#include "afxcmn.h"

// CWOLDLG 대화 상자입니다.

class CWOLDLG : public CDialog, CCLITransfer::IResultHandler, ICallProgressHandler
{
	DECLARE_DYNAMIC(CWOLDLG)

public:

	enum { eCheck,eErrCode,eResult, eHostVNCStat, eMonitorStat, eMacAddress, 
		   eSiteName, eHostName, eHostID,eHostIP,eRelayHostName,eRelayHostIP,eRelayHostID,
		   eHostEnd };
	enum { eMonitorOn, eMonitorOff, eMonitorUnknown, eVncOn, eVncOff, eDummy, eSucceed,eCommandFail,eConnectFail,eNA,eInit };

	CWOLDLG(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CWOLDLG();

	void Clear() { if(m_CliTransfer){ delete m_CliTransfer; m_CliTransfer=0;} }

	// IResultHandler
	void CLIResultEvent(CCLITransfer::ITEM item);
	void CLIBeforeEvent(CCLITransfer::ITEM item, LPCTSTR cmd);
	void CLIFinishEvent();

	// ICallProcessHandler
	void ProgressEvent(int max, int current); 
	void ResultComplete(bool retval);

	static UINT ThreadGetRelayHostInfo(LPVOID pParam);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_WOL_DLG };

	// input
	CList<SHostInfo> m_lsSelHost;

protected:
	void InitList();
	void RefreshHostList(bool setRelayInfo=false);
	void UpdateListRow(int nRow, SHostInfo* pInfo);

	void _setRelayInfo(SHostInfo* pInfo);
	BOOL _getRelayInfo();
	int	_runCLI(const char* cmd, int ip_column_index=eHostIP);

	void _getWOLPort(CString& portStr);
	void _setWOLPort(CString portStr);

	void _setErrorCode(int nRow, LPCTSTR tooltipText, int errCode);

	CCLITransfer*	m_CliTransfer;
	CImageList		m_ilHostList;
	CUTBListCtrlEx	m_lcList;
	CString			m_szHostColum[eHostEnd];
	RelayHostInfoMap m_relayHostMap;
	int				m_progress_counter;
	bool			m_hasRelayInfo;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBnPoweron();
	afx_msg void OnBnClickedBnPoweroff();
	afx_msg void OnBnClickedBnRefresh();
	afx_msg void OnBnClickedBnFilesave();
	afx_msg void OnBnClickedCancel();

	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();

	CHoverButton m_btPowerOn;
	CHoverButton m_btPowerOff;
	CHoverButton m_btRefresh;
	CHoverButton m_btSave;
	CHoverButton m_btExit;
	CButton m_check_except;
	CEdit m_edit_wolPort;
	afx_msg void OnBnClickedCheckExcept();
	CProgressCtrl m_Progress;
	CStatic m_stCounter;
	CStatic m_stProgress;
	bool			m_bIsPlaying;

};
