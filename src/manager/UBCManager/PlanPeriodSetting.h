#pragma once

#include "afxdtctl.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "SubBroadcastPlan.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\EditEx.h"

// CPlanPeriodSetting 대화 상자입니다.

class CPlanPeriodSetting : public CSubBroadcastPlan
{
	DECLARE_DYNAMIC(CPlanPeriodSetting)

public:
	CPlanPeriodSetting(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPlanPeriodSetting();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PLAN_PERIOD_SETTING };
	enum { eCheck, eExceptDay, eMaxCol };

	bool IsModified();
	void UpdateInfo();
	bool InputErrorCheck(bool bMsg);
	void RefreshInfo();

protected:
	CDateTimeCtrl	m_dtStart;
	CDateTimeCtrl	m_dtEnd;
	CButton			m_kbWeek[7];
	CMonthCalCtrl	m_calExceptDay;
	CHoverButton	m_bnExceptDayAdd;
	CHoverButton	m_bnExceptDayDel;
	CUTBListCtrlEx	m_lcExceptDay;
	CUTBListCtrlEx	m_lcView;

	CDateTimeCtrl	m_dtDownloadTime;
	CButton			m_kbImmediate;
	CEditEx			m_ebRetryCnt;
	CSpinButtonCtrl m_spRetryCnt;
	CEditEx			m_ebRetryInterval;
	CSpinButtonCtrl m_spRetryInterval;

	SBroadcastingPlanInfo m_oldInfo;

	CString			m_szColum[eMaxCol];
	void			InitExceptDayList();
	void			RefreshExceptDayList();

	void			InitViewList();
	void			RefreshViewList();

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnDtnDatetimechangeDtStart(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDtnDatetimechangeDtEnd(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBnExceptdayAdd();
	afx_msg void OnBnClickedBnExceptdayDel();
	afx_msg void OnBnClickedKbWeek(UINT nID);
	afx_msg void OnNMDblclkLcExceptday(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedKbImmediate();
};
