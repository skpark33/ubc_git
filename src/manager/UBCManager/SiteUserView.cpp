// SiteUserView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Enviroment.h"
#include "SiteUserView.h"
#include "UserInfoDlg.h"
#include "SiteDelDlg.h"
#include "ubccopcommon\SiteInfoDlg.h"
#include "common\libCommon\ubcSite.h"
#include "HostExcelSave.h"
#include "common\libscratch\scratchUtil.h"

//===================================
// 조직 처리 권한
// super admin CRUD
// manager     RU
// user        R
//===================================
// User 처리 권한
// super admin CRUD
// manager     CRUD (권한(manager/user))
// user        RU (자신의 id만, 권한x, 롤x)
//===================================

// CSiteUserView

#define MINUS_IMG_IDX	1
#define LINK_IMG_IDX	1
#define UNLINK_IMG_IDX	2
#define REPEAT_IMG_IDX	3

IMPLEMENT_DYNCREATE(CSiteUserView, CFormView)

CSiteUserView::CSiteUserView()
	: CFormView(CSiteUserView::IDD)
,	m_Reposition(this)
{
	m_pDragImage = NULL; // 드래그시 생성된 이미지 사용
	m_hDragItem = NULL;  // 드래그시 처음 선택된 아이템 핸들 기억용
	m_bSiteParentChanged = false;
}

CSiteUserView::~CSiteUserView()
{
}

void CSiteUserView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_SITE, m_treeSite);
	DDX_Control(pDX, IDC_TAB_MAIN, m_tabMain);
	DDX_Control(pDX, IDC_BUTTON_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_bnDel);
	DDX_Control(pDX, IDC_BUTTON_MOD, m_bnMod);
	DDX_Control(pDX, IDC_LC_SITE, m_lcSite);
	DDX_Control(pDX, IDC_LC_USER, m_lcUser);
	DDX_Control(pDX, IDC_KB_INCLUDE_CHILD, m_kbIncludeChild);
	DDX_Control(pDX, IDC_BN_TO_EXCEL, m_btnExcelSave);
	DDX_Control(pDX, IDC_TREE_EDIT_CHECK, m_checkTreeEdit);
	DDX_Control(pDX, IDC_BUTTON_SAVE, m_btSaveTree);
	DDX_Control(pDX, IDC_BUTTON_CANCEL, m_btCancelTree);
}

BEGIN_MESSAGE_MAP(CSiteUserView, CFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CSiteUserView::OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CSiteUserView::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CSiteUserView::OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_MOD, &CSiteUserView::OnBnClickedButtonMod)
	ON_NOTIFY(NM_DBLCLK, IDC_LC_SITE, &CSiteUserView::OnNMDblclkLcSite)
	ON_NOTIFY(NM_DBLCLK, IDC_LC_USER, &CSiteUserView::OnNMDblclkLcUser)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_KB_INCLUDE_CHILD, &CSiteUserView::OnBnClickedKbIncludeChild)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_MAIN, &CSiteUserView::OnTcnSelchangeTabMain)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_SITE, &CSiteUserView::OnTvnSelchangedTreeSite)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL, &CSiteUserView::OnBnClickedBnToExcel)
	ON_NOTIFY(TVN_BEGINDRAG, IDC_TREE_SITE, &CSiteUserView::OnTvnBegindragTreeSite)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_TREE_EDIT_CHECK, &CSiteUserView::OnBnClickedTreeEditCheck)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CSiteUserView::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CSiteUserView::OnBnClickedButtonCancel)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CSiteUserView 진단입니다.

#ifdef _DEBUG
void CSiteUserView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSiteUserView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSiteUserView 메시지 처리기입니다.

int CSiteUserView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}

void CSiteUserView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CSiteUserView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CWaitMessageBox wait;

	// 컨트롤 초기화
	m_bnAdd.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_bnDel.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_bnMod.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_bnRefresh.LoadBitmap(IDB_BUTTON_REFRESH2, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));
	if(!scratchUtil::getInstance()->isGScert())
		m_btnExcelSave.ShowWindow(SW_HIDE);


	InitPosition(m_rcClient);

	m_tabMain.InsertItem(0, LoadStringById(IDS_SITEUSERVIEW_STR001));
	m_tabMain.InsertItem(1, LoadStringById(IDS_SITEUSERVIEW_STR002));

	m_tabMain.SetCurSel(GetEnvPtr()->m_strDocParam.CompareNoCase(_T("Site")) == 0 ? 0 : 1);

	m_lcSite.EnableWindow(m_tabMain.GetCurSel() == 0);
	m_lcSite.ShowWindow(m_tabMain.GetCurSel() == 0);

	m_lcUser.EnableWindow(m_tabMain.GetCurSel() == 1);
	m_lcUser.ShowWindow(m_tabMain.GetCurSel() == 1);

	//m_kbIncludeChild.EnableWindow(false);  //skpark 2012.11.10 bug 가 많아서 사용하지 않는다.
	//m_kbIncludeChild.ShowWindow(false);  //skpark 2012.11.10 bug 가 많아서 사용하지 않는다.
	// 데이터 초기화
	CString strWhere;

	m_lsRole.RemoveAll();
	CCopModule::GetObject()->GetRoleList(m_lsRole);

	m_lsChildSite.RemoveAll();
	m_mapChildSite.RemoveAll();

	CCopModule::GetObject()->GetChildSite(GetEnvPtr()->m_strCustomer, GetEnvPtr()->m_strCustomer, m_lsChildSite, "order by id_path" );

	//2015.2.13  in query 가 너무 길어지면 에러가 나므로 10개씩 잘라서 처리한다.

	CStringArray childSiteList;
	POSITION pos = m_lsChildSite.GetHeadPosition();
	while(pos)
	{
		SChildSiteInfo& info = m_lsChildSite.GetNext(pos);

		if(info.siteId == GetEnvPtr()->m_szSite){
			m_mySiteInfo = info;
		}

		childSiteList.Add(info.siteId);
	}

	//skpark 2013.11.18 만약 Admin 급이 아니라면 (즉, Manager 급과 User급) 은 자신이 속한 Site 및 하위 사이트,
	// 그리고, 직계 상위 사이트만 보이도록 한다.
	if(CCopModule::eSiteAdmin != GetEnvPtr()->m_Authority && !m_mySiteInfo.siteId.IsEmpty()){
		childSiteList.RemoveAll();
		POSITION pos = m_lsChildSite.GetHeadPosition();
		while(pos)
		{
			SChildSiteInfo& info = m_lsChildSite.GetNext(pos);
			if(_isMyLine(info)){
				childSiteList.Add(info.siteId);
			}
		}
	}


	m_lsSite.RemoveAll();
	SSiteInfo siteInfo;
	CString strChildSiteList;
	for(int i=0,j=0; i<childSiteList.GetCount() ;i++,j++){
		CString strTmp;
		strTmp.Format("'%s'", childSiteList[i]);
		strChildSiteList += (strChildSiteList.IsEmpty() ? "" : ",") + strTmp;
		if(j==9){
			strWhere.Format(_T("siteId in (%s) and franchizeType = '%s'")
						  , strChildSiteList
						  , GetEnvPtr()->m_strCustomer
						  );
			TraceLog(("GetSite(%s)", strWhere));
			CCopModule::GetObject()->GetSite(siteInfo, m_lsSite, strWhere, FALSE);
			j=0;
			strChildSiteList.Empty();
		}
	}
	if(!strChildSiteList.IsEmpty()){
		strWhere.Format(_T("siteId in (%s) and franchizeType = '%s'")
					  , strChildSiteList
					  , GetEnvPtr()->m_strCustomer
					  );
		TraceLog(("GetSite(%s)", strWhere));
		CCopModule::GetObject()->GetSite(siteInfo, m_lsSite, strWhere, FALSE);
		strChildSiteList.Empty();
	}


/*
	CString strChildSiteList;
	POSITION pos = m_lsChildSite.GetHeadPosition();
	while(pos)
	{
		SChildSiteInfo& info = m_lsChildSite.GetNext(pos);

		if(info.siteId == GetEnvPtr()->m_szSite){
			m_mySiteInfo = info;
		}

		CString strTmp;
		strTmp.Format("'%s'", info.siteId);

		strChildSiteList += (strChildSiteList.IsEmpty() ? "" : ",") + strTmp;
	}

	//skpark 2013.11.18 만약 Admin 급이 아니라면 (즉, Manager 급과 User급) 은 자신이 속한 Site 및 하위 사이트,
	// 그리고, 직계 상위 사이트만 보이도록 한다.
	if(CCopModule::eSiteAdmin != GetEnvPtr()->m_Authority && !m_mySiteInfo.siteId.IsEmpty()){
		strChildSiteList = "";
		POSITION pos = m_lsChildSite.GetHeadPosition();
		while(pos)
		{
			SChildSiteInfo& info = m_lsChildSite.GetNext(pos);
			if(_isMyLine(info)){
				CString strTmp;
				strTmp.Format("'%s'", info.siteId);
				strChildSiteList += (strChildSiteList.IsEmpty() ? "" : ",") + strTmp;
			}
		}
	}

	m_lsSite.RemoveAll();
	SSiteInfo siteInfo;
	//strWhere.Format(_T("siteId in (select siteId from dbo.GetChildSite('%s','%s')) and franchizeType = '%s'")
	strWhere.Format(_T("siteId in (%s) and franchizeType = '%s'")
				  , strChildSiteList
				  , GetEnvPtr()->m_strCustomer
				  );
	CCopModule::GetObject()->GetSite(siteInfo, m_lsSite, strWhere, FALSE);
*/


	m_lsUser.RemoveAll();
	SUserInfo userInfo;
	CCopModule::GetObject()->GetUser(userInfo, m_lsUser, FALSE);

	if(m_ilTreeImage.Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 1))	// add an images list with appropriate background (transparent) color
	{
		CBitmap bmpContentsList1;
		bmpContentsList1.LoadBitmap(IDB_OVERLAP_REPEAT16);

		CBitmap bmpContentsList2;
		bmpContentsList2.LoadBitmap(IDB_OVERLAP_MINUS16);

		CBitmap bmpNormalNode;
		bmpNormalNode.LoadBitmap(IDB_OVERLAP_MINUS16);

		CBitmap bmpRepeat;
		bmpRepeat.LoadBitmap(IDB_OVERLAP_REPEAT16);

		m_ilTreeImage.Add(&bmpContentsList1, RGB(255,255,255));
		m_ilTreeImage.Add(&bmpContentsList2, RGB(255,255,255));

		int nOverlayIndex = m_ilTreeImage.GetImageCount();

		m_ilTreeImage.Add(&bmpNormalNode, RGB(255,255,255));
		m_ilTreeImage.Add(&bmpRepeat, RGB(255,255,255));

		m_ilTreeImage.SetOverlayImage(nOverlayIndex  , MINUS_IMG_IDX ); // minus
		m_ilTreeImage.SetOverlayImage(nOverlayIndex+1, REPEAT_IMG_IDX); // repeat

		m_treeSite.SetImageList(&m_ilTreeImage, TVSIL_NORMAL);
	}

	RefreshTree();

	InitSiteList();
	RefreshSiteList();

	InitUserList();
	RefreshUserList();

	InitButtonState();

	SetScrollSizes(MM_TEXT, CSize(1,1));
}

bool CSiteUserView::_isMyLine(SChildSiteInfo& info)
{
	//TraceLog(("_isMyLine(m_mySiteInfo=%s)", m_mySiteInfo.id_path));

	// 자기자신
	if(info.siteId == m_mySiteInfo.siteId) {
		return true;
	}

	// 자기 자식
	int len = m_mySiteInfo.id_path.GetLength();
	if(info.id_path.GetLength() > len ){
		CString my_path = m_mySiteInfo.id_path + "/";
		CString your_path = info.id_path.Mid(0,len+1);
		if(my_path == your_path){
			return true;
		}
	}

	// 자기 조상들

	int pos=0;
	CString token = m_mySiteInfo.id_path.Tokenize("/",pos);
	while(token != ""){
		if(info.siteId == token) {
			return true;
		}
		token = m_mySiteInfo.id_path.Tokenize("/",pos);
	}

	return false;
}

void CSiteUserView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_treeSite , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_MOVE);
	m_Reposition.AddControl(&m_tabMain  , REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	//m_Reposition.AddControl(&m_bnRefresh, REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	//m_Reposition.AddControl(&m_bnAdd    , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	//m_Reposition.AddControl(&m_bnDel    , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	//m_Reposition.AddControl(&m_bnMod    , REPOS_FIX, REPOS_FIX, REPOS_FIX, REPOS_FIX);
	m_Reposition.AddControl(&m_lcSite   , REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_lcUser   , REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_checkTreeEdit   , REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_Reposition.AddControl(&m_btSaveTree   , REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
	m_Reposition.AddControl(&m_btCancelTree   , REPOS_FIX, REPOS_MOVE, REPOS_FIX, REPOS_MOVE);
}

void CSiteUserView::RefreshTree()
{
	TraceLog(("RefreshTree start"));

	m_treeSite.SetRedraw(FALSE);

	m_treeSite.DeleteAllItems();

	HTREEITEM itemParent = NULL;
	HTREEITEM itemBefore = NULL;
	SChildSiteInfo infoBefore;

	POSITION pos = m_lsChildSite.GetHeadPosition();
	while(pos)
	{
		SChildSiteInfo& info = m_lsChildSite.GetNext(pos);
		TraceLog(("idPath=[%s]", info.id_path));

		if(info.id_path.IsEmpty()){
			//TraceLog(("skpark 111 idPath is empty , will be skipped"));
			//TraceLog(("[siteId=%s,siteName=%s]", info.siteId, info.siteName));
			continue;
		}
		//skpark 2013.11.18 만약 Admin 급이 아니라면 (즉, Manager 급과 User급) 은 자신이 속한 Site 및 하위 사이트,
		// 그리고, 직계 상위 사이트만 보이도록 한다.
		if(CCopModule::eSiteAdmin != GetEnvPtr()->m_Authority && !m_mySiteInfo.siteId.IsEmpty()){
			if(!_isMyLine(info)){
				TraceLog(("%s is not myLine", info.id_path));
				continue;
			}
		}

		if(itemBefore)
		{
			int nLevel = (infoBefore.lvl-info.lvl);

			if(nLevel < 0)
			{
				itemParent = itemBefore;
			}
			else if(infoBefore.lvl > info.lvl)
			{
				for(int i=0; i<nLevel ;i++)
				{
					itemParent = m_treeSite.GetParentItem(itemParent);
				}
			}
		}

		HTREEITEM item = m_treeSite.InsertItem(info.siteName, itemParent, TVI_SORT);
		m_treeSite.SetItemData(item, (DWORD_PTR)&info);

		SetSiteInfoFromMap(info.siteId, &info);

		itemBefore = item;
		infoBefore = info;
	}

	m_treeSite.Expand(m_treeSite.GetRootItem(), TVE_EXPAND);
	m_treeSite.SelectItem(m_treeSite.GetRootItem());

	m_treeSite.SetRedraw(TRUE);

	TraceLog(("RefreshTree end"));


}

void CSiteUserView::InitSiteList()
{
	m_lcSite.SetExtendedStyle(m_lcSite.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_INFOTIP);

	m_lcSite.InsertColumn(eSiteCheck  , _T("")                             , LVCFMT_LEFT, 22);
	m_lcSite.InsertColumn(eSiteName   , LoadStringById(IDS_SITEVIEW_LST002), LVCFMT_LEFT, 100);
	m_lcSite.InsertColumn(eSiteID     , LoadStringById(IDS_SITEVIEW_LST001), LVCFMT_LEFT, 80);
	m_lcSite.InsertColumn(eMgrID     , "ProcessID", LVCFMT_LEFT, 60);   // 2012.12.5 skpark insert
	m_lcSite.InsertColumn(eSiteTel    , LoadStringById(IDS_SITEVIEW_LST006), LVCFMT_LEFT, 100);
	m_lcSite.InsertColumn(eSiteAddr   , LoadStringById(IDS_SITEVIEW_LST007), LVCFMT_LEFT, 100);
	m_lcSite.InsertColumn(eSiteMan    , LoadStringById(IDS_SITEVIEW_LST008), LVCFMT_LEFT, 60);
	m_lcSite.InsertColumn(eSiteManTel , LoadStringById(IDS_SITEVIEW_LST009), LVCFMT_LEFT, 100);
	m_lcSite.InsertColumn(eSiteRemark , LoadStringById(IDS_SITEVIEW_LST010), LVCFMT_LEFT, 100);

	m_lcSite.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcSite.LoadColumnState("SITE-LIST", szPath);
}

SChildSiteInfo* CSiteUserView::GetSiteInfoFromMap(CString strSiteId)
{
	return (SChildSiteInfo*)m_mapChildSite[strSiteId.MakeUpper()];
}

void CSiteUserView::SetSiteInfoFromMap(CString strSiteId, SChildSiteInfo* pSiteInfo)
{
	m_mapChildSite[strSiteId.MakeUpper()] = pSiteInfo;
}

void CSiteUserView::UpdateSiteRow(int nRow, SSiteInfo& info)
{
	m_lcSite.SetItemText(nRow, eSiteName   , info.siteName);
	m_lcSite.SetItemText(nRow, eSiteID     , info.siteId);
	m_lcSite.SetItemText(nRow, eMgrID     , info.mgrId);  // 2012.12.5 skpark insert
	m_lcSite.SetItemText(nRow, eSiteTel    , info.phoneNo1);
	m_lcSite.SetItemText(nRow, eSiteAddr   , info.comment1);
	m_lcSite.SetItemText(nRow, eSiteMan    , info.ceoName);
	m_lcSite.SetItemText(nRow, eSiteManTel , info.phoneNo2);
	m_lcSite.SetItemText(nRow, eSiteRemark , info.comment2);

	if(m_kbIncludeChild.GetCheck())
	{
		SChildSiteInfo* pChildSiteInfo = GetSiteInfoFromMap(info.siteId);

		if(pChildSiteInfo)
		{
			m_lcSite.SetItemText(nRow, eSiteName , pChildSiteInfo->name_path);
		}
	}

	if(info.bReadOnly) m_lcSite.SetRowColor(nRow, RGB(255,255,255), RGB(128,128,128));
}

void CSiteUserView::RefreshSiteList(bool reget/*=false*/)
{
	TraceLog(("RefreshSiteList(%d)", reget));
	if(reget) {
		/*
		m_lsSite.RemoveAll();
		SSiteInfo siteInfo;
		//strWhere.Format(_T("siteId in (select siteId from dbo.GetChildSite('%s','%s')) and franchizeType = '%s'")
		CString strWhere;
		strWhere.Format(_T("siteId in (%s) and franchizeType = '%s'")
					  , strChildSiteList
					  , GetEnvPtr()->m_strCustomer
					  );
		CCopModule::GetObject()->GetSite(siteInfo, m_lsSite, strWhere, FALSE);
		*/
	}

	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(m_treeSite.GetSelectedItem());

	m_lcSite.SetRedraw(FALSE);
	m_lcSite.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsSite.GetHeadPosition();

	while(pos)
	{
		//TraceLog(("pos is not null"));
		POSITION posOld = pos;
		SSiteInfo& info = m_lsSite.GetNext(pos);

		SChildSiteInfo* pChildSiteInfo = GetSiteInfoFromMap(info.siteId);

		if(pSelectedSiteInfo)
		{
			//TraceLog(("SChildSiteInfo(%s)", info.siteId));
			if(m_kbIncludeChild.GetCheck())
			{
				//TraceLog(("m_kbIncludeChild(%s)", info.siteId));
				if((pChildSiteInfo->id_path+_T("/")).Find(pSelectedSiteInfo->id_path+_T("/")) < 0)
				{
					//TraceLog(("pChildSiteInfo->id_path(%s) is not in pSelectedSiteInfo->id_path(%s)", 
					//	pChildSiteInfo->id_path,pSelectedSiteInfo->id_path));
					continue;
				}
			}
			else
			{
				//TraceLog(("not m_kbIncludeChild(%s)", info.siteId));
				if(pSelectedSiteInfo->siteId != pChildSiteInfo->siteId ) { // 자기자신은 나와야!
					if(pSelectedSiteInfo->siteId != pChildSiteInfo->parentId )
					{
						continue;
					}
				}
			}
		}

		m_lcSite.InsertItem(nRow, "");
		
		UpdateSiteRow(nRow, info);

		m_lcSite.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lcSite.GetCurSortCol();
	if(-1 != nCol)
		m_lcSite.SortList(nCol, m_lcSite.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcSite);
	else
		m_lcSite.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcSite);

	m_lcSite.SetRedraw(TRUE);
}

void CSiteUserView::InitUserList()
{
	m_lcUser.SetExtendedStyle(m_lcUser.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_INFOTIP);

	m_lcUser.InsertColumn(eUserCheck, _T("")                             , LVCFMT_LEFT, 22);
	m_lcUser.InsertColumn(eUserSite , LoadStringById(IDS_USERVIEW_LST001), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eUserID   , LoadStringById(IDS_USERVIEW_LST002), LVCFMT_LEFT, 100);
//	m_lcUser.InsertColumn(eUserPW   , LoadStringById(IDS_USERVIEW_LST004), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eUserName , LoadStringById(IDS_USERVIEW_LST005), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eUserPerm , LoadStringById(IDS_USERVIEW_LST003), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eRole     , LoadStringById(IDS_USERVIEW_LST013), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eValidDate, LoadStringById(IDS_USERVIEW_LST014), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eUserTel  , LoadStringById(IDS_USERVIEW_LST007), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eUserMail , LoadStringById(IDS_USERVIEW_LST008), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eUseAlarm , LoadStringById(IDS_USERVIEW_LST009), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eAlarmList, LoadStringById(IDS_USERVIEW_LST010), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eSiteList , LoadStringById(IDS_USERVIEW_LST011), LVCFMT_LEFT, 100);
	m_lcUser.InsertColumn(eHostList , LoadStringById(IDS_USERVIEW_LST012), LVCFMT_LEFT, 100);

	m_lcUser.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcUser.LoadColumnState("USER-LIST", szPath);
}

void CSiteUserView::UpdateUserRow(int nRow, SUserInfo& info)
{
	CString strUseAlarm;
	if(info.useSms) strUseAlarm = "SMS";
	if(info.useEmail) strUseAlarm += (strUseAlarm.IsEmpty() ? "": ",") + CString("Email");

	CString strAlarmList = info.probableCauseList;
	strAlarmList.Replace("[","");
	strAlarmList.Replace("]","");
	strAlarmList.Replace("\"","");

	CString strSiteList = info.siteList;
	strSiteList.Replace("[","");
	strSiteList.Replace("]","");
	strSiteList.Replace("\"","");

	CString strHostList = info.hostList;
	strHostList.Replace("[","");
	strHostList.Replace("]","");
	strHostList.Replace("\"","");

	CString strValidDate = info.validationDate.Format("%Y/%m/%d");
	if(strValidDate == _T("1970/01/01"))
	{
		strValidDate.Empty();
	}

	m_lcUser.SetItemText(nRow, eUserSite , info.siteId);
	m_lcUser.SetItemText(nRow, eUserID   , info.userId);
//		m_lcUser.SetItemText(nRow, eUserPW   , SetHide(info.password));
	m_lcUser.SetItemText(nRow, eUserName , info.userName);
	m_lcUser.SetItemText(nRow, eUserPerm , CCopModule::GetObject()->UserType(info.userType));
	m_lcUser.SetItemText(nRow, eRole     , GetRoleName(info.roleId));
	m_lcUser.SetItemText(nRow, eValidDate, strValidDate);
	m_lcUser.SetItemText(nRow, eUserTel  , info.phoneNo);
	m_lcUser.SetItemText(nRow, eUserMail , info.email);
	m_lcUser.SetItemText(nRow, eUseAlarm , strUseAlarm);
	m_lcUser.SetItemText(nRow, eAlarmList, strAlarmList);
	m_lcUser.SetItemText(nRow, eSiteList , strSiteList);
	m_lcUser.SetItemText(nRow, eHostList , strHostList);

	SChildSiteInfo* pChildSiteInfo = GetSiteInfoFromMap(info.siteId);

	if(pChildSiteInfo)
	{
		if(m_kbIncludeChild.GetCheck())
		{
			m_lcUser.SetItemText(nRow, eUserSite, pChildSiteInfo->name_path);
		}
		else
		{
			m_lcUser.SetItemText(nRow, eUserSite, pChildSiteInfo->siteName);
		}
	}

	if(info.bReadOnly) m_lcUser.SetRowColor(nRow, RGB(255,255,255), RGB(128,128,128));
}

CString CSiteUserView::GetRoleName(CString strRoleId)
{
	POSITION pos = m_lsRole.GetHeadPosition();
	while(pos)
	{
		SRoleInfo& info = m_lsRole.GetNext(pos);

		if(info.customer.CompareNoCase(GetEnvPtr()->m_strCustomer) != 0) continue;

		if(info.roleId == strRoleId)
		{
			return info.roleName;
		}
	}

	return strRoleId;
}

void CSiteUserView::RefreshUserList(ciBoolean reget /*=false*/)
{
	if(reget){
		m_lsUser.RemoveAll();
		SUserInfo userInfo;
		CCopModule::GetObject()->GetUser(userInfo, m_lsUser, FALSE);
	}

	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(m_treeSite.GetSelectedItem());

	m_lcUser.SetRedraw(FALSE);

	int nRow = 0;
	POSITION pos = m_lsUser.GetHeadPosition();
	m_lcUser.DeleteAllItems();

	while(pos)
	{
		POSITION posOld = pos;
		SUserInfo& info = m_lsUser.GetNext(pos);

		SChildSiteInfo* pChildSiteInfo = GetSiteInfoFromMap(info.siteId);
		if(!pChildSiteInfo) continue;

		info.siteName = pChildSiteInfo->siteName;

		if(pSelectedSiteInfo)
		{
			if(m_kbIncludeChild.GetCheck() && pChildSiteInfo)
			{
				if((pChildSiteInfo->id_path+_T("/")).Find(pSelectedSiteInfo->id_path+_T("/")) < 0)
				{
					continue;
				}
			}
			else
			{
				if(pSelectedSiteInfo->siteId != info.siteId)
				{
					continue;
				}
			}
		}

		m_lcUser.InsertItem(nRow, "");

		UpdateUserRow(nRow, info);

		m_lcUser.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lcUser.GetCurSortCol();
	if(-1 != nCol)
		m_lcUser.SortList(nCol, m_lcUser.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcUser);
	else
		m_lcUser.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcUser);

	m_lcUser.SetRedraw(TRUE);
}

void CSiteUserView::OnBnClickedButtonRefresh()
{
	RefreshSiteList(true);
	RefreshUserList(true);
}

void CSiteUserView::OnBnClickedButtonAdd()
{
	UpdateData(TRUE);

	if(m_tabMain.GetCurSel() == 0)
	{
		AddSite();
	}
	else
	{
		AddUser();
	}
}

void CSiteUserView::OnBnClickedButtonDel()
{
	UpdateData(TRUE);

	if(m_tabMain.GetCurSel() == 0)
	{
		DeleteSite();
	}
	else
	{
		DeleteUser();
	}
}

void CSiteUserView::OnBnClickedButtonMod()
{
	UpdateData(TRUE);

	if(m_tabMain.GetCurSel() == 0)
	{
		ModifySite();
	}
	else
	{
		ModifyUser();
	}
}

void CSiteUserView::AddUser()
{
	HTREEITEM itemParent = m_treeSite.GetSelectedItem();
	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(itemParent);
	if(!pSelectedSiteInfo)
	{
		UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG002));
		return;
	}

	SUserInfo info;
	info.siteName = pSelectedSiteInfo->siteName;
	info.siteId = pSelectedSiteInfo->siteId;

	CUserInfoDlg dlg;
	dlg.SetInfo(info);
	if(dlg.DoModal() != IDOK) return;
	info = dlg.GetInfo();

	if(!CCopModule::GetObject()->AddUser(info))
	{
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG001), MB_ICONERROR);
		return;
	}

	POSITION posNew = m_lsUser.AddTail(info);

	int nRow = m_lcUser.GetItemCount();
	m_lcUser.InsertItem(nRow, "");
	UpdateUserRow(nRow, info);
	m_lcUser.SetItemData(nRow, (DWORD_PTR)posNew);

	UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG003));
}

void CSiteUserView::DeleteUser()
{
	HTREEITEM itemParent = m_treeSite.GetSelectedItem();
	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(itemParent);
	if(!pSelectedSiteInfo)
	{
		UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG002));
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG003), MB_YESNO) != IDYES) return;

	CArray<int> arRow;
	for(int nRow = m_lcUser.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcUser.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lcUser.GetItemData(nRow);
		if(!pos) continue;

		SUserInfo Info = m_lsUser.GetAt(pos);

		if(Info.siteId == GetEnvPtr()->m_szSite && Info.userId == GetEnvPtr()->m_szLoginID)
		{
			m_lcUser.SetCheck(nRow, FALSE);
			continue;
		}

		if(Info.siteId == "Default" && Info.userId == "super")
		{
			m_lcUser.SetCheck(nRow, FALSE);
			continue;
		}

		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG002), MB_ICONINFORMATION);
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG003), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}

	int nDelCnt = 0;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);

		POSITION pos = (POSITION)m_lcUser.GetItemData(nRow);
		if(!pos) continue;

		SUserInfo info = m_lsUser.GetAt(pos);

		if(info.bReadOnly) continue;

		if(info.siteId.GetLength()==0 || info.userId.GetLength()==0) continue;

		if(info.userId == GetEnvPtr()->m_szLoginID) {
			// 자기자신은 지울수 없다.
			UbcMessageBox(IDS_SITEVIEW_MSG012);
			continue;
		}


		if(!CCopModule::GetObject()->DelUser(info.siteId, info.userId))
			continue;

		nDelCnt++;

		m_lsUser.RemoveAt(pos);
		m_lcUser.DeleteItem(nRow);
	}

	if(nDelCnt == 1)
	{
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG005), MB_ICONINFORMATION);
	}
	else if(nDelCnt > 1)
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_USERVIEW_MSG006), nDelCnt);
		UbcMessageBox(szMsg, MB_ICONINFORMATION);
	}
}

void CSiteUserView::ModifyUser()
{
	HTREEITEM itemParent = m_treeSite.GetSelectedItem();
	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(itemParent);
	if(!pSelectedSiteInfo)
	{
		UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG002));
		return;
	}

	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lcUser.GetItemCount(); nRow++){
		if(!m_lcUser.GetCheck(nRow))
			continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_USERVIEW_MSG007), MB_ICONINFORMATION);
		return;
	}

	BOOL bResult = FALSE;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lcUser.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lcUser.GetItemData(nRow);
		if(!pos)	continue;
		
		SUserInfo& info = m_lsUser.GetAt(pos);

		if(info.bReadOnly) continue;
		if(!CCopModule::GetObject()->IsMySite(info.siteId)) continue;

		CUserInfoDlg dlg;
		dlg.SetInfo(info);
		if(dlg.DoModal() != IDOK) continue;
		info = dlg.GetInfo();

		if(!CCopModule::GetObject()->SetUser(info))
		{
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_USERVIEW_MSG008), info.userId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			continue;
		}

		UpdateUserRow(nRow, info);

		bResult = TRUE;
	}

	//RefreshUserList();
	TraceLog(("Modify user"));

	if(arRow.GetCount() >= 1 && bResult)
	{
		UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG004));
	}
}

void CSiteUserView::AddSite()
{
	HTREEITEM itemParent = m_treeSite.GetSelectedItem();
	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(itemParent);
	if(!pSelectedSiteInfo)
	{
		UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG002));
		return;
	}

	CSiteInfoDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetParentSite(pSelectedSiteInfo->siteId);
	if(dlg.DoModal() != IDOK)
		return;

	SSiteInfo info = dlg.GetSite();
	info.parentId = pSelectedSiteInfo->siteId;

	if(!CCopModule::GetObject()->AddSite(GetEnvPtr()->m_strCustomer, info))
	{
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG001), MB_ICONERROR);
		return;
	}

	// skpark 2012.11.12  site가 추가/삭제되면 permition 을 refresh 해야 한다.
	CCopModule::GetObject()->RefreshPermitionSite(GetEnvPtr()->m_strCustomer);

	SChildSiteInfo childSiteInfo;

	childSiteInfo.mgrId = info.mgrId;
	childSiteInfo.siteId = info.siteId;
	childSiteInfo.siteName = info.siteName;
	childSiteInfo.parentId = pSelectedSiteInfo->siteId;
	childSiteInfo.lvl = childSiteInfo.lvl + 1;
	childSiteInfo.name_path = childSiteInfo.name_path + _T("/") + info.siteName;
	childSiteInfo.id_path = childSiteInfo.id_path + _T("/") + info.siteId;
	childSiteInfo.is_parent_changed = false;

	HTREEITEM item = m_treeSite.InsertItem(childSiteInfo.siteName, itemParent, TVI_SORT);

	SChildSiteInfo& newChildSiteInfo = m_lsChildSite.GetAt(m_lsChildSite.AddTail(childSiteInfo));
	m_treeSite.SetItemData(item, (DWORD_PTR)&newChildSiteInfo);
	SetSiteInfoFromMap(info.siteId, &newChildSiteInfo);

	POSITION posNew = m_lsSite.AddTail(info);

	int nRow = m_lcSite.GetItemCount();
	m_lcSite.InsertItem(nRow, "");
	UpdateSiteRow(nRow, info);
	m_lcSite.SetItemData(nRow, (DWORD_PTR)posNew);

	UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG002), MB_ICONINFORMATION);
}

void CSiteUserView::DeleteSite()
{
	HTREEITEM itemParent = m_treeSite.GetSelectedItem();
	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(itemParent);
	if(!pSelectedSiteInfo)
	{
		UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG002));
		return;
	}

	int targetCount = 0;
	for(int nRow = m_lcSite.GetItemCount()-1; 0 <= nRow; nRow--) {
		if(!m_lcSite.GetCheck(nRow)) continue;
		POSITION pos = (POSITION)m_lcSite.GetItemData(nRow);
		if(!pos) continue;
		targetCount++;
	}
	if(targetCount > 1) {
		UbcMessageBox(IDS_HOSTVIEW_MSG024);
		return;
	}


	if(UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG001), MB_YESNO) != IDYES) return;

	int nCnt = 0;
	CString szBuf;
	for(int nRow = m_lcSite.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcSite.GetCheck(nRow)) continue;

		POSITION pos = (POSITION)m_lcSite.GetItemData(nRow);
		if(!pos) continue;

		SSiteInfo info = m_lsSite.GetAt(pos);

		if(info.bReadOnly) continue;

//		szBuf.Format(LoadStringById(IDS_SITEVIEW_MSG003), m_lcSite.GetText(nRow, m_szSiteColum[eSiteID]));
//		if(UbcMessageBox(szBuf, MB_YESNO|MB_ICONINFORMATION) != IDYES){
//			continue;
//		}

		if(info.siteId == GetEnvPtr()->m_szSite) {
			// 자기자신은 지울수 없다.
			UbcMessageBox(IDS_SITEVIEW_MSG011);
			continue;
		}

		ubcSite aSite(info.siteId);

		ciStringList  programList;
		int programCount = aSite.getProgramList(programList);
		// program 이 있으면 삭제할 수 없다.
		if(programCount > 0) {
			UbcMessageBox(IDS_HOSTVIEW_MSG025);
			return;
		}
		ciStringList hostList;
		int hostCount = aSite.getHostList(hostList);

		SiteInfoList myChildren;
		int childrenNumber = getMyChildren(info.siteId, myChildren);
		if(childrenNumber > 0){
			// 해당 그룹의 자식이 존재한다. 이 경우 자식까지 지울것인가 물어보는 다이얼로그가 필요한다.
			TraceLog(("%s child site [%d] founded", info.siteId, childrenNumber));
			// ...
			UbcMessageBox(LoadStringById(IDS_CHILD_SITE_EXIST));
			return;
		}

		//해당 그룹을 삭제하면,  아래와 같은 단말(또는 프로그램)도 함께 삭제됩니다. 삭제하시겠습니까?
		//the terminal is also removed, if you remove the site. Do you want to remove the site.
		if(hostCount > 0 || programCount > 0)
		{
			CSiteDelDlg dlg;
			dlg.SetList(hostList, programList);
			if(dlg.DoModal() != IDOK)
			{
				continue;
			}
		}

		if(info.siteId.GetLength()==0) continue;
		if(!CCopModule::GetObject()->DelSite(info.siteId)) {
			TraceLog(("ERROR %s delete failed", info.siteId));		
			continue;
		}
		
		//HTREEITEM item = FindTreeItem(m_treeSite.GetChildItem(itemParent), info.siteId);
		HTREEITEM item = FindTreeItem(itemParent, info.siteId);
		if(item)
		{
			TraceLog(("%s site delete on the Tree", info.siteId));
			m_treeSite.DeleteItem(item);
		}else{
			TraceLog(("%s site not founded on the Tree", info.siteId));
		}


		nCnt++;

		m_lcSite.DeleteItem(nRow);
		m_lsSite.RemoveAt(pos);
	}

	if(nCnt <= 0) UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG004), MB_ICONINFORMATION);
	else          UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG005), MB_ICONINFORMATION);

	// skpark 2012.11.12  site가 추가/삭제되면 permition 을 refresh 해야 한다.
	CCopModule::GetObject()->RefreshPermitionSite(GetEnvPtr()->m_strCustomer);
	RefreshSiteList();
}

void CSiteUserView::ModifySite()
{
	TraceLog(("ModifySite()"));

	HTREEITEM itemParent = m_treeSite.GetSelectedItem();
	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(itemParent);
	if(!pSelectedSiteInfo)
	{
		UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG002));
		return;
	}

TraceLog(("skpark 1"));


	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lcSite.GetItemCount(); nRow++)
	{
		if(!m_lcSite.GetCheck(nRow))
			continue;
		arRow.Add(nRow);
	}
TraceLog(("skpark 2"));

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG006), MB_ICONINFORMATION);
		return;
	}
TraceLog(("skpark 3 %d", arRow.GetCount()));

	bool bResult = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lcSite.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lcSite.GetItemData(nRow);
		if(!pos) {
			TraceLog(("Invalid position"));		
			continue;
		}

		SSiteInfo& info = m_lsSite.GetAt(pos);

		if(info.bReadOnly) {
			TraceLog(("%s is readonly site", info.siteId));		
			continue;
		}
		if(!CCopModule::GetObject()->IsMySite(info.siteId)) {
			TraceLog(("%s is not my site", info.siteId));		
			continue;
		}

		CSiteInfoDlg dlg(GetEnvPtr()->m_strCustomer);
		dlg.SetSite(info);
		if(dlg.DoModal() != IDOK) continue;
		info = dlg.GetSite();

		if(!CCopModule::GetObject()->SetSite(info))
		{
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_SITEVIEW_MSG007), info.siteId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			continue;
		}

		HTREEITEM item = FindTreeItem(m_treeSite.GetChildItem(itemParent), info.siteId);
		if(item)
		{
			m_treeSite.SetItemText(item, info.siteName);
			SChildSiteInfo* pChildSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(item);
			if(pChildSiteInfo)
			{
				pChildSiteInfo->siteName = info.siteName;
				pChildSiteInfo->name_path = pChildSiteInfo->name_path.Left(pChildSiteInfo->name_path.ReverseFind('/')) + _T("/") + info.siteName;
			}
		}

		UpdateSiteRow(nRow, info);
		bResult = true;
	}

	if(arRow.GetCount() >= 1 && bResult)
	{
		UbcMessageBox(LoadStringById(IDS_SITEVIEW_MSG008), MB_ICONINFORMATION);
	}
}

void CSiteUserView::OnNMDblclkLcUser(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
//	int	nColumn = pNMLV->iSubItem;

	m_lcUser.SetCheck(nRow, TRUE);

	ModifyUser();
}

void CSiteUserView::OnNMDblclkLcSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
//	int	nColumn = pNMLV->iSubItem;

	m_lcSite.SetCheck(nRow, TRUE);

	ModifySite();
}

void CSiteUserView::OnDestroy()
{
	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcSite.SaveColumnState("SITE-LIST", szPath);
	m_lcUser.SaveColumnState("USER-LIST", szPath);

	CFormView::OnDestroy();
}

void CSiteUserView::OnBnClickedKbIncludeChild()
{
	RefreshSiteList();
	RefreshUserList();
}

void CSiteUserView::OnTcnSelchangeTabMain(NMHDR *pNMHDR, LRESULT *pResult)
{
	UpdateData(TRUE);

	m_lcSite.EnableWindow(m_tabMain.GetCurSel() == 0);
	m_lcSite.ShowWindow(m_tabMain.GetCurSel() == 0);

	m_lcUser.EnableWindow(m_tabMain.GetCurSel() == 1);
	m_lcUser.ShowWindow(m_tabMain.GetCurSel() == 1);

	InitButtonState();

	*pResult = 0;
}

void CSiteUserView::OnTvnSelchangedTreeSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

	if( !m_checkTreeEdit.GetCheck() )
	{
		RefreshSiteList();
		RefreshUserList();
	}

	InitButtonState();

	*pResult = 0;
}

HTREEITEM CSiteUserView::FindTreeItem(HTREEITEM item, CString strSiteId)
{
	while(item)
	{
		//
		SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(item);
		if(pSelectedSiteInfo && pSelectedSiteInfo->siteId == strSiteId)
		{
			return item;
		}

		//
		if(m_treeSite.ItemHasChildren(item))
		{
			HTREEITEM child_item = m_treeSite.GetChildItem(item);
			HTREEITEM find_item = FindTreeItem(child_item, strSiteId);
			if(find_item)
			{
				return find_item;
			}
		}

		//
		item = m_treeSite.GetNextSiblingItem(item);
	}

	return NULL;
}

void CSiteUserView::InitButtonState()
{
	SChildSiteInfo* pSelectedSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(m_treeSite.GetSelectedItem());

	BOOL bIsMySite = FALSE;

	if(pSelectedSiteInfo)
	{
		bIsMySite = CCopModule::GetObject()->IsMySite(pSelectedSiteInfo->siteId);
	}

	if(m_tabMain.GetCurSel() == 0)  // 조직
	{
		BOOL is_enable =  bIsMySite && 
						  ( CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority || 
							CCopModule::eSiteManager == GetEnvPtr()->m_Authority );

		m_bnAdd.EnableWindow(is_enable && !m_checkTreeEdit.GetCheck());
		m_bnDel.EnableWindow(is_enable && !m_checkTreeEdit.GetCheck());
		m_bnMod.EnableWindow(is_enable && !m_checkTreeEdit.GetCheck());
		m_checkTreeEdit.EnableWindow(is_enable);
	}
	else // 유저
	{
		BOOL is_enable =  bIsMySite && 
						  ( CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority || 
							CCopModule::eSiteManager == GetEnvPtr()->m_Authority );

		m_bnAdd.EnableWindow(is_enable && !m_checkTreeEdit.GetCheck());
		m_bnDel.EnableWindow(is_enable && !m_checkTreeEdit.GetCheck());
		m_bnMod.EnableWindow(bIsMySite && CCopModule::eSiteUnkown != GetEnvPtr()->m_Authority && !m_checkTreeEdit.GetCheck());
	}

	m_tabMain.EnableWindow(!m_checkTreeEdit.GetCheck());
	m_bnRefresh.EnableWindow(!m_checkTreeEdit.GetCheck());
	m_lcSite.EnableWindow(!m_checkTreeEdit.GetCheck());
	m_lcUser.EnableWindow(!m_checkTreeEdit.GetCheck());
	m_kbIncludeChild.EnableWindow(!m_checkTreeEdit.GetCheck());
	m_btnExcelSave.EnableWindow(!m_checkTreeEdit.GetCheck());
}

int CSiteUserView::getMyChildren(CString parentId, SiteInfoList& children)
{
	POSITION pos = m_lsSite.GetHeadPosition();
	while(pos)
	{
		SSiteInfo& info = m_lsSite.GetNext(pos);
		if(info.parentId == parentId){
			children.AddHead(info);
		}
	}
	return children.GetSize();
}

void CSiteUserView::OnBnClickedBnToExcel()
{
	CHostExcelSave dlg;

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_SITEVIEW_STR001)
									, ( m_tabMain.GetCurSel()==0 ? m_lcSite : m_lcUser )
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}

void CSiteUserView::OnTvnBegindragTreeSite(NMHDR *pNMHDR, LRESULT *pResult)
{
	TraceLog(("OnTvnBegindragTreeSite 1"));

	if(!m_checkTreeEdit.GetCheck()){
		return;
	}

	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	// 드래그 가능한 노드인지 체크
	if( m_treeSite.GetItemData(pNMTreeView->itemNew.hItem) == NULL )
	{
		return;
	}
	TraceLog(("OnTvnBegindragTreeSite 2"));

	// 드래그 이미지 생성
	if(m_pDragImage) m_pDragImage->DeleteImageList();
	m_pDragImage = m_treeSite.CreateDragImage(pNMTreeView->itemNew.hItem);

	if(!m_pDragImage){
		TraceLog(("ERROR : CreateDragImage failed"));
		return;
	}

	TraceLog(("OnTvnBegindragTreeSite 3"));

	CPoint ptAction(pNMTreeView->ptDrag);

	TraceLog(("OnTvnBegindragTreeSite 4"));

	// 드래그시 사용할 이미지 크기 계산
	RECT rcItem;
	m_treeSite.GetItemRect(pNMTreeView->itemNew.hItem, &rcItem, TRUE); // 아이콘을 포함하는 크기

	TraceLog(("OnTvnBegindragTreeSite 5"));

	CRect rcWnd;
	m_treeSite.GetWindowRect(&rcWnd);
	ScreenToClient(&rcWnd);

	TraceLog(("OnTvnBegindragTreeSite 6"));

	// 드래그를 시작
	m_pDragImage->BeginDrag(0, CPoint(ptAction.x - rcItem.left + 16, ptAction.y - rcItem.top - rcWnd.top));

	TraceLog(("OnTvnBegindragTreeSite 6.1"));

	m_treeSite.ClientToScreen(&ptAction);

	TraceLog(("OnTvnBegindragTreeSite 6.2"));

	ScreenToClient(&ptAction);

	TraceLog(("OnTvnBegindragTreeSite 7"));

	// 드래그 이미지 표시
	m_pDragImage->DragEnter(this, ptAction);

	TraceLog(("OnTvnBegindragTreeSite 8"));

	// 마우스 메시지를 잡아두고
	SetCapture();

	// 현재 선택된 아이템 핸들을 기억
	m_hDragItem = pNMTreeView->itemNew.hItem;

	TraceLog(("OnTvnBegindragTreeSite 9"));

	SetTimer(1023, 75, NULL);
}

void CSiteUserView::OnMouseMove(UINT nFlags, CPoint point)
{
	// 드래그 중이 아니라면
	if(!m_pDragImage) return;

	TraceLog(("OnMouseMove 1"));

	CPoint ptScreen(point);
	ClientToScreen(&ptScreen);
	CWnd* pWndDrop = WindowFromPoint(ptScreen);
	BOOL bCopyEnable = FALSE;
	HTREEITEM hTargetItem = NULL;

	CPoint ptClient = ptScreen;
	pWndDrop->ScreenToClient(&ptClient);

	if(pWndDrop->m_hWnd == m_treeSite.m_hWnd)
	{
		TraceLog(("OnMouseMove 2"));

	// 마우스가 위치한 아이템을 검사한다.항목이 트리 뷰 항목위에 있는지 확인하고 그렇다면 항목이 밝게 표시되도록한다.
		hTargetItem = m_treeSite.HitTest(ptClient);
		bCopyEnable = (hTargetItem != NULL);

		// 드랍 가능한 노드인지 체크
		if(hTargetItem)
		{
			if((m_treeSite.GetItemState(hTargetItem, TVIS_OVERLAYMASK) & INDEXTOOVERLAYMASK(MINUS_IMG_IDX)) != 0)
			{
				bCopyEnable = FALSE;
			}
			else
			{
				if(!IsFitChildNode(hTargetItem, m_hDragItem))
				{
					bCopyEnable = FALSE;
				}

				//if(!IsFitChildNode(hTargetItem, (DWORD_PTR)m_pDragContentsInfo))
				//{
				//	bCopyEnable = FALSE;
				//}
			}
		}
	}
	TraceLog(("OnMouseMove 3"));

	// 드래그 이미지 그리기 중지
	m_pDragImage->DragLeave(this);
	//m_pDragImage->DragShowNolock(FALSE);

	if(bCopyEnable)
	{
		m_treeSite.SelectDropTarget(hTargetItem);
	}
	else
	{
		m_treeSite.SelectDropTarget(NULL);
	}
	TraceLog(("OnMouseMove 4"));

	// 드래그 이미지를 다시 보여준다.
	m_pDragImage->DragEnter(this, point);
	//m_pDragImage->DragShowNolock(TRUE);
	m_pDragImage->DragMove(point);

	SetCursor(LoadCursor(NULL, (bCopyEnable ? IDC_ARROW : IDC_NO)));

	TraceLog(("OnMouseMove 5"));

	CFormView::OnMouseMove(nFlags, point);
}

// 현재 트리의 모든 아이템 데이터 이동
BOOL MoveChildTreeItem(CTreeCtrl *pTree, HTREEITEM hChildItem,
					   HTREEITEM hDestItem)
{
	HTREEITEM hSrcItem = hChildItem;

	while(hSrcItem)
	{
		// 이동할 아이템의 정보를 알아내자.
		TVITEM    TV;
		char    str[256];
		ZeroMemory(str, sizeof(str));
		TV.hItem     = hSrcItem;
		TV.mask     = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
		TV.pszText = str;
		TV.cchTextMax = sizeof(str);
		pTree->GetItem(&TV);
		DWORD dwData = pTree->GetItemData(hSrcItem);

		// 아이템을 추가 하자.
		TVINSERTSTRUCT  TI;
		TI.hParent       = hDestItem;
		TI.hInsertAfter  = TVI_LAST;
		TI.item.mask    = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
		TI.item.iImage   = TV.iImage;
		TI.item.iSelectedImage = TV.iSelectedImage;
		TI.item.pszText   = TV.pszText;
		HTREEITEM hItem  = pTree->InsertItem(&TI);
		pTree->SetItemData(hItem, dwData);

		// 현재 아이템에 자식 아이템이 있다면
		HTREEITEM hChildItem = pTree->GetChildItem(hSrcItem);

		// pTree->GetNextItem(hSrcItem, TVGN_CHILD);
		if(hChildItem)
		{
			MoveChildTreeItem(pTree, hChildItem, hItem);
		}

		// 확장 여부를 알아서 똑같이 하자.
		TVITEM  item;
		item.mask = TVIF_HANDLE;
		item.hItem = hSrcItem;
		pTree->GetItem(&item);
		if(item.state & TVIS_EXPANDED)
		{
			pTree->Expand(hItem, TVE_EXPAND);
		}

		// 다음 아이템을 알아보자.
		hSrcItem = pTree->GetNextItem(hSrcItem, TVGN_NEXT);
	}

	// 기존 아이템을 제거한다.
	pTree->DeleteItem(hChildItem);

	return TRUE;
}

BOOL MoveTreeItem(CTreeCtrl *pTree, HTREEITEM hSrcItem, HTREEITEM hDestItem)
{
	// 이동할 아이템의 정보를 알아내자.
	TVITEM    TV;
	char    str[256];
	ZeroMemory(str, sizeof(str));
	TV.hItem = hSrcItem;
	TV.mask  = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	TV.pszText = str;
	TV.cchTextMax = sizeof(str);
	pTree->GetItem(&TV);
	DWORD dwData = pTree->GetItemData(hSrcItem);

	// 아이템을 추가 하자.
	TVINSERTSTRUCT  TI;
	TI.hParent        = hDestItem;
	TI.hInsertAfter   = TVI_LAST;
	TI.item.mask     = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	TI.item.iImage   = TV.iImage;
	TI.item.iSelectedImage = TV.iSelectedImage;
	TI.item.pszText   = TV.pszText;
	HTREEITEM hItem  = pTree->InsertItem(&TI);
	pTree->SetItemData(hItem, dwData);

	// 현재 아이템에 자식 아이템이 있다면
	HTREEITEM hChildItem = pTree->GetChildItem(hSrcItem);
	if(hChildItem)
	{
		// 자식 아이템이 있다면 같이 이동하자.
		MoveChildTreeItem(pTree, hChildItem, hItem);
	}

	// 확장 여부를 알아서 똑같이 하자.
	TVITEM  item;
	item.mask = TVIF_HANDLE;
	item.hItem = hSrcItem;
	pTree->GetItem(&item);
	if(item.state & TVIS_EXPANDED)
	{
		pTree->Expand(hItem, TVE_EXPAND);
	}

	// 아이템을 선택하자.
	pTree->SelectItem(hItem);

	// 기존 아이템을 제거한다.
	pTree->DeleteItem(hSrcItem);

	return TRUE;
}

void CSiteUserView::OnLButtonUp(UINT nFlags, CPoint point)
{
	KillTimer(1023);

	if(!m_pDragImage) return;

	TraceLog(("OnLButtonUp 1"));

	// 마우스 메시지 캡쳐 기능을 제거한다.
	ReleaseCapture();

	// 드래그 과정을 중단한다.
	m_pDragImage->DragLeave(&m_treeSite);
	m_pDragImage->EndDrag();
	m_pDragImage->DeleteImageList();
	m_pDragImage = NULL;

	TraceLog(("OnLButtonUp 2"));

	// 일단 마지막으로 밝게 표시되었던 항목을 찾는다.
	HTREEITEM hTargetItem = m_treeSite.GetDropHilightItem();
	if(!hTargetItem) return;

	TraceLog(("OnLButtonUp 3"));
	// 밝게 표시된 드롭 항목의 선택을 취소한다.
	m_treeSite.SelectDropTarget(NULL);

	TraceLog(("OnLButtonUp 4"));

	SChildSiteInfo* pSiteInfo = NULL;

	if(m_hDragItem)
	{
		TraceLog(("OnLButtonUp 5"));
		// 현재 자식의 부모 아이템 핸들을 구한다.
		HTREEITEM hParentItem = m_treeSite.GetNextItem(m_hDragItem, TVGN_PARENT);

		if( m_hDragItem != hTargetItem &&  // 선택된 아이템과 이동될 곳의 아이템이 같다면 이동할 필요가 없다.
			hParentItem != hTargetItem )   // 이동하려는 곳이 자신이 직접속한 항목 이라면 이동할 필요가 없다.
		{
			TraceLog(("OnLButtonUp 5.1"));
			pSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(m_hDragItem);
			ChangeParent(hTargetItem, m_hDragItem, pSiteInfo);

			MoveTreeItem(&m_treeSite, m_hDragItem, hTargetItem);

			//HTREEITEM new_node = m_treeSite.InsertItem(m_treeSite.GetItemText(m_hDragItem), hTargetItem, hTargetItem);
			//m_treeSite.SetItemData(new_node, (DWORD_PTR)pSiteInfo);
			//m_treeSite.DeleteItem(m_hDragItem);
			//m_treeSite.Expand(hTargetItem, TVE_EXPAND);
		}
		TraceLog(("OnLButtonUp 5.2"));
	}else{
		return;
	}
	//else if(m_pDragContentsInfo)
	//{
	//	pSiteInfo = m_pDragContentsInfo;
	//}
	TraceLog(("OnLButtonUp 6"));

	//if(pSiteInfo)
	//{
	//	ChangeParent(hTargetItem, pSiteInfo);

	//	RefreshTree();

	//	//HTREEITEM hWizardItem = FindChild(NULL, (DWORD_PTR)m_pDragContentsInfo);
	//	//if(hWizardItem)
	//	//{
	//	//	m_treeSite.SelectItem(hWizardItem);
	//	//	m_treeSite.Expand(hWizardItem, TVE_EXPAND);
	//	//	m_treeSite.EnsureVisible(hWizardItem);
	//	//}
	//}

	TraceLog(("OnLButtonUp 7"));

	m_hDragItem = NULL;
//	m_pDragContentsInfo = NULL;

	CFormView::OnLButtonUp(nFlags, point);
}

BOOL CSiteUserView::IsFitChildNode(HTREEITEM hSrcItem, HTREEITEM hChildItem)
{
	if(hSrcItem == NULL || hChildItem == NULL) return TRUE;

	DWORD_PTR pChildData = m_treeSite.GetItemData(hChildItem);

	BOOL bFindIt = FALSE;
	HTREEITEM hParentItem = hSrcItem;
	while( (hParentItem = m_treeSite.GetParentItem(hParentItem)) != NULL)
	{
		DWORD_PTR pItemData = m_treeSite.GetItemData(hParentItem);
		bFindIt = (pItemData == pChildData);
		if(bFindIt) break;

		bFindIt = (FindChild(hChildItem, pItemData) != NULL);
		if(bFindIt) break;
	}

	return !bFindIt;
}

BOOL CSiteUserView::IsFitChildNode(HTREEITEM hSrcItem, DWORD_PTR pInItemData)
{
	if(hSrcItem == NULL || pInItemData == NULL) return TRUE;

	BOOL bFindIt = FALSE;
	HTREEITEM hParentItem = hSrcItem;
	while( (hParentItem = m_treeSite.GetParentItem(hParentItem)) != NULL)
	{
		DWORD_PTR pItemData = m_treeSite.GetItemData(hParentItem);
		bFindIt = (pItemData == pInItemData);
		if(bFindIt) break;
	}

	return !bFindIt;
}

HTREEITEM CSiteUserView::FindChild(HTREEITEM hParent, DWORD_PTR pFindData )
{
	HTREEITEM hChild = m_treeSite.GetNextItem( hParent, TVGN_CHILD );

	while( NULL != hChild )
	{
		if( m_treeSite.GetItemData( hChild ) == pFindData) return hChild;

		HTREEITEM hSubChild = FindChild( hChild, pFindData );
		if( hSubChild ) return hSubChild;

		hChild = m_treeSite.GetNextItem( hChild, TVGN_NEXT );
	}
	return NULL;
}

void CSiteUserView::ChangeParent(HTREEITEM hParentItem, HTREEITEM hItem, SChildSiteInfo* pChildSiteInfo)
{
	TraceLog(("ChangeParent()"));

	if(!pChildSiteInfo){
		TraceLog(("Child site info is null"));	
		return;
	}

	if(hParentItem == NULL || hItem == NULL) {
		return;
	}

	TraceLog(("ChangeParent() 1"));
	int nIndex = 0;
	SChildSiteInfo* pParentSiteInfo = (SChildSiteInfo*)m_treeSite.GetItemData(hParentItem);
	if(!pParentSiteInfo) return;
	TraceLog(("ChangeParent(%s --> %s) ", pChildSiteInfo->parentId, pParentSiteInfo->siteId));

	pChildSiteInfo->changing_parentId = pParentSiteInfo->siteId;
	pChildSiteInfo->is_parent_changed = true;

	m_btSaveTree.EnableWindow(TRUE);
}

SSiteInfo* CSiteUserView::GetTreeItemInfo(HTREEITEM hItem, int& nContentsIndex)
{
	nContentsIndex = 0;
	if(hItem == NULL) return NULL;

	HTREEITEM hWizardItem = m_treeSite.GetParentItem(hItem);
	if(hWizardItem == NULL) return NULL;

	if(m_treeSite.GetItemData(hWizardItem) == NULL) return NULL;

	HTREEITEM hSiblingItem = hItem;
	while( (hSiblingItem = m_treeSite.GetPrevSiblingItem(hSiblingItem)) != NULL)
	{
		nContentsIndex++;
	}

	return (SSiteInfo*)m_treeSite.GetItemData(hWizardItem);
}

void CSiteUserView::OnBnClickedTreeEditCheck()
{
	if( m_checkTreeEdit.GetCheck() )
	{
		m_bSiteParentChanged = false;

		m_btSaveTree.EnableWindow(FALSE);

		m_btSaveTree.ShowWindow(SW_SHOW);
		m_btCancelTree.ShowWindow(SW_SHOW);
	}
	else
	{
		OnBnClickedButtonCancel();
	}

	InitButtonState();
}

void CSiteUserView::OnBnClickedButtonSave()
{
	bool find_error = false;
	POSITION pos = m_lsChildSite.GetHeadPosition();
	while(pos)
	{
		SChildSiteInfo& info = m_lsChildSite.GetNext(pos);

		// 변경되지 않은 경우
		if( !info.is_parent_changed ) continue;

		// 변경후 원복했을경우
		if( info.changing_parentId == info.parentId )
		{
			info.is_parent_changed = false;
			continue;
		}

		m_bSiteParentChanged = true;

		BOOL ret_val = CCopModule::GetObject()->ChangeParent(info.siteId, info.changing_parentId);
		if( ret_val )
		{
			info.parentId = info.changing_parentId;
			info.is_parent_changed = false;
		}
		else
		{
			find_error = true;
		}
	}

	if( !find_error )
		m_btSaveTree.EnableWindow(FALSE);
}

void CSiteUserView::OnBnClickedButtonCancel()
{
	m_checkTreeEdit.SetCheck(BST_UNCHECKED);

	m_btSaveTree.ShowWindow(SW_HIDE);
	m_btCancelTree.ShowWindow(SW_HIDE);

	if( m_bSiteParentChanged )
	{
		m_treeSite.DeleteAllItems();

		m_lsChildSite.RemoveAll();
		m_mapChildSite.RemoveAll();

		CCopModule::GetObject()->GetChildSite(GetEnvPtr()->m_strCustomer, GetEnvPtr()->m_strCustomer, m_lsChildSite, "order by id_path" );
	}

	bool find_changed = false;
	POSITION pos = m_lsChildSite.GetHeadPosition();
	while(pos)
	{
		SChildSiteInfo& info = m_lsChildSite.GetNext(pos);

		if(info.is_parent_changed)
		{
			info.is_parent_changed = false;
			find_changed = true;
		}
	}

	if( find_changed || m_bSiteParentChanged ) RefreshTree();
	RefreshSiteList();
	RefreshUserList();

	InitButtonState();
}

#define		SCROLL_BORDER				10
#define		SCROLL_SPEED_ZONE_WIDTH		20

void CSiteUserView::OnTimer(UINT_PTR nIDEvent)
{
	if( nIDEvent == 1023 )
	{
		static int m_timerticks = 0;
	// Doesn't matter that we didn't initialize m_timerticks
		m_timerticks++;

		POINT pt;
		GetCursorPos( &pt );
		CRect rect;
		m_treeSite.GetClientRect( &rect );
		m_treeSite.ClientToScreen( &rect );

		// NOTE: Screen coordinate is being used because the call
		// to DragEnter had used the Desktop window.
		//CImageList::DragMove(pt);

		HTREEITEM hitem = m_treeSite.GetFirstVisibleItem();

		int iMaxV = m_treeSite.GetScrollLimit(SB_VERT);
		int iPosV = m_treeSite.GetScrollPos(SB_VERT);
		// The cursor must not only be SOMEWHERE above/beneath the tree control
		// BUT RIGHT above or beneath it 
		// i.e. the x-coordinates must be those of the control (+/- SCROLL_BORDER)
		if ( pt.x < rect.left - SCROLL_BORDER ) 
			; // Too much to the left
		else if ( pt.x > rect.right + SCROLL_BORDER ) 
			; // Too much to the right
		else if( (pt.y < rect.top + SCROLL_BORDER) && iPosV )
		{
			// We need to scroll up
			// Scroll slowly if cursor near the treeview control
			int slowscroll = 6 - (rect.top + SCROLL_BORDER - pt.y) / SCROLL_SPEED_ZONE_WIDTH;
			if( 0 == ( m_timerticks % (slowscroll > 0? slowscroll : 1) ) )
			{
				CImageList::DragShowNolock(FALSE);
				m_treeSite.SendMessage( WM_VSCROLL, SB_LINEUP);
				m_treeSite.SelectDropTarget(hitem);
				//m_hitemDrop = hitem;
				CImageList::DragShowNolock(TRUE);
			}
		}
		else if( (pt.y > rect.bottom - SCROLL_BORDER) && (iPosV!=iMaxV) )
		{
			// We need to scroll down
			// Scroll slowly if cursor near the treeview control
			int slowscroll = 6 - (pt.y - rect.bottom + SCROLL_BORDER ) / SCROLL_SPEED_ZONE_WIDTH;
			if( 0 == ( m_timerticks % (slowscroll > 0? slowscroll : 1) ) )
			{
				CImageList::DragShowNolock(FALSE);
				m_treeSite.SendMessage( WM_VSCROLL, SB_LINEDOWN);
				int nCount = m_treeSite.GetVisibleCount();
				for ( int i=0; i<nCount-1; ++i )
					hitem = m_treeSite.GetNextVisibleItem(hitem);
				if( hitem )
					m_treeSite.SelectDropTarget(hitem);
				//m_hitemDrop = hitem;
				CImageList::DragShowNolock(TRUE);
			}
		}

		// The cursor must be in a small zone IN the treecontrol at the left/right
		int iPosH = m_treeSite.GetScrollPos(SB_HORZ);
		int iMaxH = m_treeSite.GetScrollLimit(SB_HORZ);

		if ( !rect.PtInRect(pt) ) return; // not in TreeCtrl
		else if ( (pt.x < rect.left + SCROLL_BORDER) && (iPosH != 0) )
		{
			// We need to scroll to the left
			CImageList::DragShowNolock(FALSE);
			m_treeSite.SendMessage(WM_HSCROLL, SB_LINELEFT);
			CImageList::DragShowNolock(TRUE);
		}
		else if ( (pt.x > rect.right - SCROLL_BORDER) && (iPosH != iMaxH) )
		{
			// We need to scroll to the right
			CImageList::DragShowNolock(FALSE);
			m_treeSite.SendMessage(WM_HSCROLL, SB_LINERIGHT);
			CImageList::DragShowNolock(TRUE);
		}
	}

	CFormView::OnTimer(nIDEvent);
}
