#pragma once

#include "resource.h"
#include "UBCCopCommon\CopModule.h"

// CChangeSiteDlg dialog
class CChangeSiteDlg : public CDialog
{
	DECLARE_DYNAMIC(CChangeSiteDlg)
public:
	CString m_szSelSiteId;
	CString m_szSelSiteName;
	bool	m_bReboot;

private:
	CButton   m_ckReboot;

public:
	CChangeSiteDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChangeSiteDlg();

// Dialog Data
	enum { IDD = IDD_CHANGESITE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonSite();
};
