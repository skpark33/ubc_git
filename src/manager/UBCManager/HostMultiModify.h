#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "common\hoverbutton.h"
#include "resource.h"
#include "afxdtctl.h"
#include "ubccopcommon\CopModule.h"

// CHostMultiModify 대화 상자입니다.

class CHostMultiModify : public CDialog
{
	DECLARE_DYNAMIC(CHostMultiModify)

public:
	CHostMultiModify(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHostMultiModify();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HOST_MULTIMODIFY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	bool	m_bInit;
	int		m_nLogLimit; // 로그보존기간 (1~356사이의 값을 갖는다)
	int		m_nPeriod2; // 스크린샷주기 (120~3600)
	bool	m_bAdminState;
	bool	m_bMute;
	bool	m_bAutoUpdate;

	CButton m_check_hostType;
	CButton m_check_vendor;
	CButton m_check_model;
	CButton m_check_addr1;
	CButton m_check_description;
	CButton m_check_monitorType;
	CButton m_check_videoRendor;
	CButton m_check_dayCriteria;
	CButton m_check_logLimit;
	CButton m_check_period2;
	CButton m_check_soundVolumeCheck;
	CButton m_check_muteCheck;
	CButton m_check_autoupdateCheck;
	CButton m_check_downloadTimeCheck;
	CButton m_check_adminState;
	
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnClose();

	afx_msg void OnBnClickedButtonUstate();
	afx_msg void OnEnKillfocusEditPeriod2();
	afx_msg void OnEnKillfocusEditLoglimit();
	afx_msg void OnBnClickedCheckDownloadtime();

	CComboBox m_cbType;
	CComboBox m_cbMonitorType;
	CComboBox m_cbVideoRendering;
	CSpinButtonCtrl m_spLogLimit;
	CSpinButtonCtrl m_spPeriod2;
	CSliderCtrl m_slcVolume;
	CHoverButton m_btUState;
	CDateTimeCtrl m_dtcDownloadTime;
	CButton m_check_downloadTime;
	CEdit m_edit_vendor;
	CEdit m_edit_model;
	CEdit m_edit_addr1;
	CEdit m_edit_description;
	CDateTimeCtrl m_date_dayCriteria;
	CEdit m_edit_logLimt;
	CEdit m_edit_period2;
	CHoverButton m_bt_mute;
	CHoverButton m_bt_autoUpdate;

	afx_msg void OnBnClickedCheckHosttype();
	afx_msg void OnBnClickedCheckVendor();
	afx_msg void OnBnClickedCheckModel();
	afx_msg void OnBnClickedCheckAddr1();
	afx_msg void OnBnClickedCheckDescription();
	afx_msg void OnBnClickedCheckMonitortype();
	afx_msg void OnBnClickedCheckVideorendor();
	afx_msg void OnBnClickedCheckDayBaseTime();
	afx_msg void OnBnClickedCheckLoglimit();
	afx_msg void OnBnClickedCheckPeriod2();
	afx_msg void OnBnClickedCheckSoundvolume();
	afx_msg void OnBnClickedCheckMutecheck();
	afx_msg void OnBnClickedCheckAutoupdatecheck();
	afx_msg void OnBnClickedCheckDownloadtimecheck();
	afx_msg void OnBnClickedCheckAdminstate();
	afx_msg void OnBnClickedButtonMute();
	afx_msg void OnBnClickedButtonAutoupdate();

	SHostInfo*	GetHostInfo() { return &m_info;}
	CDirtyFlag*	GetHostInfoDirty() { return &m_infoDirty; }

protected:
	SHostInfo m_info;
	CDirtyFlag m_infoDirty;
public:
	CEdit m_editCacheServer;
	CButton m_check_cacheServer;
	afx_msg void OnBnClickedCheckCacheServerIp();
};
