#pragma once

#include "resource.h"
#include "afxwin.h"


// CAutoUpdateConfigDlg 대화 상자입니다.

class CAutoUpdateConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CAutoUpdateConfigDlg)

public:
	CAutoUpdateConfigDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CAutoUpdateConfigDlg();

	// Input/Output
	BOOL	m_bIsAutoUpdateFlag;
	CString m_strAutoUpdateTime;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_AUTO_UPDATE_CONFIG_DLG };

protected:
	CButton			m_kbIsAutoUpdate;
	CDateTimeCtrl	m_dtAutoUpdateTime;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
};
