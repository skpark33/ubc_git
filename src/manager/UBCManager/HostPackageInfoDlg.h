#pragma once

#include "afxwin.h"
#include "afxcmn.h"
#include "afxdtctl.h"

#include "common\utblistctrl.h"
#include "ubccopcommon\copmodule.h"

// CHostPackageInfoDlg 대화 상자입니다.

class CHostPackageInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CHostPackageInfoDlg)

public:
	CHostPackageInfoDlg(SHostInfo* pInfo,CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHostPackageInfoDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HOST_PACKAGEINFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();

	void InitList();
	void SetList();

protected:
	static int CALLBACK CompareSite(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);
	
	CString m_edtPackageCur1;
	CString m_edtPackageTime1;
	CString m_edtPackageLast1;
	CString m_edtPackageAuto1;
	CString m_edtPackageCur2;
	CString m_edtPackageTime2;
	CString m_edtPackageLast2;
	CString m_edtPackageAuto2;
	CUTBListCtrl m_lscDisplay1;
	CUTBListCtrl m_lscDisplay2;

	SHostInfo*		m_HostInfo;

};
