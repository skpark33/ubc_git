// UpdateCenterView.cpp : implementation file
//
#include "stdafx.h"
#include "UBCManager.h"
#include "UpdateCenterView.h"
#include "Enviroment.h"
#include "MainFrm.h"
#include "ChangeSiteDlg.h"

#include "common\ubcdefine.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libCommon\ubcMuxRegacy.h"
#include "common\libscratch\scratchUtil.h"
#include "common\libcommon\ubchost.h"
#include "common\libinstall\installutil.h"
#include "HostExcelSave.h"
#include "DownloadStateDlg.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "ubccopcommon/SiteSelectDlg.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "AutoUpdateConfigDlg.h"
#include "RemoteCliCall.h"
#include "WOLDLG.h"
#include "ChngPasswordDlg.h"
#include "RemoteDeleteFileDlg.h"
#include "InputBox.h"
#include "IPInputDLG.h"
#include "RemoteLoginSettingsDlg.h"

#define STR_SEP				_T(",")

#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(0,128,255)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)

// CUpdateCenterView
IMPLEMENT_DYNCREATE(CUpdateCenterView, CFormView)

CUpdateCenterView::CUpdateCenterView() : CFormView(CUpdateCenterView::IDD)
,	m_Reposition(this)
,	m_pMainFrame(NULL)
{
	m_pBackupHostList = NULL;

	m_pMainFrame = (CMainFrame*)AfxGetMainWnd();
}

CUpdateCenterView::~CUpdateCenterView()
{
	DeleteBackupHostList();
}

void CUpdateCenterView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HOST, m_lcList);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_Filter);

	DDX_Control(pDX, IDC_BUTTON_HOST_REF, m_btnRefHost);
	DDX_Control(pDX, IDC_BUTTON_UPDATE_NOW, m_btnUpdateNow);
	DDX_Control(pDX, IDC_BUTTON_SET_TIME, m_btnSetTime);
	DDX_Control(pDX, IDC_BUTTON_HOST_CNGSITE, m_btnChnSite);
	DDX_Control(pDX, IDC_BUTTON_REMOTE_CLI_CALL, m_btnRemoteCall);
	DDX_Control(pDX, IDC_BUTTON_CHNG_OS_PW, m_btnHostOsChngPwd);
	DDX_Control(pDX, IDC_BUTTON_DELETE_FILE, m_btnDeleteFile);
	DDX_Control(pDX, IDC_BN_TO_EXCEL2, m_btnExcelSave);

	DDX_Control(pDX, IDC_COMBO_FILTER_AUTO_UPDATE, m_cbAutoUpdate);
	DDX_Control(pDX, IDC_COMBO_FILTER_ADMIN, m_cbAdmin);
	DDX_Control(pDX, IDC_COMBO_FILTER_OPR, m_cbOperation);
	DDX_Control(pDX, IDC_BUTTON_FILTER_HOSTTYPE, m_cbHostType);
	DDX_Control(pDX, IDC_COMBO_FILTER_TAG2, m_cbFilterTag);
	DDX_Control(pDX, IDC_KB_ADVANCED_FILTER, m_kbAdvancedFilter);
	DDX_Control(pDX, IDC_COMBO_ADVANCED_FILTER2, m_cbAdvancedFilter);
	DDX_Control(pDX, IDC_BN_ADDTAG, m_btAddTags);
	DDX_Control(pDX, IDC_BN_DELTAG, m_btDelTags);
	DDX_Control(pDX, IDC_BUTTON_WOL, m_btnWOL);
}

BEGIN_MESSAGE_MAP(CUpdateCenterView, CFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN, OnRemotelogin)
	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN2, OnRemotelogin2) // 0001442: 원격접속 방법에 서버를 경유하지 않는 vnc원격접속을 만든다.
	ON_COMMAND(ID_HOSTLIST_REMOTELOGIN3, OnRemotelogin3) // 원격접속 방법에 지정된 서버를 경유하는 vnc원격접속을 만든다.
	ON_COMMAND(ID_HOSTLIST_REMOTE_DESKTOP, OnRemoteDesktop)

	ON_BN_CLICKED(IDC_BUTTON_HOST_REF, OnBnClickedButtonHostrfs)
	ON_BN_CLICKED(IDC_BUTTON_HOST_CNGSITE, OnBnClickButtonHostChgSite)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL2, &CUpdateCenterView::OnBnClickedBnToExcel2)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SITE, OnBnClickedButtonFilterSite)
	ON_REGISTERED_MESSAGE(WM_FILTER_HOST_CHANGE, OnFilterHostChanged)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE_NOW, &CUpdateCenterView::OnBnClickedButtonUpdateNow)
	ON_BN_CLICKED(IDC_BUTTON_SET_TIME, &CUpdateCenterView::OnBnClickedButtonSetTime)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_HOST, &CUpdateCenterView::OnNMDblclkListHost)
	ON_BN_CLICKED(IDC_BUTTON_REMOTE_CLI_CALL, &CUpdateCenterView::OnBnClickedButtonRemoteCliCall)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_HOST, &CUpdateCenterView::OnNMRclickListHost)
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
	ON_BN_CLICKED(IDC_BUTTON_CHNG_OS_PW, &CUpdateCenterView::OnBnClickedButtonChngOsPw)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_FILE, &CUpdateCenterView::OnBnClickedButtonDeleteFile)
	ON_BN_CLICKED(IDC_KB_ADVANCED_FILTER, &CUpdateCenterView::OnBnClickedKbAdvancedFilter)
	ON_BN_CLICKED(IDC_BN_ADDTAG, &CUpdateCenterView::OnBnClickedBnAddtag)
	ON_BN_CLICKED(IDC_BN_DELTAG, &CUpdateCenterView::OnBnClickedBnDeltag)
	ON_BN_CLICKED(IDC_BUTTON_WOL, &CUpdateCenterView::OnBnClickedButtonWol)
	ON_COMMAND(ID_REMOTE_LOGIN_SETTINGS, &CUpdateCenterView::OnRemoteLoginSettings)
END_MESSAGE_MAP()

// CUpdateCenterView diagnostics
#ifdef _DEBUG
void CUpdateCenterView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CUpdateCenterView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CUpdateCenterView message handlers
int CUpdateCenterView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);
//	GetEnvPtr()->InitSite();

	return 0;
}

void CUpdateCenterView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CodeItemList listHostType;
	CUbcCode::GetInstance()->GetCategoryInfo(_T("HostType"), listHostType);

	POSITION pos = listHostType.GetHeadPosition();
	while(pos)
	{
		SCodeItem info = listHostType.GetNext(pos);
		m_cbHostType.AddString(info.strEnumString, info.nEnumNumber);
	}

	CRect rc;
	m_cbHostType.GetWindowRect(&rc);
	m_cbHostType.SetDroppedWidth(rc.Width() * 1.5);

	m_cbAutoUpdate.AddString("All");
	m_cbAutoUpdate.AddString(LoadStringById(IDS_HOSTVIEW_LIST043));
	m_cbAutoUpdate.AddString(LoadStringById(IDS_HOSTVIEW_LIST044));
	m_cbAutoUpdate.SetCurSel(0);

	m_cbAdmin.AddString("All");
	m_cbAdmin.AddString(LoadStringById(IDS_HOSTVIEW_LIST001));
	m_cbAdmin.AddString(LoadStringById(IDS_HOSTVIEW_LIST002));
	m_cbAdmin.SetCurSel(0);

	m_cbOperation.AddString("All");
	m_cbOperation.AddString(LoadStringById(IDS_HOSTVIEW_LIST003));
	m_cbOperation.AddString(LoadStringById(IDS_HOSTVIEW_LIST004));
	m_cbOperation.SetCurSel(0);

	CStringArray aTagList;
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_btnRefHost.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnUpdateNow.LoadBitmap(IDB_BUTTON_AUTOUPDATE_NOW, RGB(255,255,255));
	m_btnSetTime.LoadBitmap(IDB_BUTTON_AUTOUPDATE_TIME, RGB(255,255,255));
	m_btnChnSite.LoadBitmap(IDB_BUTTON_CNGSITE, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));
	m_btnRemoteCall.LoadBitmap(IDB_BUTTON_REMOTE_CMD, RGB(255,255,255));
	m_btnWOL.LoadBitmap(IDB_BITMAP_POWERON, RGB(255,255,255));
	m_btnHostOsChngPwd.LoadBitmap(IDB_BUTTON_HOST_CHNG_PWD, RGB(255,255,255));
	m_btnDeleteFile.LoadBitmap(IDB_BUTTON_DELETE_FILE, RGB(255,255,255));
	m_btAddTags.LoadBitmap(IDB_BUTTON_ADDTAGS, RGB(255,255,255));
	m_btDelTags.LoadBitmap(IDB_BUTTON_DELTAGS, RGB(255,255,255));

	m_btnRefHost.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN004));
	m_btnUpdateNow.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN016));
	m_btnSetTime.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN017));
	m_btnChnSite.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN010));
	m_btnExcelSave.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN012));
	m_btAddTags.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN019));
	m_btDelTags.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN020));

	// 권한에 따라 활성화 한다
	m_btnRefHost.EnableWindow(IsAuth(_T("UQRY")));
	m_btnUpdateNow.EnableWindow(IsAuth(_T("UMOD")));
	m_btnSetTime.EnableWindow(IsAuth(_T("UMOD")));
	m_btnChnSite.EnableWindow(IsAuth(_T("HGMD")));

	m_btnRemoteCall.ShowWindow(IsAuth(_T("HRMC")));
	m_btnRemoteCall.EnableWindow(IsAuth(_T("HRMC")));

	m_btnWOL.ShowWindow(IsAuth(_T("UMOD")));
	m_btnWOL.EnableWindow(IsAuth(_T("UMOD")));

	m_btnHostOsChngPwd.ShowWindow(IsAuth(_T("HRMC")));
	m_btnHostOsChngPwd.EnableWindow(IsAuth(_T("HRMC")));

	m_btnDeleteFile.ShowWindow(IsAuth(_T("HRMC")));
	m_btnDeleteFile.EnableWindow(IsAuth(_T("HRMC")));

	m_btAddTags.EnableWindow(IsAuth(_T("UMOD")));
	m_btDelTags.EnableWindow(IsAuth(_T("UMOD")));

	LoadFilterData();

	UpdateData(FALSE);

	InitPosition(m_rcClient);

	InitList();

//	RefreshHost(false);

	SetScrollSizes(MM_TEXT, CSize(1,1));

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcList.LoadColumnState("HOST-LIST", szPath, eHostID);
}

void CUpdateCenterView::OnClose()
{
	CFormView::OnClose();
}

void CUpdateCenterView::OnDestroy()
{
	CFormView::OnDestroy();

	if(m_pMainFrame)
	{
		m_pMainFrame->SendMessage(WM_SET_STATUSBAR_TEXT, (WPARAM)SBID_HOST, (LPARAM)(LPCTSTR)_T(""));
	}

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lcList.SaveColumnState("HOST-LIST", szPath);
}

void CUpdateCenterView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lcList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_Filter, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
}

void CUpdateCenterView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CUpdateCenterView::InitList()
{
	m_szHostColum[eCheck] = _T("");	// 2010.02.17 by gwangsoo
	m_szHostColum[eHostVNCStat] = LoadStringById(IDS_HOSTVIEW_LIST033);
	m_szHostColum[eMonitorStat] = LoadStringById(IDS_HOSTVIEW_LIST041);
	m_szHostColum[eHostType] = LoadStringById(IDS_HOSTVIEW_LIST042);
	m_szHostColum[eSiteName] = LoadStringById(IDS_HOSTVIEW_LIST007);
	m_szHostColum[eHostName] = LoadStringById(IDS_HOSTVIEW_LIST009);
	m_szHostColum[eTag] = LoadStringById(IDS_HOSTVIEW_LIST036);
	m_szHostColum[eVersion] = LoadStringById(IDS_HOSTVIEW_LIST034);
	m_szHostColum[eAutoUpdate] = LoadStringById(IDS_HOSTVIEW_LIST045);
	m_szHostColum[eDownloadTime] = LoadStringById(IDS_HOSTVIEW_LIST035);
	m_szHostColum[eHostID] = LoadStringById(IDS_HOSTVIEW_LIST008);
	m_szHostColum[eHostIP] = LoadStringById(IDS_HOSTVIEW_LIST016);
	m_szHostColum[eHostMacAddr] = LoadStringById(IDS_HOSTVIEW_LIST019);
	m_szHostColum[eHostEdition] = LoadStringById(IDS_HOSTVIEW_LIST020);
	m_szHostColum[eHostBootUpTime] = LoadStringById(IDS_HOSTVIEW_LIST021);
	m_szHostColum[eHostUpDate] = LoadStringById(IDS_HOSTVIEW_LIST031);
	m_szHostColum[eHostDesc] = LoadStringById(IDS_HOSTVIEW_LIST015);
	m_szHostColum[eDisk1Avail] = LoadStringById(IDS_HOSTVIEW_LIST049);
	m_szHostColum[eDisk2Avail] = LoadStringById(IDS_HOSTVIEW_LIST050);
	m_szHostColum[eProtocolType] = LoadStringById(IDS_HOSTVIEW_LIST051);

	//m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP);

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilHostList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilHostList.Add(&bmCheck, RGB(255, 255, 255));
	m_lcList.SetImageList(&m_ilHostList, LVSIL_SMALL);

	for(int nCol = 0; nCol < eHostEnd; nCol++)
	{
		if(nCol == eDisk1Avail || nCol == eDisk2Avail)
		{
			m_lcList.InsertColumn(nCol, m_szHostColum[nCol], LVCFMT_RIGHT, 100);
		}
		else
		{
			m_lcList.InsertColumn(nCol, m_szHostColum[nCol], LVCFMT_LEFT, 100);
		}
	}

	m_lcList.SetColumnWidth(eCheck, 22); // 2010.02.17 by gwangsoo
	m_lcList.SetColumnWidth(eHostVNCStat, 15);
	m_lcList.SetColumnWidth(eMonitorStat, 15);

	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CUpdateCenterView::RefreshHostList()
{
	TraceLog(("RefreshHostList begin"));

	int nRow = 0;
	LVITEM item;
	CString szBuf;
	POSITION pos = m_lsHost.GetHeadPosition();
	m_lcList.DeleteAllItems();

	while(pos)
	{
		POSITION posOld = pos;
		SHostInfo info = m_lsHost.GetNext(pos);

		m_lcList.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, &info);

		m_lcList.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lcList.GetCurSortCol();
	if(-1 != nCol)
		m_lcList.SortList(nCol, m_lcList.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);
	else
		m_lcList.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lcList);

	TraceLog(("RefreshHostList end"));
}

void CUpdateCenterView::OnBnClickedButtonHostrfs()
{
	DeleteBackupHostList();
	RefreshHost();

	SaveFilterData();

	// 같은 필터를 사용하는 윈도우에 통지
	((CMainFrame*)AfxGetMainWnd())->UpdateFilterData(this);
}

BOOL CUpdateCenterView::RefreshHost(bool bCompMsg)
{
	TraceLog(("RefreshHost begin"));
	CWaitMessageBox wait;

	CString szSite = (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite);

	UpdateData();

	CString strHostName  ;
	CString strHostId    ;
	CString strHostType  ;
	CString strSiteName  ;
	CString strSiteId    ;
	CString strVersion   ;
	CString strAutoUpdate;
	CString strAdminStat ;
	CString strOperaStat ;
	CString strCategory  ;
	CString strAdvancedFilter;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_VERSION  , strVersion     );
	strAutoUpdate= ToString(m_cbAutoUpdate.GetCurSel());
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);
	if(m_kbAdvancedFilter.GetCheck()) m_cbAdvancedFilter.GetWindowText(strAdvancedFilter);

	// 고급필터에 select,(,) 문자사용하지 못하도록함.
	if(!strAdvancedFilter.IsEmpty())
	{
		CString strTmp = strAdvancedFilter;
		strTmp.MakeUpper();
		if(strTmp.Find("SELECT") >= 0 || strTmp.FindOneOf("()") >= 0)
		{
			UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG021));
			return FALSE;
		}
	}

	CString strWhere;

	// 단말이름
	strHostName.Trim();
	if(!strHostName.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostName like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostName
							);
	}

	// 단말ID
	strHostId.Trim();
	if(!strHostId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s hostId like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostId
							);
	}

	// 단말타입
	if(!strHostType.IsEmpty())
	{
		strHostType.Replace(",", "','");
		strWhere.AppendFormat(_T(" %s hostType in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strHostType
							);
	}

	// 소속조직
	if(!strSiteId.IsEmpty())
	{
		strSiteId.Replace(_T(","), _T("','"));
		strWhere.AppendFormat(_T(" %s siteId in ('%s')")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strSiteId
							);
	}

	// 버전
	strVersion.Trim();
	if(!strVersion.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s version like '%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strVersion
							);
	}

	// 자동업데이트여부
	if(_ttoi(strAutoUpdate) > 0)
	{
		strWhere.AppendFormat(_T(" %s autoUpdateFlag = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAutoUpdate)==1 ? 1:0)
							);
	}

	// 연결상태
	if(_ttoi(strOperaStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s operationalState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strOperaStat)==1 ? 1:0)
							);
	}

	// 사용상태
	if(_ttoi(strAdminStat) > 0)
	{
		strWhere.AppendFormat(_T(" %s adminState = %d")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, (_ttoi(strAdminStat)==1 ? 1:0)
							);
	}

	// 검색어
	strCategory.Trim();
	if(!strCategory.IsEmpty())
	{
		int pos = 0;
		CString strTag;
		while((strTag = strCategory.Tokenize("/", pos)) != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', category, '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s category like '%%%s%%'")
							 	 , (strWhere.IsEmpty() ? _T("") : _T("and"))
								 , strTag
								 );
		}
	}

	strAdvancedFilter.Trim();
	if(!strAdvancedFilter.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s (%s) ")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strAdvancedFilter
							);
	}

	//BOOL bRet = GetEnvPtr()->RefreshHost(m_lsHost, szSite, (strWhere.IsEmpty() ? NULL : strWhere));
	m_lsHost.RemoveAll();
	cciCall aCall;
	BOOL bRet = CCopModule::GetObject()->GetHostForCenter(&aCall, szSite, "*", (strWhere.IsEmpty() ? NULL : strWhere));
	if(bRet)
	{
		bRet = CCopModule::GetObject()->GetHostData(&aCall, m_lsHost);
	}

	RefreshHostList();

	TraceLog(("RefreshHost end"));

	if(!bRet)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG007), MB_ICONINFORMATION);
		return FALSE;
	}

	if(bCompMsg)
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_HOSTVIEW_MSG008), m_lcList.GetItemCount());
		UbcMessageBox(strMsg, MB_ICONINFORMATION);
	}

	return bRet;
}

// 0001389: [RFP] 단말 OS 패스워드 변경 기능
void CUpdateCenterView::OnBnClickButtonHostChgSite()
{
	CList<int> lsRow;
	CStringList lsHost;
	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);

		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost)	continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		CString szHost = _T("PM=*/Site=");
		szHost += Info.siteId;
		szHost += _T("/Host=");
		szHost += Info.hostId;

		lsHost.AddTail(szHost);
	}

	if(lsRow.GetCount() == 0){
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CChangeSiteDlg dlg;
	if(dlg.DoModal() != IDOK) return;

	CCopModule::GetObject()->ChangeSite(dlg.m_szSelSiteId, lsHost, dlg.m_bReboot);

	RefreshHost(false);
}

LRESULT CUpdateCenterView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case IEH_OPSTAT:
// cli >>> post PM=2/Site=SQI/Host=SQI-06112 operationalStateChanged siteId="SQI", hostId="SQI-06116", operationalState=true
		return ChangeOPStat((SOpStat*)lParam);

	case IEH_ADSTAT:
		return ChangeAdStat((SAdStat*)lParam);

	case IEH_CHNGVNC:
		return ChangeVncStat((cciEvent*)lParam);

		// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	case IEH_MOSTAT:
		return ChangeMonitorStat((SMonitorStat*)lParam);

	case IEH_HDDCHNG:
		return ChangeDiskAvail((SDiskAvailChanged*)lParam);

	case IEH_UNKOWN:
		//cciAttributeList* pList = (cciAttributeList*)lParam;

		//for(int i = 0; i < (int)pList->length(); i++){
		//	cciAttribute& attr = (*pList)[i];

		//	if(strcmp(attr.getName(),"siteId") == 0){
		//		attr.getValue(strSite);
		//	}
		//}
		break;
	}
	return 0;
}

int CUpdateCenterView::ChangeOPStat(SOpStat* pOpStat)
{
	for(int nRow = 0; nRow < m_lcList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pOpStat->siteId || Info.hostId != pOpStat->hostId)
			continue;

		TraceLog(("operationalState : %s", pOpStat->operationalState?"true":"false"));
		Info.operationalState = pOpStat->operationalState;
		m_lsHost.GetAt(pos) = Info;

//		RefreshHostList();
//		m_lcList.Invalidate();
		UpdateListRow(nRow, &Info);
		return 0;
	}

	return 0;
}

int CUpdateCenterView::ChangeAdStat(SAdStat* pAdStat)
{
	for(int nRow = 0; nRow < m_lcList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pAdStat->siteId || Info.hostId != pAdStat->hostId)
			continue;

		Info.adminState = pAdStat->adminState;
		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info);

		//LVITEM item;
		//item.iItem = nRow;
		//item.iSubItem = m_lcList.GetColPos(m_szHostColum[eSiteName]);
		//item.mask = LVIF_IMAGE;
		//item.iImage = (Info.adminState ? eMonitorOn : eMonitorOff);
		//m_lcList.SetItem(&item);
//		m_lcList.Invalidate();
		return 0;
	}

	return 0;
}

int CUpdateCenterView::ChangeVncStat(cciEvent* ev)
{
	ciString hostId;
	ciBoolean vncState;

	ev->getItem("hostId", hostId);
	ev->getItem("vncState", vncState);

	for(int nRow = 0; nRow < m_lcList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo info = m_lsHost.GetAt(pos);
		if(info.hostId != hostId.c_str()) continue;

		m_lsHost.GetAt(pos).vncState = vncState;

		LVITEM item;
		item.iItem = nRow;
		item.iSubItem = eHostVNCStat;//m_lcList.GetColPos(m_szHostColum[eHostVNCStat]);
		item.mask = LVIF_IMAGE|LVIF_TEXT;
		item.pszText = (info.vncState || info.operationalState ? _T("On") : _T("Off"));
		item.iImage = (info.vncState || info.operationalState ? eVncOn : eVncOff);
		m_lcList.SetItem(&item);

		break;
	}

	return 0;
}

void CUpdateCenterView::UpdateListRow(int nRow, SHostInfo* pInfo)
{
	if(!pInfo || m_lcList.GetItemCount() < nRow)
		return;

	// 0000707: 콘텐츠 다운로드 상태 조회 기능
    COLORREF crTextBk = COLOR_WHITE;
	if(IsJustNowHost(pInfo->hostId))
	{
		crTextBk = COLOR_LILAC;
	}
	else if(pInfo->operationalState)
	{
		crTextBk = COLOR_YELLOW;
	}
	else
	{
		crTextBk = COLOR_GRAY;
	}
	m_lcList.SetRowColor(nRow, crTextBk, m_lcList.GetTextColor());

	CString szBuf;

	m_lcList.SetItemText(nRow, eSiteName, pInfo->siteName);
	m_lcList.SetItemText(nRow, eHostID, pInfo->hostId);
	m_lcList.SetItemText(nRow, eHostName, pInfo->hostName);
	m_lcList.SetItemText(nRow, eHostType, CUbcCode::GetInstance()->GetCodeName(_T("HostType"), pInfo->hostType));
	m_lcList.SetItemText(nRow, eVersion, pInfo->version);
	m_lcList.SetItemText(nRow, eAutoUpdate, (pInfo->autoUpdateFlag?LoadStringById(IDS_HOSTVIEW_LIST043):LoadStringById(IDS_HOSTVIEW_LIST044)));
	m_lcList.SetItemText(nRow, eDownloadTime, (pInfo->contentsDownloadTime == _T("NOSET") ? _T("") : pInfo->contentsDownloadTime));
	m_lcList.SetItemText(nRow, eHostDesc, pInfo->description);
	m_lcList.SetItemText(nRow, eHostIP, pInfo->ipAddress);
	m_lcList.SetItemText(nRow, eHostMacAddr, pInfo->macAddress);
	m_lcList.SetItemText(nRow, eHostEdition, pInfo->edition);
	m_lcList.SetItemText(nRow, eHostBootUpTime, (pInfo->bootUpTime.GetTime()==0?_T(""):pInfo->bootUpTime.Format(STR_ENV_TIME)));
	m_lcList.SetItemText(nRow, eHostUpDate, pInfo->lastUpdateTime.Format(STR_ENV_TIME));

	//CString strCategory;
	//GetDlgItemText(IDC_EDIT_FILTER_TAG, strCategory);
	//m_lcList.SetItemText(nRow, eTag, (strCategory.IsEmpty() ? pInfo->category : strCategory));
	m_lcList.SetItemText(nRow, eTag, pInfo->category);

	// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
	//bool bMonitorStat = utvUtil::getInstance()->monitorState(pInfo->monitorOff, pInfo->monitorOffList);
	int nMonitorImgIdx = 2;
	if(pInfo->operationalState)
	{
		if(pInfo->monitorState > 0) nMonitorImgIdx = eMonitorOn;
		if(pInfo->monitorState == 0) nMonitorImgIdx = eMonitorOff;
		if(pInfo->monitorState < 0) nMonitorImgIdx = eMonitorUnknown;
	}

	m_lcList.SetItemText(nRow, eDisk1Avail, (pInfo->disk1Avail < 0 ? "" : ToString((double)pInfo->disk1Avail, 2)+_T(" G")));
	m_lcList.SetItemText(nRow, eDisk2Avail, (pInfo->disk2Avail < 0 ? "" : ToString((double)pInfo->disk2Avail, 2)+_T(" G")));

	m_lcList.SetItemText(nRow, eProtocolType, pInfo->protocolType);

	LVITEM itemMonitorStat;
	itemMonitorStat.iItem = nRow;
	itemMonitorStat.iSubItem = eMonitorStat;//m_lcList.GetColPos(m_szHostColum[eMonitorStat]);
	itemMonitorStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemMonitorStat.pszText = (nMonitorImgIdx == eMonitorUnknown ? _T("Unknown") : (nMonitorImgIdx == eMonitorOn ? _T("On") : _T("Off")));
	itemMonitorStat.iImage = nMonitorImgIdx;
	m_lcList.SetItem(&itemMonitorStat);

	LVITEM itemHostVNCStat;
	itemHostVNCStat.iItem = nRow;
	itemHostVNCStat.iSubItem = eHostVNCStat;//m_lcList.GetColPos(m_szHostColum[eHostVNCStat]);
	itemHostVNCStat.mask = LVIF_IMAGE|LVIF_TEXT;
	itemHostVNCStat.pszText = (pInfo->vncState || pInfo->operationalState ? _T("On") : _T("Off"));
	itemHostVNCStat.iImage = (pInfo->vncState || pInfo->operationalState ? eVncOn : eVncOff);
	m_lcList.SetItem(&itemHostVNCStat);
}

void CUpdateCenterView::BackupHostList()
{
	DeleteBackupHostList();

	m_pBackupHostList = new CMapStringToString();

	for(int nRow = 0; nRow < m_lcList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo info = m_lsHost.GetAt(pos);
		m_pBackupHostList->SetAt(info.hostId, info.hostId);
	}
}

void CUpdateCenterView::DeleteBackupHostList()
{
	if(m_pBackupHostList) delete m_pBackupHostList;
	m_pBackupHostList = NULL;
}

bool CUpdateCenterView::IsJustNowHost(CString strHostId)
{
	if(!m_pBackupHostList) return false;

	CString strValue;
	m_pBackupHostList->Lookup(strHostId, strValue);

	return strValue.IsEmpty();
}

void CUpdateCenterView::OnBnClickedBnToExcel2()
{
	CHostExcelSave dlg;

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_HOSTVIEW_STR001)
									, m_lcList
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}

void CUpdateCenterView::OnBnClickedButtonFilterSite()
{
	CString strSiteName;
	CString strSiteId;

	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() == IDOK)
	{
		SiteInfoList siteInfoList;
		if(dlg.GetSelectSiteInfoList(siteInfoList))
		{
			POSITION pos = siteInfoList.GetHeadPosition();
			while(pos)
			{
				SSiteInfo info = siteInfoList.GetNext(pos);
				strSiteName += (strSiteName.IsEmpty() ? _T("") : _T(",")) + info.siteName;
				strSiteId += (strSiteId.IsEmpty() ? _T("") : _T(",")) + info.siteId;
			}
		}
	}

	SetDlgItemText(IDC_EDIT_FILTER_SITENAME, strSiteName);
	m_strSiteId = strSiteId;
}

void CUpdateCenterView::SaveFilterData()
{
	CString strHostName  ;
	CString strHostId    ;
	CString strHostType  ;
	CString strSiteName  ;
	CString strSiteId    ;
	CString strVersion   ;
	CString strAutoUpdate;
	CString strAdminStat ;
	CString strOperaStat ;
	CString strCategory  ;
	CString strAdvancedFilter;

	GetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	GetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	strHostType = m_cbHostType.GetCheckedIDs();
	if(strHostType == _T("All")) strHostType = _T("");
	else if(strHostType.GetLength() >= 2) strHostType = strHostType.Mid(1, strHostType.GetLength()-2);
	GetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	strSiteId = m_strSiteId;
	GetDlgItemText(IDC_EDIT_FILTER_VERSION  , strVersion     );
	strAutoUpdate= ToString(m_cbAutoUpdate.GetCurSel());
	strAdminStat = ToString(m_cbAdmin.GetCurSel());
	strOperaStat = ToString(m_cbOperation.GetCurSel());
	//GetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.GetWindowText(strCategory);
	if(m_kbAdvancedFilter.GetCheck()) m_cbAdvancedFilter.GetWindowText(strAdvancedFilter);

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	WritePrivateProfileString("HOST-FILTER", "HostName"  , strHostName  , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostID"    , strHostId    , szPath);
	WritePrivateProfileString("HOST-FILTER", "HostType"  , strHostType  , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteName"  , strSiteName  , szPath);
	WritePrivateProfileString("HOST-FILTER", "SiteID"    , strSiteId    , szPath);
	WritePrivateProfileString("HOST-FILTER", "AdminStat" , strAdminStat , szPath);
	WritePrivateProfileString("HOST-FILTER", "OperaStat" , strOperaStat , szPath);
	WritePrivateProfileString("HOST-FILTER", "Category"  , strCategory  , szPath);
	WritePrivateProfileString("HOST-FILTER", "Version"   , strVersion   , szPath);
	WritePrivateProfileString("HOST-FILTER", "AutoUpdate", strAutoUpdate, szPath);
	WritePrivateProfileString("HOST-FILTER", "AdvancedFilter", strAdvancedFilter, szPath);

	if(m_kbAdvancedFilter.GetCheck() && !strAdvancedFilter.IsEmpty() && m_cbAdvancedFilter.FindStringExact(0, strAdvancedFilter) < 0) 
	{
		m_cbAdvancedFilter.InsertString(0, strAdvancedFilter);
		for(int i=m_cbAdvancedFilter.GetCount(); i > 20 ; i--)
		{
			m_cbAdvancedFilter.DeleteString(i-1);
		}

		for(int i=0; i<m_cbAdvancedFilter.GetCount() ;i++)
		{
			CString strText;
			m_cbAdvancedFilter.GetLBText(i, strText);

			CString strKey;
			strKey.Format(_T("History%d"), i+1);
			WritePrivateProfileString("HOST-FILTER", strKey, strText, szPath);
		}
	}
}

void CUpdateCenterView::LoadFilterData()
{
	CString strHostName  ;
	CString strHostId    ;
	CString strHostType  ;
	CString strSiteName  ;
	CString strSiteId    ;
	CString strVersion   ;
	CString strAutoUpdate;
	CString strAdminStat ;
	CString strOperaStat ;
	CString strCategory  ;
	CString strAdvancedFilter;

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("HOST-FILTER", "HostName"  , "", buf, 1024, szPath); strHostName   = buf;
	GetPrivateProfileString("HOST-FILTER", "HostID"    , "", buf, 1024, szPath); strHostId     = buf;
	GetPrivateProfileString("HOST-FILTER", "HostType"  , "", buf, 1024, szPath); strHostType   = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteName"  , "", buf, 1024, szPath); strSiteName   = buf;
	GetPrivateProfileString("HOST-FILTER", "SiteID"    , "", buf, 1024, szPath); strSiteId     = buf;
	GetPrivateProfileString("HOST-FILTER", "AdminStat" , "", buf, 1024, szPath); strAdminStat  = buf;
	GetPrivateProfileString("HOST-FILTER", "OperaStat" , "", buf, 1024, szPath); strOperaStat  = buf;
	GetPrivateProfileString("HOST-FILTER", "Category"  , "", buf, 1024, szPath); strCategory   = buf;
	GetPrivateProfileString("HOST-FILTER", "Version"   , "", buf, 1024, szPath); strVersion    = buf;
	GetPrivateProfileString("HOST-FILTER", "AutoUpdate", "", buf, 1024, szPath); strAutoUpdate = buf;
	GetPrivateProfileString("HOST-FILTER", "AdvancedFilter"   , "", buf, 1024, szPath); strAdvancedFilter = buf;

	SetDlgItemText(IDC_EDIT_FILTER_HOSTNAME , strHostName    );
	SetDlgItemText(IDC_EDIT_FILTER_HOSTID   , strHostId      );
	if(strHostType.IsEmpty())
	{
		m_cbHostType.CheckAll(TRUE);
	}
	else
	{
		int pos = 0;
		CString strCheck;
		while((strCheck = strHostType.Tokenize(",", pos)) != _T(""))
		{
			m_cbHostType.SetCheck(_ttoi(strCheck), TRUE);
		}
	}
	SetDlgItemText(IDC_EDIT_FILTER_SITENAME , strSiteName    );
	m_strSiteId = strSiteId;
	SetDlgItemText(IDC_EDIT_FILTER_VERSION  , strVersion     );
	m_cbAutoUpdate.SetCurSel(_ttoi(strAutoUpdate));
	m_cbAdmin.SetCurSel(_ttoi(strAdminStat));
	m_cbOperation.SetCurSel(_ttoi(strOperaStat));
	//SetDlgItemText(IDC_EDIT_FILTER_TAG      , strCategory    );
	m_cbFilterTag.SetWindowText(strCategory);
	m_kbAdvancedFilter.SetCheck(!strAdvancedFilter.IsEmpty());
	m_cbAdvancedFilter.EnableWindow(m_kbAdvancedFilter.GetCheck());
	m_cbAdvancedFilter.SetWindowText(strAdvancedFilter);

	for(int i=0; i<20 ;i++)
	{
		CString strKey;
		strKey.Format(_T("History%d"), i+1);
		GetPrivateProfileString("HOST-FILTER", strKey, "", buf, 1024, szPath);

		CString strValue = buf;
		if(!strValue.IsEmpty())
		{
			m_cbAdvancedFilter.AddString(strValue);
		}
	}
}

LRESULT CUpdateCenterView::OnFilterHostChanged(WPARAM wParam, LPARAM lParam)
{
	LoadFilterData();
	UpdateData(FALSE);
	return 0;
}

void CUpdateCenterView::OnBnClickedButtonUpdateNow()
{
	CList<int> lsRow;
	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG011), MB_ICONINFORMATION);
		return;
	}

	m_szMsg = LoadStringById(IDS_UPDATECENTER_MSG001);
	if(UbcMessageBox(m_szMsg, MB_ICONINFORMATION|MB_YESNO) != IDYES) return;

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow)
	{
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;

		m_lcList.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);
		CCopModule::GetObject()->AutoUpdate(Info.siteId, Info.hostId);
	}
}

void CUpdateCenterView::OnBnClickedButtonSetTime()
{
	CList<int> lsRow;
	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;
		lsRow.AddTail(nRow);
	}

	if(lsRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG011), MB_ICONINFORMATION);
		return;
	}

	CAutoUpdateConfigDlg dlg;
	dlg.m_bIsAutoUpdateFlag = BST_INDETERMINATE;

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	POSITION posRow = lsRow.GetHeadPosition();
	while(posRow)
	{
		int nRow = lsRow.GetNext(posRow);

		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;

		m_lcList.SetCheck(nRow, FALSE);
		SHostInfo Info = m_lsHost.GetAt(posHost);
		CCopModule::GetObject()->AutoUpdateConfig(Info.siteId, Info.hostId, dlg.m_bIsAutoUpdateFlag, dlg.m_strAutoUpdateTime);
	}

	RefreshHost(false);
}

void CUpdateCenterView::OnNMDblclkListHost(NMHDR *pNMHDR, LRESULT *pResult)
{

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
	if(!posHost) return;

	m_lcList.SetCheck(nRow, FALSE);
	SHostInfo& info = m_lsHost.GetAt(posHost);

	CAutoUpdateConfigDlg dlg;
	dlg.m_bIsAutoUpdateFlag = info.autoUpdateFlag;
	dlg.m_strAutoUpdateTime = info.contentsDownloadTime;

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	CCopModule::GetObject()->AutoUpdateConfig(info.siteId, info.hostId, dlg.m_bIsAutoUpdateFlag, dlg.m_strAutoUpdateTime);

	if(dlg.m_bIsAutoUpdateFlag != BST_INDETERMINATE)
	{
		info.autoUpdateFlag = dlg.m_bIsAutoUpdateFlag;
	}

	if(!dlg.m_strAutoUpdateTime.IsEmpty())
	{
		info.contentsDownloadTime = dlg.m_strAutoUpdateTime;
	}

	UpdateListRow(nRow, &info);
}

void CUpdateCenterView::OnBnClickedButtonRemoteCliCall()
{
	POSITION pos = m_cliDlgList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		CRemoteCliCall* dlg = m_cliDlgList.GetNext(pos);
		if(dlg && dlg->m_bIsPlaying == false){
			TraceLog(("WOLDLG END....WOLDLG DELETED....."));
			delete dlg;
			m_cliDlgList.RemoveAt(posOld);
			//UbcMessageBox("WOLDLG DELETED");
		}
	}

	CString msg;
	msg.Format("%d idle CRemoteCliCall exist", m_cliDlgList.GetSize());
	TraceLog((msg));


	CRemoteCliCall* pDlg = new CRemoteCliCall(this);

	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;
		
		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;

		SHostInfo Info = m_lsHost.GetAt(posHost);
		pDlg->m_lsSelHost.AddTail(Info);
	}

	if(pDlg->m_lsSelHost.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG011), MB_ICONINFORMATION);
		return;
	}

	//pDlg->DoModal();
	pDlg->Create(CRemoteCliCall::IDD, this);

	m_cliDlgList.AddTail(pDlg);

	pDlg->ShowWindow(SW_NORMAL);
}

void CUpdateCenterView::OnNMRclickListHost(NMHDR *pNMHDR, LRESULT *pResult)
{

	if(!IsAuth(_T("HVNC"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
	if(!pos) return;
	SHostInfo Info = m_lsHost.GetAt(pos);

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);

	pPopup = menu.GetSubMenu(2);
//	pPopup->EnableMenuItem(ID_HOSTLIST_REMOTELOGIN, Info.vncState);
	pPopup->EnableMenuItem(ID_HOSTLIST_REMOTELOGIN, (IsAuth(_T("HVNC")) ?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_HOSTLIST_REMOTE_DESKTOP, (IsAuth(_T("HVNC")) ?MF_ENABLED : MF_DISABLED | MF_GRAYED));

	CString strOsName = Info.os;
	strOsName.Trim();
	strOsName.MakeLower();
	if(!strOsName.IsEmpty() && strOsName.Find(_T("win")) < 0)
	{
		pPopup->DeleteMenu(ID_HOSTLIST_REMOTE_DESKTOP, MF_BYCOMMAND);
	}

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CUpdateCenterView::OnRemotelogin()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscList.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lcList.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // 창일향 : 원격 로그인은 텔넷으로 대체한다. (기존 UKIPA version 처럼)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(Info.siteId);
		if(!aData)	return;

		CString szPath;
#ifdef _DEBUG
		szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
		szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
		TraceLog((szPath));

		int nPos = Info.hostId.Find(_T("-"));
		CString strId = (nPos < 0 ? _T("") : Info.hostId.Mid(nPos+1));

		CString szParam = " /proxy ";
		szParam += aData->ipAddress.c_str();
		szParam += ":5901 ID:";
		//szParam += ToString(Info.vncPort);
		szParam += strId;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);
*/
		CMainFrame::RemoteLogin_viaServer(Info.siteId, Info.hostId);
	}
	else if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType){
		CString szPath = "";
		CString szParam = " ";
		szParam += Info.ipAddress;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, szPath, SW_SHOWNORMAL);
	}
}

void CUpdateCenterView::OnRemotelogin2()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscList.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lcList.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // 창일향 : 원격 로그인은 텔넷으로 대체한다. (기존 UKIPA version 처럼)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		CString szPath;
#ifdef _DEBUG
		szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
#else
		szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
#endif
		TraceLog((szPath));

		CString szParam;
		szParam.Format(_T("%s:%d"), Info.ipAddress, Info.vncPort);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);
*/
		CMainFrame::RemoteLogin_Direct(Info.ipAddress, Info.vncPort);
	}
	else if(CEnviroment::eUSTBType == GetEnvPtr()->m_StudioType){
		CString szPath = "";
		CString szParam = " ";
		szParam += Info.ipAddress;

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, szPath, SW_SHOWNORMAL);
	}
}
void CUpdateCenterView::OnRemotelogin3()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0)
		return;

//	TRACE("%d  %d   %s\n", m_ptSelList.x, m_ptSelList.y, m_lscHost.GetColText(m_ptSelList.x));
	POSITION pos = (POSITION)m_lcList.GetItemData(m_ptSelList.y);
	//POSITION pos = (POSITION)m_lscHost.GetItemData(m_ptSelList.y);
	if(!pos)	return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	
	if( CEnviroment::eADASSET  == GetEnvPtr()->m_Customer ) // 창일향 : 원격 로그인은 텔넷으로 대체한다. (기존 UKIPA version 처럼)
	{
		CString szParam = " ";
		szParam.Format(" %s 2233", Info.ipAddress);

		HINSTANCE nRet = ShellExecute(NULL, NULL, "telnet.exe", szParam, "", SW_SHOWNORMAL);
	}
	else if(CEnviroment::eUBCType == GetEnvPtr()->m_StudioType)
	{
/*
		ubcMuxData* aData = ubcMux::getInstance(muxFactorySelector::select())->getMuxData(Info.siteId);
		if(!aData) return;
		TraceLog((aData->ipAddress.c_str()));

		CIPInputDLG		aDlg;

		aDlg.setIpAddress(aData->ipAddress.c_str());

		if(aDlg.DoModal() == IDOK){

			CString repeaterIp = aDlg.getIpAddress();

			if(repeaterIp.IsEmpty()){
				return;
			}

			CString szPath;
	#ifdef _DEBUG
			szPath.Format(_T("%s\\%s"), GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);
	#else
			szPath.Format(_T("%s%s"), GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);
	#endif
			TraceLog((szPath));

			int nPos = Info.hostId.Find(_T("-"));
			CString strId = (nPos < 0 ? _T("") : Info.hostId.Mid(nPos+1));

			CString szParam = " /proxy ";
			szParam += repeaterIp;
			szParam += ":5901 ID:";
			//szParam += ToString(Info.vncPort);
			szParam += strId;

			HINSTANCE nRet = ShellExecute(NULL, NULL, "vncviewer.exe", szParam, szPath, SW_SHOWNORMAL);

		}
*/
		CMainFrame::RemoteLogin_viaSpecified(Info.siteId, Info.hostId);
	}
}

void CUpdateCenterView::OnRemoteDesktop()
{
	if(m_ptSelList.x < 0 || m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lcList.GetItemData(m_ptSelList.y);
	if(!pos) return;

	SHostInfo Info = m_lsHost.GetAt(pos);

	CString szParam;
	szParam.Format(_T(" /v %s"), Info.ipAddress);

	HINSTANCE nRet = ShellExecute(NULL, NULL, "mstsc.exe", szParam, "", SW_SHOWNORMAL);
}

// 0001371: [RFP] 실제로 모니터의 ON/OFF 상황을 보여주는 기능 (매니저 파트)
int CUpdateCenterView::ChangeMonitorStat(SMonitorStat* pMonitorStat)
{
	for(int nRow = 0; nRow < m_lcList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pMonitorStat->siteId || Info.hostId != pMonitorStat->hostId)
			continue;

		Info.monitorState = pMonitorStat->monitorState;
		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info);

		//LVITEM item;
		//item.iItem = nRow;
		//item.iSubItem = m_lcList.GetColPos(m_szHostColum[eSiteName]);
		//item.mask = LVIF_IMAGE;
		//item.iImage = (Info.adminState ? eMonitorOn : eMonitorOff);
		//m_lcList.SetItem(&item);
//		m_lcList.Invalidate();
		return 0;
	}

	return 0;
}

// 0001389: [RFP] 단말 OS 패스워드 변경 기능
void CUpdateCenterView::OnBnClickedButtonChngOsPw()
{
	HostInfoList lsHost;
	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		lsHost.AddTail(Info);
	}

	if(lsHost.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CChngPasswordDlg dlg;
	if(dlg.DoModal() != IDOK) return;

	POSITION pos = lsHost.GetHeadPosition();
	while(pos)
	{
		SHostInfo info = lsHost.GetNext(pos);
		CCopModule::GetObject()->SetWinPassword(info.siteId, info.hostId, GetEnvPtr()->m_szLoginID, dlg.m_strPassword);
	}

	// 화면에 보는 필드가 아니므로 다시 조회할 필요 없음
	//RefreshHost(false);
}

// 0001447: [RFP] 셋톱박스의 특정 파일 삭제 기능
void CUpdateCenterView::OnBnClickedButtonDeleteFile()
{
	POSITION pos = m_delDlgList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		CRemoteDeleteFileDlg* dlg = m_delDlgList.GetNext(pos);
		if(dlg && dlg->m_bIsPlaying == false){
			TraceLog(("WOLDLG END....WOLDLG DELETED....."));
			delete dlg;
			m_delDlgList.RemoveAt(posOld);
			//UbcMessageBox("WOLDLG DELETED");
		}
	}

	CString msg;
	msg.Format("%d idle WOLDLG exist", m_delDlgList.GetSize());
	TraceLog((msg));


	CRemoteDeleteFileDlg* pDlg = new CRemoteDeleteFileDlg(this);

	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;
		
		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;

		SHostInfo Info = m_lsHost.GetAt(posHost);
		pDlg->m_lsSelHost.AddTail(Info);
	}

	if(pDlg->m_lsSelHost.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG011), MB_ICONINFORMATION);
		return;
	}

	//pDlg->DoModal();
	pDlg->Create(CRemoteDeleteFileDlg::IDD, this);

	m_delDlgList.AddTail(pDlg);

	pDlg->ShowWindow(SW_NORMAL);
}

int CUpdateCenterView::ChangeDiskAvail(SDiskAvailChanged* pData)
{
	for(int nRow = 0; nRow < m_lcList.GetItemCount(); nRow++)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
		if(!pos) continue;

		SHostInfo Info = m_lsHost.GetAt(pos);

		if(Info.siteId != pData->siteId || Info.hostId != pData->hostId)
			continue;

		if(pData->diskId == 1)
		{
			Info.disk1Avail = pData->diskAvail;
		}
		else if(pData->diskId == 2)
		{
			Info.disk2Avail = pData->diskAvail;
		}
		else
		{
			return 0;
		}

		m_lsHost.GetAt(pos) = Info;

		UpdateListRow(nRow, &Info);

		return 0;
	}

	return 0;
}

void CUpdateCenterView::OnBnClickedKbAdvancedFilter()
{
	m_cbAdvancedFilter.EnableWindow(m_kbAdvancedFilter.GetCheck());
}

void CUpdateCenterView::OnBnClickedBnAddtag()
{
	int nSelCnt = 0;
	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;

		nSelCnt++;
	}

	if(nSelCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CInputBox dlg;
	dlg.m_strTitle = LoadStringById(IDS_HOSTVIEW_STR010);

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	// 검색어
	CStringArray aTagList;

	int pos = 0;
	CString strTag;
	while((strTag = dlg.m_strInputText.Tokenize("/", pos)) != _T(""))
	{
		aTagList.Add(strTag);
	}

	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		CString strLowerCategory = "/" + Info.category + "/";
		strLowerCategory.MakeLower();
		for(int i=0; i<aTagList.GetCount(); i++)
		{
			CString strLowerTag = aTagList[i];
			strLowerTag.MakeLower();
			if(strLowerCategory.Find( "/" + strLowerTag + "/" ) < 0)
			{
				Info.category += "/" + aTagList[i];
			}
		}

		Info.category.Trim("/");

		CCopModule::GetObject()->SetCategory(Info, GetEnvPtr()->m_szLoginID, Info.category);
	}

	m_cbFilterTag.GetWindowText(strTag);

	// 검색어 콤보 초기화
	aTagList.RemoveAll();
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_cbFilterTag.SetWindowText(strTag);

	RefreshHost(false);
}

void CUpdateCenterView::OnBnClickedBnDeltag()
{
	int nSelCnt = 0;
	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;

		nSelCnt++;
	}

	if(nSelCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG013), MB_ICONINFORMATION);
		return;
	}

	CInputBox dlg;
	dlg.m_strTitle = LoadStringById(IDS_HOSTVIEW_STR011);

	if(dlg.DoModal() != IDOK) return;

	CWaitMessageBox wait;

	// 검색어
	CStringArray aTagList;

	int pos = 0;
	CString strTag;
	while((strTag = dlg.m_strInputText.Tokenize("/", pos)) != _T(""))
	{
		aTagList.Add(strTag);
	}

	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;

		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;
		SHostInfo Info = m_lsHost.GetAt(posHost);

		CString strLowerCategory = "/" + Info.category + "/";
		strLowerCategory.MakeLower();
		for(int i=0; i<aTagList.GetCount(); i++)
		{
			CString strLowerTag = aTagList[i];
			strLowerTag.MakeLower();

			int nFind = strLowerCategory.Find( "/" + strLowerTag + "/" );
			if(nFind >= 0)
			{
				Info.category.Delete(nFind, strLowerTag.GetLength() + 1);
				strLowerCategory.Delete(nFind, strLowerTag.GetLength() + 1);
			}
		}

		Info.category.Trim("/");

		CCopModule::GetObject()->SetCategory(Info, GetEnvPtr()->m_szLoginID, Info.category);
	}

	m_cbFilterTag.GetWindowText(strTag);

	// 검색어 콤보 초기화
	aTagList.RemoveAll();
	CCopModule::GetObject()->GetAllCategory(aTagList);
	m_cbFilterTag.ResetContent();
	for(int i=0; i<aTagList.GetCount() ;i++)
	{
		m_cbFilterTag.AddString(aTagList[i]);
	}

	m_cbFilterTag.SetWindowText(strTag);

	RefreshHost(false);
}

void CUpdateCenterView::OnBnClickedButtonWol()
{
	POSITION pos = m_wolDlgList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		CWOLDLG* dlg = m_wolDlgList.GetNext(pos);
		if(dlg && dlg->m_bIsPlaying == false){
			TraceLog(("WOLDLG END....WOLDLG DELETED....."));
			delete dlg;
			m_wolDlgList.RemoveAt(posOld);
			//UbcMessageBox("WOLDLG DELETED");
		}
	}

	CString msg;
	msg.Format("%d idle WOLDLG exist", m_wolDlgList.GetSize());
	TraceLog((msg));

	CWOLDLG* pDlg = new CWOLDLG(this);
	//CWOLDLG pDlg(this);

	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		if(!m_lcList.GetCheck(nRow)) continue;
		
		POSITION posHost = (POSITION)m_lcList.GetItemData(nRow);
		if(!posHost) continue;

		SHostInfo Info = m_lsHost.GetAt(posHost);
		pDlg->m_lsSelHost.AddTail(Info);
		//pDlg.m_lsSelHost.AddTail(Info);
	}

	if(pDlg->m_lsSelHost.GetCount() == 0)
	//if(pDlg.m_lsSelHost.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG011), MB_ICONINFORMATION);
		return;
	}

	//pDlg.DoModal();
	pDlg->Create(CWOLDLG::IDD, this);

	m_wolDlgList.AddTail(pDlg);

	pDlg->ShowWindow(SW_NORMAL);

	pDlg->OnBnClickedBnRefresh();
	//TraceLog(("WOL END"));
}

void CUpdateCenterView::OnRemoteLoginSettings()
{
	CRemoteLoginSettingsDlg dlg;
	dlg.DoModal();
}
