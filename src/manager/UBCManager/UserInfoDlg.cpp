// CUserInfoDlg.cpp : implementation file
//
#include "stdafx.h"
#include "UBCManager.h"
#include "UserInfoDlg.h"
#include "Enviroment.h"
#include "ubccopcommon\MultiSelectHost.h"
#include "ubccopcommon\SiteSelectDlg.h"

// CUserInfoDlg dialog
IMPLEMENT_DYNAMIC(CUserInfoDlg, CDialog)

CUserInfoDlg::CUserInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUserInfoDlg::IDD, pParent)
{
}

CUserInfoDlg::~CUserInfoDlg()
{
}

void CUserInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_COMBO_PERMIS, m_cbPerm);
	DDX_Control(pDX, IDC_COMBO_ROLE, m_cbRole);

	DDX_Control(pDX, IDC_BUTTON_FAULT, m_cbFault);
	DDX_Control(pDX, IDC_CHECK_FAULT_SMS, m_kbFaultSMS);
	DDX_Control(pDX, IDC_CHECK_FAULT_EMAIL, m_kbFaultEMAIL);
	DDX_Control(pDX, IDC_LC_SITE_LIST, m_lcSiteList);
	DDX_Control(pDX, IDC_LC_HOST_LIST, m_lcHostList);
	DDX_Control(pDX, IDC_DT_VALIDATIONDATE, m_dtValidationDate);
	DDX_Control(pDX, IDC_DT_LASTPWCHANGETIME, m_dtLastPWChangeTime);
	DDX_Control(pDX, IDC_DT_LASTLOGINTIME, m_dtLastLoginTime);
}

BEGIN_MESSAGE_MAP(CUserInfoDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CUserInfoDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_SITE_LIST, &CUserInfoDlg::OnBnClickedButtonSiteList)
	ON_BN_CLICKED(IDC_BUTTON_HOST_LIST, &CUserInfoDlg::OnBnClickedButtonHostList)
	ON_BN_CLICKED(IDC_BUTTON_SITE_DEL, &CUserInfoDlg::OnBnClickedButtonSiteDel)
END_MESSAGE_MAP()

// CUserInfoDlg message handlers
BOOL CUserInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	for(int nPerm = GetEnvPtr()->m_Authority; nPerm <= CCopModule::eSiteUser; nPerm++)
	{
		int nIndex = m_cbPerm.AddString(CCopModule::GetObject()->UserType(nPerm));
		m_cbPerm.SetItemData(nIndex, (DWORD_PTR)nPerm);

		if(m_UserInfo.userType == nPerm)
			m_cbPerm.SetCurSel(nIndex);
	}

	CCopModule::GetObject()->GetRoleList(m_lsRole);
	POSITION pos = m_lsRole.GetHeadPosition();
	while(pos)
	{
		SRoleInfo& info = m_lsRole.GetNext(pos);

		if(info.customer.CompareNoCase(GetEnvPtr()->m_strCustomer) != 0) continue;

		int nIndex = m_cbRole.AddString(info.roleName);
		m_cbRole.SetItemData(nIndex, (DWORD_PTR)&info);

		if(m_UserInfo.roleId == info.roleId)
			m_cbRole.SetCurSel(nIndex);
	}

	if(!m_UserInfo.userId.IsEmpty())
	{
		((CEdit*)GetDlgItem(IDC_EDIT_USERID))->SetReadOnly();
	}
	
	SetDlgItemText(IDC_EDIT_SITE_NAME, m_UserInfo.siteName);
	SetDlgItemText(IDC_EDIT_USERID, m_UserInfo.userId);
	SetDlgItemText(IDC_EDIT_NAME, m_UserInfo.userName);
	SetDlgItemText(IDC_EDIT_PASSWD, m_UserInfo.password);
	SetDlgItemText(IDC_EDIT_PASSWD2, m_UserInfo.password);
	SetDlgItemText(IDC_EDIT_PHONE, m_UserInfo.phoneNo);
	SetDlgItemText(IDC_EDIT_MAIL, m_UserInfo.email);

	m_dtValidationDate.SetFormat(_T("yyyy/MM/dd"));
	m_dtLastPWChangeTime.SetFormat(_T("yyyy/MM/dd HH:mm:ss"));
	m_dtLastLoginTime.SetFormat(_T("yyyy/MM/dd HH:mm:ss"));
	
	if(m_UserInfo.validationDate.Format(_T("%Y/%m/%d")) == _T("1970/01/01"))
	{
		COleDateTime tmTemp(2037,12,31,23,59,59);
		m_dtValidationDate.SetTime(tmTemp);
	}
	else
	{
		m_dtValidationDate.SetTime(&m_UserInfo.validationDate);
	}
	
	//if(m_UserInfo.lastLoginTime.Format(_T("%Y/%m/%d")) == _T("1970/01/01"))
	//{
	//	COleDateTime tmTemp(2037,12,31,23,59,59);
	//	m_dtLastLoginTime.SetTime(tmTemp);
	//}
	//else
	//{
		m_dtLastLoginTime.SetTime(&m_UserInfo.lastLoginTime);
	//}

	//if(m_UserInfo.lastPWChanageTime.Format(_T("%Y/%m/%d")) == _T("1970/01/01"))
	//{
	//	COleDateTime tmTemp(2037,12,31,23,59,59);
	//	m_dtLastPWChanageTime.SetTime(tmTemp);
	//}
	//else
	//{
		m_dtLastPWChangeTime.SetTime(&m_UserInfo.lastPWChangeTime);
	//}

	m_kbFaultSMS.SetCheck(m_UserInfo.useSms);
	m_kbFaultEMAIL.SetCheck(m_UserInfo.useEmail);

	InitCauseList();
	InitSiteList();
	InitHostList();

	m_cbPerm.EnableWindow(CCopModule::eSiteUser != GetEnvPtr()->m_Authority);
	m_cbRole.EnableWindow(CCopModule::eSiteUser != GetEnvPtr()->m_Authority);
	m_dtValidationDate.EnableWindow(CCopModule::eSiteUser != GetEnvPtr()->m_Authority);
	m_dtLastLoginTime.EnableWindow(CCopModule::eSiteUser != GetEnvPtr()->m_Authority);
	m_dtLastPWChangeTime.EnableWindow(CCopModule::eSiteUser != GetEnvPtr()->m_Authority);
	GetDlgItem(IDC_BUTTON_SITE_LIST)->EnableWindow(CCopModule::eSiteUser != GetEnvPtr()->m_Authority);
	GetDlgItem(IDC_BUTTON_HOST_LIST)->EnableWindow(CCopModule::eSiteUser != GetEnvPtr()->m_Authority);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CUserInfoDlg::SetInfo(SUserInfo info)
{
	m_UserInfo = info;
}

SUserInfo CUserInfoDlg::GetInfo()
{
	return m_UserInfo;
}

void CUserInfoDlg::OnBnClickedOk()
{
	UpdateData();

	CString oldPassword = m_UserInfo.password;  // skpark 2013.01.30  password 가 바뀐경우를 감지해서 이 경우만 set 되도록 하며,
	                                            // 이경우만 strong password check 을 하도록 한다.

	GetDlgItemText(IDC_EDIT_USERID, m_UserInfo.userId);
	GetDlgItemText(IDC_EDIT_NAME, m_UserInfo.userName);
	GetDlgItemText(IDC_EDIT_PASSWD, m_UserInfo.password);
	CString strPassword2;
	GetDlgItemText(IDC_EDIT_PASSWD2, strPassword2);
	GetDlgItemText(IDC_EDIT_PHONE, m_UserInfo.phoneNo);
	GetDlgItemText(IDC_EDIT_MAIL, m_UserInfo.email);

	m_UserInfo.userId.Trim();
	if(m_UserInfo.userId.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_USERINFODLG_MSG002));
		GetDlgItem(IDC_EDIT_USERID)->SetFocus();
		return;
	}

	m_UserInfo.password.Trim();
	if(m_UserInfo.password.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_USERINFODLG_MSG003));
		GetDlgItem(IDC_EDIT_PASSWD)->SetFocus();
		return;
	}

	strPassword2.Trim();
	if(strPassword2.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_USERINFODLG_MSG003));
		GetDlgItem(IDC_EDIT_PASSWD2)->SetFocus();
		return;
	}

	if(m_UserInfo.password != strPassword2)
	{
		UbcMessageBox(LoadStringById(IDS_USERINFODLG_MSG004));
		GetDlgItem(IDC_EDIT_PASSWD2)->SetFocus();
		return;
	}

	if(m_UserInfo.password != oldPassword){
		// password changed !!!
		m_UserInfo.passwordChanged = true;
	}else{
		m_UserInfo.passwordChanged = false;
	}

	if(m_cbPerm.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_USERINFODLG_MSG006));
		m_cbPerm.SetFocus();
		return;
	}

	if(m_cbRole.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_USERINFODLG_MSG005));
		m_cbRole.SetFocus();
		return;
	}

	m_UserInfo.userType = m_cbPerm.GetItemData(m_cbPerm.GetCurSel());

	SRoleInfo* pInfo = (SRoleInfo*)m_cbRole.GetItemData(m_cbRole.GetCurSel());
	if(pInfo) m_UserInfo.roleId = pInfo->roleId;

	m_dtValidationDate.GetTime(m_UserInfo.validationDate);
	m_dtLastLoginTime.GetTime(m_UserInfo.lastLoginTime);
	m_dtLastPWChangeTime.GetTime(m_UserInfo.lastPWChangeTime);

	m_UserInfo.useSms = m_kbFaultSMS.GetCheck();
	m_UserInfo.useEmail = m_kbFaultEMAIL.GetCheck();

	CString strCause = m_cbFault.GetCheckedTexts();
	strCause.Replace("(","");
	strCause.Replace(")","");
	if(strCause == "All") strCause = "";
	else if(!strCause.IsEmpty())
	{
		strCause.Replace(",", "\",\"");
		strCause = "[\"" + strCause + "\"]";
	}
	m_UserInfo.probableCauseList = strCause;

	CString strSiteList = "";
	for(int i=0; i<m_lcSiteList.GetItemCount(); i++)
	{
		if(!strSiteList.IsEmpty()) strSiteList += "\",\"";
		strSiteList += m_lcSiteList.GetItemText(i,0);
	}
	if(!strSiteList.IsEmpty())
	{
		strSiteList = "[\"" + strSiteList + "\"]";
	}
	m_UserInfo.siteList = strSiteList;

	CString strHostList = "";
	for(int i=0; i<m_lcHostList.GetItemCount(); i++)
	{
		if(!strHostList.IsEmpty()) strHostList += "\",\"";
		strHostList += m_lcHostList.GetItemText(i,0);
	}
	if(!strHostList.IsEmpty())
	{
		strHostList = "[\"" + strHostList + "\"]";
	}
	m_UserInfo.hostList = strHostList;

	OnOK();
}

void CUserInfoDlg::InitCauseList()
{
	m_cbFault.AddString(SERVER_PROCESS_DOWN		 , 0);
	m_cbFault.AddString(SERVER_COMMUNICATION_FAIL, 1);
	m_cbFault.AddString(SERVER_VNC_FAIL			 , 2);
	m_cbFault.AddString(SERVER_HDD_OVERLOAD		 , 3);
	m_cbFault.AddString(SERVER_MEM_OVERLOAD		 , 4);
	m_cbFault.AddString(SERVER_CPU_OVERLOAD		 , 5);
	m_cbFault.AddString(SERVER_NET_OVERLOAD		 , 6);
	m_cbFault.AddString(SERVER_DB_OVERLOAD		 , 7);
	m_cbFault.AddString(HOST_PROCESS_DOWN		 , 8);
	m_cbFault.AddString(HOST_COMMUNICATION_FAIL	 , 9);
	m_cbFault.AddString(HOST_VNC_FAIL			 ,10);
	m_cbFault.AddString(HOST_FTP_FAIL			 ,11);
	m_cbFault.AddString(HOST_UPDATE_FAIL		 ,12);
	m_cbFault.AddString(HOST_PLAY_CONTENTS_FAIL	 ,13);
	m_cbFault.AddString(HOST_CHANGE_SCHEDULE_FAIL,14);
	m_cbFault.AddString(HOST_SCREENSHOT_FAIL	 ,15);
	m_cbFault.AddString(HOST_HDD_OVERLOAD		 ,16);
	m_cbFault.AddString(HOST_MEM_OVERLOAD		 ,17);
	m_cbFault.AddString(HOST_CPU_OVERLOAD		 ,18);
	m_cbFault.AddString(HOST_NET_OVERLOAD		 ,19);

	if(m_UserInfo.probableCauseList.Find(SERVER_PROCESS_DOWN	  ) >= 0) m_cbFault.SetCheck( 0, TRUE);
	if(m_UserInfo.probableCauseList.Find(SERVER_COMMUNICATION_FAIL) >= 0) m_cbFault.SetCheck( 1, TRUE);
	if(m_UserInfo.probableCauseList.Find(SERVER_VNC_FAIL		  ) >= 0) m_cbFault.SetCheck( 2, TRUE);
	if(m_UserInfo.probableCauseList.Find(SERVER_HDD_OVERLOAD	  ) >= 0) m_cbFault.SetCheck( 3, TRUE);
	if(m_UserInfo.probableCauseList.Find(SERVER_MEM_OVERLOAD	  ) >= 0) m_cbFault.SetCheck( 4, TRUE);
	if(m_UserInfo.probableCauseList.Find(SERVER_CPU_OVERLOAD	  ) >= 0) m_cbFault.SetCheck( 5, TRUE);
	if(m_UserInfo.probableCauseList.Find(SERVER_NET_OVERLOAD	  ) >= 0) m_cbFault.SetCheck( 6, TRUE);
	if(m_UserInfo.probableCauseList.Find(SERVER_DB_OVERLOAD		  ) >= 0) m_cbFault.SetCheck( 7, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_PROCESS_DOWN		  ) >= 0) m_cbFault.SetCheck( 8, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_COMMUNICATION_FAIL  ) >= 0) m_cbFault.SetCheck( 9, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_VNC_FAIL			  ) >= 0) m_cbFault.SetCheck(10, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_FTP_FAIL			  ) >= 0) m_cbFault.SetCheck(11, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_UPDATE_FAIL		  ) >= 0) m_cbFault.SetCheck(12, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_PLAY_CONTENTS_FAIL  ) >= 0) m_cbFault.SetCheck(13, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_CHANGE_SCHEDULE_FAIL) >= 0) m_cbFault.SetCheck(14, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_SCREENSHOT_FAIL	  ) >= 0) m_cbFault.SetCheck(15, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_HDD_OVERLOAD		  ) >= 0) m_cbFault.SetCheck(16, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_MEM_OVERLOAD		  ) >= 0) m_cbFault.SetCheck(17, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_CPU_OVERLOAD		  ) >= 0) m_cbFault.SetCheck(18, TRUE);
	if(m_UserInfo.probableCauseList.Find(HOST_NET_OVERLOAD		  ) >= 0) m_cbFault.SetCheck(19, TRUE);
}

void CUserInfoDlg::InitSiteList()
{
	m_lcSiteList.SetExtendedStyle(m_lcSiteList.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lcSiteList.InsertColumn(0, LoadStringById(IDS_USERINFODLG_LST002), LVCFMT_LEFT, 200);
	m_lcSiteList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcSiteList.SetSortEnable(true);

	CString strList = m_UserInfo.siteList;
	strList.Replace("[", "");
	strList.Replace("]", "");
	strList.Replace("\"", "");

	int nPos = 0;
	CString strTok;
	while(!(strTok = strList.Tokenize(",", nPos)).IsEmpty())
	{
		m_lcSiteList.InsertItem(m_lcSiteList.GetItemCount(), strTok);
	}
}

void CUserInfoDlg::InitHostList()
{
	m_lcHostList.SetExtendedStyle(m_lcHostList.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	m_lcHostList.InsertColumn(0, LoadStringById(IDS_USERINFODLG_LST001), LVCFMT_LEFT, 200);
	m_lcHostList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
	m_lcHostList.SetSortEnable(true);

	CString strList = m_UserInfo.hostList;
	strList.Replace("[", "");
	strList.Replace("]", "");
	strList.Replace("\"", "");

	int nPos = 0;
	CString strTok;
	while(!(strTok = strList.Tokenize(",", nPos)).IsEmpty())
	{
		m_lcHostList.InsertItem(m_lcHostList.GetItemCount(), strTok);
	}
}

void CUserInfoDlg::OnBnClickedButtonSiteDel()
{
	if(UbcMessageBox(LoadStringById(IDS_SITEUSERVIEW_MSG001), MB_YESNO) != IDYES) return;

	//m_lcHostList.DeleteAllItems();
	// skpark 2012.10.10 조직삭제를 눌렀는데, 조직은 지워지지 않고, 단말만 지워진다.
	// 단말을 지울필요가 없고, 선택한 조직이 지워져야 한다.
	POSITION pos = m_lcSiteList.GetFirstSelectedItemPosition();
	while(pos){
		int nRow = m_lcSiteList.GetNextSelectedItem(pos);
		m_lcSiteList.DeleteItem(nRow);
	}

}

void CUserInfoDlg::OnBnClickedButtonSiteList()
{
	CSiteSelectDlg dlg(GetEnvPtr()->m_strCustomer);
//	dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
	if(dlg.DoModal() != IDOK) return;

	// skpark 2012.10.10 host 를 지울필요가 없음
	//m_lcHostList.DeleteAllItems();

	SiteInfoList siteInfoList;
	if(dlg.GetSelectSiteInfoList(siteInfoList))
	{
		POSITION pos = siteInfoList.GetHeadPosition();
		while(pos)
		{
			SSiteInfo info = siteInfoList.GetNext(pos);

			if(!CCopModule::GetObject()->IsMySite(info.siteId)) continue;

			m_lcSiteList.InsertItem(m_lcSiteList.GetItemCount(), info.siteId);
		}
	}
}

void CUserInfoDlg::OnBnClickedButtonHostList()
{
	CMultiSelectHost dlg(GetEnvPtr()->m_strCustomer);

	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	for(int i=0; i<m_lcHostList.GetItemCount() ;i++)
	{
		dlg.m_astrSelHostList.Add( m_lcHostList.GetItemText(i,0) );
	}

	if(dlg.DoModal() != IDOK) return;

	m_lcHostList.DeleteAllItems();
	for(int i=0; i<dlg.m_astrSelHostList.GetCount() ;i++)
	{
		m_lcHostList.InsertItem(i, dlg.m_astrSelHostList[i]);
	}
}
