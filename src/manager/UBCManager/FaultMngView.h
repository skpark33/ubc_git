#pragma once

#include "afxwin.h"
#include "common\HoverButton.h"
#include "common\utblistctrlex.h"
#include "common\reposcontrol.h"
#include "ubccopcommon\eventhandler.h"
#include "ColorPickerCB.h"

// CFaultMngView 폼 뷰입니다.

class CFaultMngView : public CFormView
{
	DECLARE_DYNCREATE(CFaultMngView)
public:
	// 리스트컬럼 인덱스
	enum { eCheck, eSiteID, eHostID, eCounter, eSeverity, eCause
		 , eEventTime, eUpdateTime, eAdditionalText, eColEnd };

private:
	void InitTab();

	void InitFaultList();
	void RefreshFaultList();
	void UpdateFaultRow(int nRow, SFaultInfo& info);
	void RefreshList();

	void InitPosition(CRect);

	int FaultAlarm(SFault*);
	int UpdateAlarm(SFaultUpdate*);

//	virtual int InvokeEvent(WPARAM, LPARAM);
	LRESULT InvokeEvent(WPARAM, LPARAM);
//	void RemoveAllEvent();

private:
	CStatic			m_Filter;
	CString			m_szSiteID;
	CString			m_szHostID;
	CColorPickerCB	m_cbSeverity;
	CComboBox		m_cbCause;
	CDateTimeCtrl	m_dtStart;
	CDateTimeCtrl	m_dtEnd;

	CImageList		m_ilList;
	CUTBListCtrlEx	m_lscList;
	CString			m_szListColum[eColEnd];

	CHoverButton	m_btnRefresh;
	CHoverButton	m_btnDelFault;

	CPoint m_ptSelList;
	CRect m_rcClient;
	CReposControl m_Reposition;

//	CList<int> m_lsEvID;
//	CEventManager m_EventManager;
//	static UINT AddEventThread(LPVOID pParam);

protected:
	CFaultMngView();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CFaultMngView();

public:
	enum { IDD = IDD_FAULT_MNG_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnBnClickedBnRefresh();
	afx_msg void OnFaultviewBnDelete();
	afx_msg void OnFaultviewDelete();
	afx_msg void OnFaultviewAdminComment();
//	afx_msg void OnFaultviewHistView();
	afx_msg void OnNMRclickListCurFault(NMHDR *pNMHDR, LRESULT *pResult);
};


