#pragma once

#include "ubccopcommon\eventhandler.h"


// CFaultMngFrm 프레임입니다.

class CFaultMngFrm : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CFaultMngFrm)
protected:
	CFaultMngFrm();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CFaultMngFrm();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
