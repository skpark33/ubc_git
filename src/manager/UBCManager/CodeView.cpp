// CodeView.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "CodeView.h"
#include "AnnounceDlg.h"
#include "common/UbcGUID.h"
#include "common/libProfileManager/ProfileManager.h"
#include "CodeDetailDlg.h"

// CCodeView
IMPLEMENT_DYNCREATE(CCodeView, CFormView)

CCodeView::CCodeView() : CFormView(CCodeView::IDD)
,	m_Reposition(this)
{
	m_bInit = false;
}

CCodeView::~CCodeView()
{
}

void CCodeView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CODE, m_lscCode);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_btAdd);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_btDel);
	DDX_Control(pDX, IDC_BUTTON_MOD, m_btMod);
	DDX_Control(pDX, IDC_BUTTON_REF, m_btRef);
	DDX_Control(pDX, IDC_COMBO_CATEGORY, m_cb_category);
}

BEGIN_MESSAGE_MAP(CCodeView, CFormView)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CODE, OnNMDblclkList)

	ON_BN_CLICKED(IDC_BUTTON_ADD, OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_MOD, OnBnClickedButtonMod)
	ON_BN_CLICKED(IDC_BUTTON_REF, OnBnClickedButtonRef)

END_MESSAGE_MAP()

// CCodeView diagnostics
#ifdef _DEBUG
void CCodeView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CCodeView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CCodeView message handlers
int CCodeView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);

	return 0;
}

void CCodeView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	if(m_bInit)	return;

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_CODEVIEW_STR001));

	m_btAdd.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btDel.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btMod.LoadBitmap(IDB_BUTTON_MODIFY, RGB(255,255,255));
	m_btRef.LoadBitmap(IDB_BUTTON_REFRESH2, RGB(255,255,255));

	m_btAdd.SetToolTipText(LoadStringById(IDS_CODEVIEW_STR002));
	m_btDel.SetToolTipText(LoadStringById(IDS_CODEVIEW_STR003));
	m_btMod.SetToolTipText(LoadStringById(IDS_CODEVIEW_STR004));
	m_btRef.SetToolTipText(LoadStringById(IDS_CODEVIEW_STR005));

	m_Reposition.SetParentRect(m_rcClient);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscCode, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	InitList();
//	SetList();
}

void CCodeView::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CFormView::OnClose();
}

void CCodeView::OnDestroy()
{
	CFormView::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscCode.SaveColumnState("CODE-LIST", szPath);
}

void CCodeView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_Reposition.Move();
}

void CCodeView::InitList()
{
	// m_cb_category 를 초기화

	CodeItemList categoryList;
	if(!CCopModule::GetObject()->GetCodeCategory(categoryList, GetEnvPtr()->m_strCustomer)){
		UbcMessageBox("GetCategory failed");
	}else{
		if(categoryList.GetSize() == 0){
			UbcMessageBox("GetCategory failed");
		}else{
			
			POSITION pos = categoryList.GetHeadPosition();
			while(pos)
			{
				SCodeItem info = categoryList.GetNext(pos);
				m_cb_category.AddString(info.strCategoryName);
			}
			m_cb_category.SetRedraw(TRUE);
		}
	}

	// eCheck, eCategory, eEnumString, eEnumNumber, eDorder, eVisible, eEnd
	m_ColumTitle[eCheck     ] = _T("");
	m_ColumTitle[eCategory     ] =	LoadStringById(IDS_CODEVIEW_LST001);
	m_ColumTitle[eEnumString ] =			LoadStringById(IDS_CODEVIEW_LST002);
	m_ColumTitle[eEnumNumber   ] =	LoadStringById(IDS_CODEVIEW_LST003);
	m_ColumTitle[eDorder   ] =			LoadStringById(IDS_CODEVIEW_LST004);
	m_ColumTitle[eVisible] =			LoadStringById(IDS_CODEVIEW_LST005);

	m_lscCode.SetExtendedStyle(m_lscCode.GetExtendedStyle() | LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_lscCode.InsertColumn(eCheck,			m_ColumTitle[eCheck], LVCFMT_LEFT, 22);
	m_lscCode.InsertColumn(eCategory,		m_ColumTitle[eCategory], LVCFMT_LEFT, 200);
	m_lscCode.InsertColumn(eEnumString,		m_ColumTitle[eEnumString], LVCFMT_LEFT, 150);
	m_lscCode.InsertColumn(eEnumNumber,	m_ColumTitle[eEnumNumber], LVCFMT_LEFT, 150);
	m_lscCode.InsertColumn(eDorder,			m_ColumTitle[eDorder], LVCFMT_LEFT, 100);
	m_lscCode.InsertColumn(eVisible,		m_ColumTitle[eVisible], LVCFMT_LEFT, 60);

	m_lscCode.InitHeader(IDB_LIST_HEADER);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscCode.LoadColumnState("CODE-LIST", szPath);
}

void CCodeView::SetList()
{
	m_cb_category.GetWindowText(m_current_category);
	if(m_current_category.IsEmpty()){
		UbcMessageBox(LoadStringById(IDS_CODE_MSG005));
		return;
	}

	m_lsCode.RemoveAll();
	if(!CCopModule::GetObject()->GetCodeItem(m_lsCode, GetEnvPtr()->m_strCustomer, m_current_category)){
		UbcMessageBox("GetCode failed");
		return;
	}
	m_lscCode.DeleteAllItems();

	int nRow = 0;
	POSITION pos = m_lsCode.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SCodeItem info = m_lsCode.GetNext(pos);
		// eCheck, eCategory, eEnumString, eEnumNumber, eDorder, eVisible, eEnd
		
		m_lscCode.InsertItem(nRow, "");
		m_lscCode.SetItemText(nRow, eCategory, info.strCategoryName);
		m_lscCode.SetItemText(nRow, eEnumString  , info.strEnumString);
		char buf[20];
		sprintf(buf,"%ld", info.nEnumNumber);
		m_lscCode.SetItemText(nRow, eEnumNumber    , buf);

		sprintf(buf,"%d", info.nDisplayOrder);
		m_lscCode.SetItemText(nRow, eDorder  , buf);
		m_lscCode.SetItemText(nRow, eVisible  , ((info.bVisible == 1) ? "true":"false"));

		m_lscCode.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
	m_lscCode.SetRedraw(TRUE);

}


void CCodeView::OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult)
{

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	POSITION pos = (POSITION)m_lscCode.GetItemData(nRow);
	SCodeItem info = m_lsCode.GetAt(pos);

	CCodeDetailDlg dlg;
	dlg.SetInfo(info);

	if(dlg.DoModal() != IDOK) return;

	dlg.GetInfo(info);


	if(!CCopModule::GetObject()->SetCodeItem(info,GetEnvPtr()->m_strCustomer)){
		UbcMessageBox("Set Code failed");
		return;
	}

	SetList();
/*
	if(info.AnnoId.IsEmpty())
	{
		info.AnnoId = CUbcGUID::GetInstance()->ToGUID(_T(""));
	}

	// 0001375: 긴급공지 방식을 현재의 Event 방식에서 INI download 방식으로 변경한다.
	CString szLocalPath;
	szLocalPath.Format("%s%sdata\\", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath);

	CString szAnnounceFile;
	szAnnounceFile.Format("UBCAnnounce_%s.ini", info.AnnoId);

	if(!SaveFile(szLocalPath + szAnnounceFile, info))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CODEVIEW_MSG003), info.Title);
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(!GetEnvPtr()->PutFile(szAnnounceFile, szLocalPath, "/config/", GetEnvPtr()->m_szSite))
	{
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_CODEVIEW_MSG004), info.Title);
		UbcMessageBox(szMsg, MB_ICONWARNING);
		return;
	}

	if(CCopModule::GetObject()->SetAnnounce(info.SiteId, info.AnnoId, info))
	{
		m_lsCode.GetAt(pos) = info;
		m_lscCode.SetItemText(nRow, eTitle, info.Title);
		
		if(!info.BgFile.IsEmpty())
		{
			CString szLocal;
			szLocal.Format("%s\\%s%s\\", GetEnvPtr()->m_szDrive, UBC_CONTENTS_PATH, STR_ANNOFOLDER);
			GetEnvPtr()->PutFile(info.BgFile, szLocal, info.BgLocation, info.SiteId);
		}
	}
*/
}

void CCodeView::OnBnClickedButtonAdd()
{
	SCodeItem info;
	info.strCategoryName = this->m_current_category;
	info.bVisible = true;

	CCodeDetailDlg dlg;
	dlg.SetInfo(info);

	if(dlg.DoModal() != IDOK) return;

	dlg.GetInfo(info);

	info.strCodeId.Append(info.strCategoryName);
	info.strCodeId.Append(info.strEnumString);

	if(!CCopModule::GetObject()->CreateCodeItem(info, GetEnvPtr()->m_strCustomer )){
		UbcMessageBox("Create Code failed");
		return;
	}

	SetList();
}

void CCodeView::OnBnClickedButtonDel()
{
	int nCnt = 0;
	for(int nRow = m_lscCode.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscCode.GetCheck(nRow)) continue;
		nCnt++;
	}

	if(nCnt == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CODE_MSG002), MB_ICONINFORMATION);
		return;
	}

	if(UbcMessageBox(LoadStringById(IDS_CODE_MSG001), MB_YESNO|MB_ICONINFORMATION) != IDYES) return;

	for(int nRow = m_lscCode.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscCode.GetCheck(nRow)) continue;

		m_lscCode.SetCheck(nRow, false);
		POSITION pos = (POSITION)m_lscCode.GetItemData(nRow);
		SCodeItem info = m_lsCode.GetNext(pos);;

		if(!CCopModule::GetObject()->DelCodeItem(info, GetEnvPtr()->m_strCustomer)){
			UbcMessageBox("Delete Code failed");
			return;
		}

	}
	SetList();
}

void CCodeView::OnBnClickedButtonMod()
{
	int nCnt = 0;
	for(int nRow = m_lscCode.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscCode.GetCheck(nRow)) continue;
		nCnt++;
	}

	if(nCnt == 0)
	{
		UbcMessageBox(LoadStringById(IDS_CODE_MSG002), MB_ICONINFORMATION);
		return;
	}

	for(int nRow = m_lscCode.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscCode.GetCheck(nRow)) continue;

		m_lscCode.SetCheck(nRow, false);
		POSITION pos = (POSITION)m_lscCode.GetItemData(nRow);
		SCodeItem info = m_lsCode.GetNext(pos);;

		CCodeDetailDlg dlg;
		dlg.SetInfo(info);

		if(dlg.DoModal() != IDOK) return;

		dlg.GetInfo(info);

		if(!CCopModule::GetObject()->SetCodeItem(info, GetEnvPtr()->m_strCustomer)){
			UbcMessageBox("Set Code failed");
			return;
		}

	}
	SetList();

}

void CCodeView::OnBnClickedButtonRef()
{
	SetList();
	UbcMessageBox(LoadStringById(IDS_REFRASHLIST_MSG), MB_ICONINFORMATION);
}

