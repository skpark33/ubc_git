// ChngPasswordDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ChngPasswordDlg.h"


// CChngPasswordDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CChngPasswordDlg, CDialog)

CChngPasswordDlg::CChngPasswordDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChngPasswordDlg::IDD, pParent)
{

}

CChngPasswordDlg::~CChngPasswordDlg()
{
}

void CChngPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CChngPasswordDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CChngPasswordDlg::OnBnClickedOk)
END_MESSAGE_MAP()


void CChngPasswordDlg::OnBnClickedOk()
{
	CString strPassword1;
	CString strPassword2;

	GetDlgItemText(IDC_EB_PASSWORD1, strPassword1);
	GetDlgItemText(IDC_EB_PASSWORD2, strPassword2);

	if(strPassword1.IsEmpty())
	{
		UbcMessageBox(LoadStringById(IDS_PASSWORD_MSG1));
		return;
	}

	if(strPassword1 != strPassword2)
	{
		UbcMessageBox(LoadStringById(IDS_PASSWORD_MSG2));
		return;
	}

	m_strPassword = strPassword1;

	OnOK();
}
