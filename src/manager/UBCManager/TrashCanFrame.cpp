// TrashCanFrame.cpp : implementation file
//

#include "stdafx.h"
#include "UBCManager.h"
#include "TrashCanFrame.h"


// CTrashCanFrame

IMPLEMENT_DYNCREATE(CTrashCanFrame, CMDIChildWnd)

CTrashCanFrame::CTrashCanFrame()
{
}

CTrashCanFrame::~CTrashCanFrame()
{
}


BEGIN_MESSAGE_MAP(CTrashCanFrame, CMDIChildWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CTrashCanFrame message handlers

int CTrashCanFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	SetIcon(AfxGetApp()->LoadIcon(IDI_PACKAGE), false);

	return 0;
}
