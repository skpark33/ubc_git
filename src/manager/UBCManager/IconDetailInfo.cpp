// IconDetailInfo.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "IconDetailInfo.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "common\UbcCode.h"
#include "UBCCopCommon\UbcMenuAuth.h"

#define STR_OPERA_ON			LoadStringById(IDS_HOSTVIEW_LIST003)
#define STR_OPERA_OFF			LoadStringById(IDS_HOSTVIEW_LIST004)

#define STR_HOST_CLASS				LoadStringById(IDS_ICONVIEW_STR001)
#define STR_LIGHT_CLASS				LoadStringById(IDS_ICONVIEW_STR002)
#define STR_MONITOR_CLASS			LoadStringById(IDS_ICONVIEW_STR003)

// CIconDetailInfo 대화 상자입니다.

IMPLEMENT_DYNAMIC(CIconDetailInfo, CDialog)

CIconDetailInfo::CIconDetailInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CIconDetailInfo::IDD, pParent)
{
	m_IsCreateMode = false;
	m_IconChanged = false;
}

CIconDetailInfo::~CIconDetailInfo()
{
}

void CIconDetailInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_NODETYPEID, m_editNodeTypeId);
	DDX_Control(pDX, IDC_COMBO_OBJECTCLASS, m_cbObjectClass);
	DDX_Control(pDX, IDC_COMBO_OBJECTTYPE, m_cbObjectType);
	DDX_Control(pDX, IDC_COMBO_OBJECTSTATE, m_cbObjectState);
	DDX_Control(pDX, IDC_PIC_ICON, m_picIcon);
}


BEGIN_MESSAGE_MAP(CIconDetailInfo, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_NEW, &CIconDetailInfo::OnBnClickedButtonNew)
	ON_BN_CLICKED(IDOK, &CIconDetailInfo::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CIconDetailInfo::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_COMBO_OBJECTCLASS, &CIconDetailInfo::OnCbnSelchangeComboObjectclass)
END_MESSAGE_MAP()


// CIconDetailInfo 메시지 처리기입니다.

BOOL CIconDetailInfo::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cbObjectClass.AddString(STR_HOST_CLASS);
	m_cbObjectClass.AddString(STR_LIGHT_CLASS);
	m_cbObjectClass.AddString(STR_MONITOR_CLASS);
	
	m_cbObjectState.AddString(STR_OPERA_OFF);
	m_cbObjectState.AddString(STR_OPERA_ON);

	if(m_IsCreateMode){
		m_cbObjectClass.SetCurSel(0);
		m_cbObjectState.SetCurSel(0);
		m_cbObjectType.EnableWindow(false);
		OnCbnSelchangeComboObjectclass();
	}else{
		this->m_editNodeTypeId.SetWindowText(this->m_IconInfo.nodeTypeId);
	
		if(this->m_IconInfo.objectClass == "Host") {
			m_cbObjectClass.SetCurSel(0);	
		} else if(this->m_IconInfo.objectClass == "Light") {
			m_cbObjectClass.SetCurSel(1);	
		} else if(this->m_IconInfo.objectClass == "Monitor") {
			m_cbObjectClass.SetCurSel(2);	
		}

		OnCbnSelchangeComboObjectclass();
		m_cbObjectType.SetCurSel(CUbcCode::GetInstance()->FindComboBoxCtrl(m_cbObjectType, this->m_IconInfo.objectType));

		m_cbObjectState.SetCurSel(this->m_IconInfo.objectState);

		CString szServerLocation = "/Config/Plano/" ;
		CString szLocalLocation;
		szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

		this->m_picIcon.Load(this->m_IconInfo.nodeTypeId,szLocalLocation,szServerLocation);

		m_cbObjectClass.EnableWindow(false);
		m_cbObjectState.EnableWindow(false);
		m_cbObjectType.EnableWindow(false);
	}



	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CIconDetailInfo::OnBnClickedButtonNew()
{
	if(!SetInfo()){
		return;
	}

	CString szFileExts = "*.png";
	CString filter;
	filter.Format("All PNG Files|%s|" , szFileExts	 );

	if(CEnviroment::GetObject()->IsValidOpenFolder())
	{
		::SetCurrentDirectory(CEnviroment::GetObject()->m_szPathOpenFile);
	}
	else
	{
		// 내그림
		TCHAR szMyPictures[MAX_PATH] = {0,};
		::SHGetSpecialFolderPath(NULL, szMyPictures, CSIDL_MYPICTURES, FALSE);
		::SetCurrentDirectory(szMyPictures);
	}

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, this);

	if(dlg.DoModal() != IDOK) return;

	TCHAR szDrv[MAX_PATH], szPath[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(dlg.GetPathName(), szDrv, szPath, NULL, szExt);

	CString strFullpath;
	strFullpath.Format("%s%s",szDrv,szPath);
	CEnviroment::GetObject()->SetPathOpenFile(strFullpath);

	// 파일이름을 바꾸어서 Plano Directory 에 copy 한다.
	CString szLocalLocation;
	szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

	//패스가 없으면 만들어야 한다.  2017.10.30
	if(::access(szLocalLocation,0)!=0){
		//CString msg;
		//msg.Format("%s folder created", szLocalLocation);
		//UbcMessageBox(msg);
		::CreateDirectory(szLocalLocation, NULL);
	}else{
	
		//CString msg;
		//msg.Format("%s folder already exist", szLocalLocation);
		//UbcMessageBox(msg);
	}

	CString localFullFileName;
	localFullFileName.Format("%s%s", szLocalLocation, this->m_IconInfo.nodeTypeId);

	if(!::CopyFile(dlg.GetPathName(),localFullFileName ,false)){
		CString msg;
		msg.Format(LoadStringById(IDS_ICONVIEW_STR017), dlg.GetPathName(),localFullFileName);
		UbcMessageBox(msg);
		return;
	}

	TraceLog(("CopFile : %s --> %s succeed", dlg.GetPathName(),localFullFileName));

	m_IconChanged = true;
	// 파일을 Display 한다.
	this->m_picIcon.LocalLoad(this->m_IconInfo.nodeTypeId,szLocalLocation);
	this->m_picIcon.RedrawWindow();
	return;
}


BOOL CIconDetailInfo::Upload()
{
	if(!m_IconChanged) return true;

	CString szLocalLocation;
	szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

	CString szServerLocation = "/Config/Plano/" ;
	
	return this->m_picIcon.Upload(this->m_IconInfo.nodeTypeId,szLocalLocation,szServerLocation);
}

BOOL CIconDetailInfo::SetInfo()
{
	UpdateData();
	if(m_cbObjectClass.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR012));
		return FALSE;
	}
	if(m_cbObjectType.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR013));
		return FALSE;
	}
	if(m_cbObjectState.GetCurSel() < 0)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR014));
		return FALSE;
	}
	m_IconInfo.objectType = (long)m_cbObjectType.GetItemData(m_cbObjectType.GetCurSel());
	m_IconInfo.objectState = m_cbObjectState.GetCurSel();
	if(m_cbObjectClass.GetCurSel() == 0){
		this->m_IconInfo.objectClass = "Host";
	}else if(m_cbObjectClass.GetCurSel() == 1){
		this->m_IconInfo.objectClass = "Light";
	}else if(m_cbObjectClass.GetCurSel() == 2){
		this->m_IconInfo.objectClass = "Monitor";
	}

	m_IconInfo.nodeTypeId.Format("ICON_%s_%d_%d.png" ,this->m_IconInfo.objectClass,  m_IconInfo.objectType, m_IconInfo.objectState);

	this->m_editNodeTypeId.SetWindowText(m_IconInfo.nodeTypeId);
	
	m_IconInfo.ext = "png";
	return TRUE;
}

void CIconDetailInfo::OnBnClickedOk()
{
	if(this->m_IsCreateMode == true && this->m_IconChanged == false){
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR022));
		return ;
	}

	CString oldId;
	this->m_editNodeTypeId.GetWindowText(oldId);

	if(SetInfo()){
		if(!oldId.IsEmpty()){
			if(m_IconInfo.nodeTypeId != oldId){
				// 아이콘을 등록한 후에,  상태 등의 값을 다시 바꾸었다.  아이콘을 다시 등록해야 한다.
				UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR023));
				return ;
			}
		}
		OnOK();
	}
}

void CIconDetailInfo::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CIconDetailInfo::OnCbnSelchangeComboObjectclass()
{
	int idx = m_cbObjectClass.GetCurSel();
	m_cbObjectType.EnableWindow(true);
	switch(idx) {
		case 0 :  CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbObjectType, _T("HostType")); break;
		case 1 :  CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbObjectType, _T("LightType")); break;
		case 2 :  CUbcCode::GetInstance()->FillComboBoxCtrl(m_cbObjectType, _T("SubMonitorType")); break;
		default : 	m_cbObjectType.EnableWindow(false);	return;
	}
}

void CIconDetailInfo::SetInfo(SNodeTypeInfo& info)
{
	NodeTypeInfoList lsIcon;
	CCopModule::GetObject()->GetIconList(info.nodeTypeId, NULL,lsIcon);
	POSITION pos = lsIcon.GetHeadPosition();
	if(pos){
		m_IconInfo = lsIcon.GetNext(pos);
	}else{
		m_IconInfo = info;
	}
}

void CIconDetailInfo::GetInfo(SNodeTypeInfo& info)
{
	info = m_IconInfo;
}