// LogExcelSave.cpp: implementation of the CLogExcelSave class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UBCManager.h"
#include "LogExcelSave.h"

#include <afxdlgs.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLogExcelSave::CLogExcelSave()
{
}

CLogExcelSave::~CLogExcelSave()
{
}

CString CLogExcelSave::Save(CString strSheetName, CListCtrl& lcList)
{
	CFileDialog dlg(
		FALSE,
		"xls",
		NULL,
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		"Worksheet Files (*.xls)|*.xls|All Files (*.*)|*.*||",
		NULL
		);

	if(dlg.DoModal() != IDOK) return _T("");

	CString strFullpath = dlg.GetPathName();

	CFileStatus fs;

	if( CFile::GetStatus(strFullpath, fs) )
	{
		try
		{
			CFile::Remove(strFullpath);
		}
		catch (CFileException* pEx)
		{
			TCHAR szErrMsg[512] = {0};
			pEx->GetErrorMessage(szErrMsg, 512);
			pEx->Delete();
			UbcMessageBox(szErrMsg, MB_OK | MB_ICONWARNING);
		}
	}

	if(m_excelApp.CreateDispatch(_T("Excel.Application")))
	{
//			m_excelApp.SetVisible(TRUE);
		
		//Get a new workbook.
		COleVariant covOptional((long)DISP_E_PARAMNOTFOUND, VT_ERROR);

		Workbooks books = m_excelApp.GetWorkbooks();
		m_book = books.Add(covOptional);

		if( SaveDataList(1, strSheetName, lcList) )
		{
			m_book.SaveCopyAs(COleVariant(strFullpath));
		}

		m_book.SetSaved(TRUE);
		m_excelApp.Quit();
	}
	else
	{
		UbcMessageBox(LoadStringById(IDS_HOSTLOGVIEW_MSG002));
		return _T("");
	}

	return strFullpath;
}

BOOL CLogExcelSave::SaveDataList(short nSheet, CString strSheetName, CListCtrl& lcList)
{
	COleVariant covTrue((short)TRUE);
	COleVariant covFalse((short)FALSE);

	Range range;

	//Get the first sheet.
	Worksheets sheets = m_book.GetSheets();
	_Worksheet sheet = sheets.GetItem(COleVariant((short)nSheet));
	sheet.SetName(strSheetName);

	int nLine = 1;

	CString data;
	CHeaderCtrl* pHeader = (CHeaderCtrl*)(lcList.GetHeaderCtrl());
	ASSERT(pHeader);
	int iNumCol = pHeader->GetItemCount();
	data.Format("%d",iNumCol);

	int *pDx;
	int *pFormat;
	char **pColTitle;

	if (iNumCol <= 0) return TRUE;

	pDx = new int[iNumCol+1];
	pFormat = new int[iNumCol];
	pColTitle = new char *[iNumCol];
	ZeroMemory(pColTitle, sizeof(char *) * iNumCol);
	ZeroMemory(pDx, sizeof(int) * (iNumCol + 1));
	ZeroMemory(pFormat, sizeof(int) * iNumCol);
	char txt[128];
	CString loc;

	for (int t = 0; t < iNumCol; t++)
	{
		HDITEM hi;
		hi.cchTextMax = 128;
		hi.pszText    = txt;

		loc.Format("%c%d",'A'+t, nLine);
		hi.mask = HDI_TEXT|HDI_WIDTH|HDI_FORMAT;
		if (pHeader->GetItem(t, &hi))
		{
			pDx[t+1] = hi.cxy;
			pFormat[t] = hi.fmt;
			if (hi.cchTextMax > 0 && hi.pszText)
			{
				pColTitle[t] = new char[hi.cchTextMax];
				ASSERT(pColTitle[t]);
				lstrcpy(pColTitle[t], hi.pszText);
				data.Format("%s",hi.pszText);			
			}
		}
		range = sheet.GetRange(COleVariant(loc),COleVariant(loc));
//		range.SetValue(COleVariant(data));
		range.SetValue2(COleVariant(data));
	}

	 COleSafeArray saRet;
	 DWORD numElements[2];

	 numElements[0]=lcList.GetItemCount();
	 numElements[1]=iNumCol;

	 saRet.Create(VT_VARIANT, 2, numElements);

	 long index[2];

	 for(UINT i=0;i<numElements[0];i++)
	 {
		 for(UINT j=0;j<numElements[1];j++)
		 {
			index[0] = i;
			index[1] = j;
			COleVariant vTemp = (COleVariant)lcList.GetItemText(i,j);
			saRet.PutElement(index, vTemp);
		 }
	 }

	CString start_num, end_num;
	start_num.Format("A%d", nLine+1);
	end_num.Format("%c%d",'A'+numElements[1]-1,numElements[0]+nLine);

	range = sheet.GetRange(COleVariant(start_num), COleVariant(/*excelEng*/end_num));

//  range.SetValue(COleVariant(saRet));
	range.SetValue2(COleVariant(saRet));
		
	Range cols = range.GetEntireColumn();
	cols.AutoFit();
	saRet.Detach();

	return TRUE;
}
