#pragma once

#include "ubccopcommon\eventhandler.h"

// CPlanoFrame frame

class CPlanoFrame : public CMDIChildWnd, public IEventHandler
{
	DECLARE_DYNCREATE(CPlanoFrame)
protected:
	CPlanoFrame();           // protected constructor used by dynamic creation
	virtual ~CPlanoFrame();

	CEventManager m_EventManager;
	virtual int InvokeEvent(WPARAM, LPARAM);

	static UINT AddEventThread(LPVOID pParam);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
};
