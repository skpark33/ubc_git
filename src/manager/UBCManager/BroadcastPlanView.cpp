// BroadcastPlanView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "common\ubcdefine.h"
#include "UBCManager.h"
#include "Enviroment.h"
//#include "MainFrm.h"
#include "BroadcastPlanView.h"
#include "ubccopcommon\SelectHostDlg.h"
#include "ubccopcommon\PackageSelectDlg.h"
#include "ubccopcommon\SelectUsers.h"
#include "BroadcastPlanDlg.h"
#include "ubccopcommon\FtpMultiSite.h"
#include "common\libscratch\scratchUtil.h"
#include "common\UBCDailyPlanTableOCX.h"
#include "DownloadSummaryDlg.h"
#include "UBCCopCommon\UbcMenuAuth.h"
/*
#include "ubccopcommon\AuthCheckDlg.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libcommon\ubcMux.h"
#include "common\libscratch\scratchUtil.h"
#include "common\libcommon\ubchost.h"
#include "common\libinstall\installutil.h"
*/

// CBroadcastPlanView

IMPLEMENT_DYNCREATE(CBroadcastPlanView, CFormView)

CBroadcastPlanView::CBroadcastPlanView()
	: CFormView(CBroadcastPlanView::IDD)
,	m_Reposition(this)
,	m_pListDragImage(NULL)
,	m_nDailyPlanView(0)
{
}

CBroadcastPlanView::~CBroadcastPlanView()
{
	POSITION pos = m_mapTimePlanInfo.GetStartPosition();
	while(pos)
	{
		CString strKey;
		TimePlanInfoList* pTimePlanInfoList = NULL;
		m_mapTimePlanInfo.GetNextAssoc(pos, strKey, (void*&)pTimePlanInfoList);
		if(pTimePlanInfoList) delete pTimePlanInfoList;
	}
	m_mapTimePlanInfo.RemoveAll();
}

void CBroadcastPlanView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_Filter);
	DDX_Control(pDX, IDC_LIST_PLAN, m_lcList);
	DDX_Control(pDX, IDC_BUTTON_PLAN_REFRESH, m_bnRefresh);
	DDX_Control(pDX, IDC_BUTTON_PLAN_NEW, m_bnNew);
	DDX_Control(pDX, IDC_BUTTON_PLAN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_BUTTON_PLAN_UP, m_bnMoveUp);
	DDX_Control(pDX, IDC_BUTTON_PLAN_DOWN, m_bnMoveDown);
	DDX_Control(pDX, IDC_BUTTON_PLAN_EXPORT, m_bnExport);
	DDX_Control(pDX, IDC_DATE_FILTER_BASEDATE, m_dtFilterBaseDate);
	DDX_Control(pDX, IDC_DATE_FILTER_START, m_dtFilterStart);
	DDX_Control(pDX, IDC_DATE_FILTER_END, m_dtFilterEnd);
	DDX_Control(pDX, IDC_DP_DAILY_PLAN, m_dpDailyPlan);
	DDX_Control(pDX, IDC_RB_DAILY_PLAN_VIEW1, m_rbDailyPlanView1);
	DDX_Control(pDX, IDC_RB_DAILY_PLAN_VIEW2, m_rbDailyPlanView2);
	DDX_Control(pDX, IDC_KB_ADVANCED_FILTER, m_kbAdvancedFilter);
	DDX_Control(pDX, IDC_COMBO_ADVANCED_FILTER2, m_cbAdvancedFilter);
}

BEGIN_MESSAGE_MAP(CBroadcastPlanView, CFormView)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON_FILTER_HOST, &CBroadcastPlanView::OnBnClickedButtonFilterHost)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_REFRESH, &CBroadcastPlanView::OnBnClickedButtonPlanRefresh)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_NEW, &CBroadcastPlanView::OnBnClickedButtonPlanNew)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PLAN, &CBroadcastPlanView::OnNMDblclkListPlan)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_DEL, &CBroadcastPlanView::OnBnClickedButtonPlanDel)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_UP, &CBroadcastPlanView::OnBnClickedButtonPlanUp)
	ON_BN_CLICKED(IDC_BUTTON_PLAN_DOWN, &CBroadcastPlanView::OnBnClickedButtonPlanDown)
	ON_WM_DESTROY()
	ON_NOTIFY(LVN_BEGINDRAG, IDC_LIST_PLAN, &CBroadcastPlanView::OnLvnBegindragListPlan)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BUTTON_PLAN_EXPORT, &CBroadcastPlanView::OnBnClickedButtonPlanExport)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_REGISTER, &CBroadcastPlanView::OnBnClickedButtonFilterRegister)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_PACKAGE, &CBroadcastPlanView::OnBnClickedButtonFilterPackage)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST_PLAN, &CBroadcastPlanView::OnLvnColumnclickListPlan)
	ON_BN_CLICKED(IDC_RB_DAILY_PLAN_VIEW1, &CBroadcastPlanView::OnBnClickedRbDailyPlanView)
	ON_BN_CLICKED(IDC_RB_DAILY_PLAN_VIEW2, &CBroadcastPlanView::OnBnClickedRbDailyPlanView)
	ON_NOTIFY(NM_CLICK, IDC_LIST_PLAN, &CBroadcastPlanView::OnNMClickListPlan)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_PLAN, &CBroadcastPlanView::OnNMRclickListPlan)
	ON_COMMAND(ID_DOWNLOADSUMMARY_VIEW, &CBroadcastPlanView::OnDownloadsummaryView)
	ON_REGISTERED_MESSAGE(WM_INVOKE_EVENT, InvokeEvent)
	ON_BN_CLICKED(IDC_KB_ADVANCED_FILTER, &CBroadcastPlanView::OnBnClickedKbAdvancedFilter)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_PLAN, OnNMCustomdrawList)  //skpark eSiteUser

END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CBroadcastPlanView, CFormView)
	ON_EVENT(CBroadcastPlanView, IDC_DP_DAILY_PLAN, 4, CBroadcastPlanView::ItemDblClickDpDailyPlan, VTS_I4)
END_EVENTSINK_MAP()


// CBroadcastPlanView 진단입니다.

#ifdef _DEBUG
void CBroadcastPlanView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CBroadcastPlanView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CBroadcastPlanView 메시지 처리기입니다.

void CBroadcastPlanView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CTime tmCur = CTime::GetCurrentTime();
	m_dtFilterEnd.SetTime(&tmCur);
	InitDateRange(m_dtFilterEnd);

	int nAddMonth = -1;
	int nTotalMonth = tmCur.GetYear() * 12 + tmCur.GetMonth() + nAddMonth;

	CTime tmStartDate  = CTime( nTotalMonth/12 - ((nTotalMonth%12) == 0 ? 1 : 0)
							, ((nTotalMonth%12) == 0 ? 12 : (nTotalMonth%12))
							, tmCur.GetDay()
							, tmCur.GetHour()
							, tmCur.GetMinute()
							, tmCur.GetSecond()
							);
	//tmStartDate -= CTimeSpan(1, 0, 0, 0);
	m_dtFilterStart.SetTime(&tmStartDate);
	m_dtFilterBaseDate.SetTime(&tmCur);

	InitDateRange(m_dtFilterStart);
	InitDateRange(m_dtFilterBaseDate);

	m_bnRefresh .LoadBitmap(IDB_BUTTON_REFRESH   , RGB(255,255,255));
	m_bnNew     .LoadBitmap(IDB_BUTTON_ADD       , RGB(255,255,255));
	m_bnDel     .LoadBitmap(IDB_BUTTON_REMOVE    , RGB(255,255,255));
	m_bnMoveUp  .LoadBitmap(IDB_BUTTON_ORDER_UP  , RGB(255,255,255));
	m_bnMoveDown.LoadBitmap(IDB_BUTTON_ORDER_DOWN, RGB(255,255,255));
	m_bnExport  .LoadBitmap(IDB_BUTTON_EXPORT2   , RGB(255,255,255));

	m_bnRefresh .SetToolTipText(LoadStringById(IDS_PLANVIEW_BTN001));
	m_bnNew     .SetToolTipText(LoadStringById(IDS_PLANVIEW_BTN002));
	m_bnDel     .SetToolTipText(LoadStringById(IDS_PLANVIEW_BTN003));
	m_bnMoveUp  .SetToolTipText(LoadStringById(IDS_PLANVIEW_BTN004));
	m_bnMoveDown.SetToolTipText(LoadStringById(IDS_PLANVIEW_BTN005));
	m_bnExport  .SetToolTipText(LoadStringById(IDS_PLANVIEW_BTN006));

	// 권한에 따라 활성화 한다.
	m_bnRefresh .EnableWindow(IsAuth(_T("BQRY")));
	m_bnNew     .EnableWindow(IsAuth(_T("BREG")));
	m_bnDel     .EnableWindow(IsAuth(_T("BDEL")));
	m_bnMoveUp  .EnableWindow(IsAuth(_T("BMOD")));
	m_bnMoveDown.EnableWindow(IsAuth(_T("BMOD")));
	m_bnExport  .EnableWindow(IsAuth(_T("BXPT")));

	InitList();

	m_Reposition.AddControl(&m_lcList, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_Filter, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_Reposition.AddControl(&m_rbDailyPlanView1, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_Reposition.AddControl(&m_rbDailyPlanView2, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_Reposition.AddControl(&m_dpDailyPlan, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	LoadFilterData();

	m_dpDailyPlan.SetDefaultViewTime(0);
	m_dpDailyPlan.SetViewRange(24);
	m_dpDailyPlan.SetEditable(FALSE);

	m_nDailyPlanView = 0;
	m_rbDailyPlanView1.SetCheck(TRUE);
	m_rbDailyPlanView2.SetCheck(FALSE);

//	RefreshList();

	UpdateData(FALSE);
}

void CBroadcastPlanView::InitList()
{
	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	m_szColum[eCheck       ] = _T("");
	m_szColum[eSiteName    ] = LoadStringById(IDS_PLANVIEW_LIST001);
	m_szColum[ePlan        ] = LoadStringById(IDS_PLANVIEW_LIST002);
	m_szColum[eDownloadTime] = LoadStringById(IDS_PLANVIEW_LIST003);
	m_szColum[eRegisterId] = LoadStringById(IDS_NOTIMNGVIEW_LST006);
	m_szColum[eCalendar    ] = _T("");

	m_lcList.InsertColumn(eCheck       , m_szColum[eCheck       ], LVCFMT_CENTER, 22);
	m_lcList.InsertColumn(eSiteName    , m_szColum[eSiteName    ], LVCFMT_CENTER, 60);
	m_lcList.InsertColumn(ePlan        , m_szColum[ePlan        ], LVCFMT_LEFT  , 90);
	m_lcList.InsertColumn(eDownloadTime, m_szColum[eDownloadTime], LVCFMT_CENTER,110);
	m_lcList.InsertColumn(eRegisterId, m_szColum[eRegisterId], LVCFMT_CENTER,60);
	m_lcList.InsertColumn(eCalendar    , m_szColum[eCalendar    ], LVCFMT_LEFT  ,100);

	// 제일 마지막에 할것...
	m_lcList.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	CTime tmBaseDate;
	m_dtFilterBaseDate.GetTime(tmBaseDate);

	m_lcList.SetSortEnable(false);
	//m_lcList.SetFixedCol(eDownloadTime);
	m_lcList.SetFixedCol(eRegisterId);
	m_lcList.SetColType(eCalendar
					  , CUTBHeaderCtrl::COL_INDICATOR_CALENDAR
					  , CTime(tmBaseDate.GetYear(), tmBaseDate.GetMonth(), tmBaseDate.GetDay(), 0, 0, 0)
					  , 3
					  );
}

void CBroadcastPlanView::RefreshList()
{
	m_lcList.DeleteAllItems();
	m_lsInfoList.RemoveAll();

	CString strWhere;
	CString strTmp;

	// 방송계획명
	if(!m_stListFilter.strBpName.IsEmpty())
	{
		strTmp.Format(_T("lower(bpName) like '%%%s%%'"), m_stListFilter.strBpName.MakeLower());
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	// 태그,키워드 조건 추가
	m_stListFilter.strTag.Trim();
	if(!m_stListFilter.strTag.IsEmpty())
	{
		int pos = 0;
		CString strTag = m_stListFilter.strTag.Tokenize("/", pos);
		while(strTag != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', description, '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s description like '%%%s%%'")
								, (strWhere.IsEmpty() ? _T("") : _T("and"))
								, strTag.MakeLower()
								);

			strTag = m_stListFilter.strTag.Tokenize("/", pos);
		}
	}

	// 작성자
	if(!m_stListFilter.strRegister.IsEmpty())
	{
		strTmp.Format(_T("registerId = '%s'"), m_stListFilter.strRegister);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	// 조회기준일을 기준으로 보이도록 한다
	// 항상 현재일을 기준으로 보이도록 한다
	if(m_stListFilter.bBaseDate)
	{
		CString strBaseDate = m_stListFilter.tmBaseDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
		//CTime tmCur = CTime::GetCurrentTime();
		//CString strBaseDate = tmCur.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
		//strTmp.Format(_T("startDate >= '%s' and endDate > '%s'"), strStartDate, strStartDate);
		strTmp.Format(_T("endDate > CAST('%s' as datetime)"), strBaseDate);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	// 작성일자
	CString strStartDate = _T("1970/01/01 00:00:00");
	CString strEndDate = _T("2037/12/31 23:59:59");
	if(m_stListFilter.bStartDate) strStartDate = m_stListFilter.tmStartDate.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	if(m_stListFilter.bEndDate) strEndDate = m_stListFilter.tmEndDate.Format(_T("%Y/%m/%d")) + _T(" 23:59:59");
	strTmp.Format(_T("createTime between CAST('%s' as datetime) and CAST('%s' as datetime)"), strStartDate, strEndDate);
	strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

	// 포함패키지
	if(!m_stListFilter.strPackageId.IsEmpty())
	{
		strTmp.Format(_T("bpId in (select bpId from UBC_TP where lower(programId) like lower('%%%s%%'))"), m_stListFilter.strPackageId);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	// 포함단말
	if(!m_stListFilter.strHostId.IsEmpty())
	{
		strTmp.Format(_T("bpId in (select bpId from UBC_TH where targetHost like '%%=%s')"), m_stListFilter.strHostId);
		strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;
	}

	if(m_kbAdvancedFilter.GetCheck())
	{
		m_cbAdvancedFilter.GetWindowText(strTmp);
		strWhere.AppendFormat(" %s (%s) "
							, (!strWhere.IsEmpty() ? _T("and") : _T(""))
							, strTmp
							);
		
		// 고급필터에 select,(,) 문자사용하지 못하도록함.
		if(!strTmp.IsEmpty())
		{
			CString strTemp = strTmp;
			strTemp.MakeUpper();
			if(strTemp.Find("SELECT") >= 0 || strTemp.FindOneOf("()") >= 0)
			{
				UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG021));
				return;
			}
		}
	}

	strWhere += _T(" order by zorder");
	strWhere.Trim();

	m_lcList.SetColType(eCalendar
					  , CUTBHeaderCtrl::COL_INDICATOR_CALENDAR
					  , CTime(m_stListFilter.tmBaseDate.GetYear(), m_stListFilter.tmBaseDate.GetMonth(), m_stListFilter.tmBaseDate.GetDay(), 0, 0, 0)
					  , 3
					  );

#ifdef _DEBUG
//	UbcMessageBox(strWhere);
#endif

	if(!CCopModule::GetObject()->GetBroadcastingPlanList( (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
														, m_lsInfoList
														, strWhere ))
	{
		return;
	}

	TraceLog(("list count = %d", m_lsInfoList.GetCount()));

	int nRow = 0;
	POSITION pos = m_lsInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SBroadcastingPlanInfo& info = m_lsInfoList.GetNext(pos);

		CString strDateList;
		strDateList.Format(_T("DateList=%s~%s,|WeekInfo=%s|ExceptDay=%s|")
						 , info.tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S"))
						 , info.tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))
						 , info.strWeekInfo
						 , info.strExceptDay
						 );

#ifdef _DEBUG
//		UbcMessageBox(strDateList);
#endif

		m_lcList.InsertItem(nRow, "");
		m_lcList.SetItemText(nRow, eSiteName    , info.strSiteId);
		m_lcList.SetItemText(nRow, ePlan        , info.strBpName);
		m_lcList.SetItemText(nRow, eDownloadTime, info.tmDownloadTime.Format(_T("%Y/%m/%d %H:%M:%S")));
		m_lcList.SetItemText(nRow, eRegisterId, info.strRegisterId);
		m_lcList.SetItemText(nRow, eCalendar    , strDateList);
		m_lcList.SetItemData(nRow, (DWORD_PTR)posOld);

		//skpark eSiteUser 자기것이 아닌 경우, 회색으로 처리
		if(CCopModule::eSiteUser == GetEnvPtr()->m_Authority){
			if(info.strRegisterId != GetEnvPtr()->m_szLoginID){
				TraceLog(("NOT ALLOWED TO MODIFY : %s!=%s", info.strRegisterId, GetEnvPtr()->m_szLoginID));
				info.bReadOnly = true;
			}
		}

		if(info.bReadOnly) m_lcList.SetRowColor(nRow, RGB(255,255,255), RGB(128,128,128));

		nRow++;
	}

	RefreshDailyPlan(CTime(m_stListFilter.tmBaseDate.GetYear(), m_stListFilter.tmBaseDate.GetMonth(), m_stListFilter.tmBaseDate.GetDay(), 0, 0, 0));
}

int CBroadcastPlanView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rcClient;
	GetClientRect(rcClient);

	m_Reposition.SetParentRect(rcClient);

	return 0;
}

void CBroadcastPlanView::OnDestroy()
{
	__super::OnDestroy();
}

void CBroadcastPlanView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CBroadcastPlanView::OnBnClickedButtonFilterHost()
{
	CSelectHostDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.m_nAuthority = GetEnvPtr()->m_Authority;
	dlg.m_strSiteId = GetEnvPtr()->m_szSite;
	dlg.m_strIniPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	m_strHostId = _T("");
	SetDlgItemText(IDC_EDIT_FILTER_HOST, _T(""));

	if(dlg.DoModal() == IDOK)
	{
		m_strHostId = dlg.m_strHostId;
		SetDlgItemText(IDC_EDIT_FILTER_HOST, dlg.m_strHostName);
	}
}

void CBroadcastPlanView::OnBnClickedButtonPlanRefresh()
{
	GetDlgItemText(IDC_EDIT_FILTER_PLAN_NAME, m_stListFilter.strBpName  );
	GetDlgItemText(IDC_EDIT_FILTER_TAG      , m_stListFilter.strTag     );
	GetDlgItemText(IDC_EDIT_FILTER_REGISTER , m_stListFilter.strRegister);
	GetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , m_stListFilter.strPackageId);
	GetDlgItemText(IDC_EDIT_FILTER_HOST     , m_stListFilter.strHostName);
	m_stListFilter.strHostId = m_strHostId;

	m_stListFilter.strAdvancedFilter.Empty();
	if(m_kbAdvancedFilter.GetCheck()) m_cbAdvancedFilter.GetWindowText(m_stListFilter.strAdvancedFilter);

	m_stListFilter.bBaseDate = (m_dtFilterBaseDate.GetTime(m_stListFilter.tmBaseDate) == GDT_VALID);
	m_stListFilter.bStartDate = (m_dtFilterStart.GetTime(m_stListFilter.tmStartDate) == GDT_VALID);
	m_stListFilter.bEndDate = (m_dtFilterEnd.GetTime(m_stListFilter.tmEndDate) == GDT_VALID);

	SaveFilterData();

	RefreshList();
}

LRESULT CBroadcastPlanView::InvokeEvent(WPARAM wParam, LPARAM lParam)
{
	cciEvent* ev = (cciEvent*)lParam;

	ciString mgrId ;
	ciString siteId;
	ciString bpId  ;

	switch(wParam)
	{
	case IEH_CREBPID:
		{
			ev->getItem("mgrId" , mgrId );
			ev->getItem("siteId", siteId);
			ev->getItem("bpId"  , bpId  );
			EventHandlerCreated(mgrId.c_str(), siteId.c_str(), bpId.c_str());
		}
		break;
	case IEH_CHGBPID:
		{
			ev->getItem("mgrId" , mgrId );
			ev->getItem("siteId", siteId);
			ev->getItem("bpId"  , bpId  );
			EventHandlerChanged(mgrId.c_str(), siteId.c_str(), bpId.c_str());
		}
		break;
	case IEH_RMVBPID:
		{
			ev->getItem("mgrId" , mgrId );
			ev->getItem("siteId", siteId);
			ev->getItem("bpId"  , bpId  );
			EventHandlerRemoved(mgrId.c_str(), siteId.c_str(), bpId.c_str());
		}
		break;
	}

	return 0;
}

void CBroadcastPlanView::EventHandlerCreated(CString strMgrId, CString strSiteId, CString strBpId)
{
	GetTimePlanInfo(strBpId, TRUE);

	// 그냥 속편하게 다시 조회하도록 한다.
	RefreshList();
/*
	SBroadcastingPlanInfo info;
	if(!CCopModule::GetObject()->GetBroadcastingPlan(strSiteId, strBpId, info)) return;

	CString strDateList;
	strDateList.Format(_T("DateList=%s~%s,|WeekInfo=%s|ExceptDay=%s|")
					 , info.tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S"))
					 , info.tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))
					 , info.strWeekInfo
					 , info.strExceptDay
					 );

#ifdef _DEBUG
//		UbcMessageBox(strDateList);
#endif

	int nRow = m_lcList.GetItemCount();

	m_lcList.InsertItem(nRow, "");
	m_lcList.SetItemText(nRow, eSiteName, info.strSiteId);
	m_lcList.SetItemText(nRow, ePlan , info.strBpName);
	m_lcList.SetItemText(nRow, eCalendar, strDateList);
	m_lcList.SetItemData(nRow, (DWORD_PTR)m_lsInfoList.AddTail(info));
*/
}

void CBroadcastPlanView::EventHandlerChanged(CString strMgrId, CString strSiteId, CString strBpId)
{
	GetTimePlanInfo(strBpId, TRUE);

	// ZOrder 가 변경되었을 수도 있으므로 그냥 속편하게 다시 조회하도록 한다.
	RefreshList();

	//for(int i=0; i<m_lcList.GetItemCount() ;i++)
	//{
	//	POSITION pos = (POSITION)m_lcList.GetItemData(i);
	//	if(!pos) return;

	//	SBroadcastingPlanInfo& info = m_lsInfoList.GetAt(pos);

	//	if(info.strSiteId == strSiteId && info.strBpId == strBpId)
	//	{
	//		SBroadcastingPlanInfo infoNew;
	//		if(!CCopModule::GetObject()->GetBroadcastingPlanList( strSiteId, strBpId, infoNew ))
	//		{
	//			return;
	//		}

	//		info = infoNew;

	//		CString strDateList;
	//		strDateList.Format(_T("DateList=%s~%s,|WeekInfo=%s|ExceptDay=%s|")
	//						 , info.tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S"))
	//						 , info.tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))
	//						 , info.strWeekInfo
	//						 , info.strExceptDay
	//						 );

	//		m_lcList.SetItemText(i, eCalendar, strDateList);
	//		return;
	//	}
	//}
}

void CBroadcastPlanView::EventHandlerRemoved(CString strMgrId, CString strSiteId, CString strBpId)
{
	GetTimePlanInfo(strBpId, TRUE);

	// 그냥 속편하게 다시 조회하도록 한다.
	RefreshList();
/*
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		POSITION pos = (POSITION)m_lcList.GetItemData(i);
		if(!pos) return;

		SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);

		if(info.strSiteId == strSiteId && info.strBpId == strBpId)
		{
			m_lsInfoList.RemoveAt(pos);
			m_lcList.DeleteItem(i);
			return;
		}
	}
*/
}

CString CBroadcastPlanView::GetZOrder(int nIndex)
{
	if(nIndex < 0 || nIndex >= m_lcList.GetItemCount()) return _T("");

	POSITION pos = (POSITION)m_lcList.GetItemData(nIndex);
	if(!pos) return _T("");

	SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);
	return info.strZOrder;
}

void CBroadcastPlanView::OnBnClickedButtonPlanNew()
{
	SBroadcastingPlanInfo info;
	info.nProcType = PROC_TYPE::PROC_NEW;
	info.strSiteId = GetEnvPtr()->m_szSite;

	CBroadcastPlanDlg dlg(CBroadcastPlanDlg::eModeCreate, &info, GetZOrder(m_lcList.GetItemCount()-1));
	if(dlg.DoModal() == IDOK)
	{
		//RefreshList();
	}

	m_lcList.Invalidate();
}

void CBroadcastPlanView::OnBnClickedButtonPlanDel()
{
	if(m_lcList.GetItemCount() <= 0) return;

	int nCheckedCnt = 0;
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		if(m_lcList.GetCheck(i)) nCheckedCnt++;
	}

	if(nCheckedCnt <= 0)
	{
		UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG001));
		return;
	}

	int targetCount = 0;
	for(int nRow = m_lcList.GetItemCount()-1; 0 <= nRow; nRow--) {
		if(!m_lcList.GetCheck(nRow)) continue;
		POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
		if(!pos) continue;
		targetCount++;
	}
	if(targetCount > 1) {
		UbcMessageBox(IDS_HOSTVIEW_MSG024);
		return;
	}

	// 정말로 삭제하시겠습니까?
	if(UbcMessageBox(LoadStringById(IDS_HOSTVIEW_MSG022), MB_YESNO) != IDYES)
	{
		return;
	}

	bool bSuccess = true;
	for(int i=m_lcList.GetItemCount()-1; i>=0 ;i--)
	{
		if(!m_lcList.GetCheck(i)) continue;

		POSITION pos = (POSITION)m_lcList.GetItemData(i);
		if(!pos) return;

		SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);

		// 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능
		if(info.bReadOnly) continue;

		if(info.strSiteId.GetLength()==0 || info.strBpId.GetLength()==0) continue;
		if(!CCopModule::GetObject()->DelBroadcastingPlan(info.strSiteId, info.strBpId))
		{
			bSuccess = false;
			CString strMsg;
			strMsg.Format(LoadStringById(IDS_PLANVIEW_MSG002)
						, info.strSiteId
						, info.strBpName
						);
			UbcMessageBox(strMsg);
			break;
		}

		m_lsInfoList.RemoveAt(pos);
		m_lcList.DeleteItem(i);
	}

	if(bSuccess)
	{
		UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG003));
	}

	//RefreshList();
}

void CBroadcastPlanView::OnNMDblclkListPlan(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("BMOD")) && !IsAuth(_T("BDTL"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	if(pNMLV->iItem < 0) return;
	*pResult = 0;

	POSITION pos = (POSITION)m_lcList.GetItemData(pNMLV->iItem);
	if(!pos) return;

	SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);
	info.nProcType = PROC_TYPE::PROC_UPDATE;

	CBroadcastPlanDlg dlg( CBroadcastPlanDlg::eModeUpdate, &info, GetZOrder(m_lcList.GetItemCount()-1));
	if(dlg.DoModal() == IDOK)
	{
//		RefreshList();

		//CString strDateList;
		//strDateList.Format(_T("DateList=%s~%s,|WeekInfo=%s|ExceptDay=%s|")
		//				 , info.tmStartDate.Format(_T("%Y/%m/%d %H:%M:%S"))
		//				 , info.tmEndDate.Format(_T("%Y/%m/%d %H:%M:%S"))
		//				 , info.strWeekInfo
		//				 , info.strExceptDay
		//				 );

		//m_lcList.SetItemText(pNMLV->iItem, eCalendar, strDateList);
		//m_lsInfoList.SetAt(pos, info);
	}

	m_lcList.Invalidate();
}

void CBroadcastPlanView::OnBnClickedButtonPlanUp()
{
	if(m_lcList.GetItemCount() <= 1) return;

	int nIndex = m_lcList.GetNextItem(-1, LVIS_SELECTED);

	if(nIndex < 0)
	{
		UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG004));
		return;
	}

	POSITION pos = (POSITION)m_lcList.GetItemData(nIndex);
	if(!pos) return;

	SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);

	// 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능
	if(info.bReadOnly)
	{
		UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG012));
		return;
	}

	if(!CCopModule::GetObject()->SetBroadcastingPlanZorder(info.strSiteId, info.strBpId, GetZOrder(nIndex-2), GetZOrder(nIndex-1)))
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_PLANVIEW_MSG005)
					, info.strSiteId
					, info.strBpName
					);
		UbcMessageBox(strMsg);
		return;
	}

	UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG006));

//	RefreshList();
}

void CBroadcastPlanView::OnBnClickedButtonPlanDown()
{
	if(m_lcList.GetItemCount() <= 1) return;

	int nIndex = m_lcList.GetNextItem(-1, LVIS_SELECTED);

	if(nIndex < 0)
	{
		UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG004));
		return;
	}

	POSITION pos = (POSITION)m_lcList.GetItemData(nIndex);
	if(!pos) return;

	SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);

	// 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능
	if(info.bReadOnly)
	{
		UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG012));
		return;
	}

	if(!CCopModule::GetObject()->SetBroadcastingPlanZorder(info.strSiteId, info.strBpId, GetZOrder(nIndex+1), GetZOrder(nIndex+2)))
	{
		CString strMsg;
		strMsg.Format(LoadStringById(IDS_PLANVIEW_MSG005)
					, info.strSiteId
					, info.strBpName
					);
		UbcMessageBox(strMsg);
		return;
	}

	UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG006));

//	RefreshList();
}

BOOL CBroadcastPlanView::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN   )
	{
		if( pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_PLAN_NAME)->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_TAG      )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_REGISTER )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_PACKAGE  )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_DATE_FILTER_START    )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_DATE_FILTER_END      )->GetSafeHwnd() ||
			pMsg->hwnd == GetDlgItem(IDC_EDIT_FILTER_HOST     )->GetSafeHwnd() )
		{
			OnBnClickedButtonPlanRefresh();
			return TRUE;
		}
	}

	return __super::PreTranslateMessage(pMsg);
}

void CBroadcastPlanView::OnLvnBegindragListPlan(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("BMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if (m_lcList.GetSelectedCount() != 1) return;

	int nIndex = m_lcList.GetNextItem(-1, LVIS_SELECTED);

	POSITION pos = (POSITION)m_lcList.GetItemData(nIndex);
	if(!pos) return;

	// 0001374: [RFP] 지점에서 상위조직에서 설정한 방송계획을 볼 수 있고, 우선순위를 조정할 수 있는 기능
	SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);
	if(info.bReadOnly)
	{
		UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG012));
		return;
	}

	CPoint pt;
	m_pListDragImage = m_lcList.CreateDragImage(nIndex, &pt);
	if(m_pListDragImage != NULL)
	{
		CPoint ptAction(pNMLV->ptAction);
		CRect rcItem;
		m_lcList.GetItemRect(nIndex, &rcItem, LVIR_BOUNDS);
		m_pListDragImage->BeginDrag(0, CPoint(ptAction.x - rcItem.left, ptAction.y - rcItem.top));

		m_lcList.ClientToScreen(&ptAction);
		ScreenToClient(&ptAction);
		m_pListDragImage->DragEnter(this, ptAction);
		SetCapture();
	}
}

void CBroadcastPlanView::OnMouseMove(UINT nFlags, CPoint point)
{
	CPoint ptScreen(point);
	ClientToScreen(&ptScreen);
	CWnd* pWndDrop = WindowFromPoint(ptScreen);
	bool bCopyEnable = false;

	if(m_pListDragImage != NULL)
	{
		m_pListDragImage->DragShowNolock(FALSE);

		// 리스트에서 이동할때
		if(pWndDrop->m_hWnd == m_lcList.m_hWnd)
		{
			LVHITTESTINFO listInfo;
			memset(&listInfo, 0x00, sizeof(LVHITTESTINFO));
			listInfo.pt = ptScreen;
			m_lcList.ScreenToClient(&listInfo.pt);
			m_lcList.HitTest(&listInfo);

			int nIndex = m_lcList.GetNextItem(-1, LVIS_SELECTED);

			if( LVHT_ONITEM & listInfo.flags &&
				listInfo.iItem >= 0          &&
				listInfo.iItem != nIndex     )
			{
				bCopyEnable = true;

				m_pListDragImage->DragShowNolock(TRUE);
				m_pListDragImage->DragMove(point);
			}
		}
		// 그외의 화면이나 컨트롤로 드래그할 경우
		else
		{
			m_pListDragImage->DragShowNolock(TRUE);
			m_pListDragImage->DragMove(point);
		}

		SetCursor(LoadCursor(NULL, (bCopyEnable ? IDC_HAND : IDC_NO)));
	}

	__super::OnMouseMove(nFlags, point);
}

void CBroadcastPlanView::OnLButtonUp(UINT nFlags, CPoint point)
{
	ReleaseCapture();

	CPoint ptScreen(point);
	ClientToScreen(&ptScreen);
	CWnd* pWndDrop = WindowFromPoint(ptScreen);
	if (!pWndDrop) return;

	if(m_pListDragImage != NULL)
	{
		m_pListDragImage->EndDrag();
		delete m_pListDragImage;
		m_pListDragImage = NULL;

		if( pWndDrop->m_hWnd != m_lcList.m_hWnd ) return;

		LVHITTESTINFO listInfo;
		memset(&listInfo, 0x00, sizeof(LVHITTESTINFO));
		listInfo.pt = ptScreen;
		m_lcList.ScreenToClient(&listInfo.pt);
		m_lcList.HitTest(&listInfo);

		int nIndex = m_lcList.GetNextItem(-1, LVIS_SELECTED);

		if( LVHT_ONITEM & listInfo.flags &&
			listInfo.iItem >= 0          &&
			listInfo.iItem != nIndex     )
		{
			POSITION pos = (POSITION)m_lcList.GetItemData(nIndex);
			if(pos)
			{
				SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);

				if(!CCopModule::GetObject()->SetBroadcastingPlanZorder(info.strSiteId
																	 , info.strBpId
																	 , GetZOrder((nIndex > listInfo.iItem) ? listInfo.iItem-1 : listInfo.iItem)
																	 , GetZOrder((nIndex < listInfo.iItem) ? listInfo.iItem+1 : listInfo.iItem)))
				{
					CString strMsg;
					strMsg.Format(LoadStringById(IDS_PLANVIEW_MSG005)
								, info.strSiteId
								, info.strBpName
								);
					UbcMessageBox(strMsg);
				}
			}
		}
	}

	__super::OnLButtonUp(nFlags, point);
}

// 방송계획 USB Export 기능
void CBroadcastPlanView::OnBnClickedButtonPlanExport()
{
	CString szMsg;
	if(m_lcList.GetItemCount() <= 0) return;

	int nCheckedCnt = 0;
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		if(m_lcList.GetCheck(i)) nCheckedCnt++;
	}

	//if(nCheckedCnt <= 0) 내보내기시 하나씩만 가능하므로 하나만 선택했는지 체크한다.
	if(nCheckedCnt != 1)
	{
		UbcMessageBox(LoadStringById(IDS_PLANVIEW_MSG007));
		return;
	}

	CString strBpFileList;

	bool bSuccess = true;
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		if(!m_lcList.GetCheck(i)) continue;

		POSITION pos = (POSITION)m_lcList.GetItemData(i);
		if(!pos) return;

		CString szFile, szLocal, szRemote;
		szRemote.Format("/config/");
		szLocal.Format("%s\\%s"
					, GetEnvPtr()->m_szDrive
					, UBC_CONFIG_PATH);

		SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);
		if(info.strBpId.IsEmpty()) continue;

		szFile.Format("%s_%s.bpi", GetEnvPtr()->m_szSite, info.strBpName);

		strBpFileList += szFile + ":";

		if(!GetEnvPtr()->GetFile(szFile, szRemote, szLocal, GetEnvPtr()->m_szSite))
		{
			//szMsg.Format(_T("서버로 부터 [%s]방송계획 파일을 다운로드 할 수 없습니다"), info.strBpName);
			szMsg.Format(LoadStringById(IDS_PLANVIEW_MSG008), info.strBpName);
			UbcMessageBox(szMsg, MB_ICONWARNING);
			return;
		}

		TimePlanInfoList lsTimePlanList;
		BOOL bResult = CCopModule::GetObject()->GetTimePlan((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
															, info.strBpId
															, lsTimePlanList
															);
		if(!bResult)
		{
			szMsg.Format(LoadStringById(IDS_PLANVIEW_MSG011), info.strBpName);
			UbcMessageBox(szMsg, MB_ICONERROR);
			return;
		}

		POSITION posTimePlan = lsTimePlanList.GetHeadPosition();
		while(posTimePlan)
		{
			STimePlanInfo& stTimePlanInfo = lsTimePlanList.GetNext(posTimePlan);
			if(stTimePlanInfo.nProcType == PROC_DELETE) continue;

			szFile.Format("%s.ini", stTimePlanInfo.strProgramId);

			if(!GetEnvPtr()->GetFile(szFile, szRemote, szLocal, GetEnvPtr()->m_szSite))
			{
				//szMsg.Format(_T("서버로 부터 [%s]콘텐츠 패키지를 다운로드 할 수 없습니다"), stTimePlanInfo.strProgramId);
				szMsg.Format(LoadStringById(IDS_PLANVIEW_MSG009), info.strBpName);
				UbcMessageBox(szMsg, MB_ICONWARNING);
				return;
			}
		}

		CFtpMultiSite dlgMultiFtp(GetEnvPtr()->m_strHttpOnly.CompareNoCase("CLIENT") == 0);
		posTimePlan = lsTimePlanList.GetHeadPosition();
		while(posTimePlan)
		{
			STimePlanInfo& stTimePlanInfo = lsTimePlanList.GetNext(posTimePlan);

			if(stTimePlanInfo.nProcType == PROC_DELETE) continue;

			CString szPackage;
			szPackage.Format("%s\\%s%s.ini", GetEnvPtr()->m_szDrive, UBC_CONFIG_PATH, stTimePlanInfo.strProgramId);

			if(!dlgMultiFtp.AddSite(GetEnvPtr()->m_szSite, szPackage))
			{
				//szMsg.Format(_T("[%s]콘텐츠 패키지에 포함된 콘텐츠를 다운로드할 서버를 찾지못하였습니다"), GetEnvPtr()->m_szSite, stTimePlanInfo.strProgramId);
				szMsg.Format(LoadStringById(IDS_PLANVIEW_MSG010), info.strBpName);
				UbcMessageBox(szMsg, MB_ICONERROR);
				return;
			}
		}

		if(!dlgMultiFtp.RunFtp(CFtpMultiSite::eDownload)) return;
	}

	strBpFileList.Trim(_T(":"));

	if(!strBpFileList.IsEmpty())
	{
		// UBC방송계획Exporter 호출
		TerminateProcess(_T("UBCImporter.exe"));

		CString szArg;
		//szArg.Format(_T(" +self +drive %s +bpi %s"
		szArg.Format(_T(" +export +drive %s +bpi %s")
					, "all" //GetEnvPtr()->m_szDrive
					, strBpFileList
					);

		//szArg += " +display ";
		//if(nA){
		//szArg += " 0";
		//if(nB) szArg += "1";
		//}
		//else if(nB){
		//szArg += " 1";
		//}

		CString strCommand;
		strCommand.Format(_T("%s\\%s%s")
						, GetEnvPtr()->m_szDrive
						, UBC_EXECUTE_PATH
						, _T("UBCImporter.exe")
						);

		if(scratchUtil::getInstance()->createProcess(strCommand.GetBuffer(0), szArg.GetBuffer(0), 0, FALSE));
	}
}

void CBroadcastPlanView::OnBnClickedButtonFilterRegister()
{
	CSelectUsers dlg;
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_REGISTER, _T(""));
		return;
	}

	UserInfoList& list = dlg.GetSelectedList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SUserInfo info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_REGISTER, info.userId);
	}
}

void CBroadcastPlanView::OnBnClickedButtonFilterPackage()
{
	CPackageSelectDlg dlg(GetEnvPtr()->m_strCustomer);
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, _T(""));
		return;
	}

	PackageInfoList& list = dlg.GetSelectedPackageList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SPackageInfo& info = list.GetNext(pos);
		SetDlgItemText(IDC_EDIT_FILTER_PACKAGE, info.szPackage);
	}
}

void CBroadcastPlanView::OnLvnColumnclickListPlan(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	CUTBHeaderCtrl::P_HDR_COL_INFO pHdrColInfo = m_lcList.GetColInfo(pNMLV->iSubItem);
	if(!pHdrColInfo) return;
	if(pHdrColInfo->m_nColType != CUTBHeaderCtrl::COL_INDICATOR_CALENDAR) return;

	RefreshDailyPlan(pHdrColInfo->m_tmSelDate);
}

void CBroadcastPlanView::RefreshDailyPlan(CTime tmSelDate)
{
	TraceLog(("RefreshDailyPlan - start"));

	m_tmSelDate = tmSelDate;

	m_dpDailyPlan.DeleteAllItem();

	TraceLog(("m_dpDailyPlan.GetItemCount() = %d", m_dpDailyPlan.GetItemCount()));

	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		POSITION posBP = (POSITION)m_lcList.GetItemData(i);
		if(!posBP) return;

		if(m_nDailyPlanView == 0 && !m_lcList.GetItemState(i, LVIS_SELECTED)) continue;

		SBroadcastingPlanInfo& infoBP = m_lsInfoList.GetAt(posBP);

		if( infoBP.tmStartDate.Format(_T("%Y/%m/%d")) > tmSelDate.Format(_T("%Y/%m/%d")) ||
			infoBP.tmEndDate.Format(_T("%Y/%m/%d")) < tmSelDate.Format(_T("%Y/%m/%d"))   )
		{
			continue;
		}

		if(infoBP.strWeekInfo.Find(ToString(tmSelDate.GetDayOfWeek()-1)) < 0) continue;
		if(infoBP.strExceptDay.Find(tmSelDate.Format(_T("%m.%d"))) >= 0) continue;

		//TimePlanInfoList lsTimePlanList;
		//BOOL bResult = CCopModule::GetObject()->GetTimePlan((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
		//													, infoBP.strBpId
		//													, lsTimePlanList
		//													);
		//if(!bResult)
		//{
		//	CString szMsg;
		//	szMsg.Format(LoadStringById(IDS_PLANVIEW_MSG011), infoBP.strBpName);
		//	UbcMessageBox(szMsg, MB_ICONERROR);
		//	return;
		//}

		TimePlanInfoList* pTimePlanInfoList = GetTimePlanInfo(infoBP.strBpId, FALSE);
		if(!pTimePlanInfoList) continue;

		POSITION posTP = pTimePlanInfoList->GetHeadPosition();
		while(posTP)
		{
			POSITION posOld = posTP;
			STimePlanInfo& infoTP = pTimePlanInfoList->GetNext(posTP);

			if(infoTP.nProcType == PROC_DELETE) continue;

			m_dpDailyPlan.AddItem( infoTP.strProgramId
								 , NormalizeTime(_ttoi(infoTP.strStartTime.Left(2)), _ttoi(infoTP.strStartTime.Mid(3,2))).GetTime()
								 , NormalizeTime(_ttoi(infoTP.strEndTime.Left(2)), _ttoi(infoTP.strEndTime.Mid(3,2))).GetTime()
								 , (LONG*)posOld
								 );
		}
	}

	m_dpDailyPlan.SetSelectedIndex(m_dpDailyPlan.GetItemCount()-1);

	TraceLog(("RefreshDailyPlan - end"));
}

void CBroadcastPlanView::OnBnClickedRbDailyPlanView()
{
	UpdateData(TRUE);

	m_nDailyPlanView = (m_rbDailyPlanView1.GetCheck() ? 0 : 1);

	RefreshDailyPlan(m_tmSelDate);
}

void CBroadcastPlanView::OnNMClickListPlan(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	if(m_nDailyPlanView == 0)
	{
		RefreshDailyPlan(m_tmSelDate);
	}
}

TimePlanInfoList* CBroadcastPlanView::GetTimePlanInfo(CString strBpId, BOOL bForceCall)
{
	TimePlanInfoList* pTimePlanInfoList = NULL;

	CString strKey = strBpId;
	if(strKey.IsEmpty()) return NULL;

	if(bForceCall || !m_mapTimePlanInfo.Lookup(strKey, (void*&)pTimePlanInfoList))
	{
		if(bForceCall && pTimePlanInfoList)
		{
			m_mapTimePlanInfo.RemoveKey(strKey);
			delete pTimePlanInfoList;
		}

		pTimePlanInfoList = new TimePlanInfoList;
		BOOL bResult = CCopModule::GetObject()->GetTimePlan((CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
															, strBpId
															, *pTimePlanInfoList
															);
		if(!bResult)
		{
			delete pTimePlanInfoList;
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_PLANVIEW_MSG011), strBpId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			return NULL;
		}

		m_mapTimePlanInfo[strKey] = pTimePlanInfoList;
	}

	return pTimePlanInfoList;
}

void CBroadcastPlanView::ItemDblClickDpDailyPlan(long nIndex)
{
	if(nIndex < 0) return;

	ST_PLAN_ITEM stInfo;
	if(m_dpDailyPlan.GetItem(nIndex, (LONG*)&stInfo))
	{
		RunStudio(stInfo.strText);
	}
}

void CBroadcastPlanView::RunStudio(CString strPackage)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szLocation[MAX_PATH];
	_tsplitpath(szModule, szDrive, szLocation, NULL, NULL);

	CString szPath;
	szPath.Format(_T("\"%s%s\""), szDrive, szLocation);

	CString szParam;
	szParam.Format(_T(" +siteid=%s +userid=%s +passwd=%s +package=%s")
				  , CEnviroment::GetObject()->m_szSite
				  , CEnviroment::GetObject()->m_szLoginID
				  , CEnviroment::GetObject()->m_szPassword
				  , strPackage
				  );

	HINSTANCE nRet = ShellExecute(NULL, NULL, UBCSTUDIOEEEXE_STR, szParam, szPath, SW_SHOWNORMAL);
}

void CBroadcastPlanView::OnNMRclickListPlan(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	if(pNMLV->iItem < 0) return;
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);

	pPopup = menu.GetSubMenu(4);

	pPopup->EnableMenuItem(ID_DOWNLOADSUMMARY_VIEW, (IsAuth(_T("HDNV")) ?MF_ENABLED : MF_DISABLED | MF_GRAYED));

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CBroadcastPlanView::OnDownloadsummaryView()
{
	if(m_ptSelList.y < 0) return;

	POSITION pos = (POSITION)m_lcList.GetItemData(m_ptSelList.y);
	if(!pos) return;

	SBroadcastingPlanInfo info = m_lsInfoList.GetAt(pos);

	CDownloadSummaryDlg dlg;
	dlg.SetParam(info.strBpId, info.strBpName, _T(""), _T(""), _T(""));
	dlg.DoModal();
}

void CBroadcastPlanView::OnBnClickedKbAdvancedFilter()
{
	m_cbAdvancedFilter.EnableWindow(m_kbAdvancedFilter.GetCheck());
}

void CBroadcastPlanView::SaveFilterData()
{
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	WritePrivateProfileString("BP-FILTER", "BpName"        , m_stListFilter.strBpName        , szPath);
	WritePrivateProfileString("BP-FILTER", "Tag"           , m_stListFilter.strTag           , szPath);
	WritePrivateProfileString("BP-FILTER", "Register"      , m_stListFilter.strRegister      , szPath);
	WritePrivateProfileString("BP-FILTER", "PackageId"     , m_stListFilter.strPackageId     , szPath);
	//WritePrivateProfileString("BP-FILTER", "BaseDate"      , m_stListFilter.tmBaseDate.Format(_T("%Y/%m/%d%%20%H:%M:%S")), szPath);
	//WritePrivateProfileString("BP-FILTER", "StartDate"     , m_stListFilter.tmStartDate.Format(_T("%Y/%m/%d%%20%H:%M:%S")), szPath);
	//WritePrivateProfileString("BP-FILTER", "EndDate"       , m_stListFilter.tmEndDate.Format(_T("%Y/%m/%d%%20%H:%M:%S")), szPath);
	WritePrivateProfileString("BP-FILTER", "HostId"        , m_stListFilter.strHostId        , szPath);
	WritePrivateProfileString("BP-FILTER", "HostName"      , m_stListFilter.strHostName      , szPath);
	WritePrivateProfileString("BP-FILTER", "AdvancedFilter", m_stListFilter.strAdvancedFilter, szPath);

	if(m_kbAdvancedFilter.GetCheck() && m_cbAdvancedFilter.FindStringExact(0, m_stListFilter.strAdvancedFilter) < 0) 
	{
		m_cbAdvancedFilter.InsertString(0, m_stListFilter.strAdvancedFilter);
		for(int i=m_cbAdvancedFilter.GetCount(); i > 20 ; i--)
		{
			m_cbAdvancedFilter.DeleteString(i-1);
		}

		for(int i=0; i<m_cbAdvancedFilter.GetCount() ;i++)
		{
			CString strText;
			m_cbAdvancedFilter.GetLBText(i, strText);

			CString strKey;
			strKey.Format(_T("History%d"), i+1);
			WritePrivateProfileString("BP-FILTER", strKey, strText, szPath);
		}
	}
}

void CBroadcastPlanView::LoadFilterData()
{
	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	CString strCurDate = CTime::GetCurrentTime().Format(_T("%Y/%m/%d%%20%H:%M:%S"));
	CString strBaseDate;
	CString strStartDate;
	CString strEndDate;

	GetPrivateProfileString("BP-FILTER", "BpName"        , "", buf, 1024, szPath); m_stListFilter.strBpName = buf;
	GetPrivateProfileString("BP-FILTER", "Tag"           , "", buf, 1024, szPath); m_stListFilter.strTag = buf;
	GetPrivateProfileString("BP-FILTER", "Register"      , "", buf, 1024, szPath); m_stListFilter.strRegister = buf;
	GetPrivateProfileString("BP-FILTER", "PackageId"     , "", buf, 1024, szPath); m_stListFilter.strPackageId = buf;
	//GetPrivateProfileString("BP-FILTER", "BaseDate"      , strCurDate, buf, 1024, szPath); strBaseDate = buf;
	//GetPrivateProfileString("BP-FILTER", "StartDate"     , strCurDate, buf, 1024, szPath); strStartDate = buf;
	//GetPrivateProfileString("BP-FILTER", "EndDate"       , strCurDate, buf, 1024, szPath); strEndDate = buf;
	GetPrivateProfileString("BP-FILTER", "HostId"        , "", buf, 1024, szPath); m_stListFilter.strHostId = buf;
	GetPrivateProfileString("BP-FILTER", "HostName"      , "", buf, 1024, szPath); m_stListFilter.strHostName = buf;
	GetPrivateProfileString("BP-FILTER", "AdvancedFilter", "", buf, 1024, szPath); m_stListFilter.strAdvancedFilter = buf;

	SetDlgItemText(IDC_EDIT_FILTER_PLAN_NAME, m_stListFilter.strBpName  );
	SetDlgItemText(IDC_EDIT_FILTER_TAG      , m_stListFilter.strTag     );
	SetDlgItemText(IDC_EDIT_FILTER_REGISTER , m_stListFilter.strRegister);
	SetDlgItemText(IDC_EDIT_FILTER_PACKAGE  , m_stListFilter.strPackageId);
	SetDlgItemText(IDC_EDIT_FILTER_HOST     , m_stListFilter.strHostName);
	m_strHostId = m_stListFilter.strHostId;

	//m_stListFilter.tmBaseDate = CUTBListCtrlEx::ToDateTime(strBaseDate);
	//m_stListFilter.tmStartDate = CUTBListCtrlEx::ToDateTime(strStartDate);
	//m_stListFilter.tmEndDate = CUTBListCtrlEx::ToDateTime(strStartDate);

	m_stListFilter.bBaseDate = (m_dtFilterBaseDate.GetTime(m_stListFilter.tmBaseDate) == GDT_VALID);
	m_stListFilter.bStartDate = (m_dtFilterStart.GetTime(m_stListFilter.tmStartDate) == GDT_VALID);
	m_stListFilter.bEndDate = (m_dtFilterEnd.GetTime(m_stListFilter.tmEndDate) == GDT_VALID);

	m_kbAdvancedFilter.SetCheck(!m_stListFilter.strAdvancedFilter.IsEmpty());
	m_cbAdvancedFilter.EnableWindow(m_kbAdvancedFilter.GetCheck());
	m_cbAdvancedFilter.SetWindowText(m_stListFilter.strAdvancedFilter);

	for(int i=0; i<20 ;i++)
	{
		CString strKey;
		strKey.Format(_T("History%d"), i+1);
		GetPrivateProfileString("BP-FILTER", strKey, "", buf, 1024, szPath);

		CString strValue = buf;
		if(!strValue.IsEmpty())
		{
			m_cbAdvancedFilter.AddString(strValue);
		}
	}
}

void CBroadcastPlanView::OnNMCustomdrawList(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );

	if( CDDS_PREPAINT == pLVCD->nmcd.dwDrawStage ){
		*pResult = CDRF_NOTIFYITEMDRAW;
		return;
	}

	if( CDDS_ITEMPREPAINT != pLVCD->nmcd.dwDrawStage )
		return;

	int nRow = pLVCD->nmcd.dwItemSpec;

	SYSTEMTIME stm;
	GetLocalTime(&stm);
	CTime tmCur(stm);


	POSITION pos = (POSITION)m_lcList.GetItemData(nRow);
	if(!pos)	return;

	SBroadcastingPlanInfo& info = m_lsInfoList.GetAt(pos);

	/*
	if(info.tmStartDate <= tmCur && tmCur < info.tmEndDate){
		COLORREF crText = RGB(0,0,0);
        COLORREF crTextBk = RGB(255,255,0);
		pLVCD->clrText = crText;
        pLVCD->clrTextBk = crTextBk;
		*pResult = CDRF_DODEFAULT;
	}
	*/
	//skpark eSiteUser 자기것이 아닌 경우, 회색으로 처리
	if(info.bReadOnly){
		TraceLog(("NOT ALLOWED TO MODIFY : %s!=%s", info.strRegisterId, GetEnvPtr()->m_szLoginID));
		COLORREF crText = RGB(128,128,128);
		pLVCD->clrText = crText;
		//*pResult = CDRF_DODEFAULT;
		m_lcList.SetCheck(nRow,false);
		//m_lscNoti.RedrawItems(nRow,nRow);
	}
}
