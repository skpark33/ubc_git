// PackageView.cpp : implementation file
//
#include "stdafx.h"
#include "UBCManager.h"
#include "PackageView.h"
#include "Enviroment.h"
#include "ubccopcommon\CopModule.h"
#include "common\libscratch\scratchUtil.h"
#include "hi/libHttp/ubcMux.h"
#include "common/libCommon/ubcMuxRegacy.h"
#include "common/libCommon/ubcUtil.h"
#include "ubccopcommon/SelectUsers.h"
#include "ubccopcommon/ContentsSelectDlg.h"
#include "common/UbcCode.h"
#include "UBCCopCommon\UbcMenuAuth.h"
#include "CleaningUnusedPackage.h"
#include "HostExcelSave.h"

#define STR_SITE		    LoadStringById(IDS_PACKAGEVIEW_LST001)
#define STR_PACKAGE		    LoadStringById(IDS_PACKAGEVIEW_LST002)
#define STR_USER		    LoadStringById(IDS_PACKAGEVIEW_LST003)
#define STR_TIME		    LoadStringById(IDS_PACKAGEVIEW_LST004)
#define STR_DESCRIPT	    LoadStringById(IDS_PACKAGEVIEW_LST005)
#define STR_CATEGORY  	    LoadStringById(IDS_PACKAGEVIEW_LST006)
#define STR_PURPOSE   	    LoadStringById(IDS_PACKAGEVIEW_LST007)
#define STR_HOSTTYPE  	    LoadStringById(IDS_PACKAGEVIEW_LST008)
#define STR_VERTICAL  	    LoadStringById(IDS_PACKAGEVIEW_LST009)
#define STR_RESOLUTION	    LoadStringById(IDS_PACKAGEVIEW_LST010)
#define STR_PUBLIC    	    LoadStringById(IDS_PACKAGEVIEW_LST012)
#define STR_VERIFY  	    LoadStringById(IDS_PACKAGEVIEW_LST011)
#define STR_VALIDATEDATE    LoadStringById(IDS_PACKAGEVIEW_LST013)

// CPackageView
IMPLEMENT_DYNCREATE(CPackageView, CFormView)

CPackageView::CPackageView()
	: CFormView(CPackageView::IDD)
,	m_Reposition(this)
{
}

CPackageView::~CPackageView()
{
}

void CPackageView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_FILTER, m_Filter);
	DDX_Control(pDX, IDC_LIST_PACKAGE, m_lscPackage);
	DDX_Control(pDX, IDC_BUTTON_DEL, m_btnDel);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_btnAdd);
	DDX_Control(pDX, IDC_BUTTON_REF, m_btnRef);
	DDX_Control(pDX, IDC_BUTTON_UNUSED_DEL, m_btnUnused);
	DDX_Control(pDX, IDC_DT_START_DATE, m_dtStartDate);
	DDX_Control(pDX, IDC_DT_END_DATE, m_dtEndDate);
	DDX_Control(pDX, IDC_LIST_CATEGORY, m_lcFilter[eFilterCategory]);
	DDX_Control(pDX, IDC_LIST_PURPOSE, m_lcFilter[eFilterPurpose]);
	DDX_Control(pDX, IDC_LIST_HOSTTYPE, m_lcFilter[eFilterHostType]);
	DDX_Control(pDX, IDC_LIST_VERTICAL, m_lcFilter[eFilterVertical]);
	DDX_Control(pDX, IDC_LIST_RESOLUTION, m_lcFilter[eFilterResolution]);
	DDX_Control(pDX, IDC_LIST_PUBLIC, m_lcFilter[eFilterPublic]);
	DDX_Control(pDX, IDC_LIST_APPROVED, m_lcFilter[eFilterVerify]);
	DDX_Control(pDX, IDC_CB_INCLUDED_CONTENTS, m_cbxIncludedContents);
	DDX_Control(pDX, IDC_BN_TO_EXCEL4, m_btnExcelSave);
}

BEGIN_MESSAGE_MAP(CPackageView, CFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_COMMAND(ID_PACKAGE01_DELETE, OnPackageDelete)
	ON_COMMAND(ID_PACKAGE01_EDIT, OnPackageEdit)
	ON_COMMAND(ID_PACKAGE01_PLAY, OnPackagePlay)

	ON_NOTIFY(NM_RCLICK, IDC_LIST_PACKAGE, OnNMRclickList)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CPackageView::OnBnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DEL, &CPackageView::OnBnClickedButtonDel)
	ON_BN_CLICKED(IDC_BUTTON_REF, &CPackageView::OnBnClickedButtonRef)
	ON_BN_CLICKED(IDC_BN_REGISTER, &CPackageView::OnBnClickedBnRegister)
	ON_CBN_SELCHANGE(IDC_CB_INCLUDED_CONTENTS, OnCbnSelchangeCbIncludedContents)
	ON_BN_CLICKED(IDC_BUTTON_UNUSED_DEL, &CPackageView::OnBnClickedButtonUnusedDel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PACKAGE, &CPackageView::OnNMDblclkListPackage)
	ON_BN_CLICKED(IDC_BN_TO_EXCEL4, &CPackageView::OnBnClickedBnToExcel4)
END_MESSAGE_MAP()

// CPackageView diagnostics
#ifdef _DEBUG
void CPackageView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPackageView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CPackageView message handlers
int CPackageView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	GetClientRect(&m_rcClient);

	return 0;
}

void CPackageView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CTime tmCur = CTime::GetCurrentTime();
	CTime tmStart = CTime(2000,1,1,0,0,0);
	m_dtStartDate.SetTime(&tmStart);
	m_dtEndDate  .SetTime(&tmCur);

	InitDateRange(m_dtStartDate);
	InitDateRange(m_dtEndDate);

	m_dtStartDate.SetFormat(_T("yyyy/MM/dd"));
	m_dtEndDate.SetFormat(_T("yyyy/MM/dd"));

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_PACKAGEVIEW_STR001));

	m_Reposition.SetParentRect(m_rcClient);
	m_Reposition.RemoveAll();
	m_Reposition.AddControl(&m_lscPackage, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_Filter, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_FIX);

	m_btnAdd.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btnDel.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btnUnused.LoadBitmap(IDB_BUTTON_UNUSED, RGB(255,255,255));
	m_btnRef.LoadBitmap(IDB_BUTTON_REFRESH2, RGB(255,255,255));
	m_btnExcelSave.LoadBitmap(IDB_BUTTON_EXCEL, RGB(255,255,255));

	m_btnAdd.SetToolTipText(LoadStringById(IDS_PACKAGEVIEW_STR002));
	m_btnDel.SetToolTipText(LoadStringById(IDS_PACKAGEVIEW_STR003));
	m_btnUnused.SetToolTipText(LoadStringById(IDS_PACKAGEVIEW_STR009));
	m_btnRef.SetToolTipText(LoadStringById(IDS_PACKAGEVIEW_STR004));
	m_btnExcelSave.SetToolTipText(LoadStringById(IDS_HOSTVIEW_BTN012));

	// 권한에 따라 활성화 한다.
	m_btnAdd.EnableWindow(IsAuth(_T("PREG")));
	m_btnDel.EnableWindow(IsAuth(_T("PDEL")));
	m_btnUnused.EnableWindow(IsAuth(_T("PDEL")));
	m_btnRef.EnableWindow(IsAuth(_T("PQRY")));

	m_cbxIncludedContents.InsertString(0, LoadStringById(IDS_UBCMANAGER_STR001));
	m_cbxIncludedContents.InsertString(1, LoadStringById(IDS_UBCMANAGER_STR002));
	m_cbxIncludedContents.SetCurSel(0);

	InitFilterListCtrl();

	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterCategory  ], _T("Kind"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterPurpose   ], _T("Purpose"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterHostType  ], _T("HostType"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterVertical  ], _T("Direction"));
	CUbcCode::GetInstance()->FillListCtrl(m_lcFilter[eFilterResolution], _T("Resolution"));

	m_lcFilter[eFilterPublic].InsertItem(0, LoadStringById(IDS_PACKAGEVIEW_STR005));
	m_lcFilter[eFilterPublic].SetItemData(0,1);
	m_lcFilter[eFilterPublic].InsertItem(1, LoadStringById(IDS_PACKAGEVIEW_STR006));
	m_lcFilter[eFilterPublic].SetItemData(1,0);

	m_lcFilter[eFilterVerify].InsertItem(0, LoadStringById(IDS_PACKAGEVIEW_STR007));
	m_lcFilter[eFilterVerify].SetItemData(0,1);
	m_lcFilter[eFilterVerify].InsertItem(1, LoadStringById(IDS_PACKAGEVIEW_STR008));
	m_lcFilter[eFilterVerify].SetItemData(1,0);

	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CRect rect;
		m_lcFilter[i].GetClientRect(rect);
		CScrollBar* pBar = m_lcFilter[i].GetScrollBarCtrl(SB_VERT);
		int width = (pBar && pBar->IsWindowVisible()) ? rect.Width()-::GetSystemMetrics(SM_CXVSCROLL) : rect.Width();
		m_lcFilter[i].SetColumnWidth(0, width);
		//m_lcFilter[i].SetColumnWidth(0, LVSCW_AUTOSIZE);
	}

	InitPackageList();
	LoadFilterData();
//	SetPackageData();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscPackage.LoadColumnState("PACKAGE-LIST", szPath);
}

void CPackageView::OnDestroy()
{
	CFormView::OnDestroy();

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 리스트의 컬럼상태 저장
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscPackage.SaveColumnState("PACKAGE-LIST", szPath);
}

void CPackageView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);
	m_Reposition.Move();
}

void CPackageView::InitPackageList()
{
	// 2010.02.17 by gwangsoo
	m_szColName[eCheck       ] = _T("");
	m_szColName[eSite        ] = STR_SITE;
	m_szColName[ePackage     ] = STR_PACKAGE;
	m_szColName[eUser        ] = STR_USER;
	m_szColName[eTime        ] = STR_TIME;
	m_szColName[eDesc        ] = STR_DESCRIPT;
	m_szColName[eCategory    ] = STR_CATEGORY  ;
	m_szColName[ePurpose     ] = STR_PURPOSE   ;
	m_szColName[eHostType    ] = STR_HOSTTYPE  ;
	m_szColName[eVertical    ] = STR_VERTICAL  ;
	m_szColName[eResolution  ] = STR_RESOLUTION;
	m_szColName[ePublic      ] = STR_PUBLIC    ;
	m_szColName[eVerify      ] = STR_VERIFY    ;
	m_szColName[eValidateDate] = STR_VALIDATEDATE;
	m_szColName[eVolume      ] = LoadStringById(IDS_PACKAGEVIEW_LST014);

	m_lscPackage.SetExtendedStyle(m_lscPackage.GetExtendedStyle() | LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);

	for(int nCol = 0; nCol < eEnd; nCol++)
	{
		if(nCol == eVolume)
		{
			m_lscPackage.InsertColumn(nCol, m_szColName[nCol], LVCFMT_RIGHT, 70);
		}
		else
		{
			m_lscPackage.InsertColumn(nCol, m_szColName[nCol], LVCFMT_LEFT, 100);
		}
	}

	m_lscPackage.SetColumnWidth(m_lscPackage.GetColPos(m_szColName[eCheck]), 22); // 2010.02.17 by gwangsoo
	m_lscPackage.SetColumnWidth(ePackage, 140);
	m_lscPackage.SetColumnWidth(eTime, 120);
	m_lscPackage.SetColumnWidth(eDesc, 120);
	m_lscPackage.SetColumnWidth(eValidateDate, 120);

	m_lscPackage.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
}

void CPackageView::SetPackageData()
{
	CWaitMessageBox wait;

	CString strSiteID      ;
	CString strPackage     ;
	CString strTagList     ;
	CString strRegister    ;
	CString strContentsName;
	CString strContentsId  ;
	CString strStartDate   ;
	CString strEndDate     ;

	GetDlgItemText(IDC_EB_PACKAGE , strPackage     );
	GetDlgItemText(IDC_EB_TAG     , strTagList     );
	GetDlgItemText(IDC_EB_REGISTER, strRegister    );
	if(m_cbxIncludedContents.GetCount() > 2)
	{
		m_cbxIncludedContents.GetLBText(1, strContentsName);
		strContentsId = m_strFilterContentsId;
	}

	CTime tmTemp;
	m_dtStartDate.GetTime(tmTemp);
	strStartDate = tmTemp.Format("%Y/%m/%d");
	m_dtEndDate.GetTime(tmTemp);
	strEndDate = tmTemp.Format("%Y/%m/%d");

	// 사이트조건
	strSiteID = (GetEnvPtr()->m_Authority == CCopModule::eSiteAdmin ? _T("*") : GetEnvPtr()->m_szSite);

	CString strWhere;

	// 패키지이름
	strPackage.Trim();
	if(!strPackage.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s lower(programId) like '%%%s%%'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strPackage.MakeLower()
							);
	}

	// 태그, 키워드
	strTagList.Trim();
	if(!strTagList.IsEmpty())
	{
		int pos = 0;
		CString strTag;
		while((strTag = strTagList.Tokenize("/", pos)) != _T(""))
		{
			//strWhere.AppendFormat(_T(" %s concat('/', lower(description), '/') like '%%/%s/%%'")
			strWhere.AppendFormat(_T(" %s description like '%%%s%%'")
								, (strWhere.IsEmpty() ? _T("") : _T("and"))
								, strTag.MakeLower()
								);
		}
	}

	// 작성자
	strRegister.Trim();
	if(!strRegister.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s lastUpdateId = '%s'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strRegister
							);
	}

	// 포함콘텐츠
	strContentsId.Trim();
	if(!strContentsId.IsEmpty())
	{
		strWhere.AppendFormat(_T(" %s programId in (select programId from ubc_contents where lower(contentsId) = lower('%s'))")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, strContentsId
							);
	}

	// 작성일자
	CTime tmStart;
	m_dtStartDate.GetTime(tmStart);
	CTime tmEnd;
	m_dtEndDate.GetTime(tmEnd);
	tmEnd += CTimeSpan(1,0,0,0);

	strWhere.AppendFormat(_T(" %s lastUpdateTime between CAST('%s 00:00:00' as datetime) and CAST('%s 00:00:00' as datetime)")
						, (strWhere.IsEmpty() ? _T("") : _T("and"))
						, tmStart.Format(_T("%Y/%m/%d"))
						, tmEnd.Format(_T("%Y/%m/%d"))
						);

	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CString strFieldName;
		switch(i)
		{
		case eFilterCategory  : strFieldName = _T("contentsCategory"); break; // 종류별
		case eFilterPurpose   : strFieldName = _T("purpose"); break; // 용도별
		case eFilterHostType  : strFieldName = _T("hostType"); break; // 단말타입별
		case eFilterVertical  : strFieldName = _T("vertical"); break; // 가로/세로별
		case eFilterResolution: strFieldName = _T("resolution"); break; // 해상도별
		case eFilterPublic    : strFieldName = _T("isPublic"); break; // 공개여부
		case eFilterVerify    : strFieldName = _T("isVerify"); break; // 승인여부
		}

		// not all
		CString strFilter = GetSelectItemsStringFromList(m_lcFilter[i], strFieldName);
		if(!strFilter.IsEmpty())
		{
			strWhere.AppendFormat(_T(" %s %s")
								, (strWhere.IsEmpty() ? _T("") : _T("and"))
								, strFilter
								);
		}
	}

	//skpark 2015.2.13  show always isPublic=1
	if(strWhere.IsEmpty()) {
		strWhere = "isPublic=1";
	}else{
		strWhere.Format("(%s) or (isPublic=1 and %s)", strWhere, strWhere); 
	}

	strWhere += _T(" order by siteId, programId");
	strWhere.Trim();

	TraceLog(("%s", strWhere));

	GetEnvPtr()->m_lsPackage.RemoveAll();

	CCopModule::GetObject()->GetPackage(strSiteID, GetEnvPtr()->m_lsPackage, strWhere);

	m_lscPackage.DeleteAllItems();

	int nRow = 0;
	POSITION pos = GetEnvPtr()->m_lsPackage.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SPackageInfo info = GetEnvPtr()->m_lsPackage.GetNext(pos);

		m_lscPackage.InsertItem(nRow, "");

		m_lscPackage.SetItemText(nRow, eSite        , info.szSiteID    );
		m_lscPackage.SetItemText(nRow, ePackage     , info.szPackage   );
		m_lscPackage.SetItemText(nRow, eUser        , info.szUpdateUser);
		m_lscPackage.SetItemText(nRow, eDesc        , info.szDescript  );
		m_lscPackage.SetItemText(nRow, eVolume      , (info.volume < 0 ? "" : ToFileSize((ULONGLONG)info.volume, 'G')));
		m_lscPackage.SetItemText(nRow, eCategory    , CUbcCode::GetInstance()->GetCodeName("Kind"    , info.nContentsCategory));
		m_lscPackage.SetItemText(nRow, ePurpose     , CUbcCode::GetInstance()->GetCodeName("Purpose"    , info.nPurpose         ));
		m_lscPackage.SetItemText(nRow, eHostType    , CUbcCode::GetInstance()->GetCodeName("HostType", info.nHostType        ));
		m_lscPackage.SetItemText(nRow, eVertical    , CUbcCode::GetInstance()->GetCodeName("Direction", info.nVertical        ));
		m_lscPackage.SetItemText(nRow, eResolution  , CUbcCode::GetInstance()->GetCodeName("Resolution"  , info.nResolution      ));
		m_lscPackage.SetItemText(nRow, ePublic      , (info.bIsPublic?LoadStringById(IDS_PACKAGEVIEW_STR005):LoadStringById(IDS_PACKAGEVIEW_STR006)));
		m_lscPackage.SetItemText(nRow, eVerify      , (info.bIsVerify?LoadStringById(IDS_PACKAGEVIEW_STR007):LoadStringById(IDS_PACKAGEVIEW_STR008)));

		if(info.tmValidationDate > 0)
		{
			CTime tmValidationDate(info.tmValidationDate);
			m_lscPackage.SetItemText(nRow, eValidateDate, tmValidationDate.Format(STR_DEF_TIME));
		}

		CTime tm(info.tmUpdateTime);
		m_lscPackage.SetItemText(nRow, eTime, tm.Format(STR_DEF_TIME));

		m_lscPackage.SetItemData(nRow, (DWORD_PTR)posOld);

		nRow++;
	}
}

void CPackageView::OnPackageDelete()
{
	// 선택한 콘텐츠패키지를 삭제하시겠습니까?
	if(UbcMessageBox(LoadStringById(IDS_PACKAGEVIEW_MSG004), MB_YESNO) != IDYES)
	{
		return;
	}

	CString strMsg;

	// 0000560: 매니저에서 콘텐츠 패키지를 삭제시, 해당 콘텐츠 패키지가 사용중이라면, 사용중이라는 경고문을 넣는다. 
	CString szPackage = m_lscPackage.GetItemText(m_ptSelList.y, ePackage);
	ciStringSet hostSet;
	int nCnt = ubcUtil::getInstance()->getHostListUsingThisProgram(szPackage, hostSet);
	if(nCnt > 0)
	{
		strMsg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG001), szPackage);

		ciStringSet::iterator itr;
		for(itr=hostSet.begin();itr!=hostSet.end();itr++)
		{
			ciString hostId = (*itr);
			strMsg += _T("\n");
			strMsg += hostId.c_str();
		}

		if(UbcMessageBox(strMsg, MB_YESNO) != IDYES) return;
	}

	// 방송계획으로 사용중인지 체크하여 지울지 판단한다.
	CString strBpList;
	if(!GetBroadcastPlan(szPackage, strBpList)) return;
	if(!strBpList.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG009), szPackage);
		UbcMessageBox(strMsg + strBpList); 
		return;
	}

	Remove(m_ptSelList.y);
}

void CPackageView::OnPackageEdit()
{
	CString szPackage = m_lscPackage.GetItemText(m_ptSelList.y, ePackage);
/*
	unsigned long lPid = scratchUtil::getInstance()->getPid(UBCSTUDIOEEEXE_STR);
	HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);

	if(hWnd){
		CWnd* pWnd = CWnd::FromHandle(hWnd);
		pWnd->ActivateTopParent();

		if(pWnd->IsIconic()){
			pWnd->ShowWindow(SW_RESTORE);
		}

		COPYDATASTRUCT cds;
		cds.dwData = (ULONG_PTR)eCDSetPackage;
		cds.cbData = szPackage.GetLength();
		cds.lpData = (PVOID)(LPCTSTR)szPackage;

		::SendMessage(hWnd, WM_COPYDATA, (WPARAM)GetSafeHwnd(), (LPARAM)&cds);

		return;
	}
*/
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szLocation[MAX_PATH];
	_tsplitpath(szModule, szDrive, szLocation, NULL, NULL);

	CString szPath = "";
	szPath.Format("\"%s%s\"", szDrive, szLocation);

	CString szParam = "";
	szParam += " +siteid=";
	szParam += GetEnvPtr()->m_szSite;
	szParam += " +userid=";
	szParam += GetEnvPtr()->m_szLoginID;
	szParam += " +passwd=";
	szParam += GetEnvPtr()->m_szPassword;;
	szParam += " +package=";
	szParam += szPackage;

	HINSTANCE nRet = ShellExecute(NULL, NULL, UBCSTUDIOEEEXE_STR, szParam, szPath, SW_SHOWNORMAL);
}

void CPackageView::OnPackagePlay()
{
	CString szPackage = m_lscPackage.GetItemText(m_ptSelList.y, ePackage);

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szLocation[MAX_PATH];
	_tsplitpath(szModule, szDrive, szLocation, NULL, NULL);

	CString szPath = "";
	szPath.Format("\"%s%s\"", szDrive, szLocation);

	CString szParam = "";
	szParam += " +autoscale +aspectratio +socketproxy +log +mouse +appId +ignore_env BROWSER ";
	szParam += " +site ";
	szParam += GetEnvPtr()->m_szSite;
	szParam += " +host ";
	szParam += szPackage;

	HINSTANCE nRet = ShellExecute(NULL, NULL, "utv_brwclient2.exe", szParam, szPath, SW_SHOWNORMAL);
}

void CPackageView::OnNMRclickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	CMenu menu, *pPopup=NULL;
	VERIFY(menu.LoadMenu(IDR_MENU_POPUP));

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	CPoint pt;
	GetCursorPos(&pt);

	pPopup = menu.GetSubMenu(1);
	pPopup->DeleteMenu(ID_PACKAGE01_PLAY, MF_BYCOMMAND);

	pPopup->EnableMenuItem(ID_PACKAGE01_DELETE, (IsAuth(_T("PDEL"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));
	pPopup->EnableMenuItem(ID_PACKAGE01_EDIT, (IsAuth(_T("PMOD"))?MF_ENABLED : MF_DISABLED | MF_GRAYED));

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,pt.x, pt.y, this);
}

void CPackageView::OnBnClickedButtonAdd()
{
	CString szPackage = m_lscPackage.GetItemText(m_ptSelList.y, ePackage);
/*
	unsigned long lPid = scratchUtil::getInstance()->getPid(UBCSTUDIOEEEXE_STR);
	HWND hWnd = scratchUtil::getInstance()->getWHandle(lPid);

	if(hWnd){
		CWnd* pWnd = CWnd::FromHandle(hWnd);
		pWnd->ActivateTopParent();

		if(pWnd->IsIconic()){
			pWnd->ShowWindow(SW_RESTORE);
		}

		COPYDATASTRUCT cds;
		cds.dwData = (ULONG_PTR)eCDNewPackage;
		cds.cbData = 0;
		cds.lpData = 0;

		::SendMessage(hWnd, WM_COPYDATA, (WPARAM)GetSafeHwnd(), (LPARAM)&cds);

		return;
	}
*/
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szLocation[MAX_PATH];
	_tsplitpath(szModule, szDrive, szLocation, NULL, NULL);

	CString szPath = "";
	szPath.Format("\"%s%s\"", szDrive, szLocation);

	CString szParam = "";
	szParam += " +siteid=";
	szParam += GetEnvPtr()->m_szSite;
	szParam += " +userid=";
	szParam += GetEnvPtr()->m_szLoginID;
	szParam += " +passwd=";
	szParam += GetEnvPtr()->m_szPassword;
	szParam += " +newpackage=";
	szParam += szPackage;

	HINSTANCE nRet = ShellExecute(NULL, NULL, UBCSTUDIOEEEXE_STR, szParam, szPath, SW_SHOWNORMAL);
}

void CPackageView::OnBnClickedButtonDel()
{
	int targetCount = 0;
	for(int nRow = m_lscPackage.GetItemCount()-1; 0 <= nRow; nRow--) {
		if(!m_lscPackage.GetCheck(nRow)) continue;
		POSITION pos = (POSITION)m_lscPackage.GetItemData(nRow);
		if(!pos) continue;
		targetCount++;
	}
	if(targetCount > 1) {
		UbcMessageBox(IDS_HOSTVIEW_MSG024);
		return;
	}

	if(targetCount == 0)
	{
		UbcMessageBox(LoadStringById(IDS_PLANSCHEDULING_MSG002), MB_ICONINFORMATION);
		return;
	}



	// 선택한 콘텐츠패키지를 삭제하시겠습니까?
	if(UbcMessageBox(LoadStringById(IDS_PACKAGEVIEW_MSG004), MB_YESNO) != IDYES)
	{
		return;
	}

	for(int nRow = m_lscPackage.GetItemCount()-1; nRow >= 0; nRow--)
	{
		if(!m_lscPackage.GetCheck(nRow)) continue;

		CString strMsg;

		// 0000560: 매니저에서 콘텐츠 패키지를 삭제시, 해당 콘텐츠 패키지가 사용중이라면, 사용중이라는 경고문을 넣는다. 
		CString szPackage = m_lscPackage.GetItemText(nRow, ePackage);
		ciStringSet hostSet;
		int nCnt = ubcUtil::getInstance()->getHostListUsingThisProgram(szPackage, hostSet);
		if(nCnt > 0)
		{
			strMsg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG001), szPackage);

			ciStringSet::iterator itr;
			for(itr=hostSet.begin();itr!=hostSet.end();itr++)
			{
				ciString hostId = (*itr);
				strMsg += _T("\n");
				strMsg += hostId.c_str();
			}

			if(UbcMessageBox(strMsg, MB_YESNO) != IDYES)
			{
				continue;
			}
		}

		// 방송계획으로 사용중인지 체크하여 지울지 판단한다.
		CString strBpList;
		if(!GetBroadcastPlan(szPackage, strBpList)) continue;
		if(!strBpList.IsEmpty())
		{
			strMsg.Format(LoadStringById(IDS_PACKAGEVIEW_MSG009), szPackage);
			UbcMessageBox(strMsg + strBpList);
			continue;
		}

		Remove(nRow);
	}
}

void CPackageView::OnBnClickedButtonRef()
{
	SetPackageData();
	UbcMessageBox(LoadStringById(IDS_REFRASHLIST_MSG), MB_ICONINFORMATION);
	SaveFilterData();
}

void CPackageView::Remove(int nRow)
{
	CString szSite = m_lscPackage.GetItemText(nRow, eSite);
	CString szPackage = m_lscPackage.GetItemText(nRow, ePackage);

	if(szSite.GetLength()==0 || szPackage.GetLength()==0) return;

	if(CCopModule::GetObject()->RemovePackage(szSite, szPackage)) {
		m_lscPackage.DeleteItem(nRow);
	}
}

void CPackageView::InitFilterListCtrl()
{
	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CString strColName;
		switch(i)
		{
		case eFilterCategory  : strColName = LoadStringById(IDS_PACKAGEVIEW_LST006); break;
		case eFilterPurpose   : strColName = LoadStringById(IDS_PACKAGEVIEW_LST007); break;
		case eFilterHostType  : strColName = LoadStringById(IDS_PACKAGEVIEW_LST008); break;
		case eFilterVertical  : strColName = LoadStringById(IDS_PACKAGEVIEW_LST009); break;
		case eFilterResolution: strColName = LoadStringById(IDS_PACKAGEVIEW_LST010); break;
		case eFilterPublic    : strColName = LoadStringById(IDS_PACKAGEVIEW_LST012); break;
		case eFilterVerify    : strColName = LoadStringById(IDS_PACKAGEVIEW_LST011); break;
		}

		CRect rect;
		m_lcFilter[i].GetClientRect(rect);
		m_lcFilter[i].SetExtendedStyle(m_lcFilter[i].GetExtendedStyle() | LVS_EX_FULLROWSELECT);
		m_lcFilter[i].SetSelTextColor(RGB(0,0,0));
		m_lcFilter[i].SetSelBackColor(RGB(220,220,220));
		m_lcFilter[i].InsertColumn(0, strColName, LVCFMT_LEFT, rect.Width()-::GetSystemMetrics(SM_CXVSCROLL));
		m_lcFilter[i].InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
		m_lcFilter[i].HideScrollBars(SB_HORZ);
	}
}

void CPackageView::OnBnClickedBnRegister()
{
	CSelectUsers dlg;
	dlg.SetSingleSelection(true);

	if(dlg.DoModal() != IDOK)
	{
		SetDlgItemText(IDC_EB_REGISTER, _T(""));
		return;
	}

	UserInfoList& list = dlg.GetSelectedList();

	POSITION pos = list.GetHeadPosition();
	if(pos)
	{
		SUserInfo info = list.GetNext(pos);
		SetDlgItemText(IDC_EB_REGISTER, info.userId);
	}
}

void CPackageView::SaveFilterData()
{
	CString strPackage     ;
	CString strTag         ;
	CString strRegister    ;
	CString strContentsName;
	CString strContentsId  ;
	CString strStartDate   ;
	CString strEndDate     ;

	GetDlgItemText(IDC_EB_PACKAGE , strPackage     );
	GetDlgItemText(IDC_EB_TAG     , strTag         );
	GetDlgItemText(IDC_EB_REGISTER, strRegister    );
	if(m_cbxIncludedContents.GetCount() > 2)
	{
		m_cbxIncludedContents.GetLBText(1, strContentsName);
		strContentsId = m_strFilterContentsId;
	}
	SYSTEMTIME stmTemp;
	m_dtStartDate.GetTime(&stmTemp);
	CTime tmTemp = CTime(stmTemp);
	strStartDate = tmTemp.Format("%Y/%m/%d");
	m_dtEndDate.GetTime(&stmTemp);
	tmTemp = CTime(stmTemp);
	strEndDate = tmTemp.Format("%Y/%m/%d");

	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CString strKey;
		switch(i)
		{
		case eFilterCategory  : strKey = _T("Category"); break; // 종류별
		case eFilterPurpose   : strKey = _T("Purpose"); break; // 용도별
		case eFilterHostType  : strKey = _T("HostType"); break; // 단말타입별
		case eFilterVertical  : strKey = _T("Vertical"); break; // 가로/세로별
		case eFilterResolution: strKey = _T("Resolution"); break; // 해상도별
		case eFilterPublic    : strKey = _T("IsPublic"); break; // 공개여부
		case eFilterVerify    : strKey = _T("IsVerify"); break; // 승인여부
		}

		CString strList;
		CStringArray ar;
		if(GetSelectItemsList(m_lcFilter[i], ar) > 0)
		{
			for(int j=0; j<ar.GetCount() ;j++)
			{
				strList += ar[j] + _T(",");
			}
		}
		WritePrivateProfileString("PACKAGE-FILTER", strKey, strList, szPath);
	}

	WritePrivateProfileString("PACKAGE-FILTER", "Package"     , strPackage     , szPath);
	WritePrivateProfileString("PACKAGE-FILTER", "Tag"         , strTag         , szPath);
	WritePrivateProfileString("PACKAGE-FILTER", "Register"    , strRegister    , szPath);
	WritePrivateProfileString("PACKAGE-FILTER", "ContentsName", strContentsName, szPath);
	WritePrivateProfileString("PACKAGE-FILTER", "ContentsId"  , strContentsId  , szPath);
//	WritePrivateProfileString("PACKAGE-FILTER", "StartDate"   , strStartDate   , szPath);
//	WritePrivateProfileString("PACKAGE-FILTER", "EndDate"     , strEndDate     , szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "IsPublic"    , ToString(nFilter[eFilterPublic    ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "IsVerify"    , ToString(nFilter[eFilterVerify    ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "Category"    , ToString(nFilter[eFilterCategory  ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "Purpose"     , ToString(nFilter[eFilterPurpose   ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "HostType"    , ToString(nFilter[eFilterHostType  ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "Vertical"    , ToString(nFilter[eFilterVertical  ]), szPath);
	//WritePrivateProfileString("PACKAGE-FILTER", "Resolution"  , ToString(nFilter[eFilterResolution]), szPath);
}

void CPackageView::LoadFilterData()
{
	CString strPackage     ;
	CString strTag         ;
	CString strRegister    ;
	CString strContentsName;
	CString strContentsId  ;
	CString strStartDate   ;
	CString strEndDate     ;

	char buf[1024];
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);

	GetPrivateProfileString("PACKAGE-FILTER", "Package"     , "", buf, 1024, szPath); strPackage      = buf;
	GetPrivateProfileString("PACKAGE-FILTER", "Tag"         , "", buf, 1024, szPath); strTag          = buf;
	GetPrivateProfileString("PACKAGE-FILTER", "Register"    , "", buf, 1024, szPath); strRegister     = buf;
	GetPrivateProfileString("PACKAGE-FILTER", "ContentsName", "", buf, 1024, szPath); strContentsName = buf;
	GetPrivateProfileString("PACKAGE-FILTER", "ContentsId"  , "", buf, 1024, szPath); strContentsId   = buf;
//	GetPrivateProfileString("PACKAGE-FILTER", "StartDate"   , "", buf, 1024, szPath); strStartDate    = buf;
//	GetPrivateProfileString("PACKAGE-FILTER", "EndDate"     , "", buf, 1024, szPath); strEndDate      = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "Category"    , "", buf, 1024, szPath); strFilter[eFilterCategory  ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "Purpose"     , "", buf, 1024, szPath); strFilter[eFilterPurpose   ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "HostType"    , "", buf, 1024, szPath); strFilter[eFilterHostType  ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "Vertical"    , "", buf, 1024, szPath); strFilter[eFilterVertical  ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "Resolution"  , "", buf, 1024, szPath); strFilter[eFilterResolution] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "IsPublic"    , "", buf, 1024, szPath); strFilter[eFilterPublic    ] = buf;
	//GetPrivateProfileString("PACKAGE-FILTER", "IsVerify"    , "", buf, 1024, szPath); strFilter[eFilterVerify    ] = buf;

	SetDlgItemText(IDC_EB_PACKAGE , strPackage     );
	SetDlgItemText(IDC_EB_TAG     , strTag         );
	SetDlgItemText(IDC_EB_REGISTER, strRegister    );

	if(m_cbxIncludedContents.GetCount() > 2)
	{
		m_cbxIncludedContents.DeleteString(1);
		m_strFilterContentsId = _T("");
	}

	if(!strContentsId.IsEmpty())
	{
		m_strFilterContentsId = strContentsId;
		m_cbxIncludedContents.InsertString(1, strContentsName);
		m_cbxIncludedContents.SetCurSel(1);
	}

	COleDateTime tmTemp;
	CTime tmCur = CTime::GetCurrentTime();
	if(strStartDate.IsEmpty())
	{
		int nAddMonth = -2;
		int nTotalMonth = tmCur.GetYear() * 12 + tmCur.GetMonth() + nAddMonth;

		CTime tmStartDate  = CTime( nTotalMonth/12 - ((nTotalMonth%12) == 0 ? 1 : 0)
								, ((nTotalMonth%12) == 0 ? 12 : (nTotalMonth%12))
								, tmCur.GetDay()
								, tmCur.GetHour()
								, tmCur.GetMinute()
								, tmCur.GetSecond()
								);
		strStartDate = tmStartDate.Format("%Y/%m/01");
	}

	tmTemp.ParseDateTime(strStartDate);
	m_dtStartDate.SetTime(tmTemp);

	if(strEndDate.IsEmpty())
	{
		strEndDate = tmCur.Format("%Y/%m/%d");
	}

	tmTemp.ParseDateTime(strEndDate);
	m_dtEndDate.SetTime(tmTemp);

	for(int i=0; i<eFilterMaxCnt ;i++)
	{
		CString strKey;
		switch(i)
		{
		case eFilterCategory  : strKey = _T("Category"); break; // 종류별
		case eFilterPurpose   : strKey = _T("Purpose"); break; // 용도별
		case eFilterHostType  : strKey = _T("HostType"); break; // 단말타입별
		case eFilterVertical  : strKey = _T("Vertical"); break; // 가로/세로별
		case eFilterResolution: strKey = _T("Resolution"); break; // 해상도별
		case eFilterPublic    : strKey = _T("IsPublic"); break; // 공개여부
		case eFilterVerify    : strKey = _T("IsVerify"); break; // 승인여부
		}

		GetPrivateProfileString("PACKAGE-FILTER", strKey, "", buf, 1024, szPath);
		CString strList = buf;

		int iStart = 0;
		CString strValue;
		while(!(strValue = strList.Tokenize(_T(","), iStart)).IsEmpty())
		{
			int nFilter = _ttoi(strValue);
			int nIndex = CUbcCode::GetInstance()->FindListCtrl(m_lcFilter[i], nFilter);
			if(nIndex >= 0)
			{
				m_lcFilter[i].SetCheck(nIndex, TRUE);
			}
		}
	}
}

void CPackageView::OnCbnSelchangeCbIncludedContents()
{
	int idx = m_cbxIncludedContents.GetCurSel();
	int count = m_cbxIncludedContents.GetCount();

	if(idx == 0)
	{
		if(count > 2)
		{
			m_cbxIncludedContents.DeleteString(1);
			m_strFilterContentsId = _T("");
		}
	}
	else if(idx == count-1 )
	{
		CContentsSelectDlg dlg(GetEnvPtr()->m_strCustomer);
		dlg.SetSingleSelection(true); // 멀티선택일 경우 주석처리
		if(dlg.DoModal() == IDOK)
		{
			if(count > 2)
			{
				m_strFilterContentsId = _T("");
				m_cbxIncludedContents.DeleteString(1);
				m_cbxIncludedContents.SetCurSel(0);
			}

			CONTENTS_INFO_EX_LIST& list = dlg.GetSelectedContentsIdList();
			POSITION pos = list.GetHeadPosition();
			if(pos)
			{
				CONTENTS_INFO_EX& info = list.GetNext(pos);
				m_strFilterContentsId = info.strContentsId;
				m_cbxIncludedContents.InsertString(1, info.strContentsName);
				m_cbxIncludedContents.SetCurSel(1);
			}
			else
			{
				m_cbxIncludedContents.SetCurSel(0);
			}
		}
		else
		{
			m_cbxIncludedContents.SetCurSel(count-2);
		}
	}
}

int CPackageView::GetSelectItemsList(CUTBListCtrlEx& lc, CStringArray& ar)
{
	int count = lc.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if(lc.GetCheck(i))
		{
			ar.Add(ToString(lc.GetItemData(i)));
		}
	}

	return ar.GetCount();
}

CString CPackageView::GetSelectItemsStringFromList(CStringArray& ar, CString strField)
{
	CString strFilter;

	int count = ar.GetCount();
	for(int i=0; i<count; i++)
	{
		strFilter.AppendFormat("'%s',", ar.GetAt(i));
	}

	strFilter.Trim(_T(','));

	if(strFilter.GetLength() > 0)
	{
		CString tmp;
		tmp.Format("%s in (", strField);

		strFilter = tmp + strFilter + ")";
	}

	return strFilter;
}

CString CPackageView::GetSelectItemsStringFromList(CUTBListCtrlEx& lc, CString strField)
{
	CStringArray ar;
	GetSelectItemsList(lc, ar);
	return GetSelectItemsStringFromList(ar, strField);
}

BOOL CPackageView::GetBroadcastPlan(CString strPackage, CString& strBpList)
{
	CString strWhere;
	CString strTmp;

	CTime tmCur = CTime::GetCurrentTime();
	CString strBaseDate = tmCur.Format(_T("%Y/%m/%d")) + _T(" 00:00:00");
	//strTmp.Format(_T("startDate >= '%s' and endDate > '%s'"), strStartDate, strStartDate);
	strTmp.Format(_T("endDate > CAST('%s' as datetime)"), strBaseDate);
	strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

	strTmp.Format(_T("bpId in (select bpId from UBC_TP where lower(programId) like lower('%%%s%%'))"), strPackage);
	strWhere += (!strWhere.IsEmpty() ? _T(" and ") : _T("")) + strTmp;

	BroadcastingPlanInfoList lsBpInfoList;
	if(!CCopModule::GetObject()->GetBroadcastingPlanList( (CCopModule::eSiteAdmin == GetEnvPtr()->m_Authority ? _T("*") : GetEnvPtr()->m_szSite)
														, lsBpInfoList
														, strWhere ))
	{
		return FALSE;
	}

	int nRow = 0;
	POSITION pos = lsBpInfoList.GetHeadPosition();
	while(pos)
	{
		POSITION posOld = pos;
		SBroadcastingPlanInfo& info = lsBpInfoList.GetNext(pos);

		strBpList += _T("\n") + info.strBpName;
	}

	return TRUE;
}

void CPackageView::OnBnClickedButtonUnusedDel()
{
	CCleaningUnusedPackage dlg(this);
	dlg.DoModal();
}

void CPackageView::OnNMDblclkListPackage(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	if(pNMLV->iItem < 0) return;
	*pResult = 0;

	m_ptSelList.x = -1;
	m_ptSelList.y = -1;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	m_ptSelList.x = nCol;
	m_ptSelList.y = nRow;

	OnPackageEdit();
}

void CPackageView::OnBnClickedBnToExcel4()
{
	CHostExcelSave dlg;

	CString strExcelFile = dlg.Save(  LoadStringById(IDS_PACKAGEVIEW_STR001)
									, m_lscPackage
									);

	CString strMsg;
	if(!strExcelFile.IsEmpty())
	{
		strMsg.Format(LoadStringById(IDS_HOSTLOGVIEW_MSG001), strExcelFile);
		UbcMessageBox(strMsg);
	}
}
