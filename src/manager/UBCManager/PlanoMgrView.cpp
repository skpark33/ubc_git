// PlanoMgrView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "PlanoMgrView.h"
#include "PlanoMgrDetailInfo.h"
#include "Enviroment.h"
#include "common\ubcdefine.h"
#include "ubccopcommon\ubccopcommon.h"
#include "common\libcommon\utvUtil.h"
#include "common\UbcCode.h"
#include "UBCCopCommon\UbcMenuAuth.h"

// CPlanoMgrView 대화 상자입니다.


IMPLEMENT_DYNCREATE(CPlanoMgrView, CFormView)

CPlanoMgrView::CPlanoMgrView()
	: CFormView(CPlanoMgrView::IDD)
	,	m_Reposition(this)
{

}

CPlanoMgrView::~CPlanoMgrView()
{
}

void CPlanoMgrView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_PLANO_REF, m_btnRefPlanoMgr);
	DDX_Control(pDX, IDC_BUTTON_PLANO_CREATE, m_btnCreatePlanoMgr);
	DDX_Control(pDX, IDC_BUTTON_PLANO_DEL, m_btnDeletePlanoMgr);
	DDX_Control(pDX, IDC_LIST_PLANO, m_lscPlanoMgr);
	DDX_Control(pDX, IDC_COMBO_FILTER_PLANOTYPE, m_cbPlanoType);
	DDX_Control(pDX, IDC_BUTTON_ICON_SYNC, m_btnIconSync);
}


BEGIN_MESSAGE_MAP(CPlanoMgrView, CFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_PLANO_REF, &CPlanoMgrView::OnBnClickedButtonPlanoMgrRef)
	ON_BN_CLICKED(IDC_BUTTON_PLANO_CREATE, &CPlanoMgrView::OnBnClickedButtonPlanoMgrCreate)
	ON_BN_CLICKED(IDC_BUTTON_PLANO_DEL, &CPlanoMgrView::OnBnClickedButtonPlanoMgrDel)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PLANO, &CPlanoMgrView::OnNMDblclkListPlanoMgr)
	ON_BN_CLICKED(IDC_BUTTON_ICON_SYNC, &CPlanoMgrView::OnBnClickedButtonIconSync)
END_MESSAGE_MAP()

#define STR_OPERA_ON			LoadStringById(IDS_IMAGECONTENTSDIALOG_BUT002)
#define STR_OPERA_OFF			LoadStringById(IDS_SMSCONTENTSDIALOG_BUT009)

#define STR_HOST_TYPE			LoadStringById(IDS_ICONVIEW_STR001)
#define STR_LIGHT_TYPE			LoadStringById(IDS_ICONVIEW_STR002)
#define STR_ETC_TYPE			"ETC."

#define STR_PLANO_ID			LoadStringById(IDS_PLANOMGRVIEW_STR001)
#define STR_PLANO_SITEID		LoadStringById(IDS_NOTIMNGVIEW_LST001)
#define STR_PLANO_TYPE			LoadStringById(IDS_PLANOMGRVIEW_STR002)
#define STR_PLANO_BGFILE		LoadStringById(IDS_PLANOMGRVIEW_STR003)
#define STR_PLANO_DESC			LoadStringById(IDS_PLANOMGRVIEW_STR004)
#define STR_PLANO_EXPAND		LoadStringById(IDS_PLANOMGRVIEW_STR005)

// CPlanoMgrView 메시지 처리기입니다.

int CPlanoMgrView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	GetClientRect(&m_rcClient);

	return 0;
}
void CPlanoMgrView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CDocument* pDoc = GetDocument();
//	pDoc->SetTitle(LoadStringById(IDS_HOSTVIEW_STR001));

	m_cbPlanoType.AddString("All");
	m_cbPlanoType.AddString(STR_HOST_TYPE);
	m_cbPlanoType.AddString(STR_LIGHT_TYPE);
	m_cbPlanoType.AddString(STR_ETC_TYPE);
	m_cbPlanoType.SetCurSel(0);


	m_btnRefPlanoMgr.LoadBitmap(IDB_BUTTON_REFRESH, RGB(255,255,255));
	m_btnCreatePlanoMgr.LoadBitmap(IDB_BUTTON_ADD, RGB(255,255,255));
	m_btnDeletePlanoMgr.LoadBitmap(IDB_BUTTON_REMOVE, RGB(255,255,255));
	m_btnIconSync.LoadBitmap(IDB_BUTTON_ICON_SYNC, RGB(255,255,255));

	InitPosition(m_rcClient);

	UpdateData(FALSE);
	InitAuthCtrl();
	InitPlanoMgrList();

	SetScrollSizes(MM_TEXT, CSize(1,1));

}

void CPlanoMgrView::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_lscPlanoMgr, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
}


void CPlanoMgrView::InitAuthCtrl()
{
	m_btnDeletePlanoMgr.EnableWindow(IsAuth(_T("HDEL")));
	m_btnRefPlanoMgr.EnableWindow(IsAuth(_T("HQRY")));
	m_btnCreatePlanoMgr.EnableWindow(IsAuth(_T("HREG")));
}

void CPlanoMgrView::InitPlanoMgrList()
{
	m_szPlanoMgrColum[eCheck] = _T("");	
	m_szPlanoMgrColum[ePlanoId] =			STR_PLANO_ID;
	m_szPlanoMgrColum[eSiteId] =			STR_PLANO_SITEID;
	m_szPlanoMgrColum[eBackgroundFile] =	STR_PLANO_BGFILE;
	m_szPlanoMgrColum[eDescription] =		STR_PLANO_DESC;
	m_szPlanoMgrColum[ePlanoType] =			STR_PLANO_TYPE;
	m_szPlanoMgrColum[eExpand] =			STR_PLANO_EXPAND;

	m_lscPlanoMgr.SetExtendedStyle(m_lscPlanoMgr.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_INFOTIP |m_lscPlanoMgr.GetExtendedStyle());

	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilPlanoMgrList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilPlanoMgrList.Add(&bmCheck, RGB(255, 255, 255));
	m_lscPlanoMgr.SetImageList(&m_ilPlanoMgrList, LVSIL_SMALL);

	m_lscPlanoMgr.InsertColumn(eCheck,			m_szPlanoMgrColum[eCheck], LVCFMT_LEFT, 22);
	m_lscPlanoMgr.InsertColumn(ePlanoId,		m_szPlanoMgrColum[ePlanoId], LVCFMT_LEFT, 240);
	m_lscPlanoMgr.InsertColumn(eSiteId,		m_szPlanoMgrColum[eSiteId], LVCFMT_LEFT, 180);
	m_lscPlanoMgr.InsertColumn(ePlanoType,		m_szPlanoMgrColum[ePlanoType], LVCFMT_LEFT, 120);
	m_lscPlanoMgr.InsertColumn(eBackgroundFile,	m_szPlanoMgrColum[eBackgroundFile], LVCFMT_LEFT, 240);
	m_lscPlanoMgr.InsertColumn(eExpand,			m_szPlanoMgrColum[eExpand], LVCFMT_LEFT, 60);
	m_lscPlanoMgr.InsertColumn(eDescription,	m_szPlanoMgrColum[eDescription], LVCFMT_LEFT, 120);

	m_lscPlanoMgr.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);

	// 0000725: 목록화면에서, 필드순서 및 소팅컬럼 저장하기
	// 저장된 컬럼상태로 초기화
	CString szPath;
	szPath.Format("%s%sdata\\%s", GetEnvPtr()->m_szDrive, GetEnvPtr()->m_szPath, ENVIROMENT_INI);
	m_lscPlanoMgr.LoadColumnState("PLANO-LIST", szPath);

}

BOOL CPlanoMgrView::RefreshPlanoMgr(bool bCompMsg)
{
	TraceLog(("RefreshPlanoMgr begin"));
	DWORD dwStartTime =  ::GetTickCount() ;

	CWaitMessageBox wait;
	
	UpdateData();

	
	int planoType=m_cbPlanoType.GetCurSel() -1;


	CString strWhere;

	// planoType
	if(planoType >= 0)
	{
		strWhere.AppendFormat(_T(" %s planoType = '%d'")
							, (strWhere.IsEmpty() ? _T("") : _T("and"))
							, planoType
							);
	}

	TraceLog(("m_lsPlanoMgr.RemoveAll() begin"));
	m_lsPlanoMgr.RemoveAll();
	TraceLog(("m_lsPlanoMgr.RemoveAll() end"));

	BOOL bRet = CCopModule::GetObject()->GetPlanoList("*", (strWhere.IsEmpty() ? NULL : strWhere),m_lsPlanoMgr);

	RefreshPlanoMgrList();

	TraceLog(("RefreshPlanoMgr end"));

	if(!bRet)
	{
		UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR006), MB_ICONINFORMATION);
		return FALSE;
	}

	if(bCompMsg)
	{
		DWORD dwElapse = ::GetTickCount() - dwStartTime;
		DWORD dwElapseSec = dwElapse / 1000;
		CString strElapse;
		strElapse.Format("\nElapse time : %ld sec", dwElapseSec);

		CString strMsg;
		strMsg.Format(LoadStringById(IDS_PLANOMGRVIEW_STR007), m_lscPlanoMgr.GetItemCount());
		strMsg.Append(strElapse);

		UbcMessageBox(strMsg, MB_ICONINFORMATION);
	}

	return bRet;
}

void CPlanoMgrView::RefreshPlanoMgrList(POSITION posStart)
{
	TraceLog(("RefreshPlanoMgrList begin"));

	m_lscPlanoMgr.SetRedraw(FALSE);

	int nRow = 0;
	CString szBuf;
	POSITION pos = m_lsPlanoMgr.GetHeadPosition();
	POSITION posTail = m_lsPlanoMgr.GetTailPosition();

	if(posStart)
	{
		pos = posStart;
		m_lsPlanoMgr.GetNext(pos);
	}
	else
	{
		m_lscPlanoMgr.DeleteAllItems();
	}

	while(pos)
	{
		POSITION posOld = pos;
		SPlanoInfo* info = m_lsPlanoMgr.GetNext(pos);

		m_lscPlanoMgr.InsertItem(nRow, _T(""), -1);
	
		UpdateListRow(nRow, info);  //OK

		m_lscPlanoMgr.SetItemData(nRow++, (DWORD_PTR)posOld);
	}

	int nCol = m_lscPlanoMgr.GetCurSortCol();
	if(-1 != nCol)
		m_lscPlanoMgr.SortList(nCol, m_lscPlanoMgr.IsAscend(), PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscPlanoMgr);
	else
		m_lscPlanoMgr.SortList(nCol, PFNLVCOMPARE(CUTBListCtrlEx::Compare), (DWORD_PTR)&m_lscPlanoMgr);

	TraceLog(("RefreshPlanoMgrList end"));

	m_lscPlanoMgr.SetRedraw(TRUE);

	//RefreshPaneText();
}


void CPlanoMgrView::UpdateListRow(int nRow, SPlanoInfo* pInfo)
{
	if(!pInfo || m_lscPlanoMgr.GetItemCount() < nRow)
		return;

	m_lscPlanoMgr.SetItemText(nRow, ePlanoId, pInfo->planoId);
	m_lscPlanoMgr.SetItemText(nRow, eSiteId, pInfo->siteId);
	m_lscPlanoMgr.SetItemText(nRow, eBackgroundFile, pInfo->backgroundFile);
	m_lscPlanoMgr.SetItemText(nRow, eDescription, pInfo->description);

	CString type = STR_HOST_TYPE;
	switch(pInfo->planoType){
		case 0: type = STR_HOST_TYPE; break;
		case 1: type = STR_LIGHT_TYPE; break;
		case 2: type = STR_ETC_TYPE; break;
	}
	m_lscPlanoMgr.SetItemText(nRow, ePlanoType, type);

	if(pInfo->expand){
		m_lscPlanoMgr.SetItemText(nRow, eExpand, STR_OPERA_ON);
	}else{
		m_lscPlanoMgr.SetItemText(nRow, eExpand, STR_OPERA_OFF);
	}
}


void CPlanoMgrView::ModifyPlanoMgr(CArray<int>& arRow, bool bMsg)
{
	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscPlanoMgr.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscPlanoMgr.GetItemData(nRow);
		if(!pos) continue;

		SPlanoInfo* InfoPtr = m_lsPlanoMgr.GetAt(pos);
		CPlanoMgrDetailInfo dlg;

		SPlanoInfo Info(InfoPtr);

		TraceLog(("Set1 : %s/%s", Info.siteId, Info.planoId));

		dlg.SetInfo(Info);
		TraceLog(("Set2 : %s/%s", Info.siteId, Info.planoId));
		if(dlg.DoModal() != IDOK)
		{
			InfoPtr->set(&Info);
			//m_lsPlanoMgr.GetAt(pos) = InfoPtr;
			UpdateListRow(nRow, &Info); //OK
			continue;
		}
		dlg.GetInfo(Info);
		TraceLog(("Set3 : %s/%s", Info.siteId, Info.planoId));

		if(!dlg.Upload()){
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_PLANOMGRVIEW_STR008), Info.backgroundFile);
			UbcMessageBox(szMsg, MB_ICONERROR);
			continue;
		}

		TraceLog(("Set4 : %s/%s", Info.siteId, Info.planoId));
		if(!CCopModule::GetObject()->SetPlano(&Info))
		{
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_PLANOMGRVIEW_STR009), Info.planoId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;
		//m_lsPlanoMgr.GetAt(pos) = Info;
		InfoPtr->set(&Info);
	}

	if(bModified)
	{
		if(bMsg) UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR010), MB_ICONINFORMATION);
		RefreshPlanoMgr(false);
	}
}


void CPlanoMgrView::OnDestroy()
{
	CFormView::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CPlanoMgrView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	m_Reposition.Move();
}

void CPlanoMgrView::OnBnClickedButtonPlanoMgrRef()
{
	RefreshPlanoMgr();
}

void CPlanoMgrView::OnBnClickedButtonPlanoMgrCreate()
{
	CPlanoMgrDetailInfo dlg;

	dlg.SetCreateMode();
	if(dlg.DoModal() != IDOK)
	{
		return;
	}
	SPlanoInfo Info;
	dlg.GetInfo(Info);

	TraceLog(("OnBnClickedButtonPlanoMgrCreate (%s,%s)", Info.planoId, Info.backgroundFile));

	PlanoInfoList dummyList;
	CCopModule::GetObject()->GetPlanoList(Info.planoId,NULL,dummyList); 
	if(dummyList.GetSize() > 0){
		CString msg;
		msg.Format(LoadStringById(IDS_PLANOMGRVIEW_STR011), Info.planoId);
		UbcMessageBox(msg);
		return ;
	}

	if(!dlg.Upload()){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_PLANOMGRVIEW_STR008), Info.backgroundFile);
		UbcMessageBox(szMsg, MB_ICONERROR);
		return;
	}


	Info.siteId = GetEnvPtr()->GetSiteId();
	if(!CCopModule::GetObject()->CreatePlano(&Info)){
		CString szMsg;
		szMsg.Format(LoadStringById(IDS_PLANOMGRVIEW_STR012), Info.planoId);
		UbcMessageBox(szMsg, MB_ICONERROR);
		return;
	}
	UbcMessageBox(LoadStringById(IDS_PLANOMGRVIEW_STR013), MB_ICONINFORMATION);
	RefreshPlanoMgr(false);
}

void CPlanoMgrView::OnBnClickedButtonPlanoMgrDel()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscPlanoMgr.GetItemCount(); nRow++)
	{
		if(!m_lscPlanoMgr.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_FLASHCONTENTSDIALOG_MSG003), MB_ICONINFORMATION);
		return;
	}

	RemovePlanoMgr(arRow, true);
}

void CPlanoMgrView::RemovePlanoMgr(CArray<int>& arRow, bool bMsg)
{
	if(UbcMessageBox(LoadStringById(IDS_LIGHTVIEW_STR012), MB_YESNO|MB_ICONINFORMATION) != IDYES)
	{
		return;
	}

	bool bModified = false;
	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscPlanoMgr.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscPlanoMgr.GetItemData(nRow);
		if(!pos) continue;

		SPlanoInfo* Info = m_lsPlanoMgr.GetAt(pos);

		TraceLog(("%s will be deleted", Info->planoId));

		if(!CCopModule::GetObject()->DeletePlano(Info))
		{
			CString szMsg;
			szMsg.Format(LoadStringById(IDS_ICONVIEW_STR020), Info->planoId);
			UbcMessageBox(szMsg, MB_ICONERROR);
			continue;
		}
		bModified = true;

		//m_lsPlanoMgr.RemoveAt(pos);
		//m_lscPlanoMgr.DeleteItem(nRow);
	}

	if(bModified)
	{
		if(bMsg) 	UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR021), MB_ICONINFORMATION);
		RefreshPlanoMgr(false);
	}

}



void CPlanoMgrView::OnNMDblclkListPlanoMgr(NMHDR *pNMHDR, LRESULT *pResult)
{
	if(!IsAuth(_T("HDTL")) && !IsAuth(_T("HMOD"))) return;

	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	int nRow = pNMLV->iItem;
	int	nCol = pNMLV->iSubItem;

	if(nRow < 0 || nCol < 0)
		return;

	CArray<int> arRow;
	arRow.Add(nRow);
	ModifyPlanoMgr(arRow, true);
}

void CPlanoMgrView::OnBnClickedButtonIconSync()
{
	CArray<int> arRow;
	for(int nRow = 0; nRow < m_lscPlanoMgr.GetItemCount(); nRow++)
	{
		if(!m_lscPlanoMgr.GetCheck(nRow)) continue;
		arRow.Add(nRow);
	}

	if(arRow.GetCount() == 0)
	{
		UbcMessageBox(LoadStringById(IDS_ICONVIEW_STR019), MB_ICONINFORMATION);
		return;
	}

	CString szServerLocation = "/Config/Plano/" ;
	CString szLocalLocation;
	szLocalLocation.Format("%s\\%sPlano\\", GetEnvPtr()->m_szDrive, UBC_EXECUTE_PATH);

	int counter=0;

	for(int nCnt = 0; nCnt < arRow.GetCount(); nCnt++)
	{
		int nRow = arRow.GetAt(nCnt);
		m_lscPlanoMgr.SetCheck(nRow, FALSE);

		POSITION pos = (POSITION)m_lscPlanoMgr.GetItemData(nRow);
		if(!pos) continue;

		SPlanoInfo* Info = m_lsPlanoMgr.GetAt(pos);

		CString iconFile = szLocalLocation + Info->backgroundFile;

		if(::DeleteFile(iconFile)){
			TraceLog(("%s file deleted", iconFile));
			if(GetEnvPtr()->GetFile(Info->backgroundFile, szServerLocation,szLocalLocation,"")){
				TraceLog(("Download succeed(%s,%s,%s)", Info->backgroundFile, szLocalLocation,szServerLocation));
				counter++;
			}else{
				TraceLog(("ERROR : Download(%s,%s,%s) failed", Info->backgroundFile, szLocalLocation,szServerLocation));
			}
		}else{
			TraceLog(("ERROR : %s file delete failed", iconFile));
		}

	}

	CString msg;
	msg.Format(LoadStringById(IDS_ICONVIEW_STR025),counter);
	UbcMessageBox(msg);
	return;
}
