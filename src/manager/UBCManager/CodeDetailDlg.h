#pragma once
#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"

#include "UBCCopCommon\CopModule.h"
// CCodeDetailDlg 대화 상자입니다.

class CCodeDetailDlg : public CDialog
{
	DECLARE_DYNAMIC(CCodeDetailDlg)

public:
	CCodeDetailDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCodeDetailDlg();

	void SetInfo(SCodeItem& info) { m_codeInfo = info; m_isSet = true;}
	void GetInfo(SCodeItem& info) {info = m_codeInfo;  }

	void InitData();
// 대화 상자 데이터입니다.
	enum { IDD = IDD_CODE_DETAIL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CEdit m_edit_category;
	CEdit m_edit_enumString;
	CEdit m_edit_enumNumber;
	CComboBox m_cb_visible;

	SCodeItem m_codeInfo;
	bool	m_isSet;

	CEdit m_edit_dorder;
	CSpinButtonCtrl m_sp_dorder;
};
