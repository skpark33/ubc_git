#pragma once

#include "UBCDailyPlanTableOCX.h"

// UbcDailyPlanTableCtrl.h : CUbcDailyPlanTableCtrl ActiveX 컨트롤 클래스의 선언입니다.
// CUbcDailyPlanTableCtrl : 구현을 보려면 UbcDailyPlanTableCtrl.cpp을(를) 참조하십시오.

class CUbcDailyPlanTableCtrl : public COleControl
{
	DECLARE_DYNCREATE(CUbcDailyPlanTableCtrl)

// 생성자입니다.
public:
	CUbcDailyPlanTableCtrl();

// 디스패치와 이벤트 ID입니다.
public:
	enum {
		eventidItemDblClick = 4L,
		eventidItemClick = 3L,
		dispidSetItemColor = 23L,
		dispidSetZOrder = 22L,
		dispidMagneticMove = 21,
		eventidInfoChange = 2L,
		eventidSelChange = 1L,
		dispidGetItem = 20L,
		dispidSetItem = 19L,
		dispidInsertItem = 18L,
		dispidAddItem = 17L,
		dispidSetSelectedIndex = 16L,
		dispidGetSelectedIndex = 15L,
		dispidGetItemCount = 14L,
		dispidDeleteItem = 13L,
		dispidDeleteAllItem = 12L,
		dispidTimeLineColor = 11,
		dispidPlanLineColor = 10,
		dispidEditable = 9,
		dispidSelectBorderColor = 8,
		dispidPlanBorderColor = 7,
		dispidPlanForeColor = 6,
		dispidPlanBackColor = 5,
		dispidViewRange = 4,
		dispidDefaultViewTime = 3,
		dispidTimeBackColor = 2,
		dispidTimeForeColor = 1
	};

// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

// 구현입니다.
protected:
	enum MOUSE_TYPE
	{ 
		MOUSE_IDLE = 0,
		MOUSE_OVER_LEFT_TOP,
		MOUSE_OVER_TOP,
		MOUSE_OVER_RIGHT_TOP,
		MOUSE_OVER_LEFT,
		MOUSE_OVER_CENTER,
		MOUSE_OVER_RIGHT,
		MOUSE_OVER_LEFT_BOTTOM,
		MOUSE_OVER_BOTTOM,
		MOUSE_OVER_RIGHT_BOTTOM,
		MOUSE_SIZING_LEFT_TOP,
		MOUSE_SIZING_TOP,
		MOUSE_SIZING_RIGHT_TOP,
		MOUSE_SIZING_LEFT,
		MOUSE_SIZING_CENTER,
		MOUSE_SIZING_RIGHT,
		MOUSE_SIZING_LEFT_BOTTOM,
		MOUSE_SIZING_BOTTOM,
		MOUSE_SIZING_RIGHT_BOTTOM,
	};

	int				m_nSeparatorX;
	int				m_nOneHourHeight;
	CSize			m_sizeFont;
	void			CalcFontSize();

	int				m_nRulerPos[24][6];
	bool			IsVisibleTime(int nHour);
	bool			IsVisibleTime(int nStartHour, int nEndHour);

	CPtrArray		m_aPlanItemList;
	long			m_lSelectedIndex;
	long			m_lClickedIndex;

	MOUSE_TYPE		m_MouseType;
	CPoint			m_pointStart;
	ST_PLAN_ITEM	m_stSizing;
	bool			SetCursor(MOUSE_TYPE mouseType);
	void			HitTest(CPoint& point);
	bool			GetPlanRect(CTime tmStart, CTime tmEnd, const CRect& rcBounds, int& nBoxCnt, CRect rcBox[]);
	CTime			GetNearTime(CTime tmValue, int nIndex=-1);
	CTime			NormalizeTime(CTime tmValue);
	CTime			NormalizeTime(int nHour, int nMinute);

	~CUbcDailyPlanTableCtrl();

	void DrawTimeTable     (CDC* pDC, const CRect& rcBounds);
	void DrawPlanBackground(CDC* pDC, const CRect& rcBounds);
	void DrawPlanTable     (CDC* pDC, const CRect& rcBounds);
	void DrawPlanBox       (CDC* pDC, const CRect& rcBounds, int nIndex);

	DECLARE_OLECREATE_EX(CUbcDailyPlanTableCtrl)    // 클래스 팩터리와 GUID입니다.
	DECLARE_OLETYPELIB(CUbcDailyPlanTableCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CUbcDailyPlanTableCtrl)     // 속성 페이지 ID입니다.
	DECLARE_OLECTLTYPE(CUbcDailyPlanTableCtrl)		// 형식 이름과 기타 상태입니다.

// 메시지 맵입니다.
	DECLARE_MESSAGE_MAP()

// 디스패치 맵입니다.
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// 이벤트 맵입니다.
	DECLARE_EVENT_MAP()

protected:
	void OnTimeForeColorChanged(void);
	OLE_COLOR m_clrTimeForeColor;
	void OnTimeBackColorChanged(void);
	OLE_COLOR m_clrTimeBackColor;
	void OnDefaultViewTimeChanged(void);
	SHORT m_nDefaultViewTime;
	void OnViewRangeChanged(void);
	SHORT m_nViewRange;
	void OnPlanBackColorChanged(void);
	OLE_COLOR m_clrPlanBackColor;
	void OnPlanForeColorChanged(void);
	OLE_COLOR m_clrPlanForeColor;
	void OnPlanBorderColorChanged(void);
	OLE_COLOR m_clrPlanBorderColor;
	void OnSelectBorderColorChanged(void);
	OLE_COLOR m_clrSelectBorderColor;
	void OnEditableChanged(void);
	VARIANT_BOOL m_bEditable;
	void OnPlanLineColorChanged(void);
	OLE_COLOR m_clrPlanLineColor;
	void OnTimeLineColorChanged(void);
	OLE_COLOR m_clrTimeLineColor;
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
protected:
	void DeleteAllItem(void);
	VARIANT_BOOL DeleteItem(LONG nIndex);
	LONG GetItemCount(void);
	LONG GetSelectedIndex(void);
	void SetSelectedIndex(LONG nIndex);
	LONG AddItem(LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, LONG* lParam);
	LONG InsertItem(LONG nIndex, LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, LONG* lParam);
	VARIANT_BOOL SetItem(LONG nIndex, ULONG nFmt, LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, LONG* lParam);
	VARIANT_BOOL GetItem(LONG nIndex, LONG* lParam);

	void SelChange(LONG nIndex)
	{
		FireEvent(eventidSelChange, EVENT_PARAM(VTS_I4), nIndex);
	}
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
protected:

	void InfoChange(LONG nIndex, DATE tmStart, DATE tmEnd)
	{
		FireEvent(eventidInfoChange, EVENT_PARAM(VTS_I4 VTS_DATE VTS_DATE), nIndex, tmStart, tmEnd);
	}
	void OnMagneticMoveChanged(void);
	USHORT m_nMagneticMove;
	VARIANT_BOOL SetZOrder(LONG nCurIndex, LONG nNewIndex);
	void SetItemColor(LONG nIndex, VARIANT_BOOL bGradient, OLE_COLOR clrColor);

	void ItemClick(LONG nItem)
	{
		FireEvent(eventidItemClick, EVENT_PARAM(VTS_I4), nItem);
	}

	void ItemDblClick(LONG nIndex)
	{
		FireEvent(eventidItemDblClick, EVENT_PARAM(VTS_I4), nIndex);
	}
};

