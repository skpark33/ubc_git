

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Jun 12 14:57:04 2015
 */
/* Compiler settings for .\UbcDailyPlanTable.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __UBCDailyPlanTableOCXidl_h__
#define __UBCDailyPlanTableOCXidl_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef ___DUbcDailyPlanTable_FWD_DEFINED__
#define ___DUbcDailyPlanTable_FWD_DEFINED__
typedef interface _DUbcDailyPlanTable _DUbcDailyPlanTable;
#endif 	/* ___DUbcDailyPlanTable_FWD_DEFINED__ */


#ifndef ___DUbcDailyPlanTableEvents_FWD_DEFINED__
#define ___DUbcDailyPlanTableEvents_FWD_DEFINED__
typedef interface _DUbcDailyPlanTableEvents _DUbcDailyPlanTableEvents;
#endif 	/* ___DUbcDailyPlanTableEvents_FWD_DEFINED__ */


#ifndef __UbcDailyPlanTable_FWD_DEFINED__
#define __UbcDailyPlanTable_FWD_DEFINED__

#ifdef __cplusplus
typedef class UbcDailyPlanTable UbcDailyPlanTable;
#else
typedef struct UbcDailyPlanTable UbcDailyPlanTable;
#endif /* __cplusplus */

#endif 	/* __UbcDailyPlanTable_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 


#ifndef __UbcDailyPlanTableLib_LIBRARY_DEFINED__
#define __UbcDailyPlanTableLib_LIBRARY_DEFINED__

/* library UbcDailyPlanTableLib */
/* [control][helpstring][helpfile][version][uuid] */ 


EXTERN_C const IID LIBID_UbcDailyPlanTableLib;

#ifndef ___DUbcDailyPlanTable_DISPINTERFACE_DEFINED__
#define ___DUbcDailyPlanTable_DISPINTERFACE_DEFINED__

/* dispinterface _DUbcDailyPlanTable */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DUbcDailyPlanTable;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("F3980719-D8ED-48B2-B311-B0ADEE28A806")
    _DUbcDailyPlanTable : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DUbcDailyPlanTableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DUbcDailyPlanTable * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DUbcDailyPlanTable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DUbcDailyPlanTable * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DUbcDailyPlanTable * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DUbcDailyPlanTable * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DUbcDailyPlanTable * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DUbcDailyPlanTable * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DUbcDailyPlanTableVtbl;

    interface _DUbcDailyPlanTable
    {
        CONST_VTBL struct _DUbcDailyPlanTableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DUbcDailyPlanTable_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _DUbcDailyPlanTable_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _DUbcDailyPlanTable_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _DUbcDailyPlanTable_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _DUbcDailyPlanTable_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _DUbcDailyPlanTable_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _DUbcDailyPlanTable_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DUbcDailyPlanTable_DISPINTERFACE_DEFINED__ */


#ifndef ___DUbcDailyPlanTableEvents_DISPINTERFACE_DEFINED__
#define ___DUbcDailyPlanTableEvents_DISPINTERFACE_DEFINED__

/* dispinterface _DUbcDailyPlanTableEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DUbcDailyPlanTableEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("AFE0E8D7-B7D7-4FC1-95F6-FB65C58FEDA1")
    _DUbcDailyPlanTableEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DUbcDailyPlanTableEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DUbcDailyPlanTableEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DUbcDailyPlanTableEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DUbcDailyPlanTableEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DUbcDailyPlanTableEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DUbcDailyPlanTableEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DUbcDailyPlanTableEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DUbcDailyPlanTableEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DUbcDailyPlanTableEventsVtbl;

    interface _DUbcDailyPlanTableEvents
    {
        CONST_VTBL struct _DUbcDailyPlanTableEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DUbcDailyPlanTableEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _DUbcDailyPlanTableEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _DUbcDailyPlanTableEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _DUbcDailyPlanTableEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _DUbcDailyPlanTableEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _DUbcDailyPlanTableEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _DUbcDailyPlanTableEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DUbcDailyPlanTableEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_UbcDailyPlanTable;

#ifdef __cplusplus

class DECLSPEC_UUID("B1F60086-AE0E-4032-97BA-73F0D6F9A833")
UbcDailyPlanTable;
#endif
#endif /* __UbcDailyPlanTableLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


