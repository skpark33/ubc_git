// UbcDailyPlanTableCtrl.cpp : CUbcDailyPlanTableCtrl ActiveX 컨트롤 클래스의 구현입니다.

#include "stdafx.h"
#include "UbcDailyPlanTable.h"
#include "UbcDailyPlanTableCtrl.h"
#include "UbcDailyPlanTablePropPage.h"
#include "MemDC.h"

#define		DEFAULT_VIEW_TIME				9
#define		DEFAULT_VEIW_RANGE				12
#define		DEFAULT_MARGIN					6
#define		DEFAULT_TIME_BACK_COLOR			RGB(0xF0,0xF8,0xFF)	// aliceblue
#define		DEFAULT_TIME_FORE_COLOR			RGB(22,22,22)
#define		DEFAULT_TIME_LINE_COLOR			::GetSysColor(COLOR_BTNSHADOW)
#define		DEFAULT_PLAN_BACK_COLOR			RGB(0xF8,0xF8,0xFF)	// ghostwhite
#define		DEFAULT_PLAN_FORE_COLOR			RGB(22,22,22)
#define		DEFAULT_PLAN_LINE_COLOR			::GetSysColor(COLOR_BTNSHADOW)
#define		DEFAULT_PLAN_BORDER_COLOR		::GetSysColor(COLOR_BTNSHADOW)
#define		DEFAULT_SELECT_BORDER_COLOR		RGB(64,64,64)

#define MAX_COLOR 12
const COLORREF g_clrBoxBack[] = { RGB(  0,255,  0)
								, RGB(  0,  0,255)
								, RGB(128,  0,  0)
								, RGB(  0,128,  0)
								, RGB(  0,  0,128)
								, RGB(255,255,  0)
								, RGB(  0,255,255)
								, RGB(255,  0,255)
								, RGB(128,128,  0)
								, RGB(  0,128,128)
								, RGB(128,  0,128)
								, RGB(255,  0,  0)
								};

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CUbcDailyPlanTableCtrl, COleControl)



// 메시지 맵입니다.

BEGIN_MESSAGE_MAP(CUbcDailyPlanTableCtrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
	ON_WM_SIZE()
	ON_WM_VSCROLL()
	ON_WM_CREATE()
	ON_WM_MOUSEWHEEL()
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()



// 디스패치 맵입니다.

BEGIN_DISPATCH_MAP(CUbcDailyPlanTableCtrl, COleControl)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
	DISP_STOCKPROP_BORDERSTYLE()
	DISP_STOCKPROP_FONT()
	DISP_STOCKPROP_BACKCOLOR()
	DISP_STOCKPROP_FORECOLOR()
	DISP_STOCKFUNC_REFRESH()
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "TimeForeColor", dispidTimeForeColor, m_clrTimeForeColor, OnTimeForeColorChanged, VT_COLOR)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "TimeBackColor", dispidTimeBackColor, m_clrTimeBackColor, OnTimeBackColorChanged, VT_COLOR)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "DefaultViewTime", dispidDefaultViewTime, m_nDefaultViewTime, OnDefaultViewTimeChanged, VT_I2)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "ViewRange", dispidViewRange, m_nViewRange, OnViewRangeChanged, VT_I2)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "PlanBackColor", dispidPlanBackColor, m_clrPlanBackColor, OnPlanBackColorChanged, VT_COLOR)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "PlanForeColor", dispidPlanForeColor, m_clrPlanForeColor, OnPlanForeColorChanged, VT_COLOR)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "PlanBorderColor", dispidPlanBorderColor, m_clrPlanBorderColor, OnPlanBorderColorChanged, VT_COLOR)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "SelectBorderColor", dispidSelectBorderColor, m_clrSelectBorderColor, OnSelectBorderColorChanged, VT_COLOR)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "Editable", dispidEditable, m_bEditable, OnEditableChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "PlanLineColor", dispidPlanLineColor, m_clrPlanLineColor, OnPlanLineColorChanged, VT_COLOR)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "TimeLineColor", dispidTimeLineColor, m_clrTimeLineColor, OnTimeLineColorChanged, VT_COLOR)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "DeleteAllItem", dispidDeleteAllItem, DeleteAllItem, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "DeleteItem", dispidDeleteItem, DeleteItem, VT_BOOL, VTS_I4)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "GetItemCount", dispidGetItemCount, GetItemCount, VT_I4, VTS_NONE)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "GetSelectedIndex", dispidGetSelectedIndex, GetSelectedIndex, VT_I4, VTS_NONE)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "SetSelectedIndex", dispidSetSelectedIndex, SetSelectedIndex, VT_EMPTY, VTS_I4)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "AddItem", dispidAddItem, AddItem, VT_I4, VTS_BSTR VTS_DATE VTS_DATE VTS_PI4)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "InsertItem", dispidInsertItem, InsertItem, VT_I4, VTS_I4 VTS_BSTR VTS_DATE VTS_DATE VTS_PI4)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "SetItem", dispidSetItem, SetItem, VT_BOOL, VTS_I4 VTS_UI4 VTS_BSTR VTS_DATE VTS_DATE VTS_PI4)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "GetItem", dispidGetItem, GetItem, VT_BOOL, VTS_I4 VTS_PI4)
	DISP_PROPERTY_NOTIFY_ID(CUbcDailyPlanTableCtrl, "MagneticMove", dispidMagneticMove, m_nMagneticMove, OnMagneticMoveChanged, VT_UI2)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "SetZOrder", dispidSetZOrder, SetZOrder, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION_ID(CUbcDailyPlanTableCtrl, "SetItemColor", dispidSetItemColor, SetItemColor, VT_EMPTY, VTS_I4 VTS_BOOL VTS_COLOR)
END_DISPATCH_MAP()



// 이벤트 맵입니다.

BEGIN_EVENT_MAP(CUbcDailyPlanTableCtrl, COleControl)
	EVENT_CUSTOM_ID("SelChange", eventidSelChange, SelChange, VTS_I4)
	EVENT_CUSTOM_ID("InfoChange", eventidInfoChange, InfoChange, VTS_I4 VTS_DATE VTS_DATE)
	EVENT_CUSTOM_ID("ItemClick", eventidItemClick, ItemClick, VTS_I4)
	EVENT_CUSTOM_ID("ItemDblClick", eventidItemDblClick, ItemDblClick, VTS_I4)
END_EVENT_MAP()



// 속성 페이지입니다.

// TODO: 필요에 따라 속성 페이지를 추가합니다. 카운트도 이에 따라 증가해야 합니다.
BEGIN_PROPPAGEIDS(CUbcDailyPlanTableCtrl, 1)
	PROPPAGEID(CUbcDailyPlanTablePropPage::guid)
END_PROPPAGEIDS(CUbcDailyPlanTableCtrl)



// 클래스 팩터리와 GUID를 초기화합니다.

IMPLEMENT_OLECREATE_EX(CUbcDailyPlanTableCtrl, "SQI.UbcDailyPlanTablCtrl.1",
	0xb1f60086, 0xae0e, 0x4032, 0x97, 0xba, 0x73, 0xf0, 0xd6, 0xf9, 0xa8, 0x33)



// 형식 라이브러리 ID와 버전입니다.

IMPLEMENT_OLETYPELIB(CUbcDailyPlanTableCtrl, _tlid, _wVerMajor, _wVerMinor)



// 인터페이스 ID입니다.

const IID BASED_CODE IID_DUbcDailyPlanTable =
		{ 0xF3980719, 0xD8ED, 0x48B2, { 0xB3, 0x11, 0xB0, 0xAD, 0xEE, 0x28, 0xA8, 0x6 } };
const IID BASED_CODE IID_DUbcDailyPlanTableEvents =
		{ 0xAFE0E8D7, 0xB7D7, 0x4FC1, { 0x95, 0xF6, 0xFB, 0x65, 0xC5, 0x8F, 0xED, 0xA1 } };



// 컨트롤 형식 정보입니다.

static const DWORD BASED_CODE _dwUbcDailyPlanTableOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CUbcDailyPlanTableCtrl, IDS_UBCDAILYPLANTABLE, _dwUbcDailyPlanTableOleMisc)



// CUbcDailyPlanTableCtrl::CUbcDailyPlanTableCtrlFactory::UpdateRegistry -
// CUbcDailyPlanTableCtrl에서 시스템 레지스트리 항목을 추가하거나 제거합니다.

BOOL CUbcDailyPlanTableCtrl::CUbcDailyPlanTableCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: 컨트롤이 아파트 모델 스레딩 규칙을 준수하는지
	// 확인합니다. 자세한 내용은 MFC Technical Note 64를
	// 참조하십시오. 컨트롤이 아파트 모델 규칙에
	// 맞지 않는 경우 다음에서 여섯 번째 매개 변수를 변경합니다.
	// afxRegInsertable | afxRegApartmentThreading에서 afxRegInsertable로 변경합니다.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_UBCDAILYPLANTABLE,
			IDB_UBCDAILYPLANTABLE,
			afxRegInsertable | afxRegApartmentThreading,
			_dwUbcDailyPlanTableOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// CUbcDailyPlanTableCtrl::CUbcDailyPlanTableCtrl - 생성자입니다.

CUbcDailyPlanTableCtrl::CUbcDailyPlanTableCtrl()
{
	InitializeIIDs(&IID_DUbcDailyPlanTable, &IID_DUbcDailyPlanTableEvents);

	m_bEditable            = TRUE;

	m_nDefaultViewTime     = DEFAULT_VIEW_TIME ;
	m_nViewRange	       = DEFAULT_VEIW_RANGE;

	m_clrTimeBackColor     = DEFAULT_TIME_BACK_COLOR	;
	m_clrTimeForeColor     = DEFAULT_TIME_FORE_COLOR	;
	m_clrTimeLineColor     = DEFAULT_TIME_LINE_COLOR	;
	m_clrPlanBackColor     = DEFAULT_PLAN_BACK_COLOR	;
	m_clrPlanForeColor     = DEFAULT_PLAN_FORE_COLOR	;
	m_clrPlanLineColor     = DEFAULT_PLAN_LINE_COLOR	;
	m_clrPlanBorderColor   = DEFAULT_PLAN_BORDER_COLOR	;
	m_clrSelectBorderColor = DEFAULT_SELECT_BORDER_COLOR;

	// Stock
	m_clrBackColor         = ::GetSysColor(COLOR_BACKGROUND);
	m_clrForeColor         = ::GetSysColor(COLOR_WINDOWTEXT);

	m_sizeFont			   = CSize(8, 16);
	m_nSeparatorX		   = DEFAULT_MARGIN + m_sizeFont.cx*3 + DEFAULT_MARGIN;

	m_MouseType			   = MOUSE_IDLE;
	m_lSelectedIndex       = -1;
	m_lClickedIndex        = -1;

	m_nMagneticMove        = 10;	// Minute;

	// TEST
//	AddItem(_T("test1test1test1test1test1test1test1test1"), CTime(2010,6,14,22,0,0).GetTime(), CTime(2010,6,14,02,0,0).GetTime(), NULL);
//	AddItem(_T("test2"), CTime(2010,6,14,12,0,0).GetTime(), CTime(2010,6,14,18,0,0).GetTime(), NULL);
//	AddItem(_T("test3"), CTime(2010,6,14,12,0,0).GetTime(), CTime(2010,6,14,18,0,0).GetTime(), NULL);
//	InsertItem(0,_T("test0"), CTime(2010,6,14,22,0,0).GetTime(), CTime(2010,6,14,02,0,0).GetTime(), NULL);
//	SetZOrder(0, 3);
//	SetItemColor(0, false, RGB(0,255,0));
//	SetItemColor(1, true , RGB(0,255,0));

	Refresh();
}

// CUbcDailyPlanTableCtrl::~CUbcDailyPlanTableCtrl - 소멸자입니다.

CUbcDailyPlanTableCtrl::~CUbcDailyPlanTableCtrl()
{
	// TODO: 여기에서 컨트롤의 인스턴스 데이터를 정리합니다.
}

int CUbcDailyPlanTableCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (COleControl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SCROLLINFO info;
	info.cbSize = sizeof(info);
	info.fMask = SIF_RANGE | SIF_POS;
	info.nMin = 0;
	info.nMax = 23;
	info.nPage = info.nMax/4;
	info.nPos = m_nDefaultViewTime;
	info.nTrackPos = 0;

	SetScrollInfo(SB_VERT, &info);

	return 0;
}


// CUbcDailyPlanTableCtrl::OnDraw - 그리기 함수입니다.

void CUbcDailyPlanTableCtrl::OnDraw(CDC* pDC, const CRect& rcBounds, const CRect& rcInvalid)
{
	if(!pDC) return;

	CalcFontSize();
	m_nOneHourHeight = (rcBounds.Height()-m_sizeFont.cy/2) / m_nViewRange;

    CMemDC dcMem(pDC);

//	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
//	pdc->Ellipse(rcBounds);

	//pDC->FillSolidRect(rcBounds, GetBackColor());
	pDC->FillSolidRect(rcBounds, m_clrBackColor);
	pDC->Draw3dRect(rcBounds, ::GetSysColor(COLOR_3DHIGHLIGHT), ::GetSysColor(COLOR_3DSHADOW));

	DrawTimeTable(&dcMem, rcBounds);
	DrawPlanBackground(&dcMem, rcBounds);
	DrawPlanTable(&dcMem, rcBounds);
}

void CUbcDailyPlanTableCtrl::DrawTimeTable(CDC* pDC, const CRect& rcBounds)
{
	CRect rcTimeTable = rcBounds;
	rcTimeTable.right = m_nSeparatorX;

	COLOR16 red   = (GetRValue(m_clrTimeBackColor) << 8);
	COLOR16 green = (GetGValue(m_clrTimeBackColor) << 8);
	COLOR16 blue  = (GetBValue(m_clrTimeBackColor) << 8);
	COLOR16 max = (( red > green ? red : green) > blue ? ( red > green ? red : green) : blue);

	COLOR16 red1   = (max > 0x7F00 ? red  /2 : red  *2);
	COLOR16 green1 = (max > 0x7F00 ? green/2 : green*2);
	COLOR16 blue1  = (max > 0x7F00 ? blue /2 : blue *2);

	TRIVERTEX vert[4] =
	{   //                                       Red   Green   Blue   Alpha
		{ rcTimeTable.left , rcTimeTable.top   , red1, green1, blue1, 0x0000 },
		{ rcTimeTable.right, rcTimeTable.bottom, red , green , blue , 0x0000 }
	};
	GRADIENT_RECT gRect = { 0, 1 };

	pDC->GradientFill(vert,2,&gRect,1,GRADIENT_FILL_RECT_H);

	int nOldBkMode = pDC->SetBkMode(TRANSPARENT); // TRANSPARENT

	// 시간 그리기
	COLORREF clrOldColor = pDC->SetTextColor(m_clrTimeForeColor);

	CRect rcTime = rcTimeTable;
	rcTime.bottom = rcTime.top + m_nOneHourHeight;

	int nTime = 0;
	for(int i=0; i<=m_nViewRange ;i++)
	{
		nTime = (m_nDefaultViewTime + i) % 24;

		CString strTime;
		strTime.Format(_T("%2d"), nTime);
		pDC->DrawText(strTime, rcTime, DT_CENTER | DT_SINGLELINE);//| DT_VCENTER);

		rcTime.top += m_nOneHourHeight;
		rcTime.bottom = rcTime.top + m_nOneHourHeight;
	}
	pDC->SetTextColor(clrOldColor);

	// Ruler 그리기
	CPen penLine(PS_SOLID, 1, m_clrTimeLineColor);
	CPen* pOldPen = pDC->SelectObject(&penLine);

	for(int i=0; i<24 ;i++)
	{
		m_nRulerPos[i][0] = -99;
		m_nRulerPos[i][1] = -99;
		m_nRulerPos[i][2] = -99;
		m_nRulerPos[i][3] = -99;
		m_nRulerPos[i][4] = -99;
		m_nRulerPos[i][5] = -99;
	}

	int nLineY = rcTimeTable.top + m_sizeFont.cy/2;
	for(int i=0; i<=m_nViewRange ;i++)
	{
		nTime = (m_nDefaultViewTime + i) % 24;

		int nTmpY = nLineY;
		pDC->MoveTo(rcTimeTable.right-7, nTmpY);
		pDC->LineTo(rcTimeTable.right-1, nTmpY);

		// 배열에 마지막 정보가 저장되는 현상을 제거함
		if(i < 24) m_nRulerPos[nTime][0] = nTmpY;

		if(i<m_nViewRange)
		{
			nTmpY = nLineY + m_nOneHourHeight/6;
			pDC->MoveTo(rcTimeTable.right-3, nTmpY);
			pDC->LineTo(rcTimeTable.right-1, nTmpY);
			m_nRulerPos[nTime][1] = nTmpY;

			nTmpY = nLineY + m_nOneHourHeight*2/6;
			pDC->MoveTo(rcTimeTable.right-3, nTmpY);
			pDC->LineTo(rcTimeTable.right-1, nTmpY);
			m_nRulerPos[nTime][2] = nTmpY;

			nTmpY = nLineY + m_nOneHourHeight/2;
			pDC->MoveTo(rcTimeTable.right-5, nTmpY);
			pDC->LineTo(rcTimeTable.right-1, nTmpY);
			m_nRulerPos[nTime][3] = nTmpY;

			nTmpY = nLineY + m_nOneHourHeight*4/6;
			pDC->MoveTo(rcTimeTable.right-3, nTmpY);
			pDC->LineTo(rcTimeTable.right-1, nTmpY);
			m_nRulerPos[nTime][4] = nTmpY;

			nTmpY = nLineY + m_nOneHourHeight*5/6;
			pDC->MoveTo(rcTimeTable.right-3, nTmpY);
			pDC->LineTo(rcTimeTable.right-1, nTmpY);
			m_nRulerPos[nTime][5] = nTmpY;
		}

		nLineY += m_nOneHourHeight;
	}

	pDC->SelectObject(pOldPen);
	pDC->SetBkMode(nOldBkMode);
}

void CUbcDailyPlanTableCtrl::DrawPlanBackground(CDC* pDC, const CRect& rcBounds)
{
	CRect rcPlanTable = rcBounds;
	rcPlanTable.left = m_nSeparatorX;

	COLOR16 red   = (GetRValue(m_clrBackColor) << 8);
	COLOR16 green = (GetGValue(m_clrBackColor) << 8);
	COLOR16 blue  = (GetBValue(m_clrBackColor) << 8);
	COLOR16 max = (( red > green ? red : green) > blue ? ( red > green ? red : green) : blue);

	COLOR16 red1   = (max > 0x7F00 ? red  /2 : red  *2);
	COLOR16 green1 = (max > 0x7F00 ? green/2 : green*2);
	COLOR16 blue1  = (max > 0x7F00 ? blue /2 : blue *2);

	TRIVERTEX vert[4] =
	{   //                                       Red   Green   Blue   Alpha
		{ rcPlanTable.left , rcPlanTable.top   , red1, green1, blue1, 0x0000 },
		{ rcPlanTable.right, rcPlanTable.bottom, red , green , blue , 0x0000 }
	};
	GRADIENT_RECT gRect = { 0, 1 };

	pDC->GradientFill(vert,2,&gRect,1,GRADIENT_FILL_RECT_H);

	int nOldBkMode = pDC->SetBkMode(TRANSPARENT); // TRANSPARENT

	CPen penLine(PS_DOT, 1, m_clrPlanLineColor);
	CPen* pOldPen = pDC->SelectObject(&penLine);

	int nLineY = rcPlanTable.top + m_sizeFont.cy/2;
	for(int i=0; i<=m_nViewRange ;i++)
	{
		pDC->MoveTo(rcPlanTable.left , nLineY);
		pDC->LineTo(rcPlanTable.right, nLineY);
		nLineY += m_nOneHourHeight;
	}

	pDC->SelectObject(pOldPen);
	pDC->SetBkMode(nOldBkMode);
}

void CUbcDailyPlanTableCtrl::DrawPlanTable(CDC* pDC, const CRect& rcBounds)
{
	for(int i=0; i<m_aPlanItemList.GetCount() ; i++)
	{
		if(m_lSelectedIndex != i)
		{
			DrawPlanBox(pDC, rcBounds, i);
		}
	}

	if(m_lSelectedIndex >= 0)
	{
		DrawPlanBox(pDC, rcBounds, m_lSelectedIndex);
	}
}

void CUbcDailyPlanTableCtrl::DrawPlanBox(CDC* pDC, const CRect& rcBounds, int nIndex)
{
	int nViewEnd = (m_nDefaultViewTime + m_nViewRange)%24;

	P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[nIndex];
	CString strText = pItem->strText    ;
	CTime   tmStart = pItem->tmStartTime;
	CTime   tmEnd   = pItem->tmEndTime  ;

	CRect rcBox[2];
	int nDrawCnt = 1;
	if(!GetPlanRect(tmStart, tmEnd, rcBounds, nDrawCnt, rcBox)) return;

	COLORREF clrColor = (pItem->clrColor == -1 ? g_clrBoxBack[nIndex%MAX_COLOR] : pItem->clrColor);

	COLOR16 red   = (GetRValue(clrColor) << 8);
	COLOR16 green = (GetGValue(clrColor) << 8);
	COLOR16 blue  = (GetBValue(clrColor) << 8);
	COLOR16 max = (( red > green ? red : green) > blue ? ( red > green ? red : green) : blue);

	COLOR16 red1   = (max > 0x7F00 ? red  /2 : red  *2);
	COLOR16 green1 = (max > 0x7F00 ? green/2 : green*2);
	COLOR16 blue1  = (max > 0x7F00 ? blue /2 : blue *2);

	int nOldBkMode = pDC->SetBkMode(TRANSPARENT); // TRANSPARENT

	// 패키지명 그리기
	COLORREF clrOldColor = pDC->SetTextColor(m_clrPlanForeColor);

	for(int i=0; i<nDrawCnt ;i++)
	{
		CRgn rgn;
		rgn.CreateRoundRectRgn(rcBox[i].left, rcBox[i].top, rcBox[i].right, rcBox[i].bottom, 6, 6);
		pDC->SelectClipRgn(&rgn);
		rgn.DeleteObject();

		pDC->FillSolidRect(rcBox[i], (m_lSelectedIndex == nIndex ? m_clrSelectBorderColor : m_clrPlanBorderColor));
		int nBorder = (m_lSelectedIndex == nIndex ? 2 : 1);
		rcBox[i].DeflateRect(nBorder,nBorder);

		rgn.CreateRoundRectRgn(rcBox[i].left, rcBox[i].top, rcBox[i].right, rcBox[i].bottom, 6, 6);
		pDC->SelectClipRgn(&rgn);
		rgn.DeleteObject();

		if(pItem->bGradient)
		{
			int nPosY = rcBox[i].CenterPoint().y + (rcBox[i].Height()/4*3);
			TRIVERTEX vert[4] =
			{   //                              Red     Green   Blue   Alpha
				{ rcBox[i].left , rcBox[i].top    , 0xFF00, 0xFF00, 0xFF00, 0x0000 },
				{ rcBox[i].right, nPosY           ,   red1, green1,  blue1, 0x0000 },
				{ rcBox[i].left , nPosY           ,   red1, green1,  blue1, 0x0000 },
				{ rcBox[i].right, rcBox[i].bottom ,   red , green ,  blue , 0x0000 }
			};
			GRADIENT_RECT gRect1 = { 0, 1 };
			GRADIENT_RECT gRect2 = { 2, 3 };

			pDC->GradientFill(vert,4,&gRect1,1,GRADIENT_FILL_RECT_V);
			pDC->GradientFill(vert,4,&gRect2,1,GRADIENT_FILL_RECT_V);
		}
		else
		{
			pDC->FillSolidRect(rcBox[i], clrColor);
		}

#ifdef _DEBUG
		CString strDebug;
		strDebug.Format(_T("%d\n%s\nstart = %s\nend = %s")
						, nIndex
						, strText
						, tmStart.Format("%H:%M:%S")
						, tmEnd.Format("%H:%M:%S")
						);
		strText = strDebug;

		pDC->DrawText(strText, rcBox[i], DT_WORD_ELLIPSIS | DT_VCENTER);
		//pDC->ExtTextOut(rcBox[i].left, rcBox[i].top, ETO_CLIPPED, rcBox[i], strText, NULL);
#else
		//pDC->DrawText(strText, rcBox[i], DT_CENTER | DT_SINGLELINE | DT_VCENTER);
		pDC->DrawText(strText, rcBox[i], DT_SINGLELINE | DT_VCENTER);
#endif

		pDC->SelectClipRgn(NULL);
	}

	pDC->SetTextColor(clrOldColor);
	pDC->SetBkMode(nOldBkMode);
}

bool CUbcDailyPlanTableCtrl::IsVisibleTime(int nHour)
{
	if(m_nViewRange == 24) return true;

	int nEndViewTime = (m_nDefaultViewTime + m_nViewRange)%24;

	if(m_nDefaultViewTime <= nEndViewTime)
	{
		if( nHour < m_nDefaultViewTime || 
			nHour > nEndViewTime       )
		{
			return false;
		}
	}
	else
	{
		if( nHour < m_nDefaultViewTime &&
			nHour > nEndViewTime        )
		{
			return false;
		}
	}

	return true;
}

bool CUbcDailyPlanTableCtrl::IsVisibleTime(int nStartHour, int nEndHour)
{
	if(nStartHour > nEndHour)
	{
		for(int i=nStartHour; i<24 ;i++)
		{
			if(IsVisibleTime(i)) return true;
		}
		for(int i=0; i<=nEndHour ;i++)
		{
			if(IsVisibleTime(i)) return true;
		}
	}
	else if(nStartHour == nEndHour)
	{
		return IsVisibleTime(nStartHour);
	}
	else
	{
		for(int i=nStartHour; i<=nEndHour ;i++)
		{
			if(IsVisibleTime(i)) return true;
		}
	}

	return false;
}


bool CUbcDailyPlanTableCtrl::GetPlanRect(CTime tmStart, CTime tmEnd, const CRect& rcBounds, int& nBoxCnt, CRect rcBox[])
{
	if(!IsVisibleTime(tmStart.GetHour(), tmEnd.GetHour())) return false;

	CRect rcPlan  = rcBounds;
	rcPlan.left   = m_nSeparatorX;
	rcPlan.left  += DEFAULT_MARGIN * 2;
	rcPlan.right -= DEFAULT_MARGIN * 2;

	rcPlan.top    = (IsVisibleTime(tmStart.GetHour()) ? m_nRulerPos[tmStart.GetHour()][0] + m_nOneHourHeight*tmStart.GetMinute()/60    : rcBounds.top    - 6);
	rcPlan.bottom = (IsVisibleTime(tmEnd.GetHour  ()) ? m_nRulerPos[tmEnd  .GetHour()][0] + m_nOneHourHeight*tmEnd  .GetMinute()/60 + 1: rcBounds.bottom + 6);

//	CRect rcBox[2];
	nBoxCnt = 1;
	rcBox[0] = rcPlan;
	if(IsVisibleTime(tmStart.GetHour()) && IsVisibleTime(tmEnd.GetHour()))
	{
		if(rcPlan.top > rcPlan.bottom)
		{
			nBoxCnt = 2;
			rcBox[0] = rcPlan;
			rcBox[0].top    = rcBounds.top - 6;
			//rcBox[0].bottom = rcPlan.bottom;

			rcBox[1] = rcPlan;
			//rcBox[1].top    = rcPlan.top;
			rcBox[1].bottom = rcBounds.bottom + 6;
		}
	}

	return true;
}

// CUbcDailyPlanTableCtrl::DoPropExchange - 지속성 지원입니다.

void CUbcDailyPlanTableCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: 지속적인 사용자 지정 속성 모두에 대해 PX_ functions를 호출합니다.
}



// CUbcDailyPlanTableCtrl::OnResetState - 컨트롤을 기본 상태로 다시 설정합니다.

void CUbcDailyPlanTableCtrl::OnResetState()
{
	COleControl::OnResetState();  // DoPropExchange에 들어 있는 기본값을 다시 설정합니다.

	// TODO: 여기에서 다른 모든 컨트롤의 상태를 다시 설정합니다.
}



// CUbcDailyPlanTableCtrl::AboutBox - "정보" 대화 상자를 사용자에게 보여 줍니다.

void CUbcDailyPlanTableCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_UBCDAILYPLANTABLE);
	dlgAbout.DoModal();
}



// CUbcDailyPlanTableCtrl 메시지 처리기입니다.

void CUbcDailyPlanTableCtrl::OnTimeForeColorChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnTimeBackColorChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnDefaultViewTimeChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnViewRangeChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnPlanBackColorChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnPlanForeColorChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnPlanBorderColorChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnSelectBorderColorChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnEditableChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnPlanLineColorChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}


void CUbcDailyPlanTableCtrl::OnTimeLineColorChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnMagneticMoveChanged(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 여기에 속성 처리기 코드를 추가합니다.

	SetModifiedFlag();
}

void CUbcDailyPlanTableCtrl::OnSize(UINT nType, int cx, int cy)
{
	COleControl::OnSize(nType, cx, cy);

	CalcFontSize();
}

void CUbcDailyPlanTableCtrl::CalcFontSize()
{
	if (IsWindow(m_hWnd))
	{
		CDC* pDC=GetDC();
		int nOldDC=pDC->SaveDC();

		m_sizeFont = pDC->GetTextExtent(_T("_"));

		pDC->RestoreDC(nOldDC);
		ReleaseDC(pDC);

		m_nSeparatorX = DEFAULT_MARGIN + m_sizeFont.cx*3 + DEFAULT_MARGIN;
	}
}

void CUbcDailyPlanTableCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	COleControl::OnVScroll(nSBCode, nPos, pScrollBar);

	switch(nSBCode)
	{
	case SB_TOP   :
		m_nDefaultViewTime = 0;
		SetScrollPos(SB_VERT, m_nDefaultViewTime);
		break;
	case SB_BOTTOM  :
		m_nDefaultViewTime = 24 - m_nViewRange;
		SetScrollPos(SB_VERT, m_nDefaultViewTime);
		break;
	case SB_PAGEUP:
		//m_nDefaultViewTime = GetScrollPos(SB_VERT)-((24 - m_nViewRange)/4);
		//if(m_nDefaultViewTime > (0xFFFF-24)) m_nDefaultViewTime = 24 - m_nViewRange;
		m_nDefaultViewTime = (m_nDefaultViewTime - 4) % 24;
		if(m_nDefaultViewTime < 0) m_nDefaultViewTime = 24 + m_nDefaultViewTime;
		SetScrollPos(SB_VERT, m_nDefaultViewTime);
		break;
	case SB_PAGEDOWN:
		//m_nDefaultViewTime = GetScrollPos(SB_VERT)+((24 - m_nViewRange)/4);
		//if(m_nDefaultViewTime > (24 - m_nViewRange)) m_nDefaultViewTime = 0;
		m_nDefaultViewTime = (m_nDefaultViewTime + 4) % 24;
		SetScrollPos(SB_VERT, m_nDefaultViewTime);
		break;
	case SB_LINEUP:
		//m_nDefaultViewTime = GetScrollPos(SB_VERT)-1;
		//if(m_nDefaultViewTime > (0xFFFF-24)) m_nDefaultViewTime = 24 - m_nViewRange;
		m_nDefaultViewTime = (m_nDefaultViewTime - 1) % 24;
		if(m_nDefaultViewTime < 0) m_nDefaultViewTime = 24 + m_nDefaultViewTime;
		SetScrollPos(SB_VERT, m_nDefaultViewTime);
		break;
	case SB_LINEDOWN:
		//m_nDefaultViewTime = GetScrollPos(SB_VERT)+1;
		//if(m_nDefaultViewTime > (24 - m_nViewRange)) m_nDefaultViewTime = 0;
		m_nDefaultViewTime = (m_nDefaultViewTime + 1) % 24;
		SetScrollPos(SB_VERT, m_nDefaultViewTime);
		break;
	case SB_THUMBTRACK:
		m_nDefaultViewTime = nPos;
		SetScrollPos(SB_VERT, m_nDefaultViewTime);
		break;
	default:
		break;
	}

	//Invalidate();
	Refresh();
}

void CUbcDailyPlanTableCtrl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch(nChar)
	{
	case VK_HOME:
		OnVScroll(SB_TOP, GetScrollPos(SB_VERT), GetScrollBarCtrl(SB_VERT));
		break;
	case VK_END:
		OnVScroll(SB_BOTTOM, GetScrollPos(SB_VERT), GetScrollBarCtrl(SB_VERT));
		break;
	case VK_PRIOR:	// page up
		OnVScroll(SB_PAGEUP, GetScrollPos(SB_VERT), GetScrollBarCtrl(SB_VERT));
		break;
	case VK_NEXT:	// page down
		OnVScroll(SB_PAGEDOWN, GetScrollPos(SB_VERT), GetScrollBarCtrl(SB_VERT));
		break;
	case VK_UP:
		if(m_lSelectedIndex >= 0)
		{
			P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[m_lSelectedIndex];
			pItem->tmStartTime -= CTimeSpan(0, 0, 1, 0);
			pItem->tmEndTime   -= CTimeSpan(0, 0, 1, 0);
		}
		break;
	case VK_DOWN:
		if(m_lSelectedIndex >= 0)
		{
			P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[m_lSelectedIndex];
			pItem->tmStartTime += CTimeSpan(0, 0, 1, 0);
			pItem->tmEndTime   += CTimeSpan(0, 0, 1, 0);
		}
		break;
	}

	//Invalidate();
	Refresh();

	COleControl::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CUbcDailyPlanTableCtrl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	if(nFlags&MK_CONTROL)
	{
		m_nViewRange += (zDelta > 0 ? 1 : -1);

		if(m_nViewRange <= 0) m_nViewRange = 1;
		if(m_nViewRange > 24)
		{
			m_nDefaultViewTime = 0;
			m_nViewRange = 24;
		}

		if(m_nViewRange > 12)
		{
			if( m_nDefaultViewTime > (12 - (m_nViewRange-12)) )
			{
				m_nDefaultViewTime = (12 - (m_nViewRange-12));
			}
		}

		SCROLLINFO info;
		info.cbSize = sizeof(info);
		info.fMask = SIF_RANGE | SIF_POS;
		info.nMin = 0;
		info.nMax = 23;
		info.nPage = info.nMax/4;
		info.nPos = m_nDefaultViewTime;
		info.nTrackPos = 0;

		SetScrollInfo(SB_VERT, &info);
	}
	else
	{
		//m_nDefaultViewTime = GetScrollPos(SB_VERT) + (zDelta > 0 ? -1 : 1);
		//if(m_nDefaultViewTime > (0xFFFF-24)) m_nDefaultViewTime = 23;
		//if(m_nDefaultViewTime > 23) m_nDefaultViewTime = 0;
		m_nDefaultViewTime = (m_nDefaultViewTime + (zDelta > 0 ? -1 : 1)) % 24;
		if(m_nDefaultViewTime < 0) m_nDefaultViewTime = 24 + m_nDefaultViewTime;

		SetScrollPos(SB_VERT, m_nDefaultViewTime);
	}

	//Invalidate();
	Refresh();

	return COleControl::OnMouseWheel(nFlags, zDelta, pt);
}

void CUbcDailyPlanTableCtrl::DeleteAllItem(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	for(int i=0; i<m_aPlanItemList.GetCount() ;i++)
	{
		P_PLAN_ITEM pPlanItem = (P_PLAN_ITEM)m_aPlanItemList[i];
		if(pPlanItem) delete pPlanItem;
	}
	m_aPlanItemList.RemoveAll();

	m_lSelectedIndex = -1;

	Refresh();
}

VARIANT_BOOL CUbcDailyPlanTableCtrl::DeleteItem(LONG nIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(nIndex < 0 || nIndex >= GetItemCount())
	{
		return VARIANT_FALSE;
	}

	P_PLAN_ITEM pPlanItem = (P_PLAN_ITEM)m_aPlanItemList[nIndex];
	if(pPlanItem) delete pPlanItem;
	m_aPlanItemList.RemoveAt(nIndex);

	if(m_lSelectedIndex == nIndex)
	{
		m_lSelectedIndex = -1;
	}

	Refresh();

	return VARIANT_TRUE;
}

LONG CUbcDailyPlanTableCtrl::GetItemCount(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return m_aPlanItemList.GetCount();
}

LONG CUbcDailyPlanTableCtrl::GetSelectedIndex(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return m_lSelectedIndex;
}

void CUbcDailyPlanTableCtrl::SetSelectedIndex(LONG nIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_lSelectedIndex = nIndex;

	Refresh();
}

LONG CUbcDailyPlanTableCtrl::AddItem(LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, LONG* lParam)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	P_PLAN_ITEM pItem = new ST_PLAN_ITEM;
	pItem->strText     = strText;
	pItem->tmStartTime = tmStartTime;
	pItem->tmEndTime   = tmEndTime;
	pItem->lParam      = (LPARAM)lParam;

	LONG nIndex = m_aPlanItemList.Add(pItem);

	Refresh();

	return nIndex;
}

LONG CUbcDailyPlanTableCtrl::InsertItem(LONG nIndex, LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, LONG* lParam)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	P_PLAN_ITEM pItem = new ST_PLAN_ITEM;
	pItem->strText     = strText;
	pItem->tmStartTime = tmStartTime;
	pItem->tmEndTime   = tmEndTime;
	pItem->lParam      = (LPARAM)lParam;

	m_aPlanItemList.InsertAt(nIndex, pItem);

	Refresh();

	return nIndex;
}

VARIANT_BOOL CUbcDailyPlanTableCtrl::SetItem(LONG nIndex, ULONG nFmt, LPCTSTR strText, DATE tmStartTime, DATE tmEndTime, LONG* lParam)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(nIndex < 0 || nIndex >= GetItemCount())
	{
		return VARIANT_FALSE;
	}

	P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[nIndex];
	if(nFmt & FMT_TEXT      ) pItem->strText     = strText;
	if(nFmt & FMT_START_DATE) pItem->tmStartTime = tmStartTime;
	if(nFmt & FMT_END_DATE  ) pItem->tmEndTime   = tmEndTime;
	if(nFmt & FMT_LPARAM    ) pItem->lParam      = (LPARAM)lParam;

	Refresh();

	return VARIANT_TRUE;
}

VARIANT_BOOL CUbcDailyPlanTableCtrl::GetItem(LONG nIndex, LONG* lParam)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	P_PLAN_ITEM pItem = (P_PLAN_ITEM)lParam;

	if(nIndex < 0 || nIndex >= GetItemCount() || !pItem)
	{
		return VARIANT_FALSE;
	}

	P_PLAN_ITEM pSrcItem = (P_PLAN_ITEM)m_aPlanItemList[nIndex];
	pItem->strText     = pSrcItem->strText    ;
	pItem->tmStartTime = pSrcItem->tmStartTime;
	pItem->tmEndTime   = pSrcItem->tmEndTime  ;
	pItem->lParam      = pSrcItem->lParam     ;

	return VARIANT_TRUE;
}

void CUbcDailyPlanTableCtrl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	COleControl::OnLButtonDblClk(nFlags, point);
	
	int nCount = m_aPlanItemList.GetCount();

	if(nCount <= 0) return;

	CRect rcBounds;
	GetClientRect(rcBounds);

	for(int i=0; i<nCount; i++)
	{
		P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[i];
		CTime tmStart = pItem->tmStartTime;
		CTime tmEnd   = pItem->tmEndTime  ;

		CRect rcBox[2];
		int nDrawCnt = 1;
		if(GetPlanRect(tmStart, tmEnd, rcBounds, nDrawCnt, rcBox))
		{
			for(int j=0; j<nDrawCnt ;j++)
			{
				if(rcBox[j].PtInRect(point))
				{
					ItemDblClick(i);
					break;
				}
			}
		}
	}
}

void CUbcDailyPlanTableCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	COleControl::OnLButtonDown(nFlags, point);

	int nCount = m_aPlanItemList.GetCount();

	if(nCount <= 0) return;

	if(m_MouseType == MOUSE_IDLE)
	{
		CRect rcBounds;
		GetClientRect(rcBounds);

		for(int i=0; i<nCount; i++)
		{
			P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[i];
			CTime tmStart = pItem->tmStartTime;
			CTime tmEnd   = pItem->tmEndTime  ;

			CRect rcBox[2];
			int nDrawCnt = 1;
			if(GetPlanRect(tmStart, tmEnd, rcBounds, nDrawCnt, rcBox))
			{
				for(int j=0; j<nDrawCnt ;j++)
				{
					if(rcBox[j].PtInRect(point))
					{
						m_lClickedIndex = i;
						if(m_lSelectedIndex != i)
						{
							m_lSelectedIndex = i;
							SelChange(i);
							Refresh();
						}
						break;
					}
				}
			}
		}
	}
	else if(m_MouseType >= MOUSE_OVER_LEFT_TOP &&  m_MouseType <= MOUSE_OVER_RIGHT_BOTTOM)
	{
		m_MouseType = (MOUSE_TYPE)((int)m_MouseType + (int)MOUSE_OVER_RIGHT_BOTTOM);
		m_pointStart = point;
		m_stSizing = *((P_PLAN_ITEM)m_aPlanItemList[m_lSelectedIndex]);
		SetCapture();
	}

	COleControl::OnLButtonDown(nFlags, point);
}

void CUbcDailyPlanTableCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	HitTest(point);

	COleControl::OnMouseMove(nFlags, point);
}

void CUbcDailyPlanTableCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(m_MouseType == MOUSE_IDLE)
	{
		HitTest(point);
		ItemClick(m_lClickedIndex);
	}
	else if(m_MouseType >= MOUSE_SIZING_LEFT_TOP && m_MouseType <= MOUSE_SIZING_RIGHT_BOTTOM)
	{
		::ReleaseCapture();
	}

	m_MouseType = MOUSE_IDLE;
	SetCursor(m_MouseType);

	COleControl::OnLButtonUp(nFlags, point);
}

void CUbcDailyPlanTableCtrl::HitTest(CPoint& pt)
{
	int nCount = m_aPlanItemList.GetCount();

	if(nCount <= 0 || !m_bEditable) return;

	if(m_lSelectedIndex < 0 || m_lSelectedIndex >= nCount)
	{
		m_MouseType = MOUSE_IDLE;
		SetCursor(m_MouseType);
		return;
	}

	// PLAN 선택
	if(m_MouseType < MOUSE_SIZING_LEFT_TOP || m_MouseType > MOUSE_SIZING_RIGHT_BOTTOM ) // idle or over
	{
		CRect rcBounds;
		GetClientRect(rcBounds);

		m_MouseType = MOUSE_IDLE;

		if( pt.x < (m_nSeparatorX  + DEFAULT_MARGIN * 2) ||
			pt.x > (rcBounds.right - DEFAULT_MARGIN * 2) )
		{
			SetCursor(m_MouseType);
			return;
		}

		P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[m_lSelectedIndex];
		CTime tmStart = pItem->tmStartTime;
		CTime tmEnd   = pItem->tmEndTime  ;

		if(IsVisibleTime(tmStart.GetHour()))
		{
			int nPosY = m_nRulerPos[tmStart.GetHour()][0] + m_nOneHourHeight*tmStart.GetMinute()/60;

			if(pt.y >= nPosY-2 && pt.y <= nPosY+2)
			{
				m_MouseType = MOUSE_OVER_TOP;
				SetCursor(m_MouseType);
				return;
			}
		}

		if(IsVisibleTime(tmEnd.GetHour()))
		{
			int nPosY = m_nRulerPos[tmEnd.GetHour()][0] + m_nOneHourHeight*tmEnd.GetMinute()/60;

			if(pt.y >= nPosY-2 && pt.y <= nPosY+2)
			{
				m_MouseType = MOUSE_OVER_BOTTOM;
				SetCursor(m_MouseType);
				return;
			}
		}

		CRect rcBox[2];
		int nDrawCnt = 1;
		if(GetPlanRect(tmStart, tmEnd, rcBounds, nDrawCnt, rcBox))
		{
			for(int j=0; j<nDrawCnt ;j++)
			{
				if(rcBox[j].PtInRect(pt))
				{
					m_MouseType = MOUSE_OVER_CENTER;
					SetCursor(m_MouseType);
					return;
				}
			}
		}

		SetCursor(m_MouseType);
		return;
	}

	// PLAN 이동 및 크기조절
	P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[m_lSelectedIndex];
	pItem->tmStartTime;
	pItem->tmEndTime  ;

	int nDiffMinute = (pt.y - m_pointStart.y) / (m_nOneHourHeight / 60.);
//	m_pointStart.y = pt.y;

	if(MOUSE_SIZING_TOP == m_MouseType)
	{
		pItem->tmStartTime = m_stSizing.tmStartTime + CTimeSpan(0, 0, nDiffMinute, 0);
		pItem->tmStartTime = GetNearTime(pItem->tmStartTime, m_lSelectedIndex);
	}
	else if(MOUSE_SIZING_BOTTOM == m_MouseType)
	{
		pItem->tmEndTime   = m_stSizing.tmEndTime   + CTimeSpan(0, 0, nDiffMinute, 0);
		pItem->tmEndTime   = GetNearTime(pItem->tmEndTime, m_lSelectedIndex);
	}
	else if(MOUSE_SIZING_CENTER == m_MouseType)
	{
		CTime tmNewStartTime = GetNearTime(m_stSizing.tmStartTime + CTimeSpan(0, 0, nDiffMinute, 0), m_lSelectedIndex);
		CTime tmNewEndTime   = GetNearTime(m_stSizing.tmEndTime   + CTimeSpan(0, 0, nDiffMinute, 0), m_lSelectedIndex);

		if(nDiffMinute < 0)
		{
			pItem->tmStartTime = m_stSizing.tmStartTime + CTimeSpan(0, 0, nDiffMinute, 0);
			pItem->tmStartTime = GetNearTime(pItem->tmStartTime, m_lSelectedIndex);

			pItem->tmEndTime   = m_stSizing.tmEndTime + (pItem->tmStartTime - m_stSizing.tmStartTime);
		}
		else
		{
			pItem->tmEndTime   = m_stSizing.tmEndTime + CTimeSpan(0, 0, nDiffMinute, 0);
			pItem->tmEndTime   = GetNearTime(pItem->tmEndTime, m_lSelectedIndex);

			pItem->tmStartTime = m_stSizing.tmStartTime + (pItem->tmEndTime - m_stSizing.tmEndTime);
		}
//		pItem->tmStartTime = m_stSizing.tmStartTime + CTimeSpan(0, 0, nDiffMinute, 0);
//		pItem->tmEndTime   = m_stSizing.tmEndTime   + CTimeSpan(0, 0, nDiffMinute, 0);
	}

	static int nTick = GetTickCount();
	if(GetTickCount() - nTick > 50)
	{
		nTick = GetTickCount();
		InfoChange(m_lSelectedIndex, pItem->tmStartTime.GetTime(), pItem->tmEndTime.GetTime());
	}

	//Invalidate();
	Refresh();
}

CTime CUbcDailyPlanTableCtrl::GetNearTime(CTime tmValue, int nIndex/*=-1*/)
{
	if(m_nMagneticMove <= 0) return tmValue;

	tmValue = NormalizeTime(tmValue);

	CTime tmNearTime = tmValue + CTimeSpan(0, 12, 0, 0);
	int   nNearDiff  = abs((int)(tmValue - tmNearTime).GetTotalMinutes());

	bool bFindIt = false;
	for(int i=0; i<m_aPlanItemList.GetCount() ;i++)
	{
		if(nIndex == i) continue;

		P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[i];
		CTime tmStart = NormalizeTime(pItem->tmStartTime);
		CTime tmEnd   = NormalizeTime(pItem->tmEndTime  );

		int nStartDiff = abs((int)(tmValue - tmStart).GetTotalMinutes());
		int nEndDiff   = abs((int)(tmValue - tmEnd  ).GetTotalMinutes());

		if( nStartDiff != 0 && nStartDiff <= m_nMagneticMove )
		{
			bFindIt = true;

			if( nStartDiff < nNearDiff )
			{
				nNearDiff = nStartDiff;
				tmNearTime = pItem->tmStartTime;
			}
		}
		if( nEndDiff != 0 && nEndDiff <= m_nMagneticMove )
		{
			bFindIt = true;

			if( nEndDiff < nNearDiff )
			{
				nNearDiff = nEndDiff;
				tmNearTime = pItem->tmEndTime;
			}
		}
	}

	int nRulerDiffMin = abs((int)(tmValue - NormalizeTime(tmValue.GetHour(), tmValue.GetMinute()/10*10)).GetTotalMinutes());
	int nRulerDiffMax = abs((int)(tmValue - NormalizeTime(tmValue.GetHour(), ((tmValue.GetMinute()/10)+1)*10)).GetTotalMinutes());

	if( nNearDiff > nRulerDiffMin &&
		nNearDiff > nRulerDiffMax )
	{
		bFindIt = true;
		if( nRulerDiffMin < nRulerDiffMax )
		{
			tmNearTime = NormalizeTime(tmValue.GetHour(), tmValue.GetMinute()/10*10);
		}
		else
		{
			tmNearTime = NormalizeTime(tmValue.GetHour(), ((tmValue.GetMinute()/10)+1)*10);
		}
	}

	return (bFindIt ? tmNearTime : tmValue);
}

CTime CUbcDailyPlanTableCtrl::NormalizeTime(CTime tmValue)
{
	return NormalizeTime(tmValue.GetHour(), tmValue.GetMinute());
}

CTime CUbcDailyPlanTableCtrl::NormalizeTime(int nHour, int nMinute)
{
	CTime tmCurTime = CTime::GetCurrentTime();
	return CTime( tmCurTime.GetYear()
				, tmCurTime.GetMonth()
				, tmCurTime.GetDay()
				, 0, 0, 0 )
				+ CTimeSpan(0, nHour, nMinute, 0);
}

BOOL CUbcDailyPlanTableCtrl::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(	SetCursor(m_MouseType) ) return TRUE;

	return COleControl::OnSetCursor(pWnd, nHitTest, message);
}

bool CUbcDailyPlanTableCtrl::SetCursor(MOUSE_TYPE mouseType)
{
	switch(mouseType)
	{
	case MOUSE_SIZING_BOTTOM:
	case MOUSE_OVER_BOTTOM:
	case MOUSE_SIZING_TOP:
	case MOUSE_OVER_TOP:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZENS));
		return TRUE;
		break;
	case MOUSE_SIZING_CENTER:
	case MOUSE_OVER_CENTER:
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
		return TRUE;
		break;
	default:
		break;
	}

	return FALSE;
}


VARIANT_BOOL CUbcDailyPlanTableCtrl::SetZOrder(LONG nCurIndex, LONG nNewIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(nCurIndex < 0 || nCurIndex >= m_aPlanItemList.GetCount())
	{
		return VARIANT_FALSE;
	}

	if(nNewIndex < 0) nNewIndex = 0;
	if(nNewIndex >= m_aPlanItemList.GetCount()) nNewIndex = m_aPlanItemList.GetCount();

	if(nCurIndex == nNewIndex)
	{
		return VARIANT_TRUE;
	}

	P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[nCurIndex];
	P_PLAN_ITEM pNewItem = new ST_PLAN_ITEM;
	*pNewItem = *pItem;

	m_aPlanItemList.RemoveAt(nCurIndex);

	// 맨뒤에 추가
	if(nNewIndex >= m_aPlanItemList.GetCount())
	{
		m_aPlanItemList.Add(pNewItem);
	}
	else
	{
		m_aPlanItemList.InsertAt(nNewIndex, pNewItem);
	}

	Refresh();

	return VARIANT_TRUE;
}

void CUbcDailyPlanTableCtrl::SetItemColor(LONG nIndex, VARIANT_BOOL bGradient, OLE_COLOR clrColor)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if(nIndex < 0 || nIndex >= m_aPlanItemList.GetCount())
	{
		return;
	}

	P_PLAN_ITEM pItem = (P_PLAN_ITEM)m_aPlanItemList[nIndex];
	pItem->bGradient = bGradient;
	pItem->clrColor  = clrColor ;

	Refresh();
}
