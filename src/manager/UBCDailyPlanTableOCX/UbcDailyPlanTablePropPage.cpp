// UbcDailyPlanTablePropPage.cpp : CUbcDailyPlanTablePropPage 속성 페이지 클래스의 구현입니다.

#include "stdafx.h"
#include "UbcDailyPlanTable.h"
#include "UbcDailyPlanTablePropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CUbcDailyPlanTablePropPage, COlePropertyPage)



// 메시지 맵입니다.

BEGIN_MESSAGE_MAP(CUbcDailyPlanTablePropPage, COlePropertyPage)
END_MESSAGE_MAP()



// 클래스 팩터리와 GUID를 초기화합니다.

IMPLEMENT_OLECREATE_EX(CUbcDailyPlanTablePropPage, "SQI.UbcDailyPlanTaPropPage.1",
	0x9cbf8a06, 0xc8b2, 0x410e, 0xb0, 0xaf, 0x71, 0xd6, 0xa0, 0xc7, 0xa7, 0xe4)



// CUbcDailyPlanTablePropPage::CUbcDailyPlanTablePropPageFactory::UpdateRegistry -
// CUbcDailyPlanTablePropPage에서 시스템 레지스트리 항목을 추가하거나 제거합니다.

BOOL CUbcDailyPlanTablePropPage::CUbcDailyPlanTablePropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_UBCDAILYPLANTABLE_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// CUbcDailyPlanTablePropPage::CUbcDailyPlanTablePropPage - 생성자입니다.

CUbcDailyPlanTablePropPage::CUbcDailyPlanTablePropPage() :
	COlePropertyPage(IDD, IDS_UBCDAILYPLANTABLE_PPG_CAPTION)
{
}



// CUbcDailyPlanTablePropPage::DoDataExchange - 페이지와 속성 사이에서 데이터를 이동시킵니다.

void CUbcDailyPlanTablePropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// CUbcDailyPlanTablePropPage 메시지 처리기입니다.
