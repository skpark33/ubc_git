//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SerialGateway.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_SERIALGATEWAY_FORM          101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_SerialGatewayTYPE           129
#define IDD_SHORTCUTKEY_SETTINGS        130
#define IDC_LIST_LOG                    1000
#define IDC_EDIT_OPEN                   1001
#define IDC_EDIT_SHORT_1                1002
#define IDC_EDIT_SHORT_2                1003
#define IDC_EDIT_SHORT_3                1004
#define IDC_EDIT_SHORT_12               1005
#define IDC_EDIT_SHORT_13               1006
#define IDC_EDIT_SHORT_23               1007
#define IDC_EDIT_SHORT_123              1008
#define IDC_CHECK1                      1009
#define IDC_CHECK_SEND_SAME_SHORTCUTKEY 1009
#define ID_SEND_INIT                    32771
#define ID_32772                        32772
#define ID_SEND_TEST                    32773
#define ID_32774                        32774
#define ID_SHORTCUTKEY_SETTINGS         32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
