// SerialGatewayView.h : CSerialGatewayView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"
#include "afxmt.h"
#include "afxtempl.h"

class CSerialMFC;


////////////////////////////////////////////////////////////////////////////////

class CSerialGatewayView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CSerialGatewayView();
	DECLARE_DYNCREATE(CSerialGatewayView)

public:
	enum{ IDD = IDD_SERIALGATEWAY_FORM };

// 특성입니다.
public:
	CSerialGatewayDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CSerialGatewayView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:

	static	int m_nComPort;
	static	bool m_bTypePushbutton;

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSendInit();
	afx_msg void OnSendTest();
	afx_msg LRESULT OnSerialMsg (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReceiveData(WPARAM wParam, LPARAM lParam);

	CListCtrl m_lcLog;

	CByteArray			m_arrData;
	bool				m_bRunThread;
	DWORD				m_dwLastUpdateTick;
	CCriticalSection	m_csData;
	CWinThread*			m_pThread;

protected:
	CSerialMFC*		m_pSerial;
	ULONGLONG		m_nColumnCount;

	CString			m_strShortcutKey[8];
	int				m_bSendSameShortcutKey;
	void			RefreshShortcutKey();

	void	InsertItem(LPBYTE pData, DWORD dwSize, LPCTSTR lpszStatus);
	void	CheckListCount();

public:
	afx_msg void OnShortcutkeySettings();
};

#ifndef _DEBUG  // SerialGatewayView.cpp의 디버그 버전
inline CSerialGatewayDoc* CSerialGatewayView::GetDocument() const
   { return reinterpret_cast<CSerialGatewayDoc*>(m_pDocument); }
#endif

static UINT CheckSerialDataThread(LPVOID pParam);