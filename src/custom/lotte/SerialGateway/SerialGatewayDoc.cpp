// SerialGatewayDoc.cpp : CSerialGatewayDoc 클래스의 구현
//

#include "stdafx.h"
#include "SerialGateway.h"

#include "SerialGatewayDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSerialGatewayDoc

IMPLEMENT_DYNCREATE(CSerialGatewayDoc, CDocument)

BEGIN_MESSAGE_MAP(CSerialGatewayDoc, CDocument)
END_MESSAGE_MAP()


// CSerialGatewayDoc 생성/소멸

CSerialGatewayDoc::CSerialGatewayDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CSerialGatewayDoc::~CSerialGatewayDoc()
{
}

BOOL CSerialGatewayDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CSerialGatewayDoc serialization

void CSerialGatewayDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CSerialGatewayDoc 진단

#ifdef _DEBUG
void CSerialGatewayDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSerialGatewayDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CSerialGatewayDoc 명령
