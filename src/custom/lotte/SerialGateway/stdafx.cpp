// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// SerialGateway.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

#include <Tlhelp32.h>

unsigned long getPid(const char* exename)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) return 0;

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	if ( !Process32First( hSnapshot, &pe32 ) )
	{
		::CloseHandle(hSnapshot);
		return 0;
	}

	do
	{
		//char strProcessName[512];
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(pe32.szExeFile, exename) == 0 )
		{
			::CloseHandle(hSnapshot);
			return pe32.th32ProcessID;
		}
	} while( Process32Next( hSnapshot, &pe32 ) );

	::CloseHandle(hSnapshot);
	return 0;
}

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if( !IsWindowVisible(hwnd) ) return TRUE;

	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);

	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;

	if( pe->pid != pid ) return TRUE;

	pe->hwnd = hwnd;
	return FALSE;
}

HWND getWHandle(unsigned long pid)
{
	if( pid>0 )
	{
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);

		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1)
		{
			parent =  GetParent(child);
			if(!parent) return child;
			child = parent;
		}
		return e.hwnd;
	}
	return (HWND)0;
}

LPCTSTR GetINIPath()
{
	static CString	_iniDirectory = _T("");

	if(_iniDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_iniDirectory = str;

		CString app;
		app.LoadString(AFX_IDS_APP_TITLE);

		_iniDirectory.Append(app);
		_iniDirectory.Append(_T(".ini"));
	}

	return _iniDirectory;
}
