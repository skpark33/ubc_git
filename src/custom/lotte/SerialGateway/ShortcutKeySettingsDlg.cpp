// ShortcutKeySettingsDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "SerialGateway.h"
#include "ShortcutKeySettingsDlg.h"


// CShortcutKeySettingsDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CShortcutKeySettingsDlg, CDialog)

CShortcutKeySettingsDlg::CShortcutKeySettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CShortcutKeySettingsDlg::IDD, pParent)
{

}

CShortcutKeySettingsDlg::~CShortcutKeySettingsDlg()
{
}

void CShortcutKeySettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_OPEN, m_editOpen);
	DDX_Control(pDX, IDC_EDIT_SHORT_1, m_editShort1);
	DDX_Control(pDX, IDC_EDIT_SHORT_2, m_editShort2);
	DDX_Control(pDX, IDC_EDIT_SHORT_3, m_editShort3);
	DDX_Control(pDX, IDC_EDIT_SHORT_12, m_editShort12);
	DDX_Control(pDX, IDC_EDIT_SHORT_13, m_editShort13);
	DDX_Control(pDX, IDC_EDIT_SHORT_23, m_editShort23);
	DDX_Control(pDX, IDC_EDIT_SHORT_123, m_editShort123);
	DDX_Control(pDX, IDC_CHECK_SEND_SAME_SHORTCUTKEY, m_chkSendSameShortcutKey);
}


BEGIN_MESSAGE_MAP(CShortcutKeySettingsDlg, CDialog)
END_MESSAGE_MAP()


// CShortcutKeySettingsDlg 메시지 처리기입니다.

BOOL CShortcutKeySettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	char buf[1024+1];

	::GetPrivateProfileString("ShortcutKey", "Open", "Ctrl+i", buf, 1024, ::GetINIPath());
	m_editOpen.SetWindowText(buf);

	::GetPrivateProfileString("ShortcutKey", "Short1", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_editShort1.SetWindowText(buf);

	::GetPrivateProfileString("ShortcutKey", "Short2", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_editShort2.SetWindowText(buf);

	::GetPrivateProfileString("ShortcutKey", "Short3", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_editShort3.SetWindowText(buf);

	::GetPrivateProfileString("ShortcutKey", "Short12", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_editShort12.SetWindowText(buf);

	::GetPrivateProfileString("ShortcutKey", "Short13", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_editShort13.SetWindowText(buf);

	::GetPrivateProfileString("ShortcutKey", "Short23", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_editShort23.SetWindowText(buf);

	::GetPrivateProfileString("ShortcutKey", "Short123", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_editShort123.SetWindowText(buf);

	::GetPrivateProfileString("ShortcutKey", "SendSameShortcutKey", "0", buf, 1024, ::GetINIPath());
	m_chkSendSameShortcutKey.SetCheck( atoi(buf) ? BS_CHECKBOX : BST_UNCHECKED );

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CShortcutKeySettingsDlg::OnOK()
{
	m_editOpen.GetWindowText(m_strShortcutKeyOpen);
	m_editShort1.GetWindowText(m_strShortcutKeyShort1);
	m_editShort2.GetWindowText(m_strShortcutKeyShort2);
	m_editShort3.GetWindowText(m_strShortcutKeyShort3);
	m_editShort12.GetWindowText(m_strShortcutKeyShort12);
	m_editShort13.GetWindowText(m_strShortcutKeyShort13);
	m_editShort23.GetWindowText(m_strShortcutKeyShort23);
	m_editShort123.GetWindowText(m_strShortcutKeyShort123);

	::WritePrivateProfileString("ShortcutKey", "Open", m_strShortcutKeyOpen, ::GetINIPath());
	::WritePrivateProfileString("ShortcutKey", "Short1", m_strShortcutKeyShort1, ::GetINIPath());
	::WritePrivateProfileString("ShortcutKey", "Short2", m_strShortcutKeyShort2, ::GetINIPath());
	::WritePrivateProfileString("ShortcutKey", "Short3", m_strShortcutKeyShort3, ::GetINIPath());
	::WritePrivateProfileString("ShortcutKey", "Short12", m_strShortcutKeyShort12, ::GetINIPath());
	::WritePrivateProfileString("ShortcutKey", "Short13", m_strShortcutKeyShort13, ::GetINIPath());
	::WritePrivateProfileString("ShortcutKey", "Short23", m_strShortcutKeyShort23, ::GetINIPath());
	::WritePrivateProfileString("ShortcutKey", "Short123", m_strShortcutKeyShort123, ::GetINIPath());

	int check = m_chkSendSameShortcutKey.GetCheck();
	::WritePrivateProfileString("ShortcutKey", "SendSameShortcutKey", (check ? "1" : "0"), ::GetINIPath());

	CDialog::OnOK();
}
