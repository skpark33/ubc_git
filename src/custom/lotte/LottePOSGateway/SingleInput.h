#pragma once
#include "afxwin.h"


// CSingleInput 대화 상자입니다.

class CSingleInput : public CDialog
{
	DECLARE_DYNAMIC(CSingleInput)

public:
	CSingleInput(LPCTSTR lpszTitle, LPCTSTR lpszComment, LPCTSTR lpszValue, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSingleInput();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SINGLE_INPUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CString		m_strTitle;
	CString		m_strComment;
	CString		m_strValue;

public:
	CEdit		m_editValue;

	LPCTSTR		GetValue() { return m_strValue; };
	CStatic m_stcComment;
};
