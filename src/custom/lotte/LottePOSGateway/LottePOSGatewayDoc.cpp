// LottePOSGatewayDoc.cpp : CLottePOSGatewayDoc 클래스의 구현
//

#include "stdafx.h"
#include "LottePOSGateway.h"

#include "LottePOSGatewayDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLottePOSGatewayDoc

IMPLEMENT_DYNCREATE(CLottePOSGatewayDoc, CDocument)

BEGIN_MESSAGE_MAP(CLottePOSGatewayDoc, CDocument)
END_MESSAGE_MAP()


// CLottePOSGatewayDoc 생성/소멸

CLottePOSGatewayDoc::CLottePOSGatewayDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CLottePOSGatewayDoc::~CLottePOSGatewayDoc()
{
}

BOOL CLottePOSGatewayDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CLottePOSGatewayDoc serialization

void CLottePOSGatewayDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CLottePOSGatewayDoc 진단

#ifdef _DEBUG
void CLottePOSGatewayDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLottePOSGatewayDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CLottePOSGatewayDoc 명령
