// GWAngelinusSet.h : CGWAngelinusSet 클래스의 구현입니다.



// CGWAngelinusSet 구현입니다.

// 코드 생성 위치 2013년 2월 6일 수요일, 오전 10:29

#include "stdafx.h"
#include "GWAngelinusSet.h"
#include <ODBCINST.H>


IMPLEMENT_DYNAMIC(CGWAngelinusSet, CRecordset)

CGWAngelinusSet::CGWAngelinusSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_data_stream = "";
	m_data_id = "";
	m_data_state = 0;
	m_event_time;
	m_nFields = 4;
	m_nDefaultType = dynaset;
}
//#error Security Issue: The connection string may contain a password
// 아래 연결 문자열에 일반 텍스트 암호 및/또는 
// 다른 중요한 정보가 포함되어 있을 수 있습니다.
// 보안 관련 문제가 있는지 연결 문자열을 검토한 후에 #error을(를) 제거하십시오.
// 다른 형식으로 암호를 저장하거나 다른 사용자 인증을 사용하십시오.
CString CGWAngelinusSet::GetDefaultConnect()
{
	return _T("DSN=Lotte_POS_ODBC; UID=gateway; PWD=dpswpf; DATABASE=gw");
}

CString CGWAngelinusSet::GetDefaultSQL()
{
	return _T("[dbo].[gw_angelinus]");
}

void CGWAngelinusSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 및 RFX_Int() 같은 매크로는 데이터베이스의 필드
// 형식이 아니라 멤버 변수의 형식에 따라 달라집니다.
// ODBC에서는 자동으로 열 값을 요청된 형식으로 변환하려고 합니다
	RFX_Text(pFX, _T("[data_stream]"), m_data_stream, 4096);
	RFX_Text(pFX, _T("[data_id]"), m_data_id);
	RFX_Byte(pFX, _T("[data_state]"), m_data_state);
	RFX_Binary(pFX, _T("[event_time]"), m_event_time);

}
/////////////////////////////////////////////////////////////////////////////
// CGWAngelinusSet 진단

#ifdef _DEBUG
void CGWAngelinusSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CGWAngelinusSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


ULONGLONG CGWAngelinusSet::GetTimeStamp()
{
	ULONGLONG ret_val = 0;

	int count = m_event_time.GetCount();
	for(int i=0; i<count; i++)
	{
		ret_val <<= 8;
		ULONGLONG tmp = (ULONGLONG)m_event_time.GetAt(i);

		ret_val |= tmp;
	}

	return ret_val;
}

bool CGWAngelinusSet::RegisterODBC(LPCTSTR lpszDSN)
{
	TCHAR szDesc[1024] = {0};
	//_stprintf( szDesc, _T("DSN=Lotte_POS_ODBC: Server=211.232.57.185: Database=gw:"));
	_tcscpy(szDesc, lpszDSN);

	int mlen = (int)_tcslen(szDesc);

	for (int i=0; i<mlen; i++)
	{
		if (szDesc[i] == _T(':')) szDesc[i] = _T('\0');
	}

	if (FALSE == SQLConfigDataSource(NULL, ODBC_ADD_DSN, _T("SQL Server\0"), (LPCTSTR)szDesc))
	{
		//AfxMessageBox(_T("데이터베이스 등록 실패 !!!"), MB_ICONSTOP);
		return false;
	}

	return true;
}
