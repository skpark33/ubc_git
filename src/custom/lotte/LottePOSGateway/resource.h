//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by LottePOSGateway.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_LOTTEPOSGATEWAY_FORM        101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_LottePOSGatewayTYPE         129
#define IDD_SINGLE_INPUT                130
#define IDC_LIST_LOG                    1000
#define IDC_EDIT_MESSAGE                1002
#define IDC_EDIT_VALUE                  1002
#define IDC_STATIC_COMMENT              1003
#define ID_EDIT_SEND_MESSAGE            32771
#define ID_32772                        32772
#define ID_EDIT_ODBC                    32773
#define ID_EDIT                         32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
