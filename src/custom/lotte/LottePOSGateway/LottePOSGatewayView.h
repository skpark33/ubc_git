// LottePOSGatewayView.h : CLottePOSGatewayView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"

#include "GWAngelinusSet.h"

#include "ReposControl2.h"


class CLottePOSGatewayView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CLottePOSGatewayView();
	DECLARE_DYNCREATE(CLottePOSGatewayView)

public:
	enum{ IDD = IDD_LOTTEPOSGATEWAY_FORM };

// 특성입니다.
public:
	CLottePOSGatewayDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CLottePOSGatewayView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

	CReposControl2	m_repos;

	CGWAngelinusSet	m_db;

	ULONGLONG	m_nLastTimeStamp;

	ULONGLONG	m_nColumnCount;

	CString		m_strSendMessage;
	CString		m_strODBC_DSN;

public:
	CListCtrl m_lcLog;

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnEditSendMessage();

	BOOL	InitDB();
	BOOL	CheckNewValue();

	void	InsertItem(LPCTSTR lpszDataStream, LPCTSTR lpszDataID, BYTE nDataState, ULONGLONG nEventTime);
	void	CheckListCount();

	afx_msg void OnEditOdbc();
};

#ifndef _DEBUG  // LottePOSGatewayView.cpp의 디버그 버전
inline CLottePOSGatewayDoc* CLottePOSGatewayView::GetDocument() const
   { return reinterpret_cast<CLottePOSGatewayDoc*>(m_pDocument); }
#endif

