// LottePOSGatewayView.cpp : CLottePOSGatewayView 클래스의 구현
//

#include "stdafx.h"
#include "LottePOSGateway.h"

#include "LottePOSGatewayDoc.h"
#include "LottePOSGatewayView.h"

#include "SingleInput.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		CHECK_DB_TIMER_ID				(1025)
#define		CHECK_DB_TIMER_TIME				(1000)	// every-1 sec

#define		SEND_MESSAGE_TIMER_ID			(1026)
#define		SEND_MESSAGE_TIMER_TIME			(2000)	// 1 sec

#define		DEFAULT_MESSAGE_STRING		_T("CTRL+i")
#ifdef DEBUG
	#define		DEFAULT_ODBC_DSN			_T("DSN=Lotte_POS_ODBC: Server=211.232.57.185: Database=gw:")
#else
	#define		DEFAULT_ODBC_DSN			_T("DSN=Lotte_POS_ODBC: Server=127.0.0.1: Database=gw:")
#endif

// CLottePOSGatewayView

IMPLEMENT_DYNCREATE(CLottePOSGatewayView, CFormView)

BEGIN_MESSAGE_MAP(CLottePOSGatewayView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_COMMAND(ID_EDIT_SEND_MESSAGE, &CLottePOSGatewayView::OnEditSendMessage)
	ON_COMMAND(ID_EDIT_ODBC, &CLottePOSGatewayView::OnEditOdbc)
END_MESSAGE_MAP()

// CLottePOSGatewayView 생성/소멸

CLottePOSGatewayView::CLottePOSGatewayView()
	: CFormView(CLottePOSGatewayView::IDD)
{
	m_nLastTimeStamp = 0;
	m_nColumnCount = 0;
}

CLottePOSGatewayView::~CLottePOSGatewayView()
{
}

void CLottePOSGatewayView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
}

BOOL CLottePOSGatewayView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CLottePOSGatewayView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//
	m_repos.SetParent(this);
	m_repos.AddControl((CWnd*)&m_lcLog, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	//
	m_lcLog.SetExtendedStyle(m_lcLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcLog.InsertColumn(0, _T("Count"), LVCFMT_RIGHT, 0);
	m_lcLog.InsertColumn(1, _T("Time"), 0, 120);
	m_lcLog.InsertColumn(2, _T("Data Stream"), 0, 300);
	m_lcLog.InsertColumn(3, _T("Data ID"), 0, 0);
	m_lcLog.InsertColumn(4, _T("Data State"), LVCFMT_RIGHT, 0);
	m_lcLog.InsertColumn(5, _T("Event Time"), 0, 150);

	//
	TCHAR buf[1024] = {0};
	GetPrivateProfileString(_T("LOTTE"), _T("POS_MESSAGE"), DEFAULT_MESSAGE_STRING, buf, 1024, _T("data\\UBCVariables.ini"));
	m_strSendMessage = buf;

	::ZeroMemory(buf, sizeof(buf));
	GetPrivateProfileString(_T("LOTTE"), _T("POS_ODBC"), DEFAULT_ODBC_DSN, buf, 1024, _T("data\\UBCVariables.ini"));
	m_strODBC_DSN = buf;

	//
	if( !m_db.RegisterODBC(m_strODBC_DSN) )
	{
		InsertItem("ERROR : Register ODBC Error", "", 0, 0);
	}

	//
	SetTimer(CHECK_DB_TIMER_ID, CHECK_DB_TIMER_TIME, NULL);
}


// CLottePOSGatewayView 진단

#ifdef _DEBUG
void CLottePOSGatewayView::AssertValid() const
{
	CFormView::AssertValid();
}

void CLottePOSGatewayView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CLottePOSGatewayDoc* CLottePOSGatewayView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLottePOSGatewayDoc)));
	return (CLottePOSGatewayDoc*)m_pDocument;
}
#endif //_DEBUG


// CLottePOSGatewayView 메시지 처리기

void CLottePOSGatewayView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if(GetSafeHwnd())
	{
		m_repos.MoveControl();
	}
}

#include <Tlhelp32.h>
//#include <shlwapi.h>
//#include <winternl.h>
//#include <shellapi.h>				//for use ShellExcute
#include <io.h>						//for use _access

unsigned long getPid(const char* exename)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) return 0;

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	if ( !Process32First( hSnapshot, &pe32 ) )
	{
		::CloseHandle(hSnapshot);
		return 0;
	}

	do
	{
		//char strProcessName[512];
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(pe32.szExeFile, exename) == 0 )
		{
			::CloseHandle(hSnapshot);
			return pe32.th32ProcessID;
		}
	} while( Process32Next( hSnapshot, &pe32 ) );

	::CloseHandle(hSnapshot);
	return 0;
}

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if( !IsWindowVisible(hwnd) ) return TRUE;

	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);

	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;

	if( pe->pid != pid ) return TRUE;

	pe->hwnd = hwnd;
	return FALSE;
}

HWND getWHandle(unsigned long pid)
{
	if( pid>0 )
	{
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);

		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1)
		{
			parent =  GetParent(child);
			if(!parent) return child;
			child = parent;
		}
		return e.hwnd;
	}
	return (HWND)0;
}

void CLottePOSGatewayView::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case CHECK_DB_TIMER_ID:
		KillTimer(CHECK_DB_TIMER_ID);

		if( InitDB() )
		{
			if( CheckNewValue() )
			{
				InsertItem(m_db.m_data_stream, m_db.m_data_id, m_db.m_data_state, m_db.GetTimeStamp());

				SetTimer(SEND_MESSAGE_TIMER_ID, SEND_MESSAGE_TIMER_TIME, NULL);
			}
		}

		SetTimer(CHECK_DB_TIMER_ID, CHECK_DB_TIMER_TIME, NULL);
		break;

	case SEND_MESSAGE_TIMER_ID:
		KillTimer(SEND_MESSAGE_TIMER_ID);
		{
			HWND brwHwnd = getWHandle(getPid(_T("UTV_brwClient2.exe")));
			if(brwHwnd)
			{
				COPYDATASTRUCT appInfo;
				appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
				appInfo.lpData = (LPVOID)(LPCSTR)m_strSendMessage;
				appInfo.cbData = m_strSendMessage.GetLength() + 1;
				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
			}
			else
			{
				InsertItem("WARNING : UTV_brwClient2.exe is not in running !!!", "", 0, 0 );
			}
		}

		break;
	}

	CFormView::OnTimer(nIDEvent);
}

void CLottePOSGatewayView::OnEditSendMessage()
{
	CSingleInput dlg(_T("전송 문자열 수정"), _T("전송 문자열"), m_strSendMessage);
	if( dlg.DoModal() == IDOK )
	{
		m_strSendMessage = dlg.GetValue();
		WritePrivateProfileString(_T("LOTTE"), _T("POS_MESSAGE"), m_strSendMessage, _T("data\\UBCVariables.ini"));
	}
}

void CLottePOSGatewayView::OnEditOdbc()
{
	CSingleInput dlg(_T("ODBC 접속정보 수정"), _T("ODBC 접속정보"), m_strODBC_DSN);
	if( dlg.DoModal() == IDOK )
	{
		m_strODBC_DSN = dlg.GetValue();

		WritePrivateProfileString(_T("LOTTE"), _T("POS_ODBC"), m_strODBC_DSN, _T("data\\UBCVariables.ini"));

		::AfxMessageBox(_T("다음 실행부터 적용됩니다.\r\n프로그램을 종료후 재실행주시기 바랍니다."), MB_ICONWARNING);
	}
}

BOOL CLottePOSGatewayView::InitDB()
{
	if(m_db.IsOpen()) return TRUE;

	TRY
	{
		if(m_db.Open()) return TRUE;
	}
	CATCH(CDBException, ex)
	{
		CString msg = _T("Exception in DB-Open : ");
		msg += ex->m_strError;

		InsertItem(msg, ex->m_strStateNativeOrigin, 0, 0);
		return FALSE;
	}
	CATCH(CMemoryException, ex)
	{
		TCHAR szMsg[1024] = _T("Exception in DB-Open : ");
		ex->GetErrorMessage(szMsg+23, sizeof(szMsg)-23);

		InsertItem(szMsg, "", 0, 0);
		return FALSE;
	}
	END_CATCH

	InsertItem("ERROR : DB-Open Error", "", 0, 0);

	return FALSE;
}

BOOL CLottePOSGatewayView::CheckNewValue()
{
	if( !m_db.IsOpen() ) return FALSE;

	m_db.m_strFilter = _T("data_stream = '보류'");
	m_db.m_strSort = _T("event_time desc");

	TRY
	{
		if( !m_db.Requery() ) return FALSE;
	}
	CATCH(CDBException, ex)
	{
		CString msg = _T("Exception in Requery : ");
		msg += ex->m_strError;

		InsertItem(msg, ex->m_strStateNativeOrigin, 0, 0);
		return FALSE;
	}
	CATCH(CMemoryException, ex)
	{
		TCHAR szMsg[1024] = _T("Exception in Requery : ");
		ex->GetErrorMessage(szMsg+23, sizeof(szMsg)-23);

		InsertItem(szMsg, "", 0, 0);
		return FALSE;
	}
	END_CATCH

	TRY
	{
		if( m_db.IsEOF() ) return FALSE;
	}
	CATCH(CDBException, ex)
	{
		CString msg = _T("Exception in IsEOF : ");
		msg += ex->m_strError;

		InsertItem(msg, ex->m_strStateNativeOrigin, 0, 0);
		return FALSE;
	}
	CATCH(CMemoryException, ex)
	{
		TCHAR szMsg[1024] = _T("Exception in IsEOF : ");
		ex->GetErrorMessage(szMsg+21, sizeof(szMsg)-21);

		InsertItem(szMsg, "", 0, 0);
		return FALSE;
	}
	END_CATCH

	ULONGLONG last_time_stamp = m_db.GetTimeStamp();

	if( m_nLastTimeStamp >= last_time_stamp ) return FALSE;

	m_nLastTimeStamp = last_time_stamp;

	return TRUE;
}

void CLottePOSGatewayView::InsertItem(LPCTSTR lpszDataStream, LPCTSTR lpszDataID, BYTE nDataState, ULONGLONG nEventTime)
{
	TCHAR buf[1024];

	_stprintf(buf, _T("%I64u"), m_nColumnCount);
	m_lcLog.InsertItem(0, buf);

	m_lcLog.SetItemText(0, 1, CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S"));
	if(lpszDataStream) m_lcLog.SetItemText(0, 2, lpszDataStream);
	if(lpszDataID) m_lcLog.SetItemText(0, 3, lpszDataID);

	_stprintf(buf, _T("%u"), nDataState);
	m_lcLog.SetItemText(0, 4, buf);

	if(nEventTime != 0)
	{
		_stprintf(buf, _T("%020I64X"), nEventTime);
		m_lcLog.SetItemText(0, 5, buf);
	}

	m_nColumnCount++;

	CheckListCount();
}

void CLottePOSGatewayView::CheckListCount()
{
	int count = m_lcLog.GetItemCount();
	if(count > 1000)
	{
		m_lcLog.SetRedraw(FALSE);
		for(int i=count-1; i>900; i--)
		{
			m_lcLog.DeleteItem(i);
		}
		m_lcLog.SetRedraw(TRUE);
	}
}
