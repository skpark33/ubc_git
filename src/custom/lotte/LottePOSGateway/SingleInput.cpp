// SingleInput.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LottePOSGateway.h"
#include "SingleInput.h"


// CSingleInput 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSingleInput, CDialog)

CSingleInput::CSingleInput(LPCTSTR lpszTitle, LPCTSTR lpszComment, LPCTSTR lpszValue, CWnd* pParent /*=NULL*/)
	: CDialog(CSingleInput::IDD, pParent)
	, m_strTitle (lpszTitle)
	, m_strComment (lpszComment)
	, m_strValue (lpszValue)
{

}

CSingleInput::~CSingleInput()
{
}

void CSingleInput::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_VALUE, m_editValue);
	DDX_Control(pDX, IDC_STATIC_COMMENT, m_stcComment);
}


BEGIN_MESSAGE_MAP(CSingleInput, CDialog)
END_MESSAGE_MAP()


// CSingleInput 메시지 처리기입니다.

BOOL CSingleInput::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(m_strTitle);
	m_stcComment.SetWindowText(m_strComment);
	m_editValue.SetWindowText(m_strValue);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSingleInput::OnOK()
{
	m_editValue.GetWindowText(m_strValue);

	CDialog::OnOK();
}
