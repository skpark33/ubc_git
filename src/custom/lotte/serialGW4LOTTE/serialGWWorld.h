 /*! \file serialGWWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _serialGWWorld_h_
#define _serialGWWorld_h_


#include <ci/libWorld/ciWorld.h> 
#include <ci/libThread/ciMutex.h> 
#include <ci/libBase/ciListType.h> 
#include <serialGW/serialSocketAcceptor.h>
#include "libSerial/serialListener.h"
#include "serialThread.h"
#include "serialUDPThread.h"

class serialGWWorld : public ciWorld {
public:
	serialGWWorld();
	~serialGWWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

	ciBoolean accept();
	ciBoolean propagateIP();

protected:	

	static int test1(char* arg);

	serialThread*	_serialThread;
	serialUDPThread*	_serialUDPThread;
	serialSocketAcceptor	_acceptor;

#if defined(_FOOD_COURT_) || defined(_KIA_)
	serialDatacheckThread*	_serialDatacheckThread;
#endif
};
		
#endif //_serialGWWorld_h_
