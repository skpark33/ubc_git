 /*! \file serialThread.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _serialThread_h_
#define _serialThread_h_

#include <ci/libThread/ciThread.h> 
#include "serialImpl.h"

class serialThread : public ciThread {
public:
	serialThread();
	virtual ~serialThread();

	int		comPort;

	virtual void run();
 
	serialImpl* GetSerialHandle() { return _listener; };

protected:	
	serialImpl* _listener;


};

#if defined(_FOOD_COURT_) || defined(_KIA_)
class serialDatacheckThread : public ciThread {
public:
	serialDatacheckThread();
	virtual ~serialDatacheckThread();

	int		typePushbutton;

	virtual void run();

	void setSerialHandle(serialImpl* handle) { _listener=handle; };

protected:
	serialImpl* _listener;

};
#endif

#endif //_serialThread_h_
