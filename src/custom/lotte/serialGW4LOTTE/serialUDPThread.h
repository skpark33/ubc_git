 /*! \file serialUDPThread.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _serialUDPThread_h_
#define _serialUDPThread_h_

#include <ci/libThread/ciThread.h> 

class serialUDPThread : public ciThread {
public:
	serialUDPThread(int port);
	virtual ~serialUDPThread();

	virtual void run();
protected:
	int _port;
};
		
#endif //_serialUDPThread_h_
