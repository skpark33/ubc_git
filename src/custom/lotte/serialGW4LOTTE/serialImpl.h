 /*! \file serialImpl.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _serialImpl_h_
#define _serialImpl_h_

#include "libSerial/BinSerialListener.h"

class serialQ {
public:
	serialQ(const char* name) : _name(name) { _lastTime = time(NULL); };
	virtual ~serialQ() {};

	void			putQ(char* buf);
	ciBoolean		getQ(ciString& buf);

	const char*	getName() { return _name.c_str(); }
	time_t		getLastTime() { return _lastTime;}
	void		setLastTime(time_t pTime) { _lastTime = pTime; }

protected:
	ciMutex			_listLock;
	ciStringList	_list;
	ciString		_name;
	time_t			_lastTime;
};
typedef map<ciString, serialQ*>	serialQMap;


class serialQManager {
public:
	static serialQManager*	getInstance();
	static void	clearInstance();

	virtual ~serialQManager() { clear(); };

	void			clear();
	void			garbageCollect(int limit);
	ciBoolean		newQ(const char* name);

	void			putQ(char* buf);
	ciBoolean		getQ(const char* name, ciString& buf);

protected:
	serialQManager() {};

	static serialQManager*	_instance;
	static ciMutex			_instanceLock;

	serialQMap	_map;
	ciMutex			_mapLock;
};

#ifdef _FOOD_COURT_
class numberItem
{
public:
	numberItem() { _number=0; _time=time(NULL); };
	virtual ~numberItem() {};

	int				_number;
	time_t			_time;
};

typedef std::list<numberItem*> numberItemList;
#endif

class serialImpl : public CBinSerialListener {
public:
	virtual bool svc(const char* msg, int size=-1);

	ciMutex			_listLock;

#ifdef _FOOD_COURT_
	numberItemList	_numberList;
	void			sendNumberlist();
#endif

	void	Start();
	void	Stop();

	DWORD	_lastestUpdateTime;
};

#endif //_serialImpl_h_
