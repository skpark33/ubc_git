// TopmostMenuDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "HoverButton.h"
#include "ximage.h"
#include "SlideMenuDlg.h"

#define SLIDE_TOGGLE		1001
#define SLIDE_LEFT_START	1002
#define SLIDE_LEFT_MOVE		1003
#define SLIDE_LEFT_STOP		1004
#define SLIDE_RIGHT_START	1005
#define SLIDE_RIGHT_MOVE	1006
#define SLIDE_RIGHT_STOP	1007

// CTopmostMenuDlg 대화 상자
class CTopmostMenuDlg : public CDialog
{
// 생성입니다.
public:
	CTopmostMenuDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	virtual ~CTopmostMenuDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TOPMOSTMENU_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.
	afx_msg HCURSOR OnQueryDragIcon();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
	virtual void OnCancel();
	afx_msg void OnBnClickedShow();
	afx_msg void OnBnClickedSet();
	afx_msg void OnBnClickedPlay();
	afx_msg void OnBnClickedLeft();
	afx_msg void OnBnClickedRight();
	afx_msg void OnBnClickedMainMenu();
	afx_msg LRESULT OnBnClickedSlideMenu(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

// 구현입니다.
protected:
	HICON m_hIcon;

	CString		m_currentKey;

	CBitmap m_bitmapRight;
	CBitmap m_bitmapLeft;

	CHoverButton m_ShowBt;
	CHoverButton m_SetBt;
	CHoverButton m_PlayBt;
	CHoverButton m_btnPreviousTemplate;
	CHoverButton m_btnNextTemplate;
	CHoverButton m_MainMenuBt;

	CSlideMenuDlg *	m_pDlgSlideMenu;

	int m_nWidth;
	int m_nHeight;

	int m_nSlideMode;
	int m_nSlidePos;
	int m_nSlidePosMin;
	int m_nSlidePosMax;
	int m_nSlidePosStep;

	int m_toggle;
	int m_nToggleMainMenu;

	DWORD m_dwActionTick;

	CRITICAL_SECTION gCS;

	void SetTopMost(bool bTopMost);
	BOOL socketAgent(const char* data, const char* ipAddress="127.0.0.1", int portNo=14007);
	BOOL SetXmlStartPage(int nMenu);

	BOOL		_getCurrentKey();
	CString		_getNextKey();
	CString		_getPrevKey();

	void	showTaskbar(bool bShow);

	void Transparent();
	void SlideAction(int nSlideMode, BOOL bInit);
};
