// TopmostMenuDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "TopmostMenu.h"
#include "TopmostMenuDlg.h"
#include "winsock2.h"
#include "MemDC.h"

#define MAX_PAGE				3
#define UNSHOW_TIME				5000
#define ACTION_REPEAT_TIME		500
#define BG_TRANSPARENT_COLOR	RGB(255,0,255)
#define BTN_TRANSPARENT_COLOR	RGB(0,0,0)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CTopmostMenuDlg 대화 상자

CTopmostMenuDlg::CTopmostMenuDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTopmostMenuDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	InitializeCriticalSection(&gCS);

	m_toggle = 1;
	m_nToggleMainMenu = 0;

	m_pDlgSlideMenu = NULL;

	m_nSlideMode = SLIDE_RIGHT_STOP;
	m_nSlidePos = 0;
	m_nSlidePosMin = 34;
	m_nSlidePosMax = 406;
	m_nSlidePosStep = 50;

	m_dwActionTick = GetTickCount();
}

CTopmostMenuDlg::~CTopmostMenuDlg()
{
}

void CTopmostMenuDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LEFT, m_btnPreviousTemplate);
	DDX_Control(pDX, IDC_RIGHT, m_btnNextTemplate);
	DDX_Control(pDX, IDC_SHOW, m_ShowBt);
	DDX_Control(pDX, IDC_SETTING, m_SetBt);
	DDX_Control(pDX, IDC_PLAY, m_PlayBt);
	DDX_Control(pDX, IDC_MAINMENU, m_MainMenuBt);
}

BEGIN_MESSAGE_MAP(CTopmostMenuDlg, CDialog)
	ON_WM_QUERYDRAGICON()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_SHOW, &CTopmostMenuDlg::OnBnClickedShow)
	ON_BN_CLICKED(IDC_LEFT, &CTopmostMenuDlg::OnBnClickedLeft)
	ON_BN_CLICKED(IDC_RIGHT, &CTopmostMenuDlg::OnBnClickedRight)
	ON_BN_CLICKED(IDC_MAINMENU, &CTopmostMenuDlg::OnBnClickedMainMenu)
	ON_MESSAGE(WM_USER_SLIDE_MENU, &CTopmostMenuDlg::OnBnClickedSlideMenu)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// CTopmostMenuDlg 메시지 처리기

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CTopmostMenuDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CTopmostMenuDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	
	m_nWidth = ::GetSystemMetrics(SM_CXSCREEN);						// 모니터의 해상도 x
	m_nHeight = ::GetSystemMetrics(SM_CYSCREEN);

	// 시작표시줄을 반짝거리지 않게 하기위해, 시작표시줄에 들어가지 않게한다.
	DWORD dwStyle = GetWindowLong(this->m_hWnd, GWL_EXSTYLE );
	dwStyle &= ~WS_EX_APPWINDOW;
	dwStyle |= WS_EX_TOOLWINDOW;
	SetWindowLong(this->m_hWnd, GWL_EXSTYLE, dwStyle );

	m_btnPreviousTemplate.SetWindowText("Previous template");
	m_btnPreviousTemplate.LoadBitmap(IDB_BTN_PREVIOUS_TEMPLATE, BTN_TRANSPARENT_COLOR);
	m_btnPreviousTemplate.SetToolTipText("Move to previous template");

	m_btnNextTemplate.SetWindowText("Next template");
	m_btnNextTemplate.LoadBitmap(IDB_BTN_NEXT_TEMPLATE, BTN_TRANSPARENT_COLOR);
	m_btnNextTemplate.SetToolTipText("Move to next template");

	m_ShowBt.LoadBitmap(IDB_BITMAP_SHOW, BTN_TRANSPARENT_COLOR);
	m_SetBt.LoadBitmap(IDB_BITMAP_SET_DISABLE, BTN_TRANSPARENT_COLOR);
	m_PlayBt.LoadBitmap(IDB_BITMAP_PLAY_DISABLE, BTN_TRANSPARENT_COLOR);

	m_SetBt.EnableWindow(FALSE);
	m_PlayBt.EnableWindow(FALSE);

	CString strCmdLine = AfxGetApp()->m_lpCmdLine;
	if (strCmdLine.Find("+nomenu") != -1)
	{
		m_MainMenuBt.LoadBitmap(IDB_BITMAP_MAINMENU_DISABLE, BTN_TRANSPARENT_COLOR);
		m_MainMenuBt.EnableWindow(FALSE);
	} else {
		m_MainMenuBt.LoadBitmap(IDB_BITMAP_MAINMENU, BTN_TRANSPARENT_COLOR);

		m_pDlgSlideMenu = new CSlideMenuDlg(this);
		m_pDlgSlideMenu->Create(CSlideMenuDlg::IDD);
	}

	SlideAction(SLIDE_RIGHT_STOP, TRUE);

	Transparent();

	SetTimer(1234, 1000, NULL);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CTopmostMenuDlg::OnPaint()
{
	CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CRect clientRect;
		GetClientRect(&clientRect);

		CMemDC memDC(&dc);
		memDC->FillSolidRect(clientRect, BG_TRANSPARENT_COLOR);
		CDialog::OnPaint();
	}
}

BOOL CTopmostMenuDlg::OnEraseBkgnd(CDC* pDC)
{
	return FALSE;
	//return CDialog::OnEraseBkgnd(pDC);
}

void CTopmostMenuDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case 1000 :
		{
			CRect rectParent;
			GetClientRect(&rectParent);
			int height = rectParent.Height();
			int width = rectParent.Width();

			switch (m_nSlideMode)
			{
			case SLIDE_LEFT_START :
				m_nSlideMode = SLIDE_LEFT_MOVE;
				m_nSlidePos = m_nSlidePosMin;

				EnableWindow(FALSE);
				SetWindowPos(&wndTopMost, m_nWidth-m_nSlidePos-2, m_nHeight-72-2, m_nSlidePos, 72, SWP_NOREDRAW);
				RedrawWindow();
				break;

			case SLIDE_LEFT_MOVE :
				m_nSlidePos += m_nSlidePosStep;
				if (m_nSlidePos > m_nSlidePosMax)
				{
					m_nSlideMode = SLIDE_LEFT_STOP;
					m_nSlidePos = m_nSlidePosMax;
				}

				SetWindowPos(&wndTopMost, m_nWidth-m_nSlidePos-2, m_nHeight-72-2, m_nSlidePos, 72, SWP_NOREDRAW);
				RedrawWindow();
				break;

			case SLIDE_LEFT_STOP :
				EnableWindow(TRUE);
				KillTimer(1000);
				break;

			case SLIDE_RIGHT_START :
				m_nSlideMode = SLIDE_RIGHT_MOVE;
				m_nSlidePos = m_nSlidePosMax;

				EnableWindow(FALSE);
				SetWindowPos(&wndTopMost, m_nWidth-m_nSlidePos-2, m_nHeight-72-2, m_nSlidePos, 72, SWP_NOREDRAW);
				RedrawWindow();
				break;

			case SLIDE_RIGHT_MOVE :
				m_nSlidePos -= m_nSlidePosStep;
				if (m_nSlidePos < m_nSlidePosMin)
				{
					m_nSlideMode = SLIDE_RIGHT_STOP;
					m_nSlidePos = m_nSlidePosMin;
				}

				SetWindowPos(&wndTopMost, m_nWidth-m_nSlidePos-2, m_nHeight-72-2, m_nSlidePos, 72, SWP_NOREDRAW);
				RedrawWindow();
				break;

			case SLIDE_RIGHT_STOP :
				EnableWindow(TRUE);
				KillTimer(1000);
				break;
			}
		} break;

	case 1234:
		{
			static CString lastKey2;

			_getCurrentKey();

			if(m_currentKey != "CTRL+1") {
				SetTopMost(TRUE);
				//this->RedrawWindow();
			}else{
				if(lastKey2 != m_currentKey) {
					SetTopMost(FALSE);
				}
			}
			lastKey2 = m_currentKey;

			// 자동 사라짐...
			if (GetTickCount() > m_dwActionTick + UNSHOW_TIME)
			{
				m_dwActionTick = GetTickCount();

				if (m_toggle == 1)
				{
					SlideAction(SLIDE_RIGHT_START, FALSE);
				}
			}
		} break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CTopmostMenuDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (m_pDlgSlideMenu)
	{
		m_pDlgSlideMenu->DestroyWindow();
		delete m_pDlgSlideMenu;
		m_pDlgSlideMenu = NULL;
	}

	KillTimer(1234);
	showTaskbar(true);
	DeleteCriticalSection(&gCS);
	PostQuitMessage(0);

	//CDialog::OnClose();
}

void CTopmostMenuDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();
}

void CTopmostMenuDlg::OnBnClickedShow()
{
	// 반복 입력 무시..
	if (GetTickCount() < m_dwActionTick + ACTION_REPEAT_TIME)
	{
		return;
	}

	m_dwActionTick = GetTickCount();

	SlideAction(SLIDE_TOGGLE, FALSE);
}

void CTopmostMenuDlg::OnBnClickedSet()
{
	// 반복 입력 무시..
	if (GetTickCount() < m_dwActionTick + ACTION_REPEAT_TIME)
	{
		return;
	}

	m_dwActionTick = GetTickCount();
}

void CTopmostMenuDlg::OnBnClickedPlay()
{
	// 반복 입력 무시..
	if (GetTickCount() < m_dwActionTick + ACTION_REPEAT_TIME)
	{
		return;
	}

	m_dwActionTick = GetTickCount();
}

void CTopmostMenuDlg::OnBnClickedLeft()
{
	// 반복 입력 무시..
	if (GetTickCount() < m_dwActionTick + ACTION_REPEAT_TIME)
	{
		return;
	}

	m_dwActionTick = GetTickCount();

	static CString lastKey1;
	if(_getCurrentKey()) {
		CString prevKey = _getPrevKey();
		if(!prevKey.IsEmpty() && lastKey1 != prevKey) {
			socketAgent(prevKey);
			lastKey1 = prevKey;
			m_toggle=1;
			OnBnClickedShow();
		}else{
			lastKey1 = "";
		}
	}
}

void CTopmostMenuDlg::OnBnClickedRight()
{
	// 반복 입력 무시..
	if (GetTickCount() < m_dwActionTick + ACTION_REPEAT_TIME)
	{
		return;
	}

	m_dwActionTick = GetTickCount();

	static CString lastKey2;
	if(_getCurrentKey()) {
		CString nextKey = _getNextKey();
		if(!nextKey.IsEmpty() && lastKey2 != nextKey) {
			socketAgent(nextKey);
			lastKey2 = nextKey;
			m_toggle=1;
			OnBnClickedShow();
		}else{
			lastKey2 = "";
		}
	}
}

void CTopmostMenuDlg::OnBnClickedMainMenu()
{
	// 반복 입력 무시..
	if (GetTickCount() < m_dwActionTick + ACTION_REPEAT_TIME)
	{
		return;
	}

	m_dwActionTick = GetTickCount();

	if (m_pDlgSlideMenu)
	{
		if (m_nToggleMainMenu == 1)
		{
			m_MainMenuBt.LoadBitmap(IDB_BITMAP_MAINMENU_CLOSE, BTN_TRANSPARENT_COLOR);
			m_MainMenuBt.RedrawWindow();
			m_nToggleMainMenu = 0;
			m_pDlgSlideMenu->SlideAction(SLIDE_UP_START, FALSE);
		}
		else
		{
			m_MainMenuBt.LoadBitmap(IDB_BITMAP_MAINMENU, BTN_TRANSPARENT_COLOR);
			m_MainMenuBt.RedrawWindow();
			m_nToggleMainMenu = 1;
			m_pDlgSlideMenu->SlideAction(SLIDE_DOWN_START, FALSE);
		}
	}
}

LRESULT CTopmostMenuDlg::OnBnClickedSlideMenu(WPARAM wParam, LPARAM lParam)
{
	// 처리중 입력 무시..
	if (m_nSlideMode != SLIDE_LEFT_STOP && m_nSlideMode != SLIDE_RIGHT_STOP)
	{
		return (LRESULT)0;
	}

	// 반복 입력 무시..
	if (GetTickCount() < m_dwActionTick + ACTION_REPEAT_TIME)
	{
		return (LRESULT)0;
	}

	m_dwActionTick = GetTickCount();

	switch (wParam)
	{
	case ID_SLIDE_MENU_01 :
		SetXmlStartPage(1);
		socketAgent("CTRL+1");
		break;

	case ID_SLIDE_MENU_02 :
		SetXmlStartPage(2);
		socketAgent("CTRL+1");
		break;

	case ID_SLIDE_MENU_03 :
		SetXmlStartPage(3);
		socketAgent("CTRL+1");
		break;

	case ID_SLIDE_MENU_04 :
		SetXmlStartPage(4);
		socketAgent("CTRL+1");
		break;

	case ID_SLIDE_MENU_05 :
		SetXmlStartPage(5);
		socketAgent("CTRL+1");
		break;

	case ID_SLIDE_MENU_06 :
		SetXmlStartPage(6);
		socketAgent("CTRL+1");
		break;

	case ID_SLIDE_MENU_07 :
		_getCurrentKey();
		if(m_currentKey != "CTRL+2")
			socketAgent("CTRL+2");
		break;

	case ID_SLIDE_MENU_08 :
		_getCurrentKey();
		if(m_currentKey != "CTRL+3")
			socketAgent("CTRL+3");
		break;
	}

	SlideAction(SLIDE_RIGHT_START, FALSE);

	return (LRESULT)1;
}

void CTopmostMenuDlg::SetTopMost(bool bTopMost)
{
	if (bTopMost)
	{
		/*
		HWND hwnd = ::GetTopWindow(NULL);
		if(hwnd) {
			char buf[256];
			if (GetWindowModuleFileName(hwnd, buf, 256) > 0)
			{
				CString binName = buf;
				if(binName.MakeLower().Find("topmostmenu") >= 0){
					return;  // 이미 Topmostwindow 라면 굳이 하지 않는다.
				}
			}
		}
		*/
		this->SetWindowPos(&wndTopMost, -1,-1,-1,-1, SWP_NOSIZE | SWP_NOMOVE);
		this->ShowWindow(SW_SHOW);
		//::SetForegroundWindow(this->m_hWnd);
		showTaskbar(false);
		//this->RedrawWindow();
/*
		BringWindowToTop();
		SetForegroundWindow();
		SetFocus();
		::SetWindowPos(this->m_hWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_SHOWWINDOW);
*/
	}
	else
	{
		this->SetWindowPos(&wndNoTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
		this->ShowWindow(SW_HIDE);
		if (m_pDlgSlideMenu) m_pDlgSlideMenu->ShowWindow(SW_HIDE);
	}//if
}

#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0

BOOL
CTopmostMenuDlg::socketAgent(const char* data, const char* ipAddress/*="127.0.0.1"*/, int portNo/*=14007*/)
 {
	 // Load WinSock DLL 
 	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA WsaData;
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {
		WSACleanup();
		return FALSE;
	}

	SOCKET socket_fd;
	if ((socket_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		return FALSE;
	}

	struct sockaddr_in server_addr;	
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(ipAddress);
	server_addr.sin_port = htons(portNo);
	
	if (connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
		closesocket(socket_fd);
		WSACleanup();
		return FALSE;
	}

	CString value = data;
	value += "\n";

	int msg_size;
	if ((msg_size = send(socket_fd, value, value.GetLength(), 0)) <= 0) {
		WSACleanup();
		closesocket(socket_fd);
		return FALSE;
	}
	
	// Receive OK
	char buf[128+1];
	memset(buf,0x00, sizeof(buf));
	if ((msg_size = recv(socket_fd, buf, 2, 0)) <= 0) {
		closesocket(socket_fd);
		WSACleanup();
		return FALSE;
	}
	
	closesocket(socket_fd);
	WSACleanup();

	return TRUE;
}

BOOL CTopmostMenuDlg::SetXmlStartPage(int nMenu)
{
	EnterCriticalSection(&gCS);

	TCHAR szAppPath[MAX_PATH] = { 0, };
	GetCurrentDirectory(MAX_PATH, szAppPath);

	CString strXmlStartPageFilePath = "";
	strXmlStartPageFilePath.Format("%s\\..\\..\\Contents\\Enc\\GPQS\\GPQS_startPage.xml", szAppPath);

	FILE* fp = 0;
	fopen_s(&fp, strXmlStartPageFilePath, "w");
	if (fp)
	{
		/*
		<?xml version="1.0" encoding="UTF-8"?>
		<GPQS>
		<MENU> 1 </MENU>
		<LASTUPDATETIME> 2015/06/25 10:00:00 </LASTUPDATETIME>
		</GPQS>
		*/

		COleDateTime currentTime = COleDateTime::GetCurrentTime();

		fprintf_s(fp, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
		fprintf_s(fp, "<GPQS>\r\n");
		fprintf_s(fp, "<MENU>%d</MENU>\r\n", nMenu);
		fprintf_s(fp, "<LASTUPDATETIME>%s</LASTUPDATETIME>\r\n", currentTime.Format("%Y/%m/%d %H:%M:%S"));
		fprintf_s(fp, "</GPQS>\r\n");

		fclose(fp);
		fp = 0;

		LeaveCriticalSection(&gCS);
		return TRUE;
	}

	LeaveCriticalSection(&gCS);
	return FALSE;
}

BOOL 
CTopmostMenuDlg::_getCurrentKey()
{
	EnterCriticalSection(&gCS);
	m_currentKey = "";
	
	FILE* fp = 0;
	fopen_s(&fp, "flash\\xml\\current_playinfo.xml", "r");
	if(fp==0) {
		LeaveCriticalSection(&gCS);
		return FALSE;
	}
	char rbuf[1024];
	memset(rbuf,0x00,1024);
	while(fgets(rbuf,1023,fp)){
		CString buf = rbuf;
		int posStart = buf.Find("CTRL+");
		if(posStart >= 0) {
			m_currentKey = buf.Mid(posStart,6);
			break;
		}
	}

	if(fp) {
		fclose(fp);
		fp = 0;
	}
	if(m_currentKey.IsEmpty()) {
		LeaveCriticalSection(&gCS);	
		return FALSE;
	}

	LeaveCriticalSection(&gCS);
	return TRUE;
}

CString
CTopmostMenuDlg::_getNextKey()
{
	int idx = atoi(m_currentKey.Mid(5));
	if(idx >= MAX_PAGE) {
		idx = 1;
	}else{
		idx++;
	}
	CString retval;
	retval.Format("CTRL+%d", idx);
	return retval;
}

CString
CTopmostMenuDlg::_getPrevKey()
{
	int idx = atoi(m_currentKey.Mid(5));
	if(idx <= 1) {
		idx = MAX_PAGE;
	}else{
		idx--;
	}
	CString retval;
	retval.Format("CTRL+%d", idx);
	return retval;
}

void CTopmostMenuDlg::showTaskbar(bool bShow)
{
	CWnd* pWnd = CWnd::FindWindow("Shell_TrayWnd", "");
	if(pWnd){
		if(bShow){
			pWnd->ShowWindow(SW_SHOW);
			pWnd->SetFocus();
		}else{
			pWnd->ShowWindow(SW_HIDE);
		}
	}
}

void CTopmostMenuDlg::Transparent()
{
	SetWindowLong(m_hWnd, GWL_EXSTYLE, GetWindowLong(m_hWnd,GWL_EXSTYLE) ^ WS_EX_LAYERED);
	SetLayeredWindowAttributes(RGB(255,0,255), 0, LWA_COLORKEY);
}

void CTopmostMenuDlg::SlideAction(int nSlideMode, BOOL bInit)
{
	CRect rectParent;
	GetClientRect(&rectParent);
	int height = rectParent.Height();
	int width = rectParent.Width();

	if (nSlideMode == SLIDE_TOGGLE)
	{
		if (m_toggle == 1)
		{
			nSlideMode = SLIDE_RIGHT_START;
		}
		else
		{
			nSlideMode = SLIDE_LEFT_START;
		}
	}

	if (bInit)
	{
		if (nSlideMode == SLIDE_RIGHT_START || nSlideMode == SLIDE_RIGHT_STOP)
		{
			SetWindowPos(&wndTopMost, m_nWidth-34-2, m_nHeight-height-2, 34, height, NULL);
			m_ShowBt.LoadBitmap(IDB_BITMAP_SHOW, BTN_TRANSPARENT_COLOR);
			m_ShowBt.RedrawWindow();
			m_toggle = 0;
			m_nSlideMode = nSlideMode;
		}
		else if (nSlideMode == SLIDE_LEFT_START || nSlideMode == SLIDE_LEFT_STOP)
		{
			SetWindowPos(&wndTopMost, m_nWidth-408-2, m_nHeight-height-2, 408, height, NULL);
			m_ShowBt.LoadBitmap(IDB_BITMAP_UNSHOW, BTN_TRANSPARENT_COLOR);
			m_ShowBt.RedrawWindow();
			m_toggle = 1;
			m_nSlideMode = nSlideMode;
		}
	}
	else
	{
		if (nSlideMode == SLIDE_RIGHT_START)
		{
			EnableWindow(FALSE);
			m_ShowBt.LoadBitmap(IDB_BITMAP_SHOW, BTN_TRANSPARENT_COLOR);
			m_ShowBt.RedrawWindow();
			m_toggle = 0;
			m_nSlideMode = nSlideMode;
			SetTimer(1000, 50, NULL);
		}
		else if (nSlideMode == SLIDE_LEFT_START)
		{
			EnableWindow(FALSE);
			m_ShowBt.LoadBitmap(IDB_BITMAP_UNSHOW, BTN_TRANSPARENT_COLOR);
			m_ShowBt.RedrawWindow();
			m_toggle = 1;
			m_nSlideMode = nSlideMode;
			SetTimer(1000, 50, NULL);
		}
	}

	if (m_pDlgSlideMenu)
	{
		if (nSlideMode == SLIDE_RIGHT_START)
		{
			m_MainMenuBt.LoadBitmap(IDB_BITMAP_MAINMENU, BTN_TRANSPARENT_COLOR);
			m_MainMenuBt.RedrawWindow();
			m_nToggleMainMenu = 1;
			m_pDlgSlideMenu->EnableWindow(FALSE);
			m_pDlgSlideMenu->SlideAction(SLIDE_DOWN_START, bInit);
		}
		else if (nSlideMode == SLIDE_LEFT_START)
		{
			m_MainMenuBt.LoadBitmap(IDB_BITMAP_MAINMENU_CLOSE, BTN_TRANSPARENT_COLOR);
			m_MainMenuBt.RedrawWindow();
			m_nToggleMainMenu = 0;
			m_pDlgSlideMenu->EnableWindow(FALSE);
			m_pDlgSlideMenu->SlideAction(SLIDE_UP_START, bInit);
		}
	}
}
