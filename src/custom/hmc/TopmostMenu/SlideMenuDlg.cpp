// SlideMenuDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TopmostMenu.h"
#include "SlideMenuDlg.h"
#include "MemDC.h"

#define BG_TRANSPARENT_COLOR	RGB(255,0,255)
#define BTN_TRANSPARENT_COLOR	RGB(0,0,0)

// CSlideMenuDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSlideMenuDlg, CDialog)

CSlideMenuDlg::CSlideMenuDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSlideMenuDlg::IDD, pParent)
{
	m_nSlideMode = SLIDE_DOWN_STOP;
	m_nSlideMarginBottom = 72;
	m_nSlidePos = 0;
	m_nSlidePosMin = 0;
	m_nSlidePosMax = 542;
	m_nSlidePosStep = 100;
}

CSlideMenuDlg::~CSlideMenuDlg()
{
}

void CSlideMenuDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON1, m_hbSlideMenu);
	DDX_Control(pDX, IDC_BUTTON2, m_hbSlideMenu01);
	DDX_Control(pDX, IDC_BUTTON3, m_hbSlideMenu02);
	DDX_Control(pDX, IDC_BUTTON4, m_hbSlideMenu03);
	DDX_Control(pDX, IDC_BUTTON5, m_hbSlideMenu04);
	DDX_Control(pDX, IDC_BUTTON6, m_hbSlideMenu05);
	DDX_Control(pDX, IDC_BUTTON7, m_hbSlideMenu06);
	DDX_Control(pDX, IDC_BUTTON8, m_hbSlideMenu07);
	DDX_Control(pDX, IDC_BUTTON9, m_hbSlideMenu08);
}

BEGIN_MESSAGE_MAP(CSlideMenuDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON2, &CSlideMenuDlg::OnBnClickedSlideMenu01)
	ON_BN_CLICKED(IDC_BUTTON3, &CSlideMenuDlg::OnBnClickedSlideMenu02)
	ON_BN_CLICKED(IDC_BUTTON4, &CSlideMenuDlg::OnBnClickedSlideMenu03)
	ON_BN_CLICKED(IDC_BUTTON5, &CSlideMenuDlg::OnBnClickedSlideMenu04)
	ON_BN_CLICKED(IDC_BUTTON6, &CSlideMenuDlg::OnBnClickedSlideMenu05)
	ON_BN_CLICKED(IDC_BUTTON7, &CSlideMenuDlg::OnBnClickedSlideMenu06)
	ON_BN_CLICKED(IDC_BUTTON8, &CSlideMenuDlg::OnBnClickedSlideMenu07)
	ON_BN_CLICKED(IDC_BUTTON9, &CSlideMenuDlg::OnBnClickedSlideMenu08)
END_MESSAGE_MAP()

// CSlideMenuDlg 메시지 처리기입니다.

BOOL CSlideMenuDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_nWidth = ::GetSystemMetrics(SM_CXSCREEN);						// 모니터의 해상도 x
	m_nHeight = ::GetSystemMetrics(SM_CYSCREEN);

	m_hbSlideMenu.LoadBitmap(IDB_BITMAP_SLIDEMENU, BTN_TRANSPARENT_COLOR);
	m_hbSlideMenu01.LoadBitmap(IDB_BITMAP_SLIDEMENU_01, BTN_TRANSPARENT_COLOR);
	m_hbSlideMenu02.LoadBitmap(IDB_BITMAP_SLIDEMENU_02, BTN_TRANSPARENT_COLOR);
	m_hbSlideMenu03.LoadBitmap(IDB_BITMAP_SLIDEMENU_03, BTN_TRANSPARENT_COLOR);
	m_hbSlideMenu04.LoadBitmap(IDB_BITMAP_SLIDEMENU_04, BTN_TRANSPARENT_COLOR);
	m_hbSlideMenu05.LoadBitmap(IDB_BITMAP_SLIDEMENU_05, BTN_TRANSPARENT_COLOR);
	m_hbSlideMenu06.LoadBitmap(IDB_BITMAP_SLIDEMENU_06, BTN_TRANSPARENT_COLOR);
	m_hbSlideMenu07.LoadBitmap(IDB_BITMAP_SLIDEMENU_07, BTN_TRANSPARENT_COLOR);
	m_hbSlideMenu08.LoadBitmap(IDB_BITMAP_SLIDEMENU_08, BTN_TRANSPARENT_COLOR);

	Transparent();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSlideMenuDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CRect clientRect;
	GetClientRect(&clientRect);

	CMemDC memDC(&dc);
	memDC->FillSolidRect(clientRect, BG_TRANSPARENT_COLOR);
}

BOOL CSlideMenuDlg::OnEraseBkgnd(CDC* pDC)
{
	return FALSE;
	//return CDialog::OnEraseBkgnd(pDC);
}

void CSlideMenuDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case 1000 :
		{
			CRect rectParent;
			GetClientRect(&rectParent);
			int height = rectParent.Height();
			int width = rectParent.Width();

			switch (m_nSlideMode)
			{
			case SLIDE_UP_START :
				m_nSlideMode = SLIDE_UP_MOVE;
				m_nSlidePos = m_nSlidePosMin;

				EnableWindow(FALSE);
				ShowWindow(SW_SHOW);
				SetWindowPos(&wndTopMost, m_nWidth-218-2, m_nHeight-m_nSlideMarginBottom-m_nSlidePos-2, 218, m_nSlidePos, SWP_NOREDRAW);
				RedrawWindow();
				break;

			case SLIDE_UP_MOVE :
				m_nSlidePos += m_nSlidePosStep;
				if (m_nSlidePos > m_nSlidePosMax)
				{
					m_nSlideMode = SLIDE_UP_STOP;
					m_nSlidePos = m_nSlidePosMax;
				}

				SetWindowPos(&wndTopMost, m_nWidth-218-2, m_nHeight-m_nSlideMarginBottom-m_nSlidePos-2, 218, m_nSlidePos, SWP_NOREDRAW);
				RedrawWindow();
				break;

			case SLIDE_UP_STOP :
				EnableWindow(TRUE);
				KillTimer(1000);
				break;

			case SLIDE_DOWN_START :
				m_nSlideMode = SLIDE_DOWN_MOVE;
				m_nSlidePos = m_nSlidePosMax;

				SetWindowPos(&wndTopMost, m_nWidth-218-2, m_nHeight-m_nSlideMarginBottom-m_nSlidePos-2, 218, m_nSlidePos, SWP_NOREDRAW);
				RedrawWindow();
				EnableWindow(FALSE);
				break;

			case SLIDE_DOWN_MOVE :
				m_nSlidePos -= m_nSlidePosStep;
				if (m_nSlidePos < m_nSlidePosMin)
				{
					m_nSlideMode = SLIDE_DOWN_STOP;
					m_nSlidePos = m_nSlidePosMin;
					ShowWindow(SW_HIDE);
				}

				SetWindowPos(&wndTopMost, m_nWidth-218-2, m_nHeight-m_nSlideMarginBottom-m_nSlidePos-2, 218, m_nSlidePos, SWP_NOREDRAW);
				RedrawWindow();
				break;

			case SLIDE_DOWN_STOP :
				KillTimer(1000);
				break;
			}
		} break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CSlideMenuDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	KillTimer(1000);
	//CDialog::OnClose();
}

void CSlideMenuDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnCancel();
}

void CSlideMenuDlg::OnBnClickedSlideMenu01()
{
	GetParent()->PostMessage(WM_USER_SLIDE_MENU, ID_SLIDE_MENU_01, 0);
}

void CSlideMenuDlg::OnBnClickedSlideMenu02()
{
	GetParent()->PostMessage(WM_USER_SLIDE_MENU, ID_SLIDE_MENU_02, 0);
}

void CSlideMenuDlg::OnBnClickedSlideMenu03()
{
	GetParent()->PostMessage(WM_USER_SLIDE_MENU, ID_SLIDE_MENU_03, 0);
}

void CSlideMenuDlg::OnBnClickedSlideMenu04()
{
	GetParent()->PostMessage(WM_USER_SLIDE_MENU, ID_SLIDE_MENU_04, 0);
}

void CSlideMenuDlg::OnBnClickedSlideMenu05()
{
	GetParent()->PostMessage(WM_USER_SLIDE_MENU, ID_SLIDE_MENU_05, 0);
}

void CSlideMenuDlg::OnBnClickedSlideMenu06()
{
	GetParent()->PostMessage(WM_USER_SLIDE_MENU, ID_SLIDE_MENU_06, 0);
}

void CSlideMenuDlg::OnBnClickedSlideMenu07()
{
	GetParent()->PostMessage(WM_USER_SLIDE_MENU, ID_SLIDE_MENU_07, 0);
}

void CSlideMenuDlg::OnBnClickedSlideMenu08()
{
	GetParent()->PostMessage(WM_USER_SLIDE_MENU, ID_SLIDE_MENU_08, 0);
}

void CSlideMenuDlg::Transparent()
{
	SetWindowLong(m_hWnd, GWL_EXSTYLE, GetWindowLong(m_hWnd,GWL_EXSTYLE) ^ WS_EX_LAYERED);
	SetLayeredWindowAttributes(RGB(255,0,255), 0, LWA_COLORKEY);
}

void CSlideMenuDlg::SlideAction(int nSlideMode, BOOL bInit)
{
	if (bInit)
	{
		if (nSlideMode == SLIDE_UP_START)
		{
			SetWindowPos(&wndTopMost, m_nWidth-218-2, m_nHeight-m_nSlideMarginBottom-m_nSlidePosMax-2, 218, m_nSlidePosMax, NULL);
			ShowWindow(SW_SHOW);
		}
		else if (nSlideMode == SLIDE_DOWN_START)
		{
			SetWindowPos(&wndTopMost, m_nWidth-218-2, m_nHeight-m_nSlideMarginBottom-m_nSlidePosMin-2, 218, m_nSlidePosMin, NULL);
			ShowWindow(SW_HIDE);
		}
	}
	else
	{
		if (nSlideMode == SLIDE_UP_START)
		{
			m_nSlideMode = nSlideMode;
			SetTimer(1000, 50, NULL);
		}
		else if (nSlideMode == SLIDE_DOWN_START)
		{
			m_nSlideMode = nSlideMode;
			SetTimer(1000, 50, NULL);
		}
	}
}
