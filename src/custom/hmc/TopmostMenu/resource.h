//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TopmostMenu.rc
//
#define IDD_TOPMOSTMENU_DIALOG          102
#define IDD_SLIDEMENU_DIALOG            103
#define IDD_SLIDETOTALMENU_DIALOG       104
#define IDD_TOPMOSTTOTALMENU_DIALOG     105
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_RIGHT                129
#define IDB_BITMAP_LEFT                 130
#define IDB_BTN_PREVIOUS_TEMPLATE       134
#define IDB_BTN_NEXT_TEMPLATE           135
#define IDB_BITMAP_STOP                 137
#define IDB_BITMAP_MAINMENU             138
#define IDB_BITMAP_PLAY                 139
#define IDB_BITMAP_SET2                 140
#define IDB_BITMAP_SET                  140
#define IDB_BITMAP_SHOW                 141
#define IDB_BITMAP_UNSHOW               142
#define IDB_BITMAP1                     147
#define IDB_BITMAP_SLIDEMENU_01         149
#define IDB_BITMAP_SLIDEMENU_02         150
#define IDB_BITMAP_SLIDEMENU_03         151
#define IDB_BITMAP_SLIDEMENU_04         152
#define IDB_BITMAP_SLIDEMENU_05         153
#define IDB_BITMAP_SLIDEMENU_06         154
#define IDB_BITMAP_SLIDEMENU_07         155
#define IDB_BITMAP_SLIDEMENU_08         156
#define IDB_BITMAP_SLIDEMENU            157
#define IDB_BITMAP2                     158
#define IDB_BITMAP_MAINMENU_CLOSE       159
#define IDB_BITMAP_SET_DISABLE          160
#define IDB_BITMAP_PLAY_DISABLE         161
#define IDB_BITMAP_MAINMENU_DISABLE     162
#define IDC_RIGHT                       1000
#define IDC_LEFT                        1001
#define IDC_PLAY                        1002
#define IDC_BUTTON1                     1002
#define IDC_MAINMENU                    1003
#define IDC_BUTTON2                     1003
#define IDC_SETTING                     1004
#define IDC_BUTTON3                     1004
#define IDC_SHOW                        1005
#define IDC_BUTTON4                     1005
#define IDC_BUTTON5                     1006
#define IDC_BUTTON6                     1007
#define IDC_BUTTON7                     1008
#define IDC_BUTTON8                     1009
#define IDC_BUTTON9                     1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        163
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           106
#endif
#endif
