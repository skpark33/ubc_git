#pragma once

#include "HoverButton.h"
#include "ximage.h"

#define SLIDE_UP_START		1001
#define SLIDE_UP_MOVE		1002
#define SLIDE_UP_STOP		1003
#define SLIDE_DOWN_START	1004
#define SLIDE_DOWN_MOVE		1005
#define SLIDE_DOWN_STOP		1006

#define WM_USER_SLIDE_MENU	WM_USER + 1000
#define ID_SLIDE_MENU_01	1001
#define ID_SLIDE_MENU_02	1002
#define ID_SLIDE_MENU_03	1003
#define ID_SLIDE_MENU_04	1004
#define ID_SLIDE_MENU_05	1005
#define ID_SLIDE_MENU_06	1006
#define ID_SLIDE_MENU_07	1007
#define ID_SLIDE_MENU_08	1008

// CSlideTotalMenuDlg 대화 상자입니다.

class CSlideTotalMenuDlg : public CDialog
{
	DECLARE_DYNAMIC(CSlideTotalMenuDlg)

public:
	CSlideTotalMenuDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSlideTotalMenuDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_SLIDETOTALMENU_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
	virtual void OnCancel();
	afx_msg void OnBnClickedSlideMenu01();
	afx_msg void OnBnClickedSlideMenu02();
	afx_msg void OnBnClickedSlideMenu03();
	afx_msg void OnBnClickedSlideMenu04();
	afx_msg void OnBnClickedSlideMenu05();
	afx_msg void OnBnClickedSlideMenu06();
	afx_msg void OnBnClickedSlideMenu07();
	DECLARE_MESSAGE_MAP()

protected:
	int m_nWidth;
	int m_nHeight;

	int m_nSlideMode;
	int m_nSlideMarginBottom;
	int m_nSlidePos;
	int m_nSlidePosMin;
	int m_nSlidePosMax;
	int m_nSlidePosStep;

	CHoverButton m_hbSlideMenu;
	CHoverButton m_hbSlideMenu01;
	CHoverButton m_hbSlideMenu02;
	CHoverButton m_hbSlideMenu03;
	CHoverButton m_hbSlideMenu04;
	CHoverButton m_hbSlideMenu05;
	CHoverButton m_hbSlideMenu06;
	CHoverButton m_hbSlideMenu07;

public:
	void Transparent();
	void SlideAction(int nSlideMode, BOOL bInit);
};
