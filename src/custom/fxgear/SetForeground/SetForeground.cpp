// SetForeground.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "SetForeground.h"
#include "SetForegroundDlg.h"

#include <Tlhelp32.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSetForegroundApp

BEGIN_MESSAGE_MAP(CSetForegroundApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CSetForegroundApp 생성

CSetForegroundApp::CSetForegroundApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CSetForegroundApp 개체입니다.

CSetForegroundApp theApp;


// CSetForegroundApp 초기화

BOOL CSetForegroundApp::InitInstance()
{
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("SetForeground"));
/*
	CSetForegroundDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
*/

	CString exe_name = _T("");

#if defined (_FXMIRROR_)
	exe_name = _T("FXMirror.exe");
#elif defined (_RIOOLYMPIC_)
	exe_name = _T("Rio_Olympic_360Kiosk.exe");
#endif

	if (__argc > 1)
	{
		exe_name = __wargv[1];
	}

	if ( exe_name.GetLength() > 0 )
	{
		HWND hwnd = getWHandle(exe_name);
		if (hwnd)
		{
			::SetForegroundWindow(hwnd);
		}
	}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

unsigned long CSetForegroundApp::getPid(const TCHAR* exename)
{
	//myDEBUG("getPid(%s)", exename);

	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE )
	{
		//myERROR("HANDLE을 생성할 수 없습니다");
		return 0;
	}

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	TCHAR strProcessName[512] = {0};

	if ( !Process32First ( hSnapshot, &pe32 ) )
	{
		//myERROR("Process32First failed.");
		::CloseHandle(hSnapshot);
		return 0;
	}

	do
	{
		//memset(strProcessName, 0, sizeof(strProcessName));
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if ( _tcsicmp(pe32.szExeFile, exename) == 0 )
		{		
			//myDEBUG("process founded(%s)",exename);
			::CloseHandle(hSnapshot);
			return pe32.th32ProcessID;
		}

	} while (Process32Next( hSnapshot, &pe32 ));
	//myDEBUG("process not founded(%s)", exename);
	::CloseHandle(hSnapshot);

	return 0;
}

HWND CSetForegroundApp::getWHandle(const TCHAR* exename)
{
	//myDEBUG("getWHandle(%s)", exename);
	return getWHandle(getPid(exename));
}

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if (!IsWindowVisible(hwnd)) return TRUE;
	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);
	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;
	if (pe->pid != pid) return TRUE;
	pe->hwnd = hwnd;
	return FALSE;
}

HWND CSetForegroundApp::getWHandle(unsigned long pid)
{
	//myDEBUG("getWHandle(%ld)", pid);
	if ( pid > 0 )
	{
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);

		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1)
		{
			parent = GetParent(child);
			if ( !parent )
			{
				return child;
			}
			child = parent;
		}
		return e.hwnd;
	}
	return (HWND)0;
}
