// ConfigDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "NumberDisplayer.h"
#include "ConfigDlg.h"
#include "NumberDisplayerDlg.h"

// CConfigDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CConfigDlg, CDialog)

CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDlg::IDD, pParent)
{

}

CConfigDlg::~CConfigDlg()
{
}

void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_KEY, m_editKey);
	DDX_Control(pDX, IDC_EDIT_IP, m_editIP);
	DDX_Control(pDX, IDC_EDIT_PORT, m_editPort);
}


BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CConfigDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CConfigDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


BOOL CConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_editPort.SetWindowText("14003");

	char szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(szModule, cDrive, cPath, cFilename, cExt);

	m_strPath = "";
	m_strPath.Format(_T("%s%s\\UBCNumber.ini"), cDrive, cPath);

	char	szValue[1024];

	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString("ROOT","SERVERIP","",szValue,sizeof(szValue),m_strPath);
	this->m_editIP.SetWindowText(szValue);

	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString("ROOT","PORT","14003",szValue,sizeof(szValue),m_strPath);
	this->m_editPort.SetWindowText(szValue);

	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString("ROOT","KEY","",szValue,sizeof(szValue),m_strPath);
	this->m_editKey.SetWindowText(szValue);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.

}

// CConfigDlg 메시지 처리기입니다.

void CConfigDlg::OnBnClickedOk()
{
	CString szValue;
	this->m_editIP.GetWindowText(szValue);
	if(szValue.IsEmpty()){
		CString errMsg;
		errMsg.Format("Please Set Server IP");
		AfxMessageBox(errMsg);
		return;
	}else{
		WritePrivateProfileString("ROOT","SERVERIP",szValue,m_strPath);
		if(m_parent) m_parent->m_serverIp = szValue;
	}

	szValue="";
	this->m_editPort.GetWindowText(szValue);
	if(szValue.IsEmpty()){
		CString errMsg;
		errMsg.Format("Please Set Port");
		AfxMessageBox(errMsg);
		return;
	}else{
		WritePrivateProfileString("ROOT","PORT",szValue,m_strPath);
		if(m_parent) m_parent->m_port = atoi(szValue);
	}
	szValue="";
	this->m_editKey.GetWindowText(szValue);
	if(szValue.IsEmpty()){
		CString errMsg;
		errMsg.Format("Please Set License Key");
		AfxMessageBox(errMsg);
		return;
	}else{
		if(szValue.GetLength() != 15){
			CString errMsg;
			errMsg.Format("Length of License Key should be 15");
			AfxMessageBox(errMsg);
		}

		WritePrivateProfileString("ROOT","KEY",szValue,m_strPath);
		if(m_parent) m_parent->m_key = szValue;
	}
		


	OnOK();
}

void CConfigDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}


