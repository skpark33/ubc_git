#ifndef _macUtil_h_
#define _macUtil_h_

#include <cstdio>
#include <string>
#include <list>
#include <map>
#include <set>
#include <fstream>
#include <vector>

#include "NetworkAdapter.h"

class macUtil {
public:

	static macUtil*	getInstance();
	static void	clearInstance();

	virtual ~macUtil() ;
	boolean getMacAddress(string& mac);

	int		loadAdapter(int nType = E_ADT_TYPE_LOCAL);					///<load system network adapter
	int		getadaptercount(void);										///<get system network adapter count
	string	getAdaptermacaddr(int nIndex = 0);							///<get system network adapter mac address at given index
	bool	IsExistmacaddr(const char* szMacaddr);						///<check system has a given mac address(network adapter)
	string	GetMacaddr(void);											///<get system network adapter mac address

protected:
	macUtil();

	static macUtil*	_instance;
	CNetworkAdapterList	m_clsAdapterList;			///<Network adapter list class
};




#endif // _macUtil_h_
