// NumberDisplayerDlg.h : 헤더 파일
//

#pragma once
#include "resource.h"
#include "afxwin.h"
#include "EditPlus.h"
#include <string>
#include <list>

// CNumberDisplayerDlg 대화 상자
class CNumberDisplayerDlg : public CDialog
{
// 생성입니다.
public:
	CNumberDisplayerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NUMBERDISPLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEditPlus m_editInput;
	CEditPlus m_editOutput1;
	CEditPlus m_editOutput2;
	CEditPlus m_editOutput3;
	CEditPlus m_editOutput4;
	CEditPlus m_editOutput5;
	CButton m_btSend;
	CButton m_btDel1;
	CButton m_btDel2;
	CButton m_btDel3;
	CButton m_btDel4;
	CButton m_btDel5;
	CButton m_btConfig;
	afx_msg void OnBnClickedButtonSend();
	afx_msg void OnBnClickedButtonDel1();
	afx_msg void OnBnClickedButtonDel2();
	afx_msg void OnBnClickedButtonDel3();
	afx_msg void OnBnClickedButtonDel4();
	afx_msg void OnBnClickedButtonDel5();
	afx_msg void OnBnClickedButtonConfig();

	bool GetConfigInfo();

	bool socketAgent3(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data,
				 CString& retval);

	bool IsAlreayExist(CString& buf);
	void OnBnClickedButtonDel(int i);

	void makeHeader(CString& header);

	CString m_serverIp;
	int		m_port;
	CString m_key;
	CString m_mac;

	std::list<std::string> m_strList;
	CEditPlus*  m_editOutputArray[5];
	int		m_lastIndex;

	HANDLE  pWaveInfo, hWaveRes;

protected:
	virtual void OnOK();
public:
	CButton m_btNext;
	afx_msg void OnBnClickedButtonNext();
	CButton m_btPrev;
	afx_msg void OnBnClickedButtonPrev();
	afx_msg void OnDestroy();
};
