// NumberDisplayerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "NumberDisplayerDlg.h"

// CEditPlus;
IMPLEMENT_DYNAMIC(CEditPlus, CEdit)

BEGIN_MESSAGE_MAP(CEditPlus, CEdit)
	ON_WM_PAINT()
END_MESSAGE_MAP()

CEditPlus::CEditPlus()
: m_parent(NULL),
 m_hasColor(false),
 m_hasFont(false)
{
	m_fontWidth = 36;
	m_fontHeight = 60;



}

CEditPlus::~CEditPlus()
{
	m_font.DeleteObject();
}

void
CEditPlus::OnPaint()
{
/*
	if(m_hasColor){
		TraceLog(("hasColor!!!"));
		CDC* dc = this->GetDC();
		//CRect rect;
		//GetClientRect(&rect);
		COLORREF textColor=RGB(255,0,0);
		//dc->FillSolidRect(rect,textColor);
		dc->SetTextColor(textColor);		
	}
*/
	if(!m_hasFont){
		m_font.CreateFont( m_fontHeight, // nHeight 
						   m_fontWidth, // nWidth 
						   0, // nEscapement 
						   0, // nOrientation 
						   1, // nWeight 
						   0, // bItalic 
						   0, // bUnderline 
						   0, // cStrikeOut 
						   0, // nCharSet 
						   OUT_DEFAULT_PRECIS, // nOutPrecision 
						   0, // nClipPrecision 
						   DEFAULT_QUALITY, // nQuality
						   DEFAULT_PITCH | FF_DONTCARE,  // nPitchAndFamily 
						  (LPCTSTR)"Tahoma" ); // lpszFacename 	
	}
	this->SetFont(&m_font);
	CEdit::OnPaint();

}
BOOL CEditPlus::PreTranslateMessage(MSG* pMsg)
{	
	if(!m_parent) 
	{
		return CEdit::PreTranslateMessage(pMsg);
	}

	if(pMsg->hwnd == this->GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			TraceLog(("PreTranslateMessage : %c (%d)", (TCHAR)pMsg->wParam, (int)pMsg->wParam));
			if(((int)pMsg->wParam) == 13) // Enterkey 처리
			{
				if(m_parent) {
					m_parent->OnBnClickedButtonSend();
				}
				return TRUE;
			}
			this->GetWindowTextA(m_str);

			if(m_str.GetLength() >= m_maximum){
				if((TCHAR)pMsg->wParam >= '0' && (TCHAR)pMsg->wParam <= '9'){
					return TRUE;
				}
			}
		}
	}

	return CEdit::PreTranslateMessage(pMsg);
}

