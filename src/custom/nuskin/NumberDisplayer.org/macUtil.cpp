#include "stdAfx.h"
#include "macUtil.h"

#   define BUF_LEN				128

macUtil* 	macUtil::_instance = 0; 

macUtil*	
macUtil::getInstance() {
	if(!_instance) {
		if(!_instance) {
			_instance = new macUtil;
		}
	}
	return _instance;
}

void	
macUtil::clearInstance() {
	if(_instance) {
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}



macUtil::macUtil() 
{
    printf("macUtil()\n");
}

macUtil::~macUtil() 
{
    printf("~macUtil()\n");
}


boolean
macUtil::getMacAddress(string& mac)
{
	/*
	int type=1;
	int count = loadAdapter(type);
	if(count==0){
		printf("ERROR : get MAC Address failed\n");
		return false;
	}
	
	mac=getAdaptermacaddr(0);
	if(mac.empty()){
		printf("ERROR : get MAC Address failed\n");
		return false;
	}
	printf("%s\n",mac.c_str());
	*/
	mac = GetMacaddr();
	if(mac.empty()) return false;
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// load system network adapter \n
/// @param (int) nType : (in) Adapter type
/// @return <형: int> \n
///			<Adapters count on scuccess> \n
///			<-1 on fail> \n
/////////////////////////////////////////////////////////////////////////////////
int macUtil::loadAdapter(int nType)
{
	return m_clsAdapterList.LoadAdapter(nType);
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// get system network adapter count \n
/// @return type: int \n
///			system network adapter count \n
/////////////////////////////////////////////////////////////////////////////////
int macUtil::getadaptercount()
{
	return m_clsAdapterList.GetCount();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// get system network adapter mac address at given index \n
/// @param (int) nIndex : (in) network adapter index
/// @return type: string \n
///			network adapter mac address : success \n
///			"" : fail \n
/////////////////////////////////////////////////////////////////////////////////
string macUtil::getAdaptermacaddr(int nIndex)
{
	return m_clsAdapterList.GetAdtMacAddr(nIndex);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// check system has a given mac address(network adapter) \n
/// @param (const char*) szMacaddr : (in) mac address string
/// @return <형: bool> \n
///			<true: exist> \n
///			<false: not exist> \n
/////////////////////////////////////////////////////////////////////////////////
bool macUtil::IsExistmacaddr(const char* szMacaddr)
{
	//int nAdtCount = m_clsAdapterList.GetCount();
	//if(nAdtCount <= 0)
	//{
	//	nAdtCount = loadAdapter(E_ADT_TYPE_LOCAL);
	//	if(nAdtCount <= 0)
	//	{
	//		return false;
	//	}//if
	//}//if

	m_clsAdapterList.ClearAdtInfolist();
	
	int nAdtCount = loadAdapter(E_ADT_TYPE_ALL);
	if(nAdtCount <= 0)
	{
		return false;
	}//if

	string strAddr;
	for(int i=0; i<nAdtCount; i++)
	{
		strAddr = m_clsAdapterList.GetAdtMacAddr(i);
		if(strcmp(szMacaddr, strAddr.c_str()) == 0)
		{
			return true;
		}//if
	}//for

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// get system network adapter mac address \n
/// @return <형: string> \n
///			<string: system network adapter mac address> \n
/////////////////////////////////////////////////////////////////////////////////
string macUtil::GetMacaddr()
{
	//먼저 유선 네트워크 카드를 검사하며, 유선 네트워크 카드가 없을 때는
	//무선 네트워크 카드의 주소를 검사한다.
	//각 카드 타입에서 검사된 네트워크 카드의 첫번째 주소를 반환한다.
	// - 수정 20110106 : 시스템에 가상 어뎁터가 잡혀있는 경우 맥주소를 반환하지 못하는 문제 수정

	string strMacAddr = "";

	//유선
	m_clsAdapterList.ClearAdtInfolist();
	int nCount = loadAdapter(E_ADT_TYPE_LOCAL);
	if(nCount != 0)
	{
		for(int i=0; i<nCount; i++)
		{
			strMacAddr = getAdaptermacaddr(i);
			if(!strMacAddr.empty())
			{
				return strMacAddr;
			}//if
		}//for
	}//if

	//무선
	m_clsAdapterList.ClearAdtInfolist();
	nCount = loadAdapter(E_ADT_TYPE_WIRELESS);
	if(nCount != 0)
	{
		for(int i=0; i<nCount; i++)
		{
			strMacAddr = getAdaptermacaddr(i);
			if(!strMacAddr.empty())
			{
				return strMacAddr;
			}//if
		}//for
	}//if

	return "";
}
