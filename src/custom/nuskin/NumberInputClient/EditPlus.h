#pragma once
#include "afxwin.h"

#define TraceLog(_f_) \
	{ \
		CString szTraceLogBuf; \
		szTraceLogBuf.Format(_T("%s(%d) : "), __FILE__, __LINE__); \
		szTraceLogBuf.AppendFormat _f_; \
		OutputDebugString(szTraceLogBuf); \
	}

class CNumberInputClientDlg ;
class CEditPlus : public CEdit
{
	DECLARE_DYNAMIC(CEditPlus)
public:
	CEditPlus();
	virtual ~CEditPlus();
	afx_msg void OnPaint() ;

	void SetParent(CNumberInputClientDlg* p) { m_parent = p; }
	CString GetString() { return m_str; }
	void SetMaximum(int p) { m_maximum = p; }
	void HasColor(bool p) { m_hasColor = p;}
	void SetFontSize(int w, int h) { m_fontWidth = w; m_fontHeight=h; }
protected:
	DECLARE_MESSAGE_MAP()

	CFont m_font;
	CNumberInputClientDlg* m_parent;
	CString m_str;
	int m_maximum;
	bool m_hasColor;
	int m_fontWidth;
	int m_fontHeight;
	bool	m_hasFont;
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
