#pragma once
#include "afxwin.h"

class CNumberInputClientDlg;
// CConfigDlg 대화 상자입니다.

class CConfigDlg : public CDialog
{
	DECLARE_DYNAMIC(CConfigDlg)

public:
	CConfigDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CConfigDlg();


// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_CONFIG };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	void SetParent(CNumberInputClientDlg* p) { m_parent = p; }
	CEdit m_editKey;
	CEdit m_editIP;
	CEdit m_editPort;

	CString m_strPath;
	CNumberInputClientDlg* m_parent;
};
