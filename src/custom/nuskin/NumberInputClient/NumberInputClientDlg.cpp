// NumberInputClientDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include <afxsock.h>		// MFC 소켓 확장


#include <windows.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
 
#include "NumberInputClient.h"
#include "NumberInputClientDlg.h"
#include "ConfigDlg.h"
#include "macUtil.h"

#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0
#	define MAX_COUNT				5

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CNumberInputClientDlg 대화 상자




CNumberInputClientDlg::CNumberInputClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNumberInputClientDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_serverIp="";
	m_port=14003;
	m_lastIndex=0;
}

void CNumberInputClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_INPUT, m_editInput);
	DDX_Control(pDX, IDC_EDIT_OUTPUT1, m_editOutput1);
	DDX_Control(pDX, IDC_EDIT_OUTPUT2, m_editOutput2);
	DDX_Control(pDX, IDC_EDIT_OUTPUT3, m_editOutput3);
	DDX_Control(pDX, IDC_EDIT_OUTPUT4, m_editOutput4);
	DDX_Control(pDX, IDC_EDIT_OUTPUT5, m_editOutput5);
	DDX_Control(pDX, IDC_BUTTON_SEND, m_btSend);
	DDX_Control(pDX, IDC_BUTTON_DEL1, m_btDel1);
	DDX_Control(pDX, IDC_BUTTON_DEL2, m_btDel2);
	DDX_Control(pDX, IDC_BUTTON_DEL3, m_btDel3);
	DDX_Control(pDX, IDC_BUTTON_DEL4, m_btDel4);
	DDX_Control(pDX, IDC_BUTTON_DEL5, m_btDel5);
	DDX_Control(pDX, IDC_BUTTON_CONFIG, m_btConfig);
	DDX_Control(pDX, IDC_BUTTON_NEXT, m_btNext);
	DDX_Control(pDX, IDC_BUTTON_PREV, m_btPrev);
}

BEGIN_MESSAGE_MAP(CNumberInputClientDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_SEND, &CNumberInputClientDlg::OnBnClickedButtonSend)
	ON_BN_CLICKED(IDC_BUTTON_DEL1, &CNumberInputClientDlg::OnBnClickedButtonDel1)
	ON_BN_CLICKED(IDC_BUTTON_DEL2, &CNumberInputClientDlg::OnBnClickedButtonDel2)
	ON_BN_CLICKED(IDC_BUTTON_DEL3, &CNumberInputClientDlg::OnBnClickedButtonDel3)
	ON_BN_CLICKED(IDC_BUTTON_DEL4, &CNumberInputClientDlg::OnBnClickedButtonDel4)
	ON_BN_CLICKED(IDC_BUTTON_DEL5, &CNumberInputClientDlg::OnBnClickedButtonDel5)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG, &CNumberInputClientDlg::OnBnClickedButtonConfig)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &CNumberInputClientDlg::OnBnClickedButtonNext)
	ON_BN_CLICKED(IDC_BUTTON_PREV, &CNumberInputClientDlg::OnBnClickedButtonPrev)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CNumberInputClientDlg 메시지 처리기

BOOL CNumberInputClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	GetConfigInfo();

	this->m_editInput.SetParent(this);
	this->m_editInput.SetMaximum(4);

	m_editOutput1.SetFontSize(50,80);
	m_editOutput1.HasColor(true);
	m_editOutput1.SetParent(this);

	m_editOutputArray[0] = &m_editOutput1;
	m_editOutputArray[1] = &m_editOutput2;
	m_editOutputArray[2] = &m_editOutput3;
	m_editOutputArray[3] = &m_editOutput4;
	m_editOutputArray[4] = &m_editOutput5;


	HBITMAP sendBitMap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP_SEND));
	m_btSend.SetBitmap(sendBitMap);

	HBITMAP nextBitMap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP_NEXT));
	m_btNext.SetBitmap(nextBitMap);

	HBITMAP prevBitMap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP_PREV));
	m_btPrev.SetBitmap(prevBitMap);

	HBITMAP deleteBitMap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP_DELETE));
	this->m_btDel1.SetBitmap(deleteBitMap);
	this->m_btDel2.SetBitmap(deleteBitMap);
	this->m_btDel3.SetBitmap(deleteBitMap);
	this->m_btDel4.SetBitmap(deleteBitMap);
	this->m_btDel5.SetBitmap(deleteBitMap);

	pWaveInfo=FindResource(NULL,(char *)(IDR_WAVE2),"WAVE");
	hWaveRes=LoadResource(NULL,(struct HRSRC__ *)pWaveInfo);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CNumberInputClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CNumberInputClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CNumberInputClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CNumberInputClientDlg::OnBnClickedButtonDel(int idx)
{
	CString buf;
	m_editOutputArray[idx]->GetWindowTextA(buf);

	if(buf.IsEmpty()) return;

	char sendStr[20];
	memset(sendStr,0x00,20);
	sprintf(sendStr, "<%05d", atol(buf));

	CString header;
	makeHeader(header);

	CString retval;
	if(!socketAgent3(m_serverIp,m_port,header,sendStr,retval)){
		CString errMsg;
		errMsg.Format("Server(%s:%d) ERROR %s", m_serverIp,m_port, retval);
		AfxMessageBox(errMsg);
		return;
	}

	TraceLog(("Retval=%s", retval));
	m_editOutputArray[idx]->SetWindowText("");

	CString prev;
	for(int i=idx;i<MAX_COUNT-1;i++){
		this->m_editOutputArray[i+1]->GetWindowText(prev);
		this->m_editOutputArray[i]->SetWindowText(prev);
	}

	if(this->m_strList.size() >0){
		std::string next = this->m_strList.front();
		TraceLog(("%s data in Queue", next.c_str()));
		this->m_editOutputArray[MAX_COUNT-1]->SetWindowText(next.c_str());
		this->m_strList.pop_front();
	}else{
		this->m_editOutputArray[MAX_COUNT-1]->SetWindowText("");
	}
	
	return;
}


void CNumberInputClientDlg::OnBnClickedButtonSend()
{
	if(m_serverIp.IsEmpty()){
		CString errMsg;
		errMsg.Format("Please Set Server IP First");
		AfxMessageBox(errMsg);
		return;
	}
	if(m_key.IsEmpty()){
		CString errMsg;
		errMsg.Format("Please Set License Key First");
		AfxMessageBox(errMsg);
		return;
	}

	CString buf;
	m_editInput.GetWindowTextA(buf);

	if(buf.IsEmpty()) return;

	char sendStr[20];
	memset(sendStr,0x00,20);
	sprintf(sendStr, ">%05d", atol(buf));

	CString header;
	makeHeader(header);

	CString retval;
	if(!socketAgent3(m_serverIp,m_port,header,sendStr,retval)){
		CString errMsg;
		errMsg.Format("Server(%s:%d) ERROR %s", m_serverIp,m_port, retval);
		AfxMessageBox(errMsg);
		return;
	}

	TraceLog(("Retval=%s", retval));

	//m_editInput.SetWindowText("");

	if(IsAlreayExist(buf)==false){

		CString current;
		this->m_editOutputArray[0]->GetWindowText(current);
		if(current.IsEmpty()){
			this->m_editOutputArray[0]->SetWindowText(buf);
		}else{
			CString last;
			this->m_editOutputArray[MAX_COUNT-1]->GetWindowText(last);
			if(!last.IsEmpty()){
				this->m_strList.push_front(last.GetBuffer());
				TraceLog(("%s pushed Queue have %d ele", last, this->m_strList.size()));

			}

			CString prev;
			for(int i=MAX_COUNT-1;i>0;i--){
				this->m_editOutputArray[i-1]->GetWindowText(prev);
				if(prev.IsEmpty()){
					continue;
				}
				this->m_editOutputArray[i]->SetWindowText(prev);
			}
			this->m_editOutputArray[0]->SetWindowText(buf);
		}

	}

	LPSTR lpWaveRes=(char *)LockResource(hWaveRes);
	sndPlaySound(lpWaveRes, SND_MEMORY | SND_SYNC);
	UnlockResource(hWaveRes);
	//PlaySound((LPCSTR)MAKEINTRESOURCE(IDR_WAVE1), NULL, SND_SYNC);
	  //PlaySound("sample.wav", NULL, SND_SYNC);
	  //PlaySound(0, 0, 0);

	return;
}

bool CNumberInputClientDlg::IsAlreayExist(CString& buf)
{
	std::list<std::string>::iterator itr;
	for(itr=m_strList.begin();itr!=m_strList.end();itr++){
		if(atoi(buf) == atoi(itr->c_str())){
			TraceLog(("%s is exist"));
			return true;
		}
	}

	for(int i=0;i<MAX_COUNT;i++){
		CString prev;
		this->m_editOutputArray[i]->GetWindowText(prev);
		if(prev.IsEmpty()){
			continue;
		}
		if(atoi(buf) == atoi(prev)){
			TraceLog(("%s is exist"));
			return true;
		}
	}
	return false;
}

void CNumberInputClientDlg::OnBnClickedButtonDel1()
{
	OnBnClickedButtonDel(0);
}
void CNumberInputClientDlg::OnBnClickedButtonDel2()
{
	OnBnClickedButtonDel(1);
}

void CNumberInputClientDlg::OnBnClickedButtonDel3()
{
	OnBnClickedButtonDel(2);
}

void CNumberInputClientDlg::OnBnClickedButtonDel4()
{
	OnBnClickedButtonDel(3);
}

void CNumberInputClientDlg::OnBnClickedButtonDel5()
{
	OnBnClickedButtonDel(4);
}

void CNumberInputClientDlg::OnBnClickedButtonConfig()
{
	CConfigDlg dlg;
	dlg.SetParent(this);

	if( dlg.DoModal() == IDOK )
	{
	}
}


bool 
CNumberInputClientDlg::GetConfigInfo()
{
	char szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(szModule, cDrive, cPath, cFilename, cExt);

	CString m_strPath = "";
	m_strPath.Format(_T("%s%s\\UBCNumber.ini"), cDrive, cPath);

	char	szValue[1024];

	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString("ROOT","SERVERIP","",szValue,sizeof(szValue),m_strPath);
	this->m_serverIp = szValue;

	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString("ROOT","PORT","14003",szValue,sizeof(szValue),m_strPath);
	this->m_port = atoi(szValue);

	memset(szValue, '\0', sizeof(szValue));
	GetPrivateProfileString("ROOT","KEY","",szValue,sizeof(szValue),m_strPath);
	this->m_key = szValue;

	std::string mac = macUtil::getInstance()->GetMacaddr();
	if(mac.empty()){
		CString errMsg;
		errMsg.Format("Fail to get macAddress");
		AfxMessageBox(errMsg);
		return false;
	}
	this->m_mac = mac.c_str();

	return true;
}


bool 
CNumberInputClientDlg::socketAgent3(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data,
				 CString& retval)
 {
	
	TraceLog(("socketAgent3(%s,%d,%s,%s)\n", ipAddress,portNo,directive,data));

	 // Load WinSock DLL 
 	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA       WsaData;            
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {		
		retval = "Can't open socket.\n";
		TraceLog((retval));
		WSACleanup();	
		return false;
	}

	SOCKET socket_fd;		
	if ((socket_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		retval = "Can't open socket.\n";
		TraceLog((retval));
		WSACleanup();	
		return false;
	}

	struct sockaddr_in server_addr;	
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(ipAddress);
	server_addr.sin_port = htons(portNo);

	TraceLog(("--------------------%u\n",server_addr.sin_port ));
	
	if (connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
		retval.Format("Can't connect IP server.[%s:%d]\n", ipAddress, portNo);
		TraceLog((retval));
		closesocket(socket_fd);
		WSACleanup();	
		return false;
	}
	TraceLog(("socket connect (%s,%d)\n", ipAddress, portNo));

	CString value = directive;
	//value += "\n";
	value += data;
	value += "\n";

	TraceLog(("send start (%s)\n", value));
	
	int msg_size;
	if ((msg_size = send(socket_fd, value, value.GetLength(), 0)) <= 0) {
		retval.Format("Can't send to server.[%s]\n", ipAddress);
		TraceLog((retval));
		WSACleanup();	
		closesocket(socket_fd);
		return false;
	}
	TraceLog(("send end\n"));

	timeval ReceiveTimeout;
	ReceiveTimeout.tv_sec  = 30;
	ReceiveTimeout.tv_usec = 0;

	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(socket_fd, &fds);
		
	TraceLog(("Wait for return ...\n"));
	int iRC = select(2, &fds, NULL, NULL, &ReceiveTimeout);
	// Timeout
	if(iRC==0) {
		TraceLog(("TimeOut!!!\n"));
		closesocket(socket_fd);
		WSACleanup();	
		return true;
	}
	// Error
	if(iRC < 0) {
		retval = "Select Error!!!\n";
		TraceLog((retval));
		closesocket(socket_fd);
		WSACleanup();	
		return false;
	}
	if(!FD_ISSET(socket_fd, &fds)){
		retval = "Somthing wrong!!!\n";
		TraceLog((retval));
		closesocket(socket_fd);
		WSACleanup();	
		return false;
	}
	
	// Receive OK
	TraceLog(("recv start\n"));
	char buf[1024+1] = {0};
	memset(buf,0x00, sizeof(buf));
	if ((msg_size = recv(socket_fd, buf, 1024, 0)) <= 0) {
		TraceLog(("Can't recv from server.[%s]\n", ipAddress));
		closesocket(socket_fd);
		WSACleanup();	
		return false;
	}
	TraceLog(("recv end (%s)\n", buf));
	retval = buf;

	if(retval.GetLength() > 5 && retval.Mid(0,5) == "ERROR"){
		closesocket(socket_fd);
		WSACleanup();		
		return false;
	}
	
	closesocket(socket_fd);
	WSACleanup();		
	return true;
}

void  
CNumberInputClientDlg::makeHeader(CString& header)
{
	header.Format("putQ:%17.17s:%15.15s:", this->m_mac, this->m_key);
	TraceLog(("header=%s", header));
}

void CNumberInputClientDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

void CNumberInputClientDlg::OnBnClickedButtonNext()
{
	CString buf;
	m_editInput.GetWindowText(buf);

	//if(buf.IsEmpty()) return;

	int value = atoi(buf);
	value++;

	TraceLog(("value=%d", value));

	buf.Format("%d", value);
	m_editInput.SetWindowText(buf);
}

void CNumberInputClientDlg::OnBnClickedButtonPrev()
{
	CString buf;
	m_editInput.GetWindowText(buf);


	int value = atoi(buf);
	value--;
	if(value <= 0) return;

	TraceLog(("value=%d", value));

	buf.Format("%d", value);
	m_editInput.SetWindowText(buf);
}

void CNumberInputClientDlg::OnDestroy()
{
	CDialog::OnDestroy();
   FreeResource(hWaveRes); 
}
