// NumberCallerDlg.h : 헤더 파일
//

#pragma once
#define MAX_NODE 16

class CNumberBlock;
// CNumberCallerDlg 대화 상자
class CNumberCallerDlg : public CDialog
{
// 생성입니다.
public:
	CNumberCallerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NUMBERCALLER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.

	void	Clear();


protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()

protected:
	CNumberBlock* m_nodeArray[MAX_NODE];
	CNumberBlock* m_noticeNode;
public:
	virtual BOOL DestroyWindow();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
