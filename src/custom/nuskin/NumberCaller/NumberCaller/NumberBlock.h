#pragma once

#define	NODE_COLOR_GRAY		RGB(238,238,238)
#define NODE_COLOR_YELLOW	RGB(0,255,255)
#define NODE_COLOR_WHITE	RGB(255,255,255)
#define NODE_COLOR_BLACK	RGB(0,0,0)

class CNumberBlock : public CStatic
{
protected:
	CWnd*		m_parent;
	int			m_number;

	COLORREF	m_bgColor;

	CFont	m_font;
	CString m_fontName;
	int		m_fontWidth;
	int		m_fontHeight;
	bool	m_hasFont;

public:
	CNumberBlock(CWnd* parent);
	virtual ~CNumberBlock();

	bool	CreateIt(int x, int y, int width, int height);

	void SetBGColor(COLORREF bgColor) { m_bgColor = bgColor; }
	void SetFontSize(int w, int h) { m_fontWidth = w; m_fontHeight=h; }
	void SetFontName(LPCTSTR f) { m_fontName = f; }

	void SetNumber(int n) { m_number = n; }

	afx_msg void OnDestroy();
	afx_msg void OnPaint();


	DECLARE_MESSAGE_MAP()

public:


};