#include "stdafx.h"
#include "resource.h"
#include "NumberBlock.h"


CNumberBlock::CNumberBlock(CWnd* pParent)
{
	m_parent = pParent;
	m_number = 10;
	
	m_bgColor = NODE_COLOR_BLACK;
	
	m_fontWidth = 90;
	m_fontHeight = 150;

	m_hasFont = false;

	m_fontName = "Tahoma";

}

CNumberBlock::~CNumberBlock()
{
}

BEGIN_MESSAGE_MAP(CNumberBlock, CStatic)
	//{{AFX_MSG_MAP(CNumberBlock)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CNumberBlock::OnPaint() 
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(rect);
	dc.FillSolidRect(rect, m_bgColor);

	if(!m_hasFont){
		m_font.CreateFont( m_fontHeight, // nHeight 
						   m_fontWidth, // nWidth 
						   0, // nEscapement 
						   0, // nOrientation 
						   1, // nWeight 
						   0, // bItalic 
						   0, // bUnderline 
						   0, // cStrikeOut 
						   0, // nCharSet 
						   OUT_DEFAULT_PRECIS, // nOutPrecision 
						   0, // nClipPrecision 
						   DEFAULT_QUALITY, // nQuality
						   DEFAULT_PITCH | FF_DONTCARE,  // nPitchAndFamily 
						  (LPCTSTR)m_fontName ); // lpszFacename 	
	}
	this->SetFont(&m_font);
	m_hasFont = true;


	if(m_number > 0){
		CString strNumber;
		strNumber.Format("%d", m_number);
		this->SetWindowText(strNumber);
		//::DrawTextA(dc, strNumber, -1, &rect, DT_SINGLELINE|DT_VCENTER|DT_CENTER);

	}

	CStatic::OnPaint();
}

void CNumberBlock::OnDestroy() 
{
	CStatic::OnDestroy();
	//CButton::OnDestroy();
}

bool CNumberBlock::CreateIt(int x, int y, int width, int height)
{
	CPoint point(x,y);
	CSize size(width, height);
	CRect rect(point,size);
	//ScreenToClient(rect);

	bool retval = this->Create(NULL, 
					WS_CHILD|WS_VISIBLE|BS_OWNERDRAW|SS_NOTIFY|SS_CENTER,
					rect,
					m_parent,
					IDC_STATIC_1);

	this->ModifyStyleEx(0,WS_EX_STATICEDGE,0);
	
	return retval;
}