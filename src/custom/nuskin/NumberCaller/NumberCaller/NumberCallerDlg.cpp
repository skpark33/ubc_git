// NumberCallerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "NumberCaller.h"
#include "NumberCallerDlg.h"
#include "NumberBlock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif




// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CNumberCallerDlg 대화 상자




CNumberCallerDlg::CNumberCallerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNumberCallerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CNumberCallerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CNumberCallerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CNumberCallerDlg 메시지 처리기

BOOL CNumberCallerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	int width = 1920;
	int height = 720;
	int y_offset = 180; // 위에서 떨어진 정도
	int x_offset = 0; // 옆에서 떨어진 정도

	CRect rect(x_offset,y_offset,width+x_offset,height+y_offset);
	ScreenToClient(rect);

	this->MoveWindow(&rect);

	int margin = 8; // 각 블럭은 8씩 띤다.
	int init_margin = margin*3 ; // 그러나 처음과 끝은 그 3배를 띤다.
	int block_count = 4; // 행,열 모두 블럭은 4개씩 있다.
	
	//이때  개별 블럭의 넓이와 높이는 다음의 식에 의해 구해진다.
	int block_width = ( width - (init_margin*2) - (margin*3) ) / block_count;  // 462 이다.
	int block_height = ( height - (init_margin*2) - (margin*3) ) / block_count;  // 162 이다.


	for(int i=0;i<MAX_NODE;i++){
		m_nodeArray[i] = new CNumberBlock(this) ;
		//4x4 형태의 매트릭스좌표는 다음의 식에 의해 구해진다.
		int x = init_margin + ((block_width+margin)*(i/block_count));
		int y = init_margin + ((block_height+margin)*(i%block_count));

		m_nodeArray[i]->CreateIt(x,y,block_width, block_height);
		m_nodeArray[i]->ShowWindow(SW_SHOW);
	}

	/*
	m_noticeNode = new CNumberBlock(this) ;
	m_noticeNode->SetBGColor(NODE_COLOR_GRAY);
	m_noticeNode->CreateIt((1920-720)/2,(720-360)/2,720,360);  // 정중앙에 딱 1/4 면적으로 나온다.
	m_noticeNode->ShowWindow(SW_SHOW);
	*/
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CNumberCallerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CNumberCallerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

void CNumberCallerDlg::Clear()
{
	for(int i=0;i<MAX_NODE;i++){
		if(m_nodeArray[i])  delete m_nodeArray[i];
	}
	if(m_noticeNode) delete m_noticeNode;
}

BOOL CNumberCallerDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	Clear();
	return CDialog::DestroyWindow();
}

HBRUSH CNumberCallerDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

  switch(nCtlColor)
    {
    case CTLCOLOR_STATIC:
        {
           //if(pWnd->GetDlgCtrlID() != IDC_EDIT9)
	           pDC->SetTextColor(NODE_COLOR_WHITE);
            pDC->SetBkColor(NODE_COLOR_BLACK);
			break;
        }
    }
return hbr;
}
