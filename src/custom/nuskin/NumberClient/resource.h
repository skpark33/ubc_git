//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NumberClient.rc
//
#define IDD_NUMBERCLIENT_DIALOG         102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_SEND                 130
#define IDB_BITMAP_CLEAR                131
#define IDC_FLASH_FRAME                 1000
#define IDC_EDIT1                       1001
#define IDC_BUTTON1                     1002
#define IDC_BUTTON_SEND                 1002
#define IDC_BUTTON_CLEAR                1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
