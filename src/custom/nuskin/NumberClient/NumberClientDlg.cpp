// NumberClientDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "NumberClient.h"
#include "NumberClientDlg.h"

#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMyEdit;
IMPLEMENT_DYNAMIC(CMyEdit, CEdit)

BEGIN_MESSAGE_MAP(CMyEdit, CEdit)
	ON_WM_PAINT()
END_MESSAGE_MAP()

CMyEdit::CMyEdit()
: m_parent(NULL)
{

	m_font.CreateFont( 58, // nHeight 
					   36, // nWidth 
					   0, // nEscapement 
					   0, // nOrientation 
					   1, // nWeight 
					   0, // bItalic 
					   0, // bUnderline 
					   0, // cStrikeOut 
					   0, // nCharSet 
					   OUT_DEFAULT_PRECIS, // nOutPrecision 
					   0, // nClipPrecision 
					   DEFAULT_QUALITY, // nQuality
					   DEFAULT_PITCH | FF_DONTCARE,  // nPitchAndFamily 
					   "Tahoma" ); // lpszFacename 
}

CMyEdit::~CMyEdit()
{
	m_font.DeleteObject();
}

void
CMyEdit::OnPaint()
{
	this->SetFont(&m_font);
	CEdit::OnPaint();
}
BOOL CMyEdit::PreTranslateMessage(MSG* pMsg)
{	
	if(!m_parent) 
	{
		return CEdit::PreTranslateMessage(pMsg);
	}

	if(pMsg->hwnd == this->GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			TraceLog(("PreTranslateMessage : %c", (TCHAR)pMsg->wParam));
			if(((TCHAR)pMsg->wParam) == '\n')
			{
				if(m_parent) {
					m_parent->OnBnClickedButtonSend();
				}
				return TRUE;
			}
			this->GetWindowTextA(m_str);

			if(m_str.GetLength() >= m_maximum){
				return TRUE;
			}
		}
	}

	return CEdit::PreTranslateMessage(pMsg);
}

// CNumberClientDlg 대화 상자




CNumberClientDlg::CNumberClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNumberClientDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pFlash = 0;

}

void CNumberClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FLASH_FRAME, m_ctrlPictureFrame);
	DDX_Control(pDX, IDC_EDIT1, m_editInputNumber);
	DDX_Control(pDX, IDC_BUTTON_SEND, m_btSend);
	DDX_Control(pDX, IDC_BUTTON_CLEAR, m_btClear);
}

BEGIN_MESSAGE_MAP(CNumberClientDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BUTTON_SEND, &CNumberClientDlg::OnBnClickedButtonSend)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR, &CNumberClientDlg::OnBnClickedButtonClear)
END_MESSAGE_MAP()


// CNumberClientDlg 메시지 처리기

//#define FLASH_WIDTH		239
//#define FLASH_HEIGHT	540
#define FLASH_WIDTH		0
#define FLASH_HEIGHT	0
#define EDIT_HEIGHT		60
#define WINDOW_WIDTH	(128*2+8)
#define MARGIN			(EDIT_HEIGHT+128+36)


BOOL CNumberClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	this->SetWindowText("UBC NumberClient");
	this->MoveWindow(0,0,WINDOW_WIDTH,FLASH_HEIGHT+MARGIN);
	ShowWindow(SW_NORMAL);
	Invalidate();

	m_editInputNumber.SetParent(this);
	m_editInputNumber.SetMaximum(5);
	m_editInputNumber.MoveWindow(6,FLASH_HEIGHT+4,FLASH_WIDTH, EDIT_HEIGHT);

	m_btSend.MoveWindow(1,FLASH_HEIGHT+EDIT_HEIGHT+5,128, 128);
	HBITMAP sendBitMap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP_SEND));
	m_btSend.SetBitmap(sendBitMap);

	m_btClear.MoveWindow(128+1,FLASH_HEIGHT+EDIT_HEIGHT+5,128, 128);
	HBITMAP clearBitMap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP_CLEAR));
	m_btClear.SetBitmap(clearBitMap);

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	//Play();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CNumberClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CNumberClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}




BOOL CNumberClientDlg::Play()
{
	static int id = 0xd000;

	m_pFlash = new CShockwaveFlash();

	CRect rectParent;
	GetClientRect(&rectParent);
	int height = rectParent.Height();
	int width = rectParent.Width();

	int x = (width-FLASH_WIDTH)/2;
	int y = (height-FLASH_HEIGHT)/2;

	CRect client_rect;
	//GetClientRect(&client_rect);

	//m_ctrlPictureFrame.MoveWindow(x, y, FLASH_WIDTH, FLASH_HEIGHT);
	m_ctrlPictureFrame.MoveWindow(6, 0, FLASH_WIDTH, FLASH_HEIGHT);

	m_ctrlPictureFrame.GetClientRect(&client_rect);
	
	BOOL ret = m_pFlash->Create("Flash", WS_CHILD | WS_VISIBLE, client_rect, &m_ctrlPictureFrame, id--);

	if(ret == FALSE)
	{
		MessageBoxA("Fail to create Flash control !!!");
		return false;
	}
	CString swf = GetCurDir();
	swf.Append("\\embeddedFlash.swf");

	m_pFlash->LoadMovie(0, swf);
	m_pFlash->ShowWindow(SW_SHOW);
	return true;
}

CString CNumberClientDlg::GetCurDir()
{
	CString strDir;
	char	szBuf[_MAX_PATH] = { 0x00 };

	::GetModuleFileName(NULL, szBuf, _MAX_PATH);
	strDir.Format("%s", szBuf);
	strDir.MakeReverse();
	int nIndex = strDir.Find("\\");
	if (nIndex < 0) return "";

	if (strDir.GetLength() < (nIndex+1)) return "";

	strDir = strDir.Mid(nIndex + 1);
	strDir.MakeReverse();

	return strDir;
}

void CNumberClientDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
/*
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(m_pFlash->GetSafeHwnd())
	{
		m_pFlash->MoveWindow(0,0,cx,cy);
	}
	*/
}

void CNumberClientDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	lpMMI->ptMaxSize.x = WINDOW_WIDTH;
	lpMMI->ptMaxSize.y = FLASH_HEIGHT+MARGIN;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CNumberClientDlg::OnBnClickedButtonSend()
{
	CString buf;
	m_editInputNumber.GetWindowTextA(buf);

	if(buf.IsEmpty()) return;

	char sendStr[20];
	memset(sendStr,0x00,20);
	sprintf(sendStr, ">%05d", atol(buf));

	if(CNumberClientApp::_currentSerialServerIp.empty()){
		CNumberClientApp::getCurrentSerialServerIP();
	}
	
	CString retval;
	socketAgent3(CNumberClientApp::_currentSerialServerIp.c_str(),
				14003,
				"putQ:",
				sendStr,
				retval);

	TraceLog(("Retval=%s", retval));

}

void CNumberClientDlg::OnBnClickedButtonClear()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


bool 
CNumberClientDlg::socketAgent3(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data,
				 CString& retval)
 {
	
	TraceLog(("socketAgent3(%s,%d,%s,%s)\n", ipAddress,portNo,directive,data));

	 // Load WinSock DLL 
 	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA       WsaData;            
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {		
		WSACleanup();	
		return false;
	}

	SOCKET socket_fd;		
	if ((socket_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		TraceLog(("Can't open socket.\n"));
		WSACleanup();	
		return false;
	}

	struct sockaddr_in server_addr;	
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(ipAddress);
	server_addr.sin_port = htons(portNo);

	TraceLog(("--------------------%u\n",server_addr.sin_port ));
	
	if (connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
		TraceLog(("Can't connect IP server.[%s:%d]\n", ipAddress, portNo));
		closesocket(socket_fd);
		WSACleanup();	
		return false;
	}
	TraceLog(("socket connect (%s,%d)\n", ipAddress, portNo));

	string value = directive;
	//value += "\n";
	value += data;
	value += "\n";

	TraceLog(("send start (%s)\n", value.c_str()));
	
	int msg_size;
	if ((msg_size = send(socket_fd, value.c_str(), value.size(), 0)) <= 0) {
		TraceLog(("Can't send to server.[%s]\n", ipAddress));
		WSACleanup();	
		closesocket(socket_fd);
		return false;
	}
	TraceLog(("send end\n"));

	timeval ReceiveTimeout;
	ReceiveTimeout.tv_sec  = 30;
	ReceiveTimeout.tv_usec = 0;

	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(socket_fd, &fds);
		
	TraceLog(("Wait for return ...\n"));
	int iRC = select(2, &fds, NULL, NULL, &ReceiveTimeout);
	// Timeout
	if(iRC==0) {
		TraceLog(("TimeOut!!!\n"));
		closesocket(socket_fd);
		WSACleanup();	
		return true;
	}
	// Error
	if(iRC < 0) {
		TraceLog(("Select Error!!!\n"));
		closesocket(socket_fd);
		WSACleanup();	
		return false;
	}
	if(!FD_ISSET(socket_fd, &fds)){
		TraceLog(("Somthing wrong!!!\n"));
		closesocket(socket_fd);
		WSACleanup();	
		return false;
	}
	
	// Receive OK
	TraceLog(("recv start\n"));
	char buf[1024+1] = {0};
	memset(buf,0x00, sizeof(buf));
	if ((msg_size = recv(socket_fd, buf, 1024, 0)) <= 0) {
		TraceLog(("Can't recv from server.[%s]\n", ipAddress));
		closesocket(socket_fd);
		WSACleanup();	
		return false;
	}
	TraceLog(("recv end (%s)\n", buf));
	retval = buf;
	
	closesocket(socket_fd);
	WSACleanup();		

	return true;
}
