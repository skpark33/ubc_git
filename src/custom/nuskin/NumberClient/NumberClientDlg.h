// NumberClientDlg.h : 헤더 파일
//

#pragma once
#include "WebBrowser/ShockwaveFlash.h"
#include "afxwin.h"


#define TraceLog(_f_) \
	{ \
		CString szTraceLogBuf; \
		szTraceLogBuf.Format(_T("%s(%d) : "), __FILE__, __LINE__); \
		szTraceLogBuf.AppendFormat _f_; \
		OutputDebugString(szTraceLogBuf); \
	}

class CNumberClientDlg ;
class CMyEdit : public CEdit
{
	DECLARE_DYNAMIC(CMyEdit)
public:
	CMyEdit();
	virtual ~CMyEdit();
	afx_msg void OnPaint() ;

	void SetParent(CNumberClientDlg* p) { m_parent = p; }
	CString GetString() { return m_str; }
	void SetMaximum(int p) { m_maximum = p; }
protected:
	DECLARE_MESSAGE_MAP()

	CFont m_font;
	CNumberClientDlg* m_parent;
	CString m_str;
	int m_maximum;
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};

// CNumberClientDlg 대화 상자
class CNumberClientDlg : public CDialog
{
// 생성입니다.
public:
	CNumberClientDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NUMBERCLIENT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	BOOL Play();
	CShockwaveFlash* m_pFlash;

	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString GetCurDir();
	CStatic m_ctrlPictureFrame;
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	CMyEdit m_editInputNumber;
	CButton m_btSend;
	CButton m_btClear;

	afx_msg void OnBnClickedButtonSend();
	afx_msg void OnBnClickedButtonClear();

	bool socketAgent3(const char* ipAddress,
				 int portNo, 
				 const char* directive, 
				 const char* data,
				 CString& retval);
};
