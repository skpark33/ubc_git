//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCWeddingImporter.rc
//
#define IDS_ABOUTBOX                    101
#define IDR_MAINFRAME                   129
#define IDB_LIST_HEADER                 130
#define IDB_CHECK                       131
#define IDB_BTN_CANCEL                  132
#define IDB_BTN_APPLY                   133
#define IDD_EXIT_DIALOG                 134
#define IDD_UBCWEDDINGIMPORTER_DIALOG   145
#define IDC_TXT_MSG                     1001
#define IDC_LC_LIST                     1007
#define IDS_MSG_001                     2102
#define IDS_MSG_003                     2103
#define IDS_SELECT_SCHEDULE             2119
#define IDS_CANCEL_SCHEDULE             2120
#define IDS_OPEN_LIST1                  2121
#define IDS_OPEN_LIST2                  2122

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
