#pragma once

class CEjectRemovableDrive
{
private:
	static CEjectRemovableDrive* _instance;

	DWORD GetDrivesDevInstByDeviceNumber(long nDeviceNumber, UINT DriveType, char* szDosDeviceName);
public:
	CEjectRemovableDrive(void) {};
	virtual ~CEjectRemovableDrive(void)
	{
		if(_instance != NULL)
		{
			delete _instance;
			_instance = NULL;
		}
	}

	static CEjectRemovableDrive* GetInstance()
	{
		if(_instance == NULL) _instance = new CEjectRemovableDrive();
		return _instance;
	}

	bool EjectDrive(char chDriveLetter, bool bMessage=true);
};
