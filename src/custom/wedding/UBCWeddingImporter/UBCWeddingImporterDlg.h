// UBCWeddingImporterDlg.h : 헤더 파일
//

#pragma once

#include "common\UTBListCtrl.h"
#include "common\hoverbutton.h"

// CUBCWeddingImporterDlg 대화 상자
class CUBCWeddingImporterDlg : public CDialog
{
	enum { eSelect, eDrive, eScheduleName };

// 생성입니다.
public:
	CUBCWeddingImporterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

	CUTBListCtrl m_lcList;
	CImageList	 m_imgForList;
	CHoverButton m_bnOK;
	CHoverButton m_bnCancel;

	void FindSchedule(CStringArray& astrScheduleList);
	void InitList();
	bool ScheduleCopy(CString strSrcPath, CString strTarPath);

	static int CALLBACK CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCWEDDINGIMPORTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

public:
	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnPaint();
	afx_msg void OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnColumnclickLcList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
};
