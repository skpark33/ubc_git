#pragma once

// CExitDlg 대화 상자입니다.

class CExitDlg : public CDialog
{
	DECLARE_DYNAMIC(CExitDlg)

public:
	CExitDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CExitDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_EXIT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
};
