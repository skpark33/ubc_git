// UBCWeddingImporterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCWeddingImporter.h"
#include "UBCWeddingImporterDlg.h"
#include "EjectRemovableDrive.h"
#include "ExitDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CUBCWeddingImporterDlg::CUBCWeddingImporterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCWeddingImporterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUBCWeddingImporterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LC_LIST, m_lcList);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
}

BEGIN_MESSAGE_MAP(CUBCWeddingImporterDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CUBCWeddingImporterDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CUBCWeddingImporterDlg::OnBnClickedCancel)
	ON_NOTIFY(NM_CLICK, IDC_LC_LIST, &CUBCWeddingImporterDlg::OnNMClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LC_LIST, &CUBCWeddingImporterDlg::OnNMDblclkList)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LC_LIST, &CUBCWeddingImporterDlg::OnLvnColumnclickLcList)
END_MESSAGE_MAP()


// CUBCWeddingImporterDlg 메시지 처리기

BOOL CUBCWeddingImporterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	CBitmap bmp;
	bmp.LoadBitmap(IDB_CHECK);

	if(m_imgForList.Create(16, 15, ILC_COLORDDB | ILC_MASK, 1, 1))
	{
		m_imgForList.Add(&bmp, RGB(255,255,255));
	}
	m_lcList.SetImageList(&m_imgForList, LVSIL_SMALL);

	m_lcList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_SUBITEMIMAGES | LVS_EX_GRIDLINES);
	m_lcList.InsertColumn(eSelect      , _T("")                          , LVCFMT_LEFT, 24);
	m_lcList.InsertColumn(eDrive       , LoadStringFromID(IDS_OPEN_LIST1), LVCFMT_LEFT, 80);
	m_lcList.InsertColumn(eScheduleName, LoadStringFromID(IDS_OPEN_LIST2), LVCFMT_LEFT, 300);
	m_lcList.InitHeader(IDB_LIST_HEADER);

	//버튼위 위치 조정
	m_bnOK.LoadBitmap(IDB_BTN_APPLY, RGB(255, 255, 255));
	m_bnOK.SetToolTipText(IDS_SELECT_SCHEDULE);

	m_bnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_bnCancel.SetToolTipText(IDS_CANCEL_SCHEDULE);

	InitList();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCWeddingImporterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCWeddingImporterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CUBCWeddingImporterDlg::OnBnClickedOk()
{
	POSITION pos = m_lcList.GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		AfxMessageBox(IDS_MSG_001);
		return;
	}

	int nItem = m_lcList.GetNextSelectedItem(pos);
	CString strDrive = m_lcList.GetItemText(nItem, eDrive);
	CString strScheduleName = m_lcList.GetItemText(nItem, eScheduleName);

	// KILL 브라우져
	//CString strKillPath;
	//strKillPath.Format("%s%s%s", "C:\\", EXCUTE_PATH, "ubckill.bat");
	//UINT nRet = WinExec(strKillPath, SW_HIDE);
	//Sleep(1000);

	// 컨텐츠 COPY
	CString strSrcPath;
	strSrcPath.Format("%s%sdata_%s"
					, strDrive
					, DATA_DEFAULT_PATH
					, strScheduleName
					);

	CString strTarPath;
	strTarPath.Format(_T("C:\\%sdata"), DATA_DEFAULT_PATH);

	if(!ScheduleCopy(strSrcPath, strTarPath)) return;

	CExitDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		// usb 제거
		CEjectRemovableDrive::GetInstance()->EjectDrive(strDrive[0]);
	}

	// 브라우져 재기동
	//CString strStarterPath;
	//strStarterPath.Format("%s%s%s +log +ignore_env", "C:\\", EXCUTE_PATH, EXCUTE_FILE);

	//nRet = WinExec(strStarterPath, SW_NORMAL);
	//if(nRet < 31)
	//{
	//	AfxMessageBox("Fail to running UBC Starter !!!");
	//}

	OnOK();
}

bool CUBCWeddingImporterDlg::ScheduleCopy(CString strSrcPath, CString strTarPath)
{
	::SHCreateDirectoryEx(GetSafeHwnd(), strTarPath, NULL);

	TCHAR szFrom[MAX_PATH+1] = {0};
	strcpy(szFrom, strTarPath);

	SHFILEOPSTRUCT sh;
	memset(&sh, 0x00, sizeof(SHFILEOPSTRUCT));
	sh.hwnd = GetSafeHwnd();
	sh.wFunc = FO_DELETE;
	sh.pFrom = szFrom;
	sh.pTo = NULL;
	sh.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_SIMPLEPROGRESS | FOF_NOERRORUI;
	sh.fAnyOperationsAborted = FALSE;
	sh.lpszProgressTitle = "홀 스케쥴 적용중";
	::SHFileOperation(&sh);

	memset(&szFrom, 0x00, sizeof(szFrom));
	strcpy(szFrom, strSrcPath);

	TCHAR szTo[MAX_PATH+1] = {0};
	strcpy(szTo, strTarPath);

	memset(&sh, 0x00, sizeof(SHFILEOPSTRUCT));
	sh.hwnd = GetSafeHwnd();
	sh.wFunc = FO_COPY;
	sh.pFrom = szFrom;
	sh.pTo = szTo;
	sh.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_SIMPLEPROGRESS;// | FOF_NOERRORUI;
	sh.fAnyOperationsAborted = FALSE;
	sh.lpszProgressTitle = "홀 스케쥴 적용중";

	int nResult = SHFileOperation(&sh);

	return (nResult == 0 && sh.fAnyOperationsAborted == false);
}

void CUBCWeddingImporterDlg::OnBnClickedCancel()
{	
	OnCancel();
}

void CUBCWeddingImporterDlg::FindSchedule(CStringArray& astrScheduleList)
{
	CStringArray astrDrive;
	int nPos =0;
	char szDrive[] = "?:\\";
	DWORD dwDriveList=::GetLogicalDrives();
	while(dwDriveList)
	{
		if(dwDriveList & 1)
		{
			szDrive[0] = 'A' + nPos;
			CString strDrive(szDrive);
			strDrive.MakeUpper();

			UINT nType = GetDriveType(strDrive);
			if( nType == DRIVE_REMOVABLE )
			{
				astrDrive.Add(strDrive);
			}
		}
		dwDriveList >>=1;
		nPos++;
	}

	for(int i=0; i<astrDrive.GetCount() ;i++)
	{
		CString strFindPath;
		strFindPath.Format("%s%s*.*", astrDrive[i], DATA_DEFAULT_PATH);

		CFileFind ff;
		BOOL bWorking = ff.FindFile(strFindPath);
		while(bWorking)
		{
			bWorking = ff.FindNextFile();

			if(ff.IsDots()) continue;
			if(!ff.IsDirectory()) continue;

			CString strFileName = ff.GetFileName();
			strFileName.MakeLower();
			if(strFileName.Find(_T("data_")) == 0)
			{
				astrScheduleList.Add(ff.GetFilePath());
			}
		}

		ff.Close();
	}
}

void CUBCWeddingImporterDlg::InitList()
{
	CStringArray astrScheduleList;
	FindSchedule(astrScheduleList);

	m_lcList.DeleteAllItems();
	for(int i=0; i<astrScheduleList.GetCount() ;i++)
	{
		CString strSchedulePath = astrScheduleList[i];
		CString strDrive = strSchedulePath.Left(strSchedulePath.Find("\\")+1);
		CString strScheduleName = strSchedulePath.Mid(strSchedulePath.ReverseFind('\\')+1+5);

		int nIdx = m_lcList.InsertItem(i, "", 0);

		m_lcList.SetItemText(nIdx, eDrive, strDrive);
		m_lcList.SetItemText(nIdx, eScheduleName, strScheduleName);

		LVITEM item;
		item.mask = LVIF_IMAGE;
		item.iItem = nIdx;
		item.iSubItem = eDrive;
		item.iImage = GetDriveType(strDrive)+5;
		m_lcList.SetItem(&item);
	}

	m_lcList.SetSortHeader(eScheduleName);
	m_lcList.SortItems(CompareProc, (DWORD_PTR)this);
}

void CUBCWeddingImporterDlg::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	POSITION pos = m_lcList.GetFirstSelectedItemPosition();
	int nItem = (pos == NULL ? -1 : m_lcList.GetNextSelectedItem(pos));

	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		LVITEM item;
		item.iItem = i;
		item.iSubItem = 0;
		item.mask = LVIF_IMAGE;
		item.iImage = (i == nItem ? 1 : 0);
		m_lcList.SetItem(&item);
	}
}

void CUBCWeddingImporterDlg::OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
	OnBnClickedOk();
}

void CUBCWeddingImporterDlg::OnLvnColumnclickLcList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
	
	int	nColumn = pNMLV->iSubItem;
	
	m_lcList.SetSortHeader(nColumn);
	m_lcList.SortItems(CompareProc, (DWORD_PTR)this);
}

int CALLBACK CUBCWeddingImporterDlg::CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	CUBCWeddingImporterDlg* pDlg = (CUBCWeddingImporterDlg*)lParam;
	CUTBListCtrl* pListCtrl = &pDlg->m_lcList;

	LVFINDINFO    lvFind;
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, pListCtrl->GetCurSortCol());
	CString strItem2 = pListCtrl->GetItemText(nIndex2, pListCtrl->GetCurSortCol());

	if(pListCtrl->IsAscend())
		return strItem1.Compare(strItem2);
	else
		return strItem2.Compare(strItem1);
}
