// ScheduleOpenDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "WeddingDataManager.h"
#include "UBCWeddingStudio.h"
#include "ScheduleOpenDlg.h"

// CScheduleOpenDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CScheduleOpenDlg, CDialog)

CScheduleOpenDlg::CScheduleOpenDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CScheduleOpenDlg::IDD, pParent)
{

}

CScheduleOpenDlg::~CScheduleOpenDlg()
{
}

void CScheduleOpenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LC_LIST, m_lcList);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
}


BEGIN_MESSAGE_MAP(CScheduleOpenDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CScheduleOpenDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CScheduleOpenDlg::OnBnClickedCancel)
	ON_NOTIFY(NM_CLICK, IDC_LC_LIST, &CScheduleOpenDlg::OnNMClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LC_LIST, &CScheduleOpenDlg::OnNMDblclkList)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LC_LIST, &CScheduleOpenDlg::OnLvnColumnclickLcList)
END_MESSAGE_MAP()


// CScheduleOpenDlg 메시지 처리기입니다.

void CScheduleOpenDlg::OnBnClickedOk()
{
	POSITION pos = m_lcList.GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		AfxMessageBox(IDS_MSG_008);
		return;
	}

	int nItem = m_lcList.GetNextSelectedItem(pos);

	m_strSchedulePath.Format("%s%sdata_%s"
							, m_lcList.GetItemText(nItem, eDrive)
							, WeddingDataMng()->GetDefaultPath()
							, m_lcList.GetItemText(nItem, eScheduleName)
							);

	OnOK();
}

void CScheduleOpenDlg::OnBnClickedCancel()
{	
	OnCancel();
}

BOOL CScheduleOpenDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CBitmap bmp;
	bmp.LoadBitmap(IDB_CHECK);

	if(m_imgForList.Create(16, 15, ILC_COLORDDB | ILC_MASK, 1, 1))
	{
		m_imgForList.Add(&bmp, RGB(255,255,255));
	}
	m_lcList.SetImageList(&m_imgForList, LVSIL_SMALL);

	m_lcList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_SUBITEMIMAGES | LVS_EX_GRIDLINES);
	m_lcList.InsertColumn(eSelect      , _T("")                          , LVCFMT_LEFT, 24);
	m_lcList.InsertColumn(eDrive       , LoadStringFromID(IDS_OPEN_LIST1), LVCFMT_LEFT, 80);
	m_lcList.InsertColumn(eScheduleName, LoadStringFromID(IDS_OPEN_LIST2), LVCFMT_LEFT, 300);
	m_lcList.InitHeader(IDB_LIST_HEADER);

	//버튼위 위치 조정
	m_bnOK.LoadBitmap(IDB_BTN_SELECT, RGB(255, 255, 255));
	m_bnOK.SetToolTipText(IDS_SELECT_SCHEDULE);

	m_bnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_bnCancel.SetToolTipText(IDS_CANCEL_SCHEDULE);

	InitList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CScheduleOpenDlg::FindSchedule(CStringArray& astrScheduleList)
{
	CStringArray astrDrive;
	int nPos =0;
	char szDrive[] = "?:\\";
	DWORD dwDriveList=::GetLogicalDrives();
	while(dwDriveList)
	{
		if(dwDriveList & 1)
		{
			szDrive[0] = 'A' + nPos;
			CString strDrive(szDrive);
			strDrive.MakeUpper();

			UINT nType = GetDriveType(strDrive);
			if( nType == DRIVE_REMOVABLE ||
				nType == DRIVE_FIXED     )
			{
				astrDrive.Add(strDrive);
			}
		}
		dwDriveList >>=1;
		nPos++;
	}

	for(int i=0; i<astrDrive.GetCount() ;i++)
	{
		CString strFindPath;
		strFindPath.Format("%s%s*.*", astrDrive[i], WeddingDataMng()->GetDefaultPath());

		CFileFind ff;
		BOOL bWorking = ff.FindFile(strFindPath);
		while(bWorking)
		{
			bWorking = ff.FindNextFile();

			if(ff.IsDots()) continue;
			if(!ff.IsDirectory()) continue;

			CString strFileName = ff.GetFileName();
			strFileName.MakeLower();
			if(strFileName.Find(_T("data_")) == 0)
			{
				astrScheduleList.Add(ff.GetFilePath());
			}
		}

		ff.Close();
	}
}

void CScheduleOpenDlg::InitList()
{
	CStringArray astrScheduleList;
	FindSchedule(astrScheduleList);

	m_lcList.DeleteAllItems();
	for(int i=0; i<astrScheduleList.GetCount() ;i++)
	{
		CString strSchedulePath = astrScheduleList[i];
		CString strDrive = strSchedulePath.Left(strSchedulePath.Find("\\")+1);
		CString strScheduleName = strSchedulePath.Mid(strSchedulePath.ReverseFind('\\')+1+5);

		int nIdx = m_lcList.InsertItem(i, "", 0);

		m_lcList.SetItemText(nIdx, eDrive, strDrive);
		m_lcList.SetItemText(nIdx, eScheduleName, strScheduleName);

		LVITEM item;
		item.mask = LVIF_IMAGE;
		item.iItem = nIdx;
		item.iSubItem = eDrive;
		item.iImage = GetDriveType(strDrive)+5;
		m_lcList.SetItem(&item);
	}

	m_lcList.SetSortHeader(eScheduleName);
	m_lcList.SortItems(CompareProc, (DWORD_PTR)this);
}

void CScheduleOpenDlg::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	POSITION pos = m_lcList.GetFirstSelectedItemPosition();
	int nItem = (pos == NULL ? -1 : m_lcList.GetNextSelectedItem(pos));

	for(int i=0; i<m_lcList.GetItemCount(); i++)
	{
		LVITEM item;
		item.iItem = i;
		item.iSubItem = 0;
		item.mask = LVIF_IMAGE;
		item.iImage = (i == nItem ? 1 : 0);
		m_lcList.SetItem(&item);
	}
}

void CScheduleOpenDlg::OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
	OnBnClickedOk();
}

void CScheduleOpenDlg::OnLvnColumnclickLcList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
	
	int	nColumn = pNMLV->iSubItem;
	
	m_lcList.SetSortHeader(nColumn);
	m_lcList.SortItems(CompareProc, (DWORD_PTR)this);
}

int CALLBACK CScheduleOpenDlg::CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	CScheduleOpenDlg* pDlg = (CScheduleOpenDlg*)lParam;
	CUTBListCtrl* pListCtrl = &pDlg->m_lcList;

	LVFINDINFO    lvFind;
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, pListCtrl->GetCurSortCol());
	CString strItem2 = pListCtrl->GetItemText(nIndex2, pListCtrl->GetCurSortCol());

	if(pListCtrl->IsAscend())
		return strItem1.Compare(strItem2);
	else
		return strItem2.Compare(strItem1);
}
