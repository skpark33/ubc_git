// UBCWeddingStudioDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCWeddingStudio.h"
#include "UBCWeddingStudioDlg.h"
#include "WeddingDataManager.h"
#include "HallInfoDlg.h"
#include "NewScheduleDlg.h"
#include "ScheduleOpenDlg.h"
#include "ImageFileOp.h"
#include "cmn\libscratch\scratchUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAboutDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CUBCWeddingStudioDlg 대화 상자




CUBCWeddingStudioDlg::CUBCWeddingStudioDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCWeddingStudioDlg::IDD, pParent)
	, m_strScheduleName(_T(""))
	, m_strHallName(_T(""))
	, m_strDesc(_T(""))
	, m_strBreakTimeImage(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CUBCWeddingStudioDlg::~CUBCWeddingStudioDlg()
{
}

void CUBCWeddingStudioDlg::OnDestroy()
{
	CDialog::OnDestroy();

	DeleteTabCtrl();
}

void CUBCWeddingStudioDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EB_SCHEDULE_NAME, m_strScheduleName);
	DDX_Text(pDX, IDC_EB_DESC, m_strDesc);
	DDV_MaxChars(pDX, m_strDesc, 100);
	DDX_Text(pDX, IDC_TXT_BREAK_TIME_IMAGE, m_strBreakTimeImage);
	DDX_Control(pDX, IDC_BN_BREAK_TIME_IMAGE, m_bnBreakTimeImage);
	DDX_Control(pDX, IDC_TAB_INFO, m_tabInfo);
	DDX_CBString(pDX, IDC_CB_HALL_NAME, m_strHallName);
	DDV_MaxChars(pDX, m_strHallName, 32);
	DDX_Control(pDX, IDC_CB_HALL_NAME, m_cbHallName);
}

BEGIN_MESSAGE_MAP(CUBCWeddingStudioDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CUBCWeddingStudioDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CUBCWeddingStudioDlg::OnBnClickedCancel)
	ON_WM_DESTROY()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_INFO, &CUBCWeddingStudioDlg::OnTcnSelchangeTabInfo)
	ON_NOTIFY(TCN_SELCHANGING, IDC_TAB_INFO, &CUBCWeddingStudioDlg::OnTcnSelchangingTabInfo)
	ON_COMMAND(ID_CMD_ABOUT, &CUBCWeddingStudioDlg::OnCmdAbout)
	ON_COMMAND(ID_CMD_DELETE, &CUBCWeddingStudioDlg::OnCmdDelete)
	ON_COMMAND(ID_CMD_EXIT, &CUBCWeddingStudioDlg::OnCmdExit)
	ON_COMMAND(ID_CMD_EXPORT, &CUBCWeddingStudioDlg::OnCmdExport)
	ON_COMMAND(ID_CMD_NEW, &CUBCWeddingStudioDlg::OnCmdNew)
	ON_COMMAND(ID_CMD_OPEN, &CUBCWeddingStudioDlg::OnCmdOpen)
	ON_COMMAND(ID_CMD_SAVE, &CUBCWeddingStudioDlg::OnCmdSave)
	ON_COMMAND(ID_CMD_SAVE_AS, &CUBCWeddingStudioDlg::OnCmdSaveAs)
	ON_BN_CLICKED(IDC_BN_BREAK_TIME_IMAGE, &CUBCWeddingStudioDlg::OnBnClickedBnBreakTimeImage)
	ON_STN_DBLCLK(IDC_TXT_BREAK_TIME_IMAGE, &CUBCWeddingStudioDlg::OnStnDblclickTxtBreakTimeImage)
END_MESSAGE_MAP()


// CUBCWeddingStudioDlg 메시지 처리기

BOOL CUBCWeddingStudioDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	InitToolbar();

	m_bnBreakTimeImage.LoadBitmap(IDB_BTN_OPEN, RGB(255, 255, 255));
	m_bnBreakTimeImage.SetToolTipText(IDS_BREAK_TIME_IMAGE);

	CreateTabCtrl();

	InitDataAndCtrl("");

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCWeddingStudioDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCWeddingStudioDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CUBCWeddingStudioDlg::OnBnClickedOk()
{
//	OnOK();
}

void CUBCWeddingStudioDlg::OnBnClickedCancel()
{
	OnCancel();
}

// 화면 초기화
bool CUBCWeddingStudioDlg::InitDataAndCtrl(CString strFileName)
{
	if(!WeddingDataMng()->LoadSchedule(strFileName))
	{
		WeddingDataMng()->SaveSchedule();
	}

	InitHallNameCombo();

	InitScheduleInfo(strFileName);
	InitChildWnd();

	if(WeddingDataMng()->GetScheduleName().IsEmpty())
	{
		SetWindowText(AfxGetApp()->m_pszAppName);
	}
	else
	{
		CString strWindowText;
		strWindowText.Format(_T("%s - Drive:%C 스케쥴:%s")
							, AfxGetApp()->m_pszAppName
							, WeddingDataMng()->GetSchedulePath().GetAt(0)
							, WeddingDataMng()->GetScheduleName()
							);
		SetWindowText(strWindowText);
	}

	UpdateData(FALSE);

	return true;
}

// 툴바 초기화
bool CUBCWeddingStudioDlg::InitToolbar()
{
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT
							, WS_CHILD | WS_VISIBLE | CBRS_TOP| CBRS_GRIPPER | CBRS_TOOLTIPS
							| CBRS_BORDER_3D | CBRS_FLYBY | CBRS_SIZE_DYNAMIC)
		|| !m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
	   TRACE0("Failed to create toolbar\n");
	   return false;
	}

/*
	m_wndToolBar.GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);
	DWORD dwStyle = m_wndToolBar.GetButtonStyle(m_wndToolBar.CommandToIndex(IDC_VIEW));
		dwStyle |= TBSTYLE_DROPDOWN;        // 서브 메뉴를 출력시킨다.

	m_wndToolBar.SetButtonStyle(m_wndToolBar.CommandToIndex(IDC_VIEW), dwStyle);
*/
	CImageList imageList;
	CBitmap    bitmap;

	// 기본 상태일 때
	bitmap.LoadBitmap(IDB_TOOLBAR_ENABLE);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	// 활성화 상태일 때
	bitmap.LoadBitmap(IDB_TOOLBAR_HOT);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETHOTIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	// 비 활성화 상태일 때
	bitmap.LoadBitmap(IDB_TOOLBAR_DISABLE);
	imageList.Create(48, 48, ILC_COLORDDB|ILC_MASK, 10, 1);
	imageList.Add(&bitmap, RGB(255,0,0));
	m_wndToolBar.SendMessage(TB_SETDISABLEDIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
	imageList.Detach();
	bitmap.Detach();

	//tool bar 크기 조정
	m_wndToolBar.SetSizes(CSize(48+7,48+6), CSize(48, 48));

	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);

	return true;
}


// 탭컨트롤 생성
bool CUBCWeddingStudioDlg::CreateTabCtrl()
{
	DeleteTabCtrl();

	CRect rClient(0,0,0,0);
	m_tabInfo.GetClientRect(&rClient);

	m_tabInfo.InitImageListHighColor(IDB_TAB_LIST, RGB(192,192,192));
	m_tabInfo.SetItemSize(CSize(rClient.Width()/10, 20));

	int nMaxValue = WeddingDataMng()->GetMaxWeddingData();

	for(int i=0; i<nMaxValue ;i++)
	{
		CString strText;
		strText.Format("%d식", i+1);
		CHallInfoDlg* pChild = new CHallInfoDlg(this, i+1);
		pChild->Create(IDD_HALL_INFO_DLG);

		CRect rTabClient(0,0,0,0);
		CRect rTabItem(0,0,0,0);
		//m_tabInfo.GetClientRect(&rTabClient);
		m_tabInfo.GetWindowRect(&rTabClient);
		ScreenToClient(rTabClient);
		m_tabInfo.GetItemRect(0, &rTabItem);
		pChild->SetWindowPos( NULL
							, rTabClient.left + 1, rTabClient.top+rTabItem.Height() + 3
							, rTabClient.Width()-4 , rTabClient.Height() - rTabItem.Height() - 6
							, SWP_FRAMECHANGED | SWP_HIDEWINDOW);
		pChild->EnableWindow(FALSE);

		m_tabInfo.InsertItem( TCIF_TEXT | TCIF_IMAGE | TCIF_PARAM
							, i
							, strText
							, 0
							, (LPARAM)pChild
							);
	}

	return true;
}

// 탭컨트롤 초기화
bool CUBCWeddingStudioDlg::InitChildWnd()
{
	for(int i=0; i<m_tabInfo.GetItemCount() ;i++)
	{
		TCITEM item;
		memset(&item, 0x00, sizeof(TCITEM));
		item.mask = TCIF_IMAGE | TCIF_PARAM;
		m_tabInfo.GetItem(i, &item);

		CHallInfoDlg* pChild = (CHallInfoDlg*)item.lParam;
		if(pChild)
		{
			pChild->ShowWindow(SW_HIDE);
			pChild->EnableWindow(FALSE);

			pChild->InitDataAndCtrl();

			item.mask = TCIF_IMAGE;
			item.iImage = (pChild->IsInputData() ? 1 : 0);
			m_tabInfo.SetItem(i, &item);
		}
	}

	m_tabInfo.SetCurSel(0);
	CWnd* pChild = GetChildWnd(m_tabInfo.GetCurSel());
	if(pChild && ::IsWindow(pChild->GetSafeHwnd()))
	{
		pChild->ShowWindow(SW_SHOW);
		pChild->EnableWindow(TRUE);
	}

	m_tabInfo.EnableWindow(WeddingDataMng()->IsInputMode());

	return true;
}

void CUBCWeddingStudioDlg::DeleteTabCtrl()
{
	for(int i=0; i<m_tabInfo.GetItemCount() ;i++)
	{
		TCITEM item;
		memset(&item, 0x00, sizeof(TCITEM));
		item.mask = TCIF_PARAM; 
		m_tabInfo.GetItem(i, &item);

		CHallInfoDlg* pChild = (CHallInfoDlg*)item.lParam;
		if(pChild && ::IsWindow(pChild->GetSafeHwnd()))
		{
			delete pChild;
		}
	}

	m_tabInfo.DeleteAllItems();
}

CWnd* CUBCWeddingStudioDlg::GetChildWnd(int nIndex)
{
	TCITEM item;
	memset(&item, 0x00, sizeof(TCITEM));
	item.mask = TCIF_PARAM; 
	m_tabInfo.GetItem(nIndex, &item);

	CHallInfoDlg* pChild = (CHallInfoDlg*)item.lParam;

	return pChild;
}

void CUBCWeddingStudioDlg::OnTcnSelchangingTabInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	CHallInfoDlg* pChild = (CHallInfoDlg*)GetChildWnd(m_tabInfo.GetCurSel());
	if(pChild && ::IsWindow(pChild->GetSafeHwnd()))
	{
		pChild->ShowWindow(SW_HIDE);
		pChild->EnableWindow(FALSE);

		TCITEM item;
		memset(&item, 0x00, sizeof(TCITEM));
		item.mask = TCIF_IMAGE; 
		m_tabInfo.GetItem(m_tabInfo.GetCurSel(), &item);
		item.iImage = (pChild->IsInputData() ? 1 : 0);
		m_tabInfo.SetItem(m_tabInfo.GetCurSel(), &item);
	}

	*pResult = 0;
}

void CUBCWeddingStudioDlg::OnTcnSelchangeTabInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	CHallInfoDlg* pChild = (CHallInfoDlg*)GetChildWnd(m_tabInfo.GetCurSel());
	if(pChild && ::IsWindow(pChild->GetSafeHwnd()))
	{
		pChild->ShowWindow(SW_SHOW);
		pChild->EnableWindow(TRUE);
	}

	*pResult = 0;
}

bool CUBCWeddingStudioDlg::InitScheduleInfo(CString strFileName)
{
	m_strScheduleName   = WeddingDataMng()->GetScheduleName();
	m_strHallName       = WeddingDataMng()->GetWeddingInfo("hall");
	m_strDesc           = WeddingDataMng()->GetWeddingInfo("desc");
	m_strBreakTimeImage = WeddingDataMng()->GetWeddingInfo("breakTime");

	GetDlgItem(IDC_EB_SCHEDULE_NAME    )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_CB_HALL_NAME        )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_EB_DESC             )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_TXT_BREAK_TIME_IMAGE)->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_BN_BREAK_TIME_IMAGE )->EnableWindow(WeddingDataMng()->IsInputMode());

	GetDlgItem(IDC_CB_HALL_NAME        )->SetFocus();

	return true;
}

bool CUBCWeddingStudioDlg::IsModified()
{
	if(!WeddingDataMng()->IsInputMode()) return false;

	if(WeddingDataMng()->GetWeddingInfo("hall"     ) != m_strHallName      ) return true;
	if(WeddingDataMng()->GetWeddingInfo("desc"     ) != m_strDesc          ) return true;
	if(WeddingDataMng()->GetWeddingInfo("breakTime") != m_strBreakTimeImage) return true;

	for(int i=0; i<m_tabInfo.GetItemCount() ;i++)
	{
		CHallInfoDlg* pChild = (CHallInfoDlg*)GetChildWnd(i);
		if(!pChild) continue;
		if(pChild->IsModified()) return true;
	}

	return false;
}

bool CUBCWeddingStudioDlg::InputErrorCheck()
{
	if(m_strScheduleName.IsEmpty())
	{
		AfxMessageBox(IDS_MSG_000);
		return false;
	}

	if(m_strHallName.IsEmpty())
	{
		AfxMessageBox(IDS_MSG_001);
		return false;
	}

	for(int i=0; i<m_tabInfo.GetItemCount() ;i++)
	{
		CHallInfoDlg* pChild = (CHallInfoDlg*)GetChildWnd(i);
		if(!pChild) continue;
		if(pChild->InputErrorCheck()) continue;

		CWnd* pCurChild = GetChildWnd(m_tabInfo.GetCurSel());
		if(pCurChild && ::IsWindow(pCurChild->GetSafeHwnd()))
		{
			pCurChild->ShowWindow(SW_HIDE);
			pCurChild->EnableWindow(FALSE);
		}

		m_tabInfo.SetCurSel(i);
		pChild->ShowWindow(SW_SHOW);
		pChild->EnableWindow(TRUE);
		pChild->SetFocus();

		return false;
	}

	return true;
}

bool CUBCWeddingStudioDlg::Save()
{
	WeddingDataMng()->SetWeddingInfo("hall"     , m_strHallName      );
	WeddingDataMng()->SetWeddingInfo("desc"     , m_strDesc          );
	WeddingDataMng()->SetWeddingInfo("breakTime", m_strBreakTimeImage);

	for(int i=0; i<m_tabInfo.GetItemCount() ;i++)
	{
		CHallInfoDlg* pChild = (CHallInfoDlg*)GetChildWnd(i);
		if(!pChild) continue;
		if(pChild->SaveData()) continue;

		CWnd* pCurChild = GetChildWnd(m_tabInfo.GetCurSel());
		if(pCurChild && ::IsWindow(pCurChild->GetSafeHwnd()))
		{
			pCurChild->ShowWindow(SW_HIDE);
			pCurChild->EnableWindow(FALSE);
		}

		m_tabInfo.SetCurSel(i);
		pChild->ShowWindow(SW_SHOW);
		pChild->EnableWindow(TRUE);
		pChild->SetFocus();

		return false;
	}

	return true;
}

void CUBCWeddingStudioDlg::OnBnClickedBnBreakTimeImage()
{
	CString strTargetFilePath;
	if(!CImageFileOp::ImageFileOpenAndCopy(strTargetFilePath)) return;
	
	TCHAR szDrv[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szName[MAX_PATH]={0}, szExt[MAX_PATH]={0};
	_tsplitpath(strTargetFilePath, szDrv, szPath, szName, szExt);

	m_strBreakTimeImage.Format("%s%s", szName, szExt);
	SetDlgItemText(IDC_TXT_BREAK_TIME_IMAGE, m_strBreakTimeImage);
}

void CUBCWeddingStudioDlg::OnCmdNew()
{
	UpdateData(TRUE);

	if(IsModified())
	{
		int nResult = AfxMessageBox(IDS_MSG_017, MB_YESNOCANCEL);
		if(nResult == IDYES) 
		{
			if(!CheckAndSave()) return;
		}
		else if(nResult == IDCANCEL) return;
	}

	CNewScheduleDlg dlg(this, CNewScheduleDlg::eNEW);
	if(dlg.DoModal() != IDOK) return;

	InitDataAndCtrl(dlg.m_strSchedulePath);
}

void CUBCWeddingStudioDlg::OnCmdOpen()
{
	UpdateData(TRUE);

	if(IsModified())
	{
		int nResult = AfxMessageBox(IDS_MSG_017, MB_YESNOCANCEL);
		if(nResult == IDYES) 
		{
			if(!CheckAndSave()) return;
		}
		else if(nResult == IDCANCEL) return;
	}

	// 스케쥴열기 다이얼로그
	CScheduleOpenDlg dlg;
	if(dlg.DoModal() != IDOK) return;

	InitDataAndCtrl(dlg.m_strSchedulePath);
}

void CUBCWeddingStudioDlg::OnCmdSave()
{
	UpdateData(TRUE);

	CheckAndSave();

	UpdateData(FALSE);
}

bool CUBCWeddingStudioDlg::CheckAndSave()
{
	if(!WeddingDataMng()->IsInputMode()) return true;

	if(!InputErrorCheck()) return false;

	if(!Save()) return false;

	if(!WeddingDataMng()->SaveSchedule()) return false;

	AfxMessageBox(IDS_MSG_011);

	SaveHallNameCombo();

	return true;
}

void CUBCWeddingStudioDlg::OnCmdSaveAs()
{
	if(!WeddingDataMng()->IsInputMode()) return;

	if(!InputErrorCheck()) return;

	if(!Save()) return;

	CNewScheduleDlg dlg(this, CNewScheduleDlg::eSAVE_AS);
	if(dlg.DoModal() != IDOK) return;

	if(WeddingDataMng()->SaveAsSchedule(dlg.m_strSchedulePath))
	{
		SaveHallNameCombo();

		InitDataAndCtrl(dlg.m_strSchedulePath);
	}

	AfxMessageBox(IDS_MSG_012);
}

void CUBCWeddingStudioDlg::OnCmdExport()
{
	UpdateData(TRUE);

	if(!WeddingDataMng()->IsInputMode()) return;

	if(IsModified())
	{
		int nResult = AfxMessageBox(IDS_MSG_017, MB_YESNOCANCEL);
		if(nResult == IDYES)
		{
			if(!CheckAndSave()) return;
		}
		else if(nResult == IDCANCEL) return;
	}

	CNewScheduleDlg dlg(this, CNewScheduleDlg::eEXPORT);
	dlg.m_strNewScheduleName = WeddingDataMng()->GetScheduleName();
	if(dlg.DoModal() != IDOK) return;

	if(WeddingDataMng()->SaveAsSchedule(dlg.m_strSchedulePath))
	{
		InitDataAndCtrl(WeddingDataMng()->GetSchedulePath());
		AfxMessageBox(IDS_MSG_019);
	}
}

void CUBCWeddingStudioDlg::OnCmdDelete()
{
	if(!WeddingDataMng()->IsInputMode()) return;

	if(AfxMessageBox(IDS_MSG_014, MB_YESNO) != IDYES) return;

	TCHAR szFrom[MAX_PATH+1] = {0};
	_tcscpy(szFrom, WeddingDataMng()->GetSchedulePath());

	SHFILEOPSTRUCT sh;
	memset(&sh, 0x00, sizeof(SHFILEOPSTRUCT));
	sh.hwnd = GetSafeHwnd();
	sh.wFunc = FO_DELETE;
	sh.pFrom = szFrom;
	sh.pTo = NULL;
	sh.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_SIMPLEPROGRESS;// | FOF_NOERRORUI;
	sh.fAnyOperationsAborted = FALSE;
	sh.lpszProgressTitle = "홀 스케쥴 삭제중";
	::SHFileOperation(&sh);

	InitDataAndCtrl("");
}

void CUBCWeddingStudioDlg::OnCmdAbout()
{
	CAboutDlg dlg;
	dlg.DoModal();
}

void CUBCWeddingStudioDlg::OnCmdExit()
{
	UpdateData(TRUE);

	if(!WeddingDataMng()->IsInputMode()) return;

	if(IsModified())
	{
		int nResult = AfxMessageBox(IDS_MSG_015, MB_YESNOCANCEL);
		if(nResult == IDYES) 
		{
			if(!CheckAndSave()) return;
		}
		else if(nResult == IDCANCEL) return;
	}

	OnCancel();
}

void CUBCWeddingStudioDlg::OnOK() {}
void CUBCWeddingStudioDlg::OnCancel()
{
	CDialog::OnOK();
}

void CAboutDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString szCopyright, szVer, szUser, szSite;
	scratchUtil* su = scratchUtil::getInstance();

	std::string sVer;
	su->getVersion(sVer, false);

	szCopyright  = "Copyright (C) 2009 SQI Soft Corporation.";
	szVer = "All rights reserved.";
	szSite.Format("Wedding Studio Version %s", sVer.c_str());
	szUser = "";

	CRect rc;
	GetDlgItem(IDC_ABOUT_COPYRIGHT)->GetClientRect(&rc);
	GetDlgItem(IDC_ABOUT_COPYRIGHT)->ClientToScreen(&rc);
	ScreenToClient(&rc);

	int nCY = rc.Height()/2;
	GetDlgItem(IDC_ABOUT_COPYRIGHT)->MoveWindow(rc.left, rc.top-rc.Height()+nCY, rc.Width(), rc.Height());
	GetDlgItem(IDC_ABOUT_VERSION)->MoveWindow(rc.left, rc.top+nCY, rc.Width(), rc.Height());
	GetDlgItem(IDC_STATIC_SITE)->MoveWindow(rc.left, rc.top+rc.Height()+nCY, rc.Width(), rc.Height());
	GetDlgItem(IDC_STATIC_USER)->MoveWindow(rc.left, rc.top+2*rc.Height()+nCY, rc.Width(), rc.Height());

	GetDlgItem(IDC_ABOUT_COPYRIGHT)->SetWindowText(szCopyright);
	GetDlgItem(IDC_ABOUT_VERSION)->SetWindowText(szVer);
	GetDlgItem(IDC_STATIC_SITE)->SetWindowText(szSite);
	GetDlgItem(IDC_STATIC_USER)->SetWindowText(szUser);

//	CStatic* pLogo = (CStatic*)GetDlgItem(IDC_ABOUT_LOGO);
//	pLogo->SetIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CUBCWeddingStudioDlg::OnStnDblclickTxtBreakTimeImage()
{
	UpdateData(TRUE);

	if(!m_strBreakTimeImage.IsEmpty())
	{
		ShellExecute(NULL, "open", m_strBreakTimeImage, NULL, WeddingDataMng()->GetImagePath(), SW_NORMAL);
	}

	UpdateData(FALSE);
}

void CUBCWeddingStudioDlg::InitHallNameCombo()
{
	m_cbHallName.ResetContent();

	CString strExePath = AfxGetApp()->m_pszHelpFilePath;
	CString strProfilePath;
	strProfilePath.Format(_T("%s\\data\\%s.ini"), strExePath.Left(strExePath.ReverseFind('\\')), AfxGetApp()->m_pszAppName);

	int nHallNameCnt = ::GetPrivateProfileInt(_T("HALL_NAME_HIST"), _T("HALL_NAME_CNT"), 0, strProfilePath);

	for(int i=0; i<nHallNameCnt ;i++)
	{
		CString strKey;
		strKey.Format(_T("HALL_NAME_%d"), i);

		TCHAR szData[1024] = {0};
		::GetPrivateProfileString(_T("HALL_NAME_HIST"), strKey, _T(""), szData, 1024, strProfilePath);

		CString strHallName = szData;
		if(strHallName.IsEmpty()) continue;

		m_cbHallName.AddString(strHallName);
	}
}

void CUBCWeddingStudioDlg::SaveHallNameCombo()
{
	int nFindIdx = m_cbHallName.FindStringExact(0, m_strHallName);
	if(nFindIdx >= 0)
	{
		m_cbHallName.DeleteString(nFindIdx);
	}

	m_cbHallName.InsertString(0, m_strHallName);

	CString strExePath = AfxGetApp()->m_pszHelpFilePath;
	CString strProfilePath;
	strProfilePath.Format(_T("%s\\data\\%s.ini"), strExePath.Left(strExePath.ReverseFind('\\')), AfxGetApp()->m_pszAppName);

	::WritePrivateProfileString(_T("HALL_NAME_HIST"), _T("HALL_NAME_CNT"), ToString(m_cbHallName.GetCount()), strProfilePath);

	for(int i=0; i<m_cbHallName.GetCount() ;i++)
	{
		CString strHallName;
		m_cbHallName.GetLBText(i, strHallName);

		CString strKey;
		strKey.Format(_T("HALL_NAME_%d"), i);
		::WritePrivateProfileString(_T("HALL_NAME_HIST"), strKey, strHallName, strProfilePath);
	}
}