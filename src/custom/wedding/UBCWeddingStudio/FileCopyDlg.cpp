// ImageFileCopy.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCWeddingStudio.h"
#include "FileCopyDlg.h"

// CFileCopyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFileCopyDlg, CDialog)

CFileCopyDlg::CFileCopyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFileCopyDlg::IDD, pParent)
	, m_bAutoResolution(TRUE)
	, m_bApplyAll(FALSE)
	, m_strFileName(_T(""))
	, m_strFilePath(_T(""))
	, m_strFileSize(_T(""))
	, m_strFileDate(_T(""))
	, m_bIsSingleFile(true)
{
}

CFileCopyDlg::~CFileCopyDlg()
{
}

void CFileCopyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FRAME_IMAGE, m_picImage);
	DDX_Check(pDX, IDC_KB_AUTO_RESOLUTION, m_bAutoResolution);
	DDX_Check(pDX, IDC_KB_APPLY_ALL, m_bApplyAll);
	DDX_Text(pDX, IDC_TXT_FILE_NAME, m_strFileName);
	DDX_Text(pDX, IDC_TXT_PATH, m_strFilePath);
	DDX_Text(pDX, IDC_TXT_SIZE, m_strFileSize);
	DDX_Text(pDX, IDC_TXT_DATE, m_strFileDate);
}


BEGIN_MESSAGE_MAP(CFileCopyDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CFileCopyDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFileCopyDlg::OnBnClickedCancel)
END_MESSAGE_MAP()

// CFileCopyDlg 메시지 처리기입니다.

BOOL CFileCopyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_picImage.SetImageFile(m_strFilePath);
	m_strFileName = m_strFilePath.Mid(m_strFilePath.ReverseFind('\\')+1);
	m_strFileSize = _T("크기: ") + ::GetFileSize(m_strFilePath);
	m_strFileDate = _T("수정한 날짜: ") + ::GetFileDate(m_strFilePath);

	m_bApplyAll = FALSE;
	GetDlgItem(IDC_KB_APPLY_ALL)->EnableWindow(!m_bIsSingleFile);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFileCopyDlg::OnBnClickedOk()
{
	UpdateData(TRUE);
	OnOK();
}

void CFileCopyDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}
