// ImageCtrl.cpp : 구현 파일입니다.
//

#include "stdafx.h"

#include "ximage.h"
#pragma comment(lib, "cximage.lib")
#pragma comment(lib, "Jpeg.lib")
#pragma comment(lib, "png.lib")
#pragma comment(lib, "Tiff.lib")
#pragma comment(lib, "jasper.lib")
#pragma comment(lib, "libdcr.lib")
#pragma comment(lib, "mng.lib")
#pragma comment(lib, "zlib.lib")
//jasper.lib jbig.lib Jpeg.lib libdcr.lib mng.lib png.lib Tiff.lib zlib.lib cximage.lib Winmm.lib Dbghelp.lib Scratch_d.lib libFTPClient.lib libFileTransfer_d.lib UBCCommon_kr_d.lib

#include "ImageCtrl.h"
#include "MemDC.h"

#define		FRAME_THICKNESS		1

// CImageCtrl

IMPLEMENT_DYNAMIC(CImageCtrl, CStatic)

CImageCtrl::CImageCtrl()
{
	m_pCxImage = new CxImage();
}

CImageCtrl::~CImageCtrl()
{
	if(m_pCxImage) delete m_pCxImage;
	m_pCxImage = NULL;
}


BEGIN_MESSAGE_MAP(CImageCtrl, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CImageCtrl 메시지 처리기입니다.

void CImageCtrl::SetImageFile(CString strFileName)
{
	if(!m_pCxImage) return;

	TCHAR szDrv[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szExt[MAX_PATH]={0};
	_tsplitpath(strFileName, szDrv, szPath, NULL, szExt);

	m_pCxImage->Load(strFileName, CxImage::GetTypeIdFromName(szExt+1));
}

void CImageCtrl::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	if(!m_pCxImage) return;
	if(!m_pCxImage->IsValid()) return;

	CRect rect;
	GetClientRect(&rect);

	CMemDC memDC(&dc, rect);

//	memDC.Draw3dRect(rect, FRAME_OUT_BACKGROUND_COLOR, FRAME_IN_BACKGROUND_COLOR);
//	memDC.FillSolidRect(rect, FRAME_OUT_BACKGROUND_COLOR);
	rect.DeflateRect(FRAME_THICKNESS, FRAME_THICKNESS);
/*
		// original
		dc.FillSolidRect(rect, RGB(255,255,255));
		rect.right = rect.left + m_pCxImage->GetWidth();
		rect.bottom = rect.top + m_pCxImage->GetHeight();
		m_pCxImage->Draw2(memDC.m_hDC, rect);
*/
	// fittoscreen
//	dc.FillSolidRect(rect, RGB(255,255,255));

	double scale;

	int image_width = m_pCxImage->GetWidth();
	int image_height = m_pCxImage->GetHeight();

	int scr_width = rect.Width();
	int scr_height = rect.Height();

	int width, height;
	width = scr_width;
	height = image_width * scr_height / scr_width;
	if(height < image_height)
	{
		height = scr_height;
		width = image_height * scr_width / scr_height;

		scale = ((double)scr_height) / ((double)image_height);
	}
	else
	{
		scale = ((double)scr_width) / ((double)image_width);
	}

	rect.right = rect.left + m_pCxImage->GetWidth() * scale;
	rect.bottom = rect.top + m_pCxImage->GetHeight() * scale;

	m_pCxImage->Draw2(memDC.m_hDC, rect);
}
