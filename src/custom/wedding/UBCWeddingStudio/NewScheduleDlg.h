#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "common\UTBListCtrl.h"
#include "common\hoverbutton.h"

// CNewScheduleDlg 대화 상자입니다.

class CNewScheduleDlg : public CDialog
{
	DECLARE_DYNAMIC(CNewScheduleDlg)

public:
	enum MODE { eNEW, eSAVE_AS, eEXPORT };
	CNewScheduleDlg(CWnd* pParent = NULL, CNewScheduleDlg::MODE nMode=eNEW);   // 표준 생성자입니다.
	virtual ~CNewScheduleDlg();

	// input
	MODE m_nMode;
	// output
	CString m_strSchedulePath;

	CUTBListCtrl m_lcList;
	CImageList	 m_imgForList;
	CHoverButton m_bnOK;
	CHoverButton m_bnCancel;
	CString m_strNewScheduleName;

	void FindDrive(CStringArray& astrDriveList);
	void InitList();

	static int CALLBACK CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_NEW_SCHEDULE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
