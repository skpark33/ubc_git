// UBCWeddingStudioDlg.h : 헤더 파일
//

#pragma once
#include "common\hoverbutton.h"
#include "common\ubctabctrl.h"
#include "afxwin.h"

// CUBCWeddingStudioDlg 대화 상자
class CUBCWeddingStudioDlg : public CDialog
{
// 생성입니다.
public:
	CUBCWeddingStudioDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	virtual ~CUBCWeddingStudioDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCWEDDINGSTUDIO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON		m_hIcon;
	CToolBar    m_wndToolBar;
	
	CString		m_strScheduleName;
	CString		m_strHallName;
	CComboBox	m_cbHallName;
	CString		m_strDesc;
	CString		m_strBreakTimeImage;
	CHoverButton m_bnBreakTimeImage;

	CUBCTabCtrl m_tabInfo;

	bool        InitToolbar();
	bool        CreateTabCtrl();
	bool        InitDataAndCtrl(CString strFileName);
	void		InitHallNameCombo();
	void		SaveHallNameCombo();
	bool        InitChildWnd();
	void		DeleteTabCtrl();
	CWnd*       GetChildWnd(int nIndex);

	bool		InitScheduleInfo(CString strFileName);
	bool		IsModified();
	bool		InputErrorCheck();
	bool		Save();
	bool		CheckAndSave(); // 오류 체크 및 저장 등

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnDestroy();
	afx_msg void OnTcnSelchangeTabInfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTcnSelchangingTabInfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCmdAbout();
	afx_msg void OnCmdDelete();
	afx_msg void OnCmdExit();
	afx_msg void OnCmdExport();
	afx_msg void OnCmdNew();
	afx_msg void OnCmdOpen();
	afx_msg void OnCmdSave();
	afx_msg void OnCmdSaveAs();
	afx_msg void OnBnClickedBnBreakTimeImage();
protected:
	virtual void OnOK();
	virtual void OnCancel();
public:
	afx_msg void OnStnDblclickTxtBreakTimeImage();
};
