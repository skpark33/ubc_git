#include "StdAfx.h"
#include "resource.h"
#include "WeddingDataManager.h"

CWeddingDataManager::CWeddingDataManager(void)
: m_nMaxWeddingData(10)
, m_nMaxImageData(20)
, m_strScheduleName(_T(""))
{
	InitXmlParser("");
}

CWeddingDataManager::~CWeddingDataManager(void)
{
}

CWeddingDataManager* CWeddingDataManager::_instance;
CWeddingDataManager* CWeddingDataManager::GetInstance()
{
	if(_instance == NULL) _instance = new CWeddingDataManager();
	return _instance;
}

int CWeddingDataManager::GetMaxWeddingData()
{
	return m_nMaxWeddingData;
}

void CWeddingDataManager::SetMaxWeddingData(int nValue)
{
	m_nMaxWeddingData = nValue;
}

int CWeddingDataManager::GetMaxImageData()
{
	return m_nMaxImageData;
}

void CWeddingDataManager::SetMaxImageData(int nValue)
{
	m_nMaxImageData = nValue;
}

bool CWeddingDataManager::InitXmlParser(CString strXmlFile)
{
	if(strXmlFile.IsEmpty())
	{
		m_objXmlParser.LoadXML(DEFAULT_XML);
		return false;
	}
	else if(m_objXmlParser.Load(strXmlFile) != XMLPARSER_SUCCESS)
	{
		m_objXmlParser.LoadXML(DEFAULT_XML);
		return false;
	}

	return true;
}

bool CWeddingDataManager::LoadSchedule(CString strSchedulePath)
{
	m_strSchedulePath = strSchedulePath;

	if(strSchedulePath.IsEmpty())
	{
		m_strScheduleName = "";
	}
	else
	{
		m_strScheduleName = strSchedulePath.Mid(strSchedulePath.ReverseFind('\\')+1);
		CString strTmp = m_strScheduleName;
		strTmp.MakeLower();
		if(strTmp.Find("data_") == 0)
		{
			m_strScheduleName = m_strScheduleName.Mid(5);
		}
	}

	bool bResult = InitXmlParser(strSchedulePath + "\\wedding.xml");
	SetWeddingInfo("name", m_strScheduleName);

	return bResult;
}

bool CWeddingDataManager::CheckSchedule()
{
	return true;
}

bool CWeddingDataManager::SaveSchedule()
{
	if(m_strSchedulePath.IsEmpty()) return false;

	HWND hWnd = NULL;
	if(AfxGetApp() && AfxGetApp()->GetMainWnd())
	{
		hWnd = AfxGetApp()->GetMainWnd()->GetSafeHwnd();
	}
	::SHCreateDirectoryEx(hWnd, m_strSchedulePath+"\\image", NULL);

	if(m_objXmlParser.Save(m_strSchedulePath+"\\wedding.xml") != XMLPARSER_SUCCESS)
	{
		return false;
	}

	return true;
}

bool CWeddingDataManager::SaveAsSchedule(CString strNewSchedulePath)
{
	if(!strNewSchedulePath.IsEmpty() && m_strSchedulePath == strNewSchedulePath)
	{
		CString strMsg;
		strMsg.Format(LoadStringFromID(IDS_MSG_020), m_strSchedulePath, strNewSchedulePath);
		AfxMessageBox(strMsg);
		return false;
	}

	HWND hWnd = NULL;
	if(AfxGetApp() && AfxGetApp()->GetMainWnd())
	{
		hWnd = AfxGetApp()->GetMainWnd()->GetSafeHwnd();
	}

	::SHCreateDirectoryEx(hWnd, strNewSchedulePath, NULL);

	TCHAR szFrom[MAX_PATH+1] = {0};
	strcpy(szFrom, strNewSchedulePath);

	SHFILEOPSTRUCT sh;
	memset(&sh, 0x00, sizeof(SHFILEOPSTRUCT));
	sh.hwnd = hWnd;
	sh.wFunc = FO_DELETE;
	sh.pFrom = szFrom;
	sh.pTo = NULL;
	sh.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_SIMPLEPROGRESS | FOF_NOERRORUI;
	sh.fAnyOperationsAborted = FALSE;
	sh.lpszProgressTitle = "다른 이름으로 저장 준비중";
	::SHFileOperation(&sh);

	memset(&szFrom, 0x00, sizeof(szFrom));
	strcpy(szFrom, m_strSchedulePath);

	TCHAR szTo[MAX_PATH+1] = {0};
	strcpy(szTo, strNewSchedulePath);

	memset(&sh, 0x00, sizeof(SHFILEOPSTRUCT));
	sh.hwnd = hWnd;
	sh.wFunc = FO_COPY;
	sh.pFrom = szFrom;
	sh.pTo = szTo;
	sh.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_SIMPLEPROGRESS;// | FOF_NOERRORUI;
	sh.fAnyOperationsAborted = FALSE;
	sh.lpszProgressTitle = "다른 이름으로 저장";

	int nResult = SHFileOperation(&sh);

	if(nResult != 0 || sh.fAnyOperationsAborted != FALSE) return false;
	else if(m_objXmlParser.Save(strNewSchedulePath+"\\wedding.xml") != XMLPARSER_SUCCESS)
	{
		return false;
	}

	return true;
}

CString CWeddingDataManager::GetWeddingInfo(CString strTag)
{
	if(m_objXmlParser.GetXPath() != "/WHS") m_objXmlParser.SetXPath("/WHS");
	CString strValue = m_objXmlParser.GetElement(strTag);
	return strValue;
}

void CWeddingDataManager::SetWeddingInfo(CString strTag, CString strValue)
{
	if(m_objXmlParser.GetXPath() != "/WHS") m_objXmlParser.SetXPath("/WHS");
	m_objXmlParser.DeleteElement(strTag);
	m_objXmlParser.NewElement(strTag, 0, strValue);
}

CString CWeddingDataManager::GetWeddingDtlInfo(int nIndex, CString strTag)
{
	CString strXPath;
	strXPath.Format("/WHS/weddingInfo [@index=\"%d\"]", nIndex);
	if(m_objXmlParser.GetXPath() != strXPath) m_objXmlParser.SetXPath(strXPath);
	CString strValue = m_objXmlParser.GetElement(strTag);
	return strValue;
}

void CWeddingDataManager::SetWeddingDtlInfo(int nIndex, CString strTag, CString strValue)
{
	CString strXPath;
	strXPath.Format("/WHS/weddingInfo [@index=\"%d\"]", nIndex);
	if(m_objXmlParser.GetXPath() != strXPath) m_objXmlParser.SetXPath(strXPath);
	m_objXmlParser.DeleteElement(strTag);
	m_objXmlParser.NewElement(strTag, 0, strValue);
}

CString CWeddingDataManager::GetWeddingTimeInfo(int nIndex, CString strTag)
{
	CString strXPath;
	strXPath.Format("/WHS/weddingInfo [@index=\"%d\"]/timeInfo", nIndex);
	if(m_objXmlParser.GetXPath() != strXPath) m_objXmlParser.SetXPath(strXPath);
	CString strValue = m_objXmlParser.GetElement(strTag);
	return strValue;
}

void CWeddingDataManager::SetWeddingTimeInfo(int nIndex, CString strTag, CString strValue)
{
	CString strXPath;
	strXPath.Format("/WHS/weddingInfo [@index=\"%d\"]/timeInfo", nIndex);
	if(m_objXmlParser.GetXPath() != strXPath) m_objXmlParser.SetXPath(strXPath);
	m_objXmlParser.DeleteElement(strTag);
	m_objXmlParser.NewElement(strTag, 0, strValue);
}

int CWeddingDataManager::GetWeddingImageList(int nIndex, CStringArray& astrFileList)
{
	CString strXPath;
	strXPath.Format("/WHS/weddingInfo [@index=\"%d\"]/images/fileName", nIndex);
	if(m_objXmlParser.SetXPath(strXPath) != XMLPARSER_SUCCESS) return 0;
	int nCnt = m_objXmlParser.GetSelCount();
	for(int i=0; i<nCnt ;i++)
	{
		CString strValue = m_objXmlParser.GetData(i);
		if(strValue.IsEmpty()) continue;
		astrFileList.Add(strValue);
	}
	return astrFileList.GetCount();
}

void CWeddingDataManager::SetWeddingImageList(int nIndex, CStringArray& astrFileList)
{
	CString strXPath;
	strXPath.Format("/WHS/weddingInfo [@index=\"%d\"]", nIndex);
	m_objXmlParser.SetXPath(strXPath+"[images]");
	m_objXmlParser.DeleteElement("images");
	m_objXmlParser.NewElement("images");

	m_objXmlParser.SetXPath(strXPath+"/images");
	for(int i=0; i<astrFileList.GetCount() ;i++)
	{
		m_objXmlParser.NewElement("fileName", 0, astrFileList[i]);
	}
}

//void CWeddingDataManager::MakeScheduleDir(CString strSchedulePath)
//{
//	if(strSchedulePath.IsEmpty()) return;
//}
