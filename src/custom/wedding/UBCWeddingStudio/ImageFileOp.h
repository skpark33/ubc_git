#pragma once

class CImageFileOp
{
public:
	CImageFileOp(void);
	virtual ~CImageFileOp(void);

	static bool ImageFileOpenAndCopy(CString& strResultFileName);
	static void ImageFilesCopy(CStringArray& astrSrcImageFiles, CStringArray& astrTarImageFiles);
};
