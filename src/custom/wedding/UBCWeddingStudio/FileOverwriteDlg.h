#pragma once

#include "ImageCtrl.h"
#include "afxwin.h"

// CFileOverwriteDlg 대화 상자입니다.

class CFileOverwriteDlg : public CDialog
{
	DECLARE_DYNAMIC(CFileOverwriteDlg)

public:
	enum COPY_METHOD { OVER_WRITE, SKIP, RENAME_COPY };
	CFileOverwriteDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFileOverwriteDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FILE_OVERWRITE_DLG };

	// input
	CString		m_strOverWriteFilePath;
	CString		m_strSkipFilePath;
	bool		m_bIsSingleFile;

	// output
	int			m_nCopyMethod;
	CString		m_strNewFilePath; // RENAME_COPY only
	BOOL		m_bAutoResolution;
	BOOL		m_bApplyAll;

	// 
	CImageCtrl	m_picOverWriteImage;
	CString		m_strOverWriteFileName;
	CString		m_strOverWriteFileSize;
	CString		m_strOverWriteFileDate;

	CImageCtrl	m_picSkip;
	CString		m_strSkipFileName;
	CString		m_strSkipFileSize;
	CString		m_strSkipFileDate;

	CString		m_strRenameCopyDesc;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedBnOverwirite();
	afx_msg void OnBnClickedBnSkip();
	afx_msg void OnBnClickedBnRenameCopy();
	virtual BOOL OnInitDialog();
};
