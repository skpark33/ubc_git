// OleListDropTarget.h: interface for the COleListDropTarget class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <afxole.h>
#define	 WM_DROP_FILE_LIST (WM_USER + 102)

class COleListDropTarget : public COleDropTarget
{
public:
	COleListDropTarget(CWnd* pParent = NULL);
	virtual ~COleListDropTarget();

	DROPEFFECT	OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	DROPEFFECT	OnDragOver (CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	BOOL		OnDrop     (CWnd* pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point );
	void		OnDragLeave(CWnd* pWnd);

	void SetParent(CWnd* pParent) {	m_pParent = pParent; };

// Attributes
protected:
	CWnd*	m_pParent;
};

class CStgMedium : public STGMEDIUM
{
public:
    CStgMedium()
    {
        ::ZeroMemory(this, sizeof(this));
    }
    virtual ~CStgMedium()
    {
        ::ReleaseStgMedium(this);
    }
};

bool EnumFileNamesFromDataObject(LPDATAOBJECT spDataObject, CStringArray& filenameList);
