#pragma once


// CImageCtrl

class CxImage;
class CImageCtrl : public CStatic
{
	DECLARE_DYNAMIC(CImageCtrl)

	CString  m_strFileName;
	CxImage* m_pCxImage;

public:
	CImageCtrl();
	virtual ~CImageCtrl();

	void SetImageFile(CString strFileName);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};


