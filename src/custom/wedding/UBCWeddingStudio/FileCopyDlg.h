#pragma once

#include "ImageCtrl.h"
#include "afxwin.h"

// CFileCopyDlg 대화 상자입니다.

class CFileCopyDlg : public CDialog
{
	DECLARE_DYNAMIC(CFileCopyDlg)

public:
	CFileCopyDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CFileCopyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FILE_COPY_DLG };

	// input
	CString		m_strFilePath;
	bool		m_bIsSingleFile;

	// output
	int			m_nCopyMethod;
	BOOL		m_bAutoResolution;
	BOOL		m_bApplyAll;

	// 
	CImageCtrl	m_picImage;
	CString		m_strFileName;
	CString		m_strFileSize;
	CString		m_strFileDate;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
};
