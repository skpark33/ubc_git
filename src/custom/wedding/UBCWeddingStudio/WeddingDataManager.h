#pragma once
#include "common/libXmlParser.h"

#define WeddingDataMng()	CWeddingDataManager::GetInstance()
#define DEFAULT_XML			"<?xml version=\"1.0\" encoding=\"EUC-KR\" standalone=\"yes\" ?> " \
							"<WHS>" \
								"<name/>" \
								"<desc/>" \
								"<hall/>" \
								"<breakTime/>" \
								"<weddingInfo index=\"1\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"2\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"3\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"4\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"5\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"6\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"7\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"8\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"9\" ><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
								"<weddingInfo index=\"10\"><groomName/><groomParent/><brideName/><brideParent/><timeInfo><weddingTime/><startTime/><endTime/></timeInfo><images><fileName/></images><guestBookImage/><weddingBanquet/></weddingInfo>" \
							"</WHS>"

class CWeddingDataManager
{
private:
	static CWeddingDataManager* _instance;

	int				m_nMaxWeddingData;
	int				m_nMaxImageData;

	CString			m_strSchedulePath; // ��) C:\SQISoft\Contents\Enc\Pluggins\Wedding\data_[Ȧ�������]
	CString			m_strScheduleName; // m_strSchedulePath ���� Ȧ ���������
	CXmlParser		m_objXmlParser;

	bool			InitXmlParser(CString strXmlFile);

public:
	CWeddingDataManager(void);
	virtual ~CWeddingDataManager(void);

	static			CWeddingDataManager* GetInstance();

	int				GetMaxWeddingData();
	void			SetMaxWeddingData(int);

	int				GetMaxImageData();
	void			SetMaxImageData(int);

	bool			IsInputMode() { return !GetScheduleName().IsEmpty(); }
	CString			GetSchedulePath() { return m_strSchedulePath; }
	CString			GetScheduleName() { return m_strScheduleName; }
	CString			GetDefaultPath() { return "SQISoft\\Contents\\Enc\\Plugins\\Wedding\\"; }
	CString			GetImagePath() { return m_strSchedulePath+"\\Image\\"; }

	// ���������� ������ ó��
	bool			LoadSchedule(CString strSchedulePath);
	bool			CheckSchedule();
	bool			SaveSchedule();
	bool			SaveAsSchedule(CString strNewSchedulePath);

	CString			GetWeddingInfo(CString strTag);
	void			SetWeddingInfo(CString strTag, CString strValue);

	CString			GetWeddingDtlInfo(int nIndex, CString strTag);
	void			SetWeddingDtlInfo(int nIndex, CString strTag, CString strValue);

	CString			GetWeddingTimeInfo(int nIndex, CString strTag);
	void			SetWeddingTimeInfo(int nIndex, CString strTag, CString strValue);

	int				GetWeddingImageList(int nIndex, CStringArray& astrFileList);
	void			SetWeddingImageList(int nIndex, CStringArray& astrFileList);
};
