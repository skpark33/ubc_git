// HallInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCWeddingStudio.h"
#include "HallInfoDlg.h"
#include "WeddingDataManager.h"
#include "ImageFileOp.h"


// CHallInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHallInfoDlg, CDialog)

CHallInfoDlg::CHallInfoDlg(CWnd* pParent, int nIndex)
	: CDialog(CHallInfoDlg::IDD, pParent)
	, m_nIndex(nIndex)
	, m_strGroomName(_T(""))
	, m_strGroomParentName(_T(""))
	, m_strBrideName(_T(""))
	, m_strBrideParentName(_T(""))
	, m_strGuestBookImage(_T(""))
	, m_tmWeddingStartTime(COleDateTime::GetCurrentTime())
	, m_tmShowStartTime(COleDateTime::GetCurrentTime())
	, m_tmShowEndTime(COleDateTime::GetCurrentTime())
	, m_bGuestBookImage(FALSE)
	, m_strWeddingBanquet(_T(""))
{
}

CHallInfoDlg::~CHallInfoDlg()
{
}

void CHallInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EB_GROOM_NAME, m_strGroomName);
	DDV_MaxChars(pDX, m_strGroomName, 32);
	DDX_Text(pDX, IDC_EB_GROOM_PARENT_NAME, m_strGroomParentName);
	DDV_MaxChars(pDX, m_strGroomParentName, 32);
	DDX_Text(pDX, IDC_EB_BRIDE_NAME, m_strBrideName);
	DDV_MaxChars(pDX, m_strBrideName, 32);
	DDX_Text(pDX, IDC_EB_BRIDE_PARENT_NAME, m_strBrideParentName);
	DDV_MaxChars(pDX, m_strBrideParentName, 32);
	DDX_DateTimeCtrl(pDX, IDC_DT_WEDDING_START_TIME, m_tmWeddingStartTime);
	DDX_DateTimeCtrl(pDX, IDC_DT_SHOW_START_TIME, m_tmShowStartTime);
	DDX_DateTimeCtrl(pDX, IDC_DT_SHOW_END_TIME, m_tmShowEndTime);
	DDX_Control(pDX, IDC_BN_CLEAR, m_bnClear);
	DDX_Control(pDX, IDC_LC_LIST, m_lcList);
	DDX_Control(pDX, IDC_BN_ADD_IMAGE, m_bnAdd);
	DDX_Control(pDX, IDC_BN_REMOVE_IMAGE, m_bnRemove);
	DDX_Control(pDX, IDC_BN_MOVE_UP_IMAGE, m_bnMoveUp);
	DDX_Control(pDX, IDC_BN_MOVE_DOWN_IMAGE, m_bnMoveDown);
	DDX_Check(pDX, IDC_KB_GUESTBOOK_IMAGE, m_bGuestBookImage);
	DDX_Text(pDX, IDC_TXT_GUESTBOOK_IMAGE, m_strGuestBookImage);
	DDX_Control(pDX, IDC_BN_GUESTBOOK_IMAGE, m_bnGuestBookImage);
	DDX_Text(pDX, IDC_EB_WEDDING_BANQUET, m_strWeddingBanquet);
	DDV_MaxChars(pDX, m_strWeddingBanquet, 32);
}


BEGIN_MESSAGE_MAP(CHallInfoDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CHallInfoDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CHallInfoDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_CLEAR, &CHallInfoDlg::OnBnClickedBnClear)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DT_WEDDING_START_TIME, &CHallInfoDlg::OnDtnDatetimechangeDtWeddingStartTime)
	ON_BN_CLICKED(IDC_BN_GUESTBOOK_IMAGE, &CHallInfoDlg::OnBnClickedBnGuestbookImage)
	ON_BN_CLICKED(IDC_BN_ADD_IMAGE, &CHallInfoDlg::OnBnClickedBnAddImage)
	ON_BN_CLICKED(IDC_BN_REMOVE_IMAGE, &CHallInfoDlg::OnBnClickedBnRemoveImage)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LC_LIST, &CHallInfoDlg::OnLvnColumnclickLcList)
	ON_WM_DESTROY()
	ON_MESSAGE(WM_DROP_FILE_LIST, OnDropFileList)
	ON_BN_CLICKED(IDC_BN_MOVE_UP_IMAGE, &CHallInfoDlg::OnBnClickedBnMoveUpImage)
	ON_BN_CLICKED(IDC_BN_MOVE_DOWN_IMAGE, &CHallInfoDlg::OnBnClickedBnMoveDownImage)
	ON_NOTIFY(NM_DBLCLK, IDC_LC_LIST, &CHallInfoDlg::OnNMDblclkLcList)
	ON_STN_DBLCLK(IDC_TXT_GUESTBOOK_IMAGE, &CHallInfoDlg::OnStnDblclickTxtGuestbookImage)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LC_LIST, &CHallInfoDlg::OnLvnKeydownLcList)
END_MESSAGE_MAP()


// CHallInfoDlg 메시지 처리기입니다.

void CHallInfoDlg::OnBnClickedOk()
{
//	OnOK();
}

void CHallInfoDlg::OnBnClickedCancel()
{
//	OnCancel();
}

BOOL CHallInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CDateTimeCtrl* pCtrl = (CDateTimeCtrl*) GetDlgItem(IDC_DT_WEDDING_START_TIME);
	pCtrl->SetFormat("yyyy/MM/dd HH:mm:ss");
	pCtrl = (CDateTimeCtrl*) GetDlgItem(IDC_DT_SHOW_START_TIME);
	pCtrl->SetFormat("yyyy/MM/dd HH:mm:ss");
	pCtrl = (CDateTimeCtrl*) GetDlgItem(IDC_DT_SHOW_END_TIME);
	pCtrl->SetFormat("yyyy/MM/dd HH:mm:ss");

	m_lcList.SetExtendedStyle(LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT | LVS_EX_SUBITEMIMAGES | LVS_EX_GRIDLINES);
	m_lcList.InsertColumn(eCheck   , _T("")               , LVCFMT_LEFT, 22);
	m_lcList.InsertColumn(eFileName, _T("Image File Name"), LVCFMT_LEFT, 300);
	m_lcList.InitHeader(IDB_LIST_HEADER);

	m_bnClear.LoadBitmap(IDB_BTN_CLEAR, RGB(255, 255, 255));
	m_bnClear.SetToolTipText(IDS_INFO_CLEAR);

	m_bnAdd.LoadBitmap(IDB_BTN_PLUS, RGB(255, 255, 255));
	m_bnAdd.SetToolTipText(IDS_IMAGE_ADD);

	m_bnRemove.LoadBitmap(IDB_BTN_MINUS, RGB(255, 255, 255));
	m_bnRemove.SetToolTipText(IDS_IMAGE_REMOVE);

	m_bnMoveUp.LoadBitmap(IDB_BTN_UP, RGB(255, 255, 255));
	m_bnMoveUp.SetToolTipText(IDS_IMAGE_MOVE_UP);

	m_bnMoveDown.LoadBitmap(IDB_BTN_DOWN, RGB(255, 255, 255));
	m_bnMoveDown.SetToolTipText(IDS_IMAGE_MOVE_DOWN);

	m_bnGuestBookImage.LoadBitmap(IDB_BTN_OPEN, RGB(255, 255, 255));
	m_bnGuestBookImage.SetToolTipText(IDS_GUESTBOOK_IMAGE);

	InitDataAndCtrl();

	RegisterDragDrop();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

bool CHallInfoDlg::InitDataAndCtrl()
{
	m_strGroomName       = WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "groomName");
	m_strGroomParentName = WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "groomParent");
	m_strBrideName       = WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "brideName");
	m_strBrideParentName = WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "brideParent");
	m_strWeddingBanquet  = WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "weddingBanquet");
	m_strGuestBookImage  = WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "guestBookImage");
	m_bGuestBookImage    = !m_strGuestBookImage.IsEmpty();

	CString strWeddingStartTime = WeddingDataMng()->GetWeddingTimeInfo(m_nIndex, "weddingTime");
	CString strShowStartTime    = WeddingDataMng()->GetWeddingTimeInfo(m_nIndex, "startTime");
	CString strShowEndTime      = WeddingDataMng()->GetWeddingTimeInfo(m_nIndex, "endTime");

	if(strWeddingStartTime.IsEmpty())
	{
		strWeddingStartTime = CTime::GetCurrentTime().Format(IDS_DATETIME_FORMAT);
		m_tmWeddingStartTime.ParseDateTime(strWeddingStartTime);
		AutoSetShowTime(strWeddingStartTime);
	}
	else if( strShowStartTime.IsEmpty() ||
			 strShowEndTime.IsEmpty()   )
	{
		m_tmWeddingStartTime.ParseDateTime(strWeddingStartTime);
		AutoSetShowTime(strWeddingStartTime);
	}
	else
	{
		m_tmWeddingStartTime.ParseDateTime(strWeddingStartTime);
		m_tmShowStartTime.ParseDateTime(strShowStartTime);
		m_tmShowEndTime.ParseDateTime(strShowEndTime);
	}
	
	m_lcList.DeleteAllItems();

	if(WeddingDataMng()->IsInputMode())
	{
		CStringArray astrFileList;
		WeddingDataMng()->GetWeddingImageList(m_nIndex, astrFileList);
		for(int i=0; i<astrFileList.GetCount() ;i++)
		{
			int nItem = m_lcList.InsertItem(i, "");
			m_lcList.SetItemText(nItem, eFileName, astrFileList[i]);
		}
	}

	GetDlgItem(IDC_EB_GROOM_NAME        )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_EB_GROOM_PARENT_NAME )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_EB_BRIDE_NAME        )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_EB_BRIDE_PARENT_NAME )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_DT_WEDDING_START_TIME)->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_DT_SHOW_START_TIME   )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_DT_SHOW_END_TIME     )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_EB_WEDDING_BANQUET   )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_BN_CLEAR             )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_LC_LIST              )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_BN_ADD_IMAGE         )->EnableWindow(WeddingDataMng()->IsInputMode());	
	GetDlgItem(IDC_BN_REMOVE_IMAGE      )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_BN_MOVE_UP_IMAGE     )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_BN_MOVE_DOWN_IMAGE   )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_KB_GUESTBOOK_IMAGE   )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_TXT_GUESTBOOK_IMAGE  )->EnableWindow(WeddingDataMng()->IsInputMode());
	GetDlgItem(IDC_BN_GUESTBOOK_IMAGE   )->EnableWindow(WeddingDataMng()->IsInputMode());

	UpdateData(FALSE);

	return true;
}

bool CHallInfoDlg::IsInputData() 
{
	CString strText;
	GetDlgItemText(IDC_EB_GROOM_NAME, strText);
	if(!strText.IsEmpty()) return true;

	GetDlgItemText(IDC_EB_BRIDE_NAME, strText);
	if(!strText.IsEmpty()) return true;

	return false;
}

bool CHallInfoDlg::IsModified()
{
	if(!WeddingDataMng()->IsInputMode()) return false;

	UpdateData(TRUE);

	if( m_strGroomName       != WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "groomName")     ) return true;
	if( m_strGroomParentName != WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "groomParent")   ) return true;
	if( m_strBrideName       != WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "brideName")     ) return true;
	if( m_strBrideParentName != WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "brideParent")   ) return true;
	if( m_strWeddingBanquet  != WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "weddingBanquet")) return true;
	if( m_strGuestBookImage  != WeddingDataMng()->GetWeddingDtlInfo(m_nIndex, "guestBookImage")) return true;

	if(IsInputData())
	{
		if( m_tmWeddingStartTime.Format((UINT)IDS_DATETIME_FORMAT) != WeddingDataMng()->GetWeddingTimeInfo(m_nIndex, "weddingTime")) return true;
		if( m_tmShowStartTime   .Format((UINT)IDS_DATETIME_FORMAT) != WeddingDataMng()->GetWeddingTimeInfo(m_nIndex, "startTime")  ) return true;
		if( m_tmShowEndTime     .Format((UINT)IDS_DATETIME_FORMAT) != WeddingDataMng()->GetWeddingTimeInfo(m_nIndex, "endTime")    ) return true;
	}

	CStringArray astrFileList;
	WeddingDataMng()->GetWeddingImageList(m_nIndex, astrFileList);
	if(m_lcList.GetItemCount() != astrFileList.GetCount()) return true;
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		bool bIsExist = false;
		CString strFileName1 = m_lcList.GetItemText(i, eFileName);
		strFileName1.MakeLower();
		for(int j=0; j<astrFileList.GetCount() ; j++)
		{
			CString strFileName2 = astrFileList[j];
			strFileName2.MakeLower();
			if(strFileName1 == strFileName2)
			{
				bIsExist = true;
				break;
			}
		}

		if(!bIsExist) return true;
	}

	return false;
}

bool CHallInfoDlg::InputErrorCheck()
{
	UpdateData(TRUE);

	if(!IsInputData()) return true;

	// 방송 시작시간이 종료시간보다 큰경우
	if( m_tmShowStartTime.Format((UINT)IDS_DATETIME_FORMAT) > 
		m_tmShowEndTime.Format((UINT)IDS_DATETIME_FORMAT)   )
	{
		AfxMessageBox(IDS_MSG_016);
		return false;
	}

	if(m_lcList.GetItemCount() <= 0)
	{
		AfxMessageBox(IDS_MSG_006);
		return false;
	}

	return true;
}

bool CHallInfoDlg::SaveData()
{
	UpdateData(TRUE);

	CString strGuestBookImage = m_strGuestBookImage;
	if(!m_bGuestBookImage) strGuestBookImage = "";

	WeddingDataMng()->SetWeddingDtlInfo(m_nIndex, "groomName"     , m_strGroomName);
	WeddingDataMng()->SetWeddingDtlInfo(m_nIndex, "groomParent"   , m_strGroomParentName);
	WeddingDataMng()->SetWeddingDtlInfo(m_nIndex, "brideName"     , m_strBrideName);
	WeddingDataMng()->SetWeddingDtlInfo(m_nIndex, "brideParent"   , m_strBrideParentName);
	WeddingDataMng()->SetWeddingDtlInfo(m_nIndex, "guestBookImage", strGuestBookImage);
	WeddingDataMng()->SetWeddingDtlInfo(m_nIndex, "weddingBanquet", m_strWeddingBanquet);

	CString strWeddingStartTime;
	CString strShowStartTime   ;
	CString strShowEndTime     ;

	if(IsInputData())
	{
		strWeddingStartTime = m_tmWeddingStartTime.Format((UINT)IDS_DATETIME_FORMAT);
		strShowStartTime    = m_tmShowStartTime.Format((UINT)IDS_DATETIME_FORMAT);
		strShowEndTime      = m_tmShowEndTime.Format((UINT)IDS_DATETIME_FORMAT);
	}

	WeddingDataMng()->SetWeddingTimeInfo(m_nIndex, "weddingTime", strWeddingStartTime);
	WeddingDataMng()->SetWeddingTimeInfo(m_nIndex, "startTime"  , strShowStartTime   );
	WeddingDataMng()->SetWeddingTimeInfo(m_nIndex, "endTime"    , strShowEndTime     );

	CStringArray astrFileList;
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		astrFileList.Add(m_lcList.GetItemText(i, eFileName));
	}
	WeddingDataMng()->SetWeddingImageList(m_nIndex, astrFileList);

	return true;
}

void CHallInfoDlg::OnBnClickedBnClear()
{
	UpdateData(TRUE);

	m_strGroomName = "";
	m_strGroomParentName = "";
	m_strBrideName = "";
	m_strBrideParentName = "";
	m_strWeddingBanquet = "";
	m_bGuestBookImage = FALSE;
	m_strGuestBookImage = "";
	
	m_tmWeddingStartTime = COleDateTime::GetCurrentTime();
	m_tmShowStartTime = COleDateTime::GetCurrentTime();
	m_tmShowEndTime = COleDateTime::GetCurrentTime();

	TCHAR szDelFileList[MAX_PATH * 20]={0};
	int nPos = 0;
	for(int i=0; i<m_lcList.GetItemCount() ;i++)
	{
		CString strFileName = m_lcList.GetItemText(i, 0);
		_tcscpy(szDelFileList + nPos, strFileName);
		nPos += strFileName.GetLength() + 1;
	}
	m_lcList.DeleteAllItems();

	//SHFILEOPSTRUCT sh;
	//memset(&sh, 0x00, sizeof(SHFILEOPSTRUCT));
	//sh.hwnd = GetSafeHwnd();
	//sh.wFunc = FO_DELETE;
	//sh.pFrom = szDelFileList;
	//sh.pTo = NULL;
	//sh.fFlags = FOF_FILESONLY;
	//sh.fAnyOperationsAborted = FALSE;
	//::SHFileOperation(&sh);

	UpdateData(FALSE);
}

void CHallInfoDlg::AutoSetShowTime(CString strWeddingTime)
{
	COleDateTime tmWeddingTime;
	tmWeddingTime.ParseDateTime(strWeddingTime);
	COleDateTimeSpan tmTimeSpan(0, 0, 30, 0);
	m_tmShowStartTime = tmWeddingTime - tmTimeSpan;
	m_tmShowEndTime = tmWeddingTime + tmTimeSpan;
}

void CHallInfoDlg::OnDtnDatetimechangeDtWeddingStartTime(NMHDR *pNMHDR, LRESULT *pResult)
{
	UpdateData(TRUE);

	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);

	AutoSetShowTime(COleDateTime(pDTChange->st).Format((UINT)IDS_DATETIME_FORMAT));

	UpdateData(FALSE);

	*pResult = 0;
}

void CHallInfoDlg::OnBnClickedBnGuestbookImage()
{
	CString strTargetFilePath;
	if(!CImageFileOp::ImageFileOpenAndCopy(strTargetFilePath)) return;

	UpdateData(TRUE);
	
	TCHAR szDrv[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szName[MAX_PATH]={0}, szExt[MAX_PATH]={0};
	_tsplitpath(strTargetFilePath, szDrv, szPath, szName, szExt);

	m_strGuestBookImage.Format("%s%s", szName, szExt);
	SetDlgItemText(IDC_TXT_GUESTBOOK_IMAGE, m_strGuestBookImage);

	m_bGuestBookImage = TRUE;

	UpdateData(FALSE);
}

void CHallInfoDlg::OnBnClickedBnAddImage()
{
	int nMaxCnt = WeddingDataMng()->GetMaxImageData();
	int nCurCnt = m_lcList.GetItemCount();

	if(nCurCnt >= nMaxCnt)
	{
		AfxMessageBox(IDS_MSG_007);
		return;
	}

	CString szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
	CString filter;
	filter.Format("All Image Files|%s|"
				  "Bitmap Files (*.bmp)|*.bmp|"
				  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
				  "GIF Files (*.gif)|*.gif|"
				  "PCX Files (*.pcx)|*.pcx|"
				  "PNG Files (*.png)|*.png|"
				  "TIFF Files (*.tif)|*.tif;*.tiff||"
				 , szFileExts
				 );

	CFileDialog dlgOpen(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT, filter, AfxGetMainWnd());
	CString strFileName;
	int nBuffSize = (nMaxCnt * (MAX_PATH+1)) + 1;
	dlgOpen.GetOFN().lpstrFile = strFileName.GetBuffer(nBuffSize);
	dlgOpen.GetOFN().nMaxFile = nBuffSize;
	int nResult = dlgOpen.DoModal();
	strFileName.ReleaseBuffer();
	if(nResult != IDOK)
	{
		DWORD dwError = CommDlgExtendedError();
		if( dwError == 0x3003 )
		{
			AfxMessageBox(LoadStringFromID(IDS_MSG_021) + _T("\n") + LoadStringFromID(IDS_MSG_007));
		}
		return;
	}

	CStringArray astrSrcImageFiles;
	POSITION pos = dlgOpen.GetStartPosition();
	while(pos)
	{
		CString strImagePath = dlgOpen.GetNextPathName(pos);
		astrSrcImageFiles.Add(strImagePath);
	}

	if(astrSrcImageFiles.GetCount() > (nMaxCnt-nCurCnt))
	{
		astrSrcImageFiles.RemoveAt(nMaxCnt-nCurCnt, astrSrcImageFiles.GetCount()-(nMaxCnt-nCurCnt));
	}

	CStringArray astrTarImageFiles;
	CImageFileOp::ImageFilesCopy(astrSrcImageFiles, astrTarImageFiles);

	for(int i=0; i<astrTarImageFiles.GetCount() ;i++)
	{
		CString strImageFile = astrTarImageFiles[i];

		if(strImageFile.IsEmpty()) continue;

		TCHAR szDrv[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szName[MAX_PATH]={0}, szExt[MAX_PATH]={0};
		_tsplitpath(strImageFile, szDrv, szPath, szName, szExt);

		strImageFile.Format("%s%s", szName, szExt);

		int nItem = m_lcList.InsertItem(m_lcList.GetItemCount(), _T(""));
		m_lcList.SetItemText(nItem, eFileName, strImageFile);
	}
}

void CHallInfoDlg::OnBnClickedBnRemoveImage()
{
	for(int nItem=m_lcList.GetItemCount()-1; nItem>=0 ;nItem--)
	{
		if(!m_lcList.GetCheck(nItem)) continue;
		m_lcList.DeleteItem(nItem);
	}
}

void CHallInfoDlg::OnLvnColumnclickLcList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	int	nColumn = pNMLV->iSubItem;

	if(nColumn == eCheck)
	{
		bool bCheck = !m_lcList.GetCheckHdrState();
		for(int nRow=0; nRow<m_lcList.GetItemCount() ;nRow++)
		{
			m_lcList.SetCheck(nRow, bCheck);
		}
		m_lcList.SetCheckHdrState(bCheck);
	}

	*pResult = 0;
}

bool CHallInfoDlg::RegisterDragDrop()
{
	m_lcList.DragAcceptFiles();

	if(m_objOleDrop.Register(&m_lcList))
	{
		m_objOleDrop.SetParent(this);
	}

	return true;
}

bool CHallInfoDlg::UnregisterDragDrop()
{
	m_objOleDrop.Revoke();
	m_objOleDrop.SetParent(NULL);

	return true;
}

void CHallInfoDlg::OnDestroy()
{
	CDialog::OnDestroy();

	UnregisterDragDrop();
}

LRESULT CHallInfoDlg::OnDropFileList(WPARAM wParam, LPARAM lParam)
{
	CStringArray* pSrcFileList = (CStringArray*)wParam;
	if(!pSrcFileList) return 0;

	int nMaxCnt = WeddingDataMng()->GetMaxImageData();
	int nCurCnt = m_lcList.GetItemCount();

	if(nCurCnt >= nMaxCnt)
	{
		AfxMessageBox(IDS_MSG_007);
		return 0;
	}

	if(pSrcFileList->GetCount() > (nMaxCnt-nCurCnt))
	{
		pSrcFileList->RemoveAt(nMaxCnt-nCurCnt, pSrcFileList->GetCount()-(nMaxCnt-nCurCnt));
	}

	CStringArray astrTarImageFiles;
	CImageFileOp::ImageFilesCopy(*pSrcFileList, astrTarImageFiles);

	for(int i=0; i<astrTarImageFiles.GetCount() ;i++)
	{
		CString strImageFile = astrTarImageFiles[i];

		TCHAR szDrv[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szName[MAX_PATH]={0}, szExt[MAX_PATH]={0};
		_tsplitpath(strImageFile, szDrv, szPath, szName, szExt);

		strImageFile.Format("%s%s", szName, szExt);

		int nItem = m_lcList.InsertItem(m_lcList.GetItemCount(), _T(""));
		m_lcList.SetItemText(nItem, eFileName, strImageFile);
	}

	delete pSrcFileList;

	return 0;
}

void CHallInfoDlg::OnBnClickedBnMoveUpImage()
{
	for(int nItem=1; nItem<m_lcList.GetItemCount(); nItem++)
	{
		if(!m_lcList.GetItemState(nItem, LVIS_SELECTED)) continue;

		int nSwapItem = nItem-1;

		BOOL bCheck = m_lcList.GetCheck(nItem);
		CString strItem = m_lcList.GetItemText(nItem, eFileName);
		m_lcList.DeleteItem(nItem);

		m_lcList.InsertItem(nSwapItem, _T(""));
		m_lcList.SetItemText(nSwapItem, eFileName, strItem);
		m_lcList.SetCheck(nSwapItem, bCheck);

		m_lcList.SetItemState(nSwapItem, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
		m_lcList.EnsureVisible(nSwapItem, FALSE);
	}
}

void CHallInfoDlg::OnBnClickedBnMoveDownImage()
{
	for(int nItem=m_lcList.GetItemCount()-2; nItem>=0; nItem--)
	{
		if(!m_lcList.GetItemState(nItem, LVIS_SELECTED)) continue;

		int nSwapItem = nItem+1;

		BOOL bCheck = m_lcList.GetCheck(nItem);
		CString strItem = m_lcList.GetItemText(nItem, eFileName);
		m_lcList.DeleteItem(nItem);

		m_lcList.InsertItem(nSwapItem, _T(""));
		m_lcList.SetItemText(nSwapItem, eFileName, strItem);
		m_lcList.SetCheck(nSwapItem, bCheck);

		m_lcList.SetItemState(nSwapItem, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
		m_lcList.EnsureVisible(nSwapItem, FALSE);
	}
}

void CHallInfoDlg::OnNMDblclkLcList(NMHDR *pNMHDR, LRESULT *pResult)
{
//	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	if(	pNMItemActivate->iItem >= 0                      &&
		pNMItemActivate->iItem < m_lcList.GetItemCount() )
	{
		CString strFileName = m_lcList.GetItemText(pNMItemActivate->iItem, eFileName);
		if(!strFileName.IsEmpty())
		{
			ShellExecute(NULL, "open", strFileName, NULL, WeddingDataMng()->GetImagePath(), SW_NORMAL);
		}
	}

	*pResult = 0;
}

void CHallInfoDlg::OnStnDblclickTxtGuestbookImage()
{
	UpdateData(TRUE);

	if(!m_strGuestBookImage.IsEmpty())
	{
		ShellExecute(NULL, "open", m_strGuestBookImage, NULL, WeddingDataMng()->GetImagePath(), SW_NORMAL);
	}

	UpdateData(FALSE);
}

void CHallInfoDlg::OnLvnKeydownLcList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	
	if(pLVKeyDow->wVKey == VK_DELETE)
	{
		OnBnClickedBnRemoveImage();
	}

	*pResult = 0;
}
