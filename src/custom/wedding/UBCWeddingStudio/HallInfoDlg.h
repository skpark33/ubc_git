#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "afxdtctl.h"
#include "common\UTBListCtrl.h"
#include "common\hoverbutton.h"
#include "atlcomtime.h"
#include "OleListDropTarget.h"

// CHallInfoDlg 대화 상자입니다.

class CHallInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CHallInfoDlg)

public:
	CHallInfoDlg(CWnd* pParent, int nIndex);   // 표준 생성자입니다.
	virtual ~CHallInfoDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HALL_INFO_DLG };
	enum { eCheck, eFileName };

	CString			m_strGroomName;
	CString			m_strGroomParentName;
	CString			m_strBrideName;
	CString			m_strBrideParentName;
	CString			m_strGuestBookImage;
	CHoverButton	m_bnClear;
	CHoverButton	m_bnAdd;
	CHoverButton	m_bnRemove;
	CHoverButton	m_bnMoveUp;
	CHoverButton	m_bnMoveDown;
	CHoverButton	m_bnGuestBookImage;
	CUTBListCtrl	m_lcList;
	COleDateTime	m_tmWeddingStartTime;
	COleDateTime	m_tmShowStartTime;
	COleDateTime	m_tmShowEndTime;
	BOOL			m_bGuestBookImage;
	CString			m_strWeddingBanquet;

	bool			InitDataAndCtrl();
	bool			IsInputData();
	bool			IsModified();
	bool			InputErrorCheck();
	bool			SaveData();

	void			AutoSetShowTime(CString strWeddingTime);

	// Drag and drop
	COleListDropTarget	m_objOleDrop;
	bool			RegisterDragDrop();
	bool			UnregisterDragDrop();

protected:
	int				m_nIndex;
	virtual void	DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnClear();
	afx_msg void OnDtnDatetimechangeDtWeddingStartTime(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBnGuestbookImage();
	afx_msg void OnBnClickedBnAddImage();
	afx_msg void OnBnClickedBnRemoveImage();
	afx_msg void OnLvnColumnclickLcList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDestroy();
	afx_msg LRESULT	OnDropFileList(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedBnMoveUpImage();
	afx_msg void OnBnClickedBnMoveDownImage();
	afx_msg void OnNMDblclkLcList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnStnDblclickTxtGuestbookImage();
	afx_msg void OnLvnKeydownLcList(NMHDR *pNMHDR, LRESULT *pResult);
};
