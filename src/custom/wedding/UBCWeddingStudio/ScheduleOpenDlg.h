#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "common\UTBListCtrl.h"
#include "common\hoverbutton.h"

// CScheduleOpenDlg 대화 상자입니다.

class CScheduleOpenDlg : public CDialog
{
	DECLARE_DYNAMIC(CScheduleOpenDlg)

	enum { eSelect, eDrive, eScheduleName };

public:
	CScheduleOpenDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CScheduleOpenDlg();

	// output
	CString m_strSchedulePath;

	CUTBListCtrl m_lcList;
	CImageList	 m_imgForList;
	CHoverButton m_bnOK;
	CHoverButton m_bnCancel;

	void FindSchedule(CStringArray& astrScheduleList);
	void InitList();

	static int CALLBACK CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPEN_SCHEDULE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnColumnclickLcList(NMHDR *pNMHDR, LRESULT *pResult);
};
