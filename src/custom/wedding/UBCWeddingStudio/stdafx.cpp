// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// UBCWeddingStudio.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

CString LoadStringFromID(UINT nID)
{
	CString strResult;
	strResult.LoadString(nID);
	return strResult;
}

BOOL GetDriveLabelName(CString strDrive, CString& strLabelName)
{
	DWORD dwSysFlags;
	char szFileSysNameBuf[MAX_PATH];
	char szLabelName[MAX_PATH];

	BOOL bResult = GetVolumeInformation( strDrive, szLabelName, MAX_PATH, NULL, NULL, &dwSysFlags, szFileSysNameBuf, MAX_PATH);
	if(bResult) strLabelName = szLabelName;

	return bResult;
}

CString ToString(int nValue)
{
	CString str;
	str.Format("%d", nValue);

	return str;
}

CString ToString(double fValue)
{
	CString str;
	str.Format("%.3f", fValue);

	return str;
}

CString ToString(ULONG ulValue)
{
	CString str;
	str.Format("%ld", ulValue);

	return str;
}

CString ToString(ULONGLONG ulValue)
{
	CString str;
	str.Format("%I64d", ulValue);

	return str;
}

CString ToMoneyTypeString(LPCTSTR lpszStr)
{
	CString str = lpszStr;

	int nLength = str.Find(_T("."));
	if(nLength<0)
        nLength = str.GetLength();

	while( nLength > 3 )
	{
		nLength = nLength - 3;
		str.Insert( nLength, ',' );
	}

	return str;
}

CString ToMoneyTypeString(int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%d"), value);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(ULONG ulvalue)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%ld"), ulvalue);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(ULONGLONG ulvalue)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%I64d"), ulvalue);
	return ToMoneyTypeString(buf);
}

CString	GetFileSize(CString strFilePath)
{
	CString strSize;
	CFileFind ff;
	if(!ff.FindFile(strFilePath)) return _T("");
	ff.FindNextFile();
	
	if(ff.GetLength() > 1024)
	{
		strSize = ::ToMoneyTypeString(ff.GetLength()/1024) + _T(" KByte");
	}
	else
	{
		strSize = ::ToMoneyTypeString(ff.GetLength()) + _T(" Byte");
	}

	ff.Close();

	return strSize;
}

CString	GetFileDate(CString strFilePath)
{
	CFileStatus fs;
	if(!CFile::GetStatus(strFilePath, fs)) return _T("");
	return fs.m_mtime.Format("%Y-%m-%d %H:%m:%S");
}

bool IsExistFile(CString strFilePath)
{
	CFileStatus fs;
	return (CFile::GetStatus(strFilePath, fs));
}
