// NewScheduleDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "WeddingDataManager.h"
#include "UBCWeddingStudio.h"
#include "NewScheduleDlg.h"
#include "common/PreventChar.h"

// CNewScheduleDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CNewScheduleDlg, CDialog)

CNewScheduleDlg::CNewScheduleDlg(CWnd* pParent /*=NULL*/, CNewScheduleDlg::MODE nMode/*=0*/)
	: CDialog(CNewScheduleDlg::IDD, pParent)
	, m_nMode(nMode)
	, m_strNewScheduleName(_T(""))
{
}

CNewScheduleDlg::~CNewScheduleDlg()
{
}

void CNewScheduleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LC_LIST, m_lcList);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
	DDX_Text(pDX, IDC_EB_SCHEDULE_NAME, m_strNewScheduleName);
	DDV_MaxChars(pDX, m_strNewScheduleName, 32);
}


BEGIN_MESSAGE_MAP(CNewScheduleDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CNewScheduleDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CNewScheduleDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CNewScheduleDlg 메시지 처리기입니다.

void CNewScheduleDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	POSITION pos = m_lcList.GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		AfxMessageBox(IDS_MSG_018);
		return;
	}

	m_strNewScheduleName.Trim();

	if(m_strNewScheduleName.IsEmpty())
	{
		AfxMessageBox(IDS_MSG_009);
		return;
	}

	if(CPreventChar::GetInstance()->HavePreventChar(m_strNewScheduleName))
	{
		AfxMessageBox(IDS_MSG_010);
		return;
	}

	int nItem = m_lcList.GetNextSelectedItem(pos);

	m_strSchedulePath.Format("%s%sdata_%s"
							, m_lcList.GetItemText(nItem, 1)
							, WeddingDataMng()->GetDefaultPath()
							, m_strNewScheduleName
							);

	OnOK();
}

void CNewScheduleDlg::OnBnClickedCancel()
{	
	OnCancel();
}

BOOL CNewScheduleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if     (m_nMode == eNEW    ) SetWindowText(LoadStringFromID(IDS_NEW_SCHEDULE_DLG1));
	else if(m_nMode == eSAVE_AS) SetWindowText(LoadStringFromID(IDS_NEW_SCHEDULE_DLG2));
	else                         SetWindowText(LoadStringFromID(IDS_NEW_SCHEDULE_DLG3));

	CBitmap bmp;
	bmp.LoadBitmap(IDB_DRIVE_LIST);

	if(m_imgForList.Create(48, 48, ILC_COLORDDB | ILC_MASK, 1, 1))
	{
		m_imgForList.Add(&bmp, RGB(255,255,255));
	}
	m_lcList.SetImageList(&m_imgForList, LVSIL_NORMAL);
	m_lcList.SetImageList(&m_imgForList, LVSIL_SMALL);

	m_lcList.SetExtendedStyle(m_lcList.GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	m_lcList.InsertColumn(0, LoadStringFromID(IDS_OPEN_LIST1), LVCFMT_LEFT, 200);
	m_lcList.InsertColumn(1, "", LVCFMT_LEFT, 0);
	m_lcList.InitHeader(IDB_LIST_HEADER);

	//버튼위 위치 조정
	if(m_nMode == eNEW)
	{
		m_bnOK.LoadBitmap(IDB_BTN_OK, RGB(255, 255, 255));
		m_bnOK.SetToolTipText(IDS_NEW_SCHEDULE);
	}
	else
	{
		m_bnOK.LoadBitmap(IDB_BTN_SAVEAS, RGB(255, 255, 255));
		m_bnOK.SetToolTipText(IDS_SAVEAS_SCHEDULE);
	}

	m_bnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_bnCancel.SetToolTipText(IDS_CANCEL_SCHEDULE);

	InitList();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CNewScheduleDlg::FindDrive(CStringArray& astrDriveList)
{
	int nPos =0;
	char szDrive[] = "?:\\";
	DWORD dwDriveList=::GetLogicalDrives();
	while(dwDriveList)
	{
		if(dwDriveList & 1)
		{
			szDrive[0] = 'A' + nPos;
			CString strDrive(szDrive);
			strDrive.MakeUpper();

			UINT nType = GetDriveType(strDrive);
			if(m_nMode == eEXPORT)
			{
				if( nType == DRIVE_REMOVABLE )
				{
					astrDriveList.Add(strDrive);
				}
			}
			else
			{
				if( nType == DRIVE_REMOVABLE ||
					nType == DRIVE_FIXED     )
				{
					astrDriveList.Add(strDrive);
				}
			}
		}
		dwDriveList >>=1;
		nPos++;
	}
}

void CNewScheduleDlg::InitList()
{
	CStringArray astrDriveList;
	FindDrive(astrDriveList);

	m_lcList.DeleteAllItems();
	for(int i=0; i<astrDriveList.GetCount() ;i++)
	{
		CString strDrive = astrDriveList[i];
		CString strLabelName;

		if(!GetDriveLabelName(strDrive, strLabelName)) continue;

		CString strDriveText;
		strDriveText.Format("%s(%s)", strLabelName, strDrive);
		int nIdx = m_lcList.InsertItem(i, strDriveText, GetDriveType(strDrive));
		m_lcList.SetItemText(nIdx, 1, strDrive);

		if(strDrive == _T("C:\\"))
		{
			m_lcList.SetSelectionMark(nIdx);
			m_lcList.SetItemState(nIdx, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
			m_lcList.SetFocus();
		}
	}
}

BOOL CNewScheduleDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->hwnd == GetDlgItem(IDC_EB_SCHEDULE_NAME)->GetSafeHwnd())
	{
		if(pMsg->message == WM_CHAR)
		{
			if(CPreventChar::GetInstance()->IsPreventChar((TCHAR)pMsg->wParam))
			{
				return TRUE;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
