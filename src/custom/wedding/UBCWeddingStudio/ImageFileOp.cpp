#include "StdAfx.h"
#include "UBCWeddingStudio.h"
#include "WeddingDataManager.h"
#include "ImageFileOp.h"
#include "FileCopyDlg.h"
#include "FileOverwriteDlg.h"
#include "ImgConvert\ImageConvert.h"

CImageFileOp::CImageFileOp(void)
{
}

CImageFileOp::~CImageFileOp(void)
{
}

bool CImageFileOp::ImageFileOpenAndCopy(CString& strResultFileName)
{
	int nMaxCnt = WeddingDataMng()->GetMaxImageData();

	CString szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
	CString filter;
	filter.Format("All Image Files|%s|"
				  "Bitmap Files (*.bmp)|*.bmp|"
				  "JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg;|"
				  "GIF Files (*.gif)|*.gif|"
				  "PCX Files (*.pcx)|*.pcx|"
				  "PNG Files (*.png)|*.png|"
				  "TIFF Files (*.tif)|*.tif;*.tiff||"
				 , szFileExts
				 );

	CFileDialog dlgOpen(TRUE, NULL, NULL, OFN_HIDEREADONLY, filter, AfxGetMainWnd());	CString strFileName;
	int nBuffSize = (nMaxCnt * (MAX_PATH+1)) + 1;
	dlgOpen.GetOFN().lpstrFile = strFileName.GetBuffer(nBuffSize);
	dlgOpen.GetOFN().nMaxFile = nBuffSize;
	int nResult = dlgOpen.DoModal();
	strFileName.ReleaseBuffer();
	if(dlgOpen.DoModal() != IDOK)
	{
		DWORD dwError = CommDlgExtendedError();
		if( dwError == 0x3003 )
		{
			AfxMessageBox(LoadStringFromID(IDS_MSG_021) + _T("\n") + LoadStringFromID(IDS_MSG_007));
		}
		return false;
	}

	TCHAR szDrv[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szName[MAX_PATH]={0}, szExt[MAX_PATH]={0};
	_tsplitpath(dlgOpen.GetPathName(), szDrv, szPath, szName, szExt);

	CString strExt(szExt);
	strExt.MakeLower();
	if(szFileExts.Find(strExt) < 0)
	{
		AfxMessageBox(IDS_MSG_013);
		return false;
	}

	CString strSrcFilePath = dlgOpen.GetPathName();
	CString strTargetFilePath = WeddingDataMng()->GetImagePath() + szName + szExt;
	bool	bAutoResolution = false;

	if(IsExistFile(strTargetFilePath))
	{
		CFileOverwriteDlg dlg;
		dlg.m_strOverWriteFilePath = strSrcFilePath;
		dlg.m_strSkipFilePath = strTargetFilePath;
		dlg.m_bIsSingleFile = true;
		if(dlg.DoModal() != IDOK) return false;
		if(dlg.m_nCopyMethod == CFileOverwriteDlg::COPY_METHOD::SKIP)
		{
			strResultFileName = strTargetFilePath;
			return true;
		}
		if(dlg.m_nCopyMethod == CFileOverwriteDlg::COPY_METHOD::RENAME_COPY) strTargetFilePath = dlg.m_strNewFilePath;
		bAutoResolution = dlg.m_bAutoResolution;
	}
	else
	{
		CFileCopyDlg dlg;
		dlg.m_strFilePath = strSrcFilePath;
		dlg.m_bIsSingleFile = true;
		if(dlg.DoModal() != IDOK) return false;
		bAutoResolution = dlg.m_bAutoResolution;
	}

	if(bAutoResolution)
	{
		CImageConvert imgCnv;

		CSize sScreen(910, 1200);
		CSize sImage(0, 0);
		ULONG ulSize=0;

		imgCnv.GetFileInfo(strSrcFilePath, (int&)sImage.cx, (int&)sImage.cy, ulSize);
		if(sScreen.cx < sImage.cx || sScreen.cy < sImage.cy)
		{
			TCHAR szTmpPath[MAX_PATH]={0};
			::GetTempPath(MAX_PATH, szTmpPath);
			CString strTmpPath;
			strTmpPath.Format("%s%s%s", szTmpPath, szName, szExt);
			::DeleteFile(strTmpPath);

			int cx = sScreen.cx;
			int cy = sScreen.cy;

			if(sScreen.cx < sScreen.cy) cy = (cx*sImage.cy)/sImage.cx;
			else						cx = (cy*sImage.cx)/sImage.cy;

			imgCnv.ImgConvert(strSrcFilePath, strTmpPath, cx, cy, ulSize, false);
			MoveFileEx(strTmpPath, strTargetFilePath, MOVEFILE_REPLACE_EXISTING);
		}
		else
		{
			::CopyFile(strSrcFilePath, strTargetFilePath, FALSE);
		}
	}
	else
	{
		::CopyFile(strSrcFilePath, strTargetFilePath, FALSE);
	}

	strResultFileName = strTargetFilePath;

	return IsExistFile(strResultFileName);
}

void CImageFileOp::ImageFilesCopy(CStringArray& astrSrcImageFiles, CStringArray& astrTarImageFiles)
{
	CString szFileExts = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";

	astrTarImageFiles.RemoveAll();
	astrTarImageFiles.SetSize(astrSrcImageFiles.GetCount());

	if(astrSrcImageFiles.GetCount() <= 0) return;

	bool bAutoResolution = false;
	bool bCopyApplyAll = false;
	bool bOverwriteApplyAll = false;
	int  nCopyMethod = CFileOverwriteDlg::COPY_METHOD::OVER_WRITE;

	for(int i=0; i<astrSrcImageFiles.GetCount() ;i++)
	{
		CString strSrcFilePath = astrSrcImageFiles[i];

		TCHAR szDrv[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szName[MAX_PATH]={0}, szExt[MAX_PATH]={0};
		_tsplitpath(strSrcFilePath, szDrv, szPath, szName, szExt);

		CString strExt(szExt);
		strExt.MakeLower();
		if(szFileExts.Find(strExt) < 0)
		{
			AfxMessageBox(LoadStringFromID(IDS_MSG_013) + "\n" + CString(szName) + szExt);
			continue;
		}

		CString strTargetFilePath = WeddingDataMng()->GetImagePath() + szName + szExt;
		
		if(IsExistFile(strTargetFilePath))
		{
			if(!bOverwriteApplyAll)
			{
				CFileOverwriteDlg dlg;
				dlg.m_strOverWriteFilePath = strSrcFilePath;
				dlg.m_strSkipFilePath = strTargetFilePath;
				dlg.m_bIsSingleFile = (astrSrcImageFiles.GetCount() <= 1);
				if(dlg.DoModal() != IDOK) continue;
				nCopyMethod = dlg.m_nCopyMethod;
				if(!bAutoResolution) bAutoResolution = dlg.m_bAutoResolution;
				bOverwriteApplyAll = dlg.m_bApplyAll;
			}

			if(nCopyMethod == CFileOverwriteDlg::COPY_METHOD::SKIP)
			{
				astrTarImageFiles[i] = strTargetFilePath;
				continue;
			}
			if(nCopyMethod == CFileOverwriteDlg::COPY_METHOD::RENAME_COPY)
			{
				CString strNewFilePath;
		
				CString strFilePath = strTargetFilePath.Left(strTargetFilePath.ReverseFind('.'));
				CString strFileExt = strTargetFilePath.Mid(strTargetFilePath.ReverseFind('.'));
				int nCnt = 1;
				do
				{
					strNewFilePath.Format("%s(%d)%s", strFilePath, nCnt++, strFileExt);
				}
				while(IsExistFile(strNewFilePath));
				strTargetFilePath = strNewFilePath;
			}
		}
		else
		{
			if(!bCopyApplyAll)
			{
				CFileCopyDlg dlg;
				dlg.m_strFilePath = strSrcFilePath;
				dlg.m_bIsSingleFile = (astrSrcImageFiles.GetCount() <= 1);
				if(dlg.DoModal() != IDOK) continue;
				if(!bAutoResolution) bAutoResolution = dlg.m_bAutoResolution;
				bCopyApplyAll = dlg.m_bApplyAll;
			}
		}

		if(bAutoResolution)
		{
			CImageConvert imgCnv;

			CSize sScreen(910, 1200);
			CSize sImage(0, 0);
			ULONG ulSize=0;

			imgCnv.GetFileInfo(strSrcFilePath, (int&)sImage.cx, (int&)sImage.cy, ulSize);
			if(sScreen.cx < sImage.cx || sScreen.cy < sImage.cy)
			{
				TCHAR szTmpPath[MAX_PATH]={0};
				::GetTempPath(MAX_PATH, szTmpPath);
				CString strTmpPath;
				strTmpPath.Format("%s%s%s", szTmpPath, szName, szExt);
				::DeleteFile(strTmpPath);

				int cx = sScreen.cx;
				int cy = sScreen.cy;

				if(sScreen.cx < sScreen.cy) cy = (cx*sImage.cy)/sImage.cx;
				else						cx = (cy*sImage.cx)/sImage.cy;

				imgCnv.ImgConvert(strSrcFilePath, strTmpPath, cx, cy, ulSize, false);
				MoveFileEx(strTmpPath, strTargetFilePath, MOVEFILE_REPLACE_EXISTING);
			}
			else
			{
				::CopyFile(strSrcFilePath, strTargetFilePath, FALSE);
			}
		}
		else
		{
			::CopyFile(strSrcFilePath, strTargetFilePath, FALSE);
		}

		astrTarImageFiles[i] = strTargetFilePath;
	}
}