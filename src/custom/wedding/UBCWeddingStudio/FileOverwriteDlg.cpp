// ImageFileCopy.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCWeddingStudio.h"
#include "FileOverwriteDlg.h"

// CFileOverwriteDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFileOverwriteDlg, CDialog)

CFileOverwriteDlg::CFileOverwriteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFileOverwriteDlg::IDD, pParent)
	, m_bAutoResolution(TRUE)
	, m_bApplyAll(FALSE)
	, m_strOverWriteFileName(_T(""))
	, m_strOverWriteFilePath(_T(""))
	, m_strOverWriteFileSize(_T(""))
	, m_strOverWriteFileDate(_T(""))
	, m_strSkipFilePath(_T(""))
	, m_strSkipFileName(_T(""))
	, m_strSkipFileSize(_T(""))
	, m_strSkipFileDate(_T(""))
	, m_strRenameCopyDesc(_T(""))
	, m_bIsSingleFile(true)
{
}

CFileOverwriteDlg::~CFileOverwriteDlg()
{
}

void CFileOverwriteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FRAME_OVERWRITE_IMAGE, m_picOverWriteImage);
	DDX_Control(pDX, IDC_FRAME_SKIP_IMAGE, m_picSkip);
	DDX_Check(pDX, IDC_KB_AUTO_RESOLUTION, m_bAutoResolution);
	DDX_Check(pDX, IDC_KB_APPLY_ALL, m_bApplyAll);
	DDX_Text(pDX, IDC_TXT_OVERWRITE_FILE_NAME, m_strOverWriteFileName);
	DDX_Text(pDX, IDC_TXT_OVERWRITE_PATH, m_strOverWriteFilePath);
	DDX_Text(pDX, IDC_TXT_OVERWRITE_SIZE, m_strOverWriteFileSize);
	DDX_Text(pDX, IDC_TXT_OVERWRITE_DATE, m_strOverWriteFileDate);
	DDX_Text(pDX, IDC_TXT_SKIP_FILE_NAME, m_strSkipFileName);
	DDX_Text(pDX, IDC_TXT_SKIP_PATH, m_strSkipFilePath);
	DDX_Text(pDX, IDC_TXT_SKIP_SIZE, m_strSkipFileSize);
	DDX_Text(pDX, IDC_TXT_SKIP_DATE, m_strSkipFileDate);
	DDX_Text(pDX, IDC_TXT_RENAME_COPY_DESC, m_strRenameCopyDesc);
}


BEGIN_MESSAGE_MAP(CFileOverwriteDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CFileOverwriteDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFileOverwriteDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BN_OVERWIRITE, &CFileOverwriteDlg::OnBnClickedBnOverwirite)
	ON_BN_CLICKED(IDC_BN_SKIP, &CFileOverwriteDlg::OnBnClickedBnSkip)
	ON_BN_CLICKED(IDC_BN_RENAME_COPY, &CFileOverwriteDlg::OnBnClickedBnRenameCopy)
END_MESSAGE_MAP()

// CFileOverwriteDlg 메시지 처리기입니다.

BOOL CFileOverwriteDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_picOverWriteImage.SetImageFile(m_strOverWriteFilePath);
	m_strOverWriteFileName = m_strOverWriteFilePath.Mid(m_strOverWriteFilePath.ReverseFind('\\')+1);
	m_strOverWriteFileSize = _T("크기: ") + ::GetFileSize(m_strOverWriteFilePath);
	m_strOverWriteFileDate = _T("수정한 날짜: ") + ::GetFileDate(m_strOverWriteFilePath);

	m_picSkip.SetImageFile(m_strSkipFilePath);
	m_strSkipFileName = m_strSkipFilePath.Mid(m_strSkipFilePath.ReverseFind('\\')+1);
	m_strSkipFileSize = _T("크기: ") + ::GetFileSize(m_strSkipFilePath);
	m_strSkipFileDate = _T("수정한 날짜: ") + ::GetFileDate(m_strSkipFilePath);

	CString strFilePath = m_strSkipFilePath.Left(m_strSkipFilePath.ReverseFind('.'));
	CString strFileExt = m_strSkipFilePath.Mid(m_strSkipFilePath.ReverseFind('.'));
	int nCnt = 1;
	do
	{
		m_strNewFilePath.Format("%s(%d)%s", strFilePath, nCnt++, strFileExt);
	}
	while(IsExistFile(m_strNewFilePath));

	TCHAR szDrv[MAX_PATH]={0}, szPath[MAX_PATH]={0}, szName[MAX_PATH]={0}, szExt[MAX_PATH]={0};
	_tsplitpath(m_strNewFilePath, szDrv, szPath, szName, szExt);

	m_strRenameCopyDesc.Format(_T("복사하는 파일 이름이 [%s%s]로 변경됩니다"), szName, szExt);

	m_bApplyAll = FALSE;
	GetDlgItem(IDC_KB_APPLY_ALL)->EnableWindow(!m_bIsSingleFile);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CFileOverwriteDlg::OnBnClickedOk()
{
//	OnOK();
}

void CFileOverwriteDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CFileOverwriteDlg::OnBnClickedBnOverwirite()
{
	UpdateData(TRUE);
	m_nCopyMethod = OVER_WRITE;
	OnOK();
}

void CFileOverwriteDlg::OnBnClickedBnSkip()
{
	UpdateData(TRUE);
	m_nCopyMethod = SKIP;
	OnOK();
}

void CFileOverwriteDlg::OnBnClickedBnRenameCopy()
{
	UpdateData(TRUE);
	m_nCopyMethod = RENAME_COPY;
	OnOK();
}

