// ExitDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "ExitDlg.h"

// CExitDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CExitDlg, CDialog)

CExitDlg::CExitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExitDlg::IDD, pParent)
{

}

CExitDlg::~CExitDlg()
{
}

void CExitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CExitDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CExitDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CExitDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CExitDlg 메시지 처리기입니다.

void CExitDlg::OnBnClickedOk()
{
	OnOK();
}

void CExitDlg::OnBnClickedCancel()
{
	OnCancel();
}

BOOL CExitDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetDlgItemText(IDC_TXT_MSG, LoadStringFromID(IDS_MSG_003));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
