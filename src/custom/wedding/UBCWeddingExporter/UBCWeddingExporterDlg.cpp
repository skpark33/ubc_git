// UBCWeddingExporterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCWeddingExporter.h"
#include "UBCWeddingExporterDlg.h"
#include "EjectRemovableDrive.h"
#include "ExitDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CUBCWeddingExporterDlg::CUBCWeddingExporterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCWeddingExporterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUBCWeddingExporterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LC_GUESTBOOK_LIST, m_lcGuestbookList);
	DDX_Control(pDX, IDC_LC_DRIVE_LIST, m_lcDriveList);
	DDX_Control(pDX, IDOK, m_bnOK);
	DDX_Control(pDX, IDCANCEL, m_bnCancel);
}

BEGIN_MESSAGE_MAP(CUBCWeddingExporterDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CUBCWeddingExporterDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CUBCWeddingExporterDlg::OnBnClickedCancel)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LC_GUESTBOOK_LIST, &CUBCWeddingExporterDlg::OnLvnColumnclickLcGuestbookList)
END_MESSAGE_MAP()


// CUBCWeddingExporterDlg 메시지 처리기

BOOL CUBCWeddingExporterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	m_lcGuestbookList.SetExtendedStyle(m_lcGuestbookList.GetExtendedStyle() | LVS_EX_SINGLEROW | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_lcGuestbookList.InsertColumn(eCheck, _T("")        , LVCFMT_LEFT, 22);
	m_lcGuestbookList.InsertColumn(eName , _T("예식이름"), LVCFMT_LEFT, 230);
	m_lcGuestbookList.InitHeader(IDB_LIST_HEADER);

	CBitmap bmp;
	bmp.LoadBitmap(IDB_DRIVE_LIST);

	if(m_imgForDriveList.Create(48, 48, ILC_COLORDDB | ILC_MASK, 1, 1))
	{
		m_imgForDriveList.Add(&bmp, RGB(255,255,255));
	}
	m_lcDriveList.SetImageList(&m_imgForDriveList, LVSIL_NORMAL);
	m_lcDriveList.SetImageList(&m_imgForDriveList, LVSIL_SMALL);

	m_lcDriveList.SetExtendedStyle(m_lcDriveList.GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	m_lcDriveList.InsertColumn(0, LoadStringFromID(IDS_OPEN_LIST1), LVCFMT_LEFT, 200);
	m_lcDriveList.InsertColumn(1, "", LVCFMT_LEFT, 0);
	m_lcDriveList.InitHeader(IDB_LIST_HEADER);

	//버튼위 위치 조정
	m_bnOK.LoadBitmap(IDB_BTN_APPLY, RGB(255, 255, 255));
	m_bnOK.SetToolTipText(IDS_SELECT_GUESTBOOK);

	m_bnCancel.LoadBitmap(IDB_BTN_CANCEL, RGB(255, 255, 255));
	m_bnCancel.SetToolTipText(IDS_CANCEL_EXPORT);

	InitGuestbookList();
	InitDriveList();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCWeddingExporterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCWeddingExporterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CUBCWeddingExporterDlg::OnBnClickedOk()
{
	CStringArray astrGuestbook;
	for(int i=0; i<m_lcGuestbookList.GetItemCount() ;i++)
	{
		if(!m_lcGuestbookList.GetCheck(i)) continue;

		CString strGuestbookName = m_lcGuestbookList.GetItemText(i, eName);
		if(!strGuestbookName.IsEmpty())
		{
			astrGuestbook.Add(strGuestbookName);
		}
	}

	if(astrGuestbook.GetCount() <= 0)
	{
		AfxMessageBox(IDS_MSG_001);
		return;
	}

	POSITION pos = m_lcDriveList.GetFirstSelectedItemPosition();
	if(pos == NULL)
	{
		AfxMessageBox(IDS_MSG_002);
		return;
	}

	int nItem = m_lcDriveList.GetNextSelectedItem(pos);
	CString strDrive = m_lcDriveList.GetItemText(nItem, 1);

	// 컨텐츠 COPY
	for(int i=0; i<astrGuestbook.GetCount() ;i++)
	{
		CString strSrcPath;
		strSrcPath.Format("c:\\%s\\guestBook\\%s", DATA_DEFAULT_PATH, astrGuestbook[i]);

		CString strTarPath;
		strTarPath.Format("%sguestBook\\%s", strDrive, astrGuestbook[i]);

		if(!GuestbookCopy(strSrcPath, strTarPath)) return;
	}

	CExitDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
		// usb 제거
		CEjectRemovableDrive::GetInstance()->EjectDrive(strDrive[0]);
	}

	OnOK();
}

bool CUBCWeddingExporterDlg::GuestbookCopy(CString strSrcPath, CString strTarPath)
{
	::SHCreateDirectoryEx(GetSafeHwnd(), strTarPath, NULL);

	TCHAR szFrom[MAX_PATH+1] = {0};
	strcpy(szFrom, strTarPath);

	SHFILEOPSTRUCT sh;
	memset(&sh, 0x00, sizeof(SHFILEOPSTRUCT));
	sh.hwnd = GetSafeHwnd();
	sh.wFunc = FO_DELETE;
	sh.pFrom = szFrom;
	sh.pTo = NULL;
	sh.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_SIMPLEPROGRESS | FOF_NOERRORUI;
	sh.fAnyOperationsAborted = FALSE;
	sh.lpszProgressTitle = "방명록 내보내기 준비중";
	::SHFileOperation(&sh);

	memset(&szFrom, 0x00, sizeof(szFrom));
	strcpy(szFrom, strSrcPath);

	TCHAR szTo[MAX_PATH+1] = {0};
	strcpy(szTo, strTarPath);

	memset(&sh, 0x00, sizeof(SHFILEOPSTRUCT));
	sh.hwnd = GetSafeHwnd();
	sh.wFunc = FO_COPY;
	sh.pFrom = szFrom;
	sh.pTo = szTo;
	sh.fFlags = FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_SIMPLEPROGRESS;// | FOF_NOERRORUI;
	sh.fAnyOperationsAborted = FALSE;
	sh.lpszProgressTitle = "방명록 내보내기중";

	int nResult = SHFileOperation(&sh);

	return (nResult == 0 && sh.fAnyOperationsAborted == false);
}

void CUBCWeddingExporterDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CUBCWeddingExporterDlg::FindGuestbook(CStringArray& astrGuestbookList)
{
	CString strFindPath;
	strFindPath.Format("c:\\%s\\guestBook\\*.*", DATA_DEFAULT_PATH);

	CFileFind ff;
	BOOL bWorking = ff.FindFile(strFindPath);
	while(bWorking)
	{
		bWorking = ff.FindNextFile();

		if(ff.IsDots()) continue;
		if(!ff.IsDirectory()) continue;

		astrGuestbookList.Add(ff.GetFilePath());
	}

	ff.Close();
}

void CUBCWeddingExporterDlg::InitGuestbookList()
{
	CStringArray astrGuestbookList;
	FindGuestbook(astrGuestbookList);

	m_lcGuestbookList.DeleteAllItems();
	for(int i=0; i<astrGuestbookList.GetCount() ;i++)
	{
		CString strGuestbookPath = astrGuestbookList[i];
		CString strGuestbookName = strGuestbookPath.Mid(strGuestbookPath.ReverseFind('\\')+1);

		int nIdx = m_lcGuestbookList.InsertItem(i, "", 0);
		m_lcGuestbookList.SetItemText(nIdx, eName, strGuestbookName);
	}

	m_lcGuestbookList.SetSortHeader(eName);
	m_lcGuestbookList.SortItems(CompareProc, (DWORD_PTR)this);
}

void CUBCWeddingExporterDlg::FindDrive(CStringArray& astrDriveList)
{
	int nPos =0;
	char szDrive[] = "?:\\";
	DWORD dwDriveList=::GetLogicalDrives();
	while(dwDriveList)
	{
		if(dwDriveList & 1)
		{
			szDrive[0] = 'A' + nPos;
			CString strDrive(szDrive);
			strDrive.MakeUpper();

			UINT nType = GetDriveType(strDrive);
			if( nType == DRIVE_REMOVABLE )
			{
				astrDriveList.Add(strDrive);
			}
		}
		dwDriveList >>=1;
		nPos++;
	}
}

BOOL CUBCWeddingExporterDlg::GetDriveLabelName(CString strDrive, CString& strLabelName)
{
	DWORD dwSysFlags;
	char szFileSysNameBuf[MAX_PATH];
	char szLabelName[MAX_PATH];

	BOOL bResult = GetVolumeInformation( strDrive, szLabelName, MAX_PATH, NULL, NULL, &dwSysFlags, szFileSysNameBuf, MAX_PATH);
	if(bResult) strLabelName = szLabelName;

	return bResult;
}

void CUBCWeddingExporterDlg::InitDriveList()
{
	CStringArray astrDriveList;
	FindDrive(astrDriveList);

	m_lcDriveList.DeleteAllItems();
	for(int i=0; i<astrDriveList.GetCount() ;i++)
	{
		CString strDrive = astrDriveList[i];
		CString strLabelName;

		if(!GetDriveLabelName(strDrive, strLabelName)) continue;

		CString strDriveText;
		strDriveText.Format("%s(%s)", strLabelName, strDrive);
		int nIdx = m_lcDriveList.InsertItem(i, strDriveText, GetDriveType(strDrive));
		m_lcDriveList.SetItemText(nIdx, 1, strDrive);

		if(strDrive == _T("C:\\"))
		{
			m_lcDriveList.SetSelectionMark(nIdx);
			m_lcDriveList.SetItemState(nIdx, LVIS_SELECTED|LVIS_FOCUSED, LVIS_SELECTED|LVIS_FOCUSED);
			m_lcDriveList.SetFocus();
		}
	}
}

void CUBCWeddingExporterDlg::OnLvnColumnclickLcGuestbookList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int	nColumn = pNMLV->iSubItem;

	// 2010.02.17 by gwangsoo
	if(nColumn == eCheck)
	{
		bool bCheck = !m_lcGuestbookList.GetCheckHdrState();
		for(int nRow=0; nRow<m_lcGuestbookList.GetItemCount() ;nRow++)
		{
			m_lcGuestbookList.SetCheck(nRow, bCheck);
		}
		m_lcGuestbookList.SetCheckHdrState(bCheck);
	}
	else
	{
		m_lcGuestbookList.SetSortHeader(nColumn);
		m_lcGuestbookList.SortItems(CompareProc, (DWORD_PTR)this);
	}
}

int CALLBACK CUBCWeddingExporterDlg::CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
	CUBCWeddingExporterDlg* pDlg = (CUBCWeddingExporterDlg*)lParam;
	CUTBListCtrl* pListCtrl = &pDlg->m_lcGuestbookList;

	LVFINDINFO    lvFind;
	lvFind.flags = LVFI_PARAM;

	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, pListCtrl->GetCurSortCol());
	CString strItem2 = pListCtrl->GetItemText(nIndex2, pListCtrl->GetCurSortCol());

	if(pListCtrl->IsAscend())
		return strItem1.Compare(strItem2);
	else
		return strItem2.Compare(strItem1);
}
