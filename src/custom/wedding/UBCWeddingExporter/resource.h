//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCWeddingExporter.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDS_ABOUTBOX                    101
#define IDD_UBCWEDDINGEXPORTER_DIALOG   102
#define IDS_MSG_001                     102
#define IDS_MSG_002                     103
#define IDS_MSG_003                     104
#define IDR_MAINFRAME                   129
#define IDB_BTN_APPLY                   130
#define IDB_LIST_HEADER                 131
#define IDB_BTN_CANCEL                  132
#define IDB_DRIVE_LIST                  133
#define IDD_EXIT_DIALOG                 134
#define IDC_LC_GUESTBOOK_LIST           1000
#define IDC_TXT_MSG                     1001
#define IDC_LC_DRIVE_LIST               1008
#define IDS_SELECT_GUESTBOOK            2119
#define IDS_CANCEL_EXPORT               2120
#define IDS_OPEN_LIST1                  2121

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
