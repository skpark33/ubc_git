// UBCWeddingExporterDlg.h : 헤더 파일
//

#pragma once

#include "common\UTBListCtrl.h"
#include "common\hoverbutton.h"

// CUBCWeddingExporterDlg 대화 상자
class CUBCWeddingExporterDlg : public CDialog
{
// 생성입니다.
public:
	CUBCWeddingExporterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCWEDDINGEXPORTER_DIALOG };
	enum {eCheck, eName };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	CUTBListCtrl m_lcGuestbookList;
	CImageList	 m_imgForGuestbookList;
	CUTBListCtrl m_lcDriveList;
	CImageList	 m_imgForDriveList;
	CHoverButton m_bnOK;
	CHoverButton m_bnCancel;

	void FindGuestbook(CStringArray& astrGuestbookList);
	void InitGuestbookList();

	void FindDrive(CStringArray& astrDriveList);
	BOOL GetDriveLabelName(CString strDrive, CString& strLabelName);
	void InitDriveList();

	bool GuestbookCopy(CString strSrcPath, CString strTarPath);

	static int CALLBACK CompareProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnLvnColumnclickLcGuestbookList(NMHDR *pNMHDR, LRESULT *pResult);
};
