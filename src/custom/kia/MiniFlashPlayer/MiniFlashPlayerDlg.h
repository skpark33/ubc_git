// MiniFlashPlayerDlg.h : header file
//

#pragma once
#include "shockwaveflash.h"


// CMiniFlashPlayerDlg dialog
class CMiniFlashPlayerDlg : public CDialog
{
// Construction
public:
	CMiniFlashPlayerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MINIFLASHPLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	LPCTSTR		GetAppPath();

	DECLARE_MESSAGE_MAP()
public:
	CShockwaveflash m_flash;
	afx_msg void OnSize(UINT nType, int cx, int cy);

	void	SetTopMost(bool bTopMost);

};
