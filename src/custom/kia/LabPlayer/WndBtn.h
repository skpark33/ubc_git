#pragma once


// CWndBtn

class CWndBtn : public CWnd
{
	DECLARE_DYNAMIC(CWndBtn)

public:
	CWndBtn();
	virtual ~CWndBtn();

	int			m_nId;		///<창의 Id
	CString		m_strName;	///<창의 이름
	CWnd*		m_pParent;	///<부모창

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};


