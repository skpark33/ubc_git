// LabPlayerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "LabPlayer.h"
#include "LabPlayerDlg.h"
#include "MemDC.h"
#include "WndBtn.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//이미지 정의
#define		IMG_BG			"res\\bg_b.jpg"		//메인배경 이미지
#define		IMG_BG_PLAY		"res\\play2.jpg"	//동영상이 재생될때의 배경 이미지
//#define		IMG_MENU		"res\\mn1_b.jpg"	//버튼메뉴 이미지
//#define		IMG_MENU_SPAS	"res\\mn2_b.jpg"	//SPAS 클릭시에 이미지
//#define		IMG_MENU_AVM	"res\\mn3_b.jpg"	//AVM 클릭시의 이미지
//#define		IMG_MENU_SCC	"res\\mn4_b.jpg"	//SCC 클릭시의 이미지
//#define		IMG_MENU_LDWS	"res\\mn5_b.jpg"	//LDWS 클릭시의 이미지
//#define		IMG_MENU_BSD	"res\\mn6_b.jpg"	//BSD 클릭시의 이미지
//#define		IMG_BTN_HOME	"res\\bt_home.jpg"	//홈버튼의 이미지

//메뉴판의 위치, 크기
#define		MENU_X		91						//X축 좌표
#define		MENU_Y		260						//Y축 좌표
#define		MENU_W		971						//넓이
#define		MENU_H		551						//높이

//SPAS 버튼영역의 위치, 크기
#define		SPAS_X		185						//X축 좌표
#define		SPAS_Y		330						//Y축 좌표
#define		SPAS_W		190						//넓이
#define		SPAS_H		132						//높이

//AVM 버튼영역의 위치, 크기
#define		AVM_X		185						//X축 좌표
#define		AVM_Y		611						//Y축 좌표
#define		AVM_W		190						//넓이
#define		AVM_H		132						//높이

//SCC 버튼영역의 위치, 크기
#define		SCC_X		382						//X축 좌표
#define		SCC_Y		468						//Y축 좌표
#define		SCC_W		190						//넓이
#define		SCC_H		132						//높이

//LDWS 버튼영역의 위치, 크기
#define		LDWS_X		583						//X축 좌표
#define		LDWS_Y		327						//Y축 좌표
#define		LDWS_W		190						//넓이
#define		LDWS_H		132						//높이

//BSD 버튼영역의 위치, 크기
#define		BSD_X		761						//X축 좌표
#define		BSD_Y		479						//Y축 좌표
#define		BSD_W		239						//넓이
#define		BSD_H		95						//높이

//Home 버튼영역의 위치, 크기
#define		HOME_X		579						//X축 좌표
#define		HOME_Y		969						//Y축 좌표
#define		HOME_W		126						//넓이
#define		HOME_H		40						//높이

//실행파일 정의, 위치, 크기
//#define		EXE_SCC			"res\\SCC\\SCC.exe"		//스마트크루즈컨트롤 시스템
//#define		EXE_AVM			"res\\AVM\\AVM.exe"		//어라운드뷰모니터링 시스템
//#define		EXE_SPAS		"res\\SPAS\\SPAS.exe"	//주차조향보조 시스템
//#define		EXE_BSD			"res\\BSD\\BSD.exe"		//후측방경보 시스템
//#define		EXE_LDWS		"res\\LDWS\\LDWS.exe"	//주행조향 보조시스템
#define		EXE_X		160						//X축 좌표
#define		EXE_Y		186						//Y축 좌표
#define		EXE_W		1024					//넓이
#define		EXE_H		768						//높이


//메뉴실행 상태
enum EN_MENU_STATE
{
	EN_STATE_NONE = 0,
	EN_STATE_SCC,
	EN_STATE_AVM,
	EN_STATE_SPAS,
	EN_STATE_BSD,
	EN_STATE_LDWS,
};

//메뉴버튼의 이미지들
static char* pszBtnImg[] = { "res\\mn1_b.jpg",
								"res\\mn2_b.jpg",
								"res\\mn3_b.jpg",
								"res\\mn4_b.jpg",
								"res\\mn5_b.jpg",
								"res\\mn6_b.jpg"
							};


//실행파일의 이름
static char* pszExeName[] = { "NONE",
								"res\\SCC\\SCC.exe",
								"res\\AVM\\AVM.exe",
								"res\\SPAS\\SPAS.exe",
								"res\\BSD\\MiniFlashPlayer.exe BSD.swf",
								"res\\LDWS\\LDWS.exe",
							};

//부모창의 타이틀
static char* pszTitle[] = { "NONE",
								"S",
								"A",
								"S",
								"MiniFlashPlayer",
								"L",
							  };

//자식창의 타이틀
static char* pszSubTitle[] = { "NONE",
								"SCC",
								"AVM",
								"SPAS",
								"MiniFlashPlayer",
								"LDWS",
							  };

//마름모 영역들의 좌표배열(좌측부터 시계방향순)
static int pntBtn[5][4][2] = {
/*	{ {91, 394}, {279, 260}, {467, 395}, {278, 530} },		//SPAS
	{ {93, 674}, {281, 537}, {470, 674}, {281, 810} },		//AVM
	{ {287, 533}, {476, 398}, {665, 534}, {476, 670} },		//SCC
	{ {484, 395}, {674, 260}, {863, 394}, {674, 531} },		//LDWS
	{ {683, 534}, {872, 398}, {1061, 534}, {872, 669} }		//BSD
*/
	{ {91, 394}, {279, 260}, {467, 395}, {278, 530} },		//SCC
	{ {93, 674}, {279, 540}, {467, 674}, {279, 810} },		//AVM
	{ {287, 533}, {475, 399}, {660, 533}, {475, 668} },		//SPAS
	{ {484, 395}, {672, 261}, {859, 394}, {672, 529} },		//BSD
	{ {681, 534}, {868, 399}, {1053, 533}, {868, 668} }		//LDWS
};

#define TM_EXIT			100						//종료 타이머
#define TM_PLAY			200						//동영상 재생 타이머
#define	TM_SPAS			300						//SPAS 재생중에 창의 위치를 변경


#ifndef LWA_ALPHA
#define LWA_ALPHA	0x00000002
#endif

#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED	0x00080000
#endif


typedef BOOL (_stdcall *TRANCPARENCY_TYPE)(HWND,COLORREF,BYTE,DWORD); 
TRANCPARENCY_TYPE btn_SetLayeredWindowAttributes; 


// CLabPlayerDlg 대화 상자




CLabPlayerDlg::CLabPlayerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLabPlayerDlg::IDD, pParent)
	, m_nMenuState(EN_STATE_NONE)
	, m_bPlaying(false)
	, m_hSubWnd(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	//이미지들의 경로 설정
	InitPath();
}

void CLabPlayerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	/*
	DDX_Control(pDX, IDC_SCC_BTN, m_btnSCC);
	DDX_Control(pDX, IDC_AVM_BTN, m_btnAVM);
	DDX_Control(pDX, IDC_SPAS_BTN, m_btnSPAS);
	DDX_Control(pDX, IDC_BSD_BTN, m_btnBSD);
	DDX_Control(pDX, IDC_LDWS_BTN, m_btnLDWS);
	*/
	DDX_Control(pDX, IDC_HOME_BTN, m_btnHome);
}

BEGIN_MESSAGE_MAP(CLabPlayerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	/*
	ON_BN_CLICKED(IDC_SCC_BTN, &CLabPlayerDlg::OnBnClickedSccBtn)
	ON_BN_CLICKED(IDC_AVM_BTN, &CLabPlayerDlg::OnBnClickedAvmBtn)
	ON_BN_CLICKED(IDC_SPAS_BTN, &CLabPlayerDlg::OnBnClickedSpasBtn)
	ON_BN_CLICKED(IDC_BSD_BTN, &CLabPlayerDlg::OnBnClickedBsdBtn)
	ON_BN_CLICKED(IDC_LDWS_BTN, &CLabPlayerDlg::OnBnClickedLdwsBtn)
	*/
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_HOME_BTN, &CLabPlayerDlg::OnBnClickedHomeBtn)
	ON_WM_LBUTTONDOWN()
	ON_WM_SETFOCUS()
	ON_WM_RBUTTONDOWN()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CLabPlayerDlg 메시지 처리기

BOOL CLabPlayerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	//ModifyStyle(WS_CAPTION | WS_SYSMENU | WS_SIZEBOX, NULL);
	ShowWindow(SW_MAXIMIZE);
	SetWindowText("LabPlayer");
/*
	m_btnSPAS.MoveWindow(SPAS_X, SPAS_Y, SPAS_W, SPAS_H);
	m_btnAVM.MoveWindow(AVM_X, AVM_Y, AVM_W, AVM_H);
	m_btnSCC.MoveWindow(SCC_X, SCC_Y, SCC_W, SCC_H);
	m_btnLDWS.MoveWindow(LDWS_X, LDWS_Y, LDWS_W, LDWS_H);
	m_btnBSD.MoveWindow(BSD_X, BSD_Y, BSD_W, BSD_H);
*/
	m_btnHome.MoveWindow(HOME_X, HOME_Y, HOME_W, HOME_H);
	m_btnHome.LoadBitmap(IDB_HOME_BMP, RGB(255, 255, 255));
	m_btnHome.ShowWindow(SW_HIDE);	//동영상이 재생되기 전까지는 숨긴다.

	InitPath();
	if(!LoadImage())
	{
		SetTimer(TM_EXIT, 10, NULL); 
	}//if	

	//버튼영역들 생성
	CreateBtnRgn();
	
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CLabPlayerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CRect rect;
		GetClientRect(rect);
		CPaintDC dc(this);
		CMemDC memDC(&dc);
		memDC.FillSolidRect(rect, RGB(0, 0, 0));	//바탕은 검정으로
		
		if(m_imgBG.IsValid())
		{
			m_imgBG.Draw2(memDC.GetSafeHdc(), rect);
		}//if

		if(m_imgMenu.IsValid())
		{
			m_imgMenu.Draw2(memDC.GetSafeHdc(), MENU_X, MENU_Y, MENU_W, MENU_H);
		}//if

		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CLabPlayerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일명에서 확장자를 분리하여 반환 \n
/// @param (const CString&) strFileName : (in) 파일명
/// @return <형: CString> \n
///			<파일의 확장자> \n
/////////////////////////////////////////////////////////////////////////////////
CString	CLabPlayerDlg::FindExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--)
	{
		if (strFileName[i] == '.')
		{
			return strFileName.Mid(i+1);
		}//if
	}//if

	return CString(_T(""));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 실행 프로그램의 경로를 반환 \n
/// @return <형: LPCTSTR> \n
///			<실행프로그램의 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR CLabPlayerDlg::GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory.Format("%s\\", str);
	}

	return _mainDirectory;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지들의 경로를 설정한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CLabPlayerDlg::InitPath()
{
	m_strBgImgPath = GetAppPath();
	m_strBgImgPath += IMG_BG;

	m_strMenuImgPath = GetAppPath();
	m_strMenuImgPath += pszBtnImg[m_nMenuState];
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지들을 읽는다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CLabPlayerDlg::LoadImage()
{
	CString strMsg;

	//배경 이미지 로드
	CString strExt = FindExtension(m_strBgImgPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	if(m_imgBG.Load(m_strBgImgPath, nImgType))
	{
/*
		int nX, nY, nWidth, nHeight;
		CRect rtClient;
		GetClientRect(&rtClient);
		nWidth = rtClient.Width();
		nHeight = rtClient.Height();
		DWORD dwWidth = m_bgImage.GetWidth();
		DWORD dwHeight = m_bgImage.GetHeight();
		if(dwWidth != nWidth || dwHeight != nHeight)
		{
			if(!m_bgImage.Resample(nWidth, nHeight, 3))	//default option(bilinear interpolation)
			{
				AfxMessageBox("Fail to resample BG image !");
			}//if
		}//if
*/
	}
	else
	{
		strMsg.Format("[ %s ] 이미지 파일이 없습니다.", m_strBgImgPath);
		AfxMessageBox(strMsg);
		return false;
	}//if

	//기본 버튼 메뉴 이미지
	strExt = FindExtension(m_strMenuImgPath);
	nImgType = CxImage::GetTypeIdFromName(strExt);
	if(!m_imgMenu.Load(m_strMenuImgPath, nImgType))
	{
		strMsg.Format("[ %s ] 이미지 파일이 없습니다.", m_strMenuImgPath);
		AfxMessageBox(strMsg);
		return false;
	}//if

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 메뉴의 실행 상태에 따른 메뉴의 이미지 업데이트 작업을 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CLabPlayerDlg::UpdateMenuState()
{
	CString strMsg, strExt;
	int nImgType;
	m_strMenuImgPath = GetAppPath();
	m_strMenuImgPath += pszBtnImg[m_nMenuState];
	strExt = FindExtension(m_strMenuImgPath);
	nImgType = CxImage::GetTypeIdFromName(strExt);
	if(!m_imgMenu.Load(m_strMenuImgPath, nImgType))
	{
		strMsg.Format("[ %s ] 이미지 파일이 없습니다.", m_strMenuImgPath);
		AfxMessageBox(strMsg);
	}//if

	Invalidate();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 동영상 프로그램을 실행시킨다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CLabPlayerDlg::PlayMovie()
{
	//배경이미지 바꾸기
	m_strBgImgPath = GetAppPath();
	m_strBgImgPath += IMG_BG_PLAY;
	CString strExt = FindExtension(m_strBgImgPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	if(!m_imgBG.Load(m_strBgImgPath, nImgType))
	{
		CString strMsg;
		strMsg.Format("[ %s ] 이미지 파일이 없습니다.", m_imgBG);
		AfxMessageBox(strMsg);
	}//if
	//메뉴버튼 이미지 지움
	m_imgMenu.Enable(false);

	//실행파일을 찾고 위치 조정
	CString strTitle = pszTitle[m_nMenuState];
	CString strSubTitle = pszSubTitle[m_nMenuState];
	CString strExeName = GetAppPath();
	strExeName += pszExeName[m_nMenuState];

	CRect rect;
	GetClientRect(&rect);

	PROCESS_INFORMATION pi = {0};
	STARTUPINFO si = {0};
	if(CreateProcess(0, (LPSTR)(LPCSTR)strExeName, 0, 0, TRUE, CREATE_SUSPENDED, 0, 0, &si, &pi) )
	{
		::ResumeThread(pi.hThread);

		HWND hwnd = NULL;
		while(hwnd == NULL)
		{
			hwnd = ::FindWindow(NULL, strTitle);
			::Sleep(1);
		}
		/*SetWindowLong(hwnd, GWL_STYLE, WS_VISIBLE | WS_OVERLAPPED/* | WS_CAPTION |
						WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME/**///);
		::MoveWindow(hwnd, 0, 0, 0, 0, TRUE);

		//Sleep(300);

		m_hSubWnd = NULL;
		m_hSubWnd = ::FindWindow(NULL, strSubTitle);
		while(m_hSubWnd == NULL)
		{
			m_hSubWnd = ::FindWindow(NULL, strSubTitle);
			//::Sleep(1);
			//SPAS인 경우에만 딜레이가 필요
			if(m_nMenuState == EN_STATE_SPAS)
			{
				::Sleep(30);
			}//if
		}
		/*SetWindowLong(sub_wnd, GWL_STYLE, WS_VISIBLE | WS_OVERLAPPED/* | WS_CAPTION |
						WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME/**///);

		::MoveWindow(m_hSubWnd, EXE_X, EXE_Y, EXE_W, EXE_H, TRUE);

		// close handle
		CloseHandle(pi.hThread);
		CloseHandle(pi.hProcess);
	}//if

	m_btnHome.ShowWindow(SW_SHOW);
	Invalidate();

	//SPAS인 경우 동영상 재생중에 클릭을 하면 창의 위치가 변경되는 문제가 있다.
	if(m_nMenuState == EN_STATE_SPAS)
	{
		SetTimer(TM_SPAS, 10, NULL);
	}//if

	return true;
}


void CLabPlayerDlg::PlayAvm()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_AVM;
	UpdateMenuState();
	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}

void CLabPlayerDlg::PlaySpas()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_SPAS;
	UpdateMenuState();

	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}

void CLabPlayerDlg::PlayLdws()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_LDWS;
	UpdateMenuState();

	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}

void CLabPlayerDlg::PlayScc()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_SCC;
	UpdateMenuState();

	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}

void CLabPlayerDlg::PlayBsd()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_BSD;
	UpdateMenuState();

	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}
/*
void CLabPlayerDlg::OnBnClickedAvmBtn()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_AVM;
	UpdateMenuState();
	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}

void CLabPlayerDlg::OnBnClickedSpasBtn()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_SPAS;
	UpdateMenuState();

	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}

void CLabPlayerDlg::OnBnClickedLdwsBtn()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_LDWS;
	UpdateMenuState();

	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}

void CLabPlayerDlg::OnBnClickedSccBtn()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_SCC;
	UpdateMenuState();

	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}

void CLabPlayerDlg::OnBnClickedBsdBtn()
{
	//무언가 재생중이라면 중복재생 방지...
	if(m_bPlaying != false)
	{
		return;
	}//if
	m_bPlaying = true;

	m_nMenuState = EN_STATE_BSD;
	UpdateMenuState();

	Invalidate();

	SetTimer(TM_PLAY, 1, NULL);	
}
*/
BOOL CLabPlayerDlg::PreTranslateMessage(MSG* pMsg)
{
	//if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	//{
	//	if(m_bPlaying)
	//	{
	//		return TRUE;
	//	}//if
	//}//if

	return CDialog::PreTranslateMessage(pMsg);
}

void CLabPlayerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == TM_EXIT)
	{
		KillTimer(TM_EXIT);
		this->SendMessage(WM_CLOSE, 0, 0);
	}
	else if(nIDEvent == TM_PLAY)
	{
		KillTimer(TM_PLAY);
		if(!PlayMovie())
		{
			//AfxMessageBox("재생할 동영상을 찾을 수 없습니다!");
		}//if	
	}
	else if(nIDEvent == TM_SPAS)
	{
		if(m_hSubWnd)
		{
			::MoveWindow(m_hSubWnd, EXE_X, EXE_Y, EXE_W, EXE_H, TRUE);
		}//if
	}//if

	CDialog::OnTimer(nIDEvent);
}

void CLabPlayerDlg::OnBnClickedHomeBtn()
{
	KillTimer(TM_SPAS);

	//재생중인 동영상 창에 'ESC' 키 이벤트를 보낸다.
	::SendMessage(m_hSubWnd, WM_CLOSE, 0, 0);
	m_hSubWnd = NULL;

	//버튼을 숨기고 배그라운드 이미지를 변경한다.
	//그리고 버튼 메뉴 이미지 변경
	m_btnHome.ShowWindow(SW_HIDE);
	m_nMenuState = EN_STATE_NONE;
	m_imgMenu.Enable(true);
	InitPath();
	LoadImage();

	//재생 플래그 변경
	m_bPlaying = false;

	Invalidate();
}

void CLabPlayerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	//동영상이 재생중 이라면 동영상 화면이 최상위로 나오도록 해야한다.
	if(m_bPlaying && m_hSubWnd)
	{
		::SetWindowPos(m_hSubWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
		::ShowWindow(m_hSubWnd, SW_SHOW);
		::SetForegroundWindow(m_hSubWnd);
	}//if

	CDialog::OnLButtonDown(nFlags, point);
}

void CLabPlayerDlg::OnSetFocus(CWnd* pOldWnd)
{
	CDialog::OnSetFocus(pOldWnd);

	//동영상이 재생중 이라면 동영상 화면이 최상위로 나오도록 해야한다.
	if(m_bPlaying && m_hSubWnd)
	{
		::SetWindowPos(m_hSubWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
		::ShowWindow(m_hSubWnd, SW_SHOW);
		::SetForegroundWindow(m_hSubWnd);
	}//if
}

void CLabPlayerDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	//동영상이 재생중 이라면 동영상 화면이 최상위로 나오도록 해야한다.
	if(m_bPlaying && m_hSubWnd)
	{
		::SetWindowPos(m_hSubWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
		::ShowWindow(m_hSubWnd, SW_SHOW);
		::SetForegroundWindow(m_hSubWnd);
	}//if

	CDialog::OnRButtonDown(nFlags, point);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 마름모 모양의 버튼 영역들을 생성한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CLabPlayerDlg::CreateBtnRgn()
{
	CRgn* pRgn = NULL;
	CString str;
	CPoint pts[4];
	CRect rect;
	GetClientRect(&rect);
	CWndBtn* pwndBtn;
	//BYTE btTrans = 255;

	//HMODULE hUser32 = ::GetModuleHandle( _T("user32.dll"));
	//if(!hUser32)
	//{
	//	AfxMessageBox("Fail to get LayeredWindowAttributes");
	//	return;
	//}//if
	//btn_SetLayeredWindowAttributes = (TRANCPARENCY_TYPE)::GetProcAddress( hUser32, "SetLayeredWindowAttributes");
	//if(!btn_SetLayeredWindowAttributes)
	//{
	//	AfxMessageBox("Fail to get LayeredWindowAttributes ProcAddress");
	//	return;
	//}//if

	for(int i=0; i<5; i++)		//5개의 버튼
	{
		for(int j=0; j<4; j++)	//4개의 꼭지점
		{
			pts[j].SetPoint(pntBtn[i][j][0], pntBtn[i][j][1]);
		}//for

		CRgn rgnBtn;
		if(!rgnBtn.CreatePolygonRgn(pts, 4, ALTERNATE))
		{
			TRACE("Fail to create Rgn\r\n");
		}//if

		pwndBtn = new CWndBtn;
		pwndBtn->m_nId = i+1;
		pwndBtn->m_pParent = this;
		pwndBtn->m_strName = pszSubTitle[i+1];
		pwndBtn->Create(NULL, "BtnTest", WS_CHILD | WS_VISIBLE, rect, this, i+1234);
		if(pwndBtn->SetWindowRgn(rgnBtn, TRUE) == 0)
		{
			LPVOID lpMsgBuf;
			FormatMessage( 
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				GetLastError(),
				0, // Default language
				(LPTSTR) &lpMsgBuf,
				0,
				NULL 
				);

			TRACE((LPCTSTR)lpMsgBuf);
			// Free the buffer.
			LocalFree(lpMsgBuf);
		}//if

		/*LONG lOldStyle = GetWindowLong(pwndBtn->m_hWnd, GWL_EXSTYLE); 
		SetWindowLong(pwndBtn->m_hWnd, GWL_EXSTYLE, lOldStyle | WS_EX_LAYERED); 
		btn_SetLayeredWindowAttributes(pwndBtn->m_hWnd, 0, btTrans, LWA_ALPHA|LWA_COLORKEY);*/

		m_aryBtn.Add(pwndBtn);
	}//for
}

void CLabPlayerDlg::OnDestroy()
{
	CWndBtn* pBtn = NULL;
	for(int i=0; i<m_aryBtn.GetCount(); i++)
	{
		pBtn = (CWndBtn*)m_aryBtn.GetAt(i);
		pBtn->DestroyWindow();
		delete pBtn;
	}//for
	m_aryBtn.RemoveAll();

	CDialog::OnDestroy();
}
