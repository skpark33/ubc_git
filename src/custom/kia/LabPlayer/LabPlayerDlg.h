// LabPlayerDlg.h : 헤더 파일
//

#pragma once

#include "ximage.h"
#include "afxwin.h"
#include "HoverButton.h"


// CLabPlayerDlg 대화 상자
class CLabPlayerDlg : public CDialog
{
// 생성입니다.
public:
	CLabPlayerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LABPLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

public:
	CString	FindExtension(const CString& strFileName);	///<파일명에서 확장자를 분리하여 반환
	LPCTSTR GetAppPath(void);							///<실행 프로그램의 경로를 반환
	bool	PlayMovie(void);							///<지정된 동영상 프로그램을 실행시킨다.
	void	InitPath(void);								///<이미지들의 경로를 설정한다.
	bool	LoadImage(void);							///<이미지들을 읽는다.
	void	UpdateMenuState(void);						///<메뉴의 실행 상태에 따른 메뉴의 이미지 업데이트 작업을 한다.
	void	CreateBtnRgn(void);							///<마름모 모양의 버튼 영역들을 생성한다.

	void	PlayScc(void);								///<SCC를 재생한다.
	void	PlayAvm(void);								///<AVM을 재생한다.
	void	PlaySpas(void);								///<SPAS를 재생한다.
	void	PlayBsd(void);								///<BSD를 재생한다.
	void	PlayLdws(void);								///<LDWS를 재생한다.

// 구현입니다.
protected:
	HICON m_hIcon;

	CxImage				m_imgBG;						///<배경 이미지
	CxImage				m_imgMenu;						///<메뉴 이미지
	CString				m_strBgImgPath;					///<배경 이미지 경로
	CString				m_strMenuImgPath;				///<메뉴 이미지 경로
	HWND				m_hSubWnd;						///<동영상 창의 자식윈도우 핸들

	/*
	CButton				m_btnSCC;						///<SCC 버튼
	CButton				m_btnAVM;						///<AVM 버튼
	CButton				m_btnSPAS;						///<SPAS 버튼
	CButton				m_btnBSD;						///<BSD 버튼
	CButton				m_btnLDWS;						///<LDWS 버튼
	*/
	CHoverButton		m_btnHome;						///<Home 버튼

	CPtrArray			m_aryBtn;						///<마름모 모양의 버튼 윈도우

	int					m_nMenuState;					///<메뉴의 실행상태
	bool				m_bPlaying;						///<현재 동영상이 재생중인지 여부

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	/*
	afx_msg void OnBnClickedSccBtn();
	afx_msg void OnBnClickedAvmBtn();
	afx_msg void OnBnClickedSpasBtn();
	afx_msg void OnBnClickedBsdBtn();
	afx_msg void OnBnClickedLdwsBtn();
	*/
	afx_msg void OnBnClickedHomeBtn();
	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
};
