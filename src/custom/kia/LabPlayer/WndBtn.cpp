// WndBtnTest.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LabPlayer.h"
#include "WndBtn.h"
#include "LabPlayerDlg.h"

COLORREF	g_ColorTable[13] = {
	RGB(255,	255,	255),
	RGB(255,	0,		0),
	RGB(255,	255,	0),
	RGB(0,		255,	0),
	RGB(0,		255,	255),
	RGB(0,		0,		255),
	RGB(255,	0,		255),
	RGB(128,	0,		128),
	RGB(0,		0,		128),
	RGB(0,		128,	128),
	RGB(0,		128,	0),
	RGB(128,	128,	0),
	RGB(128,	0,		0),
};


// CWndBtn

IMPLEMENT_DYNAMIC(CWndBtn, CWnd)

CWndBtn::CWndBtn()
: m_nId(0)
, m_strName("")
, m_pParent(NULL)
{

}

CWndBtn::~CWndBtn()
{
}


BEGIN_MESSAGE_MAP(CWndBtn, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()



// CWndBtn 메시지 처리기입니다.



void CWndBtn::OnPaint()
{
	COLORREF rgb = g_ColorTable[ (m_nId+10) % 13 ];

	CPaintDC dc(this);
/*
	CRect client_rect;
	GetClientRect(client_rect);
	dc.FillSolidRect(client_rect, rgb);
*/
}

int CWndBtn::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CWndBtn::OnLButtonDown(UINT nFlags, CPoint point)
{/*
	CString str;
	str.Format("[%d] %s", m_nId, m_strName);
	AfxMessageBox(str);
*/

	CLabPlayerDlg* pDlg = (CLabPlayerDlg*)m_pParent;
	switch(m_nId)
	{
	case 1:
		{
			pDlg->PlayScc();
		}
		break;
	case 2:
		{
			pDlg->PlayAvm();
		}
		break;
	case 3:
		{
			pDlg->PlaySpas();
		}
		break;
	case 4:
		{
			pDlg->PlayBsd();
		}
		break;
	case 5:
		{
			pDlg->PlayLdws();
		}
		break;
	default:
		{
			TRACE("Unknown id\r\n");
		}
	}//switch

	CWnd::OnLButtonDown(nFlags, point);
}
