//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by LabPlayer.rc
//
#define IDD_LABPLAYER_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDB_HOME_BMP                    129
#define IDI_ICON1                       132
#define IDC_SPAS_BTN                    1000
#define IDC_SCC_BTN                     1001
#define IDC_AVM_BTN                     1002
#define IDC_LDWS_BTN                    1003
#define IDC_BSD_BTN                     1004
#define IDC_HOME_BTN                    1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
