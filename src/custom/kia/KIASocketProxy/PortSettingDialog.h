#pragma once
#include "afxwin.h"


// CPortSettingDialog 대화 상자입니다.

class CPortSettingDialog : public CDialog
{
	DECLARE_DYNAMIC(CPortSettingDialog)

public:
	CPortSettingDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPortSettingDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PORT_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	UINT	m_nCommandListenPort;
	UINT	m_nControlListenPort;

public:
	CEdit	m_editCommandPort;
	CEdit	m_editControlPort;

	void	SetPort(UINT nCmdPort, UINT nCtlPort) { m_nCommandListenPort=nCmdPort; m_nControlListenPort=nCtlPort; };
	void	GetPort(UINT& nCmdPort, UINT& nCtlPort) { nCmdPort=m_nCommandListenPort; nCtlPort=m_nControlListenPort; };
};
