// CommandSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"

#include "KIASocketProxy.h"
#include "CommandSocket.h"

#include "ControlSocket.h"

CCriticalSection CCommandSocket::m_Lock;
CArray<CCommandSocket*, CCommandSocket*> CCommandSocket::m_CmdSocketList;
int CCommandSocket::m_nExpireTime = 3;


// CCommandSocket

CCommandSocket::CCommandSocket()
{
}

CCommandSocket::~CCommandSocket()
{
}


// CCommandSocket 멤버 함수

void CCommandSocket::OnConnect(int nErrorCode)
{
	CProxySocket::OnConnect(nErrorCode);
}

void CCommandSocket::OnClose(int nErrorCode)
{
	CString str;
	str.Format("%s:%d", m_strPeerName, m_nPeerPort);

	POST_LOG( 
		m_hParentWnd,
		"Command", "Close", 
		m_strPeerName, m_nPeerPort,
		"", 0, 
		NULL, 0
	);

	::PostMessage(m_hParentWnd, WM_CLOSE_COMMAND_SOCKET, (WPARAM)this, NULL);

	CProxySocket::OnClose(nErrorCode);
}

void CCommandSocket::OnReceive(int nErrorCode)
{
	if(m_pReadBuffer)
	{
		int nRead = Receive(m_pReadBuffer, 1024); 
		//ciDEBUG(10, ("Receive (0x%08X) ReadSize=%d", this, nRead) );

		switch (nRead)
		{
		case 0:
			// read nothing -> socket error
			//ciWARN( ("Fail to receive !!! (0x%08X) Read Nothing", this) );
			//m_strErrMsg = "WARNING : Fail to receive (Read Nothing)";
			Close();
			//::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			break;

		case SOCKET_ERROR:
			if (GetLastError() != WSAEWOULDBLOCK) 
			{
				// socket error occurred
				//ciWARN( ("Fail to receive !!! (0x%08X) SOCKET_ERROR", this) );
				//m_strErrMsg = "WARNING : Fail to receive (SOCKET_ERROR)";
				Close();
				//::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			}
			break;

		default:
			POST_LOG( 
				m_hParentWnd, 
				"Command", "Receive", 
				"", m_nAcceptPort, 
				m_strPeerName, m_nPeerPort, 
				m_pReadBuffer, nRead
			);
//			BufferToString(m_pReadBuffer, nRead);

			if( m_Buffer.Append(m_pReadBuffer, nRead) )
			{
				PacketAnalysis();
			}
			else
			{
				// fail to append received-buffer -> not enough memory
				//ciWARN( ("Fail to append Buffer !!! (0x%08X)", this) );
				//m_strErrMsg = "WARNING : Fail to append Buffer";
				Close();
				//	::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			}
			break;
		}
	}
	else
	{
		// receive-buffer is null -> not enough memory
		//ciWARN( ("ReadBuffer is NULL !!! (0x%08X)", this) );
		//m_strErrMsg = "WARNING : ReadBuffer is NULL";
		Close();
		//::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CCommandSocket::PacketAnalysis()
{
	int buf_size = m_Buffer.GetBufferSize();

	for( ; buf_size >= 18; buf_size-=18 )
	{
		LPBYTE buff;
		m_Buffer.Pop(18, buff);

		char buff_scr[3] = {0};
		char buff_act[3] = {0};

		memcpy(buff_scr, buff+14, 2);
		memcpy(buff_act, buff+16, 2);
		delete[]buff;

		CString msg;
		msg.Format("Screen:%s, Action:%s", buff_scr, buff_act);

		POST_LOG2( 
			m_hParentWnd, 
			"Command", "Change", 
			"", 0, 
			"", 0, 
			msg );

		//
		HWND brwHwnd = getWHandle(getPid("UTV_brwClient2.exe"));
		if(brwHwnd)
		{
			if(memcmp(buff_scr,"00",2)==0 && memcmp(buff_act,"00",2)==0)
			{
				// start
				//// check last_screen_changed_time
				//DWORD cur_tick = ::GetTickCount();
				//while(cur_tick - m_dwLastScreenChanged < 2000)
				//{
				//	::Sleep(0);
				//	cur_tick = ::GetTickCount();
				//}
				//m_dwLastScreenChanged = cur_tick;

				//// send close_socket_command to flash
				//CControlSocket::SendAll(NULL, (LPBYTE)"123456789012348888", 18);
				//::Sleep(100);

				//// end
				//COPYDATASTRUCT appInfo;
				//appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
				//appInfo.lpData = (char*)(LPCSTR)m_strLastScreen;
				//appInfo.cbData =m_strLastScreen.GetLength() +1 ;
				//::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
			}
			if(memcmp(buff_scr,"99",2)==0 && memcmp(buff_act,"99",2)==0)
			{
				// check last_screen_changed_time
				DWORD cur_tick = ::GetTickCount();
				while(cur_tick - m_dwLastScreenChanged < 2000)
				{
					::Sleep(0);
					cur_tick = ::GetTickCount();
				}
				m_dwLastScreenChanged = cur_tick;

				// send close_socket_command to flash
				CControlSocket::SendAll(NULL, (LPBYTE)"123456789012348888", 18);
				::Sleep(100);

				// end
				COPYDATASTRUCT appInfo;
				appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
				appInfo.lpData = (char*)"CTRL+I";
				appInfo.cbData =strlen("CTRL+I") +1 ;
				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
			}
		}

		//
		BYTE result[] = "1";
		POST_LOG( 
			m_hParentWnd, 
			"Command", "Send", 
			"", m_nAcceptPort, 
			m_strPeerName, m_nPeerPort, 
			result, 1
		);

		if(Send(result, 1))
		{
			//
			// fail to send result
			//
		}
	}
}
