// KIASocketProxyView.h : CKIASocketProxyView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"

#include "ListenSocket.h"

#include "LogListCtrl.h"

class CKIASocketProxyView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CKIASocketProxyView();
	DECLARE_DYNCREATE(CKIASocketProxyView)

public:
	enum{ IDD = IDD_KIASOCKETPROXY_FORM };

// 특성입니다.
public:
	CKIASocketProxyDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CKIASocketProxyView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

	CListenSocket	m_CommandListenSocket;
	CListenSocket	m_ControlListenSocket;

	UINT	m_nCommandListenPort;
	UINT	m_nControlListenPort;

public:
	//CListCtrl m_lcLog;
	CLogListCtrl	m_lcLog;

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT	OnReportLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnResetListenSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseCommandSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseControlSocket(WPARAM wParam, LPARAM lParam);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPortSettings();
};

#ifndef _DEBUG  // KIASocketProxyView.cpp의 디버그 버전
inline CKIASocketProxyDoc* CKIASocketProxyView::GetDocument() const
   { return reinterpret_cast<CKIASocketProxyDoc*>(m_pDocument); }
#endif

