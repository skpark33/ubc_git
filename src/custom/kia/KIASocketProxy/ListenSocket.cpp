// ListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "KIASocketProxy.h"
#include "ListenSocket.h"

#include "CommandSocket.h"
#include "ControlSocket.h"

#include "Lib.h"

// CListenSocket

CListenSocket::CListenSocket()
:	m_bOpen (false)
,	m_bRetryConnect (true)
{
}

CListenSocket::~CListenSocket()
{
}

// CListenSocket 멤버 함수

void CListenSocket::OnAccept(int nErrorCode)
{
	if(nErrorCode)
	{
		//ciWARN( ("OnAccept ERROR !!! [ErrCode:%s]", ::GetErrorMessageString(nErrorCode)) );
	}
	else
	{
		//ciDEBUG(10, ("OnAccept()") );
	}

	CProxySocket* p_sock = NULL;
	switch(m_eListenType)
	{
	case eCommand:
		p_sock = new CCommandSocket();
		break;
	case eControl:
		p_sock = new CControlSocket();
		break;
	}

	// add to listctrl
	if(p_sock)
	{
		p_sock->m_hParentWnd = m_hParentWnd;

		// success to create command-socket obj
		if( Accept(*p_sock) )
		{
			switch(m_eListenType)
			{
			case eCommand:
				CCommandSocket::AddSocket((CCommandSocket*)p_sock);
				break;
			case eControl:
				CControlSocket::AddSocket((CControlSocket*)p_sock);
				break;
			}
			p_sock->m_nAcceptPort = m_nPort;

			// success to accept
			p_sock->GetPeerName(p_sock->m_strPeerName, p_sock->m_nPeerPort);
			//ciDEBUG(10, ("Accept (0x%08X) %s:%u", call_socket, call_socket->m_strPeerName, call_socket->m_nPeerPort) );

			CString str;
			str.Format("%s:%d", p_sock->m_strPeerName, p_sock->m_nPeerPort);

			POST_LOG(
				m_hParentWnd,
				"Listen", "Accept", 
				"", m_nPort,
				p_sock->m_strPeerName, p_sock->m_nPeerPort,
				NULL, 0);
		}
		else
		{
			// fail to accept
			DWORD nErrorCode = GetLastError();
			//ciWARN( ("Fail to accept !!! (0x%08X) [ErrCode:%s] %s:%u", call_socket, ::GetErrorMessageString(nErrorCode), call_socket->m_strPeerName, call_socket->m_nPeerPort) );

			CString msg;
			msg.Format("ERROR : Fail to accept [ErrCode:%s]", ::GetErrorMessageString(nErrorCode) );

			delete p_sock;

			Close();

			::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
		}
	}
	else
	{
		// fail to create call-socket obj
		//ciERROR(("Fail to create CallSocket object !!!"));
		POST_LOG( 
			m_hParentWnd, 
			"Listen", "Can't create Socket", 
			"", m_nPort, 
			"", 0, 
			NULL, 0
		);
	}

	CAsyncSocket::OnAccept(nErrorCode);
}

void CListenSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	m_bOpen = false;

	if(nErrorCode)
	{
		//ciWARN( ("OnClose ERROR !!! [ErrCode:%s]", ::GetErrorMessageString(nErrorCode)) );
	}
	else
	{
		//ciDEBUG(10, ("OnClose()") );
	}

	POST_LOG( 
		m_hParentWnd,
		"Listen", "Close", 
		"", m_nPort, 
		"", 0, 
		NULL, 0
	);

	if(m_bRetryConnect)
	{
		// not StopListen -> retry StartListen
		//ciWARN( ("Retry Connect Listen Socket !!!") );
		::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
	}

	CAsyncSocket::OnClose(nErrorCode);
}

void CListenSocket::Init(HWND hParentWnd, UINT nPort, LISTEN_TYPE eType)
{
	//ciDEBUG(10, ("Init(Port:%u)", nPort) );

	m_hParentWnd = hParentWnd;
	m_nPort = nPort;
	m_eListenType = eType;
}

BOOL CListenSocket::StartListen()
{
	//ciDEBUG(10, ("StartListen(Port:%u)", m_nPort) );

	m_bRetryConnect = true;
	if(Create(m_nPort))
	{
		// success to create
		BOOL ret_val = Listen();
		if(ret_val)
		{
			// success to listen
			m_bOpen = true;
			POST_LOG( 
				m_hParentWnd, 
				"Listen", "Start",
				"", m_nPort, 
				"", 0, 
				NULL, 0
			);
		}
		else
		{
			// fail to listen -> retry StartListen
			//ciERROR( ("Fail to listen ListenSocket !!! [ErrCode:%s]", ::GetErrorMessageString(GetLastError()) ) );
			Close();
			::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
		}
		return ret_val;
	}
	else
	{
		// fail to create -> retry StartListen
		//ciERROR( ("Fail to open ListenSocket !!! [ErrCode:%s]", ::GetErrorMessageString(GetLastError()) ) );
		Close();
		::PostMessage(m_hParentWnd, WM_RESET_LISTEN_SOCKET, NULL, NULL);
		return NULL;
	}
}

void CListenSocket::StopListen()
{
	//ciDEBUG(10, ("StopListen()") );

	m_bRetryConnect = false;
	m_bOpen = false;
	Close();
}


DWORD CProxySocket::m_dwLastScreenChanged = 0;
CString	CProxySocket::m_strLastScreen = "Ctrl+K";


CString CProxySocket::BufferToString(LPBYTE pBuff, int size)
{
	if(size == 0)
		return "No Data";

	CString ret_val = "";
	CString str_tmp;

	for(int i=0; i<size; i++)
	{
		if( 32 <= pBuff[i] || pBuff[i] < 127 )
			ret_val.AppendChar((char)pBuff[i]);
		else
			ret_val += " ";

		str_tmp.Format("(%02X)", pBuff[i]);
		ret_val += str_tmp;
	}

	return ret_val;
}


unsigned long CProxySocket::getPid(const char* exename)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if( hSnapshot == INVALID_HANDLE_VALUE ) return 0;

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	char strProcessName[512] = {0};
	if( !Process32First ( hSnapshot, &pe32 ) )
	{
		::CloseHandle(hSnapshot);
		return 0;
	}

	do
	{
		//memset(strProcessName, 0, sizeof(strProcessName));
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(pe32.szExeFile, exename) == 0 )
		{
			::CloseHandle(hSnapshot);
			return pe32.th32ProcessID;
		}

	} while (Process32Next( hSnapshot, &pe32 ));

	::CloseHandle(hSnapshot);
	return 0;
}

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if (!IsWindowVisible(hwnd)) return TRUE;
	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);
	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;
	if (pe->pid != pid) return TRUE;
	pe->hwnd = hwnd;
	return FALSE;
}

HWND CProxySocket::getWHandle(unsigned long pid)
{
	if( pid > 0 )
	{
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);

		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1)
		{
			parent =  GetParent(child);
			if(!parent){
				return child;
			}
			child = parent;
		}
		return e.hwnd;
	}
	return (HWND)0;
}
