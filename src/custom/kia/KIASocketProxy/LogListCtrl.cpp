// LogListCtrl.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "KIASocketProxy.h"
#include "LogListCtrl.h"


// CLogListCtrl

IMPLEMENT_DYNAMIC(CLogListCtrl, CListCtrl)

CLogListCtrl::CLogListCtrl()
{

}

CLogListCtrl::~CLogListCtrl()
{
}


BEGIN_MESSAGE_MAP(CLogListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CLogListCtrl::OnNMCustomdraw)
END_MESSAGE_MAP()



// CLogListCtrl 메시지 처리기입니다.



void CLogListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int iRow;
	switch(lplvcd->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		break;

	// Modify item text and or background
	case CDDS_ITEMPREPAINT:
		iRow = static_cast<int>( lplvcd->nmcd.dwItemSpec );

//		if(m_bReadOnly)
//		{
//			lplvcd->clrTextBk = ::GetSysColor(COLOR_BTNFACE);
//			*pResult = CDRF_DODEFAULT;
//		}
//		else
		{
			// 특수한 조건일 경우 가로줄 색상 변경

//			if(m_listRowColor.GetSize() > iRow)
//				lplvcd->clrTextBk = m_listRowColor[iRow].dwColor;
//			lplvcd->clrText = RGB(176, 176, 176);
			CString str = GetItemText(iRow, 2);
			if(str == "Listen")
				lplvcd->clrTextBk = RGB(192,255,192);
			else if(str == "Command")
				lplvcd->clrTextBk = RGB(255,192,192);
			else if(str == "Control")
				lplvcd->clrTextBk = RGB(192,192,255);

			*pResult = CDRF_NOTIFYSUBITEMDRAW;
		}

		break;

	// Modify sub item text and/or background
	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		// 특정 컬럼 색상 맞춤
		//for(int x=0; x<m_listColumnColor.GetSize(); x++)
		//{
		//	if(m_listColumnColor[x].nColumn == lplvcd->iSubItem)
		//	{
		//		lplvcd->clrTextBk = LIST_COLOR_01;
		//		break;
		//	}
		//	else
		//		lplvcd->clrTextBk = RGB(255, 255, 255);
		//}

		*pResult = CDRF_NEWFONT;
		break;

	default:
		*pResult = CDRF_DODEFAULT;
		break;
	}
}
