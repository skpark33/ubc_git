//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by KIASocketProxy.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_KIASOCKETPROXY_FORM         101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_KIASocketProxyTYPE          129
#define IDD_PORT_SETTINGS               130
#define IDC_LIST_LOG                    1000
#define IDC_EDIT_COMMAND_PORT           1001
#define IDC_EDIT2                       1002
#define IDC_EDIT_CONTROL_PORT           1002
#define ID_32771                        32771
#define ID_PORT_SETTINGS                32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
