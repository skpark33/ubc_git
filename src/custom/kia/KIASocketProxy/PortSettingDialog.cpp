// PortSettingDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "KIASocketProxy.h"
#include "PortSettingDialog.h"

#include "Lib.h"


// CPortSettingDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPortSettingDialog, CDialog)

CPortSettingDialog::CPortSettingDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPortSettingDialog::IDD, pParent)
{
	char buf[1024];
	CString ini_path = ::GetINIPath();

	GetPrivateProfileString("Settings", "CommandPort", "8081", buf, 1024, ini_path);
	m_nCommandListenPort = atoi(buf);

	GetPrivateProfileString("Settings", "ControlPort", "8082", buf, 1024, ini_path);
	m_nControlListenPort = atoi(buf);
}

CPortSettingDialog::~CPortSettingDialog()
{
}

void CPortSettingDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_COMMAND_PORT, m_editCommandPort);
	DDX_Control(pDX, IDC_EDIT_CONTROL_PORT, m_editControlPort);
}


BEGIN_MESSAGE_MAP(CPortSettingDialog, CDialog)
END_MESSAGE_MAP()


// CPortSettingDialog 메시지 처리기입니다.

BOOL CPortSettingDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_editCommandPort.SetWindowText(::ToString(m_nCommandListenPort));
	m_editControlPort.SetWindowText(::ToString(m_nControlListenPort));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPortSettingDialog::OnOK()
{
	//
	CString ini_path = ::GetINIPath();
	CString str_port;

	m_editCommandPort.GetWindowText(str_port);
	m_nCommandListenPort = atoi(str_port);
	WritePrivateProfileString("Settings", "CommandPort", str_port, ini_path);

	m_editControlPort.GetWindowText(str_port);
	m_nControlListenPort = atoi(str_port);
	WritePrivateProfileString("Settings", "ControlPort", str_port, ini_path);

	CDialog::OnOK();
}
