// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <afxsock.h>		// MFC 소켓 확장
#include <afxtempl.h>

#include "Lib.h"


#define		WM_REPORT_LOG				(WM_USER + 1026)
#define		WM_RESET_LISTEN_SOCKET		(WM_USER + 1027)
#define		WM_CLOSE_COMMAND_SOCKET		(WM_USER + 1028)
#define		WM_CLOSE_CONTROL_SOCKET		(WM_USER + 1029)
#define		WM_SOCKET_CHECK_AND_CLEAR	(WM_USER + 1030)


typedef struct {
	CTime		tmCurrent;
	CString		strType;
	CString		strStatus;
	CString		strSourceIP;
	CString		strTargetIP;
	CString		strData;
} LOG_ITEM;

#define		POST_LOG(hwnd, type, status, src_ip, src_port, tgt_ip, tgt_port, data, len)	\
			{																			\
				LOG_ITEM* log_item = new LOG_ITEM;										\
				log_item->tmCurrent = CTime::GetCurrentTime();							\
				log_item->strType = type;												\
				log_item->strStatus = status;											\
				if(src_ip) log_item->strSourceIP = src_ip;								\
				if(src_port) { log_item->strSourceIP += ":"; log_item->strSourceIP += ::ToString(src_port); }	\
				if(tgt_ip) log_item->strTargetIP = tgt_ip;								\
				if(tgt_port) { log_item->strTargetIP += ":"; log_item->strTargetIP += ::ToString(tgt_port); }	\
				if(len) log_item->strData = ::ByteArrayToString(data, len);				\
				::PostMessage(hwnd, WM_REPORT_LOG, (WPARAM)log_item, NULL);				\
			}

#define		POST_LOG2(hwnd, type, status, src_ip, src_port, tgt_ip, tgt_port, data)	\
			{																			\
				LOG_ITEM* log_item = new LOG_ITEM;										\
				log_item->tmCurrent = CTime::GetCurrentTime();							\
				log_item->strType = type;												\
				log_item->strStatus = status;											\
				if(src_ip) log_item->strSourceIP = src_ip;								\
				if(src_port) { log_item->strSourceIP += ":"; log_item->strSourceIP += ::ToString(src_port); }	\
				if(tgt_ip) log_item->strTargetIP = tgt_ip;								\
				if(tgt_port) { log_item->strTargetIP += ":"; log_item->strTargetIP += ::ToString(tgt_port); }	\
				if(data) log_item->strData = data;										\
				::PostMessage(hwnd, WM_REPORT_LOG, (WPARAM)log_item, NULL);				\
			}

CString		GetErrorMessageString(DWORD dwErrCode);

CString		ByteArrayToString(LPBYTE pData, int len);


//#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
//#endif


