#pragma once

#include <afxmt.h>
#include <afxtempl.h>

#include "ListenSocket.h"



////////////////////////////////////////////////////////////
// CCommandSocket 명령 대상입니다.

class CCommandSocket : public CProxySocket
{
public:
	static	CCriticalSection	 m_Lock;
	static	CArray<CCommandSocket*, CCommandSocket*>	m_CmdSocketList;
	static	void AddSocket(CCommandSocket* pCmdSock) { m_Lock.Lock(); m_CmdSocketList.Add(pCmdSock); m_Lock.Unlock(); };
	static	bool DeleteSocket(CCommandSocket* pCmdSock)
			{
				m_Lock.Lock();
				for(int i=0; i<m_CmdSocketList.GetCount(); i++)
				{
					CCommandSocket* cmd_sock = (CCommandSocket*)m_CmdSocketList.GetAt(i);
					if(cmd_sock == pCmdSock)
					{
						delete cmd_sock;
						m_CmdSocketList.RemoveAt(i);
						m_Lock.Unlock();
						return true;
					}
				}
				m_Lock.Unlock();
				return false;
			};
	static	int m_nExpireTime;
	static	void CheckAndClear()
			{
				m_Lock.Lock();
				for(int i=0; i<m_CmdSocketList.GetCount(); i++)
				{
					CCommandSocket* cmd_sock = (CCommandSocket*)m_CmdSocketList.GetAt(i);
					cmd_sock->m_Buffer.CheckAndClear(m_nExpireTime);
				}
				m_Lock.Unlock();
			};

public:
	CCommandSocket();
	virtual ~CCommandSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

protected:
	void PacketAnalysis();
};


