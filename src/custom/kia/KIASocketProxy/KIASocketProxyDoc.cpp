// KIASocketProxyDoc.cpp : CKIASocketProxyDoc 클래스의 구현
//

#include "stdafx.h"
#include "KIASocketProxy.h"

#include "KIASocketProxyDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CKIASocketProxyDoc

IMPLEMENT_DYNCREATE(CKIASocketProxyDoc, CDocument)

BEGIN_MESSAGE_MAP(CKIASocketProxyDoc, CDocument)
END_MESSAGE_MAP()


// CKIASocketProxyDoc 생성/소멸

CKIASocketProxyDoc::CKIASocketProxyDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CKIASocketProxyDoc::~CKIASocketProxyDoc()
{
}

BOOL CKIASocketProxyDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CKIASocketProxyDoc serialization

void CKIASocketProxyDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CKIASocketProxyDoc 진단

#ifdef _DEBUG
void CKIASocketProxyDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CKIASocketProxyDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CKIASocketProxyDoc 명령
