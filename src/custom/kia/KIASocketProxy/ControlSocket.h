#pragma once

#include <afxmt.h>
#include <afxtempl.h>

#include "ListenSocket.h"


// CControlSocket 명령 대상입니다.

class CControlSocket : public CProxySocket
{
public:
	static	CCriticalSection	 m_Lock;
	static	CArray<CControlSocket*, CControlSocket*>	m_CtlSocketList;
	static	void AddSocket(CControlSocket* pCtlSock) { m_Lock.Lock(); m_CtlSocketList.Add(pCtlSock); m_Lock.Unlock(); };
	static	bool DeleteSocket(CControlSocket* pCtlSock)
			{
				m_Lock.Lock();
				for(int i=0; i<m_CtlSocketList.GetCount(); i++)
				{
					CControlSocket* ctl_sock = (CControlSocket*)m_CtlSocketList.GetAt(i);
					if(ctl_sock == pCtlSock)
					{
						delete ctl_sock;
						m_CtlSocketList.RemoveAt(i);
						m_Lock.Unlock();
						return true;
					}
				}
				m_Lock.Unlock();
				return false;
			};
	static	void SendAll(CControlSocket* pSendSocket, LPBYTE pData, int nSize)
			{
				m_Lock.Lock();
				for(int i=0; i<m_CtlSocketList.GetCount(); i++)
				{
					CControlSocket* ctl_sock = (CControlSocket*)m_CtlSocketList.GetAt(i);
					if(ctl_sock != pSendSocket)
					{
						if(pSendSocket && ctl_sock->m_strPeerName=="127.0.0.2")
						{
							if(memcmp(pData, "123456789012340000", 18) )
								continue;
						}

						if(pSendSocket)
						{
							POST_LOG(
								ctl_sock->m_hParentWnd, 
								"Control",
								"Send",
								pSendSocket->m_strPeerName, pSendSocket->m_nPeerPort,
								ctl_sock->m_strPeerName, ctl_sock->m_nPeerPort,
								pData, nSize );
						}
						else
						{
							POST_LOG(
								ctl_sock->m_hParentWnd, 
								"Control",
								"Send",
								"127.0.0.1", 0,
								ctl_sock->m_strPeerName, ctl_sock->m_nPeerPort,
								pData, nSize );
						}

						int total_send_size = 0;
						while(total_send_size < nSize)
						{
							int send_size = ctl_sock->Send(pData+total_send_size, nSize-total_send_size);
							if(send_size == SOCKET_ERROR)
							{
								//
								// fail to send
								//
								break;
							}
							total_send_size += send_size;
						}
					}
				}
				m_Lock.Unlock();
			};

public:
	CControlSocket();
	virtual ~CControlSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

protected:
	void	PacketAnalysis();

};


