// KIASocketProxyView.cpp : CKIASocketProxyView 클래스의 구현
//

#include "stdafx.h"
#include "KIASocketProxy.h"

#include "KIASocketProxyDoc.h"
#include "KIASocketProxyView.h"

#include "PortSettingDialog.h"

#include "CommandSocket.h"
#include "ControlSocket.h"

#include "Lib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CKIASocketProxyView

IMPLEMENT_DYNCREATE(CKIASocketProxyView, CFormView)

BEGIN_MESSAGE_MAP(CKIASocketProxyView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_COMMAND(ID_PORT_SETTINGS, &CKIASocketProxyView::OnPortSettings)
	ON_MESSAGE(WM_REPORT_LOG, OnReportLog)
	ON_MESSAGE(WM_RESET_LISTEN_SOCKET, OnResetListenSocket)
	ON_MESSAGE(WM_CLOSE_COMMAND_SOCKET, OnCloseCommandSocket)
	ON_MESSAGE(WM_CLOSE_CONTROL_SOCKET, OnCloseControlSocket)
END_MESSAGE_MAP()

// CKIASocketProxyView 생성/소멸

CKIASocketProxyView::CKIASocketProxyView()
	: CFormView(CKIASocketProxyView::IDD)
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CKIASocketProxyView::~CKIASocketProxyView()
{
}

void CKIASocketProxyView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
}

BOOL CKIASocketProxyView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CKIASocketProxyView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//
	m_lcLog.SetExtendedStyle(m_lcLog.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	m_lcLog.InsertColumn(0, "No", 0, 35);
	m_lcLog.InsertColumn(1, "Time", 0, 125);
	m_lcLog.InsertColumn(2, "Type", 0, 125);
	m_lcLog.InsertColumn(3, "Status", 0, 125);
	m_lcLog.InsertColumn(4, "Source IP", 0, 135);
	m_lcLog.InsertColumn(5, "Target IP", 0, 135);
	m_lcLog.InsertColumn(6, "Data", 0, 535);

	//
	char buf[1024];
	CString ini_path = ::GetINIPath();

	GetPrivateProfileString("Settings", "CommandPort", "8081", buf, 1024, ini_path);
	m_nCommandListenPort = atoi(buf);

	GetPrivateProfileString("Settings", "ControlPort", "8082", buf, 1024, ini_path);
	m_nControlListenPort = atoi(buf);

	//
	m_CommandListenSocket.Init(GetSafeHwnd(), m_nCommandListenPort, CListenSocket::eCommand);
	m_ControlListenSocket.Init(GetSafeHwnd(), m_nControlListenPort, CListenSocket::eControl);

	SetTimer(WM_RESET_LISTEN_SOCKET, 1000, NULL);
	SetTimer(WM_SOCKET_CHECK_AND_CLEAR, 100, NULL);
}


// CKIASocketProxyView 진단

#ifdef _DEBUG
void CKIASocketProxyView::AssertValid() const
{
	CFormView::AssertValid();
}

void CKIASocketProxyView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CKIASocketProxyDoc* CKIASocketProxyView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CKIASocketProxyDoc)));
	return (CKIASocketProxyDoc*)m_pDocument;
}
#endif //_DEBUG


// CKIASocketProxyView 메시지 처리기

void CKIASocketProxyView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if(GetSafeHwnd() && m_lcLog.GetSafeHwnd())
	{
		m_lcLog.MoveWindow(0, 0, cx, cy);
	}
}

void CKIASocketProxyView::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case WM_RESET_LISTEN_SOCKET:
		KillTimer(WM_RESET_LISTEN_SOCKET);
		m_CommandListenSocket.StartListen();
		m_ControlListenSocket.StartListen();
		break;
	case WM_SOCKET_CHECK_AND_CLEAR:
		KillTimer(WM_SOCKET_CHECK_AND_CLEAR);
		CCommandSocket::CheckAndClear();
		SetTimer(WM_SOCKET_CHECK_AND_CLEAR, 100, NULL);
		break;
	}

	CFormView::OnTimer(nIDEvent);
}

void CKIASocketProxyView::OnPortSettings()
{
	CPortSettingDialog dlg;
	if(dlg.DoModal() == IDOK)
	{
		::AfxMessageBox("프로그램이 재실행될때 적용됩니다.\r\n\r\n프로그램을 재실행해 주시기 바랍니다.", MB_ICONWARNING);
	}
}

LRESULT CKIASocketProxyView::OnReportLog(WPARAM wParam, LPARAM lParam)
{
	if(wParam == NULL) return 0;

	// limit line-count
	int idx = m_lcLog.GetItemCount();
	if(idx > 1000)
	{
		for(int i=0; i<100; i++, idx--)
		{
			m_lcLog.DeleteItem(i);
		}
	}

	//
	static int no = 0;
	LOG_ITEM* log_item = (LOG_ITEM*)wParam;
	m_lcLog.InsertItem(idx, ::ToString(no++));
	m_lcLog.SetItemText(idx, 1, log_item->tmCurrent.Format("%Y-%m-%d %H:%M:%S"));
	m_lcLog.SetItemText(idx, 2, log_item->strType);
	m_lcLog.SetItemText(idx, 3, log_item->strStatus);
	m_lcLog.SetItemText(idx, 4, log_item->strSourceIP);
	m_lcLog.SetItemText(idx, 5, log_item->strTargetIP);
	m_lcLog.SetItemText(idx, 6, log_item->strData);

	delete log_item;

	return 1;
}

LRESULT CKIASocketProxyView::OnResetListenSocket(WPARAM wParam, LPARAM lParam)
{
	//
	// log
	//

	SetTimer(WM_RESET_LISTEN_SOCKET, 1000, NULL);

	return 0;
}

LRESULT CKIASocketProxyView::OnCloseCommandSocket(WPARAM wParam, LPARAM lParam)
{
	//
	// log
	//

	CCommandSocket* cmd_sock = (CCommandSocket*)wParam;
	CCommandSocket::DeleteSocket(cmd_sock);

	return 0;
}

LRESULT CKIASocketProxyView::OnCloseControlSocket(WPARAM wParam, LPARAM lParam)
{
	//
	// log
	//

	CControlSocket* ctl_sock  = (CControlSocket*)wParam;
	CControlSocket::DeleteSocket(ctl_sock);

	return 0;
}
