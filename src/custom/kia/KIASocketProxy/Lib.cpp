#include "stdafx.h"
#include "Lib.h"
/*
ControlLib::ControlLib()
{
}

ControlLib::~ControlLib()
{
}
*/

CString ToString(int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%d"), value);
	return buf;
}

CString ToString(unsigned int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%u"), value);
	return buf;
}

CString ToString(LONGLONG value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%I64d"), value);
	return buf;
}

CString ToString(ULONGLONG value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%I64u"), value);
	return buf;
}

CString ToString(double value, unsigned int precision)
{
	TCHAR format[128];
	_stprintf(format, _T("%%.%df"), precision);

	TCHAR buf[128];
	_stprintf(buf, format, value);
	return buf;
}

CString ToMoneyTypeString(LPCTSTR lpszStr)
{
	CString str = lpszStr;

	int nLength = str.Find(_T("."));
	if(nLength<0)
        nLength = str.GetLength();

	while( nLength > 3 )
	{
		nLength = nLength - 3;
		str.Insert( nLength, ',' );
	}

	return str;
}

CString ToMoneyTypeString(int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%d"), value);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(unsigned int value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%u"), value);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(LONGLONG value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%I64d"), value);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(ULONGLONG value)
{
	TCHAR buf[128];
	_stprintf(buf, _T("%I64u"), value);
	return ToMoneyTypeString(buf);
}

CString ToMoneyTypeString(double value, unsigned int precision)
{
	TCHAR format[128];
	_stprintf(format, _T("%%.%df"), precision);

	TCHAR buf[128];
	_stprintf(buf, format, value);
	return ToMoneyTypeString(buf);
}

int ToInt(LPCTSTR lpszStr)
{
	return _ttoi(lpszStr);
}

LONGLONG ToInt64(LPCTSTR lpszStr)
{
	return _ttoi64(lpszStr);
}

double ToDouble(LPCTSTR lpszStr)
{
	return _tstof(lpszStr);
}

LPCTSTR GetAppName()
{
	static CString appName = _T("");

	if(appName.GetLength() == 0)
	{
		appName.LoadString(AFX_IDS_APP_TITLE);
	}

	return (LPCTSTR)appName;
}

void GetAppName(CString &strAppName)
{
	strAppName = GetAppName();
}

//LPCTSTR GetAppVersion()
//{
//	static CString _appVersion = _T("");
//	if(_appVersion.GetLength() == 0)
//	{
//		TCHAR szFullPath[MAX_PATH];
//		GetModuleFileName(AfxGetApp()->m_hInstance, szFullPath, MAX_PATH);
//
//		DWORD dwVerHnd;
//		int dwVerInfoSize = GetFileVersionInfoSize(szFullPath, &dwVerHnd); 
//
//		char achData[4096];
//		BOOL bRtn = GetFileVersionInfo(szFullPath, dwVerHnd, 4096, (void*)achData);
//		if (bRtn == FALSE)
//		{
//			_appVersion.Empty();
//			return _T("");
//		}
//
//		LPVOID			lpBuffer;
//		unsigned int	dwBytes;
//
//		bRtn = VerQueryValue(
//						achData, 
//						TEXT("\\StringFileInfo\\041203b5\\ProductVersion"), 
//						&lpBuffer, 
//						&dwBytes); 
//
//		LPTSTR pTemp = (LPTSTR)lpBuffer;
//		_appVersion = pTemp;
//	}
//
//	return _appVersion;
//}
//
//void GetAppVersion(CString &strVersion)
//{
//	strVersion = GetAppVersion();
//}

LPCTSTR GetMainPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}

void GetINIPath(CString& strDirectory)
{
	strDirectory = GetINIPath();
}

LPCTSTR GetINIPath()
{
	static CString	_iniDirectory = _T("");

	if(_iniDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_iniDirectory = str;

		CString app;
		app.LoadString(AFX_IDS_APP_TITLE);

		_iniDirectory.Append(app);
		_iniDirectory.Append(_T(".ini"));
	}

	return _iniDirectory;
}

void GetMainPath(CString& strDirectory)
{
	strDirectory = GetMainPath();
}

BOOL WritePrivateProfileInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName)
{
	return WritePrivateProfileString(lpAppName, lpKeyName, ::ToString(nValue), lpFileName);
}

LPCTSTR GetWindowsVersion()
{
	static CString version = _T("");

	if(version.GetLength() == 0)
	{
		#define BUFSIZE 80

		OSVERSIONINFOEX osvi;
		BOOL bOsVersionInfoEx;

		// Try calling GetVersionEx using the OSVERSIONINFOEX structure.
		// If that fails, try using the OSVERSIONINFO structure.

		ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

		if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
		{
			osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
			if ( ! GetVersionEx ( (OSVERSIONINFO *) &osvi) )
				return NULL;
		}

		switch (osvi.dwPlatformId)
		{
		// Test for the Windows NT product family.
		case VER_PLATFORM_WIN32_NT:

			// Test for the specific product.
			if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
				version += "Microsoft Windows Server 2003, ";

			if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
				version += "Microsoft Windows XP ";

			if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
				version += "Microsoft Windows 2000 ";

			if ( osvi.dwMajorVersion <= 4 )
				version += "Microsoft Windows NT ";

			// Test for specific product on Windows NT 4.0 SP6 and later.
			if( bOsVersionInfoEx )
			{
				// Test for the workstation type.
				if ( osvi.wProductType == VER_NT_WORKSTATION )
				{
					if( osvi.dwMajorVersion == 4 )
						version += "Workstation 4.0 ";
					else if( osvi.wSuiteMask & VER_SUITE_PERSONAL )
						version += "Home Edition ";
					else
						version += "Professional ";
				}
			    
				// Test for the server type.
				else if ( osvi.wProductType == VER_NT_SERVER || 
							osvi.wProductType == VER_NT_DOMAIN_CONTROLLER )
				{
					if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
					{
						if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
							version += "Datacenter Edition ";
						else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
							version += "Enterprise Edition ";
						else if ( osvi.wSuiteMask == VER_SUITE_BLADE )
							version += "Web Edition ";
						else
							version += "Standard Edition ";
					}

					else if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
					{
						if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
							version += "Datacenter Server ";
						else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
							version += "Advanced Server ";
						else
							version += "Server ";
					}

					else  // Windows NT 4.0 
					{
						if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
							version += "Server 4.0, Enterprise Edition ";
						else
							version += "Server 4.0 ";
					}
				}
			}
			else  // Test for specific product on Windows NT 4.0 SP5 and earlier
			{
				HKEY hKey;
				char szProductType[BUFSIZE];
				DWORD dwBufLen=BUFSIZE;
				LONG lRet;

				lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
					_T("SYSTEM\\CurrentControlSet\\Control\\ProductOptions"),
					0, KEY_QUERY_VALUE, &hKey );
				if( lRet != ERROR_SUCCESS )
					return NULL;

				lRet = RegQueryValueEx( hKey, _T("ProductType"), NULL, NULL,
					(LPBYTE) szProductType, &dwBufLen);
				if( (lRet != ERROR_SUCCESS) || (dwBufLen > BUFSIZE) )
					return NULL;

				RegCloseKey( hKey );

				if ( strcmpi( "WINNT", szProductType) == 0 )
					version += _T("Workstation ");
				if ( strcmpi( "LANMANNT", szProductType) == 0 )
					version += _T("Server ");
				if ( strcmpi( "SERVERNT", szProductType) == 0 )
					version += _T("Advanced Server ");

				char buf[64];
				sprintf(buf, "%d.%d ", osvi.dwMajorVersion, osvi.dwMinorVersion );
				version += buf;
			}

		// Display service pack (if any) and build number.

			if( osvi.dwMajorVersion == 4 && 
				lstrcmpi( osvi.szCSDVersion, _T("Service Pack 6") ) == 0 )
			{
				HKEY hKey;
				LONG lRet;

				// Test for SP6 versus SP6a.
				lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
					_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Hotfix\\Q246009"),
					0, KEY_QUERY_VALUE, &hKey );
				if( lRet == ERROR_SUCCESS )
				{
					char buf[128];
					sprintf(buf, "Service Pack 6a (Build %d)", osvi.dwBuildNumber & 0xFFFF );
					version += buf;
				}
				else // Windows NT 4.0 prior to SP6a
				{
					char buf[128];
					sprintf(buf, "%s (Build %d)",
						osvi.szCSDVersion,
						osvi.dwBuildNumber & 0xFFFF);
					version += buf;
				}

				RegCloseKey( hKey );
			}
			else // not Windows NT 4.0 
			{
				char buf[128];
				sprintf(buf, "%s (Build %d)",
					osvi.szCSDVersion,
					osvi.dwBuildNumber & 0xFFFF);
				version += buf;
			}
			break;

		// Test for the Windows Me/98/95.
		case VER_PLATFORM_WIN32_WINDOWS:

			if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0)
			{
				version += "Microsoft Windows 95 ";
				if ( osvi.szCSDVersion[1] == 'C' || osvi.szCSDVersion[1] == 'B' )
					version += "OSR2 ";
			} 

			if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 10)
			{
				version += "Microsoft Windows 98 ";
				if ( osvi.szCSDVersion[1] == 'A' )
					version += "SE ";
			} 

			if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 90)
			{
				version += "Microsoft Windows Millennium Edition";
			} 
			break;

		case VER_PLATFORM_WIN32s:

			version += "Microsoft Win32s";
			break;
		}
	}
	else
	{
		//
	}

	return (LPCTSTR)version;
}


