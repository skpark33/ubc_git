// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// KIASocketProxy.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


CString GetErrorMessageString(DWORD dwErrCode)
{
	switch(dwErrCode)
	{
	case WSANOTINITIALISED:	return _T("WSANOTINITIALISED");	break;
	case WSAENETDOWN:		return _T("WSAENETDOWN");		break;
	case WSAEADDRINUSE:		return _T("WSAEADDRINUSE");		break;
	case WSAEINPROGRESS:	return _T("WSAEINPROGRESS");	break;
	case WSAEINVAL:			return _T("WSAEINVAL");			break;
	case WSAEISCONN:		return _T("WSAEISCONN");		break;
	case WSAEMFILE:			return _T("WSAEMFILE");			break;
	case WSAENOBUFS:		return _T("WSAENOBUFS");		break;
	case WSAENOTSOCK:		return _T("WSAENOTSOCK");		break;
	case WSAEOPNOTSUPP:		return _T("WSAEOPNOTSUPP");		break;
	case WSAEFAULT:			return _T("WSAEFAULT");			break;
	case WSAEWOULDBLOCK:	return _T("WSAEWOULDBLOCK");	break;
	case WSAEACCES:			return _T("WSAEACCES");			break;
	case WSAENETRESET:		return _T("WSAENETRESET");		break;
	case WSAENOTCONN:		return _T("WSAENOTCONN");		break;
	case WSAESHUTDOWN:		return _T("WSAESHUTDOWN");		break;
	case WSAEMSGSIZE:		return _T("WSAEMSGSIZE");		break;
	case WSAECONNABORTED:	return _T("WSAECONNABORTED");	break;
	case WSAECONNRESET:		return _T("WSAECONNRESET");		break;
	case WSAEPROTONOSUPPORT:return _T("WSAEPROTONOSUPPORT");break;
	case WSAEPROTOTYPE:		return _T("WSAEPROTOTYPE");		break;
	case WSAESOCKTNOSUPPORT:return _T("WSAESOCKTNOSUPPORT");break;

	case 0:					return _T("NoERROR");			break;
	default:
		{
			CString err_str;
			err_str.Format("ErrCode(%u)", dwErrCode);
			return err_str;
			break;
		}
	}
}

CString ByteArrayToString(LPBYTE pData, int len)
{
	if(pData == NULL || len == 0)
		return "No Data";

	CString ret_val = "";
	CString str_tmp;

	for(int i=0; i<len; i++)
	{
		BYTE byte = pData[i];
		if( 32 <= byte || byte < 127 )
			ret_val.AppendChar((char)byte);
		else
			ret_val += " ";

		str_tmp.Format("(%02X) ", byte);
		ret_val += str_tmp;
	}

	return ret_val;
}
