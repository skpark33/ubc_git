// ManualClickerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ManualClicker.h"
#include "ManualClickerDlg.h"


#include <tlhelp32.h>
#include <string>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CManualClickerDlg 대화 상자




CManualClickerDlg::CManualClickerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CManualClickerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CManualClickerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CManualClickerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CManualClickerDlg 메시지 처리기

BOOL CManualClickerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	SetTimer(1025, 1000, NULL);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CManualClickerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CManualClickerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CManualClickerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}




unsigned long getPid(const char* exename, bool likeCond/*=false*/)
{
	//myDEBUG("getPid(%s)", exename);

	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) {
		//myERROR("HANDLE을 생성할 수 없습니다");
		return 0;
	}
	PROCESSENTRY32 pe32 ;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	char strProcessName[512];

	if ( !Process32First ( hSnapshot, &pe32 ) )	{
		//myERROR("Process32First failed.");
		::CloseHandle(hSnapshot);
		return 0;
	}
	

	do 	{
		//memset(strProcessName, 0, sizeof(strProcessName));
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if(likeCond)
		{
			string exename1 = pe32.szExeFile;
			string exename2 = exename;
			::strlwr((char*)exename1.c_str());
			::strlwr((char*)exename2.c_str());

			if( strstr(exename1.c_str(), exename2.c_str()) != NULL ) {
				//myDEBUG("process founded(%s)",exename);
				::CloseHandle(hSnapshot);
				return pe32.th32ProcessID;
			}
		}
		else if( stricmp(pe32.szExeFile, exename) == 0 ) {
			//myDEBUG("process founded(%s)",exename);
			::CloseHandle(hSnapshot);
			return pe32.th32ProcessID;
		}
		
	} while (Process32Next( hSnapshot, &pe32 ));
	//myDEBUG("process not founded(%s)", exename);
	::CloseHandle(hSnapshot);

	return 0;
}

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if (!IsWindowVisible(hwnd)) return TRUE;
	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);
	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;
	if (pe->pid != pid) return TRUE;
	pe->hwnd = hwnd;
	return FALSE;
}


HWND getWHandle(unsigned long pid)
{
	//myDEBUG("getWHandle(%ld)", pid);
	if(pid>0){
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);
		
		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1){
			parent =  GetParent(child);
			if(!parent){
				return child;
			}
			child = parent;
		}
		return e.hwnd;
	}
	return (HWND)0;
}

HWND getWHandle(const char* exename, bool likeCond/*=false*/)
{
	//myDEBUG("getWHandle(%s)", exename);
	return getWHandle(getPid(exename, likeCond));
}

void SetForegroundWindowForce(HWND hWnd)
{
	HWND hForeground = ::GetForegroundWindow();
	if (hForeground == hWnd) return;

	DWORD foreground_id = ::GetWindowThreadProcessId(hForeground, NULL);
	DWORD id = ::GetWindowThreadProcessId(hWnd, NULL);
	if (AttachThreadInput(id, foreground_id, TRUE))
	{
		::SetForegroundWindow(hWnd);
		::BringWindowToTop(hWnd);
		::AttachThreadInput(id, foreground_id, FALSE); 
	}
}

int killProcess(unsigned long pid) 
{
	//myDEBUG("killProcess(%d)", pid);

	if(pid<=0) return -1;

	//printf(("killProcess : pid(%ld)",pid));
	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ((int)hSnapshot == -1) {
		//ciWARN( ("killProcess : fail to CreateToolhelp32Snapshot") );
		return 0;
	}
	//ciDEBUG(("killProcess : success to CreateToolhelp32Snapshot") );

	PROCESSENTRY32 pe32 ;
	pe32.dwSize=sizeof(PROCESSENTRY32);
	BOOL bContinue ;

	if (!Process32First(hSnapshot, &pe32)) {
		//ciWARN( ("killProcess : fail to killProcess") );
		return 0;
	}

	do {
		if( pe32.th32ProcessID == pid ) {
			HANDLE hProcess = OpenProcess( PROCESS_ALL_ACCESS, 0, pe32.th32ProcessID );
			if( hProcess ) {
				DWORD   dwExitCode;
				GetExitCodeProcess( hProcess, &dwExitCode );
				::TerminateProcess( hProcess, dwExitCode );
				//SafeTerminateProcess( hProcess, dwExitCode );
				CloseHandle( hProcess );
				CloseHandle( hSnapshot );
				//ciDEBUG(1,("killProcess : success to Closehandle") );
				return 1;
			}
		}
		bContinue = Process32Next ( hSnapshot, &pe32 );
	} while ( bContinue );
	CloseHandle( hSnapshot );
	//ciERROR(("killProcess : Process is not found.") );
	return -1;
}

void MouseCLick(int x, int y)
{
	CPoint pos(x, y);
	//CPoint pos(40,240);

	int x_pos = pos.x * 65535 / GetSystemMetrics(SM_CXSCREEN); 
	int y_pos = pos.y * 65535 / GetSystemMetrics(SM_CYSCREEN);

	//BOOL ret = ::SetCursorPos(40, 240);//::SetCursorPos(282, 1000);
	//::PostMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(pos.x,pos.y) );
	//::PostMessage(hwnd, WM_LBUTTONUP  , MK_LBUTTON, MAKELPARAM(pos.x,pos.y) );
	::mouse_event(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE, x_pos, y_pos, 0, ::GetMessageExtraInfo());
	::mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_ABSOLUTE, x_pos, y_pos, 0, ::GetMessageExtraInfo());
	::mouse_event(MOUSEEVENTF_LEFTUP | MOUSEEVENTF_ABSOLUTE, x_pos, y_pos, 0, ::GetMessageExtraInfo());
}

void MouseDrag(int x1, int y1, int x2, int y2)
{
	CPoint pos1(x1, y1);
	CPoint pos2(x2, y2);
	//CPoint pos(40,240);

	int x_pos1 = pos1.x * 65535 / GetSystemMetrics(SM_CXSCREEN); 
	int y_pos1 = pos1.y * 65535 / GetSystemMetrics(SM_CYSCREEN);

	int x_pos2 = pos2.x * 65535 / GetSystemMetrics(SM_CXSCREEN); 
	int y_pos2 = pos2.y * 65535 / GetSystemMetrics(SM_CYSCREEN);

	//BOOL ret = ::SetCursorPos(40, 240);//::SetCursorPos(282, 1000);
	//::PostMessage(hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(pos.x,pos.y) );
	//::PostMessage(hwnd, WM_LBUTTONUP  , MK_LBUTTON, MAKELPARAM(pos.x,pos.y) );
	::mouse_event(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE, x_pos1, y_pos1, 0, ::GetMessageExtraInfo());
	::mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_ABSOLUTE, x_pos1, y_pos1, 0, ::GetMessageExtraInfo());
	::mouse_event(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE, x_pos2, y_pos2, 0, ::GetMessageExtraInfo());
	::mouse_event(MOUSEEVENTF_LEFTUP | MOUSEEVENTF_ABSOLUTE, x_pos2, y_pos2, 0, ::GetMessageExtraInfo());
}

void CManualClickerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	if(nIDEvent == 1025)
	{
		KillTimer(1025);

		HWND hwnd = getWHandle("LogiDPPApp.exe", false);
		//HWND hwnd = getWHandle("calc.exe", false);

		if(hwnd)
		{
			SetForegroundWindowForce(hwnd);

			MouseCLick(282, 1000);
			::Sleep(100);

			//for(int i=0; i<11; i++)
			{
				MouseCLick(275, 1040);
				::Sleep(100);
			}
			keybd_event(VK_HOME, 0, 0, 0);
			keybd_event(VK_HOME, 0, KEYEVENTF_KEYUP, 0);

			MouseDrag(280, 1040, 353, 1040);
			::Sleep(100);

			MouseCLick(654, 1226);

			//SetTimer(1026, 1000, NULL);
		}
		else
		{
			//::AfxMessageBox("no process");
		}
	}
	else if(nIDEvent == 1026)
	{
		KillTimer(1026);
		killProcess(::getPid("LogiDPPApp.exe", false));
	}

	CDialog::OnTimer(nIDEvent);
}
