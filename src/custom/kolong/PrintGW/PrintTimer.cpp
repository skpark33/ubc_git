#include "stdafx.h"
#include "PrintTimer.h"
#include "ci/libFile/ciDirHandler.h"
#include "common/libHttpRequest/HttpRequest.h"
#include "common/libCommon/ubcIni.h"
#include <GdiPlus.h>
#pragma comment(lib, "gdiplus.lib")
#include <WinSpool.h>
#pragma comment(lib, "winspool.lib")

ciSET_DEBUG(10, "PrintTimer");

void
printData::print()
{
	ciDEBUG(1,("%d,%s,%s,%d,%d,%d",
		zorder,
		printId.c_str(),
		uccId.c_str(),
		volume,
		alreadyDownload,
		printResult
		));
}

PrintTimer::PrintTimer(ciString& site, ciString& host)
{
	ciDEBUG(3, ("PrintTimer(%s, %s)", site.c_str(), host.c_str()));

	_site = site;
	_host = host;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR cFilename[MAX_PATH], cExt[MAX_PATH];
	_tsplitpath(szModule, _cDrive, _cPath, cFilename, cExt);

	ubcConfig aIni("UBCConnect");

	aIni.get("IOT","ServerApi",_serverApi);
	if(_serverApi.empty()){
		_serverApi = "ofs-kolon.sqisoft.com";
	}

	_serverApiPort = 0;
	aIni.get("IOT","ServerApiPort",_serverApiPort);
	if(_serverApiPort == 0) _serverApiPort = 8000;

	aIni.get("IOT","SiteId",_siteId);
	if(_siteId.empty()){
		_siteId = "SE6255";
	}

	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&_gdiplusToken, &gdiplusStartupInput, NULL);
}

PrintTimer::~PrintTimer()
{
	ciDEBUG(3, ("~PrintTimer()"));

	Gdiplus::GdiplusShutdown(_gdiplusToken);
}

void
PrintTimer::initTimer(const char* pname, int interval) {
	ciDEBUG(3, ("initTimer(%s,%d)", pname, interval));
	_timer = new ciTimer(pname, this);
	_timer->setInterval(interval);
}

void
PrintTimer::processExpired(ciString name, int counter, int interval)
{
	ciDEBUG(3, ("processExpired(%s,%d,%d)", name.c_str(), counter, interval));

	if (counter <= 1) {
		// 첫회는 건너뛴다.
		return;
	}

	CStringArray line_list;

	CString printReadyUrl;
	printReadyUrl.Format("/api/photoPrint/Api033List.do?siteId=%s", _siteId);
	if(_getPrint(printReadyUrl, line_list)){
		if(!_setPrint(line_list)) {
			ciWARN(("No Print changed"));
		}
	}

	if(!_downloadPrint()) {
		ciWARN(("No downloadPrint count"));
	}

	if(!_printAction()){
		ciWARN(("No printAction count"));
	}

	if (counter % 5 == 0) {
		// 5회 반복마다 실행한다.
		if(!_removePrint()){
			ciWARN(("No removePrint count"));
		}

		// 메모리에 있는 MAP을 삭제한다.
		clearMap();
	}
}

void
PrintTimer::clearMap()
{
	ciDEBUG(1,("clearMap()"));
	ciGuard aGuard(_mapLock);

	PrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		delete (itr->second);
	}
	_map.clear();
}

void
PrintTimer::printMap()
{
	ciDEBUG(1,("printMap()"));
	ciGuard aGuard(_mapLock);
	PrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		itr->second->print();
	}
}

void
PrintTimer::_makePath(const char* relpath, ciString& outval)
{
	ciString strPath;
	strPath += _cDrive;
	strPath += _cPath;
	strPath += "..\\..\\";
	strPath += relpath;

	CFileStatus status;
	CFile::GetStatus(strPath.c_str(), status);
	outval = status.m_szFullName;
}

ciBoolean
PrintTimer::_getPrint(const char* url, CStringArray& line_list)
{
	ciDEBUG(3, ("_getPrint(%s)", url));

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _serverApi.c_str(), _serverApiPort);
	BOOL ret_val = http_request.RequestGet(url,line_list);
	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	}

	ciBoolean getSucceed = ciFalse;
	if(line_list.GetCount() > 1 )
	{
		CString first_line =  line_list.GetAt(0);
		if (first_line.GetLength() >= 2 && first_line.Mid(0,2).CompareNoCase("OK") == 0) {
			getSucceed = ciTrue;
		} else if(first_line.GetLength() >= 4 && first_line.Mid(0,4).CompareNoCase("FAIL") == 0) {
			ciWARN(("No printList"));
		}
	}

	return getSucceed;
}

int
PrintTimer::_setPrint(CStringArray& line_list)
{
	ciDEBUG(3, ("_setPrint()"));

	int retval = 0;
	printData* aData = NULL;

	int count = line_list.GetCount();
	for(int i=0; i<count; i++)
	{
		ciString line = line_list.GetAt(i);

		ciString key, value;
		ciStringUtil::divide(line,'=',&key,&value);

		if(key == "zorder") {
			ciGuard aGuard(_mapLock);
			PrintMAP::iterator itr =_map.find(atoi(value.c_str()));
			if(itr != _map.end()){
				aData = itr->second;
				ciDEBUG(1,("%d data founded", aData->zorder));
				retval++;
			}else{
				aData = new printData();
				aData->zorder = atoi(value.c_str());
				_map.insert(PrintMAP::value_type(aData->zorder, aData));
				ciDEBUG(1,("%d data inserted", aData->zorder));
			}
			continue;
		}
		if(aData==0){
			continue;
		}

		if(key == "printId")
		{
			ciDEBUG(1,("printId=%s", value.c_str()));
			aData->printId = value;
		}
		else if(key == "uccId")
		{
			ciDEBUG(1,("uccId=%s", value.c_str()));
			aData->uccId = value;
		}
		else if(key == "siteId")
		{
			ciDEBUG(1,("siteId=%s", value.c_str()));
			aData->siteId = value;
		}
		else if(key == "volume")
		{
			unsigned long buf = strtoul(value.c_str(),NULL,10);
			if(aData->volume != buf) {
				ciDEBUG(1,("chanaged"));
				aData->volume = buf;
				aData->alreadyDownload = ciFalse;
				aData->printResult = ciFalse;
				retval++;
			}
		}
	}

	printMap();
	return retval;
}

int
PrintTimer::_downloadPrint()
{
	ciDEBUG(3, ("_downloadPrint()"));

	int counter=0;
	PrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		printData* aData = itr->second;
		aData->print();
		if(!aData->printId.empty() && aData->alreadyDownload == ciFalse) {
			ciString fullpath;
			_makePath("Contents\\Enc\\print", fullpath);
			ciDirHandler::create(fullpath);
			fullpath += "\\";
			fullpath += aData->printId;

			CString printReadyUrl;
			printReadyUrl.Format("http://%s:%d/api/api015/UccDownURL/%s/", _serverApi.c_str(), _serverApiPort, aData->siteId.c_str(), aData->uccId.c_str());
			DeleteUrlCacheEntry(printReadyUrl);
			HRESULT hr = URLDownloadToFile(NULL, printReadyUrl, fullpath.c_str(), 0, NULL);
			if (SUCCEEDED(hr)) {
				unsigned long downloadedSize = _getFileSize(aData->printId.c_str());
				aData->volume = downloadedSize;
				if (downloadedSize > 0) {
					ciDEBUG(1,("print/%s file download succeed", aData->printId.c_str()));
					aData->alreadyDownload = ciTrue;
				} else {
					ciERROR(("print/%s file download failed downloadedSize(%ld)"
						, aData->printId.c_str(), downloadedSize));
					ciString printResult = "FAIL";
					_printUpdate(aData->printId, printResult);
					aData->alreadyDownload = ciTrue;
					aData->printResult = ciTrue;
				}

				counter++;
			} else {
				ciERROR(("print/%s file download failed", aData->printId.c_str()));
			}
		}
	}
	ciDEBUG(1,("%d data downloaded", counter));
	return counter;
}

int
PrintTimer::_removePrint()
{
	ciDEBUG(3, ("_removePrint()"));

	int counter=0;

	ciString printDir, filename;
	_makePath("Contents\\Enc\\print", printDir);

	counter = _removePrint(counter, printDir, filename);

	ciDEBUG(1,("%d file removed", counter));
	return counter;
}

int
PrintTimer::_removePrint(int counter, ciString filepath, ciString filename)
{
	string dirPath = filepath;
	if (filename.empty() == false) {
		dirPath += "\\";
		dirPath += filename;
	}
	dirPath += "\\";
	dirPath += "*.*";

	WIN32_FIND_DATA FileData;
	HANDLE hFile = FindFirstFile(dirPath.c_str(), &FileData);
	if (hFile == INVALID_HANDLE_VALUE) {
		ciERROR(("contents file not found"));
		return 0;
	}

	do {
		if (FileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			if (FileData.cFileName[0] != '.') {
				ciString filePath = filename;
				if (filename.empty() == false) {
					filePath += "\\";
				}
				filePath += FileData.cFileName;

				counter = _removePrint(counter, filepath, filePath.c_str());
			}
			continue;
		}

		ciString filePath = filepath;
		if (filename.empty() == false) {
			filePath += "\\";
			filePath += filename;
		}
		filePath += "\\";
		filePath += FileData.cFileName;
		ciDEBUG(5,("%s file founded", filePath.c_str()));

		ciString printId = FileData.cFileName;
		if (_isPrint(printId) == ciFalse) {
			if(::access(filePath.c_str(),0) == 0) {
				::remove(filePath.c_str());
				ciDEBUG(5,("%s file deleted", filePath.c_str()));
			}

			counter++;
		}

	} while (FindNextFile(hFile, &FileData));
	FindClose(hFile);

	return counter;
}

int
PrintTimer::_printAction()
{
	ciDEBUG(3, ("_printAction()"));

	int counter=0;

	ciString printDir, filename;
	_makePath("Contents\\Enc\\print", printDir);

	PrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		printData* aData = itr->second;
		if(!aData->printId.empty() && _isExist(aData->printId) == ciTrue && aData->alreadyDownload == ciTrue && aData->printResult == ciFalse) {
			ciString filePath = printDir;
			filePath += "\\";
			filePath += aData->printId;

			ciString printResult;
			if (_printAction(filePath) == ciTrue) {
				printResult = "DONE";
			} else {
				printResult = "FAIL";
			}

			_printUpdate(aData->printId, printResult);

			aData->printResult = ciTrue;

			counter++;
		}
	}

	return counter;
}

ciBoolean
PrintTimer::_printAction(ciString& filename)
{
	ciDEBUG(3, ("_printAction(%s)", filename.c_str()));

	CPrintDialog printDlg(FALSE);
	printDlg.GetDefaults();
	printDlg.m_pd.Flags &= ~PD_RETURNDEFAULT;

	ciString printName = printDlg.GetDeviceName();
	if (_isPrinterOnline(printName) == false) {
		ciERROR(("printer(%s) status fail", printName.c_str()));
		return ciFalse;
	}

	BSTR bstrFilePath;
	LPCTSTR_to_BSTR(&bstrFilePath, filename.c_str());

	Gdiplus::Bitmap *pPrintImage = Gdiplus::Bitmap::FromFile(bstrFilePath);

	LPDEVMODE dev = printDlg.GetDevMode();
	GlobalUnlock(dev);
	dev->dmOrientation=DMORIENT_PORTRAIT;
	dev->dmPaperSize=DMPAPER_JAPANESE_POSTCARD;

	CDC dc;
	if (!dc.Attach(printDlg.GetPrinterDC()))
	{
		ciERROR(("dc attach failed"));
		return ciFalse;
	}
	dc.ResetDC(dev);
	dc.m_bPrinting = TRUE;

	ciShort paperHorz = 0;
	ciShort paperVert = 0;
	ubcConfig aIni("UBCVariables");
	aIni.get("PRINT", "PaperHorz", paperHorz);
	if (paperHorz == 0) paperHorz = 89;
	aIni.get("PRINT", "PaperVert", paperVert);
	if (paperVert == 0) paperVert = 119;

	double f64MaxwInPixels = pPrintImage->GetWidth();
	double f64MaxhInPixels = pPrintImage->GetHeight();
	//double f64MaxwInMillimeters = dc.GetDeviceCaps(HORZSIZE);
	//double f64MaxhInMillimeters = dc.GetDeviceCaps(VERTSIZE);
	double f64MaxwInMillimeters = paperHorz;
	double f64MaxhInMillimeters = paperVert;
	double f64PixelRate = f64MaxhInPixels / f64MaxhInMillimeters;
	double f64DPI = 100;
	double f64OneMM2Pixel_width = f64MaxwInMillimeters / 25.4;
	double f64OneMM2Pixel_height = f64MaxhInMillimeters / 25.4;
	double f64OneMM2Pixel_width_rate = (f64MaxwInPixels / f64PixelRate) / 25.4;
	int left = static_cast<int>(((f64OneMM2Pixel_width - f64OneMM2Pixel_width_rate)/2)*f64DPI);
	int maxw = static_cast<int>(f64OneMM2Pixel_width_rate*f64DPI);
	int maxh = static_cast<int>(f64OneMM2Pixel_height*f64DPI);

	DOCINFO di;
	::ZeroMemory (&di, sizeof (DOCINFO));
	di.cbSize = sizeof(DOCINFO);
	di.lpszDocName = "PrintGW";

	int startResult = dc.StartDoc(&di);

	CPrintInfo Info;
	Info.SetMaxPage(1);
	Info.m_nCurPage = 1;
	Info.m_rectDraw.SetRect(0, 0, static_cast<int>(f64MaxwInPixels), static_cast<int>(f64MaxhInPixels));

	dc.StartPage(); // begin new page

	Gdiplus::Graphics graphics(dc);
	Gdiplus::Rect paperRect(left, 0, maxw, maxh);
	graphics.DrawImage(pPrintImage, paperRect);

	int endResult = dc.EndPage(); // end page

	if (endResult)
		dc.EndDoc(); // end a print job
	else
		dc.AbortDoc();  // abort job.

	dc.Detach();
	delete pPrintImage;

	return ciTrue;
}

ciBoolean
PrintTimer::_printUpdate(ciString& printId, ciString& printResult)
{
	ciDEBUG(3, ("_printUpdate(%s,%s)", printId.c_str(), printResult.c_str()));

	CString strHttpResult = "";
	CString strResultUrl = "", strPost = "";
	strResultUrl.Format("/api/photoPrint/Api033Update.do");
	strPost.Format("printId=%s&printResult=%s", printId.c_str(), printResult.c_str());

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_USER_DEFINED, _serverApi.c_str(), _serverApiPort);
	BOOL ret_val = http_request.RequestPost(strResultUrl, strPost, strHttpResult);
	if(!ret_val) {
		ciERROR((http_request.GetErrorMsg()));
		return ciFalse;
	} else {
		ciDEBUG(5,("%s result send", strResultUrl));
	}

	return ciTrue;
}

ciBoolean
PrintTimer::_isPrinterOnline(ciString& printerName)
{
	ciDEBUG(3, ("_isPrinterOnline(%s)", printerName.c_str()));

	bool ret = true;

	if (printerName.find("XPS") != -1 || printerName.find("PDF") != -1)
	{
		ciERROR(("File Print not support"));
		return false;
	}

	HANDLE hPrinter;
	if (OpenPrinter((LPSTR)printerName.c_str(), &hPrinter, NULL) == 0)
	{
		ciERROR(("OpenPrinter failed"));
		return false;
	}

	DWORD dwBufsize = 0;
	PRINTER_INFO_2* pinfo = 0;
	int nRet = 0;
	nRet = GetPrinter(hPrinter, 2,(LPBYTE)pinfo, dwBufsize, &dwBufsize); //Get dwBufsize
	DWORD dwGetPrinter = 0;
	if (nRet == 0)
	{
		dwGetPrinter = GetLastError();
	}

	PRINTER_INFO_2* pinfo2 = (PRINTER_INFO_2*)malloc(dwBufsize); //Allocate with dwBufsize
	nRet = GetPrinter(hPrinter, 2, reinterpret_cast<LPBYTE>(pinfo2), dwBufsize, &dwBufsize);
	if (nRet == 0)
	{
		ciERROR(("GetPrinter failed"));
		dwGetPrinter = GetLastError();
		ret = false;
	}
	else if (pinfo2->Attributes & PRINTER_ATTRIBUTE_WORK_OFFLINE)
	{
		ciERROR(("GetPrinter Attributes(%d) : PRINTER_ATTRIBUTE_WORK_OFFLINE", pinfo2->Attributes));
		ret = false;
	}
	else if (pinfo2->Status & PRINTER_STATUS_OFFLINE)
	{
		ciERROR(("GetPrinter Status(%d) : PRINTER_STATUS_OFFLINE", pinfo2->Status));
		ret = false;
	}
	else if (pinfo2->Status & PRINTER_STATUS_NO_TONER)
	{
		ciERROR(("GetPrinter Status(%d) : PRINTER_STATUS_NO_TONER", pinfo2->Status));
		ret = false;
	}
	else if (pinfo2->Status & PRINTER_STATUS_PAPER_OUT)
	{
		ciERROR(("GetPrinter Status(%d) : PRINTER_STATUS_PAPER_OUT", pinfo2->Status));
		ret = false;
	}
	else if (pinfo2->Status & PRINTER_STATUS_PAPER_PROBLEM)
	{
		ciERROR(("GetPrinter Status(%d) : PRINTER_STATUS_PAPER_PROBLEM", pinfo2->Status));
		ret = false;
	}

	if (ret == false) {
		if (pinfo2->cJobs > 0) {
			ciWARN(("Remove Print Jobs(%d)", pinfo2->cJobs));

			// 대기열 삭제
			SetPrinter(hPrinter, 0, 0, PRINTER_CONTROL_PURGE);
		}
	}

	free(pinfo2);
	ClosePrinter( hPrinter );
	return ret;
}

ciBoolean
PrintTimer::_isPrint(ciString& printId)
{
	PrintMAP::iterator itr;
	for(itr=_map.begin();itr!=_map.end();itr++){
		printData* aData = itr->second;
		if (aData->printId == printId) {
			ciDEBUG(1,("printId %s exist", printId.c_str()));
			return ciTrue;
		}
	}
	ciDEBUG(1,("printId %s not exist", printId.c_str()));
	return ciFalse;
}

ciBoolean
PrintTimer::_isExist(ciString& printId)
{
	ciString source;
	_makePath("Contents\\Enc\\print", source);
	source += "\\";
	source += printId.c_str();
	if(::access(source.c_str(),0) == 0) {
		ciDEBUG(1,("file %s exist", source.c_str()));
		return ciTrue;
	}
	ciDEBUG(1,("file %s not exist", source.c_str()));
	return ciFalse;
}

unsigned long
PrintTimer::_getFileSize(const char* filename)
{
	unsigned long retval = 0;

	ciString fullpath;
	_makePath("Contents\\Enc\\print", fullpath);
	fullpath += "\\";
	fullpath += filename;

	ciDEBUG(1,("stat(%s)", fullpath.c_str()));
	int fdes = open(fullpath.c_str(),O_RDONLY);
	if(fdes<0){
		ciWARN(("%s file open failed", fullpath.c_str()));
		return retval;
	}
	struct stat statBuf;
	if(fstat(fdes, &statBuf)==0){
		retval = statBuf.st_size;
	}
	ciDEBUG(1,("filesize(%s)=%ld", fullpath.c_str(), retval));
	close(fdes);
	return retval;
}

HRESULT
PrintTimer::LPCTSTR_to_BSTR(BSTR *pbstr, LPCTSTR psz)
{
#ifndef UNICODE
	BSTR bstr;
	int i;
	HRESULT hr;
	// compute the length of the required BSTR
	//
	i =  MultiByteToWideChar(CP_ACP, 0, psz, -1, NULL, 0);
	if (i <= 0) { return E_UNEXPECTED; };
	// allocate the widestr, +1 for terminating null
	//
	bstr = SysAllocStringLen(NULL, i-1);
	// SysAllocStringLen adds 1
	if (bstr != NULL)
	{
		MultiByteToWideChar(CP_ACP, 0, psz, -1, (LPWSTR)bstr, i);
		((LPWSTR)bstr)[i - 1] = 0;
		*pbstr = bstr; hr = S_OK;
	}
	else
	{
		hr = E_OUTOFMEMORY;
	};
	return hr;
#else
	BSTR bstr;
	bstr = SysAllocString(psz);
	if (bstr != NULL)
	{
		*pbstr = bstr;
		return S_OK;
	}
	else
	{
		return E_OUTOFMEMORY;
	};
#endif
	// UNICODE
}
