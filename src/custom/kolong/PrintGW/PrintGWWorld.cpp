#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libConfig/ciXProperties.h>

#include "PrintGWWorld.h"

ciSET_DEBUG(10, "PrintGWWorld");

PrintGWWorld::PrintGWWorld()
{
	ciDEBUG(3, ("PrintGWWorld()"));
	_printThread = 0;
}

PrintGWWorld::~PrintGWWorld()
{
	ciDEBUG(3, ("~PrintGWWorld()"));
	if(_printThread){
		_printThread->stop();
		delete _printThread;
		_printThread = 0;
	}
}

ciBoolean
PrintGWWorld::init(int& p_argc, char** &p_argv)
{
	ciDEBUG(5, ("PrintGWWorld::init(%d)", p_argc));

	if(ciWorld::init(p_argc, p_argv) == ciFalse) {
		ciERROR(("ciWorld::init() is FAILED"));
		return ciFalse;
	}

	accept();

	return ciTrue;
}

ciBoolean
PrintGWWorld::fini(long p_finiCode)
{
	ciDEBUG(1,("fini()"));
	if(_printThread){
		_printThread->stop();
		delete _printThread;
		_printThread = 0;
	}

	return ciWorld::fini(p_finiCode);
}

ciBoolean
PrintGWWorld::accept()
{
	ciDEBUG(1,("PrintThread() before start"));

	_printThread = new PrintThread();
	_printThread->start();

	ciDEBUG(1,("PrintThread() after start"));

	return ciTrue;
}
