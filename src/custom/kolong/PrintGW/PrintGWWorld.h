#ifndef _PrintGWWorld_h_
#define _PrintGWWorld_h_

#include <ci/libWorld/ciWorld.h>
#include <ci/libThread/ciMutex.h>
#include <ci/libBase/ciListType.h>
#include "PrintThread.h"

class PrintGWWorld : public ciWorld {
public:
	PrintGWWorld();
	~PrintGWWorld();

	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

	ciBoolean accept();

protected:
	PrintThread*	_printThread;
};

#endif _PrintGWWorld_h_
