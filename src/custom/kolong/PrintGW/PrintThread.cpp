#include "PrintThread.h"

#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libBase/ciStringTokenizer.h>
#include "common/libScratch/scratchUtil.h"
#include "common/libInstall/installUtil.h"
#include "common/libCommon/ubcIni.h"

ciSET_DEBUG(10, "PrintThread");

PrintThread::PrintThread()
{
	ciDEBUG(3, ("PrintThread()"));
}

PrintThread::~PrintThread()
{
	ciDEBUG(3, ("~PrintThread()"));
}

void
PrintThread::run()
{
	ciDEBUG(1, ("run()"));

	ciString mac,edition,hostId;
	if(!scratchUtil::getInstance()->readAuthFile(hostId, mac, edition)){
		edition = STANDARD_EDITION;
	}

	ciString siteId;
	if(edition == ENTERPRISE_EDITION){
		installUtil::getInstance()->getSiteId(siteId);
	}

	_printTimer = new PrintTimer(siteId, hostId);
	_printTimer->initTimer("printTimer", 10);
	_printTimer->startTimer();
}
