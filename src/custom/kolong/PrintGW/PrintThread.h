#ifndef _PrintThread_h_
#define _PrintThread_h_

#include <ci/libThread/ciThread.h>
#include "PrintTimer.h"

class PrintThread : public ciThread {
public:
	PrintThread();
	virtual ~PrintThread();

	virtual void run();

protected:
	PrintTimer* _printTimer;
};

#endif _PrintThread_h_
