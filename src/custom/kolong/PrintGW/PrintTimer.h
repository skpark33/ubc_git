#ifndef _PrintTimer_h_
#define _PrintTimer_h_

#include <ci/libTimer/ciTimer.h>
#include <hi/libHttp/ubcMux.h>

class CStringArray;

class printData {
public:
	printData() : zorder(0), volume(0), alreadyDownload(ciFalse), printResult(ciFalse) {}
	ciShort			zorder;
	ciString		printId;
	ciString		uccId;
	ciString		siteId;
	ciULong			volume;
	ciBoolean		alreadyDownload;
	ciBoolean		printResult;

	void print();
};
typedef map<int, printData*> PrintMAP;

class PrintTimer : public ciPollable {
public:
	PrintTimer(ciString& site, ciString& host);
	virtual ~PrintTimer();

	virtual void initTimer(const char* pname, int interval = 1);
	virtual void processExpired(ciString name, int counter, int interval);

	void		clearMap();
	void		printMap();

protected:
	void		_makePath(const char* relpath, ciString& outval);
	ciBoolean	_getPrint(const char* url, CStringArray& line_list);
	int			_setPrint(CStringArray& line_list);
	int			_downloadPrint();
	int			_removePrint();
	int			_removePrint(int counter, ciString filepath, ciString filename);
	int			_printAction();
	ciBoolean	_printAction(ciString& printId);
	ciBoolean	_printUpdate(ciString& printId, ciString& printResult);
	ciBoolean	_isPrinterOnline(ciString& printerName);
	ciBoolean	_isPrint(ciString& printId);
	ciBoolean	_isExist(ciString& printId);
	unsigned long	_getFileSize(const char* filename);
	HRESULT		LPCTSTR_to_BSTR(BSTR *pbstr, LPCTSTR psz);

	TCHAR		_cDrive[MAX_PATH];
	TCHAR		_cPath[MAX_PATH];

	ciMutex		_mapLock;
	ciString	_site;
	ciString	_host;

	ciString	_serverApi;
	ciULong		_serverApiPort;
	ciString	_siteId;

	PrintMAP	_map;

	ULONG_PTR	_gdiplusToken;
};

#endif _PrintTimer_h_
