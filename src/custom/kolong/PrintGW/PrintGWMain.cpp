#include "PrintGWWorld.h"
#include "ci/libDebug/ciArgParser.h"

LONG WINAPI NoMsgExceptionFilter(struct _EXCEPTION_POINTERS* /*ExceptionInfo*/)
{
	// 아무일도 하지 않고 그냥 종료하기
	return EXCEPTION_EXECUTE_HANDLER;
}

void _myPurecallHandler(void)
{
	printf("R0625 - pure virtual function call error 를 안나게 한다.");
	ciWorld* world = ciWorld::getWorld();
	if(world){
		world->fini(0);
	}
}

int main(int argc, char** argv)
{
	::SetUnhandledExceptionFilter(NoMsgExceptionFilter);

	PrintGWWorld* world = new PrintGWWorld();

	if(!world) {
		cerr<<"Unable to create PrintGWWorld"<<endl;
		exit(1);
	}

	_set_purecall_handler(_myPurecallHandler);

	ciArgParser::initialize(argc, argv);

	if(world->init(argc, argv) == ciFalse) {
		cerr<<"Init world error"<<endl;
		world->fini(2);
	}

	world->run();
	world->fini(0);
	return 0;
}
