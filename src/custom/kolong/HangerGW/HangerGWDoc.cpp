// HangerGWDoc.cpp : CHangerGWDoc 클래스의 구현
//

#include "stdafx.h"
#include "HangerGW.h"

#include "HangerGWDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CHangerGWDoc

IMPLEMENT_DYNCREATE(CHangerGWDoc, CDocument)

BEGIN_MESSAGE_MAP(CHangerGWDoc, CDocument)
END_MESSAGE_MAP()


// CHangerGWDoc 생성/소멸

CHangerGWDoc::CHangerGWDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CHangerGWDoc::~CHangerGWDoc()
{
}

BOOL CHangerGWDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CHangerGWDoc serialization

void CHangerGWDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CHangerGWDoc 진단

#ifdef _DEBUG
void CHangerGWDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CHangerGWDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CHangerGWDoc 명령
