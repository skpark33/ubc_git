#pragma once
#include "afxwin.h"


#define		MAX_BIT_COUNT		(64)	// 시리얼bit수
#define		MAX_ID_COUNT		(48)	// 행거ID개수

#define		MAX_DATA_BODY_LENGTH		((MAX_BIT_COUNT/8)*2) // 8bit => FF => 2byte


// CIDRemappingDlg 대화 상자입니다.

class CIDRemappingDlg : public CDialog
{
	DECLARE_DYNAMIC(CIDRemappingDlg)

public:
	CIDRemappingDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CIDRemappingDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ID_REMAPPING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

public:
	CComboBox	m_cbxSerial[MAX_ID_COUNT];
	CComboBox	m_cbxHanger[MAX_ID_COUNT];

protected:
	void	InitAllSerialComboBox();
	void	InitAllHangerComboBox();

	void	InitSerialComboBox(int nCbxID);

public:
	afx_msg void OnCbnSelchangeComboSerial00();
	afx_msg void OnCbnSelchangeComboSerial01();
	afx_msg void OnCbnSelchangeComboSerial02();
	afx_msg void OnCbnSelchangeComboSerial03();
	afx_msg void OnCbnSelchangeComboSerial04();
	afx_msg void OnCbnSelchangeComboSerial05();
	afx_msg void OnCbnSelchangeComboSerial06();
	afx_msg void OnCbnSelchangeComboSerial07();
	afx_msg void OnCbnSelchangeComboSerial08();
	afx_msg void OnCbnSelchangeComboSerial09();
	afx_msg void OnCbnSelchangeComboSerial10();
	afx_msg void OnCbnSelchangeComboSerial11();
	afx_msg void OnCbnSelchangeComboSerial12();
	afx_msg void OnCbnSelchangeComboSerial13();
	afx_msg void OnCbnSelchangeComboSerial14();
	afx_msg void OnCbnSelchangeComboSerial15();
	afx_msg void OnCbnSelchangeComboSerial16();
	afx_msg void OnCbnSelchangeComboSerial17();
	afx_msg void OnCbnSelchangeComboSerial18();
	afx_msg void OnCbnSelchangeComboSerial19();
	afx_msg void OnCbnSelchangeComboSerial20();
	afx_msg void OnCbnSelchangeComboSerial21();
	afx_msg void OnCbnSelchangeComboSerial22();
	afx_msg void OnCbnSelchangeComboSerial23();
	afx_msg void OnCbnSelchangeComboSerial24();
	afx_msg void OnCbnSelchangeComboSerial25();
	afx_msg void OnCbnSelchangeComboSerial26();
	afx_msg void OnCbnSelchangeComboSerial27();
	afx_msg void OnCbnSelchangeComboSerial28();
	afx_msg void OnCbnSelchangeComboSerial29();
	afx_msg void OnCbnSelchangeComboSerial30();
	afx_msg void OnCbnSelchangeComboSerial31();
	afx_msg void OnCbnSelchangeComboSerial32();
	afx_msg void OnCbnSelchangeComboSerial33();
	afx_msg void OnCbnSelchangeComboSerial34();
	afx_msg void OnCbnSelchangeComboSerial35();
	afx_msg void OnCbnSelchangeComboSerial36();
	afx_msg void OnCbnSelchangeComboSerial37();
	afx_msg void OnCbnSelchangeComboSerial38();
	afx_msg void OnCbnSelchangeComboSerial39();
	afx_msg void OnCbnSelchangeComboSerial40();
	afx_msg void OnCbnSelchangeComboSerial41();
	afx_msg void OnCbnSelchangeComboSerial42();
	afx_msg void OnCbnSelchangeComboSerial43();
	afx_msg void OnCbnSelchangeComboSerial44();
	afx_msg void OnCbnSelchangeComboSerial45();
	afx_msg void OnCbnSelchangeComboSerial46();
	afx_msg void OnCbnSelchangeComboSerial47();

	afx_msg void OnCbnSelchangeComboHanger00();
	afx_msg void OnCbnSelchangeComboHanger01();
	afx_msg void OnCbnSelchangeComboHanger02();
	afx_msg void OnCbnSelchangeComboHanger03();
	afx_msg void OnCbnSelchangeComboHanger04();
	afx_msg void OnCbnSelchangeComboHanger05();
	afx_msg void OnCbnSelchangeComboHanger06();
	afx_msg void OnCbnSelchangeComboHanger07();
	afx_msg void OnCbnSelchangeComboHanger08();
	afx_msg void OnCbnSelchangeComboHanger09();
	afx_msg void OnCbnSelchangeComboHanger10();
	afx_msg void OnCbnSelchangeComboHanger11();
	afx_msg void OnCbnSelchangeComboHanger12();
	afx_msg void OnCbnSelchangeComboHanger13();
	afx_msg void OnCbnSelchangeComboHanger14();
	afx_msg void OnCbnSelchangeComboHanger15();
	afx_msg void OnCbnSelchangeComboHanger16();
	afx_msg void OnCbnSelchangeComboHanger17();
	afx_msg void OnCbnSelchangeComboHanger18();
	afx_msg void OnCbnSelchangeComboHanger19();
	afx_msg void OnCbnSelchangeComboHanger20();
	afx_msg void OnCbnSelchangeComboHanger21();
	afx_msg void OnCbnSelchangeComboHanger22();
	afx_msg void OnCbnSelchangeComboHanger23();
	afx_msg void OnCbnSelchangeComboHanger24();
	afx_msg void OnCbnSelchangeComboHanger25();
	afx_msg void OnCbnSelchangeComboHanger26();
	afx_msg void OnCbnSelchangeComboHanger27();
	afx_msg void OnCbnSelchangeComboHanger28();
	afx_msg void OnCbnSelchangeComboHanger29();
	afx_msg void OnCbnSelchangeComboHanger30();
	afx_msg void OnCbnSelchangeComboHanger31();
	afx_msg void OnCbnSelchangeComboHanger32();
	afx_msg void OnCbnSelchangeComboHanger33();
	afx_msg void OnCbnSelchangeComboHanger34();
	afx_msg void OnCbnSelchangeComboHanger35();
	afx_msg void OnCbnSelchangeComboHanger36();
	afx_msg void OnCbnSelchangeComboHanger37();
	afx_msg void OnCbnSelchangeComboHanger38();
	afx_msg void OnCbnSelchangeComboHanger39();
	afx_msg void OnCbnSelchangeComboHanger40();
	afx_msg void OnCbnSelchangeComboHanger41();
	afx_msg void OnCbnSelchangeComboHanger42();
	afx_msg void OnCbnSelchangeComboHanger43();
	afx_msg void OnCbnSelchangeComboHanger44();
	afx_msg void OnCbnSelchangeComboHanger45();
	afx_msg void OnCbnSelchangeComboHanger46();
	afx_msg void OnCbnSelchangeComboHanger47();

};
