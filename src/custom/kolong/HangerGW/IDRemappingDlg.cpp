// IDRemappingDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HangerGW.h"
#include "IDRemappingDlg.h"

#include "Lib.h"


#define		EMPTY_STRING		(" Empty ")


// CIDRemappingDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CIDRemappingDlg, CDialog)

CIDRemappingDlg::CIDRemappingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIDRemappingDlg::IDD, pParent)
{

}

CIDRemappingDlg::~CIDRemappingDlg()
{
}

void CIDRemappingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	for(int i=0; i<MAX_ID_COUNT; i++)
	{
		DDX_Control(pDX, IDC_COMBO_SERIAL00+i, m_cbxSerial[i]);
		DDX_Control(pDX, IDC_COMBO_HANGER00+i, m_cbxHanger[i]);
	}
}


BEGIN_MESSAGE_MAP(CIDRemappingDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL00, &CIDRemappingDlg::OnCbnSelchangeComboSerial00)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL01, &CIDRemappingDlg::OnCbnSelchangeComboSerial01)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL02, &CIDRemappingDlg::OnCbnSelchangeComboSerial02)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL03, &CIDRemappingDlg::OnCbnSelchangeComboSerial03)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL04, &CIDRemappingDlg::OnCbnSelchangeComboSerial04)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL05, &CIDRemappingDlg::OnCbnSelchangeComboSerial05)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL06, &CIDRemappingDlg::OnCbnSelchangeComboSerial06)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL07, &CIDRemappingDlg::OnCbnSelchangeComboSerial07)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL08, &CIDRemappingDlg::OnCbnSelchangeComboSerial08)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL09, &CIDRemappingDlg::OnCbnSelchangeComboSerial09)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL10, &CIDRemappingDlg::OnCbnSelchangeComboSerial10)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL11, &CIDRemappingDlg::OnCbnSelchangeComboSerial11)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL12, &CIDRemappingDlg::OnCbnSelchangeComboSerial12)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL13, &CIDRemappingDlg::OnCbnSelchangeComboSerial13)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL14, &CIDRemappingDlg::OnCbnSelchangeComboSerial14)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL15, &CIDRemappingDlg::OnCbnSelchangeComboSerial15)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL16, &CIDRemappingDlg::OnCbnSelchangeComboSerial16)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL17, &CIDRemappingDlg::OnCbnSelchangeComboSerial17)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL18, &CIDRemappingDlg::OnCbnSelchangeComboSerial18)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL19, &CIDRemappingDlg::OnCbnSelchangeComboSerial19)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL20, &CIDRemappingDlg::OnCbnSelchangeComboSerial20)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL21, &CIDRemappingDlg::OnCbnSelchangeComboSerial21)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL22, &CIDRemappingDlg::OnCbnSelchangeComboSerial22)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL23, &CIDRemappingDlg::OnCbnSelchangeComboSerial23)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL24, &CIDRemappingDlg::OnCbnSelchangeComboSerial24)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL25, &CIDRemappingDlg::OnCbnSelchangeComboSerial25)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL26, &CIDRemappingDlg::OnCbnSelchangeComboSerial26)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL27, &CIDRemappingDlg::OnCbnSelchangeComboSerial27)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL28, &CIDRemappingDlg::OnCbnSelchangeComboSerial28)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL29, &CIDRemappingDlg::OnCbnSelchangeComboSerial29)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL30, &CIDRemappingDlg::OnCbnSelchangeComboSerial30)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL31, &CIDRemappingDlg::OnCbnSelchangeComboSerial31)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL32, &CIDRemappingDlg::OnCbnSelchangeComboSerial32)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL33, &CIDRemappingDlg::OnCbnSelchangeComboSerial33)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL34, &CIDRemappingDlg::OnCbnSelchangeComboSerial34)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL35, &CIDRemappingDlg::OnCbnSelchangeComboSerial35)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL36, &CIDRemappingDlg::OnCbnSelchangeComboSerial36)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL37, &CIDRemappingDlg::OnCbnSelchangeComboSerial37)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL38, &CIDRemappingDlg::OnCbnSelchangeComboSerial38)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL39, &CIDRemappingDlg::OnCbnSelchangeComboSerial39)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL40, &CIDRemappingDlg::OnCbnSelchangeComboSerial40)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL41, &CIDRemappingDlg::OnCbnSelchangeComboSerial41)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL42, &CIDRemappingDlg::OnCbnSelchangeComboSerial42)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL43, &CIDRemappingDlg::OnCbnSelchangeComboSerial43)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL44, &CIDRemappingDlg::OnCbnSelchangeComboSerial44)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL45, &CIDRemappingDlg::OnCbnSelchangeComboSerial45)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL46, &CIDRemappingDlg::OnCbnSelchangeComboSerial46)
	ON_CBN_SELCHANGE(IDC_COMBO_SERIAL47, &CIDRemappingDlg::OnCbnSelchangeComboSerial47)

	ON_CBN_SELCHANGE(IDC_COMBO_HANGER00, &CIDRemappingDlg::OnCbnSelchangeComboHanger00)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER01, &CIDRemappingDlg::OnCbnSelchangeComboHanger01)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER02, &CIDRemappingDlg::OnCbnSelchangeComboHanger02)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER03, &CIDRemappingDlg::OnCbnSelchangeComboHanger03)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER04, &CIDRemappingDlg::OnCbnSelchangeComboHanger04)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER05, &CIDRemappingDlg::OnCbnSelchangeComboHanger05)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER06, &CIDRemappingDlg::OnCbnSelchangeComboHanger06)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER07, &CIDRemappingDlg::OnCbnSelchangeComboHanger07)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER08, &CIDRemappingDlg::OnCbnSelchangeComboHanger08)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER09, &CIDRemappingDlg::OnCbnSelchangeComboHanger09)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER10, &CIDRemappingDlg::OnCbnSelchangeComboHanger10)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER11, &CIDRemappingDlg::OnCbnSelchangeComboHanger11)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER12, &CIDRemappingDlg::OnCbnSelchangeComboHanger12)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER13, &CIDRemappingDlg::OnCbnSelchangeComboHanger13)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER14, &CIDRemappingDlg::OnCbnSelchangeComboHanger14)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER15, &CIDRemappingDlg::OnCbnSelchangeComboHanger15)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER16, &CIDRemappingDlg::OnCbnSelchangeComboHanger16)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER17, &CIDRemappingDlg::OnCbnSelchangeComboHanger17)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER18, &CIDRemappingDlg::OnCbnSelchangeComboHanger18)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER19, &CIDRemappingDlg::OnCbnSelchangeComboHanger19)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER20, &CIDRemappingDlg::OnCbnSelchangeComboHanger20)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER21, &CIDRemappingDlg::OnCbnSelchangeComboHanger21)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER22, &CIDRemappingDlg::OnCbnSelchangeComboHanger22)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER23, &CIDRemappingDlg::OnCbnSelchangeComboHanger23)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER24, &CIDRemappingDlg::OnCbnSelchangeComboHanger24)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER25, &CIDRemappingDlg::OnCbnSelchangeComboHanger25)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER26, &CIDRemappingDlg::OnCbnSelchangeComboHanger26)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER27, &CIDRemappingDlg::OnCbnSelchangeComboHanger27)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER28, &CIDRemappingDlg::OnCbnSelchangeComboHanger28)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER29, &CIDRemappingDlg::OnCbnSelchangeComboHanger29)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER30, &CIDRemappingDlg::OnCbnSelchangeComboHanger30)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER31, &CIDRemappingDlg::OnCbnSelchangeComboHanger31)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER32, &CIDRemappingDlg::OnCbnSelchangeComboHanger32)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER33, &CIDRemappingDlg::OnCbnSelchangeComboHanger33)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER34, &CIDRemappingDlg::OnCbnSelchangeComboHanger34)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER35, &CIDRemappingDlg::OnCbnSelchangeComboHanger35)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER36, &CIDRemappingDlg::OnCbnSelchangeComboHanger36)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER37, &CIDRemappingDlg::OnCbnSelchangeComboHanger37)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER38, &CIDRemappingDlg::OnCbnSelchangeComboHanger38)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER39, &CIDRemappingDlg::OnCbnSelchangeComboHanger39)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER40, &CIDRemappingDlg::OnCbnSelchangeComboHanger40)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER41, &CIDRemappingDlg::OnCbnSelchangeComboHanger41)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER42, &CIDRemappingDlg::OnCbnSelchangeComboHanger42)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER43, &CIDRemappingDlg::OnCbnSelchangeComboHanger43)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER44, &CIDRemappingDlg::OnCbnSelchangeComboHanger44)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER45, &CIDRemappingDlg::OnCbnSelchangeComboHanger45)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER46, &CIDRemappingDlg::OnCbnSelchangeComboHanger46)
	ON_CBN_SELCHANGE(IDC_COMBO_HANGER47, &CIDRemappingDlg::OnCbnSelchangeComboHanger47)

END_MESSAGE_MAP()


// CIDRemappingDlg 메시지 처리기입니다.

BOOL CIDRemappingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int idx = 0;
	for(int i=0; i<MAX_BIT_COUNT && idx<MAX_ID_COUNT; i++)
	{
		CString str_serial_id;
		str_serial_id.Format("Serial%03d", i);

		char buf[1024] = {0};
		::GetPrivateProfileString("Remapping", str_serial_id, "-1", buf, 1023, ::GetINIPath());

		int hander_id = atoi(buf);
		if( hander_id == 0 ) continue;

		str_serial_id.Format("%03d", i);
		m_cbxSerial[idx].AddString(str_serial_id);
		m_cbxSerial[idx].SetCurSel(0);

		CString str_hanger_id;
		str_hanger_id.Format("%02d", hander_id);
		m_cbxHanger[idx].AddString(str_hanger_id);
		m_cbxHanger[idx].SetCurSel(0);
		idx++;
	}

	for(; idx<MAX_ID_COUNT; idx++)
	{
		m_cbxSerial[idx].AddString(EMPTY_STRING);
		m_cbxSerial[idx].SetCurSel(0);

		m_cbxHanger[idx].SetCurSel(-1);
		m_cbxHanger[idx].EnableWindow(FALSE);
	}

	InitAllSerialComboBox();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CIDRemappingDlg::OnOK()
{
	CString str_serial_id;
	CString str_hanger_id;

	//
	CMapStringToString map_serialID_to_hangerID;
	for(int i=0; i<MAX_BIT_COUNT; i++)
	{
		str_serial_id.Format("%03d", i);

		map_serialID_to_hangerID.SetAt(str_serial_id, "00");
	}

	//
	for(int i=0; i<MAX_ID_COUNT; i++)
	{
		m_cbxSerial[i].GetWindowText(str_serial_id);
		if( str_serial_id == EMPTY_STRING ) continue;

		m_cbxHanger[i].GetWindowText(str_hanger_id);

		map_serialID_to_hangerID.SetAt(str_serial_id, str_hanger_id);
	}

	//
	POSITION pos = map_serialID_to_hangerID.GetStartPosition();
	while( pos != NULL )
	{
		map_serialID_to_hangerID.GetNextAssoc( pos, str_serial_id, str_hanger_id );

		CString str_id;
		str_id.Format("Serial%03d", atoi(str_serial_id));

		::WritePrivateProfileString("Remapping", str_id, str_hanger_id, ::GetINIPath() );
	}

	CDialog::OnOK();
}

void CIDRemappingDlg::InitAllSerialComboBox()
{
	BeginWaitCursor();

	///////////////////////////////////////////////////////
	// add all (0 ~ MAX_BIT_COUNT-1)
	CMapStringToString map_serial_id;
	for(int i=0; i<MAX_BIT_COUNT; i++)
	{
		CString str_serial_id;
		str_serial_id.Format("%03d", i);

		map_serial_id.SetAt(str_serial_id, str_serial_id);
	}

	// remove already mapping-id
	for(int i=0; i<MAX_ID_COUNT; i++)
	{
		CString str_id;
		m_cbxSerial[i].GetWindowText(str_id);

		map_serial_id.RemoveKey(str_id);
	}

	for(int i=0; i<MAX_ID_COUNT; i++)
	{
		// get current-id
		CString str_id;
		m_cbxSerial[i].GetWindowText(str_id);

		// clear & add current-id
		m_cbxSerial[i].ResetContent();
		if( str_id != "" )
		{
			m_cbxSerial[i].AddString(str_id);
			m_cbxSerial[i].SetCurSel(0);
		}

		// add no-mapping-id
		POSITION pos = map_serial_id.GetStartPosition();
		CString key, value;
		while( pos != NULL )
		{
			map_serial_id.GetNextAssoc( pos, key, value );
			m_cbxSerial[i].AddString(value);
		}
		m_cbxSerial[i].AddString(EMPTY_STRING);
	}

	EndWaitCursor();
}

void CIDRemappingDlg::InitAllHangerComboBox()
{
	BeginWaitCursor();

	///////////////////////////////////////////////////////
	// add all (01 ~ MAX_BIT_COUNT)
	CMapStringToString map_hanger_id;
	for(int i=0; i<MAX_ID_COUNT; i++)
	{
		CString str_hanger_id;
		str_hanger_id.Format("%02d", i+1);

		map_hanger_id.SetAt(str_hanger_id, str_hanger_id);
	}

	// remove already mapping-id
	for(int i=0; i<MAX_ID_COUNT; i++)
	{
		CString str_id;
		m_cbxHanger[i].GetWindowText(str_id);

		map_hanger_id.RemoveKey(str_id);
	}

	for(int i=0; i<MAX_ID_COUNT; i++)
	{
		if( !m_cbxHanger[i].IsWindowEnabled() ) continue;

		// get current-id
		CString str_id;
		m_cbxHanger[i].GetWindowText(str_id);

		// clear & add current-id
		m_cbxHanger[i].ResetContent();
		if( str_id != "" )
		{
			m_cbxHanger[i].AddString(str_id);
			m_cbxHanger[i].SetCurSel(0);
		}

		// add no-mapping-id
		POSITION pos = map_hanger_id.GetStartPosition();
		CString key, value;
		while( pos != NULL )
		{
			map_hanger_id.GetNextAssoc( pos, key, value );
			m_cbxHanger[i].AddString(value);
		}
		if( m_cbxHanger[i].GetCurSel() < 0 ) m_cbxHanger[i].SetCurSel(0);
	}
	
	EndWaitCursor();
}

void CIDRemappingDlg::InitSerialComboBox(int nCbxID)
{
	CString str_id;
	m_cbxSerial[nCbxID].GetWindowText(str_id);

	if( str_id == EMPTY_STRING ) m_cbxHanger[nCbxID].SetCurSel(-1);
	m_cbxHanger[nCbxID].EnableWindow(str_id == EMPTY_STRING ? FALSE : TRUE);

	InitAllSerialComboBox();
	InitAllHangerComboBox();
}

void CIDRemappingDlg::OnCbnSelchangeComboSerial00() { InitSerialComboBox(0); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial01() { InitSerialComboBox(1); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial02() { InitSerialComboBox(2); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial03() { InitSerialComboBox(3); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial04() { InitSerialComboBox(4); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial05() { InitSerialComboBox(5); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial06() { InitSerialComboBox(6); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial07() { InitSerialComboBox(7); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial08() { InitSerialComboBox(8); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial09() { InitSerialComboBox(9); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial10() { InitSerialComboBox(10); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial11() { InitSerialComboBox(11); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial12() { InitSerialComboBox(12); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial13() { InitSerialComboBox(13); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial14() { InitSerialComboBox(14); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial15() { InitSerialComboBox(15); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial16() { InitSerialComboBox(16); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial17() { InitSerialComboBox(17); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial18() { InitSerialComboBox(18); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial19() { InitSerialComboBox(19); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial20() { InitSerialComboBox(20); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial21() { InitSerialComboBox(21); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial22() { InitSerialComboBox(22); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial23() { InitSerialComboBox(23); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial24() { InitSerialComboBox(24); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial25() { InitSerialComboBox(25); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial26() { InitSerialComboBox(26); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial27() { InitSerialComboBox(27); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial28() { InitSerialComboBox(28); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial29() { InitSerialComboBox(29); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial30() { InitSerialComboBox(30); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial31() { InitSerialComboBox(31); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial32() { InitSerialComboBox(32); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial33() { InitSerialComboBox(33); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial34() { InitSerialComboBox(34); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial35() { InitSerialComboBox(35); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial36() { InitSerialComboBox(36); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial37() { InitSerialComboBox(37); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial38() { InitSerialComboBox(38); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial39() { InitSerialComboBox(39); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial40() { InitSerialComboBox(40); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial41() { InitSerialComboBox(41); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial42() { InitSerialComboBox(42); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial43() { InitSerialComboBox(43); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial44() { InitSerialComboBox(44); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial45() { InitSerialComboBox(45); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial46() { InitSerialComboBox(46); }
void CIDRemappingDlg::OnCbnSelchangeComboSerial47() { InitSerialComboBox(47); }

void CIDRemappingDlg::OnCbnSelchangeComboHanger00() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger01() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger02() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger03() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger04() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger05() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger06() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger07() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger08() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger09() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger10() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger11() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger12() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger13() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger14() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger15() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger16() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger17() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger18() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger19() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger20() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger21() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger22() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger23() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger24() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger25() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger26() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger27() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger28() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger29() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger30() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger31() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger32() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger33() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger34() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger35() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger36() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger37() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger38() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger39() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger40() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger41() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger42() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger43() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger44() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger45() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger46() { InitAllHangerComboBox(); }
void CIDRemappingDlg::OnCbnSelchangeComboHanger47() { InitAllHangerComboBox(); }
