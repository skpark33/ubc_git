// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// HangerGW.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

#include <Tlhelp32.h>

unsigned long getPid(const char* exename)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) return 0;

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	if ( !Process32First( hSnapshot, &pe32 ) )
	{
		::CloseHandle(hSnapshot);
		return 0;
	}

	do
	{
		//char strProcessName[512];
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(pe32.szExeFile, exename) == 0 )
		{
			::CloseHandle(hSnapshot);
			return pe32.th32ProcessID;
		}
	} while( Process32Next( hSnapshot, &pe32 ) );

	::CloseHandle(hSnapshot);
	return 0;
}

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if( !IsWindowVisible(hwnd) ) return TRUE;

	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);

	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;

	if( pe->pid != pid ) return TRUE;

	pe->hwnd = hwnd;
	return FALSE;
}

HWND getWHandle(unsigned long pid)
{
	if( pid>0 )
	{
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);

		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1)
		{
			parent =  GetParent(child);
			if(!parent) return child;
			child = parent;
		}
		return e.hwnd;
	}
	return (HWND)0;
}
//
//LPCTSTR GetINIPath()
//{
//	static CString	_iniDirectory = _T("");
//
//	if(_iniDirectory.GetLength() == 0)
//	{
//		TCHAR str[MAX_PATH];
//		::ZeroMemory(str, MAX_PATH);
//		::GetModuleFileName(NULL, str, MAX_PATH);
//		int length = _tcslen(str) - 1;
//		while( (length > 0) && (str[length] != _T('\\')) )
//			str[length--] = 0;
//
//		_iniDirectory = str;
//
//		CString app;
//		app.LoadString(AFX_IDS_APP_TITLE);
//
//		_iniDirectory.Append(app);
//		_iniDirectory.Append(_T(".ini"));
//	}
//
//	return _iniDirectory;
//}

CString GetErrorMessageString(DWORD dwErrCode)
{
	switch(dwErrCode)
	{
	case WSANOTINITIALISED:	return _T("WSANOTINITIALISED");	break;
	case WSAENETDOWN:		return _T("WSAENETDOWN");		break;
	case WSAEADDRINUSE:		return _T("WSAEADDRINUSE");		break;
	case WSAEINPROGRESS:	return _T("WSAEINPROGRESS");	break;
	case WSAEINVAL:			return _T("WSAEINVAL");			break;
	case WSAEISCONN:		return _T("WSAEISCONN");		break;
	case WSAEMFILE:			return _T("WSAEMFILE");			break;
	case WSAENOBUFS:		return _T("WSAENOBUFS");		break;
	case WSAENOTSOCK:		return _T("WSAENOTSOCK");		break;
	case WSAEOPNOTSUPP:		return _T("WSAEOPNOTSUPP");		break;
	case WSAEFAULT:			return _T("WSAEFAULT");			break;
	case WSAEWOULDBLOCK:	return _T("WSAEWOULDBLOCK");	break;
	case WSAEACCES:			return _T("WSAEACCES");			break;
	case WSAENETRESET:		return _T("WSAENETRESET");		break;
	case WSAENOTCONN:		return _T("WSAENOTCONN");		break;
	case WSAESHUTDOWN:		return _T("WSAESHUTDOWN");		break;
	case WSAEMSGSIZE:		return _T("WSAEMSGSIZE");		break;
	case WSAECONNABORTED:	return _T("WSAECONNABORTED");	break;
	case WSAECONNRESET:		return _T("WSAECONNRESET");		break;
	case WSAEPROTONOSUPPORT:return _T("WSAEPROTONOSUPPORT");break;
	case WSAEPROTOTYPE:		return _T("WSAEPROTOTYPE");		break;
	case WSAESOCKTNOSUPPORT:return _T("WSAESOCKTNOSUPPORT");break;

	case 0:					return _T("NoERROR");			break;
	default:
		{
			CString err_str;
			err_str.Format("ErrCode(%u)", dwErrCode);
			return err_str;
			break;
		}
	}
}

CString HexToString(LPBYTE pData, int size)
{
	TCHAR buf[1024];
	CString str_data1;
	CString str_data2;
	for(int i=0; i<size; i++)
	{
		if( str_data2.GetLength() > 0 ) str_data2 += _T(",");

		if( 32 <= pData[i] && pData[i] < 127 )
		{
			_stprintf(buf, _T("%c"), pData[i]);
			str_data1 += buf;

			_stprintf(buf, _T("%02X"), pData[i]);
			str_data2 += buf;
		}
		else
		{
			str_data1 += " ";

			_stprintf(buf, _T("%02X"), pData[i]);
			str_data2 += buf;
		}
	}
	str_data1 += " (";
	str_data1 += str_data2;
	str_data1 += ")";

	return str_data1;
}
