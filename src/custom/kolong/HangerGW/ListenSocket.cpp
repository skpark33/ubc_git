// ListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HangerGW.h"
#include "ListenSocket.h"

#include "ControlSocket.h"

//#include "Lib.h"

// CListenSocket

CListenSocket::CListenSocket()
:	m_bOpen (false)
{
}

CListenSocket::~CListenSocket()
{
}

// CListenSocket 멤버 함수

void CListenSocket::OnAccept(int nErrorCode)
{
	if(nErrorCode)
	{
		//
		// warning
		// ::GetErrorMessageString(nErrorCode)
		//
	}
	else
	{
		// ok
	}

	CControlSocket* p_sock = NULL;
	p_sock = new CControlSocket();

	// add to listctrl
	if( p_sock )
	{
		p_sock->SetParentWnd( m_hParentWnd );

		// success to create command-socket obj
		if( Accept(*p_sock) )
		{
			// add all-socket-map
			CControlSocket::AddSocket((CControlSocket*)p_sock);

			// success to accept -> get peer info
			p_sock->GetPeerInfo();

			//
			CString str_log;
			str_log.Format("Connect to peer (%s:%d)", p_sock->GetPeerName(), p_sock->GetPeerPort());
			POST_LOG3(m_hParentWnd, "INFO", str_log);
		}
		else
		{
			// fail to accept
			DWORD nErrorCode = GetLastError();

			//
			CString str_log;
			str_log.Format("Fail to accept [ErrCode:%s(%u)]", ::GetErrorMessageString(nErrorCode), nErrorCode );
			POST_LOG3(m_hParentWnd, "ERROR", str_log);

			delete p_sock;
		}
	}
	else
	{
		// fail to create call-socket obj
		POST_LOG3(m_hParentWnd, "ERROR", "Can't create ControlSocket");
	}

	CAsyncSocket::OnAccept(nErrorCode);
}

void CListenSocket::OnClose(int nErrorCode)
{
	m_bOpen = false;

	if(nErrorCode)
	{
		//
		// warning
		// ::GetErrorMessageString(nErrorCode)
		//
	}
	else
	{
		// ok
	}

	//
	CString str_log;
	str_log.Format("Close ListenSocket(Port:%u)", m_nPort);
	POST_LOG3(m_hParentWnd, "WARN", str_log);

	CAsyncSocket::OnClose(nErrorCode);
}

void CListenSocket::Init(HWND hParentWnd, UINT nPort)
{
	m_hParentWnd = hParentWnd;
	m_nPort = nPort;
}

BOOL CListenSocket::StartListen()
{
	if( m_bOpen ) return TRUE;

	if( !Create(m_nPort) )
	{
		// fail to create -> retry StartListen
		Close();

		CString str_log;
		str_log.Format("Fail to create ListenSocket(Port:%u)", m_nPort);
		POST_LOG3(m_hParentWnd, "ERROR", str_log);

		return FALSE;
	}

	// success to create
	BOOL ret_val = Listen();
	if( !ret_val )
	{
		// fail to listen -> retry StartListen
		Close();

		CString str_log;
		str_log.Format("Fail to listen ListenSocket(Port:%u)", m_nPort);
		POST_LOG3(m_hParentWnd, "ERROR", str_log);

		return FALSE;
	}

	// success to listen
	m_bOpen = true;

	CString str_log;
	str_log.Format("Start ListenSocket(Port:%u)", m_nPort);
	POST_LOG3(m_hParentWnd, "INFO", str_log);

	return ret_val;
}

void CListenSocket::StopListen()
{
	m_bOpen = false;
	Close();
}


//DWORD CProxySocket::m_dwLastScreenChanged = 0;
//CString	CProxySocket::m_strLastScreen = "Ctrl+K";

/*
CString CProxySocket::BufferToString(LPBYTE pBuff, int size)
{
	if(size == 0)
		return "No Data";

	CString ret_val = "";
	CString str_tmp;

	for(int i=0; i<size; i++)
	{
		if( 32 <= pBuff[i] || pBuff[i] < 127 )
			ret_val.AppendChar((char)pBuff[i]);
		else
			ret_val += " ";

		str_tmp.Format("(%02X)", pBuff[i]);
		ret_val += str_tmp;
	}

	return ret_val;
}


unsigned long CProxySocket::getPid(const char* exename)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if( hSnapshot == INVALID_HANDLE_VALUE ) return 0;

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	char strProcessName[512] = {0};
	if( !Process32First ( hSnapshot, &pe32 ) )
	{
		::CloseHandle(hSnapshot);
		return 0;
	}

	do
	{
		//memset(strProcessName, 0, sizeof(strProcessName));
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(pe32.szExeFile, exename) == 0 )
		{
			::CloseHandle(hSnapshot);
			return pe32.th32ProcessID;
		}

	} while (Process32Next( hSnapshot, &pe32 ));

	::CloseHandle(hSnapshot);
	return 0;
}

typedef struct {
	DWORD pid;
	HWND hwnd;
} find_hwnd_from_pid_t;

static BOOL CALLBACK find_hwnd_from_pid_proc(HWND hwnd, LPARAM lParam)
{
	if (!IsWindowVisible(hwnd)) return TRUE;
	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);
	find_hwnd_from_pid_t *pe = (find_hwnd_from_pid_t *)lParam;
	if (pe->pid != pid) return TRUE;
	pe->hwnd = hwnd;
	return FALSE;
}

HWND CProxySocket::getWHandle(unsigned long pid)
{
	if( pid > 0 )
	{
		find_hwnd_from_pid_t e;
		e.pid = pid;
		e.hwnd = NULL;
		EnumWindows(find_hwnd_from_pid_proc, (LPARAM)&e);

		HWND child = e.hwnd;
		HWND parent = NULL;
		while(1)
		{
			parent =  GetParent(child);
			if(!parent){
				return child;
			}
			child = parent;
		}
		return e.hwnd;
	}
	return (HWND)0;
}
*/
