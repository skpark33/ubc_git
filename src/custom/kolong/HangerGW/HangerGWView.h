// HangerGWView.h : CHangerGWView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"
#include "afxmt.h"
#include "afxtempl.h"

#include "ListenSocket.h"

class CSerialMFC;


////////////////////////////////////////////////////////////////////////////////

class CHangerGWView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CHangerGWView();
	DECLARE_DYNCREATE(CHangerGWView)

public:
	enum{ IDD = IDD_HANGERGW_FORM };

// 특성입니다.
public:
	CHangerGWDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CHangerGWView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:

	static	int m_nComPort;

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnSerialMsg (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnReceiveData(WPARAM wParam, LPARAM lParam);

	CListCtrl m_lcLog;

	CByteArray			m_arrData;
	bool				m_bRunThread;
	DWORD				m_dwLastUpdateTick;
	CCriticalSection	m_csData;
	CWinThread*			m_pThread;

protected:
	CSerialMFC*		m_pSerial;
	ULONGLONG		m_nRowCount;
	bool			m_pSIDPushStatus[128]; // serial-ID, true=pushed, false=released
	CMapStringToString	m_mapSerialToHanger; // serial-ID <==> hanger-ID

	CString			m_strShortcutKey[8];
//	int				m_bSendSameShortcutKey;

	CListenSocket	m_ListenSocket;

	int				m_nTestHangerID;

	void	InsertItem(LPBYTE pData, DWORD dwSize, LPCTSTR lpszStatus);
	void	CheckListCount();

	BOOL	CheckDataString(LPCSTR lpszData, int size);

	void	ReloadIDRemapping();
	CString	GetHangerID(int nSerialID);

	void	SendToAllSocket(bool bPushStatus, int nSerialID, int nHangerID=-1);
public:
	//afx_msg void OnShortcutkeySettings();
	afx_msg void OnIdRemapping();
	afx_msg LRESULT OnCloseSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPostLog(WPARAM wParam, LPARAM lParam);

	afx_msg void OnSelectHangerId(UINT nID);
	afx_msg void OnUpdateSelectHangerId(CCmdUI *pCmdUI);

	afx_msg void OnSendReleaseDataToSocket();
	afx_msg void OnSendSetDataToSocket();
};

#ifndef _DEBUG  // HangerGWView.cpp의 디버그 버전
inline CHangerGWDoc* CHangerGWView::GetDocument() const
   { return reinterpret_cast<CHangerGWDoc*>(m_pDocument); }
#endif

static UINT CheckSerialDataThread(LPVOID pParam);