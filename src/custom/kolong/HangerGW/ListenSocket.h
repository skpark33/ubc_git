#pragma once

#include <tlhelp32.h>
#include <shlwapi.h>
#include <winternl.h>

// CListenSocket 명령 대상입니다.


class CListenSocket : public CAsyncSocket
{
public:
	CListenSocket();
	virtual ~CListenSocket();

	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);

	UINT	m_nPort;

protected:
	HWND	m_hParentWnd;
	bool	m_bOpen;

public:
	void	Init(HWND hParentWnd, UINT nPort);
	BOOL	StartListen();
	void	StopListen();
};

/*
////////////////////////////////////////////////
class CProxySocket : public CAsyncSocket
{
protected:
	static DWORD	m_dwLastScreenChanged;

public:
	CProxySocket() { m_pReadBuffer = new BYTE[1024]; };
	virtual ~CProxySocket() { if(m_pReadBuffer) delete[]m_pReadBuffer; };

	HWND		m_hParentWnd;
	CString		m_strPeerName;
	UINT		m_nPeerPort;
	UINT		m_nAcceptPort;

protected:
	CSocketBuffer	m_Buffer;
	LPBYTE		m_pReadBuffer;

	static CString	m_strLastScreen;

	CString		BufferToString(LPBYTE pBuff, int size);

	unsigned long getPid(const char* exename);
	HWND getWHandle(unsigned long pid);
};
*/