#pragma once

#include <afxmt.h>
#include <afxtempl.h>

#include "ListenSocket.h"



////////////////////////////////////////////////////////////
class CSocketBuffer
{
public:
	CSocketBuffer() { m_pBuffer=NULL; m_nBufferSize=0; };
	virtual ~CSocketBuffer()
	{
		Init();
	};

protected:
	LPBYTE		m_pBuffer;
	int			m_nBufferSize;
	CTime		m_tmLastUpdate;

public:
	void	Init()
	{
		if(m_pBuffer) delete[]m_pBuffer;
		m_pBuffer=NULL;
		m_nBufferSize=0;
	};

	bool	Append(LPBYTE pAppend, int nSize)
	{
		m_tmLastUpdate = CTime::GetCurrentTime();

		int new_size = m_nBufferSize + nSize;
		LPBYTE new_buff = new BYTE[new_size+1];
		if(new_buff == NULL) return false;
		::ZeroMemory(new_buff, new_size+1);
		if(m_pBuffer)
		{
			memcpy(new_buff, m_pBuffer, m_nBufferSize);
		}
		memcpy(new_buff+m_nBufferSize, pAppend, nSize);
		if(m_pBuffer) delete[]m_pBuffer;
		m_pBuffer = new_buff;
		m_nBufferSize = new_size;
		return true;
	};

	bool	Pop(int nSize, LPBYTE& pData)
	{
		if(nSize == m_nBufferSize)
		{
			pData = m_pBuffer;
			m_pBuffer = NULL;
			m_nBufferSize = 0;
			return true;
		}

		int new_size = m_nBufferSize - nSize;
		LPBYTE new_buff = new BYTE[new_size+1];
		if(new_buff == NULL) return false;
		::ZeroMemory(new_buff, new_size+1);
		if(m_pBuffer)
		{
			memcpy(new_buff, m_pBuffer+nSize, m_nBufferSize-nSize);
		}
		pData = m_pBuffer;
		m_pBuffer = new_buff;
		m_nBufferSize -= nSize;
		return true;
	}

	LPBYTE	GetBuffer() { return m_pBuffer; };
	int		GetBufferSize() { return m_nBufferSize; };
	void	CheckAndClear(int nLimitSec)
	{
		CTime cur_tm = CTime::GetCurrentTime();
		if( m_nBufferSize > 0 &&
			m_tmLastUpdate.GetTime()+nLimitSec < cur_tm.GetTime() )
		{
			// nLimitSec초 이후 초기화
			Init();
		}
	};
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CControlSocket 명령 대상입니다.

class CControlSocket : public CAsyncSocket
{
public:
	static	CCriticalSection	 _Lock;
	static	CArray<CControlSocket*, CControlSocket*>	m_AllSocketList;
	static	void AddSocket(CControlSocket* pSock) { LOCK(_Lock); m_AllSocketList.Add(pSock); };
	static	bool DeleteSocket(CControlSocket* pSock)
			{
				LOCK(_Lock);
				for(int i=0; i<m_AllSocketList.GetCount(); i++)
				{
					CControlSocket* tmp_sock = (CControlSocket*)m_AllSocketList.GetAt(i);
					if(tmp_sock == pSock)
					{
						delete tmp_sock;
						m_AllSocketList.RemoveAt(i);
						return true;
					}
				}
				return false;
			};
	static void DeleteAllSocket()
			{
				LOCK(_Lock);
				for(int i=0; i<m_AllSocketList.GetCount(); i++)
				{
					CControlSocket* tmp_sock = (CControlSocket*)m_AllSocketList.GetAt(i);
					if( tmp_sock ) delete tmp_sock;
				}
				m_AllSocketList.RemoveAll();
			};
	static	void SendToAll(HWND hParentWnd, LPBYTE pData, int nSize)
			{
				LOCK(_Lock);
				CString str_data = ::HexToString(pData, nSize);
				for(int i=0; i<m_AllSocketList.GetCount(); i++)
				{
					CControlSocket* tmp_sock = (CControlSocket*)m_AllSocketList.GetAt(i);

					if( tmp_sock == NULL ) continue;

					//
					CString str_log;
					str_log.Format("Send %s to %s:%u", str_data, tmp_sock->GetPeerName(), tmp_sock->GetPeerPort() );
					POST_LOG3(hParentWnd, "INFO", str_log);

					int total_send_size = 0;
					while(total_send_size < nSize)
					{
						int send_size = tmp_sock->Send(pData+total_send_size, nSize-total_send_size);
						if(send_size == SOCKET_ERROR)
						{
							// fail to send
							str_log.Format("fail to send %s:%u", tmp_sock->GetPeerName(), tmp_sock->GetPeerPort() );
							POST_LOG3(hParentWnd, "ERROR", str_log);
							break;
						}
						total_send_size += send_size;
					}
				}
			};
	static CString BufferToString(LPBYTE pBuff, int size);

public:
	CControlSocket();
	virtual ~CControlSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

	void	SetParentWnd(HWND hParentWnd) { m_hParentWnd = hParentWnd; };
	void	GetPeerInfo() { CAsyncSocket::GetPeerName(m_strPeerName, m_nPeerPort); };
	LPCTSTR	GetPeerName() { return m_strPeerName; };
	UINT	GetPeerPort() { return m_nPeerPort; };

protected:
	CSocketBuffer	m_Buffer;
	LPBYTE		m_pReadBuffer;

	HWND		m_hParentWnd;
	CString		m_strPeerName;
	UINT		m_nPeerPort;

	void		PacketAnalysis();


};
