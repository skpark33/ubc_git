// ControlSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HangerGW.h"
#include "ControlSocket.h"


CCriticalSection CControlSocket::_Lock;
CArray<CControlSocket*, CControlSocket*> CControlSocket::m_AllSocketList;


// CControlSocket

CControlSocket::CControlSocket()
{
	m_pReadBuffer = new BYTE[1024];
}

CControlSocket::~CControlSocket()
{
	if(m_pReadBuffer) delete[]m_pReadBuffer;
}


// CControlSocket 멤버 함수

void CControlSocket::OnConnect(int nErrorCode)
{
	CAsyncSocket::OnConnect(nErrorCode);
}

void CControlSocket::OnClose(int nErrorCode)
{
	::PostMessage(m_hParentWnd, WM_CLOSE_SOCKET, (WPARAM)this, NULL);

	CAsyncSocket::OnClose(nErrorCode);
}

void CControlSocket::OnReceive(int nErrorCode)
{
	if( m_pReadBuffer )
	{
		int nRead = Receive(m_pReadBuffer, 1024); 
		//ciDEBUG(10, ("Receive (0x%08X) ReadSize=%d", this, nRead) );

		switch (nRead)
		{
		case 0:
			// read nothing -> socket error
			Close();
			//::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			break;

		case SOCKET_ERROR:
			if (GetLastError() != WSAEWOULDBLOCK) 
			{
				// socket error occurred
				Close();
				//::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			}
			break;

		default:
			//POST_LOG( 
			//	m_hParentWnd, 
			//	"Control", "Receive", 
			//	"", m_nAcceptPort, 
			//	m_strPeerName, m_nPeerPort, 
			//	m_pReadBuffer, nRead
			//);
			//BufferToString(m_pReadBuffer, nRead);

			if( m_Buffer.Append(m_pReadBuffer, nRead) )
			{
				//PacketAnalysis();
			}
			else
			{
				// fail to append received-buffer -> not enough memory
				//ciWARN( ("Fail to append Buffer !!! (0x%08X)", this) );
				//m_strErrMsg = "WARNING : Fail to append Buffer";
				Close();
				//	::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
			}
			break;
		}
	}
	else
	{
		// receive-buffer is null -> not enough memory
		//ciWARN( ("ReadBuffer is NULL !!! (0x%08X)", this) );
		//m_strErrMsg = "WARNING : ReadBuffer is NULL";
		Close();
		//::PostMessage(m_hParentWnd, WM_CLOSE_CALL_SOCKET, (WPARAM)this, NULL);
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CControlSocket::PacketAnalysis()
{
	int buf_size = m_Buffer.GetBufferSize();
/*
	for( ; buf_size >= 18; buf_size-=18 )
	{
		LPBYTE buff;
		m_Buffer.Pop(18, buff);

		char buff_scr[3] = {0};
		char buff_act[3] = {0};

		memcpy(buff_scr, buff+14, 2);
		memcpy(buff_act, buff+16, 2);

		CString msg;
		msg.Format("Screen:%s, Action:%s", buff_scr, buff_act);

		POST_LOG2( 
			m_hParentWnd, 
			"Control", "Change", 
			"", 0, 
			"", 0, 
			msg );
		SendAll(this, buff, 18);
		delete[]buff;

		//
		HWND brwHwnd = getWHandle(getPid("UTV_brwClient2.exe"));
		if(brwHwnd)
		{
			if(memcmp(buff_scr,"01",2)==0 && memcmp(buff_act,"02",2)==0)
			{
				// check last_screen_changed_time
				DWORD cur_tick = ::GetTickCount();
				while(cur_tick - m_dwLastScreenChanged < 2000)
				{
					::Sleep(0);
					cur_tick = ::GetTickCount();
				}
				m_dwLastScreenChanged = cur_tick;

				// send close_socket_command to flash
				CControlSocket::SendAll(NULL, (LPBYTE)"123456789012340000", 18);
				::Sleep(100);
				CControlSocket::SendAll(this, (LPBYTE)"123456789012348888", 18);
				::Sleep(100);

				// korean
				COPYDATASTRUCT appInfo;
				appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
				appInfo.lpData = (char*)"CTRL+K";
				appInfo.cbData =strlen("CTRL+K") +1 ;
				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);

				m_strLastScreen = "CTRL+K";
			}
			if(memcmp(buff_scr,"01",2)==0 && memcmp(buff_act,"03",2)==0)
			{
				// check last_screen_changed_time
				DWORD cur_tick = ::GetTickCount();
				while(cur_tick - m_dwLastScreenChanged < 2000)
				{
					::Sleep(0);
					cur_tick = ::GetTickCount();
				}
				m_dwLastScreenChanged = cur_tick;

				// send close_socket_command to flash
				CControlSocket::SendAll(NULL, (LPBYTE)"123456789012340000", 18);
				::Sleep(100);
				CControlSocket::SendAll(this, (LPBYTE)"123456789012348888", 18);
				::Sleep(100);

				// english
				COPYDATASTRUCT appInfo;
				appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
				appInfo.lpData = (char*)"CTRL+E";
				appInfo.cbData =strlen("CTRL+E") +1 ;
				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);

				m_strLastScreen = "CTRL+E";
			}
		}
	}
*/
}

CString CControlSocket::BufferToString(LPBYTE pBuff, int size)
{
	if(size == 0)
		return "No Data";

	CString ret_val = "";
	CString str_tmp;

	for(int i=0; i<size; i++)
	{
		if( 32 <= pBuff[i] || pBuff[i] < 127 )
			ret_val.AppendChar((char)pBuff[i]);
		else
			ret_val += " ";

		str_tmp.Format("(%02X)", pBuff[i]);
		ret_val += str_tmp;
	}

	return ret_val;
}

