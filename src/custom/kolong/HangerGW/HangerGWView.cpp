// HangerGWView.cpp : CHangerGWView 클래스의 구현
//

#include "stdafx.h"
#include "HangerGW.h"

#include "HangerGWDoc.h"
#include "HangerGWView.h"

#include "IDRemappingDlg.h"

#include "ControlSocket.h"
#include "SerialMFC.h"
#include "Lib.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		WM_RECEIVE_DATA		(WM_USER+100)


// CHangerGWView

IMPLEMENT_DYNCREATE(CHangerGWView, CFormView)

BEGIN_MESSAGE_MAP(CHangerGWView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_SERIAL(OnSerialMsg)
	ON_MESSAGE(WM_RECEIVE_DATA, OnReceiveData)
	ON_COMMAND(ID_ID_REMAPPING, &CHangerGWView::OnIdRemapping)
	ON_MESSAGE(WM_CLOSE_SOCKET, OnCloseSocket)
	ON_MESSAGE(WM_POST_LOG, OnPostLog)
	ON_COMMAND_RANGE(ID_SELECT_HANGER_ID_01, ID_SELECT_HANGER_ID_24, &CHangerGWView::OnSelectHangerId)
	ON_COMMAND(ID_SEND_RELEASE_DATA_TO_SOCKET, &CHangerGWView::OnSendReleaseDataToSocket)
	ON_COMMAND(ID_SEND_SET_DATA_TO_SOCKET, &CHangerGWView::OnSendSetDataToSocket)
	ON_UPDATE_COMMAND_UI_RANGE(ID_SELECT_HANGER_ID_01, ID_SELECT_HANGER_ID_24, &CHangerGWView::OnUpdateSelectHangerId)
END_MESSAGE_MAP()

// CHangerGWView 생성/소멸

int CHangerGWView::m_nComPort = 1;


CHangerGWView::CHangerGWView()
	: CFormView(CHangerGWView::IDD)
{
	m_pSerial = new CSerialMFC;
	m_nRowCount = 0;
	m_pThread = NULL;

	m_nTestHangerID = 1;

	::ZeroMemory(m_pSIDPushStatus, sizeof(m_pSIDPushStatus));
}

CHangerGWView::~CHangerGWView()
{
	//
	if(m_pSerial) delete m_pSerial;

	//
	CControlSocket::DeleteAllSocket();

	//
	if(m_pThread)
	{
		m_bRunThread = false;

		try
		{
			DWORD dwExitCode;

			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 1000);
				if(timeout == WAIT_TIMEOUT)
				{
					//__WARNING__(_T("Timeout !!!"), _NULL);
				}
				else
				{
				}
			}

			delete m_pThread;
		}
		catch(...)
		{
			//__WARNING__(_T("Exception in DeleteThread !!!"), _NULL);
		}

		m_pThread = NULL;
	}
}

void CHangerGWView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
}

BOOL CHangerGWView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CHangerGWView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//
	ReloadIDRemapping();

	//
	m_lcLog.SetExtendedStyle(m_lcLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcLog.InsertColumn(0, _T("Count"), LVCFMT_RIGHT, 0);
	m_lcLog.InsertColumn(1, _T("Time"), 0, 120);
	m_lcLog.InsertColumn(2, _T("Type"), 0, 120);
	m_lcLog.InsertColumn(3, _T("Receive Data Packet"), 0, 500);
	m_lcLog.InsertColumn(4, _T("Data Size"), 0, 60);

	//
#ifdef DEBUG
	// no serial-port (debug only)
	SetTimer(1026, 5000, NULL);
#else
	// use serial-port
	SetTimer(1025, 1000, NULL);
#endif

	//
	m_ListenSocket.Init(GetSafeHwnd(), 14001);
	m_ListenSocket.StartListen();
}


// CHangerGWView 진단

#ifdef _DEBUG
void CHangerGWView::AssertValid() const
{
	CFormView::AssertValid();
}

void CHangerGWView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CHangerGWDoc* CHangerGWView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CHangerGWDoc)));
	return (CHangerGWDoc*)m_pDocument;
}
#endif //_DEBUG


// CHangerGWView 메시지 처리기

void CHangerGWView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if( GetSafeHwnd() && m_lcLog.GetSafeHwnd() )
	{
		m_lcLog.MoveWindow(0, 0, cx, cy);
	}
}

void CHangerGWView::OnTimer(UINT_PTR nIDEvent)
{
	long errCode = ERROR_SUCCESS;
	CString str_comport;
	CString str_msg;

	switch(nIDEvent)
	{
	// Open & Init COM-port
	case 1025:

		KillTimer(1025);

		str_comport.Format(_T("COM%d"), (m_nComPort==0 ? 1 : m_nComPort) );

		// Attempt to open the _serial port (COM1)
		errCode = m_pSerial->Open(str_comport, this);
		if (errCode != ERROR_SUCCESS)
		{
			//_errMsg = "Unable to open COM-port";
			//printf("code=%d,msg=%s\n", errCode,_errMsg.c_str());
			str_msg.Format("Fail to Open %s", str_comport);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			SetTimer(1025, 10000, NULL);
			return;
		}
		str_msg.Format("Open %s", str_comport);
		InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "INFO");

		// Setup the _serial port (9600,8N1, which is the default setting)
		errCode = m_pSerial->Setup(	(CSerial::EBaudrate)CSerial::EBaud9600,
									(CSerial::EDataBits)CSerial::EData8,
									(CSerial::EParity)CSerial::EParNone,
									(CSerial::EStopBits)CSerial::EStop1		);

		if (errCode != ERROR_SUCCESS)
		{
			//_errMsg = "Unable to set COM-port setting";
			//printf("code=%d,msg=%s\n",errCode,_errMsg.c_str());
			str_msg.Format("Fail to Setup %s", str_comport);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			SetTimer(1025, 10000, NULL);
			return;
		}
		str_msg.Format("Setup %s (9600,8,N,1)", str_comport);
		InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "INFO");

		// Register only for the receive event
		errCode = m_pSerial->SetMask(CSerial::EEventBreak |
									CSerial::EEventCTS   |
									CSerial::EEventDSR   |
									CSerial::EEventError |
									CSerial::EEventRing  |
									CSerial::EEventRLSD  |
									CSerial::EEventRecv);
		
		if (errCode != ERROR_SUCCESS)
		{
			//_errMsg = "Unable to set COM-port event mask";
			//printf("code=%d,msg=%s\n",errCode,_errMsg.c_str());
			str_msg.Format("Fail to SetMask %s", str_comport);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			SetTimer(1025, 10000, NULL);
			return;
		}
		str_msg.Format("SetMask %s", str_comport);
		InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "INFO");

		// Use 'non-blocking' reads, because we don't know how many bytes
		// will be received. This is normally the most convenient mode
		// (and also the default mode for reading data).
		errCode = m_pSerial->SetupReadTimeouts(CSerial::EReadTimeoutNonblocking);
		if (errCode != ERROR_SUCCESS)
		{
			//_errMsg = "Unable to set COM-port read timeout.";
			//printf("code=%d,msg=%s\n",errCode,_errMsg.c_str());
			str_msg.Format("Fail to SetupReadTimeouts %s", str_comport);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			SetTimer(1025, 10000, NULL);
			return;
		}
		str_msg.Format("SetupReadTimeouts %s (Nonblocking)", str_comport);
		InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "INFO");

		// create data-thread
		m_pThread = ::AfxBeginThread(CheckSerialDataThread, (LPVOID)this, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = FALSE;
		m_bRunThread = true;
		m_pThread->ResumeThread();

		break;

	case 1026:
		KillTimer(1026);
		{
			// debug test code
			CString* str = new CString;

			static ULONGLONG upper=0;
			static ULONGLONG lower=1;
			if( lower > 0 )
			{
				lower<<=1;
				if( lower == 0 ) upper=1;
			}
			else
			{
				upper<<=1;
				if( upper == 0 ) lower=1;
			}

			str->Format(" TEST S,%08I64X%08I64X,E TEST \r\n", upper, lower);
			//str->SetAt(10, 'Z');

			PostMessage(WM_RECEIVE_DATA, (WPARAM)str, NULL);
		}
		SetTimer(1026, 5000, NULL);
		break;
	}

	CFormView::OnTimer(nIDEvent);
}

void CHangerGWView::InsertItem(LPBYTE pData, DWORD dwSize, LPCTSTR lpszStatus)
{
	m_lcLog.SetRedraw(FALSE);

	TCHAR buf[1024];

	_stprintf(buf, _T("%I64u"), m_nRowCount);
	m_lcLog.InsertItem(0, buf);

	m_lcLog.SetItemText(0, 1, CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S"));
	m_lcLog.SetItemText(0, 2, lpszStatus);

	if(dwSize == MAXDWORD)
	{
		// log,error,info type
		LPTSTR str_msg = (LPTSTR)pData;
		m_lcLog.SetItemText(0, 3, str_msg);
	}
	else if(dwSize > 0)
	{
		// data type
/*		CString str_data1;
		CString str_data2;
		for(int i=0; i<dwSize; i++)
		{
			if( str_data2.GetLength() > 0 ) str_data2 += _T(",");

			if( 32 <= pData[i] && pData[i] < 127 )
			{
				_stprintf(buf, _T("%c"), pData[i]);
				str_data1 += buf;

				_stprintf(buf, _T("%02X"), pData[i]);
				str_data2 += buf;
			}
			else
			{
				str_data1 += " ";

				_stprintf(buf, _T("%02X"), pData[i]);
				str_data2 += buf;
			}
		}
		str_data1 += " (";
		str_data1 += str_data2;
		str_data1 += ")";
		m_lcLog.SetItemText(0, 3, str_data1);
*/
		m_lcLog.SetItemText(0, 3, ::HexToString(pData, dwSize));

		_stprintf(buf, _T("%u"), dwSize);
		m_lcLog.SetItemText(0, 4, buf);
	}

	m_nRowCount++;

	CheckListCount();

	m_lcLog.SetRedraw(TRUE);
}

void CHangerGWView::CheckListCount()
{
	int count = m_lcLog.GetItemCount();
	if(count > 1000)
	{
		m_lcLog.SetRedraw(FALSE);
		for(int i=count-1; i>900; i--)
		{
			m_lcLog.DeleteItem(i);
		}
		m_lcLog.SetRedraw(TRUE);
	}
}

LRESULT CHangerGWView::OnSerialMsg(WPARAM wParam, LPARAM lParam)
{
	CSerial::EEvent eEvent = CSerial::EEvent(LOWORD(wParam));
	CSerial::EError eError = CSerial::EError(HIWORD(wParam));

	BYTE buf[1024] = {0};
	DWORD read_size = 0;

	// lParam: User-defined 32-bit integer (type LPARAM)
	if( eEvent & CSerialMFC::EEventRecv )
	{
		m_pSerial->Read(buf, 1024, &read_size);
		if(read_size > 0)
		{
			m_csData.Lock();

			int old_size = m_arrData.GetSize();
			int new_size = old_size + read_size;
			m_arrData.SetSize(new_size); // expand to new-size

			int idx=0;
			for(int i=old_size; i<new_size; i++, idx++)
			{
				// append to buffer
				m_arrData.SetAt(i, buf[idx]);
			}

			m_csData.Unlock();
		}
	}

	// Return successful
	return 0;
}

ULONGLONG HexToInt(LPCSTR lpszHex)
{
	ULONGLONG ret_val = 0;
	int pos = -1;

	while(lpszHex[++pos] != 0)
	{
		ret_val <<= 4;

		char ch = lpszHex[pos];
		if( '0' <= ch && ch <= '9' )
		{
			ret_val |= ((ULONGLONG)(ch - '0'));
			continue;
		}
		else if( 'a' <= ch && ch <= 'f' )
		{
			ret_val |= ((ULONGLONG)(ch - 'a' + 10));
			continue;
		}
		else if( 'A' <= ch && ch <= 'F' )
		{
			ret_val |= ((ULONGLONG)(ch - 'A' + 10));
			continue;
		}
		break;
	}

	return ret_val;
}

BYTE HexToByte(char ch)
{
	BYTE ret_val = 0;

	if( '0' <= ch && ch <= '9' )
	{
		ret_val |= ((ULONGLONG)(ch - '0'));
	}
	else if( 'a' <= ch && ch <= 'f' )
	{
		ret_val |= ((ULONGLONG)(ch - 'a' + 10));
	}
	else if( 'A' <= ch && ch <= 'F' )
	{
		ret_val |= ((ULONGLONG)(ch - 'A' + 10));
	}

	return ret_val;
}

BOOL CHangerGWView::CheckDataString(LPCSTR lpszData, int size)
{
	for(int i=0; i<size; i++)
	{
		char ch = lpszData[i];
		if( '0' <= ch && ch <= '9' ) continue;
		if( 'a' <= ch && ch <= 'f' ) continue;
		if( 'A' <= ch && ch <= 'F' ) continue;
		return FALSE;
	}
	return TRUE;
}

LRESULT CHangerGWView::OnReceiveData(WPARAM wParam, LPARAM lParam)
{
	static CString str_prev = "";
	static bool initialized = false;

	CString* str = (CString*)wParam;
	str->Replace(" ", "");
	str->Replace("\r", "");
	str->Replace("\n", "");

	if( str_prev != *str )
	{
		str_prev = *str;

		int pos1 = str_prev.Find("S,");
		int pos2 = str_prev.Find(",E");

		if( pos1 < 0 || pos2 < 0 )
		{
			// invalid serial-packet
			InsertItem((LPBYTE)(LPCTSTR)str_prev, str->GetLength(), "Serial(NoHeaderTail)");
		}
		else if( (pos2 - (pos1+2)) != MAX_DATA_BODY_LENGTH )
		{
			// invalid serial-packet
			InsertItem((LPBYTE)(LPCTSTR)str_prev, str->GetLength(), "Serial(InvalidDataSize)");
		}
		else if( !CheckDataString((LPCTSTR)str_prev + pos1+2, MAX_DATA_BODY_LENGTH) )
		{
			// invalid serial-packet
			InsertItem((LPBYTE)(LPCTSTR)str_prev, str->GetLength(), "Serial(InvalidData)");
		}
		else
		{
			// normal serial-packet
			InsertItem((LPBYTE)(LPCTSTR)str_prev, str_prev.GetLength(), "Serial");

			// 128bit ver
			int idx=0;
			LPTSTR ptr = (LPTSTR)((LPCTSTR)str_prev + pos1 + 2);
			for(int i=0; i<32 && (*ptr)!=',' && (*ptr)!=0; i++,ptr++ ) // total 16-byte
			{
				BYTE data = HexToByte(*ptr);

				BYTE mask = 0x08;
				for(int j=0; j<4; j++,idx++,mask>>=1)
				{
					bool new_pushed = ((data & mask) != 0);

					if( m_pSIDPushStatus[idx] != new_pushed )
					{
						m_pSIDPushStatus[idx] = new_pushed;

						if( initialized )
						{
							SendToAllSocket(new_pushed, idx);
						}
					}
				}
			}
/*
			// 64bit ver
			LPCTSTR ptr = (LPCTSTR)str_prev + pos1 + 2;
			ULONGLONG data = HexToInt(ptr);

			ULONGLONG mask = 0x00000001;
			int i=0;
			for(int i=0; mask!=0; i++,mask<<=1)
			{
				bool new_pushed = ((data & mask) != 0);

				if( m_pSIDPushStatus[i] != new_pushed )
				{
					m_pSIDPushStatus[i] = new_pushed;

					if( initialized )
					{
						//CString str_serial_id;
						//str_serial_id.Format("%02d", i);

						//CString str_hanger_id = GetHangerID(str_serial_id);

						//CString str_msg;
						//str_msg.Format("Serial(%02d)-Hanger(%s) %s", i, str_hanger_id, (new_pushed ? "Set" : "Release"));
						//InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "INFO");

						//CString str_packet;
						//if( new_pushed )
						//	str_packet.Format("CTRL000402%s\r\n", str_hanger_id);
						//else
						//	str_packet.Format("CTRL000401%s\r\n", str_hanger_id);

						//CControlSocket::SendToAll(GetSafeHwnd(), (LPBYTE)(LPCTSTR)str_packet, str_packet.GetLength());
						SendToAllSocket(new_pushed, i);
					}
				}
			}
*/
		}
	}

	delete str;

	initialized = true;

	//if(size > 0)
	//{
	//	static int prev_short_pin = -1;

	//	CString str_status;
	//	if(data[1] == 0x23 && data[4] == 0x07)
	//	{
	//		// open
	//		str_status.Format(_T("Open"));

	//		//
	//		HWND brwHwnd = getWHandle(getPid("UTV_brwClient2.exe"));
	//		if(brwHwnd && m_bTypePushbutton==false)
	//		{
	//			if( m_strShortcutKey[0].GetLength() > 0 && 
	//				( prev_short_pin != 0 || m_bSendSameShortcutKey ) )
	//			{
	//				prev_short_pin = 0;

	//				COPYDATASTRUCT appInfo;
	//				appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
	//				appInfo.lpData = (char*)(LPCSTR)m_strShortcutKey[0];
	//				appInfo.cbData = m_strShortcutKey[0].GetLength() + 1;
	//				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
	//			}
	//		}
	//	}
	//	else if(data[1] == 0x23)
	//	{
	//		// short
	//		BYTE data_inv = ~(data[4]);
	//		BYTE filter = 0x01;
	//		CString str;
	//		for(int i=0; i<3; i++, filter<<=1)
	//		{
	//			if(data_inv & filter)
	//			{
	//				if(str.GetLength() > 0) str+=_T(",");
	//				TCHAR buf[2]={0};
	//				buf[0] = _T('1') + i;
	//				str+=buf;
	//			}
	//		}
	//		str_status.Format(_T("Short (%s)"), str);

	//		//
	//		HWND brwHwnd = getWHandle(getPid("UTV_brwClient2.exe"));
	//		if(brwHwnd)
	//		{
	//			int short_pin = data_inv & 0x07;

	//			if( m_strShortcutKey[short_pin].GetLength() > 0 && 
	//				( prev_short_pin != short_pin || m_bSendSameShortcutKey ) )
	//			{
	//				prev_short_pin = short_pin;

	//				COPYDATASTRUCT appInfo;
	//				appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
	//				appInfo.lpData = (char*)(LPCSTR)m_strShortcutKey[short_pin];
	//				appInfo.cbData = m_strShortcutKey[short_pin].GetLength() +1 ;
	//				::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
	//			}
	//		}
	//	}
	//	else if(data[1] == 0x2A)
	//	{
	//		// watch-dog
	//		if(data[2] == 0x00 && data[3] == 0x01)
	//		{
	//			// watch-dog start
	//			str_status.Format(_T("WatchDog Start (Reset-Count: %d)"), data[4]);
	//		}
	//		else if(data[2] == 0x00 && data[3] == 0x00)
	//		{
	//			// watch-dog stop
	//			str_status.Format(_T("WatchDog Stop (Reset-Count: %d)"), data[4]);
	//		}
	//		else if(data[2] == 0x00 && data[3] == 0x02)
	//		{
	//			// watch-dog test
	//			str_status.Format(_T("WatchDog Test (Reset-Count: %d)"), data[4]);
	//		}
	//		else if(data[2] == 0x01 && data[3] == 0x00)
	//		{
	//			// watch-dog reset(stop)
	//			str_status.Format(_T("WatchDog Reset-Stop (Reset-Count: %d)"), data[4]);
	//		}
	//		else if(data[2] == 0x01 && data[3] == 0x01)
	//		{
	//			// watch-dog reset(start)
	//			str_status.Format(_T("WatchDog Reset-Start (Reset-Count: %d)"), data[4]);
	//		}
	//		else if(data[2] == 0x01 && data[3] == 0x02)
	//		{
	//			// watch-dog reset(test)
	//			str_status.Format(_T("WatchDog Reset-Test (Reset-Count: %d)"), data[4]);
	//		}
	//	}
	//	else
	//	{
	//		str_status = _T("Unknown");
	//	}

	//	InsertItem(data, size, str_status);
	//}

	//delete[]data;

	return 0;
}

UINT CheckSerialDataThread(LPVOID pParam)
{
	CHangerGWView* parent_wnd = (CHangerGWView*)pParam;

	while(parent_wnd->m_bRunThread)
	{
		parent_wnd->m_csData.Lock();

		int size = parent_wnd->m_arrData.GetCount();

		if( size > 0 )
		{
			LPBYTE src_data = (LPBYTE)parent_wnd->m_arrData.GetData();
			LPBYTE tmp_data = new BYTE[size+1];
			::ZeroMemory(tmp_data, size+1);
			memcpy(tmp_data, src_data, size);

			for(int i=0; i<size; i++)
			{
				if( tmp_data[i] == 0x0D || tmp_data[i] == 0x0A ) // \r,\n delimiter
				{
					if( i > 0 )
					{
						tmp_data[i] = 0;
						CString* str = new CString;
						*str = (LPCTSTR)tmp_data;

						parent_wnd->PostMessage(WM_RECEIVE_DATA, (WPARAM)str, NULL);
					}

					int new_size = size - i - 1;
					memcpy(src_data, tmp_data+i+1, new_size);
					parent_wnd->m_arrData.SetSize(new_size);
				}
			}

			delete[]tmp_data;

			//int end;
			//for(int i=0; i<size; i+=16)
			//{
			//	end = i+16;
			//	if(end > size) end = size;
			//	int part_size = end-i;

			//	LPBYTE part_data = new BYTE[size];
			//	memcpy(part_data, parent_wnd->m_arrData.GetData() + i, part_size);
			//	parent_wnd->PostMessage(WM_RECEIVE_DATA, (WPARAM)part_data, (LPARAM)part_size);
			//}
			//parent_wnd->m_arrData.RemoveAll();
		}

		parent_wnd->m_csData.Unlock();

		::Sleep(1);
	}

	return 0;
}

void CHangerGWView::OnIdRemapping()
{
	CIDRemappingDlg dlg;
	if( dlg.DoModal() == IDOK )
		ReloadIDRemapping();
}

LRESULT CHangerGWView::OnCloseSocket(WPARAM wParam, LPARAM lParam)
{
	CControlSocket* tmp_sock = (CControlSocket*)wParam;
	if( tmp_sock == NULL ) return FALSE;

	CString str_log;
	str_log.Format("Socket Closed (%s:%u)", tmp_sock->GetPeerName(), tmp_sock->GetPeerPort());
	InsertItem((LPBYTE)(LPCTSTR)str_log, MAXDWORD, "INFO");

	CControlSocket::DeleteSocket(tmp_sock);

	return TRUE;
}

LRESULT CHangerGWView::OnPostLog(WPARAM wParam, LPARAM lParam)
{
	CString* type = (CString*)wParam;
	CString* log = (CString*)lParam;

	if( type==NULL || log==NULL )
	{
		if( type ) delete type;
		if( log ) delete log;
		return FALSE;
	}

	InsertItem((LPBYTE)(LPCTSTR)*log, MAXDWORD, *type);

	delete type;
	delete log;

	return TRUE;
}

void CHangerGWView::ReloadIDRemapping()
{
	m_mapSerialToHanger.RemoveAll();

	for(int i=0; i<MAX_BIT_COUNT; i++)
	{
		CString str_serial_id;
		CString str_hanger_id;

		str_serial_id.Format("Serial%03d", i);

		char buf[1024] = {0};
		::GetPrivateProfileString("Remapping", str_serial_id, "-1", buf, 1023, ::GetINIPath());

		int hander_id = atoi(buf);
		if( hander_id == 0 ) continue;
		if( hander_id < 0 )
		{
			::WritePrivateProfileString("Remapping", str_serial_id, "00", ::GetINIPath());
			continue;
		}

		str_serial_id.Format("%03d", i);
		str_hanger_id.Format("%02d", hander_id);

		m_mapSerialToHanger.SetAt(str_serial_id, str_hanger_id);
	}
}

CString CHangerGWView::GetHangerID(int nSerialID)
{
	CString str_serial_id;
	str_serial_id.Format("%03d", nSerialID);

	CString str_hanger_id;
	if( m_mapSerialToHanger.Lookup(str_serial_id, str_hanger_id) )
		return str_hanger_id;
	return "00";
}

void CHangerGWView::SendToAllSocket(bool bPushStatus, int nSerialID, int nHangerID)
{
	CString str_hanger_id;
	if( nHangerID < 0 )
		str_hanger_id = GetHangerID(nSerialID);
	else
		str_hanger_id.Format("%02d", nHangerID);

	CString str_msg;
	str_msg.Format("Serial(%03d)-Hanger(%s) %s", nSerialID, str_hanger_id, (bPushStatus ? "Set" : "Release"));
	InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "INFO");

	CString str_packet;
	if( bPushStatus )
		str_packet.Format("CTRL000402%s\r\n", str_hanger_id);
	else
		str_packet.Format("CTRL000401%s\r\n", str_hanger_id);

	CControlSocket::SendToAll(GetSafeHwnd(), (LPBYTE)(LPCTSTR)str_packet, str_packet.GetLength());
}

void CHangerGWView::OnSelectHangerId(UINT nID)
{
	m_nTestHangerID = nID - ID_SELECT_HANGER_ID_01 + 1;
}

void CHangerGWView::OnUpdateSelectHangerId(CCmdUI *pCmdUI)
{
	int select_hanger_id = ID_SELECT_HANGER_ID_01 + m_nTestHangerID - 1;
	pCmdUI->SetCheck(select_hanger_id == pCmdUI->m_nID ? 1 : 0);
}

void CHangerGWView::OnSendReleaseDataToSocket()
{
	SendToAllSocket(false, 99, m_nTestHangerID);
}

void CHangerGWView::OnSendSetDataToSocket()
{
	SendToAllSocket(true, 99, m_nTestHangerID);
}
