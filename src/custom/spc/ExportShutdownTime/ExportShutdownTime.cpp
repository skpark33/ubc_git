// ExportShutdownTime.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "ExportShutdownTime.h"
#include "ExportShutdownTimeDlg.h"

#include "HttpRequest.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExportShutdownTimeApp

BEGIN_MESSAGE_MAP(CExportShutdownTimeApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CExportShutdownTimeApp 생성

CExportShutdownTimeApp::CExportShutdownTimeApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CExportShutdownTimeApp 개체입니다.

CExportShutdownTimeApp theApp;


// CExportShutdownTimeApp 초기화

BOOL CExportShutdownTimeApp::InitInstance()
{
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("SQISoft"));

	CString ini_fullpath[4];
	ini_fullpath[0] = "C:\\VWC\\CDaemon.ini";
	ini_fullpath[1] = "D:\\VWC\\CDaemon.ini";

	char buf[MAX_PATH+1] = {0};
	::SHGetSpecialFolderPath (NULL, buf, CSIDL_DESKTOP, FALSE);
	if(strlen(buf) == 0)
	{
		ini_fullpath[2] = getenv("USERPROFILE");
		ini_fullpath[2] += "\\바탕 화면";
	}
	else
		ini_fullpath[2] = buf;
	ini_fullpath[2] += "\\VWC\\CDaemon.ini";

	bool find = false;
	for(int i=0; i<3; i++)
	{
		CFileStatus fs;
		if(!CFile::GetStatus(ini_fullpath[i], fs)) continue;

		::ZeroMemory(buf, sizeof(buf));
		::GetPrivateProfileString("CLIENTINFO", "CID", "", buf, MAX_PATH, ini_fullpath[i]);
		CString str_cid = buf;

		::ZeroMemory(buf, sizeof(buf));
		::GetPrivateProfileString("CLIENTINFO", "SHUTTIME", "", buf, MAX_PATH, ini_fullpath[i]);
		CString str_shutdowntime = buf;

		if(str_cid.GetLength() == 0 || str_shutdowntime.GetLength() == 0) continue;

		CString send_msg;
		send_msg.Format("cid=%s&stime=%s", str_cid, str_shutdowntime);

		CString response;
		CHttpRequest http_request;
		http_request.RequestPost("UBC_Server/set_shutdowntime.asp", send_msg, response);

		if(response == "OK")
		{
			CString msg;
			msg.Format("단말기의 종료시간이 \r\n[ %s ]으로 설정완료되었습니다.", str_shutdowntime);
			::AfxMessageBox(msg, MB_ICONINFORMATION);
		}
		else if(response == "NotExist")
		{
			::AfxMessageBox("단말기의 종료시간 설정중\r\n단말기의 ID가 서버에 존재하지 않습니다.\r\n\r\n반드시 전화연락 해주시기 바랍니다.", MB_ICONSTOP);
		}
		else
		{
			::AfxMessageBox("단말기의 종료시간 설정중 서버에 연결할 수가 없습니다.\r\n\r\n반드시 전화연락 해주시기 바랍니다.", MB_ICONSTOP);
		}

		find = true;
		break;
	}

	if(!find)
	{
		// 구버전의 SPC단말기는 없음 ==> 더이상 아래메시지를 띄울 필요없음 (by seventhstone 2014.05.08)
		//::AfxMessageBox("단말기의 종료시간 설정정보를 찾을 수 없습니다.\r\n\r\n반드시 전화연락 해주시기 바랍니다.", MB_ICONSTOP);
	}

	//CExportShutdownTimeDlg dlg;
	//m_pMainWnd = &dlg;
	//INT_PTR nResponse = dlg.DoModal();
	//if (nResponse == IDOK)
	//{
	//	// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
	//	//  코드를 배치합니다.
	//}
	//else if (nResponse == IDCANCEL)
	//{
	//	// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
	//	//  코드를 배치합니다.
	//}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}
