// HttpRequest.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HttpRequest.h"

#include <afxinet.h>

#include <string>
#include <map>
using namespace std;

#define		DEFAULT_SERVER_IP			_T("125.141.230.153")
#define		DEFAULT_SERVER_WEBPORT		8080

#define		HTTP_SESSION_TIMEOUT		(60*1000)	// 1min


// CHttpRequest

CHttpRequest::CHttpRequest()
:	m_connType ( CONNECTION_USER_DEFINED )
,	m_strServerIP ( DEFAULT_SERVER_IP )
,	m_nServerPort ( DEFAULT_SERVER_WEBPORT )
{
}

CHttpRequest::~CHttpRequest()
{
}

BOOL CHttpRequest::Init(CONNECTION_TYPE conType, LPCTSTR lpszServerIP, UINT nServerPort)
{
	m_connType = conType;

	switch(conType)
	{
	case CONNECTION_USER_DEFINED:
		m_strServerIP = (lpszServerIP ? lpszServerIP : DEFAULT_SERVER_IP);
		m_nServerPort = (nServerPort ? nServerPort : DEFAULT_SERVER_WEBPORT);
		break;

	//case CONNECTION_CENTER:
	//	{
	//		//ubcConfig aIni("UBCConnect");
	//		ubcIni aIni("UBCConnect", "", "data");
	//		ciString center_ip="", center_port="";

	//		//
	//		aIni.get("UBCCENTER", "IP", center_ip);
	//		m_strServerIP = ( center_ip.length()>0 ? center_ip.c_str() : DEFAULT_CENTER_IP );

	//		//
	//		aIni.get("UBCCENTER", "WEBPORT", center_port);
	//		m_nServerPort = ( center_port.length()>0 ? atoi(center_port.c_str()) : DEFAULT_CENTER_WEBPORT );
	//	}
	//	break;

	//case CONNECTION_SERVER:
	//	{
	//		//ubcConfig aIni("UBCConnect");
	//		ubcIni aIni("UBCConnect", "", "data");
	//		ciString server_ip="", server_port="";

	//		//
	//		aIni.get("ORB_NAMESERVICE", "IP", server_ip);
	//		m_strServerIP = ( server_ip.length()>0 ? server_ip.c_str() : DEFAULT_SERVER_IP );

	//		//
	//		aIni.get("AGENTMUX", "HTTP_PORT", server_port);
	//		m_nServerPort = ( server_port.length()>0 ? atoi(server_port.c_str()) : DEFAULT_SERVER_WEBPORT );
	//	}
	//	break;

	default:
		return FALSE;
		break;
	}
	return TRUE;
}

BOOL CHttpRequest::Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg)
{
	//
	if( _tcsnicmp( lpUrl, _T("http://"), 7) )
	{
		m_strRequestURL.Format(_T("http://%s:%d%s%s"), 
			m_strServerIP,
			m_nServerPort,
			(lpUrl[0] == _T('/') ? "" : _T("/")),
			ToEncodingString(lpUrl)
		);
	}
	else
		m_strRequestURL = lpUrl;

	//
	bool is_https = false;
	//if(m_connType != CONNECTION_CENTER) // center => only use http, not https!!!
	//{
	//	//ciString https;
	//	//if(ciIniUtil::HttpToHttps(m_strRequestURL, https))
	//	//{
	//	//	m_strRequestURL = https.c_str();
	//	//}
	//	CString https;
	//	if(HttpToHttps(m_strRequestURL, https))
	//	{
	//		is_https = true;
	//		m_strRequestURL = https;
	//	}
	//}

	//
	DWORD dwSearviceType;
	CString strServer, strObject;
	INTERNET_PORT nPort;

	if(!AfxParseURL(m_strRequestURL, dwSearviceType, strServer, strObject, nPort))
	{
		return FALSE;
	}

	CInternetSession Session;
	Session.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT,			HTTP_SESSION_TIMEOUT);
	Session.SetOption(INTERNET_OPTION_CONTROL_RECEIVE_TIMEOUT,	HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_CONTROL_SEND_TIMEOUT,		HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT,		HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT,		HTTP_SESSION_TIMEOUT );

	BOOL bRet = FALSE;
	DWORD dwReadSize;

	try
	{
		CHttpConnection* pServer = Session.GetHttpConnection(strServer, nPort);
		if(pServer==NULL) return FALSE;
		CBufferDeleter hc_deleter(pServer);

		DWORD flag;
		if(isUseSSL() && is_https)
			flag = INTERNET_FLAG_EXISTING_CONNECT | INTERNET_FLAG_SECURE;
		else
			flag = INTERNET_FLAG_EXISTING_CONNECT;

		CHttpFile *pFile = NULL;
		if(isGet)
		{
			CString strHeader = "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\nAccept: */*\r\n";
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_GET, strObject, NULL, 1, NULL, NULL, flag);
			if(pFile==NULL) return FALSE;
			pFile->SendRequest(strHeader, (LPVOID)(LPCTSTR)strHeader, strHeader.GetLength());
		}
		else
		{
			CString strHeader = "Content-Type: application/x-www-form-urlencoded\r\nUser-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject, NULL, 1, NULL, NULL, flag);
			if(pFile==NULL) return FALSE;
			pFile->SendRequest(strHeader, (LPVOID)lpszSendMsg, _tcslen(lpszSendMsg));
		}
		CBufferDeleter hf_deleter(pFile);

		//
		char szLen[32]="";
		DWORD dwLenSize = sizeof(szLen);
		pFile->QueryInfo( HTTP_QUERY_CONTENT_LENGTH, szLen, &dwLenSize );
		int length = atoi( szLen);

		TCHAR* buf = new TCHAR[length+1];
		CBufferDeleter buf_deleter(buf);
		::ZeroMemory(buf, sizeof(TCHAR)*(length+1));
		dwReadSize = pFile->Read(buf, length);

		//
		if(dwReadSize != strlen(buf) || buf[0] == '<')
		{
			bRet = FALSE;
			m_strErrorMsg = buf;
		}
		else
		{
			const char* utf8_idx = strstr(buf, "CodePage=utf-8");

			if( utf8_idx != NULL )
			{
				std::string out_msg;
//				UTF8toA(buf, out_msg);
				strOutMsg = out_msg.c_str();
			}
			else
				strOutMsg = buf;

			bRet = TRUE;
		}

		if(pFile) pFile->Close();
		if(pServer) pServer->Close();
	}
	catch (CInternetException* e)
	{
		TCHAR szError[255];
		e->GetErrorMessage(szError, 255);
		e->Delete();
		return bRet;
	}
	catch (CException* e)
	{
		TCHAR szError[255];
		e->GetErrorMessage(szError, 255);
		e->Delete();
		return bRet;
	}
	catch (...)
	{
		return bRet;
	}

	return bRet;
}

BOOL CHttpRequest::RequestGet(LPCTSTR lpUrl, CString &strOutMsg)
{
	return Request(true, lpUrl, NULL, strOutMsg);
}

BOOL CHttpRequest::RequestGet(LPCTSTR lpUrl, CStringArray &strOutMsgList)
{
	CString strOutMsg;
	BOOL ret_val = Request(true, lpUrl, NULL, strOutMsg);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

BOOL CHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg)
{
	return Request(false, lpUrl, lpszSendMsg, strOutMsg);
}

BOOL CHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CStringArray& strOutMsgList)
{
	CString strOutMsg;
	BOOL ret_val = Request(false, lpUrl, lpszSendMsg, strOutMsg);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

CString CHttpRequest::ToEncodingString(CString str, bool bReturnNullToString)
{
	if(str.GetLength() == 0) return (bReturnNullToString ? _T("(null)") : _T(""));
	str.Replace(_T("%"), _T("%25"));
	str.Replace(_T(" "), _T("%20"));
	str.Replace(_T("&"), _T("%26"));
	str.Replace(_T("="), _T("%3d"));
	str.Replace(_T("+"), _T("%2b"));
	str.Replace(_T("?"), _T("%3f"));
	return str;
}

CString CHttpRequest::ToString(int nValue)
{
	TCHAR buf[16] = {0};
	_stprintf(buf, _T("%d"), nValue);

	return buf;
}

void CHttpRequest::GetLineList(CString& str, CStringArray& line_list)
{
	int pos = 0;
	CString token = str.Tokenize(_T("\r\n"), pos);
	while(token != _T(""))
	{
		line_list.Add(token);
		token = str.Tokenize(_T("\r\n"), pos);
	}
}

#define UBC_SERVER_EXECUTE_PATH		_T("Project\\ubc\\config")
#define UBC_CLIENT_EXECUTE_PATH		_T("SQISoft\\UTV1.0\\execute")
#define UBCVARS_INI					_T("UBCVariables.ini")

CString	CHttpRequest::GetINIPath()
{
	TCHAR szModule[MAX_PATH+1] = {0};
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	CString program_path = szModule;
	program_path.MakeLower();

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath_s(szModule, szDrive, MAX_PATH, szPath, MAX_PATH, szFilename, MAX_PATH, szExt, MAX_PATH);

	CString strPath;
	if(program_path.Find("\\bin8") > 0)
	{
		// server
		strPath.Format("%s\\%s\\data\\%s", szDrive, UBC_SERVER_EXECUTE_PATH, UBCVARS_INI);
	}
	else
	{
		// client
		strPath.Format("%s\\%s\\data\\%s", szDrive, UBC_CLIENT_EXECUTE_PATH, UBCVARS_INI);
	}

	return strPath;
}

BOOL
CHttpRequest::isUseSSL()
{
	static int iUseSSL = -1;
	static httpCriticalSection criticalSection;

	criticalSection.lock();
	if(iUseSSL >= 0){
		criticalSection.unlock();
		return BOOL(iUseSSL);
	}

	CString strPath = GetINIPath();

	BOOL bUseSSL = GetPrivateProfileInt(_T("ROOT"), _T("USE_SSL"), 0, strPath);
	iUseSSL = bUseSSL;
	criticalSection.unlock();
	return bUseSSL;
}

BOOL
CHttpRequest::IpToDomainName(const char* strHttpServerIp, CString& domain)
{
	static map<string,string>	ipDomainMap;
	static httpCriticalSection criticalSection;
	
	criticalSection.lock();
	map<string,string>::iterator itr = ipDomainMap.find(strHttpServerIp);
	if(itr!=ipDomainMap.end()){
		domain = itr->second.c_str();
		criticalSection.unlock();
		return true;
	}

	CString strPath = GetINIPath();

	char buf[1024] = {0};
	DWORD retval = GetPrivateProfileString(_T("SSLDomainNameList"), strHttpServerIp, "", buf, 1024, strPath);
	if(retval > 0){
		domain = buf;
		ipDomainMap.insert(map<string,string>::value_type(strHttpServerIp, (const char*)domain.GetBuffer()));
	}else{
		domain = strHttpServerIp;
	}
	
	criticalSection.unlock();
	return (retval >0 ? true : false);
}

BOOL 
CHttpRequest::HttpToHttps(const char* http, CString& https)
{
	if(isUseSSL() == false) //
	{
		https = http;
		return false;
	}
	if(strncmp(http, "http://", 7) != 0)
	{
		https = http;
		return false;
	}

	char* ip_end_ptr = (char*)http+7; // ip-pointer after of "http://"
	while(*ip_end_ptr != ':' && *ip_end_ptr != '/' && *ip_end_ptr != 0) // find end char (:, /, NULL)
		ip_end_ptr++;
	char ip_end_char = *ip_end_ptr;

	*ip_end_ptr = 0;
	string ip = http+7;

	*ip_end_ptr = ip_end_char;
	string url = ip_end_ptr;

	CString domainName;
	if(IpToDomainName(ip.c_str(), domainName))
	{
		https = "https://";
		https += domainName;
		https += url.c_str();

		return true;
	}
	else
	{
		https = http;
	}

	return false;
}


//////////////////////////////////////////////////////////
// httpCriticalSection
//////////////////////////////////////////////////////////

httpCriticalSection::httpCriticalSection() {
	::InitializeCriticalSection(&_handle);
}

httpCriticalSection::~httpCriticalSection() {
	::DeleteCriticalSection(&_handle);
}

void httpCriticalSection::lock() {
	::EnterCriticalSection(&_handle);
}

void httpCriticalSection::unlock() {
	::LeaveCriticalSection(&_handle);
}
