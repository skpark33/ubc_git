del UBCStudioLauncher.cab
cabarc N UBCStudioLauncher.cab UBCStudioLauncher.ocx UBCStudioLauncher.inf UBCAXDownloader.exe
@echo -
@echo 비밀번호를 묻는 윈도우가 나온다. 비밀번호 입력후 기억해 두자. 
@echo -
makecert -sv UBCStudioLauncher.pvk -n "CN=UBC Studio Launcher OCX" UBCStudioLauncher.cer
cert2spc UBCStudioLauncher.cer UBCStudioLauncher.spc
@echo -
@echo 비밀번호를 묻는 윈도우가 나오며 위에서 입력한 비밀번호를 넣는다.
@echo -
signcode -v UBCStudioLauncher.pvk -spc UBCStudioLauncher.spc UBCStudioLauncher.cab
setreg -q 1 TRUE
