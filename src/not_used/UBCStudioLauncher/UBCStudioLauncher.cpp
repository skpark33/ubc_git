// UBCStudioLauncher.cpp : CUBCStudioLauncherApp 및 DLL 등록의 구현입니다.

#include "stdafx.h"
#include "UBCStudioLauncher.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CUBCStudioLauncherApp theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0xDA365AA7, 0xE069, 0x4E43, { 0xBE, 0x92, 0x65, 0xAA, 0xFD, 0x52, 0x55, 0x9E } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CUBCStudioLauncherApp::InitInstance - DLL 초기화입니다.
 
LONG WINAPI NoMsgExceptionFilter(struct _EXCEPTION_POINTERS* /*ExceptionInfo*/)
{
    // 아무일도 하지 않고 그냥 종료하기
    return EXCEPTION_EXECUTE_HANDLER;
}

BOOL CUBCStudioLauncherApp::InitInstance()
{
    ::SetUnhandledExceptionFilter(NoMsgExceptionFilter);
 
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: 여기에 직접 작성한 모듈 초기화 코드를 추가합니다.
	}

	return bInit;
}



// CUBCStudioLauncherApp::ExitInstance - DLL 종료입니다.

int CUBCStudioLauncherApp::ExitInstance()
{
	// TODO: 여기에 직접 작성한 모듈 종료 코드를 추가합니다.

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - 시스템 레지스트리에 항목을 추가합니다.

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - 시스템 레지스트리에서 항목을 제거합니다.

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
