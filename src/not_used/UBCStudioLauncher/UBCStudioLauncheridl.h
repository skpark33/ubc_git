

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Thu Sep 30 14:23:44 2010
 */
/* Compiler settings for .\UBCStudioLauncher.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __UBCStudioLauncheridl_h__
#define __UBCStudioLauncheridl_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef ___DUBCStudioLauncher_FWD_DEFINED__
#define ___DUBCStudioLauncher_FWD_DEFINED__
typedef interface _DUBCStudioLauncher _DUBCStudioLauncher;
#endif 	/* ___DUBCStudioLauncher_FWD_DEFINED__ */


#ifndef ___DUBCStudioLauncherEvents_FWD_DEFINED__
#define ___DUBCStudioLauncherEvents_FWD_DEFINED__
typedef interface _DUBCStudioLauncherEvents _DUBCStudioLauncherEvents;
#endif 	/* ___DUBCStudioLauncherEvents_FWD_DEFINED__ */


#ifndef __UBCStudioLauncher_FWD_DEFINED__
#define __UBCStudioLauncher_FWD_DEFINED__

#ifdef __cplusplus
typedef class UBCStudioLauncher UBCStudioLauncher;
#else
typedef struct UBCStudioLauncher UBCStudioLauncher;
#endif /* __cplusplus */

#endif 	/* __UBCStudioLauncher_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 


#ifndef __UBCStudioLauncherLib_LIBRARY_DEFINED__
#define __UBCStudioLauncherLib_LIBRARY_DEFINED__

/* library UBCStudioLauncherLib */
/* [control][helpstring][helpfile][version][uuid] */ 


EXTERN_C const IID LIBID_UBCStudioLauncherLib;

#ifndef ___DUBCStudioLauncher_DISPINTERFACE_DEFINED__
#define ___DUBCStudioLauncher_DISPINTERFACE_DEFINED__

/* dispinterface _DUBCStudioLauncher */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DUBCStudioLauncher;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("734C72F7-7D21-4CB2-9222-9BF34DB698A2")
    _DUBCStudioLauncher : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DUBCStudioLauncherVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DUBCStudioLauncher * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DUBCStudioLauncher * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DUBCStudioLauncher * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DUBCStudioLauncher * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DUBCStudioLauncher * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DUBCStudioLauncher * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DUBCStudioLauncher * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DUBCStudioLauncherVtbl;

    interface _DUBCStudioLauncher
    {
        CONST_VTBL struct _DUBCStudioLauncherVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DUBCStudioLauncher_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _DUBCStudioLauncher_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _DUBCStudioLauncher_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _DUBCStudioLauncher_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _DUBCStudioLauncher_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _DUBCStudioLauncher_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _DUBCStudioLauncher_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DUBCStudioLauncher_DISPINTERFACE_DEFINED__ */


#ifndef ___DUBCStudioLauncherEvents_DISPINTERFACE_DEFINED__
#define ___DUBCStudioLauncherEvents_DISPINTERFACE_DEFINED__

/* dispinterface _DUBCStudioLauncherEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DUBCStudioLauncherEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("9C13B041-FF04-4200-B050-AF78646AF28A")
    _DUBCStudioLauncherEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DUBCStudioLauncherEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DUBCStudioLauncherEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DUBCStudioLauncherEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DUBCStudioLauncherEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DUBCStudioLauncherEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DUBCStudioLauncherEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DUBCStudioLauncherEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DUBCStudioLauncherEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DUBCStudioLauncherEventsVtbl;

    interface _DUBCStudioLauncherEvents
    {
        CONST_VTBL struct _DUBCStudioLauncherEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DUBCStudioLauncherEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _DUBCStudioLauncherEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _DUBCStudioLauncherEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _DUBCStudioLauncherEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _DUBCStudioLauncherEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _DUBCStudioLauncherEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _DUBCStudioLauncherEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DUBCStudioLauncherEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_UBCStudioLauncher;

#ifdef __cplusplus

class DECLSPEC_UUID("F0773C7E-311C-45B0-9D2A-E7A88C271DD2")
UBCStudioLauncher;
#endif
#endif /* __UBCStudioLauncherLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


