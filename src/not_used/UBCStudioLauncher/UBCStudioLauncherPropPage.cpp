// UBCStudioLauncherPropPage.cpp : CUBCStudioLauncherPropPage 속성 페이지 클래스의 구현입니다.

#include "stdafx.h"
#include "UBCStudioLauncher.h"
#include "UBCStudioLauncherPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CUBCStudioLauncherPropPage, COlePropertyPage)



// 메시지 맵입니다.

BEGIN_MESSAGE_MAP(CUBCStudioLauncherPropPage, COlePropertyPage)
END_MESSAGE_MAP()



// 클래스 팩터리와 GUID를 초기화합니다.

IMPLEMENT_OLECREATE_EX(CUBCStudioLauncherPropPage, "UBCSTUDIOLAUNC.UBCStudioLauncPropPage.1",
	0x884bab2a, 0x5005, 0x42f7, 0x90, 0x4e, 0x96, 0x83, 0x83, 0xb, 0xda, 0x98)



// CUBCStudioLauncherPropPage::CUBCStudioLauncherPropPageFactory::UpdateRegistry -
// CUBCStudioLauncherPropPage에서 시스템 레지스트리 항목을 추가하거나 제거합니다.

BOOL CUBCStudioLauncherPropPage::CUBCStudioLauncherPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_UBCSTUDIOLAUNCHER_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// CUBCStudioLauncherPropPage::CUBCStudioLauncherPropPage - 생성자입니다.

CUBCStudioLauncherPropPage::CUBCStudioLauncherPropPage() :
	COlePropertyPage(IDD, IDS_UBCSTUDIOLAUNCHER_PPG_CAPTION)
{
}



// CUBCStudioLauncherPropPage::DoDataExchange - 페이지와 속성 사이에서 데이터를 이동시킵니다.

void CUBCStudioLauncherPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// CUBCStudioLauncherPropPage 메시지 처리기입니다.
