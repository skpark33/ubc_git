#pragma once

// UBCStudioLauncherPropPage.h : CUBCStudioLauncherPropPage 속성 페이지 클래스의 선언입니다.


// CUBCStudioLauncherPropPage : 구현을 보려면 UBCStudioLauncherPropPage.cpp을(를) 참조하십시오.

class CUBCStudioLauncherPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CUBCStudioLauncherPropPage)
	DECLARE_OLECREATE_EX(CUBCStudioLauncherPropPage)

// 생성자입니다.
public:
	CUBCStudioLauncherPropPage();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PROPPAGE_UBCSTUDIOLAUNCHER };

// 구현입니다.
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 메시지 맵입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

