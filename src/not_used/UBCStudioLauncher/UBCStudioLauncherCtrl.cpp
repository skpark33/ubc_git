// UBCStudioLauncherCtrl.cpp : CUBCStudioLauncherCtrl ActiveX 컨트롤 클래스의 구현입니다.

#include "stdafx.h"
#include "UBCStudioLauncher.h"
#include "UBCStudioLauncherCtrl.h"
#include "UBCStudioLauncherPropPage.h"


#include <afxinet.h>

#define		MAIN_SERVER_IP_ADDRESS		_T("211.232.57.202")
#define		UBC_AX_DOWNLOADER			_T("UBCAXDownloader.exe")


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CUBCStudioLauncherCtrl, COleControl)



// 메시지 맵입니다.

BEGIN_MESSAGE_MAP(CUBCStudioLauncherCtrl, COleControl)
	ON_MESSAGE(OCM_COMMAND, &CUBCStudioLauncherCtrl::OnOcmCommand)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()



// 디스패치 맵입니다.

BEGIN_DISPATCH_MAP(CUBCStudioLauncherCtrl, COleControl)
	DISP_FUNCTION_ID(CUBCStudioLauncherCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()



// 이벤트 맵입니다.

BEGIN_EVENT_MAP(CUBCStudioLauncherCtrl, COleControl)
END_EVENT_MAP()



// 속성 페이지입니다.

// TODO: 필요에 따라 속성 페이지를 추가합니다. 카운트도 이에 따라 증가해야 합니다.
BEGIN_PROPPAGEIDS(CUBCStudioLauncherCtrl, 1)
	PROPPAGEID(CUBCStudioLauncherPropPage::guid)
END_PROPPAGEIDS(CUBCStudioLauncherCtrl)



// 클래스 팩터리와 GUID를 초기화합니다.

IMPLEMENT_OLECREATE_EX(CUBCStudioLauncherCtrl, "UBCSTUDIOLAUNCHE.UBCStudioLauncheCtrl.1",
					   0xf0773c7e, 0x311c, 0x45b0, 0x9d, 0x2a, 0xe7, 0xa8, 0x8c, 0x27, 0x1d, 0xd2)



// 형식 라이브러리 ID와 버전입니다.

IMPLEMENT_OLETYPELIB(CUBCStudioLauncherCtrl, _tlid, _wVerMajor, _wVerMinor)



// 인터페이스 ID입니다.

const IID BASED_CODE IID_DUBCStudioLauncher =
{ 0x734C72F7, 0x7D21, 0x4CB2, { 0x92, 0x22, 0x9B, 0xF3, 0x4D, 0xB6, 0x98, 0xA2 } };
const IID BASED_CODE IID_DUBCStudioLauncherEvents =
{ 0x9C13B041, 0xFF04, 0x4200, { 0xB0, 0x50, 0xAF, 0x78, 0x64, 0x6A, 0xF2, 0x8A } };



// 컨트롤 형식 정보입니다.

static const DWORD BASED_CODE _dwUBCStudioLauncherOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CUBCStudioLauncherCtrl, IDS_UBCSTUDIOLAUNCHER, _dwUBCStudioLauncherOleMisc)



// CUBCStudioLauncherCtrl::CUBCStudioLauncherCtrlFactory::UpdateRegistry -
// CUBCStudioLauncherCtrl에서 시스템 레지스트리 항목을 추가하거나 제거합니다.

BOOL CUBCStudioLauncherCtrl::CUBCStudioLauncherCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: 컨트롤이 아파트 모델 스레딩 규칙을 준수하는지
	// 확인합니다. 자세한 내용은 MFC Technical Note 64를
	// 참조하십시오. 컨트롤이 아파트 모델 규칙에
	// 맞지 않는 경우 다음에서 여섯 번째 매개 변수를 변경합니다.
	// afxRegApartmentThreading을 0으로 설정합니다.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_UBCSTUDIOLAUNCHER,
			IDB_UBCSTUDIOLAUNCHER,
			afxRegApartmentThreading,
			_dwUBCStudioLauncherOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// CUBCStudioLauncherCtrl::CUBCStudioLauncherCtrl - 생성자입니다.

CUBCStudioLauncherCtrl::CUBCStudioLauncherCtrl()
{
	InitializeIIDs(&IID_DUBCStudioLauncher, &IID_DUBCStudioLauncherEvents);
	// TODO: 여기에서 컨트롤의 인스턴스 데이터를 초기화합니다.
}



// CUBCStudioLauncherCtrl::~CUBCStudioLauncherCtrl - 소멸자입니다.

CUBCStudioLauncherCtrl::~CUBCStudioLauncherCtrl()
{
	// TODO: 여기에서 컨트롤의 인스턴스 데이터를 정리합니다.
}



// CUBCStudioLauncherCtrl::OnDraw - 그리기 함수입니다.

void CUBCStudioLauncherCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	if (!pdc)
		return;

	DoSuperclassPaint(pdc, rcBounds);

	CFont font;
	font.CreatePointFont(9*10, _T("Gulim"));

	CRect rect = rcBounds;
	CString str = _T("스튜디오");
	pdc->SetBkMode(TRANSPARENT);
	CFont* old_font = pdc->SelectObject(&font);
	pdc->DrawText(str, rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	pdc->SelectObject(old_font);
}



// CUBCStudioLauncherCtrl::DoPropExchange - 지속성 지원입니다.

void CUBCStudioLauncherCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: 지속적인 사용자 지정 속성 모두에 대해 PX_ functions를 호출합니다.
}



// CUBCStudioLauncherCtrl::OnResetState - 컨트롤을 기본 상태로 다시 설정합니다.

void CUBCStudioLauncherCtrl::OnResetState()
{
	COleControl::OnResetState();  // DoPropExchange에 들어 있는 기본값을 다시 설정합니다.

	// TODO: 여기에서 다른 모든 컨트롤의 상태를 다시 설정합니다.
}



// CUBCStudioLauncherCtrl::AboutBox - "정보" 대화 상자를 사용자에게 보여 줍니다.

void CUBCStudioLauncherCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_UBCSTUDIOLAUNCHER);
	dlgAbout.DoModal();
}



// CUBCStudioLauncherCtrl::PreCreateWindow - CreateWindowEx의 매개 변수를 수정합니다.

BOOL CUBCStudioLauncherCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.lpszClass = _T("BUTTON");
	return COleControl::PreCreateWindow(cs);
}



// CUBCStudioLauncherCtrl::IsSubclassedControl - 서브클래싱된 컨트롤입니다.

BOOL CUBCStudioLauncherCtrl::IsSubclassedControl()
{
	return TRUE;
}



// CUBCStudioLauncherCtrl::OnOcmCommand - 핸들 명령 메시지입니다.

LRESULT CUBCStudioLauncherCtrl::OnOcmCommand(WPARAM wParam, LPARAM lParam)
{
#ifdef _WIN32
	WORD wNotifyCode = HIWORD(wParam);
#else
	WORD wNotifyCode = HIWORD(lParam);
#endif

	// TODO: 여기에서 wNotifyCode로 전환합니다.

	switch (wNotifyCode)
	{
	case BN_CLICKED:
		// Fire click event when button is clicked
		//serve il custom altrimenti lo spazio non viene processato
		FireClick();

		//
		{
			CStringA strIpAddr="", strID="", strPW="";
			CString url;
			url.Format(_T("http://%s:8080/ubc_get_ax_down_info.asp?SvrIP=1&ID=1&PW=1"), MAIN_SERVER_IP_ADDRESS );

			CInternetSession is;
			try 
			{
				CHttpFile* file = (CHttpFile*)is.OpenURL(url, 1, INTERNET_FLAG_TRANSFER_BINARY | INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE);
				if(file)
				{
					char info[1024] = {0};

					int idx = 0;
					char buf[128] = {0};
					int read_size = file->Read(buf, 128);
					while(read_size != 0 && idx+read_size < 1023)
					{
						memcpy(info+idx, buf, read_size);
						idx += read_size;
						::ZeroMemory(buf, sizeof(buf));
						read_size = file->Read(buf, 128);
					}

					CStringA strInfo = info;
					strInfo.TrimLeft(" ");
					strInfo.TrimRight(" ");
					int tab_count = 0;
					for(int i=0; i<strInfo.GetLength(); i++)
					{
						char tch = strInfo.GetAt(i);
						if( tch == '\r')
						{
							tab_count++;
							continue;
						}
					}
					if(tab_count == 2)
					{
						int pos = 0;
						strIpAddr = strInfo.Tokenize("\r\n", pos);
						strID = strInfo.Tokenize("\r\n", pos);
						strPW = strInfo.Tokenize("\r\n", pos);
					}
				}
			}
			catch(CInternetException* ex)
			{
				ex->Delete();
			}

			if(strIpAddr.GetLength() != 0 && strID.GetLength() != 0 && strPW.GetLength() != 0 )
			{
				TCHAR szSysPath[MAX_PATH] = {0};
				SHGetFolderPath(NULL, CSIDL_SYSTEM, NULL, 0, szSysPath);

				CString full_path;
				full_path.Format(_T("%s\\%s \"%s\" \"%s\" \"%s\""), szSysPath, UBC_AX_DOWNLOADER, strIpAddr, strID, strPW);

				// find -> run
				STARTUPINFO si;
				PROCESS_INFORMATION pi;

				::ZeroMemory( &si, sizeof(si) );
				si.cb = sizeof(si);
				::ZeroMemory( &pi, sizeof(pi) );

				if( !CreateProcess( NULL,   // No module name (use command line)
					(LPTSTR)(LPCTSTR)full_path,        // Command line
					NULL,           // Process handle not inheritable
					NULL,           // Thread handle not inheritable
					FALSE,          // Set handle inheritance to FALSE
					0,              // No creation flags
					NULL,           // Use parent's environment block
					NULL,           // Use parent's starting directory 
					&si,            // Pointer to STARTUPINFO structure
					&pi )           // Pointer to PROCESS_INFORMATION structure
					) 
				{
					// fail
					::MessageBox(NULL, _T("UBC Active-X Downloader를 찾을 수 없습니다."), _T("UBC Studio Launcher"), MB_ICONSTOP);
				}
				else
				{
					// Close process and thread handles. 
					CloseHandle( pi.hProcess );
					CloseHandle( pi.hThread );
				}
			}
		}
		//

		//questo codice viene chiamato prima del messaggio di default della super-class
		break;
	}

	return 0;
}



// CUBCStudioLauncherCtrl 메시지 처리기입니다.
