// mfcSampleDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "mfcSample.h"
#include "mfcSampleDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CmfcSampleDlg 대화 상자




CmfcSampleDlg::CmfcSampleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CmfcSampleDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CmfcSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_URL, m_url);
	DDX_Control(pDX, IDC_EDIT_SRCDIR, m_srcdir);
	DDX_Control(pDX, IDC_EDIT_TARGETDIR, m_targetdir);
	DDX_Control(pDX, IDC_FILENAME, m_filename);
	DDX_Control(pDX, IDC_EDIT_MESSAGE, m_message);
	DDX_Control(pDX, ID_STOP_DOWNLOAD, m_btStop);
	DDX_Control(pDX, ID_START_DOWNLOAD, m_btDownload);
}

BEGIN_MESSAGE_MAP(CmfcSampleDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CmfcSampleDlg::OnBnClickedOk)
	ON_BN_CLICKED(ID_START_DOWNLOAD, &CmfcSampleDlg::OnBnClickedStartDownload)
	ON_BN_CLICKED(ID_STOP_DOWNLOAD, &CmfcSampleDlg::OnBnClickedStopDownload)
END_MESSAGE_MAP()


// CmfcSampleDlg 메시지 처리기

BOOL CmfcSampleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.


	::SetWindowTextA(this->GetSafeHwnd(), "Download Tester");

	// TODO: 여기에 추가 초기화 작업을 추가합니다.


	m_url.SetWindowTextA("http://211.232.57.163:8080");
	m_srcdir.SetWindowTextA("announce");
	m_targetdir.SetWindowTextA("C:\\Project\\Temp");
	m_filename.SetWindowTextA("foo_baby1.avi");

	m_download = new CHttpDownload(this);
	m_download->InternetOpen();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CmfcSampleDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CmfcSampleDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CmfcSampleDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

bool CmfcSampleDlg::HTTPFileDownload() 
{
	CString debugMsg;

	////------------------------------------------------------------------------------
	 //// 다운받을 HTTP 경로 정보들
	LPSTR lpHTTPAddress = "211.232.57.163:8080";            // 접속 IP
	LPSTR lpHTTPDirectory = "";//"UBC_HTTP/Contents";     // 접속 IP의 하위 경로
	LPSTR lpFileName = "8.jpg";                 // 다운받을 파일 이름
	 
    char szCurrDir[MAX_PATH]="";
    GetCurrentDirectory( MAX_PATH, szCurrDir );
	 
	// 다운받을 파일을 저장할 위치
	CString lpDestPath =szCurrDir; 	
	lpDestPath.Append("\\");
	lpDestPath.Append(lpFileName);

	 ////------------------------------------------------------------------------------
	 
	 // 인터넷 연결
	 HINTERNET hInternet;
	 
	 if(!(hInternet = ::InternetOpen("HTTP Download", INTERNET_OPEN_TYPE_PRECONFIG , NULL, NULL, NULL))){
		 AfxMessageBox("Error : Network connection failed", MB_OK);
 		 return false;
	 }
	 
	 // URL (파일 주소 연결)
	 
	 HINTERNET hOpenURL;
	 char szDownFileURL_[MAX_PATH] = "HTTP://";
	 strcat(szDownFileURL_,lpHTTPAddress);
	 strcat(szDownFileURL_,"/");
	 strcat(szDownFileURL_,lpHTTPDirectory);
	 strcat(szDownFileURL_,"/");
	 strcat(szDownFileURL_,lpFileName);
	 
	 if(!(hOpenURL = InternetOpenUrl( hInternet, szDownFileURL_, NULL, 0, INTERNET_FLAG_RELOAD, 0 )))
	 {
		debugMsg.Format("Error : Open URL(%s) failed ", szDownFileURL_);
		AfxMessageBox(debugMsg, MB_OK);
		InternetCloseHandle(hOpenURL);
		InternetCloseHandle(hInternet);
		return false;
	 }
	 
	 // 연결정보 확인
	 
	 TCHAR szStatusCode[20];
	 DWORD dwCodeSize = 20;
	 HttpQueryInfoA(hOpenURL, HTTP_QUERY_STATUS_CODE, szStatusCode, &dwCodeSize, NULL);
	 long nStatusCode = _ttol(szStatusCode);
	 
	 if (nStatusCode != HTTP_STATUS_OK) // 정상 적으로 연결안됨
	 {    
		debugMsg.Format("%d Error : URL(%s) not found", nStatusCode, szDownFileURL_);
		AfxMessageBox(debugMsg, MB_OK);
		InternetCloseHandle(hOpenURL);
		InternetCloseHandle(hInternet);
		return false;
	 }

	// 파일용량 확인
	TCHAR szFileSizeBuff[20];
	DWORD dwBuffSize = 20;

	HttpQueryInfo(hOpenURL, HTTP_QUERY_CONTENT_LENGTH, szFileSizeBuff, &dwBuffSize, NULL);
	DWORD dwFileSize = atol((const char*)szFileSizeBuff);  // unsigned long 으로 변환 필요...

	debugMsg.Format("Server File(%s) size = %ld", lpFileName, dwFileSize);
	AfxMessageBox(debugMsg, MB_OK);
	 
	// 다운 받을 파일이 있으면 0, 파일이 없으면 -1;
	int result = ::access(lpFileName,0);
	 
	bool filematch = FALSE;
	if(result == 0) // 다운받을 파일이 이미 있다면
	{
		// 이미 있는 파일용량을 확인해 모두 받아 졌는지 검사
		WIN32_FIND_DATA fi;
		FindFirstFileA(lpDestPath,&fi);
		DWORD LocalFilesize = fi.nFileSizeHigh + fi.nFileSizeLow;
	 
		if(dwFileSize == LocalFilesize) //받은 파일 용량이 받을 파일 용량과 같을 경우 
		{
			 filematch = TRUE;
	  		 debugMsg.Format("Local File Size: %d\n == Server File Size: %d\n",LocalFilesize,dwFileSize);
		}
		else // 파일 용량이 다를경우
		{
	  		 debugMsg.Format("Local File Size: %d\n != Server File Size: %d\n",LocalFilesize,dwFileSize);
		}
		AfxMessageBox(debugMsg, MB_OK);
	}
	 

	if(filematch) // 이미 완료된 파일
	{
		debugMsg.Format("Server File(%s) already downloaded", lpFileName);
		AfxMessageBox(debugMsg, MB_OK);
		InternetCloseHandle(hOpenURL);
		InternetCloseHandle(hInternet);
		return true;
	}	 
	
	// 패일 생성
	HANDLE hFile = CreateFile( lpDestPath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
	if (hFile==INVALID_HANDLE_VALUE)
	{
		debugMsg.Format("Server File(%s) already downloaded", lpFileName);
		AfxMessageBox(debugMsg, MB_OK);

		InternetCloseHandle(hOpenURL);
		InternetCloseHandle(hInternet);
		return false;
	}
	 
	INTERNET_BUFFERS ib;
	char *m_pBuffer[1024] = {NULL}; // Read form file in 1k chunks
	 
	//ResetBuffer;
	memset(&ib, 0, sizeof(INTERNET_BUFFERS));
	ib.dwStructSize = sizeof(INTERNET_BUFFERS);
	ib.lpvBuffer = m_pBuffer;
	ib.dwBufferLength = 1024;
	 
	DWORD dwWritten=0;
	DWORD Recv = 0;
	BOOL bOk = TRUE;
	 
   // Data 다운로드
	do
	{
		bOk = InternetReadFileEx( hOpenURL, &ib, NULL, NULL);
		if(!bOk && GetLastError()==ERROR_IO_PENDING)
		{ 
			bOk = TRUE;  
		}   
		// 읽은 파일 쓰기
		WriteFile( hFile, ib.lpvBuffer, ib.dwBufferLength, &dwWritten, NULL );
		// 다운받은 양
		Recv += dwWritten;
	}while( bOk && (ib.dwBufferLength !=0 )  );
	CloseHandle(hFile);

	if(Recv != dwFileSize){
		debugMsg.Format("ERROR : %s file Transfer fail : Recv size=%ld != File size=%ld", lpFileName, Recv, dwFileSize);
	}else{
		debugMsg.Format("SUCCEES : %s file Transfer complete : Recv size=%ld == File size=%ld", lpFileName, Recv, dwFileSize);
	}
	AfxMessageBox(debugMsg, MB_OK);
	 
	InternetCloseHandle(hOpenURL);
	InternetCloseHandle(hInternet);
	return true;

}

void CmfcSampleDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//HTTPFileDownload();
	OnOK();
}

void CmfcSampleDlg::OnBnClickedStartDownload()
{
	m_message.SetWindowText("");

	m_btDownload.EnableWindow(false);
	m_btStop.EnableWindow(true);


	CString url;
	m_url.GetWindowText(url);

	CString sourceDir;
	m_srcdir.GetWindowText(sourceDir);

	CString targetDir;
	m_targetdir.GetWindowText(targetDir);

	CString filename;
	m_filename.GetWindowText(filename);

	m_download->SetURL(url);
	m_download->SetSourceDir(sourceDir);
	m_download->SetTargetDir(targetDir);
	m_download->DownloadFile(filename);
	
}

void CmfcSampleDlg::OnBnClickedStopDownload()
{
	m_btDownload.EnableWindow(true);
	m_btStop.EnableWindow(false);
	m_download->Stop();

}


void 
CmfcSampleDlg::OnConnectError(int errCode, CString errMsg)
{
	this->m_message.SetWindowTextA(errMsg);
	m_btDownload.EnableWindow(true);
	m_btStop.EnableWindow(false);
}

void 
CmfcSampleDlg::OnBeforeDownload(CString filename, unsigned long srcFileSize)
{
	CString msg;
	msg.Format("Filename=%s,source file size=%u",  filename, srcFileSize);
	this->m_message.SetWindowTextA(msg);
}

void 
CmfcSampleDlg::OnDownload(CString filename, unsigned long srcFileSize, unsigned long recvFileSize)
{
	CString msg;
	msg.Format("Filename=%s, source file size=%u, received file size=%u", filename, srcFileSize, recvFileSize);
	this->m_message.SetWindowTextA(msg);

}

void 
CmfcSampleDlg::OnAfterDownload(int errCode, CString errMsg, CString filename, 
							   unsigned long srcFileSize, unsigned long recvFileSize)
{
	CString msg ;
	msg.Format("%s, Filename=%s,\\nsource file size=%u, received file size=%u", 
		errMsg, filename, srcFileSize, recvFileSize);
	this->m_message.SetWindowTextA(msg);

	m_btDownload.EnableWindow(true);
	m_btStop.EnableWindow(false);
}
