// mfcSampleDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include <libHttpRequest/HttpDownload.h>


// CmfcSampleDlg 대화 상자
class CmfcSampleDlg : public CDialog, public IHttpDownloadUI
{
// 생성입니다.
public:
	CmfcSampleDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MFCSAMPLE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	bool	HTTPFileDownload();

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedStartDownload();
	afx_msg void OnBnClickedStopDownload();

	CEdit m_url;
	CEdit m_srcdir;
	CEdit m_targetdir;
	CEdit m_filename;
	CEdit m_message;
	CButton m_btStop;
	CButton m_btDownload;

	CHttpDownload*	m_download;

	// from IHttpDownloadUI
	virtual void OnConnectError(int errCode, CString errMsg) ;
	virtual void OnBeforeDownload(CString filename, unsigned long srcFileSize);
	virtual void OnDownload(CString filename, 
		unsigned long srcFileSize, unsigned long recvFileSize);
	virtual void OnAfterDownload(int errCode, CString errMsg, CString filename, 
		unsigned long srcFileSize, unsigned long recvFileSize);

};
