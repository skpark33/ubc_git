 /*! \file xmlSampleWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _xmlSampleWorld_h_
#define _xmlSampleWorld_h_


#include <cci/libWorld/cciORBWorld.h> 


class xmlSampleWorld : public cciORBWorld {
public:
	xmlSampleWorld();
	~xmlSampleWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

protected:	

	static int test1(char* arg);
	
	

};
		
#endif //_xmlSampleWorld_h_
