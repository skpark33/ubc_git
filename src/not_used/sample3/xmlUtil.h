#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>

#include "libxml/xmlreader.h"


class xmlData
{
public:
	ciString classname;
	ciStringMap	nvMap;
	void clear() {
		nvMap.clear();
		classname="";
	}
};

typedef list<xmlData>	xmlDataList;

class xmlUtil {
public:
	xmlUtil();
	virtual ~xmlUtil() ;

	ciBoolean open(const char* buffer, int len);
	void close();

	virtual int makeQuery();
	virtual int executeQuery(const char* classname);
	virtual ciBoolean	getValue(const char* classname, const char* attrname, ciString& outVal);

	int read()			{	return xmlTextReaderRead(_reader);}
	int depth()			{	return xmlTextReaderDepth(_reader);}
	int nodeType()		{	return xmlTextReaderNodeType(_reader);}
	xmlChar * name()	{	return xmlTextReaderName(_reader);}
	xmlChar * value()	{	return xmlTextReaderValue(_reader);}
	int  hasValue()		{	return xmlTextReaderHasValue(_reader);}


protected:
	xmlTextReaderPtr _reader;
	xmlDataList		_dataList;
};

