#include "xmlSampleWorld.h"

int main(int argc, char** argv)
{
    
	//ciEnv::defaultEnv(ciFalse, "utv1");

	xmlSampleWorld* world = new xmlSampleWorld();

    if(!world) {
        cerr<<"Unable to create xmlSampleWorld"<<endl;
        exit(1);
    }

    if(world->init(argc, argv) == ciFalse) {
        cerr<<"Init world error"<<endl;
        world->fini(2);
    }
    
	world->run();
    world->fini(0);
	return 0;
}

