 /*! \file netImporterWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _netImporterWorld_h_
#define _netImporterWorld_h_

#include <ci/libWorld/ciWorld.h> 

class netImporterWorld : public ciWorld {
public:
	netImporterWorld();
	~netImporterWorld();
 	
	//[
	//	From ciWorld
	//
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode=0);
	//
	//]

	ciBoolean	tcpServer(int port);
	ciBoolean	parse(const char* command,ciString& res);
	ciBoolean	import(string& scheduleFile);
	ciBoolean	shutdownTime(string& timeStr);
	ciBoolean	firmwareView(const char* directive, const char* data, ciString& res);
	ciBoolean	keyboardSimulator(const char* command, const char* data);
	
protected:	
	string		_curTimeStr();
	int			_kbdEvent(const char* value, DWORD flag);
	int			_getFocus();

	ciBoolean	_setData(ciString& data);
	const char*	_getData(const char* name);

	ciBoolean	_readXml();
	ciBoolean	_writeXml();
	ciBoolean	_isEOLXml(const char* xmlLine);
	ciBoolean	_channelModify(const char* ch, const char* channelName);
	ciBoolean	_channelAdd(const char* channelName,
							  const char* siteName,
							  const char* desc,
							  const char* networkUse);
	ciBoolean	_refresh();
	ciBoolean	_getIniInfo(const char* channelName,
							  ciString& out_siteId,ciString& out_desc, ciString& out_networkUse);

	void myLogOpen(const char *filename=0) ;
	void myLogClose() ;
	void myDEBUG(const char *pFormat,...); 
	void myERROR(const char *pFormat,...) ;
	void myWARN(const char *pFormat,...); 

	ciStringMap	_dataMap;
	ciString   _xmlBuf;

	ofstream	_log;
	ciBoolean	_hasLog;

};
		
#endif //_netImporterWorld_h_
