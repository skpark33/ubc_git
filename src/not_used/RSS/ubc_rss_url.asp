<%
Dim url
url = Request.QueryString("url")
Dim width, height
width = Request.QueryString("width")
height = Request.QueryString("height")
%>
<%
Dim title
title = url
Set xmlObj = Server.CreateObject("Microsoft.XMLDOM")
with xmlObj
	.async = False
	.setProperty "ServerHTTPRequest", True
	.Load(url)
end with

Set Nodes = xmlObj.getElementsByTagName("channel")
For each SubNodes in Nodes
	title = SubNodes.getElementsByTagName("title")(0).Text
Next
Set xmlObj = Nothing
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="refresh" content="300">
<title>UBC RSS</title>
<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAARneaVwrkjKHYWKGmGyM-shTQOUl_P1lVf9siWTl_L-13L79bOhRXcSK9AaDXpNLcRefFJ6lZo0dcPg"></script>
<script type="text/javascript" src="gfeedfetcher.js"></script>
<script type="text/javascript" src="gajaxscroller.js">
/***********************************************
* gAjax RSS Pausing Scroller- (c) Dynamic Drive (www.dynamicdrive.com)
* Requires "gfeedfetcher.js" class
* This notice MUST stay intact for legal use
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/
</script>
<style type="text/css">
body {margin-top:0; margin-left:0;}
A.title:link {font-family:tahoma;font-size:16pt;color:black; text-decoration: none}
A.title:visited {font-family:tahoma;font-size:16pt;color:black; text-decoration: none} 
A.title:active {font-family:tahoma;font-size:16pt;color:black;;}
A.title:hover {font-family:tahoma;font-size:16pt;color:black;; text-decoration:underline}
  
.titlefield{ /*CSS for RSS title link in general*/
text-decoration: none;
font-family:tahoma;
font-size:16pt;
color:black;
}
.titlefield:hover{ /*CSS for RSS title link in general*/
text-decoration: underline;
font-family:tahoma;
font-size:16pt;
color:#390;
}
.labelfield{ /*CSS for label field in general*/
color:brown;
font-family:tahoma;
font-size:16pt;
}
.datefield{ /*CSS for date field in general*/
color:gray;
font-family:tahoma;
font-size:16pt;
}

#ubcrss { /*main container*/
width: 740px;
height: 90px;
border: 0px;
padding: 0px;
}

#ubcrss div ul{ /*UL container*/
margin: 0;
padding-left: 16pt;
}

#ubcrss div ul li{ /*LI that surrounds each entry*/
margin-bottom: 4px;
}

#ubcrss div p{ /*P element that separates each entry*/
margin-top: 0;
margin-bottom: 7px;
}

#ubcrss div { /*P element that separates each entry*/
margin-top: 0;
margin-bottom: 0px;
}

code{ /*CSS for insructions*/
color: red;
}
</style>
<script>
/* ƫ�� �ڽ� 
   by junsung park */
var bTooltip = 
{
 tip_id : "btooltip",
 makeLayer : function (str)
 {  
  var tip_obj = document.getElementById(this.tip_id);
  if (tip_obj==undefined)
  {
   var tip = document.createElement("div");
   tip.id = this.tip_id;
   tip.style.backgroundColor= "#FFFFE1";
   tip.style.fontFamily= "����";
   tip.style.fontSize= "12px";
   tip.style.padding= "2px";
   tip.style.color= "#000";
   tip.style.border= "1px solid #000";
   tip.style.position= "absolute";
   tip.style.zIndex= "99999";
   tip.style.display= "none";
  
   document.body.appendChild(tip);
   tip.appendChild(document.createTextNode(str));
  }
 },
 moveLayer : function (e)
 {
  var xp = 0, yp = 0;
  if (document.all)  
  {
   xp = event.clientX + document.documentElement.scrollLeft;
   yp = event.clientY + document.documentElement.scrollTop;
  }else
  {
   xp = e.pageX; 
   yp = e.pageY; 
  }
  xp += 10;
  yp += 10;
  var tip_obj = document.getElementById(this.tip_id);
  if (tip_obj!=undefined)
  {
   tip_obj.style.top = yp + "px";
   tip_obj.style.left = xp + "px";
   tip_obj.style.display= "block";
  }
 },
 hiddenLyaer : function (e)
 {
  var to = e?e.relatedTarget:event.toElement; 
  var to_id = null;  
  try
  {
   to_id = to.id;
  }
  catch (e)
  {
   to_id = null;
  }
  if (this.tip_id==to_id)
  return;
  if (document.all) 
  {
   try
   {
    document.getElementById(this.tip_id).removeNode(true); 
   }
   catch (e){}   
  }else
  {
   try
   {
    document.body.removeChild(document.getElementById(this.tip_id));
   }
   catch (e){} 
  }
 },
 on : function (tg)
 {
  var str = tg.title;
  tg.title= "";
  bTooltip.makeLayer(str);
  tg.onmouseover = new Function("bTooltip.makeLayer('" + str + "')");
  tg.onmousemove = new Function("bTooltip.moveLayer(arguments[0])");
  tg.onmouseout = new Function("bTooltip.hiddenLyaer(arguments[0])");
 }
}
</script>
</head>

<body bgcolor="FFCADD"> 
<table border="0">
<tr><td>
<table>
<tr>
<td valign="top" width=142>
<a href='<%=url%>' class="title" onmouseover="bTooltip.on(this)", title="<%=title%>">
<b><%=Left(title,6)%>...</b>
</a>
</td>
<td valign="top">
<img src="btn.gif"> 
</td>
<td>
<script type="text/javascript">
var newsfeed=new gfeedpausescroller("ubcrss", "UBC_RSSclass", 2500, "_new")
newsfeed.addFeed("<%=title%>", "<%=url%>") //Specify "label" plus URL to RSS feed
//newsfeed.displayoptions("datetime snippet") //show the specified additional fields
//newsfeed.displayoptions("datetime") //show the specified additional fields
newsfeed.setentrycontainer("div") //Display each entry as a paragraph
newsfeed.filterfeed(20, "datetime") //Show 50 entries, sort by date
newsfeed.entries_per_page(2)
newsfeed.init() //Always call this last
</script>
</td>
</tr>
</table>
</td></tr>
</table>
</body>
</html>
