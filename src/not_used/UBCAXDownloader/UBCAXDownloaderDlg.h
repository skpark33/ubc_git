// UBCAXDownloaderDlg.h : 헤더 파일
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CUBCAXDownloaderDlg 대화 상자
class CUBCAXDownloaderDlg : public CDialog
{
// 생성입니다.
public:
	CUBCAXDownloaderDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCAXDOWNLOADER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

	bool			m_bRunThread;
	CWinThread*		m_pThread;
	void			RemoveThread();

	bool			RunStudio();

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnUpdateDownRange(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateDownSize(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDownloadComplete(WPARAM wParam, LPARAM lParam);

	CStatic			m_stcFilename;
	CProgressCtrl	m_pgCurrent;
	CProgressCtrl	m_pgTotal;
};

//
UINT DownloadThreadFunc(LPVOID pParam);

typedef struct {
	CDialog*	pDlg;
	bool*		pRunThread;
} DOWNLOAD_PARAM;

//
typedef struct {
	CString		strURL;			// url
	CString		strLocalPath;	// 경로만
	CString		strFileName;	// 파일이름
	ULONGLONG	nSize;			// 파일크기
	bool		bDownComplete;	// 다운완료 플래그
} DOWNLOAD_ITEM;

typedef		CArray<DOWNLOAD_ITEM, DOWNLOAD_ITEM&>		DOWNLOAD_ITEM_LIST;

BOOL CreateShortcut(LPCTSTR strPathObj, LPCTSTR strPathLink, LPCTSTR ShortcutTitle, LPCTSTR strDesc, LPCTSTR strIconPath);
