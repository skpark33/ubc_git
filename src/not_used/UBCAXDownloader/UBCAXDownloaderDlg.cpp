// UBCAXDownloaderDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCAXDownloader.h"
#include "UBCAXDownloaderDlg.h"

#include <io.h>
#include "Dbghelp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		PROJECT_HOME_PATH		_T("C:\\SQISoft")

//#define		SERVER_IP				_T("211.232.57.202")
//#define		SERVER_ID				_T("ent_center")
//#define		SERVER_PW				_T("rjtlrl2009")
extern CString		SERVER_IP;
extern CString		SERVER_ID;
extern CString		SERVER_PW;

#define			WM_UPDATE_DOWN_RANGE	(WM_USER + 1025)
#define			WM_UPDATE_DOWN_SIZE		(WM_USER + 1026)
#define			WM_DOWNLOAD_COMPLETE	(WM_USER + 1027)


ULONGLONG		g_nCurrentSize = 0;
ULONGLONG		g_nCurrentDownSize = 0;
ULONGLONG		g_nTotalSize = 0;
ULONGLONG		g_nTotalDownSize = 0;
CString			g_strFilename;


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUBCAXDownloaderDlg 대화 상자




CUBCAXDownloaderDlg::CUBCAXDownloaderDlg(CWnd* pParent /*=NULL*/)
: CDialog(CUBCAXDownloaderDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_pThread = NULL;
	m_bRunThread = false;
}

void CUBCAXDownloaderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_CURRENT, m_pgCurrent);
	DDX_Control(pDX, IDC_PROGRESS_TOTAL, m_pgTotal);
	DDX_Control(pDX, IDC_STATIC_FILENAME, m_stcFilename);
}

BEGIN_MESSAGE_MAP(CUBCAXDownloaderDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_MESSAGE(WM_UPDATE_DOWN_RANGE, OnUpdateDownRange)
	ON_MESSAGE(WM_UPDATE_DOWN_SIZE, OnUpdateDownSize)
	ON_MESSAGE(WM_DOWNLOAD_COMPLETE, OnDownloadComplete)
END_MESSAGE_MAP()


// CUBCAXDownloaderDlg 메시지 처리기

BOOL CUBCAXDownloaderDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	if(!RunStudio())
	{
		SetTimer(1023, 100, NULL);
	}

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCAXDownloaderDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCAXDownloaderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCAXDownloaderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CUBCAXDownloaderDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnOK();

	RemoveThread();

}

void CUBCAXDownloaderDlg::RemoveThread()
{
	if(m_pThread)
	{
		m_bRunThread = false;

		DWORD dwExitCode;
		::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);
		if(dwExitCode == STILL_ACTIVE)
		{
			DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 5000);

			if(timeout == WAIT_TIMEOUT)
			{
			}
			else
			{
			}
		}

		::TerminateThread(m_pThread->m_hThread, STILL_ACTIVE);
		delete m_pThread;
		m_pThread = NULL;
	}
}

bool CUBCAXDownloaderDlg::RunStudio()
{
	CString running_path = PROJECT_HOME_PATH;
	running_path += _T("\\UTV1.0\\execute");

	CString version_path = PROJECT_HOME_PATH;
	version_path += _T("\\UTV1.0\\version.txt");

	CString studio_path = PROJECT_HOME_PATH;
	studio_path += _T("\\UTV1.0\\execute\\UBCStudioEEUpdate.bat");

	if(::_taccess(version_path, 0) == 0 && ::_taccess(studio_path, 0) == 0)
	{
		// find -> run
		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		::ZeroMemory( &si, sizeof(si) );
		si.cb = sizeof(si);
		::ZeroMemory( &pi, sizeof(pi) );

		if( !CreateProcess( NULL,           // No module name (use command line)
							(LPTSTR)(LPCTSTR)studio_path,        // Command line
							NULL,           // Process handle not inheritable
							NULL,           // Thread handle not inheritable
							FALSE,          // Set handle inheritance to FALSE
							0,              // No creation flags
							NULL,           // Use parent's environment block
							running_path,   // Use parent's starting directory 
							&si,            // Pointer to STARTUPINFO structure
							&pi )           // Pointer to PROCESS_INFORMATION structure
							) 
		{
			// fail
		}
		else
		{
			// Close process and thread handles. 
			CloseHandle( pi.hProcess );
			CloseHandle( pi.hThread );

			OnOK();

			return true;
		}
	}

	return false;
}

void CUBCAXDownloaderDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == 1023)
	{
		KillTimer(1023);

		DOWNLOAD_PARAM* param	= new DOWNLOAD_PARAM;
		param->pDlg = this;
		param->pRunThread = &m_bRunThread;

		m_bRunThread = true;
		m_pThread = ::AfxBeginThread(DownloadThreadFunc, param, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = FALSE;
		m_pThread->ResumeThread();
	}
}

LRESULT CUBCAXDownloaderDlg::OnUpdateDownRange(WPARAM wParam, LPARAM lParam)
{
	m_stcFilename.SetWindowText(g_strFilename);

	//
	static ULONGLONG current_size = 0;
	if(current_size != g_nCurrentSize)
	{
		current_size = g_nCurrentSize;
		if(g_nCurrentSize > MAXDWORD)
		{
			m_pgCurrent.SetRange32(0, g_nCurrentSize >> 8);
		}
		else
		{
			m_pgCurrent.SetRange32(0, g_nCurrentSize);
		}
		m_pgCurrent.SetPos(0);
	}

	//
	static ULONGLONG total_size = 0;
	if(total_size != g_nTotalSize)
	{
		total_size = g_nTotalSize;
		if(g_nTotalSize > MAXDWORD)
		{
			m_pgTotal.SetRange32(0, g_nTotalSize >> 8);
		}
		else
		{
			m_pgTotal.SetRange32(0, g_nTotalSize);
		}
		m_pgTotal.SetPos(0);
	}

	return 0;
}

LRESULT CUBCAXDownloaderDlg::OnUpdateDownSize(WPARAM wParam, LPARAM lParam)
{
	if(g_nCurrentSize > MAXDWORD)
	{
		m_pgCurrent.SetPos(g_nCurrentDownSize >> 8);
	}
	else
	{
		m_pgCurrent.SetPos(g_nCurrentDownSize);
	}

	if(g_nTotalSize > MAXDWORD)
	{
		m_pgTotal.SetPos((g_nTotalDownSize + g_nCurrentDownSize) >> 8);
	}
	else
	{
		m_pgTotal.SetPos(g_nTotalDownSize + g_nCurrentDownSize);
	}

	return 0;
}

LRESULT CUBCAXDownloaderDlg::OnDownloadComplete(WPARAM wParam, LPARAM lParam)
{
	if(g_nTotalSize == g_nTotalDownSize)
	{
		if(RunStudio())
		{
			TCHAR deskTopPath[MAX_PATH+2] = {0};
			SHGetSpecialFolderPath (GetSafeHwnd(), deskTopPath, CSIDL_COMMON_DESKTOPDIRECTORY, FALSE);

			if(_tcslen(deskTopPath) > 0)
			{
				//바로가기 파일 만들기
				CreateShortcut(_T("C:\\SQISoft\\UTV1.0\\execute\\UBCStudioEEUpdate.bat"), deskTopPath, _T("UBC Enterprise Studio.lnk"), _T(""), _T("C:\\SQISoft\\3rdparty\\icon\\UBCStudioEnterprise.ico"));
				CreateShortcut(_T("C:\\SQISoft\\UTV1.0\\execute\\UBCManagerUpdate.bat"), deskTopPath, _T("UBC Enterprise Manager.lnk"), _T(""), _T("C:\\SQISoft\\3rdparty\\icon\\UBCManager.ico"));
			}

			return TRUE;
		}
		else
		{
			::AfxMessageBox(_T("UBC Studio 실행중 오류가 발생하였습니다."), MB_ICONSTOP);
		}
	}
	else
	{
		::AfxMessageBox(_T("다운로드중 오류가 발생하였습니다."), MB_ICONSTOP);
	}

	OnOK();

	return TRUE;
}

bool FindSubFolder(bool* pRunThread, CFtpFileFind& finder, LPCTSTR lpszFindPath, DOWNLOAD_ITEM_LIST& itemList)
{
	CString find_path = lpszFindPath;
	find_path += _T("/*");

	CStringArray folder_list;

	try
	{
		BOOL bWorking = finder.FindFile(find_path, INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE /*| INTERNET_FLAG_EXISTING_CONNECT*/ );
		while (bWorking && *pRunThread)
		{
			bWorking = finder.FindNextFile();
			if(finder.IsDirectory())
			{
				folder_list.Add(finder.GetFileName());
			}
			else
			{
				DOWNLOAD_ITEM item;
				item.strURL.Format(_T("%s/%s"), lpszFindPath, finder.GetFileName());
				item.strLocalPath.Format(_T("%s%s"), PROJECT_HOME_PATH, lpszFindPath);
				item.strLocalPath.Replace(_T("/"), _T("\\"));
				item.strFileName = finder.GetFileName();
				item.nSize = finder.GetLength();
				item.bDownComplete = false;

				g_nTotalSize += item.nSize;

				if( _tcsicmp(item.strFileName, _T("version.txt")) == 0)
				{
					item.strFileName = _T("version_bak.txt");
				}

				itemList.Add(item);
				TRACE(_T("%s\r\n"), item.strFileName);
			}
		}
	}
	catch(CInternetException* ex)
	{
		TCHAR sz[1024] = {0};
		ex->GetErrorMessage(sz, 1023);
		_tprintf_s(_T("ERROR!  %s\n"), sz);
		ex->Delete();
	}

	if(folder_list.GetCount() > 0)
	{
		for(int i=0; i<folder_list.GetCount() && *pRunThread; i++)
		{
			const CString& sub_path = folder_list.GetAt(i);
			find_path.Format(_T("%s/%s"), lpszFindPath, sub_path);
			FindSubFolder(pRunThread, finder, (LPCTSTR)find_path, itemList);
		}
	}

	return true;
}

UINT DownloadThreadFunc(LPVOID pParam)
{
	DOWNLOAD_PARAM* param	= (DOWNLOAD_PARAM*)pParam;
	CDialog* parent_wnd = param->pDlg;
	bool* pRunThread = param->pRunThread;
	delete param;

	CInternetSession is;

	CFtpConnection* ftp_conn = NULL;

	try
	{
		DOWNLOAD_ITEM_LIST down_list;

		ftp_conn = is.GetFtpConnection(SERVER_IP, SERVER_ID, SERVER_PW, INTERNET_INVALID_PORT_NUMBER, TRUE);

		CFtpFileFind finder(ftp_conn);

		FindSubFolder(pRunThread, finder, _T(""), down_list);

		//
		int len = down_list.GetCount();
		for(int i=0; i<len && *pRunThread; i++)
		{
			DOWNLOAD_ITEM& item = down_list.GetAt(i);
			//TRACE(_T("%s\t%s\t%s\t%d\r\n"), item.strURL, item.strLocalPath, item.strFileName, item.nSize);

			g_strFilename = item.strURL;
			g_nCurrentSize = item.nSize;

			parent_wnd->SendMessage(WM_UPDATE_DOWN_RANGE);

			//
			CString path = item.strURL;
			CInternetFile* pfile = ftp_conn->OpenFile(path);

			if(pfile != NULL)
			{
				CString full_path;
				full_path.Format(_T("%s\\%s"), item.strLocalPath, item.strFileName);
#ifndef UNICODE
				if(!::MakeSureDirectoryPathExists((PCSTR)full_path)) // ansi
#else
				if(SHCreateDirectory(NULL, (LPCTSTR)item.strLocalPath) != ERROR_SUCCESS) // unicode
#endif
				{
					DWORD err_code = ::GetLastError();
					TRACE(_T("MakeSureDirectoryPathExists Error : %d\r\n"), err_code);
				}

				CFile file;
				if(file.Open(full_path, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary))
				{
					try
					{
						BYTE buf[4096];
						while(g_nCurrentSize > g_nCurrentDownSize && *pRunThread)
						{
							int read_size = pfile->Read(buf, 4096);
							file.Write(buf, read_size);
							g_nCurrentDownSize += read_size;

							static DWORD last_tick = 0;
							DWORD cur_tick = ::GetTickCount();
							if(cur_tick - last_tick > 100)
							{
								last_tick = cur_tick;
								parent_wnd->SendMessage(WM_UPDATE_DOWN_SIZE);
							}
						}
						if(g_nCurrentSize == g_nCurrentDownSize)
							item.bDownComplete = true;
						if(*pRunThread)
						{
							parent_wnd->SendMessage(WM_UPDATE_DOWN_SIZE);
						}
					}
					catch(CFileException* ex)
					{
						TCHAR sz[1024] = {0};
						ex->GetErrorMessage(sz, 1023);
						_tprintf_s(_T("ERROR!  %s\n"), sz);
						ex->Delete();
					}
				}

				pfile->Close();
				delete pfile;
			}

			//
			if(g_nCurrentDownSize != g_nCurrentSize)
			{
				TRACE(_T("wrong : %s\\%s(%d)\r\n"), item.strLocalPath, item.strFileName, (int)item.nSize);
			}
			g_nTotalDownSize += g_nCurrentDownSize;
			g_nCurrentDownSize = 0;
		}

		if(g_nTotalSize == g_nTotalDownSize)
		{
			int len = down_list.GetCount();
			for(int i=0; i<len && *pRunThread; i++)
			{
				DOWNLOAD_ITEM& item = down_list.GetAt(i);

				if(_tcsicmp(item.strFileName, _T("version_bak.txt")) == 0 && item.bDownComplete)
				{
					CString old_path;
					old_path.Format(_T("%s\\%s"), item.strLocalPath, item.strFileName);

					item.strFileName = _T("version.txt");

					CString new_path;
					new_path.Format(_T("%s\\%s"), item.strLocalPath, item.strFileName);

					::DeleteFile(new_path);
					::MoveFile(old_path, new_path);
				}
			}
		}
	}
	catch(CInternetException* ex)
	{
		TCHAR sz[1024] = {0};
		ex->GetErrorMessage(sz, 1023);
		_tprintf_s(_T("ERROR! %s\n"), sz);
		ex->Delete();
	}

	if (ftp_conn != NULL) 
	{
		ftp_conn->Close();
		delete ftp_conn;
	}

	parent_wnd->PostMessage(WM_DOWNLOAD_COMPLETE);

	return 0;
}

BOOL CreateShortcut(LPCTSTR strPathObj,		//실제 파일의 위치
					LPCTSTR strPathLink,		//바로가기의 위치
					LPCTSTR ShortcutTitle,	//바로가기 파일의 이름(.Lnk포함시켜야한다.)
					LPCTSTR strDesc,			//바로가기 설명
					LPCTSTR strIconPath)		//아이콘
{
	HRESULT hres;
	IShellLink* psl;
	CString strMyPath = strPathLink;
	TCHAR filePath[MAX_PATH+2];
	TCHAR *pfilename;

	//Init COM Object
	CoInitialize(NULL);

	hres = CoCreateInstance(CLSID_ShellLink,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IShellLink,
		(LPVOID*) &psl);
	if (SUCCEEDED(hres))
	{
		IPersistFile* ppf;

		psl->SetPath(strPathObj);
		psl->SetDescription(strDesc);
		psl->SetIconLocation(strIconPath, 0);
		//--------------------------
		//실행디렉토리넣기
		_tcscpy(filePath, strPathObj);
		//디렉토리얻기
		pfilename = _tcsrchr(filePath, _T('\\'));
		if(pfilename!=NULL)
		{
			*pfilename = _T('\0');
		}
		psl->SetWorkingDirectory(filePath);
		//--------------------------

		hres = psl->QueryInterface( IID_IPersistFile, (LPVOID *) &ppf);

		if (SUCCEEDED(hres))
		{
			strMyPath += _T("\\");
			if(ShortcutTitle==NULL)
			{
				GetModuleFileName(NULL, filePath, MAX_PATH);
				//확장자 제거
				pfilename = _tcsrchr(filePath, _T('.'));
				*pfilename = _T('\0');
				//파일이름얻기
				pfilename = _tcsrchr(filePath, _T('\\'));
				pfilename++;
				strMyPath += pfilename;
				strMyPath += _T(".lnk");
			}
			else
			{
				strMyPath +=ShortcutTitle;
			}

			WCHAR wsz[MAX_PATH];
#ifndef UNICODE
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, strMyPath, -1, wsz, MAX_PATH);
#else
			_tcscpy(wsz, strMyPath);
#endif
			hres = ppf->Save(wsz, TRUE);
			if (hres != S_OK )
			{
				MessageBox(NULL, _T("IPersistFile->Save() Error"), _T("Error"), MB_OK);
			}
			ppf->Release();
		}
		psl->Release();
		//COM 해제
		CoUninitialize();
		return TRUE;
	}
	return FALSE;
}
