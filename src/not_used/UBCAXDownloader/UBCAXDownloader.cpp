// UBCAXDownloader.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "UBCAXDownloader.h"
#include "UBCAXDownloaderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CString		SERVER_IP = _T("");//				_T("211.232.57.202")
CString		SERVER_ID = _T("");//				_T("ent_center")
CString		SERVER_PW = _T("");//				_T("rjtlrl2009")


// CUBCAXDownloaderApp

BEGIN_MESSAGE_MAP(CUBCAXDownloaderApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CUBCAXDownloaderApp 생성

CUBCAXDownloaderApp::CUBCAXDownloaderApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.

	m_hMutex = NULL;
	m_bDeleteMutex = false;
}


// 유일한 CUBCAXDownloaderApp 개체입니다.

CUBCAXDownloaderApp theApp;


// CUBCAXDownloaderApp 초기화

LONG WINAPI NoMsgExceptionFilter(struct _EXCEPTION_POINTERS* /*ExceptionInfo*/)
{
    // 아무일도 하지 않고 그냥 종료하기
    return EXCEPTION_EXECUTE_HANDLER;
}

BOOL CUBCAXDownloaderApp::InitInstance()
{
    ::SetUnhandledExceptionFilter(NoMsgExceptionFilter);

	if(__argc < 4)
	{
		::AfxMessageBox(_T("명령 인수가 부족합니다."), MB_ICONSTOP);
		return FALSE;
	}

	SERVER_IP = __targv[1];
	SERVER_ID = __targv[2];
	SERVER_PW = __targv[3];

	if(!CheckMultiInstance())
	{
		return FALSE;
	}

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("로컬 응용 프로그램 마법사에서 생성된 응용 프로그램"));

	CUBCAXDownloaderDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}


int CUBCAXDownloaderApp::ExitInstance()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(m_bDeleteMutex)
		::ReleaseMutex(m_hMutex);

	return CWinApp::ExitInstance();
}

BOOL CUBCAXDownloaderApp::CheckMultiInstance()
{
	m_hMutex = CreateMutex(NULL, FALSE, _T("UBCAXDownloader"));

	if (GetLastError() == ERROR_ALREADY_EXISTS) 
	{
		CloseHandle(m_hMutex);

		::AfxMessageBox(_T("중복 실행할 수 없습니다."), MB_ICONWARNING);

		CString program_name = _T("UBC Active-X Downloader");

		CWnd* pWnd = CWnd::FindWindow(NULL, program_name);
		if (pWnd)
		{
			pWnd->ShowWindow(SW_RESTORE);
			pWnd->SetForegroundWindow();
			//::PostQuitMessage(0);
			return FALSE;
		}
	}
	else
		m_bDeleteMutex = true;

	return TRUE;
}

/* 문제점
	1. 실행인자에 ip, id, pw가 들어가므로 보안상에 취약할 수 있다.
		-> 암호화 및 레지스트리(또는 파일)로 생성하여 보안을 강화
	2. 
*/

