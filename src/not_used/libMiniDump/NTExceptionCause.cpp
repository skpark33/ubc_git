//######################################################################################
// File    : NTExceptionCause.cpp
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : Exception Cause
// Bug     : 
//######################################################################################
#include "NTExceptionCause.h"

//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : 충돌 원인 분석
//######################################################################################
void OnExceptionCause(DWORD ExceptionCode, char* pszString, size_t nLen)
{
	struct ExceptionNames
	{
		DWORD	ExceptionCode;
		TCHAR *	ExceptionName;
	};

	ExceptionNames ExceptionMap[] =
	{
		{0x40010005, "Control-C로 인한 오류가 발생하였습니다."},
		{0x40010008, "Control-Break로 인한 오류가 발생하였습니다."},
		{0x80000002, "정렬되지 않은 데이타에 접근하였습니다."},
		{0x80000003, "Breakpoint가 걸렸습니다."},
		{0xc0000005, "잘못된 참조가 발생하였습니다"},
		{0xc0000006, "page를 Load할 수 없어 진행 할 수 없습니다."},
		{0xc0000017, "Memory할당에 실패 하였습니다."},
		{0xc000001d, "잘못된 명령어를 실행하였습니다."},
		{0xc0000025, "오류로 인해 더이상 진행 시킬수 없습니다."},
		{0xc0000026, "잘못된 기능에 접근 하였습니다."},
		{0xc000008c, "배열 범위를 초과 하였습니다."},
		{0xc000008d, "Float정규화가 되지않는 작은값 입니다."},
		{0xc000008e, "Float를 0으로 나누었습니다."},
		{0xc000008f, "Float소수를 정확하게 나타낼수 없습니다."},
		{0xc0000090, "Float에서 알수없는 오류가 발생하였습니다."},
		{0xc0000091, "Float Overflow가 발생하였습니다."},
		{0xc0000092, "Float Stack값을 넘었거나 값이 너무 작습니다."},
		{0xc0000093, "Float 값이 너무 작습니다."},
		{0xc0000094, "Integer를 0으로 나누었습니다."},
		{0xc0000095, "Integer Overflow가 발생하였습니다."},
		{0xc0000096, "해당 명령어를 실행 할 수 없습니다."},
		{0xc00000fD, "Stack Overflow가 발생하였습니다."},
		{0xc0000142, "DLL을 초기화 할 수 없습니다."},
		{0xe06d7363, "Microsoft C++에서 오류가 발생하였습니다."},
	};

	for (int i = 0; i < sizeof(ExceptionMap) / sizeof(ExceptionMap[0]); i++)
		if (ExceptionCode == ExceptionMap[i].ExceptionCode)
		{
			strncpy( pszString, ExceptionMap[i].ExceptionName, nLen );
			// Modified by 정운형 2008-11-26 오후 2:03
			// 변경내역 :  버그 수정
			return;
			// Modified by 정운형 2008-11-26 오후 2:03
			// 변경내역 :  버그 수정
		}

	strncpy( pszString, "정확한 오류 원인을 찾을 수 없습니다.", nLen );
	pszString[nLen-1] = NULL;
}