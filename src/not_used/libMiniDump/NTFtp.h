//######################################################################################
// File    : NTFtp.h
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : Ftp파일 전송
// Bug     : 
//######################################################################################
#ifndef __NEXT_FTP__
#define __NEXT_FTP__

#include <windows.h>
#include <wininet.h>

class CNTFtp
{
public:
	CNTFtp();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정
	//~CNTFtp();
	virtual ~CNTFtp();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정

	void DisConnect();
	bool Connect(const char *pszServer, WORD dPort, const char *pszUserID, const char *pszUserPassword);
	bool UpLoad(HWND hWnd, const char *pszFileName);
	HINTERNET GetFtp(void) { return m_hFtp; }

private:

	HINTERNET m_hInternet;
	HINTERNET m_hFtp;
};


#endif //(__NEXT_FTP__)