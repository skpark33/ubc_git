//######################################################################################
// File    : NTSystemInfo.h
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : System 정보 얻기
// Bug     : 
//######################################################################################
#ifndef __NTSYSTEM_INFO__
#define __NTSYSTEM_INFO__

// Modified by 정운형 2008-11-25 오후 3:41
// 변경내역 :  _CLSID_DxDiagProvider, _IID_IDxDiagProvider 링크에러
#include <InitGuid.h>
// Modified by 정운형 2008-11-25 오후 3:41
// 변경내역 :  _CLSID_DxDiagProvider, _IID_IDxDiagProvider 링크에러

#include <dxdiag.h>
#include <string>

class CNTSystemInfo
{
public:
	CNTSystemInfo();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정
	//~CNTSystemInfo();
	virtual ~CNTSystemInfo();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정

	bool Initialize();

	const char* GetCpuString(void)			{ return m_wszCpuString.c_str(); }
	const char* GetVideoCardString(void)	{ return m_wsVideoCardName.c_str(); }
	int   GetVideoPhysicMemory(void)		{ return m_nVideoPhysicMemory; }
	const char* GetDxVersionString(void);
	const char* GetWindowVersionString(void){ return m_strWinVersion.c_str(); }

private:
	void FreeIDxDiagContainer(void);
	void GetPhysicalMemoryInMB(void);
	bool GetProperty( IDxDiagContainer * pContainer, LPCWSTR property_name, std::string* out_value );
	bool GetProperty( LPCWSTR container_name0, LPCWSTR property_name, std::string* out_value );
	bool GetChildContainer( LPCWSTR name0, IDxDiagContainer ** ppChild );
	bool GetDirectXVersion( DWORD * pdwDirectXVersionMajor, DWORD* pdwDirectXVersionMinor,TCHAR* pcDirectXVersionLetter );
	bool GetDisplayDeviceDescription( DWORD dwDevice, std::string* pwstrName );
	bool GetDisplayDeviceMemoryInMB( DWORD dwDevice, int* pDisplayMemory );
	bool GetDisplayDeviceProp( DWORD dwDevice, LPCWSTR prop_name, std::string* pwstrProp );
	bool GetDisplayDeviceNode( DWORD dwDeviceIndex, IDxDiagContainer ** ppNode );
	bool GetChildByIndex(  IDxDiagContainer* pParent, DWORD dwIndex,IDxDiagContainer** ppChild );
	std::string WStringToString( const std::wstring* in_pwstring );
	std::string lpcwstrToString( const LPCWSTR in_lpcwstr );
	void CPUInfo(void);

	bool				m_bCleanupCOM;
	IDxDiagProvider*	m_pDxDiagProvider;
	IDxDiagContainer*	m_pDxDiagRoot;

	std::string			m_wszCpuString;
	std::string			m_wsVideoCardName;
	std::string			m_wsDXVersion;
	std::string			m_strWinVersion;
	float				m_fSystemPhysicMemory;
	int					m_nVideoPhysicMemory;
	DWORD				m_dwDXVersionMajor;
	DWORD				m_dwDXVersionMinor;
	TCHAR				m_cDXVersionLetter;
};


#endif //(__NTSYSTEM_INFO__)