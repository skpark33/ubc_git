//######################################################################################
// File    : NTMiniDump.cpp
// Date    : 2007-1-8
// Author  : 绊府(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : MiniDump file 积己
// Bug     : 
//######################################################################################
#include "NTMiniDump.h"
#include <stdio.h>

#pragma comment(lib, "Dbghelp.lib")


//######################################################################################
// Date    : 2007-1-8
// Author  : 绊府(goli81@naver.com)
// Desc    : 积己磊
//######################################################################################
CNTMiniDump::CNTMiniDump() :
m_eDumpType( MiniDumpNormal )
{
}


//######################################################################################
// Date    : 2007-1-8
// Author  : 绊府(goli81@naver.com)
// Desc    : 家戈磊
//######################################################################################
CNTMiniDump::~CNTMiniDump()
{
}


//######################################################################################
// Date    : 2007-1-8
// Author  : 绊府(goli81@naver.com)
// Desc    : Dump颇老 积己
//######################################################################################
LONG CNTMiniDump::OnSaveMiniDump(_EXCEPTION_POINTERS *pException, const char* pszFileName, HMODULE hDllModule,
								 const char* pszSysInfo, const char* pszUserData)
{
	LONG lResult = EXCEPTION_CONTINUE_SEARCH;

	// 颇老俊 扁废
	HANDLE hFile = CreateFile(pszFileName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if( INVALID_HANDLE_VALUE == hFile )
		return lResult;

	HANDLE hProcess = GetCurrentProcess();
	DWORD dwProcessID = GetCurrentProcessId();
	DWORD dwThreadID = GetCurrentThreadId();

	MINIDUMP_EXCEPTION_INFORMATION sExceptionInfo;
	ZeroMemory ( &sExceptionInfo ,sizeof(MINIDUMP_EXCEPTION_INFORMATION));

	sExceptionInfo.ThreadId = dwThreadID;
	sExceptionInfo.ExceptionPointers = pException;
	sExceptionInfo.ClientPointers = false;

	MINIDUMP_USER_STREAM_INFORMATION sUserInfo; 
	MINIDUMP_USER_STREAM m_vUseData[2];
	m_vUseData[0].Type = 0;
	m_vUseData[0].Buffer = (PVOID)pszSysInfo;
	m_vUseData[0].BufferSize = (ULONG)strlen(pszSysInfo);
	m_vUseData[1].Type = 1;
	m_vUseData[1].Buffer = (PVOID)pszUserData;
	m_vUseData[1].BufferSize = (ULONG)strlen(pszUserData);
	sUserInfo.UserStreamCount = 2; 
	sUserInfo.UserStreamArray = m_vUseData;

	if( MiniDumpWriteDump( hProcess, dwProcessID, hFile, m_eDumpType, &sExceptionInfo, &sUserInfo, NULL) )
	{
		lResult = EXCEPTION_EXECUTE_HANDLER;
	}

	CloseHandle(hFile);

	return lResult;
}


