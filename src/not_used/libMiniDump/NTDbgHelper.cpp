//######################################################################################
// File    : NTDbgHelper.h
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : NTDbgHelper
// Bug     : 
//######################################################################################
#include "./Network/NTClientSocket.h"
#include "NTDbgHelper.h"
// Modified by 정운형 2008-11-26 오후 2:11
// 변경내역 :  오류정보 생성 기능 이동
#include "NTExceptionCause.h"
// Modified by 정운형 2008-11-26 오후 2:11
// 변경내역 :  오류정보 생성 기능 이동


CNTDbgHelper g_cDbgHelper;

//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : Exception Filter
//######################################################################################
LONG WINAPI OnExceptionFilter(_EXCEPTION_POINTERS *pException)
{
	LONG lResult = EXCEPTION_CONTINUE_SEARCH;

	lResult = g_cDbgHelper.OnPlayMiniDump(pException);

	return lResult;
}

//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : 생성자
//######################################################################################
CNTDbgHelper::CNTDbgHelper() :
m_hDllModule( NULL ),
m_bUseServer( false ),
// Modified by 정운형 2008-11-26 오전 11:10
// 변경내역 :  멤버 변수. 함수 추가
m_strAppName( "" ),
m_strVersion( "" ),
m_strErrorInfo( "" ),
m_bUseNotice(false)
// Modified by 정운형 2008-11-26 오전 11:10
// 변경내역 :  멤버 변수. 함수 추가
{
	memset( &m_dwCallStack , 0, sizeof(m_dwCallStack) );
	::SetUnhandledExceptionFilter(OnExceptionFilter);
}

//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : 소멸자
//######################################################################################
CNTDbgHelper::~CNTDbgHelper()
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Application의 이름을 설정 \n
/// @param (const) char* pszAppName : (in) 사용하는 application의 이름
/////////////////////////////////////////////////////////////////////////////////
void CNTDbgHelper::SetAppName(const char* pszAppName)
{
	m_strAppName = pszAppName;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Application의 버전을 설정 \n
/// @param (const) char* pszVersion : (in) 사용하는 application의 버전
/////////////////////////////////////////////////////////////////////////////////
void CNTDbgHelper::SetVersion(const char* pszVersion)
{
	m_strVersion = pszVersion;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Error가 생겼음을 알려주는 dialog 사용 여부를 설정 \n
/// @param (bool) bUse : (in) Error가 생겼음을 알려주는 dialog 사용 여부(true:사용, false:사용하지 않음)
/////////////////////////////////////////////////////////////////////////////////
void CNTDbgHelper::SetUseNoticeDialog(bool bUse)
{
	m_bUseNotice = bUse;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Mini dump 파일의 종류를 설정 \n
///typedef enum _MINIDUMP_TYPE { \n
///    MiniDumpNormal                         = 0x0000, \n
///    MiniDumpWithDataSegs                   = 0x0001, \n
///    MiniDumpWithFullMemory                 = 0x0002, \n
///    MiniDumpWithHandleData                 = 0x0004, \n
///    MiniDumpFilterMemory                   = 0x0008, \n
///    MiniDumpScanMemory                     = 0x0010, \n
///    MiniDumpWithUnloadedModules            = 0x0020, \n
///    MiniDumpWithIndirectlyReferencedMemory = 0x0040, \n
///    MiniDumpFilterModulePaths              = 0x0080, \n
///    MiniDumpWithProcessThreadData          = 0x0100, \n
///    MiniDumpWithPrivateReadWriteMemory     = 0x0200, \n
///    MiniDumpWithoutOptionalData            = 0x0400, \n
///} MINIDUMP_TYPE; \n
/// @param (MINIDUMP_TYPE) nDumpType : (in) Mini dump 파일의 종류
/////////////////////////////////////////////////////////////////////////////////
void CNTDbgHelper::SetDumpType(MINIDUMP_TYPE nDumpType)
{
	m_cMiniDump.SetDumpType(nDumpType);
}


//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : Debug 관리
//######################################################################################
LONG CNTDbgHelper::OnPlayMiniDump(_EXCEPTION_POINTERS *pException)
{
	if( false == OnLoadDbgDllFile() )
		return EXCEPTION_EXECUTE_HANDLER;

	SetCallStack(pException);
	m_cSystemInfo.Initialize();
	// Modified by 정운형 2008-11-26 오전 11:20
	// 변경내역 :  옵션에의해서 dialog 사용 선택
	/*
	m_cDbgWindow.OnCreateWindow();
	m_cDbgWindow.SetMoreInfo(pException, &m_cSystemInfo);
	*/

	SetMoreInfo(pException, &m_cSystemInfo);
	if(m_bUseNotice)
	{
		m_cDbgWindow.OnCreateWindow();
		m_cDbgWindow.SetMoreInfo(m_strErrorInfo.c_str());
	}//if
	// Modified by 정운형 2008-11-26 오전 11:20
	// 변경내역 :  옵션에의해서 dialog 사용 선택

	// Modified by 정운형 2008-11-26 오전 10:52
	// 변경내역 :  파일 이름 변경
	char szSaveFilename[512+100];
	char szLogFilename[512+100];
	char szDumpFolder[512+100];
	char szTempString[100];

	SYSTEMTIME stime;
	::GetLocalTime(&stime);
	//_snprintf( szTempString, 100, "\\%d-%d-%d_%d-%d-%d", stime.wYear, stime.wMonth, stime.wDay, stime.wHour, stime.wMinute, stime.wSecond);
	_snprintf( szTempString, 100, "\\[%s_Ver.%s]%04d-%02d-%02d_%02d;%02d;%02d",
							m_strAppName.c_str(), m_strVersion.c_str(), stime.wYear, stime.wMonth, stime.wDay, stime.wHour, stime.wMinute, stime.wSecond);

	_fullpath(szSaveFilename,NULL,512);
	
	strcpy(szDumpFolder, szSaveFilename);
	strcat(szDumpFolder, "\\dump");
	strcat(szSaveFilename, "\\dump\\");
	CreateDirectory(szDumpFolder, NULL);					//dump 폴더
	strncat( szSaveFilename, szTempString, 100 );			// 날짜 더하기
	//strcat(szSaveFilename, "-" );
	//strcat(szSaveFilename, GAMECODE_STRING);				//파일 고유 번호
	//strcat(szSaveFilename, "-" );
	//strcat(szSaveFilename, m_pstrUserName.c_str());		//유저이름 더하기
	strcpy(szLogFilename, szSaveFilename);					//log 파일
	strcat(szLogFilename, ".log");
	strcat(szSaveFilename, ".dmp");
	// Modified by 정운형 2008-11-26 오전 10:52
	// 변경내역 :  파일 이름 변경

	SendBaseExceptionInfo(szSaveFilename);

	// Modified by 정운형 2008-11-26 오전 11:30
	// 변경내역 :  옵션에의해서 dialog 사용 선택
	/*
	if( IDC_DUMPCANCEL == m_cDbgWindow.GetEvent() )
	{
		return EXCEPTION_EXECUTE_HANDLER;
	}
	*/
	if( m_bUseNotice && (IDC_DUMPCANCEL == m_cDbgWindow.GetEvent()) )
	{
		return EXCEPTION_EXECUTE_HANDLER;
	}
	// Modified by 정운형 2008-11-26 오전 11:30
	// 변경내역 :  옵션에의해서 dialog 사용 선택

	char szDataString[1024] = {0};
	m_strSysInfo += "\n#============================================#\n";
	m_strSysInfo += "[System Info]\n";
	//m_cDbgWindow.GetSystemString( szDataString, sizeof(szDataString) );
	//m_strSysInfo += szDataString;
	m_strSysInfo += m_strErrorInfo;
	m_strSysInfo += "\n#============================================#\n";


	szDataString[0] = NULL;
	m_cDbgWindow.GetUserString( szDataString, sizeof(szDataString) );
	m_szUserData += "\n#============================================#\n";
	m_szUserData += "[User Info]\n";
	m_szUserData += "  ";
	for( size_t i = 0; i < strlen(szDataString); i++ )
	{
		if( szDataString[i] == 13 )
			m_szUserData += ' ';
		m_szUserData += szDataString[i];
		if( szDataString[i] == 10 )
			m_szUserData += ' ';
	}
	m_szUserData += m_szMyDebugData;
	m_szUserData += "\n#============================================#\n";

	//Log 파일 생성
	WriteLogFile(szLogFilename, m_strSysInfo.c_str(), m_szUserData.c_str());

	//LONG lResult = EXCEPTION_EXECUTE_HANDLER;
	LONG lResult = m_cMiniDump.OnSaveMiniDump( pException, szSaveFilename, m_hDllModule, m_strSysInfo.c_str(), m_szUserData.c_str() );

	if( EXCEPTION_EXECUTE_HANDLER == lResult )
	{
		if( !m_pstrFTPServerIP.empty() )
		{
			if( m_cUploadFtp.Connect( m_pstrFTPServerIP.c_str(), m_dFTPPort, m_strFTPID.c_str(), m_strFTPPassword.c_str() ) )
			{
				if( false == m_cUploadFtp.UpLoad(m_cDbgWindow.GetProgressHWND(), szSaveFilename) )
				{
					MessageBox(NULL, "FTP전송에 실패하였습니다.", "오류", MB_OK);
				}

			}
			else
			{
				MessageBox(NULL, "FTP연결에 실패하였습니다.", "오류", MB_OK);
			}

			DeleteFile( szSaveFilename );
		}
	}

	return lResult;
}


//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : Ftp연결 관련 설정
//######################################################################################
void CNTDbgHelper::SetFtpInfo(const char *pszFTPIP, WORD dFTPPort, const char *pszFTPID, const char *pszFTPPassword)
{
	m_pstrFTPServerIP = pszFTPIP;
	m_dFTPPort = dFTPPort;
	m_strFTPID = pszFTPID;
	m_strFTPPassword = pszFTPPassword;
}


//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : Ftp연결 관련 설정
//######################################################################################
void CNTDbgHelper::SetServerInfo(const char *pszServerIP, WORD dPort, const char *pszName)
{
	m_pstrServerIP = pszServerIP;
	m_pstrUserName = pszName;
	m_dPort = dPort;
}


//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : CallStack을 설정한다
//######################################################################################
void CNTDbgHelper::SetCallStack(_EXCEPTION_POINTERS *pException)
{
	memset( &m_dwCallStack, 0, sizeof(m_dwCallStack) );

	STACKFRAME64 sStackFrame;
	memset(&sStackFrame, 0, sizeof(sStackFrame));
	DWORD imageType;
#ifdef _M_IX86
	imageType = IMAGE_FILE_MACHINE_I386;
	sStackFrame.AddrPC.Offset = pException->ContextRecord->Eip;
	sStackFrame.AddrPC.Mode = AddrModeFlat;
	sStackFrame.AddrFrame.Offset = pException->ContextRecord->Ebp;
	sStackFrame.AddrFrame.Mode = AddrModeFlat;
	sStackFrame.AddrStack.Offset = pException->ContextRecord->Esp;
	sStackFrame.AddrStack.Mode = AddrModeFlat;
#elif _M_X64
	imageType = IMAGE_FILE_MACHINE_AMD64;
	sStackFrame.AddrPC.Offset = pException->ContextRecord->Rip;
	sStackFrame.AddrPC.Mode = AddrModeFlat;
	sStackFrame.AddrFrame.Offset = pException->ContextRecord->Rsp;
	sStackFrame.AddrFrame.Mode = AddrModeFlat;
	sStackFrame.AddrStack.Offset = pException->ContextRecord->Rsp;
	sStackFrame.AddrStack.Mode = AddrModeFlat;
#elif _M_IA64
	imageType = IMAGE_FILE_MACHINE_IA64;
	sStackFrame.AddrPC.Offset = pException->ContextRecord->StIIP;
	sStackFrame.AddrPC.Mode = AddrModeFlat;
	sStackFrame.AddrFrame.Offset = pException->ContextRecord->IntSp;
	sStackFrame.AddrFrame.Mode = AddrModeFlat;
	sStackFrame.AddrBStore.Offset = pException->ContextRecord->RsBSP;
	sStackFrame.AddrBStore.Mode = AddrModeFlat;
	sStackFrame.AddrStack.Offset = pException->ContextRecord->IntSp;
	sStackFrame.AddrStack.Mode = AddrModeFlat;
#else
#error "Platform not supported!"
#endif

	HANDLE process = GetCurrentProcess();
	HANDLE thread  = GetCurrentThread();
	for(UINT i = 0; i < MAX_CALLSTACK ; ++i) 
	{
		if( FALSE == StackWalk64(imageType, process, thread, &sStackFrame, &pException->ContextRecord, 
			NULL, NULL, NULL, NULL ) )
		{
			break;
		}

		//char szTemp[256];
		//sprintf( szTemp, "0x%08x \n", sStackFrame.AddrPC.Offset);
		//OutputDebugString( szTemp );
		if( sStackFrame.AddrPC.Offset > 268435456 )
			m_dwCallStack[i] = 0;
		else
			m_dwCallStack[i] = (DWORD)sStackFrame.AddrPC.Offset;
	}

}


//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : Dll파일 로딩
//######################################################################################
bool CNTDbgHelper::OnLoadDbgDllFile()
{
	char szDllHelpPath[MAX_PATH] = {0};

	// DLL 로딩
	if(::GetModuleFileName(NULL, szDllHelpPath, _MAX_PATH))
	{
		char *pSlash = ::strrchr(szDllHelpPath, '\\');
		if( pSlash )
		{
			::lstrcpy(pSlash + 1, "DBGHELP.DLL");
			m_hDllModule = ::LoadLibrary(szDllHelpPath);
		}
	}

	if( NULL == m_hDllModule )
	{
		m_hDllModule = ::LoadLibrary( "DBGHELP.DLL" );
	}


	if( NULL == m_hDllModule )
	{
		MessageBox(NULL, "DBGHELP.DLL을 찾을수 없습니다.", "오류", MB_OK);
		return false;
	}

	return true;
}

//######################################################################################
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Desc    : 기본 충돌정보 서버 전송
//######################################################################################
void CNTDbgHelper::SendBaseExceptionInfo(const char* pszDumpCode)
{
	if( m_pstrServerIP.empty() )
		return;

	CNTClientSocket cClintSocket;

	if( false == cClintSocket.Initialize( m_pstrServerIP.c_str(), m_dPort ) )
	{
		//::MessageBox( NULL, "소켓초기화 오류!", "오류", MB_OK );
		return;
	}

	std::string strDumpName;

	size_t nCutNum = 0;
	size_t nLen = strlen(pszDumpCode);
	for( size_t i = nLen-1; i > 0; i-- )
	{
		if( pszDumpCode[i] == '\\' )
		{
			nCutNum = i+1;
			break;
		}
	}
	strDumpName = pszDumpCode+nCutNum;

	CNTPacket vData;
	vData.SetMsgID(MSGID_EXCEPTION);

	vData << (int)GAMECODE << m_pstrUserName.c_str() << strDumpName.c_str();

	for( int i = 0; i < MAX_CALLSTACK; i++ )
		vData << m_dwCallStack[i];


	cClintSocket.SendPacket( &vData );

}


// Modified by 정운형 2008-11-26 오후 2:11
// 변경내역 :  오류정보 생성 기능 이동
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 오류발생 정보 문자열을 만든다 \n
/// @param (_EXCEPTION_POINTERS) *pException : (in) 설명
/// @param (CNTSystemInfo*) pSystemInfo : (in) 설명
/////////////////////////////////////////////////////////////////////////////////
void CNTDbgHelper::SetMoreInfo(_EXCEPTION_POINTERS *pException, CNTSystemInfo* pSystemInfo)
{
	char szTempString[256];
	char szExceptionCause[256];
	std::string strSnedEdit;

	SYSTEMTIME time;
	::GetLocalTime(&time);
	_snprintf( szTempString, 256, " 발생시간 : %d년%d월%d일 %d:%d:%d \r\n ", time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);
	strSnedEdit += szTempString;

	OnExceptionCause(  pException->ExceptionRecord->ExceptionCode, szExceptionCause, sizeof(szExceptionCause) );
	//_snprintf( szTempString, 256, "발생원인 : %s \r\n ", szExceptionCause );
	_snprintf( szTempString, 256, "발생원인 : %s(0x%x) \r\n ", szExceptionCause, pException->ExceptionRecord->ExceptionCode );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "오류주소 : 0x%08x \r\n ", pException->ContextRecord->Eip );
	strSnedEdit += szTempString;


	strSnedEdit += "==================컴퓨터 정보================== \r\n ";
	_snprintf( szTempString, 256, "운영체제	: %s \r\n ", pSystemInfo->GetWindowVersionString() );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "그래픽 카드	: %s \r\n ", pSystemInfo->GetVideoCardString() );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "CPU		: %s \r\n ", pSystemInfo->GetCpuString() );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "DirectX	: %s \r\n ", pSystemInfo->GetDxVersionString() );
	strSnedEdit += szTempString;


	strSnedEdit += "==================메모리 정보================== \r\n ";
	MEMORYSTATUS MemStatus;
	MemStatus.dwLength=sizeof(MemStatus);
	GlobalMemoryStatus(&MemStatus);

	_snprintf( szTempString, 256, "전체 메모리		: %dMB \r\n ", MemStatus.dwTotalPhys/(1024*1024) );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "사용한 메모리		: %dMB \r\n ", MemStatus.dwAvailPhys/(1024*1024) );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "가상 메모리		: %dMB \r\n ", MemStatus.dwTotalVirtual/(1024*1024) );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "사용한 가상 메모리	: %dMB \r\n ", MemStatus.dwAvailVirtual/(1024*1024) );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "비디오 메모리		: %dMB \r\n ", pSystemInfo->GetVideoPhysicMemory() );
	strSnedEdit += szTempString;


	strSnedEdit += "=================레지스터 정보================= \r\n ";
	_snprintf( szTempString, 256, "EDI	: 0x%08x \r\n ESI	: 0x%08x \r\n EAX	: 0x%08x \r\n ", 
		pException->ContextRecord->Edi, pException->ContextRecord->Esi, pException->ContextRecord->Eax );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "EBX	: 0x%08x \r\n ECX	: 0x%08x \r\n EDX	: 0x%08x \r\n ", 
		pException->ContextRecord->Ebx, pException->ContextRecord->Ecx, pException->ContextRecord->Edx );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "EIP	: 0x%08x \r\n EBP	: 0x%08x \r\n SegCs	: 0x%08x \r\n ", 
		pException->ContextRecord->Eip, pException->ContextRecord->Ebp, pException->ContextRecord->SegCs );
	strSnedEdit += szTempString;

	_snprintf( szTempString, 256, "EFlags	: 0x%08x \r\n ESP	: 0x%08x \r\n SegSs	: 0x%08x \r\n ", 
		pException->ContextRecord->EFlags, pException->ContextRecord->Esp, pException->ContextRecord->SegSs );
	strSnedEdit += szTempString;

	m_strErrorInfo = strSnedEdit;
}
// Modified by 정운형 2008-11-26 오후 2:11
// 변경내역 :  오류정보 생성 기능 이동




/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 오류발생 정보 log파일을 만든다 \n
/// @param (const char*) pszLogFileName : (in) Log 파일의 생성 경로(이름)
/// @param (const char*) pszSysInfo : (in) 오류시스템정보 문자열
/// @param (const char*) pszUserData : (in) 사용자 추가 문자열
/////////////////////////////////////////////////////////////////////////////////
void CNTDbgHelper::WriteLogFile(const char* pszLogFileName, const char* pszSysInfo, const char* pszUserData)
{
	FILE* log = NULL;
	if (fopen_s(&log, pszLogFileName, "a") == 0)
	{
		fprintf(log, "%s\n%s", pszSysInfo,pszUserData);
		fclose(log);
	}//if
}









