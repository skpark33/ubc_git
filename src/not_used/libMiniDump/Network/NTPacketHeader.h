//######################################################################################
// File    : NTPacketHeader.h
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : 패킷 헤더
// Bug     : 
//######################################################################################
#ifndef __NEXT_PACKETHEADER__
#define __NEXT_PACKETHEADER__

#include <WINSOCK2.h>
#pragma comment( lib, "ws2_32.lib")

#include <windows.h>
#include <string>

#define BUFFER_SIZE 1024

typedef void (CALLBACK *PCALLBACKNETWORK) ( char *pData );

struct sCompletionKey
{
	SOCKET hClientntSocket;
	SOCKADDR_IN sClientAddr;
};

struct sOverlapData
{
	OVERLAPPED overlapped;
	char szBuffer[BUFFER_SIZE];
	WSABUF wsaBuf;
};

struct sPacketData
{
	sCompletionKey	sKeyData;
	sOverlapData	sData;
};



//########################## Packet Data ##########################
#pragma pack(1)

struct sPacketHeader
{
	// 메세지ID
	USHORT usMessageID;   
	// 패킷크기
	USHORT nSize;
	// 에러코드
	UCHAR  ucErrorCode;
	//
	USHORT usBodyLen;
};
#define PACKETHEADER_SIZE sizeof(sPacketHeader)

struct sLoginData : sPacketHeader
{
	char szUserName[32];
	char szFileName[128];
};
#define LOGINDATA_SIZE sizeof(sLoginData)

#pragma pack()

#define MSGID_EXCEPTION		0x0001

#endif//(__NEXT_PACKETHEADER__)