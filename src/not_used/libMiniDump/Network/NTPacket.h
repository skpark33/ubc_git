#ifndef __NEXT_PACKET__
#define __NEXT_PACKET__

#include "NTPacketHeader.h"

class CNTPacket
{
public:
	CNTPacket();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정
	//~CNTPacket();
	virtual ~CNTPacket();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정

	CNTPacket&	operator<<(char		rhs);
	CNTPacket&	operator<<(short	rhs);
	CNTPacket&	operator<<(int		rhs);
	CNTPacket&	operator<<(BYTE		rhs);
	CNTPacket&	operator<<(WORD		rhs);
	CNTPacket&	operator<<(DWORD	rhs);
	CNTPacket&	operator<<(float	rhs);
	CNTPacket&	operator<<(const char* rhs);

	CNTPacket&	operator>>(char&	rhs);	
	CNTPacket&	operator>>(short&	rhs);	
	CNTPacket&	operator>>(int&		rhs);	
	CNTPacket&	operator>>(BYTE&	rhs);
	CNTPacket&	operator>>(WORD&	rhs);	
	CNTPacket&	operator>>(DWORD&	rhs);
	CNTPacket&	operator>>(float&	rhs);
	CNTPacket&	operator>>(std::string&	rhs);	
	CNTPacket&	operator=(const CNTPacket& packet);

	WORD	GetMsgID()				{ return m_pHeader->usMessageID; }
	void	SetMsgID(WORD nID)		{ m_pHeader->usMessageID = nID; }
	UCHAR	GetErrorID()			{ return m_pHeader->ucErrorCode; }
	void	SetErrorID(UCHAR nID)	{ m_pHeader->ucErrorCode = nID; }
	const char* GetBuffer(void)		{ return m_szBuf; }
	UINT	GetROffset(void)		{ return m_nROffset; }
	UINT	GetWOffset(void)		{ return m_nWOffset; }
	sPacketHeader* GetHeader(void)	{ return m_pHeader; }
	void	Clear();

	void	ReadData(const void* pData, UINT n);
	void	WriteData(void* pData, UINT n);

	bool	ReadPacket(void* ptr);
	bool	ReadHead(void* ptr);
	int		ReadBody(void* ptr, DWORD dwLen);

	void	PushStruct(void* pStruct, UINT size);
	void	PopStruct(void* pStruct, UINT size);

private:

	sPacketHeader*	m_pHeader;
	char			m_szBuf[BUFFER_SIZE];
	UINT			m_nROffset;
	UINT			m_nWOffset;
};

#endif//(__NEXT_PACKET__)