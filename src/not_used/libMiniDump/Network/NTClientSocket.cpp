//######################################################################################
// File    : NTClientSocket.cpp
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : Client Socket
// Bug     : 
//######################################################################################
#include "NTClientSocket.h"

CNTClientSocket* g_pSocket = NULL;

//######################################################################################
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Desc    : 생성자
//######################################################################################
CNTClientSocket::CNTClientSocket()
{
	g_pSocket = this;
}


//######################################################################################
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Desc    : 소멸자
//######################################################################################
CNTClientSocket::~CNTClientSocket()
{
}


//######################################################################################
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Desc    : 초기화
//######################################################################################
bool CNTClientSocket::Initialize(const char* pszIP, WORD wPort)
{
	if( false == OnSocketStart() )
	{
		return false;
	}

	if( false == OnConnectServer( pszIP, wPort) )
	{
		return false;
	}

	

	return true;
}


//######################################################################################
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Desc    : 소켓시작
//######################################################################################
bool CNTClientSocket::OnSocketStart(void)
{
	WSADATA wsaData;

	if( ::WSAStartup( MAKEWORD( 2, 2 ), &wsaData ) != 0 )
		return false;

	if( wsaData.wVersion != MAKEWORD( 2, 2 ) )
	{
		::WSACleanup();
		return false;
	}

	return true;
}

//######################################################################################
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Desc    : 소켓 종료
//######################################################################################
bool CNTClientSocket::OnSocketEnd(void)
{
	closesocket( m_vSocket );
	::WSACleanup();

	return true;
}

//######################################################################################
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Desc    : 서버연결
//######################################################################################
bool CNTClientSocket::OnConnectServer(const char* pszIP, WORD wPort)
{
	char szIPAddress[MAX_PATH] = {"127.0.0.1"};
	if( NULL != pszIP )
	{
		strcpy( szIPAddress, pszIP );
	}

	m_vSocket = ::socket( AF_INET, SOCK_STREAM, 0 );
	if( m_vSocket == INVALID_SOCKET ) 
		return false;

	struct sockaddr_in ServAddr;
	ZeroMemory( &ServAddr, sizeof(ServAddr) );
	ServAddr.sin_family			= PF_INET;
	ServAddr.sin_addr.s_addr	= ::inet_addr( szIPAddress );
	ServAddr.sin_port			= ::htons( wPort );

	struct linger ling;
	ling.l_onoff  = 1;
	ling.l_linger = 0;
	setsockopt(m_vSocket, SOL_SOCKET, SO_LINGER, (char *)&ling, sizeof(ling));

	if( connect( m_vSocket, (struct sockaddr*)&ServAddr, sizeof(ServAddr)) != 0 )
	{
		return FALSE;
	}

	return true;
}


//######################################################################################
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Desc    : 패킷 전송
//######################################################################################
bool CNTClientSocket::SendPacket(CNTPacket* pPacket)
{
	return SendPacket( pPacket->GetBuffer(), pPacket->GetWOffset() );
}


//######################################################################################
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Desc    : 패킷 전송
//######################################################################################
bool CNTClientSocket::SendPacket(const char* pszBuffer, int nSize)
{
	if( send( m_vSocket, pszBuffer, nSize, 0 ) <= 0 )
		return false;

	return true;
}


