//######################################################################################
// File    : NTClientSocket.h
// Date    : 2007-1-10
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : Client Socket
// Bug     : 
//######################################################################################
#ifndef __NEXT_SOCKET__
#define __NEXT_SOCKET__

#include "NTPacketHeader.h"
#include "NTPacket.h"

class CNTClientSocket
{
public:
	CNTClientSocket();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정
	//~CNTClientSocket();
	virtual ~CNTClientSocket();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정

	bool	Initialize(const char* pszIP, WORD wPort = 5002);
	bool	SendPacket(CNTPacket* pPacket);
	bool	SendPacket(const char* pszBuffer, int nSize);
	SOCKET* GetSocket(void) { return &m_vSocket; }


private:

	bool OnSocketStart(void);
	bool OnSocketEnd(void);
	bool OnConnectServer(const char* pszIP, WORD wPort);

	SOCKET	m_vSocket;

};


extern CNTClientSocket* g_pSocket;

#endif//(__NEXT_SOCKET__)