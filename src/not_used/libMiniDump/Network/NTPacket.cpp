#include "NTPacket.h"

CNTPacket::CNTPacket()
{
	Clear();
}

CNTPacket::~CNTPacket()
{
}


void CNTPacket::ReadData(const void* pData, UINT n)
{
	if (m_nROffset+n+PACKETHEADER_SIZE > BUFFER_SIZE)
		return;

	memcpy ((BYTE *)pData, m_szBuf+m_nROffset, n);
	m_nROffset += n;
}

void CNTPacket::WriteData (void* pData, UINT n)
{
	if (m_nWOffset+n+PACKETHEADER_SIZE > BUFFER_SIZE)
		return;

	memcpy (m_szBuf + m_nWOffset, pData, n);
	m_nWOffset += n;
	m_pHeader->usBodyLen = m_nWOffset - PACKETHEADER_SIZE;
}

void CNTPacket::Clear ()
{
	memset(m_szBuf, 0, BUFFER_SIZE);
	m_pHeader = reinterpret_cast<sPacketHeader*>(m_szBuf);
	m_nROffset = PACKETHEADER_SIZE; 
	m_nWOffset = PACKETHEADER_SIZE;
}



CNTPacket& CNTPacket::operator<<(const char rhs)
{
	WriteData ((void*)(&rhs), 1);
	return *this;
}

CNTPacket& CNTPacket::operator<<(const short rhs)
{
	WriteData ((void*)(&rhs), 2);
	return *this;
}

CNTPacket& CNTPacket::operator<<(const int rhs)
{
	WriteData ((void*)(&rhs), 4);
	return *this;
}

CNTPacket& CNTPacket::operator<<(const BYTE rhs)
{
	WriteData ((void*)(&rhs), 1);
	return *this;
}

CNTPacket& CNTPacket::operator<<(const WORD rhs)
{
	WriteData ((void*)(&rhs), 2);
	return *this;
}

CNTPacket& CNTPacket::operator<<(const DWORD rhs)
{
	WriteData ((void*)(&rhs), 4);
	return *this;
}

CNTPacket& CNTPacket::operator<<(const float rhs)
{
	WriteData ((void*)(&rhs), 4);
	return *this;
}

CNTPacket& CNTPacket::operator <<(const char* rhs)
{
	WORD wLength = (WORD)(strlen(rhs) + 1);
	int nBufLen = wLength + sizeof(WORD);
	BYTE *pBuf = new BYTE[nBufLen];

	if( pBuf )
	{
		memcpy( pBuf, &wLength, sizeof(WORD));

		if( nBufLen  - wLength -1 > 0 )
			memcpy( pBuf + sizeof(WORD), rhs, wLength - 1);
		pBuf[nBufLen - 1] = '\0';
		WriteData(static_cast<void*>(pBuf), nBufLen);

		delete[] pBuf;
	}

	return *this;
}



CNTPacket& CNTPacket::operator>>(char& rhs)
{
	ReadData (&rhs, 1);
	return *this;
}

CNTPacket& CNTPacket::operator>>(short& rhs)
{
	ReadData (&rhs, 2);
	return *this;
}

CNTPacket& CNTPacket::operator>>(int& rhs)
{
	ReadData (&rhs, 4);
	return *this;
}

CNTPacket& CNTPacket::operator>>(BYTE& rhs)
{
	ReadData (&rhs, 1);
	return *this;
}

CNTPacket& CNTPacket::operator>>(WORD& rhs)
{
	ReadData (&rhs, 2);
	return *this;
}

CNTPacket& CNTPacket::operator>>(DWORD& rhs)
{
	ReadData (&rhs, 4);
	return *this;
}

CNTPacket& CNTPacket::operator>>(float& rhs)
{
	ReadData (&rhs, 4);
	return *this;
}

CNTPacket& CNTPacket::operator>>(std::string& rhs)
{
	WORD wLength;
	char pBuf[BUFFER_SIZE];

	ReadData((void *)&wLength, sizeof(WORD));
	if(wLength>BUFFER_SIZE-1)
	{
		rhs = "Error - String";
		return *this;
	}

	ReadData(pBuf, wLength);
	pBuf[wLength]=0;
	rhs = pBuf;
	return *this;
}

CNTPacket& CNTPacket::operator=(const CNTPacket& packet)
{
	m_nROffset = packet.m_nROffset;
	m_nWOffset = packet.m_nWOffset;
	memcpy(m_pHeader, packet.m_pHeader, BUFFER_SIZE);

	return *this;
}

void CNTPacket::PushStruct(void* pStruct, UINT size)
{
	WriteData(pStruct, size);
}

void CNTPacket::PopStruct(void* pStruct, UINT size)
{
	ReadData(pStruct, size);
}

bool CNTPacket::ReadPacket(void* ptr)
{
	memcpy(m_pHeader, static_cast<sPacketHeader*>(ptr), PACKETHEADER_SIZE);
	memcpy(m_szBuf+PACKETHEADER_SIZE, (char*)ptr+PACKETHEADER_SIZE, m_pHeader->usBodyLen);
	m_nWOffset = m_pHeader->usBodyLen;
	return true;
}

bool CNTPacket::ReadHead(void* ptr)
{
	memcpy(m_pHeader, static_cast<sPacketHeader*>(ptr), PACKETHEADER_SIZE);
	return TRUE;
}

int CNTPacket::ReadBody(void* ptr, DWORD dwLen)
{
	int iResult = dwLen-(m_pHeader->usBodyLen+PACKETHEADER_SIZE); 
	if(0>iResult)
		return iResult;

	memcpy(m_szBuf+PACKETHEADER_SIZE, (char*)ptr+PACKETHEADER_SIZE, m_pHeader->usBodyLen);
	m_nWOffset = dwLen;

	return iResult;
}


