//######################################################################################
// File    : NTMiniDump.h
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : MiniDump file 생성
// Bug     : 
//######################################################################################
#ifndef __NEXT_MINIDUMP__
#define __NEXT_MINIDUMP__

#include <windows.h>
#include <dbghelp.h>

class CNTMiniDump
{
public:
	CNTMiniDump();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정
	//~CNTMiniDump();
	virtual ~CNTMiniDump();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정

	LONG OnSaveMiniDump(_EXCEPTION_POINTERS *pException, const char* pszFileName, HMODULE hDllModule,
						const char* pszSysInfo, const char* pszUserData);

	void SetDumpType(MINIDUMP_TYPE nDumpType)			{ m_eDumpType = nDumpType; }
	MINIDUMP_TYPE GetDumpType(MINIDUMP_TYPE nDumpType)	{ return m_eDumpType; }

private:

	MINIDUMP_TYPE m_eDumpType;
};

#endif //(__NEXT_MINIDUMP__)