//######################################################################################
// File    : NTDbgHelper.cpp
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : NTDbgHelper
// Bug     : 
//######################################################################################
#ifndef __NEXT_DBGHELPER__
#define __NEXT_DBGHELPER__

#include "NTMiniDump.h"
#include "NTDbgHelperWindow.h"
#include "NTSystemInfo.h"
#include "NTFtp.h"
#include <string>

#define MAX_CALLSTACK 20
#define GAMECODE 1001
#define GAMECODE_STRING "1001"

class CNTDbgHelper
{
public:
	CNTDbgHelper();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정
	//~CNTDbgHelper();
	virtual ~CNTDbgHelper();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정

	void AddDebugData(const char* pszData)	{ m_szMyDebugData += pszData; }
	void SetFtpInfo(const char *pszFTPIP, WORD dFTPPort, const char *pszFTPID, const char *pszFTPPassword);
	LONG OnPlayMiniDump(_EXCEPTION_POINTERS *pException);
	void SetServerInfo(const char *pszServerIP, WORD dPort, const char *pszName);

private:
	void SendBaseExceptionInfo(const char* pszDumpCode);
	void SetCallStack(_EXCEPTION_POINTERS *pException);
	bool OnLoadDbgDllFile(void);

	CNTMiniDump			m_cMiniDump;
	CNTDbgHelperWindow	m_cDbgWindow;
	CNTSystemInfo		m_cSystemInfo;
	CNTFtp				m_cUploadFtp;
	bool				m_bUseServer;
	std::string			m_strSysInfo;
	std::string			m_szUserData;
	std::string			m_szMyDebugData;


	std::string			m_pstrFTPServerIP;
	std::string			m_strFTPID;
	std::string			m_strFTPPassword;
	WORD				m_dFTPPort;

	std::string			m_pstrServerIP;
	std::string			m_pstrUserName;
	WORD				m_dPort;

	DWORD				m_dwCallStack[MAX_CALLSTACK];

	HMODULE				m_hDllModule;

	// Modified by 정운형 2008-11-26 오전 11:10
	// 변경내역 :  멤버 변수. 함수 추가
	std::string			m_strAppName;					///<Application의 이름
	std::string			m_strVersion;					///<Application의 버전
	std::string			m_strErrorInfo;					///<오류발생 정보
	bool				m_bUseNotice;					///<Error가 생겼음을 알려주는 dialog 사용 여부

public:
	void	SetAppName(const char* pszAppName);			///<Application의 이름을 설정
	void	SetVersion(const char* pszVersion);			///<Application의 버전을 설정
	void	SetUseNoticeDialog(bool bUse);				///<Error가 생겼음을 알려주는 dialog 사용 여부를 설정
	void	SetDumpType(MINIDUMP_TYPE nDumpType);		///<Mini dump 파일의 종류를 설정
	void	SetMoreInfo(_EXCEPTION_POINTERS *pException,
						CNTSystemInfo* pSystemInfo);	///<오류발생 정보 문자열을 만든다
	void	WriteLogFile(const char* pszLogFileName,
							const char* pszSysInfo,
							const char* pszUserData);	///<오류발생 정보 log파일을 만든다 
	// Modified by 정운형 2008-11-26 오전 11:10
	// 변경내역 :  멤버 변수. 함수 추가

};

extern CNTDbgHelper g_cDbgHelper;

#endif//(__NEXT_DBGHELPER__)