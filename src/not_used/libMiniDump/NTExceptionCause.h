//######################################################################################
// File    : NTExceptionCause.h
// Date    : 2007-1-8
// Author  : ����(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : Exception Cause
// Bug     : 
//######################################################################################
#ifndef __NEXT_EXCEPTION_CAUSE__
#define __NEXT_EXCEPTION_CAUSE__

#include <Windows.h>

void OnExceptionCause(DWORD ExceptionCode, char* pszString, size_t nLen);

#endif //(__NEXT_EXCEPTION_CAUSE__)