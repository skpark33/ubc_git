//######################################################################################
// File    : NTDbgHelperWindows.h
// Date    : 2007-1-8
// Author  : 고리(goli81@naver.com)
// Blog    : http://blog.naver.com/goli81
// Desc    : Create Window
// Bug     : 
//######################################################################################
#ifndef __NEXT_DBGHELPER_WINDOW__
#define __NEXT_DBGHELPER_WINDOW__

#include <Windows.h>

#define IDC_DUMPSEND		101
#define IDC_DUMPCANCEL		102
#define IDC_DUMPEDITBOX		103
#define IDC_PROGRESS		104
#define IDC_ERROREDITBOX	105
#define IDC_MOREINFO		106
#define IDC_MAININFO		107

class CNTSystemInfo;
struct _EXCEPTION_POINTERS;

class CNTDbgHelperWindow
{
public:
	CNTDbgHelperWindow();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정
	//~CNTDbgHelperWindow();
	virtual ~CNTDbgHelperWindow();
	// Modified by 정운형 2008-11-27 오전 11:19
	// 변경내역 :  소멸자 수정

	void OnCreateWindow(void);
	// Modified by 정운형 2008-11-26 오후 2:11
	// 변경내역 :  오류정보 생성 기능 이동
	//void SetMoreInfo(_EXCEPTION_POINTERS *pException, CNTSystemInfo* pSystemInfo);
	void CNTDbgHelperWindow::SetMoreInfo(const char* pszInfo);			///<오류 발생 정보의 문자열을 창에 표시
	// Modified by 정운형 2008-11-26 오후 2:11
	// 변경내역 :  오류정보 생성 기능 이동
	void GetUserString(char *pszString, int nLen);
	void GetSystemString(char *pszString, int nLen);
	UINT GetEvent(void);
	HWND GetProgressHWND(void)		{ return m_hProgresshWnd; }
	HWND GetUserEditBoxHWND(void)	{ return m_hUserEdithWnd; }
	HWND GetSendEditBoxHWND(void)	{ return m_hSendEdithWnd; }
	void CleanUp(void);

	void OnCreateControl(void);
	void OnBaseWindows(void);
	void OnMoreInfoWindows(void);


private:
	static	LRESULT CALLBACK CNTDbgHelperWindow::WndProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );


	HBITMAP m_Image;
	HWND m_Mainhwnd;
	HWND m_Morehwnd;
	HWND m_hUserEdithWnd;
	HWND m_hSendEdithWnd;
	HWND m_hProgresshWnd;
	HWND m_hMainProgresshWnd;
	HWND m_hMoreProgresshWnd;
	UINT m_nResult;
	bool m_bMainWindow;
};

#endif //(__NEXT_DBGHELPER_WINDOW__)