#pragma once


// CBypassSocket 명령 대상입니다.

class CListenSocket;

class CBypassSocket : public CAsyncSocket
{
public:
	CBypassSocket(CListenSocket* pListenSocket, int port, int index, SOCKET_TYPE socket_type);
	virtual ~CBypassSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnSend(int nErrorCode);

	void	SendEx(void* lpBuf, int nBufLen);
	int		m_nPort;

	void	ClearBuffer();

protected:
	CListenSocket*	m_pParentListenSocket;
	int				m_nIndex;
	CBypassSocket*	m_pSocket;
	SOCKET_TYPE		m_SocketType;

	// for Thread-Sending
	CMutex		m_mutex;
	bool		m_bProcessThread;
	CWinThread*	m_pThread;
	PACKET_DATA_LIST	m_PacketDataList;

	bool		DeleteThread();
	static UINT	SendFunc(LPVOID pParam);
	static UINT HealthCheckFunc(LPVOID pParam);
};

typedef	CArray<CBypassSocket*, CBypassSocket*>	BYPASS_SOCKET_LIST;

typedef struct {
	bool*	pProcess;
	CListenSocket*	pParentListenSocket;
	CBypassSocket*	pBypassSocket;
} THREAD_PARAM;
