/************************************************************************************/
/*! @file HostInfo.cpp
	@brief Host 정보를 갖는 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2008/11/11\n
	▶ 사용파일: stdafx.h, VNCManager.h, HostInfo.h\n
	▶ 참고사항:
		- 서버로 부터 받아온 host 정보를 갖는 클래스

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2008/11/11:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#include "stdafx.h"
#include "VNCManager.h"
#include "HostInfo.h"


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CHostInfo::CHostInfo()
{	
	m_strHostName		= _T("");
	m_strSiteID			= _T("");
	m_strHostID			= _T("");
	m_strAdminState		= _T("");
	m_strOperationState	= _T("");
	m_strModel			= _T("");
	m_strDescription	= _T("");

	m_strStatus			= _T("");
	m_strIP				= _T("");
	m_strMainPort		= _T("");
	m_strSubPort		= _T("");
	m_strStartupTime	= _T("");
	m_strShutdownTime	= _T("");

	// Modified by 정운형 2008-11-28 오전 10:50
	// 변경내역 :  addr4필드 추가
	m_strAddr4			= _T("");
	// Modified by 정운형 2008-11-28 오전 10:50
	// 변경내역 :  addr4필드 추가

	m_strMacaddr		= _T("");
	m_strAuthDate		= _T("");
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CHostInfo::~CHostInfo()
{	
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CVNCInfo::CVNCInfo()
{	
	pHealthCheckListenSocket	= NULL;
	pLocalListenVNCSocket		= NULL;
	pRemoteListenVNCSocket		= NULL;
	pLocalListenRDCSocket		= NULL;
	pRemoteListenRDCSocket		= NULL;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CVNCInfo::~CVNCInfo()
{
}	
