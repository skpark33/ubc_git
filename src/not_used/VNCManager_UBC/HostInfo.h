/************************************************************************************/
/*! @file HostInfo.h
	@brief Host 정보를 갖는 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2008/11/11\n
	▶ 참고사항:
		- 서버로 부터 받아온 host 정보를 갖는 클래스
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2008/11/11:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#pragma once

#include "ListenSocket.h"

// Modified by 정운형 2008-11-11 오전 10:58
// 변경내역 :  Host 정보 보이기 수정작업
//! Host 정보를 갖는 class
/*!
	서버로 부터 받아오는 Host의 정보를 갖는 class\n
*/
class CHostInfo
{
public:
	CHostInfo(void);						///<생성자
	virtual ~CHostInfo(void);				///<소멸자

	CString		m_strHostName;				///<host명
	CString		m_strSiteID;				///<site id
	CString		m_strHostID;				///<host id
	CString		m_strAdminState;			///<동작여부(usage)
	CString		m_strOperationState;		///<고장여부(power)
	CString		m_strModel;					///<model 명
	CString		m_strDescription;			///<description

	CString		m_strStatus;				///<network on/off status
	CString		m_strIP;					///<host 의 ipaddress
	CString		m_strMainPort;				///<vnc proxy port
	CString		m_strSubPort;				///<vnc proxy port + LOCAL_LOCAL_RDC_PORT_GAP
	CString		m_strStartupTime;			///<auto startup time
	CString		m_strShutdownTime;			///<auto shutdown time

	// Modified by 정운형 2008-11-28 오전 10:50
	// 변경내역 :  addr4필드 추가
	CString		m_strAddr4;					///<주소 뒷자리
	// Modified by 정운형 2008-11-28 오전 10:50
	// 변경내역 :  addr4필드 추가
	CString		m_strMacaddr;
	CString		m_strAuthDate;

	CHostInfo& CHostInfo::operator=(const CHostInfo& obj)
	{ 
		m_strHostName		= obj.m_strHostName;		
		m_strSiteID			= obj.m_strSiteID;		
		m_strHostID			= obj.m_strHostID;		
		m_strAdminState		= obj.m_strAdminState;	
		m_strOperationState	= obj.m_strOperationState;
		m_strModel			= obj.m_strModel;			
		m_strDescription	= obj.m_strDescription;	
										
		m_strStatus			= obj.m_strStatus;		
		m_strIP				= obj.m_strIP;			
		m_strMainPort		= obj.m_strMainPort;		
		m_strSubPort		= obj.m_strSubPort;		
		m_strStartupTime	= obj.m_strStartupTime;	
		m_strShutdownTime	= obj.m_strShutdownTime;

		// Modified by 정운형 2008-11-28 오전 10:50
		// 변경내역 :  addr4필드 추가
		m_strAddr4			= obj.m_strAddr4;
		// Modified by 정운형 2008-11-28 오전 10:50
		// 변경내역 :  addr4필드 추가

		return *this;
	};									
};

/*
typedef struct 
{
	CListenSocket*	pHealthCheckListenSocket;
	CListenSocket*	pLocalListenVNCSocket;
	CListenSocket*	pRemoteListenVNCSocket;
	CListenSocket*	pLocalListenRDCSocket;
	CListenSocket*	pRemoteListenRDCSocket;
} VNC_INFO;
*/
class CVNCInfo
{
public:
	CVNCInfo(void);					///<생성자
	virtual ~CVNCInfo(void);		///<소멸자

	CListenSocket*	pHealthCheckListenSocket;
	CListenSocket*	pLocalListenVNCSocket;
	CListenSocket*	pRemoteListenVNCSocket;
	CListenSocket*	pLocalListenRDCSocket;
	CListenSocket*	pRemoteListenRDCSocket;

	CHostInfo*		m_pclsHostInfo;
};


#define VNC_INFO	CVNCInfo
typedef CArray<VNC_INFO, VNC_INFO&>		VNC_INFO_LIST;

// Modified by 정운형 2008-11-11 오전 10:58
// 변경내역 :  Host 정보 보이기 수정작업