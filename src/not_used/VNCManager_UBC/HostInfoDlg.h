/************************************************************************************/
/*! @file HostInfoDlg.h
	@brief Host 상세 정보를 보여주는 dialog 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2008/11/11\n
	▶ 참고사항:
		- 메인 view 화면에서 보여주는 정보를 포함하여 기타 상세 정보를 보여준다

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2008/11/11:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/


#pragma once

#include "ListCtrlEx/ListCtrlEx.h"
#include "HostInfo.h"
#include "PropertyGrid/PropertyGrid.h"
// CHostInfoDlg 대화 상자입니다.

class CHostInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CHostInfoDlg)

public:
	CHostInfoDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CHostInfoDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HOST_DETAIL_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	CPropertyGrid	m_ctrlGrid;							///<Host 정보를 보여주는 리스트 컨트롤
	CHostInfo		m_clsHostInfo;						///<Host 정보를 갖는 클래스
	//CFont			m_clsFont;
	//CBrush			m_brushBack;						// 대화창 바탕색

	DECLARE_MESSAGE_MAP()
public:
	void	SetHostInfo(CHostInfo* pclsHostInfo);		///<Host 정보를 설정한다.
	
	virtual BOOL OnInitDialog();
	//afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
