// VNCManagerView.cpp : CVNCManagerView 클래스의 구현
//

#include "stdafx.h"
#include "VNCManager.h"

#include "VNCManagerDoc.h"
#include "VNCManagerView.h"

#include "BypassSocket.h"
#include "ListenSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		VNC_PATH	"C:\\Program Files\\UltraVNC\\vncviewer.exe"

// Modified by 정운형 2008-11-10 오후 1:52
// 변경내역 :  Host 정보 보이기 수정작업
#include "HostInfoDlg.h"
#include "MainFrm.h"
// Modified by 정운형 2008-11-10 오후 1:52
// 변경내역 :  Host 정보 보이기 수정작업


// CVNCManagerView


IMPLEMENT_DYNCREATE(CVNCManagerView, CFormView)

BEGIN_MESSAGE_MAP(CVNCManagerView, CFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_COMMAND(ID_CONFIG_VNC_PATH, &CVNCManagerView::OnConfigVNCPath)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_VNC, &CVNCManagerView::OnNMDblclkListVnc)
	ON_MESSAGE(WM_CONNECT_SOCKET, OnConnectSocket)
	ON_MESSAGE(WM_CLOSE_SOCKET, OnCloseSocket)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_VNC, &CVNCManagerView::OnNMRclickListVnc)
	ON_COMMAND(ID_RUN_MAIN_VNC_VIEWER, &CVNCManagerView::OnRunMainVNCViewer)
	ON_COMMAND(ID_RUN_SUB_VNC_VIEWER, &CVNCManagerView::OnRunSubVNCViewer)
	ON_COMMAND(ID_VIEW_HOST_DETAILINFO, &CVNCManagerView::OnViewDetail)
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_COMMAND(ID_INIT_HOST, &CVNCManagerView::OnInitHost)
END_MESSAGE_MAP()

// CVNCManagerView 생성/소멸

CVNCManagerView::CVNCManagerView()
	: CFormView(CVNCManagerView::IDD)
{
	// TODO: 여기에 생성 코드를 추가합니다.
	//m_brushBG.CreateSolidBrush(RGB(0,0,0));
}

CVNCManagerView::~CVNCManagerView()
{
}

void CVNCManagerView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_VNC, m_listVNC);
}

void CVNCManagerView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(m_listVNC.GetSafeHwnd())
	{
		CRect rect;
		GetClientRect(rect);

		m_listVNC.MoveWindow(rect);
	}
}

void CVNCManagerView::OnDestroy()
{
	ClearList();

	CFormView::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 리스트를 정리한다 \n
/////////////////////////////////////////////////////////////////////////////////
void CVNCManagerView::ClearList()
{
	int count = m_listVNC.GetItemCount();
	for(int i=0; i<count; i++)
	{
		VNC_INFO* info = (VNC_INFO*)m_listVNC.GetItemData(i);

		// Modified by 정운형 2008-11-11 오전 11:52
		// 변경내역 :  Host 정보 보이기 수정작업
		if(!info)
		{
			continue;
		}//if
		// Modified by 정운형 2008-11-11 오전 11:52
		// 변경내역 :  Host 정보 보이기 수정작업

		delete (info->pHealthCheckListenSocket);
		delete (info->pLocalListenVNCSocket);
		delete (info->pRemoteListenVNCSocket);
		delete (info->pLocalListenRDCSocket);
		delete (info->pRemoteListenRDCSocket);

		// Modified by 정운형 2008-11-11 오전 11:52
		// 변경내역 :  Host 정보 보이기 수정작업
		delete (info->m_pclsHostInfo);
		// Modified by 정운형 2008-11-11 오전 11:52
		// 변경내역 :  Host 정보 보이기 수정작업

		delete info;
	}//for

	m_listVNC.DeleteAllItems();
}

BOOL CVNCManagerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CVNCManagerView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	// Modified by 정운형 2008-11-10 오후 5:28
	// 변경내역 :  Host 정보 보이기 수정작업
	/*
	{
		CBitmap bmp;
		bmp.LoadBitmap(IDB_VNC32);
		m_imglist32.Create(32, 32, ILC_COLORDDB | ILC_MASK, 1, 2);
		m_imglist32.Add(&bmp, RGB(192, 192, 192)); // 마스크색 지정
		m_listVNC.SetImageList(&m_imglist32, LVSIL_SMALL);
	}
	//
	{
		CBitmap bmp;
		bmp.LoadBitmap(IDB_VNC48);
		m_imglist48.Create(48, 48, ILC_COLORDDB | ILC_MASK, 1, 2);
		m_imglist48.Add(&bmp, RGB(192, 192, 192)); // 마스크색 지정
		m_listVNC.SetImageList(&m_imglist48, LVSIL_NORMAL);
	}
	*/
	//CBitmap bmp;
	//bmp.LoadBitmap(IDB_VNC24);
	//m_imglist24.Create(24, 24, ILC_COLORDDB | ILC_MASK, 1, 2);
	//m_imglist24.Add(&bmp, RGB(192, 192, 192)); // 마스크색 지정
	//m_listVNC.SetImageList(&m_imglist24, LVSIL_SMALL);

	////CBitmap bmp;
	//bmp.LoadBitmap(IDB_VNC32);
	//m_imglist32.Create(32, 32, ILC_COLORDDB | ILC_MASK, 1, 2);
	//m_imglist32.Add(&bmp, RGB(192, 192, 192)); // 마스크색 지정
	//m_listVNC.SetImageList(&m_imglist32, LVSIL_NORMAL);
	
	// Modified by 정운형 2008-11-10 오후 5:28
	// 변경내역 :  Host 정보 보이기 수정작업

	// Modified by 정운형 2008-11-07 오후 5:12
	// 변경내역 :  Host 정보 보이기 수정작업
	/*
	m_clsFont.CreateFont(16,						// nHeight
                         0,							// nWidth
                         0,							// nEscapement
                         0,							// nOrientation
                         FW_BOLD,					// nWeight
                         FALSE,						// bItalic
                         FALSE,						// bUnderline
                         0,							// cStrikeOut
                         ANSI_CHARSET,				// nCharSet
                         OUT_DEFAULT_PRECIS,		// nOutPrecision
                         CLIP_DEFAULT_PRECIS,		// nClipPrecision
                         DEFAULT_QUALITY,			// nQuality
                         DEFAULT_PITCH | FF_SWISS,
                         _T("굴림"));
	 */
	LOGFONT LogFont;
    memset(&LogFont, 0x00, sizeof(LogFont));
    strncpy(LogFont.lfFaceName, _T("Tahoma"), LF_FACESIZE);
	LogFont.lfWeight = FW_BOLD;
    LogFont.lfHeight = 16;

    m_clsFont.CreateFontIndirect(&LogFont);
	m_listVNC.SetFont(&m_clsFont);

	CRect clsRect;
	m_listVNC.GetClientRect(&clsRect);
	int nWidth = clsRect.Width();
	// Modified by 정운형 2008-11-28 오전 10:50
	// 변경내역 :  addr4필드 추가
	//float fSize[7] = { 1.0, 0.5, 1.0, 0.5, 0.5, 1.0, 2.0 };
	float fSize[8] = { 1.0, 1.0, 0.5, 1.0, 0.5, 0.5, 1.0, 2.0 };
	int i=0;
	//m_listVNC.InsertColumn(COLUMN_TITLE,	"Title",	0, 50);
	//m_listVNC.InsertColumn(COLUMN_SITE,	"Site",		LVCFMT_LEFT,	50);
	//m_listVNC.InsertColumn(COLUMN_HOST,	"Host",		LVCFMT_LEFT,	50);
	m_listVNC.InsertColumn(COLUMN_ADDR4,		"ADDR4",		LVCFMT_CENTER,	nWidth*fSize[i]);
	// Modified by 정운형 2008-11-28 오전 10:50
	// 변경내역 :  addr4필드 추가
	m_listVNC.InsertColumn(COLUMN_TITLE,		"Host name",	LVCFMT_CENTER,	nWidth*fSize[i]);
	m_listVNC.InsertColumn(COLUMN_SITE,			"Site id",		LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_HOST,			"Host id",		LVCFMT_CENTER,	nWidth*fSize[++i]);

	m_listVNC.InsertColumn(COLUMN_USAGE,		"Usage",		LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_POWER,		"Power",		LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_MODEL,		"Model",		LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_DESC,			"Description",	LVCFMT_LEFT,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_MACADDR,		"MAC Address",	LVCFMT_LEFT,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_AUTHDATE,		"Auth date",	LVCFMT_LEFT,	nWidth*fSize[++i]);

	/*
	m_listVNC.InsertColumn(COLUMN_STATUS,		"Status",		LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_IP,			"IP",			LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_MAIN_PORT,	"MainPort",		LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_SUB_PORT,		"SubPort",		LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_STARTUP_TIME,	"Startup time",	LVCFMT_CENTER,	nWidth*fSize[++i]);
	m_listVNC.InsertColumn(COLUMN_SHUTDOWN_TIME,"Shutdown time",LVCFMT_CENTER,	nWidth*fSize[++i]);
	*/
	
	//m_listVNC.ModifyStyle(NULL, LVS_ICON);
	m_listVNC.ModifyStyle(NULL, LVS_REPORT|LVS_SINGLESEL);
	m_listVNC.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_DOUBLEBUFFER);

	// Modified by 정운형 2008-11-28 오전 10:50
	// 변경내역 :  addr4필드 추가
	m_listVNC.SetColumnSorting(COLUMN_ADDR4, CListCtrlEx::Auto, CListCtrlEx::StringNoCase);
	// Modified by 정운형 2008-11-28 오전 10:50
	// 변경내역 :  addr4필드 추가
	m_listVNC.SetColumnSorting(COLUMN_TITLE, CListCtrlEx::Auto, CListCtrlEx::StringNoCase);
	m_listVNC.SetColumnSorting(COLUMN_SITE, CListCtrlEx::Auto, CListCtrlEx::StringNoCase);
	m_listVNC.SetColumnSorting(COLUMN_HOST, CListCtrlEx::Auto, CListCtrlEx::StringNoCase);
	m_listVNC.SetColumnSorting(COLUMN_USAGE, CListCtrlEx::Auto, CListCtrlEx::StringNoCase);
	m_listVNC.SetColumnSorting(COLUMN_POWER, CListCtrlEx::Auto, CListCtrlEx::StringNoCase);
	m_listVNC.SetColumnSorting(COLUMN_MODEL, CListCtrlEx::Auto, CListCtrlEx::StringNoCase);
	
	//m_listVNC.SetIconSpacing(CSize(80, 105));
	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업
	//
	InitList();

	m_strVNCPath = VNC_PATH;

	SetTimer(TIMER_WND_SIZE, 100, NULL);
}


// CVNCManagerView 진단

#ifdef _DEBUG
void CVNCManagerView::AssertValid() const
{
	CFormView::AssertValid();
}

void CVNCManagerView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CVNCManagerDoc* CVNCManagerView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CVNCManagerDoc)));
	return (CVNCManagerDoc*)m_pDocument;
}
#endif //_DEBUG


// CVNCManagerView 메시지 처리기

int CVNCManagerView::InitList()
{
	ClearList();
//#ifdef DEBUG
	/*AddHost("MVP", "MVP", "192.168.1.4", 5904);
	AddHost("UBC1", "UBC1", "192.168.1.1", 5901);
	AddHost("UBC2", "UBC2", "192.168.1.2", 5902);
	AddHost("UBC3", "UBC3", "192.168.1.3", 5903);
	AddHost("UBC5", "UBC5", "192.168.1.5", 5905);*/
	//AddHost("ktf", "KTF-0001", "192.168.1.7", 5907);
//#endif

	int counter=0;
	cciCall call("get");

	cciEntity aEntity("PM=*/Site=*/Host=*");
	call.setEntity(aEntity);

	cciAny nullAny;
	call.addItem("attributes", nullAny);

	try
	{
		if(call.call())
		{
			ciString	siteId;
			ciString	hostId;
			ciString	ipAddress;
			// Modified by 정운형 2008-11-07 오후 4:43
			// 변경내역 :  Host 정보 보이기 수정작업
			ciString	strHostName;
			ciString	strStartupTime;
			ciString	strSutdownTime;
			ciBoolean	bAdminState;
			ciBoolean	bOperationState;
			ciString	strModel;
			ciString	strDesc;
			ciLong		vncPort;
			// Modified by 정운형 2008-11-07 오후 4:43
			// 변경내역 :  Host 정보 보이기 수정작업
			//ciInt	vncPort;

			// Modified by 정운형 2008-11-28 오전 10:50
			// 변경내역 :  addr4필드 추가
			ciString	strAddr4;
			// Modified by 정운형 2008-11-28 오전 10:50
			// 변경내역 :  addr4필드 추가
			ciString	strMacaddr;
			cciTime		tmAuthDate;

			while(call.hasMore()) { // skpark add aCall.hasMore()!
			cciReply reply;
			while(call.getReply(reply))	 // skpark add aCall.hasMore()!
			{
				CString str = reply.toString();
				if(str.GetLength() < 10) continue;

				reply.unwrap();

				vncPort = 0;

				reply.getItem("siteId",				siteId);
				reply.getItem("hostId",				hostId);
				reply.getItem("ipAddress",			ipAddress);
				// Modified by 정운형 2008-11-07 오후 4:43
				// 변경내역 :  Host 정보 보이기 수정작업
				reply.getItem("hostName",			strHostName);
				reply.getItem("startupTime",		strStartupTime);
				reply.getItem("shutdownTime",		strSutdownTime);
				reply.getItem("adminState",			bAdminState);
				reply.getItem("operationalState",	bOperationState);
				reply.getItem("model",				strModel);
				reply.getItem("description",		strDesc);
				reply.getItem("vncPort",			vncPort);
				// Modified by 정운형 2008-11-07 오후 4:43
				// 변경내역 :  Host 정보 보이기 수정작업

				// Modified by 정운형 2008-11-28 오전 10:50
				// 변경내역 :  addr4필드 추가
				reply.getItem("addr4",				strAddr4);
				// Modified by 정운형 2008-11-28 오전 10:50
				// 변경내역 :  addr4필드 추가

				/*cciAttribute attr;
				reply.getItem("vncPort",	attr);
				attr.getValueInt(vncPort);*/

				reply.getItem("macAddress",				strMacaddr);
				reply.getItem("authDate",				tmAuthDate);

				// Modified by 정운형 2008-11-07 오후 4:43
				// 변경내역 :  Host 정보 보이기 수정작업
				/*if(vncPort != 0)
					AddHost(siteId.c_str(), hostId.c_str(), ipAddress.c_str(), vncPort);*/
				if(vncPort != 0)
				{
					CHostInfo*	pclsHostInfo = new CHostInfo();
					pclsHostInfo->m_strHostName			= strHostName.c_str();			
					pclsHostInfo->m_strSiteID			= siteId.c_str();			
					pclsHostInfo->m_strHostID			= hostId.c_str();			
					pclsHostInfo->m_strAdminState		= (bAdminState==true ? _T("On") : _T("Off"));		
					pclsHostInfo->m_strOperationState	= (bOperationState==true ? _T("On") : _T("Off"));	
					pclsHostInfo->m_strModel			= strModel.c_str();				
					pclsHostInfo->m_strDescription		= strDesc.c_str();		

					pclsHostInfo->m_strStatus			= _T("Off");			
					pclsHostInfo->m_strIP				= ipAddress.c_str();				
					pclsHostInfo->m_strMainPort			= ::ToString(vncPort);		
					pclsHostInfo->m_strSubPort			= ::ToString(vncPort + LOCAL_LOCAL_RDC_PORT_GAP);			
					pclsHostInfo->m_strStartupTime		= strStartupTime.c_str();		
					pclsHostInfo->m_strShutdownTime		= strSutdownTime.c_str();

					// Modified by 정운형 2008-11-28 오전 10:50
					// 변경내역 :  addr4필드 추가
					pclsHostInfo->m_strAddr4			= strAddr4.c_str();

					pclsHostInfo->m_strMacaddr			= strMacaddr.c_str();
					pclsHostInfo->m_strAuthDate			= tmAuthDate.toString();

					/*AddHost(siteId.c_str(), hostId.c_str(), ipAddress.c_str(), vncPort,
								strHostName.c_str(), strStartupTime.c_str(), strSutdownTime.c_str());*/
					AddHost(pclsHostInfo);
					counter++;
				}//if
				// Modified by 정운형 2008-11-07 오후 4:43
				// 변경내역 :  Host 정보 보이기 수정작업
			}}
		}
		else
		{
			// call fail
		}
	}
	catch(CORBA::Exception& ex)
	{
		// call fail
	}

	RefreshListStatus();

	// Modified by 정운형 2008-11-10 오후 5:10
	// 변경내역 :  Host 정보 보이기 수정작업
	m_listVNC.AdjustColumnWidth();
	// Modified by 정운형 2008-11-10 오후 5:10
	// 변경내역 :  Host 정보 보이기 수정작업

	return counter;
}

// 호스트의 상태가 변경되었음을 PM 서버에 알림(cciCall)
bool CVNCManagerView::SetHostStatusCCICall(LPCTSTR siteId, LPCTSTR hostId, bool status)
{
	cciCall call("set");

	cciEntity aEntity("PM=*/Site=%s/Host=%s", siteId, hostId);
	call.setEntity(aEntity);

	call.addItem("vncState", status);
	call.wrap();

	try
	{
		if(call.call())
		{
			cciReply reply;
			while(call.getReply(reply))
			{
				; // dummy code		
			}
		}
		else
		{
			// call fail
			return false;
		}
	}
	catch(CORBA::Exception& ex)
	{
		// call fail
		return false;
	}
	return true;
}

// Modified by 정운형 2008-11-07 오후 4:43
// 변경내역 :  Host 정보 보이기 수정작업
//void CVNCManagerView::AddHost(LPCTSTR lpszSite, LPCTSTR lpszHost, LPCTSTR lpszIP, int nPort)
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 호스트 정보로 각 소켓을 생성하고 리스트 컨트롤에 넣는다 \n
/// @param (LPCTSTR) lpszSite : (in) site id
/// @param (LPCTSTR) lpszHost : (in) host id
/// @param (LPCTSTR) lpszIP : (in) host의 ip address
/// @param (int) nPort : (in/out) vnc proxy port
/// @param (LPCTSTR) lpstrHostName : (in) host name
/// @param (LPCTSTR) lpstrStartupTime : (in) auto startup time
/// @param (LPCTSTR) lpstrSutdownTime : (in) auto shutdown time
/// @return <형: void> \n
/////////////////////////////////////////////////////////////////////////////////
void CVNCManagerView::AddHost(LPCTSTR lpszSite, LPCTSTR lpszHost, LPCTSTR lpszIP, int nPort,
							  LPCTSTR lpstrHostName, LPCTSTR lpstrStartupTime, LPCTSTR lpstrSutdownTime)
							  {
	//
	VNC_INFO* info = new VNC_INFO;
	info->pHealthCheckListenSocket	= new CListenSocket(SOCKET_TYPE_HEALTH_CHECK,	nPort + LOCAL_HEALTH_PORT_GAP);
	info->pLocalListenVNCSocket		= new CListenSocket(SOCKET_TYPE_LOCAL,			nPort/*VNC_PORT*//*);
	info->pRemoteListenVNCSocket	= new CListenSocket(SOCKET_TYPE_REMOTE,			nPort + LOCAL_REMOTE_VNC_PORT_GAP);
	info->pLocalListenRDCSocket		= new CListenSocket(SOCKET_TYPE_LOCAL,			nPort + LOCAL_LOCAL_RDC_PORT_GAP);
	info->pRemoteListenRDCSocket	= new CListenSocket(SOCKET_TYPE_REMOTE,			nPort + LOCAL_REMOTE_RDC_PORT_GAP);

	info->pHealthCheckListenSocket->SetLinkListenSocket(info->pRemoteListenVNCSocket, info->pLocalListenVNCSocket);
	info->pLocalListenVNCSocket->SetLinkListenSocket(info->pRemoteListenVNCSocket, info->pHealthCheckListenSocket);
	info->pRemoteListenVNCSocket->SetLinkListenSocket(info->pLocalListenVNCSocket, info->pHealthCheckListenSocket);
	info->pLocalListenRDCSocket->SetLinkListenSocket(info->pRemoteListenRDCSocket, info->pHealthCheckListenSocket);
	info->pRemoteListenRDCSocket->SetLinkListenSocket(info->pLocalListenRDCSocket, info->pHealthCheckListenSocket);

	info->pHealthCheckListenSocket->StartListen();
	info->pLocalListenVNCSocket->StartListen();
	info->pRemoteListenVNCSocket->StartListen();
	info->pLocalListenRDCSocket->StartListen();
	info->pRemoteListenRDCSocket->StartListen();

	//
	int count = m_listVNC.GetItemCount();

	m_listVNC.InsertItem(count, "", 0);
	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업
	//m_listVNC.SetItemText(count, COLUMN_TITLE,		"");
	m_listVNC.SetItemText(count, COLUMN_TITLE,		lpstrHostName);
	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업
	m_listVNC.SetItemText(count, COLUMN_SITE,		lpszSite);
	m_listVNC.SetItemText(count, COLUMN_HOST,		lpszHost);
	//m_listVNC.SetItemText(count, COLUMN_STATUS,		"0");
	m_listVNC.SetItemText(count, COLUMN_STATUS,		"off");
	m_listVNC.SetItemText(count, COLUMN_IP,			lpszIP);
	m_listVNC.SetItemText(count, COLUMN_MAIN_PORT,	::ToString(nPort));
	m_listVNC.SetItemText(count, COLUMN_SUB_PORT,	::ToString(nPort + LOCAL_LOCAL_RDC_PORT_GAP));
	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업
	m_listVNC.SetItemText(count, COLUMN_STARTUP_TIME,	lpstrStartupTime);
	m_listVNC.SetItemText(count, COLUMN_SHUTDOWN_TIME,	lpstrSutdownTime);
	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업

	m_listVNC.SetItemData(count, (DWORD_PTR)info);
}
*/
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 호스트 정보로 각 소켓을 생성하고 리스트 컨트롤에 넣는다 \n
/// @param (CHostInfo*) pclsHost : (in) 서버로 부터 받은 host 정보를 갖는 클래스
/// @return <형: void> \n
/////////////////////////////////////////////////////////////////////////////////
void CVNCManagerView::AddHost(CHostInfo* pclsHost)
// Modified by 정운형 2008-11-07 오후 4:43
// 변경내역 :  Host 정보 보이기 수정작업
{
	int nPort = atoi(pclsHost->m_strMainPort);
	VNC_INFO* info = new VNC_INFO;
	info->pHealthCheckListenSocket	= new CListenSocket(SOCKET_TYPE_HEALTH_CHECK,	nPort + LOCAL_HEALTH_PORT_GAP);
	info->pLocalListenVNCSocket		= new CListenSocket(SOCKET_TYPE_LOCAL,			nPort/*VNC_PORT*/);
	info->pRemoteListenVNCSocket	= new CListenSocket(SOCKET_TYPE_REMOTE,			nPort + LOCAL_REMOTE_VNC_PORT_GAP);
	info->pLocalListenRDCSocket		= new CListenSocket(SOCKET_TYPE_LOCAL,			nPort + LOCAL_LOCAL_RDC_PORT_GAP);
	info->pRemoteListenRDCSocket	= new CListenSocket(SOCKET_TYPE_REMOTE,			nPort + LOCAL_REMOTE_RDC_PORT_GAP);

	info->pHealthCheckListenSocket->SetLinkListenSocket(info->pRemoteListenVNCSocket, info->pLocalListenVNCSocket);
	info->pLocalListenVNCSocket->SetLinkListenSocket(info->pRemoteListenVNCSocket, info->pHealthCheckListenSocket);
	info->pRemoteListenVNCSocket->SetLinkListenSocket(info->pLocalListenVNCSocket, info->pHealthCheckListenSocket);
	info->pLocalListenRDCSocket->SetLinkListenSocket(info->pRemoteListenRDCSocket, info->pHealthCheckListenSocket);
	info->pRemoteListenRDCSocket->SetLinkListenSocket(info->pLocalListenRDCSocket, info->pHealthCheckListenSocket);

	info->pHealthCheckListenSocket->StartListen();
	info->pLocalListenVNCSocket->StartListen();
	info->pRemoteListenVNCSocket->StartListen();
	info->pLocalListenRDCSocket->StartListen();
	info->pRemoteListenRDCSocket->StartListen();

	info->m_pclsHostInfo = pclsHost;
	//
	int count = m_listVNC.GetItemCount();

	m_listVNC.InsertItem(count, "", 0);
	// Modified by 정운형 2008-11-28 오전 10:52
	// 변경내역 :  addr4필드 추가
	m_listVNC.SetItemText(count, COLUMN_ADDR4,		pclsHost->m_strAddr4);
	// Modified by 정운형 2008-11-28 오전 10:52
	// 변경내역 :  addr4필드 추가
	m_listVNC.SetItemText(count, COLUMN_TITLE,		pclsHost->m_strHostName);
	m_listVNC.SetItemText(count, COLUMN_SITE,		pclsHost->m_strSiteID);
	m_listVNC.SetItemText(count, COLUMN_HOST,		pclsHost->m_strHostID);

	m_listVNC.SetItemText(count, COLUMN_USAGE,		pclsHost->m_strAdminState);
	m_listVNC.SetItemText(count, COLUMN_POWER,		pclsHost->m_strOperationState);
	m_listVNC.SetItemText(count, COLUMN_MODEL,		pclsHost->m_strModel);
	m_listVNC.SetItemText(count, COLUMN_DESC,		pclsHost->m_strDescription);	

	m_listVNC.SetItemText(count, COLUMN_MACADDR,	pclsHost->m_strMacaddr);	
	m_listVNC.SetItemText(count, COLUMN_AUTHDATE,	pclsHost->m_strAuthDate);	

	m_listVNC.SetItemData(count, (DWORD_PTR)info);
}

void CVNCManagerView::RefreshListStatus(int index)
{
	int i=0, count;

	if(index == -1)
		count = m_listVNC.GetItemCount();
	else
	{
		i = index;
		count = i+1;
	}

	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업
	CString strStatus;
	VNC_INFO* pInfo = NULL;
	/*LVITEM item;
	item.mask = LVIF_IMAGE;
	item.iSubItem = 0;*/
	for(; i<count; i++)
	{
		/*
		int status = atoi(m_listVNC.GetItemText(i, COLUMN_STATUS));

		CString title;
		title.Format("%s\n%s"
			, m_listVNC.GetItemText(i, COLUMN_HOST)
			, (status == 0 ? "Off-line" : "On-line")
		);

		m_listVNC.SetItemText(i, COLUMN_TITLE, title);
	
		LVITEM item;
		item.mask = LVIF_IMAGE;
		item.iItem = i;
		item.iSubItem = 0;
		item.iImage = (status == 0);

		m_listVNC.SetItem(&item);
		*/

		//item.iItem = i;
		//strStatus = m_listVNC.GetItemText(i, COLUMN_STATUS);
		pInfo = (VNC_INFO*)m_listVNC.GetItemData(i);			
		if(pInfo->m_pclsHostInfo->m_strStatus == _T("On"))
		{
			m_listVNC.SetRowColors(i, COLOR_ONLINE_BK, COLOR_ONLINE);
			SetHostStatusCCICall(
				pInfo->m_pclsHostInfo->m_strSiteID.GetBuffer(), 
				pInfo->m_pclsHostInfo->m_strHostID, 
				true
				);
			//item.iImage = 0;
		}
		else
		{
			m_listVNC.SetRowColors(i, COLOR_OFFLINE_BK, COLOR_OFFLINE);
			SetHostStatusCCICall(
				pInfo->m_pclsHostInfo->m_strSiteID.GetBuffer(), 
				pInfo->m_pclsHostInfo->m_strHostID, 
				false
				);
			//item.iImage = 1;
		}//if

		//m_listVNC.SetItem(&item);
	}//for
	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업
}

void CVNCManagerView::OnNMDblclkListVnc(NMHDR *pNMHDR, LRESULT *pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int nItem = pNMListView->iItem;
	int nSubItem = pNMListView->iSubItem;

	if(nItem<0 || nSubItem<0)
		return;

	RunVNCViewer(true, nItem);
}

void CVNCManagerView::RunVNCViewer(bool bMain, int nIndex)
{
	// Modified by 정운형 2008-11-10 오후 5:04
	// 변경내역 :  Host 정보 보이기 수정작업
	/*
	CString title		= m_listVNC.GetItemText(nIndex, COLUMN_TITLE);
	CString site		= m_listVNC.GetItemText(nIndex, COLUMN_SITE);
	CString host		= m_listVNC.GetItemText(nIndex, COLUMN_HOST);
	CString status		= m_listVNC.GetItemText(nIndex, COLUMN_STATUS);
	CString ip			= m_listVNC.GetItemText(nIndex, COLUMN_IP);
	CString main_port	= m_listVNC.GetItemText(nIndex, COLUMN_MAIN_PORT);
	CString sub_port	= m_listVNC.GetItemText(nIndex, COLUMN_SUB_PORT);
	*/

	VNC_INFO* pInfo = (VNC_INFO*)m_listVNC.GetItemData(nIndex);
	CString title		= pInfo->m_pclsHostInfo->m_strHostName;
	CString site		= pInfo->m_pclsHostInfo->m_strSiteID;
	CString host		= pInfo->m_pclsHostInfo->m_strHostID;
	CString status		= pInfo->m_pclsHostInfo->m_strStatus;
	CString ip			= pInfo->m_pclsHostInfo->m_strIP;
	CString main_port	= pInfo->m_pclsHostInfo->m_strMainPort;
	CString sub_port	= pInfo->m_pclsHostInfo->m_strSubPort;

	//if(status.CompareNoCase("1") != 0)
	if(status.CompareNoCase(_T("On")) != 0)
	// Modified by 정운형 2008-11-10 오후 5:04
	// 변경내역 :  Host 정보 보이기 수정작업
	{
		::AfxMessageBox("This Host is OFF-LINE !!!", MB_ICONWARNING);
		return;
	}

	CFileStatus fs;
	CString filename;
	if(m_strVNCPath.GetLength() == 0 || CFile::GetStatus(m_strVNCPath, fs) == FALSE)
	{
		::AfxMessageBox("Not found VNC-Viewer.\r\n\t\nCheck and Config VNC-Viewer Path !!!\r\n ", MB_ICONSTOP);
		return;
	}

	if(bMain)
		filename.Format("%s %s:%s", m_strVNCPath, "127.0.0.1", main_port);
	else
		filename.Format("%s %s:%s", m_strVNCPath, "127.0.0.1", sub_port);

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );

	// Start the child process. 
	if( !CreateProcess( NULL,   // No module name (use command line). 
		(LPSTR)(LPCTSTR)filename, // Command line. 
		NULL,             // Process handle not inheritable. 
		NULL,             // Thread handle not inheritable. 
		FALSE,            // Set handle inheritance to FALSE. 
		0,                // No creation flags. 
		NULL,             // Use parent's environment block. 
		NULL,             // Use parent's starting directory. 
		&si,              // Pointer to STARTUPINFO structure.
		&pi )             // Pointer to PROCESS_INFORMATION structure.
		)
	{
		//
		// fail
		//
	}
}

void CVNCManagerView::OnConfigVNCPath()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.

	TCHAR szFilters[]=
//		_T("Windows Bitmap (*.bmp)|*.bmp|All Files (*.*)|*.*||");
		_T("VNC-Viewer File (*.exe)|*.exe||");

	CFileDialog dlg(TRUE, _T("exe"), _T("vncviewer.exe"), OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilters, this);;

	if(dlg.DoModal() == IDOK)
	{
		m_strVNCPath = dlg.GetPathName();
	}
}

LRESULT CVNCManagerView::OnConnectSocket(WPARAM wParam, LPARAM lParam)
{
	int port = (int)wParam;
/*
	int count = m_listVNC.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if(port == atoi(m_listVNC.GetItemText(i, COLUMN_MAIN_PORT)))
		{
			// Modified by 정운형 2008-11-10 오후 1:35
			// 변경내역 :  Host 정보 보이기 수정작업
			//m_listVNC.SetItemText(i, COLUMN_STATUS, "1");
			m_listVNC.SetItemText(i, COLUMN_STATUS, "on");
			// Modified by 정운형 2008-11-10 오후 1:35
			// 변경내역 :  Host 정보 보이기 수정작업
			RefreshListStatus(i);

			break;
		}
	}
	*/

	// Modified by 정운형 2008-11-10 오후 1:35
	// 변경내역 :  Host 정보 보이기 수정작업
	VNC_INFO* pInfo = NULL;
	int count = m_listVNC.GetItemCount();
	for(int i=0; i<count; i++)
	{
		pInfo = (VNC_INFO*)m_listVNC.GetItemData(i);
		if(port == atoi(pInfo->m_pclsHostInfo->m_strMainPort))
		{
			pInfo->m_pclsHostInfo->m_strStatus = _T("On");
			RefreshListStatus(i);

			break;
		}//if
	}//for
	// Modified by 정운형 2008-11-10 오후 1:35
	// 변경내역 :  Host 정보 보이기 수정작업

	return 0;
}

LRESULT CVNCManagerView::OnCloseSocket(WPARAM wParam, LPARAM lParam)
{
	/*
	int port = wParam;
	int index = lParam;
	int count = m_listVNC.GetItemCount();

	for(int i=0; i<count; i++)
	{
		int p = atoi(m_listVNC.GetItemText(i, COLUMN_MAIN_PORT));
		VNC_INFO* info = (VNC_INFO*)m_listVNC.GetItemData(i);

		if( port == p + LOCAL_HEALTH_PORT_GAP)
		{
			int idx = info->pHealthCheckListenSocket->GetSocketCount()-1;
			if( idx == index )
			{
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				//m_listVNC.SetItemText(i, COLUMN_STATUS, "0");
				m_listVNC.SetItemText(i, COLUMN_STATUS, "off");
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				RefreshListStatus(i);
			}
			break;
		}
		else if( port == p )
		{
			int idx = info->pLocalListenVNCSocket->GetSocketCount()-1;
			if( idx == index )
			{
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				//m_listVNC.SetItemText(i, COLUMN_STATUS, "0");
				m_listVNC.SetItemText(i, COLUMN_STATUS, "off");
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				RefreshListStatus(i);
			}
			break;
		}
		else if( port == p + LOCAL_REMOTE_VNC_PORT_GAP )
		{
			int idx = info->pRemoteListenVNCSocket->GetSocketCount()-1;
			if( idx == index )
			{
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				//m_listVNC.SetItemText(i, COLUMN_STATUS, "0");
				m_listVNC.SetItemText(i, COLUMN_STATUS, "off");
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				RefreshListStatus(i);
			}
			break;
		}
		else if( port == p + LOCAL_LOCAL_RDC_PORT_GAP )
		{
			int idx = info->pLocalListenRDCSocket->GetSocketCount()-1;
			if( idx == index )
			{
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				//m_listVNC.SetItemText(i, COLUMN_STATUS, "0");
				m_listVNC.SetItemText(i, COLUMN_STATUS, "off");
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				RefreshListStatus(i);
			}
			break;
		}
		else if( port == p + LOCAL_REMOTE_RDC_PORT_GAP )
		{
			int idx = info->pRemoteListenRDCSocket->GetSocketCount()-1;
			if( idx == index )
			{
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				//m_listVNC.SetItemText(i, COLUMN_STATUS, "0");
				m_listVNC.SetItemText(i, COLUMN_STATUS, "off");
				// Modified by 정운형 2008-11-10 오후 1:35
				// 변경내역 :  Host 정보 보이기 수정작업
				RefreshListStatus(i);
			}
			break;
		}
	}
	*/

	// Modified by 정운형 2008-11-10 오후 1:35
	// 변경내역 :  Host 정보 보이기 수정작업
	int port = wParam;
	int index = lParam;
	int count = m_listVNC.GetItemCount();

	for(int i=0; i<count; i++)
	{
		VNC_INFO* info = (VNC_INFO*)m_listVNC.GetItemData(i);
		int p = atoi(info->m_pclsHostInfo->m_strMainPort);

		if( port == p + LOCAL_HEALTH_PORT_GAP)
		{
			int idx = info->pHealthCheckListenSocket->GetSocketCount()-1;
			if( idx == index )
			{
				info->m_pclsHostInfo->m_strStatus = _T("Off");
				RefreshListStatus(i);
			}
			break;
		}
		else if( port == p )
		{
			int idx = info->pLocalListenVNCSocket->GetSocketCount()-1;
			if( idx == index )
			{
				info->m_pclsHostInfo->m_strStatus = _T("Off");
				RefreshListStatus(i);
			}
			break;
		}
		else if( port == p + LOCAL_REMOTE_VNC_PORT_GAP )
		{
			int idx = info->pRemoteListenVNCSocket->GetSocketCount()-1;
			if( idx == index )
			{
				info->m_pclsHostInfo->m_strStatus = _T("Off");
				RefreshListStatus(i);
			}
			break;
		}
		else if( port == p + LOCAL_LOCAL_RDC_PORT_GAP )
		{
			int idx = info->pLocalListenRDCSocket->GetSocketCount()-1;
			if( idx == index )
			{
				info->m_pclsHostInfo->m_strStatus = _T("Off");
				RefreshListStatus(i);
			}
			break;
		}
		else if( port == p + LOCAL_REMOTE_RDC_PORT_GAP )
		{
			int idx = info->pRemoteListenRDCSocket->GetSocketCount()-1;
			if( idx == index )
			{
				info->m_pclsHostInfo->m_strStatus = _T("Off");
				RefreshListStatus(i);
			}
			break;
		}//if
	}//for

	return 0;
}

void CVNCManagerView::OnNMRclickListVnc(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	CMenu menu;
	if ( menu.LoadMenu(IDR_POPUP_MENU) )
	{
		CMenu* pSubmenu = menu.GetSubMenu(0);
		ASSERT(pSubmenu!=NULL);

		CPoint point;
		GetCursorPos(&point);

		POSITION pos = m_listVNC.GetFirstSelectedItemPosition();
		if (pos == NULL) {
			pSubmenu->EnableMenuItem(ID_RUN_MAIN_VNC_VIEWER, MF_GRAYED);
			pSubmenu->EnableMenuItem(ID_RUN_SUB_VNC_VIEWER, MF_GRAYED);
			pSubmenu->EnableMenuItem(ID_VIEW_HOST_DETAILINFO, MF_GRAYED);
		}

		pSubmenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
			point.x, point.y, this);

	}
}

void CVNCManagerView::OnRunMainVNCViewer()
{
	int index = m_listVNC.GetSelectionMark();

	if(index < 0) return;

	RunVNCViewer(true, index);
}

void CVNCManagerView::OnRunSubVNCViewer()
{
	int index = m_listVNC.GetSelectionMark();

	if(index < 0) return;

	RunVNCViewer(false, index);
}



void CVNCManagerView::OnViewDetail()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	POSITION pos = m_listVNC.GetFirstSelectedItemPosition();
	int nItem = m_listVNC.GetNextSelectedItem(pos);
	VNC_INFO* info = (VNC_INFO*)m_listVNC.GetItemData(nItem);
	
	CHostInfoDlg clsDlg;
	clsDlg.SetHostInfo(info->m_pclsHostInfo);
	clsDlg.DoModal();
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VNC 리스트 컨트롤의 각 column의 합을 반환한는 함수 \n
/// @return <형: int> \n
///			<m_listVNC의 각 column 넓이의 총합> \n
/////////////////////////////////////////////////////////////////////////////////
int CVNCManagerView::GetTotalColumnWidth()
{
	int nTotal = 0;
	// Modified by 정운형 2008-11-28 오전 10:52
	// 변경내역 :  addr4필드 추가
	nTotal += m_listVNC.GetColumnWidth(COLUMN_ADDR4);
	// Modified by 정운형 2008-11-28 오전 10:52
	// 변경내역 :  addr4필드 추가
	nTotal += m_listVNC.GetColumnWidth(COLUMN_TITLE);
	nTotal += m_listVNC.GetColumnWidth(COLUMN_SITE);
	nTotal += m_listVNC.GetColumnWidth(COLUMN_HOST);
	nTotal += m_listVNC.GetColumnWidth(COLUMN_USAGE);
	nTotal += m_listVNC.GetColumnWidth(COLUMN_POWER);
	nTotal += m_listVNC.GetColumnWidth(COLUMN_MODEL);
	nTotal += m_listVNC.GetColumnWidth(COLUMN_DESC);

	return nTotal;
}

void CVNCManagerView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	KillTimer(TIMER_WND_SIZE);

	int nWidth = GetTotalColumnWidth();

	WINDOWPLACEMENT stWndDpl;
	memset(&stWndDpl, 0x00, sizeof(WINDOWPLACEMENT));
	CMainFrame* pclsFrame = (CMainFrame*)::AfxGetMainWnd();
	pclsFrame->GetWindowPlacement(&stWndDpl);
	pclsFrame->MoveWindow(stWndDpl.rcNormalPosition.left,
							stWndDpl.rcNormalPosition.top,
							nWidth+15,
							stWndDpl.rcNormalPosition.right);

	Invalidate(TRUE);

	CFormView::OnTimer(nIDEvent);
}

HBRUSH CVNCManagerView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	/*
	if(	pWnd->GetSafeHwnd() != m_listVNC.GetSafeHwnd())
	{
		pDC->SetBkColor(RGB(0,0,0));
		//pDC->SetBkMode(TRANSPARENT);

		hbr = (HBRUSH)m_brushBG;
	}//if
	*/

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CVNCManagerView::OnInitHost()
{
	ciString siteId = "*";
	ciString enterpriseKey;
	ciString password,mac;
	scratchUtil::getInstance()->readAuthFile(enterpriseKey,mac,password);

	dbUtil* aUtil = dbUtil::getInstance();
	if(aUtil->hostAutoUpdate(siteId, enterpriseKey, password)){
		InitList();
	}

}
