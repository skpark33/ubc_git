#pragma once

// CListenSocket 명령 대상입니다.


#include "BypassSocket.h"


class CListenSocket : public CAsyncSocket
{
public:
	CListenSocket(SOCKET_TYPE socketType, int nPort);
	virtual ~CListenSocket();
	virtual void OnAccept(int nErrorCode);

	//
	CMutex	m_mutex;

	int		m_nPort;
	SOCKET_TYPE	m_SocketType;

	CListenSocket*		m_pLinkListenSocket1;
	CListenSocket*		m_pLinkListenSocket2;
	BYPASS_SOCKET_LIST	m_SocketList;
	CBypassSocket*		m_pCurrentBypassSocket;

	void	SetLinkListenSocket(CListenSocket* pSocket1, CListenSocket* pSocket2);
	void	LinkListenSocketCloseSocket();

	CBypassSocket*	CreateNewSocket();
	BOOL	StartListen();
	bool	CloseSocket();

	int		GetSocketCount() { return m_SocketList.GetCount(); };

	int 	SendEx(void* lpBuf, int nBufLen);

	void	CleartBuffer();
};


typedef	CArray<CListenSocket*, CListenSocket*>	LISTEN_SOCKET_LIST;
