//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VNCManager.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_VNCMANAGER_FORM             101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_VNCManagerTYPE              129
#define IDR_MAINFRAME1                  129
#define IDB_VNC32                       130
#define IDB_VNC48                       131
#define IDR_POPUP_MENU                  132
#define IDB_VNC24                       135
#define IDD_HOST_DETAIL_DLG             136
#define IDC_LIST_VNC                    1001
#define IDC_HOSTINFO_LIST               1002
#define IDC_INFO_STATIC                 1004
#define ID_CONFIG_VNC_PATH              32771
#define ID_RUN_MAIN_VNC_VIEWER          32772
#define ID_RUN_SUB_VNC_VIEWER           32773
#define ID_VIEW_HOST_DETAILINFO         32774
#define ID_32776                        32776
#define ID_INIT_HOST                    32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
