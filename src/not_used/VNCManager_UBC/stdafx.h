// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.
#include <afxmt.h>
#include <afxtempl.h>



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <afxsock.h>		// MFC 소켓 확장


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


#ifdef _VBROKER
#	include <corba.h>
#endif
#include "ace/ace.h"
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciAny.h>
#include <cci/libValue/cciAttribute.h>
#include <cci/libWrapper/cciEventHandler.h>
#include <cci/libWrapper/cciEventManager.h>
#include <cci/libWrapper/cciEvent.h>

#include "CMN/libCommon/utvMacro.h"
#include "CMN/libScratch/scratchUtil.h"
#include "CMN/libDBUtil/dbUtil.h"
#include "ci/libDebug/ciArgParser.h"


#define		WM_CONNECT_SOCKET		(WM_USER+100)
#define		WM_CLOSE_SOCKET			(WM_USER+101)


#define		LOCAL_HEALTH_PORT_GAP		100000
#define		LOCAL_REMOTE_VNC_PORT_GAP	200000
#define		LOCAL_LOCAL_RDC_PORT_GAP	300000
#define		LOCAL_REMOTE_RDC_PORT_GAP	400000


CString ToString(int nValue);


//
typedef struct
{
	int 	length;
	void*	buf;
} PACKET_DATA;

typedef CArray<PACKET_DATA, PACKET_DATA&> PACKET_DATA_LIST;

//
enum SOCKET_TYPE {
	SOCKET_TYPE_LOCAL = 0,
	SOCKET_TYPE_REMOTE,
	SOCKET_TYPE_HEALTH_CHECK,
};

//
typedef struct
{
	int port;
	int index;
	SOCKET_TYPE socket_type;
} SOCKET_CLOSE_PARAM;

// Modified by 정운형 2008-11-10 오후 1:52
// 변경내역 :  Host 정보 보이기 수정작업

#define COLOR_ONLINE_BK			RGB(255, 255, 0)		///<Host가 on-line status의 배경색상
#define COLOR_ONLINE			RGB(0, 0, 0)			///<Host가 on-line status의 색상
#define COLOR_OFFLINE_BK		RGB(204, 199, 186)		///<Host사 off-line status의 배경색상
#define COLOR_OFFLINE			RGB(225, 50, 50)		///<Host사 off-line status의 색상
/*
#define COLOR_ONLINE_BK			RGB(174, 222, 127)		///<Host가 on-line status의 배경색상
#define COLOR_ONLINE			RGB(255, 255, 255)		///<Host가 on-line status의 색상
#define COLOR_OFFLINE_BK		RGB(153, 153, 153)		///<Host사 off-line status의 배경색상
#define COLOR_OFFLINE			RGB(225, 255, 255)		///<Host사 off-line status의 색상
*/

#define TIMER_WND_SIZE		0x01					///<메인창의 넓이 조정을 위한 타이머 id
// Modified by 정운형 2008-11-10 오후 1:52
// 변경내역 :  Host 정보 보이기 수정작업


