// VNCManagerDoc.cpp : CVNCManagerDoc 클래스의 구현
//

#include "stdafx.h"
#include "VNCManager.h"

#include "VNCManagerDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CVNCManagerDoc

IMPLEMENT_DYNCREATE(CVNCManagerDoc, CDocument)

BEGIN_MESSAGE_MAP(CVNCManagerDoc, CDocument)
END_MESSAGE_MAP()


// CVNCManagerDoc 생성/소멸

CVNCManagerDoc::CVNCManagerDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CVNCManagerDoc::~CVNCManagerDoc()
{
}

BOOL CVNCManagerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CVNCManagerDoc serialization

void CVNCManagerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CVNCManagerDoc 진단

#ifdef _DEBUG
void CVNCManagerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CVNCManagerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CVNCManagerDoc 명령
