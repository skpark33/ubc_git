// VNCManagerView.h : CVNCManagerView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"

#include "ListenSocket.h"
// Modified by 정운형 2008-11-10 오전 11:45
// 변경내역 :  Host 정보 보이기 수정작업
//#include "VNCListCtrl.h"
//#include "ColorListCtrl/ColorListCtrl.h"
#include "ListCtrlEx/ListCtrlEx.h"
#include "HostInfo.h"
// Modified by 정운형 2008-11-10 오전 11:45
// 변경내역 :  Host 정보 보이기 수정작업



class CVNCManagerView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CVNCManagerView();
	DECLARE_DYNCREATE(CVNCManagerView)

public:
	enum{ IDD = IDD_VNCMANAGER_FORM };

// 특성입니다.
public:
	CVNCManagerDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CVNCManagerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnNMDblclkListVnc(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnConfigVNCPath();
	afx_msg LRESULT OnConnectSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseSocket(WPARAM wParam, LPARAM lParam);

protected:
	//CBrush	m_brushBG;

	enum {
		// Modified by 정운형 2008-11-28 오전 10:52
		// 변경내역 :  addr4필드 추가
		//COLUMN_TITLE=0,		///<Host name
		COLUMN_ADDR4=0,		///<Addr4
		COLUMN_TITLE,		///<Host name
		COLUMN_SITE,		///<Site id
		COLUMN_HOST,		///<Host id

		COLUMN_USAGE,		///<Admin state
		COLUMN_POWER,		///<Operation state
		COLUMN_MODEL,		///<Model
		COLUMN_DESC,		///<Description
		COLUMN_MACADDR,		///<Mac Address
		COLUMN_AUTHDATE		///<Auth Date

		// Modified by 정운형 2008-11-07 오후 5:12
		// 변경내역 :  Host 정보 보이기 수정작업
		/*
		COLUMN_STATUS,
		COLUMN_IP,
		COLUMN_MAIN_PORT,
		COLUMN_SUB_PORT,
		
		COLUMN_STARTUP_TIME,
		COLUMN_SHUTDOWN_TIME,
		*/
		// Modified by 정운형 2008-11-07 오후 5:12
		// 변경내역 :  Host 정보 보이기 수정작업
	};

	// Modified by 정운형 2008-11-10 오전 11:45
	// 변경내역 :  Host 정보 보이기 수정작업
	//CListCtrl	m_listVNC;
	//CColorListCtrl	m_listVNC;
	CListCtrlEx		m_listVNC;
	CFont			m_clsFont;
	// Modified by 정운형 2008-11-10 오전 11:45
	// 변경내역 :  Host 정보 보이기 수정작업
	CString		m_strVNCPath;

	// Modified by 정운형 2008-11-10 오후 5:28
	// 변경내역 :  Host 정보 보이기 수정작업
	//CImageList	m_imglist24;
	//CImageList	m_imglist32;
	//CImageList	m_imglist48;
	// Modified by 정운형 2008-11-10 오후 5:28
	// 변경내역 :  Host 정보 보이기 수정작업

	LISTEN_SOCKET_LIST	m_ListenSocketList;

	// 호스트의 상태가 변경되었음을 PM 서버에 알림(cciCall)
	bool	SetHostStatusCCICall(LPCTSTR siteId, LPCTSTR hostId, bool status);

	void	RefreshListStatus(int index = -1);
	int		InitList();
	void	ClearList(void);
	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업
	//void	AddHost(LPCTSTR lpszSite, LPCTSTR lpszHost, LPCTSTR lpszIP, int nPort);
	/*void	AddHost(LPCTSTR lpszSite, LPCTSTR lpszHost, LPCTSTR lpszIP, int nPort,
					LPCTSTR lpstrHostName, LPCTSTR lpstrStartupTime, LPCTSTR lpstrSutdownTime);	///<호스트 정보로 각 소켓을 생성하고 리스트 컨트롤에 넣는다 */
	void	AddHost(CHostInfo* pclsHost);		///<호스트 정보로 각 소켓을 생성하고 리스트 컨트롤에 넣는다
	int		GetTotalColumnWidth(void);			///<VNC 리스트 컨트롤의 각 column의 합을 반환하는 함수
	// Modified by 정운형 2008-11-07 오후 4:43
	// 변경내역 :  Host 정보 보이기 수정작업

	void	RunVNCViewer(bool bMain, int nIndex);

public:

	afx_msg void OnNMRclickListVnc(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnRunMainVNCViewer();
	afx_msg void OnRunSubVNCViewer();
	afx_msg void OnViewDetail();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnInitHost();
};

#ifndef _DEBUG  // VNCManagerView.cpp의 디버그 버전
inline CVNCManagerDoc* CVNCManagerView::GetDocument() const
   { return reinterpret_cast<CVNCManagerDoc*>(m_pDocument); }
#endif


/////////////////////////////////////////



