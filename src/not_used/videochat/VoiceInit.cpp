/*-----------------------------------------------------------------------------
| KJH                                                                          |
|------------------------------------------------------------------------------|
| Date: 2000.12.20                                                             |
| Tool: Visual C++ 6.0                                                         |
 -----------------------------------------------------------------------------*/
#include "stdafx.h"
#include "main.h"
#include "VideoViewDlg.h"

HANDLE hVoice;//음성파일 출력 쓰레탛E......핸탛E

SoundBuffer RecordReceivedSoundBuffer;//Sound buffer from network 레코드한것을 저장하기위한 사웝�갋버퍼
bool VoiceRecordSave=FALSE;          //퀋E실歐갋위함 퀋E�/입출력 Flag이다 TRUE이툈E퀋E슝緻謗� if문에 들엉黔다

bool VoiceAllSending;							   //모두에게 픸E� 할콅E

int InputReset=FALSE;			  				 //음성 입력이 끝난후 메모리 제거후 waveInStop컖EwaveInReset을 하콅E위함
int InputStop=FALSE;		  					 //wave 입력이 멈추엉缶있는가
int InputActive=FALSE;							 //wave 입력 파일이 열려있나
int OutputActive=FALSE;							 //wave 출력 파일이 열려있나?

int AudioChannel;		  						   //오디오 입력 채널(1:mone,2:streo)(1로하툈E여자목소리:2로하면남자목소리났다)

int samplesPerSecond=8000;					 //셈플 second
//int samplesPerSecond=11025;					 //셈플 second

//int bytesPerSecond=16000;						 //초큱E셈플 바이트
//int bytesPerSecond=22050;						 //초큱E셈플 바이트
int bytesPerSecond=16000;						 //초큱E셈플 바이트

//int sampleAlignment=65;							 //셈플 프레임 사이햨E
int sampleAlignment=2;	 						 //셈플 프레임 사이햨E
 
int bitsPerSample=16;		 						 //셈플큱E비트(8비트,16비트,32비트)

//8비트:파장을 256개로 나눔
//16비트:파장을 65535개로 나눔
//32비트 파장을 4294967296개로 나눔
//비트가 클수록 음성 파장을 더퓖E세분하콅E때문에 더퓖E음질이 섬세하도 그리컖E픸E邦� 틒E４�

int aboutInSamples=0;								 //초큱E입력 셈플
int aboutInBits;					  				 //셈플큱E입력 비트
int aboutOutSamples=0;							 //초큱E출력 셈플
int aboutOutBits;					  				 //셈플큱E출력 비트

int NoiseProcess;										 //값으로 음성 입력 커트라인 조절값(잡음 소음따病 절절하게 없애준다)

#define NumberWaveHeaders 4               //Wave �갋갋개펯E
LPWAVEHDR inWaveHeader[NumberWaveHeaders];//웨이틒E입력 버퍼 퓖E포인터
int waveHeadersAllocated=0;          //할당된 웨이틒E�갋促湧� 숫자

int waAudioHalf=FALSE;			   			 //Assume audio half-duplex; don't test
int waAudio11025=FALSE;							 //Assume audio 11025 samples/sec	
int halfDuplexTransition=FALSE;			 //Transitioning from output to input ?
int halfDuplex=FALSE;				  			 //Audio hardware is half-duplex (반이중)
int audioIs8Bit=FALSE;							 //만푳E오디오가 8비트퓖E見갋제로가 아니다
int audioUse8Bit=FALSE;							 //8비트 오디오를 쓴다
int netMaxSamples;									 //네트윅이 보낼펯E있는 최큱ESamples

LPWAVEHDR WaveRecord;								 //음성퀋E�
LPWAVEHDR WavePlay;									 //음성출력
LPWAVEHDR WavePlayTemp;									 //음성출력

HWAVEOUT hWaveOut=NULL;						   //wave 출력 핸탛E
HWAVEIN hWaveIn=NULL;								 //wave 입력 핸탛E

long outputPending=0;								 //큐의 출력 버퍼

static int squelched=FALSE;				//Squelched by VOX ? 찰斛러지는 소리 vox에 의해서	 
static int sqpacket=FALSE;				//Is this packet squelched 이패킷이 눌러찰斛러졌나?

int AdpcmCodecCompression=FALSE;    //ADPCM compression mode
int GsmCodecCompression=FALSE;      //GSM compression mode
int CurrentCodecSize;//현재코덱사이햨E
long CurrentCompression;//현픸E압축방식

extern char NowSelectAddress[128];//현픸E선택된 주소(보내는 주소)
extern long inputPacketsLost;     //Output packets lost due to net traffic
extern long outputPacketsLost;    //Output packets lost due to net traffic
extern int currentInputLength;	 	//현픸E기큱E풔� 입력 버퍼의 길이
extern int currentInputSamples;	  //현픸E입력 버퍼의 Samples
extern bool ThreadOn;      				//유저가 따遝 되툈E네트워크 쓰레드를 동작 하도록함
extern bool ClickOnceState;       //다시 다른 놈컖E흟E탔� 하는데 1:1로 만 할 펯E있도록 중복으로 엉망되는 음성을 방지한다
extern HWND hwnd;                 //윈도퓖E메시지퓖E핸탛E
extern SoundBuffer receivedSoundBuffer;    //Sound buffer from network
extern int ReadHandle;//읽콅E핸탛E
//extern bool G_Recording;          //레코딩인햨E??? TRUE / FALSE

//음성 출력 장치를 초기화시킨다
int InitVoiceDevice()
{
	//OutputDebugString("음성 출력 장치를 초기화 하였습니다!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  if(!OutputActive)//한번만 초기화 하도록 한다(만푳E장치에 에러가 생기툈EOutputActive값을 변경시켜 다시 초기화 시켜준다
	{
	  PCMWAVEFORMAT wfi;    
   	MMRESULT woo;
		if(halfDuplex && InputActive) 
		{
		  ++inputPacketsLost;
			return FALSE;
		} 
		else 
		{
			if(halfDuplex && InputActive) 
			{
				WaveInputStop();
				InputStop=TRUE;
				if(InputActive)//음성입력이 열려있다툈E
				{
					++inputPacketsLost;
					return FALSE;
				}
			}
    	wfi.wf.wFormatTag=WAVE_FORMAT_PCM;//Sampling방식
	   	wfi.wf.nChannels=AudioChannel;//1이툈EMono,2이툈EStreo
	   	wfi.wf.nSamplesPerSec=samplesPerSecond;//Sample Rate
	   	wfi.wf.nAvgBytesPerSec=bytesPerSecond;//for buffer estimation
	   	wfi.wf.nBlockAlign=sampleAlignment;//데이타 빚遝 사이햨E
	   	wfi.wBitsPerSample=bitsPerSample;//number of bits per sample of mono data
	   	while(TRUE) 
			{//음성 디바이스 장치를 검사한다
				woo=waveOutOpen(&hWaveOut,
					              (UINT)WAVE_MAPPER,
												(LPWAVEFORMATEX)&wfi,
												0L,
												0L,
												WAVE_FORMAT_QUERY);
		 		if(bitsPerSample>8&&woo==WAVERR_BADFORMAT) audioIs8Bit=TRUE;//8비트 방식으로 TRUE
		 		if((audioUse8Bit||woo==WAVERR_BADFORMAT)&&bitsPerSample>8) 
				{
		 			bitsPerSample/=2;
		 			sampleAlignment/=2;
		 			bytesPerSecond/=2;	
			    wfi.wf.nAvgBytesPerSec=bytesPerSecond;
			    wfi.wf.nBlockAlign=sampleAlignment;
			    wfi.wBitsPerSample=bitsPerSample;
			 		woo=waveOutOpen(&hWaveOut,//Wave장치를 검사한다
						               (UINT)WAVE_MAPPER,
													 (LPWAVEFORMATEX)&wfi,
													 0L,
													 0L,
													 WAVE_FORMAT_QUERY);
		 		}
		 		if(woo==WAVERR_BADFORMAT&&samplesPerSecond==8000) 
				{
  	 			samplesPerSecond=11025;
		 			bitsPerSample=16;
		 			sampleAlignment=bitsPerSample/8;
		 			bytesPerSecond=samplesPerSecond*sampleAlignment;  
		 		} 
				else break;
		 	} 
	 		if(woo!=0) 
			{
  	   	char et[MAXERRORLENGTH];
	    	waveOutGetErrorText(woo,et,sizeof et);//The waveform device can't play this format.
				//OutputDebugString("IDS_T_WAVE_PLAY_FORMAT_ERR");
				return FALSE;
	    }
		  if((woo=waveOutOpen(&hWaveOut,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)&OutWaveData,(DWORD)0L,(DWORD)CALLBACK_FUNCTION))!=0) 
		//if(woo=waveOutOpen(&hWaveOut,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)(UINT)hwnd,(DWORD)0L,(DWORD)CALLBACK_WINDOW)!=0) 
			{
		   	char et[MAXERRORLENGTH];
		    if((woo==MMSYSERR_ALLOCATED||woo==MMSYSERR_NOTSUPPORTED) && InputActive)
				{
			    ++inputPacketsLost;
					return FALSE;
			  }
		    waveOutGetErrorText(woo,et,sizeof et);//Can't Open Wave Audio Output
				return FALSE;
		  }
		  OutputActive=TRUE;//이 함수를 한번만 실행하도록 TRUE를 해준다
		  aboutOutSamples=samplesPerSecond;
		  aboutOutBits=bitsPerSample;
		}
  }
  return TRUE;
}

//반이중 흟E탔适갋검사한다(웨이틒E입출력을 한다 실패하툈E반이중흟E탔甄�)
int HardWareChecking()
{
  PCMWAVEFORMAT wfi;
	MMRESULT woo;
	int hdx=FALSE;
	if(waAudio11025) 
	{
		samplesPerSecond=11025;
		bytesPerSecond=samplesPerSecond*2;
	}
	wfi.wf.wFormatTag=WAVE_FORMAT_PCM;//Sampling방식
	wfi.wf.nChannels=AudioChannel;//채널 1이툈EMONO,2이툈EStero
	while(TRUE) 
	{
    wfi.wf.nSamplesPerSec=samplesPerSecond;//Sample
    wfi.wf.nAvgBytesPerSec=bytesPerSecond;//for buffer estimation
    wfi.wf.nBlockAlign=sampleAlignment;//block size of data
    wfi.wBitsPerSample=bitsPerSample;//number of bits per sample of mono data
		//검사해준다
 		woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,0L,0L,WAVE_FORMAT_QUERY);
 		if(bitsPerSample>8 && woo==WAVERR_BADFORMAT) audioIs8Bit=TRUE;
 		if((audioUse8Bit || woo==WAVERR_BADFORMAT) && (bitsPerSample>8)) 
		{
 			bitsPerSample/=2;
 			sampleAlignment/=2;
 			bytesPerSecond/=2;	
	    wfi.wf.nAvgBytesPerSec=bytesPerSecond;
	    wfi.wf.nBlockAlign=sampleAlignment;
	    wfi.wBitsPerSample=bitsPerSample;
	 		woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,0L,0L,WAVE_FORMAT_QUERY);
 		}
		if(woo==WAVERR_BADFORMAT && samplesPerSecond==8000) 
		{
 			samplesPerSecond=11025;
 			bitsPerSample=16;
 			sampleAlignment=bitsPerSample/8;
 			bytesPerSecond=samplesPerSecond*sampleAlignment;  
 		} 
		else break;
 	} 
	if(woo!=0) 
	{
		return RECORD_FORMAT_ERROR;
    //OutputDebugString("IDS_T_WAVE_RECORD_FORMAT_ERR0");//The audio waveform device can't record in the required format.
		//printf("IDS_T_WAVE_RECORD_FORMAT_ERR0");
		//return -1;
	}
	//if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)InWaveData,(DWORD)0L,(DWORD)CALLBACK_WINDOW))!=MMSYSERR_NOERROR) 
	//if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)InWaveData,(DWORD)0L,(DWORD)CALLBACK_THREAD))!=MMSYSERR_NOERROR) 
  //if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)&InWaveData,(DWORD)0L,(DWORD)CALLBACK_FUNCTION))!=MMSYSERR_NOERROR) 
	if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)(UINT)hwnd,(DWORD)0L,(DWORD)CALLBACK_WINDOW))!=MMSYSERR_NOERROR) 
	{
   	char et[MAXERRORLENGTH];
   	waveInGetErrorText(woo,et,sizeof(et));
		//OutputDebugString("IDS_T_ERR_OPEN_WAVE_INPUT1");//Error Opening Wave Audio Input
		//printf("IDS_T_ERR_OPEN_WAVE_INPUT1");
		return WAVE_INPUT_OPEN_ERROR;
		//return -1;
  }
  aboutInBits=bitsPerSample;
  aboutInSamples=samplesPerSecond;
	if(waAudioHalf)//하프
	{
  	waveInClose(hWaveIn);
   	hdx=halfDuplex=TRUE;//반이중흟E�:무픸E繡逆�
  }
  while(TRUE) 
	{ //검사해준다
 		woo=waveOutOpen(&hWaveOut,WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,0L,0L,WAVE_FORMAT_QUERY);
 		if(bitsPerSample>8&&woo==WAVERR_BADFORMAT) audioIs8Bit=TRUE;
 		if((audioUse8Bit||woo==WAVERR_BADFORMAT)&&bitsPerSample>8) 
		{
 			bitsPerSample/=2;
 			sampleAlignment/=2;
 			bytesPerSecond/=2;	

	    wfi.wf.nAvgBytesPerSec=bytesPerSecond;
	    wfi.wf.nBlockAlign=sampleAlignment;
	    wfi.wBitsPerSample=bitsPerSample;
	 		woo=waveOutOpen(&hWaveOut,WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,0L,0L,WAVE_FORMAT_QUERY);
 		}
 		if(woo==WAVERR_BADFORMAT&&samplesPerSecond==8000) 
		{
 			samplesPerSecond=11025;
 			bitsPerSample=16;
 			sampleAlignment=bitsPerSample/8;
 			bytesPerSecond=samplesPerSecond*sampleAlignment;
 		} 
		else break;
 	} 
	if(woo!=0) 
	{				
  	char et[MAXERRORLENGTH];
   	waveOutGetErrorText(woo,et,sizeof et);
		//OutputDebugString("IDS_T_WAVE_PLAY_FORMAT_ERR2");//The waveform device can't play this format.
		//printf("IDS_T_WAVE_PLAY_FORMAT_ERR2");
		//hdx=-1;
		return WAVE_PLAY_FORMAT_ERROR;
	}
	if(woo=waveOutOpen(&hWaveOut,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)&OutWaveData,(DWORD)0L,(DWORD)CALLBACK_FUNCTION)!=0) 
	//if(woo=waveOutOpen(&hWaveOut,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)(UINT)hwnd,(DWORD)0L,(DWORD)CALLBACK_WINDOW)!=0) 
	{
   	char et[MAXERRORLENGTH];
		if(!waAudioHalf && (woo==MMSYSERR_ALLOCATED||woo==MMSYSERR_NOTSUPPORTED)) 
		{
			hdx=halfDuplex=TRUE;
			waveInClose(hWaveIn);
			if(waveOutOpen(&hWaveOut,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)&OutWaveData,(DWORD)0L,(DWORD)CALLBACK_FUNCTION)==0) 
			//if(waveOutOpen(&hWaveOut,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)(UINT)hwnd,(DWORD)0L,(DWORD)CALLBACK_WINDOW)!=0) 
			{
			  aboutOutBits=bitsPerSample;
			  aboutOutSamples=samplesPerSecond;
			  waveOutClose(hWaveOut);
			  goto HdxAudioExit;					  		
			}
	  } 
		else 
		{	
	    waveOutGetErrorText(woo,et,sizeof et);
	    //OutputDebugString("IDS_T_ERR_OPEN_WAVE_OUTPUT3");//Can't Open Wave Audio Output
			//hdx=-1;
			return WAVE_OUTPUT_OPEN_ERROR;
	  }
	} 
	else 
	{
	  aboutOutBits=bitsPerSample;
	  aboutOutSamples=samplesPerSecond;
		waveOutClose(hWaveOut);
	}
	if(!waAudioHalf) waveInClose(hWaveIn);
	HdxAudioExit:
  return hdx;
}					  


// 웨이틒E입력을 시작한다
int StartWaveInput()
{
  if(!InputActive)//입력파일이 열려있햨E않을때 한다
	{
		//OutputDebugString("\n음성 입력을 시작합니다!\n");
   	int i;
	  PCMWAVEFORMAT wfi;    
  	MMRESULT woo;
   	wfi.wf.wFormatTag=WAVE_FORMAT_PCM;//PCM포툈E
   	wfi.wf.nChannels=AudioChannel;//오디오 채널
   	while(TRUE) 
		{
	   	wfi.wf.nSamplesPerSec=samplesPerSecond;//초당셈플
	  	wfi.wf.nAvgBytesPerSec=bytesPerSecond;//초당바이트
	   	wfi.wf.nBlockAlign=sampleAlignment;
	   	wfi.wBitsPerSample=bitsPerSample;
			woo=waveInOpen(&hWaveIn,//입력장치를 검퍊E
				             (UINT)WAVE_MAPPER,
										 (LPWAVEFORMATEX)&wfi,
										 0L,
										 0L,
										 WAVE_FORMAT_QUERY);
	 		if(bitsPerSample>8&&woo==WAVERR_BADFORMAT) 
			{
	 			audioIs8Bit=TRUE;//8비트방식
	 		} 
	 		if((audioUse8Bit||woo==WAVERR_BADFORMAT)&&(bitsPerSample>8)) 
			{
	 			bitsPerSample/=2;
	 			sampleAlignment/=2;
	 			bytesPerSecond/=2;
		    wfi.wf.nAvgBytesPerSec=bytesPerSecond;
		    wfi.wf.nBlockAlign=sampleAlignment;
		    wfi.wBitsPerSample=bitsPerSample;
		 		woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,0L,0L,WAVE_FORMAT_QUERY);
	 		}
	 		if(woo==WAVERR_BADFORMAT&&samplesPerSecond==8000)
			{
	 			samplesPerSecond=11025;
				bitsPerSample=16;
	 			sampleAlignment=bitsPerSample/8;
	 			bytesPerSecond=samplesPerSecond*sampleAlignment;
	 		}
			else break;
	 	}
		if(woo!=0) 
		{
  		//OutputDebugString("IDS_T_WAVE_RECORD_FORMAT_ERR");//The audio waveform device can't record in the required format.
			return FALSE;
    }
    for(i=0;i<2;i++) 
		{//입력장치 OPen
			//if(waveOutOpen (&myhwo[i], i, &wfx, (DWORD) waveOutProc,(DWORD) this, CALLBACK_FUNCTION) != MMSYSERR_NOERROR)
      //rc=waveInOpen(&hwi,nDevId,&wfx,(DWORD)(VOID*)waveInProc,(DWORD)hWnd,CALLBACK_FUNCTION );
			//if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER, (LPWAVEFORMATEX)&wfi, (DWORD)(VOID*)InWaveData, (DWORD) this,(DWORD)CALLBACK_FUNCTION))!=MMSYSERR_NOERROR) 
			//if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)InWaveData,(DWORD)0L,(DWORD)CALLBACK_THREAD))!=MMSYSERR_NOERROR) 
		  if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)&InWaveData,(DWORD)0L,(DWORD)CALLBACK_FUNCTION))!=MMSYSERR_NOERROR)
			{
		   	char et[MAXERRORLENGTH];
			  if(i==0 && OutputActive && (woo==MMSYSERR_ALLOCATED || woo==MMSYSERR_NOTSUPPORTED)) 
				{
		    	waveOutReset(hWaveOut);
		    	halfDuplex=TRUE;
					if(outputPending)
					{
						halfDuplexTransition=TRUE;
	    			return TRUE;
					} 
					else 
					{
						waveOutClose(hWaveOut);
						OutputActive=FALSE;
					}
			    continue;   
			  }	
		    waveInGetErrorText(woo,et,sizeof et);
				//Error Opening Wave Audio Input
				
			  //if(woo==MMSYSERR_BADDEVICEID) printf("입력 오픈 에러! 초기화 퀋E� 에러!1");
				//if(woo==MMSYSERR_NODRIVER) printf("입력 오픈 에러! 초기화 퀋E� 에러!2");
	      //if(woo==MMSYSERR_NOMEM) printf("입력 오픈 에러! 초기화 퀋E� 에러!3");
				//if(woo==WAVERR_BADFORMAT) printf("입력 오픈 에러! 초기화 퀋E� 에러!4");
				//if(woo==MMSYSERR_ALLOCATED) printf("입력 오픈 에러! 초기화 퀋E� 에러!5");
				//if(woo==MMSYSERR_NOTSUPPORTED) printf("입력 오픈 에러! 초기화 퀋E� 에러!6");
				
				return FALSE;
		  }
		  break;
	  }
    currentInputLength=inputBufferLength();
    currentInputSamples=inputSampleCount();
    for(i=0;i<NumberWaveHeaders;i++) inWaveHeader[i]=NULL;//만일을 위해서 제거해준다
    for(i=0;i<NumberWaveHeaders;i++) 
		{
		  inWaveHeader[i]=(LPWAVEHDR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,sizeof(WAVEHDR));
		  if(inWaveHeader[i]==NULL)//메모리할당시 에러이툈E
			{
				int j;
				for(j=i-1;j>=0;j--) 
				{
					waveInUnprepareHeader(hWaveIn,inWaveHeader[j],sizeof(WAVEHDR));
					GlobalFreePtr(inWaveHeader[j]->lpData);
					GlobalFreePtr(inWaveHeader[j]);
					inWaveHeader[j]=NULL;
				}
				//Can't allocate wave input headers.
			  //OutputDebugString("IDS_T_INPUT_HEADER_ERR3");
   			waveInClose(hWaveIn);
				return FALSE;
			}
			inWaveHeader[i]->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)((samplesPerSecond==8000)?7200:10000));
			if(inWaveHeader[i]->lpData==NULL)//메모리할당시 에러이툈E
			{
				int j;
				GlobalFreePtr(inWaveHeader[i]);
				inWaveHeader[i]=0;
				for(j=i-1;j>=0;j++) 
				{
					waveInUnprepareHeader(hWaveIn,inWaveHeader[j],sizeof(WAVEHDR));
					GlobalFreePtr(inWaveHeader[j]->lpData);
					GlobalFreePtr(inWaveHeader[j]);
					inWaveHeader[j]=NULL;
				}
		  	//OutputDebugString("IDS_T_INPUT_BUFFER_ERR2");
			  waveInClose(hWaveIn);
				return FALSE;
			}
			//waveInPrepareHeader을 호출하기픸E� lPData와,dwBufferLength,dwFlags가 이미 설정되엉雹 하컖EdwFlags는 0이엉雹 한다
			inWaveHeader[i]->dwBufferLength=currentInputLength;
			inWaveHeader[i]->dwFlags=0L;
	    inWaveHeader[i]->dwLoops=0L;
			waveInPrepareHeader(hWaveIn,inWaveHeader[i],sizeof(WAVEHDR));
	  }
	  waveHeadersAllocated=NumberWaveHeaders; 
		for(i=0;i<NumberWaveHeaders;i++) waveInAddBuffer(hWaveIn,inWaveHeader[i],sizeof(WAVEHDR)); 
		InputReset=FALSE;
	  waveInStart(hWaveIn);//퀋E슬쳄�
	  InputActive=TRUE;//음성입력을 열었음
	  aboutInSamples=samplesPerSecond;
	  aboutInBits=bitsPerSample;
  }
  return TRUE;
}


int InitWaveInOpen(void)
{
  //printf("\n음성 입력을 시작합니다!\n");
  int i;
	PCMWAVEFORMAT wfi;    
  MMRESULT woo;
 	wfi.wf.wFormatTag=WAVE_FORMAT_PCM;//PCM포툈E
 	wfi.wf.nChannels=AudioChannel;//오디오 채널
 	while(TRUE) 
	{
	 	wfi.wf.nSamplesPerSec=samplesPerSecond;//초당셈플
	 	wfi.wf.nAvgBytesPerSec=bytesPerSecond;//초당바이트
	 	wfi.wf.nBlockAlign=sampleAlignment;
	 	wfi.wBitsPerSample=bitsPerSample;
		woo=waveInOpen(&hWaveIn,//입력장치를 검퍊E
			             (UINT)WAVE_MAPPER,
									 (LPWAVEFORMATEX)&wfi,
									 0L,
									 0L,
									 WAVE_FORMAT_QUERY);
		if(bitsPerSample>8&&woo==WAVERR_BADFORMAT) 
		{
			audioIs8Bit=TRUE;//8비트방식
		//	printf("오케발콅E1");//getchar();
		} 
		if((audioUse8Bit||woo==WAVERR_BADFORMAT)&&(bitsPerSample>8)) 
		{
		//	printf("오케발콅E!");//getchar();
			bitsPerSample/=2;
			sampleAlignment/=2;
			bytesPerSecond/=2;	
	    wfi.wf.nAvgBytesPerSec=bytesPerSecond;
	    wfi.wf.nBlockAlign=sampleAlignment;
	    wfi.wBitsPerSample=bitsPerSample;
	 		woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,0L,0L,WAVE_FORMAT_QUERY);
		}
		if(woo==WAVERR_BADFORMAT&&samplesPerSecond==8000) 
		{
//			printf("오케발콅E!");//getchar();
	 		samplesPerSecond=11025;
	 		bitsPerSample=16;
	 		sampleAlignment=bitsPerSample/8;
	 		bytesPerSecond=samplesPerSecond*sampleAlignment;  
	 	} 
		else break;
	} 
	if(woo!=0) 
	{
  	//OutputDebugString("IDS_T_WAVE_RECORD_FORMAT_ERR");//The audio waveform device can't record in the required format.
		return FALSE;
  }
  for(i=0;i<2;i++) 
	{//입력장치 OPen
		//if(waveOutOpen (&myhwo[i], i, &wfx, (DWORD) waveOutProc,(DWORD) this, CALLBACK_FUNCTION) != MMSYSERR_NOERROR)
    //rc=waveInOpen(&hwi,nDevId,&wfx,(DWORD)(VOID*)waveInProc,(DWORD)hWnd,CALLBACK_FUNCTION );
		//if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER, (LPWAVEFORMATEX)&wfi, (DWORD)(VOID*)InWaveData, (DWORD) this,(DWORD)CALLBACK_FUNCTION))!=MMSYSERR_NOERROR) 
		//if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)InWaveData,(DWORD)0L,(DWORD)CALLBACK_THREAD))!=MMSYSERR_NOERROR) 
	  //Rc=waveInOpen(&hWaveIn, WAVE_MAPPER, &PCMWaveFmtRecord,(DWORD)recordCallBack,0L,CALLBACK_FUNCTION);
		/*
		WAVEINCAPS    wic;
	 UINT          uiDevID;
	   UINT          nMaxDev = ::waveInGetNumDevs();
	  for(uiDevID = 0;uiDevID<nMaxDev; uiDevID++)
		waveInGetDevCaps(uiDevID, &wic, sizeof(wic));
		*/
		//if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)&InWaveData,(DWORD)0L,(DWORD)CALLBACK_FUNCTION))!=MMSYSERR_NOERROR)
		if((woo=waveInOpen(&hWaveIn,(UINT)WAVE_MAPPER,(LPWAVEFORMATEX)&wfi,(DWORD)(UINT)hwnd,(DWORD)0L,(DWORD)CALLBACK_WINDOW))!=MMSYSERR_NOERROR)
		{
	   	char et[MAXERRORLENGTH];
		  if(i==0 && OutputActive && (woo==MMSYSERR_ALLOCATED || woo==MMSYSERR_NOTSUPPORTED)) 
			{
	    	waveOutReset(hWaveOut);
	    	halfDuplex=TRUE;
				if(outputPending)
				{
					halfDuplexTransition=TRUE;
	   			return TRUE;
				}
				else 
				{
					waveOutClose(hWaveOut);
					OutputActive=FALSE;
				}
		    continue;   
		  }	
	    waveInGetErrorText(woo,et,sizeof et);
			//Error Opening Wave Audio Input
			/*
		  if(woo==MMSYSERR_BADDEVICEID)printf("입력 오픈 에러! 초기화 퀋E� 에러!1");
			if(woo==MMSYSERR_NODRIVER)printf("입력 오픈 에러! 초기화 퀋E� 에러!2");
	    if(woo==MMSYSERR_NOMEM)printf("입력 오픈 에러! 초기화 퀋E� 에러!3");
			if(woo==WAVERR_BADFORMAT)printf("입력 오픈 에러! 초기화 퀋E� 에러!4");
			if(woo==MMSYSERR_ALLOCATED)printf("입력 오픈 에러! 초기화 퀋E� 에러!5");
			if(woo==MMSYSERR_NOTSUPPORTED)printf("입력 오픈 에러! 초기화 퀋E� 에러!6");
			*/
			return FALSE;
	  }
	  break;
  }
	return TRUE;
}

//퀋E실� 메모리를 할큱E
int WaveInAlloc()
{
	int i;
	currentInputLength=inputBufferLength();
  currentInputSamples=inputSampleCount();
  for(i=0;i<NumberWaveHeaders;i++) inWaveHeader[i]=NULL;//만일을 위해서 제거해준다
  for(i=0;i<NumberWaveHeaders;i++) 
	{
	  inWaveHeader[i]=(LPWAVEHDR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,sizeof(WAVEHDR));
	  if(inWaveHeader[i]==NULL)//메모리할당시 에러이툈E
		{
			int j;
			for(j=i-1;j>=0;j--) 
			{
				waveInUnprepareHeader(hWaveIn,inWaveHeader[j],sizeof(WAVEHDR));
				GlobalFreePtr(inWaveHeader[j]->lpData);
				GlobalFreePtr(inWaveHeader[j]);
				inWaveHeader[j]=NULL;
			}
			//Can't allocate wave input headers.
			//OutputDebugString("IDS_T_INPUT_HEADER_ERR3");
 			waveInClose(hWaveIn);
			return FALSE;
		}
		inWaveHeader[i]->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)((samplesPerSecond==8000)?7200:10000));
		if(inWaveHeader[i]->lpData==NULL)//메모리할당시 에러이툈E
		{
			int j;
			GlobalFreePtr(inWaveHeader[i]);
			inWaveHeader[i]=0;
			for(j=i-1;j>=0;j++) 
			{
				waveInUnprepareHeader(hWaveIn,inWaveHeader[j],sizeof(WAVEHDR));
				GlobalFreePtr(inWaveHeader[j]->lpData);
				GlobalFreePtr(inWaveHeader[j]);
				inWaveHeader[j]=NULL;
			}
	  	//OutputDebugString("IDS_T_INPUT_BUFFER_ERR2");
		  waveInClose(hWaveIn);
			return FALSE;
		}
		//waveInPrepareHeader을 호출하기픸E� lPData와,dwBufferLength,dwFlags가 이미 설정되엉雹 하컖EdwFlags는 0이엉雹 한다
		inWaveHeader[i]->dwBufferLength=currentInputLength;
		inWaveHeader[i]->dwFlags=0;
		inWaveHeader[i]->dwLoops=0;
		waveInPrepareHeader(hWaveIn,inWaveHeader[i],sizeof(WAVEHDR));
  }
	return TRUE;
}

//퀋E습� 시작한다
int StartWavePlay()
{
  int i;
  waveHeadersAllocated=NumberWaveHeaders; 
	for(i=0;i<NumberWaveHeaders;i++) waveInAddBuffer(hWaveIn,inWaveHeader[i],sizeof(WAVEHDR)); 
	InputReset=FALSE;
  waveInStart(hWaveIn);//퀋E슬쳄�
	//printf("퀋E슬쳄�!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	/*
	MMSYSERR_INVALHANDLE Specified device handle is invalid. 
  MMSYSERR_NODRIVER No device driver is present. 
  MMSYSERR_NOMEM Unable to allocate or lock memory. 
  */
  InputActive=TRUE;//음성입력을 열었음
  aboutInSamples=samplesPerSecond;
  aboutInBits=bitsPerSample;
	return TRUE;
}
int output=0;
//퀋E슉� 음성을 스피커로 출력한다
void RecordWaveOutput(SoundBuffer *d,int bitsPerSample,int samplesPerSecond)
{
	int messageQueueSize=120;//Inbound message queue size
	short *sbuf;
	unsigned char *ulp;
	WORD stat;
	int i,len;
  char *val;
	
	//if(outputPending!=10) return;
	//printf("결과값:%d %d\n",outputPending,((3*messageQueueSize)/4));

//	if(outputPending>=((3*messageQueueSize)/4)) return;
	//printf("들엉黔입력값:%d %d\n",outputPending,((3*messageQueueSize)/4));
  
	WavePlay=(LPWAVEHDR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,sizeof(WAVEHDR)+sizeof(CHAR*));
	if(WavePlay==NULL) return;
  len=(int) d->buffer.buffer_len;
  val=d->buffer.buffer_val;

  //printf("압축방식:%d 보내는호스트:%s 사이햨E%d 길이:%d 값:%d\n",d->compression,d->sendinghost,sizeof(WavePlay),len,val);

  //CurrentCodecSize=d->buffer.buffer_len;//나중에 Play하콅E위해서..
  //CurrentCompression=d->compression;

  if(d->compression & fSetDest)
	if(!(d->compression & fDestJack))
	{//볼륨을 설정한다(상위 16Bit:퓖E編섭� 하위 16Bit:좌측볼륨) 0xffff:제일큰소리 0:작은소리
		waveOutSetVolume(hWaveOut,(DWORD)MAKELONG(0xFFFF,0xFFFF));
	}
  //if(d->compression & fCompGSM) 
  if(GsmCodecCompression)
	{
		//CurrentCodecSize=d->buffer.buffer_len;//나중에 Play하콅E위해서..
		//CurrentCompression=d->compression;
		//printf("GSM Codec 복원\n");
		GsmSoundDecoder((GSMBuffer*)d);//GSM코덱
    len=(int)d->buffer.buffer_len;
		//printf("해제후음성사이햨E%d\n",d->buffer.buffer_len);
		//printf("해제후압축방식:%d\n",d->compression);
		//printf("22222222222222222222222222\n");
  }
  if(AdpcmCodecCompression)
	//if(d->compression & fCompADPCM)//ADPCM
	{
		//CurrentCodecSize=d->buffer.buffer_len;
		//CurrentCompression=d->compression;
	  //printf("ADPCM Codec 복원\n");
    AdpcmSoundDecoder((ADPCMBuffer*)d);//ADPCM코덱
    len=(int)d->buffer.buffer_len;
		//printf("33333333333333333333333333\n");
  }
  if(samplesPerSecond==11025)//11025 Second일때
	{
	  if(bitsPerSample==16)//16Bit
		{	   
			WavePlay->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(((SOUNDBUFFER*((DWORD)sizeof(short))*12)/8))); 
			if(WavePlay->lpData==NULL) 
			{
				GlobalFreePtr(WavePlay);
				return;
			}
			sbuf=(short*)WavePlay->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) 
			{
				int j=i&7;
				*sbuf++=audio_u2s(*ulp);
				if(j>0&&!(j&1)) *sbuf++=audio_u2s(*ulp);
				else if (j%320==319) *sbuf++=audio_u2s(*ulp);
				ulp++; 
			}
			WavePlay->dwBufferLength=WavePlay->dwBytesRecorded=(((LPSTR)sbuf)-WavePlay->lpData);
		} 
		else if(bitsPerSample==8)//8Bit
		{
			BYTE *bbuf;
			WavePlay->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(((SOUNDBUFFER*((DWORD) sizeof(short))*12)/16))); 
			if(WavePlay->lpData==NULL) 
			{
				GlobalFreePtr(WavePlay);
				return;
			}
			bbuf=(BYTE*)WavePlay->lpData;
			ulp=(unsigned char*)val;
			for (i=0;i<len;i++) 
			{
				int j=i&7;
				*bbuf++=audio_u2c(*ulp);
				if(j>0&&!(j&1)) *bbuf++=audio_u2c(*ulp);
				else if (j%320==319) *bbuf++=audio_u2c(*ulp);
				ulp++; 
			}
			WavePlay->dwBufferLength=WavePlay->dwBytesRecorded=(((LPSTR)bbuf)-WavePlay->lpData);
		}
	} 
	else//8000 Second일때
	{
	  if(bitsPerSample==16)//16비트
		{ 
			WavePlay->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(len*sizeof(short))); 
			if(WavePlay->lpData==NULL) 
			{
				GlobalFreePtr(WavePlay);
				//GlobalFreePtr(WavePlay->lpData);내가해줬다 일부로
				//printf("메모리 에러 발생!\n");
				WavePlay=NULL;
				return;	
			}
			sbuf=(short*)WavePlay->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) *sbuf++=audio_u2s(*ulp++);
			WavePlay->dwBufferLength=WavePlay->dwBytesRecorded=(((LPSTR)sbuf)-WavePlay->lpData);
		} 
		else if(bitsPerSample==8)//8비트
		{
			BYTE *bbuf;
			WavePlay->lpData=(LPSTR) GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(len*sizeof(BYTE))); 
			if(WavePlay->lpData==NULL) 
			{
				GlobalFreePtr(WavePlay);
				WavePlay=NULL;
				return;
			}
			bbuf=(BYTE*)WavePlay->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) *bbuf++=audio_u2c(*ulp++);
			WavePlay->dwBufferLength=WavePlay->dwBytesRecorded=(((LPSTR)bbuf)-WavePlay->lpData);
		}
	}
	WavePlay->dwFlags=0;
	//WavePlay->dwLoops=0;
	waveOutPrepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));//재생을 위한 버퍼를 준틒E
	stat=waveOutWrite(hWaveOut,WavePlay,sizeof(WAVEHDR));//음성을 재생한다
	if(stat==0)
	{   /*
    	AnyPassDocument *pDoc=(AnyPassDocument*)((AnyPassFrameWnd*)AfxGetMainWnd())->GetActiveDocument();
		AnyPassDialogBottom *pChatView=pDoc->GetCurrentFormViewPointer();
		CString ok;
		ok.Format("입력값 %d",outputPending);
		pChatView->ChatStateMessage(ok);
		*/
	//	printf("입력값:%d",outputPending);
		//output=outputPending;//outputPending;
		outputPending++;
		//outputPending=1;
	}
	/*
	stat=waveOutWrite(hWaveOut,WavePlay,sizeof(WAVEHDR));//음성을 재생한다
	if(stat==0)
	{
		printf("입력:%d\n",outputPending);
	//	outputPending++;
 	}
	*/
	else//에러발생시
	{
	  char et[MAXERRORLENGTH];
	  waveOutGetErrorText(stat,et,sizeof(et));
	  waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	  GlobalFreePtr(WavePlay);
	  WavePlay=NULL;
	  return;
	}
}

//퀋E슉� 음성을 스피커로 출력한다
void TempWaveOutput(SoundBuffer *d,int bitsPerSample,int samplesPerSecond)
{
	int messageQueueSize=120;//Inbound message queue size
	short *sbuf;
	unsigned char *ulp;
	WORD stat;
	int i,len;
  char *val;
	
	//if(outputPending!=10) return;
	//printf("결과값:%d %d\n",outputPending,((3*messageQueueSize)/4));

//	if(outputPending>=((3*messageQueueSize)/4)) return;
	//printf("들엉黔입력값:%d %d\n",outputPending,((3*messageQueueSize)/4));
  
	WavePlayTemp=(LPWAVEHDR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,sizeof(WAVEHDR)+sizeof(CHAR*));
	if(WavePlayTemp==NULL) return;
  len=(int) d->buffer.buffer_len;
  val=d->buffer.buffer_val;

  //printf("압축방식:%d 보내는호스트:%s 사이햨E%d 길이:%d 값:%d\n",d->compression,d->sendinghost,sizeof(WavePlay),len,val);

  //CurrentCodecSize=d->buffer.buffer_len;//나중에 Play하콅E위해서..
  //CurrentCompression=d->compression;

  if(d->compression & fSetDest)
	if(!(d->compression & fDestJack))
	{//볼륨을 설정한다(상위 16Bit:퓖E編섭� 하위 16Bit:좌측볼륨) 0xffff:제일큰소리 0:작은소리
		waveOutSetVolume(hWaveOut,(DWORD)MAKELONG(0xFFFF,0xFFFF));
	}
  //if(d->compression & fCompGSM) 
  if(GsmCodecCompression)
	{
		//CurrentCodecSize=d->buffer.buffer_len;//나중에 Play하콅E위해서..
		//CurrentCompression=d->compression;
		//printf("GSM Codec 복원\n");
		GsmSoundDecoder((GSMBuffer*)d);//GSM코덱
    len=(int)d->buffer.buffer_len;
		//printf("해제후음성사이햨E%d\n",d->buffer.buffer_len);
		//printf("해제후압축방식:%d\n",d->compression);
		//printf("22222222222222222222222222\n");
  }
  if(AdpcmCodecCompression)
	//if(d->compression & fCompADPCM)//ADPCM
	{
		//CurrentCodecSize=d->buffer.buffer_len;
		//CurrentCompression=d->compression;
	  //printf("ADPCM Codec 복원\n");
    AdpcmSoundDecoder((ADPCMBuffer*)d);//ADPCM코덱
    len=(int)d->buffer.buffer_len;
		//printf("33333333333333333333333333\n");
  }
  if(samplesPerSecond==11025)//11025 Second일때
	{
	  if(bitsPerSample==16)//16Bit
		{	   
			WavePlayTemp->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(((SOUNDBUFFER*((DWORD)sizeof(short))*12)/8))); 
			if(WavePlayTemp->lpData==NULL) 
			{
				GlobalFreePtr(WavePlayTemp);
				return;
			}
			sbuf=(short*)WavePlayTemp->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) 
			{
				int j=i&7;
				*sbuf++=audio_u2s(*ulp);
				if(j>0&&!(j&1)) *sbuf++=audio_u2s(*ulp);
				else if (j%320==319) *sbuf++=audio_u2s(*ulp);
				ulp++; 
			}
			WavePlayTemp->dwBufferLength=WavePlayTemp->dwBytesRecorded=(((LPSTR)sbuf)-WavePlayTemp->lpData);
		} 
		else if(bitsPerSample==8)//8Bit
		{
			BYTE *bbuf;
			WavePlayTemp->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(((SOUNDBUFFER*((DWORD) sizeof(short))*12)/16))); 
			if(WavePlayTemp->lpData==NULL) 
			{
				GlobalFreePtr(WavePlayTemp);
				return;
			}
			bbuf=(BYTE*)WavePlayTemp->lpData;
			ulp=(unsigned char*)val;
			for (i=0;i<len;i++) 
			{
				int j=i&7;
				*bbuf++=audio_u2c(*ulp);
				if(j>0&&!(j&1)) *bbuf++=audio_u2c(*ulp);
				else if (j%320==319) *bbuf++=audio_u2c(*ulp);
				ulp++; 
			}
			WavePlayTemp->dwBufferLength=WavePlayTemp->dwBytesRecorded=(((LPSTR)bbuf)-WavePlayTemp->lpData);
		}
	} 
	else//8000 Second일때
	{
	  if(bitsPerSample==16)//16비트
		{ 
			WavePlayTemp->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(len*sizeof(short))); 
			if(WavePlayTemp->lpData==NULL) 
			{
				GlobalFreePtr(WavePlayTemp);
				//GlobalFreePtr(WavePlayTemp->lpData);내가해줬다 일부로
				//printf("메모리 에러 발생!\n");
				WavePlayTemp=NULL;
				return;	
			}
			sbuf=(short*)WavePlayTemp->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) *sbuf++=audio_u2s(*ulp++);
			WavePlayTemp->dwBufferLength=WavePlayTemp->dwBytesRecorded=(((LPSTR)sbuf)-WavePlayTemp->lpData);
		} 
		else if(bitsPerSample==8)//8비트
		{
			BYTE *bbuf;
			WavePlayTemp->lpData=(LPSTR) GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(len*sizeof(BYTE))); 
			if(WavePlayTemp->lpData==NULL) 
			{
				GlobalFreePtr(WavePlayTemp);
				WavePlayTemp=NULL;
				return;
			}
			bbuf=(BYTE*)WavePlayTemp->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) *bbuf++=audio_u2c(*ulp++);
			WavePlayTemp->dwBufferLength=WavePlayTemp->dwBytesRecorded=(((LPSTR)bbuf)-WavePlayTemp->lpData);
		}
	}
	WavePlayTemp->dwFlags=0;
	//WavePlay->dwLoops=0;
	waveOutPrepareHeader(hWaveOut,WavePlayTemp,sizeof(WAVEHDR));//재생을 위한 버퍼를 준틒E
	stat=waveOutWrite(hWaveOut,WavePlayTemp,sizeof(WAVEHDR));//음성을 재생한다
	if(stat==0)
	{   /*
    	AnyPassDocument *pDoc=(AnyPassDocument*)((AnyPassFrameWnd*)AfxGetMainWnd())->GetActiveDocument();
		AnyPassDialogBottom *pChatView=pDoc->GetCurrentFormViewPointer();
		CString ok;
		ok.Format("입력값 %d",outputPending);
		pChatView->ChatStateMessage(ok);
		*/
	//	printf("입력값:%d",outputPending);
		//output=outputPending;//outputPending;
		outputPending++;
		//outputPending=1;
	}
	/*
	stat=waveOutWrite(hWaveOut,WavePlay,sizeof(WAVEHDR));//음성을 재생한다
	if(stat==0)
	{
		printf("입력:%d\n",outputPending);
	//	outputPending++;
 	}
	*/
	else//에러발생시
	{
	  char et[MAXERRORLENGTH];
	  waveOutGetErrorText(stat,et,sizeof(et));
	  waveOutUnprepareHeader(hWaveOut,WavePlayTemp,sizeof(WAVEHDR));
	  GlobalFreePtr(WavePlayTemp);
	  WavePlayTemp=NULL;
	  return;
	}
}


//음성을 스피커로 출력한다
void StartWaveOutput(SoundBuffer *d,int bitsPerSample,int samplesPerSecond)
{
	int messageQueueSize=120;//Inbound message queue size
	short *sbuf;
	unsigned char *ulp;
	
	// skpark modify WORD --> UNIT
	//WORD stat;
	UINT	stat=0;

	int i,len;
  char *val;
	//printf("입력값:%d %d\n",outputPending,((3*messageQueueSize)/4));
	//static unsigned char auxbuf[SOUNDBUFFER+2];
  if(outputPending>=((3*messageQueueSize)/4)) return;
	//printf("들엉黔입력값:%d %d\n",outputPending,((3*messageQueueSize)/4));
	WavePlay=(LPWAVEHDR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,sizeof(WAVEHDR)+sizeof(CHAR*));
	if(WavePlay==NULL) return;
  len=(int) d->buffer.buffer_len;
	
  val=d->buffer.buffer_val;
  if(d->compression & fSetDest)
	if(!(d->compression & fDestJack))
	{//볼륨을 설정한다(상위 16Bit:퓖E編섭� 하위 16Bit:좌측볼륨) 0xffff:제일큰소리 0:작은소리
		waveOutSetVolume(hWaveOut,(DWORD)MAKELONG(0xFFFF,0xFFFF));
	}
  if(d->compression & fCompGSM) 
	{
		//printf("GSM Codec 복원\n");
		GsmSoundDecoder((GSMBuffer*)d);//GSM코덱
    len=(int)d->buffer.buffer_len;
  }
	if(d->compression & fCompADPCM)//ADPCM
	{
		//printf("ADPCM Codec 복원\n");
    AdpcmSoundDecoder((ADPCMBuffer*)d);//ADPCM코덱
    len=(int)d->buffer.buffer_len;
  }
  if(samplesPerSecond==11025)//11025 Second일때
	{
	  if(bitsPerSample==16)//16Bit
		{	   
			WavePlay->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(((SOUNDBUFFER*((DWORD)sizeof(short))*12)/8))); 
			if(WavePlay->lpData==NULL) 
			{
				GlobalFreePtr(WavePlay);
				return;
			}
			sbuf=(short*)WavePlay->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) 
			{
				int j=i&7;
				*sbuf++=audio_u2s(*ulp);
				if(j>0&&!(j&1)) *sbuf++=audio_u2s(*ulp);
				else if (j%320==319) *sbuf++=audio_u2s(*ulp);
				ulp++; 
			}
			WavePlay->dwBufferLength=WavePlay->dwBytesRecorded=(((LPSTR)sbuf)-WavePlay->lpData);
		} 
		else if(bitsPerSample==8)//8Bit
		{
			BYTE *bbuf;
			WavePlay->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(((SOUNDBUFFER*((DWORD) sizeof(short))*12)/16))); 
			if(WavePlay->lpData==NULL) 
			{
				GlobalFreePtr(WavePlay);
				return;
			}
			bbuf=(BYTE*)WavePlay->lpData;
			ulp=(unsigned char*)val;
			for (i=0;i<len;i++) 
			{
				int j=i&7;
				*bbuf++=audio_u2c(*ulp);
				if(j>0&&!(j&1)) *bbuf++=audio_u2c(*ulp);
				else if (j%320==319) *bbuf++=audio_u2c(*ulp);
				ulp++; 
			}
			WavePlay->dwBufferLength=WavePlay->dwBytesRecorded=(((LPSTR)bbuf)-WavePlay->lpData);
		}
	} 
	else//8000 Second일때
	{
	  if(bitsPerSample==16)//16비트
		{	   
			WavePlay->lpData=(LPSTR)GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(len*sizeof(short))); 
			if(WavePlay->lpData==NULL) 
			{
				GlobalFreePtr(WavePlay);
				//GlobalFreePtr(WavePlay->lpData);내가해줬다 일부로
				return;
			}
			sbuf=(short*)WavePlay->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) *sbuf++=audio_u2s(*ulp++);
			WavePlay->dwBufferLength=WavePlay->dwBytesRecorded=(((LPSTR)sbuf)-WavePlay->lpData);
		} 
		else if(bitsPerSample==8)//8비트
		{
			BYTE *bbuf;
			WavePlay->lpData=(LPSTR) GlobalAllocPtr(GMEM_MOVEABLE|GMEM_SHARE,(DWORD)(len*sizeof(BYTE))); 
			if(WavePlay->lpData==NULL) 
			{
				GlobalFreePtr(WavePlay);
				return;
			}
			bbuf=(BYTE*)WavePlay->lpData;
			ulp=(unsigned char*)val;
			for(i=0;i<len;i++) *bbuf++=audio_u2c(*ulp++);
			WavePlay->dwBufferLength=WavePlay->dwBytesRecorded=(((LPSTR)bbuf)-WavePlay->lpData);
		}
	}
	WavePlay->dwFlags=0;
	//WavePlay->dwLoops=0;
	waveOutPrepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));//재생을 위한 버퍼를 준틒E
	/*
	if(jitterPause&&(outputPending>=(messageQueueSize/2))) 
	{
		jitterPause=FALSE;
		waveOutRestart(hWaveOut);
	}
	*/
	if(outputPending<=2)
	{
	  stat=waveOutWrite(hWaveOut,WavePlay,sizeof(WAVEHDR));//음성을 재생한다
	}
	if(stat==0)
	{
//	    printf("입력값 %d\n",outputPending);
		outputPending++;
	}
	else 
	{	
		//printf("메모리해제");
	  char et[MAXERRORLENGTH];
	  waveOutGetErrorText(stat,et,sizeof(et));
	  waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	  GlobalFreePtr(WavePlay);
		//Can't write to sound device: %s.
	  return;
	}
}

static struct
{
	char header[4];
	unsigned short len,ilen;
	SoundBuffer sbm;
}mb={{1,2,3,4}};
#define sb mb.sbm

long PacketLength;				      		//송신할 패킷의 길이(InWaveData()함펯ESoundBufferCreate()함수에사퓖E
int spurt=TRUE;			  							//Start of talk spurt flag
unsigned short seq;			  				  //RTP packet sequence number
unsigned long ssrc;							  	//RTP synchronisation source identifier
unsigned long timestamp; 						//RTP packet timestamp


//사웝�갋버퍼를 생성한다
void SoundBufferCreate(LPSTR buffer,WORD buflen,DWORD channels,DWORD rate,DWORD bytesec,WORD align)
{
	int knownFormat=FALSE;
	if(rate==8000)
	{
//		printf("11111111111111111111111111111111");
		if(align==2)
		{
			long i;
			int j;
			for(i=j=0;i<(LONG)buflen/align;i++){sb.buffer.buffer_val[j++]=audio_s2u((((WORD FAR *)buffer)[i]));}
		}
		else if(align==1)
		{
			LONG i;
			int j;
			for(i=j=0;i<(LONG)buflen;i++){sb.buffer.buffer_val[j++]=audio_c2u((((BYTE FAR *)buffer)[i]));}
		} 
		else align=1;
		sb.buffer.buffer_len=buflen/align;
		knownFormat=TRUE;
	} 
	else if(rate==11025 && align==2) 
	{
//		printf("222222222222222222222222222222222");
		long i;
		int j,k;
		for(i=j=k=0;i<(LONG)(buflen/align);i++) 
		{
			if((k&3)!=2&&((i%580)!=579)){sb.buffer.buffer_val[j++]=audio_s2u((((WORD FAR *)buffer)[i]));}
			k=(k+1)%11;
		} 
		sb.buffer.buffer_len=j;
		knownFormat=TRUE;
	}
	else if(rate==11025 && align==1) 
	{
//		printf("3333333333333333333333333333333333");
		long i;
		int j,k;
		for(i=j=k=0;i<(LONG)(buflen/align);i++) 
		{
			if((k&3)!=2&&((i%580)!=579)){sb.buffer.buffer_val[j++]=audio_c2u((((BYTE FAR *)buffer)[i]));}
			k=(k+1)%11;
		} 
		sb.buffer.buffer_len=j;
		knownFormat=TRUE;
	}
	sqpacket=FALSE;//Vox를 사퓖E훌杉摸갋FALSE 사퓖E杉摸갋TRUE
	if(knownFormat)//여기서 vox를 사퓖E歐갋위함
	{
		long i;
		long alevel=0;
		static long voxSampleCountdown=4000;//가장틒E０� 설정 4000,8000,12000
		int j,thresh;
		//char sNum[16];
		//double v;
		//DTX ( Discontinuous Transmission )
	  //흟E� 중 50%의 duty cycle로 말하는 것에 착안하여 말을 하햨E않는 시간 동안 픸E邦� 중지시켜 air traffic을 줄일 펯E있다.
		//air traffic을 줄임으로퐗E인접채널과의 간섭을 줄일 펯E있으툈E또한 mobile의 battery수뫄祁 늘릴 펯E있다.
		//comfort noise : SID frame을 수신할 때 수신하컖E있는 speech decoder가 background noise를 생성함으로퐗Ewire-line connection된 것처럼 보임. 
		thresh=(int)exp(log(32767.0)*((1000-NoiseProcess)/1000.0));// 0~1000깩�갋음성 커트라인)
		for(i=0,j=0;i<sb.buffer.buffer_len;i++,j++)
		{
			int samp=audio_u2s(sb.buffer.buffer_val[j]);
			if(samp<0) samp=-samp;
			alevel+=samp;
		}
		alevel/=sb.buffer.buffer_len;//사웝珞버퍼의 길이를 구해서 vox의 레벨에 넣는다
	  //예제->char s[132]; 
		//v=100.0-((double)NoiseProcess/10.0);
		//sprintf(sNum,"[%2.2lf]",v);//%단위로 보여준다 0~100깩�갋음성이 들엉邦기를
		//printf("[%s]\n",sNum);
		//printf("Nt=%d,Thresh=%d,Max=%d,VU=%.2g\r\n",NoiseProcess, thresh,alevel,log((double)alevel)/log(32767.0));//OutputDebugString(s);
		//voxMonitorUpdate(alevel,0);//VoxMenuShow
		if(alevel<thresh) 
		{
			if(voxSampleCountdown<=0) 
			{
				sqpacket=TRUE;//TRUE를 줌으로퐗E커트라인을 패킷보낼때...조절할펯E있도록한다
				sb.buffer.buffer_len=0;
				return;
			}
			voxSampleCountdown-=sb.buffer.buffer_len;
		}
		else voxSampleCountdown=4000;//틒E０�

		if(AdpcmCodecCompression) 
		{
			//printf("픸E슨본瑛訣갋%d\n",sb.buffer.buffer_len);
		  //printf("픸E勤仙逆�:%d\n",sb.compression);
			//printf("ADPCM Codec압축\n");
			AdpcmSoundCoder((ADPCMBuffer*)&sb);//Adpcom 압횁E
			//VoiceFileWrite((CHAR FAR*)sb.buffer.buffer_val,sizeof(sb.buffer.buffer_len));//음성을 파일에 저장한다
			/*
			BYTE* m_pVoiceBuff;
			m_pVoiceBuff=new BYTE[20];
			memset(m_pVoiceBuff,0,20);

      int ok=Encode((unsigned char*)&sb,sb.buffer.buffer_len,(unsigned char*)m_pVoiceBuff,20);
			if(ok==TRUE) printf("G723성공\n");
			else printf("G723실패\n");

			VoiceFileWrite((char*)m_pVoiceBuff,20);//음성을 파일에 저장한다
			*/
  		//if(G_Recording==TRUE) 
			//VoiceFileWrite((CHAR FAR*)sb.buffer.buffer_val,sb.buffer.buffer_len);//음성을 파일에 저장한다

 	 	  CurrentCodecSize=sb.buffer.buffer_len;
		  CurrentCompression=sb.compression;

			//printf("음성사이햨E%d\n",sb.buffer.buffer_len);
		  //printf("압축방식:%d\n",sb.compression);
		}
		if(GsmCodecCompression)
		{
			
//			printf("픸E슨본瑛訣갋%d\n",sb.buffer.buffer_len);
//		  printf("픸E勤仙逆�:%d\n",sb.compression);
			//printf("GSM Codec압축\n");
			GsmSoundCoder((GSMBuffer*)&sb);//Gsm 압횁E
			
			//PBYTE PDST;
			//int Length=0;
		  //Encode((unsigned char*)&sb,sb.buffer.buffer_len,(unsigned char*)&sb,sb.buffer.buffer_len);
			//Encode((unsigned char*)&sb,sb.buffer.buffer_len,PDST,Length);
			
      /*
			int ok=Encode((unsigned char*)&sb.buffer.buffer_val,sb.buffer.buffer_len,szBuff,sb.buffer.buffer_len);
			if(ok==TRUE) printf("G723성공\n");
			else printf("G723실패\n");
      */
			//if(G_Recording==TRUE) 
			//VoiceFileWrite((CHAR FAR*)sb.buffer.buffer_val,sb.buffer.buffer_len);//음성을 파일에 저장한다
			//VoiceFileWrite((char*)&szBuff,sb.buffer.buffer_len);//음성을 파일에 저장한다
 	 	  CurrentCodecSize=sb.buffer.buffer_len;
		  CurrentCompression=sb.compression;

//			printf("음성사이햨E%d\n",sb.buffer.buffer_len);
//		  printf("압축방식:%d\n",sb.compression);
				
			//VoiceFileWrite((CHAR FAR*)sb.buffer.buffer_val,sizeof(sb.buffer.buffer_len));//음성을 파일에 저장한다
		}
//		printf("패킷길이:%d\n",PacketLength);
	  PacketLength=sb.buffer.buffer_len+(sizeof(struct SOUND_BUFFER)-SOUNDBUFFER);//사웝�갋버퍼크콅E
		sb.compression=fProtocol;//압축방식 RTP/VAT필퓖E
		sb.compression|=AdpcmCodecCompression?fCompADPCM:0;//adpcmCodec
		sb.compression|=GsmCodecCompression?fCompGSM:0;//GsmCodec
		PacketLength=RTPOutput(&sb,ssrc,timestamp,seq,spurt);
		seq++;
		timestamp+=currentInputSamples;
		spurt=FALSE;//말을 하햨E못하도록 한다
	}
}

//현픸E압횁E방식컖E입력 버퍼를 위해 가픸E좋은 sample 카웝獸를 계퍊E磯�		   
int inputSampleCount(void){int l=(AdpcmCodecCompression|GsmCodecCompression)?(160*4):480;return l;}						  													 

/*현픸E압횁E방식컖E오디오 하드웨엉洑 특성에 기초를 둔 오디오 입력 버퍼를 위해 바이트의 가픸E좋은 길이를 계퍊E彎갋*/
int inputBufferLength(void){return(bitsPerSample/8)*inputSampleCount();}					

// Stop record
void WaveInputStop(void)
{
	int i;
	/*
	for(i=0;i<NumberWaveHeaders;i++)
	{
		inWaveHeader[i]=NULL;
	  //inWaveHeader[i]->lpData=NULL;
	}*/
	if(InputActive)
	{
		/*
		for(i=0;i<NumberWaveHeaders;i++) 
		{
	 	  if(inWaveHeader[i]==NULL)//메모리할당시 에러이툈E
			{
				//printf("뫈礫실행했남\n");
				int j;
				for(j=i-1;j>=0;j--) 
				{
					waveInUnprepareHeader(hWaveIn,inWaveHeader[j],sizeof(WAVEHDR));
					GlobalFreePtr(inWaveHeader[j]->lpData);
					GlobalFreePtr(inWaveHeader[j]);
					inWaveHeader[j]=NULL;
				}
 				waveInClose(hWaveIn);
				return FALSE;
			}
			if(inWaveHeader[i]->lpData==NULL)//메모리할당시 에러이툈E
			{
				int j;
				GlobalFreePtr(inWaveHeader[i]);
				inWaveHeader[i]=0;
				for(j=i-1;j>=0;j++) 
				{
					waveInUnprepareHeader(hWaveIn,inWaveHeader[j],sizeof(WAVEHDR));
					GlobalFreePtr(inWaveHeader[j]->lpData);
					GlobalFreePtr(inWaveHeader[j]);
					inWaveHeader[j]=NULL;
				}
				waveInClose(hWaveIn);
				return FALSE;
			}
		}
		*/
		//printf("닫았다!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  	    InputReset=TRUE;
		//waveInStop(hWaveIn);
		waveInReset(hWaveIn);
		for(i=0;i<NumberWaveHeaders;i++) inWaveHeader[i]=NULL;
		//printf("닫았다!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		//printf("waveInReset실행되부렀냐!!!!!!!!!!!!!!!!!!!!!!!!");
		/*
		for(i=0;i<NumberWaveHeaders;i++)
		{
			inWaveHeader[i]=NULL;
		  inWaveHeader[i]->lpData=NULL;
		}*/
	}
}							 

//접속 중인 모탛E사퓖E悶“� 음성을 픸E徘磯�
bool VoicePlayAll(void)
{
	VoiceAllSending=TRUE;//여러뫄發게 보낸다
	static int OneOnlyStart=TRUE;//한번만 실행하도록
    if(!InputActive)//!inputActive는 또 VoicePlayAll이 두번실행되툈E에러 발생되햨E않도록 해준다
	{
		InputStop=FALSE;//퀋E습� 가능하게 한다
		if(OneOnlyStart==TRUE)//한번실행되도 상컖E愎� 부분이다
		{
			InitWaveInOpen();//버그를 방햨E하콅E위해서는 waveInopen컖E할큱E퀋E� 시작따甁 한번만 해줘야 한다 저번에 니가 한뫄�갋하나씩해서 버그가 발생한거다
			WaveInAlloc();//음성입력버퍼할큱E
		  StartWavePlay();
	 		spurt=TRUE;//Mark start of talk spurt
		//	LoopFlush(List);//Flush any unplayed local loop packets
			OneOnlyStart=FALSE;
			//ThreadOn=TRUE;
		}
		VoiceInfo.VoiceInputState=TRUE;//더빚細릭이툈E2를 넣컖E그냥 클릭이툈ETRUE를 넣는다
		VoiceInfo.ClientState=SendingLiveAudio;//보내는중
		OneOnlyStart=TRUE;//다시 함수를 사퓖E玖갋사퓖E寗痔令돈逑�
		return TRUE;
	}
	else
	{
		OneOnlyStart=TRUE;//다시 함수를 사퓖E玖갋사퓖E寗痔令돈逑�
		return FALSE;
	}
}


//접속된 모탛E사퓖E悶“� 음성 픸E邦� 중단한다(패킷픸E滂� 멈춘다)
bool VoiceStopAll(void)
{
    WaveInputStop();//Wave의 입력을 중지시킨다
    InputStop=TRUE;//입력을 멈춘다
	return TRUE;
}

//현픸E주소에 해당되는 컴퓨터에 음성을 퀋E실� 픸E邦� 시작한다 곳픸E패킷으로 픸E� 가능
bool VoicePlay(char *Address)
{
	VoiceAllSending=FALSE;//한뫄發게만 보내도록한다
	InputStop=FALSE;
	//StartWavePlay();
	InitWaveInOpen();
    WaveInAlloc();//음성입력버퍼할큱E
	StartWavePlay();
	spurt=TRUE;//Mark start of talk spurt
	VoiceInfo.VoiceInputState=TRUE;//더빚細릭이툈E2를 넣컖E그냥 클릭이툈ETRUE를 넣는다
	VoiceInfo.ClientState=SendingLiveAudio;//보내는중
	return TRUE;
}

//음성픸E邦� 멈춘다(패킷픸E滂� 멈춘다)
bool VoiceStop(void)
{
	if(VoiceInfo.VoiceInputState==TRUE)//그냥 한번만 클릭했다툈E
	{
	 	VoiceInfo.VoiceInputState=FALSE;//다시 클릭하햨E않은 상태로 바꾼다 
	 	WaveInputStop();//Wave의 입력을 중지시킨다
		InputStop=TRUE;//입력을 멈춘다
		ClickOnceState=TRUE;//다시 다른 놈컖E흟E탔� 할 펯E있도록 해준다
		return TRUE;
	}
	else return FALSE;
}

int ook=0;
int svoice=0;
/*
//음성파일 읽콅E쓰레탛E
unsigned long __stdcall VoiceThread(void *Arg)
{
	SoundBuffer *d=&receivedSoundBuffer;// receivedSoundBuffer에 음성을 받아서 가리킨다
	InitVoiceDevice();//출력장치 초기화
	VoiceRecordSave=TRUE;//플래를 위해서..true
	ThreadOn=TRUE;//플래이를 위한 돌려준다
	static int ok=0;
	if(GsmCodecCompression)
	{
		CurrentCodecSize=134;//Gsm사이햨E
		CurrentCompression=167773056;//Gsm...
	}
	if(AdpcmCodecCompression)
	{
		CurrentCodecSize=323;//Adpcm사이햨E
		CurrentCompression=989857152;//Adpcm..
	}

	do
	{
		if(svoice==1 && outputPending==output)
		{
		  receivedSoundBuffer.buffer.buffer_len=CurrentCodecSize;
  		  receivedSoundBuffer.compression=CurrentCompression;
	  	  VoiceFileRead((CHAR FAR*)d->buffer.buffer_val,d->buffer.buffer_len);//음성을 파일에 저장한다
		//  InitVoiceDevice();
  		  RecordWaveOutput(d,bitsPerSample,samplesPerSecond);//스피커로 소리가 난다(자신의 컴퓨터로 음성을 출력한다)
		}
	}while(!eof(ReadHandle));//파일이 끝인지검퍊E한다
	return 0;
}*/


/*
void TempStartvoice()
{
	if(!eof(ReadHandle))
	{
		SoundBuffer *d=&receivedSoundBuffer;// receivedSoundBuffer에 음성을 받아서 가리킨다
	//	InitVoiceDevice();//출력장치 초기화
		VoiceRecordSave=TRUE;//플래를 위해서..true
		ThreadOn=TRUE;//플래이를 위한 돌려준다
		if(GsmCodecCompression)
		{
			CurrentCodecSize=134;//Gsm사이햨E
			CurrentCompression=167773056;//Gsm...
		}
		if(AdpcmCodecCompression)
		{
			CurrentCodecSize=323;//Adpcm사이햨E
			CurrentCompression=989857152;//Adpcm..
		}
		receivedSoundBuffer.buffer.buffer_len=CurrentCodecSize;
		receivedSoundBuffer.compression=CurrentCompression;
  		VoiceFileRead((CHAR FAR*)d->buffer.buffer_val,d->buffer.buffer_len);//음성을 파일에 저장한다
		InitVoiceDevice();
		TempWaveOutput(d,bitsPerSample,samplesPerSecond);//스피커로 소리가 난다(자신의 컴퓨터로 음성을 출력한다)
	}
}*/

/*
void Startvoice()
{
	if(!eof(ReadHandle))
	{
		SoundBuffer *d=&receivedSoundBuffer;// receivedSoundBuffer에 음성을 받아서 가리킨다
	//	InitVoiceDevice();//출력장치 초기화
		VoiceRecordSave=TRUE;//플래를 위해서..true
		ThreadOn=TRUE;//플래이를 위한 돌려준다
		if(GsmCodecCompression)
		{
			CurrentCodecSize=134;//Gsm사이햨E
			CurrentCompression=167773056;//Gsm...
		}
		if(AdpcmCodecCompression)
		{
			CurrentCodecSize=323;//Adpcm사이햨E
			CurrentCompression=989857152;//Adpcm..
		}
		receivedSoundBuffer.buffer.buffer_len=CurrentCodecSize;
		receivedSoundBuffer.compression=CurrentCompression;
  		VoiceFileRead((CHAR FAR*)d->buffer.buffer_val,d->buffer.buffer_len);//음성을 파일에 저장한다
		InitVoiceDevice();
		RecordWaveOutput(d,bitsPerSample,samplesPerSecond);//스피커로 소리가 난다(자신의 컴퓨터로 음성을 출력한다)
	}
}*/

long voicepointer=0L;
int good=0;

//음성파일 플래이를 스톱한다
void VoiceFilePlayStop(void)
{
	//outputPending=0;
	waveOutPause(hWaveOut);
	svoice=0;
//	SendMessage(hwnd,MM_WOM_DONE,0L,0L);
//	SetTimer(hwnd,100,10,NULL);
/*
	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
	Sleep(100);
	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
	Sleep(100);
    waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
	Sleep(100);
*/	
//	printf("음냐리");
	
	//KillTimer(hwnd,1000);
   // voicepointer=VoiceFileGetReadPointer();
	/*
	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
	Sleep(100);
//	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
	Sleep(100);
//	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
	Sleep(100);
//	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
	Sleep(100);
//	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
	*/
	
	/*
	if(WavePlay!=NULL)
	{
		printf("해제");
    waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
    }
	if(WavePlay!=NULL)
	{
		printf("해제");
    waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
    }
	if(WavePlay!=NULL)
	{
		printf("해제");
    waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	GlobalFreePtr(WavePlay);
    }
	*/
//	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
//	GlobalFreePtr(WavePlay->lpData);
//	GlobalFreePtr(WavePlay);
//	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
//	GlobalFreePtr(WavePlay->lpData);
//	GlobalFreePtr(WavePlay);
//	waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
//  GlobalFreePtr(WavePlay->lpData);
//	GlobalFreePtr(WavePlay);
//	TerminateThread(hVoice,0);//가퍊E윈도퓖E메시지를 받는 쓰레드를 종료시킨다
	//GlobalFreePtr(WavePlay->lpData);
	//GlobalFreePtr(WavePlay);
}	

/*버그 쓸데 없이 핸들이 아주조금씩 증가가 된다 */
void CALLBACK InWaveData(HWAVEIN WaveIn,UINT message,DWORD instance,DWORD Param1,DWORD Param2)
{
	if(message==WIM_DATA && ThreadOn==TRUE)
	{
		//VOICE_DATA_NODE *Create=NULL;
		long pkl=PacketLength;
		static LPARAM qsb=0;
		static int RecCount=0;
	    int bOverRun;
		//printf("QSB[%d]\n",qsb);
		//printf("[%d]큰일남!\n",RecCount);
  	    //waveInMessage(WaveIn,WOM_DONE,Param1,Param2);
		/*
		if(InputStop==TRUE)//멈춰있다툈E다웝兢하햨E않게 그냥 버퍼에 음성을 넣엉佛기만 한다
		{
		  WaveRecord->dwBufferLength=currentInputLength;
		  waveInAddBuffer(hWaveIn,WaveRecord,sizeof(WAVEHDR));
			return;
		}
		*/
		bOverRun=(RecCount++>0);
		if(bOverRun) 
		{
          if(qsb==0) 
			{
			  qsb=Param1;
				RecCount--;
				return;
			}
			WaveRecord=(LPWAVEHDR)Param1;
			WaveRecord->dwBufferLength=currentInputLength;				    
			waveInAddBuffer(hWaveIn,WaveRecord,sizeof(WAVEHDR));
			++outputPacketsLost;
			RecCount--;
			return;
		}
		--RecCount;
		WaveRecord=(LPWAVEHDR)Param1;
		if(WaveRecord->dwBytesRecorded>(DWORD)currentInputLength)	WaveRecord->dwBytesRecorded=currentInputLength;
		else if(WaveRecord->dwBytesRecorded<(DWORD)currentInputLength && WaveRecord->dwBytesRecorded>0) 
		{
			memset(WaveRecord->lpData+WaveRecord->dwBytesRecorded,sampleAlignment==1?0x80:0,((int)(currentInputLength-WaveRecord->dwBytesRecorded)));
			WaveRecord->dwBytesRecorded=currentInputLength;
		}
  	//printf("%d",WaveRecord->dwBytesRecorded);
		if(WaveRecord->dwBytesRecorded>0 && !InputStop)
		{
			//printf("[%d]무조건호횁E\r\n",message);
			//if(Create->VoiceInputState)//현픸E클라이언트에서 퀋E습� 가능하다툈E
			if(!bOverRun)
			{
//				printf("값:%d",WaveRecord->lpData);
		        SoundBufferCreate(WaveRecord->lpData,(WORD)WaveRecord->dwBytesRecorded,AudioChannel,samplesPerSecond,bytesPerSecond,(WORD)sampleAlignment);//사웝�갋버퍼를 만든다(vox따卍정)
				if(sqpacket!=squelched) squelched=sqpacket;//vox커트라인
				if(!squelched)//음성이 Vox커트라인에 해당되엉雹 픸E邦� 된다(만푳E음성이 그값만큼 들엉邦햨E않으툈E픸E滂프갋않는다)
				{
					//g_xMain->SendVoiceToUser((LPSTR)&sb,(int)pkl);
					CVideoViewDlg::getInstance()->SendVoiceToUser((LPSTR)&sb,(int)pkl);
				  //VoiceDataSend(NowSelectAddress,(LPSTR)&sb,(int)pkl);//정해햨E주소로 음성을 픸E徘磯�

				}
			}
		}
		if(InputReset)//버그:이걸 없애야 멈춘후 소리가 또 남
		{ 
		  waveInUnprepareHeader(hWaveIn,WaveRecord,sizeof(WAVEHDR));
			if(WaveRecord->lpData!=NULL) GlobalFreePtr(WaveRecord->lpData);
			GlobalFreePtr(WaveRecord);
		  if(--waveHeadersAllocated==0) 
			{
			  //waveInClose(hWaveIn);
				waveInClose(hWaveIn);
			  hWaveIn=NULL;
			  InputActive=FALSE;
			}
		}
		else
		{
			//WaveRecord->dwFlags=0;
			//printf("값은:%d\r\n",waveHeadersAllocated);
		  WaveRecord->dwBufferLength=currentInputLength;
		  waveInAddBuffer(hWaveIn,WaveRecord,sizeof(WAVEHDR));
		}
		if(qsb==0) return;
		Param1=qsb;
		qsb=0;
	}
}

//버퍼에 저장된 음성을 출력한다
void CALLBACK OutWaveData(HWAVEOUT WaveOut,UINT message,DWORD instance,DWORD Param1,DWORD Param2)
{	
//	printf("출력해준다");
  if(message==WOM_DONE && ThreadOn==TRUE)
  {  
	  
 	  //waveOutUnprepareHeader(hWaveOut,WavePlay,sizeof(WAVEHDR));
	  LPWAVEHDR waveHdr=(LPWAVEHDR)Param1;
	  outputPending--;
   	 // printf("출력값:%d",outputPending);
	  waveOutUnprepareHeader(hWaveOut,waveHdr,sizeof(WAVEHDR));
	  GlobalFreePtr(waveHdr->lpData);
	  GlobalFreePtr(waveHdr);
	  waveHdr=NULL;
//	  if(svoice==1 && outputPending<=2) Startvoice();
  }
}

static DWORD lastYieldTime=0;
void MessageLoop(MSG *pmsg)//메시지를 받는 컖E
{
	TranslateMessage(pmsg);
    DispatchMessage(pmsg);
}

void DelayLoop(void)//꼭해줘야 한다 안해주툈E음성이 딜레이 된다
{
	MSG msg;
	if((GetTickCount()-lastYieldTime)>350) 
	{
	  while(PeekMessage(&msg,NULL,0,0,PM_REMOVE)) 
		{
			MessageLoop(&msg);
	  }
  }
}

//윈도퓖E핸들퓖E퀋E� 메시햨E음성으로 들엉邦는 메시지를 퀋E� 한다 퀋E실� 메시지는 소켓에 접속되엉復는걸로 쏜다!)
void WimData(WPARAM wParam,LPARAM lParam)
{
  if(ThreadOn==TRUE || VoiceRecordSave==TRUE)
	{
	//printf(".");
		//VOICE_DATA_NODE *Create=NULL;
		static LPARAM qsb=0;
		static int RecCount=0;
	  int bOverRun;
		bOverRun=(RecCount++>0);
		DelayLoop();
		/*
		if(bOverRun) 
		{
      if(qsb==0) 
			{
			  qsb=wParam;
				RecCount--;
				return;
			}
			WaveRecord=(LPWAVEHDR)lParam;
			WaveRecord->dwBufferLength=currentInputLength;				    
			waveInAddBuffer(hWaveIn,WaveRecord,sizeof(WAVEHDR));
			++outputPacketsLost;
			RecCount--;
			return;	//WSAEACCES
		}*/
		--RecCount;
		WaveRecord=(LPWAVEHDR)lParam;

		if(WaveRecord->dwBytesRecorded>(DWORD)currentInputLength)	WaveRecord->dwBytesRecorded=currentInputLength;
		else if(WaveRecord->dwBytesRecorded<(DWORD)currentInputLength && WaveRecord->dwBytesRecorded>0) 
		{
			memset(WaveRecord->lpData+WaveRecord->dwBytesRecorded,sampleAlignment==1?0x80:0,((int)(currentInputLength-WaveRecord->dwBytesRecorded)));
			WaveRecord->dwBytesRecorded=currentInputLength;
		}
  	//printf("%d",WaveRecord->dwBytesRecorded);
		if(WaveRecord->dwBytesRecorded>0 && !InputStop)
		{
			//printf("[%d]무조건호횁E\r\n",message);
			//if(Create->VoiceInputState)//현픸E클라이언트에서 퀋E습� 가능하다툈E
			if(!bOverRun)
		  {
		    SoundBufferCreate(WaveRecord->lpData,(WORD)WaveRecord->dwBytesRecorded,AudioChannel,samplesPerSecond,bytesPerSecond,(WORD)sampleAlignment);//사웝�갋버퍼를 만든다(vox따卍정)
				if(sqpacket!=squelched) squelched=sqpacket;//vox커트라인
				if(!squelched)//음성이 Vox커트라인에 해당되엉雹 픸E邦� 된다(만푳E음성이 그값만큼 들엉邦햨E않으툈E픸E滂프갋않는다)
				{
					if(VoiceRecordSave==FALSE)
					{
						//g_xMain->SendVoiceToUser((LPSTR)&sb,(int)PacketLength);
						CVideoViewDlg::getInstance()->SendVoiceToUser((LPSTR)&sb,(int)PacketLength);
						//VoiceDataSend(NowSelectAddress,(LPSTR)&sb,(int)PacketLength);//정해햨E주소로 음성을 픸E徘磯�
					}
					else
					{
					}
				}
			}
		}
	  if(InputReset)//버그:이걸 없애야 멈춘후 소리가 또 남
		{ 
		  waveInUnprepareHeader(hWaveIn,WaveRecord,sizeof(WAVEHDR));
			if(WaveRecord->lpData!=NULL) GlobalFreePtr(WaveRecord->lpData);
			GlobalFreePtr(WaveRecord);
		  if(--waveHeadersAllocated==0) 
			{
			  //waveInClose(hWaveIn);
				waveInClose(hWaveIn);
			  hWaveIn=NULL;
			  InputActive=FALSE;
			}
		}
    else
		{
			//WaveRecord->dwFlags=0;
			//printf("값은:%d\r\n",waveHeadersAllocated);
		  WaveRecord->dwBufferLength=currentInputLength;
		  waveInAddBuffer(hWaveIn,WaveRecord,sizeof(WAVEHDR));
		}
		if(qsb==0) return;
		lParam=qsb;
		qsb=0;
	}	
}

//버퍼가 차툈E여기로 메시지가 호횁E//BYTE
void WomDone(WPARAM wParam,LPARAM lParam)
{
	//do{}while ((!WavePlay->dwFlags & WHDR_DONE));
  if(ThreadOn==TRUE || VoiceRecordSave==TRUE)
  {
	  LPWAVEHDR waveHdr=(LPWAVEHDR)lParam;
  	  outputPending--;
	  //printf("출력중[%d]",outputPending);
	  waveOutUnprepareHeader(hWaveOut,waveHdr,sizeof(WAVEHDR));
	  GlobalFreePtr(waveHdr->lpData);
	  GlobalFreePtr(waveHdr);
//	  if(svoice==1 && outputPending<=2) Startvoice();
  }
}

/*
waveOutClose() 웨이틒E오디오 출력장치를 닫는다.
waveOutOpen() 재생을 위한 웨이틒E오디오 출력 장치를 연다.
waveOutPause() 웨이틒E오디오 출력장치의 재생을 잠시 중지시키컖E현픸E위치를 저장한다.
waveOutPrepareHeader() 웨이틒E오디오 출력장치의 재생을 위해 출력 데이터 버퍼 빚遝을 준비한다.
waveOutReset() 웨이틒E오디오 출력장치의 재생을 중지시키컖E�갋위치를 0으로 재설정한다.
waveOutRestart() 현위치로부터 웨이틒E윳嘲� 출력장치의 재생을 다시 시작한다.
waveOutProc() CALLBACK함펯E 재생시 메시지가 발생했을 때 사퓖E磯�.
waveUnPrepareHeader() 버퍼 빚遝을 해제하콅E픸E�, 준비했큱E�갋矗� 해제한다.
waveOutWrite() 재생을 위해서 웨이틒E출력 장치에게 데이터 빚遝을 보낸다.
waveInAddBuffer() 웨이틒E韜쩜梁×“� 입력버퍼를 제공한다.
waveInClose() 웨이틒E韜쩜梁「� 닫는다.
waveInMessage() 웨이틒E韜쩜梁×“� 메시지를 보낸다.
waveInOpen() 퀋E습� 위한 웨이틒E韜쩜梁「� 연다.
waveInPrepareHeader() 웨이틒E梁×� 채워햨E입력 버퍼를 준비한다.
waveInProc() waveInOpen에서 지정하는 CALLBACK함펯E waveInAddbuffer에서 지정한 버퍼가 다 채워졌을 경퓖E이를 사퓖E� 다음에 사퓖E� 버퍼를 준비한다.
waveInReset() 웨이틒E韜쩜梁》觀壙� 모탛E입력을 중지하컖E 현위치를 0으로 바꾼다. 
waveInStart() 웨이틒E韜쩜梁×“� 퀋E슬쳄� 메시지를 보낸다. 
waveInStop() 웨이틒E韜쩠◐×“� 퀋E습� 끝낼 메시지를 보낸다.
waveInUnprepareHeader() waveInprepareHeader에 의해 준비된 버퍼를 해제한다.
*/
