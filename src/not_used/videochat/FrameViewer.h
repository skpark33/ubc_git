#if !defined(AFX_FRAMEVIEWER_H__3D5068E4_1E6E_416A_9E5B_F60625370068__INCLUDED_)
#define AFX_FRAMEVIEWER_H__3D5068E4_1E6E_416A_9E5B_F60625370068__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FrameViewer.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFrameViewer window
#include "CBitmapEx.h"
#include "CISBitmap.h"
#include "AviFile.h"

#include "ximage.h"

class CFrameViewer : public CWnd
{
// Construction
public:
	CFrameViewer();

// Attributes
public:
    CAviFile * paviFile;
    CBitmapEx	m_ImageBitmap;
    #pragma pack(1)
	typedef struct _STRUCT_BITMAPS
	{
		HBITMAP		hBitmap;		// Handle to bitmap
		DWORD		dwWidth;		// Width of bitmap
		DWORD		dwHeight;		// Height of bitmap
		HBITMAP		hMask;			// Handle to mask bitmap
		COLORREF	crTransparent;	// Transparent color
	} STRUCT_BITMAPS;
    #pragma pack()

    STRUCT_BITMAPS	m_csBitmaps[2];

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFrameViewer)
	//}}AFX_VIRTUAL

// Implementation
public:
	CWnd* m_pCloneWnd;
	void SetIsMyView(bool bMyView);
	bool m_bMyView;
//	CContact* GetUser();
	int GetRemotePort();
	CString GetRemoteAddress();
	void SetRemotePort(int remotePort);
	void SetRemoteAddress(CString remoteAddress);
	CSize GetImageSize();
	CSize m_sizeImage;
	CString m_strRecFileName;
	BOOL m_bRec;
	CString StopRecAVI();
	BOOL StartRecAVI(CString strAviFile, DWORD dwRate = 15);
	BOOL m_bUseBackImage;
	void SetUseBackImage( BOOL bUse);
	BOOL GetUseBackImage();
    LPBYTE GetImageBitsBuffer();
	LPBITMAPINFO GetDIB();
	BITMAPINFO m_BitmapInfo;
	COLORREF m_nTransColor;
	CCISBitmap* m_BgBitmap;
    void FreeResources(BOOL bCheckForNULL = TRUE);
    void DrawBitmap(CDC* pDC, RECT* rpItem);
    DWORD SetBitmaps(HBITMAP hBitmapIn, COLORREF crTransColorIn);
    DWORD SetBitmaps(int nBitmapIn, COLORREF crTransColorIn);
    HBITMAP CreateBitmapMask(HBITMAP hSourceBitmap, DWORD dwWidth, DWORD dwHeight, COLORREF crTransColor);
	void SetBGImg(int nResource, COLORREF m_nTransColor = RGB(0,0,0), BOOL bUse = TRUE);

	CBitmapEx* GetBitPtr();
	BOOL SetFrame(LPBITMAPINFO lpBi);
    void SetFrameSnap(LPBITMAPINFO lpBi);
    void SetFrameSnap(LPCSTR filename);

	virtual ~CFrameViewer();

	// Generated message map functions
protected:
////	CContact* m_pUser;
	int m_nRemotePort;
	CString m_szRemoteAddress;
	LPVOID imageData;
	//{{AFX_MSG(CFrameViewer)
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CxImage		m_image;

public:
	bool	LoadImage(LPCSTR lpszFilename);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	CRgn	m_rgn;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FRAMEVIEWER_H__3D5068E4_1E6E_416A_9E5B_F60625370068__INCLUDED_)
