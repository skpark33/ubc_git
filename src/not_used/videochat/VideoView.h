
#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.

// CTestdllDlgApp
// 이 클래스의 구현을 보려면 TestdllDlg.cpp를 참조하십시오.

//
//extern "C" AFX_EXT_API void ShowViewDlg(char* RemoteHost , char* RemotePort, int cx, int cy, int nWidth,int nHeight );
extern "C" __declspec(dllexport) void ShowViewDlg(LPCSTR lpszRemoteHost, LPCSTR lpszRemotePort, CRect rc, CWnd* wnd, LPCSTR lpszBgImage, bool bViewPartner=true);

extern "C" __declspec(dllexport) void DeleteViewDlg();


class CVideoViewApp : public CWinApp
{
public:
	CVideoViewApp();

// 재정의입니다.
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()

public:

};