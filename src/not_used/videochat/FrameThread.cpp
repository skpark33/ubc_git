// FrameThread.cpp : implementation file
//

#include "stdafx.h"
#include "FrameThread.h"
#include "VideoViewDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFrameThread

IMPLEMENT_DYNCREATE(CFrameThread, CWinThread)


CFrameThread::CFrameThread()
{
	m_pWnd = NULL;
}

CFrameThread::~CFrameThread()
{
}

BOOL CFrameThread::InitInstance()
{
     //음성을 위주로 동작하자.. 우선순위를 좀 낮추자..
    this->SetThreadPriority(   THREAD_PRIORITY_IDLE  );

	return TRUE;
}

int CFrameThread::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CFrameThread, CWinThread)
	//{{AFX_MSG_MAP(CFrameViewerThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	ON_THREAD_MESSAGE(WM_THREADMSG_SETMYFRM, OnSetMyFrm)
	ON_THREAD_MESSAGE(WM_THREADMSG_VIEWPARTNER, OnViewPartner)
	ON_THREAD_MESSAGE(WM_THREADMSG_SENDFRAME, OnSendFrame)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFrameViewerThread message handlers

//skpark 리턴타입을 void 형으로 바꿈
void CFrameThread::OnSetMyFrm(WPARAM wParam, LPARAM lParam)
//LRESULT CFrameThread::OnSetMyFrm(WPARAM wParam, LPARAM lParam)
{
	if( m_pWnd && m_pWnd->GetSafeHwnd()) 
	{
		((CVideoViewDlg*)m_pWnd)->SetMyFrame();
	}
	//skpark 리턴타입을 void 형으로 바꿈
	//return 0;
	return;
}

//skpark 리턴타입을 void 형으로 바꿈
void CFrameThread::OnViewPartner(WPARAM wParam, LPARAM lParam)
//LRESULT CFrameThread::OnViewPartner(WPARAM wParam, LPARAM lParam)
{
	char*pJpg = NULL;
	DWORD dwSize =0;
	CWnd* pWnd = NULL;
	char * p = (char*) lParam;
	FRAME_T * pFrame = (FRAME_T *)lParam;
	if( m_pWnd) 
	{
		Sleep(10);
		((CVideoViewDlg*)m_pWnd)->ViewPartnerJPEG(pFrame->pImg,pFrame->dwSize,pFrame->pWnd);
	}
	free( pFrame);
	//skpark 리턴타입을 void 형으로 바꿈
	//return 0;
	return;
}


//skpark 리턴타입을 void 형으로 바꿈
void CFrameThread::OnSendFrame(WPARAM wParam, LPARAM lParam)
//LRESULT CFrameThread::OnSendFrame(WPARAM wParam, LPARAM lParam)
{
	if(m_pWnd)
	{
		((CVideoViewDlg*)m_pWnd)->SendJPEGFrame();
	}
	//skpark 리턴타입을 void 형으로 바꿈
	//return 0;
	return;
}
