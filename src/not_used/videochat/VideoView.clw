; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CVideoViewDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "VideoView.h"

ClassCount=3
Class1=CVideoViewApp
Class2=CVideoViewDlg
Class3=CAboutDlg

ResourceCount=7
Resource1=IDD_VIDEOVIEW_DIALOG
Resource2=IDR_MAINFRAME
Resource3=IDD_ABOUTBOX
Resource4=IDD_CHOOSEFRAME
Resource5=IDD_SNAPFILE_WND
Resource6=IDD_BG_SELECT_WND
Resource7=IDD_CLONEFRM_WND

[CLS:CVideoViewApp]
Type=0
HeaderFile=VideoView.h
ImplementationFile=VideoView.cpp
Filter=N

[CLS:CVideoViewDlg]
Type=0
HeaderFile=VideoViewDlg.h
ImplementationFile=VideoViewDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_IPADDRESS

[CLS:CAboutDlg]
Type=0
HeaderFile=VideoViewDlg.h
ImplementationFile=VideoViewDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_VIDEOVIEW_DIALOG]
Type=1
Class=CVideoViewDlg
ControlCount=27
Control1=IDC_MAINAV_VIDEO_ME,static,1342177540
Control2=IDC_MAINAV_VIDEO_PARTNER,static,1342177540
Control3=IDC_BUTTON1,button,1342242816
Control4=IDC_BUTTON2,button,1342242816
Control5=IDC_BUTTON3,button,1342242816
Control6=IDC_BUTTON4,button,1342242816
Control7=IDC_BUTTON5,button,1342242816
Control8=IDC_BUTTON6,button,1342242816
Control9=IDC_BUTTON7,button,1342242816
Control10=IDC_BUTTON8,button,1342242816
Control11=IDC_BUTTON9,button,1342242816
Control12=IDC_BUTTON10,button,1342242816
Control13=IDC_BUTTON11,button,1342242816
Control14=IDC_START,button,1342242816
Control15=IDC_IPADDRESS,edit,1350631552
Control16=IDC_PORT,edit,1350631552
Control17=IDC_STATIC,static,1342308352
Control18=IDC_STATIC,static,1342308352
Control19=IDC_STOP,button,1342242816
Control20=IDC_STATIC,button,1342177287
Control21=IDC_STATIC,button,1342177287
Control22=IDC_STATIC,button,1342177287
Control23=IDC_STATIC,button,1342177287
Control24=IDC_STATIC,button,1342177287
Control25=IDC_STATIC,button,1342177287
Control26=IDC_STATIC,button,1342177287
Control27=IDC_STATIC,button,1342177287

[DLG:IDD_CHOOSEFRAME]
Type=1
Class=?
ControlCount=14
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_SLIDER_FRAME,msctls_trackbar32,1342242832
Control4=IDC_SLIDER_QUALITY,msctls_trackbar32,1342242832
Control5=IDC_STATIC_FRAME,static,1342308352
Control6=IDC_STATIC_QUALITY,static,1342308352
Control7=IDC_STATIC,button,1342177287
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,button,1342177287
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,static,1342308352

[DLG:IDD_SNAPFILE_WND]
Type=1
Class=?
ControlCount=2
Control1=IDC_ST_SNAPVIVEW,static,1342177284
Control2=IDC_ST_SNAP_FILEPATH,static,1342308608

[DLG:IDD_BG_SELECT_WND]
Type=1
Class=?
ControlCount=10
Control1=IDC_BTN_BG0,button,1342242816
Control2=IDC_BTN_BG1,button,1342242816
Control3=IDC_BTN_BG2,button,1342242816
Control4=IDC_BTN_BG3,button,1342242816
Control5=IDC_BTN_BG5,button,1342242816
Control6=IDC_BTN_BG6,button,1342242816
Control7=IDC_BTN_BG7,button,1342242816
Control8=IDC_BTN_BG8,button,1342242816
Control9=IDC_BTN_BG4,button,1342242816
Control10=IDC_BTN_BG9,button,1342242816

[DLG:IDD_CLONEFRM_WND]
Type=1
Class=?
ControlCount=0

