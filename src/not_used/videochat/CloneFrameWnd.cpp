// CloneFrameWnd.cpp : implementation file
//

#include "stdafx.h"
#include "CloneFrameWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCloneFrameWnd dialog
CCloneFrameWnd::CCloneFrameWnd(CWnd* pParent /*=NULL*/)
	: CDialog(CCloneFrameWnd::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCloneFrameWnd)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    	m_hIcon = AfxGetApp()->LoadIcon(IDD_VIDEOVIEW_DIALOG);
}


void CCloneFrameWnd::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCloneFrameWnd)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCloneFrameWnd, CDialog)
	//{{AFX_MSG_MAP(CCloneFrameWnd)
		// NOTE: the ClassWizard will add message map macros here
        ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_NCPAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCloneFrameWnd message handlers
void CCloneFrameWnd::OnOK() 
{
	DestroyWindow();

//	CDialog::OnOK();
}

void CCloneFrameWnd::OnCancel() 
{
	DestroyWindow();	
//	CDialog::OnCancel();
}

void CCloneFrameWnd::SetFrameViewerPtr(CFrameViewer *pFrmView)
{
	m_pFrameViewer = pFrmView;
}

void CCloneFrameWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	CRect rect;
	GetClientRect(rect);

	if( m_pFrameViewer && 
		m_pFrameViewer->m_bMyView &&
				m_pFrameViewer->m_ImageBitmap.GetSafeHandle() ) {
		dc.SetStretchBltMode(HALFTONE) ;
		m_pFrameViewer->m_ImageBitmap.StretchBlt(&dc, rect);
    }
    else {
		//SendMessage(WM_CLOSE,0,0);
	


		dc.FillSolidRect( rect, RGB( 255,255,255));
    	CFont wFont;
		wFont.CreateFont(24, 0, 0, 0, 0,
							0, 0, 0,
                 			0, 0, 0, 0,
                 		VARIABLE_PITCH | FF_SWISS, "MS Sans Serif");
		CFont *of=dc.SelectObject(&wFont);
			
		CString s="No Image Data";
		dc.SetBkMode(TRANSPARENT);
		dc.SetTextColor(GetSysColor(COLOR_BTNSHADOW));
		dc.DrawText(s,&rect,
				DT_CENTER|DT_SINGLELINE|DT_VCENTER);
		dc.SelectObject(of);
		wFont.DeleteObject();

    }

	// Do not call CDialog::OnPaint() for painting messages
}



void CCloneFrameWnd::OnTimer(UINT nIDEvent) 
{
	switch (nIDEvent)
	{
	case	REFRESH_TIMER :
		{
			Invalidate(FALSE);
		}
		break;
	}
	
	CDialog::OnTimer(nIDEvent);
}

BOOL CCloneFrameWnd::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	SetWindowText("CloneFrame");
//	SetWindowPos(&wndTopMost , 0,0, 162,122,SWP_NOMOVE );
	//SetWindowPos(&wndTopMost , 640-162,480-122, 162,122,SWP_NOMOVE);
	//MoveWindow(640-162,480-122, 162,122, TRUE);

//	SetTimer( 	REFRESH_TIMER , 71, NULL);	//14fps

	nRegMsg3 = ::RegisterWindowMessage(_T("PhoneViewer"));

	SetForegroundWindow();

	if(this != GetFocus())
	{
		SetFocus();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CCloneFrameWnd::PreTranslateMessage(MSG* pMsg) 
{

	//if (pMsg->message == WM_TIMER)
	//	OnTimer();
	//else if(pMsg->message == WM_KEYDOWN)

	if(pMsg->message == WM_KEYDOWN)
	{
		//TRACE("----------------CCloneFrameWnd::WM_KEYDOWN (%d, %d)\n", pMsg->wParam, pMsg->lParam);
		CString msg;
		msg.Format("----------------CCloneFrameWnd::WM_KEYDOWN (%d, %d)\n", pMsg->wParam, pMsg->lParam);

		::AfxMessageBox(msg);

		CString str = NULL;
		CString isReject = NULL;
       switch(pMsg->wParam)

        {					
            case VK_ESCAPE://ESCŰ�� ��������..

			::SendMessage(HWND_BROADCAST, nRegMsg3, (WPARAM)(LPCSTR)str, (LPARAM)(LPCSTR)isReject);	

			return TRUE;
        }
	}

	return CDialog::PreTranslateMessage(pMsg);;
}

void CCloneFrameWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));
	CDialog::OnLButtonDown(nFlags, point);
}

void CCloneFrameWnd::OnDestroy() 
{
	KillTimer( REFRESH_TIMER);
//	if( m_pFrameViewer && m_pFrameViewer->GetSafeHwnd() )
//		m_pFrameViewer->m_pCloneWnd = NULL;

	CDialog::OnDestroy();

}

// â�� ����..
void CCloneFrameWnd::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CDialog::OnLButtonDblClk(nFlags, point);
	OnOK() ;
}

BOOL CCloneFrameWnd::OnEraseBkgnd(CDC* pDC) 
{
//	CRect rect;
//	GetClientRect(rect);
//	pDC->FillSolidRect( rect, RGB( 255,255,255));
	return TRUE;
//	return CDialog::OnEraseBkgnd(pDC);
}

void CCloneFrameWnd::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	CRect rect;
	GetWindowRect(rect);


	CRgn rgn;
	rgn.CreateRectRgn (2, 2, rect.Width()-1, rect.Height()-1);
//    rgn.CreateRoundRectRgn( 1,1 , rc.Width()+6, rc.Height()+6 ,6,6);
	SetWindowRgn (rgn, TRUE);


	if( cx > 0 && cy >0)
	{
		SetWindowPos(NULL , 0,0, rect.Width(),rect.Height(),SWP_NOMOVE );
	}
}

void CCloneFrameWnd::OnNcPaint() 
{
/*
	CRect    rc;
    GetClientRect (rc);

    CRgn * rgn = new CRgn;
    rgn->CreateRectRgn (rc.left, rc.top, rc.right, rc.bottom);

    CDC * dc = GetDCEx (rgn, DCX_WINDOW | DCX_CACHE |  DCX_CLIPSIBLINGS);    

    rc.InflateRect (0, 0, 6, 6);
    dc->Draw3dRect(rc, RGB (0,0, 0), RGB (0,0, 0));

    rc.DeflateRect (1, 1, 1, 1);
    dc->Draw3dRect(rc, RGB (0,0, 0), RGB (0,0,0));

    rc.DeflateRect (1, 1, 1, 1);
    dc->Draw3dRect(rc, RGB (0, 0, 0), RGB (0, 0, 0));

    ReleaseDC (dc);    
    delete rgn;
	*/
	// Do not call CDialog::OnNcPaint() for painting messages
}

