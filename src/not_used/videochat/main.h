/*-----------------------------------------------------------------------------
| Programming by KimJaehong                                                    |
|------------------------------------------------------------------------------|
| Voice Tool Engine                                                            |
| Date: 2000.12.20                                                             |
| Tool: Visual C++ 6.0                                                         |
 -----------------------------------------------------------------------------*/
#ifndef __VOICE_TOOL_KIT_H//만일 __VOICE_TOOL_KIT_H가 선언되어있지 않다면 이부분을 컴파일한다
#define __VOICE_TOOL_KIT_H

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxsock.h>		// MFC socket extensions
//#include <winsock2.h>//winsock2 소켓을 사용한 헤더파일
#include <windowsx.h>
#include <mmsystem.h>//음성을 사용한 헤더파일
#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include <dsound.h>
#include <process.h>

#include "SOUNDBUFFER.H"//SoundBuffer
#include "RTP.h"//RTP Protocol
#include "GSMCODEC.h"//GsmCodec6.01
#include "ADPCMCODEC.h"//AdpcmCodec
//#include "g723.h"//G723Codec
//#include "RECORDFILE.H"//Voice Recording

//초기화시 정의해주는 코덱 매크로 상수
#define ADPCM_CODEC 1//ADPCM Codec
#define GSM_CODEC   2//GSM Codec

//음성 전송중 패킷 구분을 위한 매크로 상수(1:1음성 채팅에서 사용할 수 있다)
#define PACKET_VOICESENDING_START 0x01//음성전송버튼을눌렀을때
#define PACKET_VOICESENDING_STOP  0x02//음성전송버튼을떼었을때
#define PACKET_VOICE_CONNECT_TEST 0x03//보이스 채팅 접속테스트
#define PACKET_VOICE_CONNECT_QUIT 0x04//보이스 채팅 프로그램 종료
#define PACKET_VOICE_NOT_SEND     0x05//아무것도 보내지 않았음(음성을 보내지 않았다)

//접속알림코드매크로(1:1용)
#define MYSELF_NOT_CONNECT 1//나자신에게 접속을 하지 못하도록 한다
#define CLIENT_NOT_CONNECT 2//알수 없는 클라이언트
#define UNKNOWN_IPADDRESS  3//알수 없는 주소
#define ADDRESS_NOINPUT    4//주소를 입력하지 않았으면
#define ALREADY_CONNECT    5//이미 접속되어 있을때
#define SUCCESS_CONNECT    6//성공적으로 접속 하였을때

//엔진 초기화 매크로
#define MAXUSER            256//유저수
#define MONO							 1//오디오입력 옵션(mono)
#define STREO							 2//오디오입력 옵션(streo)
#define RTP_PROTOCOL       1//RTP프로토콜
#define ENCRYPTED_PROTOCOL 2//암호화 프로토콜
#define UNKNOWN_PROTOCOL   3//알수없는 프로토콜

//오디오 초기화 에러 검사 매크로
#define RECORD_FORMAT_ERROR    1
#define WAVE_INPUT_OPEN_ERROR  2
#define WAVE_OUTPUT_OPEN_ERROR 3
#define WAVE_PLAY_FORMAT_ERROR 4

//네트워크 메시지 ID(//WM_USER을 해주면 윈도우메시지 ID로 사용 할 수 있다)
#define WM_ASYNCSERVER (WM_USER+1001)//서버가 받는 메시지
#define WM_ASYNCCLIENT (WM_USER+1002)//클라이언트가 받는 메시지

//네트워크 소켓 이벤트 발생 메시지
#define WM_DATA_RECEIVE (WM_USER+1003)
#define WM_DATA_OPTION  (WM_USER+1004)

struct LOCAL_LOOPBACK//Local loopback packet buffer
{
	struct LOCAL_LOOPBACK *llnext;//chain or NULL의 다음버퍼
	int lllen;//패킷의 바이트 길이
	int llsamples;//샘플의 패킷 갯수
	SOCKADDR_IN lladdr;//패킷을 보낼 주소
	char llpacket;//패킷데이타
};

typedef enum AUDIO_STATE
{
  NewCreate,//접속을 새로 만든다
  Idle,//아무것도 하지 않는 상태
  SendingLiveAudio,//실시간 음성 메시지를 전송중
  Transferring,//음성 메시지를 전송한다
  PlayingReceivedAudio//리모트 호스트로부터 받아들여진 음성을 PLAYING중
}VOICE_STATE;

typedef struct CLIENT_INFORMATION
{
	char Handle[128];//유저구분핸들(주소를 넣어서 구분했다)
	VOICE_STATE ClientState;//현재음성의 입/출력등 상태
  int VoiceInputState;//음성녹음을 해도 되는가?
//	SOCKET ServerReceive;//접속을 기다리는 소켓
//	SOCKET ServerOption;//RTP/VAT제어 소켓
//	SOCKET TCPIPClient;//TCPIP클라이언트
//	SOCKET TCPIPServer;//TCPIP서버
	struct sockaddr_in AcceptAddress;//데이타를 받을 상대편의 주소
//	struct sockaddr_in OptionAddress;//RTP/VAT제어 메시지 받을 상대편의 주소
  SOCKADDR_IN ClientAddress;//클라이언트 소켓 주소 
  unsigned int CurrentPort;//목적지의 데이타 포트 번호
  char CurrentHostName[30];//호스트명
	int CurrentProtocol;//보내는 프로토콜 방식
	int localLoopback;//루프백이 가능한지 검사
  struct LOCAL_LOOPBACK *llhead,*lltail;//Local loopback packet chain(머리,꼬리)더블 링크드 리스트
	struct CLIENT_INFORMATION *prev,*next;//이전데이타/앞에데이타를 가리킨다(현구조체 클라이언트)
}VOICE_DATA_NODE;

extern void TracePrintf(char *format,...);//디버그할때 디버그메시지를 창에서 Printf처럼 쓸 수 있다
extern void OnAsyncSelectServer(WPARAM wParam, LPARAM lParam);//서버가 클라이언트로부터 받은 이벤트
extern void OnAsyncSelectClient(WPARAM wParam,LPARAM lParam);//클라이언트가 서버로 부터 받은 메시지 
extern int TCPIPInitClientSocket(char *address);//클라이언트로서 상대방에게 접속을 한다
extern int TCPIPInitServerSocket(void);//서버로서 상대방에 접속을 기다린다
extern void DataReceive(SOCKET Socket);//실제 사운드 데이타를 받는 소켓

//음성파일을 플래이한다
extern void VoiceFilePlayStart(void);//음성파일 플래이를 시작한다
extern void VoiceFilePlayStop(void);//음성파일 플래이를 스톱한다
extern void WindowCreateMainHandle(void *argv);//윈도우즈 핸들 시작 초기화!! 생성해준다
extern bool VoicePlayAll(void);//음성중에 Stop을 사용할 수 있다(신버젼용)
extern bool VoiceStopAll(void);//접속된 모든 사용자에게 음성 전송을 중단한다(패킷전송도 멈춘다)
extern bool VoicePlay(char *Address);//음성출력시작(주소,음성전송할 상대 주소를 넣는다 TRUE현재 출력중인 상대편을 바꾸고 FALSE이면 원래 주소로 음성을 play한다)
extern bool VoiceStop(void);//음성출력을 멈춘다(단일사용자)
extern void WaveInputStop(void);//녹음을 정지시킨다(잔여 버퍼를 처리해준다)
extern int HardWareChecking(void);//반이중 통신인지 하드웨어 체크
extern int InitVoiceDevice(void);//음성입력 장치를 초기화 시킨다
extern int StartWaveInput(void);//음성을 저장한다
extern bool RecordVoiceStop(void);//음성 녹음을 멈춘다(패킷전송도 멈춘다)
extern bool RecordVoicePlay(void);//음성을 파일로 저장하기 위해 녹음을 한다
extern void RecordDataReceive(void);//,VOICE_DATA_NODE *Create) //음성데이타를 받는다 
extern void RecordWaveOutput(SoundBuffer *d,int bitsPerSample,int samplesPerSecond);//녹음된 음성을 스피커로 출력한다
extern void StartWaveOutput(SoundBuffer *d,int bitsPerSample,int samplesPerSecond);//음성을 스피커로 출력해준다
extern void SoundBufferCreate(LPSTR buffer,WORD buflen,DWORD channels,DWORD rate,DWORD bytesec,WORD align);//사운드버퍼를생성한다(vox등 커트라인조절)
extern int VoiceDataSend(char *Address,LPSTR buf,int buflen);//음성데이타를 목적지로 전송해준다(UDP)(직접 sendto해준다)
extern bool HostInfo(char* HostInfo,LPSTR pszHostName,LPIN_ADDR paddr);//호스트의 정보를 얻어온다
extern int inputBufferLength(void);//입력 버퍼 길이
extern int inputSampleCount(void);//입력 셈플 갯수
extern int WaveInAlloc(void);//녹음할 메모리를 할당
extern int InitWaveInOpen(void);//녹음을 하기 위해 WaveInopen을 초기화 해준다
extern void CALLBACK OutWaveData(HWAVEOUT WaveOut,UINT message,DWORD instance,DWORD Param1,DWORD Param2);//음성출력데이타
extern void CALLBACK InWaveData(HWAVEIN WaveIn,UINT message,DWORD instance,DWORD Param1,DWORD Param2);//음성녹음데이타
extern void WimData(WPARAM wParam,LPARAM lParam);//윈도우 메시지용 음성 데이타처리(녹음)
extern void WomDone(WPARAM wParam,LPARAM lParam);//윈도우 메시지용 음성 데이타처리(출력)

extern void DelayLoop(void);//음성지연이되면느려지는것을방지
extern void LoopBackGlobalCheckCode(int CheckCode,char* AddressCheckCode);//음성 코드가 확인이 되면 다시 원래 상태로 값을 복구 시켜줌 
extern char* LoopBackCheckCode(char *CheckCode);//클라이언트 껍데기에서 각종 음성 진단등 접속 보내기등..체크용으로 돌아갈 현재 상태를 검사하기 위한 루틴
extern char* LoopBackCheckCode(void);//껍데기에서 쓰이도록 인자를 주지 않도록했다
extern int VoiceDataCheckCodeSend(int PacketSection);//현재 음성 상태 코드를 상대방에게 전송해준다
extern int VoiceDateReceiveCheck(int OperationCode,int CheckData);//음성 데이타를 보냈으니 너는 버튼을 누르지말고 듣기만 하라고 지시하는 메시지를 보낸다
extern int PacketRecv(SOCKET Socket,SOCKADDR_IN ClientAddress);//패킷을 받아서 확인한다(음성전송,전송해제,클라이언트접속,접속해제코드등을 받아서 확인해준다 콜콜콜!)
extern void PacketSend(SOCKET Socket,char* MsgBuffer,int PacketSection);//패킷을 만든다음 구분해서 전송한다(TCP/IP전용)
extern char* LocalComputerAddress(void);//자기 컴퓨터의 호스트 주소를 얻어낸다
extern bool VoiceStateCheck(char *Address,int PacketSection);//상대방과 접속되어있는지 검사한다(접속테스트,음성전송,음성중지버튼등체크기능)
extern void ConnectChange(char *Address);//원하는 주소를 입력하여 접속위치를 바꾼다
extern bool IpChecking(char* Address);//IP주소검사
extern int CreateClient(char *Address);//주소에 해당하는 곳에 접속을 생성(클라이언트 리스트 하나 만든다)
extern int DataSearch(char *handle);//데이타를 검색한다(있다면TRUE,없으면FALSE)
extern int DataDelete(char *handle);//데이타를 삭제한다(실패 FALSE리턴)
extern int DataDeleteAll(void);//모든 접속 유저 데이타를 삭제 한다	  
extern int DataList(VOICE_DATA_NODE *List,char *UserListData[MAXUSER]);//데이타의 리스트를 본다
extern bool UserDataCreate(char *address);//유저 데이타를 새롭게 생성해준다
extern bool UserSocketCreate();//유저 소켓을 새롭게 생성해준다
extern int ConnectAddressList(bool Screen);//접속자의 주소의 리스트를 출력(값이 TRUE면 출력하고 인원수리턴 하고 FALSE이면 출력하지 않고 인원수리턴)
extern bool VoiceToolKitInit(int port,int NoiseCleanup,int CodecMode);//보이스 툴킷을 초기화 한다
extern bool VoiceToolKitInit(int NoiseCleanup,int CodecMode);//소켓으로 접속 안하고 녹음하고 플래이만 할때
extern void VoiceToolKitClose(void);
extern void VoiceToolKitClose(VOICE_DATA_NODE *Option);//보이스 툴킷을 끝낸다
extern int VoiceDataSend(char *Address,LPSTR buf,int buflen);//음성 데이타를 상대방에게 수신한다
extern int VoiceDataSendAll(LPSTR buf,int buflen);//접속된 사용자모두에게 음성 데이타를 전송한다
extern void ConnectRequestReceive(SOCKET Socket);//접속 요청을 받는다
extern void SocketClose();//열려있는 소켓을 알아서 몽땅 닫아준다
extern int WinSocketInit(void);//윈속 DLL초기화(소켓을 사용하기 위함)
extern int CloseSocket(SOCKET sock);//하나의 소켓을 Reset한다
extern int CreateSocket(SOCKET *psock,INT type,ULONG address,WORD port);//하나의 소켓을 생성해준다
extern bool CreateConnectRequest(char *Address);//주소를 넣어서 한개의 클라이언트를 생성한다
extern void UserDataScreen(char *address);//현재접속자상태를 보여준다(도스용 디버깅용!)
extern int YourConnect(char *Address);//상대방에서 접속했을경우

extern void LoopFlush(VOICE_DATA_NODE *d);//대기된 loopback 패킷들을 배출해준다
extern int LoopSendto(VOICE_DATA_NODE *d,const char *buf,int len,const struct sockaddr *to,int tolen);//음성데이타를 보낸다
extern int LoopRecvfrom(VOICE_DATA_NODE *d,char *buf,int len,struct sockaddr *from,int *fromlen);//음성데이타를 받는다
extern int LoopSamples(VOICE_DATA_NODE *d);//패킷에 있는 샘플의 수를 리턴하라. 만일 대기된것이 하나도 없다면 zero를 리턴하라
extern bool LoopControlport(VOICE_DATA_NODE *d);//loopback패킷을 대기시키고 control포트가 목적지라면 true를 리턴하라

//extern unsigned long WINAPI NetworkThread(void *arg);//네트워크 READ/WRITE 쓰레드
extern void NetworkThread(void *arg);//네트워크 READ/WRITE 쓰레드
extern unsigned long WINAPI Accept(void *arg);//상대방에 접속을 확인하기 위한 쓰레드 루틴
//extern unsigned long __stdcall WindowHandleThread(void *Arg);//윈도우 메시지를 받기위한 핸들 쓰레드(음성 데이타 이벤트가 발생하면 쓰레드로 메시지를 받는다)
extern void WindowHandleThread(void *Arg);//윈도우 메시지를 받기위한 핸들 쓰레드(음성 데이타 이벤트가 발생하면 쓰레드로 메시지를 받는다)
extern unsigned long __stdcall VoiceThread(void *Arg);//음성파일읽기쓰레드

extern VOICE_DATA_NODE VoiceInfo;      		//음성데이타
#endif//__VOICE_TOOL_KIT_H

/*-----------------Compile Option---------------------------
vtk.lib:VoiceTookEngine Library
winmm.lib:LowLevel media Libaray(media Api library)
wsock32.lib:Winsock Library(winsock 32bit library)
ws2_32.lib:Winsock Library(윈속2)등을 추가하여 컴파일 시켜야한다
-----------------------------------------------------------*/
