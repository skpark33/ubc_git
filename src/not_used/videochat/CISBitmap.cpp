// CISBitmap.cpp: implementation of the CCISBitmap class.
// Author:	Paul Reynolds 
// Date:	24/04/1998
// Version:	1.0
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include "CISBitmap.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCISBitmap::CCISBitmap()
{
	m_crBlack = 0;
	m_crWhite = RGB(255,255,255);
}

CCISBitmap::~CCISBitmap()
{

}

int CCISBitmap::Width()
{
	BITMAP bm;
	GetBitmap(&bm);
	return bm.bmWidth;
}

int CCISBitmap::Height()
{
	BITMAP bm;
	GetBitmap(&bm);
	return bm.bmHeight;
}

void CCISBitmap::DrawTransparent(CDC * pDC, int x, int y, COLORREF crColour)
{
	COLORREF crOldBack = pDC->SetBkColor(m_crWhite);
	COLORREF crOldText = pDC->SetTextColor(m_crBlack);
	CDC dcImage, dcTrans;

	// Create two memory dcs for the image and the mask
	dcImage.CreateCompatibleDC(pDC);
	dcTrans.CreateCompatibleDC(pDC);

	// Select the image into the appropriate dc
	CBitmap* pOldBitmapImage = dcImage.SelectObject(this);

	// Create the mask bitmap
	CBitmap bitmapTrans;
	int nWidth = Width();
	int nHeight = Height();
	bitmapTrans.CreateBitmap(nWidth, nHeight, 1, 1, NULL);

	// Select the mask bitmap into the appropriate dc
	CBitmap* pOldBitmapTrans = dcTrans.SelectObject(&bitmapTrans);

	// Build mask based on transparent colour
	dcImage.SetBkColor(crColour);
	dcTrans.BitBlt(0, 0, nWidth, nHeight, &dcImage, 0, 0, SRCCOPY);

	// Do the work - True Mask method - cool if not actual display
	pDC->BitBlt(x, y, nWidth, nHeight, &dcImage, 0, 0, SRCINVERT);
	pDC->BitBlt(x, y, nWidth, nHeight, &dcTrans, 0, 0, SRCAND);
	pDC->BitBlt(x, y, nWidth, nHeight, &dcImage, 0, 0, SRCINVERT);

	// Restore settings
	dcImage.SelectObject(pOldBitmapImage);
	dcTrans.SelectObject(pOldBitmapTrans);
	pDC->SetBkColor(crOldBack);
	pDC->SetTextColor(crOldText);
    bitmapTrans.DeleteObject(); 
}


void CCISBitmap::DrawTransparent(CDC * pDC, int x, int y, int width, int height, COLORREF crColour)
{
	COLORREF crOldBack = pDC->SetBkColor(m_crWhite);
	COLORREF crOldText = pDC->SetTextColor(m_crBlack);
	CDC dcImage, dcTrans;

	// Create two memory dcs for the image and the mask
	dcImage.CreateCompatibleDC(pDC);
	dcTrans.CreateCompatibleDC(pDC);

	// Select the image into the appropriate dc
	CBitmap* pOldBitmapImage = dcImage.SelectObject(this);

	// Create the mask bitmap
	CBitmap bitmapTrans;
	int nWidth = Width();
	int nHeight = Height();
	bitmapTrans.CreateBitmap(width, height, 1, 1, NULL);

	// Select the mask bitmap into the appropriate dc
	CBitmap* pOldBitmapTrans = dcTrans.SelectObject(&bitmapTrans);

	// Build mask based on transparent colour
	dcImage.SetBkColor(crColour);
	dcTrans.StretchBlt(0, 0, width, height, &dcImage, 0, 0, nWidth, nHeight, SRCCOPY);

	// Do the work - True Mask method - cool if not actual display
	pDC->StretchBlt(x, y, width, height, &dcImage, 0, 0, nWidth, nHeight, SRCINVERT);
	pDC->BitBlt(x, y, width, height, &dcTrans, 0, 0, SRCAND);
	pDC->StretchBlt(x, y, width, height, &dcImage, 0, 0, nWidth, nHeight, SRCINVERT);

	// Restore settings
	dcImage.SelectObject(pOldBitmapImage);
	dcTrans.SelectObject(pOldBitmapTrans);
	pDC->SetBkColor(crOldBack);
	pDC->SetTextColor(crOldText);
    bitmapTrans.DeleteObject(); 
}


void CCISBitmap::DrawTransparent2(CDC * pDC, int x, int y, COLORREF crColor)
{
	BITMAP bm;
	GetObject (sizeof (BITMAP), &bm);
	CPoint size (bm.bmWidth, bm.bmHeight);
	pDC->DPtoLP (&size);

    	CPoint org (0, 0);
	pDC->DPtoLP (&org);

    // Create a memory DC (dcImage) and select the bitmap into it
	CDC dcImage;
	dcImage.CreateCompatibleDC (pDC);
	CBitmap* pOldBitmapImage = dcImage.SelectObject (this);
	dcImage.SetMapMode (pDC->GetMapMode ());

    // Create a second memory DC (dcAnd) and in it create an AND mask
	CDC dcAnd;
	dcAnd.CreateCompatibleDC (pDC);
	dcAnd.SetMapMode (pDC->GetMapMode ());

    CBitmap bitmapAnd;
	bitmapAnd.CreateBitmap (bm.bmWidth, bm.bmHeight, 1, 1, NULL);
	CBitmap* pOldBitmapAnd = dcAnd.SelectObject (&bitmapAnd);

    dcImage.SetBkColor (crColor);
	dcAnd.BitBlt (org.x, org.y, size.x, size.y, &dcImage, org.x, org.y,
	    SRCCOPY);

    // Create a third memory DC (dcXor) and in it create an XOR mask
	CDC dcXor;
	dcXor.CreateCompatibleDC (pDC);
	dcXor.SetMapMode (pDC->GetMapMode ());

    CBitmap bitmapXor;
	bitmapXor.CreateCompatibleBitmap (&dcImage, bm.bmWidth, bm.bmHeight);
	CBitmap* pOldBitmapXor = dcXor.SelectObject (&bitmapXor);

    dcXor.BitBlt (org.x, org.y, size.x, size.y, &dcImage, org.x, org.y, SRCCOPY);

    dcXor.BitBlt (org.x, org.y, size.x, size.y, &dcAnd, org.x, org.y, 0x220326);

    // Copy the pixels in the destination rectangle to a temporary
	// memory DC (dcTemp)
	CDC dcTemp;
	dcTemp.CreateCompatibleDC (pDC);
	dcTemp.SetMapMode (pDC->GetMapMode ());

    CBitmap bitmapTemp;
	bitmapTemp.CreateCompatibleBitmap (&dcImage, bm.bmWidth, bm.bmHeight);
	CBitmap* pOldBitmapTemp = dcTemp.SelectObject (&bitmapTemp);

    dcTemp.BitBlt (org.x, org.y, size.x, size.y, pDC, x, y, SRCCOPY);

    // Generate the final image by applying the AND and XOR masks to
	// the image in the temporary memory DC
	dcTemp.BitBlt (org.x, org.y, size.x, size.y, &dcAnd, org.x, org.y, SRCAND);

    dcTemp.BitBlt (org.x, org.y, size.x, size.y, &dcXor, org.x, org.y, SRCINVERT);

    // Blit the resulting image to the screen
	pDC->BitBlt (x, y, size.x, size.y, &dcTemp, org.x, org.y, SRCCOPY);

    // Restore the default bitmaps
	dcTemp.SelectObject (pOldBitmapTemp);
	dcXor.SelectObject (pOldBitmapXor);
	dcAnd.SelectObject (pOldBitmapAnd);
	dcImage.SelectObject (pOldBitmapImage);

}