#if !defined(AFX_CLONEFRAMEWND_H__56159718_9AC1_4648_8FBD_3DC975985C5E__INCLUDED_)
#define AFX_CLONEFRAMEWND_H__56159718_9AC1_4648_8FBD_3DC975985C5E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CloneFrameWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCloneFrameWnd dialog
#include "FrameViewer.h"
#include "resource.h"

#define	REFRESH_TIMER	70

class CCloneFrameWnd : public CDialog
{
// Construction
public:
   	void SetFrameViewerPtr( CFrameViewer* pFrmView);

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CFrameViewer* m_pFrameViewer;

	CCloneFrameWnd(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCloneFrameWnd)
	enum { IDD = IDD_CLONEFRM_WND };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCloneFrameWnd)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	// Generated message map functions
	//{{AFX_MSG(CCloneFrameWnd)
		// NOTE: the ClassWizard will add member functions here
    virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnNcPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


	UINT nRegMsg3;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLONEFRAMEWND_H__56159718_9AC1_4648_8FBD_3DC975985C5E__INCLUDED_)
