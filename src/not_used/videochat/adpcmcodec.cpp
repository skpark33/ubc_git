//Adpcm Codec C++ Routines
//Source Convert Kim jae hong
#include "stdafx.h"
#include "main.h"
/*
*adpcm:
-앞 샘플링 값과의 차이를 4비트로 
표시하는 방식.
-32kbps의 전송속도로 만들어지며 
G.721이라는 이름으로도 알려짐.
-g721의 기능을 확장한 것으로 
64kbps의 전송속도를 감안한 것
이어서 더 좋은 음질을 냄.
ima에서 독립적으로 발표한 
adpcm알고리즘도 널리 사용됨. 
Adaptive Differential Pulse Code Modulation(ADPCM)은 음성신호에 대한 디지털 코덱으로 가장 널리 이용되고 있는 PCM의 
신호를 압축/복원하는 기술이다. ITU-T G.721 ADPCM 코덱은 64Kbps PCM 신호의 음질은 그대로 유지하면서, 데이터를 1/2 
압축하여 32Kbps의 데이터 전송율을 갖는다. ADPCM 코덱은 앞서 입력된 PCM 데이터를 근거로 다음에 입력될 PCM 데이터를 예측하기 위해 필터를 사용한다. 예측한 PCM 데이터와 실제 입력된 PCM 데이터와의 오차값이 ADPCM의 데이터 정보가 된다. 
본 SSTG721-1은 ITU-T G.721 32Kbps ADPCM 표준 권고안을 만족하는 코덱 칩이다. 상용화된 표준 PCM 코덱과의 인터페이
스를 용이하게 하기 위해 직렬로 데이터를 입출력한다. 본 제품은 디지털 무선전화기, 음성우편 시스템 및 음성녹음/재생기 등의 
제품에 적용이 가능하다
32kbps 32,000x8비트=4000k  1초에 4키로 전송
4000k
초당 4K전송
*/
static const int indexTable[16]=
{
	-1, -1, -1, -1, 2, 4, 6, 8,
	-1, -1, -1, -1, 2, 4, 6, 8,
};
                                                      
static const int stepsizeTable[89]=
{
	7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
	19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
	50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
	130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
	337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
	876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
	2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
	5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
	15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
};

//ADPCM을 사용한 사운드 버퍼의 내용을 압축한다
void AdpcmSoundCoder(ADPCMBuffer *sb)
{
	unsigned char *dp=(unsigned char*)sb->buffer.buffer_val;
	static _ADPCM_State istate;
	static _ADPCM_State adpcm;
	istate=adpcm;
	AdpcmCoder(dp,(char*)dp,sb->buffer.buffer_len,(ADPCMState*)&adpcm);
	sb->buffer.buffer_len/=2;
	dp+=sb->buffer.buffer_len;
	*dp++=((unsigned int)istate.ValuePrevious)>>8;
	*dp++=istate.ValuePrevious&0xFF;
	*dp=istate.index;
	sb->buffer.buffer_len+=3;
}

//ADPCM을 압축을 푼다 
void AdpcmSoundDecoder(ADPCMBuffer *sb)
{
	char *dp=(char*)sb->buffer.buffer_val;
	unsigned char *sp;
	static unsigned char dob[512*2];
	struct _ADPCM_State adpcm;
	sp=(unsigned char*)dp+(sb->buffer.buffer_len-3);
	adpcm.ValuePrevious=(short)((((int)sp[0])<<8)|((int)sp[1]));
	adpcm.index=sp[2];
	sb->buffer.buffer_len-=3;
	AdpcmDecoder(dp,dob,(int)(sb->buffer.buffer_len*2),&adpcm);
	sb->buffer.buffer_len*=2;
	memcpy(dp,dob,(size_t)sb->buffer.buffer_len);
}

void AdpcmCoder( unsigned char indata[], char outdata[], int len, ADPCMState* state )
{
	unsigned char *inp; 		/* Input buffer pointer */
	signed char *outp;			/* output buffer pointer */
	int val;					/* Current input sample value */
	int sign;					/* Current adpcm sign bit */
	int delta;					/* Current adpcm output value */
	long diff;					/* Difference between val and valprev */
	int step;					/* Stepsize */
	long valpred;				/* Predicted output value */
	int vpdiff; 				/* Current change to valpred */
	int index;					/* Current step change index */
	int outputbuffer;			/* place to keep previous 4-bit value */
	int bufferstep; 			/* toggle between outputbuffer/output */

	outp = (signed char *)outdata;
	inp = indata;

	valpred = state->ValuePrevious;
	index = state->index;
	step = stepsizeTable[index];
	
	bufferstep = 1;

	for ( ; len > 0 ; len-- ) {
		val = audio_u2s(*inp++);

		/* Step 1 - compute difference with previous value */
		diff = val - valpred;
		sign = (diff < 0) ? 8 : 0;
		if ( sign ) diff = (-diff);

		/* Step 2 - Divide and clamp */
		/* Note:
		** This code *approximately* computes:
		**	  delta = diff*4/d;
		**	  vpdiff = (delta+0.5)*step/4;
		** but in shift step bits are dropped. The net result of this is
		** that even if you have fast mul/div hardware you cannot put it to
		** good use since the fixup would be too expensive.
		*/
		delta = 0;
		vpdiff = (step >> 3);
		
		if ( diff >= step ) {
			delta = 4;
			diff -= step;
			vpdiff += step;
		}
		step >>= 1;
		if ( diff >= step  ) {
			delta |= 2;
			diff -= step;
			vpdiff += step;
		}
		step >>= 1;
		if ( diff >= step ) {
			delta |= 1;
			vpdiff += step;
		}

		/* Step 3 - Update previous value */
		if ( sign )
		  valpred -= vpdiff;
		else
		  valpred += vpdiff;

		/* Step 4 - Clamp previous value to 16 bits */
		if ( valpred > 32767L )
		  valpred = 32767L;
		else if ( valpred < -32768L )
		  valpred = -32768L;

		/* Step 5 - Assemble value, update index and step values */
		delta |= sign;
		
		index += indexTable[delta];
		if ( index < 0 ) index = 0;
		if ( index > 88 ) index = 88;
		step = stepsizeTable[index];

		/* Step 6 - Output value */
		if ( bufferstep ) {
			outputbuffer = (delta << 4) & 0xf0;
		} else {
			*outp++ = (delta & 0x0f) | outputbuffer;
		}
		bufferstep = !bufferstep;
	}

	/* Output last step, if needed */
	if ( !bufferstep )
	  *outp++ = outputbuffer;
	
	state->ValuePrevious = (short) valpred;
	state->index = index;
}

void AdpcmDecoder( char indata[], unsigned char outdata[],int len, ADPCMState *state )
{
	signed char *inp;			/* Input buffer pointer */
	unsigned char *outp;		/* Output buffer pointer */
	int sign;					/* Current adpcm sign bit */
	int delta;					/* Current adpcm output value */
	int step;					/* Stepsize */
	long valpred;				/* Predicted value */
	int vpdiff; 				/* Current change to valpred */
	int index;					/* Current step change index */
	int inputbuffer;			/* place to keep next 4-bit value */
	int bufferstep; 			/* toggle between inputbuffer/input */

	outp = outdata;
	inp = (signed char *)indata;

	valpred = state->ValuePrevious;
	index = state->index;
	step = stepsizeTable[index];

	bufferstep = 0;
	
	for ( ; len > 0 ; len-- ) {
		
		/* Step 1 - get the delta value */
		if ( bufferstep ) {
			delta = inputbuffer & 0xf;
		} else {
			inputbuffer = *inp++;
			delta = (inputbuffer >> 4) & 0xf;
		}
		bufferstep = !bufferstep;

		/* Step 2 - Find new index value (for later) */
		index += indexTable[delta];
		if ( index < 0 ) index = 0;
		if ( index > 88 ) index = 88;

		/* Step 3 - Separate sign and magnitude */
		sign = delta & 8;
		delta = delta & 7;

		/* Step 4 - Compute difference and new predicted value */
		/*
        ** Computes 'vpdiff = (delta+0.5)*step/4', but see comment
		** in adpcm_coder.
		*/
		vpdiff = step >> 3;
		if ( delta & 4 ) vpdiff += step;
		if ( delta & 2 ) vpdiff += step>>1;
		if ( delta & 1 ) vpdiff += step>>2;

		if ( sign )
		  valpred -= vpdiff;
		else
		  valpred += vpdiff;

		/* Step 5 - clamp output value */
		if ( valpred > 32767L )
		  valpred = 32767L;
		else if ( valpred < -32768L )
		  valpred = -32768L;

		/* Step 6 - Update step value */
		step = stepsizeTable[index];

		/* Step 7 - Output value */
		*outp++ = audio_s2u(valpred);
	}

	state->ValuePrevious = (short) valpred;
	state->index = index;
}