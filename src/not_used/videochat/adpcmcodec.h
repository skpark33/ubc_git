//Adpcm Codec HeaderFiles
typedef struct _ADPCM_State 
{
  short ValuePrevious;//Previous output value 
  char index;//Index into stepsize table 
}ADPCMState,*PADPCMState;

extern void AdpcmSoundCoder(ADPCMBuffer *sb);//사운드버퍼압축
extern void AdpcmSoundDecoder(ADPCMBuffer *sb);//사운드버퍼해제
extern void AdpcmCoder(unsigned char indata[],char outdata[],int len,ADPCMState *state);//압축
extern void AdpcmDecoder(char indata[],unsigned char outdata[],int len,ADPCMState *state);//해제
