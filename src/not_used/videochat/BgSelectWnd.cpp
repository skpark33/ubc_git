// BgSelectWnd.cpp : implementation file
//

#include "stdafx.h"
#include "BgSelectWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBgSelectWnd dialog
CBgSelectWnd::CBgSelectWnd(CRect rect,bool& pExist, CWnd* pParent)
: CDialog(CBgSelectWnd::IDD, pParent) , m_existBackGroundDialog (pExist) 
{
	//{{AFX_DATA_INIT(CChooseFrame)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    m_btnRect = rect;
    m_pWnd = pParent;
}



void CBgSelectWnd::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBgSelectWnd)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_BTN_BG0, m_BG0);
	DDX_Control(pDX, IDC_BTN_BG1, m_BG1);
	DDX_Control(pDX, IDC_BTN_BG2, m_BG2);
	DDX_Control(pDX, IDC_BTN_BG3, m_BG3);
	DDX_Control(pDX, IDC_BTN_BG4, m_BG4);
	DDX_Control(pDX, IDC_BTN_BG5, m_BG5);
	DDX_Control(pDX, IDC_BTN_BG6, m_BG6);
	DDX_Control(pDX, IDC_BTN_BG7, m_BG7);
	DDX_Control(pDX, IDC_BTN_BG8, m_BG8);
	DDX_Control(pDX, IDC_BTN_BG9, m_BG9);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBgSelectWnd, CDialog)
	//{{AFX_MSG_MAP(CBgSelectWnd)
	ON_WM_CTLCOLOR()
	ON_WM_NCDESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER+10, OnHookMsg)
	ON_CONTROL_RANGE(BN_CLICKED,IDC_BTN_BG0,IDC_BTN_BG9,OnButtonClick)
	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBgSelectWnd message handlers

BOOL CBgSelectWnd::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText("BackGround_Dialog");

	m_brush.CreateSolidBrush(RGB(255, 255, 255));
	
	//Buttpon Setting
	m_BG0.SetBitmap(IDB_BMP_BG0, RGB(255,0,0));
	m_BG1.SetBitmap(IDB_BMP_BG1, RGB(255,0,0));
	m_BG2.SetBitmap(IDB_BMP_BG2, RGB(255,0,0));
	m_BG3.SetBitmap(IDB_BMP_BG3, RGB(255,0,0));
	m_BG4.SetBitmap(IDB_BMP_BG4, RGB(255,0,0));
	m_BG5.SetBitmap(IDB_BMP_BG5, RGB(255,0,0));
	m_BG6.SetBitmap(IDB_BMP_BG6, RGB(255,0,0));
	m_BG7.SetBitmap(IDB_BMP_BG7, RGB(255,0,0));
	m_BG8.SetBitmap(IDB_BMP_BG8, RGB(255,0,0));
	m_BG9.SetBitmap(IDB_BMP_BG9, RGB(255,0,0));

	//Hooking Start!
	OnHook();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBgSelectWnd::OnButtonClick( UINT ID )
{

    int clickedBtn = ID - IDC_BTN_BG0;

    int bitmapID  = 0;
	switch( clickedBtn ){
	case 0 :
		bitmapID = 0;
		break;
	case 1 :
		bitmapID = IDB_BMP_BG1 ;
		break;
	case 2 :
		bitmapID = IDB_BMP_BG2 ;
		break;
	case 3 :
        bitmapID = IDB_BMP_BG3 ;
		break;
	case 4 :
        bitmapID = IDB_BMP_BG4 ;
		break;
	case 5 :
        bitmapID = IDB_BMP_BG5 ;
		break;
	case 6 :
		bitmapID = IDB_BMP_BG6 ;
		break;
	case 7 :
        bitmapID = IDB_BMP_BG7 ;
		break;
	case 8 :
        bitmapID = IDB_BMP_BG8 ;
		break;
	case 9 :
        bitmapID = IDB_BMP_BG9 ; 
		break;
	default:
		break;
	}

    m_pWnd->PostMessage( WM_SELECT_VIDEO_BG, (WPARAM )bitmapID,0);

/*
	CCapBoyWnd *wnd = ((CCapBoyWnd *)GetParent());	//Button owner

	int clickedBtn = ID - IDC_BTN_BG0;
	switch( clickedBtn ){
	case 0 :
		wnd->m_myView.SetUseBackImage(FALSE);
		break;
	case 1 :
		//m_SelectImg = 1;
		wnd->m_myView.SetBGImg(IDB_BMP_BG1 , RGB(255,255,255));
		break;
	case 2 :
		//m_SelectImg = 2;
		wnd->m_myView.SetBGImg(IDB_BMP_BG2 , RGB(255,255,255));
		break;
	case 3 :
		//m_SelectImg = 3;
		wnd->m_myView.SetBGImg(IDB_BMP_BG3 , RGB(255,255,255));
		break;
	case 4 :
		//m_SelectImg = 4;
		wnd->m_myView.SetBGImg(IDB_BMP_BG4 , RGB(255,255,255));
		break;
	case 5 :
		//m_SelectImg = 5;
		wnd->m_myView.SetBGImg(IDB_BMP_BG5 , RGB(255,255,255));
		break;
	case 6 :
		//m_SelectImg = 6;
		wnd->m_myView.SetBGImg(IDB_BMP_BG6 , RGB(255,255,255));
		break;
	case 7 :
		//m_SelectImg = 7;
		wnd->m_myView.SetBGImg(IDB_BMP_BG7 , RGB(255,255,255));
		break;
	case 8 :
		//m_SelectImg = 8;
		wnd->m_myView.SetBGImg(IDB_BMP_BG8 , RGB(255,255,255));
		break;
	case 9 :
		//m_SelectImg = 8;
		wnd->m_myView.SetBGImg(IDB_BMP_BG9 , RGB(255,255,255));
		break;
	default:
		break;

	}
*/

	//Button Click
	OnHookStop();	//Hooking Stop!
    m_existBackGroundDialog = FALSE;

	this->DestroyWindow();
}

HBRUSH CBgSelectWnd::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
//	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
//	return hbr;
	return m_brush;
}

void CBgSelectWnd::OnNcDestroy() 
{
	CDialog::OnNcDestroy();

	m_existBackGroundDialog = FALSE;
	delete this;
}

void CBgSelectWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
}

//====================== Hooking ==============================//

HHOOK hAVHook = NULL;

LRESULT CALLBACK AVHookMouseProc(int nCode , WPARAM wParam, LPARAM lParam)
{
	//Hooking Method -- when Mouse Event happened ..
	LRESULT lResult = 1;
	HWND hWnd;

	if(nCode == HC_ACTION)
	{
		if(((PEVENTMSG) lParam)->message == WM_LBUTTONDOWN  )
		{
			
			hWnd = FindWindow(NULL,"BackGround_Dialog");
			POINT point;		//현 마우스 포인터			
			GetCursorPos(&point);

			RECT rect;			//EmoticonDetail Rect
			GetWindowRect(hWnd,&rect);

			if ( rect.left <= point.x && rect.right >= point.x && rect.top <= point.y && rect.bottom >= point.y )
			{
				//Mouse Pointer 가 EmoticonDetail 의 Button 을 가리킬때...
				return CallNextHookEx(hAVHook,nCode,wParam,lParam);
			}

			SendMessage(hWnd,WM_USER+10,2,2);
			return lResult;
			
		}
	}
	return CallNextHookEx(hAVHook,nCode,wParam,lParam);
}

void CBgSelectWnd::OnHook()
{
	//Hooking Start
	hAVHook = SetWindowsHookEx(WH_JOURNALRECORD,AVHookMouseProc,GetModuleHandle(NULL),0); //CALLBACK 함수가 먼저 위에 정의 되어야한다.
}

void CBgSelectWnd::OnHookStop()
{
	//Hooking Stop
	if (hAVHook)
	{
		UnhookWindowsHookEx(hAVHook);
		hAVHook=NULL;
	}
}


//고쳐질 필요가 있다..

// skpark void --> LRESULT
//void CBgSelectWnd::OnHookMsg( WPARAM wparam, LPARAM lParam)
LRESULT CBgSelectWnd::OnHookMsg( WPARAM wparam, LPARAM lParam)
{

	POINT point;	//Mouse Posision when mouse's clicked


	//Mouse Position Setting
	GetCursorPos(&point);
	//check  Mouse position against the Button Position
	if( m_btnRect.left <= point.x && m_btnRect.right >= point.x && 
                m_btnRect.top <= point.y && m_btnRect.bottom >= point.y )
	{
		//Mouse's clicked in Button
        m_existBackGroundDialog = TRUE;
	}
	else 
        m_existBackGroundDialog = FALSE;


	OnHookStop();

	this->DestroyWindow();

	// skpark add 
	return 0;

}
