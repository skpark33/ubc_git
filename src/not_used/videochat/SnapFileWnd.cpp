// SnapFileWnd.cpp : implementation file
//

#include "stdafx.h"
#include "SnapFileWnd.h"
#include "VideoView.h"
#include "VideoViewDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSnapFileWnd dialog


CSnapFileWnd::CSnapFileWnd(CWnd* pParent /*=NULL*/)
	: CDialog(CSnapFileWnd::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSnapFileWnd)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSnapFileWnd::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSnapFileWnd)
	DDX_Control(pDX, IDC_ST_SNAPVIVEW, m_snapView);
	DDX_Control(pDX, IDC_ST_SNAP_FILEPATH, m_stFilePath);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSnapFileWnd, CDialog)
	//{{AFX_MSG_MAP(CSnapFileWnd)
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_ST_SNAP_FILEPATH, OnStSnapFilepath)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSnapFileWnd message handlers

void CSnapFileWnd::OnOK() 
{
    DestroyWindow();	

//	CDialog::OnOK();
}

void CSnapFileWnd::OnCancel() 
{
    DestroyWindow();	

//	CDialog::OnCancel();
}

BOOL CSnapFileWnd::OnInitDialog() 
{
    CDialog::OnInitDialog();

    m_snapView.SetSize( 160,120);
    
    //밑줄~
   	LOGFONT m_LogFont={-12,0,0,0,400,0,1,0,129,3,2,1,50,"굴림"};   


	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CSnapFileWnd::OnEraseBkgnd(CDC* pDC) 
{
    DrawBG( pDC);
    return TRUE;

	return CDialog::OnEraseBkgnd(pDC);
}


void CSnapFileWnd::DrawBG(CDC *pDC)
{
    CRect rcClip;
	pDC->GetClipBox(&rcClip);

	CDC dc;
	dc.CreateCompatibleDC(pDC);
	dc.OffsetViewportOrg(-rcClip.left, -rcClip.top);
	dc.SetBrushOrg(rcClip.left % 8, rcClip.top % 8);
	CBitmap bitmap, *pOldBitmap;
	bitmap.CreateCompatibleBitmap(pDC, rcClip.Width(), rcClip.Height());
	pOldBitmap = dc.SelectObject(&bitmap);

	CRect rect;
	GetClientRect(&rect);

    COLORREF clrMainBG = RGB( 255,255,255);

   	//main BG//
	dc.FillSolidRect(0, 0, rect.Width(), rect.Height(), clrMainBG);
    	
	dc.SetViewportOrg(0, 0);
	pDC->BitBlt(rcClip.left, rcClip.top, rcClip.Width(), rcClip.Height(), &dc, 0, 0, SRCCOPY);
	dc.SelectObject(pOldBitmap);
	dc.DeleteDC();
}

void nopath(char *to, const char *from)

{
	const char *s = from;
	if (s[strlen(s) - 1] == '\\') 
{
		to[0] = 0;
		return;
	}
	if (!strchr(s, '\\'))
	{
		while (*s) 
		*to++ = *(s++);
		*to = 0;
		return;
	}
	while (*s) s++;
	while (*s != '\\') s--;
	s++;
	while (*s) 
	*to++ = *(s++);
	*to = 0;
}


void CSnapFileWnd::LoadFile(CString strFile)
{
    m_snapView.Load( strFile);

    char fullPath[256]="", fileName[128]="";
    strcpy( fullPath , strFile);

    nopath(fileName, fullPath);

    m_stFilePath.SetWindowText(fileName);
    Invalidate();
}

//실행경로 열기..
void CSnapFileWnd::OnStSnapFilepath() 
{
    //JPG로 저장해보자꾸나..//
	CString dataPath ;
	dataPath.Format("%s\\data", ((CVideoViewDlg*)GetParent())->GetExecPath());

    HINSTANCE hInst;
	hInst = ShellExecute(0, "explore",dataPath , "", "", SW_RESTORE);
}

void CSnapFileWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{

    PostMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));
	CDialog::OnLButtonDown(nFlags, point);
}

void CSnapFileWnd::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CDialog::OnLButtonDblClk(nFlags, point);
    OnOK();
}
