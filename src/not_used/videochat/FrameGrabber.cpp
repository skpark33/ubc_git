// CFrameGrabber.cpp : implementation file
//
// (c) Vadim Gorbatenko, 1999 
// gvv@mail.tomsknet.ru
// All rights reserved
//
// CFrameGrabber window class
// Release: 1199

#include "stdafx.h"
#include "FrameGrabber.h"

#define	DEFAULT_CAPTURE_DRIVER	0 //in most cases

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "vfw32.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef LRESULT (CALLBACK *FRAMECALLBACK)(HWND , LPVIDEOHDR);
#define	IMAGEWIDTH(lpd) ((LPBITMAPINFOHEADER)lpd)->biWidth
#define	IMAGEHEIGHT(lpd) ((LPBITMAPINFOHEADER)lpd)->biHeight
#define	IMAGEBITS(lpd) ((LPBITMAPINFOHEADER)lpd)->biBitCount

LRESULT FAR PASCAL _grabber_CallbackProc(HWND hWnd, LPVIDEOHDR lpVHdr);
LRESULT CALLBACK capVideoStreamCallback( HWND hWnd,  LPVIDEOHDR lpVHdr ) ;

CFrameGrabber	*theOnlyOneGrabber = NULL;
/////////////////////////////////////////////////////////////////////////////
// CFrameGrabber

//CFrameGrabber::CFrameGrabber(int x, int y)
CFrameGrabber::CFrameGrabber( CWnd * pWnd)	//CapBoyWnd..
{
	dwLastCallback = NULL;
	imageData = NULL;
	vfs = 0;//offset image buffer

	m_pCapBoyWnd = pWnd ;
/*
    m_X = x;
    m_Y = y;
*/

	checkTime =0;
	nTotalCnt =0;
}

CFrameGrabber::~CFrameGrabber()
{

    OnDestroy(); 


	if(imageData)
	{	
        HGLOBAL hg = GlobalHandle(imageData);
		GlobalUnlock(hg);
		GlobalFree(hg);
    }
}


BEGIN_MESSAGE_MAP(CFrameGrabber, CWnd)
	//{{AFX_MSG_MAP(CFrameGrabber)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CFrameGrabber message handlers
// create unvisible child window at (x,y)
BOOL CFrameGrabber::Create(int x, int y, CWnd *pParentWnd)
{
	if(theOnlyOneGrabber)
		return FALSE;

	ASSERT(!GetSafeHwnd());
	
	if(GetSafeHwnd()) 
    {
		return FALSE;		//already created
    }
	
	m_capwnd=capCreateCaptureWindow(_T("AviCap_FrameGrabber_lite"),
//        							WS_CHILD|WS_CLIPSIBLINGS, m_X, m_Y, 160, 120, 
        							WS_CHILD|WS_VISIBLE, x, y, 1, 1, 
//        							WS_CHILD, x, y, 160, 120, 
									pParentWnd?pParentWnd->GetSafeHwnd():NULL, 0xffff);


	if(!m_capwnd)
		return FALSE;

	BOOL ret = InitialCaptureStream() ;
//    BOOL ret = InitCapture();
    if( !ret) 
    {
        ::DestroyWindow(m_capwnd);
		return FALSE;
    }

/*
	//initial setup for capture
	if( !capDriverConnect(m_capwnd, DEFAULT_CAPTURE_DRIVER)||
		!capSetCallbackOnFrame(m_capwnd,(LPVOID) _grabber_CallbackProc)||
        !capSetCallbackOnVideoStream(m_capwnd,(LPVOID)capVideoStreamCallback) ||
            !capSetCallbackOnYield(GetSafeHwnd(), (LPVOID) _grabber_CallbackProc) || 
        !capPreviewRate(m_capwnd, 30) ||    //초당 16프레임으로 preview//
		!capPreview(m_capwnd, TRUE))
	
	{::DestroyWindow(m_capwnd);
		return FALSE;}

    CAPTUREPARMS s; //녹화하는 비율 설정. 
    capCaptureGetSetup(m_capwnd, &s ,sizeof(CAPTUREPARMS));
    s.dwRequestMicroSecPerFrame = 66667 ;   //15프레임////    s.fYield  = TRUE;
	s.fCaptureAudio = FALSE;
    s.wStepCaptureAverageFrames = 15;
    capCaptureSetSetup(m_capwnd, &s ,sizeof(CAPTUREPARMS));
*/

//    SetUseCaptureAudio(TRUE);    //화상캡쳐시 음성사용.

	//subclass window
	SubclassWindow(m_capwnd);
  

	update_buffer_size();
	if(!imageData)
	{
        ::DestroyWindow(m_capwnd);
        theOnlyOneGrabber = NULL;
		return FALSE;
    }
	theOnlyOneGrabber = this;
	return TRUE;
}

void CFrameGrabber::OnDestroy() 
{
	//disconnect from capture driver
	if(theOnlyOneGrabber) {
		capSetCallbackOnFrame(m_capwnd, NULL);
		capDriverDisconnect(m_capwnd);
	}

	CWnd::OnDestroy();
	theOnlyOneGrabber = NULL;
}

BOOL	CFrameGrabber::VideoFormatDialog()
{

	//CapStream을 멈추고.. GrabFrame..으로..
	StopCaptureStream();
	ResumeCaptureFrame();

	MSG msg;
    CTime time(CTime::GetCurrentTime());
	do	
	{
		if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
        }else {
			Sleep(5);	//	Don't use CPU time
        }
	}while((CTime::GetCurrentTime() - time).GetTotalSeconds() < 2);


	BOOL r = FALSE;
	CAPDRIVERCAPS caps;
	capDriverGetCaps(m_capwnd ,  &caps , sizeof( CAPDRIVERCAPS));
	if(caps.fHasDlgVideoFormat) 
	{
		if(!GetSafeHwnd())	
			return FALSE;
		r = (BOOL)capDlgVideoFormat(m_capwnd);
		update_buffer_size();
	}
	else 
	{
		MessageBox("비디오 포멧창을 디바이스 드라이버에서 지원하지 않습니다.");
	}

	//GrabFrm을 멈추고.. CapStream..으로..
	StopCaptureFrame();
	ResumeCaptureStream();

	return r;
}

void CFrameGrabber::SetVideoFormat(CSize size)
{
    if(!GetSafeHwnd())	
        return ;

    BITMAPINFO bm;
    capGetVideoFormat(m_capwnd,  &bm, sizeof(BITMAPINFO)) ;
    bm.bmiHeader.biWidth = size.cx ;
    bm.bmiHeader.biHeight = size.cy;
    bm.bmiHeader.biSizeImage = size.cx * size.cy * 3;

    capSetVideoFormat(m_capwnd,  &bm, sizeof(BITMAPINFO)) ;

	update_buffer_size();
}


BOOL	CFrameGrabber::VideoSourceDialog()
{
	if(!GetSafeHwnd())	
		return FALSE;

	//CapStream을 멈추고 GrabFrame..
	StopCaptureStream();
	ResumeCaptureFrame();

	MSG msg;
    CTime time(CTime::GetCurrentTime());
	do	
	{
		if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
        }else {
			Sleep(5);	//	Don't use CPU time
        }
	}while((CTime::GetCurrentTime() - time).GetTotalSeconds() < 2);


	BOOL r = FALSE;
	CAPDRIVERCAPS caps;
	capDriverGetCaps(m_capwnd ,  &caps , sizeof( CAPDRIVERCAPS));
	if(caps.fHasDlgVideoSource) 
	{
		r =(BOOL)capDlgVideoSource(m_capwnd);
		update_buffer_size();
	}
	else 
	{
		MessageBox("비디오 소스창을 디바이스 드라이버에서 지원하지 않습니다.");
	}


	//GrabFrm을 멈추고.. CapStream..으로..
	StopCaptureFrame();
	ResumeCaptureStream();

	return r;
}

LPBITMAPINFO	CFrameGrabber::GetDIB()
{
	if(!imageData)	return NULL;
	//get the new one of more then 20 ms passed
	if((timeGetTime()-dwLastCallback)>20)
		capGrabFrameNoStop(m_capwnd);
	return (LPBITMAPINFO)imageData;
}

LPBYTE			CFrameGrabber::GetImageBitsBuffer()
{
	if(!imageData)	return NULL;
	//get the new one of more then 20 ms passed
	if((timeGetTime()-dwLastCallback)>20)
		capGrabFrameNoStop(m_capwnd);
	return ((LPBYTE)imageData)+vfs;
}

CSize		CFrameGrabber::GetImageSize()
{
	if(!imageData)	return CSize(0,0);
	else	return CSize(IMAGEWIDTH(imageData), IMAGEHEIGHT(imageData));
}

DWORD		CFrameGrabber::GetImageBitsResolution()
{
	if(!imageData)	return 0;
	else	return IMAGEBITS(imageData);
}


// this is internal use only!
BOOL	validCallHint=FALSE;
void	CFrameGrabber::SetImageData(LPVOID data)
{
	ASSERT(validCallHint);
	// do not call this method indirectly!
	if(!validCallHint)	return;
	if(!imageData)		return;
	CopyMemory(	((LPBYTE)imageData)+vfs,
				data, 
				IMAGEWIDTH(imageData)*IMAGEHEIGHT(imageData)*IMAGEBITS(imageData)/8);
	dwLastCallback	=  timeGetTime();
	if(IsWindowVisible())
		InvalidateRect(NULL);

	m_pCapBoyWnd->SendMessage(WM_CALLBACKMSG_CAPUTRE_IMAGE);

}

void	CFrameGrabber::SetImageData2(LPVOID data)
{
	ASSERT(validCallHint);
	// do not call this method indirectly!
	if(!validCallHint)	return;
	if(!imageData)		return;
	CopyMemory(	((LPBYTE)imageData)+vfs,
				data, 
				IMAGEWIDTH(imageData)*IMAGEHEIGHT(imageData)*IMAGEBITS(imageData)/8);
	dwLastCallback	=  timeGetTime();

	m_pCapBoyWnd->SendMessage(WM_CALLBACKMSG_CAPUTRE_IMAGE);
}


void	CFrameGrabber::update_buffer_size()
{
    
	vfs=capGetVideoFormatSize(m_capwnd);
		if(!vfs)
		{
			if(imageData)
				{HGLOBAL hg = GlobalHandle(imageData);
				GlobalUnlock(hg);
				GlobalFree(hg);}
				vfs = 0;
				imageData = NULL;
			return;		
		}

	DWORD	lastBufferSize=0;
	if(imageData)
		lastBufferSize = vfs +
		IMAGEWIDTH(imageData)*IMAGEHEIGHT(imageData)*IMAGEBITS(imageData)/8;
	
	DWORD newBufferSize;
	

	BITMAPINFO *lpBmpInfo =(BITMAPINFO	*)( new char[vfs]);
	
	((LPBITMAPINFOHEADER)lpBmpInfo)->biSize= sizeof BITMAPINFOHEADER;
	
	if(!capGetVideoFormat(m_capwnd, lpBmpInfo, (WORD)vfs))
	{
		delete lpBmpInfo;
		
		if(imageData)
			{HGLOBAL hg = GlobalHandle(imageData);
			GlobalUnlock(hg);
			GlobalFree(hg);}
			vfs = 0;
			imageData = NULL;
		return;		
	}

	newBufferSize = vfs + IMAGEWIDTH(lpBmpInfo)*IMAGEHEIGHT(lpBmpInfo)*IMAGEBITS(lpBmpInfo)/8;

// 필요할까? 
/*
	SetWindowPos(NULL,0,0,IMAGEWIDTH(lpBmpInfo),IMAGEHEIGHT(lpBmpInfo),
				SWP_NOMOVE|SWP_NOZORDER);
*/

	if(newBufferSize > lastBufferSize)
	{
		if(imageData)//reallocate
		{	HGLOBAL hg = GlobalHandle(imageData);
			GlobalUnlock(hg);
			GlobalFree(hg);}

		imageData = GlobalLock(GlobalAlloc(GMEM_MOVEABLE, newBufferSize+256/*add 256 bytes, just in case...*/));
	}

	memcpy(imageData, lpBmpInfo, vfs);
	
	delete lpBmpInfo;
}

//////////////////////////////////////////////////////////////////
LRESULT FAR PASCAL _grabber_CallbackProc(HWND hWnd, LPVIDEOHDR lpVHdr)
{

    static long nowTime = timeGetTime();
	static int cnt = 0;
/*
    CString str;
    str.Format("\n###Frame nowTime :[%d] - oldTime :%d = %d",timeGetTime(),nowTime, timeGetTime() - nowTime);
    OutputDebugString(str);
*/
    nowTime = timeGetTime();

	if( cnt ++ < 30) 
		return 0;

//	ASSERT_VALID(theOnlyOneGrabber);
	validCallHint = TRUE;
	theOnlyOneGrabber ->SetImageData(lpVHdr->lpData);
	validCallHint = FALSE;
	

	return 0;
}


LRESULT CALLBACK StreamCallbackProc( HWND hWnd,  LPVIDEOHDR lpVHdr ) 
{

    static long nowTime = timeGetTime();
	static int cnt = 0;

/*
    CString str;
    str.Format("\n$$$Stream nowTime :[%d] - oldTime :%d = %d",timeGetTime(),nowTime, timeGetTime() - nowTime);
    OutputDebugString(str);
*/
    nowTime = timeGetTime();

	if( cnt < 10) 
    {
        cnt++;
		return 0;
    }      

//	ASSERT_VALID(theOnlyOneGrabber);
	if( theOnlyOneGrabber == NULL) 
		return 0;

	theOnlyOneGrabber->nTotalCnt++;


	validCallHint = TRUE;
	theOnlyOneGrabber ->SetImageData2(lpVHdr->lpData);
	validCallHint = FALSE;

	
    return 0;
}


BOOL CFrameGrabber::InitCapture()
{
    //initial setup for capture
	if( !capDriverConnect(m_capwnd, DEFAULT_CAPTURE_DRIVER))
        return FALSE;

	CAPTUREPARMS capParams; 
    capCaptureGetSetup(m_capwnd, &capParams, sizeof(capParams)); 
    capParams.dwRequestMicroSecPerFrame = 33333;//(DWORD)(1.0e6 / FramesPerSec);  초당 30프레임..

    capParams.wNumVideoRequested = 2;       //비디오버퍼수..
    capParams.fCaptureAudio = TRUE; 
    capParams.wNumAudioRequested = 0; 
    capParams.fYield = TRUE; 
    capParams.vKeyAbort = 0; 
    capParams.fAbortLeftMouse = FALSE; 
    capParams.fAbortRightMouse = FALSE; 
    capParams.fMakeUserHitOKToCapture=FALSE;
    capParams.wStepCaptureAverageFrames = 30;

    capCaptureSetSetup(m_capwnd, &capParams, sizeof(CAPTUREPARMS));

	if(	!capSetCallbackOnFrame(m_capwnd,(LPVOID) _grabber_CallbackProc))
        return FALSE;

	capPreviewRate(m_capwnd,30);//./NumFrameBySec); //Overlay(g_hWndCap,TRUE); // overlay 할경우 사용. 
    capPreview(m_capwnd, TRUE); 

    capCaptureStop(m_capwnd);	// REC STOP

    capSetCallbackOnVideoStream(m_capwnd,(LPVOID) StreamCallbackProc); // 콜백 함수 설정
	return TRUE;

}


BOOL CFrameGrabber::InitialCaptureStream()
{

	//initial setup for capture
	if( !capDriverConnect(m_capwnd, DEFAULT_CAPTURE_DRIVER))
        return FALSE;

    //////////////////////////////////
    //초기화.. 160*120 24bit 이미지//
    ////////////////////////////////
    BITMAPINFO bm;
	bm.bmiHeader.biBitCount		= 24;
	bm.bmiHeader.biClrImportant	= 0;
	bm.bmiHeader.biClrUsed		= 0;
	bm.bmiHeader.biCompression	= BI_RGB;
	bm.bmiHeader.biHeight		= 120;
	bm.bmiHeader.biPlanes		= 1;
	bm.bmiHeader.biSize			= 40;
	bm.bmiHeader.biSizeImage		= 57600;
	bm.bmiHeader.biWidth			= 160;
	bm.bmiHeader.biXPelsPerMeter	= 0;
	bm.bmiHeader.biYPelsPerMeter	= 0;
	bm.bmiColors[0].rgbBlue		= 0;
	bm.bmiColors[0].rgbGreen		= 0;
	bm.bmiColors[0].rgbRed		= 0;
	bm.bmiColors[0].rgbReserved	= 0;

	//콜백 함수에서 제대로된 DIB정보를 얻기위해 비디오 포맷을 RGB 24bit로 설정합니다.
	if( !capSetVideoFormat( m_capwnd, &bm, sizeof(BITMAPINFO) ) )
	{
		capDlgVideoFormat( m_capwnd );
		if( !capSetVideoFormat( m_capwnd, &bm, sizeof(BITMAPINFO) ) )
		{
			AfxMessageBox("비디오 포맷 설정에 실패했습니다.");
			return FALSE;
		}
	}

//	capSetCallbackOnVideoStream(m_capwnd,(LPVOID) StreamCallbackProc); // 콜백 함수 설정


	CAPTUREPARMS capParams; 
	capCaptureGetSetup(m_capwnd, &capParams, sizeof(capParams)); 
//    capParams.dwRequestMicroSecPerFrame = 33333;//(DWORD)(1.0e6 / FramesPerSec);  초당 30프레임..
    //너무 빠른 프레임 캡쳐는 부하의 원인이다..
    capParams.dwRequestMicroSecPerFrame =  100000;//(DWORD)(1.0e6 / FramesPerSec);  초당 10프레임..
    capParams.wNumVideoRequested = 2;       //비디오버퍼수..
    capParams.fCaptureAudio = FALSE; 
    capParams.wNumAudioRequested = 0; 
    capParams.fYield = TRUE; 
    capParams.vKeyAbort = 0; 
    capParams.fAbortLeftMouse = FALSE; 
    capParams.fAbortRightMouse = FALSE; 
    capParams.fMakeUserHitOKToCapture=FALSE;
    capParams.wStepCaptureAverageFrames = 30;

    capCaptureSetSetup(m_capwnd, &capParams, sizeof(CAPTUREPARMS));

	capPreviewRate(m_capwnd,30);//./NumFrameBySec); //Overlay(g_hWndCap,TRUE); // overlay 할경우 사용. 
//    capPreview(m_capwnd, TRUE); 
    capPreview(m_capwnd, FALSE);    //눈에 보이지 않을거 궅이 필요없다..

	capPreviewScale( m_capwnd, true );


	capSetCallbackOnVideoStream(m_capwnd,(LPVOID) StreamCallbackProc); // 콜백 함수 설정
	capCaptureSequenceNoFile(m_capwnd);

	return TRUE;
}

void CFrameGrabber::SetUseCaptureAudio(BOOL bFlag)
{
    CAPTUREPARMS s;
    capCaptureGetSetup(m_capwnd, &s, sizeof( CAPTUREPARMS));
    s.fCaptureAudio = bFlag;
    capCaptureSetSetup(m_capwnd, &s ,sizeof(CAPTUREPARMS));
}

// 캡쳐를 중지한다..
void CFrameGrabber::StopCaptureFrame()
{
	if(theOnlyOneGrabber) {
		capCaptureStop(m_capwnd);	//capture STOP
		capSetCallbackOnFrame(m_capwnd, NULL);
	}
}

//캡쳐를 시작한다//
void CFrameGrabber::ResumeCaptureFrame()
{
	if(theOnlyOneGrabber) {
		capSetCallbackOnFrame(m_capwnd,(LPVOID)_grabber_CallbackProc);
	}
}

// capture 스트리밍을 멈춤.
void CFrameGrabber::StopCaptureStream()
{
	if(theOnlyOneGrabber) {
		capCaptureStop(m_capwnd);	//capture STOP
		capSetCallbackOnVideoStream(m_capwnd, NULL);
	}
}

// 스트리밍 캡쳐..재기..
void CFrameGrabber::ResumeCaptureStream()
{
	if(theOnlyOneGrabber) {
		capSetCallbackOnVideoStream( m_capwnd,(LPVOID)StreamCallbackProc);
		capCaptureSequenceNoFile(m_capwnd);
	}
}
