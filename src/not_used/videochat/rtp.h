//RTP Protocol header file
#define RTP_SEQ_MOD (1<<16)
#define RTP_TS_MOD	(0xffffffff)
#define RTP_VERSION 2//Current type value
#define RTP_MAX_SDES 256//maximum text length for SDES 
//Packet mode flags
#define fComp2X     1//Simple 2 to 1 compression
#define fDebug	    2//Debug mode
#define fSetDest    4//Set sound output destination
#define fDestSpkr   0//Destination: speaker
#define fDestJack   8//Destination: audio output jack
#define fLoopBack   16//Loopback packets for testing
#define fCompGSM    32//GSM compression
#define fEncDES     64//DES encrypted
#define fEncOTP     128//One-time pad encrypted
#define fEncIDEA    256//IDEA encrypted
#define fCompADPCM	512//ADPCM compressed
#define fEncPGP			1024//PGP-protected session key encrypted
#define fKeyPGP			2048//Packet contains PGP-encrypted session key
#define fCompLPC		4096//LPC compressed
#define fFaceData   8192//Request/reply for face data
#define fFaceOffer  16384//Offer face image to remote host
#define fNewProto   0x8000//Version 7.1 Protocol Enhancements
#define fCompVOX		0x10000//VOX compressed
#define fCompLPC10	0x20000//LPC-10 compressed
#define fCompRobust	0x40000//Robust duplicate packet mode
#define fEncBF			0x80000//Blowfish encrypted
#define fProtocol   0x40000000//protocol flag
typedef enum 
{
  RTCP_SR=200,
  RTCP_RR=201,
  RTCP_SDES=202,
  RTCP_BYE=203,
  RTCP_APP=204
} rtcp_type_t;
typedef enum 
{
  RTCP_SDES_END=0,
  RTCP_SDES_CNAME=1,
  RTCP_SDES_NAME=2,
  RTCP_SDES_EMAIL=3,
  RTCP_SDES_PHONE=4,
  RTCP_SDES_LOC=5,
  RTCP_SDES_TOOL=6,
  RTCP_SDES_NOTE=7,
  RTCP_SDES_PRIV=8, 
  RTCP_SDES_IMG=9,
  RTCP_SDES_DOOR=10,
  RTCP_SDES_SOURCE=11
} rtcp_sdes_type_t;
typedef enum//Common audio description for files and workstations 
{
	AE_PCMU,
	AE_PCMA,
	AE_G721,
	AE_IDVI,
	AE_G723,
	AE_GSM,
	AE_1016,
	AE_LPC,
	AE_L8,
	AE_L16,
	AE_L24,
	AE_L32,
	AE_G728,
	AE_TRUE,
	AE_MAX
}audio_encoding_t;
typedef struct 
{
	audio_encoding_t encoding;//type of encoding (differs) 
	unsigned sample_rate;//sample frames per second 
	unsigned channels;//number of interleaved channels 
}audio_descr_t;
#define ELEMENTS(array) (sizeof(array)/sizeof((array)[0]))//배열의 요소의 수를 결정한다
static audio_descr_t adt[]=
{//enc sample ch 
  {AE_PCMU,8000,1},//0 PCMU			 
  {AE_MAX,8000,1},//1 1016			 
  {AE_G721,8000,1},//2 G721		   
  {AE_GSM,8000,1},//3 GSM				 
  {AE_G723,8000,1},//4 Unassigned 
	{AE_IDVI,8000,1},//5 DVI4			 
  {AE_IDVI,16000,1},//6 DVI4		   
  {AE_LPC,8000,1},//7 LPC			   
  {AE_PCMA,8000,1},//8 PCMA		   
  {AE_MAX,0,1},//9 G722			 
  {AE_L16,44100,2},//10 L16				 
  {AE_L16,44100,1},//11 L16			   
  {AE_MAX,0,1},//12						 
};
//RTP input packet construction and parsing
#define bcopy(s,d,l) _fmemcpy(d,s,l)
#define bzero(d,l)	_fmemset(d,0,l)
#define MAX_MISORDER 100
#define MAX_DROPOUT  3000
#ifdef MICROSOFT_COMPILERS_CAN_BE_TRUSTED
typedef struct 
{
  unsigned int version:2;//protocol version 
  unsigned int p:1;//padding flag 
  unsigned int x:1;//header extension flag 
  unsigned int cc:4;//CSRC count 
  unsigned int m:1;//marker bit 
  unsigned int pt:7;//payload type 
  u_int16 seq;//sequence number 
  u_int32 ts;//timestamp 
  u_int32 ssrc;//synchronization source 
  u_int32 csrc[1];//optional CSRC list 
} rtp_hdr_t;
typedef struct 
{
  unsigned int version:2;//protocol version 
  unsigned int p:1;//padding flag 
  unsigned int count:5;//varies by payload type 
  unsigned int pt:8;//payload type 
  u_int16 length;//packet length in words, without this word 
} rtcp_common_t;
typedef struct//reception report
{
  u_int32 ssrc;//data source being reported 
  unsigned long fraction:8;//fraction lost since last SR/RR 
  long lost:24;//cumulative number of packets lost (signed!) 
	u_int32 last_seq;//extended last sequence number received 
	u_int32 jitter;//interarrival jitter 
  u_int32 lsr;//last SR packet from this source 
  u_int32 dlsr;//delay since last SR packet 
} rtcp_rr_t;
typedef struct 
{
  u_int8 type;//type of SDES item (rtcp_sdes_type_t) 
  u_int8 length;//length of SDES item (in octets) 
  char data[1];//text, not zero-terminated 
} rtcp_sdes_item_t;
typedef struct//one RTCP packet 
{
  rtcp_common_t common;//common header
  union 
	{
		//sender report (SR)
		struct 
		{
			u_int32 ssrc;//source this RTCP packet refers to
			u_int32 ntp_sec;//NTP timestamp
			u_int32 ntp_frac;
			u_int32 rtp_ts;//RTP timestamp 
			u_int32 psent;//packets sent 
			u_int32 osent;//octets sent 
			rtcp_rr_t rr[1];//variable-length list 
		}sr;
		struct//reception report (RR)
		{
		  u_int32 ssrc;//source this generating this report 
			rtcp_rr_t rr[1];//variable-length list
		}rr;
		struct 
		{
		  u_int32 src[1];//list of sources
		  //can't express trailing text
		}bye;
		struct rtcp_sdes_t//source description (SDES)
		{
		  u_int32 src;//first SSRC/CSRC
			rtcp_sdes_item_t item[1];//list of SDES items
		}sdes;
	}r;
}rtcp_t;
#endif
extern int RTPCreateSdes(char **pkt,unsigned long ssrc_i,int port,int exact,int strict);//SDES암호와
extern int RTPCreateBye(unsigned char *p,unsigned long ssrc_i,char *raison,int strict);//BYE생성
extern long RTPOutput(SoundBuffer *sb,unsigned long ssrc_i,unsigned long timestamp_i,unsigned short seq_i,int spurt);//RTP패킷을 내보낸다
extern int IsValidRTCPpacket(unsigned char *p,int len);//RTCP가능한지
extern int IsRTCPByepacket(unsigned char *p,int len);//Bye인지
extern int IsRTPpacket(unsigned char *pkt,int len);//RTP패킷인지 검사한다
extern int RTPMakeBye(unsigned char *p,unsigned long ssrc_i,char *raison,int strict);//RTPMakeBye