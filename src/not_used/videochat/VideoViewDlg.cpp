// VideoViewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VideoView.h"
#include "VideoViewDlg.h"
#include "main.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVideoViewDlg dialog

CVideoViewDlg* 	CVideoViewDlg::_instance = 0; 

CVideoViewDlg*	
CVideoViewDlg::getInstance() {
	if(!_instance) {
		if(!_instance) {
			_instance = new CVideoViewDlg;
		}
	}
	return _instance;
}

void CVideoViewDlg::clearInstance()
{
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
}



CVideoViewDlg::CVideoViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVideoViewDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CVideoViewDlg)
	m_pPartnerViewThread = NULL;
	m_pMyViewThread = NULL;
	m_pSendFrameThread = NULL;
	m_pFrameGrabber = NULL;
    m_pUdpSock = NULL;
	m_pVoiceSock = NULL;
	m_bHaveCamer = FALSE;
    m_bListening = FALSE;       //리슨했어?
	m_bVoiceListening = FALSE;
    m_bSending = FALSE;         // 전송중이야?
    m_bHaveCamer = FALSE;
	m_nFrmRate = 5;//0~20
	m_nQuality = 50;//0~100 비디오 압축정도( 화질)  75%로 gif압축을 한다.. --눈으로는 100으로 보인다.
	m_nRemotePort = 0;
    m_existBackGroundDialog = false;
    m_pSnapFileWnd = NULL;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDD_VIDEOVIEW_DIALOG);

	m_bDestroying = false;
	m_bConnect = false;
}

void CVideoViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVideoViewDlg)
	DDX_Control(pDX, IDC_PORT, m_Port);
	DDX_Control(pDX, IDC_IPADDRESS, m_IpAddress);
    DDX_Control(pDX, IDC_MAINAV_VIDEO_ME, m_myView);
    DDX_Control(pDX, IDC_MAINAV_VIDEO_PARTNER, m_partnerView);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CVideoViewDlg, CDialog)
	//{{AFX_MSG_MAP(CVideoViewDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	ON_BN_CLICKED(IDC_BUTTON7, OnButton7)
	ON_BN_CLICKED(IDC_BUTTON8, OnButton8)
	ON_BN_CLICKED(IDC_BUTTON9, OnButton9)
	ON_BN_CLICKED(IDC_BUTTON10, OnButton10)
	ON_BN_CLICKED(IDC_BUTTON11, OnButton11)
	ON_BN_CLICKED(IDC_START, OnStart)
	ON_BN_CLICKED(IDC_STOP, OnStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CVideoViewDlg::SetVideoFrmRate(int nRate)
{
    m_nFrmRate = nRate;
  //  WriteProfileInt("Settings","FrmRate", m_nFrmRate);     //비디오 프레임 전송 속도.. 

}

void CVideoViewDlg::SetGifQuality(int nQuality)
{
    m_nQuality = nQuality;
//    WriteProfileInt( "Settings","GifQuality", m_nGifQuality);  //비디오 압축정도( 화질)  75%로 gif압축을 한다.. --눈으로는 100으로 보인다.
}


BOOL CVideoViewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

//	m_Config.Create(IDD_CHOOSEFRAME,this);

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	memset(&m_szRemoteHost ,0x00, sizeof( m_szRemoteHost));

	//SetWindowPos(&wndTop , 0,0, 640,480,SWP_NOMOVE );
	//MoveWindow(0,0,640,480,TRUE);
	

	//m_myView.MoveWindow(600,0,60,40,TRUE);
	//m_myView.ShowWindow(SW_SHOW);
	
	//m_partnerView.MoveWindow(cX,cY,nWidth,nHeight,TRUE);
 //   m_partnerView.ShowWindow(SW_SHOW);

	SetVideoFrmRate( 30);   //세팅한다..
	SetGifQuality( 100);

	if (!AfxSocketInit())
	{
		AfxMessageBox("소켓 초기화에 실패하였습니다");
		return FALSE;
	}	


	//CString ip,port;
	//ip = "211.232.57.175";
	//port = "20001";
	//StartVideo(atoi((LPSTR)(LPCTSTR)strRemotePort),strRemoteHost);

	//VoicePlayAll();


	////CreateCloneFrameView(&m_partnerView);

 //   CreateCloneFrameView(&m_myView);

	//CSize size;
	//size.cx = 640;
	//size.cy = 480;

	//
 //   if(m_pFrameGrabber->GetSafeHwnd())
	//{
	//	m_pFrameGrabber->SetVideoFormat(size);
	//}

	return TRUE;  // return TRUE  unless you set the focus to a control
}


BOOL CVideoViewDlg::PreTranslateMessage(MSG* pMsg) 
{

	//if (pMsg->message == WM_TIMER)
	//	OnTimer();
	//else if(pMsg->message == WM_KEYDOWN)
	if(pMsg->message == WM_KEYDOWN)
	{
		CString msg;
		msg.Format("----------------CVideoViewDlg::WM_KEYDOWN (%d, %d)\n", pMsg->wParam, pMsg->lParam);

		::AfxMessageBox(msg);

		CString str = NULL;
		CString isReject = NULL;
       switch(pMsg->wParam)

        {					
            case VK_ESCAPE://ESC키를 눌렀을때..

			

			return TRUE;
        }
	}

	return CDialog::PreTranslateMessage(pMsg);;
}


void CVideoViewDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CVideoViewDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CVideoViewDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

//실행파일 구하기..
CString CVideoViewDlg::GetExecPath()
{
	CString strPath;
	char execPath[128]="";
	GetModuleFileName(NULL,execPath,sizeof(execPath));
	
	for(int len=strlen(execPath);len;len--) {
		if(*(execPath+len)=='\\') {
			*(execPath+len)=0;
			break;
		}
	}
	strPath = execPath ; 
	return strPath;
}

void CVideoViewDlg::SetMyFrame()
{
	if( m_pFrameGrabber->GetSafeHwnd())
	{
		LPBITMAPINFO lpBi = m_pFrameGrabber->GetDIB();
		m_myView.SetFrame(lpBi);   //snap shot창에 보여준다.//
	}
}


//JPEG 파일을 Decode해서 뷰어에 뿌려주자..
void CVideoViewDlg::ViewPartnerJPEG(char *pJpg, DWORD dwSize, CWnd *pWnd)
{
	if(m_bDestroying)
		return;

/*
	CString result;
    result.Format("\nRecv udp socket jpeg size = %d\n", dwSize);
    OutputDebugString(result);
*/
    
	CFrameViewer* pPartnerView = (CFrameViewer*)pWnd;
	if( !pPartnerView || !pPartnerView->GetSafeHwnd() )
		return;

	LPBITMAPINFO pOutputBmp = NULL;
    int nWidth, nHeight, nHeadSize;

	// 1. Read Jpeg Header..
	CTonyJpegDecoder decoder;
	if( decoder.ReadJpgHeader( (unsigned char*) pJpg, dwSize, nWidth, nHeight, nHeadSize ) == false )
	{
//		AfxMessageBox("지원하지 않는 이미지 포멧입니다.");
		return;		
	}
	///////////////////////////////////////////////
	// 2. prepare bmp to receive data
	DWORD m_dwDibSize;//not include file header
	unsigned char *m_pDib, *m_pDibBits;	
	BITMAPINFOHEADER *m_pBIH;

	int nRowBytes = ( nWidth * 3 + 3 ) / 4 * 4;	//	BMP row bytes
	int nDataBytes = nRowBytes * nHeight;
	m_dwDibSize = nDataBytes + sizeof( BITMAPINFOHEADER );

	m_pDib = new unsigned char [m_dwDibSize ];
	m_pBIH = ( BITMAPINFOHEADER * ) m_pDib;
	m_pDibBits = & m_pDib[ sizeof( BITMAPINFOHEADER ) ];

	m_pBIH->biSize				= sizeof(BITMAPINFOHEADER);
	m_pBIH->biWidth				= nWidth;
	m_pBIH->biHeight			= nHeight;
	m_pBIH->biPlanes			= 1;
	m_pBIH->biBitCount			= 24;
	m_pBIH->biCompression		= BI_RGB;
	m_pBIH->biSizeImage			= 0;
	m_pBIH->biXPelsPerMeter		= 0;
	m_pBIH->biYPelsPerMeter		= 0;
	m_pBIH->biClrUsed			= 0;
	m_pBIH->biClrImportant		= 0;

	/////////////////////////////
	// 3. decompress to bmp buffer
	decoder.DecompressImage( (unsigned char*)pJpg + nHeadSize, m_pDibBits );

	// 4. View..
	if( pPartnerView && pPartnerView->GetSafeHwnd())
		pPartnerView->SetFrame((LPBITMAPINFO)m_pDib);

	delete []m_pDib;
	GlobalFree(pJpg);
}


//프레임을  jpeg로 인코딩하여 전송한다..
int CVideoViewDlg::SendJPEGFrame()
{
	LPBITMAPINFO pInputBmp ;

    CBitmapEx* pBitmapEx = m_myView.GetBitPtr();
    HANDLE hdib = pBitmapEx->DibFromBitmap();
 	if(!hdib)	
        return FALSE;

    pInputBmp = (LPBITMAPINFO)GlobalLock (hdib);

	int headSize = pInputBmp->bmiHeader.biSize;
    int dataSize = pInputBmp->bmiHeader.biSizeImage;
	int nWidth = pInputBmp->bmiHeader.biWidth;
	int nHeight = pInputBmp->bmiHeader.biHeight;

    LPVOID pImg = GlobalAlloc(GMEM_FIXED, dataSize + headSize+1);
    //Head//
    CopyMemory( pImg, pInputBmp, headSize);
    //Data//
    CopyMemory(	((LPBYTE)pImg)+ headSize, (LPBYTE)pInputBmp + headSize, dataSize);

//  GlobalFree(pInputBmp);
    GlobalUnlock (hdib);
    GlobalFree (hdib);

	unsigned char *pJpg;
	pJpg = new unsigned char [headSize+dataSize+1];//cannot bigger than bmp
	memset( pJpg, 0x00, headSize+dataSize+1);

	//jpeg encoding..//
	int nOutputBytes = 0;
	CTonyJpegEncoder encoder( m_nQuality );
	encoder.CompressImage( (unsigned char*)pImg + headSize, pJpg, nWidth, nHeight, nOutputBytes );

    GlobalFree(pImg);

    // pJpg를 모든 user들에게..send한다//
	int ret = SendFrameToUser( pJpg, nOutputBytes);

	delete []pJpg;

    return nOutputBytes;
}

// USER List에서 보낼 USER를 찾아서 보낸다....
int CVideoViewDlg::SendFrameToUser(LPVOID pImg, int nSize)
{
	if(m_bDestroying)
		return 0;

    int ret = m_pUdpSock->SendTo(pImg, nSize, 4000, m_szRemoteHost);
//    TRACE("\n Send Video Data , %s:%d , %d", m_szRemoteHost, m_nRemotePort , nSize);
	return ret;
}

int CVideoViewDlg::SendVoiceToUser(LPVOID buf, int nSize)
{
    int ret = m_pUdpSock->SendTo(buf, nSize, 4001, m_szRemoteHost);
    TRACE("\n Send Voice Data , %s:%d , %d", m_szRemoteHost, m_nRemotePort , nSize);
	return ret;
}

void CVideoViewDlg::ViewPartner(char *pImg , DWORD dwSize,  CString strRemoteAddress, int nRemotePort)
{
	// Get ParnterView's window handle//
//	CFrameViewer *pPartnerView = GetPartnerView( strRemoteAddress, nRemotePort);
    //이창에서는 1:1 그래서..partnerview가 하나..

	if(m_bConnect == false)
	{
		if(m_myView.m_pCloneWnd && m_myView.m_pCloneWnd->GetSafeHwnd())
			m_myView.m_pCloneWnd->ShowWindow(SW_SHOW);
		m_bConnect = true;
	}

    CFrameViewer *pPartnerView = &m_partnerView;
	if( !pPartnerView ) {
		 GlobalFree(pImg);
		return;
	}

	FRAME_T *pFrame = (FRAME_T*)malloc( sizeof( FRAME_T));
	pFrame->dwSize = dwSize;
	pFrame->pImg = pImg;
	pFrame->pWnd = pPartnerView;
	
	m_pPartnerViewThread->PostThreadMessage( WM_THREADMSG_VIEWPARTNER, 0 , (LPARAM)pFrame);	
}

void CVideoViewDlg::OnTimer(UINT nIDEvent) 
{
    switch(nIDEvent)
    {	
		case XFER_TIMER://내 프레임을 쓰레드를 통해서 전송한다..
		{
			if(m_pSendFrameThread)
			{
				m_pSendFrameThread->PostThreadMessage( WM_THREADMSG_SENDFRAME , 0,0);
			}
			break;
		}
	}
	CDialog::OnTimer(nIDEvent);
}

//remotePort로 날려주자.. 나의 비디오를..
void CVideoViewDlg::StartVideo(int remotePort, CString strAddr)
{
    m_nRemotePort = remotePort;
    strcpy( m_szRemoteHost , strAddr );    
    InitVideo();
}

extern bool VoiceToolKitInit(int port,int NoiseCleanup,int CodecMode);

void CVideoViewDlg::InitVideo()
{
	////////////////////////////////////////////////////////////////////////////
    m_bHaveCamer = StartPreview();
    if(! m_bHaveCamer)
	{
        TRACE("\n웹 카메라 디바이스를  찾을 수 없습니다.");
        MessageBox("웹 카메라 디바이스를  찾을 수 없습니다.");
	}

	CreateFrameViewThread();
    
	m_myView.SetIsMyView(true);		//이렇게 해야만 화면에 나온다//

	this->m_partnerView.SetIsMyView(true);		//상대방 화면을 보이게 한다

    //data디렉 토리가 없으면 만든다.//
    CFileStatus status;
    CString dataPath;
    dataPath.Format("%s\\data",GetExecPath());
	if ( CFile::GetStatus(dataPath, status ) == FALSE ) {
		CreateDirectory(dataPath,NULL);
	}

	VoiceToolKitInit(9878,900,ADPCM_CODEC);//음성 엔진을 초기화
	// udp로 통신할 소켓들을 리슨한다.. 
	StartListen();	
	StartVoiceListen();
	Sleep(100);

    SetXfer(TRUE);  //나의 화상을 보낸다..  m_partner에게..
}

// 나의 화면을 보여준다..
BOOL CVideoViewDlg::StartPreview()
{
    int ret = FALSE;
    if(!m_pFrameGrabber) {
        CRect rect;
        this->GetClientRect(rect);
        m_pFrameGrabber  = new CFrameGrabber( this);
        ret = m_pFrameGrabber->Create(0,0,this);
        if( ret)
            m_pFrameGrabber->MoveWindow(0,0,1,1);				//아주 조그맣게 만들자.. ^^		
													// INVISIBLE 하면 callback이 잘 안된다..
        return ret;
    }
    else 
    {
        return TRUE;    //이미 만들어져있다.
    }
}


//뷰어를 위한 Thread..
void CVideoViewDlg::CreateFrameViewThread()
{
	//내화면을 나에게 뷰어하는 쓰레드
	m_pMyViewThread = new CFrameThread();
	m_pMyViewThread->CreateThread();
	m_pMyViewThread->m_pWnd = this;

	//상대방 프레임을 뷰어하는 쓰레드..
	m_pPartnerViewThread = new CFrameThread();
	m_pPartnerViewThread->CreateThread();
	m_pPartnerViewThread->m_pWnd = this;

	//내프레임을 전송하는 쓰레드.//
	m_pSendFrameThread = new CFrameThread();
	m_pSendFrameThread->CreateThread();
	m_pSendFrameThread->m_pWnd = this;

}

BOOL CVideoViewDlg::StartVoiceListen()
{
    m_pVoiceSock = new CUdpStack(this);
    m_pVoiceSock->SetMode(SOCKET_FOR_VOICE);

    int listenPort = 4001;

    if(! m_pVoiceSock->Create( listenPort, SOCK_DGRAM)  ) {
        static int ret = GetLastError( );
        return FALSE;
    }

    m_bVoiceListening = TRUE;
    return TRUE;
}
//비디오를 위한 socket를 listen한다..
BOOL CVideoViewDlg::StartListen()
{
    m_pUdpSock = new CUdpStack(this);
    m_pUdpSock->SetMode(SOCKET_FOR_VIDEO);

    int listenPort = 4000;//UaDevice::instance()->getVideoPort();

    if(! m_pUdpSock->Create( listenPort, SOCK_DGRAM)  ) {
        static int ret = GetLastError( );
//        PrintError(ret);
        return FALSE;
    }

    m_bListening = TRUE;
    return TRUE;
}



//프레임을 보낼까? 말까?
void CVideoViewDlg::SetXfer(BOOL bFlag)
{
    if( bFlag) {
        SetTimer( XFER_TIMER, 1000/m_nFrmRate , NULL);   //초당 m_nFrmRate 프레임씩 전송하자..
        m_bSending = TRUE;
    }
    else {
		if(::IsWindow(m_hWnd))
			KillTimer( XFER_TIMER); 
        m_bSending = FALSE;
    }
}

void CVideoViewDlg::StopAudio()
{
	m_bVoiceListening = FALSE;
}

extern bool VoiceStopAll(void);

//비디오를 멈춘다..
void CVideoViewDlg::StopVideo()
{
	VoiceStopAll();

	SetXfer(FALSE); //화상전송 stop.
	//ctrl을 제거해야 할까?..
	CloseData() ;

    m_nRemotePort = 0;
    memset(&m_szRemoteHost ,0x00, sizeof( m_szRemoteHost));

    m_bSending = FALSE;
    m_bListening = FALSE;

    //내프레임과 상대방 프레임.. 이미지 제거..
    m_myView.SetIsMyView(false);	
    m_partnerView.SetIsMyView(false);

	if(m_pFrameGrabber)
	{
		delete m_pFrameGrabber;
		m_pFrameGrabber = NULL;
	}

	if(::IsWindow(m_hWnd))
		Invalidate();
}


void CVideoViewDlg::CloseData()
{
	if(::IsWindow(m_hWnd))
		KillTimer(XFER_TIMER);	//내프레임 전송 중지..
	if( m_pFrameGrabber) m_pFrameGrabber->StopCaptureStream();// stop capture .. audio & video//
	// Thread를 제거한다//
	if(m_pMyViewThread)
	{
		m_pMyViewThread->PostThreadMessage(WM_QUIT, 0,0);
		m_pMyViewThread = NULL;
	}
	if(m_pPartnerViewThread)
	{
		m_pPartnerViewThread->PostThreadMessage(WM_QUIT, 0,0);
		m_pPartnerViewThread = NULL;
	}
	if(m_pSendFrameThread)
	{
		m_pSendFrameThread->PostThreadMessage(WM_QUIT, 0,0);
		m_pSendFrameThread = NULL;
	}

    CloseSocket();
}

extern void VoiceToolKitClose();
//Socket Close;
BOOL CVideoViewDlg::CloseSocket()
{
	if(::IsWindow(m_hWnd))
	    KillTimer(XFER_TIMER);
    Sleep(100);
    if( m_pUdpSock) 
	{
        m_pUdpSock->Close();
        delete m_pUdpSock;
    }
    
    m_bListening = m_bSending = FALSE;

	//음성중지
	if( m_pVoiceSock)
	{
		m_pVoiceSock->Close();
		delete m_pVoiceSock;
	}
	m_bVoiceListening = FALSE;
	VoiceToolKitClose();
    return TRUE;
}

LRESULT CVideoViewDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
    switch (message) {
	case	WM_CALLBACKMSG_CAPUTRE_IMAGE :	//이미지가 캡쳐 되었을때..
		if( m_pMyViewThread) 
		{
			DWORD dwexitcode;
			::GetExitCodeThread( m_pMyViewThread->m_hThread, &dwexitcode);
			if( dwexitcode == STILL_ACTIVE )
				m_pMyViewThread->OnSetMyFrm(0,0);
		}
		break;
    case    WM_SELECT_VIDEO_BG :    //이미지의 배경 변경..
        {
            ChangeMyVideoBG( (int)wParam);
            break;
        }
    }
	return CDialog::WindowProc(message, wParam, lParam);
}


//나의 배경이미지를 바꾸자..
void CVideoViewDlg::ChangeMyVideoBG(int nBitmapID)
{
    if( nBitmapID == 0)
        m_myView.SetUseBackImage(FALSE);
    else
        m_myView.SetBGImg(nBitmapID , RGB(255,255,255));
}

BOOL CVideoViewDlg::DestroyWindow() 
{
	m_bDestroying = true;

	StopVideo();
	
	if(m_myView.m_pCloneWnd && m_myView.m_pCloneWnd->GetSafeHwnd())
	{
		m_myView.m_pCloneWnd->DestroyWindow();
		delete ((CCloneFrameWnd*)m_myView.m_pCloneWnd);
		m_myView.m_pCloneWnd = NULL;
	}

	return CDialog::DestroyWindow();
}

void CVideoViewDlg::OnButton1() 
{
	m_Config.CenterWindow();
	m_Config.ShowWindow(SW_SHOW);
}

void CVideoViewDlg::OnButton2() 
{
    static bool bFlag = true;

    if( bFlag) {
        COleDateTime cTime;
        cTime = COleDateTime::GetCurrentTime();

        CString aviFile;
       

		aviFile.Format("%s\\data\\Rec_%04d%02d%02d_%02d%02d%02d(%s).avi", 
                            GetExecPath(),
                            cTime.GetYear(), cTime.GetMonth(),cTime.GetDay(), 
                            cTime.GetHour(), cTime.GetMinute(), cTime.GetSecond(),
                            "ME" );

        m_myView.StartRecAVI(aviFile, 15);
       // m_btnRec.SetBitmaps( IDB_MAINAV_STOP_OVER,RGB(255, 0, 0),
        //                    IDB_MAINAV_STOP,RGB(255, 0, 0));

        bFlag = FALSE;
    }    
    else {

     //   m_btnRec.SetBitmaps( IDB_MAINAV_REC_OVER, RGB(255,0,0) ,
     //                        IDB_MAINAV_REC, RGB(255,0,0));

        CString strFile;
        strFile = m_myView.StopRecAVI();
        CString str;
        str.Format("녹화가 완료되었습니다.\n"
                    "%s 파일로 저장되었습니다."
                    "\n저장된 동영상을 재생하시겠습니까?",strFile); 

   		int result = MessageBox(str, "동영상 재생" , MB_YESNO | MB_ICONINFORMATION);
		if(result == IDYES) {
            ShellExecute(NULL, "open", strFile, NULL, NULL, SW_SHOWNORMAL);
        }
        bFlag = TRUE;
    }
}

void CVideoViewDlg::OnButton3() 
{
    CBitmapEx	*m_pImageBitmap = m_myView.GetBitPtr();
    if(! m_pImageBitmap)
        return;

    COleDateTime cTime;
    cTime = COleDateTime::GetCurrentTime();

    //JPG로 저장해보자꾸나..//
	CString jpgFile ;
	jpgFile.Format("%s\\data\\SnapStill_%04d%02d%02d_%02d%02d%02d(%s).jpg", 
                        GetExecPath(),
                        cTime.GetYear(), cTime.GetMonth(),cTime.GetDay(), 
                        cTime.GetHour(), cTime.GetMinute(),cTime.GetSecond(),
                        "ME" );

	SnapJPEG( m_pImageBitmap , jpgFile);	

    OpenJpgFile(jpgFile) ;
}


void CVideoViewDlg::OpenJpgFile(CString strFileName)
{

    if( !m_pSnapFileWnd || ! m_pSnapFileWnd->GetSafeHwnd())
    {
        CRect btnRect;
        m_myView.GetWindowRect( btnRect);

        m_pSnapFileWnd = new CSnapFileWnd;
        m_pSnapFileWnd->Create( IDD_SNAPFILE_WND , this);
        m_pSnapFileWnd->SetWindowPos( m_pSnapFileWnd, btnRect.left, btnRect.bottom - 170, 0,0,
                                        SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
    }
    m_pSnapFileWnd->LoadFile( strFileName);

}

// Snap Still을 jpg로 저장..
BOOL CVideoViewDlg::SnapJPEG(CBitmapEx *pBitmapEx, CString jpgFile)
{

	LPBITMAPINFO pInputBmp ;
    HANDLE hdib = pBitmapEx->DibFromBitmap();
 	if(!hdib)	
        return FALSE;

    pInputBmp = (LPBITMAPINFO)GlobalLock (hdib);

	int headSize = pInputBmp->bmiHeader.biSize;
    int dataSize = pInputBmp->bmiHeader.biSizeImage;
	int nWidth = pInputBmp->bmiHeader.biWidth;
	int nHeight = pInputBmp->bmiHeader.biHeight;

    LPVOID pImg = GlobalAlloc(GMEM_FIXED, dataSize + headSize+1);
    //Head//
    CopyMemory( pImg, pInputBmp, headSize);
    //Data//
    CopyMemory(	((LPBYTE)pImg)+ headSize, (LPBYTE)pInputBmp + headSize, dataSize);

    GlobalFree(pInputBmp);
    GlobalUnlock (hdib);


	unsigned char *pJpg;
	pJpg = new unsigned char [headSize+dataSize+1];//cannot bigger than bmp
	memset( pJpg, 0x00, headSize+dataSize+1);

	//jpeg encoding..//
	int nOutputBytes = 0;
	CTonyJpegEncoder encoder( m_nQuality );
	encoder.CompressImage( (unsigned char*)pImg + headSize, pJpg, nWidth, nHeight, nOutputBytes );

	//저장해보까나?//
	CFile cf;
	if( !cf.Open( jpgFile, CFile::modeCreate | CFile::modeWrite ) )
	{
		AfxMessageBox("Can not create jpg file!");
		return FALSE;
	}
	cf.Write( pJpg, nOutputBytes );

    GlobalFree(pImg);
	delete []pJpg;

	return TRUE;
}

void CVideoViewDlg::OnButton4() 
{
//비디오 배경선택.
    if( !m_existBackGroundDialog )
	{
		CWnd *pWnd = GetDlgItem(IDC_BUTTON4);
		CRect rect;
		pWnd->GetClientRect(&rect);
		
		CBgSelectWnd *bgDialog = new CBgSelectWnd(rect,m_existBackGroundDialog , this );
		bgDialog->Create(IDD_BG_SELECT_WND,this);

		//화면을 띄울때 버튼아래에 만들어지게...
        CRect dlgRect;
        bgDialog->GetClientRect(dlgRect);

	//	bgDialog->SetWindowPos(NULL,rect.left,rect.top - dlgRect.Height() ,0,0,SWP_NOSIZE|SWP_NOZORDER);
		bgDialog->ShowWindow(SW_SHOW);
	}
	else m_existBackGroundDialog = false;
}


void CVideoViewDlg::CreateCloneFrameView(CFrameViewer *pView)
{	
/*
	if( !(pView->GetUser() || pView->m_bMyView ) )
		return;
*/

	if( pView->m_pCloneWnd &&
		pView->m_pCloneWnd->GetSafeHwnd())
	{
		pView->m_pCloneWnd->ShowWindow(SW_SHOW);
	}
	else
	{
		CCloneFrameWnd* pCloneView = new CCloneFrameWnd();
		pCloneView->SetFrameViewerPtr( pView);
		//pCloneView->Create(IDD_CLONEFRM_WND,CWnd::GetDesktopWindow());
		pCloneView->Create(IDD_CLONEFRM_WND, &m_partnerView);
		pCloneView->ModifyStyle(0, WS_CLIPSIBLINGS | WS_SYSMENU  );
		pCloneView->ModifyStyleEx(0, WS_EX_PALETTEWINDOW );  //작업표시줄에서 제거하자..
		

		pView->m_pCloneWnd = (CWnd*)pCloneView;
		//pCloneView->ShowWindow(SW_SHOW);
		//pCloneView->SetWindowPos(&wndTopMost , (nWidth-cX)-162,(nHeight-cY)-122, 162,122,SWP_NOMOVE);

	}


}

void CVideoViewDlg::OnButton5() 
{
    CreateCloneFrameView(&m_myView);
}

void CVideoViewDlg::OnButton6() 
{
	CreateCloneFrameView(&m_partnerView);
}

void CVideoViewDlg::OnButton7() 
{
    CBitmapEx	*m_pImageBitmap = m_partnerView.GetBitPtr();
    if(! m_pImageBitmap)
        return;

    COleDateTime cTime;
    cTime = COleDateTime::GetCurrentTime();

    //JPG로 저장해보자꾸나..//
	CString jpgFile ;
	jpgFile.Format("%s\\data\\SnapStill_%04d%02d%02d_%02d%02d%02d(%s).jpg", 
                        GetExecPath(),
                        cTime.GetYear(), cTime.GetMonth(),cTime.GetDay(), 
                        cTime.GetHour(), cTime.GetMinute(),cTime.GetSecond(),
                        "ME" );

	SnapJPEG( m_pImageBitmap , jpgFile);	

    OpenJpgFile(jpgFile) ;
}

void CVideoViewDlg::OnButton8() 
{
    static bool bFlag = true;

    if( bFlag) {
        COleDateTime cTime;
        cTime = COleDateTime::GetCurrentTime();

        CString aviFile;
       

		aviFile.Format("%s\\data\\Rec_%04d%02d%02d_%02d%02d%02d(%s).avi", 
                            GetExecPath(),
                            cTime.GetYear(), cTime.GetMonth(),cTime.GetDay(), 
                            cTime.GetHour(), cTime.GetMinute(), cTime.GetSecond(),
                            "ME" );

        m_partnerView.StartRecAVI(aviFile, 15);
       // m_btnRec.SetBitmaps( IDB_MAINAV_STOP_OVER,RGB(255, 0, 0),
        //                    IDB_MAINAV_STOP,RGB(255, 0, 0));

        bFlag = FALSE;
    }    
    else {

     //   m_btnRec.SetBitmaps( IDB_MAINAV_REC_OVER, RGB(255,0,0) ,
     //                        IDB_MAINAV_REC, RGB(255,0,0));

        CString strFile;
        strFile = m_partnerView.StopRecAVI();
        CString str;
        str.Format("녹화가 완료되었습니다.\n"
                    "%s 파일로 저장되었습니다."
                    "\n저장된 동영상을 재생하시겠습니까?",strFile); 

   		int result = MessageBox(str, "동영상 재생" , MB_YESNO | MB_ICONINFORMATION);
		if(result == IDYES) {
            ShellExecute(NULL, "open", strFile, NULL, NULL, SW_SHOWNORMAL);
        }
        bFlag = TRUE;
    }
}

void CVideoViewDlg::OnButton9() 
{
    if(m_pFrameGrabber->GetSafeHwnd())
	{
		m_pFrameGrabber->VideoFormatDialog();
        
        CSize size = m_pFrameGrabber->GetImageSize();
        //m_sizePreview = size;
	}
}

void CVideoViewDlg::OnButton10() 
{
//비디오 옵션이 변경되었다...
//    m_nFrmRate = GetApp()->m_nFrmRate;
  //  m_nQuality = GetApp()->m_nGifQuality;

    if( m_bSending) {
        KillTimer(XFER_TIMER);
        SetTimer( XFER_TIMER, 1000/ m_nFrmRate, NULL);
    }
	else 
    {
		KillTimer(XFER_TIMER);
        SetTimer( XFER_TIMER, 1000/ m_nFrmRate, NULL);
    }
}

void CVideoViewDlg::OnButton11() 
{
	if(m_pFrameGrabber->GetSafeHwnd())
	{
		m_pFrameGrabber->VideoSourceDialog();
	}
}

extern bool VoicePlayAll(void);

void CVideoViewDlg::OnStart() 
{
	CString ip,port;
	this->m_IpAddress.GetWindowText(ip);
	this->m_Port.GetWindowText(port);
	StartVideo(atoi((LPSTR)(LPCTSTR)port),ip);

	VoicePlayAll();
}

void CVideoViewDlg::OnStop() 
{
	m_pFrameGrabber->StopCaptureStream();
	capDriverDisconnect(m_pFrameGrabber->m_capwnd);
//	pView->StopCaptureStream();
}

void CVideoViewDlg::CloseDlg()
{
	//m_myView.m_pCloneWnd->DestroyWindow();
	//SendMessage(WM_DESTROY);
	
	//if(m_myView.m_pCloneWnd->GetSafeHwnd())
	//	m_myView.m_pCloneWnd->DestroyWindow();

	DestroyWindow();
}

void CVideoViewDlg::InitDlgSize(CRect rc, LPCSTR lpszBgImage)
{
	MoveWindow(rc);
//	SetWindowPos(&wndTopMost , cx,cy,cWidth,cHeight,SWP_NOMOVE);

	StartVideo(atoi((LPSTR)(LPCTSTR)strRemotePort),strRemoteHost);

	VoicePlayAll();

	if(m_bViewPartner)
	    CreateCloneFrameView(&m_myView);

	CSize size;
	size.cx = 640;
	size.cy = 480;

    if(m_pFrameGrabber->GetSafeHwnd())
	{
		m_pFrameGrabber->SetVideoFormat(size);
	}


	//MoveWindow(rc,TRUE);
	//SetWindowPos(&wndTopMost, rc.left, rc.top, rc.Width(), rc.Height(), SWP_NOMOVE);
	ShowWindow(SW_SHOW);

	m_partnerView.MoveWindow(rc,TRUE);
    m_partnerView.ShowWindow(SW_SHOW);

//	ClientToScreen(rc);
	CRect clone_rc = rc;

	clone_rc.right = clone_rc.left + rc.Width() / 4;
	clone_rc.top  = clone_rc.bottom - rc.Height() / 4;

	if(m_myView.m_pCloneWnd && m_myView.m_pCloneWnd->GetSafeHwnd())
		m_myView.m_pCloneWnd->MoveWindow(clone_rc, TRUE);
//	m_myView.m_pCloneWnd->SetWindowPos(&wndTopMost, clone_rc.left, clone_rc.top, clone_rc.Width(), clone_rc.Height(),SWP_NOMOVE);
//	m_myView.m_pCloneWnd->ShowWindow(SW_SHOW);
	//m_myView.m_pCloneWnd->SetActiveWindow();

	m_myView.LoadImage(lpszBgImage);
	m_partnerView.LoadImage(lpszBgImage);

	if(m_bViewPartner)
	{
		CPoint pts[6];
		pts[0].SetPoint(rc.left,       rc.top);
		pts[1].SetPoint(rc.right,      rc.top);
		pts[2].SetPoint(rc.right,      rc.bottom);
		pts[3].SetPoint(clone_rc.right, rc.bottom);
		pts[4].SetPoint(clone_rc.right, clone_rc.top);
		pts[5].SetPoint(rc.left,       clone_rc.top);

		if(m_partnerView.m_rgn.GetSafeHandle() != NULL)
			m_partnerView.m_rgn.DeleteObject();

		m_partnerView.m_rgn.CreatePolygonRgn(pts, 6, ALTERNATE);
	}
}
