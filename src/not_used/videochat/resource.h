//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by VideoView.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    200
#define IDS_ABOUTBOX                    201
#define IDD_VIDEOVIEW_DIALOG            202
//#define IDR_MAINFRAME                   228
#define IDD_BG_SELECT_WND               278
#define IDB_BMP_BG1                     280
#define IDB_BMP_BG2                     281
#define IDB_BMP_BG3                     282
#define IDB_BMP_BG4                     283
#define IDB_BMP_BG5                     284
#define IDB_BMP_BG6                     285
#define IDB_BMP_BG7                     286
#define IDB_BMP_BG8                     287
#define IDB_BMP_BG9                     288
#define IDB_BMP_BG0                     289
#define IDD_CHOOSEFRAME                 573
#define IDD_CLONEFRM_WND                574
#define IDC_MAINAV_VIDEO_ME             2000
#define IDC_MAINAV_VIDEO_PARTNER        2001
#define IDC_BUTTON1                     2002
#define IDC_BUTTON2                     2003
#define IDC_BUTTON3                     2004
#define IDC_BUTTON4                     2005
#define IDC_BUTTON5                     2006
#define IDC_BUTTON6                     2007
#define IDC_BUTTON7                     2008
#define IDC_BUTTON8                     2009
#define IDC_BUTTON9                     2010
#define IDC_BUTTON10                    2011
#define IDC_BUTTON11                    2012
#define IDC_EDIT1                       2013
#define IDC_START                       2014
#define IDC_IPADDRESS                   2015
#define IDC_PORT                        2016
#define IDC_STOP                        2017
#define IDC_BTN_BG0                     2060
#define IDC_BTN_BG1                     2061
#define IDC_BTN_BG2                     2062
#define IDC_BTN_BG3                     2063
#define IDC_BTN_BG4                     2064
#define IDC_BTN_BG5                     2065
#define IDC_BTN_BG6                     2066
#define IDC_BTN_BG7                     2067
#define IDC_BTN_BG8                     2068
#define IDC_BTN_BG9                     2069
#define IDC_SLIDER_FRAME                2266
#define IDC_SLIDER_QUALITY              2267
#define IDC_STATIC_FRAME                2268
#define IDC_STATIC_QUALITY              2269
#define IDC_ST_SNAP_FILEPATH            3028
#define IDC_ST_SNAPVIVEW                3029
#define IDD_SNAPFILE_WND                4041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
