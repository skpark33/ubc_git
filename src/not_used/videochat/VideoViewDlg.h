// VideoViewDlg.h : header file
//

#if !defined(AFX_VIDEOVIEWDLG_H__41EA4F23_840D_4815_A17C_FDB5344F9F9F__INCLUDED_)
#define AFX_VIDEOVIEWDLG_H__41EA4F23_840D_4815_A17C_FDB5344F9F9F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define LEVEL_TIMER				 0
#define XFER_TIMER				10
#define SNAP_TIMER				20
#define SNAP_PARTNER_TIMER     30
#define	SEND_STUN_TIMER			50
#define FRAMERATE_TEST			60

enum {
	AUDIO_DEVICE =0,
	VIDEO_DEVICE,
	TEXT_DEVICE,
	WHITEBOARD_DEVICE
};

typedef struct {
	char * pImg;
	DWORD  dwSize;
	CWnd * pWnd;
}FRAME_T ;

#include "resource.h"
#include "FrameThread.h"
#include "FrameViewer.h"
#include "FrameGrabber.h"
#include "TonyJpegDecoder.h"
#include "TonyJpegEncoder.h"
#include "UdpStack.h"
#include "ChooseFrame.h"
#include "SnapFileWnd.h"
#include "bgselectwnd.h"
#include "CloneFrameWnd.h"
#include "singleton.h"

class CUdpStack;

//class CVideoViewDlg : public CDialog, public CSingleton<CVideoViewDlg>
class CVideoViewDlg : public CDialog
{
// Construction
public:
	CVideoViewDlg(CWnd* pParent = NULL);	// standard constructor

	static CVideoViewDlg*	getInstance();
	static void clearInstance();

    int m_nRemotePort;  //상대방의 UDP 비디오 port
	CString strRemoteHost;
	CString strRemotePort;
    char m_szRemoteHost[128] ;    //상대방의 ADDRESS
	
	void SetVideoFrmRate(int nRate);
	void SetGifQuality(int nQuality);

	CFrameThread *m_pPartnerViewThread;
	CFrameThread *m_pMyViewThread;
	CFrameThread *m_pSendFrameThread;
    CFrameGrabber	*m_pFrameGrabber;
	
	CUdpStack    *m_pUdpSock;
	CUdpStack    *m_pVoiceSock;

	CChooseFrame m_Config;

	CFrameViewer	m_myView;
	CFrameViewer	m_partnerView;

    CSnapFileWnd * m_pSnapFileWnd;
    bool m_existBackGroundDialog;   //배경이미지 윈도가 있냐?
	BOOL m_bSending;
	BOOL m_bVoiceListening;
	BOOL m_bListening;
	int m_nFrmRate;
    BOOL m_bHaveCamer;
	int m_nQuality;//비디오 압축정도( 화질)

	void CreateCloneFrameView(CFrameViewer *pView);
	void ChangeMyVideoBG(int nBitmapID);
	BOOL SnapJPEG(CBitmapEx *pBitmapEx, CString jpgFile) ;
	CString GetExecPath();
	BOOL CloseSocket();
	void CloseData();
	void StopVideo();
	void StopAudio();
	void SetXfer(BOOL bFlag);
	BOOL StartListen();
	BOOL StartVoiceListen();
	void CreateFrameViewThread();
	BOOL StartPreview();
	void InitVideo();
	void StartVideo(int remotePort, CString strAddr);
	int SendJPEGFrame();
	void SetMyFrame();
	void ViewPartnerJPEG(char *pJpg, DWORD dwSize, CWnd *pWnd);
	int SendFrameToUser(LPVOID pImg, int nSize);
	int SendVoiceToUser(LPVOID buf, int nSize);
	void ViewPartner(char *pImg , DWORD dwSize,  CString strRemoteAddress, int nRemotePort);
	void OpenJpgFile(CString strFileName);
	void CloseDlg();
	void InitDlgSize(CRect rc, LPCSTR lpszBgImage);

// Dialog Data
	//{{AFX_DATA(CVideoViewDlg)
	enum { IDD = IDD_VIDEOVIEW_DIALOG };
	CEdit	m_Port;
	CEdit	m_IpAddress;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVideoViewDlg)
	public:
	virtual BOOL DestroyWindow();

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	static CVideoViewDlg*	_instance;

	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CVideoViewDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	afx_msg void OnButton5();
	afx_msg void OnButton6();
	afx_msg void OnButton7();
	afx_msg void OnButton8();
	afx_msg void OnButton9();
	afx_msg void OnButton10();
	afx_msg void OnButton11();
	afx_msg void OnStart();
	afx_msg void OnStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	BOOL PreTranslateMessage(MSG* pMsg);

	bool	m_bDestroying;
	bool	m_bConnect;
	public:
	bool	m_bViewPartner;
};

//#define g_xMain CVideoViewDlg::GetInstancePtr()

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIDEOVIEWDLG_H__41EA4F23_840D_4815_A17C_FDB5344F9F9F__INCLUDED_)
