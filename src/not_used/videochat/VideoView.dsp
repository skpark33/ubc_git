# Microsoft Developer Studio Project File - Name="VideoView" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=VideoView - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "VideoView.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "VideoView.mak" CFG="VideoView - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "VideoView - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "VideoView - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "VideoView - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "VideoView - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x412 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "VideoView - Win32 Release"
# Name "VideoView - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "framethread"

# PROP Default_Filter ""
# Begin Group "jpgeencode"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\TonyJpegDecoder.cpp
# End Source File
# Begin Source File

SOURCE=.\TonyJpegDecoder.h
# End Source File
# Begin Source File

SOURCE=.\TonyJpegEncoder.cpp
# End Source File
# Begin Source File

SOURCE=.\TonyJpegEncoder.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\FrameGrabber.cpp
# End Source File
# Begin Source File

SOURCE=.\FrameGrabber.h
# End Source File
# Begin Source File

SOURCE=.\FrameThread.cpp
# End Source File
# Begin Source File

SOURCE=.\FrameThread.h
# End Source File
# Begin Source File

SOURCE=.\FrameViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\FrameViewer.h
# End Source File
# End Group
# Begin Group "picture"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Picture.cpp
# End Source File
# Begin Source File

SOURCE=.\Picture.h
# End Source File
# Begin Source File

SOURCE=.\PictureEx.cpp
# End Source File
# Begin Source File

SOURCE=.\PictureEx.h
# End Source File
# Begin Source File

SOURCE=.\pictureview.cpp
# End Source File
# Begin Source File

SOURCE=.\pictureview.h
# End Source File
# End Group
# Begin Group "bitmap"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CBitmapEx.cpp
# End Source File
# Begin Source File

SOURCE=.\CBitmapEx.h
# End Source File
# Begin Source File

SOURCE=.\CISBitmap.cpp
# End Source File
# Begin Source File

SOURCE=.\CISBitmap.h
# End Source File
# End Group
# Begin Group "avi"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\AviFile.cpp
# End Source File
# Begin Source File

SOURCE=.\AviFile.h
# End Source File
# End Group
# Begin Group "udpstack"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\UdpStack.cpp
# End Source File
# Begin Source File

SOURCE=.\UdpStack.h
# End Source File
# End Group
# Begin Group "config"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ChooseFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\ChooseFrame.h
# End Source File
# End Group
# Begin Group "snap"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\SnapFileWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\SnapFileWnd.h
# End Source File
# End Group
# Begin Group "frameview"

# PROP Default_Filter ""
# End Group
# Begin Group "bgselect"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\BgSelectWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\BgSelectWnd.h
# End Source File
# Begin Source File

SOURCE=.\Thumbs.cpp
# End Source File
# Begin Source File

SOURCE=.\Thumbs.h
# End Source File
# End Group
# Begin Group "clon"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\CloneFrameWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\CloneFrameWnd.h
# End Source File
# End Group
# Begin Group "voice"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\adpcmcodec.cpp
# End Source File
# Begin Source File

SOURCE=.\adpcmcodec.h
# End Source File
# Begin Source File

SOURCE=.\gsmcodec.cpp
# End Source File
# Begin Source File

SOURCE=.\gsmcodec.h
# End Source File
# Begin Source File

SOURCE=.\main.cpp
# End Source File
# Begin Source File

SOURCE=.\main.h
# End Source File
# Begin Source File

SOURCE=.\rtp.cpp
# End Source File
# Begin Source File

SOURCE=.\rtp.h
# End Source File
# Begin Source File

SOURCE=.\singleton.h
# End Source File
# Begin Source File

SOURCE=.\soundbuffer.h
# End Source File
# Begin Source File

SOURCE=.\VoiceInit.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\Tong.rc"
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\VideoView.cpp
# End Source File
# Begin Source File

SOURCE=.\VideoView.rc
# End Source File
# Begin Source File

SOURCE=.\VideoViewDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\VideoView.h
# End Source File
# Begin Source File

SOURCE=.\VideoViewDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\away.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg0.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg0.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg1.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg1.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg2.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg2.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg3.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg3.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg4.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg4.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg5.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg5.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg6.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg6.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg7.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg7.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg8.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg8.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\img\bgselect\bg9.bmp"
# End Source File
# Begin Source File

SOURCE=.\res\img\bgselect\bg9.bmp
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\block.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\calling.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\drag.cur"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\eating.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\ico\file.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\Hand.cur"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\idl2.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\idl3.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\idl4.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\offline.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\online.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\ico\talk.ico"
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\Tong.ico"
# End Source File
# Begin Source File

SOURCE=.\res\VideoView.ico
# End Source File
# Begin Source File

SOURCE=.\res\VideoView.rc2
# End Source File
# Begin Source File

SOURCE="..\..\개발용\Tong\Tong\res\tong_ico\working.ico"
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
