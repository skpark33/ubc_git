// ChooseFrame.cpp : implementation file
//

#include "stdafx.h"
#include "ChooseFrame.h"
#include "VideoViewDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChooseFrame dialog

CChooseFrame::CChooseFrame(CWnd* pParent /*=NULL*/)
	: CDialog(CChooseFrame::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChooseFrame)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    m_nRate = 0;//((CVideoViewDlg*)GetParent())->m_nFrmRate;
	m_nQuality = 0;//((CVideoViewDlg*)GetParent())->m_nGifQuality;
}


void CChooseFrame::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChooseFrame)
	DDX_Control(pDX, IDC_STATIC_QUALITY, m_stQuality);
	DDX_Control(pDX, IDC_SLIDER_QUALITY, m_sliderQuality);
	DDX_Control(pDX, IDC_SLIDER_FRAME, m_sliderFrame);
	DDX_Control(pDX, IDC_STATIC_FRAME, m_stFrame);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChooseFrame, CDialog)
	//{{AFX_MSG_MAP(CChooseFrame)
    	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_FRAME, OnReleasedcaptureSliderFrame)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_QUALITY, OnReleasedcaptureSliderQuality)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChooseFrame message handlers

void CChooseFrame::OnCancel() 
{

	CDialog::OnCancel();
}

void CChooseFrame::OnOK() 
{

    m_nRate = m_sliderFrame.GetPos();

	CString ok;
	ok.Format("%d %d",m_nRate,m_nQuality);
	AfxMessageBox(ok);
    ((CVideoViewDlg*)GetParent())->SetVideoFrmRate( m_nRate );   //�����Ѵ�..
	((CVideoViewDlg*)GetParent())->SetGifQuality( m_nQuality );


	CDialog::OnOK();
}
BOOL CChooseFrame::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
    m_sliderFrame.SetRange( 3,20);
    m_sliderFrame.SetPos( m_nRate);

	m_sliderQuality.SetRange( 15, 75);
    m_sliderQuality.SetPos( m_nQuality);

    CString str;
    str.Format("%dFrame/sec",m_nRate);
    m_stFrame.SetWindowText(str);

	str.Format("%d",m_nQuality + 25);
	m_stQuality.SetWindowText(str);
    
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChooseFrame::OnReleasedcaptureSliderFrame(NMHDR* pNMHDR, LRESULT* pResult) 
{
    m_nRate = m_sliderFrame.GetPos();

    CString str;
    str.Format("%dFrame/sec",m_nRate);

    m_stFrame.SetWindowText(str);
    
	*pResult = 0;
}

void CChooseFrame::OnReleasedcaptureSliderQuality(NMHDR* pNMHDR, LRESULT* pResult) 
{
	m_nQuality = m_sliderQuality.GetPos();

    CString str;
    str.Format("%d",m_nQuality + 25);
    m_stQuality.SetWindowText(str);
	
	*pResult = 0;
}
