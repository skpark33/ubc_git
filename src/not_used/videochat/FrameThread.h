#if !defined(AFX_FRAMETHREAD_H__5656F91B_DC57_46E0_9F2B_E5DE611B9045__INCLUDED_)
#define AFX_FRAMETHREAD_H__5656F91B_DC57_46E0_9F2B_E5DE611B9045__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WM_THREADMSG_SETMYFRM		WM_USER + 311
#define WM_THREADMSG_VIEWPARTNER	WM_USER + 312
#define WM_THREADMSG_SENDFRAME		WM_USER + 313

class CFrameThread : public CWinThread
{
	DECLARE_DYNCREATE(CFrameThread)

public:
	CFrameThread();
	virtual ~CFrameThread();

// Attributes
public:
	CWnd * m_pWnd;
// Operations
public:
	// skpark [ 함수반환형이 불일치하여 void 로 교체하였다.
	void OnSendFrame( WPARAM wParam, LPARAM lParam);
	void OnViewPartner( WPARAM wParam, LPARAM lParam);
	void OnSetMyFrm( WPARAM wParam, LPARAM lParam );

	//LRESULT OnSendFrame( WPARAM wParam, LPARAM lParam);
	//LRESULT OnViewPartner( WPARAM wParam, LPARAM lParam);
	//LRESULT OnSetMyFrm( WPARAM wParam, LPARAM lParam );
	
	// skpark ]


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFrameThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFrameThread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FRAMETHREAD_H__5656F91B_DC57_46E0_9F2B_E5DE611B9045__INCLUDED_)
