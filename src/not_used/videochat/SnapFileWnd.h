#if !defined(AFX_SNAPFILEWND_H__1E081D8B_FDEA_402E_82B7_1C5198404B7B__INCLUDED_)
#define AFX_SNAPFILEWND_H__1E081D8B_FDEA_402E_82B7_1C5198404B7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SnapFileWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSnapFileWnd dialog
#include "PictureView.h"   
#include "resource.h"  

class CSnapFileWnd : public CDialog
{
// Construction
public:
	void LoadFile( CString strFile);
	void DrawBG(CDC* pDC);
	CSnapFileWnd(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSnapFileWnd)
	enum { IDD = IDD_SNAPFILE_WND };
	CPictureView	m_snapView;
	CStatic	    m_stFilePath;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSnapFileWnd)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSnapFileWnd)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnStSnapFilepath();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SNAPFILEWND_H__1E081D8B_FDEA_402E_82B7_1C5198404B7B__INCLUDED_)
