/*-----------------------------------------------------------------------------
| kjh                                                                          |
|------------------------------------------------------------------------------|
| VOICE TOOL KIT Ver 1.0                                                       |
| Date: 2000.12.20                                                             |
| Tool: Visual C++ 6.0                                                         |
-----------------------------------------------------------------------------*/
#include "stdafx.h"
#include "main.h"


//쓰레드 ID값은 98등 지원을 위해서 사용해야 한다
DWORD  hThread_ID;
DWORD  hWindowThread_ID;

HWND hwnd;                          //윈도우 메시지용 핸들
//HANDLE hWindowThread;               //윈도우 핸들 쓰레드(윈도우 메시지를 받기 위해서 쓰레드를 돌린다 편법)
//HANDLE hThreadAccept;               //Accetp Thread 접속 쓰레드
HANDLE hThread;											//Network Thread
HANDLE hTimer;											//VAT/RTP Thread
bool ThreadOn=FALSE;	 							//유저가 등록 되면.네트워크 쓰레드를 동작 하도록함
bool SocketInit;                    //소켓으로 접속하냐 그냥 음성만 쓰기 위해서냐..

char G_StatePacket[256]="";         //상태코드와 주소가 동시에 껍데기에 보내기 위한 문자열 변수
int G_CheckCode=PACKET_VOICE_NOT_SEND;//아주 중요한 코드 Accept쓰레드에서 각종 음성전송,중지,접속,종료등을 처리한 값을 가지고 있다 이것은 껍데기에서 중점적으로 쓰인다(아무것도 받지 않는 상태로 초기값을 주었다)
char DebugString[126];              //디버그시 디버그 printf의 문자열 출력
int VOICE_NET_PORT;									//포트!(중요)초기화시 필요
bool ClickOnceState=TRUE;           //음성을 전송 받았음 클릭상태로 되어 다른 사람에게 전송을 받을수 없다
//char DeadLockAddressBuffer[128]="";//현재 음성을 전송중인 클라이언트 주소를 저장함(교착상태를 막기 위함)
char NowSelectAddress[128]="";      //현재 접속한 주소
SOCKET CommandSocket=INVALID_SOCKET;//Command socket 명령소켓
//SOCKET ControlSocket=INVALID_SOCKET;//RTP/VAT control port socket제어 포트 소켓
//SOCKET ClientSocket=INVALID_SOCKET; //TCP/IP접속을 하는 소켓
//SOCKET ServerSocket=INVALID_SOCKET; //TCP/IP접속을 받는 소켓
SoundBuffer receivedSoundBuffer;    //Sound buffer from network
VOICE_DATA_NODE VoiceInfo;      		//음성데이타노드 
int currentInputLength;							//현재 기대되는 입력 버퍼의 길이
int currentInputSamples;						//현재 입력 버퍼의 Samples
//Sampling이란 sound를 녹음하는 작업을 말하는데 sampling rate라는 것을 이해할 필요가 있다. 이것은 sound를 녹음할 때 사용하는 주파수를 말한다. 보통 음성은 4,000∼8,000Hz, 음향은 11,025∼22,025Hz사이, 음악을 넣을 경우에는 22,025Hz이상을 사용해야 한다. 또한 bit수에 따라서 음질의 차이가 생긴다. 8bit의 경우 256음정을 표현할 수 있고 16bit의 경우 65,536음정까지 표현할 수 있다. 
char *rtpsdes=NULL;			  	  			//RTP SDES packet
int rtpsdesl;					  						//RTP SDES packet length

long packetsReceived=0;							//Network packet traffic counters
long packetsSent=0;
long inputPacketsLost=0;						//Input packets lost due to half-duplex
long outputPacketsLost=0;						//Output packets lost due to net traffic

extern unsigned long ssrc;					//RTP synchronisation source identifier
extern int samplesPerSecond;				//셈플당 비트 예>22KHz 22050
extern int OutputActive;						//셈플 second
extern int bitsPerSample;						//Audio hardware is half-duplex (반이중)
extern HWAVEOUT hWaveOut;						//wave 출력 파일이 열려있나?
extern int halfDuplex;							//wave 출력 핸들
extern HWAVEIN hWaveIn;							//wave 입력 핸들
extern int InputActive;     				//wave 입력 파일이 열려있나(Control에서 쓰인다)
extern bool VoiceAllSending;        //모두에게 전송 할까?
extern int NoiseProcess;						//값으로 음성 입력 커트라인 조절값(잡음 소음등을 절절하게 없애준다)
extern int AudioChannel;		  	    //오디오 입력 채널(1:mone,2:streo)(1로하면 여자목소리:2로하면남자목소리났다)
extern int InputStop;	              //wave 입력이 멈추어져있는가
extern int AdpcmCodecCompression;   //ADPCM compression mode
extern int GsmCodecCompression;     //GSM compression mode
extern int WriteHandle;							//저수준 녹음용 파일쓰기 핸들
extern int ReadHandle;							//저수준 녹음용 파일읽기 핸들
extern bool VoiceRecordSave;        //녹음하기 위함 녹음/입출력 Flag이다 TRUE이면 녹음버퍼에 if문에 들어간다
extern int CurrentCodecSize;//현재코덱사이즈
extern long CurrentCompression;//현재 압축방식
extern LPWAVEHDR WavePlay;			//음성출력
//bool G_Recording;//레코딩 중인지?
extern long outputPending;								 //큐의 출력 버퍼


//IP를 검사한다
bool IpChecking(char* Address)
{
	LPHOSTENT lpHostEntry;    
	struct in_addr iaHost;
	iaHost.s_addr=inet_addr(Address);//인터넷주소로 바꾼다
	if(iaHost.s_addr==INADDR_NONE) lpHostEntry=gethostbyname(Address);//호스트 이름을 알아낸다 
	else lpHostEntry=gethostbyaddr((const char*)&iaHost,sizeof(struct in_addr),AF_INET);//호스트 주소의 정보를 알아낸다
	if(lpHostEntry==NULL){return FALSE;}//알수 없는 호스트 이다
	else{return TRUE;}//호스트가 있다
}

//자기 컴퓨터의 호스트 주소를 얻어낸다
char* LocalComputerAddress(void)
{
	//자기 자신의 주소를 얻는 루틴
	char *LocalIP;
	char name[255]="";
	PHOSTENT hostinfo;
	if(gethostname(name,sizeof(name))==0)
	{                          
		if((hostinfo=gethostbyname(name))!=NULL)
			LocalIP=(char*)inet_ntoa(*(struct in_addr*)*hostinfo->h_addr_list);
	}//서버는 자기 자신의 주소를 가지고 있어야 함!! 그래야 상대방 접속을 구분함! 콜콜 오케바리슛
	return LocalIP;
}

//음성 데이타를 보냈으니 너는 버튼을 누르지말고 듣기만 하라고 지시하는 메시지를 보낸다
//아주 중요하다 이부분은 클라이언트 껍데기 프로그램에서 돌려주며 체크해야 한다
//첫번째 인자는 오퍼레이션 코드로 현재 메시지를 받아 가동되었다면 OperationCode를 TRUE로
//보내주기때문에 리턴하여 프로그램 상태를 파악할 수 있다 
int VoiceDateReceiveCheck(int OperationCode,int CheckData)
{
	if(OperationCode==TRUE)//만약 코드를 보내왔다면 메시지 확인을 리턴해준다
	{
		switch(CheckData)//보내주는 현재 메시지 코드
		{
		case PACKET_VOICESENDING_START:return PACKET_VOICESENDING_START;//음성 전송 시작
		case PACKET_VOICESENDING_STOP :return PACKET_VOICESENDING_STOP;//음성 전송 해제
		case PACKET_VOICE_CONNECT_TEST:return PACKET_VOICE_CONNECT_TEST;//상대방이 접속해있는지 Checking
		case PACKET_VOICE_CONNECT_QUIT:return PACKET_VOICE_CONNECT_QUIT;//상대방이 프로그램을 종료 했으므로 접속을 해제시켜주기 위함
		default:return PACKET_VOICE_NOT_SEND;//음성 전송을 하지 않는 상태
		}
	}
	else return PACKET_VOICE_NOT_SEND;
}

//소켓을 모두 닫아준다
void SocketClose()
{
//	CloseSocket(CommandSocket);
//	CloseSocket(ControlSocket);
//	CloseSocket(VoiceInfo.ServerOption);
//	CloseSocket(VoiceInfo.ServerReceive);
}

//유저 데이타 정보를 생성 초기화 시킨다
bool UserDataCreate(char *address)
{
	//bool state;
	char HostName[128]="";//호스트이름을 저장할 변수
	SOCKADDR_IN sockHost;//주소를 알아내기 위한 변수
	HostInfo(address,HostName,&sockHost.sin_addr);//주소를 넣어서 호스트의 정보를 알아낸다
	
	VoiceInfo.ClientState=NewCreate;//Wave상태
	VoiceInfo.VoiceInputState=FALSE;//Wave입력을원하는가?
//	VoiceInfo.ServerReceive=INVALID_SOCKET;//접속소켓
	//VoiceInfo.ServerOption=INVALID_SOCKET;//RTP/VAT정보소켓
	VoiceInfo.ClientAddress.sin_addr.s_addr=sockHost.sin_addr.s_addr;//주소설정
	VoiceInfo.CurrentProtocol=RTP_PROTOCOL;//프로토콜설정
	VoiceInfo.CurrentPort=VOICE_NET_PORT;//포트설정
	VoiceInfo.localLoopback=(ntohl((long)(sockHost.sin_addr.s_addr))==0x7F000001);
	VoiceInfo.llhead=NULL;
	VoiceInfo.lltail=NULL;
	strcpy(VoiceInfo.CurrentHostName,HostName);//현재 호스트의 도메인명
	strcpy(NowSelectAddress,address);
	if(UserSocketCreate())
	{
		ThreadOn=TRUE;//쓰레드함수가 동작하도록한다(생성되었을 경우만 쓰레드를 생성한다)네트워크 쓰레드를 가능하게 하기 위함
		return TRUE;
	}
	else return FALSE;
}

//유저의 소켓을 생성한다
bool UserSocketCreate()
{
	int serr=0;
	//	int ConnectState;
//	serr=CreateSocket(&VoiceInfo.ServerReceive,SOCK_DGRAM,htonl(INADDR_ANY),0);//소켓
	VoiceInfo.AcceptAddress.sin_family=PF_INET;
	VoiceInfo.AcceptAddress.sin_addr=VoiceInfo.ClientAddress.sin_addr;
	VoiceInfo.AcceptAddress.sin_port=htons(VoiceInfo.CurrentPort);
	
	//상대방의 알려진 주소를 보관하고 있다가 통신이 이루어지면 socket계층에서 상대방주소(서버)로
	//데이터를 전송한다.이렇게 함으로써 비연결 클라이언트는 매번 서버에 데이터를 전송할 때마다 서버와
	//연결을 할 필요가 없다
/*	
	serr=CreateSocket(&VoiceInfo.ServerOption,SOCK_DGRAM,htonl(INADDR_ANY),0);//소켓
	VoiceInfo.OptionAddress.sin_family=PF_INET;//internetwork:UDP,TCP.etc.
	VoiceInfo.OptionAddress.sin_addr=VoiceInfo.ClientAddress.sin_addr;//주소설정
	VoiceInfo.OptionAddress.sin_port=htons(VoiceInfo.CurrentPort+1);//포트설정
*/	
	return TRUE;
}

//네트워크 Read/Write
//unsigned long __stdcall NetworkThread(void *Arg)
void NetworkThread(void *Arg)
{
	/*
	WSAEVENT hRecvEvent=WSACreateEvent();//윈속 이벤트 오브젝트를 초기화하고 그 핸들을 넘긴다
	WSANETWORKEVENTS event_command;//네트윅 이벤트를 저장할 구조체
	memset(&event_command,0,sizeof(event_command));//이벤트발생후 새로운 이벤트를 넣기 위한 초기화
	WSAEventSelect(CommandSocket,hRecvEvent,FD_READ);//음성을 받는 소켓
	while(TRUE)
	{
		WSAEnumNetworkEvents(CommandSocket,hRecvEvent,&event_command);//이벤트가 발생되는 소켓을 넣어준다
		
		if((event_command.lNetworkEvents & FD_READ)==FD_READ  && ThreadOn==TRUE)//읽기가 가능한가(서버로 받아서 음성출력)
		{
			if(event_command.iErrorCode[FD_READ_BIT]==0) //DataReceive(CommandSocket,Create);
				DataReceive(CommandSocket);
		}

		Sleep(1);//Cpu의 부하를 방지 하기 위해 1을 주었다
	}*/
	return;
}

//보이스툴킷을초기화시킨다
bool VoiceToolKitInit(int port,int NoiseCleanup,int CodecMode)
{ 
	SocketInit=TRUE;
	AudioChannel=MONO;//RecordOption;//Mono,Streo(녹음입력옵션(모노,스테레오))
	//음성소음을 제거하기 위한 깔끔한 음성을 전송하기 위한 기법
	if(NoiseCleanup==0) NoiseProcess=350;//0으로 하면 default값으로 정해진다
	else NoiseProcess=NoiseCleanup;//소음제거 하기 위한 커트라인 값을 집어넣는다(NoiseProcess는 사운드 버퍼 함수에서 알아서 처리한다 vox)
	VOICE_NET_PORT=port;//포트를 넣어서 VOICE_NET_PORT가 전부 쓰일 수 있도록 해준다
	int serr;//에러코드받기

	rtpsdesl=RTPCreateSdes(&rtpsdes,ssrc,port,FALSE,TRUE);//rtp정보
	currentInputLength=inputBufferLength();//Calculate packet size based on selected modes
    currentInputSamples=inputSampleCount();	
	
	//hWindowThread=CreateThread(NULL,0,(unsigned long (__stdcall*)(void*))WindowHandleThread,0,0,&hWindowThread_ID);//윈도우 메시지를 받는곳!! Call gogo 재일 중요!!
	if(CodecMode==ADPCM_CODEC) AdpcmCodecCompression=TRUE;//ADPCM코덱으로
	else//GSM코덱으로
	{//220.86.104.65 61.100.139.189
		GsmInit();//GSM은 코덱을 초기화 해주어야 한다(끝날때는 반드시 GsmClose으로 끝내줘야함)
		GsmCodecCompression=TRUE;//GSM코덱으로
	}
	//_beginthread(NetworkThread, 0, 0);//쓰레드 가동
	//	hThread=CreateThread(NULL,0,(unsigned long (__stdcall*)(void*))NetworkThread,0,0,&hThread_ID);//네트워크 쓰레드
	_beginthread(WindowCreateMainHandle, 0, 0);//쓰레드 가동
	ThreadOn=TRUE;
	return TRUE;//보이스툴킷 초기화 성공
}


bool VoiceToolKitInit(int NoiseCleanup,int CodecMode)
{ 
	SocketInit=FALSE;
	AudioChannel=MONO;//RecordOption;//Mono,Streo(녹음입력옵션(모노,스테레오))
	//음성소음을 제거하기 위한 깔끔한 음성을 전송하기 위한 기법
	if(NoiseCleanup==0) NoiseProcess=350;//0으로 하면 default값으로 정해진다
	else NoiseProcess=NoiseCleanup;//소음제거 하기 위한 커트라인 값을 집어넣는다(NoiseProcess는 사운드 버퍼 함수에서 알아서 처리한다 vox)
	currentInputLength=inputBufferLength();//Calculate packet size based on selected modes
	currentInputSamples=inputSampleCount();	
	halfDuplex=HardWareChecking();//오디오 하드웨어를 검사한다(반이중,전이중통신이 가능한지)
	//hWindowThread=CreateThread(NULL,0,(unsigned long (__stdcall*)(void*))WindowHandleThread,0,0,&hWindowThread_ID);//윈도우 메시지를 받는곳!! Call gogo 재일 중요!!
	//	SetThreadPriority(hThread, THREAD_PRIORITY_TIME_CRITICAL);
	SetThreadPriority(hThread, THREAD_PRIORITY_NORMAL);
	if(CodecMode==ADPCM_CODEC) AdpcmCodecCompression=TRUE;//ADPCM코덱으로
	else//GSM코덱으로
	{
		GsmInit();//GSM은 코덱을 초기화 해주어야 한다(끝날때는 반드시 GsmClose으로 끝내줘야함)
		GsmCodecCompression=TRUE;//GSM코덱으로
	}
	return TRUE;//보이스툴킷 초기화 성공
}

//VoiceToolKit을 종료시킨다
void VoiceToolKitClose()
{
	if(SocketInit==TRUE)//소켓을 사용하는 초기화였다면...소켓을 사용하는 쓰레드도 종료해야지..
	{
//		TerminateThread(hThread,0);//NetWork/Read/Write쓰레드를 종료시킨다
	}
	if(GsmCodecCompression==TRUE) GsmClose();//GSM코덱은 반드시 끝날때 해줘야함
	if(SocketInit==TRUE) WaveInputStop();
	if(rtpsdes!=NULL) free(rtpsdes);//rtp정보메모리해제
//	if(SocketInit==TRUE) SocketClose();//모든소켓을 닫아준다
//	if(SocketInit==TRUE) WSACleanup();//끝내기전에 윈속의 DLL을 제거한다
//	CloseWindow(hwnd);
}

//설정된 인터넷 주소로 음성 데이타를 보낸다
//int VoiceDataSend(VOICE_DATA_NODE *data,LPSTR buf,int buflen)	
int VoiceDataSend(char *Address,LPSTR buf,int buflen)
{
	int stat=FALSE;
//	while(WSAGetLastError()!=WSAEWOULDBLOCK)
	{
		stat=sendto(CommandSocket,buf,buflen,0,(LPSOCKADDR)&(VoiceInfo.AcceptAddress),sizeof(VoiceInfo.AcceptAddress));
	}
	if(stat==SOCKET_ERROR) stat=FALSE;
	return stat;
}

//음성데이타를 받는다 
void DataReceive(SOCKET Socket)//,VOICE_DATA_NODE *Create)
{
	int rll;
	SOCKADDR_IN addrClient;//받을주소
	int cbAddrClient=sizeof(addrClient);//받을 주소의 크기를 구함
	SoundBuffer *d=&receivedSoundBuffer;// receivedSoundBuffer에 음성을 받아서 가리킨다
	int loopedBack=(Socket==INVALID_SOCKET);//소켓이 죽어있으면 loopedBack에 -1을 넣는다 죽어 있지 않으면 0을 넣는다
	static int RecCount=0;
	/*
	서로 실행하려고 하는데.. 서로서로 먼가가 필요한게 있어서.. 선점을 못하는 상태를 데드락이라고 해요.. 쉽게 예를 들면..
	4거리에서 차들이 모두 서로 가려고 가운데로 몰렸지만.. 누구도 먼저 가지 못하는 현상을 말해요.
	*/
	// Prevent audio deadlock 오디오 데드락을 막는다
	int hdxPacketLost=(RecCount++>0) && (!loopedBack);
	int wasrtp=FALSE;
	int ispurt=!OutputActive;
	if(hdxPacketLost) ++inputPacketsLost;
	if(!loopedBack) DelayLoop();
	--RecCount;
	{
		//소켓으로 부터 데이타를 받는다
		rll=recvfrom(Socket,//받을 소켓
		(CHAR FAR*)d,//받은 데이타
		sizeof(SoundBuffer),//사운드 버퍼의 크기
		0,//Flags
		(SOCKADDR FAR*)&addrClient,//받을 클라이언트 주소
		&cbAddrClient);//받을 클라이언트 주소의 사이즈
		//printf("받았습니다!!!!!!압축방식:%d 보내는호스트:%s  사이즈:%d\n",d->compression,d->sendinghost,sizeof(SoundBuffer));
		//printf("[%d %d %d]\n",d,bitsPerSample,samplesPerSecond);
	}
	if(rll==SOCKET_ERROR)
	{
		//printf("소켓에러!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		return;//소켓에러나면
	}
	++packetsReceived;//패킷을 받았다는것을 증가시킨다
	if(hdxPacketLost || !InitVoiceDevice())
	{
		hdxPacketLost=TRUE;
		return;//보이스 입력 디바이스 장치 초기화
	}
	if(IsRTPpacket((unsigned char*)d,rll))
	{
		
		if(d->buffer.buffer_len==0) return;
		else wasrtp=TRUE;
	}
	if(!wasrtp)
	{
		long xbl;
		d->compression=ntohl(d->compression);
		d->buffer.buffer_len=ntohl(d->buffer.buffer_len);
		xbl=d->buffer.buffer_len+(sizeof(struct SOUND_BUFFER)-SOUNDBUFFER);
		if(xbl!=rll)//패킷이 틀리면 에러이므로 return한다
		{//알수 없는 프로토콜 UNKNOWN_PROTOCOL;
			return;
		}
	}
	if(!hdxPacketLost)//패킷을 잃어 버리지 않으면
	{
		StartWaveOutput(d,bitsPerSample,samplesPerSecond);//스피커로 소리가 난다(자신의 컴퓨터로 음성을 출력한다)
	}
	return;
}

//호스트 정보를 알아낸다
bool HostInfo(char* HostInfo,LPSTR pszHostName,LPIN_ADDR paddr)
{
	char *HostName=pszHostName;;//호스트 이름을 넘긴쪽에 호스트 이름을 저장시킨다
	ULONG laddr;//주소
	LPIN_ADDR ppaddr;//호스트 주소의 구조체
	ppaddr=paddr;
	laddr=inet_addr(HostInfo);//호스트이름
	struct in_addr in;
	in.s_addr=laddr;//주소
	_fmemcpy(ppaddr,&laddr,sizeof(IN_ADDR));//호스트주소
	return TRUE;//return FALSE;
}

extern void Startvoice();
extern int svoice;

WNDPROC OldWndProc;//원래 윈도우 프로시져
//윈도우 이벤트를 받기 위한 곳!!
LRESULT CALLBACK WndProc(HWND hwnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case MM_WIM_DATA:WimData(wParam,lParam);break;//마이크 입력이 들어올때 메시지를 받는다
	case WM_DESTROY:PostQuitMessage(0);break;//종료
	default:return CallWindowProc(OldWndProc,hwnd,uMsg,wParam,lParam);//기본적인 처리를 수행하기 위해 원래 윈도우 프로시저를 호출한다
	}
	return 0;
}

//가상으로 윈도우 창을 만들어준다(마이크 핸들을 얻기위한 !! 최상의 슈퍼 울트라 원더풀 맛세이 기가 테라 맛탱구리구리 가리가리 gogo
void WindowCreateMainHandle(void *argv)
{
	MSG msg;
	HINSTANCE hInstance=NULL;
	hwnd=CreateWindow("STATIC","VirtualVoiceComponent",/*WS_VISIBLE |BS_CENTERWS_VISIBLE*/NULL/*|WS_OVERLAPPEDWINDOW*/,0,0,0,0,NULL,NULL,(HINSTANCE)hInstance,NULL);
	OldWndProc=(WNDPROC)SetWindowLong(hwnd,GWL_WNDPROC,(LONG)WndProc);//서브 클래싱 한다
	while(GetMessage(&msg,NULL,0,0))//윈도우 프로시져로부터 메시지를 받는다
	{
		Sleep(1);//Cpu부하 방지!!
		DispatchMessage(&msg);//DispatchMessage 함수에 의해 WndProc으로 보내진다
	}

	return;
}

//다음 함수는 서식화된 TRACE 함수다
//디버그 창에서 Printf처럼 쓸 수 있다
void TracePrintf(char *format,...)
{
	char buf[1024];
	va_list marker;
	va_start(marker,format);
	vsprintf(buf,format,marker);
	OutputDebugString(buf);
}//예>TracePrintf("ID=%d, Str=%s",id,str);

void mains()
{
	int state;
	char data[100]="";
	printf("사이즈:%d\n",sizeof(int));
	printf("-NetWork Voice Communication Program-\n");
	printf("접속 대기 상태 입니다. 접속할 컴퓨터의 IP를 입력하여주십시오:");
    VoiceToolKitInit(9878,900,ADPCM_CODEC);//음성 엔진을 초기화
	while(TRUE)
	{ 
		if(strcmp("quit",gets(data))==0) break; 
		UserDataCreate(data);
		VoicePlayAll();
		getchar();
	} 
	VoiceToolKitClose();//음성 엔진을 종료시킨다
}
