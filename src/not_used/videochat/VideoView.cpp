// VideoView.cpp : 해당 DLL의 초기화 루틴을 정의합니다.
//

#include "stdafx.h"
#include "VideoView.h"
#include "VideoViewDlg.h"
//#include "afxwin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: 이 DLL이 MFC DLL에 대해 동적으로 링크되어 있는 경우
//		MFC로 호출되는 이 DLL에서 내보내지는 모든 함수의
//		시작 부분에 AFX_MANAGE_STATE 매크로가
//		들어 있어야 합니다.
//
//		예:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// 일반적인 함수 본문은 여기에 옵니다.
//		}
//
//		이 매크로는 MFC로 호출하기 전에
//		각 함수에 반드시 들어 있어야 합니다.
//		즉, 매크로는 함수의 첫 번째 문이어야 하며 
//		개체 변수의 생성자가 MFC DLL로
//		호출할 수 있으므로 개체 변수가 선언되기 전에
//		나와야 합니다.
//
//		자세한 내용은
//		MFC Technical Note 33 및 58을 참조하십시오.
//


//extern "C" void ShowViewDlg(char* RemoteHost , char* RemotePort, char* cx, char* cy, char* nWidth,char* nHeight )
extern "C" __declspec(dllexport) void ShowViewDlg(LPCSTR lpszRemoteHost, LPCSTR lpszRemotePort, CRect rc, CWnd* wnd, LPCSTR lpszBgImage, bool bViewPartner)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());


	CVideoViewDlg* dlg = CVideoViewDlg::getInstance();

	CWnd* pWnd = CWnd::FromHandle(wnd->m_hWnd);

	dlg->Create(IDD_VIDEOVIEW_DIALOG,pWnd);

	dlg->strRemoteHost = lpszRemoteHost;
	dlg->strRemotePort = lpszRemotePort;
	dlg->m_bViewPartner = bViewPartner;


	//dlg->ModifyStyle(0, WS_CLIPSIBLINGS | WS_SYSMENU  );
	//dlg->ModifyStyleEx(0, WS_EX_PALETTEWINDOW );  //작업표시줄에서 제거하자..


	dlg->InitDlgSize(rc, lpszBgImage);
}



extern "C" __declspec(dllexport) void DeleteViewDlg()
{
	
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CVideoViewDlg* dlg = CVideoViewDlg::getInstance();
	if(dlg) dlg->CloseDlg();

	CVideoViewDlg::clearInstance();
}

// CTestdllDlgApp

BEGIN_MESSAGE_MAP(CVideoViewApp, CWinApp)
END_MESSAGE_MAP()


// CTestdllDlgApp 생성

CVideoViewApp::CVideoViewApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CTestdllDlgApp 개체입니다.

//CVideoViewApp theApp;


// CTestdllDlgApp 초기화

BOOL CVideoViewApp::InitInstance()
{
	CWinApp::InitInstance();

	AfxEnableControlContainer();

	CVideoViewDlg* dlg = CVideoViewDlg::getInstance();
	m_pMainWnd = dlg;


	return TRUE;
}

