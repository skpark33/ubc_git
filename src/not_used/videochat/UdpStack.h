#if !defined(AFX_UDPSTACK_H__13FF71B5_F062_4200_9EBB_66D286413890__INCLUDED_)
#define AFX_UDPSTACK_H__13FF71B5_F062_4200_9EBB_66D286413890__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UdpStack.h : header file
//
#define SOCKET_FOR_TEXT    0
#define SOCKET_FOR_VOICE    1
#define SOCKET_FOR_VIDEO    2

#include "VideoViewDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CUdpStack command target
class CVideoViewDlg;
class CUdpStack : public CAsyncSocket
{
// Attributes
public:
    CVideoViewDlg* m_pWnd;
// Operations
public:
	CUdpStack(CVideoViewDlg* pWnd);
    CUdpStack(CVideoViewDlg* pWnd, int n);
	virtual ~CUdpStack();

// Overrides
public:
  	void SetSendtoIP(CString strIP);
	void SetSendtoPort(int nport);
	int m_nListenPort;
	CString m_strSendtoIP;
	int m_nSendtoPort;

	int SendRawData(char* rawData, int nSize);
	void SetMode(int nMode);
	int m_nMode;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUdpStack)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CUdpStack)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UDPSTACK_H__13FF71B5_F062_4200_9EBB_66D286413890__INCLUDED_)
