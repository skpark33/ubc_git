#if !defined(AFX_THUMBS_H__A23F489D_BC68_4E9D_A069_76AB9FD801DC__INCLUDED_)
#define AFX_THUMBS_H__A23F489D_BC68_4E9D_A069_76AB9FD801DC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Thumbs.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CThumbs window
// Return values
#ifndef	BKDLGST_OK
#define	BKDLGST_OK						0
#endif
#ifndef	BKDLGST_INVALIDRESOURCE
#define	BKDLGST_INVALIDRESOURCE			1
#endif
#ifndef	BKDLGST_INVALIDMODE
#define	BKDLGST_INVALIDMODE				2
#endif
#ifndef	BKDLGST_FAILEDREGION
#define	BKDLGST_FAILEDREGION			3
#endif


class CThumbs : public CWnd
{
// Construction
public:
	CThumbs();

// Attributes
public:

// Operations
public:
    void OnPostEraseBkgnd(CDC* pDC);
    LPBYTE Get24BitPixels(HBITMAP hBitmap, LPDWORD lpdwWidth, LPDWORD lpdwHeight);
    HRGN ScanRegion(HBITMAP hBitmap, BYTE byTransR, BYTE byTransG, BYTE byTransB);
    DWORD SetMode(BYTE byMode, BOOL bRepaint = TRUE);
    DWORD SetBitmap(int nBitmap, COLORREF crTransColor = -1L);
	DWORD SetBitmap(HBITMAP hBitmap, COLORREF crTransColor = -1L);  
    void FreeResources(BOOL bCheckForNULL = TRUE);

    DWORD		m_dwWidth;			// Width of bitmap
	DWORD		m_dwHeight;			// Height of bitmap
	BYTE		m_byMode;			// Current drawing bitmap mode
	BOOL		m_bEasyMoveMode;	// Indicates if EasyMove mode must be used
    HBITMAP		m_hBitmap;			// Handle to bitmap
    HRGN		m_hRegion;			// Handle to region
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThumbs)
	//}}AFX_VIRTUAL


    enum	{
				BKDLGST_MODE_TILE = 0,
				BKDLGST_MODE_CENTER,
				BKDLGST_MODE_STRETCH,
				BKDLGST_MODE_TILETOP,
				BKDLGST_MODE_TILEBOTTOM,
				BKDLGST_MODE_TILELEFT,
				BKDLGST_MODE_TILERIGHT,
				BKDLGST_MODE_TOPLEFT,
				BKDLGST_MODE_TOPRIGHT,
				BKDLGST_MODE_TOPCENTER,
				BKDLGST_MODE_BOTTOMLEFT,
				BKDLGST_MODE_BOTTOMRIGHT,
				BKDLGST_MODE_BOTTOMCENTER,

				BKDLGST_MAX_MODES
			};

// Implementation
public:
	virtual ~CThumbs();

	// Generated message map functions
protected:
	//{{AFX_MSG(CThumbs)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THUMBS_H__A23F489D_BC68_4E9D_A069_76AB9FD801DC__INCLUDED_)
