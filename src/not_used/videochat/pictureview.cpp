// pictureview.cpp : implementation file
//

#include "stdafx.h"
#include "pictureview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPictureView

CPictureView::CPictureView()
{
	m_nSizeMode = SIZEMODE_ABSOLUTE ;  //절대사이즈..
//	m_nSizeMode = SIZEMODE_RATIO;
	m_nRatio		= 1.0;			// 비율 사이즈..

	m_nWidth = m_nHeight =0;	//이미지의 사이즈( 가로,세로)
	m_pPicture = NULL;
}

CPictureView::~CPictureView()
{
	if( m_pPicture )
		delete m_pPicture;
}


BEGIN_MESSAGE_MAP(CPictureView, CWnd)
	//{{AFX_MSG_MAP(CPictureView)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CPictureView message handlers

void CPictureView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	
	// Do not call CWnd::OnPaint() for painting messages
}

//이미지를 보여줄 사이즈 설정.. - SIZEMODE_ABSOLUTE일때 유효..
void CPictureView::SetSize(int nWidth, int nHeight)
{
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	Invalidate();
}

//이미지를 SIZEMODE_RATIO로 보여줄때.. 비율 설정..
void CPictureView::SetRatio(double ratio)
{
	m_nRatio = ratio;
}


// 이미지를 리소스에서 로드한다..
bool CPictureView::Load(CString sResourceType, CString sResource)
{
	if(!m_pPicture)
		m_pPicture = new CPicture ();

	bool ret = m_pPicture->Load( sResourceType, sResource);
	Invalidate();
	return ret;
}

//이미지를 파일에서 로드한다..
bool CPictureView::Load(CString sFileName)
{
	if(!m_pPicture)
		m_pPicture = new CPicture ();

	bool ret =  m_pPicture->Load( sFileName);
	Invalidate();
	return ret;
}

BOOL CPictureView::OnEraseBkgnd(CDC* pDC) 
{

//	CMemDC cDC(pDC);		//메모리 DC를 사용하여..한번에 뿌려준다..

	if(! m_pPicture )
		return CWnd::OnEraseBkgnd(pDC);

	switch (m_nSizeMode)
	{
	case SIZEMODE_ABSOLUTE:
//		m_pPicture->Draw(&cDC, CPoint(0,0), CSize(m_nWidth, m_nHeight)); //설정 사이즈로..
		m_pPicture->Draw(pDC, CPoint(0,0), CSize(m_nWidth, m_nHeight)); //설정 사이즈로..
		break;
	case SIZEMODE_RATIO:
//		m_pPicture->Draw(&cDC, CPoint(0,0), m_nRatio);		//비율사이즈로..
		m_pPicture->Draw(pDC, CPoint(0,0), m_nRatio);		//비율사이즈로..
		break;
	}

//	return CWnd::OnEraseBkgnd(pDC);
	return TRUE;
}
