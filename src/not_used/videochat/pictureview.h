#if !defined(AFX_PICTUREVIEW_H__108424E9_B13E_422A_A86F_784047E37B3A__INCLUDED_)
#define AFX_PICTUREVIEW_H__108424E9_B13E_422A_A86F_784047E37B3A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// pictureview.h : header file
//

#include "Picture.h"
//#include "memdc.h"
/////////////////////////////////////////////////////////////////////////////
// CPictureView window
enum {
	SIZEMODE_ABSOLUTE =0,
	SIZEMODE_RATIO	
};


class CPictureView : public CWnd
{
// Construction
public:
	CPictureView();

// Attributes
public:
	CPicture *	m_pPicture;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPictureView)
	//}}AFX_VIRTUAL

// Implementation
public:
	bool Load(CString sFileName);
	bool Load(CString sResourceType, CString sResource);
	void SetRatio(double ratio);
	void SetSize( int nWidth, int mHeight);
	int m_nWidth;
	int m_nHeight;
	double	m_nRatio;
	int m_nSizeMode;
	virtual ~CPictureView();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPictureView)
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PICTUREVIEW_H__108424E9_B13E_422A_A86F_784047E37B3A__INCLUDED_)
