#if !defined(AFX_BGSELECTWND_H__566F1904_2C4C_41E9_BD0D_AD7431260992__INCLUDED_)
#define AFX_BGSELECTWND_H__566F1904_2C4C_41E9_BD0D_AD7431260992__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BgSelectWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBgSelectWnd dialog
#include "Thumbs.h"
#include "resource.h"

class CBgSelectWnd : public CDialog
{
// Construction
public:
    CWnd* m_pWnd;
	int m_SelectImg;
    bool& m_existBackGroundDialog ;
    CRect m_btnRect;

  	CBgSelectWnd(CRect rect,bool& pExist, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBgSelectWnd)
	enum { IDD = IDD_BG_SELECT_WND };
	CThumbs m_BG0;
	CThumbs	m_BG1;
	CThumbs	m_BG2;
	CThumbs	m_BG3;
	CThumbs	m_BG4;
	CThumbs m_BG5;
	CThumbs m_BG6;
	CThumbs m_BG7;
	CThumbs m_BG8;
	CThumbs m_BG9;


		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBgSelectWnd)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CBrush m_brush;

	// Generated message map functions
	//{{AFX_MSG(CBgSelectWnd)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnNcDestroy();
	afx_msg void OnPaint();
	afx_msg void OnButtonClick( UINT id );
	afx_msg void OnHook();
	afx_msg void OnHookStop();
	
	// skpark void --> LRESULT
	//afx_msg void OnHookMsg(WPARAM wParam , LPARAM lParam);
	afx_msg LRESULT OnHookMsg(WPARAM wParam , LPARAM lParam);
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BGSELECTWND_H__566F1904_2C4C_41E9_BD0D_AD7431260992__INCLUDED_)
