#if !defined(AFX_CHOOSEFRAME_H__7073E2D8_3B98_4B98_AB9A_51CB0666BD88__INCLUDED_)
#define AFX_CHOOSEFRAME_H__7073E2D8_3B98_4B98_AB9A_51CB0666BD88__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChooseFrame.h : header file
//
#include "resource.h"

class CChooseFrame : public CDialog
{
// Construction
public:
//	CChooseFrame(CWnd* pParent = NULL);   // standard constructor
   	int m_nRate;
	int m_nQuality;
	CChooseFrame( CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(CChooseFrame)
	enum { IDD = IDD_CHOOSEFRAME };
    	CStatic	m_stQuality;
	CSliderCtrl	m_sliderQuality;
	CSliderCtrl	m_sliderFrame;
	CStatic	m_stFrame;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChooseFrame)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChooseFrame)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnReleasedcaptureSliderFrame(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReleasedcaptureSliderQuality(NMHDR* pNMHDR, LRESULT* pResult);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHOOSEFRAME_H__7073E2D8_3B98_4B98_AB9A_51CB0666BD88__INCLUDED_)
