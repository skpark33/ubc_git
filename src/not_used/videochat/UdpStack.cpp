// UdpStack.cpp : implementation file
//

#include "stdafx.h"
#include "UdpStack.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUdpStack

CUdpStack::CUdpStack(CVideoViewDlg* pWnd)
{
    m_pWnd = pWnd;
    SetMode(SOCKET_FOR_VIDEO);
}

CUdpStack::CUdpStack(CVideoViewDlg* pWnd, int n)
{
	m_pWnd = NULL;
    SetMode(SOCKET_FOR_VIDEO);
}


CUdpStack::~CUdpStack()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CUdpStack, CAsyncSocket)
	//{{AFX_MSG_MAP(CUdpStack)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

extern void DataReceive(SOCKET Socket);

void CUdpStack::OnReceive(int nErrorCode) 
{
    CString rSocketAddress=_T("");;
    UINT rSocketPort=0;

/* old 버전..
    char buf[20000];
    int recvd = ReceiveFrom( buf, 20000, rSocketAddress,rSocketPort);

    LPBITMAPINFO p = (LPBITMAPINFO)buf;
    int headSize = p->bmiHeader.biSize;
    int dataSize = p->bmiHeader.biSizeImage;
    LPVOID pImg = GlobalAlloc(GMEM_FIXED, dataSize + headSize+1);
    CopyMemory( (LPBYTE)pImg, buf, headSize + dataSize);
    m_pWnd->ViewPartner((char*)pImg);
    GlobalFree(pImg);
*/

	//UI메시지 처리.. 
//	GetApp()->PeekAndPump(); 느려진다..


    if( nErrorCode == WSAENETDOWN)  {
        AfxMessageBox("Socket Error");
        return ;
    }

    switch (m_nMode) {
    case    SOCKET_FOR_VOICE :
        {
			DataReceive(this->m_hSocket);
        }
        break;

    case    SOCKET_FOR_VIDEO :
        {
			Sleep(10);				
/*		STUN 을 사용하기전..
            char buf[20000];
            int recvd = ReceiveFrom( buf, 20000, rSocketAddress,rSocketPort);


            if( recvd <=0 || buf == NULL || rSocketAddress.IsEmpty() || rSocketPort==0) {
                break;
            }

            LPBITMAPINFO p = (LPBITMAPINFO)buf;
            int headSize = p->bmiHeader.biSize;
            int dataSize = p->bmiHeader.biSizeImage;

            LPVOID pImg = GlobalAlloc(GMEM_FIXED, dataSize + headSize+1);
            CopyMemory( (LPBYTE)pImg, buf, headSize + dataSize);

			//상대방에 따라서 다르게 뷰를 해야하겠지?//
            m_pWnd->ViewPartner((char*)pImg , rSocketAddress, rSocketPort);
*/
			char buf[30000]="";
			int recvd = ReceiveFrom( buf, 30000, rSocketAddress,rSocketPort);
            if( recvd <=0 || buf == NULL || rSocketAddress.IsEmpty() || rSocketPort==0) {
                break;
            }

			//case Receive from Turn server//
//			if( rSocketAddress == GetApp()->GetTurnServerAddr()) 
            if( 0)
			{
				char *s = strchr(buf,'/');
				*s = '\0';
				char ip[20]="";
				strcpy( ip, buf);
				char* s2 = strchr( ++s, '/');
				*s2 = '\0';
				char port[6]="";
				strcpy( port, s);
				++s2;
				LPBITMAPINFO p = (LPBITMAPINFO)s2;
				int headSize = p->bmiHeader.biSize;
				int dataSize = p->bmiHeader.biSizeImage;

				LPVOID pImg = GlobalAlloc(GMEM_FIXED, dataSize + headSize+1);
				CopyMemory( (LPBYTE)pImg, s2, headSize + dataSize);
				//상대방에 따라서 다르게 뷰를 해야하겠지?//
		        m_pWnd->ViewPartner((char*)pImg , recvd, ip, atoi(port));
			}
			else
			{

/*	 for vcm..

				LPBITMAPINFO p = (LPBITMAPINFO)buf;
	            int headSize = p->bmiHeader.biSize;
		        int dataSize = p->bmiHeader.biSizeImage;

			    LPVOID pImg = GlobalAlloc(GMEM_FIXED, dataSize + headSize+1);
				CopyMemory( (LPBYTE)pImg, buf, headSize + dataSize);
				//상대방에 따라서 다르게 뷰를 해야하겠지?//
				m_pWnd->ViewPartner((char*)pImg , rSocketAddress, rSocketPort);
*/

				//for jpeg codec..
				LPVOID pJpg = GlobalAlloc(GMEM_FIXED, recvd );
				CopyMemory( (LPBYTE)pJpg, buf, recvd);
				//상대방에 따라서 다르게 뷰를 해야하겠지?//

//                TRACE("\n CUdpStack:OnRecevie => %s:%d ,  %dbytes",rSocketAddress, rSocketPort, recvd );

                if( m_pWnd)	//mainav
    				m_pWnd->ViewPartner((char*)pJpg ,recvd, rSocketAddress, rSocketPort);
			}
        }
        break;
    }
    
	CAsyncSocket::OnReceive(nErrorCode);
}


void CUdpStack::SetMode(int nMode)
{
    m_nMode = nMode;    
}

int CUdpStack::SendRawData(char *rawData, int nSize)
{
    return 1;
}



void CUdpStack::SetSendtoPort(int nport)
{
    m_nSendtoPort = nport;
}

void CUdpStack::SetSendtoIP(CString strIP)
{
    m_strSendtoIP = strIP;
}
