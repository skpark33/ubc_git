// FrameViewer.cpp : implementation file
//

#include "stdafx.h"
#include "FrameViewer.h"
#include "CISBitmap.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFrameViewer

CFrameViewer::CFrameViewer()
{
    m_BgBitmap = NULL;
    m_nTransColor = RGB(0,0,0);
    imageData = NULL;
    m_bUseBackImage = FALSE;
    m_bRec = FALSE;
    m_strRecFileName = _T("");
    paviFile = NULL;

	m_nRemotePort = 0;
	m_szRemoteAddress = _T("");
	m_bMyView = false;
//	m_pUser = NULL;
	m_pCloneWnd = NULL;
}

CFrameViewer::~CFrameViewer()
{
   	if(imageData)
	{	HGLOBAL hg = GlobalHandle(imageData);
		GlobalUnlock(hg);
		GlobalFree(hg);
	}
}


BEGIN_MESSAGE_MAP(CFrameViewer, CWnd)
	//{{AFX_MSG_MAP(CFrameViewer)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CFrameViewer message handlers

BOOL CFrameViewer::SetFrame(LPBITMAPINFO lpBi)
{
	if(GetSafeHwnd())
	{
	//    memcpy( &m_BitmapInfo , lpBi, sizeof( BITMAPINFO));
		m_sizeImage.cx = lpBi->bmiHeader.biWidth;
		m_sizeImage.cy = lpBi->bmiHeader.biHeight;
		m_ImageBitmap.CreateFromDib(lpBi);

		//배경이미지가 있다면 깔자..//
		if( m_bUseBackImage && m_BgBitmap ) {
			CDC *pImgDC=m_ImageBitmap.BegingModify();	
			if( pImgDC )
				m_BgBitmap->DrawTransparent(pImgDC, 0, 0,m_sizeImage.cx , m_sizeImage.cy, RGB(255,255,255));

			m_ImageBitmap.EndModify();
		}

		//파일로 저장하자..
		if( paviFile && m_bRec) {
			paviFile->AppendNewFrame((HBITMAP)(m_ImageBitmap.GetSafeHandle()));
		}

		if(m_hWnd && IsWindowVisible())
			InvalidateRect(NULL);
	}
    return TRUE;
}

void CFrameViewer::SetFrameSnap(LPBITMAPINFO lpBi)
{
    m_ImageBitmap.CreateFromDib(lpBi);

    CDC *pDC=m_ImageBitmap.BegingModify();	
	pDC->SetTextColor(RGB(255,255,255));
	pDC->SetBkMode(TRANSPARENT);
	CSize sz=m_ImageBitmap.GetSize();
	char str[32];
	char str2[32];
	CString title;
	
	title.Format("%s %s", _strdate(str2),_strtime(str));;
	pDC->TextOut(2,sz.cy-16, title);
    //////////////////////  로고 박기/////////////////////////
    ///////////////////////////////////////////////////////////
	m_ImageBitmap.EndModify();

    if(IsWindowVisible())
		InvalidateRect(NULL);
    
}

void CFrameViewer::SetFrameSnap(LPCSTR filename)
{
    m_ImageBitmap.Open(filename);

    if(IsWindowVisible())
		InvalidateRect(NULL);
    
}

void CFrameViewer::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	// Do not call CWnd::OnPaint() for painting messages

	CRect rect;
	GetClientRect(rect);
/*
	CPen penRect(PS_SOLID, 0, RGB(189,189,189) );   // Dark gray
	CPen penShadow(PS_SOLID,0, RGB(226,226,226));   // Light gray

	CPen* pOldPen = dc.SelectObject(&penRect);
	// 테두리..//
	dc.MoveTo(0,0);
	dc.LineTo( rect.Width()-2, 0);
	dc.LineTo( rect.Width()-2, rect.Height());
	dc.LineTo( 0, rect.Height());
	dc.LineTo( 0, 0);

	dc.SelectObject(&penShadow);
	//그림자2.
	dc.MoveTo( rect.Width(), 0);
	dc.SetBkColor( RGB( 226,226,226));
	dc.LineTo( rect.Width(), rect.Height());

   	dc.SelectObject(pOldPen);
 
	penRect.DeleteObject();
	penShadow.DeleteObject();
*/

if( m_ImageBitmap.GetSafeHandle() && m_bMyView )
	{
			if(m_rgn.m_hObject)
				dc.SelectClipRgn(&m_rgn);

			CRect rect;
			GetClientRect(rect);
			CRect to(CPoint(2,2),CSize(rect.Width()-5,rect.Height()-3));   //사이즈를 조절하자..

			dc.SetStretchBltMode(HALFTONE) ;
			m_ImageBitmap.StretchBlt(&dc, to);

    }
    else
	{
		if(m_image.IsValid())
		{
			m_image.Draw(dc.m_hDC, rect);
		}

    }
}

CBitmapEx* CFrameViewer::GetBitPtr()
{
    if( m_ImageBitmap.GetSafeHandle()) {
        return &m_ImageBitmap;
    }
    return NULL;
}

//백그라운드 이미지 파일..
void CFrameViewer::SetBGImg(int nResource, COLORREF crTransColor, BOOL bUse)
{
    if( m_BgBitmap ) 
	{
        delete m_BgBitmap;
    }
    m_BgBitmap = new CCISBitmap;
    m_BgBitmap->LoadBitmap( nResource);
    m_nTransColor = crTransColor;

    SetUseBackImage( bUse);
}

HBITMAP CFrameViewer::CreateBitmapMask(HBITMAP hSourceBitmap, DWORD dwWidth, DWORD dwHeight, COLORREF crTransColor)
{
	HBITMAP		hMask		= NULL;
	HDC			hdcSrc		= NULL;
	HDC			hdcDest		= NULL;
	HBITMAP		hbmSrcT		= NULL;
	HBITMAP		hbmDestT	= NULL;
	COLORREF	crSaveBk;
	COLORREF	crSaveDestText;

	hMask = ::CreateBitmap(dwWidth, dwHeight, 1, 1, NULL);
	if (hMask == NULL)	return NULL;

	hdcSrc	= ::CreateCompatibleDC(NULL);
	hdcDest	= ::CreateCompatibleDC(NULL);

	hbmSrcT = (HBITMAP)::SelectObject(hdcSrc, hSourceBitmap);
	hbmDestT = (HBITMAP)::SelectObject(hdcDest, hMask);

	crSaveBk = ::SetBkColor(hdcSrc, crTransColor);

	::BitBlt(hdcDest, 0, 0, dwWidth, dwHeight, hdcSrc, 0, 0, SRCCOPY);

	crSaveDestText = ::SetTextColor(hdcSrc, RGB(255, 255, 255));
	::SetBkColor(hdcSrc,RGB(0, 0, 0));

	::BitBlt(hdcSrc, 0, 0, dwWidth, dwHeight, hdcDest, 0, 0, SRCAND);

	SetTextColor(hdcDest, crSaveDestText);

	::SetBkColor(hdcSrc, crSaveBk);
	::SelectObject(hdcSrc, hbmSrcT);
	::SelectObject(hdcDest, hbmDestT);

	::DeleteDC(hdcSrc);
	::DeleteDC(hdcDest);

	return hMask;
} // End of CreateBitmapMask

DWORD CFrameViewer::SetBitmaps(int nBitmapIn, COLORREF crTransColorIn)
{
	HBITMAP		hBitmapIn		= NULL;
	HINSTANCE	hInstResource	= NULL;
	
	// Find correct resource handle
	hInstResource = AfxFindResourceHandle(MAKEINTRESOURCE(nBitmapIn), RT_BITMAP);

	// Load bitmap In
	hBitmapIn = (HBITMAP)::LoadImage(hInstResource, MAKEINTRESOURCE(nBitmapIn), IMAGE_BITMAP, 0, 0, 0);

	return SetBitmaps(hBitmapIn, crTransColorIn) ;
} // End of SetBitmaps

DWORD CFrameViewer::SetBitmaps(HBITMAP hBitmapIn, COLORREF crTransColorIn)
{
	int		nRetValue = 0;
	BITMAP	csBitmapSize;

	// Free any loaded resource
	FreeResources();

	if (hBitmapIn)
	{
		m_csBitmaps[0].hBitmap = hBitmapIn;
		m_csBitmaps[0].crTransparent = crTransColorIn;
		// Get bitmap size
		nRetValue = ::GetObject(hBitmapIn, sizeof(csBitmapSize), &csBitmapSize);
		if (nRetValue == 0)
		{
			FreeResources();
			return 0;
		} // if
		m_csBitmaps[0].dwWidth = (DWORD)csBitmapSize.bmWidth;
		m_csBitmaps[0].dwHeight = (DWORD)csBitmapSize.bmHeight;

		// Create mask for bitmap In
		m_csBitmaps[0].hMask = CreateBitmapMask(hBitmapIn, m_csBitmaps[0].dwWidth, m_csBitmaps[0].dwHeight, crTransColorIn);
		if (m_csBitmaps[0].hMask == NULL)
		{
			FreeResources();
			return 0;
		} // if
	} // if

	Invalidate(FALSE);

	return TRUE;
} // End of SetBitmaps

void CFrameViewer::DrawBitmap(CDC* pDC, RECT* rpItem)
{
    
	HDC			hdcBmpMem	= NULL;
	HBITMAP		hbmOldBmp	= NULL;
	HDC			hdcMem		= NULL;
	HBITMAP		hbmT		= NULL;

	BYTE		byIndex		= 0;

    if( ! m_csBitmaps[byIndex].hBitmap) {
        pDC->SetStretchBltMode(HALFTONE) ;
        m_ImageBitmap.StretchBlt(pDC, rpItem);
        return;
    }

    //이미지에 그림을 그린다...//
   	CDC *pImgDC=m_ImageBitmap.BegingModify();	
    CSize size = m_ImageBitmap.GetSize();

	CRect	rImage;

	hdcBmpMem = ::CreateCompatibleDC(pDC->m_hDC);
	hbmOldBmp = (HBITMAP)::SelectObject(hdcBmpMem, m_csBitmaps[byIndex].hBitmap);

	hdcMem = ::CreateCompatibleDC(NULL);

	hbmT = (HBITMAP)::SelectObject(hdcMem, m_csBitmaps[byIndex].hMask);

    ::BitBlt(pDC->m_hDC, 0, 0, m_csBitmaps[byIndex].dwWidth, m_csBitmaps[byIndex].dwHeight, hdcMem, 0, 0, SRCAND);

	::BitBlt(pDC->m_hDC, 0, 0, m_csBitmaps[byIndex].dwWidth, m_csBitmaps[byIndex].dwHeight, hdcBmpMem, 0, 0, SRCPAINT);


   	::SelectObject(hdcMem, hbmT);
	::DeleteDC(hdcMem);

	::SelectObject(hdcBmpMem, hbmOldBmp);
	::DeleteDC(hdcBmpMem);

    m_ImageBitmap.EndModify();
    pDC->SetStretchBltMode(HALFTONE) ;
    m_ImageBitmap.StretchBlt(pDC, rpItem);


} // End of DrawTheBitmap

void CFrameViewer::FreeResources(BOOL bCheckForNULL)
{
	if (bCheckForNULL)
	{
		// Destroy bitmaps
		if (m_csBitmaps[0].hBitmap)	::DeleteObject(m_csBitmaps[0].hBitmap);
		if (m_csBitmaps[1].hBitmap)	::DeleteObject(m_csBitmaps[1].hBitmap);

		// Destroy mask bitmaps
		if (m_csBitmaps[0].hMask)	::DeleteObject(m_csBitmaps[0].hMask);
		if (m_csBitmaps[1].hMask)	::DeleteObject(m_csBitmaps[1].hMask);
	} // if

	::ZeroMemory(&m_csBitmaps, sizeof(m_csBitmaps));
} // End of FreeResources



LPBITMAPINFO CFrameViewer::GetDIB()
{
    return &m_BitmapInfo;    
}

LPBYTE CFrameViewer::GetImageBitsBuffer()
{
    return 0;

}

BOOL CFrameViewer::GetUseBackImage()
{
    return m_bUseBackImage;
}

void CFrameViewer::SetUseBackImage(BOOL bUse)
{
    m_bUseBackImage = bUse;
}

BOOL CFrameViewer::StartRecAVI(CString strAviFile, DWORD dwRate)
{
    paviFile = new CAviFile(strAviFile,dwRate);
    m_bRec = TRUE;
    m_strRecFileName = strAviFile;
    return m_bRec;
}

CString CFrameViewer::StopRecAVI()
{
    m_bRec = FALSE;
    delete paviFile;
    paviFile = NULL;
    return m_strRecFileName;
}   

CSize CFrameViewer::GetImageSize()
{
    return m_sizeImage;
}

void CFrameViewer::SetRemoteAddress(CString remoteAddress)
{
	m_szRemoteAddress = remoteAddress;
}

void CFrameViewer::SetRemotePort(int remotePort)
{
	m_nRemotePort = remotePort;
}

CString CFrameViewer::GetRemoteAddress()
{
	return m_szRemoteAddress;
}

int CFrameViewer::GetRemotePort()
{
	return m_nRemotePort;
}

//이창이 나의 창이냐?
void CFrameViewer::SetIsMyView(bool bMyView)
{
	m_bMyView = bMyView;
}

bool CFrameViewer::LoadImage(LPCSTR lpszFilename)
{
	return m_image.Load(lpszFilename);
}



BOOL CFrameViewer::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(pMsg->message == WM_KEYDOWN)
	{
		//TRACE("----------------CFrameViewer::WM_KEYDOWN (%d, %d)\n", pMsg->wParam, pMsg->lParam);
		CString msg;
		msg.Format("----------------CFrameViewer::WM_KEYDOWN (%d, %d)\n", pMsg->wParam, pMsg->lParam);

		::AfxMessageBox(msg);
	}

	return CWnd::PreTranslateMessage(pMsg);
}
