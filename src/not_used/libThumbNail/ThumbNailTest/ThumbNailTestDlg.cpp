// ThumbNailTestDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ThumbNailTest.h"
#include "ThumbNailTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CThumbNailTestDlg 대화 상자




CThumbNailTestDlg::CThumbNailTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CThumbNailTestDlg::IDD, pParent)
	, m_strImagePath(_T(""))
	, m_strVideoPath(_T(""))
	, m_strImageWidth(_T(""))
	, m_strImageHeight(_T(""))
	, m_strVideoWidth(_T(""))
	, m_strVideoHeight(_T(""))
	, m_strVideoLength(_T(""))
	, m_strImageName(_T(""))
	, m_strVideoName(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CThumbNailTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_IMAGE_PATH_EDT, m_strImagePath);
	DDX_Text(pDX, IDC_VIDEO_PATH_EDT, m_strVideoPath);
	DDX_Text(pDX, IDC_IMAGE_WIDTH_EDT, m_strImageWidth);
	DDX_Text(pDX, IDC_IMAGE_HEIGHT_EDT, m_strImageHeight);
	DDX_Text(pDX, IDC_VIDEO_WIDTH_EDT, m_strVideoWidth);
	DDX_Text(pDX, IDC_VIDEO_HEIGHT_EDT, m_strVideoHeight);
	DDX_Text(pDX, IDC_VIDEO_LENGTH_EDT, m_strVideoLength);
	DDX_Control(pDX, IDC_IMAGE_COMBO, m_comboImage);
	DDX_Control(pDX, IDC_VIDEO_COMBO, m_comboVideo);
}

BEGIN_MESSAGE_MAP(CThumbNailTestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_IMAGE_SELECT_BTN, &CThumbNailTestDlg::OnBnClickedImageSelectBtn)
	ON_BN_CLICKED(IDC_VIDEO_SELECT_BTN, &CThumbNailTestDlg::OnBnClickedVideoSelectBtn)
	ON_BN_CLICKED(IDC_IMAGE_CREATE_BTN, &CThumbNailTestDlg::OnBnClickedImageCreateBtn)
	ON_BN_CLICKED(IDC_VIDEO_CREATE_BTN, &CThumbNailTestDlg::OnBnClickedVideoCreateBtn)
END_MESSAGE_MAP()


// CThumbNailTestDlg 메시지 처리기

BOOL CThumbNailTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	int nIdx = 0;
	nIdx = m_comboImage.AddString("16x16");
	m_comboImage.SetItemData(nIdx, E_THUMBNAIL_SMALL);
	nIdx = m_comboImage.AddString("24x24");
	m_comboImage.SetItemData(nIdx, E_THUMBNAIL_MIDDLE);
	nIdx = m_comboImage.AddString("32x32");
	m_comboImage.SetItemData(nIdx, E_THUMBNAIL_LARGE);
	m_comboImage.SetCurSel(0);

	nIdx = m_comboVideo.AddString("16x16");
	m_comboVideo.SetItemData(nIdx, E_THUMBNAIL_SMALL);
	nIdx = m_comboVideo.AddString("24x24");
	m_comboVideo.SetItemData(nIdx, E_THUMBNAIL_MIDDLE);
	nIdx = m_comboVideo.AddString("32x32");
	m_comboVideo.SetItemData(nIdx, E_THUMBNAIL_LARGE);
	m_comboVideo.SetCurSel(0);


	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CThumbNailTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CThumbNailTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CThumbNailTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CThumbNailTestDlg::OnBnClickedImageSelectBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strFilter;
	CString szFileExe = "*.bmp; *.jpg; *.jpeg; *.gif; *.pcx; *.png; *.tif; *.tiff";
	strFilter.Format("All Image Files (%s)|%s||", szFileExe, szFileExe);

	CFileDialog dlg(TRUE, NULL, "", OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFilter, this);
	if(dlg.DoModal() == IDOK)
	{
		m_strImagePath = dlg.GetPathName();
		m_strImageName = dlg.GetFileName();
		if(!m_clsThumbNail.OpenImageFile(m_strImagePath))
		{
			AfxMessageBox("Fail to open image file !");
			return;
		}//if

		int nWidth, nHeight;
		if(!m_clsThumbNail.GetImageSize(nWidth, nHeight))
		{
			AfxMessageBox("Fail to get image size !");
			return;
		}//if

		m_strImageWidth.Format("%d", nWidth);
		m_strImageHeight.Format("%d", nHeight);

		UpdateData(FALSE);
	}//if
}

void CThumbNailTestDlg::OnBnClickedVideoSelectBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString szFileExe = "*.avi; *.asf; *.mkv; *.mp4; *.mpg; *.mpeg; *.wmv; *.mov; *.tp; *.flv";
	CString strFilter;
	strFilter.Format("All Video Files(%s)|%s||", szFileExe, szFileExe);

	CFileDialog dlg(TRUE, NULL, "", OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFilter, this);
	if(dlg.DoModal() == IDOK)
	{
		m_strVideoPath = dlg.GetPathName();
		m_strVideoName = dlg.GetFileName();

		if(!m_clsThumbNail.OpenVidoeFile(m_strVideoPath))
		{
			AfxMessageBox("Fail to open Video file !");
			return;
		}//if

		int nWidth, nHeight, nLength;
		m_clsThumbNail.GetVideoSize(nWidth, nHeight, nLength);

		m_strVideoWidth.Format("%d", nWidth);
		m_strVideoHeight.Format("%d", nHeight);
		m_strVideoLength.Format("%d", nLength);

		UpdateData(FALSE);
	}//if
}

void CThumbNailTestDlg::OnBnClickedImageCreateBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if(m_strImagePath == "" || m_strImageWidth == "" || m_strImageHeight == "")
	{
		return;
	}//if

	int nSel = m_comboImage.GetCurSel();
	int nThumbType = m_comboImage.GetItemData(nSel);
	CString strThumbName;
	strThumbName.Format("%s%dx%d_%s", GetAppPath(), nThumbType, nThumbType, m_strImageName);
	if(!m_clsThumbNail.CreateImageThumbNail(strThumbName, nThumbType))
	{
		AfxMessageBox("Fail to create image thumbnail !");
		return;
	}//if
}

void CThumbNailTestDlg::OnBnClickedVideoCreateBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if(m_strVideoPath == "" || m_strVideoWidth == "" || m_strVideoHeight == "")
	{
		return;
	}//if

	int nSel = m_comboVideo.GetCurSel();
	int nThumbType = m_comboVideo.GetItemData(nSel);
	CString strThumbName;
	strThumbName.Format("%s%dx%d_%s.bmp", GetAppPath(), nThumbType, nThumbType, m_strVideoName);
	if(!m_clsThumbNail.CreateVideoThumbNail(strThumbName, nThumbType))
	{
		AfxMessageBox("Fail to create video thumbnail !");
		return;
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램이 실행된 경로를 반환 \n
/// @return <형: LPCTSTR> \n
///			<프로그램이 실행된 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR CThumbNailTestDlg::GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}
