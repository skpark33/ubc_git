// ThumbNailTestDlg.h : 헤더 파일
//

#pragma once

#include "ThumbNail.h"
#include "afxwin.h"


// CThumbNailTestDlg 대화 상자
class CThumbNailTestDlg : public CDialog
{
// 생성입니다.
public:
	CThumbNailTestDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_THUMBNAILTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedImageSelectBtn();
	afx_msg void OnBnClickedVideoSelectBtn();
	afx_msg void OnBnClickedImageCreateBtn();
	afx_msg void OnBnClickedVideoCreateBtn();
	DECLARE_MESSAGE_MAP()

public:
	CString			m_strImageName;			///<이미지파일의 이름
	CString			m_strVideoName;			///<동영상파일의 이름
	CString			m_strImagePath;			///<이미지파일의 경로
	CString			m_strVideoPath;			///<동영상파일의 경로
	CString			m_strImageWidth;		///<이미지 원본의 넓이
	CString			m_strImageHeight;		///<이미지 원본의 높이
	CString			m_strVideoWidth;		///<동영상 원본의 넓이
	CString			m_strVideoHeight;		///<동영상 원본의 높이
	CString			m_strVideoLength;		///<동영상 원본의 재생시간
	CComboBox		m_comboImage;
	CComboBox		m_comboVideo;

	CThumbNail		m_clsThumbNail;			///<ThumbNail을 만들어 주는 클래스

	LPCTSTR			GetAppPath();			///<프로그램이 실행된 경로를 반환
};
