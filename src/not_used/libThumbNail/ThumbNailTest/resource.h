//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ThumbNailTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_THUMBNAILTEST_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDC_IMAGE_PATH_EDT              1000
#define IDC_IMAGE_SELECT_BTN            1001
#define IDC_IMAGE_WIDTH_EDT             1002
#define IDC_IMAGE_HEIGHT_EDT            1003
#define IDC_VIDEO_PATH_EDT              1004
#define IDC_VIDEO_SELECT_BTN            1005
#define IDC_VIDEO_WIDTH_EDT             1006
#define IDC_VIDEO_HEIGHT_EDT            1007
#define IDC_IMAGE_CREATE_BTN            1008
#define IDC_VIDEO_CREATE_BTN            1009
#define IDC_VIDEO_LENGTH_EDT            1010
#define IDC_IMAGE_COMBO                 1011
#define IDC_COMBO2                      1012
#define IDC_VIDEO_COMBO                 1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
