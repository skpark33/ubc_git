#include "StdAfx.h"
#include "ThumbNail.h"
#include "MemDC.h"

/*
typedef LPBITMAPINFOHEADER PDIB;
#define DibWidth(lpbi) (UINT)(((LPBITMAPINFOHEADER)(lpbi))->biWidth)

#define DibHeight(lpbi) (UINT)(((LPBITMAPINFOHEADER)(lpbi))->biHeight)

#define DibColors(lpbi) ((RGBQUAD FAR *)((LPBYTE)(lpbi) + (int)(lpbi)->biSize))

#ifdef WIN32
#define DibPtr(lpbi) ((lpbi)->biCompression == BI_BITFIELDS ? (LPVOID)(DibColors(lpbi) + 3) : (LPVOID)(DibColors(lpbi) + (UINT)(lpbi)->biClrUsed))
#else
#define DibPtr(lpbi) (LPVOID)(DibColors(lpbi) + (UINT)(lpbi)->biClrUsed)
#endif

#define DibInfo(pDIB) ((BITMAPINFO FAR *)(pDIB))

#define DibNumColors(lpbi) ((lpbi)->biClrUsed == 0 && (lpbi)->biBitCount <= 8 ? (int)(1 << (int)(lpbi)->biBitCount) : (int)(lpbi)->biClrUsed)

#define DibPaletteSize(lpbi) (DibNumColors(lpbi) * sizeof(RGBQUAD))

#define BFT_BITMAP 0x4d42

#define WIDTHBYTES(i) ((unsigned)((i+31)&(~31))/8)

#define DibWidthBytesN(lpbi, n) (UINT)WIDTHBYTES((UINT)(lpbi)->biWidth * (UINT)(n))

#define DibWidthBytes(lpbi) DibWidthBytesN(lpbi, (lpbi)->biBitCount)

#define DibSizeImage(lpbi) ((lpbi)->biSizeImage == 0 ? ((DWORD)(UINT)DibWidthBytes(lpbi) * (DWORD)(UINT)(lpbi)->biHeight) : (lpbi)->biSizeImage)

#define DibSize(lpbi) ((lpbi)->biSize + (lpbi)->biSizeImage + (int)(lpbi)->biClrUsed * sizeof(RGBQUAD))  

#ifndef BI_BITFIELDS
#define BI_BITFIELDS 3
#endif
#define FixBitmapInfo(lpbi) if((lpbi)->biSizeImage == 0) (lpbi)->biSizeImage = DibSizeImage(lpbi);\
if((lpbi)->biClrUsed == 0) (lpbi)->biClrUsed = DibNumColors(lpbi);\
if((lpbi)->biCompression == BI_BITFIELDS && (lpbi)->biClrUsed == 0)
*/

// Macros   
#define DibNumColors(lpbi)      ((lpbi)->biClrUsed == 0 && (lpbi)->biBitCount <= 8 \
								? (int)(1 << (int)(lpbi)->biBitCount)          \
								: (int)(lpbi)->biClrUsed)   
   
#define DibSize(lpbi)           ((lpbi)->biSize + (lpbi)->biSizeImage + (int)(lpbi)->biClrUsed * sizeof(RGBQUAD))   
   
#define DibPaletteSize(lpbi)    (DibNumColors(lpbi) * sizeof(RGBQUAD))   
   
//   
// Constants   
//    
#define BFT_BITMAP 0x4d42   /* 'BM' */   


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CThumbNail::CThumbNail()
:	m_bIsImageOpen(false)
,	m_bIsVideoOpen(false)
,	m_strImageSrcPath(_T(""))
,	m_strVideoSrcPath(_T(""))
,	m_pdsRender(NULL)
{
	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지파일의 넓이와 높이를 반환 \n
/// @param (int&) nWidth : (out) 이미지파일의 넓이
/// @param (int&) nHeight : (out) 이미지파일의 높이
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
CThumbNail::~CThumbNail()
{
	if(m_cxImage.IsValid())
	{
		m_cxImage.Destroy();
	}//if

	if(m_pdsRender)
	{
		m_pdsRender->Close();
		delete m_pdsRender;
	}//if

	CoUninitialize();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지파일을 open한다. \n
/// @param (CString) strSrcPath : (in) 이미지파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CThumbNail::OpenImageFile(CString strSrcPath)
{
	if(m_cxImage.IsValid())
	{
		m_cxImage.Destroy();
	}//if

	CString strExt = FindExtension(strSrcPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	if(!m_cxImage.Load(strSrcPath, nImgType))
	{
		return false;
	}//if

	m_strImageSrcPath = strSrcPath;
	m_bIsImageOpen = true;

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상파일을 open한다. \n
/// @param (CString) strSrcPath : (in) 동영상파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CThumbNail::OpenVidoeFile(CString strSrcPath)
{
	if(m_pdsRender)
	{
		m_pdsRender->Close();
		delete m_pdsRender;
		m_pdsRender = NULL;
	}//if

	HWND hDeskTop = ::GetDesktopWindow();
	//m_pdsRender = new CVMRRender(pWnd);
	//m_pdsRender->SetParentWindow(pWnd->GetSafeHwnd());
	m_pdsRender = new CVMRRender(NULL);
	m_pdsRender->SetParentWindow(hDeskTop);
	m_pdsRender->SetRenderType(E_WINDOWLESS);
	if(!m_pdsRender->Open(strSrcPath))
	{
		return false;
	}//if

	m_strVideoSrcPath = strSrcPath;
	m_bIsVideoOpen = true;

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지파일의 넓이와 높이를 반환 \n
/// @param (int&) nWidth : (out) 이미지파일의 넓이
/// @param (int&) nHeight : (out) 이미지파일의 높이
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CThumbNail::GetImageSize(int& nWidth, int& nHeight)
{
	if(!m_bIsImageOpen)
	{
		return false;
	}//if

	nWidth = (int)m_cxImage.GetWidth();
	nHeight = (int)m_cxImage.GetHeight();

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상파일의 넓이와 높이를 반환 \n
/// @param (int&) nWidth : (out) 동영상파일의 넓이
/// @param (int&) nHeight : (out) 동영상파일의 높이
/// @param (int&) nLength : (out) 동영상파일의 재생시간
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CThumbNail::GetVideoSize(int& nWidth, int& nHeight, int& nLength)
{
	if(!m_bIsVideoOpen)
	{
		return false;
	}//if

	RECT stRect;
	if(!m_pdsRender->GetVideoRect(&stRect))
	{
		return false;
	}//if

	nWidth = (stRect.right - stRect.left);
	nHeight = (stRect.bottom - stRect.top);
	nLength = (int)m_pdsRender->GetTotalPlayTime();	 

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지파일의 thumbnail을 만들어준다. \n
/// @param (CString) strTargetPath : (in) 타겟파일의 경로
/// @param (short) eThumbNailType : (in) thumbnail의 종류(E_THUMBNAIL_SMALL, E_THUMBNAIL_MIDDLE, E_THUMBNAIL_LARGE)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CThumbNail::CreateImageThumbNail(CString strTargetPath, short eThumbNailType)
{
	if(!m_bIsImageOpen)
	{
		return false;
	}//if

	//원본파일의 가로세로 비율을 맞추어 축소한다.
	int nWidth, nHeight;
	int nOffsetX = 0;
	int nOffsetY = 0;
	nWidth = m_cxImage.GetWidth();
	nHeight = m_cxImage.GetHeight();

	if(nWidth >= nHeight)
	{
		int nNewHeight = (nHeight*eThumbNailType)/nWidth;
		nOffsetY = (eThumbNailType-nNewHeight)/2;
		if(!m_cxImage.Resample(eThumbNailType, nNewHeight, 3))	//default option(bilinear interpolation)
		{
			return false;
		}//if
	}
	else
	{
		int nNewWidth = (nWidth*eThumbNailType)/nHeight;
		nOffsetX = (eThumbNailType-nNewWidth)/2;
		if(!m_cxImage.Resample(nNewWidth, eThumbNailType, 3))	//default option(bilinear interpolation)
		{
			return false;
		}//if
	}//if

	//배경이미지만들기
	CDC dcDeskTop;
	dcDeskTop.m_hDC = ::GetDC(NULL);
	CMemDC memDC(&dcDeskTop);
	int nSizeBitmap = eThumbNailType * eThumbNailType * 4;
	LPBYTE pByte = new BYTE[nSizeBitmap];
	::memset(pByte, 0xff, nSizeBitmap);

	BITMAP stBMP;
	stBMP.bmType = 0;
	stBMP.bmWidth = eThumbNailType;
	stBMP.bmHeight = eThumbNailType;
	stBMP.bmWidthBytes = eThumbNailType*4; // 32bit
	stBMP.bmPlanes = 1;
	stBMP.bmBitsPixel = 32;
	stBMP.bmBits = pByte;

	CBitmap bmpBG;
	if(!bmpBG.CreateBitmapIndirect(&stBMP))
	{
		delete []pByte;
		return false;
	}//if
	delete []pByte;		

	CBitmap *pOldBitmap = (CBitmap*)memDC.SelectObject(&bmpBG);
	memDC.FillSolidRect(CRect(0, 0, eThumbNailType, eThumbNailType), RGB(0, 0, 0));
	memDC.SelectObject(pOldBitmap);

	//배경이미지와 resampl된 이미지를 합성한다.
	CxImage imgBG;
	if(!imgBG.CreateFromHBITMAP((HBITMAP)bmpBG))
	{
		if(imgBG.IsValid())
		{
			imgBG.Destroy();
		}//if
		::ReleaseDC(NULL, dcDeskTop.m_hDC);

		return false;
	}//if

	imgBG.MixFrom(m_cxImage, nOffsetX, nOffsetY);

	CString strExt = FindExtension(m_strImageSrcPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	if(!imgBG.Save(strTargetPath, nImgType))
	{
		if(imgBG.IsValid())
		{
			imgBG.Destroy();
		}//if

		::ReleaseDC(NULL, dcDeskTop.m_hDC);
		return false;
	}//if

	if(imgBG.IsValid())
	{
		imgBG.Destroy();
	}//if

	::ReleaseDC(NULL, dcDeskTop.m_hDC);

	return true;
}	


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상파일의 thumbnail을 만들어준다. \n
/// @param (CString) strTargetPath : (in) 타겟파일의 경로
/// @param (short) eThumbNailType : (in) thumbnail의 종류(E_THUMBNAIL_SMALL, E_THUMBNAIL_MIDDLE, E_THUMBNAIL_LARGE)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CThumbNail::CreateVideoThumbNail(CString strTargetPath, short eThumbNailType)
{
	if(!m_bIsVideoOpen)
	{
		return false;
	}//if

	//동영상의 전체길이를 얻어와서 MIN_VIDEO_LENGTH 보다 작다면 중간시간의 샘플을
	//MIN_VIDEO_LENGTH보다 길다면 SMP_VIDEO_LENGTH의 샘플을 가져온다.
	double dbSampleTime = SMP_VIDEO_LENGTH;
	double dbLength = m_pdsRender->GetTotalPlayTime();
	if(dbLength <= MIN_VIDEO_LENGTH)
	{
		dbSampleTime = dbSampleTime/2;
	}//if
/*
	//샘플의 위치로 이동
	m_pdsRender->SetPosition(dbSampleTime);
	m_pdsRender->Play();
	m_pdsRender->Pause();
	m_pdsRender->Stop();

	//Sleep(1000);
*/
	BYTE* pByte = NULL;
	if(!m_pdsRender->GetCurrentImage(&pByte, dbSampleTime))
	{
		return false;
	}//if

	BITMAPFILEHEADER hdr;
	DWORD dwSize, dwWritten;
	LPBITMAPINFOHEADER pdib = (LPBITMAPINFOHEADER)pByte;

	// 비트맵 데이터를 저장하는 새로운 파일
	HANDLE hFile = CreateFile(strTargetPath,
								GENERIC_WRITE,
								FILE_SHARE_READ,
								NULL,
								CREATE_ALWAYS,
								FILE_ATTRIBUTE_NORMAL,
								0);

	if(hFile == INVALID_HANDLE_VALUE)
	{
		CoTaskMemFree(pByte);
		return false;
	}//if

	// 비트맵 해더 초기화
	dwSize = DibSize(pdib);
	hdr.bfType = BFT_BITMAP;
	hdr.bfSize = dwSize + sizeof(BITMAPFILEHEADER);
	hdr.bfReserved1 = 0;
	hdr.bfReserved2 = 0;
	hdr. bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + pdib->biSize + DibPaletteSize(pdib);

	// 비트맵 헤더와 비트맵 비트를 파일로 출력
	WriteFile(hFile, (LPCVOID)&hdr, sizeof(BITMAPFILEHEADER), &dwWritten, 0);
	WriteFile(hFile, (LPCVOID)pdib, dwSize, &dwWritten, 0);

	// 파일 닫기
	CloseHandle(hFile);
	// GetCurrentImage()가 리턴해준 이미지 데이터 해제
	CoTaskMemFree(pByte);

	//썸네일 이미지를 만든다.
	if(!OpenImageFile(strTargetPath))
	{
		return false;
	}//if

	if(!CreateImageThumbNail(strTargetPath, eThumbNailType))
	{
		return false;
	}//if
/*
	// save DIB file as Bitmap.
	// This sample saves image as bitmap to help
	// understanding the sample.
	HANDLE fh;
	BITMAPFILEHEADER bmphdr;
	BITMAPINFOHEADER bmpinfo;
	DWORD nWritten;

	memset(&bmphdr, 0, sizeof(bmphdr));
	memset(&bmpinfo, 0, sizeof(bmpinfo));

	bmphdr.bfType = ('M' << 8) | 'B';
	bmphdr.bfSize = sizeof(bmphdr) + sizeof(bmpinfo) + bufSize;
	bmphdr.bfOffBits = sizeof(bmphdr) + sizeof(bmpinfo);

	bmpinfo.biSize = sizeof(bmpinfo);
	bmpinfo.biWidth = width;
	bmpinfo.biHeight = height;
	bmpinfo.biPlanes = 1;
	bmpinfo.biBitCount = 32;

	fh = CreateFile("result.bmp",
		GENERIC_WRITE, 0, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	WriteFile(fh, &bmphdr, sizeof(bmphdr), &nWritten, NULL);
	WriteFile(fh, &bmpinfo, sizeof(bmpinfo), &nWritten, NULL);
	WriteFile(fh, imgData, bufSize, &nWritten, NULL);
	CloseHandle(fh);
*/

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일명에서 확장자를 분리하여 반환 \n
/// @param (const CString&) strFileName : (in) 파일명
/// @return <형: CString> \n
///			<파일의 확장자> \n
/////////////////////////////////////////////////////////////////////////////////
CString	CThumbNail::FindExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--){
		if (strFileName[i] == '.'){
			return strFileName.Mid(i+1);
		}
	}
	return CString(_T(""));
}