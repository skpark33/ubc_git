/************************************************************************************/
/*! @file ThumbNail.h
	@brief ThumbNail을 만들어 주는 클래스
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/08/27\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2010/08/27:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include "CxImage/ximage.h"
//#include "DirectShow/DSGrabber.h"
#include "DirectShow/VMRRender.h"

enum E_THUMBNAIL_TYPE
{ 
	E_THUMBNAIL_SMALL	= 16,		//16X16 크기의 thumbnail
	E_THUMBNAIL_MIDDLE	= 24,		//24X24 크기의 thumbnail
	E_THUMBNAIL_LARGE	= 32,		//32X32 크기의 thumbnail
};

#define		MIN_VIDEO_LENGTH		(15)	//샘플을 얻기위한 최소한의 동영상 길이(시간)
#define		SMP_VIDEO_LENGTH		(10)	//샘플을 얻는 동영상의 길이(시간)


//! ThumbNail을 만들어 주는 클래스
/*!
	동영상과 이미지 파일의 ThumbNail을 만들어 준다.\n
*/
class CThumbNail
{
public:
	CThumbNail(void);												///<생성자
	~CThumbNail(void);												///<소멸자

	bool OpenImageFile(CString strSrcPath);							///<이미지파일을 open한다.
	bool OpenVidoeFile(CString strSrcPath);							///<동영상파일을 open한다.
	bool GetImageSize(int& nWidth, int& nHeight);					///<이미지파일의 넓이와 높이를 반환
	bool GetVideoSize(int& nWidth, int& nHeight, int& nLength);		///<동영상파일의 넓이와 높이를 반환

	bool CreateImageThumbNail(CString strTargetPath,
								short eThumbNailType);				///<이미지파일의 thumbnail을 만들어준다.	
	bool CreateVideoThumbNail(CString strTargetPath,
								short eThumbNailType);				///<동영상파일의 thumbnail을 만들어준다.

	CString	FindExtension(const CString& strFileName);				///<파일명에서 확장자를 분리하여 반환

private:
	bool				m_bIsImageOpen;								///<이미지파일의 open 여부
	bool				m_bIsVideoOpen;								///<동영상파일의 open 여부

	CString				m_strImageSrcPath;							///<이미지파일의 원본경로
	CString				m_strVideoSrcPath;							///<동영상파일의 원본경로

	CxImage				m_cxImage;									///<이미지 thumbnail을 만들어주는 클래스
	//CDSGrabber*			m_pdsGtabber;								///<동영상 thumbnail을 만들어주는 클래스
	CVMRRender*			m_pdsRender;								///<동영상 thumbnail을 만들어주는 클래스
};
