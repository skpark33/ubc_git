#include "StdAfx.h"
#include "BasicInterface.h"

#include <math.h>
#include <atlbase.h>


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CBasicInterface::CBasicInterface(CWnd* pWnd)
:	m_pParentWnd(pWnd)
,	m_pGraphBuilder(NULL)
,	m_pFilter(NULL)
,	m_dwRotId(0)
,	m_nRenderType(E_WINDOWLESS)
,	m_bOpen(false)
,	m_hParentWnd(NULL)
,	m_strFilePath("")
,	m_strSamiPath("")
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CBasicInterface::~CBasicInterface(void)
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 재생한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::Play()
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return false;
	}//if

	HRESULT hr;
	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		return false;
	}//if

	JIB(pMediaControl->Run());

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 재생을 일시 중지 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::Pause()
{
	if(!m_bOpen)
	{
		return false;
	}//if

	HRESULT hr;
	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		return false;
	}//if

	JIB(pMediaControl->Pause());
	
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 재생을 중지 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::Stop()
{
	if(!m_bOpen)
	{
		return false;
	}//if

	HRESULT hr;
	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		return false;
	}//if

	JIB(pMediaControl->Pause());
	SeekToStart();
	JIB(pMediaControl->Stop());

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 총 재생시간을 반환한다. \n
/// @return <형: double> \n
///			<double: 동영상 파일의 총 재생시간> \n
/////////////////////////////////////////////////////////////////////////////////
double CBasicInterface::GetTotalPlayTime()
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return false;
	}//if

	REFTIME tm = 0.0f;
	CComQIPtr<IMediaSeeking> pMediaSeeking(m_pGraphBuilder);
	if(pMediaSeeking != NULL)
	{
		HRESULT hr;
		LONGLONG lDuration;
		LIF(pMediaSeeking->GetDuration(&lDuration));
		if(SUCCEEDED(hr))
		{
			tm = double(lDuration) / UNITS;
		}//if
	}//if
	
	return tm;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 현재 재생시간을 반환한다. \n
/// @return <형: double> \n
///			<double: 동영상 파일의 현재 재생시간> \n
/////////////////////////////////////////////////////////////////////////////////
double CBasicInterface::GetCurrentPlayTime()
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return false;
	}//if

	REFTIME tm = 0.0f;
	CComQIPtr<IMediaSeeking> pMediaSeeking(m_pGraphBuilder);
	if(pMediaSeeking != NULL)
	{
		HRESULT hr;
		LONGLONG lPosition;
		LIF(pMediaSeeking->GetPositions(&lPosition, NULL));
		if(SUCCEEDED(hr))
		{
			tm = double(lPosition) / UNITS;
		}//if
	}//if
	
	return tm;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 지정된 위치로 이동한다. \n
/// @param (double) pos : (in) 이동하려는 동영상의 시간
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::SetPosition(double pos)
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return;
	}//if

	CComQIPtr<IMediaSeeking> pMediaSeeking(m_pGraphBuilder);
	if(pMediaSeeking != NULL)
	{
		HRESULT hr;
		LONGLONG llTime = LONGLONG(pos * double(UNITS));

		//FILTER_STATE fs;
		//hr = pMediaControl->GetState(100, (OAFilterState *)&fs);
		LIF(pMediaSeeking->SetPositions(&llTime, AM_SEEKING_AbsolutePositioning, NULL, 0));
		//m_pMediaControl->Pause();

		// This gets new data through to the renderers
		//if(fs == State_Stopped && bFlushData)
		//{
		//    hr = pMediaControl->Pause();
		//    hr = pMediaControl->GetState(INFINITE, (OAFilterState *)&fs);
		//    hr = pMediaControl->Stop();
		//}//if
    }//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 처음 위치로 이동한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::SeekToStart()
{
	SetPosition(0);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 닫는다. \n
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::Close()
{
	RemoveFromRot();
	ReleaseAllInterfaces();
	SAFE_RELEASE(m_pGraphBuilder);

	m_bOpen  = false;
	m_strFilePath = "";
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// let the graph instance be accessible from graphedit \n
/// @param (IUnknown) *pUnkGraph : (in) Grapheditor interface pointer
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::AddToRot(IUnknown *pUnkGraph) 
{
	if(m_dwRotId != 0)
	{
		__DEBUG__("Registerd filtergraph in ROT", (int)m_dwRotId);
	}//if

	if(pUnkGraph == NULL)
	{
		__DEBUG__("Graphbuilder is NULL", _NULL);
		return false;
	}//if

	HRESULT hr;
    IMoniker * pMoniker;
    IRunningObjectTable *pROT;
    JIB(GetRunningObjectTable(0, &pROT));

    WCHAR wsz[256];
    wsprintfW(wsz, L"FilterGraph %08x pid %08x", (DWORD_PTR)pUnkGraph, GetCurrentProcessId());
    JIB(CreateItemMoniker(L"!", wsz, &pMoniker));
    if(SUCCEEDED(hr))
	{
       // LIF(pROT->Register(0, pUnkGraph, pMoniker, &m_dwRotId));
		 LIF(pROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, pUnkGraph, pMoniker, &m_dwRotId));
		SAFE_RELEASE(pMoniker);
    }//if
	
    SAFE_RELEASE(pROT);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// remove the graph instance accessibility from graphedit \n
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::RemoveFromRot()
{
	if(m_dwRotId != 0)
	{
		IRunningObjectTable *pROT;
		if(SUCCEEDED(GetRunningObjectTable(0, &pROT)))
		{
			pROT->Revoke(m_dwRotId);		//Running Object Table에서 제거함
			SAFE_RELEASE(pROT);				//컴퍼넌트 작업이 끝났을때 하나 감소
		}//if
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 자막 파일의 이름을 구한다. \n
/// @param (CString) strFilename : (in) 동영상파일의 경로
/// @return <형: CString> \n
///			<자막파일의 경로를 반환> \n
/////////////////////////////////////////////////////////////////////////////////
CString CBasicInterface::GetSamiFile(CString strFilename)
{
	int nIndex = strFilename.ReverseFind( '.' );

	strFilename.Delete( nIndex, (strFilename.GetLength() - nIndex) );

	strFilename += _T(".smi");

	return strFilename;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 음량을 설정한다. \n
/// @param (int) nVol : (in) 설정하려는 음량
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::SetVolume(int nVol)
{
	if(m_pGraphBuilder)
	{
		// 볼륨조절
		double lf_cur_sound = ((1 - pow(10.0f, -10)) * ((double)nVol / 100)) + pow(10.0f, -10);// 핵심코드
		lf_cur_sound = 10 * log10(lf_cur_sound) * 100;
		long l_cur_sound = (long)lf_cur_sound;

		CComQIPtr<IBasicAudio> pBasicAudio(m_pGraphBuilder);
		if(pBasicAudio)
		{
			HRESULT hr;
			JIB(pBasicAudio-> put_Volume(l_cur_sound));

			return true;
		}//if
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 해당하는 pin이 연결된 상태인지 구한다. \n
/// @param (IPin*) pPin : (in) 연결상태를 판단하려는 pin
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::IsPinConnected(IPin* pPin)
{
	//CheckPointer(pPin, E_POINTER);

	CComPtr<IPin> pPinTo;
	
	return SUCCEEDED(pPin->ConnectedTo(&pPinTo)) && pPinTo ? true : false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 그래프빌더에서 필터의 이름으로 해당하는 필터인터페이스를 찾는다. \n
/// @param (IGraphBuilder) *pGraphBuilder : (in) 필터를 찾으려는 그래프빌더 인터페이스
/// @param (LPTSTR) szNameToFind : (in) 찾으려는 필터의 이름
/// @return <형: IBaseFilter*> \n
///			<이름으로 검ㅎ색된 필터 인터페이스> \n
/////////////////////////////////////////////////////////////////////////////////
IBaseFilter* CBasicInterface::FindFilterFromName(IGraphBuilder *pGraphBuilder, LPTSTR szNameToFind)
{
    USES_CONVERSION;

    HRESULT hr;
    IEnumFilters *pEnum = NULL;
    IBaseFilter *pFilter = NULL;
    ULONG cFetched;
    BOOL bFound = FALSE;

    // Verify graph builder interface
    if(!pGraphBuilder)
	{
        return NULL;
	}//if

    // Get filter enumerator
    hr = pGraphBuilder->EnumFilters(&pEnum);
    if(FAILED(hr))
	{
        return NULL;
	}//if

    // Enumerate all filters in the graph
    while((pEnum->Next(1, &pFilter, &cFetched) == S_OK) && (!bFound))
    {
        FILTER_INFO FilterInfo;
        TCHAR szName[256];
        
        hr = pFilter->QueryFilterInfo(&FilterInfo);
        if(FAILED(hr))
        {
            SAFE_RELEASE(pFilter);
            SAFE_RELEASE(pEnum);
            return NULL;
        }//if

        // Compare this filter's name with the one we want
        lstrcpy(szName, W2T(FilterInfo.achName));
        if(!lstrcmp(szName, szNameToFind))
        {
            bFound = TRUE;
        }//if

        FilterInfo.pGraph->Release();

        // If we found the right filter, don't release its interface.
        // The caller will use it and release it later.
        if(!bFound)
		{
            SAFE_RELEASE(pFilter);
		}
        else
		{
            break;
		}//if
    }//while
    SAFE_RELEASE(pEnum);

    return (bFound ? pFilter : NULL);
}
