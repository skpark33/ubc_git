/************************************************************************************/
/*! @file VMRRender.cpp
	@brief VMR을 이용한 동영상 재생 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/01/07\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2010/01/07:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"
#include "VMRRender.h"
#include <math.h>
#include <io.h>
#include <D3d9.h>
#include <Vmr9.h>
#include <d3dx9tex.h>
#include "dshowutil.h"
#include "AviInfo.h"
#include "MediaInfoDLL.h"
#define MediaInfoNameSpace MediaInfoDLL;
using namespace MediaInfoNameSpace;


//static HRESULT SetRenderingMode(IBaseFilter* pBaseFilter, VMRMode mode)
//{
//    // Test VMRConfig, VMRMonitorConfig
//    IVMRFilterConfig* pConfig;
//
//    HRESULT hr = pBaseFilter->QueryInterface(IID_IVMRFilterConfig, (LPVOID *)&pConfig);
//    if(SUCCEEDED(hr))
//    {
//        hr = pConfig->SetRenderingMode(mode);
//        hr = pConfig->SetRenderingPrefs(RenderPrefs_ForceOverlays|RenderPrefs_AllowOverlays);
//        pConfig->Release();
//    }//if
//    return hr;
//}//if


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (HWND) hwnd : (in) 동영상을 재생할 창의 핸들
/// @param (int) nMode : (in) 동영상을 랜더링 할 모드
/////////////////////////////////////////////////////////////////////////////////
CVMRRender::CVMRRender(CWnd* pWnd)
: CBasicInterface(pWnd)
//, m_pVideoDisplayControl(NULL)
{
	
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CVMRRender::~CVMRRender()
{
	if(m_bOpen)
	{
		Stop();
		Close();
	}//if

	m_hParentWnd = NULL;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 오픈한다. \n
/// @param (LPCSTR) lpszFilename : (in) 재생하려는 파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::Open(LPCSTR lpszFilename)
{
	Close();
	m_strFilePath = lpszFilename;
	if(m_strFilePath.GetLength() == 0)
	{
		__DEBUG__("Invlid file path", m_strFilePath);
		return false;
	}//if

	//USES_CONVERSION;
	WCHAR szFileName[MAX_PATH] = { 0x00 };
	//wcsncpy(FileName, T2W(strFile), NUMELMS(FileName));
	MultiByteToWideChar(CP_ACP, 0, (LPCTSTR)m_strFilePath, -1, szFileName, MAX_PATH);

	HRESULT hr;
	JIB(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (LPVOID *)&m_pGraphBuilder));
	
	//Setting render filter
	if(!SetRenderFilter())
	{
		Close();
		return false;
	}//if

	//SetAspectRatioMode(STRETCHED);

	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx != NULL)
	{
		LIF(pMediaEventEx->SetNotifyWindow((OAHWND)(m_hParentWnd), WM_DSINTERFACES_GRAPHNOTIFY, 0)); // 이벤트 통지를 처리하는 윈도우를 등록
	}//if

	if(IsWindowsMediaFile(lpszFilename))
	{
		if(!SetWindowsMediaFilters(szFileName))
		{
			__DEBUG__("Fail to se window media file filter", m_strFilePath);
			JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
			//return false;
		}//if
	}
	else
	{
		//Render file
		JIB(m_pGraphBuilder->RenderFile(szFileName, NULL));
	}//if	

	if(m_nRenderType == E_WINDOWED || m_nRenderType == E_OVERLAY)
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
			LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
			LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
			LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
			LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
			LIF(pVideoWindow->put_BackgroundPalette(0));
			//LIF(pVideoWindow->put_WindowStyle(WS_OVERLAPPED));			//  렌더링을 위한 구문
		}//if

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
	}//if

	//SetAspectRatioMode(STRETCHED);

	m_bOpen = true;
	AddToRot(m_pGraphBuilder);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 비디오 랜더를 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetRenderFilter()
{
	if(!m_pGraphBuilder)
	{
		__DEBUG__("m_pGraphBuilder is NULL", _NULL);
		return false;
	}//if

	bool bRet;
	switch(m_nRenderType)
	{
	case E_WINDOWLESS:
		{
			if(GetOSVersion() == E_OS_WIN7)
			{
				__DEBUG__("VMR9 windowless mode", _NULL);
				bRet = SetVMR9WindowLess();
			}
			else
			{
				__DEBUG__("VMR7 windowless mode", _NULL);
				bRet = SetVMR7WindowLess();
			}//if
		}
		break;
	case E_WINDOWED:
		{
			if(GetOSVersion() == E_OS_WIN7)
			{
			}
			else
			{
				__DEBUG__("VMR7 windowed mode", _NULL);
				bRet = SetVMR7Windowed();
			}//if
		}
		break;
		/*
	case E_EVR:
		{
			__DEBUG__("Enhanced video renderer mode", _NULL);
			bRet = SetEnhancedVideoRenderer();
		}
		break;
		*/
	case E_OVERLAY:
		{
			__DEBUG__("Overlay Mixer mode", _NULL);
			bRet = SetOverlay();
		}
		break;
	default :
		{
			__DEBUG__("Default Windowed mode", _NULL);
			bRet = SetDefaultWindowed();
		}
	}//switch
	
	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VMR7 windowless 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetVMR7WindowLess()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_VideoMixingRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Video Mixing Renderer 7"));
		m_pFilter->Release();

		//Set rendering mode
		CComQIPtr<IVMRFilterConfig> pConfig(m_pFilter);
		if(pConfig == NULL)
		{
			__DEBUG__("Fail to get IID_IVMRFilterConfig", _NULL);
			Close();
			return false;
		}//if

		JIB(pConfig->SetRenderingMode(VMRMode_Windowless));

		CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
		if(pWindowlessControl == NULL)
		{
			__DEBUG__("Fail to get IID_IVMRWindowlessControl", _NULL);
			Close();
			return false;
		}//if
		
		JIB(pWindowlessControl->SetVideoClippingWindow(m_hParentWnd));
		//JIB(pWindowlessControl->SetBorderColor(RGB(0, 0, 0)));

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		JIB(pWindowlessControl->SetVideoPosition(NULL, &rc));

		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VMR9 windowless 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetVMR9WindowLess()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_VideoMixingRenderer9, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Video Mixing Renderer 9"));
		m_pFilter->Release();

		//Set rendering mode
		CComQIPtr<IVMRFilterConfig9> pConfig(m_pFilter);
		if(pConfig == NULL)
		{
			__DEBUG__("Fail to get IID_IVMRFilterConfig9", _NULL);
			Close();
			return false;
		}//if

		JIB(pConfig->SetRenderingMode(VMR9Mode_Windowless));
		//LIF(pConfig->SetNumberOfStreams(1));

		CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
		if(pWindowlessControl == NULL)
		{
			__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
			return false;
		}//if

		JIB(pWindowlessControl->SetVideoClippingWindow(m_hParentWnd));
		//JIB(pWindowlessControl->SetBorderColor(RGB(0, 0, 0)));

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		JIB(pWindowlessControl->SetVideoPosition(NULL, &rc));

		return true;
	}//if

	return false;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// EnhancedVideoRenderer를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetEnhancedVideoRenderer()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_EnhancedVideoRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Enhanced Video Renderer"));
		m_pFilter->Release();

		IMFGetService *pService = NULL;
		IMFVideoDisplayControl *pDisplay = NULL;

		hr = m_pFilter->QueryInterface(__uuidof(IMFGetService), (void**)&pService); 
		if(SUCCEEDED(hr)) 
		{ 
			hr = pService->GetService(MR_VIDEO_RENDER_SERVICE, 
										__uuidof(IMFVideoDisplayControl),
										(void**)&pDisplay);
		}
		else
		{
			return false;
		}//if

		// Set the clipping window.
		if(SUCCEEDED(hr))
		{
			hr = pDisplay->SetVideoWindow(m_hParentWnd);
		}//if

		if(SUCCEEDED(hr))
		{
			m_pVideoDisplayControl = pDisplay;
			m_pVideoDisplayControl->AddRef();
		}//if

		SAFE_RELEASE(pService);
		SAFE_RELEASE(pDisplay);

		return true;
	}//if

	return false;
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VMR7 windowed 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetVMR7Windowed()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_VideoMixingRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Video Mixing Renderer 7"));
		m_pFilter->Release();

		//이곳에서 설정하면 왠지 Active Window에서 재생이 된다...
/*
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
			LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
			LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
			LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
			LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
			LIF(pVideoWindow->put_BackgroundPalette(0));
			//LIF(pVideoWindow->put_WindowStyle(WS_OVERLAPPED));				//  렌더링을 위한 구문
		}//if

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
*/
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// System default windowed 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetDefaultWindowed()
{
	HRESULT hr;

	JIB(CoCreateInstance(CLSID_VideoRendererDefault, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Default Video Render"));
		m_pFilter->Release();

		//이곳에서 설정하면 왠지 Active Window에서 재생이 된다...
/*
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			LIF(pVideoWindow->put_Owner((OAHWND)(m_hParentWnd)));			//부모윈도우를 결정
			LIF(pVideoWindow->put_MessageDrain((OAHWND)(m_hParentWnd)));	//메세지를 받는 윈도우를 결정
			LIF(pVideoWindow->put_WindowStyle(WS_CHILD|WS_CLIPSIBLINGS|WS_CLIPCHILDREN));
			LIF(pVideoWindow->put_AutoShow(-1));							// -1 온(=OATURE), 0 오프(=OAFLASE)
			LIF(pVideoWindow->put_Visible(-1));								//윈도우를 표시하는지, 비표시로 하는지를 지정하는 값
			LIF(pVideoWindow->put_BackgroundPalette(0));
			//LIF(pVideoWindow->put_WindowStyle(WS_OVERLAPPED));				//  렌더링을 위한 구문
		}//if

		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
*/
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Overlay 필터를 랜더필터로 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetOverlay()
{
	HRESULT hr;
	//IBaseFilter* pOverlay = NULL;

	JIB(CoCreateInstance(CLSID_OverlayMixer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
	if(SUCCEEDED(hr))
	{
		//Add to filter graph
		JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Overlay Mixer"));
		m_pFilter->Release();
/*
		JIB(CoCreateInstance(CLSID_VideoRenderer, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
		//JIB(CoCreateInstance(CLSID_VideoRendererDefault, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&m_pFilter));
		if(SUCCEEDED(hr))
		{
			//Add to filter graph
			JIB(m_pGraphBuilder->AddFilter(m_pFilter, L"Video Renderer"));
			m_pFilter->Release();

			return true;
		}//if
*/
		return true;
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 그래프빌더의 모든 필터와 인터페이스들을 제거한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CVMRRender::ReleaseAllInterfaces()
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return;
	}//if

	HRESULT hr;
	LIF(m_pGraphBuilder->Abort());

	//그래프를 정지한다.
	Stop();

	if(m_nRenderType != E_WINDOWLESS || m_nRenderType != E_EVR)
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow != NULL)
		{
			LIF(pVideoWindow->put_Visible(OAFALSE));		//윈도우를 비표시함
			LIF(pVideoWindow->put_Owner(NULL));				//기존의 부모 윈도우를 삭제
		}//if
	}//if

	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx != NULL)
	{
		LIF(pMediaEventEx->SetNotifyWindow((OAHWND)NULL, 0, 0));		//이벤트 통지를 처리하는 윈도우를 등록
	}//if

	//그래프의 필터를 열거한다.
	IEnumFilters *pEnum = NULL;
	LIF(m_pGraphBuilder->EnumFilters(&pEnum));
	if(SUCCEEDED(hr))
	{
		IBaseFilter *pFilter = NULL;
		while(S_OK == pEnum->Next(1, &pFilter, NULL))
		{
			//필터를 삭제한다.
			LIF(m_pGraphBuilder->RemoveFilter(pFilter));

			//열거자를 리셋한다.
			pEnum->Reset();
			SAFE_RELEASE(pFilter);
		}//while
		SAFE_RELEASE(pEnum);
	}//if
/*
	if(m_nRenderType == E_EVR)
	{
		SAFE_RELEASE(m_pVideoDisplayControl);
	}//if
*/
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상을 다시 그려준다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::RePaintVideo()
{
	bool bRet = false;
	if(m_nRenderType == E_WINDOWLESS)
	{
		if(GetOSVersion() == E_OS_WIN7)
		{
			CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
				return false;
			}//if

			HRESULT hr;
			//JIB(pWindowlessControl->RepaintVideo(m_hParentWnd, hdc));
			HDC hDC = ::GetDC(m_hParentWnd);
			LIF(pWindowlessControl->RepaintVideo(m_hParentWnd, hDC));
			::ReleaseDC(m_hParentWnd, hDC);
		}
		else
		{
			CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IID_IVMRWindowlessControl", _NULL);
				return false;
			}//if

			HRESULT hr;
			//JIB(pWindowlessControl->RepaintVideo(m_hParentWnd, hdc));
			HDC hDC = ::GetDC(m_hParentWnd);
			LIF(pWindowlessControl->RepaintVideo(m_hParentWnd, hDC));
			::ReleaseDC(m_hParentWnd, hDC);
		}//if
	}
/*
	else if(m_nRenderType == E_EVR)
	{
		if(m_pVideoDisplayControl == NULL)
		{
			__DEBUG__("m_pVideoDisplayControl is NULL", _NULL);
			return false;
		}//if

		HRESULT hr;
		JIB(m_pVideoDisplayControl->RepaintVideo());
	}//if
*/
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 영역을 설정한다. \n
/// @param (LPRECT) pRect : (in) 설정하려는 동영상의 영역
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetVideoRect(LPRECT pRect)
{
	HRESULT hr;
	if(m_nRenderType == E_WINDOWLESS)
	{
		if(GetOSVersion() == E_OS_WIN7)
		{
			CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->SetVideoPosition(NULL, pRect));
		}
		else
		{
			CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->SetVideoPosition(NULL, pRect));
		}//if
	}
/*
	else if(m_nRenderType == E_EVR)
	{
		if(m_pVideoDisplayControl == NULL)
		{
			__DEBUG__("m_pVideoDisplayControl is NULL", _NULL);
			return false;
		}//if

		HRESULT hr;
		JIB(m_pVideoDisplayControl->SetVideoPosition(NULL, pRect));
	}
*/
	else
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow == NULL)
		{
			__DEBUG__("Fail to get IVideoWindow", _NULL);
			return false;
		}//if

		JIB(pVideoWindow->SetWindowPosition(pRect->left,
											pRect->top,
											(pRect->right-pRect->left),
											(pRect->bottom-pRect->top)));
	}//if

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 원본영역을 반한한다. \n
/// @param (LPRECT) pRect : (out) 동영상의 원본 영역
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::GetVideoRect(LPRECT pRect)
{
	memset(pRect, 0x00, sizeof(RECT));
	HRESULT hr;
	if(m_nRenderType == E_WINDOWLESS)
	{
		if(GetOSVersion() == E_OS_WIN7)
		{
			CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->GetNativeVideoSize(&pRect->right, &pRect->bottom, NULL, NULL));
		}
		else
		{
			CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->GetNativeVideoSize(&pRect->right, &pRect->bottom, NULL, NULL));
		}//if
	}
/*
	else if(m_nRenderType == E_EVR)
	{
		HRESULT hr;
		if(m_pVideoDisplayControl == NULL)
		{
			__DEBUG__("m_pVideoDisplayControl is NULL", _NULL);
			return false;
		}//if

		SIZE size;
		JIB(m_pVideoDisplayControl->GetNativeVideoSize(&size, NULL));
		pRect->right = size.cx;
		pRect->bottom= size.cy;
	}
*/
	else
	{
		CComQIPtr<IVideoWindow> pVideoWindow(m_pGraphBuilder);
		if(pVideoWindow == NULL)
		{
			__DEBUG__("Fail to get IVideoWindow", _NULL);
			return false;
		}//if

		JIB(pVideoWindow->GetWindowPosition(&pRect->left, &pRect->top, &pRect->right, &pRect->bottom));
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 재생시에 화면의 비율을 유지하는지를 설정한다. \n
/// @param (ASPECT_RATIO_MODE) mode : (in) STRETCHED, LETTER_BOX, ....
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetAspectRatioMode(ASPECT_RATIO_MODE mode)
{
	bool bRet = FALSE;
	HRESULT hr;

	if(m_nRenderType == E_WINDOWLESS)
	{
		if(GetOSVersion() == E_OS_WIN7)
		{
			CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl9", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->SetAspectRatioMode(mode));
		}
		else
		{
			CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
			if(pWindowlessControl == NULL)
			{
				__DEBUG__("Fail to get IVMRWindowlessControl", _NULL);
				return false;
			}//if

			JIB(pWindowlessControl->SetAspectRatioMode(mode));
		}//if
	}
/*
	else if(m_nRenderType == E_EVR)
	{
		HRESULT hr;
		if(m_pVideoDisplayControl == NULL)
		{
			__DEBUG__("m_pVideoDisplayControl is NULL", _NULL);
			return false;
		}//if

		JIB(m_pVideoDisplayControl->SetAspectRatioMode(mode));
	}
*/
	else if(m_nRenderType == E_OVERLAY)
	{
		CComPtr<IPin> pPin;
		JIB(m_pFilter->FindPin(L"Input0", &pPin));
		if(SUCCEEDED(hr))
		{
			CComQIPtr<IMixerPinConfig, &IID_IMixerPinConfig> pMixerPinConfig(pPin);
			if(pMixerPinConfig == NULL)
			{
				__DEBUG__("Fail to get IMixerPinConfig", _NULL);
				return false;
			}//if

			JIB(pMixerPinConfig->SetAspectRatioMode((AM_ASPECT_RATIO_MODE)mode))
		}//if
	}
	else
	{
		CComQIPtr<IVMRAspectRatioControl> pAspectRatio(m_pFilter);
		if(pAspectRatio == NULL)
		{
			__DEBUG__("Fail to get IVMRAspectRatioControl", _NULL);
			return false;
		}//if

		JIB(pAspectRatio->SetAspectRatioMode(mode));
	}//if

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우 미디어 파일인지를 판단한다. \n
/// @param (LPCTSTR) lpszFilename : (in) 미디어 파일 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::IsWindowsMediaFile(LPCTSTR lpszFilename)
{
	//먼저 헤더 포켓을 보고 왼도우미디어 타입인지 확인을 하고
	//그 다음에 고덱아이디로 판단을 한다.
	//이유는 그냥 mediainfo를 사용하면, mpeg등의 파일을 판단하는데에
	//시간이 오래 걸리고 정보를 못 뽑아오는 경우가 있어서....
	AviCodecInfo codec;
	GetDivxInfo(lpszFilename, &codec);

	if(codec.videoCodec.dwCodec == FCCWMV3
		/*&& codec.audioCodec.dwCodec == FCCWMV3*/)
	{
		String strVideoCodecID, strAudioCodecID;
		int nAudioCodecID = 0;
		MediaInfo clsMInfo;
		clsMInfo.Open(lpszFilename);

		strVideoCodecID = clsMInfo.Get(Stream_Video, 0, _T("CodecID"), Info_Text, Info_Name);
		strAudioCodecID = clsMInfo.Get(Stream_Audio, 0, _T("CodecID"), Info_Text, Info_Name);
		nAudioCodecID = atoi(strAudioCodecID.c_str());
		__DEBUG__("VideoCodecID", strVideoCodecID.c_str());
		__DEBUG__("Audio Format", nAudioCodecID);
		clsMInfo.Close();
		if(strstr(strVideoCodecID.c_str(), "WMV")
			&& (nAudioCodecID >= 160 && nAudioCodecID <= 163))
		{
			return true;
		}//if
	}//if	

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우 미디어 파일을 재생하기위한 필터들을 구성한다. \n
/// @param (LPCWSTR) lpszFileName : (in) 미디어 파일 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::SetWindowsMediaFilters(LPCWSTR lpszFileName)
{
	HRESULT hr;

	CComPtr <IBaseFilter> pSource;
	CComPtr <IFileSourceFilter> pFileSource;
	CComPtr <IPin> pOutputPin;
	CComPtr <IPin> pInputPin;
	CComPtr <IBaseFilter> pAudioRenderer;

	// Load the improved ASF reader filter by CLSID
	JIB(CoCreateInstance(CLSID_WMAsfReader, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&pSource));
	
	// Add the ASF reader filter to the graph.  For ASF/WMV/WMA content,
	// this filter is NOT the default and must be added explicitly.
	JIB(m_pGraphBuilder->AddFilter(pSource, L"WM ASF Reader"));

	// Set its source filename
	JIB(pSource->QueryInterface(IID_IFileSourceFilter, (void **) &pFileSource));

	// Attempt to load this file
	JIB(pFileSource->Load(lpszFileName, NULL));

	ULONG count = 0;
	CountTotalFilterPins(pSource, &count);

	for (ULONG i = 0; i < count; i ++)
	{
		pOutputPin = GetOutPin(pSource, i);
		PIN_INFO info;
		pOutputPin->QueryPinInfo(&info);
		info.pFilter->Release();

		if(wcsstr(info.achName, L"Video"))
		{
			IBaseFilter* Codec = CreateEncodec(_T("WMVideo Decoder DMO"));
			if(Codec)
			{
				m_pGraphBuilder->AddFilter(Codec, L"WMVideo Decoder DMO");
				Codec->Release();

				pInputPin = GetInPin(Codec, 0);

				JIB(m_pGraphBuilder->Connect(pOutputPin, pInputPin));

				pOutputPin = GetOutPin(Codec, 0);
				pInputPin = GetInPin(m_pFilter, 0);

				JIB(m_pGraphBuilder->Connect(pOutputPin, pInputPin));
			}//if
		}
		else if(wcsstr(info.achName, L"Audio"))
		{
			IBaseFilter* Codec = CreateEncodec(_T("WMAudio Decoder DMO"));
			if(Codec)
			{
				m_pGraphBuilder->AddFilter(Codec, L"WMAudio Decoder DMO");
				Codec->Release();

				pInputPin = GetInPin(Codec, 0);

				JIB(m_pGraphBuilder->Connect(pOutputPin, pInputPin));

				pOutputPin = GetOutPin(Codec, 0);

				if(NULL == pAudioRenderer)
				{
					JIB(CoCreateInstance(CLSID_DSoundRender, NULL,
						CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void **)&pAudioRenderer))
					
					// The audio renderer was successfully created, so add it to the graph
					JIB(m_pGraphBuilder->AddFilter(pAudioRenderer, L"Audio Renderer"));			
				}//if

				pInputPin = GetInPin(pAudioRenderer, 0);

				JIB(m_pGraphBuilder->Connect(pOutputPin, pInputPin));
			}//if
		}//if
	}//for

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 코덱의 이름으로 필터를 찾아준다 \n
/// @param (CString) inFriendlyName : (in) 고덱의 이름
/// @return <형: IBaseFilter*> \n
///			<NULL: 실패> \n
///			<IBaseFilter: 찾은 필터의 주소값> \n
/////////////////////////////////////////////////////////////////////////////////
IBaseFilter* CVMRRender::CreateEncodec(CString inFriendlyName)
{
	HRESULT hr = NOERROR;
	ICreateDevEnum * enumHardware =NULL;
	IBaseFilter    * hardwareFilter = NULL;

	LIF(CoCreateInstance(CLSID_SystemDeviceEnum,NULL,CLSCTX_INPROC_SERVER, IID_ICreateDevEnum,(void **)&enumHardware));
	if(FAILED(hr))
	{
		return NULL;
	}//if

	IEnumMoniker * enumMoniker = NULL;

	LIF(enumHardware->CreateClassEnumerator(CLSID_LegacyAmFilterCategory, &enumMoniker,0));
	if(FAILED(hr))
	{
		return NULL;
	}//if

	if(enumMoniker)
	{
		enumMoniker->Reset();

		IMoniker * moniker = NULL;
		char friendlyName[256];
		ZeroMemory(friendlyName,256);

		while(S_OK == enumMoniker->Next(1,&moniker,0))
		{
			if(moniker)
			{
				IPropertyBag * proBag = NULL;
				VARIANT    name;
				VariantInit(&name);

				hr = moniker->BindToStorage(NULL,NULL,IID_IPropertyBag,(void **)&proBag);
				if(SUCCEEDED(hr))
				{
					name.vt = VT_BSTR;
					proBag->Read(L"FriendlyName",&name,NULL);
				}//if

				if(SUCCEEDED(hr))
				{
					WideCharToMultiByte(CP_ACP, 0, name.bstrVal, -1,friendlyName, 256, NULL, NULL);
					CString str = (CString)friendlyName;
					if(inFriendlyName == str)
					{
						moniker->BindToObject(NULL,NULL,IID_IBaseFilter, (void **)&hardwareFilter);
					}//if
				}//if

				if(proBag)
				{
					proBag->Release();
					proBag = NULL;
				}//if

				VariantClear(&name);

				moniker->Release();
			}//if
		}//while

		enumMoniker->Release();
	}//if

	enumHardware->Release();

	return hardwareFilter;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우 버전을 구한다.(major 버전만 보고 판단한다.)\n
/// @return <형: int> \n
///			<-1: error> \n
///			<5: WinXP> \n
///			<6: Vista, Win7> \n
/////////////////////////////////////////////////////////////////////////////////
int	CVMRRender::GetOSVersion()
{
	OSVERSIONINFO stOSver;
	memset(&stOSver, 0x00, sizeof(OSVERSIONINFO));
	stOSver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if(GetVersionEx(&stOSver))
	{        
		return (int)stOSver.dwMajorVersion;
	}//if

	__DEBUG__("Get OS version fail", _NULL);
	return -1;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 현재화면의 이미지를 얻어온다. \n
/// @param (BYTE**) pByte : (out) 이미지 데이터
/// @param (double) dbPos : (in) 이미지를 얻어올 위치(시간)
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CVMRRender::GetCurrentImage(BYTE** pByte, double dbPos)
{
	if(m_nRenderType != E_WINDOWLESS)
	{
		return false;
	}//if

	//샘플의 위치로 이동
	SetPosition(dbPos);
	if(!Play())
	{
		return false;
	}//if

	if(!Pause())
	{
		return false;
	}//if

	Sleep(1000);		//잠시 기다려야지만 이미지를 얻어온다...ㅠㅠ

	HRESULT hr;
	if(GetOSVersion() == E_OS_WIN7)
	{
		CComQIPtr<IVMRWindowlessControl9> pWindowlessControl(m_pFilter);
		if(pWindowlessControl == NULL)
		{
			return false;
		}//if

		JIB(pWindowlessControl->GetCurrentImage(pByte));
	}
	else
	{
		CComQIPtr<IVMRWindowlessControl> pWindowlessControl(m_pFilter);
		if(pWindowlessControl == NULL)
		{
			return false;
		}//if
		
		JIB(pWindowlessControl->GetCurrentImage(pByte));
	}//if

	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		return false;
	}//if

	JIB(pMediaControl->Stop());

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 모니터가 변경되었을때 처리를 해 준다. \n
/////////////////////////////////////////////////////////////////////////////////
void CVMRRender::DisplayChanged()
{
	if(m_nRenderType != E_WINDOWLESS)
	{
		SetAspectRatioMode(STRETCHED);
		CRect rc;
		::GetClientRect(m_hParentWnd, &rc);
		SetVideoRect(rc);		// 윈도우의 크기를 정함
		RePaintVideo();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Video render의 type을 설정한다. \n
/// @param (int) nType : (in/out) 설명
/////////////////////////////////////////////////////////////////////////////////
void CVMRRender::SetRenderType(int nType)
{
	if(GetUseOverlayMixer() == E_NOT_USE_OVERLAY_MIXER)
	{
		nType = E_WINDOWLESS;
	}//if

	m_nRenderType = nType;
}
*/