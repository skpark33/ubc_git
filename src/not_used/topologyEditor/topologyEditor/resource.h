//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by topologyEditor.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TOPOLOGYEDITOR_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDI_ICON_YELLOW                 131
#define IDI_ICON_RED                    132
#define IDI_ICON_GREEN                  133
#define IDB_BITMAP1                     134
#define IDC_PIC_CTRL                    1000
#define IDC_BUTTON1                     1002

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
