// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// topologyEditor.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


CString	FindExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--){
		if (strFileName[i] == '.'){
			return strFileName.Mid(i+1);
		}
	}
	return CString(_T(""));
}
