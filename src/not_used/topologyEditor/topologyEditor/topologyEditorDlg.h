// topologyEditorDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"

#include "ximage.h"
#include "PictureEx.h"

#define MAX_ICON_NUMBER  2

// CtopologyEditorDlg 대화 상자
class CtopologyEditorDlg : public CDialog
{
// 생성입니다.
public:
	CtopologyEditorDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TOPOLOGYEDITOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	CBitmapButton m_button[MAX_ICON_NUMBER];
	IconInfo	m_info[MAX_ICON_NUMBER];

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_picCtrl;
	CxImage					m_bgImage;

	virtual BOOL DestroyWindow();
	//afx_msg void OnBnClickedButton1();
	virtual BOOL PreTranslateMessage(MSG* pMsg);






};
