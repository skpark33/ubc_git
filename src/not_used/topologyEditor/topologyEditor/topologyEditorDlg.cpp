// topologyEditorDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "topologyEditor.h"
#include "topologyEditorDlg.h"
#include "MemDC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CtopologyEditorDlg 대화 상자




CtopologyEditorDlg::CtopologyEditorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CtopologyEditorDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CtopologyEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PIC_CTRL, m_picCtrl);
}

BEGIN_MESSAGE_MAP(CtopologyEditorDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	//ON_BN_CLICKED(IDC_BUTTON1, &CtopologyEditorDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CtopologyEditorDlg 메시지 처리기

BOOL CtopologyEditorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.


	CString m_strMediaFullPath = "C:\\Users\\Administrator\\Pictures\\aurora_blue.jpg";
	CString strExt = FindExtension(m_strMediaFullPath);

	int nImgType = CxImage::GetTypeIdFromName(strExt);
	bool bRet = m_bgImage.Load(m_strMediaFullPath, nImgType);


	//m_button.Attach(((CBitmapButton *)GetDlgItem(IDC_BUTTON1))->m_hWnd);
	
	m_button[0].Create(NULL, 
					WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
					CRect(100,100,100,100),
					this,
					IDC_BUTTON1);
	m_button[0].LoadBitmaps(IDB_BITMAP1,IDB_BITMAP1,IDB_BITMAP1,IDB_BITMAP1);
	m_button[0].SizeToContent();

	m_button[1].Create(NULL, 
					WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
					CRect(200,200,100,100),
					this,
					IDC_BUTTON1);
	m_button[1].LoadBitmaps(IDB_BITMAP1,IDB_BITMAP1,IDB_BITMAP1,IDB_BITMAP1);
	m_button[1].SizeToContent();
	

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CtopologyEditorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CtopologyEditorDlg::OnPaint()
{

	if(m_bgImage.IsValid()){
		CRect rect;
		m_picCtrl.GetWindowRect(rect);
		ScreenToClient(rect);
		rect.DeflateRect(1,1);

		//CRect rect;
		//GetClientRect(rect);

		CPaintDC dc(this); // device context for painting
		CMemDC memDC(&dc);

		dc.FillSolidRect(rect, RGB(255,255,255));

		double scale;

		int image_width = m_bgImage.GetWidth();
		int image_height = m_bgImage.GetHeight();

		int scr_width = rect.Width();
		int scr_height = rect.Height();

		int width, height;
		width = scr_width;
		height = image_width * scr_height / scr_width;
		if(height < image_height)
		{
			height = scr_height;
			width = image_height * scr_width / scr_height;

			scale = ((double)scr_height) / ((double)image_height);
		}
		else
		{
			scale = ((double)scr_width) / ((double)image_width);
		}

		rect.right = rect.left + m_bgImage.GetWidth() * scale;
		rect.bottom = rect.top + m_bgImage.GetHeight() * scale;

		m_bgImage.Draw2(memDC.m_hDC, rect);
	}


	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);

		CRect rect;
		GetWindowRect(rect);
	
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CtopologyEditorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


BOOL CtopologyEditorDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}//if


	return CDialog::DestroyWindow();
}
/*
void CtopologyEditorDlg::OnBnClickedButton1()
{
	bool static once = false;
	if(!once){
		m_button[2].Create(NULL, 
						WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,
						CRect(300,300,100,100),
						this,
						IDC_BUTTON1);
		m_button[2].LoadBitmaps(IDB_BITMAP1,IDB_BITMAP1,IDB_BITMAP1,IDB_BITMAP1);
		m_button[2].SizeToContent();
		once = true;
	}else{
		m_button[2].DestroyWindow();
		once =  false;
	}
	this->RedrawWindow();
}
*/
BOOL CtopologyEditorDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_LBUTTONDOWN){
		for(int i=0;i<MAX_ICON_NUMBER;i++){
			if(m_info[i].selected){
				continue;
			}

			CRect bRect;
			m_button[i].GetWindowRect(bRect);
			int y = bRect.top;
			int dy = bRect.bottom;
			int x = bRect.left;
			int dx = bRect.right;
		

			if( x <= pMsg->pt.x && pMsg->pt.x <= dx &&
				y <= pMsg->pt.y && pMsg->pt.y <= dy ){
			//if(pMsg->hwnd == m_button[i].GetSafeHwnd()){
				m_info[i].selected = true;
				m_info[i].x = x;
				m_info[i].y = y;
				m_info[i].dx = dx;
				m_info[i].dy = dy;
				m_info[i].ptx = pMsg->pt.x;
				m_info[i].pty = pMsg->pt.y;


				//CString msg;
				//msg.Format("%d th m_button Left BUTTON DOWN pt(%d,%d), button(%d,%d)", 
				//	i, pMsg->pt.x, pMsg->pt.y, x,y);
				//this->MessageBox(msg);
				break;
			}
		}
	}
	if(pMsg->message == WM_MOUSEMOVE){
		for(int i=0;i<MAX_ICON_NUMBER;i++){
			if(m_info[i].selected){

				int x = m_info[i].x;
				int dx = m_info[i].dx;
				int y = m_info[i].y;
				int dy = m_info[i].dy;

				int width = dx-x;
				int height = dy-y;

				int new_ptx = pMsg->pt.x;
				int new_pty = pMsg->pt.y;


				CRect wRect;
				GetWindowRect(wRect);

				int ddx = (x - wRect.left) + (new_ptx - m_info[i].ptx);
				int ddy = (y - wRect.top)  + (new_pty - m_info[i].pty);

				CRect pRect;
				m_picCtrl.GetWindowRect(pRect);

				if(ddx < pRect.left - wRect.left)	ddx = pRect.left - wRect.left;
				if(ddy < pRect.top - wRect.top)		ddy = pRect.top - wRect.top;
				if(ddx > pRect.right - wRect.left - width)	ddx = pRect.right - wRect.left - width;
				if(ddy > pRect.bottom - wRect.top - height)  ddy = pRect.bottom - wRect.top - height;

				m_button[i].SetWindowPos(&wndTop,ddx,ddy, width,height,SWP_SHOWWINDOW);


				break;
			}
		}
	}

	if(pMsg->message == WM_LBUTTONUP){
		for(int i=0;i<MAX_ICON_NUMBER;i++){
			if(m_info[i].selected){
				m_info[i].selected = false;
				//CString msg;
				//msg.Format("%d th m_button Left BUTTON UP %d,%d", i, pMsg->pt.x, pMsg->pt.y);
				//this->MessageBox(msg);
				break;
			}
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
