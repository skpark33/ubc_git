/*! \class mgwCallImpl
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief UTV_Mgr Implement
 *  (Environment: Visibroker 7.0, Windows XP)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _mgwCallImpl_h_
#define _mgwCallImpl_h_


#include <cci/libValue/cciEntity.h>
#include <cci/libValue/cciAttribute.h>
#include <cci/libValue/cciAttributeList.h>
#include <cci/libValue/cciRequest.h>
#include <cci/libValue/cciReply.h>
#include <cci/libValue/cciFilter.h>
#include <cci/libValue/cciScopeInfo.h>

#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libBase/ciBaseType.h>
#include <cci/libUtil/cciPOAUtil.h>

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>
#include "aci/libCall/aciCall.h"

class mgwCallImpl : virtual public  aciCall {
public:
	mgwCallImpl(const char* pname) : aciCall(pname) {} ;
	virtual ~mgwCallImpl() {} ;
	virtual aciReply*	newCallReply(cciEntity& pEntity, const char* directive);
protected:
};	

#endif //_mgwCallImpl_h_
