/*! \class mgwMon
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief DC Mgr Implement
 *  (Environment: SORBA 2.2, Solaris 2.8)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/08/06 19:01:00
 */

#ifndef _mgwMon_h_
#define _mgwMon_h_


#include <ci/libTimer/ciTimer.h>




class mgwMon :  public  ciPollable {
public:

	mgwMon(const char* id);
	virtual ~mgwMon();

	virtual void processExpired(ciString name, int counter, int interval);
protected:

	void		clearError() { _errString == ""; }
	void		setError(const char* fmt,...);

	ciString	_errString;

	void		_processSMS();
	void		_processPromotion();

	ciBoolean	_isSMS(ciString& sCode);
	ciBoolean	_isPromotion(ciString& sCode);

	ciBoolean	_updateComment(ciString& sCode, ciString& sData, ciString& pNo);
	ciBoolean	_updateValueList(ciString& sCode, int index, int value);


	ciBoolean	_postScheduleUpdated(const char* contentsId);
	ciBoolean	_updateIsProcessed(ciString& id,ciBoolean isProcessed,const char* tableName, const char* idField);
	ciBoolean	_updateIsProcessedPromotion(ciString& today,ciString sCode,int index, ciBoolean isProcessed);

	ciInt		_postScheduleUpdatedWithContents(const char* table, const char* contentsId);
	ciBoolean	_postScheduleUpdated(const char* table,
									const char* siteId, 
									const char* hostId,
									const char* scheduleId,
									ciLong	castingState,
									ciBoolean	isPhone);

	cciObject*	_proObj;
	cciObject*	_smsObj;
	ciString	_isProcessed;

	/*
	class _ProData {
	public:	
		_ProData() {
			frameIdList		= new cciStringList();
			siteIdList		= new cciStringList();
			addrList		= new cciStringList();
			franchizeType	= new cciStringList();
			hostIdList		= new cciStringList();
			endTime			= new cciString();
			contentsId		= new cciString();
		}
		~_ProData() {
			delete frameIdList;
			delete siteIdList;
			delete addrList;
			delete franchizeType;
			delete hostIdList;
			delete endTime;
			delete contentsId;
		}
		cciStringList *frameIdList;
		cciStringList *siteIdList;
		cciStringList *addrList;
		cciStringList *franchizeType;
		cciStringList *hostIdList;
		cciString	*endTime;
		cciString	*contentsId;
	};
*/

//	ciBoolean	_createRequest(ciString& sCode, ciString& sData, ciString& pNo);
//	ciBoolean	_updatePromotion(ciString& sCode, ciString& sData);
//	ciBoolean	_getPromotionInfo(ciString& sCode, _ProData& pData);

//	ciBoolean _evalResult(const char* siteId, const char* hostId, ciBoolean isAlive);
//	ciBoolean _postEvent(const char* siteId, const char* hostId, ciBoolean isAlive);
//	ciBoolean _setOperState(const char* hostId, ciBoolean isAlive);
	
//	typedef map<ciString, ciBoolean> _HostMap;
//	_HostMap _map;
//	ciMutex _mapLock;

};	

#endif //_mgwMon_h_
