// FTPForwardingServerDlg.h : 헤더 파일
//

#pragma once

#include "afxwin.h"
#include "ListenSocket.h"

#include "ReposControl.h"
#include "afxcmn.h"


// CFTPForwardingServerDlg 대화 상자
class CFTPForwardingServerDlg : public CDialog
{
// 생성입니다.
public:
	CFTPForwardingServerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FTPFORWARDINGSERVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

	CReposControl		m_ReposControl;

	CListenSocket*		m_MainListenSocket;

	CMutex				m_mutexListenSocket;
	CListenSocketList	m_listListenSocket;
	CMutex				m_mutexCommandSocket;
	CBypassSocketList	m_listCommandSocket;
	CMutex				m_mutexDataSocket;
	CBypassSocketList	m_listDataSocket;

	void			InitValueSettings();

public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);

	afx_msg void OnBnClickedAutoLocalIp();
	afx_msg void OnBnClickedSameAsServerPort();
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnBnClickedButtonModify();
	afx_msg void OnBnClickedButtonApply();
	afx_msg void OnBnClickedButtonCancel();

	afx_msg LRESULT OnSendLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnConnectCommandSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnConnectDataSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnOpenDataSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseCommandSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseDataSocket(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseListenSocket(WPARAM wParam, LPARAM lParam);

	CEdit			m_editLog;

	CIPAddressCtrl	m_ipServer;
	CEdit			m_editServerIP;
	CEdit			m_editServerPort;

	CIPAddressCtrl	m_ipLocal;
	CEdit			m_editLocalIP;
	CButton			m_chkAutoLocalIP;
	CEdit			m_editLocalPort;
	CButton			m_chkSameAsServerPort;


	CButton			m_btnStart;
	CButton			m_btnStop;
	CButton			m_btnModify;
	CButton			m_btnApply;
	CButton			m_btnCancel;

	BOOL			ResetAllConnection();

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
};
