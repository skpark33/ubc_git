#pragma once


#include <afxtempl.h>


/////////////////////////////////////////////////////////////////////////////////////////
typedef struct {
	CWnd*	pWnd;
	CRect	rectParent;
	CRect	rect;
	int		nLeft;
	int		nTop;
	int		nRight;
	int		nBottom;
	int		nHorzCount;
	int		nVertCount;
	CWnd*	pParentWnd;
}	REPOS_CONTROL_INFO;

typedef CArray<REPOS_CONTROL_INFO, REPOS_CONTROL_INFO&>		REPOS_CONTROL_INFO_LIST;


/////////////////////////////////////////////////////////////////////////////////////////
class CReposControl
{
public:
	CReposControl(CWnd*	pParent);
	virtual ~CReposControl();

protected:
	int		m_nWindowWidth;
	int		m_nWindowHeight;

	CWnd*	m_pParent;
	CRect	m_rectParent;

	bool	m_bInit;

	REPOS_CONTROL_INFO_LIST		m_ReposControlInfoList;

public:

	void	Clear();

	void	AddControl(CWnd* pControl, int nLeft, int nTop, int nRight, int nBottom, int nHorzCount=1, int nVertCount=1, CWnd* pParentWnd=NULL);
	void	MoveControl(BOOL bRepaint=FALSE);

	void	Move(CWnd* pWnd, int left, int top, int right, int bottom);

	void	SetWindowSize(int nWidth, int nHeight, bool bForce=false);
};


/////////////////////////////////////////////////////////////////////////////////////////
#define		REPOS_FIX				-1
#define		REPOS_MOVE				0
#define		REPOS_RESIZE_DIV2		1
#define		REPOS_RESIZE_DIV3		2
#define		REPOS_RESIZE_DIV4		3
#define		REPOS_RESIZE_DIV5		5
#define		REPOS_RESIZE_DIV6		6
#define		REPOS_RESIZE_DIV7		7
#define		REPOS_RESIZE_DIV8		8
#define		REPOS_RESIZE_DIV9		9
#define		REPOS_RESIZE_DIV10		10

