#pragma once

#include <afxmt.h>

// CHttpRequest

class CHttpRequest
{
public:
	virtual ~CHttpRequest();

	static	CHttpRequest*	getInstance();
protected:
	static	CHttpRequest*	_instance;
	static	CMutex			_mutex;

	CHttpRequest();

	CString	m_strServerIP;
	UINT	m_nServerPort;

	BOOL	Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg);

public:
	BOOL	Init(LPCTSTR lpszServerIP, UINT nServerPort);

	BOOL	RequestGet(LPCTSTR lpUrl, CString &strOutMsg);
	BOOL	RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg);

	static CString	ConvertToURLString(CString str, bool bReturnNullString);
	static CString	ToString(int nValue);
};
