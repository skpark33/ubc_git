// FTPMonitorView.cpp : CFTPMonitorView 클래스의 구현
//

#include "stdafx.h"
#include "FTPMonitor.h"

#include "FTPMonitorDoc.h"
#include "FTPMonitorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		DEFAULT_IP				"127.0.0.1"
#define		DEFAULT_WEBPORT			"8080"


#define		TIMER_INIT_ID			1025
#define		TIMER_INIT_TIME			100
#define		TIMER_INIT_RECONN_TIME	(60*1000)		// 1min

#define		TIMER_UPDATE_ID			1026
#ifdef _DEBUG
	#define		TIMER_UPDATE_TIME		1000			// 1sec
#else
	#define		TIMER_UPDATE_TIME		(60*1000)		// 1min
#endif

// CFTPMonitorView

IMPLEMENT_DYNCREATE(CFTPMonitorView, CFormView)

BEGIN_MESSAGE_MAP(CFTPMonitorView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_COMMAND(ID_START_MONITOR, &CFTPMonitorView::OnStartMonitor)
	ON_COMMAND(ID_STOP_MONITOR, &CFTPMonitorView::OnStopMonitor)
	ON_UPDATE_COMMAND_UI(ID_START_MONITOR, &CFTPMonitorView::OnUpdateStartMonitor)
	ON_UPDATE_COMMAND_UI(ID_STOP_MONITOR, &CFTPMonitorView::OnUpdateStopMonitor)
END_MESSAGE_MAP()

// CFTPMonitorView 생성/소멸

CFTPMonitorView::CFTPMonitorView()
	: CFormView(CFTPMonitorView::IDD)
{
	// TODO: 여기에 생성 코드를 추가합니다.

	m_bMonitoring = true;

	m_nUpdateInterval = TIMER_UPDATE_TIME;
}

CFTPMonitorView::~CFTPMonitorView()
{
}

void CFTPMonitorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PMO, m_lcPMO);
}

BOOL CFTPMonitorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CFTPMonitorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	CString ip,port,updateinterval;
	GetArguments("+ip", ip);
	GetArguments("+webport", port);
	GetArguments("+updateInterval", updateinterval);

	if(ip.GetLength() == 0)
		ip = DEFAULT_IP;
	if(port.GetLength() == 0)
		port = DEFAULT_WEBPORT;
	if(updateinterval.GetLength() > 0)
		m_nUpdateInterval = _ttoi(updateinterval);

	__DEBUG__("+ip", ip);
	__DEBUG__("+webport", port);
	__DEBUG__("+updateInterval", m_nUpdateInterval);

	m_httpRequest.Init(CHttpRequest::CONNECTION_USER_DEFINED, ip, _ttoi(port));

	m_lcPMO.InsertColumn(COL_PMID, "pmId", 0, 40);
	m_lcPMO.InsertColumn(COL_IP, "ipAddress", 0, 100);
	//m_lcPMO.InsertColumn(COL_ID, "ftpId", 0, width);
	//m_lcPMO.InsertColumn(COL_PWD, "ftpPasswd", 0, width);
	//m_lcPMO.InsertColumn(COL_PORT, "ftpPort", 0, width);
	m_lcPMO.InsertColumn(COL_CONN_COUNT, "currentConnectionCount", 0, 50);
	m_lcPMO.InsertColumn(COL_STATUS, "status", 0, 150);
	m_lcPMO.InsertColumn(COL_LASTUPDATETIME, "Last Update Time", 0, 120);

	SetTimer(TIMER_INIT_ID, TIMER_INIT_TIME, NULL);
}


// CFTPMonitorView 진단

#ifdef _DEBUG
void CFTPMonitorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CFTPMonitorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CFTPMonitorDoc* CFTPMonitorView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFTPMonitorDoc)));
	return (CFTPMonitorDoc*)m_pDocument;
}
#endif //_DEBUG


// CFTPMonitorView 메시지 처리기

void CFTPMonitorView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if(GetSafeHwnd() && m_lcPMO.GetSafeHwnd())
	{
		m_lcPMO.MoveWindow(0, 0, cx, cy);
	}
}

void CFTPMonitorView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CFormView::OnTimer(nIDEvent);

	if(nIDEvent == TIMER_INIT_ID)
	{
		KillTimer(TIMER_INIT_ID);
		DeleteAllItems();

		CString title;
		title.LoadString(AFX_IDS_APP_TITLE);

		__DEBUG__("RequestPost", "UBC_Server/get_od_list.asp");

		CString out_msg;
		if(m_httpRequest.RequestPost("/UBC_Server/get_od_list.asp", "id=ubc&pwd=sqicop", out_msg))
		{
			__DEBUG__("Response", out_msg);

			CStringArray line_list;

			GetLineList(out_msg, line_list);

			if(line_list.GetCount() > 0 && line_list.GetAt(0) == "OK")
			{
				PMO_INFO* info = NULL;

				int index = 0;
				for(int i=0; i<line_list.GetCount(); i++)
				{
					const CString& line = line_list.GetAt(i);

					int equal_idx = line.Find(_T("="));

					if(equal_idx > 0 && line.GetLength() >= equal_idx+1 )
					{
						int pos = 0;
						CString key = line.Tokenize(_T("="), pos);
						LPCTSTR szValue = (LPCTSTR)line+equal_idx+1;

						if(key == "pmId")
						{
							info = new PMO_INFO;
							info->pmId = szValue;
							m_lcPMO.InsertItem(index, info->pmId);
							m_lcPMO.SetItemData(index++, (DWORD)info);
						}
						else if(key == "ipAddress" && info)	info->ipAddress = szValue;
						else if(key == "ftpId" && info)		info->ftpId = szValue;
						else if(key == "ftpPasswd" && info)	info->ftpPasswd = szValue;
						else if(key == "ftpPort" && info)	info->ftpPort = szValue;
						else if(key == "currentConnectionCount" && info) info->currentConnectionCount = _ttoi(szValue);
					}
				}

				int count = m_lcPMO.GetItemCount();
				for(int i=0; i<count; i++)
				{
					PMO_INFO* info = (PMO_INFO*)m_lcPMO.GetItemData(i);

					__DEBUG__("pmId", info->pmId);
					__DEBUG__("ipAddress", info->ipAddress);

					m_lcPMO.SetItemText(i, COL_IP, info->ipAddress);

					info->monitor = new CFTPConnectionMonitor();
					info->monitor->Create(info->ipAddress, info->ftpPasswd, 14147, m_nUpdateInterval/2);
				}

				OnTimer(TIMER_UPDATE_ID);
			}
			else if(line_list.GetCount() > 0 && line_list.GetAt(0) == "Fail")
			{
				title += " - Authentication Fail !!!";
				__ERROR__("Authentication Fail !!!", NULL);

				SetTimer(TIMER_INIT_ID, TIMER_INIT_RECONN_TIME, NULL);
			}
			else
			{
				title += " - Fail to connect server !!!";
				__ERROR__("Fail to connect server !!!", NULL);

				SetTimer(TIMER_INIT_ID, TIMER_INIT_RECONN_TIME, NULL);
			}
		}
		else
		{
			title += " - Fail to connect server !!!";
				__ERROR__("Fail to connect server !!!", NULL);

			SetTimer(TIMER_INIT_ID, TIMER_INIT_RECONN_TIME, NULL);
		}

		::AfxGetMainWnd()->SetWindowText(title);
	}
	else if(nIDEvent == TIMER_UPDATE_ID)
	{
		KillTimer(TIMER_UPDATE_ID);

		int count = m_lcPMO.GetItemCount();
		for(int i=0; i<count; i++)
		{
			PMO_INFO* info = (PMO_INFO*)m_lcPMO.GetItemData(i);
			info->currentConnectionCount = info->monitor->GetConnCount();

			CString str;
			str.Format("%d", info->currentConnectionCount);
			m_lcPMO.SetItemText(i, COL_CONN_COUNT, str);

			CString send_msg, out_msg;
			send_msg.Format("pmId=%s&count=%d", info->pmId, info->currentConnectionCount );

			m_httpRequest.RequestPost("/UBC_Server/set_od.asp", send_msg, out_msg);
			if(out_msg == "OK")
			{
				if(info->currentConnectionCount < 0)
				{
					__ERROR__("Fail to connect FTP-Server", info->pmId);
					m_lcPMO.SetItemText(i, COL_STATUS, "Fail to connect FTP-Server");
				}
				else
				{
					m_lcPMO.SetItemText(i, COL_STATUS, "Update OK");
				}
			}
			else if(out_msg == "Fail")
			{
				__ERROR__(send_msg, "Not exist PMO");
				m_lcPMO.SetItemText(i, COL_STATUS, "Not exist PMO");
			}
			else
			{
				__ERROR__(send_msg, "Fail to connect server");
				m_lcPMO.SetItemText(i, COL_STATUS, "Fail to connect server");
			}

			CTime cur_tm = CTime::GetCurrentTime();
			m_lcPMO.SetItemText(i, COL_LASTUPDATETIME, cur_tm.Format("%Y/%m/%d %H:%M:%S"));
		}

		SetTimer(TIMER_UPDATE_ID, m_nUpdateInterval, NULL);
	}
}

void CFTPMonitorView::DeleteAllItems()
{
	int count = m_lcPMO.GetItemCount();
	for(int i=0; i<count; i++)
	{
		PMO_INFO* info = (PMO_INFO*)m_lcPMO.GetItemData(i);
		if(info) delete info;
		m_lcPMO.SetItemData(i, NULL);
	}
	m_lcPMO.DeleteAllItems();
}

void CFTPMonitorView::GetLineList(CString& str, CStringArray& line_list)
{
	int pos = 0;
	CString token = str.Tokenize(_T("\r\n"), pos);
	while(token != _T(""))
	{
		line_list.Add(token);
		token = str.Tokenize(_T("\r\n"), pos);
	}
}

void CFTPMonitorView::OnStartMonitor()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.

	__DEBUG__("OnStartMonitor()", NULL);

	m_bMonitoring = true;

	SetTimer(TIMER_UPDATE_ID, m_nUpdateInterval, NULL);
}

void CFTPMonitorView::OnStopMonitor()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.

	__DEBUG__("OnStopMonitor()", NULL);

	m_bMonitoring = false;

	KillTimer(TIMER_UPDATE_ID);
}

void CFTPMonitorView::OnUpdateStartMonitor(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.

	pCmdUI->Enable(!m_bMonitoring);
}

void CFTPMonitorView::OnUpdateStopMonitor(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.

	pCmdUI->Enable(m_bMonitoring);
}
