// FTPMonitorView.h : CFTPMonitorView 클래스의 인터페이스
//


#pragma once
#include "afxcmn.h"

#include "../libFTPConnectionMonitor/FTPConnectionMonitor.h"
#include "../libHttpRequest/HttpRequest.h"


class PMO_INFO
{
public:
	PMO_INFO() { currentConnectionCount=0; monitor=NULL; };
	~PMO_INFO() { if(monitor) { monitor->DestroyWindow(); delete monitor; } };

	CString	pmId;
	CString	ipAddress;
	CString	ftpId;
	CString	ftpPasswd;
	CString	ftpPort;
	int		currentConnectionCount;
	CString	status;
	CFTPConnectionMonitor*	monitor;
};


class CFTPMonitorView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CFTPMonitorView();
	DECLARE_DYNCREATE(CFTPMonitorView)

public:
	enum{ IDD = IDD_FTPMONITOR_FORM };

	enum {
		COL_PMID = 0,
		COL_IP,
		//COL_ID,
		//COL_PWD,
		//COL_PORT,
		COL_CONN_COUNT,
		COL_STATUS,
		COL_LASTUPDATETIME,
		COL_COUNT,
	};

// 특성입니다.
public:
	CFTPMonitorDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CFTPMonitorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

	CHttpRequest	m_httpRequest;

	bool	m_bMonitoring;
	int		m_nUpdateInterval;

	void	GetLineList(CString& str, CStringArray& line_list);
	void	DeleteAllItems();

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnStartMonitor();
	afx_msg void OnStopMonitor();
	afx_msg void OnUpdateStartMonitor(CCmdUI *pCmdUI);
	afx_msg void OnUpdateStopMonitor(CCmdUI *pCmdUI);

	CListCtrl m_lcPMO;
};

#ifndef _DEBUG  // FTPMonitorView.cpp의 디버그 버전
inline CFTPMonitorDoc* CFTPMonitorView::GetDocument() const
   { return reinterpret_cast<CFTPMonitorDoc*>(m_pDocument); }
#endif

