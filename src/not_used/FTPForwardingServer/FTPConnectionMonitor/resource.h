//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FTPMonitor.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_FTPMONITOR_FORM             101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_FTPConnectionMoTYPE         129
#define IDC_LIST_PMO                    1000
#define IDC_EDIT1                       1001
#define IDC_EDIT_MANUAL                 1001
#define ID_START_MONITOR                32772
#define ID_STOP_MONITOR                 32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
