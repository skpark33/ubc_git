// FTPMonitorDoc.cpp : CFTPMonitorDoc 클래스의 구현
//

#include "stdafx.h"
#include "FTPMonitor.h"

#include "FTPMonitorDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFTPMonitorDoc

IMPLEMENT_DYNCREATE(CFTPMonitorDoc, CDocument)

BEGIN_MESSAGE_MAP(CFTPMonitorDoc, CDocument)
END_MESSAGE_MAP()


// CFTPMonitorDoc 생성/소멸

CFTPMonitorDoc::CFTPMonitorDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CFTPMonitorDoc::~CFTPMonitorDoc()
{
}

BOOL CFTPMonitorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CFTPMonitorDoc serialization

void CFTPMonitorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CFTPMonitorDoc 진단

#ifdef _DEBUG
void CFTPMonitorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFTPMonitorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CFTPMonitorDoc 명령
