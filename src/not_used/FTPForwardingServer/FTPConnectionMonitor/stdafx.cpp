// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// FTPMonitor.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


CMapStringToString	g_mapArguments;

void ParsingArguments()
{
	for(int i=0; i<__argc; i++)
	{
		char* param = __argv[i];

		if(param[0] == '+')
		{
			char* value = "";
			if(i+1 < __argc)
			{
				char* value = __argv[i+1];

				if(value[0] == '+')
				{
					value = "";
				}

				g_mapArguments.SetAt(param, value);
			}
		}
	}
	__argc =1;
}

BOOL GetArguments(LPCTSTR param)
{
	CString value;
	return g_mapArguments.Lookup(param, value);
}

BOOL GetArguments(LPCTSTR param, CString& value)
{
	return g_mapArguments.Lookup(param, value);
}
