#pragma once


#define ID_ICON_NOTIFY_CLICK            9998
#define ID_ICON_NOTIFY                  9999


#include "TrayIcon.h"
// CFrameWndEx 프레임입니다.


class CFrameWndEx : public CFrameWnd
{
	DECLARE_DYNCREATE(CFrameWndEx)
protected:
	CFrameWndEx();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CFrameWndEx();

protected:
	DECLARE_MESSAGE_MAP()

	int		m_nFirstX;
	int		m_nFirstY;
	int		m_nFirstWidth;
	int		m_nFirstHeight;

	bool	m_bMagnetEffect;
	int		m_nMagnetGap;

	// tray icon
	CTrayIcon	m_TrayIcon;	// 트레이
	HICON		m_hTrayIcon;		// 트레이 아이콘 (ICON)
	CBitmap		m_BMP24bit;		// 트레이 아이콘 (BMP)
	CImageList	m_imglstToolbarEnable;
	CImageList	m_imglstToolbarDisable;

	BOOL	CreateTrayIcon(CWnd* pWnd, LPCTSTR lpszTitle, UINT nMenuID, UINT nBmpIcon);

public:
	virtual BOOL DestroyWindow();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
	LRESULT OnTrayNotification(WPARAM wParam, LPARAM lParam);

public:

	bool	GetUseMagnetEffect();
	void	SetUseMagnetEffect(bool bEffect=true, int nGap=10);
	void	SetAlwaysOnTop(bool bTop);
	void	CreateHighColorToolbar(CToolBar* pToolbar, int cx, int cy, int nGrow, UINT nEnableID, UINT nDisableID, COLORREF bgRGB = RGB(192,192,192));

};

/*
_WIN32_WINNT, WINVER

Windows Longhorn		0x0600
Windows Server 2003		0x0502
Windows XP				0x0501
Windows 2000			0x0500
Windows 98				0x0410
Windows 95				0x0400

_WIN32_IE
IE 7.0					0x0700
IE 6.0 SP2				0x0603
IE 6.0 SP1				0x0601
IE 6.0					0x0600
IE 5.5					0x0550
IE 5.01					0x0501
IE 5.0,5.0a,5.0b		0x0500
IE 4.01					0x0401
IE 4.0					0x0400
IE 3.0,3.01,3.02		0x0300
*/
