#pragma once

/*
class AFX_CONTROL_LIB_DLL ControlLib
{
public:
	ControlLib();
	~ControlLib();

	static CString	ToString(int value);
	static CString	ToString(long value);
	static CString	ToString(unsigned int value);
	static CString	ToString(unsigned long value);
	static CString	ToString(double value);
	static CString	ToString(__int64 value);
	static CString	ToString(unsigned __int64 value);
	static CString	ToMoneyTypeString(LPCTSTR lpszStr);
	static CString	ToMoneyTypeString(int value);
	static CString	ToMoneyTypeString(__int64 value);

	static int		ToInt(LPCTSTR lpszStr);
	static LONGLONG	ToInt64(LPCTSTR lpszStr);
	static double	ToDouble(LPCTSTR lpszStr);

	static LPCTSTR	GetAppVersion();
	static void		GetAppVersion(CString &strVersion);
	static LPCTSTR	GetMainPath();
	static void		GetMainPath(CString& strDirectory);
	static LPCTSTR	GetINIPath();
	static void		GetINIPath(CString& strDirectory);
};
*/

CString		ToString(int value);
CString		ToString(unsigned int value);
CString		ToString(LONGLONG value);
CString		ToString(ULONGLONG value);
CString		ToString(double value, unsigned int precision=2);
CString		ToMoneyTypeString(LPCTSTR lpszStr);
CString		ToMoneyTypeString(int value);
CString		ToMoneyTypeString(unsigned int value);
CString		ToMoneyTypeString(LONGLONG value);
CString		ToMoneyTypeString(ULONGLONG value);
CString		ToMoneyTypeString(double value, unsigned int precision=2);

int			ToInt(LPCTSTR lpszStr);
LONGLONG	ToInt64(LPCTSTR lpszStr);
double		ToDouble(LPCTSTR lpszStr);

LPCTSTR		GetAppName();
void		GetAppName(CString &strAppName);
//LPCTSTR		GetAppVersion();
//void		GetAppVersion(CString &strVersion);
LPCTSTR		GetMainPath();
void		GetMainPath(CString& strDirectory);
LPCTSTR		GetINIPath();
void		GetINIPath(CString& strDirectory);

BOOL		WritePrivateProfileInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName);

LPCTSTR		GetWindowsVersion();

