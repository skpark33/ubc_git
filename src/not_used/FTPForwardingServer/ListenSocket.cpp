// ListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ListenSocket.h"

//#include "BypassSocket.h"

// CListenSocket

#define		BUFF_SIZE		65536


extern CString			g_strLocalIPAddr;
extern BYTE				g_byLocalIPAddr[4];

extern CMutex			g_mutexLog;
extern CStringArray		g_listLog;


CListenSocket::CListenSocket(SOCKET_TYPE socketType, UINT nPort)
:	m_SocketType (socketType)
,	m_nPort (nPort)
{
	m_tmCreated = CTime::GetCurrentTime();
}

CListenSocket::~CListenSocket()
{
}

// CListenSocket 멤버 함수

void CListenSocket::OnAccept(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	// server쪽, client쪽 소켓을 생성 & 링크
	CBypassSocket* pClientSocket = new CBypassSocket(m_SocketType, m_nPort);
	CBypassSocket* pServerSocket = new CBypassSocket(m_SocketType, m_nPort);

	pServerSocket->m_pLinkSocket = pClientSocket;
	pClientSocket->m_pLinkSocket = pServerSocket;

	if( Accept(*pClientSocket) )
	{
		pClientSocket->GetPeerName(pClientSocket->m_strDestIpaddr, pClientSocket->m_nDestPort);

		SEND_LOG(("Accept (%s) - %s:%d <-- %s:%d\r\n", 
				SocketTypeString[m_SocketType], 
				g_strLocalIPAddr, m_nPort, 
				pClientSocket->m_strDestIpaddr, pClientSocket->m_nDestPort));

		switch(m_SocketType)
		{
		case SOCKETTYPE_COMMAND:
			// command 소켓 연결 (command소켓은 오직 1개만 사용하므로 그대로둠)
			::AfxGetMainWnd()->PostMessage(WM_CONNECT_COMMAND_SOCKET, (WPARAM)pServerSocket, (WPARAM)pClientSocket);
			break;

		case SOCKETTYPE_DATA:
			// data소켓 연결
			::AfxGetMainWnd()->PostMessage(WM_CONNECT_DATA_SOCKET, (WPARAM)pServerSocket, (WPARAM)pClientSocket);
			// data소켓 연결된후 listen소켓 닫기 (한번쓰면 필요없어짐)
			::AfxGetMainWnd()->PostMessage(WM_CLOSE_LISTEN_SOCKET, (WPARAM)this);
			break;
		}
	}

	CAsyncSocket::OnAccept(nErrorCode);
}

void CListenSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CAsyncSocket::OnClose(nErrorCode);

	SEND_LOG(("Close Listen (%s) - %d\r\n", SocketTypeString[m_SocketType], m_nPort));
}

BOOL CListenSocket::StartListen()
{
	if(Create(m_nPort))
	{
		BOOL ret_val = Listen();
		if(ret_val)
		{
			SEND_LOG(("Start Listen (%s) - %d\r\n", SocketTypeString[m_SocketType], m_nPort));
		}
		return ret_val;
	}

	return FALSE;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
CBypassSocket::CBypassSocket(SOCKET_TYPE socketType, UINT nPort)
:	m_SocketType(socketType),m_nPort(nPort)
{
	m_tmUpdated = CTime::GetCurrentTime();
}

CBypassSocket::~CBypassSocket()
{
}

void CBypassSocket::OnConnect(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CAsyncSocket::OnConnect(nErrorCode);

	GetPeerName(m_strDestIpaddr, m_nDestPort);

	SEND_LOG(("Link (%s) - %s:%d <--> %s:%d\r\n", 
			SocketTypeString[m_SocketType], 
			m_strDestIpaddr, m_nDestPort, 
			m_pLinkSocket->m_strDestIpaddr, m_pLinkSocket->m_nDestPort ));
}

void CBypassSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CAsyncSocket::OnClose(nErrorCode);

	SEND_LOG(("Close (%s) - %s:%d\r\n", SocketTypeString[m_SocketType], m_strDestIpaddr, m_nDestPort));

	switch(m_SocketType)
	{
	case SOCKETTYPE_COMMAND:
		::AfxGetMainWnd()->PostMessage(WM_CLOSE_COMMAND_SOCKET, (WPARAM)this, (WPARAM)m_pLinkSocket);
		break;

	case SOCKETTYPE_DATA:
		::AfxGetMainWnd()->PostMessage(WM_CLOSE_DATA_SOCKET, (WPARAM)this, (WPARAM)m_pLinkSocket);
		break;
	}
}

void CBypassSocket::OnReceive(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	m_tmUpdated = CTime::GetCurrentTime();

	char buff[BUFF_SIZE] = {0};
	int nRead = Receive(buff, BUFF_SIZE);

	if(nRead > 0)
	{
		// ex : "227 Entering Passive Mode (192,168,147,131,9,217)"
		if(m_SocketType == SOCKETTYPE_COMMAND && memcmp(buff, "227 Entering Passive Mode (", 27) == 0)
		{
			int ip1, ip2, ip3, ip4, upper, lower;
			sscanf(buff+27, "%d,%d,%d,%d,%d,%d", &ip1, &ip2, &ip3, &ip4, &upper, &lower);

			int port = (int)upper*256 + lower;
			::AfxGetMainWnd()->SendMessage(WM_OPEN_DATA_SOCKET, port);

			sprintf(buff, "227 Entering Passive Mode (%d,%d,%d,%d,%d,%d)%c%c", 
					g_byLocalIPAddr[0], g_byLocalIPAddr[1], g_byLocalIPAddr[2], g_byLocalIPAddr[3], 
					upper, lower, 0x0d, 0x0a);
			nRead = strlen(buff);
		}

		if(m_pLinkSocket)
		{
			int total_send = 0;
			int remain = nRead;
			while(remain > 0)
			{
				int send = m_pLinkSocket->Send(buff+total_send, remain);
				if(send == SOCKET_ERROR)
				{
					DWORD err_code = GetLastError();
					if(err_code == WSAEWOULDBLOCK) continue;

					SEND_LOG(("Error : m_pLinkSocket->Send (ErrCode:%d)\r\n", err_code));
					break;
				}
				m_pLinkSocket->m_tmUpdated = CTime::GetCurrentTime();
				total_send += send;
				remain -= send;
				::Sleep(0);
			}
		}
	}

	CAsyncSocket::OnReceive(nErrorCode);
}
