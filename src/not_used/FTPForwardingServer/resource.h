//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FTPForwardingServer.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FTPFORWARDINGSERVER_DIALOG  102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDI_ROTATE_1                    129
#define IDI_ROTATE_2                    130
#define IDI_ROTATE_3                    131
#define IDI_ROTATE_4                    132
#define IDI_ROTATE_5                    133
#define IDI_ROTATE_6                    134
#define IDI_ROTATE_7                    135
#define IDI_ROTATE_8                    136
#define IDC_EDIT_LOG                    1000
#define IDC_SERVER_IP                   1001
#define IDC_SERVER_PORT                 1002
#define IDC_LOCAL_IP                    1003
#define IDC_BUTTON_APPLY                1004
#define IDC_AUTO_LOCAL_IP               1005
#define IDC_LOCAL_PORT                  1006
#define IDC_SAME_AS_SERVER_PORT         1007
#define IDC_BUTTON_CANCEL               1008
#define IDC_EDIT_SERVER_IP              1009
#define IDC_EDIT_LOCAL_IP               1010
#define IDC_STATIC_ROTATE               1011
#define IDC_BUTTON_START                1012
#define IDC_BUTTON_STOP                 1013
#define IDC_BUTTON_CHANGE               1014
#define IDC_BUTTON_MODIFY               1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
