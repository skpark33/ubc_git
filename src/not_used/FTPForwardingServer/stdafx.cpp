// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// FTPForwardingServer.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


CString	DWORDToAddr(DWORD dwAddress)
{
	LPBYTE pAddr = (LPBYTE)&dwAddress;

	char buf[128];
	sprintf(buf, "%d.%d.%d.%d", *(pAddr+3), *(pAddr+2), *(pAddr+1), *(pAddr+0));

	return buf;
}

DWORD AddrToDWORD(LPCSTR lpszAddress)
{
	CString strAddr = lpszAddress;

	DWORD addr[4];
	::ZeroMemory(addr, sizeof(addr));

	int count = 0;
	int pos = 0;
	CString token = strAddr.Tokenize(".,", pos);
	while(token != "" && count < 4)
	{
		addr[count] = atoi(token);

		token = strAddr.Tokenize(".,", pos);
		count++;
	}

	return (addr[0] << 24) | (addr[1] << 16) | (addr[2] << 8) | (addr[3]);
}

void AddrToBYTE(LPCSTR lpszAddress, LPBYTE pAddr)
{
	CString strAddr = lpszAddress;

	int count = 0;
	int pos = 0;
	CString token = strAddr.Tokenize(".,", pos);
	while(token != "" && count < 4)
	{
		*pAddr = atoi(token);

		token = strAddr.Tokenize(".,", pos);
		count++;
		pAddr++;
	}
}

LPCTSTR GetMainPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}

void GetMainPath(CString& strDirectory)
{
	strDirectory = GetMainPath();
}

LPCTSTR GetINIPath()
{
	static CString	_iniDirectory = _T("");

	if(_iniDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_iniDirectory = str;
		_iniDirectory.Append("data\\");
		_iniDirectory.Append("UBCConnect");
		_iniDirectory.Append(_T(".ini"));
	}

	return _iniDirectory;
}

void GetINIPath(CString& strDirectory)
{
	strDirectory = GetINIPath();
}

BOOL WritePrivateProfileInt(LPCTSTR lpAppName, LPCTSTR lpKeyName, int nValue, LPCTSTR lpFileName)
{
	CString str_val;
	str_val.Format("%d", nValue);
	return WritePrivateProfileString(lpAppName, lpKeyName, str_val, lpFileName);
}
