#pragma once

// CListenSocket 명령 대상입니다.


//#include "BypassSocket.h"


enum SOCKET_TYPE {
	SOCKETTYPE_COMMAND = 0,
	SOCKETTYPE_DATA,
	SOCKETTYPE_COUNT,
};

static const char* SocketTypeString[SOCKETTYPE_COUNT] = {"Command", "Data"};


///////////////////////////////////////////////////////////////////
class CListenSocket : public CAsyncSocket
{
public:
	CListenSocket(SOCKET_TYPE socketType, UINT nPort);
	virtual ~CListenSocket();
	virtual void OnAccept(int nErrorCode);

	SOCKET_TYPE	m_SocketType;	// 소켓 종류 : command or data
	CTime		m_tmCreated;	// 생성 시간
	UINT		m_nPort;		// 오픈 포트

	BOOL	StartListen();
	virtual void OnClose(int nErrorCode);
};

typedef	CArray<CListenSocket*, CListenSocket*>	CListenSocketList;


///////////////////////////////////////////////////////////////////
class CBypassSocket : public CAsyncSocket
{
public:
	CBypassSocket(SOCKET_TYPE socketType, UINT nPort);
	virtual ~CBypassSocket();

	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);

	SOCKET_TYPE	m_SocketType;		// 소켓 종류 : command or data
	CTime		m_tmUpdated;		// 갱신 시간
	CString		m_strDestIpaddr;	// 상대 ip_addr
	UINT		m_nDestPort;		// 상대 port
	UINT		m_nPort;			// 오픈 포트

	CBypassSocket*	m_pLinkSocket;	// 전달할 소켓
};
typedef	CArray<CBypassSocket*, CBypassSocket*>	CBypassSocketList;
