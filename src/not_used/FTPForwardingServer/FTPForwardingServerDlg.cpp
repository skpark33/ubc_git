// FTPForwardingServerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "FTPForwardingServer.h"
#include "FTPForwardingServerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


extern CString	g_strServerIPAddr;
extern BYTE		g_byServerIPAddr[4];
extern UINT		g_nServerPort;

extern CString	g_strLocalIPAddr;
extern BYTE		g_byLocalIPAddr[4];
extern CString	g_strAutoLocalIPAddr;
extern BYTE		g_byAutoLocalIPAddr[4];
extern UINT		g_nLocalPort;
extern BOOL		g_bAutoLocalIPAddr;
extern BOOL		g_bSameAsServerPort;


#define		CHECK_SOCKET_TIMEOUT_TIMER_ID		1025
#define		CHECK_SOCKET_TIMEOUT_TIMER_TIME		1000

#define		CHECK_ICON_TIMER_ID					1026
#define		CHECK_ICON_TIMER_TIME				250

#define		MAX_LOG_LINE_COUNT					1000
#define		MIN_LOG_LINE_COUNT					900


CMutex			g_mutexLog;
CStringArray	g_listLog;

int				m_nIconIndex = 0;
HICON			m_iconRotate[8];

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CFTPForwardingServerDlg 대화 상자


CFTPForwardingServerDlg::CFTPForwardingServerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFTPForwardingServerDlg::IDD, pParent)
	, m_MainListenSocket ( NULL )
	, m_ReposControl (this)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFTPForwardingServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_SERVER_IP, m_editServerIP);
	DDX_Control(pDX, IDC_SERVER_IP, m_ipServer);
	DDX_Control(pDX, IDC_SERVER_PORT, m_editServerPort);
	DDX_Control(pDX, IDC_EDIT_LOCAL_IP, m_editLocalIP);
	DDX_Control(pDX, IDC_LOCAL_IP, m_ipLocal);
	DDX_Control(pDX, IDC_LOCAL_PORT, m_editLocalPort);
	DDX_Control(pDX, IDC_AUTO_LOCAL_IP, m_chkAutoLocalIP);
	DDX_Control(pDX, IDC_SAME_AS_SERVER_PORT, m_chkSameAsServerPort);
	DDX_Control(pDX, IDC_BUTTON_START, m_btnStart);
	DDX_Control(pDX, IDC_BUTTON_STOP, m_btnStop);
	DDX_Control(pDX, IDC_BUTTON_MODIFY, m_btnModify);
	DDX_Control(pDX, IDC_BUTTON_APPLY, m_btnApply);
	DDX_Control(pDX, IDC_BUTTON_CANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_EDIT_LOG, m_editLog);
}

BEGIN_MESSAGE_MAP(CFTPForwardingServerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_WM_SIZING()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_AUTO_LOCAL_IP, &CFTPForwardingServerDlg::OnBnClickedAutoLocalIp)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CFTPForwardingServerDlg::OnBnClickedButtonApply)
	ON_BN_CLICKED(IDC_SAME_AS_SERVER_PORT, &CFTPForwardingServerDlg::OnBnClickedSameAsServerPort)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CFTPForwardingServerDlg::OnBnClickedButtonCancel)
	ON_BN_CLICKED(IDC_BUTTON_START, &CFTPForwardingServerDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CFTPForwardingServerDlg::OnBnClickedButtonStop)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY, &CFTPForwardingServerDlg::OnBnClickedButtonModify)
	ON_MESSAGE(WM_SEND_LOG, OnSendLog)
	ON_MESSAGE(WM_CONNECT_COMMAND_SOCKET, OnConnectCommandSocket)
	ON_MESSAGE(WM_CONNECT_DATA_SOCKET, OnConnectDataSocket)
	ON_MESSAGE(WM_OPEN_DATA_SOCKET, OnOpenDataSocket)
	ON_MESSAGE(WM_CLOSE_COMMAND_SOCKET, OnCloseCommandSocket)
	ON_MESSAGE(WM_CLOSE_DATA_SOCKET, OnCloseDataSocket)
	ON_MESSAGE(WM_CLOSE_LISTEN_SOCKET, OnCloseListenSocket)
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CFTPForwardingServerDlg 메시지 처리기

BOOL CFTPForwardingServerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	InitValueSettings();

	m_ReposControl.AddControl((CWnd*)&m_editLog, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	for(int i=0; i<8; i++)
		m_iconRotate[i] = (HICON)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ROTATE_1+i), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);//AfxGetApp()->LoadIcon(IDI_ROTATE_1);

	SetTimer(CHECK_SOCKET_TIMEOUT_TIMER_ID, CHECK_SOCKET_TIMEOUT_TIMER_TIME, NULL);

	OnBnClickedButtonCancel();
	OnBnClickedButtonStart();


	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CFTPForwardingServerDlg::InitValueSettings()
{
	char buf[1024];

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("FTPFOWARDINGSERVER", "SERVERIP", "211.232.57.202", buf, 1023, GetINIPath());
	g_strServerIPAddr = buf;
	AddrToBYTE(g_strServerIPAddr, g_byServerIPAddr);

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("FTPFOWARDINGSERVER", "SERVERPORT", "21", buf, 1023, GetINIPath());
	g_nServerPort = atoi(buf);

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("FTPFOWARDINGSERVER", "LOCALIP", "192.168.0.2", buf, 1023, GetINIPath());
	g_strLocalIPAddr = buf;
	AddrToBYTE(g_strLocalIPAddr, g_byLocalIPAddr);

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("FTPFOWARDINGSERVER", "AUTOLOCALIP", "1", buf, 1023, GetINIPath());
	g_bAutoLocalIPAddr = atoi(buf);

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("FTPFOWARDINGSERVER", "LOCALPORT", "21", buf, 1023, GetINIPath());
	g_nLocalPort = atoi(buf);

	//
	::ZeroMemory(buf, sizeof(buf));
	::GetPrivateProfileString("FTPFOWARDINGSERVER", "SAMEASSERVERPORT", "1", buf, 1023, GetINIPath());
	g_bSameAsServerPort = atoi(buf);
}

void CFTPForwardingServerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CFTPForwardingServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HBRUSH CFTPForwardingServerDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	static CBrush brush(RGB(255,255,255));

	if(m_editLog.GetSafeHwnd() == pWnd->GetSafeHwnd())
	{
		// 새로운 배경색이 보일수 있도록 백그라운드 모드를 설정한다.
		pDC->SetBkMode(TRANSPARENT);
		// 빨간색 위에서도 보일수 있도록 텍스트의 색깔을 흰색으로 한다.
		pDC->SetTextColor(RGB(0,0,0));

		return (HBRUSH)brush;
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

void CFTPForwardingServerDlg::OnSizing(UINT fwSide, LPRECT pRect)
{
	CDialog::OnSizing(fwSide, pRect);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	m_ReposControl.MoveControl();
}

void CFTPForwardingServerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == CHECK_SOCKET_TIMEOUT_TIMER_ID)
	{
		// listen socket check
		{
			CGuard lock(m_mutexListenSocket);

			__time64_t current_time = CTime::GetCurrentTime().GetTime();
			int count = m_listListenSocket.GetCount();
			for(int i=0; i<count; i++)
			{
				CListenSocket* sock = m_listListenSocket.GetAt(i);

				__time64_t created_time = sock->m_tmCreated.GetTime();
				if(created_time + 60 < current_time)	// 1분 초과
				{
					sock->Close();
					delete sock;
					m_listListenSocket.RemoveAt(i);
					count--;
					i--;
				}
			}
		}

		// command socket check
		CBypassSocketList command_socket_list;
		{
			CGuard lock(m_mutexCommandSocket);

			__time64_t current_time = CTime::GetCurrentTime().GetTime();
			int count = m_listCommandSocket.GetCount();
			for(int i=0; i<count; i++)
			{
				CBypassSocket* sock = m_listCommandSocket.GetAt(i);

				__time64_t created_time = sock->m_tmUpdated.GetTime();
				if(created_time + 60*3 < current_time)	// 3분 초과
				{
					command_socket_list.Add(sock);
					command_socket_list.Add(sock->m_pLinkSocket);
				}
			}
		}
		for(int i=0; i<command_socket_list.GetCount(); i+=2)
		{
			CBypassSocket* sock1 = command_socket_list.GetAt(i);
			CBypassSocket* sock2 = command_socket_list.GetAt(i+1);
			OnCloseCommandSocket((WPARAM)sock1, (LPARAM)sock2);
		}

		// data socket check
		CBypassSocketList data_socket_list;
		{
			CGuard lock(m_mutexDataSocket);

			__time64_t current_time = CTime::GetCurrentTime().GetTime();
			int count = m_listDataSocket.GetCount();
			for(int i=0; i<count; i++)
			{
				CBypassSocket* sock = m_listDataSocket.GetAt(i);

				__time64_t created_time = sock->m_tmUpdated.GetTime();
				if(created_time + 60*3 < current_time)	// 3분 초과
				{
					data_socket_list.Add(sock);
					data_socket_list.Add(sock->m_pLinkSocket);
				}
			}
		}
		for(int i=0; i<data_socket_list.GetCount(); i+=2)
		{
			CBypassSocket* sock1 = data_socket_list.GetAt(i);
			CBypassSocket* sock2 = data_socket_list.GetAt(i+1);
			OnCloseDataSocket((WPARAM)sock1, (LPARAM)sock2);
		}
	}
	else if(nIDEvent == CHECK_ICON_TIMER_ID)
	{
		CStatic* rotate = (CStatic*)GetDlgItem(IDC_STATIC_ROTATE);
		rotate->SetIcon(m_iconRotate[m_nIconIndex % 8]);
		rotate->ModifyStyle(NULL, SS_REALSIZEIMAGE);
		m_nIconIndex++;
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.

HCURSOR CFTPForwardingServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CFTPForwardingServerDlg::OnBnClickedAutoLocalIp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int check = m_chkAutoLocalIP.GetCheck();
	m_ipLocal.EnableWindow(check==BST_CHECKED ? FALSE : TRUE);
}

void CFTPForwardingServerDlg::OnBnClickedSameAsServerPort()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int check = m_chkSameAsServerPort.GetCheck();
	if(check==BST_CHECKED)
	{
		CString str;
		m_editServerPort.GetWindowText(str);
		m_editLocalPort.SetWindowText(str);
	}
	m_editLocalPort.SetReadOnly(check==BST_CHECKED ? TRUE : FALSE);
}

void CFTPForwardingServerDlg::OnBnClickedButtonStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(m_MainListenSocket != NULL)
	{
		OnBnClickedButtonStop();
	}

	m_btnStart.EnableWindow(FALSE);
	m_btnStop.EnableWindow(TRUE);

	m_MainListenSocket = new CListenSocket(SOCKETTYPE_COMMAND, g_nLocalPort);
	m_MainListenSocket->StartListen();

	SetTimer(CHECK_ICON_TIMER_ID, CHECK_ICON_TIMER_TIME, NULL);
}

void CFTPForwardingServerDlg::OnBnClickedButtonStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_btnStart.EnableWindow(TRUE);
	m_btnStop.EnableWindow(FALSE);

	ResetAllConnection();

	KillTimer(CHECK_ICON_TIMER_ID);
}

void CFTPForwardingServerDlg::OnBnClickedButtonModify()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	// server ip
	m_editServerIP.ShowWindow(SW_HIDE);
	m_ipServer.ShowWindow(SW_SHOW);

	// server port
	m_editServerPort.SetReadOnly(FALSE);

	// local ip
	m_editLocalIP.ShowWindow(SW_HIDE);
	m_ipLocal.ShowWindow(SW_SHOW);
	m_ipLocal.EnableWindow(g_bAutoLocalIPAddr ? FALSE : TRUE);

	// auto local ip
	m_chkAutoLocalIP.EnableWindow(TRUE);
	m_chkAutoLocalIP.SetCheck(g_bAutoLocalIPAddr ? BST_CHECKED : BST_UNCHECKED);

	// local port
	m_editLocalPort.SetReadOnly(g_bSameAsServerPort ? TRUE : FALSE);

	// same port as server
	m_chkSameAsServerPort.EnableWindow(TRUE);
	m_chkSameAsServerPort.SetCheck(g_bSameAsServerPort ? BST_CHECKED : BST_UNCHECKED);

	// buttons...
	m_btnModify.ShowWindow(SW_HIDE);
	m_btnApply.ShowWindow(SW_SHOW);
	m_btnCancel.EnableWindow(TRUE);

	m_ipServer.SetFocus();
}

void CFTPForwardingServerDlg::OnBnClickedButtonApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(::AfxMessageBox("It will be reset all Connection.\r\n\r\nApply Now ?", MB_ICONWARNING | MB_YESNO) == IDYES)
	{
		CString port;

		// server ip
		m_ipServer.GetAddress(g_byServerIPAddr[0], g_byServerIPAddr[1], g_byServerIPAddr[2], g_byServerIPAddr[3]);
		g_strServerIPAddr.Format("%d.%d.%d.%d", g_byServerIPAddr[0], g_byServerIPAddr[1], g_byServerIPAddr[2], g_byServerIPAddr[3]);
		m_editServerIP.SetWindowText(g_strServerIPAddr);

		// server port
		m_editServerPort.GetWindowText(port);
		g_nServerPort = atoi(port);

		// local ip
		m_ipLocal.GetAddress(g_byLocalIPAddr[0], g_byLocalIPAddr[1], g_byLocalIPAddr[2], g_byLocalIPAddr[3]);
		g_strLocalIPAddr.Format("%d.%d.%d.%d", g_byLocalIPAddr[0], g_byLocalIPAddr[1], g_byLocalIPAddr[2], g_byLocalIPAddr[3]);
		m_editLocalIP.SetWindowText(g_strLocalIPAddr);

		// auto local ip
		g_bAutoLocalIPAddr = ( m_chkAutoLocalIP.GetCheck()==BST_CHECKED ? TRUE : FALSE );

		// local port
		if(m_chkSameAsServerPort.GetCheck()==BST_CHECKED)
			m_editLocalPort.SetWindowText(port);
		m_editLocalPort.GetWindowText(port);
		g_nLocalPort = atoi(port);

		// same port as server
		g_bSameAsServerPort = m_chkSameAsServerPort.GetCheck()==BST_CHECKED ? TRUE : FALSE;

		//
		::WritePrivateProfileString("FTPFOWARDINGSERVER", "SERVERIP", g_strServerIPAddr, GetINIPath());
		::WritePrivateProfileInt   ("FTPFOWARDINGSERVER", "SERVERPORT", g_nServerPort, GetINIPath());
		::WritePrivateProfileString("FTPFOWARDINGSERVER", "LOCALIP", g_strLocalIPAddr, GetINIPath());
		::WritePrivateProfileInt   ("FTPFOWARDINGSERVER", "AUTOLOCALIP", g_bAutoLocalIPAddr, GetINIPath());
		::WritePrivateProfileInt   ("FTPFOWARDINGSERVER", "LOCALPORT", g_nLocalPort, GetINIPath());
		::WritePrivateProfileInt   ("FTPFOWARDINGSERVER", "SAMEASSERVERPORT", g_bSameAsServerPort, GetINIPath());

		//
		OnBnClickedButtonCancel();
		OnBnClickedButtonStop();
		OnBnClickedButtonStart();
	}
}

void CFTPForwardingServerDlg::OnBnClickedButtonCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString port;

	// server ip
	m_ipServer.SetAddress(g_byServerIPAddr[0], g_byServerIPAddr[1], g_byServerIPAddr[2], g_byServerIPAddr[3]);
	g_strServerIPAddr.Format("%d.%d.%d.%d", g_byServerIPAddr[0], g_byServerIPAddr[1], g_byServerIPAddr[2], g_byServerIPAddr[3]);
	m_editServerIP.SetWindowText(g_strServerIPAddr);

	// server port
	port.Format("%d", g_nServerPort);
	m_editServerPort.SetWindowText(port);

	// local ip
	if(g_bAutoLocalIPAddr) memcpy(g_byLocalIPAddr, g_byAutoLocalIPAddr, sizeof(g_byLocalIPAddr));
	m_ipLocal.SetAddress(g_byLocalIPAddr[0], g_byLocalIPAddr[1], g_byLocalIPAddr[2], g_byLocalIPAddr[3]);
	g_strLocalIPAddr.Format("%d.%d.%d.%d", g_byLocalIPAddr[0], g_byLocalIPAddr[1], g_byLocalIPAddr[2], g_byLocalIPAddr[3]);
	m_editLocalIP.SetWindowText(g_strLocalIPAddr);

	// auto local ip
	m_chkAutoLocalIP.SetCheck(g_bAutoLocalIPAddr ? BST_CHECKED : BST_UNCHECKED);

	// local port
	if(g_bSameAsServerPort) g_nLocalPort = g_nServerPort;
	port.Format("%d", g_nLocalPort);
	m_editLocalPort.SetWindowText(port);

	// same port as server
	m_chkSameAsServerPort.SetCheck(g_bSameAsServerPort ? BST_CHECKED : BST_UNCHECKED);

	/////////////////////////////////

	// server ip
	m_editServerIP.ShowWindow(SW_SHOW);
	m_ipServer.ShowWindow(SW_HIDE);

	// server port
	m_editServerPort.SetReadOnly(TRUE);

	// local ip
	m_editLocalIP.ShowWindow(SW_SHOW);
	m_ipLocal.ShowWindow(SW_HIDE);

	// local port
	m_chkSameAsServerPort.EnableWindow(FALSE);
	m_editLocalPort.SetReadOnly(TRUE);

	// auto local ip
	m_chkAutoLocalIP.EnableWindow(FALSE);
	m_chkAutoLocalIP.SetCheck(g_bAutoLocalIPAddr ? BST_CHECKED : BST_UNCHECKED);

	// same port as server
	m_chkSameAsServerPort.SetCheck(g_bSameAsServerPort ? BST_CHECKED : BST_UNCHECKED);

	// buttons...
	m_btnModify.ShowWindow(SW_SHOW);
	m_btnApply.ShowWindow(SW_HIDE);
	m_btnCancel.EnableWindow(FALSE);

	m_editLog.SetFocus();
}

LRESULT CFTPForwardingServerDlg::OnSendLog(WPARAM wParam, LPARAM lParam)
{
	CGuard lock(g_mutexLog);

	m_editLog.SetRedraw(FALSE);

	CString text;
	m_editLog.GetWindowText(text);

	int pos_start, pos_end;
	m_editLog.GetSel(pos_start, pos_end);
	bool move_to_end = false;
	if(pos_start == pos_end && text.GetLength() == pos_start)
		move_to_end = true;

	int count = g_listLog.GetCount();
	for(int i=0; i<count; i++)
	{
		const CString& str = g_listLog.GetAt(i);
		m_editLog.GetWindowText(text);
		m_editLog.SetSel(text.GetLength(), text.GetLength());
		m_editLog.ReplaceSel(str);
	}

	int delete_line_count = 0;
	if(m_editLog.GetLineCount() > MAX_LOG_LINE_COUNT)
	{
		delete_line_count = m_editLog.GetLineCount() - MIN_LOG_LINE_COUNT;

		m_editLog.GetWindowText(text);

		int pos = 0;
		for(int i=0; i<delete_line_count; i++)
			pos = text.Find('\r', pos);

		m_editLog.SetSel(0, pos+2, TRUE);
		m_editLog.Clear();

		pos_start -= (pos+2);
		pos_end -= (pos+2);

		if(pos_start < 0) pos_start = 0;
		if(pos_end < 0) pos_end = 0;
	}

	if(move_to_end)
	{
		m_editLog.GetWindowText(text);
		m_editLog.SetSel(text.GetLength(), text.GetLength());
	}
	else
	{
		m_editLog.SetSel(pos_start, pos_end);
	}

	g_listLog.RemoveAll();

	m_editLog.SetRedraw(TRUE);

	return 0;
}

LRESULT CFTPForwardingServerDlg::OnConnectCommandSocket(WPARAM wParam, LPARAM lParam)
{
	CBypassSocket* pServerSocket = (CBypassSocket*)wParam;
	CBypassSocket* pClientSocket = (CBypassSocket*)lParam;

	if(pServerSocket->Create())
	{
//		if(pServerSocket->Connect(g_strServerIPAddr, g_nServerPort))
		SEND_LOG(("Connect (%s) - %s --> %s:%d\r\n", 
				SocketTypeString[pServerSocket->m_SocketType], 
				g_strLocalIPAddr, 
				g_strServerIPAddr, 
				g_nServerPort));

		pServerSocket->Connect(g_strServerIPAddr, g_nServerPort);
		{
			CGuard lock(m_mutexCommandSocket);

			m_listCommandSocket.Add(pServerSocket);
			m_listCommandSocket.Add(pClientSocket);

			return 1;
		}
	}

	pClientSocket->Close();
	delete pClientSocket;

	pServerSocket->Close();
	delete pServerSocket;

	return 0;
}

LRESULT CFTPForwardingServerDlg::OnConnectDataSocket(WPARAM wParam, LPARAM lParam)
{
	CBypassSocket* pServerSocket = (CBypassSocket*)wParam;
	CBypassSocket* pClientSocket = (CBypassSocket*)lParam;

	if(pServerSocket->Create())
	{
		SEND_LOG(("Connect (%s) - %s --> %s:%d\r\n", 
				SocketTypeString[pServerSocket->m_SocketType], 
				g_strLocalIPAddr, 
				g_strServerIPAddr, 
				pServerSocket->m_nPort));

		//if(pServerSocket->Connect(g_strServerIPAddr, pServerSocket->m_nPort))
		while(!pServerSocket->Connect(g_strServerIPAddr, pServerSocket->m_nPort))
		{
			if(GetLastError() == WSAEISCONN) break;
			Sleep(0);
		}

		{
			CGuard lock(m_mutexDataSocket);

			m_listDataSocket.Add(pServerSocket);
			m_listDataSocket.Add(pClientSocket);

			return 1;
		}
	}

	pClientSocket->Close();
	delete pClientSocket;

	pServerSocket->Close();
	delete pServerSocket;

	return 0;
}

LRESULT CFTPForwardingServerDlg::OnOpenDataSocket(WPARAM wParam, LPARAM lParam)
{
	CGuard lock(m_mutexListenSocket);

	CListenSocket* listen_socket = new CListenSocket(SOCKETTYPE_DATA, wParam);
	m_listListenSocket.Add(listen_socket);
	listen_socket->StartListen();

	return 0;
}

LRESULT CFTPForwardingServerDlg::OnCloseCommandSocket(WPARAM wParam, LPARAM lParam)
{
	CBypassSocket* pSocket = (CBypassSocket*)wParam;
	CBypassSocket* pLinkSocket = (CBypassSocket*)lParam;

	CGuard lock(m_mutexCommandSocket);

	int count = m_listCommandSocket.GetCount();
	for(int i=0; i<count; i++)
	{
		CBypassSocket* sock = m_listCommandSocket.GetAt(i);
		if(sock == pSocket || sock == pLinkSocket)
		{
			sock->Close();
			delete sock;
			m_listCommandSocket.RemoveAt(i);
			count--;
			i--;
		}
	}

	return 0;
}

LRESULT CFTPForwardingServerDlg::OnCloseDataSocket(WPARAM wParam, LPARAM lParam)
{
	CBypassSocket* pSocket = (CBypassSocket*)wParam;
	CBypassSocket* pLinkSocket = (CBypassSocket*)lParam;

	CGuard lock(m_mutexDataSocket);

	int count = m_listDataSocket.GetCount();
	for(int i=0; i<count; i++)
	{
		CBypassSocket* sock = m_listDataSocket.GetAt(i);
		if(sock == pSocket || sock == pLinkSocket)
		{
			sock->Close();
			delete sock;
			m_listDataSocket.RemoveAt(i);
			count--;
			i--;
		}
	}

	return 0;
}

LRESULT CFTPForwardingServerDlg::OnCloseListenSocket(WPARAM wParam, LPARAM lParam)
{
	CListenSocket* pSocket = (CListenSocket*)wParam;

	CGuard lock(m_mutexListenSocket);

	int count = m_listListenSocket.GetCount();
	for(int i=0; i<count; i++)
	{
		CListenSocket* sock = m_listListenSocket.GetAt(i);
		if(sock == pSocket)
		{
			SEND_LOG(("Close Listen (%s) - %d\r\n", SocketTypeString[sock->m_SocketType], sock->m_nPort));

			sock->Close();
			delete sock;
			m_listListenSocket.RemoveAt(i);
			return 1;
		}
	}

	return 0;
}

BOOL CFTPForwardingServerDlg::ResetAllConnection()
{
	//
	if(m_MainListenSocket != NULL)
	{
		SEND_LOG(("Close Listen (%s) - %d\r\n", SocketTypeString[m_MainListenSocket->m_SocketType], m_MainListenSocket->m_nPort));

		m_MainListenSocket->Close();
		delete m_MainListenSocket;
		m_MainListenSocket = NULL;
	}

	// command-socket remove all
	{
		CGuard lock(m_mutexCommandSocket);

		int count = m_listCommandSocket.GetCount();
		for(int i=0; i<count; i++)
		{
			CBypassSocket* sock = m_listCommandSocket.GetAt(i);
			sock->Close();
			delete sock;
		}
		m_listCommandSocket.RemoveAll();
	}

	// data-socket remove all
	{
		CGuard lock(m_mutexDataSocket);

		int count = m_listDataSocket.GetCount();
		for(int i=0; i<count; i++)
		{
			CBypassSocket* sock = m_listDataSocket.GetAt(i);
			sock->Close();
			delete sock;
		}
		m_listDataSocket.RemoveAll();
	}

	// listen-socket remove all
	{
		CGuard lock(m_mutexListenSocket);

		int count = m_listListenSocket.GetCount();
		for(int i=0; i<count; i++)
		{
			CListenSocket* sock = m_listListenSocket.GetAt(i);
			sock->Close();
			delete sock;
		}
		m_listListenSocket.RemoveAll();
	}

	return TRUE;
}

void CFTPForwardingServerDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnGetMinMaxInfo(lpMMI);

	lpMMI->ptMinTrackSize.x = 497 + ::GetSystemMetrics(SM_CXSIZEFRAME)*2 /*+ ::GetSystemMetrics(SM_CXDLGFRAME)*2*/ + ::GetSystemMetrics(SM_CXBORDER)*2;
	lpMMI->ptMinTrackSize.y = 250 + ::GetSystemMetrics(SM_CYSIZEFRAME)*2 /*+ ::GetSystemMetrics(SM_CYDLGFRAME)*2*/ + ::GetSystemMetrics(SM_CYBORDER)*2 + ::GetSystemMetrics(SM_CYCAPTION);
}
