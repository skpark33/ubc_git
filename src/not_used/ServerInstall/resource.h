//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ServerInstall.rc
//
#define IDOK2                           3
#define IDOK3                           6
#define IDOK4                           7
#define IDOK5                           9
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SERVERINSTALL_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     130
#define IDB_BITMAP2                     133
#define IDB_BITMAP3                     134
#define IDC_BUTTON1                     1000
#define IDC_BUTTON_3                    1000
#define IDC_EDIT_KEY                    1001
#define IDC_EDIT_PASSWD                 1002
#define IDC_LABEL_KEY                   1003
#define IDC_LABEL_INTRO                 1005
#define IDC_CHECK_INSTALL_DB            1007
#define IDC_CHECK_CREATE_USER           1008
#define IDC_CHECK_CREATE_DB             1009
#define IDC_CHECK_CREATE_TABLE          1010
#define IDC_CHECK_INIT_DATA             1011
#define IDC_CHECK_FILEZILLA             1012
#define IDC_CHECK_VNC                   1013
#define IDC_CHECK_FILEZILLA2            1014
#define IDC_LABEL_PASSWD                1015
#define IDC_CHECK_INIT_HOST             1016
#define IDC_STATE_INSTALL_DB            1026
#define IDC_STATE_FILEZILLA             1031
#define IDC_STATE_VNC                   1032
#define IDC_STATE_FILEZILLA2            1033
#define IDC_STATIC_LOGO                 1041
#define IDC_BUTTON_4                    1042
#define IDC_GROUP_1                     1043
#define IDC_STATE_KEY                   1044
#define IDC_STATE_IP                    1045
#define IDC_CHECK_VCREDIST              1046
#define IDC_EDIT_IP                     1047
#define IDC_CHECK_VC2005                1048
#define IDC_STATE_VCREDIST              1049
#define IDC_STATE_VC2005                1050
#define IDC_BUTTON_1                    1051
#define IDC_GROUP_2                     1052
#define IDC_GROUP_3                     1053
#define IDC_GROUP_4                     1054
#define IDC_GROUP_5                     1055
#define IDC_GROUP_6                     1056
#define IDC_GROUP_7                     1057
#define IDC_GROUP_8                     1058
#define IDC_STATIC_VERSION              1059
#define IDC_BUTTON_5                    1060
#define IDC_STATE_CREATE_USER           1061
#define IDC_STATE_CREATE_DB             1062
#define IDC_STATE_CREATE_TABLE          1063
#define IDC_STATE_INIT_DATA             1064
#define IDC_STATE_INIT_HOST             1065
#define IDC_BUTTON_6                    1066
#define IDC_CHECK_SOAP                  1067
#define IDC_STATE_SOAP                  1068
#define IDC_STATIC_LOGO2                1069
#define IDC_CHECK_VAR1                  1074
#define IDC_CHECK_VAR2                  1075
#define IDC_STATE_VAR1                  1076
#define IDC_STATE_VAR2                  1077
#define IDC_BUTTON_2                    1079
#define IDC_CHECK_IIS                   1081
#define IDC_CHECK_START                 1082
#define IDC_STATIC27                    1083
#define IDC_STATE_IIS                   1083
#define IDC_STATE_START                 1084
#define IDC_BUTTON_7                    1085
#define IDC_BUTTON_8                    1086
#define IDC_CHECK_RSS                   1087
#define IDC_STATE_RSS                   1088
#define IDC_STATE_START2                1089

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1060
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
