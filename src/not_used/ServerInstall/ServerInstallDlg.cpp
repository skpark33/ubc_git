// ServerInstallDlg.cpp : 구현 파일
//
#include <ci/libBase/ciAceType.h>
#include "stdafx.h"
#include "ServerInstall.h"
#include "ServerInstallDlg.h"

#include "CMN/libScratch/scratchUtil.h"
#include "CMN/libInstall/installUtil.h"
#include "CMN/libDBUtil/dbUtil.h"
#include <CMN/libCommon/ubcIni.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libConfig/ciXProperties.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libBase/ciUtil.h>
#include <ci/libMySQL/ciMySQL.h>
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciEntity.h>
#include <cci/libPfUtil/cciDSUtil.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(10,"ServerInstallDlg");

#define DOWNLOAD_ROOT	_COP_CD("C:\\Project\\ubc\\3rdparty\\")
#define SQL_ROOT		_COP_CD("C:\\Project\\ubc\\config\\sql\\")
#define CONFIG_ROOT		_COP_CD("C:\\Project\\ubc\\config\\")
#define MYSQL_COMMAND	_COP_PROGRAM_FILES("C:\\Program Files\\MySQL\\MySQL Server 5.0\\bin\\mysql")
#define FILEZILLA_ROOT	_COP_PROGRAM_FILES("C:\\Program Files\\FileZilla Server\\")


// CServerInstallDlg 대화 상자


CServerInstallDlg::CServerInstallDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServerInstallDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	should_be_added_path.clear();
	_orbThread = 0;

	should_be_exist_path.push_back(".");
	should_be_exist_path.push_back(_COP_PROGRAM_FILES("C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE"));
	should_be_exist_path.push_back(_COP_PROGRAM_FILES("C:\\Program Files\\Microsoft Visual Studio 8\\VC\\bin"));
	should_be_exist_path.push_back(_COP_CD("C:\\Project\\ubc\\bin8"));
	should_be_exist_path.push_back(_COP_CD("C:\\Project\\ubc\\util\\win32"));
	should_be_exist_path.push_back(_COP_CD("C:\\Project\\ubc\\cop\\bin8"));
	should_be_exist_path.push_back(_COP_CD("C:\\Project\\ubc\\cop\\util\\win32"));
	should_be_exist_path.push_back(_COP_CD("C:\\Project\\3rdparty\\win32\\ACE_wrappers\\bin"));
	should_be_exist_path.push_back(_COP_CD("C:\\Project\\3rdparty\\win32\\ACE_wrappers\\lib"));
	should_be_exist_path.push_back(_COP_CD("C:\\Project\\3rdparty\\win32\\bin"));
	should_be_exist_path.push_back(_COP_CD("C:\\Project\\3rdparty\\win32\\UnxUtils\\bin"));
	should_be_exist_path.push_back("C:\\WINDOWS\\system32");
	should_be_exist_path.push_back("C:\\WINDOWS");
	should_be_exist_path.push_back("C:\\WINDOWS\\System32\\Wbem");
	should_be_exist_path.push_back(_COP_PROGRAM_FILES("C:\\Program Files\\MySQL\\MySQL Server 5.0\\bin"));


}

void CServerInstallDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_VCREDIST, _CHECK_VCREDIST);
	DDX_Control(pDX, IDC_CHECK_VC2005, _CHECK_VC2005);
	DDX_Control(pDX, IDC_CHECK_SOAP, _CHECK_SOAP);
	DDX_Control(pDX, IDC_STATE_VCREDIST, _STATE_VCREDIST);
	DDX_Control(pDX, IDC_STATE_VC2005, _STATE_VC2005);
	DDX_Control(pDX, IDC_STATE_SOAP, _STATE_SOAP);
	DDX_Control(pDX, IDC_CHECK_VAR1, _CHECK_VAR1);
	DDX_Control(pDX, IDC_CHECK_VAR2, _CHECK_VAR2);
	DDX_Control(pDX, IDC_STATE_VAR1, _STATE_VAR1);
	DDX_Control(pDX, IDC_STATE_VAR2, _STATE_VAR2);
	DDX_Control(pDX, IDC_EDIT_KEY, _EDIT_KEY);
	DDX_Control(pDX, IDC_EDIT_PASSWD, _EDIT_PASSWD);
	DDX_Control(pDX, IDC_STATE_KEY, _STATE_KEY);
	DDX_Control(pDX, IDC_EDIT_IP, _EDIT_IP);
	DDX_Control(pDX, IDC_STATE_IP, _STATE_IP);
	DDX_Control(pDX, IDC_CHECK_INSTALL_DB, _CHECK_INSTALL_DB);
	DDX_Control(pDX, IDC_CHECK_CREATE_USER, _CHECK_CREATE_USER);
	DDX_Control(pDX, IDC_CHECK_CREATE_DB, _CHECK_CREATE_DB);
	DDX_Control(pDX, IDC_CHECK_CREATE_TABLE, _CHECK_CREATE_TABLE);
	DDX_Control(pDX, IDC_CHECK_INIT_DATA, _CHECK_INIT_DATA);
	DDX_Control(pDX, IDC_STATE_INSTALL_DB, _STATE_INSTALL_DB);
	DDX_Control(pDX, IDC_STATE_CREATE_USER, _STATE_CREATE_USER);
	DDX_Control(pDX, IDC_STATE_CREATE_DB, _STATE_CREATE_DB);
	DDX_Control(pDX, IDC_STATE_CREATE_TABLE, _STATE_CREATE_TABLE);
	DDX_Control(pDX, IDC_STATE_INIT_DATA, _STATE_INIT_DATA);
	DDX_Control(pDX, IDC_CHECK_FILEZILLA, _CHECK_FILEZILLA);
	DDX_Control(pDX, IDC_CHECK_VNC, _CHECK_VNC);
	DDX_Control(pDX, IDC_STATE_FILEZILLA, _STATE_FILEZILLA);
	DDX_Control(pDX, IDC_STATE_VNC, _STATE_VNC);
	DDX_Control(pDX, IDC_CHECK_IIS, _CHECK_IIS);
	DDX_Control(pDX, IDC_CHECK_RSS, _CHECK_RSS);
	DDX_Control(pDX, IDC_STATE_IIS, _STATE_IIS);
	DDX_Control(pDX, IDC_STATE_RSS, _STATE_RSS);
	DDX_Control(pDX, IDC_LABEL_INTRO, _LABEL_INTRO);
	DDX_Control(pDX, IDC_BUTTON_1, _BUTTON_1);
	DDX_Control(pDX, IDC_BUTTON_2, _BUTTON_2);
	DDX_Control(pDX, IDC_BUTTON_3, _BUTTON_3);
	DDX_Control(pDX, IDC_BUTTON_4, _BUTTON_4);
	DDX_Control(pDX, IDC_BUTTON_5, _BUTTON_5);
	DDX_Control(pDX, IDC_BUTTON_6, _BUTTON_6);
	DDX_Control(pDX, IDC_BUTTON_7, _BUTTON_7);
	DDX_Control(pDX, IDC_CHECK_FILEZILLA2, _CHECK_FILEZILLA2);
	DDX_Control(pDX, IDC_STATE_FILEZILLA2, _STATE_FILEZILLA2);
	DDX_Control(pDX, IDC_BUTTON_8, _BUTTON_8);
	DDX_Control(pDX, IDC_STATE_START, _STATE_START);
	DDX_Control(pDX, IDC_CHECK_START, _CHECK_START);
	DDX_Control(pDX, IDC_CHECK_INIT_HOST, _CHECK_INIT_HOST);
	DDX_Control(pDX, IDC_STATE_INIT_HOST, _STATE_INIT_HOST);
	DDX_Control(pDX, IDC_STATIC_LOGO, logoVar);
	DDX_Control(pDX, IDC_STATIC_VERSION, versionLabel);
}

BEGIN_MESSAGE_MAP(CServerInstallDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_1, &CServerInstallDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON_2, &CServerInstallDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON_3, &CServerInstallDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON_4, &CServerInstallDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON_5, &CServerInstallDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON_6, &CServerInstallDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON_7, &CServerInstallDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDOK2, &CServerInstallDlg::OnBnClickedOk2)
	ON_BN_CLICKED(IDC_BUTTON_8, &CServerInstallDlg::OnBnClickedButton8)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK4, &CServerInstallDlg::OnBnClickedOk4)
	ON_BN_CLICKED(IDOK5, &CServerInstallDlg::OnBnClickedOk5)
END_MESSAGE_MAP()


// CServerInstallDlg 메시지 처리기

BOOL CServerInstallDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.


	ciArgParser::initialize(__argc,__argv);
	ciEnv::defaultEnv(ciFalse, "ubc");
	ciDebug::setDebugOn();
	ciDebug::logOpen("ServerInstall.log");

	
	ACE::init();

	// ORB Init
	_orbThread = new ::cciGUIORBThread(__argc,__argv,"UBCCENTER");
	_orbThread->start();

	ciDEBUG(1,("ORB init"));

	ciString ver;
	scratchUtil::getInstance()->getVersion(ver,ciTrue);
	ciString version = "Ver. " + ver;
	versionLabel.SetWindowText(version.c_str());

	ciString lang = scratchUtil::getInstance()->getLang();
	if(lang.empty() || lang != "ko"){
		ciString label = "Please Confirm Server Installation (" + lang + ")";
		_LABEL_INTRO.SetWindowText(label.c_str()); 
		if(lang=="jp"){
			ciString codepage;
			ciXProperties::getInstance()->get("PS.CM_DB.CODEPAGE",codepage);
			if(codepage.empty()){
				codepage="sjis";
				ciXProperties::getInstance()->set("PS.CM_DB.CODEPAGE",codepage.c_str());
				ciXProperties::getInstance()->set("PS.MYSQL_DB.CODEPAGE",codepage.c_str());
				ciXProperties::getInstance()->write();
			}
		}
	}
/*
	if(ciArgParser::getInstance()->isSet("+en") || ciArgParser::getInstance()->isSet("+jp") ){
		_LABEL_INTRO.SetWindowText("Please Confirm UBC Installation"); 
	}
*/
	cciDSUtil::onDS();
	cciDSUtil* aDsUtil = cciDSUtil::getInstance();
	aDsUtil->init_without_event();


	ciString customerInfo;
	ubcIni aIni("UBCVariables",_COP_CD("C:\\Project\\ubc\\config\\data"),"");
	aIni.get("CUSTOMER_INFO","NAME",customerInfo);
	if(customerInfo=="NARSHA"){
		ciDEBUG(1,("It's narsha !!!"));
		HBITMAP bitmap = ::LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_BITMAP2));
		HBITMAP oldBitMap = logoVar.SetBitmap(bitmap);
		DeleteObject(oldBitMap);
	}

	UpdateData(FALSE);

	SetTimer(300, 100, NULL);
	//_checkIntalledItem();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CServerInstallDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}


bool CServerInstallDlg::_checkIntalledItem()
{
	UpdateData(TRUE);

	//1. VC Stuff check
	bool bRet = true;
	bRet &= _checkVCRedist();
	bRet &= _checkVC2005();
	bRet &= _checkSOAP();

	if(bRet){
		_BUTTON_1.EnableWindow(0);
	}else{
		_BUTTON_1.EnableWindow(1);
	}
	
	//2. Env var check
	bRet = true;
	bRet &= _checkEnv1();
	bRet &= _checkEnv2();

	if(bRet){
		_BUTTON_2.EnableWindow(0);
	}else{
		_BUTTON_2.EnableWindow(1);
	}

	//3. Enterprise Key check
	bRet = true;
	bRet &= _checkEnterpriseKey();

	/* 인증은 언제나 재컴펌가능하다.
	if(bRet){
		_BUTTON_3.EnableWindow(0);
	}else{
		_BUTTON_3.EnableWindow(1);
	}
	*/ 

	//4. Ip address check
	bRet = true;
	bRet &= _checkIP();

	/* IP 언제나 재컴펌가능하다. 
	if(bRet){
		_BUTTON_4.EnableWindow(0);
	}else{
		_BUTTON_4.EnableWindow(1);
	}
	*/

	//5. Database check

	_CHECK_INIT_HOST.SetCheck(0);
	_CHECK_INIT_HOST.EnableWindow(0);
	_STATE_INIT_HOST.SetWindowText("ignored");
	//_STATE_INIT_HOST.SetTextColor(RGB(0xFF,0x00,0x00));
	//ciDEBUG(1,("Data Not initalized"));

	bRet = true;
	bool dbCheck=true;
	bRet &= _checkDBInstall();
	if(bRet){
		bRet &= _checkCreateUser();
		if(bRet){
			bRet &= _checkCreateDB();
			bRet &= _checkCreateTable();
			bRet &= _checkInitData();
			//bRet &= _checkInitHost();
		}else{
			dbCheck=false;
		}
	}else{
		dbCheck=false;
	}

	if(!dbCheck){
		_CHECK_CREATE_USER.SetCheck(1);
		_CHECK_CREATE_USER.EnableWindow(1);
		_STATE_CREATE_USER.SetWindowText("NOT Created");
		_STATE_CREATE_USER.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("User Not Created"));

		_CHECK_CREATE_DB.SetCheck(1);
		_CHECK_CREATE_DB.EnableWindow(1);
		_STATE_CREATE_DB.SetWindowText("NOT Created");
		_STATE_CREATE_DB.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("DB Not Created"));

		_CHECK_CREATE_TABLE.SetCheck(1);
		_CHECK_CREATE_TABLE.EnableWindow(1);
		_STATE_CREATE_TABLE.SetWindowText("NOT Created");
		_STATE_CREATE_TABLE.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Table Not Created"));

		_CHECK_INIT_DATA.SetCheck(1);
		_CHECK_INIT_DATA.EnableWindow(1);
		_STATE_INIT_DATA.SetWindowText("NOT initalized");
		_STATE_INIT_DATA.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Data Not initalized"));

		_CHECK_INIT_HOST.SetCheck(1);
		_CHECK_INIT_HOST.EnableWindow(1);
		_STATE_INIT_HOST.SetWindowText("NOT initalized");
		//_STATE_INIT_HOST.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Data Not initalized"));
	}

	if(bRet){
		_BUTTON_5.EnableWindow(0);
	}else{
		_BUTTON_5.EnableWindow(1);
	}

	//6. FileZilla and VNC check
	bRet = true;
	bRet &= _checkFileZillar();
	bRet &= _checkFileZillar2();
	bRet &= _checkVNC();

	if(bRet){
		_BUTTON_6.EnableWindow(0);
	}else{
		_BUTTON_6.EnableWindow(1);
	}

	//7. IIS check
	bRet = true;
	bRet &= _checkIIS();
	bRet &= _checkRSS();

	if(bRet){
		_BUTTON_7.EnableWindow(0);
	}else{
		_BUTTON_7.EnableWindow(1);
	}
	
	//8. 시작메뉴 check
	bRet = true;
	bRet &= _checkStart();

	if(bRet){
		_BUTTON_8.EnableWindow(0);
	}else{
		_BUTTON_8.EnableWindow(1);
	}
	UpdateData(FALSE);
	return bRet;
}


bool CServerInstallDlg::_checkVCRedist()
{
	ciDEBUG(1,("_checkVCRedist"));
	_CHECK_VCREDIST.SetCheck(0);
	_CHECK_VCREDIST.EnableWindow(0);
	_STATE_VCREDIST.SetWindowText("Installed");
	_STATE_VCREDIST.SetTextColor(RGB(0x00,0x00,0x00));
	
	return true;
}
bool CServerInstallDlg::_checkVC2005()
{
	ciDEBUG(1,("_checkVC2005"));
	bool retval=true;
	CFileFind ff;
	if(!ff.FindFile(_COP_PROGRAM_FILES("C:\\Program Files\\Microsoft Visual Studio 8\\VC\\bin\\cl.exe"))){
		_CHECK_VC2005.SetCheck(1);
		_CHECK_VC2005.EnableWindow(1);
		_STATE_VC2005.SetWindowText("NOT Installed");
		_STATE_VC2005.SetTextColor(RGB(0xFF,0x00,0x00));

		retval = false;
		ciDEBUG(1,("_checkVC2005 Not Installed"));
	}else{
		_CHECK_VC2005.SetCheck(0);
		_CHECK_VC2005.EnableWindow(0);
		_STATE_VC2005.SetWindowText("Installed");
		_STATE_VC2005.SetTextColor(RGB(0x00,0x00,0x00));

		ciDEBUG(1,("_checkVC2005 Installed"));
		retval = true;
	}
	ff.Close();
	return retval;
}

bool CServerInstallDlg::_checkSOAP()
{
	ciDEBUG(1,("_checkSOAP"));
	bool retval=true;
	CFileFind ff;
	if(!ff.FindFile(_COP_PROGRAM_FILES("C:\\Program Files\\MSSOAP\\Binaries\\MsSoapT3.exe"))){
		_CHECK_SOAP.SetCheck(1);
		_CHECK_SOAP.EnableWindow(1);
		_STATE_SOAP.SetWindowText("NOT Installed");
		_STATE_SOAP.SetTextColor(RGB(0xFF,0x00,0x00));

		retval = false;
		ciDEBUG(1,("_checkSOAP Not Installed"));
	}else{
		_CHECK_SOAP.SetCheck(0);
		_CHECK_SOAP.EnableWindow(0);
		_STATE_SOAP.SetWindowText("Installed");
		_STATE_SOAP.SetTextColor(RGB(0x00,0x00,0x00));

		ciDEBUG(1,("_checkSOAP Installed"));
		retval = true;
	}
	ff.Close();
	return retval;
}


bool CServerInstallDlg::_checkEnv1()
{
	ciDEBUG(1,("_checkEnv1"));

	bool retval=true;

	const char* ACE_ROOT = getenv("ACE_ROOT");
	const char* TAO_ROOT = getenv("TAO_ROOT");
	const char* CONFIGROOT = getenv("CONFIGROOT");
	const char* TRDPARTYROOT = getenv("TRDPARTYROOT");
	const char* COP_HOME = getenv("COP_HOME");
	const char* PROJECT_HOME = getenv("PROJECT_HOME");
	const char* PROJECT_CODE = getenv("PROJECT_CODE");
	const char* MYSQL_HOME = getenv("MYSQL_HOME");
	const char* COP_VCVER = getenv("COP_VCVER");

	if(	ACE_ROOT &&
		 TAO_ROOT &&
		 CONFIGROOT &&
		 TRDPARTYROOT &&
		 COP_HOME &&
		 PROJECT_HOME &&
		 PROJECT_CODE &&
		 MYSQL_HOME &&
		 COP_VCVER )
	{
		_CHECK_VAR1.SetCheck(0);
		_CHECK_VAR1.EnableWindow(0);
		_STATE_VAR1.SetWindowText("Installed");
		_STATE_VAR1.SetTextColor(RGB(0x00,0x00,0x00));
		ciDEBUG(1,("_checkEnv1 Installed"));
		retval = true;
	}else{
		_CHECK_VAR1.SetCheck(1);
		_CHECK_VAR1.EnableWindow(1);
		_STATE_VAR1.SetWindowText("NOT Installed");
		_STATE_VAR1.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("_checkEnv1 NOT Installed"));
		retval = false;
	}
	return retval;
}
bool CServerInstallDlg::_checkEnv2()
{
	ciDEBUG(1,("_checkEnv2"));

	bool retval=true;
	const char* PATH = getenv("PATH");

	should_be_added_path.clear();
	ciStringList::iterator itr;
	for(itr=should_be_exist_path.begin();itr!=should_be_exist_path.end();itr++){
		ciString ele = (*itr);
		ciBoolean matched = ciFalse;
		ciStringTokenizer tokens(PATH,";");
		while(tokens.hasMoreTokens()){
			ciString aToken = tokens.nextToken();
			ciStringUtil::toUpper(aToken);
			ciStringUtil::toUpper(ele);
			if(aToken == ele){
				matched = ciTrue;
				break;
			}
		}
		if(!matched){
			ciWARN(("%s is NOT exist in PATH",ele.c_str()));
			should_be_added_path.push_back(ele);
		}
	}
	if(should_be_added_path.size()==0){
		_CHECK_VAR2.SetCheck(0);
		_CHECK_VAR2.EnableWindow(0);
		_STATE_VAR2.SetWindowText("Installed");
		_STATE_VAR2.SetTextColor(RGB(0x00,0x00,0x00));
		ciDEBUG(1,("_checkEnv2 Installed"));
		retval = true;
	}else{
		_CHECK_VAR2.SetCheck(1);
		_CHECK_VAR2.EnableWindow(1);
		_STATE_VAR2.SetWindowText("NOT Installed");
		_STATE_VAR2.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("_checkEnv2 NOT Installed"));
		retval = false;
	}
	return retval;
}

bool CServerInstallDlg::_checkEnterpriseKey()
{
	ciDEBUG(1,("_checkEnterpriseKey"));
	bool retval=true;

	ciString enterpriseKey, passwd,mac;
	scratchUtil::getInstance()->readAuthFile(enterpriseKey,mac,passwd);

	if(enterpriseKey.empty()){
		retval=false;
	}else{
		_EDIT_KEY.SetWindowText(enterpriseKey.c_str());
	}
	if(passwd.empty()){
		retval=false;
	}else{
		_EDIT_PASSWD.SetWindowText(passwd.c_str());
	}

	if(retval){
		_STATE_KEY.SetWindowText("Confirmed");
		_STATE_KEY.SetTextColor(RGB(0x00,0x00,0x00));
		ciDEBUG(1,("_checkKey Authorized"));
	}else{
		_STATE_KEY.SetWindowText("NOT Confirmed");
		_STATE_KEY.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("_checkKey NOT Authorized"));
	}
	return retval;
}

bool CServerInstallDlg::_checkIP()
{
	ciDEBUG(1,("_checkIP"));


	ciString currentIp;
	const char* NAMESERVER_HOST = getenv("NAMESERVER_HOST");
	if(NAMESERVER_HOST && strlen(NAMESERVER_HOST)){
		currentIp = NAMESERVER_HOST;
	}else{
		scratchUtil::getInstance()->getIpAddress(currentIp);
	}

	if(currentIp.empty()){
		ciERROR(("GET Current IPAddress faild"));
		_STATE_IP.SetWindowText("NOT Confirmed, Please check your network First!!!");
		_STATE_IP.SetTextColor(RGB(0xFF,0x00,0x00));
		return false;
	}

	_EDIT_IP.SetWindowText(currentIp.c_str());

	//const char* NAMESERVER_HOST = getenv("NAMESERVER_HOST");
	if(!NAMESERVER_HOST || currentIp != NAMESERVER_HOST){
		_STATE_IP.SetWindowText("NOT Confirmed (NAMESERVER_HOST)");
		_STATE_IP.SetTextColor(RGB(0xFF,0x00,0x00));
		return false;
	}

	bool retval=true;
	retval &= _isSameInProperties("COP.SERVER_SP01.IPADDRESS",	currentIp.c_str());
	retval &= _isSameInProperties("COP.SERVER_SP02.IPADDRESS",	currentIp.c_str());
	retval &= _isSameInProperties("COP.SERVER_SP03.IPADDRESS",	currentIp.c_str());
	retval &= _isSameInProperties("COP.SOCKET_PROXY_SERVER.IP",	currentIp.c_str());
	retval &= _isSameInProperties("PS.CM_DB.SERVER",			currentIp.c_str());
	retval &= _isSameInProperties("PS.MYSQL_DB.SERVER",			currentIp.c_str());
	retval &= _isSameInProperties("PS.MS_DB.COP_ODBC",			currentIp.c_str());
	retval &= _isSameInProperties("AGT.SERVER.MUX",				currentIp.c_str());
	retval &= _isSameInProperties("AGT.SERVER.1",				currentIp.c_str());
	retval &= _isSameInProperties("AGT.SERVER.2",				currentIp.c_str());

	if(!retval){
		_STATE_IP.SetWindowText("NOT Confirmed (Properties)");
		_STATE_IP.SetTextColor(RGB(0xFF,0x00,0x00));
		return false;
	}
	
	retval &= _isSameInIni("ORB_NAMESERVICE","IP", currentIp.c_str());
	retval &= _isSameInIni("AGENTMUX","IP", currentIp.c_str());
	retval &= _isSameInIni("TIMESYNCER","IP", currentIp.c_str());
	retval &= _isSameInIni("VNCMANAGER","IP", currentIp.c_str());
	ciString url = "http://" + currentIp;
	retval &= _isSameInIni("DEVICEWIZARD","TestURL", url.c_str());

	if(!retval){
		_STATE_IP.SetWindowText("NOT Confirmed (ini)");
		_STATE_IP.SetTextColor(RGB(0xFF,0x00,0x00));
		return false;
	}

	_STATE_IP.SetWindowText("Confirmed");
	_STATE_IP.SetTextColor(RGB(0x00,0x00,0x00));
	return true;
}

bool
CServerInstallDlg::_isSameInIni(const char* section, const char* name, const char* value)
{
	ubcIni aIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");

	ciString buf;
	aIni.get(section,name,buf);

	if(strncmp(value,buf.c_str(),strlen(value))==0){
		ciDEBUG(1,("%s,%s=%s is same with %s", section, name, buf.c_str(), value));
		return true;
	}
	ciDEBUG(1,("%s,%s=%s is not same with %s", section, name, buf.c_str(), value));
	return false;
}

bool
CServerInstallDlg::_isSameInProperties(const char* name, const char* value)
{
	ciXProperties* aProp = ciXProperties::getInstance();

	ciString buf;
	aProp->get(name,buf);

	if(strncmp(value,buf.c_str(),strlen(value))==0){
		ciDEBUG(1,("%s=%s is same with %s", name, buf.c_str(), value));
		return true;
	}
	ciDEBUG(1,("%s=%s is not same with %s", name, buf.c_str(), value));
	return false;
}


bool CServerInstallDlg::_checkDBInstall()
{
	ciDEBUG(1,("_checkDBInstall"));

	bool retval=true;

	CFileFind ff;
	if(!ff.FindFile(_COP_PROGRAM_FILES("C:\\Program Files\\MySQL\\MySQL Server 5.0\\bin\\mysql.exe"))){
		_CHECK_INSTALL_DB.SetCheck(1);
		_CHECK_INSTALL_DB.EnableWindow(1);
		_STATE_INSTALL_DB.SetWindowText("NOT Installed");
		_STATE_INSTALL_DB.SetTextColor(RGB(0xFF,0x00,0x00));
		retval = false;
		ciDEBUG(1,("DB Not Installed"));
	}else{
		_CHECK_INSTALL_DB.SetCheck(0);
		_CHECK_INSTALL_DB.EnableWindow(0);
		_STATE_INSTALL_DB.SetWindowText("Installed");
		_STATE_INSTALL_DB.SetTextColor(RGB(0x00,0x00,0x00));
		ciDEBUG(1,("DB Installed"));
		retval = true;
	}
	ff.Close();
	return retval;
}

bool CServerInstallDlg::_checkCreateUser()
{
	ciDEBUG(1,("_checkCreateUser"));

	ciXProperties* aProp = ciXProperties::getInstance();

	ciString db,pwd,server,usr;
	ciShort port;

	aProp->get("PS.MYSQL_DB.DBNAME", db);
	aProp->get("PS.MYSQL_DB.PORT",port);
	aProp->get("PS.MYSQL_DB.PWD",pwd);
	aProp->get("PS.MYSQL_DB.SERVER",server);
	aProp->get("PS.MYSQL_DB.USR",usr);

	ciMySQL aSQL(db.c_str(),usr.c_str(),pwd.c_str(),server.c_str(),port);

	if(!aSQL.connect()){
		_CHECK_CREATE_USER.SetCheck(1);
		_CHECK_CREATE_USER.EnableWindow(1);
		_STATE_CREATE_USER.SetWindowText("NOT Created");
		_STATE_CREATE_USER.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("User Not Created"));
		return false;
	}
	aSQL.disconnect();
	_CHECK_CREATE_USER.SetCheck(0);
	_CHECK_CREATE_USER.EnableWindow(0);
	_STATE_CREATE_USER.SetWindowText("Created");
	_STATE_CREATE_USER.SetTextColor(RGB(0x00,0x00,0x00));
	ciDEBUG(1,("User Created"));
	return true;
}

bool CServerInstallDlg::_checkCreateDB()
{
	ciDEBUG(1,("_checkCreateDB"));
	ciXProperties* aProp = ciXProperties::getInstance();

	ciString db,pwd,server,usr;
	ciShort port;

	aProp->get("PS.MYSQL_DB.DBNAME", db);
	aProp->get("PS.MYSQL_DB.PORT",port);
	aProp->get("PS.MYSQL_DB.PWD",pwd);
	aProp->get("PS.MYSQL_DB.SERVER",server);
	aProp->get("PS.MYSQL_DB.USR",usr);

	ciMySQL aSQL(db.c_str(),usr.c_str(),pwd.c_str(),server.c_str(),port);

	if(!aSQL.connect()){
		_CHECK_CREATE_DB.SetCheck(1);
		_CHECK_CREATE_DB.EnableWindow(1);
		_STATE_CREATE_DB.SetWindowText("NOT Created");
		_STATE_CREATE_DB.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("DB Not Created"));
		return false;
	}
	aSQL.disconnect();
	_CHECK_CREATE_DB.SetCheck(0);
	_CHECK_CREATE_DB.EnableWindow(0);
	_STATE_CREATE_DB.SetWindowText("Created");
	_STATE_CREATE_DB.SetTextColor(RGB(0x00,0x00,0x00));
	ciDEBUG(1,("DB Created"));
	return true;
}

bool CServerInstallDlg::_checkCreateTable()
{
	ciDEBUG(1,("_checkCreateTable"));
	ciXProperties* aProp = ciXProperties::getInstance();

	ciString db,pwd,server,usr;
	ciShort port;

	aProp->get("PS.MYSQL_DB.DBNAME", db);
	aProp->get("PS.MYSQL_DB.PORT",port);
	aProp->get("PS.MYSQL_DB.PWD",pwd);
	aProp->get("PS.MYSQL_DB.SERVER",server);
	aProp->get("PS.MYSQL_DB.USR",usr);

	ciMySQL aSQL(db.c_str(),usr.c_str(),pwd.c_str(),server.c_str(),port);

	if(!aSQL.connect()){
		_CHECK_CREATE_TABLE.SetCheck(1);
		_CHECK_CREATE_TABLE.EnableWindow(1);
		_STATE_CREATE_TABLE.SetWindowText("NOT Created");
		_STATE_CREATE_TABLE.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Table Not Created"));
		return false;
	}

	if(!aSQL.execute("SELECT userId FROM ubc_user WHERE userId='super'")){
		_CHECK_CREATE_TABLE.SetCheck(1);
		_CHECK_CREATE_TABLE.EnableWindow(1);
		_STATE_CREATE_TABLE.SetWindowText("NOT Created");
		_STATE_CREATE_TABLE.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Table Not Created"));
		return false;
	}

	aSQL.disconnect();
	_CHECK_CREATE_TABLE.SetCheck(0);
	_CHECK_CREATE_TABLE.EnableWindow(0);
	_STATE_CREATE_TABLE.SetWindowText("Created");
	_STATE_CREATE_TABLE.SetTextColor(RGB(0x00,0x00,0x00));
	ciDEBUG(1,("Table Created"));
	return true;

}
bool CServerInstallDlg::_checkInitData()
{
	ciDEBUG(1,("_checkInitData"));
	ciXProperties* aProp = ciXProperties::getInstance();

	ciString db,pwd,server,usr;
	ciShort port;

	aProp->get("PS.MYSQL_DB.DBNAME", db);
	aProp->get("PS.MYSQL_DB.PORT",port);
	aProp->get("PS.MYSQL_DB.PWD",pwd);
	aProp->get("PS.MYSQL_DB.SERVER",server);
	aProp->get("PS.MYSQL_DB.USR",usr);

	ciMySQL aSQL(db.c_str(),usr.c_str(),pwd.c_str(),server.c_str(),port);

	if(!aSQL.connect()){
		_CHECK_INIT_DATA.SetCheck(1);
		_CHECK_INIT_DATA.EnableWindow(1);
		_STATE_INIT_DATA.SetWindowText("NOT initalized");
		_STATE_INIT_DATA.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Data Not initalized"));
		return false;
	}

	if(!aSQL.execute("SELECT userId FROM ubc_user WHERE userId='super'")){
		_CHECK_INIT_DATA.SetCheck(1);
		_CHECK_INIT_DATA.EnableWindow(1);
		_STATE_INIT_DATA.SetWindowText("NOT initalized");
		_STATE_INIT_DATA.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Data Not initalized"));
		return false;
	}

	if(!aSQL.next()){
		_CHECK_INIT_DATA.SetCheck(1);
		_CHECK_INIT_DATA.EnableWindow(1);
		_STATE_INIT_DATA.SetWindowText("NOT initalized");
		_STATE_INIT_DATA.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Data Not initalized"));
		return false;
	}

	aSQL.disconnect();
	_CHECK_INIT_DATA.SetCheck(0);
	_CHECK_INIT_DATA.EnableWindow(0);
	_STATE_INIT_DATA.SetWindowText("initalized");
	_STATE_INIT_DATA.SetTextColor(RGB(0x00,0x00,0x00));
	ciDEBUG(1,("Data initalized"));
	return true;
}

bool CServerInstallDlg::_checkInitHost()
{
	ciDEBUG(1,("_checkInitHost"));
	ciXProperties* aProp = ciXProperties::getInstance();

	ciString db,pwd,server,usr;
	ciShort port;

	aProp->get("PS.MYSQL_DB.DBNAME", db);
	aProp->get("PS.MYSQL_DB.PORT",port);
	aProp->get("PS.MYSQL_DB.PWD",pwd);
	aProp->get("PS.MYSQL_DB.SERVER",server);
	aProp->get("PS.MYSQL_DB.USR",usr);

	ciMySQL aSQL(db.c_str(),usr.c_str(),pwd.c_str(),server.c_str(),port);

	if(!aSQL.connect()){
		_CHECK_INIT_HOST.SetCheck(1);
		_CHECK_INIT_HOST.EnableWindow(1);
		_STATE_INIT_HOST.SetWindowText("NOT initalized");
		//_STATE_INIT_HOST.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Host Not initalized"));
		return false;
	}

	if(!aSQL.execute("SELECT hostId FROM ubc_host WHERE hostId is not null")){
		_CHECK_INIT_HOST.SetCheck(1);
		_CHECK_INIT_HOST.EnableWindow(1);
		_STATE_INIT_HOST.SetWindowText("NOT initalized");
		//_STATE_INIT_HOST.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Host Not initalized"));
		return false;
	}

	if(!aSQL.next()){
		_CHECK_INIT_HOST.SetCheck(1);
		_CHECK_INIT_HOST.EnableWindow(1);
		_STATE_INIT_HOST.SetWindowText("NOT initalized");
		//_STATE_INIT_HOST.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Host Not initalized"));
		return false;
	}

	aSQL.disconnect();
	_CHECK_INIT_HOST.SetCheck(0);
	_CHECK_INIT_HOST.EnableWindow(0);
	_STATE_INIT_HOST.SetWindowText("initalized");
	_STATE_INIT_HOST.SetTextColor(RGB(0x00,0x00,0x00));
	ciDEBUG(1,("Host initalized"));
	return true;
}

bool CServerInstallDlg::_checkFileZillar()
{
	ciDEBUG(1,("_checkFileZillar"));
	// patch 도 깔렸는지 확인이필요함.

	bool retval=true;

	CFileFind ff;
	if(!ff.FindFile(_COP_PROGRAM_FILES("C:\\Program Files\\FileZilla Server\\FileZilla server.exe"))){
		_CHECK_FILEZILLA.SetCheck(1);
		_CHECK_FILEZILLA.EnableWindow(1);
		_STATE_FILEZILLA.SetWindowText("NOT Installed");
		_STATE_FILEZILLA.SetTextColor(RGB(0xFF,0x00,0x00));
		retval = false;
		ciDEBUG(1,("FileZilla Not Installed"));
	}else{
		_CHECK_FILEZILLA.SetCheck(0);
		_CHECK_FILEZILLA.EnableWindow(0);
		_STATE_FILEZILLA.SetWindowText("Installed");
		_STATE_FILEZILLA.SetTextColor(RGB(0x00,0x00,0x00));
		ciDEBUG(1,("FileZilla Installed"));
		retval = true;
	}
	ff.Close();
	return retval;
}

bool CServerInstallDlg::_checkFileZillar2()
{
	ciDEBUG(1,("_checkFileZillar2"));
	// patch 도 깔렸는지 확인이필요함.


	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = FindFirstFile(
		_COP_PROGRAM_FILES("C:\\Program Files\\FileZilla Server\\FileZilla server.exe")
		,&FindFileData);

	if(hFind == INVALID_HANDLE_VALUE){
		_CHECK_FILEZILLA2.SetCheck(1);
		_CHECK_FILEZILLA2.EnableWindow(1);
		_STATE_FILEZILLA2.SetWindowText("NOT Installed");
		_STATE_FILEZILLA2.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("FileZilla2 Not Installed"));

		return false;
	}

	WIN32_FIND_DATA FindFileData2;
	HANDLE hFind2 = FindFirstFile(
		_COP_CD("C:\\Project\\ubc\\3rdparty\\FTP\\FileZilla server.exe")
		,&FindFileData2);

	if(hFind2 == INVALID_HANDLE_VALUE){
		_CHECK_FILEZILLA2.SetCheck(1);
		_CHECK_FILEZILLA2.EnableWindow(1);
		_STATE_FILEZILLA2.SetWindowText("NOT Installed");
		_STATE_FILEZILLA2.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("FileZilla2 patch is not exist"));
		return false;
	}

	bool retval=true;

	if(	FindFileData2.nFileSizeHigh != FindFileData.nFileSizeHigh ||
		FindFileData2.nFileSizeLow != FindFileData.nFileSizeLow ){
		_CHECK_FILEZILLA2.SetCheck(1);
		_CHECK_FILEZILLA2.EnableWindow(1);
		_STATE_FILEZILLA2.SetWindowText("NOT Installed");
		_STATE_FILEZILLA2.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("FileZilla2 patch is not exist"));
		retval=false;
	} else {
		_CHECK_FILEZILLA2.SetCheck(0);
		_CHECK_FILEZILLA2.EnableWindow(0);
		_STATE_FILEZILLA2.SetWindowText("Installed");
		_STATE_FILEZILLA2.SetTextColor(RGB(0x00,0x00,0x00));
		ciDEBUG(1,("FileZilla2 Installed"));
		retval=true;
	}
	::FindClose(hFind);
	::FindClose(hFind);
	return retval;
}


bool CServerInstallDlg::_checkVNC()
{
	ciDEBUG(1,("_checkVNC"));
	bool retval=true;

	CFileFind ff;
	if(!ff.FindFile(_COP_PROGRAM_FILES("C:\\Program Files\\UltraVNC\\vncviewer.exe"))){
		_CHECK_VNC.SetCheck(1);
		_CHECK_VNC.EnableWindow(1);
		_STATE_VNC.SetWindowText("NOT Installed");
		_STATE_VNC.SetTextColor(RGB(0xFF,0x00,0x00));
		retval = false;
		ciDEBUG(1,("VNC Not Installed"));
	}else{
		_CHECK_VNC.SetCheck(0);
		_CHECK_VNC.EnableWindow(0);
		_STATE_VNC.SetWindowText("Installed");
		_STATE_VNC.SetTextColor(RGB(0x00,0x00,0x00));
		ciDEBUG(1,("VNC Installed"));
		retval = true;
	}
	ff.Close();
	return retval;
}

bool CServerInstallDlg::_checkIIS()
{
	ciDEBUG(1,("_checkIIS"));

	ciString os;
	scratchUtil::getInstance()->getOSInfo(os);
	ciDEBUG(1,("os=%s", os.c_str()));

	ciString os2008 = "Microsoft Windows Server 2008";
	ciString os2003 = "Microsoft Windows Server 2003";

	FILE* fp = 0;
	if(os.substr(0,os2003.length())==os2003){
		fp = fopen("C:\\Windows\\system32\\inetsrv\\MetaBase.xml","r");
	}else{
		fp = fopen("C:\\Windows\\system32\\inetsrv\\config\\applicationHost.config","r");
	}

	if(!fp){
		_CHECK_IIS.SetCheck(1);
		_CHECK_IIS.EnableWindow(1);
		_STATE_IIS.SetWindowText("NOT Installed");
		_STATE_IIS.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("IIS Not Installed"));
		return false;
	}
	bool ubchome=false;
	bool ScreenShot=false;

	char buf[2048+1];
	while(fgets(buf,2048,fp)){
		ciString key = buf;
		ciStringUtil::toUpper(key);
		ciDEBUG(9,("%s",key.c_str()));
		if(strstr(key.c_str(),"UBCHOME")){
			ubchome=true;
		}else if(strstr(key.c_str(),"SCREENSHOT")){
			ScreenShot=true;
		}
		if(ubchome && ScreenShot) break;
	}
	fclose(fp);

	if(!ubchome || !ScreenShot) {
		_CHECK_IIS.SetCheck(1);
		_CHECK_IIS.EnableWindow(1);
		_STATE_IIS.SetWindowText("NOT Installed");
		_STATE_IIS.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("IIS NOT Installed"));
		return false;
	}
	_CHECK_IIS.SetCheck(0);
	_CHECK_IIS.EnableWindow(0);
	_STATE_IIS.SetWindowText("Installed");
	_STATE_IIS.SetTextColor(RGB(0x00,0x00,0x00));
	ciDEBUG(1,("IIS Installed"));
	return true;
}

bool CServerInstallDlg::_checkRSS()
{
	ciDEBUG(1,("_checkRSS"));
	{
		FILE* fp = fopen(_COP_CD("C:\\Inetpub\\wwwroot\\btn.gif"), "r");
		if(!fp){
			_CHECK_RSS.SetCheck(1);
			_CHECK_RSS.EnableWindow(1);
			_STATE_RSS.SetWindowText("NOT Installed");
			_STATE_RSS.SetTextColor(RGB(0xFF,0x00,0x00));
			ciDEBUG(1,("RSS Not Installed"));
			return false;
		}
		fclose(fp);
	}
	{
		FILE* fp = fopen(_COP_CD("C:\\Inetpub\\wwwroot\\gajaxscroller.js"), "r");
		if(!fp){
			_CHECK_RSS.SetCheck(1);
			_CHECK_RSS.EnableWindow(1);
			_STATE_RSS.SetWindowText("NOT Installed");
			_STATE_RSS.SetTextColor(RGB(0xFF,0x00,0x00));
			ciDEBUG(1,("RSS Not Installed"));
			return false;
		}
		fclose(fp);
	}
	{
		FILE* fp = fopen(_COP_CD("C:\\Inetpub\\wwwroot\\gfeedfetcher.js"), "r");
		if(!fp){
			_CHECK_RSS.SetCheck(1);
			_CHECK_RSS.EnableWindow(1);
			_STATE_RSS.SetWindowText("NOT Installed");
			_STATE_RSS.SetTextColor(RGB(0xFF,0x00,0x00));
			ciDEBUG(1,("RSS Not Installed"));
			return false;
		}
		fclose(fp);
	}
	{
		FILE* fp = fopen(_COP_CD("C:\\Inetpub\\wwwroot\\ubc_rss_pub.asp"), "r");
		if(!fp){
			_CHECK_RSS.SetCheck(1);
			_CHECK_RSS.EnableWindow(1);
			_STATE_RSS.SetWindowText("NOT Installed");
			_STATE_RSS.SetTextColor(RGB(0xFF,0x00,0x00));
			ciDEBUG(1,("RSS Not Installed"));
			return false;
		}
		fclose(fp);
	}
	{
		FILE* fp = fopen(_COP_CD("C:\\Inetpub\\wwwroot\\ubc_rss_url2.asp"), "r");
		if(!fp){
			_CHECK_RSS.SetCheck(1);
			_CHECK_RSS.EnableWindow(1);
			_STATE_RSS.SetWindowText("NOT Installed");
			_STATE_RSS.SetTextColor(RGB(0xFF,0x00,0x00));
			ciDEBUG(1,("RSS Not Installed"));
			return false;
		}
		fclose(fp);
	}
	{
		FILE* fp = fopen(_COP_CD("C:\\Inetpub\\wwwroot\\ubc_rss_url.asp"), "r");
		if(!fp){
			_CHECK_RSS.SetCheck(1);
			_CHECK_RSS.EnableWindow(1);
			_STATE_RSS.SetWindowText("NOT Installed");
			_STATE_RSS.SetTextColor(RGB(0xFF,0x00,0x00));
			ciDEBUG(1,("RSS Not Installed"));
			return false;
		}
		fclose(fp);
	}

	_CHECK_RSS.SetCheck(0);
	_CHECK_RSS.EnableWindow(0);
	_STATE_RSS.SetWindowText("Installed");
	_STATE_RSS.SetTextColor(RGB(0x00,0x00,0x00));
	ciDEBUG(1,("RSS Installed"));

	return true;
}
bool CServerInstallDlg::_checkStart()
{
	ciDEBUG(1,("_checkStart"));

	char TargetPath[MAX_PATH+1];
	::SHGetSpecialFolderPath (NULL, TargetPath, CSIDL_STARTUP, FALSE);

	ciString targetFile = TargetPath;
	targetFile += "\\UBC Start.lnk";

	CFileFind ff;
	if(!ff.FindFile(targetFile.c_str())){
		_CHECK_START.SetCheck(1);
		_CHECK_START.EnableWindow(1);
		_STATE_START.SetWindowText("NOT registered");
		_STATE_START.SetTextColor(RGB(0xFF,0x00,0x00));
		ciDEBUG(1,("Start Menu not registered"));
		return false;
	}

	_CHECK_START.SetCheck(0);
	_CHECK_START.EnableWindow(0);
	_STATE_START.SetWindowText("registered");
	_STATE_START.SetTextColor(RGB(0x00,0x00,0x00));
	ciDEBUG(1,("Start menu registered"));

	return true;
}
// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CServerInstallDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CServerInstallDlg::OnBnClickedButton1()
{
	bool flag1= false;
	bool flag2= false;
	bool flag3= false;

	if(_CHECK_VCREDIST.GetCheck()){
		ciString command = DOWNLOAD_ROOT;
		command += "VC\\vcredist_x86.exe";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Install vcredist_x86 Succeed");
			_CHECK_VCREDIST.SetCheck(0);
			_CHECK_VCREDIST.EnableWindow(0);
			_STATE_VCREDIST.SetWindowText("Installed");
			_STATE_VCREDIST.SetTextColor(RGB(0x00,0x00,0x00));
			flag1 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Install vcredist_x86 failed");
		}
	}else{
		flag1=true;
	}
	if(_CHECK_VC2005.GetCheck()){
		ciString command = DOWNLOAD_ROOT;
		command += "VC\\vcsetup.exe";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Install VC2005 Succeed");
			_CHECK_VC2005.SetCheck(0);
			_CHECK_VC2005.EnableWindow(0);
			_STATE_VC2005.SetWindowText("Installed");
			_STATE_VC2005.SetTextColor(RGB(0x00,0x00,0x00));
			flag2 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Install VC2005 failed");
		}
	}else{
		flag2=true;
	}
	if(_CHECK_SOAP.GetCheck()){
		ciString command = DOWNLOAD_ROOT;
		command += "VC\\soapsdk.exe";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Install SOAP Succeed");
			_CHECK_SOAP.SetCheck(0);
			_CHECK_SOAP.EnableWindow(0);
			_STATE_SOAP.SetWindowText("Installed");
			_STATE_SOAP.SetTextColor(RGB(0x00,0x00,0x00));
			flag3 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Install SOAP failed, download soapsdk.exe first");
		}
	}else{
		flag3=true;
	}	
	
	if(flag1 && flag2 && flag3){
		_BUTTON_1.EnableWindow(0);
	}
}

void CServerInstallDlg::OnBnClickedButton2()
{
	if(_CHECK_VAR1.GetCheck() || _CHECK_VAR2.GetCheck()){
		if(installUtil::getInstance()->setServerEnv()){
			ciDEBUG(1,("set environment variable  succeed"));
			MessageBox("set environment variable  Succeed");
			_CHECK_VAR1.SetCheck(0);
			_CHECK_VAR1.EnableWindow(0);
			_STATE_VAR1.SetWindowText("Installed");
			_STATE_VAR1.SetTextColor(RGB(0x00,0x00,0x00));
			_CHECK_VAR2.SetCheck(0);
			_CHECK_VAR2.EnableWindow(0);
			_STATE_VAR2.SetWindowText("Installed");
			_STATE_VAR2.SetTextColor(RGB(0x00,0x00,0x00));
			_BUTTON_2.EnableWindow(0);

		}else{
			ciERROR(("set environment variable fail"));
			MessageBox("set environment variable  failed");
		}
	}
}

void CServerInstallDlg::OnBnClickedButton3()
{
	char szKey[256];
	memset(szKey,0x00,256);

	char szPasswd[256];
	memset(szPasswd,0x00,256);

	char szIP[256];
	memset(szIP,0x00,256);

	char state[256];
	memset(state,0x00,sizeof(state));
	_STATE_IP.GetWindowText(state,sizeof(state));
	if(strcmp(state,"Confirmed")!=0){
		MessageBox("WARNING : Please Confirm IPAddress first.\n");
		return;
	}
	/*
	if(_BUTTON_4.IsWindowEnabled()){
		MessageBox("WARNING : Please Confirm IPAddress first.\n");
		return;
	}
	*/
	_EDIT_IP.GetWindowText(szIP,sizeof(szIP));


	_EDIT_KEY.GetWindowText(szKey,sizeof(szKey));
	if(strlen(szKey)==0){
		MessageBox("Please INPUT Enterprise Key");
		return;
	}
	_EDIT_PASSWD.GetWindowText(szPasswd,sizeof(szPasswd));
	if(strlen(szPasswd)==0){
		MessageBox("Please INPUT Password ");
		return;
	}

	ciString errString;
	ciString limit_max="0,0,0";

	// 1. password 검사.
	{
		cciCall aCall("get");
		cciEntity aEntity("SM=*/Enterprise=%s", szKey);
		aCall.setEntity(aEntity);
		aCall.addItem("password", "");
		aCall.addItem("comment1", ""); // 단말수, 사이트수, User수

		ciDEBUG(1,(aCall.toString()));
		try {
			if(!aCall.call()){
				errString="ERROR : Authentication Server Connection failed";
				ciERROR((errString.c_str()));
				MessageBox(errString.c_str());
				return ;
			}	
		}catch(CORBA::Exception& ex){
			errString="ERROR : Network Connection  failed : ";
			errString+=ex._name() ;
			ciERROR((errString.c_str()));
			MessageBox(errString.c_str());
			return ;
		}
		errString="ERROR : EnterpriseKey or Password is not matched";

		if(!aCall.hasMore()) {
			errString="ERROR : Authentication Server Operation failed(1)";
			MessageBox(errString.c_str());
			ciERROR((errString.c_str()));
			return ;

		}

		cciReply reply;
		if ( !aCall.getReply(reply)) {
			errString="ERROR : Authentication Server Operation failed(2)";
			MessageBox(errString.c_str());
			ciERROR((errString.c_str()));
			return ;
		}

		reply.unwrap();
		ciString password;
		if(!reply.getItem("password", password)){
			errString="ERROR : EnterpriseKey is not matched";
			MessageBox(errString.c_str());
			ciERROR((errString.c_str()));
			return ;
		}
		if(password != szPasswd){
			errString="ERROR : Password is not matched";
			MessageBox(errString.c_str());
			ciERROR((errString.c_str()));
			return ;	
		}
		if(!reply.getItem("comment1", limit_max)){
			errString="ERROR : EnterpriseKey is not matched";
			MessageBox(errString.c_str());
			ciERROR((errString.c_str()));
			return ;
		}

	}
	
	// 2.IP Address 를 set 한다.
	{
		cciCall aCall("set");
		cciEntity aEntity("SM=*/Enterprise=%s", szKey);
		aCall.addItem("ipAddress", (const char*)szIP);
		aCall.setEntity(aEntity);

		ciDEBUG(1,(aCall.toString()));
		try {
			if(!aCall.call()){
				errString="ERROR : Authentication Server Connection failed";
				ciERROR((errString.c_str()));
			}	
		}catch(CORBA::Exception& ex){
			errString="ERROR : Network Connection  failed : ";
			errString+=ex._name() ;
			ciERROR((errString.c_str()));
		}
	}

	// 인증
	//ciString mac=szPasswd;
	ciString mac;
	if(AUTH_SUCCEED != installUtil::getInstance()->auth("SERVER",(const char*)szKey,szPasswd,errString,mac)){
		MessageBox(errString.c_str());
		ciERROR((errString.c_str()));
		return ;
	}

	// 단말수, 사이트수, 유저수를 다른 인증서에 적는다.
	ciStringTokenizer aTokens(limit_max.c_str(),",");
	ciString max_host = "0";
	ciString max_site = "0";
	ciString max_user = "0";

	if(aTokens.hasMoreTokens()){
		max_host = aTokens.nextToken();
	}
	if(aTokens.hasMoreTokens()){
		max_site = aTokens.nextToken();
	}
	if(aTokens.hasMoreTokens()){
		max_user = aTokens.nextToken();
	}
	scratchUtil::getInstance()->writeLimitFile(max_host.c_str(),max_site.c_str(),max_user.c_str());

	MessageBox(errString.c_str());
	ciDEBUG(1,(errString.c_str()));
	
	_STATE_KEY.SetWindowText("Confirmed");
	_STATE_KEY.SetTextColor(RGB(0x00,0x00,0x00));
	//_BUTTON_3.EnableWindow(0);

	ciDEBUG(1,("_checkKey Authorized"));
	
	
	return;
}

void CServerInstallDlg::OnBnClickedButton4()
{
	char IP[256];
	memset(IP,0x00,256);
	_EDIT_IP.GetWindowText(IP,sizeof(IP));

	if(dbUtil::getInstance()->changeServerIP(IP)){
		_STATE_IP.SetWindowText("Confirmed");
		_STATE_IP.SetTextColor(RGB(0x00,0x00,0x00));
		//_BUTTON_4.EnableWindow(0);
		//_BUTTON_3.EnableWindow(1);
		MessageBox("IP Address Confirmed, Please RE-BOOT t system to apply changes");
		return;
	}
	MessageBox("IP Address Confirm failed");
	return;


}

void CServerInstallDlg::OnBnClickedButton5()
{
	bool flag1=false;
	bool flag2=false;
	bool flag3=false;
	bool flag4=false;
	bool flag5=false;
	bool flag6=false;

	if(_CHECK_INSTALL_DB.GetCheck()){
		ciString command = DOWNLOAD_ROOT;
		command += "mysql\\Setup.exe";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Install mysql Succeed");
			_CHECK_INSTALL_DB.SetCheck(0);
			_CHECK_INSTALL_DB.EnableWindow(0);
			_STATE_INSTALL_DB.SetWindowText("Installed");
			_STATE_INSTALL_DB.SetTextColor(RGB(0x00,0x00,0x00));
			flag1 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Install mysql failed");
		}
	}else{
		flag1=true;
	}

	if(_CHECK_CREATE_USER.GetCheck()){
		ciString command = "\"";
		command += MYSQL_COMMAND;
		command += "\"";
		command += " -h localhost -u root -p mysql < ";
		command += SQL_ROOT;
		command += "createUser.sql";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("create db user Succeed");
			_CHECK_CREATE_USER.SetCheck(0);
			_CHECK_CREATE_USER.EnableWindow(0);
			_STATE_CREATE_USER.SetWindowText("Installed");
			_STATE_CREATE_USER.SetTextColor(RGB(0x00,0x00,0x00));
			flag2 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("create db user failed");
		}
	}else{
		flag2=true;
	}

	if(_CHECK_CREATE_DB.GetCheck()){
		ciString command = "\"";
		command += MYSQL_COMMAND;
		command += "\"";
		command += " -h localhost -u ubc -p < ";
		command += SQL_ROOT;
		command += "createDB.sql";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("create db Succeed");
			_CHECK_CREATE_DB.SetCheck(0);
			_CHECK_CREATE_DB.EnableWindow(0);
			_STATE_CREATE_DB.SetWindowText("Installed");
			_STATE_CREATE_DB.SetTextColor(RGB(0x00,0x00,0x00));
			flag3 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("create db failed");
		}
	}else{
		flag3=true;
	}

	if(_CHECK_CREATE_TABLE.GetCheck()){
		ciString command = "\"";
		command += MYSQL_COMMAND;
		command += "\"";
		command += " -h localhost -u ubc -p ubc < ";
		command += SQL_ROOT;
		command += "createTable.sql";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("create table Succeed");
			_CHECK_CREATE_TABLE.SetCheck(0);
			_CHECK_CREATE_TABLE.EnableWindow(0);
			_STATE_CREATE_TABLE.SetWindowText("Installed");
			_STATE_CREATE_TABLE.SetTextColor(RGB(0x00,0x00,0x00));
			flag4 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("create table failed");
		}
	}else{
		flag4=true;
	}

	if(_CHECK_INIT_DATA.GetCheck()){
		ciString command = "\"";
		command += MYSQL_COMMAND;
		command += "\"";
		command += " -h localhost -u ubc -p ubc < ";
		command += SQL_ROOT;
		command += "createData.sql";
		if(system(command.c_str())==0){

			char szKey[256];
			memset(szKey,0x00,256);
			char szPasswd[256];
			memset(szPasswd,0x00,256);

			_EDIT_KEY.GetWindowText(szKey,sizeof(szKey));
			_EDIT_PASSWD.GetWindowText(szPasswd,sizeof(szPasswd));
			if(strlen(szKey) && strlen(szPasswd)){
				ciString siteId = "*";
				ciString enterpriseKey = szKey;
				ciString password = szPasswd;
				ciString hostId = "*";
				ciString customer= "";
				dbUtil* aUtil = dbUtil::getInstance();
				if(!aUtil->hostAutoUpdate(siteId, enterpriseKey, password,hostId,customer)){
					ciERROR((aUtil->getErrString()));
				}
			}

			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Initialize data Succeed");
			_CHECK_INIT_DATA.SetCheck(0);
			_CHECK_INIT_DATA.EnableWindow(0);
			_STATE_INIT_DATA.SetWindowText("Initialized");
			_STATE_INIT_DATA.SetTextColor(RGB(0x00,0x00,0x00));
			flag5 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Initialize data failed");
		}
	}else{
		flag5=true;
	}
	/*
	if(_CHECK_INIT_HOST.GetCheck()){
		
		char szKey[256];
		memset(szKey,0x00,256);
		char szPasswd[256];
		memset(szPasswd,0x00,256);

		_EDIT_KEY.GetWindowText(szKey,sizeof(szKey));
		_EDIT_PASSWD.GetWindowText(szPasswd,sizeof(szPasswd));

		if(strlen(szKey)==0 || strlen(szPasswd)==0){
			MessageBox("Input EnterpriseKey and Password first");
		}else{
			ciString siteId = "*";
			ciString enterpriseKey = szKey;
			ciString password = szPasswd;
			dbUtil* aUtil = dbUtil::getInstance();
			if(aUtil->hostAutoUpdate(siteId, enterpriseKey, password)){
				ciDEBUG(1,("hostAutUpdate succeed"));
				MessageBox("Initialize host Succeed");
				_CHECK_INIT_HOST.SetCheck(0);
				_CHECK_INIT_HOST.EnableWindow(0);
				_STATE_INIT_HOST.SetWindowText("Initialized");
				_STATE_INIT_HOST.SetTextColor(RGB(0x00,0x00,0x00));
				flag6 = true;
			}else{
				ciERROR((aUtil->getErrString()));
				MessageBox("Initialize host failed");
			}
		}
	}else{
		flag6=true;
	}
	*/
	//if(flag1 && flag2 && flag3 && flag4 && flag5 && flag6){
	if(flag1 && flag2 && flag3 && flag4 && flag5 ){
		_BUTTON_5.EnableWindow(0);
	}

}

void CServerInstallDlg::OnBnClickedButton6()
{
	bool flag1=false;
	bool flag2=false;
	bool flag3=false;

	if(_CHECK_FILEZILLA.GetCheck()){
		ciString command = DOWNLOAD_ROOT;
		command += "FTP\\FileZilla_Server-0_9_33.exe";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Install FileZilla Succeed");
			_CHECK_FILEZILLA.SetCheck(0);
			_CHECK_FILEZILLA.EnableWindow(0);
			_STATE_FILEZILLA.SetWindowText("Installed");
			_STATE_FILEZILLA.SetTextColor(RGB(0x00,0x00,0x00));
			flag1 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Install FileZilla failed");
		}
	}else{
		flag1=true;
	}
	if(_CHECK_FILEZILLA2.GetCheck()){
		_stopFTP();
		_copyFTPXML();
		ciString command = "copy /y ";
		command += "\"";
		command += DOWNLOAD_ROOT;
		command += "FTP\\FileZilla server.exe";
		command += "\"";
		command += " \"";
		command += FILEZILLA_ROOT;
		command += "\"";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Patch FileZilla Succeed");
			_CHECK_FILEZILLA2.SetCheck(0);
			_CHECK_FILEZILLA2.EnableWindow(0);
			_STATE_FILEZILLA2.SetWindowText("patched");
			_STATE_FILEZILLA2.SetTextColor(RGB(0x00,0x00,0x00));
			flag2 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Patch FileZilla failed");
		}
		_startFTP();
	}else{
		flag2=true;
	}
	if(_CHECK_VNC.GetCheck()){
		ciString command = DOWNLOAD_ROOT;
		command += "VNC\\UltraVNC_1.0.4_Setup.exe ";
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Install VNC Succeed");
			_CHECK_VNC.SetCheck(0);
			_CHECK_VNC.EnableWindow(0);
			_STATE_VNC.SetWindowText("installed");
			_STATE_VNC.SetTextColor(RGB(0x00,0x00,0x00));
			flag3 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Install VNC failed");
		}
	}else{
		flag3=true;
	}
	if(flag1 && flag2 && flag3){
		_BUTTON_6.EnableWindow(0);
	}
}
bool CServerInstallDlg::_copyFTPXML()
{
	ciString command = "copy /y ";
	command += "\"";
	command += DOWNLOAD_ROOT;
	command += "FTP\\FileZilla Server.xml";
	command += "\"";
	command += " \"";
	command += FILEZILLA_ROOT;
	command += "\"";
	if(system(command.c_str())==0){
		ciDEBUG(1,("%s succeed", command.c_str()));
		return true;
	}
	ciERROR(("%s fail", command.c_str()));
	return false;
}
bool CServerInstallDlg::_stopFTP()
{
	ciString command = "";
	command += "\"";
	command += FILEZILLA_ROOT;
	command += "FileZilla server.exe\" /stop";
	if(system(command.c_str())==0){
		ciDEBUG(1,("%s succeed", command.c_str()));
		return true;
	}
	ciERROR(("%s fail", command.c_str()));
	return false;
}

bool CServerInstallDlg::_startFTP()
{
	ciString command = "";
	command += "\"";
	command += FILEZILLA_ROOT;
	command += "FileZilla server.exe\" /start";
	if(system(command.c_str())==0){
		ciDEBUG(1,("%s succeed", command.c_str()));
		return true;
	}
	ciERROR(("%s fail", command.c_str()));
	return false;
}

void CServerInstallDlg::OnBnClickedButton7()
{
	ciBoolean flag1=false;
	ciBoolean flag2=false;

	if(_CHECK_IIS.GetCheck()){
		ciString command = _COP_CD("C:\\Project\\ubc\\bin8\\iis.lnk");
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("IIS Setup Succeed");
			_CHECK_IIS.SetCheck(0);
			_CHECK_IIS.EnableWindow(0);
			_STATE_IIS.SetWindowText("Installed");
			_STATE_IIS.SetTextColor(RGB(0x00,0x00,0x00));
			flag1 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("IIS Setup failed");
		}

	}else{
		flag1=true;
	}

	if(_CHECK_RSS.GetCheck()){
		// RSS 페이지를 COPY 한다.
		BOOL retval=true;
		retval &= ::CopyFile(_COP_CD("C:\\Project\\ubc\\config\\data\\btn.gif"), _COP_CD("C:\\Inetpub\\wwwroot\\btn.gif"), false);
		retval &= ::CopyFile(_COP_CD("C:\\Project\\ubc\\config\\data\\gajaxscroller.js"), _COP_CD("C:\\Inetpub\\wwwroot\\gajaxscroller.js"), false);
		retval &= ::CopyFile(_COP_CD("C:\\Project\\ubc\\config\\data\\gfeedfetcher.js"), _COP_CD("C:\\Inetpub\\wwwroot\\gfeedfetcher.js"), false);
		retval &= ::CopyFile(_COP_CD("C:\\Project\\ubc\\config\\data\\ubc_rss_pub.asp"), _COP_CD("C:\\Inetpub\\wwwroot\\ubc_rss_pub.asp"), false);
		retval &= ::CopyFile(_COP_CD("C:\\Project\\ubc\\config\\data\\ubc_rss_url2.asp"), _COP_CD("C:\\Inetpub\\wwwroot\\ubc_rss_url2.asp"), false);
		retval &= ::CopyFile(_COP_CD("C:\\Project\\ubc\\config\\data\\ubc_rss_url.asp"), _COP_CD("C:\\Inetpub\\wwwroot\\ubc_rss_url.asp"), false);
		if(retval){
			ciDEBUG(1,("COPY RSS succeed"));
			MessageBox("RSS Server Setup Succeed");
			_CHECK_RSS.SetCheck(0);
			_CHECK_RSS.EnableWindow(0);
			_STATE_RSS.SetWindowText("Installed");
			_STATE_RSS.SetTextColor(RGB(0x00,0x00,0x00));
			flag2 = true;
		}else{
			ciERROR(("COPY RSS file fail"));
			MessageBox("RSS Server Setup failed");
		}
	}else{
		flag2=true;
	}

	if(flag1 && flag2 ){
		_BUTTON_7.EnableWindow(0);
	}

}

void CServerInstallDlg::OnBnClickedButton8()
{
	ciBoolean flag1=false;
	if(_CHECK_START.GetCheck()){
		ciString command = _COP_CD("C:\\Project\\ubc\\bin8\\menuRegister /install /server");
		if(system(command.c_str())==0){
			ciDEBUG(1,("%s succeed", command.c_str()));
			MessageBox("Starup Menu Register Succeed");
			_CHECK_START.SetCheck(0);
			_CHECK_START.EnableWindow(0);
			_STATE_START.SetWindowText("Registered");
			_STATE_START.SetTextColor(RGB(0x00,0x00,0x00));
			flag1 = true;
		}else{
			ciERROR(("%s fail", command.c_str()));
			MessageBox("Starup Menu Register failed");
		}
	}else{
		flag1=true;
	}
	if(flag1){
		_BUTTON_8.EnableWindow(0);
	}
}

void CServerInstallDlg::OnBnClickedOk2()
{
	ciString errString;
	if(_BUTTON_1.IsWindowEnabled()){
		errString +="WARNING : VC is not installed.\n";
	}
	if(_BUTTON_2.IsWindowEnabled()){
		errString +="WARNING : Environment variable is not setup.\n";
	}

	char state[256];
	memset(state,0x00,sizeof(state));
	_STATE_KEY.GetWindowText(state,sizeof(state));
	if(strcmp(state,"Confirmed")!=0){
		errString +="WARNING : Enterprise key is not authorized.\n";
	}
	memset(state,0x00,sizeof(state));
	_STATE_IP.GetWindowText(state,sizeof(state));
	if(strcmp(state,"Confirmed")!=0){
		errString +="WARNING : IPAddress is not confirmed.\n";
	}
	/*
	if(_BUTTON_3.IsWindowEnabled()){
		errString +="WARNING : Enterprise key is not authorized.\n";
	}
	*/
	/*
	if(_BUTTON_4.IsWindowEnabled()){
		errString +="WARNING : IPAddress is not confirmed.\n";
	}
	*/
	if(_BUTTON_5.IsWindowEnabled()){
		errString +="WARNING : Database is not configured correctly.\n";
	}
	if(_BUTTON_6.IsWindowEnabled()){
		errString +="WARNING : 3rdParty is not installed.\n";
	}
	if(_BUTTON_7.IsWindowEnabled()){
		errString +="WARNING : IIS or RSS is not configured correctly.\n";
	}
	if(_BUTTON_8.IsWindowEnabled()){
		errString +="WARNING : Start menu is not registered.\n";
	}
	
	if(!errString.empty()){
		MessageBox(errString.c_str());
	}

	MessageBox("Note : Restart system to apply changes");
	CDialog::OnOK();
}



void CServerInstallDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nIDEvent == 300)
	{
		this->BeginWaitCursor();
		this->_checkIntalledItem();
		this->EndWaitCursor();
		KillTimer(300);
	}

	CDialog::OnTimer(nIDEvent);
}

void CServerInstallDlg::OnBnClickedOk4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// To MS-SQL
	installUtil::getInstance()->toMSSQL();
}

void CServerInstallDlg::OnBnClickedOk5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// To MY-SQL
	installUtil::getInstance()->toMYSQL();

}
