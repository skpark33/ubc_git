// ServerInstallDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <fstream>
#include "cci/libWorld/cciGUIORBThread.h"
#include "ci/libBase/ciListType.h"
#include "ExtraInstall/ColorStaticST.h"

using namespace std;
#include "afxdtctl.h"




// CServerInstallDlg 대화 상자
class CServerInstallDlg : public CDialog
{
// 생성입니다.
public:
	CServerInstallDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SERVERINSTALL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	::cciGUIORBThread* _orbThread;
	ciStringList should_be_added_path;
	ciStringList should_be_exist_path;


	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	bool _checkIntalledItem();
	bool _checkVCRedist();
	bool _checkVC2005();
	bool _checkSOAP();
	bool _checkEnv1();
	bool _checkEnv2();
	bool _checkEnterpriseKey();
	bool _checkIP();
	bool _checkDBInstall();
	bool _checkCreateUser();
	bool _checkCreateDB();
	bool _checkCreateTable();
	bool _checkInitData();
	bool _checkInitHost();
	bool _checkFileZillar();
	bool _checkFileZillar2();
	bool _checkVNC();
	bool _checkIIS();
	bool _checkRSS();
	bool _checkStart();

	bool	_isSameInProperties(const char* name, const char* value);
	bool	_isSameInIni(const char* section, const char* name, const char* value);

	bool	_stopFTP();
	bool	_startFTP();
	bool	_copyFTPXML();


public:
	CButton _CHECK_VCREDIST;
	CButton _CHECK_VC2005;
	CButton _CHECK_SOAP;
	CColorStaticST _STATE_VCREDIST;
	CColorStaticST _STATE_VC2005;
	CColorStaticST _STATE_SOAP;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	CButton _CHECK_VAR1;
	CButton _CHECK_VAR2;
	CColorStaticST _STATE_VAR1;
	CColorStaticST _STATE_VAR2;
	CEdit _EDIT_KEY;
	CEdit _EDIT_PASSWD;
	CColorStaticST _STATE_KEY;
	CEdit _EDIT_IP;
	CColorStaticST _STATE_IP;
	CButton _CHECK_INSTALL_DB;
	CButton _CHECK_CREATE_USER;
	CButton _CHECK_CREATE_DB;
	CButton _CHECK_CREATE_TABLE;
	CButton _CHECK_INIT_DATA;
	CColorStaticST _STATE_INSTALL_DB;
	CColorStaticST _STATE_CREATE_USER;
	CColorStaticST _STATE_CREATE_DB;
	CColorStaticST _STATE_CREATE_TABLE;
	CColorStaticST _STATE_INIT_DATA;
	CButton _CHECK_FILEZILLA;
	CButton _CHECK_VNC;
	CColorStaticST _STATE_FILEZILLA;
	CColorStaticST _STATE_VNC;
	CButton _CHECK_IIS;
	CColorStaticST _STATE_IIS;
	CButton _CHECK_RSS;
	CColorStaticST _STATE_RSS;
	CColorStaticST _LABEL_INTRO;
	CButton _BUTTON_1;
	CButton _BUTTON_2;
	CButton _BUTTON_3;
	CButton _BUTTON_4;
	CButton _BUTTON_5;
	CButton _BUTTON_6;
	CButton _BUTTON_7;
	CButton _CHECK_FILEZILLA2;
	CColorStaticST _STATE_FILEZILLA2;
	afx_msg void OnBnClickedOk2();
	CButton _BUTTON_8;
	CColorStaticST _STATE_START;
	CButton _CHECK_START;
	afx_msg void OnBnClickedButton8();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CButton _CHECK_INIT_HOST;
	CColorStaticST _STATE_INIT_HOST;
	CStatic logoVar;
	CStatic versionLabel;
	afx_msg void OnBnClickedOk4();
	afx_msg void OnBnClickedOk5();
};
