﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Password.Sha256
{
    public class fnSha256
    {
        public static String Password_Sha256(String pwd)
        {
            byte[] pwdbyte = Encoding.UTF8.GetBytes(pwd);
            SHA256Managed sha256 = new System.Security.Cryptography.SHA256Managed();
            byte[] output = sha256.ComputeHash(pwdbyte);

            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < output.Length; i++)
            {
                sb2.AppendFormat("{0,2:X2}", (int)(output[i]));
            }
            return sb2.ToString();
            //return Convert.ToBase64String(output);
        }
    }
}
