// UBCInstallDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "ListCtrl/ListCtrlEx.h"
#include "LogoFrame.h"


// CUBCInstallDlg 대화 상자
class CUBCInstallDlg : public CDialog
{
// 생성입니다.
public:
	CUBCInstallDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCINSTALL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	//afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg LRESULT OnCompleteInstall(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompleteCheck(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCheckedCount(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedLicenceBtn();
	DECLARE_MESSAGE_MAP()

	bool			m_bVncInstall;					///< VNC가 설치되었는지 여부
	bool			m_bCodecInstall;				///< Codec이 설치되었는지 여부
	//bool			m_bSetCodec;					///< Codec 프로퍼티가 설정되었는지 여부
	bool			m_bFlvInstall;					///< FLV Splitter가 설치되었는지 여부
	bool			m_bSetSoftDrive;				///< Soft drive사 설정되었는지 여부
	bool			m_bShortCut;					///< ShortCut이 설치되었는지 여부
	bool			m_bSecurity;					///< SecurityLevel1이 설정되었는지 여부
	bool			m_bWindowTrivial;				///< WindowTrivial이 설정되었는지 여부
	bool			m_bSetFirewall;					///< Firewall Exception이 설정되었는지 여부
	bool			m_bFlashInstall;				///< Flash Player가 설치되었는지 여부
	//bool			m_bLogMeInInstall;				///< LogMeIn이 설치되었는지 여부
	//bool			m_bAutoShutDown;				///< AutoShutDown 시간이 설정되었는지 여부

	CButton			m_btnInstall;					///< Install 시작 버튼
	CButton			m_btnCancel;					///< Install 종료,취소 버튼
	CString			m_strInstalledVersion;			///< Installed Version 값

	CLogoFrame*		m_pfrmLogo;						///< 에디션 로고를 표시하는 창
	CStatic			m_staticLogo;					///< 에디션 로고를 표시하는 컨트롤
	CListCtrlEx		m_listInstall;					///< 설치하는 항목을 표시하는 리스트 컨트롤
	CStringArray	m_aryArg;						///< 실행인자 배열
	CString			m_strIniFileName;				///< Ini 파일의 이름

	void	CheckIniFile(void);						///< UBCInstall.ini파일을 확인하고 없다면 초기화 한다.
	void	StartInstall(void);						///< 인스톨 과정을 시작한다.
	void	EndInstall(void);						///< 인스톨 과정을 종료한다.
	void	CheckInstall(void);						///< 인스톨이 되어 있는 상태를 확인한다.
	void	CreateList(void);						///< 리스트 컨트롤을 생성한다.
	void	SetInstallProgress(int nIdx,
								int nState,
								int nProgress=0);	///< 리스트 컨트롤에 작업상태를 설정한다.

	static UINT	ThreadCheckInstall(LPVOID param);	///< 인스톨 상태를 확인하는 thread
	static UINT	ThreadStartInstall(LPVOID param);	///< 인스톨을 시작하는 thread

	/////////////////////////////////////////////////////
	//인스톨과정에서 해야하는 것들
	// VNC 설치
	// K-lite codec pack 설치
	// 코덱 properties 설정
	// FLV splitter 설치
	// Soft drive 설정
	// 바로가기 만들기
	// Security level 1
	// Disable Windows OS trivial
	// Flash player 설치
	// LogMeIn client 설치
	/////////////////////////////////////////////////////
	void	CheckInstallVnc(void);					///< VNC가 설치되어 있는지 확인한다.
	void	CheckInstallCodec(void);				///< Codec이 설치되어 있는지 확인한다.
	//void	CheckSetCodecProperties(void);			///< Codec Properties가 설정되어 있는지 확인한다.
	void	CheckInstallFlv(void);					///< FLV Splitter가 설치되어 있는지 확인한다.
	//void	CheckInstallFlashPlayer(void);			///< Flash Player가 설치되어 있는지 확인한다.
	void	CheckSetSoftDrive(void);				///< Soft Drive가 설정되어 있는지 확인한다.
	void	CheckCreateShortCut(void);				///< ShortCut이 설정되어 있는지 확인한다.
	void	CheckDisableWindowTrivial(void);		///< Window Trivial이 설정되어 있는지 확인한다.
	void	CheckSetSecurity(void);					///< Security가 설정되어 있는지 확인한다.
	void	CheckSetFirewallException(void);		///< Firewall Exception이 설정되어 있는지 확인한다.
	//void	CheckInstallLogMeIn(void);				///< LogMeIn이 설치되어 있는지 확인한다.
	//void	CheckSetAutoShutDownTime(void);			///< Auto-shutdown time이 설정되었는지 확인한다.
		
	bool	InstallVnc(void);						///< VNC를 설치한다.	
	bool	InstallCodec(void);						///< K-lite codec pack을 설치한다.
	//bool	SetCodecPoperties(void);				///< 코덱 properties를 설정한다.
	bool	InstallFlvSplitter(void);				///< FLV splitter를 설치한다.
	//bool	InstallFlashPlayer(void);				///< Flash player를 설치한다.
	bool	SetSoftDrive(void);						///< Soft drive를 설정한다.
	bool	CreateShortCut(void);					///< 바로가기를 만든다.
	bool	DisableWindowTrivial(void);				///< Disable Windows OS trivial
	bool	SetSecurity(void);						///< Security를 설정한다.
	bool	SetFirewallException(void);				///< Set firewall exception automatically
	//bool	InstallLogMeIn(void);					///< LogMeIn client를 설치한다.
	//bool	SetAutoShutdownTime(void);				///< Auto-shutdown time을 설정한다.

	////////////////////////////////////////////////////////////////////////////////////////
	//인증 모듈에서 처리해야하는 기능들
	// SiteID 설정
	// VNC admin properties 설정
	// VNC background 파일 변경
	// 컴퓨터 이름 변경
	//bool	SetSiteId(void);						///< SiteID를 설정한다.
	//bool	SetVncAdminPoperties(void);				///< VNC admin properties 파일을 변경한다.
	//bool	ChangeVncBackgroundFile(void);			///< VNC background 파일을 변경한다.
	//bool	ChangeComputerName(void);				///< 컴퓨터 이름을 변경한다.
	////////////////////////////////////////////////////////////////////////////////////////

public:
	bool	CopyFiles(CString strSrc, CString strTarget);					///<소스의 모든 파일을 타겟의 위치로 복사한다.
	bool	RegistFile(CString strFile);									///<*.reg 파일을 regedit를 통하여 등록해준다.
	bool	RegistComponent(CString strCom, bool bRegistServer = true);		///<regsvr32를 통하여 컴포넌트를 시스템에 등록한다.
	bool	RunCmd(CString strCmd, CString strApp = _T(""));				///<CMD 명령을 실행
	bool	CreatePathDirectory(LPCTSTR lpszPath);							///<주어진 경로의 폴더를 만든다
	bool	StartSvc(CString strServiceName);								///<지정된 서비스를 시작한다.
	bool	StopSvc(CString strServiceName);								///<지정된 서비스를 중지한다.


	/*TCHAR* pszInstallList[8];
	TCHAR* pszInstallName[9];
	TCHAR* pszState[6];
	TCHAR* pszFilters[27];*/

protected:
	bool	IsExistNewCodecInstallFile();
};
