#pragma once

#include "ximage.h"

// CLogoFrame

class CLogoFrame : public CWnd
{
	DECLARE_DYNAMIC(CLogoFrame)

public:
	CLogoFrame();
	virtual ~CLogoFrame();

protected:
	DECLARE_MESSAGE_MAP()


	CxImage		m_bgLogo;						///<로고 이미지
public:
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


