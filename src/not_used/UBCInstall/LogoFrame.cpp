// LogoFrame.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCInstall.h"
#include "LogoFrame.h"
#include "MainDlg.h"
#include "MemDC.h"


// CLogoFrame

IMPLEMENT_DYNAMIC(CLogoFrame, CStatic)

CLogoFrame::CLogoFrame()
{
	
}

CLogoFrame::~CLogoFrame()
{
}


BEGIN_MESSAGE_MAP(CLogoFrame, CStatic)
	ON_WM_PAINT()
	ON_WM_CREATE()
END_MESSAGE_MAP()



// CLogoFrame 메시지 처리기입니다.

void CLogoFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CStatic::OnPaint()을(를) 호출하지 마십시오.
	CMemDC memDC(&dc);
	CRect rtLogo;
	GetClientRect(&rtLogo);
	if(m_bgLogo.IsValid())
	{
		//m_bgLogo.Draw2(memDC.m_hDC, rtLogo.left, rtLogo.top);
		m_bgLogo.Draw2(memDC.m_hDC, rtLogo);
	}//if
}

int CLogoFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	CMainDlg* pDlg = (CMainDlg*)AfxGetMainWnd();

	switch(pDlg->GetEdition())
	{
	case E_EDITION_PLUS:
		{
			HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_PLS_BMP));
			m_bgLogo.CreateFromHBITMAP(hBitmap);
		}
		break;
	case E_EDITION_ENT:
		{
			CString strCustomer = GetINIValue(_T("UBCVariables.ini"), _T("CUSTOMER_INFO"), _T("NAME"));
			if(strCustomer == _T("NARSHA"))
			{
				HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_NARSHA_BMP));
				m_bgLogo.CreateFromHBITMAP(hBitmap);
			}
			else
			{
				HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_ENT_BMP));
				m_bgLogo.CreateFromHBITMAP(hBitmap);
			}//if
		}
		break;
	default:
		{
			HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_STD_BMP));
			m_bgLogo.CreateFromHBITMAP(hBitmap);
		}
	}//switch

	CRect rtClient;
	GetClientRect(rtClient);
	m_bgLogo.Resample(rtClient.Width(), rtClient.Height(), 3);

	return 0;
}
