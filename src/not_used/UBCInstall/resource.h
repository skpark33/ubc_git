//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCInstall.rc
//
#define IDCANCEL2                       3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_UBCINSTALL_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDB_CHECK_BOXES                 130
#define IDB_STD_BMP                     131
#define IDB_PLS_BMP                     132
#define IDB_ENT_BMP                     133
#define IDB_NARSHA_BMP                  134
#define IDB_WALL_BMP                    135
#define IDD_MAIN_DLG                    136
#define IDC_INSTALL_LIST                1000
#define IDC_INSTALLED_STATIC            1002
#define IDC_LOGO_FRAME                  1004
#define ID_                             1005
#define ID_LICENCE_BTN                  1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
