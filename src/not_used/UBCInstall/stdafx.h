// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

using namespace std;

//인스톨 목록
enum E_STEP_INSTALL
{
	E_INSTALL_VNC,				//Install VNC
	E_INSTALL_CODEC,			//Install codec
	//E_SET_CODECPROPERTIES,		//Set codec properties
	E_INSTALL_FLVSPLITTER,		//Install FLV splitter
	//E_INSTALL_FLASHPLAYER,		//Install falsh plash player
	E_SET_SFTDRIVE,				//Set soft drive
	E_CREATE_SHORTCUT,			//Create shortcut
	E_SET_FIREWALLEXCEPTION,	//Set firewall exception
	E_DISABLE_WINDOWTRIVIAL,	//Disable windows trivial
	E_SET_SECURITY,				//Set security
	//E_INSTALL_LOGMEIN,			//Install logmein
	//E_SET_AUTOSHUTDOWN,			//Set auto shutdown time
	E_STEP_MAX					//13
};

//인스톨 상태
enum E_STATE_INSTALL
{
	E_STATE_INSTALL_FAIL = -1,
	E_STATE_NOTINSTALLED,
	E_STATE_INSTALLED,
	E_STATE_CHECKING,
	E_STATE_INSTALLING,
	E_STATE_OPTIONAL,
	E_STATE_SKIPED,
	E_STATE_MAX
};

//제품 에디션
enum E_UBC_EDITION
{
	E_EDITION_PE,		//Personal Edition
	E_EDITION_PLUS,		//Plus Edition
	E_EDITION_ENT,		//Enterprise Edition
};

 enum E_OS_VERSION
{
	E_OS_WINXP = 5,
	E_OS_WIN7
};

//컬럼 인포
typedef struct {
	TCHAR*	text;
	int		idx;
	int		width;
	int		align;
} LISTCTRL_COLUMN_INFO;



#define		COUNT_FILTER			27					//등록하는 필터의 개수	
#define		COLUMN_COUNT			3					//컬럼의 수
#define		BUF_SIZE				(1024*100)			//버퍼 사이즈

#define		RGB_CHECKING_BG			RGB(204 ,218, 231)	//Checking 상태의 배경 색
#define		RGB_CHECKING_FG			RGB(0 ,0, 0)		//Checking 상태의 글자 색

#define		RGB_INSTALLING_BG		RGB(204 ,218, 231)	//Installing 상태의 배경 색
#define		RGB_INSTALLING_FG		RGB(0 ,0, 0)		//Installing 상태의 글자 색

#define		RGB_INSTALLED_BG		RGB(255 ,255, 255)	//Installed 상태의 배경 색
#define		RGB_INSTALLED_FG		RGB(0 ,0, 0)		//Installed 상태의 글자 색

#define		RGB_NOTINSTALLED_BG		RGB(255 ,238, 98)	//Not installed 상태의 배경 색
#define		RGB_NOTINSTALLED_FG		RGB(0 ,0, 0)		//Not installed 상태의 글자 색

#define		RGB_INSTALL_FAIL_BG		RGB(255 ,255, 255)	//Install fail 상태의 배경 색
#define		RGB_INSTALL_FAIL_FG		RGB(255 ,0, 0)		//Install fail 상태의 글자 색

#define		RGB_OPTIONAL_BG			RGB(220 ,220, 220)	//Optional 상태의 배경 색
#define		RGB_OPTIONAL_FG			RGB(0 ,0, 0)		//Optional 상태의 글자 색

#define		RGB_SKIPED_BG			RGB(220 ,220, 220)	//Optional 상태의 배경 색
#define		RGB_SKIPED_FG			RGB(0 ,0, 0)		//Optional 상태의 글자 색

#define		WM_COMPLETE_INSTALL		(WM_USER + 100)		//Install 완료 메시지
#define		WM_CHECKED_COUNT		(WM_USER + 101)		//인스톨 리스트에서 체크된 개수를 알리는 메시지
#define		WM_COMPLETE_CHECK		(WM_USER + 102)		//Check 완료 메시지

#define		PATH_3RD_PARTY			_T("\\SQISoft\\3rdparty\\")
//#define		PATH_REGEDIT			_T("C:\\Windows\\regedit /s ")
//#define		PATH_REGEDIT			_T("C:\\Windows\\regedit")
#define		PATH_CONTENTS			_T("\\SQISoft\\Contents /s /d")

#define		FILE_VNC_REGIST			_T("uvnc_service.reg")
#define		FILE_VNC_REGIST64		_T("uvnc_service64.reg")
#define		FILE_SOFTDRIVE_BAT		_T("softDrive.bat")
//#define		FILE_MENU_REGIST		_T("MenuRegister.exe /install")
#define		FILE_MENU_REGIST		_T("MenuRegister.exe")
#define		FILE_FLASH_PLAYER		_T("Flash10l.ocx")
#define		FILE_FLV_FILTER			_T("FLVSplitter.ax")
#define		FILE_FLV_REGIST			_T("FLVSplitter.reg")
#define		FILE_FLV_REGIST64		_T("FLVSplitter64.reg")
#define		FILE_CODEC_REGIST		_T("Codec.reg")
#define		FILE_CODEC_REGIST64		_T("Codec64.reg")
#define		FILE_FLASH_REGIST		_T("Flash.reg")
#define		FILE_FLASH_REGIST64		_T("Flash64.reg")
#define		FILE_LICENCE_EXE		_T("UBCHostAuthorizer.exe")

#define		CMD_SECURITY			_T("attrib +h ")





#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


CString GetINIValue(CString strFileName, CString strSection, CString strKey);						///< 지정된 ini 파일의 값을 반환한다.
void	WriteINIValue(CString strFileName, CString strSection, CString strKey, CString strValue);	///< 지정된 ini 파일의 key에 값을 쓴다.
int		GetOSVersion(void);																			///< 시스템의 Windows 버전을 반환한다.
bool	Is64OS(void);																				///< 설치된 운영체제가 64비트 운영체제인지 판단한다.
bool	IsPosReady(void);																			///< 설치된 운영체제가 PosReady인지 판단한다.

LPCTSTR	GetAppPath(void);																			///< 실행프로그램의 경로를 반환
LPCTSTR GetAppDrive(void);																			///< 실행 프로그램의 드라이브 반환
LPCTSTR GetWinDir(void);																			///< 시스템의 Windows 경로를 반환한다.
LPCTSTR	GetProgramDir(void);																		///< 시스템의 Program Files 경로를 반환한다.
LPCTSTR	GetSystemDir(void);																			///< 시스템의 System 경로를 반환한다.
LPCTSTR	Get3rdPartyDir(void);																		///< 3rdParty 경로를 반환한다.
