#pragma once

//#include "ximage.h"
#include "UBCInstallDlg.h"


// CMainDlg 대화 상자입니다.

class CMainDlg : public CDialog
{
	DECLARE_DYNAMIC(CMainDlg)

public:
	CMainDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMainDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MAIN_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);


public:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnDestroy();

	//CxImage		m_bgLogo;						///<로고 이미지
	//CxImage		m_bgImage;						///<배경 이미지
	CUBCInstallDlg*	m_pdlgInstall;					///<인스톨 화면
	COLORREF		m_rgbBgColor;					///<배경색
	CStringArray	m_aryArg;						///< 실행인자 배열
	short			m_sEdition;						///< 패키지의 Edition
	static bool		m_bInstallVNC;					///< VNC 설치유무

	void			AddArg(CString strArg);			///< 프로그램의 실행인자를 설정한다.
	bool			IsSetArg(CString strArg);		///< 실행인자가 설정되었는지 학인한다.
	short			GetEdition(void);				///< 에디션을 반환
};
