// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// UBCInstall.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);

LPFN_ISWOW64PROCESS fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle(_T("kernel32")),_T("IsWow64Process"));

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 ini 파일의 값을 반환한다. \n
/// @param (CString) strFileName : (in) ini파일 이름
/// @param (CString) strSection : (in) ini파일의 section 
/// @param (CString) strKey : (in) 구하려는 값의 key값
/// @return <형: CString> \n
///			<key값의 value> \n
/////////////////////////////////////////////////////////////////////////////////
CString GetINIValue(CString strFileName, CString strSection, CString strKey)
{
	char cBuffer[BUF_SIZE] = { 0x00 };
	CString strPath = GetAppPath();
	strPath += _T("data\\");
	strPath += strFileName;
	GetPrivateProfileString(strSection, strKey, _T(""), cBuffer, BUF_SIZE, strPath);

	return CString(cBuffer);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 ini 파일의 key에 값을 쓴다. \n
/// @param (CString) strFileName : (in) ini파일 이름
/// @param (CString) strSection : (in) ini파일의 section
/// @param (CString) strKey : (in) 기록하려는 값의 key값
/// @param (CString) strValue : (in) 기록하려는 값
/////////////////////////////////////////////////////////////////////////////////
void WriteINIValue(CString strFileName, CString strSection, CString strKey, CString strValue)
{
	CString strPath = GetAppPath();
	strPath += _T("data\\");
	strPath += strFileName;
	WritePrivateProfileString(strSection, strKey, strValue, strPath);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 실행프로그램의 경로를 반환 \n
/// @return <형: LPCTSTR> \n
///			<실행프로그램의 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR GetAppPath(void)
{
	static CString strAppPath = _T("");

	if(strAppPath.GetLength() == 0)
	{
		CString strPath;
		TCHAR szModule[MAX_PATH];
		::ZeroMemory(szModule, MAX_PATH);
		::GetModuleFileName(NULL, szModule, MAX_PATH);

		TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
		_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
		strPath = szDrive;
		strPath += szPath;
		strAppPath = strPath;
	}//if

	return strAppPath;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 실행 프로그램의 드라이브 반환 \n
/// @return <형: LPCTSTR> \n
///			<실행 프로그램의 드라이브> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR GetAppDrive()
{
	static CString strAppDrive = _T("");

	if(strAppDrive.GetLength() == 0)
	{
		TCHAR szModule[MAX_PATH];
		::ZeroMemory(szModule, MAX_PATH);
		::GetModuleFileName(NULL, szModule, MAX_PATH);

		TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
		_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);
		strAppDrive = szDrive;
	}//if

	return strAppDrive;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템의 Windows 버전을 반환한다. \n
/// @return <형: int> \n
///			Windows Major 버전 번호 \n
/////////////////////////////////////////////////////////////////////////////////
int GetOSVersion()
{
	OSVERSIONINFO stOSver;
	memset(&stOSver, 0x00, sizeof(OSVERSIONINFO));
	stOSver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if(GetVersionEx(&stOSver))
	{        
		return (int)stOSver.dwMajorVersion;
	}//if

	return -1;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설치된 운영체제가 64비트 운영체제인지 판단한다. \n
/// @return <형: bool> \n
///			<true: 64비트 운영체제> \n
///			<false: 64비트 운영체제가 아님> \n
/////////////////////////////////////////////////////////////////////////////////
bool Is64OS()
{
	if(GetOSVersion() != E_OS_WIN7)
	{
		return false;
	}//if

	BOOL bIsWow64 = FALSE; 
    if(NULL != fnIsWow64Process)
    {
        if(!fnIsWow64Process(GetCurrentProcess(), &bIsWow64))
        {
            // handle error
        }//if
    }//if

    return (bool)bIsWow64;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 설치된 운영체제가 PosReady인지 판단한다. \n
/// @return <형: bool> \n
///			<true: PosReady> \n
///			<false: XP> \n
/////////////////////////////////////////////////////////////////////////////////
bool IsPosReady()
{
	HKEY hKey;
	LONG lRet;
	bool bPos;

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\POSReady"), 0, KEY_ALL_ACCESS, &hKey);
	if(lRet == ERROR_SUCCESS)
	{
		bPos = false;
	
	}
	else
	{
		bPos = true;
	}//if
	RegCloseKey(hKey);

	return bPos;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템의 Windows 경로를 반환한다. \n
/// @return <형: LPCTSTR> \n
///			<값: Windows 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR GetWinDir()
{
	static CString strWinDir = _T("");

	if(strWinDir.GetLength() == 0)
	{
		TCHAR szPath[MAX_PATH] = { 0x00 };
		if(!SHGetSpecialFolderPath(NULL, szPath, CSIDL_PROGRAM_FILES, FALSE))
		{
			strWinDir = _T("C:\\WINDOWS\\");
		}
		else
		{
			strWinDir = szPath;
			strWinDir += _T("\\");
		}//if
	}//if

	return strWinDir;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템의 Program Files 경로를 반환한다. \n
/// @return <형: LPCTSTR> \n
///			<값: Program Files 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR GetProgramDir()
{
	static CString strProgramDir = _T("");

	if(strProgramDir.GetLength() == 0)
	{
		TCHAR szPath[MAX_PATH] = { 0x00 };
		if(!SHGetSpecialFolderPath(NULL, szPath, CSIDL_PROGRAM_FILES, FALSE))
		{
			strProgramDir = _T("C:\\Program Files\\");
		}
		else
		{
			strProgramDir = szPath;
			strProgramDir += _T("\\");
		}//if
	}//if

	return strProgramDir;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 시스템의 System 경로를 반환한다. \n
/// @return <형: LPCTSTR> \n
///			<값: System 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR GetSystemDir()
{
	static CString strSystemDir = _T("");

	if(strSystemDir.GetLength() == 0)
	{
		TCHAR szPath[MAX_PATH] = { 0x00 };
		if(!SHGetSpecialFolderPath(NULL, szPath, CSIDL_SYSTEM, FALSE))
		{
			if(!Is64OS())
			{
				strSystemDir = _T("C:\\WINDOWS\\System32\\");
			}
			else
			{
				strSystemDir = _T("C:\\WINDOWS\\SysWOW64\\");
			}//if
		}
		else
		{
			strSystemDir = szPath;
			strSystemDir += _T("\\");
			if(Is64OS())
			{
				strSystemDir.Replace(_T("system32"), _T("SysWOW64"));
			}//if
		}//if
	}//if

	return strSystemDir;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 3rdParty 경로를 반환한다. \n
/// @return <형: LPCTSTR> \n
///			<값: 3rdParty 경로> \n
/////////////////////////////////////////////////////////////////////////////////
LPCTSTR Get3rdPartyDir()
{
	static CString str3rdDir = _T("");

	if(str3rdDir.GetLength() == 0)
	{
#ifndef _DEBUG
		str3rdDir = GetAppDrive();
#else if
		str3rdDir = _T("C:");
#endif
		str3rdDir += PATH_3RD_PARTY;
	}//if

	return str3rdDir;
}


