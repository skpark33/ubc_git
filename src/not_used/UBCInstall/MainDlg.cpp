// MainDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCInstall.h"
#include "MainDlg.h"
#include "MemDC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define		GRADIANT_VERT
#define		COLOR_STD		RGB(53, 66, 10)
#define		COLOR_PLS		RGB(138, 4, 15)
#define		COLOR_ENT		RGB(19, 33, 56)



// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMainDlg 대화 상자입니다.


bool CMainDlg::m_bInstallVNC = true;					///< VNC 설치유무


IMPLEMENT_DYNAMIC(CMainDlg, CDialog)

CMainDlg::CMainDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMainDlg::IDD, pParent)
	,m_pdlgInstall(NULL)
{
	m_rgbBgColor = RGB(0, 0, 0);
}

CMainDlg::~CMainDlg()
{
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMainDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()


// CMainDlg 메시지 처리기입니다.

BOOL CMainDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	/*HBITMAP hBitmap = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_WALL_BMP));
	m_bgImage.CreateFromHBITMAP(hBitmap);*/
	ShowWindow(SW_MAXIMIZE);

	if(IsSetArg(_T("+PLS")))
	{
		m_sEdition = E_EDITION_PLUS;
		m_rgbBgColor = RGB(138, 4, 15);
	}
	else if(IsSetArg(_T("+ENT")))
	{
		m_sEdition = E_EDITION_ENT;
		m_rgbBgColor = RGB(19, 33, 56);
	}
	else
	{
		m_sEdition = E_EDITION_PE;
		m_rgbBgColor = RGB(53, 66, 10);
	}//if

	if(IsSetArg(_T("+NoVNC")))
		m_bInstallVNC = false;
	else
		m_bInstallVNC = true;

	m_pdlgInstall = new CUBCInstallDlg(this);
	m_pdlgInstall->Create(IDD_UBCINSTALL_DIALOG, this);

	//ModifyStyle(/*WS_CAPTION |*/ WS_SYSMENU | WS_MAXIMIZEBOX | WS_SIZEBOX, NULL);
	ModifyStyle(NULL, WS_CLIPCHILDREN);
	SetWindowPos(&wndTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_SHOWWINDOW);
	ShowWindow(SW_MAXIMIZE);

	m_pdlgInstall->CenterWindow(this);
	m_pdlgInstall->SetWindowPos(&wndTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_SHOWWINDOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMainDlg::OnPaint()
{
	//CDialog::OnPaint();

	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
	CMemDC memDC(&dc);

	CRect rtRect;
	GetClientRect(&rtRect);

#ifdef GRADIANT_VERT
	TRIVERTEX			vert[4];
	GRADIENT_TRIANGLE	gTri[2];

	vert [0] .x       =  rtRect.left;
	vert [0] .y       =  rtRect.top;
	vert [0] .Red     =  0xff00;
	vert [0] .Green   =  0xff00;
	vert [0] .Blue    =  0xff00;
	vert [0] .Alpha   =  0x0000;

	vert [1] .x       =  rtRect.right;
	vert [1] .y       =  rtRect.top;
	vert [1] .Red     =  0xff00;
	vert [1] .Green   =  0xff00;
	vert [1] .Blue    =  0xff00;
	vert [1] .Alpha   =  0x0000;

	vert [2] .x       =  rtRect.right;
	vert [2] .y       =  rtRect.bottom; 
	vert [2] .Red     =  (m_rgbBgColor & 0x000000FF) << 8;
	vert [2] .Green   =  (m_rgbBgColor & 0x0000FF00);
	vert [2] .Blue    =  (m_rgbBgColor & 0x00FF0000) >> 8;
	vert [2] .Alpha   =  0x0000;

	vert [3] .x       =  rtRect.left;
	vert [3] .y       =  rtRect.bottom;
	vert [3] .Red     =  (m_rgbBgColor & 0x000000FF) << 8;
	vert [3] .Green   =  (m_rgbBgColor & 0x0000FF00);
	vert [3] .Blue    =  (m_rgbBgColor & 0x00FF0000) >> 8;
	vert [3] .Alpha   =  0x0000;

	gTri[0].Vertex1   = 0;
	gTri[0].Vertex2   = 1;
	gTri[0].Vertex3   = 2;

	gTri[1].Vertex1   = 0;
	gTri[1].Vertex2   = 2;
	gTri[1].Vertex3   = 3;

	GradientFill(memDC.m_hDC, vert, 4,&gTri, 2, GRADIENT_FILL_TRIANGLE);

#else if

	TRIVERTEX			vert[4];
	GRADIENT_TRIANGLE	gTri[2];
	vert [0] .x       =  rtRect.left;
	vert [0] .y       =  rtRect.top;
	vert [0] .Red     =  0xff00;
	vert [0] .Green   =  0xff00;
	vert [0] .Blue    =  0xff00;
	vert [0] .Alpha   =  0x0000;

	vert [1] .x       =  rtRect.right;
	vert [1] .y       =  rtRect.top;
	vert [1] .Red     =  (m_rgbBgColor & 0x000000FF) << 8;
	vert [1] .Green   =  (m_rgbBgColor & 0x0000FF00);
	vert [1] .Blue    =  (m_rgbBgColor & 0x00FF0000) >> 8;
	vert [1] .Alpha   =  0x0000;

	vert [2] .x       =  rtRect.right;
	vert [2] .y       =  rtRect.bottom; 
	vert [2] .Red     =  (m_rgbBgColor & 0x000000FF) << 8;
	vert [2] .Green   =  (m_rgbBgColor & 0x0000FF00);
	vert [2] .Blue    =  (m_rgbBgColor & 0x00FF0000) >> 8;
	vert [2] .Alpha   =  0x0000;

	vert [3] .x       =  rtRect.left;
	vert [3] .y       =  rtRect.bottom;
	vert [3] .Red     =  0xff00;
	vert [3] .Green   =  0xff00;
	vert [3] .Blue    =  0xff00;
	vert [3] .Alpha   =  0x0000;

	gTri[0].Vertex1   = 0;
	gTri[0].Vertex2   = 1;
	gTri[0].Vertex3   = 2;

	gTri[1].Vertex1   = 0;
	gTri[1].Vertex2   = 2;
	gTri[1].Vertex3   = 3;

	GradientFill(memDC.m_hDC, vert, 4, &gTri, 2, GRADIENT_FILL_TRIANGLE);
#endif

	//if(m_pdlgInstall)
	//{
	//	m_pdlgInstall->Invalidate();
	//}//if

	CDialog::OnPaint();
}

void CMainDlg::OnDestroy()
{
	if(m_pdlgInstall)
	{
		m_pdlgInstall->DestroyWindow();
		delete m_pdlgInstall;
	}//if

	//if(m_bgLogo.IsValid())
	//{
	//	m_bgLogo.Destroy();
	//}//if

	//if(m_bgImage.IsValid())
	//{
	//	m_bgImage.Destroy();
	//}//if

	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

BOOL CMainDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	{
		return TRUE;
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 프로그램의 실행인자를 설정한다. \n
/// @param (CString) strArg : (in) 프로그램의 실행 인자 문자열('+'로 구분...) 
/////////////////////////////////////////////////////////////////////////////////
void CMainDlg::AddArg(CString strArg)
{
	//AfxMessageBox(strArg);
	CString strTok;
	int nPos = 0;
	//'+'를 기준으로 각 실행인자를 분리
	strTok = strArg.Tokenize(_T("+"), nPos);
	while(strTok != _T(""))
	{
		strTok.Trim();
		m_aryArg.Add(strTok);

		strTok = strArg.Tokenize(_T("+"), nPos);		
	}//while
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 실행인자가 설정되었는지 학인한다. \n
/// @param (CString) strArg : (in) 주어진 실행인자가 설정되었는지 확인('+'로 구분...) 
/// @return <형: bool> \n
///			<true: 설정되어 있음> \n
///			<false: 설정되어 있지 않음> \n
/////////////////////////////////////////////////////////////////////////////////
bool CMainDlg::IsSetArg(CString strArg)
{
	if(m_aryArg.GetCount() == 0)
	{
		return false;
	}//if

	strArg.Remove(_T('+'));

	for(int i=0; i<m_aryArg.GetCount(); i++)
	{
		if(strArg.CompareNoCase(m_aryArg.GetAt(i)) == 0)
		{
			return true;
		}//if
	}//for

	return false;
}
void CMainDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(nID == SC_CLOSE)
	{
		return;
	}//if

	CDialog::OnSysCommand(nID, lParam);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 에디션을 반환 \n
/// @return <형: short> \n
///			<제품의 에디션 enum 값> \n
/////////////////////////////////////////////////////////////////////////////////
short CMainDlg::GetEdition()
{
	return m_sEdition;
}
