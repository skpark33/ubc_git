// UBCInstallDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "Winsvc.h"
#include "UBCInstall.h"
#include "UBCInstallDlg.h"
#include "scratchUtil.h"
#include <ci/libDebug/ciDebug.h>
#include "MainDlg.h"
#include "shlwapi.h"
#include "scPath.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(10,"UBCInstall");

#define		SIZE_VNC_EXE	(1386048)
#define		MAX_KEY_LENGTH	(260)
#define		MAX_VALUE_NAME	(260)


//인스톨 목록
static TCHAR* pszInstallList[] = { _T("VNC"),
								_T("CODEC_PACK"),
								//_T("CODEC_PROPERTIES"),
								_T("FLV_SPLITTER"),
								//_T("FLASH_PLAYER"),
								_T("SOFT_DRIVE"),
								_T("SHORT_CUT"),
								_T("FIRE_WALL"),
								_T("WINDOW_TRIVIAL"),
								_T("SECURITY_LEVEL"),
								//_T("LOGMI_IN"),
								//_T("AUTO_SHUTDOWN")
							  };

static TCHAR* pszInstallName[] = { _T("Install Ultra VNC"),
								_T("Install K-lite Codec Pack"),
								//_T("Set Codec Properties"),
								_T("Install FLV Splitter"),
								//_T("Install Flash Player"),
								_T("Set Soft Drive"),
								_T("Create Shortcut"),
								_T("Set Firewall Exception"),
								_T("Disable Windows Trivial"),
								_T("Set Security"),																
								//_T("Install LogMeIn"),
								//_T("Set Auto Suhtdown Time")
							  };

//인스톨 상태
static TCHAR* pszState[] = { _T("Install fail."),
							_T("Not installed."),
							_T("Installed."),
							_T("Checking install state..."),
							_T("Installing..."),
							_T("Optional install."),
							_T("Skiped. (Windows 7 not support)")
							};

//등록하는 필터 들
static TCHAR* pszFilters[] = { _T("ffdshow\\ffdshow.ax"),
							_T("Filters\\DivXDecH264.ax"),
							_T("Filters\\xvid.ax"),
							_T("Filters\\vp7dec.ax"),
							_T("Filters\\ac3file.ax"),
							_T("Filters\\mmamr.ax"),
							_T("Filters\\mmmpcdmx.ax"),
							_T("Filters\\mmmpcdec.ax"),
							_T("Filters\\CoreVorbis.ax"),
							_T("Filters\\WavPackDSDecoder.ax"),
							_T("Filters\\WavPackDSSplitter.ax"),
							_T("Filters\\madFlac.ax"),
							_T("Filters\\MonkeySource.ax"),
							_T("Filters\\CLVSD.ax"),
							_T("Filters\\MpegSplitter.ax"),
							_T("Filters\\FLVSplitter.ax"),
							_T("Filters\\MP4Splitter.ax"),
							_T("Filters\\Haali\\mkx.dll"),
							_T("Filters\\Haali\\ogm.dll"),
							_T("Filters\\Haali\\mp4.dll"),
							_T("Filters\\Haali\\ts.dll"),
							_T("Filters\\Haali\\avi.dll"),
							_T("Filters\\Haali\\splitter.ax"),
							_T("Filters\\Haali\\avs.dll"),
							_T("Filters\\Haali\\dxr.dll"),
							_T("Filters\\vsfilter.dll"),
							_T("Filters\\mmaacd.ax")
							};


// CUBCInstallDlg 대화 상자


CUBCInstallDlg::CUBCInstallDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCInstallDlg::IDD, pParent)
	, m_bVncInstall(false)
	, m_bCodecInstall(false)
	//, m_bSetCodec(false)
	, m_bFlvInstall(false)
	, m_bFlashInstall(false)
	, m_bSetSoftDrive(false)
	, m_bShortCut(false)
	, m_bWindowTrivial(false)
	, m_bSecurity(false)
	, m_bSetFirewall(false)
	//, m_bLogMeInInstall(false)
	//, m_bAutoShutDown(false)
	, m_strIniFileName(_T("UBCInstall.ini"))
	, m_strInstalledVersion(_T(""))
	, m_pfrmLogo(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
/*
	//인스톨 목록
pszInstallList[8] = { _T("VNC"),
								_T("CODEC_PACK"),
								//_T("CODEC_PROPERTIES"),
								_T("FLV_SPLITTER"),
								_T("FLASH_PLAYER"),
								_T("SOFT_DRIVE"),
								_T("SHORT_CUT"),
								_T("WINDOW_TRIVIAL"),
								_T("FIRE_WALL"),
								_T("SECURITY_LEVEL"),
								//_T("LOGMI_IN"),
								//_T("AUTO_SHUTDOWN")
							  };

pszInstallName[9] = { _T("Install Ultra VNC"),
								_T("Install K-lite Codec Pack"),
								//_T("Set Codec Properties"),
								_T("Install FLV Splitter"),
								_T("Install Flash Player"),
								_T("Set Soft Drive"),
								_T("Create Shortcut"),
								_T("Disable Windows Trivial"),
								_T("Set Firewall Exception"),
								_T("Set Security"),																
								//_T("Install LogMeIn"),
								//_T("Set Auto Suhtdown Time")
							  };

//인스톨 상태
pszState[6] = { _T("Install fail."),
							_T("Not installed."),
							_T("Installed."),
							_T("Checking install state..."),
							_T("Installing..."),
							_T("Optional install."),
							_T("Skiped.")
							};

//등록하는 필터 들
pszFilters[27] = { _T("ffdshow\\ffdshow.ax"),
							_T("Filters\\DivXDecH264.ax"),
							_T("Filters\\xvid.ax"),
							_T("Filters\\vp7dec.ax"),
							_T("Filters\\ac3file.ax"),
							_T("Filters\\mmamr.ax"),
							_T("Filters\\mmmpcdmx.ax"),
							_T("Filters\\mmmpcdec.ax"),
							_T("Filters\\CoreVorbis.ax"),
							_T("Filters\\WavPackDSDecoder.ax"),
							_T("Filters\\WavPackDSSplitter.ax"),
							_T("Filters\\madFlac.ax"),
							_T("Filters\\MonkeySource.ax"),
							_T("Filters\\CLVSD.ax"),
							_T("Filters\\MpegSplitter.ax"),
							_T("Filters\\FLVSplitter.ax"),
							_T("Filters\\MP4Splitter.ax"),
							_T("Filters\\Haali\\mkx.dll"),
							_T("Filters\\Haali\\ogm.dll"),
							_T("Filters\\Haali\\mp4.dll"),
							_T("Filters\\Haali\\ts.dll"),
							_T("Filters\\Haali\\avi.dll"),
							_T("Filters\\Haali\\splitter.ax"),
							_T("Filters\\Haali\\avs.dll"),
							_T("Filters\\Haali\\dxr.dll"),
							_T("Filters\\vsfilter.dll"),
							_T("Filters\\mmaacd.ax")
							};

*/

}

void CUBCInstallDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_LOGO_STATIC, m_staticLogo);
	DDX_Control(pDX, IDC_INSTALL_LIST, m_listInstall);
	DDX_Control(pDX, IDOK, m_btnInstall);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Text(pDX, IDC_INSTALLED_STATIC, m_strInstalledVersion);
	DDX_Control(pDX, IDC_LOGO_FRAME, m_staticLogo);
}

BEGIN_MESSAGE_MAP(CUBCInstallDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	//ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDOK, &CUBCInstallDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CUBCInstallDlg::OnBnClickedCancel)
	ON_MESSAGE(WM_COMPLETE_INSTALL, OnCompleteInstall)
	ON_MESSAGE(WM_COMPLETE_CHECK, OnCompleteCheck)
	ON_MESSAGE(WM_CHECKED_COUNT, OnCheckedCount)
	ON_BN_CLICKED(ID_LICENCE_BTN, &CUBCInstallDlg::OnBnClickedLicenceBtn)
END_MESSAGE_MAP()


// CUBCInstallDlg 메시지 처리기

LRESULT CUBCInstallDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	static BOOL bFlag = FALSE;
 
    switch(message)
    {
    case WM_NCLBUTTONDOWN:    // 넌 클라이언트 영역(캡션바)을 클릭했다면
			bFlag = TRUE;
    case WM_NCMOUSEMOVE:      // 넌 클라이언트 영역을 클릭하고 있는 상태에서 이동한다면
        if(bFlag)
        {
            bFlag = FALSE;
 
            return TRUE;      // 디폴트 윈도우 프로시저로 넘기지 않고 정상적인 처리로 리턴
        }//if
        return 0;
    }//switch

	return CDialog::WindowProc(message, wParam, lParam);
}


BOOL CUBCInstallDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	/*ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}*/

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	CRect rtLogo;
	::GetClientRect(m_staticLogo.GetSafeHwnd(), &rtLogo);
	m_pfrmLogo = new CLogoFrame;
	m_pfrmLogo->Create(NULL, _T("Logo"), WS_CHILD | WS_VISIBLE, rtLogo, &m_staticLogo, 3456);

	CString strVal = GetINIValue(m_strIniFileName, _T("ROOT"), _T("InstalledVersion"));
	if(strVal.GetLength() == 0)
	{
		m_strInstalledVersion.Format(_T("Not Installed"));
	}
	else
	{
		m_strInstalledVersion.Format(_T("Installed Package Ver. : %s"), strVal);
	}//if

	UpdateData(FALSE);

	//Install factory의 화면을 최소화 한다.
	HWND hWnd = ::FindWindow(NULL, _T("Installation of UBC"));
	if(hWnd)
	{
		HWND hPWnd = ::GetParent(hWnd);
		if(hPWnd)
		{
			::ShowWindow(hPWnd, SW_SHOWMINIMIZED);
		}
		else
		{
			::ShowWindow(hWnd, SW_SHOWMINIMIZED);
		}//if
	}//if

	//ini 파일을 확인, 초기화 한다.
	CheckIniFile();

	CreateList();
	//CheckInstall();
	CWinThread* pThread = ::AfxBeginThread(CUBCInstallDlg::ThreadCheckInstall, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;

	m_btnInstall.EnableWindow(FALSE);
	m_btnCancel.EnableWindow(FALSE);

	//C 드라이브가 아닌곳을 대응하기 위하여 소프트 링크를 건다.
	//if(!scPath::getInstance()->playerLink())
	//{
	//	ciDEBUG(1, ("Fail to playerLink"));
	//}//if

	pThread->ResumeThread();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCInstallDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if(nID == SC_CLOSE)
	{
		return;
	}//if

	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
	/*	CAboutDlg dlgAbout;
		dlgAbout.DoModal();*/
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}


BOOL CUBCInstallDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	{
		return TRUE;
	}//if

	return CDialog::PreTranslateMessage(pMsg);
}


// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCInstallDlg::OnPaint()
{
	CDialog::OnPaint();

	//CPaintDC dc(this);
	//if(m_bgLogo.IsValid())
	//{
	//	CMemDC memDC(&dc);
	//	CRect rtLogo;
	//	GetClientRect(&rtLogo);
	//	m_bgLogo.Draw2(memDC.m_hDC, rtLogo.left, rtLogo.top);
	//}//if
/*
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
		//m_frmLogo.Invalidate();
	}
*/
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
//HCURSOR CUBCInstallDlg::OnQueryDragIcon()
//{
//	return static_cast<HCURSOR>(m_hIcon);
//}


void CUBCInstallDlg::OnDestroy()
{
	if(m_pfrmLogo)
	{
		m_pfrmLogo->DestroyWindow();
		delete m_pfrmLogo;
	}//if

	scratchUtil::clearInstance();
	//m_ftFont.DeleteObject();

	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


LRESULT CUBCInstallDlg::OnCompleteInstall(WPARAM wParam, LPARAM lParam)
{
	int nChecked = 0;
	int nCount = m_listInstall.GetItemCount();
	for(int nItem = 0; nItem < nCount; nItem++)
	{
		if(ListView_GetCheckState(m_listInstall.GetSafeHwnd(), nItem))
		{
			nChecked++;
		}//if
	}//for

	if(nChecked == 0)
	{
		m_btnInstall.EnableWindow(FALSE);
	}
	else
	{
		m_btnInstall.EnableWindow(TRUE);
	}//if
	
	m_btnCancel.EnableWindow(TRUE);

	Invalidate(FALSE);

	return 0;
}


LRESULT CUBCInstallDlg::OnCompleteCheck(WPARAM wParam, LPARAM lParam)
{
	int nChecked = 0;
	int nCount = m_listInstall.GetItemCount();
	for(int nItem = 0; nItem < nCount; nItem++)
	{
		if(ListView_GetCheckState(m_listInstall.GetSafeHwnd(), nItem))
		{
			nChecked++;
		}//if
	}//for

	if(nChecked == 0)
	{
		m_btnInstall.EnableWindow(FALSE);
	}
	else
	{
		m_btnInstall.EnableWindow(TRUE);
		m_btnInstall.SetFocus();
	}//if
	
	m_btnCancel.EnableWindow(TRUE);

	Invalidate(FALSE);

	return 0;
}


LRESULT CUBCInstallDlg::OnCheckedCount(WPARAM wParam, LPARAM lParam)
{
	if(lParam == 0)
	{
		m_btnInstall.EnableWindow(FALSE);
	}
	else
	{
		m_btnInstall.EnableWindow(TRUE);
	}//if

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// UBCInstall.ini파일을 확인하고 없다면 초기화 한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckIniFile()
{
	CString strValue, strPath;

	//ROOT
	strValue = GetINIValue(m_strIniFileName, _T("ROOT"), _T("InstallList"));
	if(strValue.IsEmpty())
	{
		CString strList;
		for(int i=0; i<E_STEP_MAX; i++)
		{
			strList += pszInstallList[i];
			strList += _T(",");
		}//for
		strList.TrimRight(_T(","));
		WriteINIValue(m_strIniFileName, _T("ROOT"), _T("InstallList"), strList);
	}//if

	strValue = GetINIValue(m_strIniFileName, _T("ROOT"), _T("FirstInstall"));
	if(strValue.IsEmpty())
	{
		WriteINIValue(m_strIniFileName, _T("ROOT"), _T("FirstInstall"), _T(""));
	}//if

	strValue = GetINIValue(m_strIniFileName, _T("ROOT"), _T("LastInstall"));
	if(strValue.IsEmpty())
	{
		WriteINIValue(m_strIniFileName, _T("ROOT"), _T("LastInstall"), _T(""));
	}//if

	for(int i=0; i<E_STEP_MAX; i++)
	{
		strValue = GetINIValue(m_strIniFileName, pszInstallList[i], _T("InstallState"));
		if(strValue.IsEmpty())
		{
			WriteINIValue(m_strIniFileName, pszInstallList[i], _T("InstallState"), _T(""));
		}//if

		//strValue = GetINIValue(m_strIniFileName, pszInstallList[i], _T("Name"));
		//if(strValue.IsEmpty())
		//{
		//	WriteINIValue(m_strIniFileName, pszInstallList[i], _T("Name"), _T(""));
		//}//if

		strValue = GetINIValue(m_strIniFileName, pszInstallList[i], _T("Version"));
		if(strValue.IsEmpty())
		{
			WriteINIValue(m_strIniFileName, pszInstallList[i], _T("Version"), _T(""));
		}//if

		strValue = GetINIValue(m_strIniFileName, pszInstallList[i], _T("Path"));
		if(strValue.IsEmpty())
		{
			switch(i)
			{
			case E_INSTALL_VNC:
				{
					strPath.Format(_T("%sUltraVNC\\"), GetProgramDir()); 
				}
				break;

			case E_INSTALL_CODEC:
				{
					strPath.Format(_T("%sK-Lite Codec Pack\\"), GetProgramDir());
				}
				break;
			case E_INSTALL_FLVSPLITTER:
				{
					strPath.Format(_T("%sGNU\\FLVSPLITTER\\"), GetProgramDir());
				}
				break;
/*
			case E_INSTALL_FLASHPLAYER:
				{
					strPath.Format(_T("%sMacromed\\Flash\\"), GetSystemDir());
				}
				break;
*/

			//case E_SET_CODECPROPERTIES:
			case E_SET_SFTDRIVE:
			case E_CREATE_SHORTCUT:
			case E_SET_SECURITY:
			case E_DISABLE_WINDOWTRIVIAL: strPath = _T(""); break;
			//case E_SET_FIREWALLEXCEPTION:
			//case E_SET_AUTOSHUTDOWN: strPath = _T(""); break;

			//case E_INSTALL_LOGMEIN: strPath = _T("3rdparty\\LogMeIn"); break;
			}//switch

			WriteINIValue(m_strIniFileName, pszInstallList[i], _T("Path"), strPath);
		}//if
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소스의 모든 파일을 타겟의 위치로 복사한다. \n
/// @param (CString) strSrc : (in) 원본파일들이 있는 경로
/// @param (CString) strTarget : (in) 파일들을 복사하려는 대상 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::CopyFiles(CString strSrc, CString strTarget)
{
	//SHFILEOPSTRUCT의 경로에 끝에는 NULL이 두개 들어가므로...
	TCHAR szSrc[MAX_PATH+1] = { 0x00 };
	TCHAR szTarget[MAX_PATH+1] = { 0x00 };
	strcpy(szSrc, strSrc);
	strcpy(szTarget, strTarget);

	SHFILEOPSTRUCT stFileOps;
	memset(&stFileOps, 0x00, sizeof(SHFILEOPSTRUCT));
	stFileOps.hwnd = this->m_hWnd;
	stFileOps.wFunc = FO_COPY;
	stFileOps.pFrom = szSrc;
	stFileOps.pTo = szTarget;
	stFileOps.fFlags = FOF_NOCONFIRMATION | FOF_NOCONFIRMMKDIR | FOF_SIMPLEPROGRESS;
	stFileOps.fAnyOperationsAborted = FALSE;

	ciDEBUG(1, ("Copy files : %s ==> %s", szSrc, szTarget));
	if(SHFileOperation(&stFileOps) == 0)
	{
		ciDEBUG(1, ("Success copy files : %s ==> %s", szSrc, szTarget));
		return true;
	}//if

	ciDEBUG(1, ("Fail copy files : %s ==> %s", szSrc, szTarget));
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// *.reg 파일을 regedit를 통하여 등록해준다. \n
/// @param (CString) strFile : (in) 등록하려는 reg 파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::RegistFile(CString strFile)
{
	CString strCmd;
	//strCmd.Format(_T("%s%s"), PATH_REGEDIT, strFile);
	strCmd.Format(_T("regedit /s \"%s\""), strFile);

	ciDEBUG(1, ("Run regedit : %s", strCmd));

	STARTUPINFO stStartupInfo = {0}; 
	stStartupInfo.cb = sizeof(STARTUPINFO);
	PROCESS_INFORMATION stProcessInfo; 
	BOOL bRet =  CreateProcess(NULL,
								(LPSTR)(LPCSTR)strCmd,
								NULL,
								NULL,
								FALSE,
								0,
								NULL,
								NULL,
								&stStartupInfo,
								&stProcessInfo);
/*
	STARTUPINFO stStartupInfo = {0}; 
	stStartupInfo.cb = sizeof(STARTUPINFO);
	PROCESS_INFORMATION stProcessInfo; 
	BOOL bRet =  CreateProcess(strCmd,
								NULL,
								NULL,
								NULL,
								FALSE,
								0,
								NULL,
								GetAppPath(),
								&stStartupInfo,
								&stProcessInfo);
*/
	if(!bRet)
	{
		if(!system(strCmd))
		{
			bRet = TRUE;
		}//if
	}
	else
	{
		CloseHandle(stProcessInfo.hProcess);
		CloseHandle(stProcessInfo.hThread);
	}//if

	if(bRet)
	{

		ciDEBUG(1, ("Success regedit : %s", strFile));
	}
	else
	{
		ciDEBUG(1, ("Fail regedit : %s", strFile));
	}//if

	return (bool)bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// regsvr32를 통하여 컴포넌트를 시스템에 등록한다. \n
/// @param (CString) strCom : (in) 등록하려는 컴포넌트 파일의 경로
/// @param (bool) bRegistServer : (in) 서버에 등록하는 옵션의 사용 여부
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::RegistComponent(CString strCom, bool bRegistServer)
{
	CString strCmd;
	if(bRegistServer)
	{
		strCmd.Format(_T("regsvr32 /s \"%s\""), strCom);
	}
	else
	{
		strCmd.Format(_T("regsvr32 /s /u \"%s\""), strCom);
	}//if

	ciDEBUG(1, ("Regsvr32 : %s", strCom));

	STARTUPINFO stStartupInfo = {0}; 
	stStartupInfo.cb = sizeof(STARTUPINFO);
	PROCESS_INFORMATION stProcessInfo; 
	BOOL bRet =  CreateProcess(NULL,
								(LPSTR)(LPCSTR)strCmd,
								NULL,
								NULL,
								FALSE,
								0,
								NULL,
								NULL,
								&stStartupInfo,
								&stProcessInfo);
	if(!bRet)
	{
		if(!system(strCmd))
		{
			bRet = TRUE;
		}//if
	}
	else
	{
		CloseHandle(stProcessInfo.hProcess);
		CloseHandle(stProcessInfo.hThread);
	}//if

	if(bRet)
	{
		ciDEBUG(1, ("Success regsvr32 : %s", strCom));
	}
	else
	{
		ciDEBUG(1, ("Fail regsvr32 : %s", strCom));
	}//if


	return (bool)bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// CMD 명령을 실행 \n
/// @param (CString) strCmd : (in) 실행할 명령어
/// @param (CString) strApp : (in) 실행할 프로그램
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::RunCmd(CString strCmd, CString strApp)
{
	ciDEBUG(1, ("Run cmd : %s %s", strApp, strCmd));

	CString strExcute;
	LPTSTR lpstrArg = NULL;
	if(strApp.GetLength() == 0)
	{
		strExcute = strCmd;
	}
	else
	{
		strExcute = strApp;
		lpstrArg = (LPSTR)(LPCTSTR)strCmd;
	}//if

	STARTUPINFO stStartupInfo = {0}; 
	stStartupInfo.cb = sizeof(STARTUPINFO);
	PROCESS_INFORMATION stProcessInfo; 
	BOOL bRet =  CreateProcess(strExcute,
								(LPSTR)(LPCTSTR)lpstrArg,
								NULL,
								NULL,
								FALSE,
								0,
								NULL,
								GetAppPath(),
								&stStartupInfo,
								&stProcessInfo);
	if(!bRet)
	{
		if(!system(strCmd))
		{
			bRet = TRUE;
		}//if
	}
	else
	{
		CloseHandle(stProcessInfo.hProcess);
		CloseHandle(stProcessInfo.hThread);
	}//if

	if(bRet)
	{
		ciDEBUG(1, ("Success run cmd : %s %s", strApp, strCmd));
	}
	else
	{
		ciDEBUG(1, ("Fail run cmd : %s %s", strApp, strCmd));
	}//if


	return (bool)bRet;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 주어진 경로의 폴더를 만든다 \n
/// @param (LPCTSTR) lpszPath : (in) 생성하려는 폴더 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::CreatePathDirectory(LPCTSTR lpszPath)
{
    TCHAR szPathBuffer[MAX_PATH];

    size_t len = _tcslen(lpszPath);

    for(size_t i = 0; i < len; i++ )
    {
        szPathBuffer[i] = *(lpszPath + i);
        if(szPathBuffer[i] == _T('\\') || szPathBuffer[i] == _T('/'))
        {
            szPathBuffer[i + 1] = NULL;
            if(!PathFileExists(szPathBuffer))
            {
                if(!::CreateDirectory(szPathBuffer, NULL))
                {
                    if(GetLastError() != ERROR_ALREADY_EXISTS)
					{
                        return false;
					}//if
                }//if
            }//if
        }//if
    }//for

    return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 인스톨 과정을 시작한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::StartInstall(void)
{
	ciDEBUG(1, ("Start install"));

	m_btnInstall.EnableWindow(FALSE);
	m_btnCancel.EnableWindow(FALSE);

	CTime tmNow = CTime::GetCurrentTime();
	CString strNow = tmNow.Format(_T("%Y-%M-%d %H:%M%S"));
	CString strVal = GetINIValue(m_strIniFileName, _T("ROOT"), _T("FirstInstall"));
	if(strVal.GetLength() == 0)
	{
		WriteINIValue(m_strIniFileName, _T("ROOT"), _T("FirstInstall"), strNow);
	}//if
	WriteINIValue(m_strIniFileName, _T("ROOT"), _T("LastInstall"), strNow);

	InstallVnc();				
	InstallCodec();				
	//SetCodecPoperties();		
	InstallFlvSplitter();
	//InstallFlashPlayer();
	SetSoftDrive();				
	CreateShortCut();
	DisableWindowTrivial();
	SetFirewallException();		
	SetSecurity();
	//InstallLogMeIn();			
	//SetAutoShutdownTime();

	ciDEBUG(1, ("End install"));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 인스톨이 되어 있는 상태를 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckInstall(void)
{
	ciDEBUG(1, ("Start check install state"));

	//윈도우 호환성 레지스트리 추가
	if(GetOSVersion() > E_OS_WINXP)
	{
		HKEY hKey;
		LONG lRet;
		DWORD dwLen = 1024;
		DWORD dwType = 1;
		CString strKey = _T("Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Compatibility Assistant\\Persisted");
		CString strName = GetAppPath();
		strName += theApp.m_pszAppName;
		strName += _T(".exe");

		lRet = RegOpenKeyEx(HKEY_CURRENT_USER, strKey, 0 ,KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegSetValueEx(hKey, strName, 0, REG_DWORD, (BYTE*)&dwType, sizeof(DWORD));
			if(lRet != ERROR_SUCCESS)
			{
				ciDEBUG(1, ("Fail to set registry : Program Compatibility Assistant"));
			}
			else
			{
				ciDEBUG(1, ("Success set registry : Program Compatibility Assistant"));
			}//if
		}//if
	}//if

	CheckInstallVnc();			
	CheckInstallCodec();		
	//CheckSetCodecProperties();	
	CheckInstallFlv();
	//CheckInstallFlashPlayer();
	CheckSetSoftDrive();		
	CheckCreateShortCut();
	CheckDisableWindowTrivial();
	CheckSetFirewallException();
	CheckSetSecurity();
	//CheckInstallLogMeIn();
	//CheckSetAutoShutDownTime();

	ciDEBUG(1, ("End check install state"));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 인스톨 과정을 종료한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::EndInstall(void)
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 리스트 컨트롤을 생성한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CreateList(void)
{
	CRect rtClient;
	m_listInstall.GetClientRect(&rtClient);
	int nIdx = 0;
	LISTCTRL_COLUMN_INFO info[COLUMN_COUNT] = {
		//{ "", idx, width, align },
		/*{ _T("Install list"),	0,  (rtClient.Width()/10)*4, LVCFMT_LEFT },
		{ _T("Progress"),		1,	(rtClient.Width()/10)*2, LVCFMT_CENTER },
		{ _T("State"),			2,  (rtClient.Width()/10)*4+4, LVCFMT_LEFT },*/
		{ _T("Install list"),	0,  (rtClient.Width()/100)*42, LVCFMT_LEFT },
		{ _T("Progress"),		1,	(rtClient.Width()/100)*21, LVCFMT_CENTER },
		{ _T("State"),			2,  (rtClient.Width()/100)*51, LVCFMT_LEFT },
	};

	for(int i=0; i<COLUMN_COUNT; i++)
	{
		m_listInstall.InsertColumn(i, info[i].text, info[i].align, info[i].width);
	}//for	

	for(int i=0; i<E_STEP_MAX; i++)
	{
		m_listInstall.InsertItem(i, pszInstallName[i]);
		//m_listInstall.SetItemText(i, j++, _T("0%"));
		m_listInstall.SetItemText(i, 2, _T(""));
		m_listInstall.SetItemData(i, 0);
	}//for

	m_listInstall.InitProgressColumn(1);
	m_listInstall.SetExtendedStyle(LVS_EX_SUBITEMIMAGES | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 리스트 컨트롤에 작업상태를 설정한다. \n
/// @param (int) nIdx : (in) 설정하려는 인덱스
/// @param (int) nState : (in) 설치 상태
/// @param (int) nProgress : (in) 설치 진행율
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::SetInstallProgress(int nIdx, int nState, int nProgress)
{
	CString strResult;
	switch(nState)
	{
	case E_STATE_CHECKING:
		{
			//m_listInstall.SetRowColors(nIdx, RGB_CHECKING_BG, RGB_CHECKING_FG);
			m_listInstall.SetCellColors(nIdx, 0, RGB_CHECKING_BG, RGB(0, 0, 0));
			m_listInstall.SetCellColors(nIdx, 2, RGB_CHECKING_BG, RGB_CHECKING_FG);
		}
		break;
	case E_STATE_INSTALLING:
		{
			//m_listInstall.SetRowColors(nIdx, RGB_INSTALLING_BG, RGB_INSTALLING_FG);
			m_listInstall.SetCellColors(nIdx, 0, RGB_INSTALLING_BG, RGB(0, 0, 0));
			m_listInstall.SetCellColors(nIdx, 2, RGB_INSTALLING_BG, RGB_INSTALLING_FG);
		}
		break;
	case E_STATE_INSTALLED:
		{
			//m_listInstall.SetRowColors(nIdx, RGB_INSTALLED_BG, RGB_INSTALLED_FG);
			m_listInstall.SetCellColors(nIdx, 0, RGB_INSTALLED_BG, RGB(0, 0, 0));
			m_listInstall.SetCellColors(nIdx, 2, RGB_INSTALLED_BG, RGB_INSTALLED_FG);
			ListView_SetCheckState(m_listInstall.GetSafeHwnd(), nIdx, FALSE);
			nProgress = 100;
			strResult.Format(_T("%d"), E_STATE_INSTALLED);
			WriteINIValue(m_strIniFileName, pszInstallList[nIdx], _T("InstallState"), strResult);
		}
		break;
	case E_STATE_NOTINSTALLED:
		{
			//m_listInstall.SetRowColors(nIdx, RGB_NOTINSTALLED_BG, RGB_NOTINSTALLED_FG);
			m_listInstall.SetCellColors(nIdx, 0, RGB_NOTINSTALLED_BG, RGB(0, 0, 0));
			m_listInstall.SetCellColors(nIdx, 2, RGB_NOTINSTALLED_BG, RGB_NOTINSTALLED_FG);
			ListView_SetCheckState(m_listInstall.GetSafeHwnd(), nIdx, TRUE);
			nProgress = 0;
			strResult.Format(_T("%d"), E_STATE_NOTINSTALLED);
			WriteINIValue(m_strIniFileName, pszInstallList[nIdx], _T("InstallState"), strResult);
		}
		break;
	case E_STATE_INSTALL_FAIL:
		{
			m_listInstall.SetCellColors(nIdx, 0, RGB_INSTALL_FAIL_BG, RGB(0, 0, 0));
			m_listInstall.SetCellColors(nIdx, 2, RGB_INSTALL_FAIL_BG, RGB_INSTALL_FAIL_FG);
			ListView_SetCheckState(m_listInstall.GetSafeHwnd(), nIdx, TRUE);
			nProgress = 0;
			strResult.Format(_T("%d"), E_STATE_INSTALL_FAIL);
			WriteINIValue(m_strIniFileName, pszInstallList[nIdx], _T("InstallState"), strResult);
		}
		break;
	case E_STATE_OPTIONAL:
		{
			m_listInstall.SetCellColors(nIdx, 0, RGB_OPTIONAL_BG, RGB(0, 0, 0));
			m_listInstall.SetCellColors(nIdx, 2, RGB_OPTIONAL_BG, RGB_OPTIONAL_FG);
			ListView_SetCheckState(m_listInstall.GetSafeHwnd(), nIdx, FALSE);
			nProgress = 0;
			strResult.Format(_T("%d"), E_STATE_OPTIONAL);
			WriteINIValue(m_strIniFileName, pszInstallList[nIdx], _T("InstallState"), strResult);
		}
		break;
	case E_STATE_SKIPED:
		{
			m_listInstall.SetCellColors(nIdx, 0, RGB_SKIPED_BG, RGB(0, 0, 0));
			m_listInstall.SetCellColors(nIdx, 2, RGB_SKIPED_BG, RGB_SKIPED_FG);
			ListView_SetCheckState(m_listInstall.GetSafeHwnd(), nIdx, FALSE);
			nProgress = 0;
			strResult.Format(_T("%d"), E_STATE_SKIPED);
			WriteINIValue(m_strIniFileName, pszInstallList[nIdx], _T("InstallState"), strResult);
		}
		break;
	default:
		{
			m_listInstall.SetRowColors(nIdx, RGB(255, 255, 255), RGB(0, 0, 0));
		}
	}//switch

	//m_listInstall.SetItemText(nIdx, 1, pszState[nState]);
	m_listInstall.SetItemText(nIdx, 2, pszState[nState+1]);
	m_listInstall.SetItemData(nIdx, nProgress);
	Invalidate(FALSE);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VNC가 설치되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckInstallVnc(void)
{
	ciDEBUG(1, ("Start : CheckInstallVnc"));

	SetInstallProgress(E_INSTALL_VNC, E_STATE_CHECKING);
	//설치 경로에 winvnc.exe가 있는지 검사한다.

	m_bVncInstall = false;
	
	CFileStatus fs;
	CString strVncPath;
	strVncPath = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_VNC], _T("Path"));
	if(strVncPath.GetLength() == 0)
	{
		strVncPath.Format(_T("%sUltraVNC\\"), GetProgramDir());
	}//if
	strVncPath += _T("winvnc.exe");

	SetInstallProgress(E_INSTALL_VNC, E_STATE_CHECKING, 40);

	if(CFile::GetStatus(strVncPath, fs) == TRUE)
	{
		//m_bVncInstall = true;
		//레지스트리에 VNC 서비스가 등록되어있는지 검사
		HKEY hKey;
		LONG lRet;
		CString szBuf;
		DWORD dwSize = sizeof(DWORD);
		DWORD dwValue;
		DWORD dwType = REG_DWORD;

		//Start 값이 2 인지 확인
		lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services\\uvnc_service"), 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegQueryValueEx(hKey, _T("Start"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
			if(lRet == ERROR_SUCCESS)
			{
				if(dwValue == 2)
				{
					m_bVncInstall = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong VNC service start value : %d", dwValue));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg query fail : uvnc_service"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : uvnc_service"));
		}//if
		RegCloseKey(hKey);
	}
	else
	{
		ciDEBUG(1, ("Can't found VNC file : %s", strVncPath));
	}//if

	SetInstallProgress(E_INSTALL_VNC, E_STATE_CHECKING, 80);

	if(!m_bVncInstall)
	{
		if( CMainDlg::m_bInstallVNC )
			SetInstallProgress(E_INSTALL_VNC, E_STATE_NOTINSTALLED);
		else
			SetInstallProgress(E_INSTALL_VNC, E_STATE_OPTIONAL);
	}
	else
	{
		SetInstallProgress(E_INSTALL_VNC, E_STATE_INSTALLED);
	}//if

	ciDEBUG(1, ("End : CheckInstallVnc"));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Codec이 설치되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckInstallCodec(void)
{
	ciDEBUG(1, ("Start : CheckInstallCodec"));

	SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING);

	//설치 경로에 ffdshow.ax 파일이 있는지 확인한다.
	CFileStatus fs;
	CString strFilterPath[2]; // 0=old_ver(ffdshow), 1=new_ver(lavfilter)
	strFilterPath[0] = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_CODEC], _T("Path"));
	strFilterPath[1] = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_CODEC], _T("Path"));
	if(strFilterPath[0].GetLength() == 0)
	{
		strFilterPath[0].Format(_T("%sK-Lite Codec Pack\\"), GetProgramDir());
		strFilterPath[1].Format(_T("%sK-Lite Codec Pack\\"), GetProgramDir());
	}//if
	strFilterPath[0] += _T("ffdshow\\ffdshow.ax");
	strFilterPath[1] += _T("Filters\\LAV\\LAVVideo.ax");

	if(CFile::GetStatus(strFilterPath[0], fs) == TRUE) // old_ver(ffdshow)
	{
		ciDEBUG(1, ("find ffdshow codec"));

		bool bFfdShow = false;
		bool bFfdShow_Audio = false;
		bool bHaali = false;

		//레지스트리에서 ffdshow video, audio, Haali splitter의 Tray Icon이 숨겨지도록 되어있어야함.
		HKEY hKey;
		LONG lRet;
		CString szBuf;
		DWORD dwSize = sizeof(DWORD);
		DWORD dwValue;
		DWORD dwType = REG_DWORD;

		//ffdshow
		lRet = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\GNU\\ffdshow"), 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegQueryValueEx(hKey, _T("trayIcon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
			if(lRet == ERROR_SUCCESS)
			{
				if(dwValue == 0)	//Hide 상태
				{
					bFfdShow = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong ffdshow trayicon state : %d", dwValue));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg query fail : ffdshow"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : ffdshow"));
		}//if
		RegCloseKey(hKey);

		SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING, 30);

		//ffdshow_audio
		lRet = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\GNU\\ffdshow_audio"), 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegQueryValueEx(hKey, _T("trayIcon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
			if(lRet == ERROR_SUCCESS)
			{
				if(dwValue == 0)	//Hide 상태
				{
					bFfdShow_Audio = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong ffdshow_audio trayicon state : %d", dwValue));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg query fail : ffdshow_audio"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : ffdshow_audio"));
		}//if
		RegCloseKey(hKey);

		SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING, 60);

		//Haali
		lRet = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Haali\\Matroska Splitter"), 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegQueryValueEx(hKey, _T("ui.trayicon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
			if(lRet == ERROR_SUCCESS)
			{
				if(dwValue == 0)	//Hide 상태
				{
					bHaali = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong Haali\\Matroska Splitter ui.trayicon state : %d", dwValue));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg query fail : Haali\\Matroska Splitter"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : Haali\\Matroska Splitter"));
		}//if
		RegCloseKey(hKey);

		SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING, 90);

		if(bFfdShow && bFfdShow_Audio && bHaali)
		{
			m_bCodecInstall = true;
			SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLED);
		}
		else
		{
			m_bCodecInstall = false;
			SetInstallProgress(E_INSTALL_CODEC, E_STATE_NOTINSTALLED);
		}//if
	}
	else if(CFile::GetStatus(strFilterPath[1], fs) == TRUE) // new_ver(lavfilter)
	{
		ciDEBUG(1, ("find lav codec"));

		bool bLAV_video = false;
		bool bLAV_audio = false;
		bool bLAV_splitter = false;

		//레지스트리에서 lav video,audio,splitter의 Tray Icon이 숨겨지도록 되어있어야함.
		HKEY hKey;
		LONG lRet;
		CString szBuf;
		DWORD dwSize = sizeof(DWORD);
		DWORD dwValue;
		DWORD dwType = REG_DWORD;

		//ffdshow
		lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\LAV\\Video"), 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegQueryValueEx(hKey, _T("TrayIcon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
			if(lRet == ERROR_SUCCESS)
			{
				if(dwValue == 0)	//Hide 상태
				{
					bLAV_video = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong LAV-video trayicon state : %d", dwValue));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg query fail : LAV-video"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : LAV-video"));
		}//if
		RegCloseKey(hKey);

		SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING, 30);

		//ffdshow_audio
		lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\LAV\\Audio"), 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegQueryValueEx(hKey, _T("TrayIcon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
			if(lRet == ERROR_SUCCESS)
			{
				if(dwValue == 0)	//Hide 상태
				{
					bLAV_audio = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong LAV-audio trayicon state : %d", dwValue));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg query fail : LAV-audio"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : LAV-audio"));
		}//if
		RegCloseKey(hKey);

		SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING, 60);

		//Haali
		lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\LAV\\Splitter"), 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegQueryValueEx(hKey, _T("TrayIcon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
			if(lRet == ERROR_SUCCESS)
			{
				if(dwValue == 0)	//Hide 상태
				{
					bLAV_splitter = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong LAV-splitter trayicon state : %d", dwValue));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg query fail : LAV-splitter"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : LAV-splitter"));
		}//if
		RegCloseKey(hKey);

		SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING, 90);

		if(bLAV_video && bLAV_audio && bLAV_splitter)
		{
			m_bCodecInstall = true;
			SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLED);
		}
		else
		{
			m_bCodecInstall = false;
			SetInstallProgress(E_INSTALL_CODEC, E_STATE_NOTINSTALLED);
		}//if
	}
	else
	{
		ciDEBUG(1, ("Can't found codec file : %s", strFilterPath));
	}//if

/*
	m_bCodecInstall = false;

	HKEY hKey;
	LONG lRet;
	CString szBuf;
	DWORD dwIndex = 0;
	DWORD dwValue;
	TCHAR szValue[1024];

	//Codec
	RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), 0, KEY_ALL_ACCESS, &hKey);
	do{
		dwValue = sizeof(szValue);
		ZeroMemory(szValue, dwValue);
		lRet = RegEnumKeyEx(hKey, dwIndex++, szValue, &dwValue, NULL, NULL, NULL, NULL);
		
		szBuf = szValue;
		szBuf.MakeLower();

		if(szBuf.Find(_T("klitecodec")) != -1)
		{
			m_bCodecInstall = true;
		}//if
	}while(lRet == ERROR_SUCCESS);
	RegCloseKey(hKey);

	SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING, 50);

	//신 버전 확인
	if(!m_bCodecInstall)
	{
		m_bCodecInstall = atoi(GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_CODEC], _T("InstallState")));
	}//if

	SetInstallProgress(E_INSTALL_CODEC, E_STATE_CHECKING, 80);

	if(m_bCodecInstall)
	{
		SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_INSTALL_CODEC, E_STATE_NOTINSTALLED);
	}//if
*/

	ciDEBUG(1, ("End : CheckInstallCodec"));
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Codec Properties가 설정되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckSetCodecProperties(void)
{
	SetInstallProgress(E_SET_CODECPROPERTIES, E_STATE_CHECKING);
	//레지스트리에서 ffdshow video, audio, Haali splitter의 Tray Icon이 숨겨지도록 되어있어야함.

	HKEY hKey;
	LONG lRet;
	CString szBuf;
	DWORD dwSize;
	DWORD dwValue;
	DWORD dwType = REG_DWORD;
	bool bFfdShow = false;
	bool bFfdShow_Audio = false;
	bool bHaali = false;
	bool bFlv = false;

	//ffdshow
	lRet = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\GNU\\ffdshow"), 0, KEY_ALL_ACCESS, &hKey);
	if(lRet == ERROR_SUCCESS)
	{
		lRet = RegQueryValueEx(hKey, _T("trayIcon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
		if(lRet == ERROR_SUCCESS)
		{
			if(dwValue == 0)	//Hide 상태
			{
				bFfdShow = true;
			}//if
		}//if
	}//if
	RegCloseKey(hKey);

	SetInstallProgress(E_SET_CODECPROPERTIES, E_STATE_CHECKING, 30);

	//ffdshow_audio
	lRet = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\GNU\\ffdshow_audio"), 0, KEY_ALL_ACCESS, &hKey);
	if(lRet == ERROR_SUCCESS)
	{
		lRet = RegQueryValueEx(hKey, _T("trayIcon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
		if(lRet == ERROR_SUCCESS)
		{
			if(dwValue == 0)	//Hide 상태
			{
				bFfdShow_Audio = true;
			}//if
		}//if
	}//if
	RegCloseKey(hKey);

	SetInstallProgress(E_SET_CODECPROPERTIES, E_STATE_CHECKING, 60);

	//Haali
	lRet = RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Haali\\Matroska Splitter"), 0, KEY_ALL_ACCESS, &hKey);
	if(lRet == ERROR_SUCCESS)
	{
		lRet = RegQueryValueEx(hKey, _T("ui.trayIcon"), NULL, &dwType, (LPBYTE)&dwValue, &dwSize);
		if(lRet == ERROR_SUCCESS)
		{
			if(dwValue == 0)	//Hide 상태
			{
				bHaali = true;
			}//if
		}//if
	}//if
	RegCloseKey(hKey);

	SetInstallProgress(E_SET_CODECPROPERTIES, E_STATE_CHECKING, 90);

	if(bFfdShow && bFfdShow_Audio && bHaali)
	{
		m_bSetCodec = true;
		SetInstallProgress(E_SET_CODECPROPERTIES, E_STATE_INSTALLED);
	}
	else
	{
		m_bSetCodec = false;
		SetInstallProgress(E_SET_CODECPROPERTIES, E_STATE_NOTINSTALLED);
	}//if
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FLV Splitter가 설치되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckInstallFlv(void)
{
	ciDEBUG(1, ("Start : CheckInstallFlv"));

	SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_CHECKING);
	//설치 경로에 필터 파일이 있는지 검사한다.

	m_bFlvInstall = false;

	CFileStatus fs;
	CString strFlvPath;
	strFlvPath = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_FLVSPLITTER], _T("Path"));
	if(strFlvPath.GetLength() == 0)
	{
		strFlvPath.Format(_T("%sGNU\\FLVSPLITTER\\"), GetProgramDir());
	}//if
	strFlvPath += _T("FLVSplitter.ax");

	SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_CHECKING, 40);

	if(CFile::GetStatus(strFlvPath, fs) == TRUE)
	{
		//레지스트리에 FLV필터가 등록되어있는지 검사
		HKEY hKey;
		LONG lRet;
		CString szBuf;
		DWORD dwSize = 1024;
		TCHAR szVal[1024];
		DWORD dwType = REG_SZ;

		lRet = RegOpenKeyEx(HKEY_CURRENT_USER, _T("SOFTWARE\\GNU\\FLVSPLITTER"), 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			lRet = RegQueryValueEx(hKey, _T("ProgramFolder"), NULL, &dwType, (LPBYTE)&szVal, &dwSize);
			if(lRet == ERROR_SUCCESS)
			{
				if(strFlvPath.Find(szVal) != -1)
				{
					m_bFlvInstall = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong FLV folder : %s", szVal));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg query fail : FLV"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : FLV"));
		}//if
		RegCloseKey(hKey);
	}
	else
	{
		ciDEBUG(1, ("Can't found FLV file : %s", strFlvPath));
	}//if

	SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_CHECKING, 80);

	if(m_bFlvInstall)
	{
		SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_NOTINSTALLED);
	}//if

	ciDEBUG(1, ("End : CheckInstallFlv"));
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Flash Player가 설치되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckInstallFlashPlayer(void)
{
	ciDEBUG(1, ("Start : CheckInstallFlashPlayer"));

	SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_CHECKING);
	//레지스트리에 플레쉬 ocx가 등록되어 있는지 확인한다.

	HKEY hKey;
	LONG lRet;
	DWORD dwValue;
	char szValue[1024];
	CString strBuf;

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Macromedia\\FlashPlayer"), 0, KEY_ALL_ACCESS, &hKey);
	if(lRet == ERROR_SUCCESS)
	{
		SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_CHECKING, 40);

		dwValue = sizeof(szValue);
		lRet = RegQueryValueEx(hKey, _T("CurrentVersion"), NULL, NULL, (LPBYTE)szValue, &dwValue);
		if(lRet == ERROR_SUCCESS)
		{
			int nStart = 0;
			strBuf = szValue;
			CString szToken = strBuf.Tokenize(_T(","), nStart);
			if(szToken.IsEmpty())
			{
				ciDEBUG(1, ("Flash player version is empty"));
				m_bFlashInstall = false;
			}
			else
			{
				if(atoi(szValue) >= 10)
				{
					m_bFlashInstall = true;
				}
				else
				{
					ciDEBUG(1, ("Wrong flash player version : %s", strBuf));
					m_bFlashInstall = false;
				}//if
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg query fail : Flash player"));
		}//if
	}
	else
	{
		ciDEBUG(1, ("Reg open fail : Flash player"));
		m_bFlashInstall = false;
	}//if
	RegCloseKey(hKey);

	SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_CHECKING, 80);

	if(m_bFlashInstall)
	{
		SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_NOTINSTALLED);
	}//if

	ciDEBUG(1, ("End : CheckInstallFlashPlayer"));
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Soft Drive가 설정되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckSetSoftDrive(void)
{
	ciDEBUG(1, ("Start : CheckSetSoftDrive"));

	SetInstallProgress(E_SET_SFTDRIVE, E_STATE_CHECKING);
	//가상 드라이브는 항상 X 드라이브로 만들어진다.
	//이 드라이브가 존재하는지 확인하는 것으로 확인...

	bool bEnc = false;
	bool bTemp = false;
	bool bConfig = false;

	if(PathFileExists(_T("X:\\SqiSoft\\Contents\\ENC")))
	{
		bEnc = true;
	}
	else
	{
		ciDEBUG(1, ("Can not found soft drive path  : X:\\SqiSoft\\Contents\\ENC"));
	}//if

	SetInstallProgress(E_SET_SFTDRIVE, E_STATE_CHECKING, 30);

	if(PathFileExists(_T("X:\\SqiSoft\\Contents\\Temp")))
	{
		bTemp = true;
	}
	else
	{
		ciDEBUG(1, ("Can not found soft drive path  : X:\\SqiSoft\\Contents\\Temp"));
	}//if

	SetInstallProgress(E_SET_SFTDRIVE, E_STATE_CHECKING, 60);

	if(PathFileExists(_T("X:\\SqiSoft\\UTV1.0\\execute\\config")))
	{
		bConfig = true;
	}
	else
	{
		ciDEBUG(1, ("Can not found soft drive path  : X:\\SqiSoft\\UTV1.0\\execute\\config"));
	}//if

	SetInstallProgress(E_SET_SFTDRIVE, E_STATE_CHECKING, 90);

	if(bEnc && bTemp && bConfig)
	{
		m_bSetSoftDrive = true;
		SetInstallProgress(E_SET_SFTDRIVE, E_STATE_INSTALLED);
	}
	else
	{
		m_bSetSoftDrive = false;
		SetInstallProgress(E_SET_SFTDRIVE, E_STATE_NOTINSTALLED);
	}//if	

	ciDEBUG(1, ("End : CheckSetSoftDrive"));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// ShortCut이 설정되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckCreateShortCut(void)
{
	ciDEBUG(1, ("Start : CheckCreateShortCut"));

	SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_CHECKING);
	//시작프로그램에 UBCReady.lnk 파일이 있는지 확인한다.

	CString strTarget;
	char szTargetPath[MAX_PATH+1];
	::SHGetSpecialFolderPath (NULL, szTargetPath, CSIDL_STARTUP, FALSE);
	strTarget = szTargetPath;
	strTarget += _T("\\UBCReady.lnk");

	SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_CHECKING, 60);

	if(PathFileExists(strTarget))
	{
		m_bShortCut = true;
		SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_INSTALLED);
	}
	else
	{
		ciDEBUG(1, ("Can not found shortcut  : %s", strTarget));
		m_bShortCut = false;
		SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_NOTINSTALLED);
	}//if

	ciDEBUG(1, ("End : CheckCreateShortCut"));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Window Trivial이 설정되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckDisableWindowTrivial(void)
{
	ciDEBUG(1, ("Start : CheckDisableWindowTrivial"));

	if(GetOSVersion() == E_OS_WIN7)
	{
		m_bWindowTrivial = false;
		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_SKIPED);
		ciDEBUG(1, ("End : CheckDisableWindowTrivial"));

		return;
	}//if

	SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_CHECKING);
	//윈도우즈에서 실행되는 어플리케이션이 에러를 발생하고 종료되었을 경우에
	//Error report를 하겠냐는 UI가 출력되는것을 막는 기능...

	m_bWindowTrivial = false;

	HKEY hKey;
	LONG lRet;
	CString regKey;
	if(Is64OS())
	{
		regKey = _T("SOFTWARE\\Wow6432Node\\Microsoft\\PCHealth\\ErrorReporting");
	}
	else
	{
		regKey = _T("SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting");
	}//if

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regKey, 0, KEY_ALL_ACCESS, &hKey);

	SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_CHECKING, 40);

	if(lRet == ERROR_SUCCESS)
	{
		DWORD dwValue=0;
		DWORD dwSize = sizeof(dwValue);
		lRet = RegQueryValueEx(hKey, _T("ShowUI"), NULL, NULL, (LPBYTE)&dwValue, &dwSize);
		if(lRet == ERROR_SUCCESS)
		{
			if(dwValue == 0)
			{
				m_bWindowTrivial = true;
			}
			else
			{
				ciDEBUG(1, ("Wrong show ui value : %d", dwValue));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg query fail : %s - ShowUI", regKey));
		}//if
	}
	else
	{
		ciDEBUG(1, ("Reg open fail : %s", regKey));
	}//if
	RegCloseKey(hKey);

	SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_CHECKING, 80);

	if(m_bWindowTrivial)
	{
		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_NOTINSTALLED);
	}//if

	ciDEBUG(1, ("End : CheckDisableWindowTrivial"));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Security가 설정되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckSetSecurity(void)
{
	ciDEBUG(1, ("Start : CheckSetSecurity"));

	SetInstallProgress(E_SET_SECURITY, E_STATE_CHECKING);
	//Contents 폴더가 Hidden으로 설정되어 있는지를 확인하며,
	//윈도우즈 탐색기에서 숨김파일이 보이도록 설정하여도 보이지 않도록되어 있는지 확인

	m_bSecurity = false;

	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	CString strContents;
#ifndef _DEBUG
	strContents = szDrive;
#else if
	strContents = _T("C:");
#endif
	strContents += _T("\\SQISoft\\Contents");

	SetInstallProgress(E_SET_SECURITY, E_STATE_CHECKING, 40);

	CFileFind ff;
	if(ff.FindFile(strContents))
	{
		ff.FindNextFile();
		if(ff.IsHidden())
		{
			HKEY hKey;
			LONG lRet;
			CString regKey = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced\\Folder\\Hidden\\SHOWALL");

			lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regKey, 0, KEY_ALL_ACCESS, &hKey);
			if(lRet == ERROR_SUCCESS)
			{
				DWORD dwValue;
				DWORD dwSize = sizeof(dwValue);
				lRet = RegQueryValueEx(hKey,_T("CheckedValue"), NULL, NULL, (LPBYTE)&dwValue, &dwSize);
				if(lRet == ERROR_SUCCESS)
				{
					if(dwValue == 0)
					{
						m_bSecurity = true;
					}
					else
					{
						ciDEBUG(1, ("Wrong checkedvalue : %d", dwValue));
					}//if
				}
				else
				{
					ciDEBUG(1, ("Reg query fail : CheckedValue"));
				}//if
			}
			else
			{
				ciDEBUG(1, ("Reg open fail : %s", regKey));
			}//if
			RegCloseKey(hKey);
		}
		else
		{
			ciDEBUG(1, ("Folder hidden state is net set : %s", strContents));
		}//if
	}
	else
	{
		ciDEBUG(1, ("Can not found : %s", strContents));
	}//if

	SetInstallProgress(E_SET_SECURITY, E_STATE_CHECKING, 80);

	if(m_bSecurity)
	{
		SetInstallProgress(E_SET_SECURITY, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_SET_SECURITY, E_STATE_OPTIONAL);
	}//if

	ciDEBUG(1, ("End : CheckSetSecurity"));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Firewall Exception이 설정되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckSetFirewallException(void)
{
	ciDEBUG(1, ("Start : CheckSetFirewallException"));

	SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_CHECKING);

	m_bSetFirewall = false;
	
	CString strFile = GetAppPath();
	strFile += "firewallException.txt";

	int nIdx;
	CStdioFile ioFile;
	CString strName, strLine;
	CString strPostfix = "";
	CString strLang = scratchUtil::getInstance()->getLang();
	if((strLang.GetLength() != 0) && (strLang != "en"))
	{
		strPostfix = "_" + strLang;
	}//if

	SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_CHECKING, 30);

	char cBuf[1024] = { 0x00 };
	bool bAdded = true;
	try
	{
		if(ioFile.Open(strFile, CFile::modeRead))
		{
			while(ioFile.ReadString(cBuf, 1024))
			{
				strLine = cBuf;
				nIdx = strLine.Find(",");
				if(nIdx == -1)
				{
					continue;
				}//if

				strName = strLine.Left(nIdx);
				if(strPostfix.GetLength() != 0)
				{
					strName.Replace("_en", strPostfix);
				}//if

				if(!scratchUtil::getInstance()->IsAppExceptionAdded((LPCSTR)(LPCTSTR)strName))
				{
					ciDEBUG(1, ("Not added on firewall exception list : %s", strName));
					bAdded = false;
				}
				else
				{
					ciDEBUG(1, ("Exist in firewall exception list : %s", strName));
				}//if
			}//while
		}
		else
		{
			ciDEBUG(1, ("File open fail : %s", strFile));
		}//if
	}
	catch(CFileException* e)
	{
		ciDEBUG(1, ("File exception : [%d]%s", e->m_cause, strFile));
	}//try

	SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_CHECKING, 90);

	m_bSetFirewall = bAdded;
	
	if(m_bSetFirewall)
	{
		SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_NOTINSTALLED);
	}//if

	ciDEBUG(1, ("End : CheckSetFirewallException"));
}


/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Security Level2이 설정되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckSetSecurityLevel2(void)
{
	SetInstallProgress(E_SET_SECURITY2, E_STATE_CHECKING);
	//윈도우즈 탐색기에서 숨김파일이 보이도록 설정하여도 보이지 않도록 하는 기능

	m_bSecurity2 = false;

	HKEY hKey;
	LONG lRet;
	CString regKey = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced\\Folder\\Hidden\\SHOWALL");

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regKey, 0, KEY_ALL_ACCESS, &hKey);

	SetInstallProgress(E_SET_SECURITY2, E_STATE_CHECKING, 40);

	if(lRet == ERROR_SUCCESS)
	{
		DWORD dwValue;
		DWORD dwSize = sizeof(dwValue);
		RegQueryValueEx(hKey,_T("CheckedValue"), NULL, NULL, (LPBYTE)&dwValue, &dwSize);
		if(dwValue == 0)
		{
			m_bSecurity2 = true;
		}//if
	}//if
	RegCloseKey(hKey);

	SetInstallProgress(E_SET_SECURITY2, E_STATE_CHECKING, 80);

	if(m_bSecurity2)
	{
		SetInstallProgress(E_SET_SECURITY2, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_SET_SECURITY2, E_STATE_NOTINSTALLED);
	}//if
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// LogMeIn이 설치되어 있는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckInstallLogMeIn(void)
{
	SetInstallProgress(E_INSTALL_LOGMEIN, E_STATE_CHECKING);
	Sleep(500);

	m_bLogMeInInstall = false;

	if(m_bLogMeInInstall)
	{
		SetInstallProgress(E_INSTALL_LOGMEIN, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_INSTALL_LOGMEIN, E_STATE_NOTINSTALLED);
	}//if
}
*/

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Auto-shutdown time이 설정되었는지 확인한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CUBCInstallDlg::CheckSetAutoShutDownTime(void)
{
	SetInstallProgress(E_SET_AUTOSHUTDOWN, E_STATE_CHECKING);
	Sleep(500);

	m_bAutoShutDown = false;

	if(m_bAutoShutDown)
	{
		SetInstallProgress(E_SET_AUTOSHUTDOWN, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_SET_AUTOSHUTDOWN, E_STATE_NOTINSTALLED);
	}//if
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VNC를 설치한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::InstallVnc(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_INSTALL_VNC))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : InstallVnc"));

	SetInstallProgress(E_INSTALL_VNC, E_STATE_INSTALLING);
	//3rdParty의 VNC 폴더내의 모든 파일을 설치 위치에 복사하고
	//서비스를 등록해 준다.

	//먼저 서비스를 종료시킨다.
	StopSvc(_T("uvnc_service"));

	CString strSrc, strTarget;
	strSrc.Format(_T("%sVNC\\*.*"), Get3rdPartyDir());
	strTarget = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_VNC], _T("Path"));

	SetInstallProgress(E_INSTALL_VNC, E_STATE_INSTALLING, 40);

	if(CopyFiles(strSrc, strTarget))
	{
		CString strRegFile;
		if(Is64OS())
		{
			strRegFile.Format(_T("%sVNC\\Regist\\%s"), Get3rdPartyDir(), FILE_VNC_REGIST64);
		}
		else
		{
			strRegFile.Format(_T("%sVNC\\Regist\\%s"), Get3rdPartyDir(), FILE_VNC_REGIST);
		}//if

		if(RegistFile(strRegFile))
		{
			m_bVncInstall = true;

			//Shortcut
			TCHAR cShortcutPath[MAX_PATH+1];
			::ZeroMemory(cShortcutPath, sizeof(cShortcutPath));
			::SHGetSpecialFolderPath(NULL, cShortcutPath, CSIDL_COMMON_PROGRAMS, FALSE);

			CString strShortcut;
			strShortcut.Format(_T("%s\\UltraVNC"), cShortcutPath);
			if(Is64OS())
			{
				strSrc.Format(_T("%sVNC\\Shortcut64\\*.*"), Get3rdPartyDir());
			}
			else
			{
				strSrc.Format(_T("%sVNC\\Shortcut\\*.*"), Get3rdPartyDir());
			}//if
			CopyFiles(strSrc, strShortcut);

			//UnInstall
			strSrc.Format(_T("%sVNC\\UnInstall\\"), Get3rdPartyDir());
			if(GetOSVersion() > E_OS_WINXP)
			{
				strSrc += _T("unins000.dat.win7");
				strTarget += _T("unins000.dat");
			}
			else
			{
				if(IsPosReady())
				{
					ciDEBUG(1, ("Windows POSReady"));
					strSrc += _T("unins000.dat.pos");
					strTarget += _T("unins000.dat");
				}
				else
				{
					ciDEBUG(1, ("Windows XP"));
					strSrc += _T("unins000.dat.xp");
					strTarget += _T("unins000.dat");
				}//if
			}//if
			CopyFiles(strSrc, strTarget);
		}//if
	}//if

	SetInstallProgress(E_INSTALL_VNC, E_STATE_INSTALLING, 60);

	//서비스를 시작시킨다.
	StartSvc(_T("uvnc_service"));

	SetInstallProgress(E_INSTALL_VNC, E_STATE_INSTALLING, 80);

	//서비스에 등록해준다.
	//Enum값과 보안 설정때문에 API로 설치와 동일하게 하기는 힘들다.
	//그냥 레지스트리 등록해 주는 것으로....
	/*
	SC_HANDLE hScm, hSrv;
	SERVICE_DESCRIPTION lpDes;
	CString strSrvPath, strDesc;
	strSrvPath.Format(_T("\"%sWinVNC.exe\" -service"), strTarget);
	strDesc = _T("Provides secure remote desktop sharing");
	
	//SCM을 연다
	hScm = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
	if(hScm == NULL)
	{
		TRACE("Fail to open SCM\r\n");
		m_bVncInstall = false;
	}//if

	hSrv = CreateService(hScm, _T("uvnc_service"), _T("uvnc_service"), SERVICE_PAUSE_CONTINUE | SERVICE_CHANGE_CONFIG,
						SERVICE_WIN32_OWN_PROCESS, SERVICE_AUTO_START, SERVICE_ERROR_IGNORE,
						strSrvPath, NULL, NULL, NULL, NULL, NULL);
	if(hSrv == NULL)
	{
		TRACE("Fail to open create service\r\n");
		m_bVncInstall = false;
	}//if

	CloseServiceHandle(hSrv);
	CloseServiceHandle(hScm);
	*/

	if(m_bVncInstall)
	{
		SetInstallProgress(E_INSTALL_VNC, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_INSTALL_VNC, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : InstallVnc"));

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// K-lite codec pack을 설치한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::InstallCodec(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_INSTALL_CODEC))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : InstallCodec"));

	SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLING);
	//3rdParty의 Codec 폴더내의 모든 파일을 설치 위치에 복사하고
	//모든 필터를 등록해 준다.

	CString strSrc, strTarget;
	strSrc.Format(_T("%sCodec\\*.*"), Get3rdPartyDir());
	strTarget = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_CODEC], _T("Path"));

	SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLING, 20);

	if(CopyFiles(strSrc, strTarget))
	{
		SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLING, 40);
		//설치된 경로의 Copy폴더안의 파일을 아래 위치에 복사한다.
		//Dest filename: C:\WINDOWS\system32\xvidvfw.dll
		//Dest filename: C:\WINDOWS\system32\xvidcore.dll
		//Dest filename: C:\WINDOWS\system32\lameACM.acm
		//Dest filename: C:\WINDOWS\system32\lame_acm.xml
		//Dest filename: C:\WINDOWS\system32\unrar.dll
		//Dest filename: C:\WINDOWS\avisplitter.ini
		strTarget += _T("Copy\\*.*");
		if(CopyFiles(strTarget, GetSystemDir()))
		{
			CString strIni;
			strIni = GetSystemDir();
			strIni += _T("avisplitter.ini");
			if(!MoveFileEx(strIni, GetWinDir(), MOVEFILE_REPLACE_EXISTING))
			{
				ciDEBUG(1, ("Move file fail : %s ==> %s", strIni, GetWinDir()));
			}//if

			SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLING, 50);

			//다음의 필터들을 등록해 준다.
			/*==================================================
			ffdshow\ffdshow.ax

			Filters\DivXDecH264.ax
			Filters\xvid.ax
			Filters\vp7dec.ax
			Filters\ac3file.ax
			Filters\mmamr.ax
			Filters\mmmpcdmx.ax
			Filters\mmmpcdec.ax
			Filters\CoreVorbis.ax
			Filters\WavPackDSDecoder.ax
			Filters\WavPackDSSplitter.ax
			Filters\madFlac.ax
			Filters\MonkeySource.ax
			Filters\CLVSD.ax
			Filters\MpegSplitter.ax
			Filters\FLVSplitter.ax
			Filters\MP4Splitter.ax	==> Filters 폴더의 모든 ax 파일 등록(avisplitter.ax 파일만 제외 ?)

			Filters\Haali\mkx.dll
			Filters\Haali\ogm.dll
			Filters\Haali\mp4.dll
			Filters\Haali\ts.dll
			Filters\Haali\avi.dll
			Filters\Haali\splitter.ax
			Filters\Haali\avs.dll
			Filters\Haali\dxr.dll

			Filters\vsfilter.dll

			Parameters: /u /s "C:\Program Files\K-Lite Codec Pack\Filters\mmaacd.ax"
			===================================================*/
			bool bSuccessFilter = true;
			bool bRegistServer = true;
			CString strFilterPath = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_CODEC], _T("Path"));
			for(int i=0; i<COUNT_FILTER; i++)
			{
				if(i == (COUNT_FILTER-1))
				{
					bRegistServer = false;	//mmaacd.ax는 서버등록을 하지 않는다.
				}//if

				if(!RegistComponent(strFilterPath + pszFilters[i], bRegistServer))
				{
					bSuccessFilter = false;
				}//if
			}//for

			SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLING, 70);

			//Regist 폴더의 설정 레지스트리 파일을 실행한다.
			CString strRegistDir = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_CODEC], _T("Path"));
			strRegistDir += _T("Regist\\*.reg");
			CString strRegistFile, strFile;
			CFileFind ff;
			BOOL bFound = ff.FindFile(strRegistDir);
			bool bSuccessRegFile = true;
			while(bFound)
			{
				bFound = ff.FindNextFile();
				strRegistFile = ff.GetFilePath();
				strFile = ff.GetFileName();
				if(strFile == _T("Uninstall.reg") && Is64OS())
				{
					continue;
				}//if

				if(strFile == _T("Uninstall64.reg") && !Is64OS())
				{
					continue;
				}//if

				//if((strRegistFile.Find("64") != -1) && !Is64OS())
				//{
				//	continue;
				//}//if

				if(!RegistFile(strRegistFile))
				{
					bSuccessRegFile = false;
				}//if
			}//while

			SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLING, 90);

			if(bSuccessFilter && bSuccessRegFile)
			{
				m_bCodecInstall = true;

				//Shortcut
				TCHAR cShortcutPath[MAX_PATH+1];
				::ZeroMemory(cShortcutPath, sizeof(cShortcutPath));
				::SHGetSpecialFolderPath(NULL, cShortcutPath, CSIDL_COMMON_PROGRAMS, FALSE);

				CString strShortcut;
				strShortcut.Format(_T("%s\\K-Lite Codeck Pack"), cShortcutPath);
				if(Is64OS())
				{
					strSrc.Format(_T("%sCodec\\Shortcut64\\*.*"), Get3rdPartyDir());
				}
				else
				{
					strSrc.Format(_T("%sCodec\\Shortcut\\*.*"), Get3rdPartyDir());
				}//if
				CopyFiles(strSrc, strShortcut);
			}
			else
			{
				m_bCodecInstall = false;
			}//if
		}//if
	}//if	

	if(m_bCodecInstall)
	{
		SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_INSTALL_CODEC, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : InstallCodec"));

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 코덱 properties를 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::SetCodecPoperties(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_SET_CODECPROPERTIES))
	{
		return true;
	}//if

	SetInstallProgress(E_SET_CODECPROPERTIES, E_STATE_INSTALLING);
	Sleep(500);

	m_bSetCodec = true;
	SetInstallProgress(E_SET_CODECPROPERTIES, E_STATE_INSTALLED);

	return true;
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FLV splitter를 설치한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::InstallFlvSplitter(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_INSTALL_FLVSPLITTER))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : InstallFlvSplitter"));

	SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_INSTALLING);

	//3rdParty의 FLV 폴더내의 모든 파일을 설치 위치에 복사하고
	//서비스를 등록해 준다.

	CString strSrc, strTarget;
	strSrc.Format(_T("%sFLV\\*.*"), Get3rdPartyDir());
	strTarget = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_FLVSPLITTER], _T("Path"));

	SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_INSTALLING, 40);

	if(CopyFiles(strSrc, strTarget))
	{
		//FLV 필터 등록
		CString strRegFile;
		if(Is64OS())
		{
			strRegFile.Format(_T("%sFLV\\Regist\\%s"), Get3rdPartyDir(), FILE_FLV_REGIST64);
		}
		else
		{
			strRegFile.Format(_T("%sFLV\\Regist\\%s"), Get3rdPartyDir(), FILE_FLV_REGIST);
		}//if

		if(RegistFile(strRegFile))
		{
			//regsvr32를 해줘야 한다...
			CString strCom;
			strCom.Format(_T("\"%s%s\""), strTarget, FILE_FLV_FILTER);
			if(RegistComponent(strCom))
			{
				m_bFlvInstall = true;
			}//if
		}//if
	}//if

	SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_INSTALLING, 80);

	if(m_bFlvInstall)
	{
		SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_INSTALL_FLVSPLITTER, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : InstallFlvSplitter"));

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Flash player를 설치한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::InstallFlashPlayer(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_INSTALL_FLASHPLAYER))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : InstallFlashPlayer"));

	SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_INSTALLING);
	
	//3rdParty의 Flash 폴더내의 모든 파일을 설치 위치에 복사하고
	//서비스를 등록해 준다.

	CString strSrc, strTarget;
	strSrc.Format(_T("%sFlash\\*.*"), Get3rdPartyDir());
	strTarget = GetINIValue(m_strIniFileName, pszInstallList[E_INSTALL_FLASHPLAYER], _T("Path"));

	SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_INSTALLING, 40);

	/////////////////////////////////////////////////////////////////////////

	//대상 폴더가 존재하지 않는다면 만들어준다.
	if(!CreatePathDirectory(strTarget))
	{
		ciDEBUG(1, ("Fail to create directory : %s", strTarget));
	}//if
	
	CFileFind ff;
	BOOL bFound = ff.FindFile(strSrc);
	CString strFileName;
	bool bFail = false;
	while(bFound)
	{
		bFound = ff.FindNextFile();

		if(ff.IsDots())
		{
			continue;
		}//if

		//Regist 폴더는 복사하지 않는다.
		if(ff.IsDirectory())
		{
			continue;
		}//if

		strFileName = strTarget + ff.GetFileName();
		//OCX 파일이 열려 있으면 덮어쒸우기가 안된다...
		//같은 OCX가 있다면 복사하지 않도록 처리...
		if(PathFileExists(strFileName))
		{
			continue;
		}//if

		if(!CopyFile(ff.GetFilePath(), strFileName, FALSE))
		{
			bFail = true;
			ciDEBUG(1, ("Copy file fail : %s ==> %s", ff.GetFilePath(), strFileName));
		}//if
	}//while

	if(!bFail)
	{
		CString strRegFile;
		if(Is64OS())
		{
			strRegFile.Format(_T("%sFlash\\Regist\\%s"), Get3rdPartyDir(), FILE_FLASH_REGIST64);
		}
		else
		{
			strRegFile.Format(_T("%sFlash\\Regist\\%s"), Get3rdPartyDir(), FILE_FLASH_REGIST);
		}//if

		if(RegistFile(strRegFile))
		{
			//Flash OCX 등록
			CString strCom;
			strCom.Format(_T("%s%s"), strTarget, FILE_FLASH_PLAYER);
			if(RegistComponent(strCom))
			{
				m_bFlashInstall = true;
			}//if
		}//if
	}//if

	SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_INSTALLING, 80);

	if(m_bFlashInstall)
	{
		SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_INSTALL_FLASHPLAYER, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : InstallFlashPlayer"));

	return true;
}
*/

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Soft drive를 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::SetSoftDrive(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_SET_SFTDRIVE))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : SetSoftDrive"));

	SetInstallProgress(E_SET_SFTDRIVE, E_STATE_INSTALLING);

	//softDrive.bat 실행
	//CString strCmd;
	// bat 파일을 실행하지 않는다 
	//strCmd.Format(_T("%s%s"), GetAppPath(), FILE_SOFTDRIVE_BAT);
	
	// softDrive.bat 의 내용을 직접실행한다.
	string ftproot = _UBC_CD("C:\\ftproot");

	scratchUtil::getInstance()->createDir((ftproot + "\\SQISoft\\Contents\\ENC").c_str());
	scratchUtil::getInstance()->createDir((ftproot + "\\SQISoft\\Contents\\Temp").c_str());
	scratchUtil::getInstance()->createDir((ftproot + "\\SQISoft\\UTV1.0\\execute\\config").c_str());

	ciDEBUG(1,("create X drive "));

	string command = GetAppPath();
	command += "subst X: ";
	command += ftproot;
	ciDEBUG(1,("run %s", command.c_str()));

	SetInstallProgress(E_SET_SFTDRIVE, E_STATE_INSTALLING, 40);

	//if(RunCmd(strCmd))
	if(RunCmd(command.c_str()))
	{
		m_bSetSoftDrive = true;
	}
	else
	{
		//ciDEBUG(1, ("Fail to set softdrive : %s", strCmd));
		ciDEBUG(1, ("Fail to set softdrive : %s", command.c_str()));
	}//if

	SetInstallProgress(E_SET_SFTDRIVE, E_STATE_INSTALLING, 80);

	if(m_bSetSoftDrive)
	{
		SetInstallProgress(E_SET_SFTDRIVE, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_SET_SFTDRIVE, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : SetSoftDrive"));

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 바로가기를 만든다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::CreateShortCut(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_CREATE_SHORTCUT))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : CreateShortCut"));

	SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_INSTALLING);

	//바로가기 링크 만들기...
	CString strApp;
	strApp.Format(_T("%s%s"), GetAppPath(), FILE_MENU_REGIST);

	SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_INSTALLING, 40);

	if(RunCmd(_T(" /install"), strApp))
	{
		m_bShortCut = true;
	}//if
	
	SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_INSTALLING, 80);

	if(m_bShortCut)
	{
		SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_CREATE_SHORTCUT, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : CreateShortCut"));

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Disable Windows OS trivial \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::DisableWindowTrivial(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_DISABLE_WINDOWTRIVIAL))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : DisableWindowTrivial"));

	if(GetOSVersion() == E_OS_WIN7)
	{
		m_bWindowTrivial = false;
		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_SKIPED);
		ciDEBUG(1, ("End : CheckDisableWindowTrivial"));

		return true;
	}//if

	SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALLING);
	//윈도우즈에서 실행되는 어플리케이션이 에러를 발생하고 종료되었을 경우에
	//Error report를 하겠냐는 UI가 출력되는것을 막는 기능...

	//오류보고를 해제한다.
	HKEY hKey;
	LONG lRet;
	DWORD dwVal;
	CString strRegKey;
	if(Is64OS())
	{
		strRegKey = _T("SOFTWARE\\Wow6432Node\\Microsoft\\PCHealth\\ErrorReporting");
	}
	else
	{
		strRegKey = _T("SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting");
	}//if

	bool bShowUI = false;
	bool bDoReport = false;
	bool bKernelReport = false;

	lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, strRegKey, 0, KEY_ALL_ACCESS, &hKey);

	SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALLING, 20);

	if(lRet == ERROR_SUCCESS)
	{
		dwVal = 0;
		lRet = RegSetValueEx(hKey, _T("ShowUI") ,0, REG_BINARY, (BYTE*)&dwVal, sizeof(dwVal));  
		if(lRet == ERROR_SUCCESS)
		{
			bShowUI = true;
		}
		else
		{
			ciDEBUG(1, ("Reg set fail : ShowUI"));
		}//if

		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALLING, 30);

		dwVal = 0;
		lRet = RegSetValueEx(hKey, _T("DoReport") ,0, REG_BINARY, (BYTE*)&dwVal, sizeof(dwVal));  
		if(lRet == ERROR_SUCCESS)
		{
			bDoReport = true;
		}
		else
		{
			ciDEBUG(1, ("Reg set fail : DoReport"));
		}//if

		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALLING, 40);

		dwVal = 1;
		lRet = RegSetValueEx(hKey, _T("IncludeKernelFault") ,0, REG_BINARY, (BYTE*)&dwVal, sizeof(dwVal));  
		if(lRet == ERROR_SUCCESS)
		{
			bKernelReport = true;
		}
		else
		{
			ciDEBUG(1, ("Reg set fail : IncludeKernelFault"));
		}//if

		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALLING, 50);
	}
	else
	{
		ciDEBUG(1, ("Reg open fail : %s", strRegKey));
	}//if
	RegCloseKey(hKey);

	//윈도우즈 시작메뉴줄에 생기는 버블 도움말을 나올수 없도록 한다.
	scratchUtil::getInstance()->showBalloonTip(false);

	SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALLING, 80);

	if(bShowUI && bDoReport && bKernelReport)
	{
		m_bWindowTrivial = true;
		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALLED);
	}
	else
	{
		m_bWindowTrivial = false;
		SetInstallProgress(E_DISABLE_WINDOWTRIVIAL, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : DisableWindowTrivial"));

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Set firewall exception automatically \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::SetFirewallException(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_SET_FIREWALLEXCEPTION))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : SetFirewallException"));

	SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_INSTALLING);
	//execute 폴더에 있는 firewallException.txt 파일에 있는 리스트를 방하벽 예외로 등록한다.

	CString strFile = GetAppPath();
	strFile += "firewallException.txt";

	int nIdx;
	CStdioFile ioFile;
	CString strName, strLine;
	CString strPostfix = "";
	CString strLang = scratchUtil::getInstance()->getLang();
	if((strLang.GetLength() != 0) && (strLang != "en"))
	{
		strPostfix = "_" + strLang;
	}//if

	SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_INSTALLING, 30);

	char cBuf[1024] = { 0x00 };
	bool bSuccess = true;
	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	try
	{
		if(ioFile.Open(strFile, CFile::modeRead))
		{
			while(ioFile.ReadString(cBuf, 1024))
			{
				strLine = cBuf;
				nIdx = strLine.Find(",");
				if(nIdx == -1)
				{
					continue;
				}//if

				strName = strLine.Left(nIdx);
				if(strPostfix.GetLength() != 0)
				{
					strName.Replace("_en", strPostfix);
				}//if

				//C드라이브가 아닌곳에 설치될 수도 있기에 등록하려는 프로그램의 경로에서
				//SQISoft경로가 포함된 프로그램은 드라이브를 현재 실행 드라이브로 바꾸어 준다.
				if(strName.Find("SQISoft") != -1)
				{
					_tsplitpath(strName, szDrive, szPath, szFilename, szExt);
					strName.Format("%s%s%s%s", GetAppDrive(), szPath, szFilename, szExt);
				}//if

				if(!scratchUtil::getInstance()->AddToExceptionList((LPCSTR)(LPCTSTR)strName))
				{
					ciDEBUG(1, ("Fail to add on firewall exception : %s", strName));
					bSuccess = false;
				}
				else
				{
					ciDEBUG(1, ("Success in add on firewall exception : %s", strName));
				}//if
			}//while
		}
		else
		{
			ciDEBUG(1, ("File open fail : %s", strFile));
		}//if
	}
	catch(CFileException* e)
	{
		ciDEBUG(1, ("File exception : [%d]%s", e->m_cause, strFile));
	}//try

	SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_INSTALLING, 90);

	m_bSetFirewall = bSuccess;
	
	if(m_bSetFirewall)
	{
		SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_SET_FIREWALLEXCEPTION, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : SetFirewallException"));

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Security를 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::SetSecurity(void)
{
	if(!ListView_GetCheckState(m_listInstall.GetSafeHwnd(), E_SET_SECURITY))
	{
		return true;
	}//if

	ciDEBUG(1, ("Start : SetSecurity"));

	SetInstallProgress(E_SET_SECURITY, E_STATE_INSTALLING);
	//Contents 폴더가 Hidden으로 설정
	//윈도우즈 탐색기에서 숨김파일이 보이도록 설정하여도 보이지 않도록 함

	CString strCmd;
	strCmd.Format(_T("%s%s%s%s"), GetAppPath(), CMD_SECURITY, GetAppDrive(), PATH_CONTENTS);

	SetInstallProgress(E_SET_SECURITY, E_STATE_INSTALLING, 40);
	
	if(RunCmd(strCmd))
	{
		HKEY hKey;
		LONG lRet;
		DWORD dwVal;
		CString strRegKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Advanced\\Folder\\Hidden\\SHOWALL";

		lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, strRegKey, 0, KEY_ALL_ACCESS, &hKey);
		if(lRet == ERROR_SUCCESS)
		{
			dwVal = 0;
			lRet = RegSetValueEx(hKey, _T("CheckedValue") ,0, REG_DWORD, (BYTE*)&dwVal, sizeof(dwVal));  
			if(lRet == ERROR_SUCCESS)
			{
				m_bSecurity = true;
			}
			else
			{
				ciDEBUG(1, ("Reg set fail : CheckedValue"));
			}//if
		}
		else
		{
			ciDEBUG(1, ("Reg open fail : %s", strRegKey));
		}//if
	}
	else
	{
		ciDEBUG(1, ("Fail to SetSecurity"));
	}//if

	SetInstallProgress(E_SET_SECURITY, E_STATE_INSTALLING, 80);

	if(m_bSecurity)
	{
		SetInstallProgress(E_SET_SECURITY, E_STATE_INSTALLED);
	}
	else
	{
		SetInstallProgress(E_SET_SECURITY, E_STATE_INSTALL_FAIL);
	}//if

	ciDEBUG(1, ("End : SetSecurity"));

	return true;
}

/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// LogMeIn client를 설치한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::InstallLogMeIn(void)
{
	SetInstallProgress(E_INSTALL_LOGMEIN, E_STATE_INSTALLING);
	Sleep(500);

	m_bLogMeInInstall = true;
	SetInstallProgress(E_INSTALL_LOGMEIN, E_STATE_INSTALLED);

	return true;
}
*/
/*
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Auto-shutdown time을 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::SetAutoShutdownTime(void)
{
	SetInstallProgress(E_SET_AUTOSHUTDOWN, E_STATE_INSTALLING);
	Sleep(500);

	m_bAutoShutDown = true;
	SetInstallProgress(E_SET_AUTOSHUTDOWN, E_STATE_INSTALLED);

	return true;
}
*/

void CUBCInstallDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//OnOK();
	CWinThread* pThread = ::AfxBeginThread(CUBCInstallDlg::ThreadStartInstall, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();
}


void CUBCInstallDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	::PostMessage(AfxGetMainWnd()->m_hWnd, WM_CLOSE, 0 ,0);
}


void CUBCInstallDlg::OnBnClickedLicenceBtn()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strArg;
	CMainDlg* pDlg = (CMainDlg*)AfxGetMainWnd();
	switch(pDlg->GetEdition())
	{
	case E_EDITION_PLUS:
		{
			strArg = _T(" +PLS");
		}
		break;
	case E_EDITION_ENT:
		{
			strArg = _T(" +ENT");
		}
		break;
	default:
		{
			strArg = _T(" +STD");
		}
	}//switch

	CString strApp;
#ifdef _DEBUG
	strApp.Format(_T("C:\\SQISoft\\UTV1.0\\execute\\UBCHostAuthorizer.exe"));
#else if
	strApp.Format(_T("%s%s"), GetAppPath(), FILE_LICENCE_EXE);
#endif
	
	RunCmd(strArg, strApp);

	//::PostMessage(AfxGetMainWnd()->m_hWnd, WM_CLOSE, 0 ,0);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 인스톨 상태를 확인하는 thread \n
/// @param (LPVOID) param : (in) 부모윈도우 주소값
/// @return <형: UINT> \n
///			0 \n
/////////////////////////////////////////////////////////////////////////////////
UINT CUBCInstallDlg::ThreadCheckInstall(LPVOID param)
{
	CUBCInstallDlg* pParent = (CUBCInstallDlg*)param;

	pParent->CheckInstall();

	::PostMessage(pParent->m_hWnd, WM_COMPLETE_CHECK, 0, 0);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 인스톨을 시작하는 thread \n
/// @param (LPVOID) param : (in) 부모윈도우 주소값
/// @return <형: UINT> \n
///			0 \n
/////////////////////////////////////////////////////////////////////////////////
UINT CUBCInstallDlg::ThreadStartInstall(LPVOID param)
{
	CUBCInstallDlg* pParent = (CUBCInstallDlg*)param;

	pParent->StartInstall();

	::PostMessage(pParent->m_hWnd, WM_COMPLETE_INSTALL, 0, 0);

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 서비스를 시작한다. \n
/// @param (CString) strServiceName : (in) 시작하려는 서비스 명
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::StartSvc(CString strServiceName)
{
	SERVICE_STATUS ss;
	SC_HANDLE hScm, hSrv;
	hScm = OpenSCManager(NULL, NULL, GENERIC_READ);
	if(hScm == NULL)
	{
		ciDEBUG(1, ("Fail to open SCManager"));
		return false;
	}//if

	hSrv = OpenService(hScm, strServiceName, SERVICE_START | SERVICE_QUERY_STATUS);
	if(hSrv == NULL)
	{
		ciDEBUG(1, ("Fail to open service = %s", strServiceName));
		return false;
	}//if

	//서비스를 시작하고 대기
	if(StartService(hSrv, 0, NULL) == TRUE)
	{
		QueryServiceStatus(hSrv, &ss);
		while(ss.dwCurrentState != SERVICE_RUNNING)
		{
			Sleep(ss.dwWaitHint);
			QueryServiceStatus(hSrv, &ss);
		}//while

		CloseServiceHandle(hSrv);
		CloseServiceHandle(hScm);

		return true;
	}//if
	
	CloseServiceHandle(hSrv);
	CloseServiceHandle(hScm);

	ciDEBUG(1, ("Fail to start service = %s", strServiceName));
	
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 서비스를 중지한다. \n
/// @param (CString) strServiceName : (in) 중지하려는 서비스 명
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::StopSvc(CString strServiceName)
{
	SERVICE_STATUS ss;
	SC_HANDLE hScm, hSrv;
	hScm = OpenSCManager(NULL, NULL, GENERIC_READ);
	if(hScm == NULL)
	{
		ciDEBUG(1, ("Fail to open SCManager"));
		return false;
	}//if

	hSrv = OpenService(hScm, strServiceName, SERVICE_STOP);
	if(hSrv == NULL)
	{
		ciDEBUG(1, ("Fail to open service = %s", strServiceName));
		return false;
	}//if

	QueryServiceStatus(hSrv, &ss);
	if(ss.dwCurrentState == SERVICE_STOPPED)
	{
		ciDEBUG(1, ("Already stoped service = %s", strServiceName));
		return true;
	}//if
	
	if(ControlService(hSrv, SERVICE_CONTROL_STOP, &ss) == TRUE)
	{
		CloseServiceHandle(hSrv);
		CloseServiceHandle(hScm);

		return true;
	}//if
	
	CloseServiceHandle(hSrv);
	CloseServiceHandle(hScm);

	ciDEBUG(1, ("Fail to stop service = %s", strServiceName));
	
	return false;
}


////////////////////////////////////////////////////////////////////////////////////////
//인증 모듈에서 처리해야하는 기능들
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// SiteID를 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
/*
bool CUBCInstallDlg::SetSiteId(void)
{
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VNC admin properties 파일을 변경한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::SetVncAdminPoperties(void)
{
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// VNC background 파일을 변경한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::ChangeVncBackgroundFile(void)
{
	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컴퓨터 이름을 변경한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CUBCInstallDlg::ChangeComputerName(void)
{
	return true;
}
*/
////////////////////////////////////////////////////////////////////////////////////////

bool CUBCInstallDlg::IsExistNewCodecInstallFile()
{
	CString strSrc, strTarget;
	strSrc.Format(_T("%sCodec\\Install\\klcodec_10.2.exe"), Get3rdPartyDir());

	if( _access(strSrc,0)==0 ) return true;

	return false;
}
