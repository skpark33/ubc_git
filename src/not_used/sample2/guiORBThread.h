#pragma once

#include <ci/libThread/ciThread.h>


////////////////////
// guiORBThread

class guiORBThread : public virtual ciThread {
private:
	bool _isAlive;

public:
	bool isWorking;

	guiORBThread();
	~guiORBThread();

	void run();
	void destroy();
};
