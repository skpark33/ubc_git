#include <cci/libWrapper/cciEvent.h>
#include "sampleEventHandler.h"


ciSET_DEBUG(10, "sampleEventHandler");

void
sampleEventHandler::processEvent(cciEvent& aEvent)
{
	ciDEBUG(5,("processEvent()"));
 
	ciString eventName = aEvent.getEventType();
	if(eventName != "defaultScheduleUpdated") {
		ciERROR(("Wrong Event %s", eventName.c_str()));
		return ;
	}

	unsigned long eventTime = aEvent.getEventTime();

	ciString siteId;
	if(!aEvent.getItem("siteId", siteId)) {
		ciERROR(("%s Event does not have siteId", eventName.c_str()));
		return ;
	}
	ciString hostId;
	if(!aEvent.getItem("siteId", siteId)) {
		ciERROR(("%s Event does not have hostId", eventName.c_str()));
		return ;
	}
	ciString scheduleId;
	if(!aEvent.getItem("scheduleId", scheduleId)) {
		ciERROR(("%s Event does not have scheduletId", eventName.c_str()));
		return ;
	}
	
	ciLong castingState;
	if(!aEvent.getItem("castingState", castingState)) {
		ciERROR(("%s Event does not have castingState", eventName.c_str()));
		return ;
	}

	printf("EVENT RECEIVED such as ...-----------------------\n");
	printf("Event : %s\n", eventName.c_str());
	printf("EventTime : %ld\n", eventTime);
	printf("id : %s/%s/%s\n", siteId.c_str(), hostId.c_str(), scheduleId.c_str());
	printf("castingState : %ld\n", castingState);
	printf("-------------------------------------------------------\n");



	return;
}

