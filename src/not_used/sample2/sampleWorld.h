 /*! \file sampleWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _sampleWorld_h_
#define _sampleWorld_h_


#include <cci/libWorld/cciORBWorld.h> 


class sampleWorld : public cciORBWorld {
public:
	sampleWorld();
	~sampleWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

	static int sample_call(char* arg);
	static int sample_prop(char* arg);
	static int sample_post(char* arg);
	static int sample_register(char* arg);
	static int sample_deregister(char* arg);
	static int sample_retrieve(char* arg);
	static int sample_create(char* arg);
	
	static ciLong _eventId;
	

};
		
#endif //_sampleWorld_h_
