﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Password.Sha256
{
    public class fnSha256
    {
        public static String Password_Sha256(String pwd)
        {
            Console.Write(pwd);
            
            byte[] pwdbyte = Encoding.UTF8.GetBytes(pwd);
            SHA256Managed sha256 = new System.Security.Cryptography.SHA256Managed();
            byte[] output = sha256.ComputeHash(pwdbyte);
            String retval = Convert.ToBase64String(output);
            Console.Write(retval);
            return retval;
        }

        public static int Password_Sha256_Test(int dummy)
        {
            String pwd = "abcdefg";
            Console.Write(pwd);

            byte[] pwdbyte = Encoding.UTF8.GetBytes(pwd);
            SHA256Managed sha256 = new System.Security.Cryptography.SHA256Managed();
            byte[] output = sha256.ComputeHash(pwdbyte);
            String retval = Convert.ToBase64String(output);
            Console.Write(retval);
            return 5;
        }
    }
}
