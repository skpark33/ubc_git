#pragma once
#include <windows.h>
#include <atlbase.h>
#include <mscoree.h>
#include <comutil.h>
#include <mscoree.h>

#import "C:\Windows\Microsoft.NET\Framework\v2.0.50727\mscorlib.tlb" raw_interfaces_only

class CzCallManagedCode
{
public:
	CzCallManagedCode(void);
	~CzCallManagedCode(void);

public:
	HRESULT CallManagedFunction(BSTR szMethodName, int iNoOfParams, VARIANT* pvArgs, VARIANT* pvRet);
	HRESULT Open(LPCTSTR szAsseblyName, LPCTSTR szClassNameWithNamespace);
	void Close();

private:
	CComPtr<ICorRuntimeHost>	_pRuntimeHost;
	CComPtr<IDispatch>			_pDisp;
};
