// Demo_CallCSharpClassLibrary.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include "zCallManagedCode.h"
using namespace std;

bool Call_Password_Sha256_Encoding(string& password, string& encodedPassword)
{
	bool retval = false;

	CzCallManagedCode zCallManagedCode;

	// 원형 : int = sum(int, int)

	VARIANT vParam[1];		// 전달될 변수
	VARIANT vRet;			// 리턴 받을 변수

	vParam[0].vt = VT_INT;	// 첫번째 인자 구성
	vParam[0].intVal = 5;

	// 첫번째는 확장자를 제외한 파일명, 두번째는 Namespace.ClassName
	HRESULT hr = zCallManagedCode.Open(L"fnSha256", L"Password.Sha256.fnSha256");
	if(SUCCEEDED(hr))
	{
		// Sum 함수 호출, vParam 전달하고 vRet로 리턴
		hr = zCallManagedCode.CallManagedFunction(L"Password_Sha256", 1, vParam, &vRet);
		if(SUCCEEDED(hr))
		{
			// 호출 결과 화면에 표시
			//printf("Call Managed Code (C#) - int = sum(int, int) | %d+%d=%d\n", vParam[0].intVal, vParam[1].intVal, vRet.intVal);
			retval = true;
		}
		// 리소스 해제
		zCallManagedCode.Close();
	}
	return retval;
}

