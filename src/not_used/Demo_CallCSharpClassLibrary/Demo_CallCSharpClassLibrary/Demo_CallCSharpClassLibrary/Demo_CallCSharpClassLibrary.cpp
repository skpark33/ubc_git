// Demo_CallCSharpClassLibrary.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include "zCallManagedCode.h"
using namespace std;

bool Call_Password_Sha256_Encoding(string& password, string& encodedPassword);

int _tmain(int argc, _TCHAR* argv[])
{
	CzCallManagedCode zCallManagedCode;

	// 원형 : int = sum(int, int)

	VARIANT vParam[2];		// 전달될 변수
	VARIANT vRet;			// 리턴 받을 변수

	vParam[0].vt = VT_INT;	// 첫번째 인자 구성
	vParam[0].intVal = 5;

	vParam[1].vt = VT_INT;	// 두번째 인자 구성
	vParam[1].intVal = 6;

	// 첫번째는 확장자를 제외한 파일명, 두번째는 Namespace.ClassName
	HRESULT hr = zCallManagedCode.Open(L"HelloREKCAHZ", L"REKCAHZ.HelloREKCHAZ");
	if(SUCCEEDED(hr))
	{
		// Sum 함수 호출, vParam 전달하고 vRet로 리턴
		hr = zCallManagedCode.CallManagedFunction(L"Sum", 2, vParam, &vRet);
		if(SUCCEEDED(hr))
		{
			// 호출 결과 화면에 표시
			printf("Call Managed Code (C#) - int = sum(int, int) | %d+%d=%d\n", vParam[0].intVal, vParam[1].intVal, vRet.intVal);
		}else{
			printf("Call Managed Code fail\n");
		}
		
		// 리소스 해제
		zCallManagedCode.Close();
	}else{
		printf("Open Managed Code fail\n");
	}

	string pwd="abcdefg";
	string encoded;
	Call_Password_Sha256_Encoding(pwd,encoded);

	return 0;

}

bool Call_Password_Sha256_Encoding(string& password, string& encodedPassword)
{
	bool retval = false;

	CzCallManagedCode zCallManagedCode;

	// 원형 : int = sum(int, int)

	VARIANT vParam[1];		// 전달될 변수
	VARIANT vRet;			// 리턴 받을 변수

	vParam[0].vt = VT_INT;	// 첫번째 인자 구성
	vParam[0].intVal = 0;//(BSTR*)strdup(password.c_str());

	// 첫번째는 확장자를 제외한 파일명, 두번째는 Namespace.ClassName
	HRESULT hr = zCallManagedCode.Open(L"HelloREKCAHZ", L"Password.Sha256.fnSha256");
	if(SUCCEEDED(hr))
	{
		// Sum 함수 호출, vParam 전달하고 vRet로 리턴
		hr = zCallManagedCode.CallManagedFunction(L"Password_Sha256_Test", 1, vParam, &vRet);
		if(SUCCEEDED(hr))
		{
			// 호출 결과 화면에 표시
			//printf("Call Managed Code (C#) (%s=%s\n)",  vParam[0].pcVal, vRet.pcVal);
			printf("Call Managed Code (C#) (%d=%d\n)",  vParam[0].intVal, vRet.intVal);
			retval = true;
		}else{
			printf("Call Managed Code fail 2\n");
		}
		// 리소스 해제
		zCallManagedCode.Close();
	}else{
		printf("Open Managed Code fail 2\n");
	}
	return retval;
}

