#include "stdafx.h"
#include "zCallManagedCode.h"

#pragma comment(lib,"mscoree.lib")

CzCallManagedCode::CzCallManagedCode(void)
{
	_pRuntimeHost = NULL;
	_pDisp		  = NULL;
}

CzCallManagedCode::~CzCallManagedCode(void)
{
}

HRESULT CzCallManagedCode::Open(LPCTSTR szAsseblyName, LPCTSTR szClassNameWithNamespace)
{
	
	HRESULT hr = S_OK;

	try
	{
		//Retrieve a pointer to the ICorRuntimeHost interface
		hr = CorBindToRuntimeEx(NULL,	//Specify the version of the runtime that will be loaded. 
								L"wks",//Indicate whether the server or workstation build should be loaded. 	
								//Control whether concurrent or non-concurrent garbage collection
								//Control whether assemblies are loaded as domain-neutral. 
								STARTUP_LOADER_SAFEMODE | STARTUP_CONCURRENT_GC, 
								CLSID_CorRuntimeHost,
								IID_ICorRuntimeHost,
								//Obtain an interface pointer to ICorRuntimeHost 
								(void**)&_pRuntimeHost);
		if(FAILED(hr)) throw hr;

		//Start the CLR
		hr = _pRuntimeHost->Start();
		if(FAILED(hr)) throw hr;

		CComPtr<IUnknown> pUnknown;

		//Retrieve the IUnknown default AppDomain
		CComPtr<mscorlib::_AppDomain> 	pDefAppDomain;
		hr = _pRuntimeHost->GetDefaultDomain(&pUnknown);
		if(FAILED(hr)) throw hr;

		hr = pUnknown->QueryInterface(&pDefAppDomain.p);
		if(FAILED(hr)) throw hr;

		_bstr_t _bstrAssemblyName(szAsseblyName);
		_bstr_t _bstrszClassNameWithNamespace(szClassNameWithNamespace);

		CComPtr<mscorlib::_ObjectHandle>	pObjectHandle;
		//Creates an instance of the Assembly
		hr = pDefAppDomain->CreateInstance( _bstrAssemblyName,
											_bstrszClassNameWithNamespace,
											&pObjectHandle);
		if(FAILED(hr)) throw hr;

		CComVariant VntUnwrapped;
		hr = pObjectHandle->Unwrap(&VntUnwrapped);
		if(FAILED(hr)) throw hr;

		if(VntUnwrapped.vt != VT_DISPATCH)	throw E_FAIL;
		_pDisp = VntUnwrapped.pdispVal;
	}
	catch(_com_error e)
	{
		hr = e.Error();
	}
	catch(HRESULT e)
	{
		hr = e;
	}

	return hr;
}

HRESULT CzCallManagedCode::CallManagedFunction(BSTR szMethodName, int iNoOfParams, VARIANT* pvArgs, VARIANT* pvRet)
{
	HRESULT hr = S_OK;

	try
	{	
		if(_pDisp == NULL) return S_FALSE;

		DISPID dispid;
		
		DISPPARAMS dispparamsArgs = {NULL, NULL, 0, 0};
		dispparamsArgs.cArgs = iNoOfParams;
		dispparamsArgs.rgvarg = pvArgs;

		(*pvRet).vt = VT_INT;

		hr = _pDisp->GetIDsOfNames(IID_NULL, 
									&szMethodName,
									1,
									LOCALE_SYSTEM_DEFAULT,
									&dispid);
		if(FAILED(hr)) throw hr;
		
		//Invoke the method on the Dispatch Interface
		hr = _pDisp->Invoke(dispid,
							IID_NULL,
							LOCALE_SYSTEM_DEFAULT,
							DISPATCH_METHOD,
							&dispparamsArgs,
							pvRet,
							NULL,
							NULL);
		if(FAILED(hr)) throw hr;
	}
	catch(_com_error e)
	{
		hr = e.Error();
	}
	catch(HRESULT e)
	{
		hr = e;
	}

	return hr;
}

void CzCallManagedCode::Close()
{
	if(_pRuntimeHost) _pRuntimeHost->Stop();

	_pDisp		  = NULL;
	_pRuntimeHost = NULL;
}