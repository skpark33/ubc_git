// FTPConnectionMonitor.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "FTPConnectionMonitor.h"

#include "AdminSocket.h"
#include "platform.h"
#include "conversion.h"

#include "Log.h"

CString		CFTPConnectionMonitor::m_strLogFilename = "log\\libFTPConnectionMonitor.log";

//#include <ci/libDebug/ciDebug.h>
//ciSET_DEBUG(10, "CFTPConnectionMonitor");


// CFTPConnectionMonitor


#define NUMCOLUMNS 6
#define COLUMN_ID 0
#define COLUMN_USER 1
#define COLUMN_IP 2
#define COLUMN_TRANSFERINIT 3
#define COLUMN_TRANSFERPROGRESS 4
#define COLUMN_TRANSFERSPEED 5

#define SPEED_MEAN_SECONDS 10


class CConnectionData
{
public:
	CConnectionData()
	{
		for (int i = 1; i < NUMCOLUMNS; i++)
			itemImages[i] = -1;
		itemImages[0] = 5;

		ResetSpeed();
	}

	~CConnectionData() { }
	int userid;
	unsigned int port;
	unsigned char transferMode;
	CString physicalFile;
	CString logicalFile;
	__int64 totalSize;
	__int64 currentOffset;
	unsigned int speed;
	
	int listIndex;
	CString columnText[NUMCOLUMNS];
	int itemImages[NUMCOLUMNS];

	inline void AddBytes(int bytes)
	{
		*current_speed += bytes;
		UpdateSpeed();
	}
	
	inline void UpdateSpeed()
	{
		speed = 0;
		int max = speedDidWrap ? SPEED_MEAN_SECONDS : (current_speed - speed_mean + 1);
		for (int i = 0; i < max; i++)
			speed += speed_mean[i];
		speed /= max;
	}

	inline void NextSpeed()
	{
		if (!*current_speed)
			UpdateSpeed();

		if ((++current_speed - speed_mean) >= SPEED_MEAN_SECONDS)
		{
			speedDidWrap = true;
			current_speed = speed_mean;
		}
		*current_speed = 0;
	}

	inline void ResetSpeed()
	{
		speedDidWrap = false;
		current_speed = speed_mean;
		*current_speed = 0;
	}

private:
	unsigned int speed_mean[SPEED_MEAN_SECONDS];
	unsigned int *current_speed;
	bool speedDidWrap;
};



IMPLEMENT_DYNAMIC(CFTPConnectionMonitor, CWnd)

CFTPConnectionMonitor::CFTPConnectionMonitor()
:	m_pAdminSocket ( NULL )
{
	static bool first_init = true;
	if(first_init)
	{
		first_init = false;
		__LOG_INIT__(m_strLogFilename, false);
	}

	//ciDEBUG(1, ("CFTPConnectionMonitor()"));
	__DEBUG__("CFTPConnectionMonitor()", NULL);

	m_nServerState = 0;
	m_nEdit = 0;

	m_showPhysical = true;

	m_nReconnectTimerID = 0;
	m_nReconnectCount = 0;

	m_nGetListInterval = 60*1000;
}

CFTPConnectionMonitor::~CFTPConnectionMonitor()
{
	//ciDEBUG(1, ("~CFTPConnectionMonitor()"));
	__DEBUG__("~CFTPConnectionMonitor()", NULL);
	CloseAdminSocket(false);
}


BEGIN_MESSAGE_MAP(CFTPConnectionMonitor, CWnd)
	ON_WM_CREATE()
	ON_WM_TIMER()
END_MESSAGE_MAP()



// CFTPConnectionMonitor 메시지 처리기입니다.



int CFTPConnectionMonitor::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	//ciDEBUG(1, ("OnCreate()-ip:%s,pwd:%s,port:%d", m_strServerIP, m_strServerPWD, m_nServerPort));
	__DEBUG__("OnCreate()", NULL);
	__DEBUG__("IP", m_strServerIP );
	__DEBUG__("PWD", m_strServerPWD );
	__DEBUG__("PORT", m_nServerPort );

	m_pAdminSocket = new CAdminSocket(this);
	ShowStatus("Connecting to server...", 0);
	m_pAdminSocket->Create();
	m_pAdminSocket->m_Password = m_strServerPWD;//m_pOptions->GetOption(IOPTION_LASTSERVERPASS);
	if (!m_pAdminSocket->Connect(m_strServerIP, m_nServerPort) && WSAGetLastError() != WSAEWOULDBLOCK)
	{
		//ciERROR( ("Error, could not connect to server"));
		__ERROR__("Error, could not connect to server", NULL);
		ShowStatus(_T("Error, could not connect to server"), 1);
		CloseAdminSocket();
	}

	SetTimer(1023, m_nGetListInterval, NULL);

	return 0;
}

void CFTPConnectionMonitor::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd::OnTimer(nIDEvent);

	if(nIDEvent == 1023)
	{
		//ciDEBUG(1, ("SendCommand(USERCONTROL_GETLIST)"));
		__DEBUG__("SendCommand(USERCONTROL_GETLIST)", NULL);

		unsigned char buffer = USERCONTROL_GETLIST;
		SendCommand(3, &buffer, 1);

		SetTimer(1023, m_nGetListInterval, NULL);
	}
	else if (nIDEvent == m_nReconnectTimerID)
	{
		KillTimer(m_nReconnectTimerID);
		m_nReconnectTimerID = 0;
		
		if (m_pAdminSocket)
			return;

		m_pAdminSocket = new CAdminSocket(this);
		//ciDEBUG(1, ("Reconnecting to server..."));
		__DEBUG__("Reconnecting to server...", NULL);
		ShowStatus("Reconnecting to server...", 0);
		m_pAdminSocket->m_Password = m_strServerPWD;//m_pOptions->GetOption(IOPTION_LASTSERVERPASS);
		m_pAdminSocket->Create();
		if (!m_pAdminSocket->Connect(m_strServerIP, m_nServerPort) && WSAGetLastError()!=WSAEWOULDBLOCK)
		{
			//ciERROR(("Error, could not connect to server"));
			__ERROR__("Error, could not connect to server", NULL);
			ShowStatus(_T("Error, could not connect to server"), 1);
			CloseAdminSocket();
		}
	}
}

void CFTPConnectionMonitor::OnAdminInterfaceConnected()
{
	m_nReconnectCount = 0;

	CString title = "Connect";
	CString ip;
	UINT port;
	if (m_pAdminSocket->GetPeerName(ip, port))
	{
		//SetWindowText(title + _T(" (") + ip + _T(")"));
		//ciDEBUG(1, (title + _T(" (") + ip + _T(")")));
		__DEBUG__(title + _T(" (") + ip + _T(")"), NULL);
		ShowStatus(title + _T(" (") + ip + _T(")"), 0);
	}
}

void CFTPConnectionMonitor::CloseAdminSocket(bool shouldReconnect /*=true*/)
{
	if (m_pAdminSocket)
	{
		m_pAdminSocket->DoClose();
		delete m_pAdminSocket;
		m_pAdminSocket = NULL;
		//SetIcon();

		//CString title;
		//title.LoadString(IDR_MAINFRAME);
		//SetWindowText(title + _T(" (disconnected)"));
		ShowStatus(_T(" (disconnected)"), 0);
	}
	m_nEdit = 0;

	if (!shouldReconnect)
	{
		if (m_nReconnectTimerID)
		{
			KillTimer(m_nReconnectTimerID);
			m_nReconnectTimerID = 0;
		}
	}
	else
	{
		if (!m_nReconnectTimerID)
		{
			m_nReconnectCount++;
			//if (m_nReconnectCount < 15) // unlimit reconnect
			{
				//ciDEBUG(1, ("Trying to reconnect in 5 seconds"));
				__DEBUG__("Trying to reconnect in 5 seconds", NULL);
				ShowStatus("Trying to reconnect in 5 seconds", 0);
				m_nReconnectTimerID = SetTimer(7779, 5000, 0);
			}
			//else
			//	m_nReconnectCount = 0;
		}
	}
}

void CFTPConnectionMonitor::ParseReply(int nReplyID, unsigned char *pData, int nDataLength)
{
	switch(nReplyID)
	{
	case 0:
		{
			//ciDEBUG(1, ("Logged on"));
			__DEBUG__("Logged on", NULL);
			ShowStatus(_T("Logged on"), 0);
			SendCommand(2);
			unsigned char buffer = USERCONTROL_GETLIST;
			SendCommand(3, &buffer, 1);
		}
		break;
	case 1:
		{
			char *pBuffer = new char[nDataLength];
			memcpy(pBuffer, pData+1, nDataLength-1);
			pBuffer[nDataLength-1] = 0;
			ShowStatus(pBuffer, *pData);
			delete [] pBuffer;
		}
		break;
	case 2:
		m_nServerState = *pData*256 + pData[1];
		//SetIcon();
		break;
	case 3:
		{
			if (nDataLength<2)
			{
				//ciERROR(("Protocol error: Unexpected data length"));
				__ERROR__("Protocol error: Unexpected data length", NULL);
				ShowStatus(_T("Protocol error: Unexpected data length"), 1);
				return;
			}
			else if (!ParseUserControlCommand(pData, nDataLength))
			{
				//ciERROR(("Protocol error: Invalid data"));
				__ERROR__("Protocol error: Invalid data", NULL);
				ShowStatus(_T("Protocol error: Invalid data"), 1);
			}
		}
		break;
	case 5:
		if (nDataLength == 1)
		{
			if (*pData == 0)
			{
				//ciERROR(("Done sending settings."));
				__ERROR__("Done sending settings.", NULL);
				ShowStatus(_T("Done sending settings."), 0);
			}
			else if (*pData == 1)
			{
				//ciERROR(("Could not change settings"));
				__ERROR__("Could not change settings", NULL);
				ShowStatus(_T("Could not change settings"), 1);
			}
			break;
		}
		//ciERROR(("Done retrieving settings"));
		__ERROR__("Done retrieving settings", NULL);
		ShowStatus(_T("Done retrieving settings"), 0);
		if (nDataLength<2)
		{
			//ciERROR(("Protocol error: Unexpected data length"));
			__ERROR__("Protocol error: Unexpected data length", NULL);
			ShowStatus(_T("Protocol error: Unexpected data length"), 1);
		}
		else
		{
			if ((m_nEdit & 0x1C) == 0x04)
			{
				//m_pOptionsDlg = new COptionsDlg(m_pOptions, m_pAdminSocket->IsLocal());
				m_nEdit |= 0x08;
				//if (!m_pOptionsDlg->Init(pData, nDataLength))
				//{
				//	ShowStatus(_T("Protocol error: Invalid data"), 1);
				//	delete m_pOptionsDlg;
				//	m_pOptionsDlg = 0;
				//	m_nEdit = 0;
				//}
				//else
				if (!PostMessage(WM_APP))
				{
					//ciERROR(("Can't send window message"));
					__ERROR__("Can't send window message", NULL);
					ShowStatus(_T("Can't send window message"), 1);
					//delete m_pOptionsDlg;
					//m_pOptionsDlg = 0;
					m_nEdit = 0;
				}
			}
		}
		break;
	case 6:
		if (nDataLength == 1)
		{
			if (*pData == 0)
			{
				//ciERROR(("Done sending account settings."));
				__ERROR__("Done sending account settings.", NULL);
				ShowStatus(_T("Done sending account settings."), 0);
			}
			else if (*pData == 1)
			{
				//ciERROR(("Could not change account settings"));
				__ERROR__("Could not change account settings", NULL);
				ShowStatus(_T("Could not change account settings"), 1);
			}
			break;
		}
		//ciERROR(("Done retrieving account settings"));
		__ERROR__("Done retrieving account settings", NULL);
		ShowStatus(_T("Done retrieving account settings"), 0);
		if (nDataLength<2)
		{
			//ciERROR(("Protocol error: Unexpected data length"));
			__ERROR__("Protocol error: Unexpected data length", NULL);
			ShowStatus(_T("Protocol error: Unexpected data length"), 1);
		}
		else
		{
			if ((m_nEdit & 0x19) == 0x01)
			{
				//m_pUsersDlg = new CUsersDlg(this, m_pAdminSocket->IsLocal());
				m_nEdit |= 0x08;
				//if (!m_pUsersDlg->Init(pData, nDataLength))
				//{
				//	ShowStatus(_T("Protocol error: Invalid data"), 1);
				//	delete m_pUsersDlg;
				//	m_pUsersDlg = 0;
				//	m_nEdit = 0;
				//	break;
				//}
				//else
				if (!PostMessage(WM_APP))
				{
					//ciERROR(("Can't send window message"));
					__ERROR__("Can't send window message", NULL);
					ShowStatus(_T("Can't send window message"), 1);
					//delete m_pUsersDlg;
					//m_pUsersDlg = 0;
					m_nEdit = 0;
				}
			}
			if ((m_nEdit & 0x1A) == 0x02)
			{
				//m_pGroupsDlg = new CGroupsDlg(this, m_pAdminSocket->IsLocal());
				//m_nEdit |= 0x08;
				//if (!m_pGroupsDlg->Init(pData, nDataLength))
				//{
				//	ShowStatus(_T("Protocol error: Invalid data"), 1);
				//	delete m_pGroupsDlg;
				//	m_pGroupsDlg = 0;
				//	m_nEdit = 0;
				//	break;
				//}
				//else
				if (!PostMessage(WM_APP))
				{
					//ciERROR(("Can't send window message"));
					__ERROR__("Can't send window message", NULL);
					ShowStatus(_T("Can't send window message"), 1);
					//delete m_pGroupsDlg;
					//m_pGroupsDlg = 0;
					m_nEdit = 0;
				}
			}
		}
		break;
	case 8:
		break;
	default:
		{
			CString str;
			str.Format(_T("Protocol error: Unexpected reply id (%d)."), nReplyID);
			//ciERROR((str));
			__ERROR__(str, NULL);
			ShowStatus(str, 1);
			break;
		}
	}
}

void CFTPConnectionMonitor::ParseStatus(int nStatusID, unsigned char *pData, int nDataLength)
{
	switch(nStatusID)
	{
	case 1:
		{
			char *pBuffer = new char[nDataLength];
			memcpy(pBuffer, pData+1, nDataLength-1);
			pBuffer[nDataLength-1] = 0;
			ShowStatus(pBuffer, *pData);
			delete [] pBuffer;
		}
		break;
	case 2:
		m_nServerState = *pData*256 + pData[1];
		//SetIcon();
		break;
	case 3:
		{
			if (nDataLength<2)
			{
				//ciERROR(("Protocol error: Unexpected data length"));
				__ERROR__("Protocol error: Unexpected data length", NULL);
				ShowStatus(_T("Protocol error: Unexpected data length"), 1);
				return;
			}
			else if (!ParseUserControlCommand(pData, nDataLength))
			{
				//ciERROR(("Protocol error: Invalid data"));
				__ERROR__("Protocol error: Invalid data", NULL);
				ShowStatus(_T("Protocol error: Invalid data"), 1);
			}
		}
		break;
	case 4:
		{
			if (nDataLength < 10)
			{
				//ciERROR(("Protocol error: Unexpected data length"));
				__ERROR__("Protocol error: Unexpected data length", NULL);
				ShowStatus(_T("Protocol error: Unexpected data length"), 1);
				return;
			}
			char *buffer = new char[nDataLength - 9 + 160 + 1];
			unsigned char *p = pData + 9;
			char *q = buffer;
			int pos = 0;
			while ((pos + 9) < nDataLength)
			{
				if (*p == '-')
				{
					DWORD timeHigh = GET32(pData + 1);
					DWORD timeLow = GET32(pData + 5);

					FILETIME fFileTime;
					fFileTime.dwHighDateTime = timeHigh;
					fFileTime.dwLowDateTime = timeLow;

					SYSTEMTIME sFileTime;
					FileTimeToSystemTime(&fFileTime, &sFileTime);
					
					*(q++) = ' ';
					int res = GetDateFormat(
							LOCALE_USER_DEFAULT,	// locale for which date is to be formatted
							DATE_SHORTDATE,			// flags specifying function options
							&sFileTime,				// date to be formatted
							0,						// date format string
							q,						// buffer for storing formatted string
							80						// size of buffer
						);

					if (res)
					{
						q += res - 1;
						*(q++) = ' ';
					}
	
					res = GetTimeFormat(
							LOCALE_USER_DEFAULT,	// locale for which date is to be formatted
							TIME_FORCE24HOURFORMAT,	// flags specifying function options
							&sFileTime,				// date to be formatted
							0,						// date format string
							q,						// buffer for storing formatted string
							80						// size of buffer
						);

					if (res)
					{
						q += res - 1;
						*(q++) = ' ';
					}

					if ((nDataLength - pos - 9) > 0)
					{
						memcpy(q, p, nDataLength - pos - 9);
						q += nDataLength - pos - 9;
					}
					break;
				}
				else
					*(q++) = *(p++);

				pos++;
			}
			*q = 0;
			if (q != buffer)
				ShowStatus(buffer, *pData);
			delete [] buffer;
		}
		break;
	case 7:
		if (nDataLength != 5)
		{
			//ciERROR(("Protocol error: Invalid data"));
			__ERROR__("Protocol error: Invalid data", NULL);
			ShowStatus(_T("Protocol error: Invalid data"), 1);
		}
		else
		{
			int nType = *pData;
			int size = (int)GET32(pData + 1);
			
			if (!nType)
			{
				//m_nRecvCount += size;
				//m_RecvLed.Ping(100);
				//CString str;
				//str.Format("%s bytes received", makeUserFriendlyString(m_nRecvCount).GetString());
				//SetStatusbarText(m_wndStatusBar.CommandToIndex(ID_INDICATOR_RECVCOUNT), str);
			}
			else
			{
				//m_nSendCount += size;
				//m_SendLed.Ping(100);
				//CString str;
				//str.Format("%s bytes sent", makeUserFriendlyString(m_nSendCount).GetString());
				//SetStatusbarText(m_wndStatusBar.CommandToIndex(ID_INDICATOR_SENDCOUNT), str);
			}

		}
		break;
	default:
		{
			CString str;
			str.Format(_T("Protocol error: Unexpected status id (%d)."), nStatusID);
			//ciERROR((str));
			__ERROR__(str, NULL);
			ShowStatus(str, 1);
		}
		break;

	}
}

BOOL CFTPConnectionMonitor::SendCommand(int nType)
{
	if (!m_pAdminSocket)
		return FALSE;
	if (!m_pAdminSocket->SendCommand(nType))
	{
		CloseAdminSocket();
		//ciERROR(("Error: Connection to server lost..."));
		__ERROR__("Error: Connection to server lost...", NULL);
		ShowStatus("Error: Connection to server lost...", 1);
		return FALSE;
	}
	return TRUE;
}

BOOL CFTPConnectionMonitor::SendCommand(int nType, void *pData, int nDataLength)
{
	if (!m_pAdminSocket)
		return FALSE;
	if (!m_pAdminSocket->SendCommand(nType, pData, nDataLength))
	{
		CloseAdminSocket();
		//ciERROR(("Error: Connection to server lost..."));
		__ERROR__("Error: Connection to server lost...", NULL);
		ShowStatus("Error: Connection to server lost...", 1);
		return FALSE;
	}
	return TRUE;
}

BOOL CFTPConnectionMonitor::ParseUserControlCommand(unsigned char *pData, DWORD dwDataLength)
{
	int type = *pData;
	if (type < 0 || type > 4)
	{
		//ciERROR(("Protocol error: Invalid data"));
		__ERROR__("Protocol error: Invalid data", NULL);
		ShowStatus(_T("Protocol error: Invalid data"), 1);
		return FALSE;
	}
	switch (type)
	{
	case USERCONTROL_GETLIST:
		if (dwDataLength < 3)
			return FALSE;
		else
		{
			for (ConnectionDataArrayIter iter = m_connectionDataArray.begin(); iter != m_connectionDataArray.end(); iter++)
				delete *iter;

			m_connectionDataMap.clear();
			m_connectionDataArray.clear();
			
			int num = pData[1] * 256 + pData[2];
			unsigned int pos = 3;
			for (int i = 0; i < num; i++)
			{
				if ((pos + 6) > dwDataLength)
					return FALSE;
				CConnectionData* pConnectionData = new CConnectionData;;
				memcpy(&pConnectionData->userid, pData+pos, 4);
				pos += 4;
				int len = pData[pos] * 256 + pData[pos+1];
				pos+=2;
				if (pos+len > dwDataLength)
				{
					delete pConnectionData;
					return FALSE;
				}

				char* ip = new char[len + 1];
				memcpy(ip, pData + pos, len);
				ip[len] = 0;
				pos += len;
#ifdef _UNICODE
				pConnectionData->columnText[COLUMN_IP] = ConvFromNetwork(ip);
#else
				pConnectionData->columnText[COLUMN_IP] = ConvToLocal(ConvFromNetwork(ip));
#endif
				delete [] ip;
				
				if ((pos+6) > dwDataLength)
				{
					delete pConnectionData;
					return FALSE;
				}
				memcpy(&pConnectionData->port, pData+pos, 4);
				
				pos+=4;
				
				len = pData[pos] * 256 + pData[pos+1];
				pos+=2;
				if ((pos + len + 1) > dwDataLength)
				{
					delete pConnectionData;
					return FALSE;
				}

				char* user = new char[len + 1];
				memcpy(user, pData + pos, len);
				user[len] = 0;
				pos += len;
#ifdef _UNICODE
				pConnectionData->columnText[COLUMN_USER] = ConvFromNetwork(user);
#else
				pConnectionData->columnText[COLUMN_USER] = ConvToLocal(ConvFromNetwork(user));
#endif
				delete [] user;

				pConnectionData->transferMode = pData[pos++];

				if (pConnectionData->transferMode)
				{
					if ((pos + 2) > dwDataLength)
					{
						delete pConnectionData;
						return FALSE;
					}
					len = pData[pos] * 256 + pData[pos+1];
					pos += 2;

					if ((pos+len) > dwDataLength)
					{
						delete pConnectionData;
						return FALSE;
					}

					char* physicalFile = new char[len + 1];
					memcpy(physicalFile, pData + pos, len);
					physicalFile[len] = 0;
					pos += len;
#ifdef _UNICODE
					pConnectionData->physicalFile = ConvFromNetwork(physicalFile);
#else
					pConnectionData->physicalFile = ConvToLocal(ConvFromNetwork(physicalFile));
#endif
					delete [] physicalFile;

					if ((pos + 2) > dwDataLength)
					{
						delete pConnectionData;
						return FALSE;
					}
					len = pData[pos] * 256 + pData[pos+1];
					pos += 2;

					
					if ((pos+len) > dwDataLength)
					{
						delete pConnectionData;
						return FALSE;
					}

					char* logicalFile = new char[len + 1];
					memcpy(logicalFile, pData + pos, len);
					logicalFile[len] = 0;
					pos += len;
#ifdef _UNICODE
					pConnectionData->logicalFile = ConvFromNetwork(logicalFile);
#else
					pConnectionData->logicalFile = ConvToLocal(ConvFromNetwork(logicalFile));
#endif
					delete [] logicalFile;

					if (pConnectionData->transferMode & 0x20)
					{
						memcpy(&pConnectionData->currentOffset, pData + pos, 8);
						pos += 8;
					}
					else
						pConnectionData->currentOffset = 0;

					if (pConnectionData->transferMode & 0x40)
					{
						memcpy(&pConnectionData->totalSize, pData + pos, 8);
						pos += 8;
					}
					else
						pConnectionData->totalSize = -1;

					// Filter out indicator bits
					pConnectionData->transferMode &= 0x9F;
				}
				else
				{
					pConnectionData->currentOffset = 0;
					pConnectionData->totalSize = -1;
				}

				pConnectionData->columnText[COLUMN_ID].Format("%06d", pConnectionData->userid);
				m_connectionDataMap[pConnectionData->userid] = pConnectionData;
				pConnectionData->listIndex = m_connectionDataArray.size();
				m_connectionDataArray.push_back(pConnectionData);

				if (pConnectionData->columnText[COLUMN_USER] == "")
					pConnectionData->columnText[COLUMN_USER] = "(not logged in)";

				pConnectionData->itemImages[COLUMN_TRANSFERINIT] = pConnectionData->transferMode;
				pConnectionData->columnText[COLUMN_TRANSFERINIT] = m_showPhysical ? pConnectionData->physicalFile : pConnectionData->logicalFile;

				//ciDEBUG(1, ("id:%d,ip:%s,user:%s,id:%s", pConnectionData->userid, pConnectionData->columnText[COLUMN_IP], pConnectionData->columnText[COLUMN_USER], pConnectionData->columnText[COLUMN_ID] ));
				//__DEBUG__("pConnectionData->userid", pConnectionData->userid);
				//__DEBUG__("pConnectionData->ip", pConnectionData->columnText[COLUMN_IP]);
				//__DEBUG__("pConnectionData->user", pConnectionData->columnText[COLUMN_USER]);
				//__DEBUG__("pConnectionData->id", pConnectionData->columnText[COLUMN_ID]);
			}
			CString msg;
			msg.Format("IP:%s,Conn:%d", m_strServerIP, num);
			__DEBUG__("ConnectionInfo", msg );
			//SetSortColumn(m_sortColumn, m_sortDir);
			//SetItemCount(m_connectionDataArray.size());
			//m_pOwner->SetIcon();
		}
		break;
	case USERCONTROL_CONNOP:
		//return ProcessConnOp(pData, dwDataLength);
		break;
	case USERCONTROL_KICK:
	case USERCONTROL_BAN:
		break;
	default:
		//ciERROR(("Protocol error: Specified usercontrol option not implemented"));
		__ERROR__("Protocol error: Specified usercontrol option not implemented", NULL);
		ShowStatus(_T("Protocol error: Specified usercontrol option not implemented"), 1);
		return FALSE;
		break;
	}
	return TRUE;
}

BOOL CFTPConnectionMonitor::Create(LPCTSTR lpszIP, LPCTSTR lpszPWD, int nPort, int nGetListInterval)
{
	m_strServerIP = lpszIP;
	m_strServerPWD = lpszPWD;
	m_nServerPort = nPort;
	m_nGetListInterval = nGetListInterval;

	CWnd* wnd = CWnd::FromHandle( ::GetDesktopWindow() );
	return CWnd::Create(NULL, _T("ftp"), WS_CHILD | WS_CAPTION /*| WS_VISIBLE*/, CRect(0,0,1,1), wnd, 0xfeff);	// 작업표시줄에 윈도우 생성 (ipc 수신 땜시...)
}
