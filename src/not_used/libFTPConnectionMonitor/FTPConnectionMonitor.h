#pragma once


// CFTPConnectionMonitor

#include <list>
#include <map>
#include <vector>


class CAdminSocket;
class CConnectionData;

typedef std::map<int, CConnectionData*>	ConnectionDataMap;
typedef	std::vector<CConnectionData*>	ConnectionDataArray;
typedef	ConnectionDataArray::iterator	ConnectionDataArrayIter;


class CFTPConnectionMonitor : public CWnd
{
	DECLARE_DYNAMIC(CFTPConnectionMonitor)

public:
	CFTPConnectionMonitor();
	virtual ~CFTPConnectionMonitor();

	static CString	m_strLogFilename;

protected:
	DECLARE_MESSAGE_MAP()

	CString		m_strStatus;
	bool		m_showPhysical;

	UINT_PTR	m_nReconnectTimerID;
	int			m_nReconnectCount;
	int			m_nServerState;
	int			m_nEdit;

	CString		m_strServerIP;
	CString		m_strServerPWD;
	int			m_nServerPort;
	int			m_nGetListInterval;

	CAdminSocket*	m_pAdminSocket;

	ConnectionDataMap	m_connectionDataMap;
	ConnectionDataArray	m_connectionDataArray;

	BOOL	ParseUserControlCommand(unsigned char *pData, DWORD dwDataLength);

public:

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void	ShowStatus(LPCTSTR lpszStatus, int nCode) { m_strStatus=lpszStatus; };

	//
	void	OnAdminInterfaceConnected();
	void	CloseAdminSocket(bool shouldReconnect=true);

	void	ParseReply(int nReplyID, unsigned char *pData, int nDataLength);
	void	ParseStatus(int nStatusID, unsigned char *pData, int nDataLength);
	BOOL	SendCommand(int nType);
	BOOL	SendCommand(int nType, void *pData, int nDataLength);

	//
	BOOL	Create(LPCTSTR lpszIP, LPCTSTR lpszPWD, int nPort=14147, int nGetListInterval=10000);

	LPCTSTR	GetStatusText() { return (LPCTSTR)m_strStatus; };
	int		GetConnCount() { if(m_pAdminSocket) return m_connectionDataArray.size(); else return -1;};
};
