//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HandInput.rc
//
#define IDCANCEL2                       3
#define IDCANCEL3                       4
#define IDCANCEL4                       5
#define IDCANCEL5                       6
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDB_BITMAP_SEND                 130
#define IDB_BITMAP_CLEAR                131
#define IDD_DIALOG_HANDINPUT            132
#define IDD_DIALOG_SerialInput          132
#define IDD_DIALOG_SERIALINPUT          132
#define IDC_BUTTON1                     1002
#define IDC_EDIT_INPUT_NUMBER           1003
#define IDC_EDIT_NUMBER1                1006
#define IDC_EDIT_NUMBER2                1007
#define IDC_EDIT_NUMBER3                1008
#define IDC_EDIT_NUMBER4                1009
#define IDC_EDIT_NUMBER5                1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
