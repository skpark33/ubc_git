// SerialInputDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HandInput.h"
#include "SerialInputDlg.h"


// CSerialInputDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSerialInputDlg, CDialog)

CSerialInputDlg::CSerialInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSerialInputDlg::IDD, pParent)
{

}

CSerialInputDlg::~CSerialInputDlg()
{
}

void CSerialInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_INPUT_NUMBER, m_editInput);
	DDX_Control(pDX, IDC_EDIT_NUMBER1, m_editNumber1);
	DDX_Control(pDX, IDC_EDIT_NUMBER2, m_editNumber2);
	DDX_Control(pDX, IDC_EDIT_NUMBER3, m_editNumber3);
	DDX_Control(pDX, IDC_EDIT_NUMBER4, m_editNumber4);
	DDX_Control(pDX, IDC_EDIT_NUMBER5, m_editNumber5);
	DDX_Control(pDX, IDOK, m_btSend);
	DDX_Control(pDX, IDCANCEL, m_btCancel1);
	DDX_Control(pDX, IDCANCEL2, m_btCancel2);
	DDX_Control(pDX, IDCANCEL3, m_btCancel3);
	DDX_Control(pDX, IDCANCEL4, m_btCancel4);
	DDX_Control(pDX, IDCANCEL5, m_btCancel5);
}


BEGIN_MESSAGE_MAP(CSerialInputDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSerialInputDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSerialInputDlg::OnBnClickedCancel1)
	ON_BN_CLICKED(IDCANCEL2, &CSerialInputDlg::OnBnClickedCancel2)
	ON_BN_CLICKED(IDCANCEL3, &CSerialInputDlg::OnBnClickedCancel3)
	ON_BN_CLICKED(IDCANCEL4, &CSerialInputDlg::OnBnClickedCancel4)
	ON_BN_CLICKED(IDCANCEL5, &CSerialInputDlg::OnBnClickedCancel5)
END_MESSAGE_MAP()


// CSerialInputDlg 메시지 처리기입니다.

void CSerialInputDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnOK();
}

void CSerialInputDlg::OnBnClickedCancel1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CSerialInputDlg::OnBnClickedCancel2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CSerialInputDlg::OnBnClickedCancel3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CSerialInputDlg::OnBnClickedCancel4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CSerialInputDlg::OnBnClickedCancel5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
