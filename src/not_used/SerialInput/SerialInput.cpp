// HandInput.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "HandInput.h"
#include "HandInputDlg.h"
#include "WinFireWall.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#   include <winsock2.h>
//  WinSock DLL을 사용할 버전
#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0
#	define BUF_LEN 128

// CHandInputApp

BEGIN_MESSAGE_MAP(CHandInputApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CHandInputApp 생성

CHandInputApp::CHandInputApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CHandInputApp 개체입니다.

CHandInputApp theApp;


// CHandInputApp 초기화

BOOL CHandInputApp::InitInstance()
{
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("로컬 응용 프로그램 마법사에서 생성된 응용 프로그램"));

	char	szBuf[_MAX_PATH] = { 0x00 };
	::GetModuleFileName(NULL, szBuf, _MAX_PATH);

	if(!this->IsAppExceptionAdded(szBuf)){
		this->AddToExceptionList(szBuf);
	}

	::AfxBeginThread(runUdpThread, this);


	CHandInputDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

CString CHandInputApp::_serialConfigFile;
string CHandInputApp::_currentSerialServerIp;

UINT
CHandInputApp::runUdpThread(LPVOID pParam)
{
	//CHandInputApp* pApp = (CHandInputApp*)pParam;

	int _port = 14008;

	_serialConfigFile = GetConfigFile();

	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA       WsaData;            // receives data from WSAStartup 
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {		
		return 0;
	}
	
	struct sockaddr_in server_addr, client_addr;	// socket address
	
	// socket 생성
	SOCKET server_fd = socket( AF_INET, SOCK_DGRAM, 0 );
	memset( &server_addr, 0, sizeof(struct sockaddr) );
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(_port);

	// bind() 호출
	if( bind(server_fd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) < 0 ){
		return 0;
	}

	while(1) {
		char buf[BUF_LEN+1];
		memset(buf,0x00,BUF_LEN);
		// 연결요청을 기다림
		int len = sizeof(client_addr);
		int recvLen = recvfrom(server_fd,buf,BUF_LEN,0,(struct sockaddr *)&client_addr, &len);
		if(recvLen < 0) {
			continue;
		}
		buf[recvLen]=0;
		string clientIp = inet_ntoa(client_addr.sin_addr);

		if (strcmp(buf,"serial")==0) {

			if(_currentSerialServerIp.empty()){
				getCurrentSerialServerIP();
			}

			char res[BUF_LEN];
			memset(res,0x00,BUF_LEN);

			if(_currentSerialServerIp.empty()) {
				sprintf(res,"NO");
			}else{
				if(_currentSerialServerIp != clientIp){
					changeSerialServerIP(clientIp.c_str());
				}
				sprintf(res,"OK");
			}

			int sendLen = sendto(server_fd,res,BUF_LEN,0,(struct sockaddr *)&client_addr, len);
			if(sendLen<0){
				continue;
			}
	
		}
	}

	closesocket(server_fd);
	WSACleanup();		
	return 1;
}

int
CHandInputApp::getCurrentSerialServerIP()
{
	FILE* fp_r = fopen(_serialConfigFile, "r");
	if(!fp_r){
		return 0;
	}

	char buf[1024];
	while(fgets(buf,sizeof(buf)-1, fp_r)){
		string buf2 = buf;
		int i1 = buf2.find("<serverurl>");
		if(i1>0){
			i1 += strlen("<serverurl>");
			int i2 = buf2.find("</serverurl>");
			if(i2>i1){
				_currentSerialServerIp = buf2.substr(i1,i2-i1);
				break;
			}
		}
	}
	fclose(fp_r);
	return 1;
}

int
CHandInputApp::changeSerialServerIP(const char* ip)
{
	FILE* fp_r = fopen(_serialConfigFile, "r");
	if(!fp_r){
		return 0;
	}
	_currentSerialServerIp = ip;
	list<string> aList;
	char buf[1024];
	while(fgets(buf,sizeof(buf)-1, fp_r)){
		string buf2 = buf;
		int i1 = buf2.find("<serverurl>");
		if(i1>0){
			string line = "\t<serverurl>" + _currentSerialServerIp + "</serverurl>\n";
			aList.push_back(line);
		}else{
			aList.push_back(buf);
		}

	}
	fclose(fp_r);

	FILE* fp_w = fopen(_serialConfigFile, "w");
	if(!fp_w){
		return 0;
	}
	list<string>::iterator itr;
	for(itr=aList.begin();itr!=aList.end();itr++){
		fputs(itr->c_str(),fp_w);
	}
	fclose(fp_w);
	return 1;
}


CString CHandInputApp::GetConfigFile()
{
	CString strDir;
	char	szBuf[_MAX_PATH] = { 0x00 };

	::GetModuleFileName(NULL, szBuf, _MAX_PATH);
	strDir.Format("%s", szBuf);
	strDir.MakeReverse();
	int nIndex = strDir.Find("\\");
	if (nIndex < 0) return "";

	if (strDir.GetLength() < (nIndex+1)) return "";

	strDir = strDir.Mid(nIndex + 1);
	strDir.MakeReverse();

	strDir.Append("\\xml\\Configure.xml");

	return strDir;
}


bool CHandInputApp::AddToExceptionList(string strAppPath)
{
	CoInitialize(NULL);

	string strAppName;

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(strAppPath.c_str(), szDrive, szPath, szFilename, szExt);
	strAppName = szFilename;
	strAppName += szExt;

	CWinFireWall clsWall;
	if(clsWall.Initialize() != FW_NOERROR)
	{
		return false;
	}//if
	
	WCHAR szAppPath[MAX_PATH] = { 0x00 };
	MultiByteToWideChar(CP_ACP, 0, strAppPath.c_str(), -1, szAppPath, MAX_PATH);

	WCHAR szAppName[MAX_PATH] = { 0x00 };
	MultiByteToWideChar(CP_ACP, 0, strAppName.c_str(), -1, szAppName, MAX_PATH);

	if(clsWall.AddApplication(szAppPath, szAppName) != FW_NOERROR)
	{
		return false;
	}//if

	return true;
}

bool CHandInputApp::IsAppExceptionAdded(string strAppPath)
{
	CoInitialize(NULL);

	CWinFireWall clsWall;
	if(clsWall.Initialize() != FW_NOERROR)
	{
		return false;
	}//if

	WCHAR szAppPath[MAX_PATH] = { 0x00 };
	MultiByteToWideChar(CP_ACP, 0, strAppPath.c_str(), -1, szAppPath, MAX_PATH);

	BOOL bEnable;
	if(clsWall.IsAppEnabled(szAppPath, bEnable) != FW_NOERROR)
	{
		return false;
	}//if

	return (bool)bEnable;
}


