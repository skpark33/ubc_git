#pragma once
#include "afxwin.h"


// CSerialInputDlg 대화 상자입니다.

class CSerialInputDlg : public CDialog
{
	DECLARE_DYNAMIC(CSerialInputDlg)

public:
	CSerialInputDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSerialInputDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_SERIALINPUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_editInput;
	CEdit m_editNumber1;
	CEdit m_editNumber2;
	CEdit m_editNumber3;
	CEdit m_editNumber4;
	CEdit m_editNumber5;
	CButton m_btSend;
	CButton m_btCancel1;
	CButton m_btCancel2;
	CButton m_btCancel3;
	CButton m_btCancel4;
	CButton m_btCancel5;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel1();
	afx_msg void OnBnClickedCancel2();
	afx_msg void OnBnClickedCancel3();
	afx_msg void OnBnClickedCancel4();
	afx_msg void OnBnClickedCancel5();
};
