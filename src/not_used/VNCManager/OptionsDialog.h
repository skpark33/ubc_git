#pragma once
#include "afxwin.h"


// COptionsDialog 대화 상자입니다.

class COptionsDialog : public CDialog
{
	DECLARE_DYNAMIC(COptionsDialog)

public:
	COptionsDialog(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~COptionsDialog();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_OPTIONS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	CString		m_strEnterpriseKey;
	CString		m_strVNCPath;

public:
	afx_msg void OnBnClickedButtonBrowse();

	CEdit		m_editEnterpriseKey;
	CEdit		m_editVNCPath;

	void		SetEnterpriseKey(LPCTSTR lpszKey) { m_strEnterpriseKey = lpszKey; };
	LPCTSTR		GetEnterpriseKey() { return m_strEnterpriseKey; };

	void		SetVNCPath(LPCTSTR lpszPath) { m_strVNCPath = lpszPath; };
	LPCTSTR		GetVNCPath() { return m_strVNCPath; };
};
