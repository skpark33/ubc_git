// ListenSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VNCManager.h"
#include "ListenSocket.h"

#include "BypassSocket.h"

// CListenSocket

CListenSocket::CListenSocket(SOCKET_TYPE socketType, int nPort)
:	m_SocketType (socketType)
,	m_nPort (nPort)
,	m_pCurrentBypassSocket (NULL)
{
}

CListenSocket::~CListenSocket()
{
	CloseSocket();
}


// CListenSocket 멤버 함수

void CListenSocket::OnAccept(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	m_mutex.Lock();

	int count = m_SocketList.GetCount();

	m_pCurrentBypassSocket = CreateNewSocket();

	if( Accept(*m_pCurrentBypassSocket) )
	{
		switch(m_SocketType)
		{
		case SOCKET_TYPE_REMOTE:
			::AfxGetMainWnd()->PostMessage(WM_CONNECT_SOCKET, m_nPort - LOCAL_REMOTE_VNC_PORT_GAP);
			break;

		case SOCKET_TYPE_LOCAL:
			{
				// send first packet
				char* buf = new char[1];
				buf[0] = 'C';
				m_pCurrentBypassSocket->SendEx(buf, 1);
			}
			break;

		case SOCKET_TYPE_HEALTH_CHECK:
			break;
		}
	}

	m_mutex.Unlock();

	CAsyncSocket::OnAccept(nErrorCode);
}

CBypassSocket* CListenSocket::CreateNewSocket()
{
//	m_mutex.Lock();

	int count = m_SocketList.GetCount();
	CBypassSocket* bypass_socket = new CBypassSocket(this, m_nPort, count, m_SocketType);
	m_SocketList.Add(bypass_socket);

//	m_mutex.Unlock();

	return bypass_socket;
}

bool CListenSocket::CloseSocket()
{
	m_mutex.Lock();

	int count = m_SocketList.GetCount();
	for(int i=0; i<count; i++)
	{
		CBypassSocket* bypass_socket = m_SocketList.GetAt(i);
		if(bypass_socket)
		{
			bypass_socket->Close();
			delete bypass_socket;
			m_SocketList.SetAt(i, NULL);
		}
	}

	m_pCurrentBypassSocket = NULL;

	m_mutex.Unlock();

	return true;
}

BOOL CListenSocket::StartListen()
{
	Create(m_nPort);
	return Listen();
}

void CListenSocket::SetLinkListenSocket(CListenSocket* pSocket1, CListenSocket* pSocket2)
{
	m_pLinkListenSocket1 = pSocket1;
	m_pLinkListenSocket2 = pSocket2;
}

void CListenSocket::LinkListenSocketCloseSocket()
{
	if(m_pLinkListenSocket1)
	{
		m_pLinkListenSocket1->CloseSocket();
	}

	if(m_pLinkListenSocket2)
	{
		m_pLinkListenSocket2->CloseSocket();
	}
}

int CListenSocket::SendEx(void* lpBuf, int nBufLen)
{
	//int ret_value = SOCKET_ERROR;
	int ret_value = 0;
	m_mutex.Lock();

	if( m_pCurrentBypassSocket != NULL && m_pCurrentBypassSocket->m_hSocket)
	{
		ret_value = m_pCurrentBypassSocket->Send(lpBuf, nBufLen);
	}

	m_mutex.Unlock();

	return ret_value;
}

void CListenSocket::CleartBuffer()
{
	m_pCurrentBypassSocket->ClearBuffer();
}
