// BypassSocket.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VNCManager.h"
#include "BypassSocket.h"

#include "ListenSocket.h"


#define		BUFF_SIZE		4096

#define		PACKET_SLEEP_TIME		(25)
#define		HEALTH_SLEEP_TIME		(1000)	// 1 sec

// CBypassSocket

CBypassSocket::CBypassSocket(CListenSocket* pListenSocket, int port, int index, SOCKET_TYPE socket_type)
:	m_bProcessThread (true)
,	m_pParentListenSocket (pListenSocket)
,	m_nPort (port)
,	m_nIndex (index)
,	m_SocketType (socket_type)
{
	THREAD_PARAM* param = new THREAD_PARAM;
	param->pProcess				= &m_bProcessThread;
	param->pParentListenSocket	= m_pParentListenSocket;
	param->pBypassSocket		= this;

	if(socket_type == SOCKET_TYPE_HEALTH_CHECK)
		m_pThread = ::AfxBeginThread(HealthCheckFunc, (LPVOID)param, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);
	else
		m_pThread = ::AfxBeginThread(SendFunc, (LPVOID)param, THREAD_PRIORITY_NORMAL, CREATE_SUSPENDED);

	m_pThread->m_bAutoDelete = FALSE;
	m_pThread->ResumeThread();
}

CBypassSocket::~CBypassSocket()
{
	m_mutex.Lock();
	m_bProcessThread = false;
	m_mutex.Unlock();

	DeleteThread();

	m_mutex.Lock();

	int count = m_PacketDataList.GetCount();
	for(int i=0; i<count; i++)
	{
		PACKET_DATA& data = m_PacketDataList.GetAt(i);
		delete data.buf;
	}

	m_mutex.Unlock();
}

// CBypassSocket 멤버 함수

void CBypassSocket::OnConnect(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CAsyncSocket::OnConnect(nErrorCode);
}

void CBypassSocket::OnClose(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	::AfxGetMainWnd()->PostMessage(WM_CLOSE_SOCKET, m_nPort, m_nIndex);

	m_pParentListenSocket->LinkListenSocketCloseSocket();

	m_mutex.Lock();
	m_bProcessThread = false;
	m_mutex.Unlock();

	DeleteThread();

	CAsyncSocket::OnClose(nErrorCode);

	m_pParentListenSocket->CloseSocket();
}

void CBypassSocket::OnReceive(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	char buff[BUFF_SIZE];
	int nRead = Receive(buff, BUFF_SIZE);

	if(nRead != 0 && nRead != SOCKET_ERROR)
	{
		char* buf = new char[nRead];
		memcpy(buf, buff, nRead);
		SendEx(buf, nRead);
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CBypassSocket::OnSend(int nErrorCode)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CAsyncSocket::OnSend(nErrorCode);
}

void CBypassSocket::SendEx(void* lpBuf, int nBufLen)
{
	m_mutex.Lock();

	if(m_PacketDataList.GetCount() < 16384)
	{
		PACKET_DATA data;
		data.buf	= lpBuf;
		data.length	= nBufLen;

		m_PacketDataList.Add(data);
	}

	m_mutex.Unlock();
}

UINT CBypassSocket::SendFunc(LPVOID pParam)
{
	THREAD_PARAM* param = (THREAD_PARAM*)pParam;
	bool* pProcess = param->pProcess;
	CListenSocket* pParentListenSocket = param->pParentListenSocket;
	CBypassSocket* pBypassSocket = param->pBypassSocket;

	int port = pBypassSocket->m_nPort;

	delete param;

	pBypassSocket->m_mutex.Lock();

	while(*pProcess)
	{
		int count = pBypassSocket->m_PacketDataList.GetCount();
		if(count != 0)
		{
			int size = 0;
			for(int i=0; i<count; i++)
			{
				PACKET_DATA& data = pBypassSocket->m_PacketDataList.GetAt(i);
				size += data.length;
			}

			char* packet = new char[size];
			int pos = 0;
			for(int i=0; i<count; i++)
			{
				PACKET_DATA data = pBypassSocket->m_PacketDataList.GetAt(i);
				char* tmp_buf = (char*)data.buf;
				while(memcmp(tmp_buf, "PingPingPing", 12) == 0)
				{
					tmp_buf += 12;
					data.length -= 12;
				}
				memcpy(packet + pos, tmp_buf, data.length);
				pos += data.length;
				//PACKET_DATA& data = pBypassSocket->m_PacketDataList.GetAt(i);
				//memcpy(packet + pos, data.buf, data.length);
				//pos += data.length;
			}

			int ret_value = pParentListenSocket->m_pLinkListenSocket1->SendEx(packet, size);
			if(ret_value != SOCKET_ERROR)
			{
				for(int i=0; i<count; i++)
				{
					PACKET_DATA& data = pBypassSocket->m_PacketDataList.GetAt(i);
					delete[]data.buf;
				}
				pBypassSocket->m_PacketDataList.RemoveAll();
			}
			else
			{
				DWORD error_code = pBypassSocket->GetLastError();
				switch(error_code)
				{
				case WSAENETDOWN:
				case WSAENETRESET:
				case WSAENOBUFS:
				case WSAENOTCONN:
				case WSAESHUTDOWN:
				case WSAECONNABORTED:
				case WSAECONNRESET:
					//pBypassSocket->m_mutex.Unlock();
					//pBypassSocket->OnClose(error_code);
					//return 0;
					break;
				}
			}
			delete[]packet;
		}

		pBypassSocket->m_mutex.Unlock();

		::Sleep(PACKET_SLEEP_TIME);

		pBypassSocket->m_mutex.Lock();
	}

	pBypassSocket->m_mutex.Unlock();

	return 0;
}

UINT CBypassSocket::HealthCheckFunc(LPVOID pParam)
{
	THREAD_PARAM* param = (THREAD_PARAM*)pParam;
	bool* pProcess = param->pProcess;
	CListenSocket* pParentListenSocket = param->pParentListenSocket;
	CBypassSocket* pBypassSocket = param->pBypassSocket;

	int port = pBypassSocket->m_nPort;

	delete param;

	pBypassSocket->m_mutex.Lock();

	while(*pProcess)
	{
		pBypassSocket->Send("Ping", 4);

		pBypassSocket->m_mutex.Unlock();

		::Sleep(HEALTH_SLEEP_TIME);

		pBypassSocket->m_mutex.Lock();
	}

	pBypassSocket->m_mutex.Unlock();

	return 0;
}

bool CBypassSocket::DeleteThread()
{
	if(m_pThread)
	{
		DWORD dwExitCode = 0;

		try
		{
			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);
		}
		catch(...)
		{
			DWORD dw = GetLastError();
			CHAR szBuf[80];
			sprintf(szBuf, "GetLastError Code : %d", dw);

			TRACE(szBuf);
			TRACE("\n");
		}

		try
		{
			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, INFINITE);
				if(timeout == WAIT_TIMEOUT)
				{
				}
				else
				{
				}
			}

			delete m_pThread;

			TRACE("ThreadDelete %d\n", m_nPort);
		}
		catch(...)
		{
			DWORD dw = GetLastError();
			CHAR szBuf[80];
			sprintf(szBuf, "GetLastError Code : %d", dw);

			TRACE(szBuf);
			TRACE("\n");
		}

		m_pThread = NULL;
	}

	return true;
}

void CBypassSocket::ClearBuffer()
{
	m_mutex.Lock();

	int count = m_PacketDataList.GetCount();
	for(int i=0; i<count; i++)
	{
		PACKET_DATA& data = m_PacketDataList.GetAt(i);
		delete data.buf;
	}
	m_PacketDataList.RemoveAll();

	m_mutex.Unlock();
}
