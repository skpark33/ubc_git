/************************************************************************************/
/*! @file HostInfoDlg.cpp
	@brief Host 상세 정보를 보여주는 dialog 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2008/11/11\n
	▶ 사용파일: stdafx.h, VNCManager.h, HostInfoDlg.h, MainFrm.h\n
	▶ 참고사항:
		- 메인 view 화면에서 보여주는 정보를 포함하여 기타 상세 정보를 보여준다

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2008/11/11:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

// HostInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VNCManager.h"
#include "HostInfoDlg.h"
#include "MainFrm.h"


// CHostInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CHostInfoDlg, CDialog)

CHostInfoDlg::CHostInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHostInfoDlg::IDD, pParent)
{
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CHostInfoDlg::~CHostInfoDlg()
{
}

void CHostInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_INFO_STATIC, m_ctrlGrid);
}


BEGIN_MESSAGE_MAP(CHostInfoDlg, CDialog)
	
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CHostInfoDlg 메시지 처리기입니다.


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Host 정보를 설정한다. \n
/// @param (CHostInfo*) pclsHostInfo : (in) host 정보를 갖는 클래스 포인터
/// @return <형: void> \n
/////////////////////////////////////////////////////////////////////////////////
void CHostInfoDlg::SetHostInfo(CHostInfo* pclsHostInfo)
{
	if(!pclsHostInfo)
	{
		return;
	}//if

	m_clsHostInfo.m_strHostName			= pclsHostInfo->m_strHostName;		
	m_clsHostInfo.m_strSiteID			= pclsHostInfo->m_strSiteID;		
	m_clsHostInfo.m_strHostID			= pclsHostInfo->m_strHostID;		
	m_clsHostInfo.m_strAdminState		= pclsHostInfo->m_strAdminState;	
	m_clsHostInfo.m_strOperationState	= pclsHostInfo->m_strOperationState;
	m_clsHostInfo.m_strModel			= pclsHostInfo->m_strModel;			
	m_clsHostInfo.m_strDescription		= pclsHostInfo->m_strDescription;	
											
	m_clsHostInfo.m_strStatus			= pclsHostInfo->m_strStatus;		
	m_clsHostInfo.m_strIP				= pclsHostInfo->m_strIP;			
	m_clsHostInfo.m_strMainPort			= pclsHostInfo->m_strMainPort;		
	m_clsHostInfo.m_strSubPort			= pclsHostInfo->m_strSubPort;		
	m_clsHostInfo.m_strStartupTime		= pclsHostInfo->m_strStartupTime;	
	m_clsHostInfo.m_strShutdownTime		= pclsHostInfo->m_strShutdownTime;	
}
BOOL CHostInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	//m_brushBack.CreateSolidBrush(RGB(255, 255, 255));

	m_ctrlGrid.ResetContents();

	//HSECTION hs = m_ctrlGrid.AddSection("Host info");
	HSECTION hs = m_ctrlGrid.AddSection((LPSTR)(LPCSTR)m_clsHostInfo.m_strHostName);

	//m_ctrlGrid.AddStringItem(hs, "Host name",				(LPSTR)(LPCSTR)m_clsHostInfo.m_strHostName,			false);
	m_ctrlGrid.AddStringItem(hs, "Site id",					(LPSTR)(LPCSTR)m_clsHostInfo.m_strSiteID,			false);
	m_ctrlGrid.AddStringItem(hs, "Host id",					(LPSTR)(LPCSTR)m_clsHostInfo.m_strHostID,			false);
	m_ctrlGrid.AddStringItem(hs, "Admin state (Usage)",		(LPSTR)(LPCSTR)m_clsHostInfo.m_strAdminState,		false);
	m_ctrlGrid.AddStringItem(hs, "Operation state (Power)",	(LPSTR)(LPCSTR)m_clsHostInfo.m_strOperationState,	false);
	m_ctrlGrid.AddStringItem(hs, "Model",					(LPSTR)(LPCSTR)m_clsHostInfo.m_strModel,			false);
	m_ctrlGrid.AddStringItem(hs, "Description",				(LPSTR)(LPCSTR)m_clsHostInfo.m_strDescription,		false);

	m_ctrlGrid.AddStringItem(hs, "Status",					(LPSTR)(LPCSTR)m_clsHostInfo.m_strStatus,			false);
	m_ctrlGrid.AddStringItem(hs, "IP address",				(LPSTR)(LPCSTR)m_clsHostInfo.m_strIP,				false);
	m_ctrlGrid.AddStringItem(hs, "Main port",				(LPSTR)(LPCSTR)m_clsHostInfo.m_strMainPort,			false);
	m_ctrlGrid.AddStringItem(hs, "Sub port",				(LPSTR)(LPCSTR)m_clsHostInfo.m_strSubPort,			false);
	m_ctrlGrid.AddStringItem(hs, "Startup time",			(LPSTR)(LPCSTR)m_clsHostInfo.m_strStartupTime,		false);
	m_ctrlGrid.AddStringItem(hs, "Shutdown time",			(LPSTR)(LPCSTR)m_clsHostInfo.m_strShutdownTime,		false);	

	if(m_clsHostInfo.m_strStatus == _T("On"))
	{
		m_ctrlGrid.SetBackColor(COLOR_ONLINE_BK);
		m_ctrlGrid.SetTextColor(COLOR_ONLINE);
		m_ctrlGrid.SetDisabledColor(COLOR_ONLINE);
	}
	else
	{
		m_ctrlGrid.SetBackColor(COLOR_OFFLINE_BK);
		m_ctrlGrid.SetTextColor(COLOR_OFFLINE);
		m_ctrlGrid.SetDisabledColor(COLOR_OFFLINE);
	}//if

	/*
	LOGFONT LogFont;
    memset(&LogFont, 0x00, sizeof(LogFont));
    strncpy(LogFont.lfFaceName, _T("Tahoma"), LF_FACESIZE);
	LogFont.lfWeight = FW_BOLD;
    LogFont.lfHeight = 300;
    m_clsFont.CreateFontIndirect(&LogFont);
	*/
	//m_clsFont.CreatePointFont(9*10, "Tahoma");
	//m_ctrlGrid.SetFont(&m_clsFont);

	WINDOWPLACEMENT stWndDpl;
	memset(&stWndDpl, 0x00, sizeof(WINDOWPLACEMENT));
	CMainFrame* pclsFrame = (CMainFrame*)::AfxGetMainWnd();
	pclsFrame->GetWindowPlacement(&stWndDpl);

	CRect clsRect;
	this->GetWindowRect(&clsRect);
	MoveWindow(stWndDpl.rcNormalPosition.right+80,
							stWndDpl.rcNormalPosition.top,
							clsRect.Width(),
							clsRect.Height());

	CString strTitle;
	strTitle.Format(_T("%s - Host information"), m_clsHostInfo.m_strHostName); 
	SetWindowText(strTitle);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
/*
HBRUSH CHostInfoDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.
	if(pWnd == this)
	{
		return (HBRUSH)m_brushBack;
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
*/