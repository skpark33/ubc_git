// OptionsDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VNCManager.h"
#include "OptionsDialog.h"


// COptionsDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(COptionsDialog, CDialog)

COptionsDialog::COptionsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDialog::IDD, pParent)
	, m_strEnterpriseKey ( _T("") )
	, m_strVNCPath ( _T("") )
{

}

COptionsDialog::~COptionsDialog()
{
}

void COptionsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_ENTERPRISE_KEY, m_editEnterpriseKey);
	DDX_Control(pDX, IDC_EDIT_VNCPATH, m_editVNCPath);
}


BEGIN_MESSAGE_MAP(COptionsDialog, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, &COptionsDialog::OnBnClickedButtonBrowse)
END_MESSAGE_MAP()


// COptionsDialog 메시지 처리기입니다.

BOOL COptionsDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_editEnterpriseKey.SetWindowText(m_strEnterpriseKey);
	m_editVNCPath.SetWindowText(m_strVNCPath);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void COptionsDialog::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnOK();

	m_editEnterpriseKey.GetWindowText(m_strEnterpriseKey);
	m_editVNCPath.GetWindowText(m_strVNCPath);

	WriteINIValue("ROOT", "ENTERPRISE_KEY", m_strEnterpriseKey);
	WriteINIValue("ROOT", "VNC_PATH", m_strVNCPath);
}

void COptionsDialog::OnBnClickedButtonBrowse()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	TCHAR szFilters[] = _T("VNC-Viewer Program (*.exe)|*.exe||");

	CFileDialog dlg(TRUE, _T("exe"), _T("vncviewer.exe"), OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, szFilters, this);;

	if(dlg.DoModal() == IDOK)
	{
		m_strVNCPath = dlg.GetPathName();
		m_editVNCPath.SetWindowText(m_strVNCPath);
	}
}
