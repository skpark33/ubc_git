// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// VNCManager.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


CString ToString(int nValue)
{
	CString str;
	str.Format("%d", nValue);

	return str;
}

LPCTSTR GetINIPath()
{
	static CString	_iniDirectory = _T("");

	if(_iniDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_iniDirectory = str;

		CString app;
		app.LoadString(AFX_IDS_APP_TITLE);

		_iniDirectory.Append(app);
		_iniDirectory.Append(_T(".ini"));
	}

	return _iniDirectory;
}

LPCTSTR GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}

CString GetINIValue(LPCTSTR strSection, LPCTSTR strKey)
{
	CString app;
	app.LoadString(AFX_IDS_APP_TITLE);

	CString strPath = GetAppPath();
	strPath += "data\\";
	strPath += app;
	strPath += _T(".ini");

	char cBuffer[MAX_PATH+1] = { 0x00 };
	GetPrivateProfileString(strSection, strKey, "", cBuffer, MAX_PATH, strPath);

	return CString(cBuffer);
}

BOOL WriteINIValue(LPCTSTR strSection, LPCTSTR strKey, LPCTSTR strValue)
{
	CString app;
	app.LoadString(AFX_IDS_APP_TITLE);

	CString strPath = GetAppPath();
	strPath += "data\\";
	::CreateDirectory(strPath, NULL);
	strPath += app;
	strPath += _T(".ini");

	return WritePrivateProfileString(strSection, strKey, strValue ,strPath);
}
