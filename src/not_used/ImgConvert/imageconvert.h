/************************************************************************************/
/*! @file ImageConvert.h
	@brief CxImage 라이브러리를 이용한 이미지 변환 클래스 선언 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/08/03\n
	
************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2009/08/03:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "ximage.h"


//! 이미지 변환과정의 진행상태를 access할 수 있는 함수를 갖는 클래스
/*!
	
*/
class CConvertProgress
{
public:
	virtual void GetProgressValue(int nTotal, int nNow, int nPercent, CString strFileName) = 0;
};


//! 이미지 파일의 정보를 갖는 클래스
/*!
	
*/
class CImgFileInfo
{
public:
	CImgFileInfo()				{
									m_strSourcePath = _T("");
									m_strTargetPath = _T("");
									m_nSourceWidth	= 0;
									m_nSourceHeight	= 0;
									m_nTargetWidth	= 0;
									m_nTargetHeight	= 0;
									m_ulSourceSize	= 0L;
									m_ulTargetSize	= 0L;
									m_bResult		= false;
								};		///<생성자
	virtual ~CImgFileInfo()		{ };	///<소멸자

	CString		m_strSourcePath;		///<Source 파일의 경로
	CString		m_strTargetPath;		///<Target 파일의 경로
	int			m_nSourceWidth;			///<Source 파일의 넓이
	int			m_nSourceHeight;		///<Source 파일의 높이
	int			m_nTargetWidth;			///<Target 파일의 넓이
	int			m_nTargetHeight;		///<Target 파일의 높이
	ULONG		m_ulSourceSize;			///<Source 파일의 크기(Byte)
	ULONG		m_ulTargetSize;			///<Target 파일의 크기(Byte)
	bool		m_bResult;				///<파일의 크기, 포멧 변환등의 결과
};

//FileInfo 리스트
typedef CList<CImgFileInfo, CImgFileInfo&> CFileInfoList;


//! 이미지 변환 클래스
/*!
	CxImage 라이브러리를 이용하여 이미지의 Resample과 포멧 변경을 하는 클래스 \n
*/
class CImageConvert
{
public:
	CImageConvert(CConvertProgress* pclsProgress = NULL);									///<생성자
	virtual ~CImageConvert();																///<소멸자

	bool		GetFileInfo(CString strFilePath, int& nWidth, int& nHeight, ULONG& ulSize);	///<이미지 파일의 넓이, 높이, 크기를 구한다.
	bool		GetFileInfo(CFileInfoList* plistFileInfo);									///<이미지 파일의 넓이, 높이를 구한다.

	bool		ImgConvert(CString strSourcePath,
							CString strTargetPath,
							int& nTargetWidth,
							int& nTargetHeight,
							ULONG& ulTargetSize,
							bool bKeepRatio = TRUE);										///<이미지 파일을 변환한다.
	bool		ImgConvert(CFileInfoList* plistFileInfo, bool bKeepRatio = TRUE);			///<이미지 파일을 변환한다.

	CxImage				m_clsImg;															///<CxImage
	CConvertProgress*	m_pclsProgress;														///<이미지파일 변환과정을 access하는 클래스
	int					m_nTotal;															///<전체 변환하는 파일의 겟수
	int					m_nNow;																///<현재 변환중인 파일의 번호
	CString				m_strFileName;														///<현재 변환중인 파일의 이름
	void		SetProgressObject(CConvertProgress* pclsProgress);							///<이미지파일 변환과정을 access하는 클래스를 설정

protected:
	int			GetImgType(CString strFilePath);											///<이미지 파일의 종류를 구한다.
	CString		FindExtension(const CString& strFileName);									///<파일이름에서 확장자를 구하여 반환한다
	ULONG		GetFileSize(CString strFileName);											///<파일의 크기를 Byte 단위로 구한다.
	static UINT	ThreadProgress(LPVOID pParam);												///<파일변환의 진행상황을 계산하는 thread
};