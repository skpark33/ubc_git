/************************************************************************************/
/*! @file ImageConvert.cpp
	@brief <간략한 설명을 기술>
	@remarks
	▶ 작성자: \n
	▶ 작성일: yyyy/mm/dd\n
	▶ DB테이블: \n
	▶ 사용파일: \n
	▶ 시스템 리소스: \n
	▶ 참고사항:
		- <참고사항 기술>
		- <참고사항 기술>

  @warning ※ <경고사항 기술>\n
		   ※ <경고사항이 없으면 삭제하세요>

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 예제)
	-# 2005/08/01:홍길동:게임머니 환전 버그수정.
		- PreCreateWindow(CREATESTRUCT& cs);
		- Dump(CDumpContext& dc) const;
************************************************************************************
	@b 작성)
	-# <yyyy/mm/dd:변경자명:변경내용>.
		- <변경모듈명>

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

/************************************************************************************/
/*! @file ImageConvert.cpp
	@brief CxImage 라이브러리를 이용한 이미지 변환 클래스 구현 파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2009/08/03\n
	▶ 사용파일: stdafx.h, ImageConvert.h\n
	▶ 시스템 리소스: \n
	▶ 참고사항:
		- <참고사항 기술>
		- <참고사항 기술>

  @warning ※ <경고사항 기술>\n
		   ※ <경고사항이 없으면 삭제하세요>

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2009/08/03:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"
#include "ImageConvert.h"


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CImageConvert::CImageConvert(CConvertProgress* pclsProgress)
:	m_nTotal(0),
	m_nNow(0),
	m_strFileName(_T(""))
{
	m_pclsProgress = pclsProgress;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CImageConvert::~CImageConvert()
{
	if(m_clsImg.IsValid())
	{
		m_clsImg.Destroy();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 원본과 변환될 이미지 파일의 넓이, 높이를 구한다. \n
/// @param (CString) strFilePath : (in) 이미지 파일의 경로
/// @param (int&) nSourceWidth : (out) 이미지파일의 넓이
/// @param (int&) nSourceHeight : (out) 이미지파일의 높이
/// @param (ULONG&) ulSize : (out) 이미지파일의 크기(Byte)
/// @return <형: bool> \n
///			<성공: true> \n
///			<실패: false> \n
/////////////////////////////////////////////////////////////////////////////////
bool CImageConvert::GetFileInfo(CString strFilePath, int& nWidth, int& nHeight, ULONG& ulSize)
{
	if(strFilePath == "")
	{
		return false;
	}//if

	int nImgType = GetImgType(strFilePath);
	if(!m_clsImg.Load(strFilePath, nImgType))
	{
		return false;
	}//if

	nWidth = (int)m_clsImg.GetWidth();
	nHeight = (int)m_clsImg.GetHeight();

	if(m_clsImg.IsValid())
	{
		m_clsImg.Destroy();
	}//if

	//파일의 크기
	ulSize = GetFileSize(strFilePath);

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 원본과 변환될 이미지 파일의 넓이, 높이를 구한다.\n
/// @param (CFileInfoList*) plistFileInfo : (in/out) 이미지파일의 정보를 갖는 클래스(CImgFileInfo)의 리스트
/// @return <형: bool> \n
///			<성공: true> \n
///			<실패: false> \n
/////////////////////////////////////////////////////////////////////////////////
bool CImageConvert::GetFileInfo(CFileInfoList* plistFileInfo)
{
	int nImgType;
	bool bRet = true;
	POSITION pos = plistFileInfo->GetHeadPosition();
	
	while(pos)
	{
		CImgFileInfo& clsFileInfo = plistFileInfo->GetNext(pos);

		if(clsFileInfo.m_strSourcePath == "")
		{
			clsFileInfo.m_bResult = false;
			bRet = false;
			continue;
		}//if

		nImgType = GetImgType(clsFileInfo.m_strSourcePath);
		if(!m_clsImg.Load(clsFileInfo.m_strSourcePath, nImgType))
		{
			clsFileInfo.m_bResult = false;
			bRet = false;
			continue;
		}//if

		clsFileInfo.m_nSourceWidth	= (int)m_clsImg.GetWidth();
		clsFileInfo.m_nSourceHeight = (int)m_clsImg.GetHeight();

		if(m_clsImg.IsValid())
		{
			m_clsImg.Destroy();
		}//if

		//파일의 크기
		clsFileInfo.m_ulSourceSize = GetFileSize(clsFileInfo.m_strSourcePath);
	}//while

	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지 파일을 변환한다. \n
/// @param (CString) strSourcePath : (in) 변환하려는 이미지 파일의 경로
/// @param (CString) strTargetPath : (in) 변환될 이미지 파일의 경로 
/// @param (int&) nTargetWidth : (in/out) 이미지파일을 변환하려는 넓이
/// @param (int&) nTargetHeight : (in/out) 이미지파일을 변환하려는 높이
/// @param (ULONG&) ulTargetSize : (in/out) 변환된 이미지파일의 크기(Byte)
/// @param (bool) bKeepRatio : (in) 이미지파일을 변환할때 원본의 비율을 유지하는지 여부
/// @return <형: bool> \n
///			<성공: true> \n
///			<실패: false> \n
/////////////////////////////////////////////////////////////////////////////////
bool CImageConvert::ImgConvert(CString strSourcePath, CString strTargetPath,
							   int& nTargetWidth, int& nTargetHeight,
							   ULONG& ulTargetSize, bool bKeepRatio)
{
	//Target Width가 0이면 변환을 할 수 없다.
	if((nTargetWidth == 0)
		|| (strSourcePath == "")
		|| (strTargetPath == ""))
	{
		return false;
	}//if

	int nImgType = GetImgType(strSourcePath);
	if(!m_clsImg.Load(strSourcePath, nImgType))
	{
		return false;
	}//if

	int nWidth = (int)m_clsImg.GetWidth();
	int nHeight = (int)m_clsImg.GetHeight();
	
	//Keep aspect ratio
	if(bKeepRatio)
	{
		float fRatio = ((float)nWidth)/((float)nHeight);
		nTargetHeight = (int)(nTargetWidth / fRatio + 0.5f);
	}//if

	//Image resample
	m_clsImg.SetProgress(0);
	m_clsImg.SetEscape(0);

	CWinThread* pThread = NULL;
	if(m_pclsProgress){
		pThread = ::AfxBeginThread(CImageConvert::ThreadProgress,
											this,
											THREAD_PRIORITY_NORMAL,
											0,
											CREATE_SUSPENDED);

		if(!pThread)
		{
			return false;
		}//if

		m_nTotal = 1;
		m_nNow = 1;
		m_strFileName = strSourcePath;

		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
	}

	TRACE("Begin resample !!!\r\n");
	bool bRet = m_clsImg.Resample(nTargetWidth, nTargetHeight, 3);	//default option(bilinear interpolation)
	m_clsImg.SetProgress(100);
	m_clsImg.SetEscape(1);
	TRACE("End resample !!!\r\n");
	if(bRet)
	{
		bRet = m_clsImg.Save(strTargetPath, nImgType);
	}//if

	if(m_clsImg.IsValid())
	{
		m_clsImg.Destroy();
	}//if

	//파일의 크기
	if(bRet)
	{
		ulTargetSize = GetFileSize(strTargetPath);
	}//if

	return bRet;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지 파일을 변환한다. \n
/// @param (CFileInfoList*) plistFileInfo : (in/out) 변환하려는 이미지파일의 정보를 갖는 클래스(CImgFileInfo)의 리스트
/// @param (bool) bKeepRatio : (in/out) 이미지파일을 변환할때 원본의 비율을 유지하는지 여부
/// @return <형: bool> \n
///			<성공: true> \n
///			<실패: false> \n
/////////////////////////////////////////////////////////////////////////////////
bool CImageConvert::ImgConvert(CFileInfoList* plistFileInfo, bool bKeepRatio)
{
	int nImgType;
	bool bRet = true;
	POSITION pos = plistFileInfo->GetHeadPosition();
	if(!pos)
	{
		return false;
	}//if

	m_clsImg.SetEscape(0);
	m_clsImg.SetProgress(0);

	int nCount = 1;
	CWinThread* pThread = NULL;
	if(m_pclsProgress){
		pThread = ::AfxBeginThread(CImageConvert::ThreadProgress,
											this,
											THREAD_PRIORITY_NORMAL,
											0,
											CREATE_SUSPENDED);
		if(!pThread)
		{
			return false;
		}//if

		m_nTotal = plistFileInfo->GetCount();
		
		pThread->m_bAutoDelete = TRUE;
		pThread->ResumeThread();
	}
	
	while(pos)
	{
		CImgFileInfo& clsFileInfo = plistFileInfo->GetNext(pos);
	
		m_clsImg.SetProgress(0);
		m_nNow = nCount++;
		m_strFileName = clsFileInfo.m_strSourcePath;
		
		//Target Width가 0이면 변환을 할 수 없다.
		if((clsFileInfo.m_nTargetWidth == 0)
			|| (clsFileInfo.m_strSourcePath == "")
			|| (clsFileInfo.m_strTargetPath == ""))
		{
			clsFileInfo.m_bResult = false;
			bRet = false;
			continue;
		}//if

		nImgType = GetImgType(clsFileInfo.m_strSourcePath);
		if(!m_clsImg.Load(clsFileInfo.m_strSourcePath, nImgType))
		{
			clsFileInfo.m_bResult = false;
			bRet = false;
			continue;
		}//if

		clsFileInfo.m_nSourceWidth = (int)m_clsImg.GetWidth();
		clsFileInfo.m_nSourceHeight = (int)m_clsImg.GetHeight();

		//Keep aspect ratio
		if(bKeepRatio)
		{
			float fRatio = ((float)clsFileInfo.m_nSourceWidth)/((float)clsFileInfo.m_nSourceHeight);
			clsFileInfo.m_nTargetHeight = (int)(clsFileInfo.m_nTargetWidth / fRatio + 0.5f);
		}//if

		//Image resample
		bool bRet = m_clsImg.Resample(clsFileInfo.m_nTargetWidth, clsFileInfo.m_nTargetHeight, 3);	//default option(bilinear interpolation)
		if(bRet)
		{
			bRet = m_clsImg.Save(clsFileInfo.m_strTargetPath, nImgType);
			clsFileInfo.m_ulTargetSize = GetFileSize(clsFileInfo.m_strTargetPath);
			clsFileInfo.m_bResult = true;
		}//if

		m_clsImg.SetProgress(100);

		if(m_clsImg.IsValid())
		{
			m_clsImg.Destroy();
		}//if
	}//while

	m_clsImg.SetEscape(1);

	return bRet;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지 파일의 종류를 구한다. \n
/// @param (CString) strFilePath : (in) 이미지파일의 경로
/// @return <형: int> \n
///			<CxImage 라이브러리에서 정의된 이미지 포멧 enum 값> \n
/////////////////////////////////////////////////////////////////////////////////
int CImageConvert::GetImgType(CString strFilePath)
{
	CString strExt = FindExtension(strFilePath);
	return CxImage::GetTypeIdFromName(strExt);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일이름에서 확장자를 구하여 반환한다 \n
/// @param (const CString&) strFileName : (in) 파일의 이름 또는 전체경로
/// @return <형: CString> \n
///			<확장자 문자여열> \n
/////////////////////////////////////////////////////////////////////////////////
CString CImageConvert::FindExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--){
		if (strFileName[i] == '.'){
			return strFileName.Mid(i+1);
		}
	}
	return CString(_T(""));
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일의 크기를 Byte 단위로 구한다. \n
/// @param (CString) strFileName : (in) 파일의 전체 경로
/// @return <형: ULONG> \n
///			<Byte 단위의 파일의 크기> \n
///			<0: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
ULONG CImageConvert::GetFileSize(CString strFileName)
{
	//파일의 크기
	CFileStatus fs;
	if(CFile::GetStatus(strFileName, fs) == FALSE)
	{
		return 0;
	}//if

	return fs.m_size;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 이미지파일 변환과정을 access하는 클래스를 설정 \n
/// @param (CConvertProgress*) pclsProgress : (in) 이미지파일 변환과정을 access하는 클래스 포인터
/////////////////////////////////////////////////////////////////////////////////
void CImageConvert::SetProgressObject(CConvertProgress* pclsProgress)
{
	m_pclsProgress = pclsProgress;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일변환의 진행상황을 계산하는 thread \n
/// @param (LPVOID) pParam : (in) Thread parameter
/// @return <형: UINT> \n
///			<값: 0> \n
/////////////////////////////////////////////////////////////////////////////////
UINT CImageConvert::ThreadProgress(LPVOID pParam)
{
	CImageConvert* pParent = (CImageConvert *)pParam;
	long lProgress = 0L;
	while(1)
	{
		Sleep(200);
		
		if(pParent->m_clsImg.GetEscape())
		{
			lProgress = pParent->m_clsImg.GetProgress();
			pParent->m_pclsProgress->GetProgressValue(pParent->m_nTotal, pParent->m_nNow, lProgress, pParent->m_strFileName);
			break;
		}//if

		lProgress = pParent->m_clsImg.GetProgress();
		TRACE("Progress = %d\r\n", lProgress);
		if(lProgress == 0)
		{
			continue;
		}//if
		
		if(pParent->m_pclsProgress)
		{
			pParent->m_pclsProgress->GetProgressValue(pParent->m_nTotal, pParent->m_nNow, lProgress, pParent->m_strFileName);
		}//if
	}//while
	
	return 0;
}