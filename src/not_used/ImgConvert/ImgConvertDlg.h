// ImgConvertDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "ImageConvert.h"


// CImgConvertDlg 대화 상자
class CImgConvertDlg : public CDialog, public CConvertProgress
{
// 생성입니다.
public:
	CImgConvertDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_IMGCONVERT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	bool			m_bkeepRatio;		///<원본 파일의 비율을 유지하는지 여부를 나타내는 falg
	CButton			m_checkKeepRatio;	///<원본 파일의 비율을 유지하는지 여부를 나타내는 체크 컨트롤
	CListCtrl		m_listSource;		///<원본 파일의 리스트
	CListCtrl		m_listTarget;		///<변환된 파일의 리스트
	CString			m_strTargetWidth;	///<변환하려는 넓이
	CString			m_strTargetHeight;	///<변환하려는 높이
	CImageConvert	m_clsConvert;		///<이미지 변환 클래스

	afx_msg void OnBnClickedKeepRatioCheck();
	afx_msg void OnBnClickedOk();	
	afx_msg void OnBnClickedSourceAddBtn();
	afx_msg void OnBnClickedSourceDelBtn();
	afx_msg void OnBnClickedSourceConvertBtn();
	afx_msg void OnBnClickedClearlBtn();

	void GetProgressValue(int nTotal, int nNow, int nPercent, CString strFileName);

protected:
	void InsertSourceItem(CString strSourcePath,
							int nWidth,
							int nHeight,
							ULONG ulSize);							///<원본파일의 정보를 리스트에 넣는다.
	void CImgConvertDlg::InsertTargetItem(CString strTargetPath,
							int nTargetWidth,
							int nTargetHeight,
							ULONG ulTargetSize,
							bool bRet);								///<변환된 파일의 결과를 Target list 컨트롤에 넣는다
	void InsertTargetItem(CFileInfoList* plistFileInfo);			///<변환된 파일의 결과를 Target list 컨트롤에 넣는다
	void DeleteLsit(int nIndex);									///<선택된 인덱스의 아이템을 Source, Target 리스트에서 삭제한다	
};


