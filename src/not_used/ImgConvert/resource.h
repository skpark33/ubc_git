//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ImgConvert.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IMGCONVERT_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_SOURCE_PATH_EDT             1000
#define IDC_SOURCE_BROWSE_BTN           1001
#define IDC_SOURCE_ADD_BTN              1001
#define IDC_TARGET_PATH_EDT             1002
#define IDC_SOURCE_DEL_BTN              1002
#define IDC_TARGET_BROWSE_BTN           1003
#define IDC_SOURCE_CONVERT_BTN          1003
#define IDC_SOURCE_WIDTH_EDT            1004
#define IDC_SOURCE_DEL_BTN2             1004
#define IDC_CLEARL_BTN                  1004
#define IDC_SOURCE_HEIGHT_EDT           1005
#define IDC_TARGET_WIDTH_EDT            1006
#define IDC_TARGET_HEIGHT_EDT           1007
#define IDC_CHECK1                      1008
#define IDC_KEEP_RATIO_CHECK            1008
#define IDC_SOURCE_LIST                 1009
#define IDC_TARGET_LIST                 1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
