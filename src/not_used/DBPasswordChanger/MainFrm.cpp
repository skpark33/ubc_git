// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "DBPasswordChanger.h"

#include "MainFrm.h"

#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(1, "CMainFrame");


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_COMMAND(ID_LANGUAGES_ENGLISH, &CMainFrame::OnLanguagesEnglish)
	ON_COMMAND(ID_LANGUAGES_KOREAN, &CMainFrame::OnLanguagesKorean)
	ON_UPDATE_COMMAND_UI(ID_LANGUAGES_ENGLISH, &CMainFrame::OnUpdateLanguagesEnglish)
	ON_UPDATE_COMMAND_UI(ID_LANGUAGES_KOREAN, &CMainFrame::OnUpdateLanguagesKorean)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	char buf[1025] = {0};
	::GetPrivateProfileString("Settings", "Language", "", buf, 1024, ::GetINIPath());
	m_strLanguage = buf;
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | /*WS_VISIBLE |*/ CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style &= (~(FWS_ADDTOTITLE | WS_MAXIMIZEBOX | WS_VISIBLE));

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers

void CMainFrame::OnLanguagesEnglish()
{
	::WritePrivateProfileString("Settings", "Language", "Eng", ::GetINIPath());

	if(m_strLanguage.CompareNoCase("Eng"))
	{
		::AfxMessageBox(IDS_LANGUAGE_CHANGED_RESTART, MB_ICONWARNING);
	}
}

void CMainFrame::OnLanguagesKorean()
{
	::WritePrivateProfileString("Settings", "Language", "Kor", ::GetINIPath());

	if(m_strLanguage.CompareNoCase("Kor"))
	{
		::AfxMessageBox(IDS_LANGUAGE_CHANGED_RESTART, MB_ICONWARNING);
	}
}

void CMainFrame::OnUpdateLanguagesEnglish(CCmdUI *pCmdUI)
{
	if( ciArgParser::getInstance()->isSet("+eng") )
	{
		pCmdUI->SetCheck();
		pCmdUI->Enable(FALSE);
	}
	else if(ciArgParser::getInstance()->isSet("+kor") ||
			ciArgParser::getInstance()->isSet("+jpn") )
	{
		pCmdUI->Enable(FALSE);
	}
	else if(m_strLanguage.CompareNoCase("eng") == 0)
	{
		pCmdUI->SetCheck();
	}
}

void CMainFrame::OnUpdateLanguagesKorean(CCmdUI *pCmdUI)
{
	if( ciArgParser::getInstance()->isSet("+kor") )
	{
		pCmdUI->SetCheck();
		pCmdUI->Enable(FALSE);
	}
	else if(ciArgParser::getInstance()->isSet("+eng") ||
			ciArgParser::getInstance()->isSet("+jpn") )
	{
		pCmdUI->Enable(FALSE);
	}
	else if(m_strLanguage.CompareNoCase("kor") == 0)
	{
		pCmdUI->SetCheck();
	}}
