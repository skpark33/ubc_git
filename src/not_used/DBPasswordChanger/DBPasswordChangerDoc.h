// DBPasswordChangerDoc.h : interface of the CDBPasswordChangerDoc class
//


#pragma once


class CDBPasswordChangerDoc : public CDocument
{
protected: // create from serialization only
	CDBPasswordChangerDoc();
	DECLARE_DYNCREATE(CDBPasswordChangerDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CDBPasswordChangerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


