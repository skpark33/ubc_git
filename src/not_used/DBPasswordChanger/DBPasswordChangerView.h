// DBPasswordChangerView.h : interface of the CDBPasswordChangerView class
//


#pragma once
#include "afxwin.h"


class CDBPasswordChangerView : public CFormView
{
protected: // create from serialization only
	CDBPasswordChangerView();
	DECLARE_DYNCREATE(CDBPasswordChangerView)

public:
	enum{ IDD = IDD_DBPASSWORDCHANGER_FORM };

// Attributes
public:
	CDBPasswordChangerDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CDBPasswordChangerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	bool		m_bPropertiesFile;
	bool		m_bASPModule;
	bool		m_bHTTPModule;
	bool		m_bWebModule;

	CStringArray	m_listASPFile;
	CStringArray	m_listHTTPFile;
	CStringArray	m_listWEBFile;

public:
	CEdit		m_editPrevPassword;
	CEdit		m_editNewPassword;
	CButton		m_btnChange;
	CButton		m_chkPropertiesFile;
	CButton		m_chkASPModule;
	CButton		m_chkHTTPModule;
	CButton		m_chkWebModule;

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonChange();
	afx_msg void OnBnClickedCheckPropertiesFile();
	afx_msg void OnBnClickedCheckAspModule();
	afx_msg void OnBnClickedCheckUbcHttpModule();
	afx_msg void OnBnClickedCheckUbcWebModule();

protected:
	bool		getPropertiesValue(LPCSTR lpszKey, CString& value);
	bool		setPropertiesValue(LPCSTR lpszKey, LPCSTR lpszValue);

	CString		GetDBPwdFromProperties();

	BOOL		GetFile(LPCSTR lpszSubPath, CStringArray& line_list);
	BOOL		CheckPropertiesPassword(LPCSTR lpszPrevPwd, LPCSTR lpszNewPwd);
	BOOL		CheckASPPassword(LPCSTR lpszSubPath, LPCSTR lpszPrevPwd, LPCSTR lpszNewPwd, CStringArray& line_list);
	BOOL		CheckConfigPassword(LPCSTR lpszSubPath, LPCSTR lpszPrevPwd, LPCSTR lpszNewPwd, CStringArray& line_list);
	BOOL		WriteFile(LPCSTR lpszSubPath, CStringArray& line_list);

	BOOL		Tokenize(CString& str, int& pos, CString& token);
	void		GetLineList(CString& str, CStringArray& line_list);
	int			FindSubStringNoCase(CString str, CString sub_str);
	BOOL		ReplaceSubStringNoCase(CString& str, CString& old_sub_str, CString& new_sub_str);

};

#ifndef _DEBUG  // debug version in DBPasswordChangerView.cpp
inline CDBPasswordChangerDoc* CDBPasswordChangerView::GetDocument() const
   { return reinterpret_cast<CDBPasswordChangerDoc*>(m_pDocument); }
#endif

