//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DBPasswordChanger.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_DBPASSWORDCHANGER_FORM      101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_DBPasswordChangTYPE         129
#define IDC_EDIT_PREV_PASSWORD          1000
#define IDC_EDIT_NEW_PASSWORD           1001
#define IDC_BUTTON_CHANGE               1002
#define IDS_LANGUAGE_CHANGED_RESTART    1003
#define IDC_CHECK_PROPERTIES_FILE       1004
#define IDS_INVALID_PASSWORD            1004
#define IDC_CHECK_ASP_MODULE            1005
#define IDS_PASSWORD_NOT_FOUND          1005
#define IDC_CHECK_UBC_HTTP_MODULE       1006
#define IDC_CHECK_UBC_WEB_MODULE        1007
#define ID_SETTINGS_LANGUAGES           32771
#define ID_LANGUAGES_KOREAN             32772
#define ID_LANGUAGES_ENGLISH            32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
