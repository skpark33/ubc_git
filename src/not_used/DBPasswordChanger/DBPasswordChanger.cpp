// DBPasswordChanger.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DBPasswordChanger.h"
#include "MainFrm.h"

#include "DBPasswordChangerDoc.h"
#include "DBPasswordChangerView.h"

#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(10, "CDBPasswordChangerApp");


// CDBPasswordChangerApp

BEGIN_MESSAGE_MAP(CDBPasswordChangerApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CDBPasswordChangerApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// CDBPasswordChangerApp construction

CDBPasswordChangerApp::CDBPasswordChangerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CDBPasswordChangerApp object

CDBPasswordChangerApp theApp;


// CDBPasswordChangerApp initialization

BOOL SetThreadLocaleEx(LCID locale)
{
	USES_CONVERSION;
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if(::GetVersionEx(&osv) == FALSE) //get Version Failed 
		return ::SetThreadLocale(locale);

	switch(osv.dwMajorVersion)
	{
	case 6:  //WINDOWS VISTA
		{
			unsigned (__stdcall* SetThreadUILanguage)(LANGID);
			CStringA strThreaUILanguage = T2A(_T("SetThreadUILanguage"));

			HINSTANCE hIns = ::LoadLibrary(_T("kernel32.dll"));  
			if(hIns == NULL) //no Instance
				return ::SetThreadLocale(locale);

			SetThreadUILanguage = (unsigned (__stdcall* )(LANGID))::GetProcAddress(hIns,strThreaUILanguage);   
			if(SetThreadUILanguage == NULL) //procAddress Getfailed 
				return ::SetThreadLocale(locale);

			LANGID lang = SetThreadUILanguage(locale);
			return (lang==LOWORD(locale))?TRUE:FALSE;
		} 
	default: //WINDOWS 2000, XP
		return ::SetThreadLocale(locale);
	}

}

BOOL CDBPasswordChangerApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("SQISoft"));
	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CDBPasswordChangerDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CDBPasswordChangerView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	//
	::DeleteFile(".\\log\\DBPasswordChanger.log.bak.bak");
	::MoveFile(".\\log\\DBPasswordChanger.log.bak", ".\\log\\DBPasswordChanger.log.bak.bak");
	::MoveFile(".\\log\\DBPasswordChanger.log", ".\\log\\DBPasswordChanger.log.bak");

	//
	ciDebug::setDebugOn();
	ciDebug::logOpen(".\\log\\DBPasswordChanger.log");
	ciArgParser::initialize(__argc,__argv);

	// arguments - language
	LANGID lang_id = PRIMARYLANGID(GetSystemDefaultLangID());

	char buf[1025] = {0};
	::GetPrivateProfileString("Settings", "Language", "", buf, 1024, ::GetINIPath());
	if(strcmpi(buf, "eng") == 0)		lang_id = LANG_ENGLISH;
	else if(strcmpi(buf, "kor") == 0)	lang_id = LANG_KOREAN;
	else if(strcmpi(buf, "jpn") == 0)	lang_id = LANG_JAPANESE;

	if(ciArgParser::getInstance()->isSet("+eng"))		lang_id = LANG_ENGLISH;
	else if(ciArgParser::getInstance()->isSet("+kor"))	lang_id = LANG_KOREAN;
	else if(ciArgParser::getInstance()->isSet("+jpn"))	lang_id = LANG_JAPANESE;

	switch(lang_id)
	{
	case LANG_KOREAN:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_KOREAN, SUBLANG_KOREAN) , SORT_DEFAULT));
		break;
	case LANG_JAPANESE:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_JAPANESE, SUBLANG_DEFAULT) , SORT_DEFAULT));
		break;
	default:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
		break;
	}


	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;

	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	return TRUE;
}



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CDBPasswordChangerApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CDBPasswordChangerApp message handlers

