// DBPasswordChanger.h : main header file for the DBPasswordChanger application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CDBPasswordChangerApp:
// See DBPasswordChanger.cpp for the implementation of this class
//

class CDBPasswordChangerApp : public CWinApp
{
public:
	CDBPasswordChangerApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CDBPasswordChangerApp theApp;