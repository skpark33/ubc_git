// DBPasswordChangerView.cpp : implementation of the CDBPasswordChangerView class
//

#include "stdafx.h"
#include "DBPasswordChanger.h"

#include "DBPasswordChangerDoc.h"
#include "DBPasswordChangerView.h"

#include "ci/libConfig/ciXProperties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

ciSET_DEBUG(10, "CDBPasswordChangerView");


#define		DEFAULT_ASP_RELATIVE_PATH		"\\web\\DefaultWebSite\\UBC_Common\\LocalSettings.asp"
#define		DEFAULT_HTTP_RELATIVE_PATH		"\\web\\UBC_HTTP\\Web.config"
#define		DEFAULT_WEB_RELATIVE_PATH		"\\web\\UBC_WEB\\Web.config"


// CDBPasswordChangerView

IMPLEMENT_DYNCREATE(CDBPasswordChangerView, CFormView)

BEGIN_MESSAGE_MAP(CDBPasswordChangerView, CFormView)
	ON_BN_CLICKED(IDC_BUTTON_CHANGE, &CDBPasswordChangerView::OnBnClickedButtonChange)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_PROPERTIES_FILE, &CDBPasswordChangerView::OnBnClickedCheckPropertiesFile)
	ON_BN_CLICKED(IDC_CHECK_ASP_MODULE, &CDBPasswordChangerView::OnBnClickedCheckAspModule)
	ON_BN_CLICKED(IDC_CHECK_UBC_HTTP_MODULE, &CDBPasswordChangerView::OnBnClickedCheckUbcHttpModule)
	ON_BN_CLICKED(IDC_CHECK_UBC_WEB_MODULE, &CDBPasswordChangerView::OnBnClickedCheckUbcWebModule)
END_MESSAGE_MAP()

// CDBPasswordChangerView construction/destruction

CDBPasswordChangerView::CDBPasswordChangerView()
	: CFormView(CDBPasswordChangerView::IDD)
{
	m_bPropertiesFile = false;
	m_bASPModule = false;
	m_bHTTPModule = false;
	m_bWebModule = false;
}

CDBPasswordChangerView::~CDBPasswordChangerView()
{
}

void CDBPasswordChangerView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PREV_PASSWORD, m_editPrevPassword);
	DDX_Control(pDX, IDC_EDIT_NEW_PASSWORD, m_editNewPassword);
	DDX_Control(pDX, IDC_BUTTON_CHANGE, m_btnChange);
	DDX_Control(pDX, IDC_CHECK_PROPERTIES_FILE, m_chkPropertiesFile);
	DDX_Control(pDX, IDC_CHECK_ASP_MODULE, m_chkASPModule);
	DDX_Control(pDX, IDC_CHECK_UBC_HTTP_MODULE, m_chkHTTPModule);
	DDX_Control(pDX, IDC_CHECK_UBC_WEB_MODULE, m_chkWebModule);
}

BOOL CDBPasswordChangerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CDBPasswordChangerView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	SetTimer(1025, 100, NULL);
}


// CDBPasswordChangerView diagnostics

#ifdef _DEBUG
void CDBPasswordChangerView::AssertValid() const
{
	CFormView::AssertValid();
}

void CDBPasswordChangerView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CDBPasswordChangerDoc* CDBPasswordChangerView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDBPasswordChangerDoc)));
	return (CDBPasswordChangerDoc*)m_pDocument;
}
#endif //_DEBUG


// CDBPasswordChangerView message handlers


void CDBPasswordChangerView::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 1025)
	{
		KillTimer(1025);

		ciDEBUG(10, ("Initialize") );

		CString str_pwd = GetDBPwdFromProperties();
		ciDEBUG(10, ("Properties Password=%s", str_pwd) );

		m_editPrevPassword.SetWindowText(str_pwd);
		m_editNewPassword.SetFocus();

		if(str_pwd == "")
		{
			ciERROR( ("Can't found password in UBC Properties file.", str_pwd) );
			::AfxMessageBox(IDS_PASSWORD_NOT_FOUND, MB_ICONSTOP);
			return;
		}
		else
		{
			m_chkPropertiesFile.SetCheck(BST_CHECKED);
			m_bPropertiesFile = true;
		}

		if( CheckASPPassword(DEFAULT_ASP_RELATIVE_PATH, str_pwd, "", m_listASPFile) )
		{
			ciDEBUG(10, ("Password found in ASP") );
			m_chkASPModule.SetCheck(BST_CHECKED);
			m_bASPModule = true;
		}
		else
		{
			ciWARN( ("Password not found in ASP") );
		}

		if( CheckConfigPassword(DEFAULT_HTTP_RELATIVE_PATH, str_pwd, "", m_listHTTPFile) )
		{
			ciDEBUG(10, ("Password found in HTTP") );
			m_chkHTTPModule.SetCheck(BST_CHECKED);
			m_bHTTPModule = true;
		}
		else
		{
			ciWARN( ("Password not found in HTTP") );
		}

		if( CheckConfigPassword(DEFAULT_WEB_RELATIVE_PATH, str_pwd, "", m_listWEBFile) )
		{
			ciDEBUG(10, ("Password found in WEB") );
			m_chkWebModule.SetCheck(BST_CHECKED);
			m_bWebModule = true;
		}
		else
		{
			ciWARN( ("Password not found in WEB") );
		}
	}

	CFormView::OnTimer(nIDEvent);
}

void CDBPasswordChangerView::OnBnClickedButtonChange()
{
	ciDEBUG(10, ("OnBnClickedButtonChange") );

	//
	CString str_prev_pwd;
	m_editPrevPassword.GetWindowText(str_prev_pwd);
	CString str_new_pwd;
	m_editNewPassword.GetWindowText(str_new_pwd);

	ciDEBUG(10, ("PrevPassword=%s", str_prev_pwd) );
	ciDEBUG(10, ("NewPassword=%s", str_new_pwd) );

	//
	int pos = 0;
	CString token = str_new_pwd.Tokenize("\\:;\"<>=& ", pos); // \ : ; " < > = &
	if(str_new_pwd != token)
	{
		ciWARN( ("Invalid Password=%s", str_new_pwd) );
		::AfxMessageBox(IDS_INVALID_PASSWORD, MB_ICONSTOP);
		return;
	}

	// set prperties
	CheckPropertiesPassword(str_prev_pwd, str_new_pwd);
#ifdef DEBUG
	ciXProperties* prop = ciXProperties::getInstance("D:\\Project\\ubc\\config\\ubc.properties");
#else
	ciXProperties* prop = ciXProperties::getInstance();
#endif
	if(prop == NULL)
	{
		ciERROR( ("ciXProperties::getInstance is NULL !!!") );
		return;
	}
	ciDEBUG(10, ("write properties") );
	prop->write();

	// change asp
	if( m_bASPModule &&
		CheckASPPassword(DEFAULT_ASP_RELATIVE_PATH, str_prev_pwd, str_new_pwd, m_listASPFile) )
	{
		ciDEBUG(10, ("Change ASP Password") );
		WriteFile(DEFAULT_ASP_RELATIVE_PATH, m_listASPFile);
	}

	// change http
	if( m_bHTTPModule &&
		CheckConfigPassword(DEFAULT_HTTP_RELATIVE_PATH, str_prev_pwd, str_new_pwd, m_listHTTPFile) )
	{
		ciDEBUG(10, ("Change HTTP Password") );
		WriteFile(DEFAULT_HTTP_RELATIVE_PATH, m_listHTTPFile);
	}

	// change web
	if( m_bWebModule &&
		CheckConfigPassword(DEFAULT_WEB_RELATIVE_PATH, str_prev_pwd, str_new_pwd, m_listWEBFile) )
	{
		ciDEBUG(10, ("Change WEB Password") );
		WriteFile(DEFAULT_WEB_RELATIVE_PATH, m_listWEBFile);
	}
}

void CDBPasswordChangerView::OnBnClickedCheckPropertiesFile()
{
	m_chkPropertiesFile.SetCheck( m_bPropertiesFile ? BST_CHECKED : BST_UNCHECKED );
}

void CDBPasswordChangerView::OnBnClickedCheckAspModule()
{
	m_chkASPModule.SetCheck( m_bASPModule ? BST_CHECKED : BST_UNCHECKED );
}

void CDBPasswordChangerView::OnBnClickedCheckUbcHttpModule()
{
	m_chkHTTPModule.SetCheck( m_bHTTPModule ? BST_CHECKED : BST_UNCHECKED );
}

void CDBPasswordChangerView::OnBnClickedCheckUbcWebModule()
{
	m_chkWebModule.SetCheck( m_bWebModule ? BST_CHECKED : BST_UNCHECKED );
}

bool CDBPasswordChangerView::getPropertiesValue(LPCSTR lpszKey, CString& value)
{
#ifdef DEBUG
	ciXProperties* prop = ciXProperties::getInstance("D:\\Project\\ubc\\config\\ubc.properties");
#else
	ciXProperties* prop = ciXProperties::getInstance();
#endif
	if(prop == NULL)
	{
		ciERROR( ("ciXProperties::getInstance is NULL !!!") );
		return false;
	}

	ciString buf = "";
	if(prop->get(lpszKey, buf) && buf.length() > 0)
	{
		value = buf.c_str();
		value.Trim(" \r\n");

		ciDEBUG(10, ("get %s=%s", lpszKey, (LPCSTR)value) );
		return true;
	}

	CString msg;
	msg.Format("%s is NULL !!!", lpszKey);
	ciWARN( ((LPCSTR)msg) );

	return false;
}

bool CDBPasswordChangerView::setPropertiesValue(LPCSTR lpszKey, LPCSTR lpszValue)
{
#ifdef DEBUG
	ciXProperties* prop = ciXProperties::getInstance("D:\\Project\\ubc\\config\\ubc.properties");
#else
	ciXProperties* prop = ciXProperties::getInstance();
#endif
	if(prop == NULL)
	{
		ciERROR( ("ciXProperties::getInstance is NULL !!!") );
		return false;
	}

	ciDEBUG(10, ("set %s=%s", lpszKey, lpszValue) );

	return prop->set(lpszKey, lpszValue);
}

CString CDBPasswordChangerView::GetDBPwdFromProperties()
{
	ciDEBUG(10, ("GetDBPwdFromProperties") );

	CString str_db_type = "";
	getPropertiesValue("PS.PS_OBJECT_MAP.UBC_EVENT", str_db_type);

	ciDEBUG(10, ("DBType=%s", str_db_type) );

	// PS.PS_OBJECT_MAP.UBC_EVENT=MSDB,MS_DB
	if(::strnicmp(str_db_type, "MSDB", 4) == 0)
	{
		// PS.MS_DB.COP_PWD=-507263a
		CString str_db_pwd = "";
		getPropertiesValue("PS.MS_DB.COP_PWD", str_db_pwd);

		ciDEBUG(10, ("DBPassword=%s", str_db_pwd) );
		return str_db_pwd;
	}
	// PS.PS_OBJECT_MAP.UBC_EVENT=MYSQL,MYSQL_DB
	else if(::strnicmp(str_db_type, "MYSQL", 5) == 0)
	{
		// PS.MYSQL_DB.PWD=sqicop
		CString str_db_pwd = "";
		getPropertiesValue("PS.MYSQL_DB.PWD", str_db_pwd);

		ciDEBUG(10, ("DBPassword=%s", str_db_pwd) );
		return str_db_pwd;
	}
	return "";
}

BOOL CDBPasswordChangerView::GetFile(LPCSTR lpszSubPath, CStringArray& line_list)
{
	ciDEBUG(10, ("GetFile(%s)", lpszSubPath) );

#ifdef DEBUG
	char* project_home = "D:\\Project\\ubc";
#else
	char* project_home = getenv("PROJECT_HOME");
#endif
	if(project_home == NULL)
	{
		// PROJECT_HOME is null
		ciERROR( ("PROJECT_HOME environment value is not defined !!!") );
		return FALSE;
	}

	CString str_full_path = project_home;
	if(str_full_path.GetLength() == 0)
	{
		ciERROR( ("PROJECT_HOME environment value is empty !!!") );
		return FALSE;
	}

	if( str_full_path.GetAt(str_full_path.GetLength()-1) != '\\' && lpszSubPath[0] != '\\')
		str_full_path += '\\';
	str_full_path += lpszSubPath;

	CFile file;
	if( !file.Open(str_full_path, CFile::modeRead | CFile::typeBinary) )
	{
		// file not found
		ciERROR( ("File not found (%s)", str_full_path) );
		return FALSE;
	}

	int file_size = file.GetLength();
	char* buf = new char[file_size + 1];
	::ZeroMemory(buf, file_size + 1);
	file.Read(buf, file_size);
	CString str_buf = buf;
	delete[]buf;

	GetLineList(str_buf, line_list);

	return TRUE;
}

BOOL CDBPasswordChangerView::CheckPropertiesPassword(LPCSTR lpszPrevPwd, LPCSTR lpszNewPwd)
{
	ciDEBUG(10, ("CheckPropertiesPassword") );
	ciDEBUG(10, ("PrevPassword", lpszPrevPwd) );
	ciDEBUG(10, ("NewPassword", lpszNewPwd) );

/*
	PS.MS_DB.COP_PWD=-507263a
	PS.MS_DB.COP_DSN_REG=DSN=ubc_event:Network=TCP/IP:Server=211.232.57.225,1433:Database=ubc:Description=ubc:Trusted_connection=No:
	PS.MYSQL_DB.PWD=sqicop
	PS.MYSQL_DB.COP_PWD=sqicop
	PS.MYSQL_DB.COP_DSN_REG=DSN=ubc_event;SERVER=211.232.57.186;UID=ubc;PWD=sqicop;DATABASE=dbo;PORT=3306;CHARSET=euckr;
*/
	//
	setPropertiesValue("PS.MS_DB.COP_PWD", lpszNewPwd);
	setPropertiesValue("PS.MYSQL_DB.PWD", lpszNewPwd);
	setPropertiesValue("PS.MYSQL_DB.COP_PWD", lpszNewPwd);

	//
	CString str_find[2];
	str_find[0] = "Pwd=";
	str_find[1] = "Password=";

	CString str_prop[2];
	str_prop[0] = "PS.MS_DB.COP_DSN_REG";
	str_prop[1] = "PS.MYSQL_DB.COP_DSN_REG";

	CString str_delimiter[2];
	str_delimiter[0] = ":";
	str_delimiter[1] = ";";

	for(int i=0; i<2; i++)
	{
		CString str_old, str_new;
		getPropertiesValue(str_prop[i], str_old);
		ciDEBUG(10, ("get %s=%s", str_prop[i], str_old) );

		int pos = 0;
		CString token = str_old.Tokenize(str_delimiter[i], pos);
		while(token != "")
		{
			int idx = 0;
			if(::_strnicmp(token, str_find[idx], str_find[idx].GetLength()) == 0 ||
				::_strnicmp(token, str_find[++idx], str_find[idx].GetLength()) == 0 )
			{
				str_new += str_find[idx];
				str_new += lpszNewPwd;
			}
			else
			{
				str_new += token;
			}

			str_new += str_delimiter[i];
			token = str_old.Tokenize(str_delimiter[i], pos);
		}

		ciDEBUG(10, ("set %s=%s", str_prop[i], str_new) );
		setPropertiesValue(str_prop[i], str_new);
	}

	return TRUE;
}

BOOL CDBPasswordChangerView::CheckASPPassword(LPCSTR lpszSubPath, LPCSTR lpszPrevPwd, LPCSTR lpszNewPwd, CStringArray& line_list)
{
	ciDEBUG(10, ("CheckASPPassword") );
	ciDEBUG(10, ("SubPath=%s", lpszSubPath) );
	ciDEBUG(10, ("PrevPassword=%s", lpszPrevPwd) );
	ciDEBUG(10, ("NewPassword=%s", lpszNewPwd) );

	line_list.RemoveAll();
	if( GetFile(lpszSubPath, line_list) == FALSE )
		return FALSE;

	CString str_prev_pwd[2];
	str_prev_pwd[0].Format("Pwd=%s;", lpszPrevPwd);
	str_prev_pwd[1].Format("Password=%s;", lpszPrevPwd);

	CString str_new_pwd[2];
	str_new_pwd[0].Format("Pwd=%s;", lpszNewPwd);
	str_new_pwd[1].Format("Password=%s;", lpszNewPwd);

	for(int i=0; i<line_list.GetCount(); i++)
	{
		CString str_line = line_list.GetAt(i);
		str_line.TrimLeft(" \t");
		str_line.TrimRight(" \t");

		int idx = 0;
		int pos = FindSubStringNoCase(str_line, str_prev_pwd[idx]);
		if(pos < 0) pos = FindSubStringNoCase(str_line, str_prev_pwd[++idx]);

		if(pos > 0 && str_line.GetAt(0) != '\'')
		{
			// find
			str_line = line_list.GetAt(i);
			ciDEBUG(10, ("find line=%s", str_line) );

			ReplaceSubStringNoCase(str_line, str_prev_pwd[idx], str_new_pwd[idx]);

			ciDEBUG(10, ("replace line=%s", str_line) );
			line_list.SetAt(i, str_line);
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CDBPasswordChangerView::CheckConfigPassword(LPCSTR lpszSubPath, LPCSTR lpszPrevPwd, LPCSTR lpszNewPwd, CStringArray& line_list)
{
	ciDEBUG(10, ("CheckConfigPassword") );
	ciDEBUG(10, ("SubPath=%s", lpszSubPath) );
	ciDEBUG(10, ("PrevPassword=%s", lpszPrevPwd) );
	ciDEBUG(10, ("NewPassword=%s", lpszNewPwd) );

	line_list.RemoveAll();
	if( GetFile(lpszSubPath, line_list) == FALSE )
		return FALSE;

	CString str_prev_pwd[4];
	str_prev_pwd[0].Format("Pwd=%s;", lpszPrevPwd);
	str_prev_pwd[1].Format("Password=%s;", lpszPrevPwd);
	str_prev_pwd[2].Format("Pwd=%s\"", lpszPrevPwd);
	str_prev_pwd[3].Format("Password=%s\"", lpszPrevPwd);

	CString str_new_pwd[4];
	str_new_pwd[0].Format("Pwd=%s;", lpszNewPwd);
	str_new_pwd[1].Format("Password=%s;", lpszNewPwd);
	str_new_pwd[2].Format("Pwd=%s\"", lpszNewPwd);
	str_new_pwd[3].Format("Password=%s\"", lpszNewPwd);

	bool find_conn_start = false;
	bool find_conn_end   = false;
	bool find_conn_string = false;
	for(int i=0; i<line_list.GetCount(); i++)
	{
		CString str_line = line_list.GetAt(i);
		str_line.TrimLeft(" \t");
		str_line.TrimRight(" \t");

		if( FindSubStringNoCase(str_line, "<connectionStrings>") >= 0)
		{
			ciDEBUG(10, ("find conn start") );
			find_conn_start = true;
		}
		if( FindSubStringNoCase(str_line, "</connectionStrings>") >= 0)
		{
			ciDEBUG(10, ("find conn end") );
			find_conn_end = true;
		}

		int idx = -1;
		int pos = -1;
		while( pos<0 && idx<(4-1) )
			pos = FindSubStringNoCase(str_line, str_prev_pwd[++idx]);

		if( pos > 0 && 
			find_conn_start == true && find_conn_end == false &&
			::_strnicmp(str_line, "<add", 4) == 0 )
		{
			ciDEBUG(10, ("find line=%s", str_line) );

			find_conn_string = true;
			str_line = line_list.GetAt(i);
			ReplaceSubStringNoCase(str_line, str_prev_pwd[idx], str_new_pwd[idx]);

			ciDEBUG(10, ("replace line=%s", str_line) );
			line_list.SetAt(i, str_line);
		}
	}

	if(find_conn_start && find_conn_end && find_conn_string)
		return TRUE;

	return FALSE;
}

BOOL CDBPasswordChangerView::WriteFile(LPCSTR lpszSubPath, CStringArray& line_list)
{
	ciDEBUG(10, ("WriteFile(%s)", lpszSubPath) );

#ifdef DEBUG
	char* project_home = "D:\\Project\\ubc";
#else
	char* project_home = getenv("PROJECT_HOME");
#endif
	if(project_home == NULL)
	{
		// PROJECT_HOME is null
		ciERROR( ("PROJECT_HOME environment value is not defined !!!") );
		return FALSE;
	}

	CString str_full_path = project_home;
	if(str_full_path.GetLength() == 0)
	{
		ciERROR( ("PROJECT_HOME environment value is empty !!!") );
		return FALSE;
	}

	if( str_full_path.GetAt(str_full_path.GetLength()-1) != '\\' && lpszSubPath[0] != '\\')
		str_full_path += '\\';
	str_full_path += lpszSubPath;

	CString str_full_path_bak         = str_full_path + ".bak";
	CString str_full_path_bak_bak     = str_full_path_bak + ".bak";
	CString str_full_path_bak_bak_bak = str_full_path_bak_bak + ".bak";

	::DeleteFile(str_full_path_bak_bak_bak);
	::MoveFile(str_full_path_bak_bak, str_full_path_bak_bak_bak);
	::MoveFile(str_full_path_bak, str_full_path_bak_bak);
	if( !::MoveFile(str_full_path, str_full_path_bak) )
	{
		ciERROR( ("MoveFile ERROR !!! (%s -> %s)", str_full_path, str_full_path_bak) );
		// file error
		::MoveFile(str_full_path_bak_bak, str_full_path_bak);
		::MoveFile(str_full_path_bak_bak_bak, str_full_path_bak_bak);
		return FALSE;
	}

	CFile file;
	if( !file.Open(str_full_path, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary) )
	{
		ciERROR( ("File create ERROR !!! (%s)", str_full_path) );
		// file error
		::DeleteFile(str_full_path);
		::MoveFile(str_full_path_bak, str_full_path);
		::MoveFile(str_full_path_bak_bak, str_full_path_bak);
		::MoveFile(str_full_path_bak_bak_bak, str_full_path_bak_bak);
		return FALSE;
	}

	for(int i=0; i<line_list.GetCount(); i++)
	{
		const CString& line = line_list.GetAt(i);
		file.Write((LPCSTR)line, line.GetLength());
		file.Write("\r\n", 2);
	}

	return TRUE;
}

BOOL CDBPasswordChangerView::Tokenize(CString& str, int& pos, CString& token)
{
	if(str.GetAt(pos) == 0) return FALSE;

	token = "";

	int start = pos;
	while ( str.GetAt(pos) != '\r' && 
			str.GetAt(pos) != '\n' && 
			str.GetAt(pos) != 0 )
		pos++;

	token = str.Mid(start, pos-start);

	if(str.GetAt(pos) == '\r') pos+=2;
	if(str.GetAt(pos) == '\n') pos+=1;

	return TRUE;
}

void CDBPasswordChangerView::GetLineList(CString& str, CStringArray& line_list)
{
	int pos = 0;
	CString token;
	BOOL token_exist = Tokenize(str, pos, token);
	while(token_exist)
	{
		line_list.Add(token);
		token_exist = Tokenize(str, pos, token);
	}
}

int CDBPasswordChangerView::FindSubStringNoCase(CString str, CString sub_str)
{
	//str.MakeLower();
	for(int i=0; i<str.GetLength(); i++)
	{
		char ch = str.GetAt(i);
		if( 'A' <= ch && ch <= 'Z' ) str.SetAt(i, 'a' + (ch - 'A') );
	}

	//sub_str.MakeLower();
	for(int i=0; i<sub_str.GetLength(); i++)
	{
		char ch = sub_str.GetAt(i);
		if( 'A' <= ch && ch <= 'Z' ) sub_str.SetAt(i, 'a' + (ch - 'A') );
	}

	return str.Find(sub_str);
}

BOOL CDBPasswordChangerView::ReplaceSubStringNoCase(CString& str, CString& old_sub_str, CString& new_sub_str)
{
	int pos = FindSubStringNoCase(str, old_sub_str);
	if(pos < 0) return FALSE;

	CString str_head = str.Left(pos);
	CString str_tail = str.Right(str.GetLength() - pos - old_sub_str.GetLength());

	str.Format("%s%s%s", str_head, new_sub_str, str_tail);
	return TRUE;
}


/*
ubc.properties
PS.PS_OBJECT_MAP.UBC_EVENT=MYSQL,MYSQL_DB
PS.MS_DB.COP_PWD=-507263a
PS.MS_DB.COP_DSN_REG=DSN=ubc_event:Network=TCP/IP:Server=211.232.57.225,1433:Database=ubc:Description=ubc:Trusted_connection=No:
PS.MYSQL_DB.PWD=sqicop
PS.MYSQL_DB.COP_PWD=sqicop
PS.MYSQL_DB.COP_DSN_REG=DSN=ubc_event;SERVER=211.232.57.186;UID=ubc;PWD=sqicop;DATABASE=dbo;PORT=3306;CHARSET=euckr;

LocalSettings.asp
objConn.Open "Driver={SQL Server}; Server=127.0.0.1; Database=ubc; Uid=ubc; Pwd=-507263a;"

UBC_HTTP/Web.config
  <connectionStrings>
    <!-- 데이타 베이스 -->
    <add name="MsSQL" providerName="System.Data.SqlClient" connectionString="Data Source=211.232.57.186;Initial Catalog=ubc;Persist Security Info=True;User ID=ubc;Password=-507263a"/>
    <add name="MySQL" providerName="MySql.Data.MySqlClient" connectionString="Server=211.232.57.186;Database=dbo;Uid=ubc;Pwd=sqicop;"/>
  </connectionStrings>

UBC_WEB/Web.config
	<connectionStrings>
    <!-- 데이타 베이스 -->
		<add name="ubc" providerName="System.Data.SqlClient" connectionString="Data Source=211.232.57.215;Initial Catalog=ubc;Persist Security Info=True;User ID=ubc;Password=-507263a"/>
		<!-- 프로파일 정보 데이타 베이스 -->
		<add name="profile" connectionString="Data Source=211.232.57.215;Initial Catalog=aspnetdb;Persist Security Info=True;User ID=ubc;password=-507263a;" providerName="System.Data.SqlClient"/>
	</connectionStrings>
*/