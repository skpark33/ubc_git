// DBPasswordChangerDoc.cpp : implementation of the CDBPasswordChangerDoc class
//

#include "stdafx.h"
#include "DBPasswordChanger.h"

#include "DBPasswordChangerDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDBPasswordChangerDoc

IMPLEMENT_DYNCREATE(CDBPasswordChangerDoc, CDocument)

BEGIN_MESSAGE_MAP(CDBPasswordChangerDoc, CDocument)
END_MESSAGE_MAP()


// CDBPasswordChangerDoc construction/destruction

CDBPasswordChangerDoc::CDBPasswordChangerDoc()
{
	// TODO: add one-time construction code here

}

CDBPasswordChangerDoc::~CDBPasswordChangerDoc()
{
}

BOOL CDBPasswordChangerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CDBPasswordChangerDoc serialization

void CDBPasswordChangerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CDBPasswordChangerDoc diagnostics

#ifdef _DEBUG
void CDBPasswordChangerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDBPasswordChangerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CDBPasswordChangerDoc commands
