// VideoPlayerDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"


class CBasicInterface;


// CVideoPlayerDlg 대화 상자
class CVideoPlayerDlg : public CDialog
{
// 생성입니다.
public:
	CVideoPlayerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIDEOPLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	DECLARE_MESSAGE_MAP()

	HWND		m_hMsgRevcWnd;
	HWND		m_hPlayScreenWnd;
	CString		m_strFullPath[10];

	CBasicInterface*	m_pInterface[10];

	int			m_nCurrentPlayIndex;
public:

	void	SetParentWnd(HWND hMsgRevcWnd, HWND hPlayScreenWnd) { m_hMsgRevcWnd=hMsgRevcWnd; m_hPlayScreenWnd=hPlayScreenWnd; };
	void	SetFilename(LPCTSTR lpszFullpath) { for(int i=0; i<10; i++) if(m_strFullPath[i].GetLength()==0) { m_strFullPath[i]=lpszFullpath; break; } };
	CEdit m_editFullPath;
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	static DWORD	m_dwStartTime;

	void	Play();
	void	Stop();

};
