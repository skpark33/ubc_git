
#include "stdafx.h"


#include <io.h>

#include "Log.h"
//#include "EncodeDecode.h"

//#include "LogDialog.h"
#include "Dbghelp.h"

bool	_log_no_init = true;
CString	g_strLogFilename;
CCriticalSection g_csLog;

CFile	logfp;

void __LOG_INIT__(LPCTSTR lpszLogFilename, bool bEraseOldLog)
{
	_log_no_init = false;

	CString strLogFilename;
	if(lpszLogFilename == NULL && g_strLogFilename.GetLength() == 0)
	{

		if(strLogFilename.LoadString(AFX_IDS_APP_TITLE))
		{
			strLogFilename.Append(_T(".log"));
			lpszLogFilename = strLogFilename;
		}
		else
		{
			lpszLogFilename = _T("__LOG__.log");
		}
		
	}

	if(g_strLogFilename.GetLength() == 0)
	{
		static CString	_mainDirectory = _T("");

		if(_mainDirectory.GetLength() == 0)
		{
			TCHAR str[MAX_PATH];
			::ZeroMemory(str, MAX_PATH);
			::GetModuleFileName(NULL, str, MAX_PATH);
			int length = _tcslen(str) - 1;
			while( (length > 0) && (str[length] != _T('\\')) )
				str[length--] = 0;

			_mainDirectory = str;
		}

		g_strLogFilename.Format(_T("%s%s"), _mainDirectory, lpszLogFilename );
	}

	g_csLog.Lock();

	if(logfp.m_hFile != CFile::hFileNull)
		logfp.Close();

	//로그를 처음 open할 때, 해당 경로폴더가 없으면 로그 open 실패를방지
	//경로의 "/"를 "\\"로 변경
	g_strLogFilename.Replace(_T("/"), _T("\\"));
	MakeSureDirectoryPathExists(g_strLogFilename);

	if(bEraseOldLog)
	{
		//로그파일의 백업본을 만든다.
		if(_access(g_strLogFilename, 0) == 0)	//.log파일이 있다면...
		{
			CString strBakup = g_strLogFilename;
			strBakup.Append(_T(".bak"));
			if(_access(strBakup, 0) == 0)		//.bak파일이 있다면...
			{
				//.bak 파일은 .bak.bak로 변경
				MoveFileEx(strBakup, strBakup+_T(".bak"), MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING);
			}//if

			//.log 파일은 .bak로 변경
			MoveFileEx(g_strLogFilename, strBakup, MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING);
		}//if

		logfp.Open(g_strLogFilename, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary | CFile::shareDenyWrite );

#ifdef _UNICODE
		unsigned short header = 0xfeff;
		logfp.Write(&header, sizeof(header));
		//logfp.Flush();
#endif
	}
	else
	{
		logfp.Open(g_strLogFilename, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::typeBinary | CFile::shareDenyWrite );
	}

	g_csLog.Unlock();
}

void __LOG_STOP__()
{
	//logfp.Flush();
	_log_no_init = false;
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, bool bValue)
{
	if(_log_no_init) return;

	g_csLog.Lock();

	if(logfp.m_hFile == CFile::hFileNull)
		__LOG_INIT__();

	if(logfp.m_hFile != CFile::hFileNull)
	{
		TCHAR timebuf[64], datebuf[64];
		_tstrtime( timebuf );
		_tstrdate( datebuf );

#ifdef _UNICODE
		CString strFuncName;
		ANSItoUNICODE(lpszFuncName, strFuncName);
		if(bValue)
			_ftprintf(fp, _T("[%s %s] [%s] %s (line:%d) %s = true\r\n"), datebuf, timebuf, lpszType, strFuncName, nLine, lpszVar);
		else
			_ftprintf(fp, _T("[%s %s] [%s] %s (line:%d) %s = false\r\n"), datebuf, timebuf, lpszType, strFuncName, nLine, lpszVar);
#else
		if(bValue)
		{
			TCHAR buf[4096];

			LPTSTR tmp = (LPTSTR)lpszVar;
			while(*tmp == _T('\t')) tmp++;
			_stprintf(buf, _T("[%s %s] [%s] %s (line:%d) %s = true\r\n"), datebuf, timebuf, lpszType, lpszFuncName, nLine, tmp);
			logfp.Write(buf, _tcslen(buf)*sizeof(TCHAR));

			_stprintf(buf, _T("%s = true\r\n"), lpszVar);
			TRACE(buf);

			//CLogDialog* log_dialog = CLogDialog::getInstance();
			//if(log_dialog && log_dialog->GetSafeHwnd())
			//{
			//	CString* pStr = new CString(buf);
			//	log_dialog->PostMessage(WM_DEBUG_STRING, (WPARAM)pStr);
			//}
		}
		else
		{
			TCHAR buf[4096];

			_stprintf(buf, _T("[%s %s] [%s] %s (line:%d) %s = false\r\n"), datebuf, timebuf, lpszType, lpszFuncName, nLine, lpszVar);
			logfp.Write(buf, _tcslen(buf)*sizeof(TCHAR));

			_stprintf(buf, _T("%s = false\r\n"), lpszVar);
			TRACE(buf);

			//CLogDialog* log_dialog = CLogDialog::getInstance();
			//if(log_dialog && log_dialog->GetSafeHwnd())
			//{
			//	CString* pStr = new CString(buf);
			//	log_dialog->PostMessage(WM_DEBUG_STRING, (WPARAM)pStr);
			//}
		}
#endif
		//logfp.Flush();
	}

	g_csLog.Unlock();
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, int nValue)
{
	__log__(lpszType, lpszFuncName, nLine, lpszVar, (LONGLONG)nValue);
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, unsigned int nValue)
{
	__log__(lpszType, lpszFuncName, nLine, lpszVar, (ULONGLONG)nValue);
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, double fValue)
{
	if(_log_no_init) return;

	g_csLog.Lock();


	if(logfp.m_hFile == CFile::hFileNull)
		__LOG_INIT__();

	if(logfp.m_hFile != CFile::hFileNull)
	{
		TCHAR timebuf[64], datebuf[64];
		_tstrtime( timebuf );
		_tstrdate( datebuf );

#ifdef _UNICODE
		CString strFuncName;
		ANSItoUNICODE(lpszFuncName, strFuncName);
		_ftprintf(fp, _T("[%s %s] %s (line:%d) %s = %.3f\r\n"), datebuf, timebuf, strFuncName, nLine, lpszVar, fValue);
#else
		TCHAR buf[4096];

		LPTSTR tmp = (LPTSTR)lpszVar;
		while(*tmp == _T('\t')) tmp++;
		_stprintf(buf, _T("[%s %s] [%s] %s (line:%d) %s = %.3f\r\n"), datebuf, timebuf, lpszType, lpszFuncName, nLine, tmp, fValue);
		logfp.Write(buf, _tcslen(buf)*sizeof(TCHAR));

		_stprintf(buf, _T("%s = %.3f\r\n"), lpszVar, fValue);
		TRACE(buf);

		//CLogDialog* log_dialog = CLogDialog::getInstance();
		//if(log_dialog && log_dialog->GetSafeHwnd())
		//{
		//	CString* pStr = new CString(buf);
		//	log_dialog->PostMessage(WM_DEBUG_STRING, (WPARAM)pStr);
		//}
#endif
		//logfp.Flush();
	}

	g_csLog.Unlock();
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, LONGLONG nValue)
{
	if(_log_no_init) return;

	g_csLog.Lock();

	if(logfp.m_hFile == CFile::hFileNull)
		__LOG_INIT__();

	if(logfp.m_hFile != CFile::hFileNull)
	{
		TCHAR timebuf[64], datebuf[64];
		_tstrtime( timebuf );
		_tstrdate( datebuf );

#ifdef _UNICODE
		CString strFuncName;
		ANSItoUNICODE(lpszFuncName, strFuncName);
		_ftprintf(fp, _T("[%s %s] [%s] %s (line:%d) %s = %I64d\r\n"), datebuf, timebuf, lpszType, strFuncName, nLine, lpszVar, nValue);
#else
		TCHAR buf[4096];

		LPTSTR tmp = (LPTSTR)lpszVar;
		while(*tmp == _T('\t')) tmp++;
		_stprintf(buf, _T("[%s %s] [%s] %s (line:%d) %s = %I64d\r\n"), datebuf, timebuf, lpszType, lpszFuncName, nLine, tmp, nValue);
		logfp.Write(buf, _tcslen(buf)*sizeof(TCHAR));

		_stprintf(buf, _T("%s = %I64d\r\n"), lpszVar, nValue);
		TRACE(buf);

		//CLogDialog* log_dialog = CLogDialog::getInstance();
		//if(log_dialog && log_dialog->GetSafeHwnd())
		//{
		//	CString* pStr = new CString(buf);
		//	log_dialog->PostMessage(WM_DEBUG_STRING, (WPARAM)pStr);
		//}
#endif
		//logfp.Flush();
	}

	g_csLog.Unlock();
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, ULONGLONG nValue)
{
	if(_log_no_init) return;

	g_csLog.Lock();

	if(logfp.m_hFile == CFile::hFileNull)
		__LOG_INIT__();

	if(logfp.m_hFile != CFile::hFileNull)
	{
		TCHAR timebuf[64], datebuf[64];
		_tstrtime( timebuf );
		_tstrdate( datebuf );

#ifdef _UNICODE
		CString strFuncName;
		ANSItoUNICODE(lpszFuncName, strFuncName);
		_ftprintf(fp, _T("[%s %s] [%s] %s (line:%d) %s = %I64u\r\n"), datebuf, timebuf, lpszType, strFuncName, nLine, lpszVar, nValue);
#else
		TCHAR buf[4096];

		LPTSTR tmp = (LPTSTR)lpszVar;
		while(*tmp == _T('\t')) tmp++;
		_stprintf(buf, _T("[%s %s] [%s] %s (line:%d) %s = %I64u\r\n"), datebuf, timebuf, lpszType, lpszFuncName, nLine, tmp, nValue);
		logfp.Write(buf, _tcslen(buf)*sizeof(TCHAR));

		_stprintf(buf, _T("%s = %I64u\r\n"), lpszVar, nValue);
		TRACE(buf);

		//CLogDialog* log_dialog = CLogDialog::getInstance();
		//if(log_dialog && log_dialog->GetSafeHwnd())
		//{
		//	CString* pStr = new CString(buf);
		//	log_dialog->PostMessage(WM_DEBUG_STRING, (WPARAM)pStr);
		//}
#endif
		//logfp.Flush();
	}

	g_csLog.Unlock();
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, LPCTSTR lpszVar, LPCTSTR strValue)
{
	if(_log_no_init) return;

	g_csLog.Lock();

	if(logfp.m_hFile == CFile::hFileNull)
		__LOG_INIT__();

	if(logfp.m_hFile != CFile::hFileNull)
	{
		TCHAR timebuf[64], datebuf[64];
		_tstrtime( timebuf );
		_tstrdate( datebuf );

#ifdef _UNICODE
		CString strFuncName;
		ANSItoUNICODE(lpszFuncName, strFuncName);
		if(strValue)
			_ftprintf(fp, _T("[%s %s] [%s] %s (line:%d) %s = %s\r\n"), datebuf, timebuf, lpszType, strFuncName, nLine, lpszVar, strValue);
		else
			_ftprintf(fp, _T("[%s %s] [%s] %s (line:%d) %s\r\n"), datebuf, timebuf, lpszType, strFuncName, nLine, lpszVar);
#else
		if(strValue)
		{
			TCHAR buf[4096];

			LPTSTR tmp = (LPTSTR)lpszVar;
			while(*tmp == _T('\t')) tmp++;
			_stprintf(buf, _T("[%s %s] [%s] %s (line:%d) %s = %s\r\n"), datebuf, timebuf, lpszType, lpszFuncName, nLine, tmp, strValue);
			logfp.Write(buf, _tcslen(buf)*sizeof(TCHAR));

			_stprintf(buf, _T("%s = %s\r\n"), lpszVar, strValue);
			TRACE(buf);

			//CLogDialog* log_dialog = CLogDialog::getInstance();
			//if(log_dialog && log_dialog->GetSafeHwnd())
			//{
			//	CString* pStr = new CString(buf);
			//	log_dialog->PostMessage(WM_DEBUG_STRING, (WPARAM)pStr);
			//}
		}
		else
		{
			TCHAR buf[4096];

			LPTSTR tmp = (LPTSTR)lpszVar;
			while(*tmp == _T('\t')) tmp++;
			_stprintf(buf, _T("[%s %s] [%s] %s (line:%d) %s\r\n"), datebuf, timebuf, lpszType, lpszFuncName, nLine, tmp);
			logfp.Write(buf, _tcslen(buf)*sizeof(TCHAR));

			if(_tcscmp(lpszVar, _T("begin...")) != 0 && _tcscmp(lpszVar, _T("end.")) != 0)
			{
				_stprintf(buf, _T("%s\r\n"), lpszVar);
				TRACE(buf);

				//CLogDialog* log_dialog = CLogDialog::getInstance();
				//if(log_dialog && log_dialog->GetSafeHwnd())
				//{
				//	CString* pStr = new CString(buf);
				//	log_dialog->PostMessage(WM_DEBUG_STRING, (WPARAM)pStr);
				//}
			}
		}
#endif
		//logfp.Flush();
	}

	g_csLog.Unlock();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, bool bValue)
{
	CString msg;
	msg.Format(nVar);

	__log__(lpszType, lpszFuncName, nLine, msg, bValue);
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, int nValue)
{
	CString msg;
	msg.LoadString(nVar);

	__log__(lpszType, lpszFuncName, nLine, msg, nValue);
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, unsigned int nValue)
{
	CString msg;
	msg.LoadString(nVar);

	__log__(lpszType, lpszFuncName, nLine, msg, nValue);
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, double fValue)
{
	CString msg;
	msg.LoadString(nVar);

	__log__(lpszType, lpszFuncName, nLine, msg, fValue);
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, LONGLONG nValue)
{
	CString msg;
	msg.LoadString(nVar);

	__log__(lpszType, lpszFuncName, nLine, msg, nValue);
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, ULONGLONG nValue)
{
	CString msg;
	msg.LoadString(nVar);

	__log__(lpszType, lpszFuncName, nLine, msg, nValue);
}

void __log__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine, unsigned int nVar, LPCTSTR strValue)
{
	CString msg;
	msg.LoadString(nVar);

	__log__(lpszType, lpszFuncName, nLine, msg, strValue);
}
