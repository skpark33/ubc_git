#pragma once


#include <dshow.h>
#include "Mpconfig.h" // Overlay
#include <reftime.h>

#pragma comment( lib, "strmiids.lib" )
#pragma comment( lib, "Quartz.lib" )


#ifndef _NULL
	#define _NULL
#endif

//#define SAFE_RELEASE( x ) { if( x ) x->Release(); x = NULL; }
#define SAFE_RELEASE( x ) { if( x ) { int n = x->Release(); TRACE("Ref count = %d\r\n", n); __DEBUG__("Ref count", n); } x = NULL; }

//#define JIF( x ) if( FAILED(hr = ( x )) ) { return hr; }

#define JIF(x) {if(FAILED(hr=(x))) \
 {TRACE(TEXT("FAILED(hr=0x%x)in ") TEXT(#x) \
 TEXT("\n"), hr); __DEBUG__("JIF FAIL !!!!!!!!!!!!!!", NULL); return hr;}}
 
#define LIF(x) {if(FAILED(hr=(x))) \
 {TRACE(TEXT("FAILED(hr=0x%x)in ") TEXT(#x) \
 TEXT("\n"), hr);}}

#define JIB(x) {if(FAILED(hr=(x))) \
 {TRACE(TEXT("FAILED(hr=0x%x)in ") TEXT(#x) \
 TEXT("\n"), hr); __DEBUG__("JIB FAIL !!!!!!!!!!!!!!", NULL); return false;}}

#define REGISTER_FILTERGRAPH

 enum ASPECT_RATIO_MODE
{
	STRETCHED,	    // don't do any aspect ratio correction
	LETTER_BOX,	    // letter box the video, paint background color in the excess region
	CROP,		    // crop the video to the right aspect ratio
	STRETCHED_AS_PRIMARY  // follow whatever the primary stream does (in terms of the mode as well as pict-aspect-ratio values)
};

 enum E_VIDEO_RENDER_TYPE
{ 
	E_DUMMY_OVERLAY = 1,	//Dummy Overlay 모드
	E_WINDOWLESS,			//Windowless 모드
	E_WINDOWED,				//Windowed 모드
	E_EVR,
	E_OVERLAY = 9			//Overlay 모드
};

 enum E_OS_VERSION
{
	E_OS_WINXP = 5,
	E_OS_WIN7
};

#ifndef WM_DSINTERFACES_GRAPHNOTIFY
	#define WM_DSINTERFACES_GRAPHNOTIFY		(WM_USER+100)
#endif
#ifndef WM_DSINTERFACES_COMPLETEPLAY
	#define	WM_DSINTERFACES_COMPLETEPLAY	 (WM_USER+101)
#endif

enum PLAYSTATE { NOTHING, BEFORE, PLAY, PAUSE, STOP, REWIND, FORWARD };


//! 동영상 재생을 위한 기본 클래스
/*!
*/
class CBasicInterface
{
public:
	CBasicInterface(CWnd* pWnd);				///<생성자
	virtual ~CBasicInterface(void);				///<소멸자

	HWND				m_hParentWnd;			///<동영상이 표시될 창의 핸들

	CString				m_strFilePath;			///<동영상 파일의 경로
	CString				m_strSamiPath;			///<자막 파일의 경로
	//CCriticalSection	m_csLock;				///<동기화 객체

	ASPECT_RATIO_MODE	m_AspectRatioMode;		///<Aspectratio mode 설정 값
	DWORD				m_dwRotId;				///<Rot 테이블에 등록되는 아이디
	int					m_nRenderType;			///<랜더링 타입
	bool				m_bOpen;				///<파일이 열린상태
	bool				m_bIsPlay;				///<동영상이 재생중인지 상태
	bool				m_bIsWindowMedia;		///<WMV 형식의 동영상인지 여부

	IGraphBuilder*		m_pGraphBuilder;		///<그래프 빌더 인터페이스
	IBaseFilter*		m_pFilter;				///<랜더 필터 인터페이스

	// Methods
	virtual void	SetParentWindow(HWND hParentWnd) { m_hParentWnd = hParentWnd; };	///<동영상이 그려질 윈도우 핸들을 설정한다.
	virtual void	ReleaseAllInterfaces(void) = 0;						///<사용한 모든 필터를 해제한다.
	virtual bool	ChangeParentWindow(HWND hParentWnd) = 0;				///<동영상이 그려질 윈도우 핸들을 변경한다.

	virtual bool	SetAspectRatioMode(ASPECT_RATIO_MODE mode) = 0;		///<Aspectratio mode를 설정한다.
	virtual bool	GetVideoRect(LPRECT pRect) = 0;						///<동영상의 크기를 얻어온다.
	virtual bool	SetVideoRect(LPRECT pRect) = 0;						///<동영상의 크기를 설정한다.

	virtual double	GetTotalPlayTime(void);								///<동영상 파일의 총 재생시간을 반환한다.
	virtual double	GetCurrentPlayTime(void);							///<동영상 파일의 현재 재생시간을 반환한다.
	virtual bool	SetVolume(int nVol);								///<동영상의 음량을 설정한다.
	virtual void    SetPosition(double pos);							///<동영상의 지정된 위치로 이동한다.
	virtual void	SeekToStart(void);									///<동영상의 처음 위치로 이동한다.

	virtual bool	Open(LPCSTR lpszFilename) = 0;						///<동영상 파일을 open한다.
	virtual void	Close(void);										///<동영상 파일을 닫는다.
	virtual bool	Play(void);											///<동영상 파일을 재생한다.
	virtual bool	Pause(void);										///<동영상 파일의 재생을 일시 중지 한다.
	virtual bool	Stop(void);											///<동영상 파일의 재생을 중지 한다.

	virtual void			SetRenderType(int nType) { m_nRenderType = nType; }	///<Video render의 type을 설정한다.
	virtual bool			RePaintVideo(void)		 { return true; }			///<동영상을 다시 그려준다.
	virtual	bool			IsEventPlayEnd(void);								///<동영상 재생이 끝난 이벤트인지를 반환한다.
	virtual bool			SetWindowsMediaFilters(LPCWSTR lpszFileName);		///<윈도우 미디어 파일을 재생하기위한 필터들을 구성한다.
	virtual bool			IsWindowsMediaFile(LPCTSTR lpszFilename);			///<윈도우 미디어 파일인지를 판단한다.
	virtual IBaseFilter*	CreateEncodec(CString inFriendlyName);				///<코덱의 이름으로 필터를 찾아준다

	void			WaitForState(FILTER_STATE p_State,
								LONG lWaitTime = INFINITE);				///<미디어 콘트롤이 지정된 상태가 될때 까지 대기하도록 한다.
	long			GetMediaControlState(void);							///<미디어 콘트롤의 상태를 얻어온다.
	bool			HandleGraphNotify(LPARAM lParam);					///<필터그래프의 이벤트를 처리한다.

	bool			AddToRot(IUnknown *pUnkGraph);						///<Running time object 테이블에 등록을 한다.
	void			RemoveFromRot(void);								///<Running time object 테이블에서 제거한다.

protected:

	CWnd*			m_pParentWnd;										///<이벤트 등을 설정하는 부모 클래스

	CString			GetSamiFile(CString strFilename);					///<자막 파일의 이름을 구한다.
	IBaseFilter*	FindFilterFromName(IGraphBuilder *pGraphBuilder,
											LPTSTR szNameToFind);		///<그래프빌더에서 필터의 이름으로 해당하는 필터인터페이스를 찾는다.
	bool			IsPinConnected(IPin* pPin);							///<해당하는 pin이 연결된 상태인지 구한다.			
};


