#include "stdafx.h"
#include "AviInfo.h"

#define	AVIINFO_OK				0
#define AVIINFO_ERR				-1
#define AVIINFO_IOERR				-1
#define AVIINFO_INVALIDFILE			-2
#define AVIINFO_THREADERR			-3
#define AVIINFO_TIMEOUT				-4
#define AVIINFO_PARAMERR			-5
#define AVIINFO_NEED_MORE_DATA		-6

static char*	g_buff;
static int	g_buffSize		= 0;
static int	g_buffOffset	= 0;

static int	g_nStrmID, g_nMaxAVStrm, g_nAVStrm, *g_pnStrm;
static AVIStreamInfo	*g_pStrmInfo;


static int avi_Open(char* pData, int dataLen);
static int avi_Seek(int offset);
static void avi_Close(void);
static int Traversestrl(INT64 nStart, INT64 nEnd);
static int Traversehdrl(INT64 nStart, INT64 nEnd);
static int TraverseAVI(INT64 nStart, INT64 nEnd);
static int GetAVIInfoMain();

static int avi_Open(char* pData, int dataLen)
{
	g_buff = pData;
	g_buffSize = dataLen;
	g_buffOffset = 0;

	return 1;
}

static int avi_Seek(int offset)
{
	g_buffOffset = offset;

	return 1;
}

static int avi_Read(void *pBuf, int nLen)
{
	if (g_buffOffset > g_buffSize)
		return 0;
	
	memcpy(pBuf, g_buff + g_buffOffset, nLen);
	g_buffOffset += nLen;

	return 1;
}

static void avi_Close(void)
{
	g_buff = NULL;
	g_buffSize = 0;
	g_buffOffset = 0;
}



static int		Traversestrl(INT64 nStart, INT64 nEnd)
{
	DWORD			tmp[2];
	BOOL			bstrh, bstrf;
	STREAMHEADER	strh;
	union {
		VIDSTREAMFORMAT			vid;
		AUDBASICSTREAMFORMAT	aud;
	} strf;
	DWORD			nstrfsize;

	bstrh = FALSE;
	bstrf = FALSE;

	while (nStart + 8 <= nEnd && !(bstrh && bstrf))
	{
		if (!avi_Seek((int)nStart))
			return AVIINFO_IOERR;
		if (!avi_Read(tmp, 8))
			return AVIINFO_IOERR;
		if (nEnd - nStart - 8 < tmp[1])
		{
			//TPrint("need more data.\r\n");
			return AVIINFO_NEED_MORE_DATA;
		}

		switch (tmp[0]) {
			case FCCstrh :
				if (bstrh || (tmp[1] < sizeof(STREAMHEADER)))
					return AVIINFO_INVALIDFILE;
				if (!avi_Read(&strh, sizeof(STREAMHEADER)))
					return AVIINFO_IOERR;
				bstrh = TRUE;
				break;
			case FCCstrf :
				if (bstrf)
					return AVIINFO_INVALIDFILE;
				bstrf = TRUE;
				nstrfsize = (tmp[1] > sizeof(strf)) ? sizeof(strf) : tmp[1];
				if (!avi_Read(&strf, nstrfsize))
					return AVIINFO_IOERR;
				break;
		}
		nStart += tmp[1];
		nStart += 8 + (tmp[1] & 1);
	}

	if (!bstrh)
		return AVIINFO_INVALIDFILE;
	if (strh.fccType != FCCvids && strh.fccType != FCCauds)
		return AVIINFO_OK;
	if (g_nAVStrm >= g_nMaxAVStrm)
		return AVIINFO_MORESTREAM;
	
	switch (strh.fccType) {
		case FCCvids :
			g_pStrmInfo[g_nAVStrm].type = aistVideo;
			switch (strf.vid.biCompression) {
				case FCCDIVX :
				case FCCdivx :
					g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcDivX4;
					break;
				case FCCDX50 :
				case FCCdx50 :
					g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcDivX5;
					break;
				case FCCXVID :
				case FCCxvid :
					g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcXviD;
					break;
				case FCCDIV3 :
				case FCCdiv3 :
					if (strh.fccHandler == FCCDIV4 || strh.fccHandler == FCCdiv4) {
						g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcDivX3FastM;
					} else {
						g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcDivX3LowM;
					}
					break;
				case FCCDIV4 :
				case FCCdiv4 :
					g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcDivX3FastM;
					break;
				case FCCMP43 :
				case FCCmp43 :
					g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcMSMPEG4V3;
					break;
				case FCCWMV3 :
				case FCCwmv3 :
					g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcWMV9;
					break;
				default :
					g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codec = aivcUnknown;
					break;
			}
			g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.codecEx = strf.vid.biCompression;
			g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.nWid = strf.vid.biWidth;
			g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.nHgt = strf.vid.biHeight;

#if defined (INT_ONLY_VERSION)
			g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.fps_mulby1000 = (INT64)strh.dwRate * 1000 / strh.dwScale;
#else
			g_pStrmInfo[g_nAVStrm].avSpecific.videoInfo.fps = (double)strh.dwRate / strh.dwScale;
#endif
			break;

		case FCCauds :
			g_pStrmInfo[g_nAVStrm].type = aistAudio;
			switch (strf.aud.wFormatTag) {
#define AUDMATCH(x)		case AUD_FORMAT_##x : g_pStrmInfo[g_nAVStrm].avSpecific.audioInfo.codec = aiac##x; break;
				AUDMATCH(PCM);
				AUDMATCH(MSADPCM);
				AUDMATCH(MP3);
				AUDMATCH(WMA1);
				AUDMATCH(WMA2);
				AUDMATCH(AC3);
				AUDMATCH(DTS);
#undef AUDMATCH
				default :
					g_pStrmInfo[g_nAVStrm].avSpecific.audioInfo.codec = aiacUnknown;
					break;
			}
			g_pStrmInfo[g_nAVStrm].avSpecific.audioInfo.codecEx = strf.aud.wFormatTag;
			g_pStrmInfo[g_nAVStrm].avSpecific.audioInfo.nBitRate = strf.aud.nAvgBytesPerSec * 8;
			g_pStrmInfo[g_nAVStrm].avSpecific.audioInfo.nSampleRate = strf.aud.nSamplesPerSec;
			g_pStrmInfo[g_nAVStrm].avSpecific.audioInfo.nChannels = strf.aud.nChannels;
			break;
	}

#if defined (INT_ONLY_VERSION)
	g_pStrmInfo[g_nAVStrm].msDur = ((INT64)strh.dwStart + strh.dwLength) * strh.dwScale * 1000 / strh.dwRate;
#else
	g_pStrmInfo[g_nAVStrm].secDur = ((double)strh.dwStart + strh.dwLength) * strh.dwScale / strh.dwRate;
#endif
	
	g_nAVStrm++;

	return AVIINFO_OK;
}

static int		Traversehdrl(INT64 nStart, INT64 nEnd)
{
	DWORD	tmp[2], dwlisttype;
	int		nRet;

	while (nStart + 8 <= nEnd)
	{
		if (!avi_Seek((int)nStart))
			return AVIINFO_IOERR;
		if (!avi_Read(tmp, 8))
			return AVIINFO_IOERR;
		if (nEnd - nStart - 8 < tmp[1])
		{
			//TPrint("need more data.\r\n");
			return AVIINFO_NEED_MORE_DATA;
		}
		
		switch (tmp[0])
		{
			case FCCLIST :
				if (tmp[1] < 4)
					return AVIINFO_INVALIDFILE;
				if (!avi_Read(&dwlisttype, 4))
					return AVIINFO_IOERR;
				switch (dwlisttype) {
					case FCCstrl :
						if (g_nStrmID == MAX_STREAM)
							return AVIINFO_INVALIDFILE;
						nRet = Traversestrl(nStart + 12, nStart + tmp[1] + 8);
						if (nRet != AVIINFO_OK)
							return nRet;
						g_nStrmID++;
						break;
				}
				break;
		}
		nStart += tmp[1];
		nStart += 8 + (tmp[1] & 1);
	}

	return AVIINFO_OK;
}

static int		TraverseAVI(INT64 nStart, INT64 nEnd)
{
	DWORD	tmp[2], dwlisttype;

	g_nStrmID = 0;

	while (nStart + 8 <= nEnd)
	{
		if (!avi_Seek((int)nStart))
			return AVIINFO_IOERR;
		if (!avi_Read(tmp, 8))
			return AVIINFO_IOERR;
		if (nEnd - nStart - 8 < tmp[1])
		{
			//TPrint("need more data.\r\n");
			return AVIINFO_NEED_MORE_DATA;
		}
		switch (tmp[0])
		{
			case FCCLIST :
				if (tmp[1] < 4)
					return AVIINFO_INVALIDFILE;
				if (!avi_Read(&dwlisttype, 4))
					return AVIINFO_IOERR;
				switch (dwlisttype)
				{
					case FCChdrl :
						return Traversehdrl(nStart + 12, nStart + tmp[1] + 8);
				}
				break;
		}
		nStart += tmp[1];
		nStart += 8 + (tmp[1] & 1);
	}

	return AVIINFO_NEED_MORE_DATA;
}

static int		GetAVIInfoMain()
{
	DWORD	tmp[3];
	int		nRet;

	if (!avi_Read(tmp, 12))
		return AVIINFO_IOERR;
	if (tmp[0] != FCCRIFF || tmp[2] != FCCAVI)
		return AVIINFO_INVALIDFILE;
	g_nAVStrm = 0;
	g_nMaxAVStrm = *g_pnStrm;

//	nret = TraverseAVI(12, tmp[1] + 8);
	nRet = TraverseAVI(12, g_buffSize);	

	if (!AVIINFO_ISERROR(nRet))
		*g_pnStrm = g_nAVStrm;
	
	return nRet;
}

int	AVIInfo_GetAVIInfo(char* pData, int dataLen, AVIStreamInfo *pInfo, int *pnStrm)
{
	int		nRet;	

	if (pData == NULL || dataLen == 0 || pInfo == NULL || pnStrm == NULL)
	{
		//TPrint("avi info param error.\r\n");
		return AVIINFO_PARAMERR;
	}
	
	g_pStrmInfo = pInfo;
	g_pnStrm = pnStrm;

	avi_Open(pData, dataLen);
	nRet = GetAVIInfoMain();
	avi_Close();
	return nRet;
}

/*
static const char *getVideoCodecString(AIVideoCodec codec)
{
	switch (codec)
	{
		case aivcDivX4:
			return "DivX4";
		case aivcDivX5:
			return "DivX5";
		case aivcXviD:
			return "XviD";
		case aivcMSMPEG4V3:
			return "MSMPEG4V3";
		case aivcWMV9:
			return "WMV9";
		case aivcDivX3LowM:
		case aivcDivX3FastM:
			//return "DivX3";	// by god.. TCC7801은 Divx3를 지원하지 않는다...
		case aivcUnknown:
		default:
			return "Unknown";
	}
}

static const char *getAudioCodecString(AIAudioCodec codec)
{
	switch (codec)
	{
		case aiacPCM:
			return "PCM";
		case aiacMSADPCM:
			return "MSADPCM";
		case aiacMP3:
			return "MP3";
		case aiacWMA1:
			return "WMA1";
		case aiacWMA2:
			return "WMA2";
		case aiacAC3:
			return "AC3";
		case aiacDTS:
			return "DTS";
		case aiacUnknown:
		default:
			return "Unknown";
	}
}

void readAviInfo(TCHAR *pszFileName, char *pszInfo, int size)
{
	int i;
	int nInfo = 10;
	int nRet;
	AVIStreamInfo info[10];
#if defined(_WIN32_WCE)
	FILE* infd;	
#else
	int infd;
#endif
	int buffSize = 32 * 1024;		// 32k
	char *buff;
 
retry :
#if defined(_WIN32_WCE)
	if ((infd = _wfopen(pszFileName, _T("rb"))) == NULL)
		return;
#else
	if ((infd = open(pszFileName, O_RDONLY)) <= 0)
		return;
#endif
	if ((buff = (char*)TMalloc(buffSize)) == NULL)
	{
#if defined(_WIN32_WCE)
		fclose(infd);
#else
		close(infd);
#endif
		return;
	}
#if defined(_WIN32_WCE)
	fread(buff, 1, buffSize, infd);
	fclose(infd);
#else
	read(infd, buff, buffSize);
	close(infd);
#endif

	nRet = AVIInfo_GetAVIInfo(buff, buffSize, info, &nInfo);

	TFree(buff);

	if (nRet == AVIINFO_NEED_MORE_DATA)		// need more data.
	{
		buffSize = buffSize * 2;
		if (buffSize < 1024 * 1024)
			goto retry;
	}

	if(AVIINFO_ISERROR(nRet))
	{
		return;
	}

	for (i = 0; i < nInfo; i++)
	{
		if (info[i].type == aistVideo)
		{
			sprintf(pszInfo, "%s<%s>=%s\n", pszInfo, "Video stream", getVideoCodecString(info[i].avSpecific.videoInfo.codec));	

#if defined (INT_ONLY_VERSION)
            sprintf(pszInfo, "%s%s=%d %s\n", pszInfo, "Duration", (int)(info[i].msDur / 1000), "Sec.");
#else
			sprintf(pszInfo, "%s%s=%d %s\n", pszInfo, "Duration", (int)info[i].secDur, szSecond[nLang]);
#endif
			sprintf(pszInfo, "%s%s=%d x %d\n", pszInfo, "Video size", info[i].avSpecific.videoInfo.nWid, info[i].avSpecific.videoInfo.nHgt);

#if defined (INT_ONLY_VERSION)
			sprintf(pszInfo, "%s%s=%d.%02d fps\n", pszInfo, "Frame rate", (int)(info[i].avSpecific.videoInfo.fps_mulby1000 / 1000), (int)(info[i].avSpecific.videoInfo.fps_mulby1000 / 10) % 100);
//            sprintf(pszInfo, "%s%s=%d %s\n", pszInfo, LoadString(RSTR_AVI_DURATION), (int)(info[i].msDur / 1000), LoadString(RSTR_AVI_SECOND));
#else
			sprintf(pszInfo, "%s%s=%d.%02d fps\n", pszInfo, "Frame rate", (int)(info[i].avSpecific.videoInfo.fps), (int)(info[i].avSpecific.videoInfo.fps * 100) % 100);
//			sprintf(pszInfo, "%s%s=%d %s\n", pszInfo, LoadString(RSTR_AVI_DURATION), (int)info[i].secDur, szSecond[nLang]);
#endif
		}
		else if (info[i].type == aistAudio)
		{
			sprintf(pszInfo, "%s<%s>=%s\n", pszInfo, "Audio stream", getAudioCodecString(info[i].avSpecific.audioInfo.codec));
			sprintf(pszInfo, "%s%s=%dkbps\n", pszInfo, "Bitrate", info[i].avSpecific.audioInfo.nBitRate/1000);
			sprintf(pszInfo, "%s%s=%dHz\n", pszInfo, "SampleRate", info[i].avSpecific.audioInfo.nSampleRate);
			sprintf(pszInfo, "%s%s=%d\n", pszInfo, "Channels", info[i].avSpecific.audioInfo.nChannels);
		}
	}
	return;
}
*/

INT64 GetMpeg4TotalTime(char* filename)
{
	int i;
	int nInfo = 10;
	int nRet;
	AVIStreamInfo info[10];
	FILE* infd;
	int buffSize = 32 * 1024;		// 32k
	char *buff;

retry :
	if ((infd = fopen(filename, _T("rb"))) <= 0)
		return 0;
	if ((buff = (char*)malloc(buffSize)) == NULL)
	{
		fclose(infd);
		return 0;
	}
	fread(buff, 1, buffSize, infd);
	fclose(infd);
	nRet = AVIInfo_GetAVIInfo(buff, buffSize, info, &nInfo);

	free(buff);

	if (nRet == AVIINFO_NEED_MORE_DATA)		// need more data.
	{
		buffSize = buffSize * 2;
		if (buffSize < 1024 * 1024)
			goto retry;
	}

	if (AVIINFO_ISERROR(nRet))
		return nRet;
	
	for (i = 0; i < nInfo; i++)
	{
		if (info[i].type == aistVideo)
		{
#if defined (INT_ONLY_VERSION)
			return (INT64)(info[i].msDur / 1000);
#else
			return (INT64)info[i].secDur;
#endif 
		}
	}

	return 0;
}


/*
int divx_GetCodecInfoFromData(char* pData, int dataLen, struct AviCodecInfo *codec)
{
	int i;
	int nInfo = 10;
	int nRet;
	AVIStreamInfo info[10];

	nRet = AVIInfo_GetAVIInfo(pData, dataLen, info, &nInfo);

	if (nRet == AVIINFO_NEED_MORE_DATA)
		return 0;
	
	if (nRet < 0)
		return -1;

	for (i = 0; i < nInfo; i++)
	{
		if (info[i].type == aistVideo)
		{
			codec->videoCodec.dwCodec = info[i].avSpecific.videoInfo.codecEx;
			codec->videoCodec.dwCodecEx = 0;
			break;
		}
	}

	for (i = 0; i < nInfo; i++)
	{
		if (info[i].type == aistAudio)
		{
			codec->audioCodec.dwCodec		 = info[i].avSpecific.audioInfo.codecEx;
			codec->audioCodec.dwCodecEx		 = 0;
			codec->audioCodec.dwChannelCount = info[i].avSpecific.audioInfo.nChannels;
			codec->audioCodec.dwFreq		 = info[i].avSpecific.audioInfo.nSampleRate;
			codec->audioCodec.dwBitPerSample = (info[i].avSpecific.audioInfo.nBitRate / codec->audioCodec.dwFreq) / codec->audioCodec.dwChannelCount;
		}
	}

	return 1;
}*/

static const char _wmv_header[] = {0x30, 0x26, 0xB2, 0x75, 0x8E, 0x66, 0xCF, 0x11, 0xA6, 0xD9, 0x00, 0xAA, 0x00, 0x62, 0xCE, 0x6C };

int GetDivxInfo(const char* pFilename, struct AviCodecInfo *codec)
{
	int i;
	int nInfo = 10;
	int nRet;
	AVIStreamInfo info[10];
	FILE* infd;
	int buffSize = 32 * 1024;		// 32k
	char *buff;

retry :
	if ((infd = fopen(pFilename, _T("rb"))) == NULL)
		return AVIINFO_IOERR;

	if ((buff = (char*)malloc(buffSize)) == NULL)
	{
		fclose(infd);
		return AVIINFO_IOERR;
	}
	fread(buff, 1, buffSize, infd);
	fclose(infd);

	if(*(unsigned long *)buff != FCCRIFF)
	{
		if(memcmp(buff, _wmv_header, sizeof(_wmv_header)) == 0)
		{
			codec->videoCodec.dwCodec = FCCWMV3;
			free(buff);
			return AVIINFO_OK;
		}//if

		free(buff);
		return AVIINFO_INVALIDFILE;
	}//if

	memset(&info, 0x00, sizeof(info));
	nRet = AVIInfo_GetAVIInfo(buff, buffSize, info, &nInfo);

	free(buff);

	if (nRet == AVIINFO_NEED_MORE_DATA)		// need more data.
	{
		buffSize = buffSize * 2;
		if (buffSize < 1024 * 1024)
			goto retry;
	}

	if (AVIINFO_ISERROR(nRet))
		return nRet;
	for (i = 0; i < nInfo; i++)
	{
		if (info[i].type == aistVideo)
		{
			codec->videoCodec.dwCodec = info[i].avSpecific.videoInfo.codecEx;
			codec->videoCodec.dwCodecEx = 0;
			codec->videoCodec.nWidth = info[i].avSpecific.videoInfo.nWid;
			codec->videoCodec.nHeight = info[i].avSpecific.videoInfo.nHgt;
			break;
		}
	}

	for (i = 0; i < nInfo; i++)
	{
		if (info[i].type == aistAudio)
		{
			codec->audioCodec.dwCodec		= info[i].avSpecific.audioInfo.codecEx;
			codec->audioCodec.dwCodecEx		= 0;
			codec->audioCodec.dwChannelCount	= info[i].avSpecific.audioInfo.nChannels;
			codec->audioCodec.dwFreq		= info[i].avSpecific.audioInfo.nSampleRate;
			codec->audioCodec.dwBitPerSample	= (info[i].avSpecific.audioInfo.nBitRate / codec->audioCodec.dwFreq) / codec->audioCodec.dwChannelCount;
			break;
		}
	}

	return AVIINFO_OK;
}

