#include "StdAfx.h"
#include "BasicInterface.h"

#include <math.h>
#include <atlbase.h>

#include "dshowutil.h"
#include "AviInfo.h"
#include "MediaInfoDLL.h"
#define MediaInfoNameSpace MediaInfoDLL;
using namespace MediaInfoNameSpace;


//IBaseFilter* CBasicInterface::m_pFilter = NULL;				///<랜더 필터 인터페이스

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CBasicInterface::CBasicInterface(CWnd* pWnd)
:	m_pParentWnd(pWnd)
,	m_pGraphBuilder(NULL)
,	m_pFilter(NULL)
,	m_dwRotId(0)
,	m_nRenderType(E_WINDOWLESS)
,	m_bOpen(false)
,	m_hParentWnd(NULL)
,	m_strFilePath("")
,	m_strSamiPath("")
,	m_bIsPlay(false)
,	m_bIsWindowMedia(false)
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CBasicInterface::~CBasicInterface(void)
{
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 재생한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::Play()
{
	__DEBUG_BEGIN__(_NULL);

	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		::AfxMessageBox("Video file not opened");
		return false;
	}//if

	HRESULT hr;
	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		__DEBUG__("Fail to get IMediaControl", _NULL);
		::AfxMessageBox("Fail to get IMediaControl");
		return false;
	}//if

	long lState = GetMediaControlState();
	if(lState == State_Running)
	{
		__DEBUG_END__(_NULL);
		m_bIsPlay = true;
		return true;
	}//if

	//__DEBUG__("Begin MediaControl Run", _NULL);
	JIB(pMediaControl->Run());
	//__DEBUG__("End MediaControl Run", _NULL);

	m_bIsPlay = true;

	__DEBUG_END__(_NULL);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 재생을 일시 중지 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::Pause()
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return false;
	}//if

	HRESULT hr;
	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		__DEBUG__("Fail to get IMediaControl", _NULL);
		return false;
	}//if

	long lState = GetMediaControlState();
	if(lState == State_Paused)
	{
		m_bIsPlay = false;
		return true;
	}//if

	//__DEBUG__("Begin MediaControl Pause", _NULL);
	JIB(pMediaControl->Pause());
	//__DEBUG__("End MediaControl Pause", _NULL);

	m_bIsPlay = false;
	
	return true;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 재생을 중지 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::Stop()
{
	__DEBUG_BEGIN__(_NULL);

	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return false;
	}//if

	HRESULT hr;
	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		__DEBUG__("Fail to get IMediaControl", _NULL);
		return false;
	}//if

	long lState = GetMediaControlState();
	if(lState == State_Stopped)
	{
		m_bIsPlay = false;
		return true;
	}//if

/*
	__DEBUG__("Begin SeekToStart", _NULL);
	SeekToStart();
	__DEBUG__("End SeekToStart", _NULL);
*/
	//__DEBUG__("Begin MediaControl Stop", _NULL);
	//JIB(pMediaControl->StopWhenReady());
	JIB(pMediaControl->Stop());
	//__DEBUG__("End MediaControl Stop", _NULL);

	//__DEBUG__("Begin Waiting Stop", _NULL);
	WaitForState(State_Stopped);
	//__DEBUG__("End Waiting Stop", _NULL);

	//__DEBUG__("Begin SeekToStart", _NULL);
	SeekToStart();
	//__DEBUG__("End SeekToStart", _NULL);

	m_bIsPlay = false;

	__DEBUG_END__(_NULL);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 총 재생시간을 반환한다. \n
/// @return <형: double> \n
///			<double: 동영상 파일의 총 재생시간> \n
/////////////////////////////////////////////////////////////////////////////////
double CBasicInterface::GetTotalPlayTime()
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return false;
	}//if

	REFTIME tm = 0.0f;
	CComQIPtr<IMediaSeeking> pMediaSeeking(m_pGraphBuilder);
	if(pMediaSeeking != NULL)
	{
		HRESULT hr=-1;
		LONGLONG lDuration = 0;
		LIF(pMediaSeeking->GetDuration(&lDuration));
		if(SUCCEEDED(hr))
		{
			tm = double(lDuration) / UNITS;
		}//if
	}//if
	
	return tm;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 현재 재생시간을 반환한다. \n
/// @return <형: double> \n
///			<double: 동영상 파일의 현재 재생시간> \n
/////////////////////////////////////////////////////////////////////////////////
double CBasicInterface::GetCurrentPlayTime()
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return false;
	}//if

	REFTIME tm = 0.0f;
	CComQIPtr<IMediaSeeking> pMediaSeeking(m_pGraphBuilder);
	if(pMediaSeeking != NULL)
	{
		HRESULT hr;
		LONGLONG lPosition;
		LIF(pMediaSeeking->GetPositions(&lPosition, NULL));
		if(SUCCEEDED(hr))
		{
			tm = double(lPosition) / UNITS;
		}//if
	}//if
	
	return tm;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 지정된 위치로 이동한다. \n
/// @param (double) pos : (in) 이동하려는 동영상의 시간
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::SetPosition(double pos)
{
	if(!m_bOpen)
	{
		__DEBUG__("Video file not opened", _NULL);
		return;
	}//if

	CComQIPtr<IMediaSeeking> pMediaSeeking(m_pGraphBuilder);
	if(pMediaSeeking != NULL)
	{
		HRESULT hr;
		LONGLONG llTime = LONGLONG(pos * double(UNITS));

		//FILTER_STATE fs;
		//hr = pMediaControl->GetState(100, (OAFilterState *)&fs);
		LIF(pMediaSeeking->SetPositions(&llTime, AM_SEEKING_AbsolutePositioning, NULL, 0));
		//m_pMediaControl->Pause();

		// This gets new data through to the renderers
		//if(fs == State_Stopped && bFlushData)
		//{
		//    hr = pMediaControl->Pause();
		//    hr = pMediaControl->GetState(INFINITE, (OAFilterState *)&fs);
		//    hr = pMediaControl->Stop();
		//}//if
    }//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 처음 위치로 이동한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::SeekToStart()
{
	__DEBUG_BEGIN__(_NULL);
	SetPosition(0);
	__DEBUG_END__(_NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 닫는다. \n
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::Close()
{
	__DEBUG__("Begin Close", _NULL);
	if(!m_bOpen)
	{
		__DEBUG__("File not opened", _NULL);
		return;
	}//if

	//if(m_bIsPlay)
	//{
		Stop();
	//}//if

	RemoveFromRot();
	ReleaseAllInterfaces();

	//__DEBUG__("Before release graphbuilder", _NULL);
	//SAFE_RELEASE(m_pGraphBuilder);
	//if(m_pGraphBuilder)
	//{
	//	m_pGraphBuilder->Release();
	//}//if
//	/*
	//while (m_pGraphBuilder->Release () != 0);		//Ref count가 0일때까지...
	int nRef;
	do
	{
		nRef = m_pGraphBuilder->Release ();
		__DEBUG__("Graphbuilder ref count", nRef);
	}while(nRef != 0);
	//__DEBUG__("After release graphbuilder", _NULL);
//	*/
	m_bOpen  = false;
	//m_strFilePath = "";
	__DEBUG__("End Close", _NULL);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// let the graph instance be accessible from graphedit \n
/// @param (IUnknown) *pUnkGraph : (in) Grapheditor interface pointer
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::AddToRot(IUnknown *pUnkGraph) 
{
	if(m_dwRotId != 0)
	{
		__DEBUG__("Registerd filtergraph in ROT", (int)m_dwRotId);
	}//if

	if(pUnkGraph == NULL)
	{
		__DEBUG__("Graphbuilder is NULL", _NULL);
		return false;
	}//if

	HRESULT hr;
    IMoniker* pMoniker;
    IRunningObjectTable *pROT;
    JIB(GetRunningObjectTable(0, &pROT));

    WCHAR wsz[256];
    wsprintfW(wsz, L"FilterGraph %08x pid %08x", (DWORD_PTR)pUnkGraph, GetCurrentProcessId());
    //JIB(CreateItemMoniker(L"!", wsz, &pMoniker));
    if( CreateItemMoniker(L"!", wsz, &pMoniker) != S_OK )
	{
		__DEBUG__("Fail to create IMoniker !!!!!!!!!!", _NULL);
		SAFE_RELEASE(pROT);
		return false;
	}

	//if(SUCCEEDED(hr))
	{
       // LIF(pROT->Register(0, pUnkGraph, pMoniker, &m_dwRotId));
		LIF(pROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, pUnkGraph, pMoniker, &m_dwRotId));
		//SAFE_RELEASE(pROT);
		pMoniker->Release();
    }//if
	
    SAFE_RELEASE(pROT);
    return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// remove the graph instance accessibility from graphedit \n
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::RemoveFromRot()
{
	if(m_dwRotId != 0)
	{
		__DEBUG__("Begin RemoveFromRot", NULL);
		IRunningObjectTable *pROT;
		if(SUCCEEDED(GetRunningObjectTable(0, &pROT)))
		{
			pROT->Revoke(m_dwRotId);		//Running Object Table에서 제거함
			SAFE_RELEASE(pROT);				//컴퍼넌트 작업이 끝났을때 하나 감소
			__DEBUG__("Revoke", (UINT)m_dwRotId);
		}//if
		__DEBUG__("End RemoveFromRot", NULL);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 자막 파일의 이름을 구한다. \n
/// @param (CString) strFilename : (in) 동영상파일의 경로
/// @return <형: CString> \n
///			<자막파일의 경로를 반환> \n
/////////////////////////////////////////////////////////////////////////////////
CString CBasicInterface::GetSamiFile(CString strFilename)
{
	int nIndex = strFilename.ReverseFind( '.' );

	strFilename.Delete( nIndex, (strFilename.GetLength() - nIndex) );

	strFilename += _T(".smi");

	return strFilename;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 음량을 설정한다. \n
/// @param (int) nVol : (in) 설정하려는 음량
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::SetVolume(int nVol)
{
	if(m_pGraphBuilder)
	{
		// 볼륨조절
		double lf_cur_sound = ((1 - pow(10.0f, -10)) * ((double)nVol / 100)) + pow(10.0f, -10);// 핵심코드
		lf_cur_sound = 10 * log10(lf_cur_sound) * 100;
		long l_cur_sound = (long)lf_cur_sound;

		CComQIPtr<IBasicAudio> pBasicAudio(m_pGraphBuilder);
		if(pBasicAudio)
		{
			HRESULT hr;
			JIB(pBasicAudio-> put_Volume(l_cur_sound));

			return true;
		}//if
	}//if

	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 해당하는 pin이 연결된 상태인지 구한다. \n
/// @param (IPin*) pPin : (in) 연결상태를 판단하려는 pin
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::IsPinConnected(IPin* pPin)
{
	//CheckPointer(pPin, E_POINTER);

	CComPtr<IPin> pPinTo;
	
	return SUCCEEDED(pPin->ConnectedTo(&pPinTo)) && pPinTo ? true : false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 그래프빌더에서 필터의 이름으로 해당하는 필터인터페이스를 찾는다. \n
/// @param (IGraphBuilder) *pGraphBuilder : (in) 필터를 찾으려는 그래프빌더 인터페이스
/// @param (LPTSTR) szNameToFind : (in) 찾으려는 필터의 이름
/// @return <형: IBaseFilter*> \n
///			<이름으로 검ㅎ색된 필터 인터페이스> \n
/////////////////////////////////////////////////////////////////////////////////
IBaseFilter* CBasicInterface::FindFilterFromName(IGraphBuilder *pGraphBuilder, LPTSTR szNameToFind)
{
    USES_CONVERSION;

    HRESULT hr;
    IEnumFilters *pEnum = NULL;
    IBaseFilter *pFilter = NULL;
    ULONG cFetched;
    BOOL bFound = FALSE;

    // Verify graph builder interface
    if(!pGraphBuilder)
	{
        return NULL;
	}//if

    // Get filter enumerator
    hr = pGraphBuilder->EnumFilters(&pEnum);
    if(FAILED(hr))
	{
        return NULL;
	}//if

    // Enumerate all filters in the graph
    while((pEnum->Next(1, &pFilter, &cFetched) == S_OK) && (!bFound))
    {
        FILTER_INFO FilterInfo;
        TCHAR szName[256];
        
        hr = pFilter->QueryFilterInfo(&FilterInfo);
        if(FAILED(hr))
        {
            SAFE_RELEASE(pFilter);
            SAFE_RELEASE(pEnum);
            return NULL;
        }//if

        // Compare this filter's name with the one we want
        lstrcpy(szName, W2T(FilterInfo.achName));
        if(!lstrcmp(szName, szNameToFind))
        {
            bFound = TRUE;
        }//if

        FilterInfo.pGraph->Release();

        // If we found the right filter, don't release its interface.
        // The caller will use it and release it later.
        if(!bFound)
		{
            SAFE_RELEASE(pFilter);
		}
        else
		{
            break;
		}//if
    }//while
    SAFE_RELEASE(pEnum);

    return (bFound ? pFilter : NULL);
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 미디어 콘트롤이 지정된 상태가 될때 까지 대기하도록 한다. \n
/// @param (FILTER_STATE) p_State : (in) 대기할 상태
/// @param (LONG) lWaitTime : (in) 대기할 시간
/////////////////////////////////////////////////////////////////////////////////
void CBasicInterface::WaitForState(FILTER_STATE p_State, LONG lWaitTime)
{
	// Make sure we have switched to the required state
	LONG lfs ;
	HRESULT hr;
	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl == NULL)
	{
		Sleep(lWaitTime);	//음..미디어 컨트롤이 없다면...
		return;
	}//if

	do
	{
		pMediaControl->GetState(lWaitTime, &lfs);
	} while (p_State != lfs) ;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 미디어 콘트롤의 상태를 얻어온다. \n
/// @return <형: long> \n
///			<미디어 콘트롤의 상태 값> \n
/////////////////////////////////////////////////////////////////////////////////
long CBasicInterface::GetMediaControlState()
{
	LONG lfs = 0;
	HRESULT hr;
	CComQIPtr<IMediaControl> pMediaControl(m_pGraphBuilder);
	if(pMediaControl != NULL)
	{
		pMediaControl->GetState(0, &lfs);
	}//if

	return lfs;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 재생이 끝난 이벤트인지를 반환한다. \n
/// @return <형: bool> \n
///			<true: 동영상 재생이 끝난 이벤트임> \n
///			<false: 다른 이벤트임> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::IsEventPlayEnd()
{
	long evCode, param1, param2;
	bool bRet = false;
	HRESULT hr;
	CComQIPtr<IMediaEventEx> pMediaEventEx(m_pGraphBuilder);
	if(pMediaEventEx != NULL)
	{
		while(hr = pMediaEventEx->GetEvent(&evCode, &param1, &param2, 0), SUCCEEDED(hr))
		{
			hr = pMediaEventEx->FreeEventParams(evCode, param1, param2);
			__DEBUG__("evCode", evCode);
			if(evCode == EC_COMPLETE)
			{
				bRet = true;
				break;
			}//if
		}//while
	}
	else
	{
		__DEBUG__("pMediaEventEx is NULL", _NULL);
	}//if

	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 필터그래프의 이벤트를 처리한다. \n
/// @param (LPARAM) lParam : (in) 이벤트 부가정보
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::HandleGraphNotify(LPARAM lParam)
{
	HRESULT hr;
	long lEvent, lParam1, lParam2;
	long lTimeOut = 0;

	//미디어 이벤트의 인터페이스
	CComQIPtr<IMediaEventEx> pMe(m_pGraphBuilder);
	if(!pMe)
	{
		__DEBUG__("Fail to get IMediaEventEx intetface", _NULL);
		return false;
	}//if

	while(SUCCEEDED(pMe->GetEvent(&lEvent, &lParam1, &lParam2, lTimeOut)))
	{
		__DEBUG__("================================", _NULL);
		__DEBUG__("Filter graph event", lEvent);
		
		switch(lEvent)
		{
		case EC_DISPLAY_CHANGED:
			{
				//m_csLock.Lock();
				__DEBUG__("EC_DISPLAY_CHANGED", _NULL);
				LIF(pMe->RestoreDefaultHandling(EC_DISPLAY_CHANGED));	//중복된 메시지는 기본처리로 넘긴다.

				//모니터 분배기등을 통하여 변경되면 동영상 재생이 멈추게 되므로
				//동영상 재생을 완전히 멈추고 정지된 시점부터 다시 재생하도록 한다.
				Pause();
				double dbCurrent = GetCurrentPlayTime();
				Close();
				if(!Open(m_strFilePath))
				{
					__DEBUG__("Fail to Open", _NULL);
				}//if

				SetPosition(dbCurrent);

				if(!Play())
				{
					__DEBUG__("Fail to Play", _NULL);
				}//if
				SetAspectRatioMode(STRETCHED);
				LIF(pMe->CancelDefaultHandling(EC_DISPLAY_CHANGED));	//처리가 끝나면 다시 기본처리를 직접한다.
				//화면 다시그리기
				//RePaintVideo();
				//m_csLock.Unlock();
			}
			break;

		case EC_COMPLETE:
			{
				__DEBUG__("EC_COMPLETE", _NULL);
				if(m_hParentWnd)
				{
					::PostMessage(m_hParentWnd, WM_DSINTERFACES_COMPLETEPLAY, 0, 0);
				}//if
			}
			break;

		case EC_ACTIVATE:
			{
				__DEBUG__("EC_ACTIVATE", _NULL);
			}
			break;

		case EC_CLOCK_CHANGED:
			{
				__DEBUG__("EC_CLOCK_CHANGED", _NULL);
			}
			break;

		case EC_PAUSED:
			{
				__DEBUG__("EC_PAUSED", _NULL);
				//if(m_nRenderType == E_DUMMY_OVERLAY)
				//{
					//화면 다시그리기
					RePaintVideo();
				//}//if
			}
			break;
		case EC_VMR_RENDERDEVICE_SET:
			{
				__DEBUG__("EC_VMR_RENDERDEVICE_SET", _NULL);
			}
			break;
/*
		case EC_QUALITY_CHANGE:
			{
				__DEBUG__("EC_QUALITY_CHANGE", _NULL);
				Pause();
				Play();
			}
			break;
*/
		}//switch

		__DEBUG__("================================", _NULL);

		//이벤트 메모리 해제
		hr = pMe->FreeEventParams(lEvent, lParam1, lParam2);
	}//while

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우 미디어 파일인지를 판단한다. \n
/// @param (LPCTSTR) lpszFilename : (in) 미디어 파일 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::IsWindowsMediaFile(LPCTSTR lpszFilename)
{
	//먼저 헤더 포켓을 보고 왼도우미디어 타입인지 확인을 하고
	//그 다음에 코덱아이디로 판단을 한다.
	//이유는 그냥 mediainfo를 사용하면, mpeg등의 파일을 판단하는데에
	//시간이 오래 걸리고 정보를 못 뽑아오는 경우가 있어서....
	AviCodecInfo codec;
	GetDivxInfo(lpszFilename, &codec);

	if(codec.videoCodec.dwCodec == FCCWMV3
		/*&& codec.audioCodec.dwCodec == FCCWMV3*/)
	{
		String strVideoCodecID, strAudioCodecID;
		int nAudioCodecID = 0;
		MediaInfo clsMInfo;
		clsMInfo.Open(lpszFilename);

		strVideoCodecID = clsMInfo.Get(Stream_Video, 0, _T("CodecID"), Info_Text, Info_Name);
		strAudioCodecID = clsMInfo.Get(Stream_Audio, 0, _T("CodecID"), Info_Text, Info_Name);
		nAudioCodecID = atoi(strAudioCodecID.c_str());
		__DEBUG__("VideoCodecID", strVideoCodecID.c_str());
		__DEBUG__("Audio Format", nAudioCodecID);
		clsMInfo.Close();
		if(strstr(strVideoCodecID.c_str(), "WMV")	//Video만 기준으로 판단하도록 변경
			/*&& (nAudioCodecID >= 160 && nAudioCodecID <= 163)*/)
		{
			m_bIsWindowMedia = true;
			return true;
		}//if
	}//if	

	m_bIsWindowMedia = false;
	return false;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 윈도우 미디어 파일을 재생하기위한 필터들을 구성한다. \n
/// @param (LPCWSTR) lpszFileName : (in) 미디어 파일 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CBasicInterface::SetWindowsMediaFilters(LPCWSTR lpszFileName)
{
	HRESULT hr;

	CComPtr <IBaseFilter> pSource;
	CComPtr <IFileSourceFilter> pFileSource;
	CComPtr <IPin> pOutputPin;
	CComPtr <IPin> pInputPin;
	CComPtr <IBaseFilter> pAudioRenderer;

	// Load the improved ASF reader filter by CLSID
	JIB(CoCreateInstance(CLSID_WMAsfReader, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (LPVOID *)&pSource));
	
	// Add the ASF reader filter to the graph.  For ASF/WMV/WMA content,
	// this filter is NOT the default and must be added explicitly.
	JIB(m_pGraphBuilder->AddFilter(pSource, L"WM ASF Reader"));

	// Set its source filename
	JIB(pSource->QueryInterface(IID_IFileSourceFilter, (void **) &pFileSource));

	// Attempt to load this file
	JIB(pFileSource->Load(lpszFileName, NULL));

	ULONG count = 0;
	CountTotalFilterPins(pSource, &count);

	for(ULONG i = 0; i < count; i ++)
	{
		pOutputPin = GetOutPin(pSource, i);
		PIN_INFO info;
		pOutputPin->QueryPinInfo(&info);
		info.pFilter->Release();

		if(wcsstr(info.achName, L"Video"))
		{
			//IBaseFilter* Codec = CreateEncodec(_T("WMVideo Decoder DMO"));
			IBaseFilter* Codec = CreateEncodec(_T("ffdshow Video Decoder"));
			if(Codec)
			{
				//m_pGraphBuilder->AddFilter(Codec, L"WMVideo Decoder DMO");
				m_pGraphBuilder->AddFilter(Codec, L"ffdshow Video Decoder");
				Codec->Release();

				pInputPin = GetInPin(Codec, 0);

				JIB(m_pGraphBuilder->Connect(pOutputPin, pInputPin));

				pOutputPin = GetOutPin(Codec, 0);
				pInputPin = GetInPin(m_pFilter, 0);

				JIB(m_pGraphBuilder->Connect(pOutputPin, pInputPin));
			}//if
		}
		else if(wcsstr(info.achName, L"Audio"))
		{
			IBaseFilter* Codec = CreateEncodec(_T("WMAudio Decoder DMO"));
			//IBaseFilter* Codec = CreateEncodec(_T("ffdshow Audio Decoder"));
			if(Codec)
			{
				m_pGraphBuilder->AddFilter(Codec, L"WMAudio Decoder DMO");
				//m_pGraphBuilder->AddFilter(Codec, L"ffdshow Audio Decoder");
				Codec->Release();

				pInputPin = GetInPin(Codec, 0);

				JIB(m_pGraphBuilder->Connect(pOutputPin, pInputPin));

				pOutputPin = GetOutPin(Codec, 0);

				if(NULL == pAudioRenderer)
				{
					JIB(CoCreateInstance(CLSID_DSoundRender, NULL,
						CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void **)&pAudioRenderer))
					
					// The audio renderer was successfully created, so add it to the graph
					JIB(m_pGraphBuilder->AddFilter(pAudioRenderer, L"Audio Renderer"));			
				}//if

				pInputPin = GetInPin(pAudioRenderer, 0);

				JIB(m_pGraphBuilder->Connect(pOutputPin, pInputPin));

			}//if
		}//if
	}//for

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 코덱의 이름으로 필터를 찾아준다 \n
/// @param (CString) inFriendlyName : (in) 고덱의 이름
/// @return <형: IBaseFilter*> \n
///			<NULL: 실패> \n
///			<IBaseFilter: 찾은 필터의 주소값> \n
/////////////////////////////////////////////////////////////////////////////////
IBaseFilter* CBasicInterface::CreateEncodec(CString inFriendlyName)
{
	HRESULT hr = NOERROR;
	ICreateDevEnum * enumHardware =NULL;
	IBaseFilter    * hardwareFilter = NULL;

	LIF(CoCreateInstance(CLSID_SystemDeviceEnum,NULL,CLSCTX_INPROC_SERVER, IID_ICreateDevEnum,(void **)&enumHardware));
	if(FAILED(hr))
	{
		return NULL;
	}//if

	IEnumMoniker * enumMoniker = NULL;

	LIF(enumHardware->CreateClassEnumerator(CLSID_LegacyAmFilterCategory, &enumMoniker,0));
	if(FAILED(hr))
	{
		return NULL;
	}//if

	if(enumMoniker)
	{
		enumMoniker->Reset();

		IMoniker * moniker = NULL;
		char friendlyName[256];
		ZeroMemory(friendlyName,256);

		while(S_OK == enumMoniker->Next(1,&moniker,0))
		{
			if(moniker)
			{
				IPropertyBag * proBag = NULL;
				VARIANT    name;
				VariantInit(&name);

				hr = moniker->BindToStorage(NULL,NULL,IID_IPropertyBag,(void **)&proBag);
				if(SUCCEEDED(hr))
				{
					name.vt = VT_BSTR;
					proBag->Read(L"FriendlyName",&name,NULL);
				}//if

				if(SUCCEEDED(hr))
				{
					WideCharToMultiByte(CP_ACP, 0, name.bstrVal, -1,friendlyName, 256, NULL, NULL);
					CString str = (CString)friendlyName;
					if(inFriendlyName == str)
					{
						moniker->BindToObject(NULL,NULL,IID_IBaseFilter, (void **)&hardwareFilter);
					}//if
				}//if

				if(proBag)
				{
					proBag->Release();
					proBag = NULL;
				}//if

				VariantClear(&name);

				moniker->Release();
			}//if
		}//while

		enumMoniker->Release();
	}//if

	enumHardware->Release();

	return hardwareFilter;
}
