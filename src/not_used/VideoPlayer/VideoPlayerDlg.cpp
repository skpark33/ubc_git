// VideoPlayerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VideoPlayer.h"
#include "VideoPlayerDlg.h"

#include "BasicInterface.h"
#include "OverlayInterface.h"
#include "VMRRender.h"


#define		WM_PLAY_FILE		(WM_USER + 1026)


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CVideoPlayerDlg 대화 상자


DWORD CVideoPlayerDlg::m_dwStartTime = 0;


CVideoPlayerDlg::CVideoPlayerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVideoPlayerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_hMsgRevcWnd = NULL;
	m_hPlayScreenWnd = NULL;

	for(int i=0; i<10; i++)
		m_pInterface[i] = NULL;

	m_nCurrentPlayIndex = -1;
}

void CVideoPlayerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FULLPATH, m_editFullPath);
}

BEGIN_MESSAGE_MAP(CVideoPlayerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CVideoPlayerDlg 메시지 처리기

BOOL CVideoPlayerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	CString str_title;
	//
	for(int i=0; i<10; i++)
	{
		if( m_hPlayScreenWnd && m_strFullPath[i].GetLength() )
		{
			str_title += m_strFullPath[i];
			str_title += ", ";

			m_nCurrentPlayIndex = 0;

			//::AfxMessageBox(m_strFullPath);
			m_editFullPath.SetWindowText(m_strFullPath[i]);

			//m_pInterface = new CVMRRender(this);
			m_pInterface[i] = new COverlayInterface(this);
			m_pInterface[i]->SetParentWindow(/*GetSafeHwnd()*/m_hPlayScreenWnd);
			m_pInterface[i]->SetRenderType(E_WINDOWLESS);
			if( !m_pInterface[i]->Open(m_strFullPath[i]) ) { CString msg; msg.Format("Fail to Open - %s", m_strFullPath[i]); ::AfxMessageBox(msg); }
			if( !m_pInterface[i]->SetAspectRatioMode(STRETCHED) ) { CString msg; msg.Format("Fail to SetAspectRatioMode - %s", m_strFullPath[i]); ::AfxMessageBox(msg); }
			//if( m_pInterface[i] )
			//{
			//	m_pInterface[i]->Play();

			//	DWORD diff = ::GetTickCount() - m_dwStartTime;

			//	::PostMessage(m_hMsgRevcWnd, WM_PLAY_FILE, (WPARAM)diff, 0);

			//	CString title;
			//	title.Format("VideoPlayer - %u ms", diff);
			//	SetWindowText(title);

			//	COPYDATASTRUCT cds;
			//	cds.dwData = 1023;
			//	cds.lpData = (LPVOID)(LPCSTR)m_strFullPath;
			//	cds.cbData = m_strFullPath[i].GetLength()+1;

			//	//::SendMessage(m_hMsgRevcWnd, WM_COPYDATA, 0, (LPARAM)&cds);
			//}

			if( !m_pInterface[i]->ChangeParentWindow(NULL) ) { CString msg; msg.Format("Fail to ChangeParentWindow - %s", m_strFullPath[i]); ::AfxMessageBox(msg); }
		}
	}
	SetTimer(1025, 1, NULL);

	SetWindowText(str_title);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CVideoPlayerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVideoPlayerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVideoPlayerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CVideoPlayerDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent)
	{
	case 1025:
		KillTimer(1025);

		Play();

		SetTimer(1026, 100, NULL);
		break;

	case 1026:
		KillTimer(1026);

		Stop();

		SetTimer(1026, 100, NULL);
		break;
	}

	CDialog::OnTimer(nIDEvent);
}

void CVideoPlayerDlg::Play()
{
	if( m_nCurrentPlayIndex>=0 && m_pInterface[m_nCurrentPlayIndex] )
	{
		m_editFullPath.SetWindowText(m_strFullPath[m_nCurrentPlayIndex]);

		//if( !m_pInterface[m_nCurrentPlayIndex]->ChangeParentWindow(m_hPlayScreenWnd) ) { CString msg; msg.Format("Fail to change parent - %s", m_strFullPath[m_nCurrentPlayIndex]); ::AfxMessageBox(msg); }
		if( !m_pInterface[m_nCurrentPlayIndex]->RePaintVideo() ) { CString msg; msg.Format("Fail to RePaintVideo - %s", m_strFullPath[m_nCurrentPlayIndex]); ::AfxMessageBox(msg); }
		m_pInterface[m_nCurrentPlayIndex]->SeekToStart();
		if( !m_pInterface[m_nCurrentPlayIndex]->Play() ) { CString msg; msg.Format("Fail to play - %s", m_strFullPath[m_nCurrentPlayIndex]); ::AfxMessageBox(msg); }

		DWORD diff = ::GetTickCount() - m_dwStartTime;

		CString title;
		title.Format("VideoPlayer - %u ms", diff);
		//SetWindowText(title);
	}
}

void CVideoPlayerDlg::Stop()
{
	if( m_pInterface[m_nCurrentPlayIndex] )
	{
		double total_play_time = m_pInterface[m_nCurrentPlayIndex]->GetTotalPlayTime();
		double current_play_time = m_pInterface[m_nCurrentPlayIndex]->GetCurrentPlayTime();

		if( current_play_time >= total_play_time )
		{

			m_pInterface[m_nCurrentPlayIndex]->Stop();
			if( !m_pInterface[m_nCurrentPlayIndex]->ChangeParentWindow(m_hPlayScreenWnd) ) { CString msg; msg.Format("Fail to change parent - %s", m_strFullPath[m_nCurrentPlayIndex]); ::AfxMessageBox(msg); }

			m_nCurrentPlayIndex++;
			if( m_pInterface[m_nCurrentPlayIndex]==NULL )
				m_nCurrentPlayIndex = 0;

			DWORD start_tick = ::GetTickCount();

			Play();
		
			DWORD end_tick = ::GetTickCount();

			CString str_title;
			str_title.Format("%d", end_tick - start_tick);
			SetWindowText(str_title);
		}
	}
}
