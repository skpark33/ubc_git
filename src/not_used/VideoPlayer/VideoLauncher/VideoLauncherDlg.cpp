// VideoLauncherDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "VideoLauncher.h"
#include "VideoLauncherDlg.h"


#define		WM_PLAY_FILE		(WM_USER + 1026)


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CVideoLauncherDlg 대화 상자



CVideoLauncherDlg::CVideoLauncherDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVideoLauncherDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVideoLauncherDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FULLPATH, m_editFullPath);
	DDX_Control(pDX, IDC_STATIC_PLAY_FRAME, m_stcPlayFrame);
}

BEGIN_MESSAGE_MAP(CVideoLauncherDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_SIZE()
	ON_MESSAGE(WM_PLAY_FILE, OnPlayFile)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()


// CVideoLauncherDlg 메시지 처리기

BOOL CVideoLauncherDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	CRect client_rect;
	m_stcPlayFrame.GetWindowRect(client_rect);

	ScreenToClient(client_rect);
	m_wnd.Create(NULL, "play frame", WS_VISIBLE | WS_CHILD, client_rect, this, 0xfeff);

	//
	m_repos.SetParent(this);
	//m_repos.AddControl((CWnd*)&m_stcPlayFrame, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl((CWnd*)&m_wnd, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	//
	char str_fullpath[MAX_PATH+1] = {0};
	::GetModuleFileName(NULL, str_fullpath, MAX_PATH);
	int len = strlen(str_fullpath)-1;
	while(str_fullpath[len]!='\\') str_fullpath[len--]=0;

	strcat(str_fullpath, "sample.avi");
	m_editFullPath.SetWindowText(str_fullpath);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CVideoLauncherDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CVideoLauncherDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CVideoLauncherDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CVideoLauncherDlg::OnOK()
{
	CString str_fullpath;
	m_editFullPath.GetWindowText(str_fullpath);

	CString str_cmd;
	//str_cmd.Format("VideoPlayer.exe %d \"%s\"", (int)m_stcPlayFrame.GetSafeHwnd(), str_fullpath);
//	str_cmd.Format("VideoPlayer.exe %d %d \"%s\"", (int)GetSafeHwnd(), (int)m_wnd.GetSafeHwnd(), str_fullpath);
	str_cmd.Format("VideoPlayer.exe %d %d \"C:\\1.avi\" \"C:\\2.avi\"", (int)GetSafeHwnd(), (int)m_wnd.GetSafeHwnd());

	STARTUPINFO             si = {0};
	PROCESS_INFORMATION     pi = {0};

	DWORD dwCreationFlag = NORMAL_PRIORITY_CLASS;

	BOOL bRun = CreateProcess(
		NULL, 
		(LPSTR)(LPCSTR)str_cmd, 
		NULL, 
		NULL, 
		FALSE, 
		dwCreationFlag, 
		NULL, 
		NULL, 
		&si, 
		&pi
		);

	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );

	//CDialog::OnOK();
}

void CVideoLauncherDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if( GetSafeHwnd() )//&& m_wnd.GetSafeHwnd() )
	{
		m_repos.MoveControl();

		CRect client_rect;
		m_wnd.GetClientRect(client_rect);

		CSize size;
		size.cx = client_rect.Width()/32;
		size.cy = client_rect.Height()/18;

		//
		CPoint pts[4];
		CRgn rgnInner, rgnOuter;

		//원래 Frame의 영역
		pts[0].SetPoint(0, 0);
		pts[1].SetPoint(client_rect.Width(), 0);
		pts[2].SetPoint(client_rect.Width(), client_rect.Height());
		pts[3].SetPoint(0, client_rect.Height());

		rgnOuter.CreatePolygonRgn(pts, 4, ALTERNATE);

		//m_listPIPFrame의 수 많큼 루프를 돌면서 영역을 clip한다.
		pts[0].SetPoint(size.cx, size.cy);
		pts[1].SetPoint(client_rect.Width()-size.cx, size.cy);
		pts[2].SetPoint(client_rect.Width()-size.cx, client_rect.Height()-size.cy);
		pts[3].SetPoint(size.cx, client_rect.Height()-size.cy);

		rgnInner.CreatePolygonRgn(pts, 4, ALTERNATE);
		rgnOuter.CombineRgn(&rgnInner, &rgnOuter, RGN_XOR);
		rgnInner.DeleteObject();

		//m_listPIPFrame의 수 많큼 루프를 돌면서 영역을 clip한다.
		pts[0].SetPoint(size.cx*2, size.cy*2);
		pts[1].SetPoint(client_rect.Width()-size.cx*2, size.cy*2);
		pts[2].SetPoint(client_rect.Width()-size.cx*2, client_rect.Height()-size.cy*2);
		pts[3].SetPoint(size.cx*2, client_rect.Height()-size.cy*2);

		rgnInner.CreatePolygonRgn(pts, 4, ALTERNATE);
		rgnOuter.CombineRgn(&rgnInner, &rgnOuter, RGN_XOR);
		rgnInner.DeleteObject();

		//m_stcPlayFrame.SetWindowRgn((HRGN)rgnOuter.GetSafeHandle(), TRUE);
		m_wnd.SetWindowRgn((HRGN)rgnOuter.GetSafeHandle(), TRUE);
	}
}

LRESULT CVideoLauncherDlg::OnPlayFile(WPARAM wParam, LPARAM lParam)
{
	CString title;
	title.Format("VideoLauncher - %d", (int)wParam);

	SetWindowText(title);
	return 0;
}

BOOL CVideoLauncherDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	if( pCopyDataStruct->dwData == 1023)
	{
		CString title;
		title.Format("VideoLauncher - %s", (LPCSTR)(pCopyDataStruct->lpData));

		SetWindowText(title);
	}

	return CDialog::OnCopyData(pWnd, pCopyDataStruct);
}
