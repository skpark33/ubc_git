// StaticEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "VideoLauncher.h"
#include "StaticEx.h"


// CStaticEx

IMPLEMENT_DYNAMIC(CStaticEx, CStatic)

CStaticEx::CStaticEx()
{

}

CStaticEx::~CStaticEx()
{
}


BEGIN_MESSAGE_MAP(CStaticEx, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CStaticEx 메시지 처리기입니다.



void CStaticEx::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CStatic::OnPaint()을(를) 호출하지 마십시오.

	CRect rect;
	GetClientRect(rect);

	dc.FillSolidRect(rect, RGB(255,128,128));
}
