// VideoLauncherDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"

#include "ReposControl2.h"
#include "StaticEx.h"
#include "PlayWnd.h"

// CVideoLauncherDlg 대화 상자
class CVideoLauncherDlg : public CDialog
{
// 생성입니다.
public:
	CVideoLauncherDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIDEOLAUNCHER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	CReposControl2	m_repos;

public:
	CEdit	m_editFullPath;
	//CStatic	m_stcPlayFrame;
	CStaticEx	m_stcPlayFrame;

	CPlayWnd	m_wnd;
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnPlayFile(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
};
