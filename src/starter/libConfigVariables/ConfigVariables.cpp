// ConfigVariables.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ConfigVariables.h"

#include "COP/ciXProperties.h"
//#include "CMN/libCommon/ubcIni.h"
#include "UBC/ubcIni.h"
#include "common/libHttpRequest/HttpRequest.h"

// CConfigVariables

CConfigVariables::CConfigVariables()
:	m_strCustomer ( "" )
,	m_strScope ( "" )
{
}

CConfigVariables::CConfigVariables(LPCSTR lpszCustomer, LPCSTR lpszScope)
:	m_strCustomer ( lpszCustomer )
,	m_strScope ( lpszScope )
{
}

CConfigVariables::~CConfigVariables()
{
	Clear();
}

void CConfigVariables::Clear()
{
	CString key;
	CV_ITEM* pItem;
	for(POSITION pos=m_mapCV.GetStartPosition(); pos != NULL; )
	{
		m_mapCV.GetNextAssoc(pos, key, (void*&)pItem);

		if(pItem)
		{
			delete pItem;
		}
	}
	m_mapCV.RemoveAll();
}

BOOL CConfigVariables::Load()
{
	CString send_msg;
	send_msg.Format("customer=%s&scope=%s", m_strCustomer, m_strScope);

	CHttpRequest http_request;
	http_request.Init(CHttpRequest::CONNECTION_CENTER);

	CStringArray line_list;
	BOOL ret_val = http_request.RequestPost("/UBC_Player/get_config_variables.asp", send_msg, line_list);

	if(ret_val)
	{
		if(line_list.GetCount() > 0 && line_list.GetAt(0) == "Exist")
		{
			CV_ITEM* pNewItem = NULL;

			for(int i=0; i<line_list.GetCount(); i++)
			{
				const CString& line = line_list.GetAt(i);

				int equal_idx = line.Find(_T("="));

				if(equal_idx > 0 && line.GetLength() >= equal_idx+1 )
				{
					int pos = 0;
					CString key = line.Tokenize(_T("="), pos);
					LPCTSTR szValue = (LPCTSTR)line+equal_idx+1;

					if(key == "scope")
					{
						if(pNewItem)
						{
							//ciDEBUG(1,("scope : %s", pNewItem->scope));
							//ciDEBUG(1,("fileName : %s", pNewItem->fileName));
							//ciDEBUG(1,("appName : %s", pNewItem->appName));
							//ciDEBUG(1,("keyName : %s", pNewItem->keyName));
							//ciDEBUG(1,("value : %d", pNewItem->value));
						}

						pNewItem = new CV_ITEM();
						pNewItem->scope = szValue;
					}
					else if(key == "scope"    && pNewItem)	pNewItem->scope    = szValue;
					else if(key == "fileName" && pNewItem)	pNewItem->fileName = szValue;
					else if(key == "appName"  && pNewItem)	pNewItem->appName  = szValue;
					else if(key == "keyName"  && pNewItem)
					{
						pNewItem->keyName = szValue;

						CV_ITEM item = *pNewItem;
						CString key;
						key.Format("%s_%s_%s", item.fileName.MakeLower(), item.appName.MakeLower(), item.keyName.MakeLower() );

						CV_ITEM *pExistItem;
						if( m_mapCV.Lookup(key, (void*&)pExistItem) )
						{
							// already exist
							if(pNewItem->scope == "Terminal")
							{
								// if scope is "Terminal" and exist duplicate key, do not overwrite
								delete pNewItem;
								pNewItem = NULL;
								continue;
							}

							*pExistItem = *pNewItem;
							delete pNewItem;
							pNewItem = pExistItem;
						}
						else
						{
							// new item
							m_mapCV.SetAt(key, pNewItem);
						}
					}
					else if(key == "value" && pNewItem)		pNewItem->value = szValue;
				}
			}

			if(pNewItem)
			{
				//ciDEBUG(1,("scope : %s", pNewItem->scope));
				//ciDEBUG(1,("fileName : %s", pNewItem->fileName));
				//ciDEBUG(1,("appName : %s", pNewItem->appName));
				//ciDEBUG(1,("keyName : %s", pNewItem->keyName));
				//ciDEBUG(1,("value : %d", pNewItem->value));
			}

			return TRUE;
		}
		else if(line_list.GetCount() > 0 && line_list.GetAt(0) == "Nothing")
		{
			// not exist
			return TRUE;
		}
		else
		{
			// unknown error
		}
	}

	return FALSE;
}

CConfigVariables::RETURN_TYPE CConfigVariables::Apply(LPCSTR lpszCustomer, LPCSTR lpszScope)
{
	if(lpszCustomer) m_strCustomer = lpszCustomer;
	if(lpszScope) m_strScope = lpszScope;

	if(m_strCustomer.GetLength()==0 && m_strScope.GetLength()==0) return ERROR_NOT_INIT;

	Clear();

	if( Load() )
	{
		RETURN_TYPE ret_val= SUCCESS_NO_CHANGE;

		CString key;
		CV_ITEM* pItem;
		for(POSITION pos=m_mapCV.GetStartPosition(); pos != NULL; )
		{
			m_mapCV.GetNextAssoc(pos, key, (void*&)pItem);

			CString filename = pItem->fileName;
			filename.MakeLower();

			if(filename.Right(4) == ".ini")
			{
				// ini type
				if(m_strScope == "Server")
				{	// Server
					ubcIni aIni(pItem->fileName.Left(pItem->fileName.GetLength()-4), "CONFIGROOT", "data");

					ciString old_val;
					aIni.get(pItem->appName, pItem->keyName, old_val);
					pItem->old_value = old_val.c_str();

					if(pItem->value != old_val.c_str())
					{
						aIni.set(pItem->appName, pItem->keyName, (LPCSTR)pItem->value);
						ret_val = SUCCESS_EXIST_CHANGE;
					}
				}
				else
				{	// Manager, Studio, Terminal
					ubcConfig aIni(pItem->fileName.Left(pItem->fileName.GetLength()-4));

					ciString old_val;
					aIni.get(pItem->appName, pItem->keyName, old_val);
					pItem->old_value = old_val.c_str();

					if(pItem->value != old_val.c_str())
					{
						aIni.set(pItem->appName, pItem->keyName, (LPCSTR)pItem->value);
						ret_val = SUCCESS_EXIST_CHANGE;
					}
				}
			}
			else if(filename.Right(11) == ".properties")
			{
				// properties type
				ciXProperties* config = ciXProperties::getInstance();

				ciString old_val;
				config->get(pItem->keyName, old_val);
				pItem->old_value = old_val.c_str();

				if(pItem->value != old_val.c_str())
				{
					config->set(pItem->keyName, (LPCSTR)pItem->value);
					config->write();
					ret_val = SUCCESS_EXIST_CHANGE;
				}
			}
		}

		return ret_val;
	}

	return ERROR_LOAD_FROM_SERVER;
}
