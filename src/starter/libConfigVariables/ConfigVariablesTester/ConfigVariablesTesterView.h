// ConfigVariablesTesterView.h : interface of the CConfigVariablesTesterView class
//


#pragma once
#include "afxwin.h"
#include "afxcmn.h"


class CConfigVariablesTesterView : public CFormView
{
protected: // create from serialization only
	CConfigVariablesTesterView();
	DECLARE_DYNCREATE(CConfigVariablesTesterView)

public:
	enum{ IDD = IDD_CONFIGVARIABLESTESTER_FORM };

// Attributes
public:
	CConfigVariablesTesterDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CConfigVariablesTesterView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_editCustomer;
	CEdit m_editScope;
	CListCtrl m_lc;
	afx_msg void OnBnClickedButtonGet();
	afx_msg void OnBnClickedButtonApply();
};

#ifndef _DEBUG  // debug version in ConfigVariablesTesterView.cpp
inline CConfigVariablesTesterDoc* CConfigVariablesTesterView::GetDocument() const
   { return reinterpret_cast<CConfigVariablesTesterDoc*>(m_pDocument); }
#endif

