// ConfigVariablesTester.h : main header file for the ConfigVariablesTester application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CConfigVariablesTesterApp:
// See ConfigVariablesTester.cpp for the implementation of this class
//

class CConfigVariablesTesterApp : public CWinApp
{
public:
	CConfigVariablesTesterApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CConfigVariablesTesterApp theApp;