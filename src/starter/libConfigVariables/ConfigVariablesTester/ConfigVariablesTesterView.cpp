// ConfigVariablesTesterView.cpp : implementation of the CConfigVariablesTesterView class
//

#include "stdafx.h"
#include "ConfigVariablesTester.h"

#include "ConfigVariablesTesterDoc.h"
#include "ConfigVariablesTesterView.h"

#include "../ConfigVariables.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CConfigVariablesTesterView

IMPLEMENT_DYNCREATE(CConfigVariablesTesterView, CFormView)

BEGIN_MESSAGE_MAP(CConfigVariablesTesterView, CFormView)
	ON_BN_CLICKED(IDC_BUTTON_GET, &CConfigVariablesTesterView::OnBnClickedButtonGet)
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CConfigVariablesTesterView::OnBnClickedButtonApply)
END_MESSAGE_MAP()

// CConfigVariablesTesterView construction/destruction

CConfigVariablesTesterView::CConfigVariablesTesterView()
	: CFormView(CConfigVariablesTesterView::IDD)
{
	// TODO: add construction code here

}

CConfigVariablesTesterView::~CConfigVariablesTesterView()
{
}

void CConfigVariablesTesterView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CUSTOMER, m_editCustomer);
	DDX_Control(pDX, IDC_EDIT_SCOPE, m_editScope);
	DDX_Control(pDX, IDC_LIST_CTRL, m_lc);
}

BOOL CConfigVariablesTesterView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CConfigVariablesTesterView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	m_editCustomer.SetWindowText("Default");
	m_editScope.SetWindowText("Server");

	m_lc.InsertColumn(0, "Scope", 0, 100);
	m_lc.InsertColumn(1, "FileName", 0, 150);
	m_lc.InsertColumn(2, "AppName", 0, 100);
	m_lc.InsertColumn(3, "KeyName", 0, 150);
	m_lc.InsertColumn(4, "Old Value", 0, 200);
	m_lc.InsertColumn(5, "Value", 0, 200);
}


// CConfigVariablesTesterView diagnostics

#ifdef _DEBUG
void CConfigVariablesTesterView::AssertValid() const
{
	CFormView::AssertValid();
}

void CConfigVariablesTesterView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CConfigVariablesTesterDoc* CConfigVariablesTesterView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CConfigVariablesTesterDoc)));
	return (CConfigVariablesTesterDoc*)m_pDocument;
}
#endif //_DEBUG


// CConfigVariablesTesterView message handlers

void CConfigVariablesTesterView::OnBnClickedButtonGet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString customer;
	m_editCustomer.GetWindowText(customer);

	CString scope;
	m_editScope.GetWindowText(scope);

	CConfigVariables cv(customer, scope);

	if(cv.Load())
	{
		CMapStringToPtr* cv_map = cv.GetValueMapPtr();
		int idx=0;
		CString key;
		CV_ITEM* pItem;
		for(POSITION pos=cv_map->GetStartPosition(); pos != NULL; idx++)
		{
			cv_map->GetNextAssoc(pos, key, (void*&)pItem);

			m_lc.InsertItem(idx, pItem->scope);
			m_lc.SetItemText(idx, 1, pItem->fileName);
			m_lc.SetItemText(idx, 2, pItem->appName);
			m_lc.SetItemText(idx, 3, pItem->keyName);
			m_lc.SetItemText(idx, 4, pItem->old_value);
			m_lc.SetItemText(idx, 5, pItem->value);
		}
	}
}

void CConfigVariablesTesterView::OnBnClickedButtonApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString customer;
	m_editCustomer.GetWindowText(customer);

	CString scope;
	m_editScope.GetWindowText(scope);

	CConfigVariables cv(customer, scope);

	CConfigVariables::RETURN_TYPE ret_val = cv.Apply();

	if(ret_val > 0)
	{
		CMapStringToPtr* cv_map = cv.GetValueMapPtr();
		int idx=0;
		CString key;
		CV_ITEM* pItem;
		for(POSITION pos=cv_map->GetStartPosition(); pos != NULL; idx++)
		{
			cv_map->GetNextAssoc(pos, key, (void*&)pItem);

			m_lc.InsertItem(idx, pItem->scope);
			m_lc.SetItemText(idx, 1, pItem->fileName);
			m_lc.SetItemText(idx, 2, pItem->appName);
			m_lc.SetItemText(idx, 3, pItem->keyName);
			m_lc.SetItemText(idx, 4, pItem->old_value);
			m_lc.SetItemText(idx, 5, pItem->value);
		}
	}
}
