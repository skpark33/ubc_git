// ConfigVariablesTesterDoc.cpp : implementation of the CConfigVariablesTesterDoc class
//

#include "stdafx.h"
#include "ConfigVariablesTester.h"

#include "ConfigVariablesTesterDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CConfigVariablesTesterDoc

IMPLEMENT_DYNCREATE(CConfigVariablesTesterDoc, CDocument)

BEGIN_MESSAGE_MAP(CConfigVariablesTesterDoc, CDocument)
END_MESSAGE_MAP()


// CConfigVariablesTesterDoc construction/destruction

CConfigVariablesTesterDoc::CConfigVariablesTesterDoc()
{
	// TODO: add one-time construction code here

}

CConfigVariablesTesterDoc::~CConfigVariablesTesterDoc()
{
}

BOOL CConfigVariablesTesterDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CConfigVariablesTesterDoc serialization

void CConfigVariablesTesterDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CConfigVariablesTesterDoc diagnostics

#ifdef _DEBUG
void CConfigVariablesTesterDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CConfigVariablesTesterDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CConfigVariablesTesterDoc commands
