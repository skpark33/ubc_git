// ConfigVariablesTesterDoc.h : interface of the CConfigVariablesTesterDoc class
//


#pragma once


class CConfigVariablesTesterDoc : public CDocument
{
protected: // create from serialization only
	CConfigVariablesTesterDoc();
	DECLARE_DYNCREATE(CConfigVariablesTesterDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CConfigVariablesTesterDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


