#pragma once

#include <afxinet.h>
#include <afxtempl.h>

// CConfigVariables

class CConfigVariables
{
public:
	CConfigVariables();
	CConfigVariables(LPCSTR lpszCustomer, LPCSTR lpszScope);
	virtual ~CConfigVariables();

	enum RETURN_TYPE
	{
		ERROR_LOAD_FROM_SERVER = -2,
		ERROR_NOT_INIT = -1,
		SUCCESS_NO_CHANGE = 1,
		SUCCESS_EXIST_CHANGE,
	};

protected:
	CString		m_strCustomer;
	CString		m_strScope;

	CMapStringToPtr	m_mapCV;

public:
	void		Clear();
	BOOL		Load();
	RETURN_TYPE	Apply(LPCSTR lpszCustomer=NULL, LPCSTR lpszScope=NULL);

	CMapStringToPtr*	GetValueMapPtr() { return &m_mapCV; };
};

typedef struct
{
	CString		scope;
	CString		fileName;
	CString		appName;
	CString		keyName;
	CString		value;
	CString		old_value;
} CV_ITEM;
