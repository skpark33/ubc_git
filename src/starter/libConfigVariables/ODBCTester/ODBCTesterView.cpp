// ODBCTesterView.cpp : CODBCTesterView 클래스의 구현
//

#include "stdafx.h"
#include "ODBCTester.h"

#include "ODBCTesterDoc.h"
#include "ODBCTesterView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CODBCTesterView

IMPLEMENT_DYNCREATE(CODBCTesterView, CView)

BEGIN_MESSAGE_MAP(CODBCTesterView, CView)
END_MESSAGE_MAP()

// CODBCTesterView 생성/소멸

CODBCTesterView::CODBCTesterView()
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CODBCTesterView::~CODBCTesterView()
{
}

BOOL CODBCTesterView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CODBCTesterView 그리기

void CODBCTesterView::OnDraw(CDC* /*pDC*/)
{
	CODBCTesterDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}


// CODBCTesterView 진단

#ifdef _DEBUG
void CODBCTesterView::AssertValid() const
{
	CView::AssertValid();
}

void CODBCTesterView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CODBCTesterDoc* CODBCTesterView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CODBCTesterDoc)));
	return (CODBCTesterDoc*)m_pDocument;
}
#endif //_DEBUG


// CODBCTesterView 메시지 처리기
