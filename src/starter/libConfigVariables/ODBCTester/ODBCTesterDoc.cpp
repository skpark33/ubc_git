// ODBCTesterDoc.cpp : CODBCTesterDoc 클래스의 구현
//

#include "stdafx.h"
#include "ODBCTester.h"

#include "ODBCTesterDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CODBCTesterDoc

IMPLEMENT_DYNCREATE(CODBCTesterDoc, CDocument)

BEGIN_MESSAGE_MAP(CODBCTesterDoc, CDocument)
END_MESSAGE_MAP()


// CODBCTesterDoc 생성/소멸

CODBCTesterDoc::CODBCTesterDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.

}

CODBCTesterDoc::~CODBCTesterDoc()
{
}

BOOL CODBCTesterDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.

	return TRUE;
}




// CODBCTesterDoc serialization

void CODBCTesterDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}


// CODBCTesterDoc 진단

#ifdef _DEBUG
void CODBCTesterDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CODBCTesterDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CODBCTesterDoc 명령
