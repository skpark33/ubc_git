
#include "stdafx.h"

#include <afxmt.h>
#include "Log.h"

FILE*				__log_file__ = NULL;
TCHAR				__log_name__[1024+1] = {0};
CCriticalSection	__log_lock__;

bool __DAILY_LOG_OPEN__(LPCTSTR lpszLogPrefix, int nStoreDays)
{
	if(lpszLogPrefix==NULL || _tcslen(lpszLogPrefix)==0 || nStoreDays>7) return false;

	// new logfile open
	static int last_log_month = -1;
	static int last_log_day = -1;

	//CTime cur_tm = CTime::GetCurrentTime();
	time_t cur_date = time(NULL);
	struct tm *t = localtime(&cur_date);

	int cur_month = t->tm_mon+1;
	int cur_day = t->tm_mday;

	// check date
	if(last_log_month == cur_month && last_log_day == cur_day) return true;

	// divide path<-->filename
	TCHAR prefix[1024] = {0};
	_tcscpy(prefix, lpszLogPrefix);

	LPTSTR prefix_path = NULL;
	LPTSTR prefix_name = NULL;
	if(_tcsstr(prefix, _T("\\")) == NULL)
	{
		// not exist '\'
		prefix_name = prefix;
	}
	else
	{
		prefix_path = prefix;
		for(int i=1023; i>=0; i--)
		{
			if(*(prefix_path+i) == _T('\\'))
			{
				*(prefix_path+i) = 0;
				prefix_name = prefix_path+i+1;
				break;
			}
		}
	}

	// new logfile open
	TCHAR log_path[1024];
	_stprintf(log_path, _T("%s_%02d%02d.log"), lpszLogPrefix, cur_month, cur_day);

	__LOG_CLOSE__();
	__LOG_OPEN__(log_path);
	last_log_month = cur_month;
	last_log_day = cur_day;

	// delete old logfile
	TCHAR leave_log_path[8][1024];
	::ZeroMemory(leave_log_path, sizeof(TCHAR)*8*1024);

	for(int i=0; i<=nStoreDays; i++)
	{
		struct tm *t = localtime(&cur_date);
		_stprintf(leave_log_path[i], _T("%s_%02d%02d.log"), prefix_name, t->tm_mon+1, t->tm_mday);
		cur_date -= (60 * 60 * 24);
	}

	_stprintf(log_path, _T("%s_*.log.*"), lpszLogPrefix);
	WIN32_FIND_DATA ffd;
	HANDLE hFind = FindFirstFile(log_path, &ffd);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		// error
		return false;
	}

	BOOL ret_val = TRUE;
	while(ret_val)
	{
		bool find = false;
		for(int i=0; i<8; i++)
		{
			int len = _tcslen(leave_log_path[i]);
			if( len > 0 && _tcsnicmp(leave_log_path[i], ffd.cFileName, len) == 0)
			{
				find = true;
				break;
			}
		}
		if( find==false )
		{
			TCHAR delete_file[1024] = {0};
			if(prefix_path)
				_stprintf(delete_file, _T("%s\\%s"), prefix_path, ffd.cFileName);
			else
				_stprintf(delete_file, _T("%s"), ffd.cFileName);
			::DeleteFile(delete_file);
		}
		ret_val = FindNextFile(hFind, &ffd);
	}
	FindClose(hFind);

	return true;
}

bool __LOG_OPEN__(LPCTSTR lpszLogFilename)
{
	if(lpszLogFilename==NULL || _tcslen(lpszLogFilename)==0) return false;

	_tcsncpy(__log_name__, lpszLogFilename, 1024);

//	TCHAR drv[8]={0}, dir[1024]={0}, fn[1024]={0}, ext[1024]={0};
//	_tsplitpath(lpszLogFilename, drv, dir, fn, ext);

	// make .bak.bak & .bak
	TCHAR log_bak[1024+1] = {0};
	TCHAR log_bak_bak[1024+1] = {0};

	//_stprintf(log_bak, _T("%s%s%s"), drv, dir, fn);
	//_tcscat(log_bak, _T(".bak.log"));

	//_stprintf(log_bak_bak, _T("%s%s%s"), drv, dir, fn);
	//_tcscat(log_bak_bak, _T(".bak.bak.log"));

	_tcsncpy(log_bak, lpszLogFilename, 1024); _tcscat(log_bak, _T(".bak"));
	_tcsncpy(log_bak_bak, log_bak, 1024); _tcscat(log_bak_bak, _T(".bak"));

	::DeleteFile(log_bak_bak);
	::MoveFile(log_bak, log_bak_bak);
	::MoveFile(lpszLogFilename, log_bak);

	__log_lock__.Lock();

	if(__log_file__) fclose(__log_file__);
	__log_file__ = _tfopen(lpszLogFilename, "w");

	__log_lock__.Unlock();

	return (__log_file__ != NULL);
}

void __LOG_CLOSE__()
{
	if(__log_file__) fclose(__log_file__);
	__log_file__ = NULL;
}

bool __IS_LOG_OPEN__()
{
	return (__log_file__ != NULL);
}

LPCTSTR __GET_LOG_NAME__()
{
	return __log_name__;
}

void __log_header__(LPCTSTR lpszType, LPCSTR lpszFuncName, int nLine)
{
	__log_lock__.Lock();

	if( __log_file__ == NULL ) return;

	_ftprintf(__log_file__, 
				_T("[%s] [%s] %s (line:%d) "), 
				CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S"), 
				lpszType, 
				lpszFuncName, 
				nLine
			);
}

void __log_body__(LPCTSTR pFormat,...)
{
	if( __log_file__ == NULL ) return;

	va_list	aVp;
	va_start(aVp, pFormat);
	_vftprintf(__log_file__, pFormat, aVp);
	va_end(aVp);
}

void __log_tail__()
{
	__log_lock__.Unlock();

	if( __log_file__ == NULL ) return;

	_ftprintf(__log_file__, _T("\n") );
	fflush(__log_file__);
}
