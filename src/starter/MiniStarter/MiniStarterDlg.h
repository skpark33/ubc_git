// MiniStarterDlg.h : 헤더 파일
//

#pragma once
#include "afxcmn.h"

#include "ReposControl2.h"
#include "ProfileManager.h"
#include "SortListCtrl.h"
#include "TrayIcon.h"


// CMiniStarterDlg 대화 상자
class CMiniStarterDlg : public CDialog
{
// 생성입니다.
public:
	CMiniStarterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MINISTARTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.
	virtual BOOL PreTranslateMessage(MSG* pMsg);

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();

	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	CReposControl2	m_repos;
	CTrayIcon		m_TrayIcon;
	CBitmap			m_bmpTrayIcon;
	HICON			m_hTrayIcon;

public:
	//CListCtrl	m_lcProcess;
	CSortListCtrl	m_lcProcess;

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnBnClickedButtonRegister();
	afx_msg void OnBnClickedButtonModify();
	afx_msg void OnBnClickedButtonDelete();
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnBnClickedButtonStatus();
	afx_msg void OnNMDblclkListProcess(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnTrayNotification(WPARAM wParam, LPARAM lParam);
	afx_msg void OnAppExit();

protected:
	PROCESS_LIST	m_ProcessList;
	CProfileManager	m_ProfileManager;

	void	ClearAll();

	void	InitProcessList();
	void	SaveProcessList();

	void	UpdateProcessInfo(PROCESS_INFO* pProcessInfo, int idx=-1, bool bSaveNow=false);
	void	SaveProcessInfo(PROCESS_INFO* pProcessInfo, bool bSaveNow=false);

	DWORD	WatchProcess(PROCESS_INFO* pProcessInfo, int nIdx); // return pid
	DWORD	StartProcess(PROCESS_INFO* pProcessInfo, int nIdx); // return pid
	BOOL	StopProcess(PROCESS_INFO* pProcessInfo, int nIdx); // return pid

	DWORD	CreateProcess(PROCESS_INFO* pProcessInfo); // return pid
	BOOL	TerminateProcess(PROCESS_INFO* pProcessInfo, DWORD dwPID);
};
