#pragma once
#include "afxwin.h"


// CProcessInfoDlg 대화 상자입니다.

class CProcessInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CProcessInfoDlg)

public:
	enum DLG_TYPE { DLG_TYPE_ADD, DLG_TYPE_MODIFY, DLG_TYPE_DETAIL_INFO };

	CProcessInfoDlg(DLG_TYPE eDlgType, PROCESS_INFO* pProcessInfo, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CProcessInfoDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PROCESS_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	DLG_TYPE	m_eDlgType;
	PROCESS_INFO*	m_pProcessInfo;

public:
	CEdit		m_editProcessName;
	CEdit		m_editPath;
	CEdit		m_editBinaryName;
	CButton		m_btnBrowser;
	CEdit		m_editParameter;
	CEdit		m_editPID;
	CComboBox	m_cbxAutoStartup;
	CComboBox	m_cbxRunningType;
	CEdit		m_editStartupTime;
	CEdit		m_editStatus;
	CComboBox	m_cbxDontMonitor; //skpark
	CEdit		m_editKillTime; //skpark

	afx_msg void OnBnClickedButtonBrowser();
};
