#pragma once

#include <afxtempl.h>

class CProfileManager
{
protected:
	CString			m_strFilename;
	CMapStringToPtr	m_mapGroup;

	LPTSTR	GetLine(LPTSTR& tch);
	void	DeleteBracket(CString& str);

public:
	CProfileManager();
	CProfileManager(LPCTSTR lpszFilename);
	virtual ~CProfileManager(void);

	void	SetFileName(CString strFilename) { m_strFilename = strFilename; }
	void	ClearAll();

	BOOL	Read(CString strFilename = _T(""));
	BOOL	Write(CString strFilename = _T(""));

	CString	GetProfileString(CString strTabName, CString strKeyName, CString strDefault = _T(""));
	DWORD	GetProfileString(CString strTabName, CString strKeyName, CString strDefault, CString& strReturnedString);
	int		GetProfileInt(CString lpTabName, CString lpKeyName, int nDefault=0);

	DWORD	WriteProfileString(CString lpTabName, CString lpKeyName, CString strString, BOOL bSaveNow=FALSE);
	DWORD	WriteProfileInt(CString lpTabName, CString lpKeyName, int nValue, BOOL bSaveNow=FALSE);
};

