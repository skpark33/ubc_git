// ProcessInfoDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MiniStarter.h"

#include "ProcessInfoDlg.h"


// CProcessInfoDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CProcessInfoDlg, CDialog)

CProcessInfoDlg::CProcessInfoDlg(DLG_TYPE eDlgType, PROCESS_INFO* pProcessInfo, CWnd* pParent /*=NULL*/)
	: CDialog(CProcessInfoDlg::IDD, pParent)
	, m_eDlgType (eDlgType)
	, m_pProcessInfo (pProcessInfo)
{
}

CProcessInfoDlg::~CProcessInfoDlg()
{
}

void CProcessInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PROCESS_NAME, m_editProcessName);
	DDX_Control(pDX, IDC_EDIT_PATH, m_editPath);
	DDX_Control(pDX, IDC_EDIT_BINARY_NAME, m_editBinaryName);
	DDX_Control(pDX, IDC_BUTTON_BROWSER, m_btnBrowser);
	DDX_Control(pDX, IDC_EDIT_PARAMETER, m_editParameter);
	DDX_Control(pDX, IDC_EDIT_PID, m_editPID);
	DDX_Control(pDX, IDC_COMBO_AUTO_STARTUP, m_cbxAutoStartup);
	DDX_Control(pDX, IDC_COMBO_RUNNING_TYPE, m_cbxRunningType);
	DDX_Control(pDX, IDC_EDIT_STARTUP_TIME, m_editStartupTime);
	DDX_Control(pDX, IDC_EDIT_STATUS, m_editStatus);
	DDX_Control(pDX, IDC_COMBO_DONTMONITOR, m_cbxDontMonitor);  //skpark
	DDX_Control(pDX, IDC_EDIT_KILL_TIME, m_editKillTime); //skpark
}


BEGIN_MESSAGE_MAP(CProcessInfoDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_BROWSER, &CProcessInfoDlg::OnBnClickedButtonBrowser)
END_MESSAGE_MAP()


// CProcessInfoDlg 메시지 처리기입니다.

BOOL CProcessInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//
	m_cbxAutoStartup.AddString("FALSE");
	m_cbxAutoStartup.AddString("TRUE");
	m_cbxAutoStartup.SetCurSel(1);

	m_cbxRunningType.AddString("Hide");
	m_cbxRunningType.AddString("Normal");
	m_cbxRunningType.AddString("Minimize");
	m_cbxRunningType.AddString("Maximize");
	m_cbxRunningType.SetCurSel(1);

	//skpark
	m_cbxDontMonitor.AddString("FALSE");
	m_cbxDontMonitor.AddString("TRUE");
	m_cbxDontMonitor.SetCurSel(0);

	//
	if( m_pProcessInfo )
	{
		m_editProcessName.SetWindowText( m_pProcessInfo->strProcessName );
		m_editPath.SetWindowText( m_pProcessInfo->strPath );
		m_editBinaryName.SetWindowText( m_pProcessInfo->strBinaryName );
		m_editParameter.SetWindowText( m_pProcessInfo->strParameter );
		m_editPID.SetWindowText( ::ToString(m_pProcessInfo->dwPID) );
		m_cbxAutoStartup.SetCurSel( m_pProcessInfo->nAutoStartup );
		m_cbxRunningType.SetCurSel( m_pProcessInfo->nRunningType );
		m_editStartupTime.SetWindowText( m_pProcessInfo->strStartupTime );
		m_editStatus.SetWindowText( ::ToString(m_pProcessInfo->eStatus) );
		m_cbxDontMonitor.SetCurSel( m_pProcessInfo->bDontMonitor ); //skpark
		m_editKillTime.SetWindowText( m_pProcessInfo->strKillTime );  //skpark

	}

	//
	switch(m_eDlgType)
	{
	case DLG_TYPE_ADD:
		m_editPath.SetReadOnly();
		m_editBinaryName.SetReadOnly();
		m_editPID.SetReadOnly();
		m_editStartupTime.SetReadOnly();
		m_editStatus.SetReadOnly();
		break;

	case DLG_TYPE_MODIFY:
		m_editProcessName.SetReadOnly();
		m_editPath.SetReadOnly();
		m_editBinaryName.SetReadOnly();
		m_editPID.SetReadOnly();
		m_editStartupTime.SetReadOnly();
		m_editStatus.SetReadOnly();
		break;

	case DLG_TYPE_DETAIL_INFO:
		m_editProcessName.SetReadOnly();
		m_editPath.SetReadOnly();
		m_editBinaryName.SetReadOnly();
		m_btnBrowser.EnableWindow(FALSE);
		m_editParameter.SetReadOnly();
		m_editPID.SetReadOnly();
		m_cbxAutoStartup.EnableWindow(FALSE);
		m_cbxRunningType.EnableWindow(FALSE);
		m_editStartupTime.SetReadOnly();
		m_editStatus.SetReadOnly();
		m_cbxDontMonitor.EnableWindow(FALSE);  //skpark
		m_editKillTime.SetReadOnly();			//skpark
		break;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CProcessInfoDlg::OnOK()
{
	m_editProcessName.GetWindowText( m_pProcessInfo->strProcessName );
	m_editPath.GetWindowText( m_pProcessInfo->strPath );
	m_editBinaryName.GetWindowText( m_pProcessInfo->strBinaryName );
	m_editParameter.GetWindowText( m_pProcessInfo->strParameter );
	//m_editPID.GetWindowText( ::ToString(m_pProcessInfo->dwPID) );
	m_pProcessInfo->nAutoStartup = m_cbxAutoStartup.GetCurSel();
	m_pProcessInfo->nRunningType = m_cbxRunningType.GetCurSel();
	//m_editStartupTime.SetWindowText( m_pProcessInfo->strStartupTime );
	//m_editStatus.SetWindowText( ::ToString(m_pProcessInfo->eStatus) );

	m_editKillTime.GetWindowText(m_pProcessInfo->strKillTime);  //skpark
	m_pProcessInfo->bDontMonitor = m_cbxDontMonitor.GetCurSel(); //skpark
	CDialog::OnOK();
}

void CProcessInfoDlg::OnBnClickedButtonBrowser()
{
	char sz_filters[] = "Execute Program (*.exe)|*.exe||";

	CFileDialog dlg(TRUE, "exe", "", OFN_FILEMUSTEXIST, sz_filters, this);
	if(dlg.DoModal() == IDOK)
	{
		char drv[MAX_PATH]={0}, dir[MAX_PATH]={0}, fn[MAX_PATH]={0}, ext[MAX_PATH]={0};
		_splitpath(dlg.GetPathName(), drv, dir, fn, ext);

		CString str_path, str_filename;
		str_path.Format("%s%s", drv, dir);
		str_filename.Format("%s%s", fn, ext);

		m_editPath.SetWindowText(str_path);
		m_editBinaryName.SetWindowText(str_filename);
	}
}
