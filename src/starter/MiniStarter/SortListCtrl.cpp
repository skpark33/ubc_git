// CheckListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "SortListCtrl.h"


#define INACTIVE_TEXT_COLOR		RGB(255,255,255)
#define INACTIVE_BACK_COLOR		RGB(127,127,127)

#define ACTIVE_TEXT_COLOR		RGB(255,255,255)
#define ACTIVE_BACK_COLOR		RGB( 96, 96,255)


CSortListCtrl::CSortListCtrl()
:	m_bSortable ( true )
,	m_nSortCol ( 0 )
,	m_bAscend ( false )
{
	m_rgbInactiveTextColor = INACTIVE_TEXT_COLOR;
	m_rgbInactiveBackColor = INACTIVE_BACK_COLOR;

	m_rgbActiveTextColor = ACTIVE_TEXT_COLOR;
	m_rgbActiveBackColor = ACTIVE_BACK_COLOR;
}

CSortListCtrl::~CSortListCtrl()
{
}


BEGIN_MESSAGE_MAP(CSortListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnNMCustomdraw)
	ON_NOTIFY_REFLECT_EX(LVN_COLUMNCLICK, OnLvnColumnclick)
END_MESSAGE_MAP()


void CSortListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
	int idx = static_cast<int>( pLVCD->nmcd.dwItemSpec );
	int count = GetItemCount();

	switch(pLVCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT:
		if( idx < count )
		{
			CString str = GetItemText(idx, 1);

			if( str == "Active" )
			{
				pLVCD->clrText   = m_rgbActiveTextColor;
				pLVCD->clrTextBk = m_rgbActiveBackColor;

				*pResult = CDRF_NOTIFYITEMDRAW;
			}
			else if( str == "Inactive" )
			{
				pLVCD->clrText   = m_rgbInactiveTextColor;
				pLVCD->clrTextBk = m_rgbInactiveBackColor;

				*pResult = CDRF_NOTIFYITEMDRAW;
			}
			else
			{
				*pResult = CDRF_DODEFAULT;
			}
		}
		else
		{
			*pResult = CDRF_DODEFAULT;
		}
		break;

	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		*pResult = CDRF_NEWFONT;
		break;

	default:
		*pResult = CDRF_DODEFAULT;
		break;
	}
}

BOOL CSortListCtrl::OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	int	nSortCol = pNMLV->iSubItem;

	return Sort(nSortCol, (m_nSortCol != nSortCol) ? true : !m_bAscend);
}

int CALLBACK CSortListCtrl::SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam)
{
	SORT_PARAM*	ptrSortParam = (SORT_PARAM *)lSortParam;
	//CListCtrl*	pListCtrl = (CListCtrl *)CWnd::FromHandle(ptrSortParam->hWnd);

	PROCESS_INFO* info1 = (PROCESS_INFO*)lParam1;
	PROCESS_INFO* info2 = (PROCESS_INFO*)lParam2;

	switch( ptrSortParam->nCol )
	{
	default:
	case 0:
		if (ptrSortParam->bAscend == true)
			return info1->strProcessName.CompareNoCase(info2->strProcessName);
		else
			return info2->strProcessName.CompareNoCase(info1->strProcessName);

	case 1:
		if (ptrSortParam->bAscend == true)
			return info1->eStatus > info2->eStatus;
		else
			return info2->eStatus > info1->eStatus;
		break;

	case 2:
		if (ptrSortParam->bAscend == true)
			return info1->strStartupTime.CompareNoCase(info2->strStartupTime);
		else
			return info2->strStartupTime.CompareNoCase(info1->strStartupTime);

		break;

	case 3:
		if (ptrSortParam->bAscend == true)
			return info1->strBinaryName.CompareNoCase(info2->strBinaryName);
		else
			return info2->strBinaryName.CompareNoCase(info1->strBinaryName);

		break;
	}
}

BOOL CSortListCtrl::Sort(int col, bool ascend)
{
	if( !m_bSortable ) return TRUE;

	CHeaderCtrl* header = GetHeaderCtrl();

	// 정렬표시 해제
	HD_ITEM hditem;
	hditem.mask = HDI_FORMAT;
	header->GetItem( m_nSortCol, &hditem );
	hditem.fmt &= ~(HDF_SORTDOWN | HDF_SORTUP);
	header->SetItem( m_nSortCol, &hditem );

	// 정렬한다.
	SORT_PARAM sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = col;
	sort_param.bAscend = ascend;

	// 현재 정렬기준 컬럼과 정렬모드를 저장한다.
	m_nSortCol = col;
	m_bAscend = sort_param.bAscend;

	BOOL ret_val = SortItems(&SortColumn, (LPARAM)&sort_param);

	// 정렬표시 설정
	header->GetItem( m_nSortCol, &hditem );

	hditem.fmt &= ~(HDF_SORTDOWN | HDF_SORTUP);
	hditem.fmt |= (m_bAscend ? HDF_SORTUP : HDF_SORTDOWN );

	header->SetItem( m_nSortCol, &hditem );

	return ret_val;
}
