//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MiniStarter.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MINISTARTER_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDD_PROCESS_INFO                129
#define IDB_TRAY_ICON                   130
#define IDR_TRAY_MENU                   131
#define IDC_LIST_PROCESS                1000
#define IDS_DELETE_FOLLOW_PROCESS       1000
#define IDC_BUTTON_REGISTER             1001
#define IDS_RUNNING_PROCESS_CANT_MODIFY 1001
#define IDC_BUTTON_START                1002
#define IDS_RUNNING_PROCESS_CANT_DELETE 1002
#define IDC_BUTTON_STOP                 1003
#define IDC_BUTTON_STATUS               1004
#define IDC_BUTTON_DELE                 1005
#define IDC_BUTTON_DELETE               1005
#define IDC_EDIT_PROCESS_NAME           1006
#define IDC_EDIT_PATH                   1007
#define IDC_EDIT_BINARY_NAME            1008
#define IDC_EDIT_PARAMETER              1009
#define IDC_EDIT_PID                    1010
#define IDC_EDIT_STARTUP_TIME           1013
#define IDC_EDIT_STATUS                 1014
#define IDC_COMBO_AUTO_STARTUP          1015
#define IDC_COMBO_RUNNING_TYPE          1016
#define IDC_BUTTON_BROWSER              1017
#define IDC_BUTTON_MODIFY               1018
#define IDC_COMBO_DONTMONITOR           1018
#define IDC_EDIT_STARTUP_TIME2          1019
#define IDC_EDIT_KILL_TIME              1019
#define ID__SHOW                        32771
#define ID__EXIT                        32772
#define ID_APP_PROGRAM                  32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
