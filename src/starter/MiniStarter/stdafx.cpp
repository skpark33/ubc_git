// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// MiniStarter.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


LPCTSTR GetINIPath()
{
	static CString	_iniDirectory = _T("");

	if(_iniDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_iniDirectory = str;

		CString app;
		app.LoadString(AFX_IDS_APP_TITLE);

		_iniDirectory.Append(app);
		_iniDirectory.Append(_T(".ini"));
	}

	return _iniDirectory;
}

PROCESS_STATUS_TYPE ToProcessStatus(int nStatus)
{
	switch(nStatus)
	{
	case PROCESS_STATUS_INACTIVE:	return PROCESS_STATUS_INACTIVE;
	case PROCESS_STATUS_ACTIVE:		return PROCESS_STATUS_ACTIVE;
	}
	return PROCESS_STATUS_UNKNOWN;
}

PROCESS_STATUS_TYPE ToProcessStatus(LPCTSTR lpszStatus)
{
	if( stricmp(lpszStatus, "Inactive") == 0 ) return PROCESS_STATUS_INACTIVE;
	if( stricmp(lpszStatus, "Active") == 0 ) return PROCESS_STATUS_ACTIVE;
	return PROCESS_STATUS_UNKNOWN;
}

CString	ToString(PROCESS_STATUS_TYPE eStatus)
{
	switch(eStatus)
	{
	case PROCESS_STATUS_UNKNOWN: return "";
	case PROCESS_STATUS_INACTIVE: return "Inactive";
	case PROCESS_STATUS_ACTIVE: return "Active";
	}
	return "Invalid Value";
}

CTime ToTime(LPCTSTR lpszTime)
{
	if( lpszTime == NULL ) return CTime(1970, 1, 2, 0, 0, 0);

	CString str_tm = lpszTime;
	return ToTime(str_tm);
}

CTime ToTime(CString& strTime)
{
	int pos = 0;

	int ymdhms[6] = {1970, 1, 2, 0, 0, 0};
	for(int i=0; i<6; i++)
	{
		CString data = strTime.Tokenize(" -/:", pos);
		if( data == "" ) break;

		ymdhms[i] = atoi(data);
	}

	return CTime(ymdhms[0], ymdhms[1], ymdhms[2], ymdhms[3], ymdhms[4], ymdhms[5]);
}

CString	ToString(CTime& tmVal)
{
	return tmVal.Format("%Y-%m-%d %H:%M:%S");
}

CString ToString(int nVal)
{
	char buf[64] = {0};
	sprintf(buf, "%d", nVal);
	return buf;
}

CString ToString(DWORD dwVal)
{
	char buf[64] = {0};
	sprintf(buf, "%u", dwVal);
	return buf;
}
