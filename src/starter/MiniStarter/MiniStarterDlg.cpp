// MiniStarterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "MiniStarter.h"
#include "MiniStarterDlg.h"

#include "ProcessInfoDlg.h"

#include <Tlhelp32.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		BUFFER_SIZE		(16*1024)	// 16k

#define		ID_ICON_NOTIFY                  9999

#define		TIMER_CHECK_PROCESS_ID				(1025)
#define		TIMER_CHECK_PROCESS_TIME			(60*1000)		// 1-min

#define		TIMER_CHECK_LOG_FILE_ID				(1026)
#define		TIMER_CHECK_LOG_FILE_TIME			(60*1000)		// 1-min


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
public:
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMiniStarterDlg 대화 상자




CMiniStarterDlg::CMiniStarterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMiniStarterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMiniStarterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_PROCESS, m_lcProcess);
}

BOOL CMiniStarterDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message==WM_SYSKEYDOWN && pMsg->wParam==VK_F4 )
	{
		__DEBUG__(("WM_SYSKEYDOWN + VK_F4"));
		SaveProcessList();
		ClearAll();
		CDialog::OnCancel();
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CMiniStarterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_REGISTER, &CMiniStarterDlg::OnBnClickedButtonRegister)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY, &CMiniStarterDlg::OnBnClickedButtonModify)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &CMiniStarterDlg::OnBnClickedButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_START, &CMiniStarterDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CMiniStarterDlg::OnBnClickedButtonStop)
	ON_BN_CLICKED(IDC_BUTTON_STATUS, &CMiniStarterDlg::OnBnClickedButtonStatus)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_PROCESS, &CMiniStarterDlg::OnNMDblclkListProcess)
	ON_MESSAGE(ID_ICON_NOTIFY, OnTrayNotification)
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
END_MESSAGE_MAP()


// CMiniStarterDlg 메시지 처리기

BOOL CMiniStarterDlg::OnInitDialog()
{
	__FUNC_BEGIN__;

	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	//if(m_bmpTrayIcon.LoadBitmap(IDR_MAINFRAME))
	{
		m_hTrayIcon = ::LoadIcon(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME) );
	}

	CString title;
	title.LoadString(AFX_IDS_APP_TITLE);
	m_TrayIcon.Create(this, ID_ICON_NOTIFY, title, m_hTrayIcon, IDR_TRAY_MENU);

	// init list control
	m_lcProcess.SetExtendedStyle(m_lcProcess.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT );
	m_lcProcess.InsertColumn(0, "Process Name", 0, 160);
	m_lcProcess.InsertColumn(1, "Status", 0, 60);
	m_lcProcess.InsertColumn(2, "Startup Time", 0, 120);
	m_lcProcess.InsertColumn(3, "Binary Name", 0, 160);
	m_lcProcess.InsertColumn(4, "Dont Monitor", 0, 160);  //skpark
	m_lcProcess.InsertColumn(5, "Kill Time", 0, 160);	  //skpark

	m_repos.SetParent(this);
	m_repos.AddControl((CWnd*)&m_lcProcess, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	InitProcessList();

	SetTimer(TIMER_CHECK_PROCESS_ID, 1000, NULL); // check process after 1-sec
	SetTimer(TIMER_CHECK_LOG_FILE_ID, TIMER_CHECK_LOG_FILE_TIME, NULL);

	__FUNC_END__;

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CMiniStarterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CMiniStarterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMiniStarterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMiniStarterDlg::OnOK()
{
	//CDialog::OnOK();
}

void CMiniStarterDlg::OnCancel()
{
	// minimize
	ShowWindow(SW_HIDE);

	//CDialog::OnCancel();
}

void CMiniStarterDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 570;
	lpMMI->ptMinTrackSize.y = 300;

	CDialog::OnGetMinMaxInfo(lpMMI);
}

void CMiniStarterDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if( GetSafeHwnd() && m_lcProcess.GetSafeHwnd() )
	{
		m_repos.MoveControl(TRUE);
	}

	if(nType == SIZE_MINIMIZED)
	{
		ShowWindow(SW_HIDE);
		return;
	}
}

void CMiniStarterDlg::OnTimer(UINT_PTR nIDEvent)
{
	__FUNC_BEGIN__;

	if( nIDEvent == TIMER_CHECK_PROCESS_ID )
	{
		__DEBUG__(("TIMER_CHECK_PROCESS_ID"));

		KillTimer(1025);

		// check process list
		int count = m_lcProcess.GetItemCount();
		for(int i=0; i<count; i++)
		{
			PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
			if( info == NULL ) continue;
			if( info->bStopManually ) continue;
		
			//skpark
			if( !info->strKillTime.IsEmpty() && info->strKillTime.GetLength() >= 5){
				CString strNow = CTime::GetCurrentTime().Format(_T("%H:%M"));
				if(info->strKillTime == strNow){
					__DEBUG__(("%s is kill time", strNow));
					StopProcess(info,i);
					StopProcess(info,i);
					continue;
				}
			}
			if( info->bDontMonitor ) continue;  //skpark

			WatchProcess(info, i);
		}

		SetTimer(TIMER_CHECK_PROCESS_ID, TIMER_CHECK_PROCESS_TIME, NULL); // check process after 1-min
	}
	else if( nIDEvent == TIMER_CHECK_LOG_FILE_ID )
	{
		__DEBUG__(("TIMER_CHECK_LOG_FILE_ID"));
		__DAILY_LOG_OPEN__("log\\MiniStarter");
	}

	CDialog::OnTimer(nIDEvent);

	__FUNC_END__;
}

void CMiniStarterDlg::OnBnClickedButtonRegister()
{
	__FUNC_BEGIN__;

	PROCESS_INFO* info = new PROCESS_INFO;

	CProcessInfoDlg dlg(CProcessInfoDlg::DLG_TYPE_ADD, info);
	if( dlg.DoModal() == IDCANCEL )
	{
		__DEBUG__(("Cancel Register Process"));
		delete info;
		return;
	}

	info->eStatus = PROCESS_STATUS_INACTIVE;
	UpdateProcessInfo(info, -1);
	SaveProcessList();

	__FUNC_END__;
}

void CMiniStarterDlg::OnBnClickedButtonModify()
{
	__FUNC_BEGIN__;

	int modify_count = 0;

	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if( !m_lcProcess.GetItemState(i, LVIS_SELECTED) ) continue;

		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;

		if( info->eStatus == PROCESS_STATUS_ACTIVE )
		{
			CString str_fmt;
			str_fmt.LoadString(IDS_RUNNING_PROCESS_CANT_MODIFY);

			CString msg;
			msg.Format(str_fmt, info->strProcessName);

			__DEBUG__((msg));
			::AfxMessageBox(msg, MB_ICONWARNING);
			continue;
		}

		CProcessInfoDlg dlg(CProcessInfoDlg::DLG_TYPE_MODIFY, info);

		if( dlg.DoModal() == IDCANCEL )
		{
			__DEBUG__(("Cancel Modify Process"));
			break;
		}

		UpdateProcessInfo(info, i);
		modify_count++;
	}

	__DEBUG__(("Modified Count=%d", modify_count));
	if( modify_count>0 )
	{
		m_ProfileManager.Write();
	}

	__FUNC_END__;
}

void CMiniStarterDlg::OnBnClickedButtonDelete()
{
	__FUNC_BEGIN__;

	CString msg;
	msg.LoadString(IDS_DELETE_FOLLOW_PROCESS);
	msg += "\r\n\r\n";

	int del_count = 0;
	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if( !m_lcProcess.GetItemState(i, LVIS_SELECTED) ) continue;

		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;

		if( info->eStatus == PROCESS_STATUS_ACTIVE )
		{
			CString str_fmt;
			str_fmt.LoadString(IDS_RUNNING_PROCESS_CANT_DELETE);

			CString msg;
			msg.Format(str_fmt, info->strProcessName);
			::AfxMessageBox(msg, MB_ICONWARNING);
			return;
		}

		msg += info->strProcessName;
		msg += "\r\n";

		del_count++;
	}

	__DEBUG__(("Delete Count=%d", del_count));
	if( del_count==0 ) return;

	int ret = ::AfxMessageBox(msg, MB_ICONWARNING | MB_YESNO);
	if( ret == IDNO ) return;

	__DEBUG__((msg));

	for(int i=count-1; i>=0; i--)
	{
		if( !m_lcProcess.GetItemState(i, LVIS_SELECTED) ) continue;

		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;

		m_lcProcess.DeleteItem(i);
		delete info;
	}

	SaveProcessList();

	__FUNC_END__;
}

void CMiniStarterDlg::OnBnClickedButtonStart()
{
	__FUNC_BEGIN__;

	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if( !m_lcProcess.GetItemState(i, LVIS_SELECTED) ) continue;

		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;

		if( info->eStatus == PROCESS_STATUS_ACTIVE ) continue;

		info->bStopManually = false;
		StartProcess(info, i);
	}

	__FUNC_END__;
}

void CMiniStarterDlg::OnBnClickedButtonStop()
{
	__FUNC_BEGIN__;

	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if( !m_lcProcess.GetItemState(i, LVIS_SELECTED) ) continue;

		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;

		if( info->eStatus == PROCESS_STATUS_INACTIVE ) continue;

		info->bStopManually = true;
		StopProcess(info, i);
	}

	__FUNC_END__;
}

void CMiniStarterDlg::OnBnClickedButtonStatus()
{
	__FUNC_BEGIN__;

	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if( !m_lcProcess.GetItemState(i, LVIS_SELECTED) ) continue;

		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;

		info->bStopManually = false;
		WatchProcess(info, i);
	}

	__FUNC_END__;
}

void CMiniStarterDlg::OnNMDblclkListProcess(NMHDR *pNMHDR, LRESULT *pResult)
{
	__FUNC_BEGIN__;

	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		if( !m_lcProcess.GetItemState(i, LVIS_SELECTED) ) continue;

		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;

		CProcessInfoDlg dlg(CProcessInfoDlg::DLG_TYPE_DETAIL_INFO, info);
		dlg.DoModal();
		break;
	}

	*pResult = 0;

	__FUNC_END__;
}

void CMiniStarterDlg::ClearAll()
{
	__FUNC_BEGIN__;

	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;
		delete info;
	}
	m_lcProcess.DeleteAllItems();

	__FUNC_END__;
}

void CMiniStarterDlg::InitProcessList()
{
	__FUNC_BEGIN__;

	m_ProfileManager.Read(::GetINIPath());

	CString process_list = m_ProfileManager.GetProfileString("PROCESS", "ALL_LIST", "");
	__DEBUG__(("All Process List=%s", process_list));

	int pos = 0;
	do
	{
		CString str_process_name = process_list.Tokenize("|", pos);
		if( str_process_name == "" ) break;

		// path
		CString str_path = m_ProfileManager.GetProfileString(str_process_name, "Path");
		if( str_path.GetLength() == 0 ) continue;

		__DEBUG__(("Add Process=%s", str_process_name));

		PROCESS_INFO* info = new PROCESS_INFO;
		info->strProcessName = str_process_name;
		info->strPath = str_path;

		// binaryname
		info->strBinaryName = m_ProfileManager.GetProfileString(str_process_name, "BinaryName");

		// parameter
		info->strParameter = m_ProfileManager.GetProfileString(str_process_name, "Parameter");

		// pid
		info->dwPID = (DWORD)m_ProfileManager.GetProfileInt(str_process_name, "PID");

		// auto startup
		info->nAutoStartup = m_ProfileManager.GetProfileInt(str_process_name, "AutoStartup");

		// running type
		info->nRunningType = m_ProfileManager.GetProfileInt(str_process_name, "RunningType");

		// startup time
		info->strStartupTime = m_ProfileManager.GetProfileString(str_process_name, "StartupTime");;

		// status
		info->eStatus = ::ToProcessStatus(m_ProfileManager.GetProfileInt(str_process_name, "Status"));

		// skpark monitor
		info->bDontMonitor = m_ProfileManager.GetProfileInt(str_process_name, "DontMonitor");

		// skpark kill time
		info->strKillTime = m_ProfileManager.GetProfileString(str_process_name, "KillTime");

		//
		m_ProcessList.Add(info);
		UpdateProcessInfo(info);

	} while(1);

	__FUNC_END__;
}

void CMiniStarterDlg::SaveProcessList()
{
	__FUNC_BEGIN__;

	m_ProfileManager.ClearAll();

	CString app_list = "";
	int count = m_lcProcess.GetItemCount();
	for(int i=0; i<count; i++)
	{
		PROCESS_INFO* info = (PROCESS_INFO*)m_lcProcess.GetItemData(i);
		if( info == NULL ) continue;

		__DEBUG__(("Save Process=%s", info->strProcessName));
		if( app_list.GetLength()>0 ) app_list+="|";
		app_list += info->strProcessName;

		SaveProcessInfo(info);
	}

	__DEBUG__(("All Process List=%s", app_list));
	m_ProfileManager.WriteProfileString("PROCESS", "ALL_LIST", app_list, TRUE);

	__FUNC_END__;
}

void CMiniStarterDlg::UpdateProcessInfo(PROCESS_INFO* pProcessInfo, int idx/*=-1*/, bool bSaveNow/*=false*/)
{
	if( pProcessInfo == NULL ) return;

	__DEBUG__(("begin... (%s)", pProcessInfo->strProcessName));

	if(idx < 0)
	{
		idx = m_lcProcess.GetItemCount();
		m_lcProcess.InsertItem(idx, pProcessInfo->strProcessName);
	}
	else
	{
		m_lcProcess.SetItemText(idx, 0, pProcessInfo->strProcessName);
	}

	m_lcProcess.SetItemText(idx, 1, ToString(pProcessInfo->eStatus));
	m_lcProcess.SetItemText(idx, 2, pProcessInfo->strStartupTime);
	m_lcProcess.SetItemText(idx, 3, pProcessInfo->strBinaryName);
	m_lcProcess.SetItemText(idx, 4, ToString(pProcessInfo->bDontMonitor));  //skpark
	m_lcProcess.SetItemText(idx, 5, pProcessInfo->strKillTime); //skpark

	m_lcProcess.SetItemData(idx, (DWORD)pProcessInfo);

	SaveProcessInfo(pProcessInfo);
	if( bSaveNow ) m_ProfileManager.Write();

	__FUNC_END__;
}

void CMiniStarterDlg::SaveProcessInfo(PROCESS_INFO* pProcessInfo, bool bSaveNow/*=false*/)
{
	if( pProcessInfo == NULL ) return;

	__DEBUG__(("begin... (%s)", pProcessInfo->strProcessName));

	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "Path", pProcessInfo->strPath);
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "BinaryName", pProcessInfo->strBinaryName );
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "Parameter", pProcessInfo->strParameter );
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "PID", ::ToString(pProcessInfo->dwPID) );
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "AutoStartup", ::ToString(pProcessInfo->nAutoStartup) );
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "RunningType", ::ToString(pProcessInfo->nRunningType) );
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "StartupTime", pProcessInfo->strStartupTime );
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "Status", ::ToString((int)pProcessInfo->eStatus) );
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "DontMonitor", ::ToString((int)pProcessInfo->bDontMonitor) );
	m_ProfileManager.WriteProfileString(pProcessInfo->strProcessName, "KillTime", pProcessInfo->strKillTime );

	if( bSaveNow ) m_ProfileManager.Write();

	__FUNC_END__;
}

DWORD CMiniStarterDlg::WatchProcess(PROCESS_INFO* pProcessInfo, int nIdx)
{
	if( pProcessInfo == NULL ) return 0;

	__DEBUG__(("begin... (%s)", pProcessInfo->strProcessName));

	CString str_binary_name = pProcessInfo->strBinaryName;

	HANDLE handle_snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if( handle_snapshot == INVALID_HANDLE_VALUE )
	{
		// ERROR !!!
		__ERROR__(("Fail to CreateToolhelp32Snapshot !!!"));
		return -1;
	}

	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	BOOL is_alive = FALSE;

	if( !::Process32First(handle_snapshot, &pe32) )
	{
		// ERROR !!!
		__ERROR__(("Fail to Process32First !!!"));
		::CloseHandle(handle_snapshot);
		return -1;
	}

	do
	{
		if( stricmp(pe32.szExeFile, str_binary_name) == 0 )
		{
			if( pProcessInfo->eStatus != PROCESS_STATUS_ACTIVE || 
				pProcessInfo->dwPID != pe32.th32ProcessID )
			{
				pProcessInfo->eStatus = PROCESS_STATUS_ACTIVE;
				pProcessInfo->dwPID = pe32.th32ProcessID;
				pProcessInfo->strStartupTime = ::ToString(CTime::GetCurrentTime());
			}
			is_alive = true;
			UpdateProcessInfo(pProcessInfo, nIdx, true);
			break;
		}
	}
	while( ::Process32Next(handle_snapshot, &pe32) );

	::CloseHandle(handle_snapshot);

	DWORD ret_pid = 0;
	if( is_alive )
	{
		ret_pid = pProcessInfo->dwPID;
	}
	else
	{
		if( pProcessInfo->eStatus != PROCESS_STATUS_INACTIVE )
		{
			pProcessInfo->eStatus = PROCESS_STATUS_INACTIVE;
			pProcessInfo->dwPID = 0;
		}

		if( pProcessInfo->nAutoStartup && !pProcessInfo->bStopManually )
			return StartProcess(pProcessInfo, nIdx);

		UpdateProcessInfo(pProcessInfo, nIdx, true);
	}

	__FUNC_END__;

	return ret_pid;
}

DWORD CMiniStarterDlg::StartProcess(PROCESS_INFO* pProcessInfo, int nIdx)
{
	if( pProcessInfo == NULL ) return 0;

	__DEBUG__(("begin... (%s)", pProcessInfo->strProcessName));

	DWORD pid = CreateProcess(pProcessInfo);

	if( pid )
	{
		pProcessInfo->dwPID = pid;
		pProcessInfo->eStatus = PROCESS_STATUS_ACTIVE;
		pProcessInfo->strStartupTime = ::ToString(CTime::GetCurrentTime());

		UpdateProcessInfo(pProcessInfo, nIdx, true);
	}

	__FUNC_END__;

	return pid;
}

DWORD getPid(const char* lpszBinaryName)
{
	HANDLE handle_snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if( handle_snapshot == INVALID_HANDLE_VALUE )
	{
		// ERROR !!!
		__ERROR__(("Fail to CreateToolhelp32Snapshot !!!"));
		return -1;  //skpark
	}

	PROCESSENTRY32 pe32 = {0};
	pe32.dwSize = sizeof(PROCESSENTRY32);

	if( !::Process32First(handle_snapshot, &pe32) )
	{
		// ERROR !!!
		__ERROR__(("Fail to Process32First !!!"));
		::CloseHandle(handle_snapshot);
		return -1; //skpark
	}

	do
	{
		if( stricmp(pe32.szExeFile, lpszBinaryName) == 0 )
		{
			// find
			::CloseHandle(handle_snapshot);
			return pe32.th32ProcessID;
		}
	} while( ::Process32Next(handle_snapshot, &pe32) );

	::CloseHandle(handle_snapshot);
	return -1;  //skpark
}

BOOL CMiniStarterDlg::StopProcess(PROCESS_INFO* pProcessInfo, int nIdx)
{
	if( pProcessInfo == NULL ) return FALSE;

	__DEBUG__(("begin... (%s)", pProcessInfo->strProcessName));

	DWORD pid = getPid(pProcessInfo->strBinaryName);
	//skpark
	if(pid < 0){
		__DEBUG__(("getPid(%s) failed", pProcessInfo->strProcessName));
		return FALSE;
	}

	BOOL result = TerminateProcess(pProcessInfo, pid);

	if( result == 1 || result == -1 )
	{
		pProcessInfo->dwPID = 0;
		pProcessInfo->eStatus = PROCESS_STATUS_INACTIVE;

		UpdateProcessInfo(pProcessInfo, nIdx, true);

		if (result == -1)
		{
			CString msg;
			msg.Format("[ %s ] 프로세스를 찾을 수 없습니다.", pProcessInfo->strProcessName);
			//AfxMessageBox(msg, MB_ICONSTOP);
		}
	}

	__FUNC_END__;

	return result;
}

DWORD CMiniStarterDlg::CreateProcess(PROCESS_INFO* pProcessInfo)
{
	if( pProcessInfo == NULL ) return 0;

	__DEBUG__(("begin... (%s)", pProcessInfo->strProcessName));

	PROCESS_INFORMATION	pi = {0};
	STARTUPINFO			si = {0};
	si.dwFlags = STARTF_USESHOWWINDOW;

	DWORD dwCreationFlag = NORMAL_PRIORITY_CLASS;

	switch( pProcessInfo->nRunningType )
	{
	case SW_HIDE:
		dwCreationFlag = DETACHED_PROCESS;
	case SW_SHOWNORMAL:
	case SW_SHOWMINIMIZED:
	case SW_SHOWMAXIMIZED:
		si.wShowWindow = pProcessInfo->nRunningType;
		break;
	}

	CString str_command = "\"";
	str_command += pProcessInfo->strPath;
	if( str_command.GetAt(str_command.GetLength()-1) != '\\' ) str_command += "\\";
	str_command += pProcessInfo->strBinaryName;
	str_command += "\" ";
	str_command += pProcessInfo->strParameter;

	BOOL bRun = ::CreateProcess(
			NULL, 
			(LPSTR)(LPCSTR)str_command, 
			NULL, 
			NULL, 
			FALSE, 
			dwCreationFlag, 
			NULL, 
			NULL, 
			&si, 
			&pi
		);

	::CloseHandle(pi.hProcess);
	::CloseHandle(pi.hThread);

	if( bRun )
	{
		__FUNC_END__;
		return pi.dwProcessId;
	}

	__ERROR__(("Fail to CreateProcess !!!"));

	return 0;
}

BOOL CMiniStarterDlg::TerminateProcess(PROCESS_INFO* pProcessInfo, DWORD dwPID)
{
	if( pProcessInfo == NULL ) return FALSE;

	__DEBUG__(("begin... (%s)", pProcessInfo->strProcessName));

	HANDLE handle_snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if( handle_snapshot == INVALID_HANDLE_VALUE )
	{
		// ERROR !!!
		__ERROR__(("Fail to CreateToolhelp32Snapshot !!!"));
		return 0;
	}

	PROCESSENTRY32 pe32 = {0};
	pe32.dwSize = sizeof(PROCESSENTRY32);

	if( !::Process32First(handle_snapshot, &pe32) )
	{
		// ERROR !!!
		__ERROR__(("Fail to Process32First !!!"));
		::CloseHandle(handle_snapshot);
		return 0;
	}

	do
	{
		// pid 값이 0이면 모든 command 프로세스 종료한다.
		// pid 값이 0이 아니면 pid 값을 갖는 command 프로세스만 종료
		if( stricmp(pe32.szExeFile, pProcessInfo->strBinaryName)==0 && (pe32.th32ProcessID == dwPID || dwPID == 0) )
		{
			HANDLE handle_process = ::OpenProcess(PROCESS_ALL_ACCESS, 0, pe32.th32ProcessID);
			if( handle_process )
			{
				DWORD dwExitCode;
				::GetExitCodeProcess( handle_process, &dwExitCode );
				::TerminateProcess( handle_process, dwExitCode );
				::CloseHandle(handle_process);

				if( dwPID )
				{
					__FUNC_END__;
					::CloseHandle(handle_snapshot);
					return 1; // pid 값이 0이 아니면 리턴
				}
			}
		}
	} while( ::Process32Next(handle_snapshot, &pe32) );

	__ERROR__(("Can't find process !!!"));
	::CloseHandle(handle_snapshot);
	return -1;
}

LRESULT CMiniStarterDlg::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
	if(m_TrayIcon.Enabled())
        return m_TrayIcon.OnTrayNotification(wParam, lParam);

	return 0;
}

void CMiniStarterDlg::OnAppExit()
{
	SaveProcessList();
	ClearAll();

	CDialog::OnOK();
}
