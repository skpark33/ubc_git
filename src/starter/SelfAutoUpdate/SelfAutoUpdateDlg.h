// SelfAutoUpdateDlg.h : header file
//

#if !defined(AFX_SELFAUTOUPDATEDLG_H__5A6B4A3F_6177_4806_89F3_8005C912A383__INCLUDED_)
#define AFX_SELFAUTOUPDATEDLG_H__5A6B4A3F_6177_4806_89F3_8005C912A383__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Tlhelp32.h"
#include <cstdio>
#include <string>
#include <list>
#include <map>
#include <set>
#include <fstream>
#include <vector>

using namespace std;

#ifdef _COP_UTV_
#	define AUTOUPDATE_EXE        "UTV_starter.exe"
#	define AUTOUPDATE_EXE_TMP    "UTV_starter.ex_"
#else
#	define AUTOUPDATE_EXE        "AutoUpdate.exe"
#	define AUTOUPDATE_EXE_TMP    "AutoUpdate.ex_"
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelfAutoUpdateDlg dialog

class CSelfAutoUpdateDlg : public CDialog
{
// Construction
public:
	BOOL IsAliveProcess(CString processName, BOOL kill);
	CSelfAutoUpdateDlg(CWnd* pParent = NULL);	// standard constructor

	inline VOID KillProcess(DWORD dwProcessId);
	BOOL GetCurDir(CString& outDir);
	BOOL ExistUpdateFile(CString& filePath);
// Dialog Data
	//{{AFX_DATA(CSelfAutoUpdateDlg)
	enum { IDD = IDD_SELFAUTOUPDATE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelfAutoUpdateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	ofstream	_log;
	BOOL	_hasLog;

	BOOL RunProcess(CString& path, CString& binName, CString& argument, BOOL waitOption);
	void myLogOpen(const char *filename) ;
	void myLogClose() ;
	void myDEBUG(const char *pFormat,...); 

	// Generated message map functions
	//{{AFX_MSG(CSelfAutoUpdateDlg)
	//virtual void OnTimer(UINT nIDEvent) ;
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELFAUTOUPDATEDLG_H__5A6B4A3F_6177_4806_89F3_8005C912A383__INCLUDED_)
