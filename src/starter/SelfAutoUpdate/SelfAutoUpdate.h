// SelfAutoUpdate.h : main header file for the SELFAUTOUPDATE application
//

#if !defined(AFX_SELFAUTOUPDATE_H__4D5BAB6B_C614_4EC0_BC12_D43FC3E06CE5__INCLUDED_)
#define AFX_SELFAUTOUPDATE_H__4D5BAB6B_C614_4EC0_BC12_D43FC3E06CE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSelfAutoUpdateApp:
// See SelfAutoUpdate.cpp for the implementation of this class
//

class CSelfAutoUpdateApp : public CWinApp
{
public:
	CSelfAutoUpdateApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelfAutoUpdateApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSelfAutoUpdateApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELFAUTOUPDATE_H__4D5BAB6B_C614_4EC0_BC12_D43FC3E06CE5__INCLUDED_)
