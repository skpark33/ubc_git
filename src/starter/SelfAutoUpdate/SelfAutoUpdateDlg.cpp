// SelfAutoUpdateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelfAutoUpdate.h"
#include "SelfAutoUpdateDlg.h"
#include "MsgBox.h"
#include <iostream>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

UINT   nSelfautoMsg;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelfAutoUpdateDlg dialog

CSelfAutoUpdateDlg::CSelfAutoUpdateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelfAutoUpdateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelfAutoUpdateDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSelfAutoUpdateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelfAutoUpdateDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSelfAutoUpdateDlg, CDialog)
	//{{AFX_MSG_MAP(CSelfAutoUpdateDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelfAutoUpdateDlg message handlers

BOOL CSelfAutoUpdateDlg::GetCurDir(CString& strDir)
{
	//CString strDir;
	char	szBuf[_MAX_PATH] = { 0x00 };

	::GetModuleFileName(NULL, szBuf, _MAX_PATH);
	strDir.Format("%s", szBuf);
	
	strDir.MakeReverse();
	int nIndex = strDir.Find("\\");
	if (nIndex < 0) return false; //return "";

	if (strDir.GetLength() < (nIndex+1)) return false; //return "";

	strDir = strDir.Mid(nIndex + 1);
	strDir.MakeReverse();

	//return strDir;
	return true;
}

BOOL CSelfAutoUpdateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	myLogOpen("SelfAutoUpdate.log");

	//printf( "CSelfAutoUpdateDlg::OnInitDialog()\n");

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 2\n");

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// skpark add
	// sleep to wait for physical network connection establishing just after booting
	for(int i=1; i<__argc; ++i) {
		CString strArg = __argv[i];
		if (strArg.Compare("+init_sleep") == 0) {
			CString secArg = __argv[i+1];
			int sec = atoi(secArg);
			if(sec==0) {
				Sleep(10*1000);
			}else{
				Sleep(sec*1000);
			}
			break;
		}
	}	
	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 3\n");

	// skpark add
	// don't start UTV_Starter if "+update_only" option were set"
	/*
	BOOL update_only = false;
	for(int i=1; i<__argc; ++i) {
		CString strArg = __argv[i];
		if (strArg.Compare("+update_only") == 0) {
			CMsgBox obj(this);
			obj.MessageBox("Check Update", 
				"Update Check Message", 
				2000, 
				MB_ICONINFORMATION);
			update_only = true;
			break;
		}
	}	
	*/

	CString path;
	if(!GetCurDir(path)) {
		AfxMessageBox(IDS_CANT_UPDATE_AUTOUPDATE);
		return false;
	}
	CString autoupdateExe = path;
	autoupdateExe += "\\";
	autoupdateExe += AUTOUPDATE_EXE;
	//IsAliveProcess(autoupdateExe);

	CString autoupdatetmpExe = path;
	autoupdatetmpExe += "\\";
	autoupdatetmpExe += AUTOUPDATE_EXE_TMP;

	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 4\n");


	if (ExistUpdateFile(autoupdatetmpExe)) {
		//skpark comment out 2008.07.28
		//nSelfautoMsg = RegisterWindowMessage("SELFAUTO_MSG");
		//::SendMessage(HWND_BROADCAST, nSelfautoMsg, 0, 0);
		//Sleep(1000);

		// AUTOUPDATE_EXE_TMP 파일이 존재하더라도 AUTOUPDATE_EXE 파일보다 
		// 최신 파일인지 여부를 확인하여 최신 파일의 경우에만 업데이트를 진행한다.
		// 2009.09.04
		BOOL selfUpdateFlag = FALSE;

		WIN32_FIND_DATA FindFileDataOrg;
		HANDLE hFindOrg;
		FILETIME orgTime; orgTime.dwHighDateTime = 0; orgTime.dwLowDateTime = 0;
		hFindOrg = FindFirstFileEx(autoupdateExe.GetBuffer(), FindExInfoStandard, &FindFileDataOrg, FindExSearchNameMatch, NULL, 0);
		if (hFindOrg != INVALID_HANDLE_VALUE)
		{
			orgTime = FindFileDataOrg.ftLastWriteTime;
			FindClose(hFindOrg);
		}

		if (orgTime.dwHighDateTime == 0) {
			// UTV_STARTER.EXE 파일이 아직 없는, 최초 업데이트 상태임
			selfUpdateFlag = TRUE;
		} else {
			WIN32_FIND_DATA FindFileDataTmp;
			HANDLE hFileTmp;
			FILETIME tmpTime; tmpTime.dwHighDateTime = 0; tmpTime.dwLowDateTime = 0;
			hFileTmp = FindFirstFileEx(autoupdatetmpExe.GetBuffer(), FindExInfoStandard, &FindFileDataTmp, FindExSearchNameMatch, NULL, 0);
			if (hFileTmp != INVALID_HANDLE_VALUE)
			{
				tmpTime = FindFileDataTmp.ftLastWriteTime;
				FindClose(hFileTmp);
			}

			if (tmpTime.dwHighDateTime > orgTime.dwHighDateTime 
				|| (tmpTime.dwHighDateTime == orgTime.dwHighDateTime 
					&& tmpTime.dwLowDateTime > orgTime.dwLowDateTime)
				) 
			{
				selfUpdateFlag = TRUE;
			}/* else {
				CMsgBox obj(this);
				obj.MessageBox("UTV_Starter.ex_ is older then UTV_Starter.exe", 
					"SelfAutoUpdate Message", 
					2000, 
					MB_ICONINFORMATION);
			}*/
		}
	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 5\n");

		if (selfUpdateFlag) {
			//--012. 파일명 바꾸기
			TRY
			{
				CMsgBox obj(this);
				obj.MessageBox("UTV_Starter patching....", 
					"SelfAutoUpdate Message", 
					2000, 
					MB_ICONINFORMATION);

				//  skpark 2010.07.07 만약 스타터가 살아있다면 먼저 죽여야 한다.  
				// 스타터가 스스로 죽으므로 여기서 죽을때까지 좀 슬립해준다. 최대 5초
				//for(int i=0;i<10;i++){
				//	if(this->IsAliveProcess(autoupdateExe.GetBuffer(),FALSE)){
						Sleep(5000);
				//	}else{
				//		break;
				//	}
				//}

				if (ExistUpdateFile(autoupdateExe)) {
					CFile::Remove(autoupdateExe);
				}

				CFile::Rename(autoupdatetmpExe, autoupdateExe);
			}
			CATCH(CFileException, e)
			{
				//AfxMessageBox(IDS_CANT_UPDATE_AUTOUPDATE);
				CMsgBox obj(this);
				obj.MessageBox("Can not update UTV_starter.exe.",
					"SelfAutoUpdate Message", 
					5000, 
					MB_ICONINFORMATION);

			}
			END_CATCH
		}
	}

	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 6\n");

	
	//--012. 다시 AoutUpdate를 실행시킨다.
	Sleep(500);
	/*
	CString strProcess;
	strProcess.Format("%s", AUTOUPDATE_EXE);
	for(int i=1; i<__argc; ++i) {
		CString strArg = __argv[i];
		if (strArg.Compare("+byAutoUpdate") == 0) {
			CMsgBox obj(this);
			obj.MessageBox("UTV_Starter was patched and rerun.", 
				"SelfAutoUpdate Message", 
				2000, 
				MB_OK | MB_ICONINFORMATION);
			//Sleep(2000);
			continue;
		}
		// ignore this option skpark
		if (strArg.Compare("+init_sleep") == 0) {
			i++;
			continue;
		}

		strProcess += " ";
		strProcess += strArg;
	}	
	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 7\n");
	*/

	BOOL update_only = FALSE;
	BOOL noupdate = FALSE;

	// 업데이터와 스타터를 분리하여, 업데이터를 띠우고, 스타터를 띠운다.
	CString strProcess;
	for(int i=1; i<__argc; ++i) {
		CString strArg = __argv[i];
		if (strArg.Compare("+byAutoUpdate") == 0) {
			CMsgBox obj(this);
			obj.MessageBox("UTV_Starter was patched and rerun.", 
				"SelfAutoUpdate Message", 
				2000, 
				MB_OK | MB_ICONINFORMATION);
			//Sleep(2000);
			continue;
		}
		// ignore this option skpark
		if (strArg.Compare("+init_sleep") == 0) {
			i++;
			continue;
		}
		if (strArg.Compare("+noupdate") == 0) {
			myDEBUG("noupdate version");	
			noupdate = TRUE;
		}
		if (strArg.Compare("+update_only") == 0) {
			myDEBUG("update_only version");	
			update_only = TRUE;
		}

		strProcess += " ";
		strProcess += strArg;
	}	
	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 7\n");

	CString starter = AUTOUPDATE_EXE;

	CString updater_arg = strProcess;
	updater_arg += " +update_only";

	CString starter_arg = strProcess;
	starter_arg += " +noupdate";

	if(!noupdate){
		// This is updater
		RunProcess(path,starter,updater_arg,TRUE);
		::Sleep(2000);
		/*
		// 아직도 살아있다면 죽여야 한다.
		if(IsAliveProcess(AUTOUPDATE_EXE, TRUE)){
			::Sleep(1000);
		}
		*/
	}
	if(!update_only){
		// This is starter

		RunProcess(path,starter,starter_arg,FALSE);
	}

	//::WinExec(strProcess.GetBuffer(), SW_SHOW);
	//::WinExec(AUTOUPDATE_EXE, SW_SHOW);
	
	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 8\n");

	//EnableWindow(FALSE);

	//printf( "CSelfAutoUpdateDlg::OnInitDialog() 9\n");


	//exit(0);
	//SetTimer(100, 100, NULL);
	EndDialog(IDOK);
	myLogClose();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

BOOL
CSelfAutoUpdateDlg::RunProcess(CString& path, CString& binName, CString& argument, BOOL waitOption)
{
	myDEBUG("RunProcess %s,%s,%s,%d", path,binName,argument,waitOption);

	STARTUPINFO si = {0};
	si.cb = sizeof(STARTUPINFO);
	si.wShowWindow = SW_HIDE;
	//si.dwFlags = STARTF_USESHOWWINDOW;	// 콘솔창이 보이지 않도록 한다

	PROCESS_INFORMATION pi;

	if(!CreateProcess((LPSTR)(LPCTSTR)binName
					, (LPSTR)(LPCTSTR)argument 
					, NULL
					, NULL
					, FALSE
					, 0
					, NULL
					, (LPSTR)(LPCTSTR)path
					, &si
					, &pi
					) )
	{
		DWORD dwErrNo = GetLastError();

		LPVOID lpMsgBuf = NULL;
		FormatMessage(  FORMAT_MESSAGE_ALLOCATE_BUFFER
					  | FORMAT_MESSAGE_IGNORE_INSERTS 
					  | FORMAT_MESSAGE_FROM_SYSTEM     
					  ,	NULL
					  , dwErrNo
					  , 0
					  , (LPTSTR)&lpMsgBuf
					  , 0
					  , NULL );

		CString strErrMsg;
		if(!lpMsgBuf)
		{
			strErrMsg.Format(_T("Unknow Error - CreateProcess [%d]"), dwErrNo);
		}
		else
		{
			strErrMsg.Format(_T("%s [%d]"), (LPCTSTR)lpMsgBuf, dwErrNo);
		}
		LocalFree( lpMsgBuf );

		myDEBUG(strErrMsg);
		AfxMessageBox(strErrMsg);
		return FALSE;
	}

	if(waitOption){
		myDEBUG("Process End Wait");
		if(WaitForSingleObject(pi.hProcess,INFINITE)==WAIT_TIMEOUT){
			// wait timeout		
			myDEBUG("WaitForSingleObject wait timeout");
		}
		// process end
	}
	myDEBUG("Process End");
	return TRUE;
}


/*
void CSelfAutoUpdateDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent==100){
		PostMessage(WM_CLOSE);
		KillTimer(100);
	}
	CWnd::OnTimer(nIDEvent);
}
*/

void CSelfAutoUpdateDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSelfAutoUpdateDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSelfAutoUpdateDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

inline VOID CSelfAutoUpdateDlg::KillProcess(DWORD dwProcessId)
{
	HANDLE hProcessGUI = ::OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId);
	if( hProcessGUI ) {
		::TerminateProcess(hProcessGUI, 0);
		::CloseHandle(hProcessGUI);
	}
}

BOOL CSelfAutoUpdateDlg::ExistUpdateFile(CString& filePath)
{
	TRY
	{
		CFileStatus status;
		if (CFile::GetStatus(filePath.GetBuffer(), status)) {
			return true;
		}
	}
	CATCH(CFileException, e)
	{
		//
	}
	END_CATCH
	return false;
}

BOOL CSelfAutoUpdateDlg::IsAliveProcess(CString processName, BOOL kill)
{
	CString newName = "";
		
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
	
	PROCESSENTRY32 pEntry;

//add. 
	pEntry.dwSize = sizeof(PROCESSENTRY32);
	

	BOOL bIsAlive = FALSE;
	
	do
	{	
		// 1. 프로세스명 얻기
		CString newName = pEntry.szExeFile;


		newName.MakeUpper();
		processName.MakeUpper();

		
		// 2. 프로세스명 비교하고 죽이기
		if( processName.Find(newName) == 0 )
		{
			if(kill){
				KillProcess(pEntry.th32ProcessID);
			}else{
				return TRUE;
			}
			bIsAlive = TRUE;
		}
	}
	while( Process32Next(hSnapShot,&pEntry) );
	
	CloseHandle(hSnapShot);

	return bIsAlive;
}


void
CSelfAutoUpdateDlg::myLogOpen(const char *filename) 
{
	string logFile = filename;

	// log file open
	_log.open(logFile.c_str(), ios_base::out);
	if( _log.fail() ) {
		_hasLog=false;
		return ;
	} 
	_hasLog=true;
	return;
}

void
CSelfAutoUpdateDlg::myLogClose() 
{
	if(_hasLog) _log.close();
	_hasLog = false;
	return;

}
void
CSelfAutoUpdateDlg::myDEBUG(const char *pFormat,...) 
{
 	char	aBuf[4096];
	va_list	aVp;
	va_start(aVp, pFormat);
	vsprintf(aBuf, pFormat, aVp);
	va_end(aVp);
	if(_hasLog){
		_log << aBuf << endl;
	}
}

