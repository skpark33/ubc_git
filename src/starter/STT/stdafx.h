// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

// 아래 지정된 플랫폼에 우선하는 플랫폼을 대상으로 하는 경우 다음 정의를 수정하십시오.
// 다른 플랫폼에 사용되는 해당 값의 최신 정보는 MSDN을 참조하십시오.
#ifndef WINVER				// Windows XP 이상에서만 기능을 사용할 수 있습니다.
#define WINVER 0x0501		// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_WINNT		// Windows XP 이상에서만 기능을 사용할 수 있습니다.                   
#define _WIN32_WINNT 0x0501	// 다른 버전의 Windows에 맞도록 적합한 값으로 변경해 주십시오.
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_WINDOWS 0x0410 // Windows Me 이상에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#ifndef _WIN32_IE			// IE 6.0 이상에서만 기능을 사용할 수 있습니다.
#define _WIN32_IE 0x0600	// 다른 버전의 IE에 맞도록 적합한 값으로 변경해 주십시오.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.
#include <afxmt.h>			// MFC 멀티스레드
#include <afxtempl.h>       // MFC CArray 템플릿 구성 요소입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT



#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

// Windows User Message 추가
#define UM_TRAYICON				WM_USER + 1
#define	WM_REFRESH				WM_USER + 101
#define WM_UPDATE				WM_USER + 111

// 추가한 소스

#define	CAPTION_HEIGHT			25
#define	BTN_TRANSPARENT_COLOR	RGB(239,173,197)

#define ACTIVE_TEXT_COLOR		RGB(255,255,255)
#define ACTIVE_BACK_COLOR		RGB( 96, 96,255)

#define INACTIVE_TEXT_COLOR		RGB(255,255,255)
#define INACTIVE_BACK_COLOR		RGB(127,127,127)

#define STARTING_TEXT_COLOR		RGB(255,255,255)
#define STARTING_BACK_COLOR		RGB( 32,224, 32)

#define STOPPING_TEXT_COLOR		RGB(255,255,255)
#define STOPPING_BACK_COLOR		RGB(255,160, 32)

#define OVERMEMORY_TEXT_COLOR	RGB(255,255,255)
#define OVERMEMORY_BACK_COLOR	RGB(255, 96, 96)

#define UPDATE_READY			100
#define UPDATE_COMPLETE			200

#define STARTER_WINDOWSNAME		"UTV Starter"
#define STARTER_PROCESSNAME		"UTV_starter.exe"
#define SELFAUTOUPDATE_EXE		"SelfAutoUpdate.exe"
#define STARTER_INI_NAME		"UBCStarter.ini"
#define AUTOUPDATE_WINDOWSNAME	"UBC AutoUpdater"

#define SttMessageBox(msg,msecond)	\
			CMsgBox obj(this); \
			obj.MessageBox(msg, "UTV Starter Message", msecond, MB_OK | MB_ICONINFORMATION | MB_TOPMOST);

#define SttMessageBoxH(handle,msg,msecond)	\
			CWnd* pWnd = CWnd::FromHandle(handle); \
			CMsgBox obj(pWnd); \
			obj.MessageBox(msg, "UTV Starter Message", msecond, MB_OK | MB_ICONINFORMATION | MB_TOPMOST);

typedef struct
{
	HWND	hWnd;			// 리스트컨트롤 핸들
	int		nCol;			// 정렬기준 컬럼
	bool	bAscend;		// true=내림차순, false=오름차순
	bool	bIsStringType;	// true = string, false = 숫자
} SORT_PARAM;

class APPLICATION_INFO 
{
public:
	APPLICATION_INFO() {
		runningType = "BG";
		autoStartUp = false;
		runTimeUpdate = false;
		isPrimitive = false;
		monitored = true; // 디폴트가 참
		initSleepSecond = 0;
		pid = 0;
		adminState=true;
	}
	APPLICATION_INFO(const char* procName) {
		appId = procName; 
		runningType = "BG";
		autoStartUp = false;
		runTimeUpdate = false;
		isPrimitive = false;
		monitored = true; // 디폴트가 참
		initSleepSecond = 0;
		pid = 0;
		adminState=true;
	}
	~APPLICATION_INFO() {}

	CString			appId;			// 어플리케이션 아이디
	CString			binaryName;		// 실행 파일 이름
	CString			argument;		// 입력 인수
	CString			properties;		// CORBA 인수
	CString			debug;			// 디버깅 인수
	CString			status;			// 어플리케이션 상태
	CString			startTime;		// 기동시각
	CString			runningType;	// 실행 방법
	unsigned long   pid;			// 프로세스 아이디
	bool			autoStartUp;	// 자동 재시동 여부
	bool			runTimeUpdate;	// 자동 업데이트 여부

	// 기능 확장 
	CString			preRunningApp;	// 선행되어야 하는 어플리케이션 아이디
	bool			isPrimitive;	// 운용상 반드시 필요한 어플리케이션
	bool			monitored;		// 모니터링 여부
	short			initSleepSecond;// 기동전 대기 시간

	bool			adminState;		// 관리자 On, Off 플래그

};

typedef CArray<APPLICATION_INFO,	APPLICATION_INFO&>	APPLICATION_LIST;

typedef struct
{
	CString	appId;
	CString	directive; // start, stop, watch
} THREAD_PARAM;
typedef CArray<THREAD_PARAM,THREAD_PARAM&> THREAD_PARAM_LIST;

COLORREF	DarkenColor(COLORREF col,double factor);
COLORREF	BrightenColor(COLORREF col,double factor);


CString		GetBinaryName(LPCSTR lpszFullPath);
CString		GetPath(LPCSTR lpszFullPath);

///////////////////////////////////////////////////////////////////////
//Modified by jwh184 2012-03-02
bool	GetUseHttp(void);		///<Http를 사용하는지 여부를 구한다.
///////////////////////////////////////////////////////////////////////
