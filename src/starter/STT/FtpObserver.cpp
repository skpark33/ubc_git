#include "stdafx.h"
#include "FtpObserver.h"
#include "LogTrace.h"

using namespace nsFTP;

FtpObserver::FtpObserver()
: m_bytesReceived(0)
{
}

FtpObserver::~FtpObserver()
{
}

void FtpObserver::OnSendCommand(const tstring& strCommand)
{
	if( strCommand.length()==0 )
		return;

	CString cszLine;
	if( strCommand.length()>4 && strCommand.substr(0, 5)==_T("PASS ") ) {
		cszLine.Format(_T("> PASS **********"));
	} else {
		cszLine.Format(_T("> %s"), strCommand.c_str());
	}
	WriteLine(cszLine.GetBuffer(0));
}

void FtpObserver::OnResponse(const CReply& response)
{
	tstring strResponse = response.Value();
	if( strResponse.length()==0 )
		return;

	WriteLine(_T("< ") + CString(strResponse.c_str()));
}

void FtpObserver::OnInternalError(const tstring& strErrorMsg, const tstring& strFileName, DWORD dwLineNr)
{
	CString cszMsg;
	cszMsg.Format(_T("%s ==> File \"%s\" (%d)"), strErrorMsg.c_str(), strFileName.c_str(), dwLineNr);
	WriteLine(cszMsg);
}

void FtpObserver::WriteLine(const char* cszLine)
{
	utvDEBUG(("%s", cszLine));;
}

void FtpObserver::OnBytesReceived(const TByteVector& Buffer, long lReceivedBytes)
{
	m_bytesReceived += lReceivedBytes;
}

unsigned long FtpObserver::GetBytesReceived()
{
	return m_bytesReceived;
}
