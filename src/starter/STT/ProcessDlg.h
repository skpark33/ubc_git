#pragma once
#include "afxwin.h"

#include "ComboBoxExt.h"
#include "EditEx.h"
#include "HoverButton.h"

class CSTTDlg;

// ProcessDlg 대화 상자입니다.

class ProcessDlg : public CDialog
{
	DECLARE_DYNAMIC(ProcessDlg)

public:
	enum AD_TYPE { AD_TYPE_ADD, AD_TYPE_MODIFY, AD_TYPE_DELETE, AD_TYPE_DETAIL_INFO };
	
	ProcessDlg(AD_TYPE type, CSTTDlg* sttDlg, LPCSTR lpszAppName = NULL, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~ProcessDlg();

	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PROCESS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

	AD_TYPE		m_type;
	CString		m_strAppName;
	CSTTDlg*	m_sttDlg;

	bool		InitComboBox();
	bool		CheckField();
	bool		LoadApplicationInfo(LPCSTR lpszAppName);

public:
	CEditEx m_appId;
	CEditEx m_binaryName;
	CEditEx m_argument;
	CEditEx m_properties;
	CEditEx m_debug;
	CEditEx m_startTime;
	CEditEx m_pid;
	CComboBoxExt m_cbxRunningType;
	CComboBoxExt m_cbxAutoStart;
	CComboBoxExt m_cbxRuntimeUpdate;

	CComboBoxExt m_cbxPreRunningApp;
	CComboBoxExt m_cbxIsPrimitive;
	CComboBoxExt m_cbxMonitored;
	CEditEx m_initSleepSecond;

	CComboBoxExt m_cbxAdminState;

	CHoverButton	m_btnOK;
	CHoverButton	m_btnCancel;

	APPLICATION_INFO	m_info;
};
