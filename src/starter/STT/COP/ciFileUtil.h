/*
 *  Copyright �� 200l SQISoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2002/12/05
 *  File name   : ciFileUtil.h
 */

#ifndef _ciFileUtil_h_
#define _ciFileUtil_h_

#include "COP/ciBaseType.h"

class COP_CI_API ciFileUtil
{
public:
	static BOOL openFile(FILE*& outFd, 
							const char* pMode,
							const char* pFilename, 
							const char* pSubPath = "", 
							const char* pHomeDir = "CONFIGROOT");

	static void makePath(ciString& outFullPath,
						const char* pFilename, 
						const char* pSubPath, 
						const char* pHomeDir);

	static BOOL isPathStartFromRoot(const char* pPath);

	static void addPath(ciString& root, const char* added);
	static void CreateDir(const char* pPath);
	static BOOL DeleteDir(const char* sPath);
	static BOOL CopyDir(const char* sourceDir, const char* targetDir, list<string>& failList);
	static BOOL CopyFile(const char* sourceFile, 
		const char* targetFile, 
		ciBoolean retry = ciFalse);
	static BOOL MoveDir(const char* fromDir, const char* toDir, list<string>& failList);
	static BOOL IsDots(const char* str);
};

#endif 

