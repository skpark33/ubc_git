/*
 *  Copyright �� 2002 SQISSoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2005/12/05
 *  File name   : ciProperties.h
 */

#ifndef _ciProperties_h_
#define _ciProperties_h_

//----------------------------------------------------------------------------
//	Include Header File
//----------------------------------------------------------------------------
#include "COP/ciConfig.h"
#include "COP/ciXConfig.h"

class COP_CI_API ciProperties : public ciXConfig
{
public:
	ciProperties();
	ciProperties(const char* module);
	virtual ~ciProperties();

	virtual void			clear();
	virtual void			setModule(const char* module);
	virtual const char*		getModule();
	virtual void			setEntryPrefix(const char* entryPrefix);
	virtual const char*		getEntryPrefix();

    virtual ciBoolean       load(const char* pFilename,
                                const char* pSubPath="",
                                const char* pHomeDir="CONFIGROOT");

	virtual ciBoolean		load(const char* pHomeDir,
								const char* pSubPath,
								const char* pFilename,
								const char* pEntry);

	virtual ciBoolean		write(const char* pFileName,
								const char* pSubPath="", 
								const char* pHomeDir="CONFIGROOT");

    virtual ciConfigEntry*  findEntry(const char* pEntry);

	// for entry buffer

	ciConfigIterator		begin();
	ciConfigIterator		end();
	ciBoolean				insert(ciConfigValueType& pVal);
	ciConfigIterator		find(const char* pVal);
	ciConfigIterator		find(ciString& pVal);
	ciBoolean           	empty();
	size_t              	size() const;

	ciString				getError( void ) { return _errMsg; }

protected:
	virtual ciBoolean		_load(const char* pFilename, 
								const char* pSubPath, 
								const char* pHomeDir);
	virtual ciBoolean		_load(const char* pFilename, 
								const char* pSubPath, 
								const char* pHomeDir, 
								const char* pEntry);

	ciString 		_module;
	ciString 		_entryPrefix;
	ciConfigEntry	_entryBuffer;
	ciString    	_errMsg;
};

#endif 

