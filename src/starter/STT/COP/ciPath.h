#ifndef _ciPath_h_
#define _ciPath_h_

#ifdef _COP_MSC_
#	define		_COP_CD(xxx)		ciPath::getInstance()->changeDrive(xxx)
#	define		_COP_PROGRAM_FILES(xxx)		ciPath::getInstance()->changeProgramFiles(xxx)
#else
#	define		_COP_CD(xxx)		xxx	
#	define		_COP_PROGRAM_FILES(xxx)		xxx	
#endif

#ifdef _COP_MSC_

#include <COP/ciBaseType.h>
#include <COP/ciMutex.h>
#include <map>
#include <windows.h>
#include <stdio.h>


class COP_CI_API ciPath {
public:

	static ciPath*	getInstance();
	static void	clearInstance();

	virtual ~ciPath() ;
	const char* changeDrive(const char* orgPath);
	const char* getDrive();

	const char* changeProgramFiles(const char* orgPath);
	bool Is64OS();

protected:
	ciPath();


	static ciPath*	_instance;
	static  ciMutex	_instanceLock;

	ciString _drive;
	ciMutex	_driveLock;

	map<string,string*>	_pathMap;
	ciMutex			_pathMapLock;

};
#endif

#endif // _ciPath_h_
