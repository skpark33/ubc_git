/*! \file ciStringUtil.h
 *
 *  Copyright ⓒ 2003 WINCC Inc.
 *  All Rights Reserved.
 *
 *  \brief 문자열 조작 관련 Utility function 들을 모아놓은 class
 *  (Environment: OSF1 5.1A)
 *
 *  \author 	jhchoi
 *  \version
 *  \date 		2003년 4월 22일 18:00
 */

#ifndef _ciStringUtil_h_
#define _ciStringUtil_h_

#include "ciBaseType.h"

class COP_CI_API ciStringUtil {
public:
	ciStringUtil();
	~ciStringUtil();

	static void divide(const ciString& pTarget, char pDeli, ciString* pFirst, ciString* pOther);

	static void toUpper(const ciString& str, ciString& retStr);
	static void toLower(const ciString& str, ciString& retStr);    
	static void toUpper(ciString& str);
	static void toLower(ciString& str);

	static void btoa(ciBoolean tf, char* pStr);
	static void itoa(int p_int, char* pStr);	
	static void uitoa(unsigned int p_uint, char* pStr);	
	static void shtoa(short p_short, char* pStr);	
	static void ushtoa(unsigned short p_ushort, char* pStr);	
	static void ltoa(long p_long, char* pStr);	
	static void ultoa(unsigned long p_ulong, char* pStr);	
	static void lltoa(ciLongLong p_longlong, char* pStr);	
	static void ulltoa(ciULongLong p_ulonglong, char* pStr);	
	static void dtoa(double p_double, char* pStr);	
	static void ftoa(float p_float, char* pStr);	
	
	static void btoa(ciBoolean tf, ciString& str);
	static void itoa(int p_int, ciString& str);
	static void uitoa(unsigned int p_uint, ciString& str);
	static void shtoa(short p_short, ciString& str);
	static void ushtoa(unsigned short p_ushort, ciString& str);
	static void ltoa(long p_long, ciString& str);
	static void ultoa(unsigned long p_ulong, ciString& str);
	static void lltoa(ciLongLong p_longlong, ciString& str);
	static void ulltoa(ciULongLong p_ulonglong, ciString& str);
	static void dtoa(double p_double, ciString& str);
	static void ftoa(float p_float, ciString& str);	

	static ciBoolean	atoB(ciString& str,ciBoolean &pBool);

	static void envTranslate(const char* target, ciString& outString);

	   static void rightTrim(char* str);

    static void rightTrim(ciString& str);
	static void	divideProperty(const char* target, 
						ciString& prefix,
						ciString& entry, ciString& lastname);

	static ciBoolean getWildPart(const char* pattern, ciString& value);
	static void cutPrefix(ciString& target, const char *prefix);
	static void cutPostfix(ciString& target, const char *prefix);
	static ciBoolean startsWith(const ciString& p_source, const ciString& p_dest);
	static ciBoolean simpleMatch(const char* string, const char* pattern);

	static ciBoolean atob(const char* value);
	static void stringReplace(ciString& target, const char* from, const char* to);
};

#endif //_ciStringUtil_h_
