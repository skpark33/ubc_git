/*! \file ciBaseType.h
 *
 *  Copyright ⓒ 2003 WINCC Inc.
 *  All Rights Reserved.
 *
 *  \brief STL 의 string type 및 프로젝트에서 사용할 타입들을 typedef 한다.
 *  (Environment: OSF1 5.1A)
 *
 *  \author 	jhchoi
 *  \version
 *  \date 		2003년 4월 22일 18:00
 */

#ifndef _ciBaseType_h_
#define _ciBaseType_h_

/*
#define _COP_HPUX_  //or _COP_LINUX_ , _COP_SOLARIS_ , _COP_WIN32_ , _COP_HPUX_ , _COP_WINCE_ , _COP_TRU64_ //OS
#define _COP_ACC_ //or _COP_CXX_[Version]_ , _COP_GCC_<Version>_ , _COP_MSC_[Version]_ //COMPILER
*/

#define COP_CI_API 
#define PTHREAD_SELF()	GetCurrentThreadId()

#define ciDoList(_list_, _itr_) \
    for( (_itr_) = (_list_).begin() ; (_itr_) != (_list_).end() ; ++(_itr_))

#define ciDoListPtr(_list_, _itr_) \
    for( (_itr_) = (_list_)->begin() ; (_itr_) != (_list_)->end() ; ++(_itr_))

#define ORA_DATE_FORMAT     "YYYY/MM/DD HH24:MI:SS"
#define ORA_DATE_FORMAT_C   "%Y/%m/%d %H:%M:%S"
#define ORA_DATE_DATE_FORMAT_C   "%Y/%m/%d"
#define UNIX_DATE_FORMAT        "+%Y/%m/%d %H:%M:%S"
#define UNIX_DATE_FORMAT_C  "%04d/%02d/%02d %02d:%02d:%02d"

#ifdef __ASSERT_FUNCTION    // in GCC LIBRARY
    #define ASSERT_FUNC " operation : "<<__ASSERT_FUNCTION
#else
    #define ASSERT_FUNC ""
#endif

#define COP_ASSERT(exp) \
    if(!(exp)) { \
        cerr<<"in "<<__FILE__<<" line : "<<__LINE__<<" "<<ASSERT_FUNC; \
        cerr<<" Assertion Failed !!"; \
        abort(); \
    }

#ifdef _COP_MSC_

#   define COP_ISSPACE(x)   iswspace(x)

#else

#   define COP_ISSPACE(x)   isspace(x)

#endif

// for debug
#define ciSET_DEBUG(p_level,p_package)

#define ciIS_DEBUG_ON(p_level) (0)

#define ciDEBUG(p_level,msg)

#define ciDEBUG2(p_level,msg)

#define ciWARN(msg)

#define ciWARN2(msg)

#define ciERROR(msg)

#define ciERROR2(msg)

#include <cstdio>
#include <string>
#include <list>

#include <iostream>
using namespace std;

//using namespace _STLP_NEW_IO_NAMESPACE;


#define ciTrue		true
#define ciFalse	false

typedef string			ciString; //basic_string<char>
typedef list<ciString>  ciStringList;

//typedef wstring		ciWString; //basic_string<wchar_t>
typedef basic_string<wchar_t>		ciWString; //wstring
typedef short			ciShort;
typedef unsigned short 	ciUShort;
typedef int 			ciInt;
typedef unsigned int 	ciUInt;

#if defined(_COP_TRU64_) || defined(_COP_HPUX_)
	typedef signed int ciLong;
	typedef unsigned int ciULong;
#else
	typedef signed long ciLong;
	typedef unsigned long ciULong;
#endif

#ifdef _COP_MSC_
	typedef __int64 ciLongLong;
	typedef unsigned __int64 ciULongLong;
    
#else
	typedef long long ciLongLong;
	typedef unsigned long long ciULongLong;
#endif

typedef float			ciFloat;
typedef double			ciDouble;
typedef char			ciByte;
typedef char 			ciChar;
typedef long			ciDate;
typedef bool 			ciBoolean;

#define null 0

#ifdef _COP_MSC_
	#define COP_INFINITE 0xFFFFFFFF
#else
	#define COP_INFINITE 0x00000000L
#endif

#endif //_ciBaseType_h_

