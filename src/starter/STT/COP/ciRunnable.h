#ifndef __CI_RUNNABLE_H__
#define __CI_RUNNABLE_H__

class COP_CI_API ciRunnable {
	public:
		virtual void run() = 0;
		virtual void closed() {};
};

#endif //__CI_RUNNABLE_H__

