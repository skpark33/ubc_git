#pragma once

#include <afxinet.h>
#include <afxtempl.h>
#include "COP/ciTimer.h"
#include "COP/ciSyncUtil.h"

// CCustomerSpecific

class AutoUpdateDlg;

class SpecificEntry 
{
public:
	SpecificEntry() : downloadSucceed(ciFalse), isExecuted(ciFalse),
		beforeMoveResult(ciTrue),afterMoveResult(ciTrue),moveResult(ciFalse),isMoved(ciFalse)  {}

	ciString targetPath;
	ciString movePath;
	ciString beforeMove;
	ciString afterMove;
	ciString versionLine;
	ciBoolean	downloadSucceed;

	ciBoolean beforeMoveResult;
	ciBoolean afterMoveResult;
	ciBoolean moveResult;

	ciBoolean	isExecuted;
	ciBoolean	isMoved;

	void		WriteLog(ubcIni* logIni, const char* key, const char* nowstr);
	ciBoolean	Execute();
};
typedef map<CString, SpecificEntry*>  SpecificEntryMap;

class CCustomerSpecific
{
public:
	static CCustomerSpecific*	getInstance();
	static void clearInstance();

	virtual ~CCustomerSpecific();

	void		Init(AutoUpdateDlg* dlg);
	BOOL		Download();
	BOOL		AppendVersion(LPCSTR localPath);
	BOOL		Execute();
	BOOL		SetDownloadResult(CString& target, BOOL result);

	void		Clear();

protected:
	BOOL		_Download(LPCSTR filename, CString& localPath);
	BOOL		_LoadIni(LPCSTR localPath);
	BOOL		_LoadVersion(LPCSTR localPath);
	BOOL		_WriteLog();


	static ciMutex _sMutex;
	static CCustomerSpecific* _instance;

	CCustomerSpecific();

	BOOL		m_hasSpecific;
	CString		m_strHostId;
	CString		m_strCustomer;
	AutoUpdateDlg*		m_pDlg;

	SpecificEntryMap	m_map;
	ciMutex			m_mapLock;

};

