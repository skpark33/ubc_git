#pragma once

#include "stdafx.h"
#include "UBC/ubcIni.h"
#include "COP/ciSyncUtil.h"

// STTDoc 문서입니다.

class CSTTDlg;

class STTDoc : public CDocument
{
	DECLARE_DYNCREATE(STTDoc)

public:
	STTDoc();
	virtual ~STTDoc();
#ifndef _WIN32_WCE
	virtual void Serialize(CArchive& ar);   // 문서 입/출력을 위해 재정의되었습니다.
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual BOOL OnNewDocument();

	DECLARE_MESSAGE_MAP()

	APPLICATION_LIST	m_applicationList;
	
	CString m_strID;		// AutoUpdate User ID
	CString m_strPassword;	// AutoUpdate Password
	CString m_strIpAddr;	// AutoUpdate IP Address
	CString m_strIpAddr2;	// AutoUpdate IP Address
	unsigned short m_port;	// AutoUpdate Port
	CString m_strVersion;	// AutoUpdate Project Version
	unsigned short m_autoUpdate;
	unsigned short m_appMonitor;
	CString m_strIniPath;	// INI Path
	bool	m_serverFlag;	// Server Management Flag
	CString m_hostId;		// Host ID from 인증서

	///////////////////////////////////////////////////
	//Added by jwh184 2002-03-06
	unsigned short	m_sHttpPort;	///<UBCConnect의 UBCCENTER 필드의 HTTP_PORT 값
	///////////////////////////////////////////////////

	ciMutex _mutex;

public:
	// 도큐먼트 초기화	
	bool init();
	void SetServerFlag(bool serverFlag = true);
	bool GetServerFlag();

	bool GetUBCCenterInfo(ciString& ip, ciUShort& port);
	///////////////////////////////////////////////////
	//Modified by jwh184 2002-03-06
	void GetFTPInfo(CString& id, CString& passwd, CString& ipAddr, CString& ipAddr2,unsigned short& port, CString& ver, unsigned short& sHttpPort) 
	{
		id = m_strID; passwd = m_strPassword; 
		ipAddr = m_strIpAddr; 
		ipAddr2 = m_strIpAddr2; 
		port = m_port; 
		ver = m_strVersion;
		sHttpPort = m_sHttpPort;
	}
	///////////////////////////////////////////////////

	void GetTimerPeriod(int& autoUpdate, int& appMonitor) {
		autoUpdate = m_autoUpdate; appMonitor = m_appMonitor;
	}

	bool GetTimerPeriodInfo( short& agentAppMonitor, 
						    short& agentHeartbeat, 
						    short& agentHostMonitor, 
						    short& agentScreenShot, 
						    short& autoUpdate, 
						    short& starterAppMonitor);
	
	bool SetTimerPeriodInfo( short agentAppMonitor, 
							 short agentHeartbeat, 
							 short agentHostMonitor, 
							 short agentScreenShot, 
							 short autoUpdate, 
							 short starterAppMonitor);

	bool	GetUpdateFtpInfo(ciString& ipAddr, ciString& id, ciString& password, ciUShort& port);
	bool	SetUpdateFtpInfo(LPCSTR ipAddr, LPCSTR id, LPCSTR password, ciUShort port);
	bool	AddUpdateFtpInfo(LPCSTR ipAddr, LPCSTR id, LPCSTR password, ciUShort port);
	bool	DeleteUpdateFtpInfo();

	bool	GetScreenShotFtpInfo(ciString& ipAddr, ciString& id, ciString& password, ciUShort& port);
	bool	SetScreenShotFtpInfo(LPCSTR ipAddr, LPCSTR id, LPCSTR password, ciUShort port);
	bool	AddScreenShotFtpInfo(LPCSTR ipAddr, LPCSTR id, LPCSTR password, ciUShort port);
	bool	DeleteScreenShotFtpInfo();

	bool	ReloadApplicationInfo();	
	bool	AddApplicationInfo(APPLICATION_INFO& app_info, BOOL write = false, BOOL send_udp = false);
	bool	GetApplicationInfo(LPCSTR lpszAppName, APPLICATION_INFO& app_info);
	bool	GetApplicationsInfo(APPLICATION_LIST& app_list);
	bool	GetApplicationNameList(CArray<CString, CString&>& appNameList);
	bool	SetApplicationInfo(APPLICATION_INFO& app_info, BOOL write = true, BOOL send_udp = false);
	bool	SetApplicationInfo(
							LPCSTR lpszAppName, 
							LPCSTR lpszAppStatus, 
							LPCSTR lpszStartTime="");
	void	SetProcessStatus(LPCSTR lpszAppName, LPCSTR lpszStatus);
	bool	DeleteApplicationInfo(LPCSTR lpszAppName, BOOL write = true, BOOL send_udp = false);

	// AGENT에게 프로세스 변경 정보를 UDP로 전송
	bool	SendApplicationInfo(const char* directive, APPLICATION_INFO& app_info);

	bool	SetProperty(LPCSTR name, LPCSTR value);
	bool	DelProperty(LPCSTR name);

	// UBCVariables.ini 파일에서 AutoUpdateFlag 값을 가져온다.
	bool	GetAutoUpdateFlag();
	bool	SetAutoUpdateFlag(bool updateFlag);

	LPCTSTR	GetHostID() {
		return m_hostId;
	}
	
	void	Lock();
	void	UnLock();
	bool	Reload();

protected:
	// starter.cfg 에서 프로세스 정보를 가져온다.
	void _initFTPInfo();
	void _initTimerPeriod();
	bool _initConfig();
	bool _initLoadProcess(LPCSTR procName);
	void _initHostInfo();

	bool _WriteAddAppInfo(APPLICATION_INFO& app_info);
	bool _WriteSetAppInfo(APPLICATION_INFO& app_info);
	bool _WriteSetAppInfo(LPCSTR lpszAppName, 
							LPCSTR lpszAppStatus, 
							LPCSTR lpszStartTime=NULL);
	bool _WriteDeleteAppInfo(LPCSTR appName);

	bool _updateAppInfo(LPCSTR name, LPCSTR value);
	bool _updateInfoValue(APPLICATION_INFO& info, ciString& attr, const char*value);
	bool _deleteAppInfo(LPCSTR name);
};
