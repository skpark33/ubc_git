// AutoUpdateDlg.h : header file
//

#if !defined(AFX_TAUTOUPDATEDLG_H__A36D6301_3E9D_420D_8261_0A85E6B70345__INCLUDED_)
#define AFX_TAUTOUPDATEDLG_H__A36D6301_3E9D_420D_8261_0A85E6B70345__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
#include <afxinet.h>
#include "STTDoc.h"
#include "COP/ciSyncUtil.h"

typedef class __EnvVersionInfo
{
public:
	__EnvVersionInfo() : linenum(-1), updateComplete(false) {}
	CString procname;
	CString keyname;
	CString version;
	CString srcname;
	CString dstname;
	CString dirname;
	CString crc;
	int linenum;
	BOOL	updateComplete;
}VERSION_INFO, *PVERSION_INFO;

typedef CArray<VERSION_INFO, VERSION_INFO> VERSIONLIST, *PVERSIONLIST;
//
//-- 데이터 화일 위치, 파일명
//

// FTP 서버의 ROOT 부터의 상대경로
// FTP 서버측 VERSION 경로 "UTV1.0"은 UBCConnect.ini의 [UBCCENTER]VERSION 값으로 설정
#define VERSION_SRC_NAME		"version.txt"
// 로컬 디스크의 PROJECT_HOME 에서의 상대경로
#define VERSION_DST_NAME_TMP	"version.txt.tmp"
#define VERSION_DST_NAME		"version.txt"

// FTP 서버의 ROOT 부터의 상대경로
// FTP 서버측 VERSION 경로 "ubc"는 UBCConnect.ini의 [UBCCENTER]VERSION 값으로 설정
#define VERSION_SERVER_SRC_NAME		"ReleaseInfo/version.txt"
// 로컬 디스크의 PROJECT_HOME 에서의 상대경로
#define VERSION_SERVER_DST_NAME_TMP	"ReleaseInfo/version.txt.tmp"
#define VERSION_SERVER_DST_NAME		"ReleaseInfo/version.txt"

#define RELEASE_SRC_NAME		"../ReleaseInfo/release.txt"
// 로컬 디스크의 PROJECT_HOME 에서의 상대경로
#define RELEASE_DST_NAME_TMP	"../ReleaseInfo/release.txt.tmp"
#define RELEASE_DST_NAME		"../ReleaseInfo/release.txt"

// FTP 서버의 ROOT 부터의 상대경로
// FTP 서버측 RELEASE 경로 "ubc"는 UBCConnect.ini의 [UBCCENTER]RELEASE 값으로 설정
#define RELEASE_SERVER_SRC_NAME		"ReleaseInfo/release.txt"
// 로컬 디스크의 PROJECT_HOME 에서의 상대경로
#define RELEASE_SERVER_DST_NAME_TMP	"ReleaseInfo/release.txt.tmp"
#define RELEASE_SERVER_DST_NAME		"ReleaseInfo/release.txt"


// FTP 서버의 업데이트 제한 리스트 파일
#define AUTOUPDATE_DISABLE_CHECKLIST		"AutoUpdateDisableList.txt"

#define ITEM_NUM_AT_LINE		7	// 2010.01.08 crc 추가
#define DOWNLOAD_TMP_DIRECTORY	"_tmp_folder"

#define TIMER_INIT_UPDATE		1007
#define TIMER_ERROR				1008
#define TIMER_CHECK_MINIMIZE	1009

#define WM_ENDUPDATE (WM_USER +777)
#define WM_SETWINDOWTEXT	(WM_ENDUPDATE+1)

#include "TProgressCtrl.h"
#include "FtpUtil.h"
#include "FileServiceWrapDll.h"
#include "resource.h"
#include "Tlhelp32.h"

typedef struct {
	CString srcname;
	CString dstname;
	CString dirname;
	FtpUtil* util;
	bool	bComplete;
} FTP_THREAD_PARAM;

typedef struct {
	BOOL serverRestartFlag;
	BOOL rerunFlag;
	CStringList appRTU;
	CStringList appNRTU;
	int updateFileCount;
	CStringList strTargetFileNameList;
} KILLED_PROCESS_INFO;

/////////////////////////////////////////////////////////////////////////////
// AutoUpdateDlg dialog

class AutoUpdateDlg : public CDialog, public CFileServiceWrap::IProgressHandler
{
// Construction
public:
	AutoUpdateDlg(CWnd* pParent = NULL);	// standard constructor
	void GetFTPInfo();
	BOOL GetVersionFile(CString& strVersionTmpPath); 
	// 업데이트 제한 리스트에 자신이 포함되어 있는지 확인
	BOOL IsAutoUpdatePossible();
	void EndUpdate(int updateFlag);

	STTDoc* GetDocument() const;
	void SetDocument(STTDoc* document);
	void SetForcedUpdateFlag(BOOL forcedUpdateFlag);

	BOOL TerminateProcess(	BOOL& serverRestartFlag, 	
							BOOL& rerunFlag,
						 	CStringList& appRTU, // runtime update passible
							CStringList& appNRTU, // runtime update impassible
							int& updateFileCount		) ;

public:
	BOOL aFileDownload(FtpUtil *ptftp, VERSION_INFO _versioninfo, int nErrorTime = 600000);
	BOOL IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs = NULL);
	BOOL StilAliveProcess(CString processName, DWORD processID);
	void TerminateProcess(CString processName);

	/////////////////////////////////////////////////////
	// Added by jwh184 2012-05-08
	bool	IsForceUpdate(VERSION_INFO& infoVersion);		///<강제로 업데이트를 해야하는 지 판단한다.
	/////////////////////////////////////////////////////

	CString GetDstDir();
	CString GetCurDir(); // 이 모듈이 위치한 디렉토리

	CBrush m_brA, m_brB;

	CString m_strID;						//repo 제외 한 파일들의 FTP 정보
	CString m_strPassword;
	CString m_strIpAddr;
	CString m_strIpAddr2;
	ciUShort m_port;

	CString m_strVersionSrcName;
	CString m_strVersionDstName;
	CString m_strVersionDstTmpName;
	CString m_strVersion;

	CString m_strReleaseSrcName;
	CString m_strReleaseDstName;
	CString m_strReleaseDstTmpName;

	////////////////////////////////////////////////
	//Added by jwh184 2012-06-27
	bool	m_bHttpMode;
	////////////////////////////////////////////////


	CString m_strDST_DIR;
	// 도큐먼트 멤버
	STTDoc* m_pDocument;


	// Mutex among Auto Update by timer, 
	// Manual Update by FirmWareView and 
	// Application Monitor by timer
	static ciMutex m_sLock;

	static UINT FTPRunThread(LPVOID pParam);
	static UINT FTPDownloadThread(LPVOID pParam);
	static BOOL ms_FTPDownloadActive;

	virtual void AutoUpdate();
	virtual BOOL AnalysisVersionFile(LPCSTR lpszFileName, PVERSIONLIST pVersionList);
	virtual void AnalysisVersionFileLocal(LPCSTR lpszFileName, PVERSIONLIST pVersionList);
	virtual void ModifyVersionInfo(PVERSION_INFO pVerInfo);
	virtual void ParsEnvFileLine(LPSTR envline, PVERSION_INFO pVersionList);
	virtual void DiscardOldVersionOnObtainList(PVERSIONLIST pOldList);

	inline VOID KillProcess(DWORD dwProcessId);
	inline BOOL IsNewVersion(LPCSTR strNewVersion, LPCSTR strCurrent);

	void RenewVersionFile();

	void	SetAutoUpdateFlag(BOOL p) { m_update_only = p; }
	void	SetShowMessageBoxFlag(BOOL flag) { m_showMessageBox = flag; }
	void	SetIsServer(BOOL p) { m_isServer = p; if(m_isServer) this->m_bHttpMode = true; }
	void	SetIsManager(BOOL p) { m_isManager = p; }

	BOOL isService(CString& filename);
	BOOL TerminateService();
	BOOL RunService();
	BOOL RunService(char* pName, int nArg, char** pArg);
	BOOL BounceProcess(char* pName, int nIndex) ;
	BOOL KillService(char* pName) ;

	static UINT ShowCheckUpdateMsg(LPVOID pParam);

	///////////////////////////////////////////////////////////////////////
	//Modified by jwh184 2012-03-02
	CFileServiceWrap*	m_pHttpFile;		///<HTTP를 이용한 파일전송 객체

	bool		HttpConnectTest();															///<두개의 주소로 웹서버에 연결 테스트를 한다.
	BOOL		HttpFileDownload(CFileServiceWrap *pHttpFile, VERSION_INFO _versioninfo);	///<Http를 이용하여 파일을 다운로드 하는 함수

	virtual void ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag,
								ULONGLONG uCurrentBytes,
								ULONGLONG uTotalBytes,
								CString strLocalPath);						///<Http FileService 진행상태 표시
	///////////////////////////////////////////////////////////////////////

protected:
	BOOL	_isCopySucceed(ciStringList& failList, CString& srcname);	
	
	ciStringList	m_ocxList;

	BOOL	m_update_only;
	BOOL	m_showMessageBox;
	BOOL	m_isServer;
	BOOL	m_isManager;
	
	BOOL _aFileDownload(FtpUtil* ptftp, 
						VERSION_INFO _versioninfo);
	BOOL m_bNewMainGUI;
	BOOL m_bSelfUpdate;
	BOOL m_bSelfUpdateInclude;  //SelfAutoUpdate.exe 가 Update 대상인지 살핀다.
	BOOL m_passiveFlag; // FTP Passive 모드 여부

	HWND m_hThis;

	VERSIONLIST m_listUpdateEnable;
	int m_TotalFileNum;
	int m_DownedNum;
	CListCtrl	m_listLog;

	BOOL m_ForcedUpdateFlag;

// Dialog Data
	//{{AFX_DATA(AutoUpdateDlg)
	enum { IDD = IDD_AUTOUPDATE_DIALOG };
	TProgressCtrl	m_ProgressBarAll;
	CStatic	m_TransState;
	CStatic	m_TransFileName;
	TProgressCtrl	m_ProgressBar;
	CStatic	m_TransFileName2;

	KILLED_PROCESS_INFO	m_KilledProcessInfo;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AutoUpdateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(AutoUpdateDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg LRESULT OnSetWindowText(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TAUTOUPDATEDLG_H__A36D6301_3E9D_420D_8261_0A85E6B70345__INCLUDED_)
