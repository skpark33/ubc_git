// STT.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "STT.h"
#include "STTDlg.h"

#include "AutoUpdateDlg.h"
#include "LogTrace.h"
#include "COP/ciArgParser.h"
#include "COP/ciStringUtil.h"
#include "COP/ciEnv.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSTTApp

BEGIN_MESSAGE_MAP(CSTTApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CSTTApp 생성

CSTTApp::CSTTApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
	_isServer = FALSE;
	_update_only = FALSE;
}


// 유일한 CSTTApp 개체입니다.

CSTTApp theApp;


// CSTTApp 초기화

BOOL SetThreadLocaleEx(LCID locale)
{
	USES_CONVERSION;
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if(::GetVersionEx(&osv) == FALSE) //get Version Failed 
		return ::SetThreadLocale(locale);

	switch(osv.dwMajorVersion)
	{
	case 6:  //WINDOWS VISTA
		{
			unsigned (__stdcall* SetThreadUILanguage)(LANGID);
			CStringA strThreaUILanguage = T2A(_T("SetThreadUILanguage"));

			HINSTANCE hIns = ::LoadLibrary(_T("kernel32.dll"));  
			if(hIns == NULL) //no Instance
				return ::SetThreadLocale(locale);

			SetThreadUILanguage = (unsigned (__stdcall* )(LANGID))::GetProcAddress(hIns,strThreaUILanguage);   
			if(SetThreadUILanguage == NULL) //procAddress Getfailed 
				return ::SetThreadLocale(locale);

			LANGID lang = SetThreadUILanguage(locale);
			return (lang==LOWORD(locale))?TRUE:FALSE;
		} 
	default: //WINDOWS 2000, XP
		return ::SetThreadLocale(locale);
	}

}

BOOL CSTTApp::InitInstance()
{
	SetErrorMode(SEM_NOGPFAULTERRORBOX);

	/////////////////////////////////////////////////////////////
	//Added by jwh184 2002-03-05
	// Initialize OLE libraries
	if(!AfxOleInit())
	{
		AfxMessageBox("OLE initialization failed.  Make sure that the OLE libraries are the correct version.");
		return FALSE;
	}//if
	/////////////////////////////////////////////////////////////

	ciArgParser::initialize(__argc,__argv);
	ciEnv::defaultEnv(ciFalse, "utv1");
	
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("로컬 응용 프로그램 마법사에서 생성된 응용 프로그램"));

	// language
	LANGID lang_id = PRIMARYLANGID(GetSystemDefaultLangID());
	if(ciArgParser::getInstance()->isSet("+eng"))		lang_id = LANG_ENGLISH;
	else if(ciArgParser::getInstance()->isSet("+jpn"))	lang_id = LANG_JAPANESE;
	else if(ciArgParser::getInstance()->isSet("+kor"))	lang_id = LANG_KOREAN;

	switch(lang_id)
	{
	case LANG_KOREAN:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_KOREAN, SUBLANG_KOREAN) , SORT_DEFAULT));
		break;
	case LANG_JAPANESE:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_JAPANESE, SORT_JAPANESE_UNICODE) , SORT_DEFAULT));
		break;
	default:
		SetThreadLocaleEx( MAKELCID( MAKELANGID(LANG_ENGLISH , SUBLANG_ENGLISH_US) , SORT_DEFAULT));
		break;
	}

	//skpark add
	ciString applName = "Starter";
	_update_only = false;
	if(ciArgParser::getInstance()->isSet("+update_only")) {
		_update_only = true;
	}
	boolean updateFlag = true;
	if(ciArgParser::getInstance()->isSet("+noupdate")) {
		updateFlag = false;
	}
	if(_update_only){
		applName = "Updater";
	}

	//for(int i=1; i<__argc; ++i) {
	//	CString strArg = __argv[i];
	//	if (strArg.Compare("+update_only") == 0) {
	//		_update_only = true;
	//		break;
	//	}
	//}	
	ciString customerId = "", scope = "";
	{
		ubcIni aIni("UBCConnect", "CONFIGROOT", "data");
		aIni.get("CUSTOMER_INFO","NAME",customerId);

		if(customerId.length() == 0)
		{
			ubcConfig bIni("UBCVariables");
			bIni.get("CUSTOMER_INFO","NAME",customerId);
		}
	
		if(customerId.length() == 0) customerId = "Default";
	}
	if(ciArgParser::getInstance()->isSet("+server")){
		_isServer = TRUE;
		scope = "Server";
	}
	else if(ciArgParser::getInstance()->isSet("+manager")){
		scope = "Manager";
	}
	else if(ciArgParser::getInstance()->isSet("+studioEE") || ciArgParser::getInstance()->isSet("+studio")) {
		scope = "Studio";
	}
	else
	{
		char ac[MAX_PATH+1];
		DWORD len=MAX_PATH;
		//if (gethostname(ac, sizeof(ac)) == -1) {
		if (GetComputerName(ac, &len) == FALSE) {
			const char* computerName = getenv("COMPUTERNAME");
			if(!computerName){
				scope = "UNKNOWN";
			}else{
				scope = computerName;
			}
		}else{
			scope = ac;
		}
	}

	if(_isServer && !_update_only){
		if(this->_getProcessCount("UTV_starter.exe") > 1){
			AfxMessageBox("UTV_starter already exist. Stop UBC and try again(server)");
			Sleep(5000);
			exit(1);
		}
	}

	BOOL starter_was_alive = FALSE;
	HANDLE hMutex =0;
	if(!_update_only) {
		hMutex = ::CreateMutex(NULL,FALSE, "UTV_starter");
		if(!hMutex || ::GetLastError() == ERROR_ALREADY_EXISTS) {
			if(!_update_only){
				AfxMessageBox("UTV_starter already exist. Stop UBC and try again");
				Sleep(5000);
				exit(1);
			}
			starter_was_alive=TRUE;  // starter 는 원래 살아있었다.
		}
	}else{
		hMutex = ::OpenMutex(MUTEX_ALL_ACCESS,FALSE, "UTV_starter");
		if(hMutex){
			starter_was_alive=TRUE;  // starter 는 원래 살아있었다.
		}
		hMutex = ::CreateMutex(NULL,FALSE, "Updater");
		if(!hMutex || ::GetLastError() == ERROR_ALREADY_EXISTS) {
			AfxMessageBox("Updater already exist. Stop UBC and try again");
			Sleep(5000);
			exit(1);
		}
	}
	//scratchUtil::getInstance()->DoAdditionalJob();


	/*
	if(!_update_only){
		if (starter_was_alive)
		{
			if(::GetLastError() == ERROR_ALREADY_EXISTS) {
				AfxMessageBox("UTV_starter already exist. Stop UBC and try again");
				exit(1);
			}
		}
	}
	*/
	/*
	// run_starter_again 옵션이 있을때는 무조건 스타터를 다시 띠운다.
	if(ciArgParser::getInstance()->isSet("+run_starter_again")) {
		//AfxMessageBox("run_starter_again");
		starter_was_alive = true;
	}
	*/


	if(ciArgParser::getInstance()->isSet("+start")) {
		::Sleep(10*1000);
	}
	// 로그파일 관련 초기화
	//ciArgParser::initialize(__argc,__argv);

	//if(ciArgParser::getInstance()->isSet("+log")) {
	ciString logFileName = ciEnv::newEnv("PROJECT_HOME");
	if (logFileName.empty()) {
		//logFileName = _COP_CD("C:\\Project\\UTV\\log\\DEBUG_STT.log");			
		logFileName = _COP_CD("C:\\Project\\UTV\\log\\");			
	} else {
		//logFileName += "\\log\\DEBUG_STT.log";
		logFileName += "\\log\\";
	}
	logFileName += applName;
	logFileName += ".log";

	// log 파일 백업
	ciString logBackupFileName = logFileName;
	logBackupFileName += ".bak";
	try {
		CFile::Remove(logBackupFileName.c_str());
	} catch (...) {
		utvDEBUG(("remove %s failed.", logBackupFileName.c_str()));
	}
	try {
		CFile::Rename(logFileName.c_str(), logBackupFileName.c_str());
	} catch (...) {
		utvDEBUG(("rename %s failed.", logBackupFileName.c_str()));
	}
	utvLogOn(logFileName.c_str());
	//}

	utvDEBUG(("====== %s Aplication 이 시작됩니다. ======", applName.c_str()));

	//
	CSTTDlg::m_strCustomerId = customerId.c_str();
	CSTTDlg::m_strScope = scope.c_str();
	CSTTDlg::CheckConfigVariables();
	//

	ciString ip="";
	if(!_isServer){
		ubcConfig aIni("UBCConnect");

		ciBoolean isStatic = ciFalse;
		if (!aIni.get("UBCCENTER","STATIC",isStatic)) {
			utvERROR(("[UBCCENTER]STATIC get failed."));
		}

		if(!isStatic) {

			if (!aIni.get("UBCCENTER","IP",ip)) {
				utvERROR(("[UBCCENTER]IP get failed."));
			}
			if(ip.empty() || ip != "UBCLICENSE.SQISOFT.COM"){
				if (!aIni.set("UBCCENTER","IP", "UBCLICENSE.SQISOFT.COM")) {
					utvERROR(("[UBCCENTER]IP set failed.1"));
				}
				//if (!aIni.set("UBCCENTER","IP_LICENSE", "UBCLICENSE.SQISOFT.COM")) {
				//	utvERROR(("[UBCCENTER]IP set failed.2"));
				//}
				//if (!aIni.set("UBCCENTER","IP_UPDATE", "UBCUPDATE.SQISOFT.COM")) {
				//	utvERROR(("[UBCCENTER]IP set failed.3"));
				//}
			}
			ip="";
			if (!aIni.get("VNCMANAGER","IP",ip)) {
				utvERROR(("[VNCMANAGER]IP get failed."));
			}
			if(ip.empty() || ip == "211.232.57.202" ){
				if (!aIni.set("VNCMANAGER","IP", "UBCLICENSE.SQISOFT.COM")) {
					utvERROR(("[VNCMANAGER]IP set failed.3"));
				}
			}

			// 현대/기아를 위해 UpdatCenter IP 를 UBCConnect.ini 에 써준다.
			{
				ciBoolean useCustomerCenter=ciFalse;
				ubcConfig bIni("UBCVariables");
				ciString customerId = "";
				bIni.get("CUSTOMER_INFO","NAME",customerId);
				ciString new_update_center_ip ="UBCLICENSE.SQISOFT.COM";  // 202 Server
				if(strncmp(customerId.c_str(),"KIA",3)==0){
					new_update_center_ip = "58.87.34.251";  // WEB02 Server
					useCustomerCenter=ciTrue;
				}else if(strncmp(customerId.c_str(),"HYUNDAI",7)==0){
					new_update_center_ip = "58.87.34.248";  // WEB02 Server
					useCustomerCenter=ciTrue;
				}else if(strncmp(customerId.c_str(),"KPOST",5)==0){
					new_update_center_ip = "10.223.2.187";  // WEB02 Server
					useCustomerCenter=ciTrue;
				}
				ubcConfig aIni("UBCConnect");
				ciString buf;
				aIni.get("UBCCENTER","FTP_IP", buf);
				if (buf.empty() || ( buf != new_update_center_ip &&  buf != "UBCLICENSE.SQISOFT.COM"))  {
					if (!aIni.set("UBCCENTER","FTP_IP", new_update_center_ip.c_str())) {
						ciERROR(("[UBCCENTER]FTP_IP=%s set failed.", new_update_center_ip.c_str()));
					}else{
						ciDEBUG(1, ("[UBCCENTER]FTP_IP=%s set succeed.", new_update_center_ip.c_str()));
					}
				}else{
					ciDEBUG(1, ("[UBCCENTER]FTP_IP=%s already set.", buf.c_str()));
				}
			}
		} // !isStatic
		this->_fixSiteIdBugInStarterArg();  // +site 가 +host 에 딱 붙어있는 경우를 해결한다.

	}else{
		ubcIni aIni("UBCConnect", "C:\\Project\\ubc\\config", "data");
		if (!aIni.get("UBCCENTER","IP", ip)) {
			utvERROR(("[UBCCENTER]IP get failed."));
		}
		if(ip.empty() || ip != "UBCLICENSE.SQISOFT.COM"){
			if (!aIni.set("UBCCENTER","IP", "UBCLICENSE.SQISOFT.COM")) {
				utvERROR(("[UBCCENTER]IP set failed.1"));
			}
			//if (!aIni.set("UBCCENTER","IP_LICENSE", "UBCLICENSE.SQISOFT.COM")) {
			///	utvERROR(("[UBCCENTER]IP set failed.2"));
			//}
			//if (!aIni.set("UBCCENTER","IP_UPDATE", "UBCUPDATE.SQISOFT.COM")) {
			//	utvERROR(("[UBCCENTER]IP set failed.3"));
			//}
		}
	}




	// 만약 UBCStarter.ini 가 없으면 utv1.properties 파일을 근간으로 제조함.
	CFileFind ff;
	if(!ff.FindFile(_COP_CD("C:\\SQISoft\\UTV1.0\\execute\\data\\UBCStarter.ini"))){
		ciString command = _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\UTV_sample.exe +util +pro2ini");
		if(system(command.c_str())==0){
			utvDEBUG(("%s succeed",command.c_str()));
		}else{
			utvERROR(("%s failed", command.c_str()));
		}
	}
	ff.Close();
	int displayCounter = scratchUtil::getInstance()->getMonitorCount();
	if(displayCounter==1){
		// 만약, 모니터 갯수가 1개라면,  brwClient2_UBC2.exe 는 떠있어서는 안되며
		// Auto 도 해제되어 있어야 한다.
		ubcConfig aIni("UBCStarter");
		aIni.set("BROWSER1","AUTO_STARTUP", "FALSE");
		utvDEBUG(("AUTO_STARTUP=FALSE"));
	}


	{ // skpark start
	TCHAR Buffer[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, Buffer);

	utvDEBUG(("====== Current Directory = %s", Buffer));

	const char* cop_home = ciEnv::newEnv("COP_HOME");
	const char* project_home = ciEnv::newEnv("PROJECT_HOME");
	const char* logroot	= ciEnv::newEnv("LOGROOT");
	const char* configroot = ciEnv::newEnv("CONFIGROOT");
	const char* path = ciEnv::newEnv("PATH");

	utvDEBUG(("====== COP_HOME = %s", cop_home));
	utvDEBUG(("====== PROJECT_HOME = %s", project_home));
	utvDEBUG(("====== LOGROOT = %s", logroot));
	utvDEBUG(("====== CONFIGROOT = %s", configroot));
	utvDEBUG(("====== PATH = %s", path));
	/*
	ifstream in("modifyBRW.log");
	if(in.fail()){
		
		//if(system("UTV_sample.exe  +util +modifyBRW")==0){
		//	ofstream out("modifyBRW.log");
		//	out << "complete" << endl;
		//	out.close();
		//}
		
		const char* command = "UTV_sample.exe";
		const char* arg=" +util +modifyBRW";
		if(scratchUtil::getInstance()->createProcess(command, arg,0,true)){
			ofstream out("modifyBRW.log");
			out << "complete" << endl;
			out.close();
		}
	}
	*/
	} // skpark end

	// Document 생성
	_doc = new STTDoc;
	if(_isServer){
		_doc->SetServerFlag(true);
	}

	if (!_doc->init()) {
		//AfxMessageBox("도큐먼트 초기화에 실패하였습니다.");
		utvERROR(("도큐먼트 초기화에 실패하였습니다."));
	}
	if(ciFalse == ciArgParser::getInstance()->isSet("+_no_auth_")){  //skpark :  인증 무시 옵션
		if (_isServer) {
			while(Authentication() == false) {
				if (IDRETRY == AfxMessageBox(
#ifdef AFX_TARG_KOR
					"서버의 인증에 실패하였습니다."
					"\n다음 사항을 점검하여 주시기 바랍니다."
					"\n"
					"\n- 서버가 인터넷에 접근가능한지 확인하시기 바랍니다."
					"\n- 만약 서버의 IP 어드레스를 변경했다면, 인증을 다시 받아야 합니다."
					"\n- 서버의 인증을 받지 않았거나,  IP 어드레스가 변경되었다면,"
					"\n  서버 Configurator 을 열어서 인증 버튼을 눌러서 인증을 받은 후에"
					"\n  다시 시도 하시기 바랍니다."
					"\n- 문제가 계속된다면 UBC Center 에 연락해 주시기 바랍니다."
#else
					"Failed to certification."
					"\nPlease, check out following sentences."
					"\n"
					"\n- Internet connection."
					"\n- If you changed IP Address, you should be certificated again."
					"\n- If you were not certificated or you changed IP Address,"
					"\n  Open Configurator and click certification botton"
					"\n  and after cerfification and try again."
					"\n- If this problem is repeated, contact to UBC Center, please."
#endif
					, MB_RETRYCANCEL | MB_ICONINFORMATION
					))
				{
					utvDEBUG(("Authentication failed. Retry."));
				} else {
					utvDEBUG(("Authentication failed. Exit."));
					exit(1);
				}
			}
			utvDEBUG(("Authentication success..."));
		}
	}
	// AutoUpdate
	// Argument +noupdate
	if (updateFlag)
	{
		// Ini File Check
		if (_doc->GetAutoUpdateFlag()) 
		{
			// AutoUpdate Initialize
			if (!InitAutoUpdate(_doc)) 
			{
				//AfxMessageBox("자동 업데이트에 실패하였습니다.");
				utvERROR(("자동 업데이트에 실패하였습니다."));
			}
		} else {
			utvDEBUG(("+noupdate is set."));
			updateFlag = false;
		}
	}

	CString arg;
	for(int i=1; i<__argc; i++) {
		CString strArg = __argv[i];
		if (strArg.Compare("+update_only") == 0)			continue;
		if (strArg.Compare("+server") == 0)					continue;
		if (strArg.Compare("+run_starter_again") == 0)		continue;
		if (strArg.Compare("+log") == 0)					continue;
		if (strArg.Compare("+noupdate") == 0)				continue;
		if (strArg.Compare("+start") == 0)					continue;
		if (strArg.Compare("+_no_auth_") == 0) 				continue;
		if (strArg.Compare("+firmwareView") == 0) 			continue;
		if (strArg.Compare("+studio") == 0)					continue;
		if (strArg.Compare("+studioWedding") == 0)			continue;
		if (strArg.Compare("+studioEE") == 0)				continue;
		if (strArg.Compare("+manager") == 0)				continue;
		
		arg += " ";
		arg += strArg;
	}	
		// HYUNDAI_KIA 의 경우에만 해당함
#ifdef _HYUNDAI_KIA_
	{
		/*
		ubcConfig aIni("UBCVariables");
		unsigned short use_click_schedule=0;
		aIni.get("ROOT","USE_CLICK_SCHEDULE", use_click_schedule);
		if(!use_click_schedule){
			utvERROR(("[ROOT]USE_CLICK_SCHEDULE get failed."));
			if(!aIni.set("ROOT","USE_CLICK_SCHEDULE", (unsigned short)1)){
				utvERROR(("[ROOT]USE_CLICK_SCHEDULE set failed."));
			}
		}
		*/
	}
#endif


	if(_update_only){

		// 만약 firmwareView 가 띠운것이고, 스타터가 죽어있다면...firmwareview를 띠워야 한다.
		if(ciArgParser::getInstance()->isSet("+firmwareView")){
			utvDEBUG(("+firmwareView is set"));
			if(starter_was_alive == FALSE){
				utvDEBUG(("starter was not alive"));
				hMutex = ::OpenMutex(MUTEX_ALL_ACCESS,FALSE, "FirmwareView");
				if(!hMutex){ // Open 이 안되면 없는거다..다시 살려야 한다.
					utvDEBUG(("run UBCFirmwareView.exe"));
					scratchUtil::getInstance()->createProcess("UBCFirmwareView.exe","",0,ciFalse);
					return FALSE;
				}
			}
		}else{
			utvDEBUG(("+firmwareView is not set"));
		}

		ciString binary="";
		//Sleep(1000);
		if(ciArgParser::getInstance()->isSet("+studio")){
			binary = "UBCStudioPE";
		}
		if(ciArgParser::getInstance()->isSet("+studioWedding")){
			binary = "UBCWeddingStudio";
		}
		if(ciArgParser::getInstance()->isSet("+studioEE")){
			binary = "UBCStudioEE";
		}
		if(ciArgParser::getInstance()->isSet("+manager")){
			binary = "UBCManager";
		}
		if(!binary.empty()){
			ciString lang = scratchUtil::getInstance()->getLang();
			utvDEBUG(("LANG=%s", lang.c_str()));
			if(!lang.empty() || binary != "UBCWeddingStudio"){
				ciString old = binary;
				binary += "_";		
				binary += lang;
				binary += ".exe";
				utvDEBUG(("binary=%s", binary.c_str()));
				if(scratchUtil::getInstance()->createProcess(binary.c_str(),arg.GetBuffer(),0,ciFalse)<=0){
					// 각 language 별 파일이 아직 없어서 실패했다고 본다.
					old += ".exe";
					utvDEBUG(("%s binary file run failed", binary.c_str()));
					utvDEBUG(("binary=%s", old.c_str()));
					scratchUtil::getInstance()->createProcess(old.c_str(),arg.GetBuffer(),0,ciFalse);
				}
			}else{
				binary += ".exe";
				scratchUtil::getInstance()->createProcess(binary.c_str(),arg.GetBuffer(),0,ciFalse);
			}
			return FALSE;
		}
		if(starter_was_alive==FALSE){
			// 스타터는 원래 살아있지 않았다.  따라서 그냥 종료한다.
			return FALSE;
		}
		// 스타터는 원래 살아있었다.!!!
		hMutex = ::OpenMutex(MUTEX_ALL_ACCESS,FALSE, "UTV_starter");
		if(hMutex){
			// 그런데 지금도 살아있다.  따라서 그냥 종료한다.
			return FALSE;		
		}
		// 스타터는 원래 살아있었다.  그런데 지금은 죽었다.  따라서,  스타터를 시작해준다.
	}

	CSTTDlg dlg(updateFlag);
	m_pMainWnd = &dlg;	
	dlg.SetDocumnet(_doc);

	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}

	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

BOOL CSTTApp::InitAutoUpdate(STTDoc* document)
{
	// AutoUpdate

	//아규먼트
	/*
	if(__argc >= 3)
	{
		AfxMessageBox("잘못된 argument 입니다");
		return FALSE;
	}*/

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	AutoUpdateDlg dlg;
	dlg.SetAutoUpdateFlag(_update_only);
	dlg.SetIsServer(_isServer);
	dlg.SetDocument(document);

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{

	}
	else if (nResponse == IDCANCEL)
	{

	}
	return TRUE;
}

BOOL CSTTApp::Authentication()
{
	// Get key and password
	ciString enterpriseKey, mac;
	if (!scratchUtil::getInstance()->readAuthFile(enterpriseKey,mac))
	{
		utvERROR(("key, mac get failed."));
		return FALSE;
	}

	//skpark 인증로직을 수정함.
	if (enterpriseKey.empty() || mac.empty())
	{
		utvERROR(("key, mac get failed."));
		return FALSE;
	}
	if(!scratchUtil::getInstance()->IsExistmacaddr(mac. c_str())){
		utvERROR(("authFile mac=%s is different", mac.c_str()));
		return FALSE;
	}


/*
	// Get UBC Center information
	ciString centerIp;
	ciUShort centerPort;
	if (_doc) {
		if (!_doc->GetUBCCenterInfo(centerIp, centerPort)) {
			utvERROR(("Check UBCCENTER section in UBCConnect.ini."));
			return FALSE;
		}
	} else {
		utvERROR(("Invalid!!!! STTDoc is null."));
		return FALSE;
	}

	// Authenticate from server
	CString command;
	command.Format("%s\\bin8\\UTV_sample.exe +util +login +key %s +pwd %s " 
		"-ORBInitRef NameService=corbaloc:iiop:%s:%d/NameService",
		ciEnv::newEnv("PROJECT_HOME"),
		enterpriseKey.c_str(), passwd.c_str(),
		centerIp.c_str(), centerPort);

	int result = system(command.GetBuffer(0));

	if (result) {
		utvERROR(("Invalid key. [%s:%s]", enterpriseKey.c_str(), passwd.c_str()));
		return FALSE;
	}
*/
	return TRUE;
}

unsigned long	
CSTTApp::_getpid(const char* exename)
{
	utvDEBUG(("getPid(%s)", exename));

	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) {
		utvERROR(("HANDLE을 생성할 수 없습니다"));
		return 0;
	}
	PROCESSENTRY32 pe32 ;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	char strProcessName[512];

	if ( !Process32First ( hSnapshot, &pe32 ) )	{
		utvERROR(("Process32First failed."));
		::CloseHandle(hSnapshot);
		return 0;
	}
	

	do 	{
		//memset(strProcessName, 0, sizeof(strProcessName));
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(pe32.szExeFile, exename) == 0 )		{		
			utvDEBUG(("process founded(%s)",exename));
			::CloseHandle(hSnapshot);
			return pe32.th32ProcessID;
		}
		
	} while (Process32Next( hSnapshot, &pe32 ));
	utvDEBUG(("process not founded(%s)", exename));
	::CloseHandle(hSnapshot);

	return 0;
}

unsigned long	
CSTTApp::_getProcessCount(const char* exename)
{
	utvDEBUG(("_getProcessCount(%s)", exename));

	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) {
		utvERROR(("HANDLE을 생성할 수 없습니다"));
		return 0;
	}
	PROCESSENTRY32 pe32 ;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	char strProcessName[512];

	if ( !Process32First ( hSnapshot, &pe32 ) )	{
		utvERROR(("Process32First failed."));
		::CloseHandle(hSnapshot);
		return 0;
	}
	
	int count=0;
	do 	{
		//memset(strProcessName, 0, sizeof(strProcessName));
		//size_t stringLength = strlen(pe32.szExeFile);
		//for(int i=0; i<stringLength; i++) // 대문자로 전환
		//	strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(pe32.szExeFile, exename) == 0 )		{		
			utvDEBUG(("process founded(%s)",exename));
			count++;
		}
		
	} while (Process32Next( hSnapshot, &pe32 ));
	utvDEBUG(("process not founded(%s)", exename));
	::CloseHandle(hSnapshot);

	return count;
}

void
CSTTApp::_fixSiteIdBugInStarterArg()
{
	// 4.22 버전에서 
	// "STARTER.APP_BROWSER.ARGUMENT" 에서 +siteId 값이 space 없이 앞에 인수에 붙어버린경우가
	// 발생할 수 있는데,  이렇게 +siteId 값이 붙었다면 space 를 삽입해준다.

	ciString arg;
	if(!ubcConfig::lvc_getProperty("STARTER.APP_BROWSER.ARGUMENT", arg)){
		// ERROR	
		return;
	}

	//////////////////////////////////////////////////////////////////////////////////
	// 2012-06-27 Modified by jwh 읽어온 arg 변수의 길이가 0인 경우 처리
	if(arg.length() != 0)
	{


		if(arg.substr(1,5) != "+site" && strstr(arg.c_str(), " +site") == NULL) { // 없으면
			if(strstr(arg.c_str(), "+site") != NULL) { // 있으면
				ciStringUtil::stringReplace(arg,"+site"," +site");
				utvDEBUG(("+site was stick to +host  (%s)", arg.c_str()));
				ubcConfig::lvc_setProperty("STARTER.APP_BROWSER.ARGUMENT", arg.c_str());
			}
		}
	}//if
	//////////////////////////////////////////////////////////////////////////////////
	return;

}
int CSTTApp::ExitInstance()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	/////////////////////////////////////////////////////////////
	//Added by jwh184 2002-03-05
	CoUninitialize();
	/////////////////////////////////////////////////////////////

	return CWinApp::ExitInstance();
}
