//  LogTrace.cpp -- Interface for the CLogTrace class
//  A class to do debug logging


#ifndef __LOGTRACE_H__
#define __LOGTRACE_H__

using namespace std;
#include "common/libScratch/scratchUtil.h"

#define utvDEBUG(msg)	scratchUtil::getInstance()->myDEBUG msg
#define utvERROR(msg)	scratchUtil::getInstance()->myERROR msg
#define utvWARN(msg)	scratchUtil::getInstance()->myWARN msg
#define utvLogOn(filename)	scratchUtil::getInstance()->myLogOpen(filename)
#define utvLogOff()			scratchUtil::getInstance()->myLogClose()


#endif // __LOGTRACE_H__</PRE>