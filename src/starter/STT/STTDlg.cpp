// STTDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "STT.h"
#include "STTDlg.h"
#include "ProcessDlg.h"
#include "LogTrace.h"
#include "AppMonitor.h"
#include "UpdateMonitor.h"
#include "AutoUpdateDlg.h"
#include "MsgBox.h"
#include "common/libScratch/scEnv.h"

#include "../libConfigVariables/ConfigVariables.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "COP/ciStringTokenizer.h"
#include "COP/ciBaseType.h"
#include "COP/ciTime.h"
#include "COP/ciEnv.h"
#include "COP/ciArgParser.h"


#define		TIMER_ID_CONFIG_VARIABLES		1023
#define		TIMER_TIME_CONFIG_VARIABLES		(60 * 1000) // 1min



// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CSTTDlg 대화 상자

CSTTDlg::CSTTDlg(BOOL update, CWnd* pParent /*=NULL*/)
	: CDialog(CSTTDlg::IDD, pParent), 
	m_pDocument(NULL), m_hasUpdate(false), m_update(update)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CSTTDlg::~CSTTDlg()
{
	if (m_pDocument) {
		delete m_pDocument;
		m_pDocument = NULL;
	}
	// 프로그램 종료후에도 트레이아이콘의 잔상이 남는 경우의 처리
	AfxGetApp()->LoadIcon(IDI_TRAY);
}

STTDoc* CSTTDlg::GetDocument() const
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(STTDoc)));
	return (STTDoc*)m_pDocument;
}

void CSTTDlg::SetDocumnet(STTDoc* document)
{
	ASSERT(document);
	m_pDocument = document;
}

void CSTTDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NEW, m_btnRegister);
	DDX_Control(pDX, IDC_START, m_btnStart);
	DDX_Control(pDX, IDC_STOP, m_btnStop);
	DDX_Control(pDX, IDC_WATCH, m_btnWatch);
	DDX_Control(pDX, IDC_AUTOUPDATE, m_btnAutoUpdate);
	DDX_Control(pDX, IDC_STATIC_TITLE, m_staticTitle);
	DDX_Control(pDX, IDC_LIST_PROCESS, m_listctrlProcess);
}

BOOL CSTTDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE) return TRUE;
		if(pMsg->wParam == VK_RETURN) return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CSTTDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_NEW, OnBnClickedNew)
	ON_BN_CLICKED(IDC_START, OnBnClickedStart)
	ON_BN_CLICKED(IDC_STOP, OnBnClickedStop)
	ON_BN_CLICKED(IDC_WATCH, OnBnClickedWatch)
	ON_BN_CLICKED(IDC_AUTOUPDATE, OnBnClickedAutoUpdate)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_PROCESS, OnNMRclickListProcess)
	ON_COMMAND(ID_PROCESS_REGISTER, OnProcessRegister)
	ON_COMMAND(ID_PROCESS_MODIFY, OnProcessModify)
	ON_COMMAND(ID_PROCESS_DELETE, OnProcessDelete)
	ON_COMMAND(ID_PROCESS_DETAIL_INFO, OnProcessDetailInfo)
	ON_MESSAGE(WM_REFRESH, OnRefresh)
	ON_MESSAGE(UM_TRAYICON,TrayIconMessage)
	ON_MESSAGE(WM_UPDATE,TrayIconUpdate)	
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_COMMAND(ID_TRAY_OPEN, OnTrayOpen)
	ON_COMMAND(ID_TRAY_CLOSE, OnTrayClose)
	ON_COMMAND(ID_MENU_ABOUT, OnTrayAbout)
	ON_COMMAND(ID_MENU_UPDATE, OnTrayUpdate)
	ON_COMMAND(ID__DEBUG, &CSTTDlg::OnDebugOnOff)
	ON_UPDATE_COMMAND_UI(ID__DEBUG, &CSTTDlg::OnUpdateDebugOnOff)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CSTTDlg 메시지 처리기

BOOL CSTTDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	CRect rect;
	m_listctrlProcess.GetClientRect(&rect);
	int nColInterval = rect.Width()/5;

	m_listctrlProcess.InsertColumn(0, "", LVCFMT_LEFT, 28);
#ifdef AFX_TARG_KOR
	m_listctrlProcess.InsertColumn(1, _T("프로세스 이름"), LVCFMT_LEFT, nColInterval+30);
	m_listctrlProcess.InsertColumn(2, _T("상태"), LVCFMT_LEFT, nColInterval-10);
	m_listctrlProcess.InsertColumn(3, _T("기동 시각"), LVCFMT_LEFT, nColInterval+40);
	m_listctrlProcess.InsertColumn(4, _T("실행 파일 이름"), LVCFMT_LEFT, nColInterval+40);
#else
	m_listctrlProcess.InsertColumn(1, _T("PROCESS NAME"), LVCFMT_LEFT, nColInterval+30);
	m_listctrlProcess.InsertColumn(2, _T("STATUS"), LVCFMT_LEFT, nColInterval-10);
	m_listctrlProcess.InsertColumn(3, _T("START TIME"), LVCFMT_LEFT, nColInterval+40);
	m_listctrlProcess.InsertColumn(4, _T("BINARY NAME"), LVCFMT_LEFT, nColInterval+40);
#endif

	m_listctrlProcess.SetMaxColumn(5);
    m_listctrlProcess.Initialize();
    m_listctrlProcess.SetExtendedStyle(
        m_listctrlProcess.GetExtendedStyle() | LVS_EX_CHECKBOXES | 
		LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_staticTitle.SetColor( BrightenColor( GetSysColor(COLOR_INACTIVECAPTION), 0.9f) );
	m_staticTitle.SetGradientColor( GetSysColor(COLOR_INACTIVECAPTION) );
	m_staticTitle.SetReverseGradient();
	m_staticTitle.SetVerticalGradient(FALSE); //set gradient to be vertical

	m_btnRegister.LoadBitmap	(IDB_BTN_REGISTRATION,	BTN_TRANSPARENT_COLOR);
	m_btnStart.LoadBitmap		(IDB_BTN_START,			BTN_TRANSPARENT_COLOR);
	m_btnStop.LoadBitmap		(IDB_BTN_STOP,			BTN_TRANSPARENT_COLOR);
	m_btnWatch.LoadBitmap		(IDB_BTN_STATUS,		BTN_TRANSPARENT_COLOR);
	m_btnAutoUpdate.LoadBitmap	(IDB_BTN_AUTOUPDATE,		BTN_TRANSPARENT_COLOR);

	// 버튼 비활성화
	m_btnAutoUpdate.EnableWindow(FALSE);

	CRect client_rect;
	GetClientRect(client_rect);
	RePosControl(client_rect.Width(), client_rect.Height());

	// 프로세스 초기화
	if (!_init()) {
		//AfxMessageBox("프로세스 초기화에 실패하였습니다.");
		utvERROR(("프로세스 초기화에 실패하였습니다."));
		return FALSE;
	}

	// TRAY 초기화
	_initTray();

	// 타이머 주기
	int updatorPeriod, monitorPeriod;
	m_pDocument->GetTimerPeriod(updatorPeriod, monitorPeriod);

	// 프로세스 모니터링 타이머 시작
	AppMonitor* monitor = AppMonitor::getInstance();
	if(monitor==0){
		utvERROR(("Fatal : Memory New Failed !!!"));
		utvERROR(("Fatal : Memory New Failed !!!"));
		utvERROR(("Fatal : Memory New Failed !!!"));
		utvERROR(("Fatal : Memory New Failed !!!"));
	}
	monitor->init(m_pDocument, monitorPeriod);
	utvDEBUG(("monitor init end"));
	monitor->startTimer();
	utvDEBUG(("monitor startTimer end"));

#ifndef _HYUNDAI_KIA_   // 현대기아의 경우에만, 안한다.!!!
	// 업데이트 모니터링 타이머 시작
	// Server 모드인 경우에는 업데이트 모니터 사용 안함, 최초 기동시에만 업데이트 받는다.
	if(m_update && FALSE == m_pDocument->GetServerFlag()) 
	{
		UpdateMonitor* updator = UpdateMonitor::getInstance();
		updator->init(updatorPeriod);
		updator->SetDocument(m_pDocument);
		updator->startTimer();
	}
#endif

	this->CenterWindow();

	//+minimize 옵션 처리
	if(ciArgParser::getInstance()->isSet("+minimize")) 
	{
		this->ShowWindow(SW_MINIMIZE);
	}//if

	//+shownormal 이 아니면 다 미니마이즈한다.  
	if(!ciArgParser::getInstance()->isSet("+shownormal")) 
	{
		this->ShowWindow(SW_MINIMIZE);
	}//if

	// <--
	// ConfigVariables 체크 타이머
	SetTimer(TIMER_ID_CONFIG_VARIABLES, TIMER_TIME_CONFIG_VARIABLES, NULL);
	// -->

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CSTTDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	// 타이머 종료
	AppMonitor::clearInstance();
	if(m_update) {
		UpdateMonitor::clearInstance();
	}

	// 로그 종료
	utvDEBUG(("====== Starter Aplication 이 종료됩니다. ======"));
	utvLogOff();

	//트래이 리소스 해제 
	NOTIFYICONDATA nid;
	::ZeroMemory(&nid, sizeof(nid));
	nid.cbSize = sizeof(nid);
	nid.hWnd = m_hWnd;
	nid.uID = IDI_TRAY;
	Shell_NotifyIcon(NIM_DELETE,&nid);//삭제플래그를 준다.
}

void CSTTDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	ShowWindow(SW_HIDE);

	//CDialog::OnClose();
}

void CSTTDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	RePosControl(cx, cy);
}

bool CSTTDlg::_init()
{

	bool retval =  ReloadProcessInfo();
	return retval;

}

void CSTTDlg::_initTray()
{
	NOTIFYICONDATA nid; 

	::ZeroMemory(&nid, sizeof(nid));
	nid.cbSize = sizeof(nid);
	nid.hWnd = m_hWnd;    //트레이아이콘과 통신할 윈도우 핸들
	nid.uID = IDI_TRAY;   //트레이아이콘의 리소스ID(Data측면)
	nid.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP;
	nid.uCallbackMessage = UM_TRAYICON; //트레이아이콘의 메시지가 수신시 수신윈도우가 처리할 메시지
	nid.hIcon = AfxGetApp()->LoadIcon(IDI_TRAY);//아이콘리소스(UI측면)

	lstrcpy(nid.szTip, STARTER_WINDOWSNAME); // 툴팁

	// taskBar상태영역에 아이콘 추가,삭제,수정할때 시스템에 메시지 전달
	Shell_NotifyIcon(NIM_ADD,&nid); //구조체내용으로 트레이아이콘을 등록한다.(Data측면)

	SendMessage(WM_SETICON,(WPARAM)TRUE,(LPARAM)nid.hIcon);//트레이화면에 붙이기(UI측면)
}

void CSTTDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CSTTDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
/*
	if(ciArgParser::getInstance()->isSet("+bg")) // +bg 가 있으면 Hide 한다.
	{
		utvDEBUG(("SHOW HIDE"));
		this->ShowWindow(SW_HIDE);
	}//if
*/

}

bool CSTTDlg::ReloadProcessInfo()
{
	bool ret_value = false;

	m_listctrlProcess.DeleteAllItems();

	STTDoc *pDoc = GetDocument();
	if(pDoc)
	{
		APPLICATION_LIST appList;
		pDoc->GetApplicationsInfo(appList);

		int count = appList.GetCount();
		for(int i=0; i<count; i++)
		{
			APPLICATION_INFO& info = appList.GetAt(i);
			// monitored application only
			if (info.monitored) InsertItem(info);
		}

		m_listctrlProcess.Sort();
		ret_value = true;
	}
	return ret_value;
}

bool CSTTDlg::UpdateProcess(APPLICATION_INFO& info)
{
	int ret_value = false;
	int count = m_listctrlProcess.GetItemCount();

	for(int i=0; i<count; i++)
	{
		CString app_id = m_listctrlProcess.GetItemText(i, 1);

		if(info.appId.CompareNoCase(app_id)==0)
		{
			InsertItem(info, i);
			ret_value = true;
			break;
		}
	}
	return ret_value;
}

bool CSTTDlg::InsertItem(APPLICATION_INFO& info, int index)
{
	STTDoc* pDoc = GetDocument();

	if(pDoc == NULL)
		return false;

	if(index < 0)
	{
		index = m_listctrlProcess.GetItemCount();
		m_listctrlProcess.InsertItem(index, "");
		m_listctrlProcess.SetItemData(index, index);
	}

	//m_listctrlProcess.InsertItem(index, "");
	m_listctrlProcess.SetItemText(index, 1, info.appId);	
	m_listctrlProcess.SetItemText(index, 2, info.status);
	m_listctrlProcess.SetItemText(index, 3, info.startTime);
	m_listctrlProcess.SetItemText(index, 4, info.binaryName);

	m_listctrlProcess.RedrawItems(index, index);

	// 도큐먼트에 추가
	//pDoc->AddApplicationInfo(info);

	return true;
}

bool CSTTDlg::DeleteItem(LPCSTR lpszAppName)
{
	STTDoc* pDoc = GetDocument();

	if(pDoc == NULL)
		return false;

	int count = m_listctrlProcess.GetItemCount();

	for(int i=0; i<count; i++)
	{
		CString app_id      = m_listctrlProcess.GetItemText(i, 1);

		if(app_id.CompareNoCase(lpszAppName)==0)
		{
			m_listctrlProcess.DeleteItem(i);

			// 도큐먼트에서 삭제
			//pDoc->DeleteApplicationInfo(lpszAppName);
			return true;
		}
	}

	return false;
}


#define	BUTTON_WIDTH 85

void CSTTDlg::RePosControl(int cx, int cy)
{
	if(m_listctrlProcess.GetSafeHwnd())
	{
		m_listctrlProcess.MoveWindow(0, CAPTION_HEIGHT*3, cx, cy-CAPTION_HEIGHT*3);
	}

	if(m_staticTitle.GetSafeHwnd())
	{
		m_staticTitle.MoveWindow(0, 0, cx, CAPTION_HEIGHT);
		m_staticTitle.Invalidate();
	}

	if(m_btnRegister.GetSafeHwnd())
		m_btnRegister.MoveWindow(10 + BUTTON_WIDTH*0, CAPTION_HEIGHT*3/2, BUTTON_WIDTH,25);

	if(m_btnStart.GetSafeHwnd())
		m_btnStart.MoveWindow(10 + BUTTON_WIDTH*1, CAPTION_HEIGHT*3/2, BUTTON_WIDTH,25);

	if(m_btnStop.GetSafeHwnd())
		m_btnStop.MoveWindow(10 + BUTTON_WIDTH*2, CAPTION_HEIGHT*3/2, BUTTON_WIDTH,25);

	if(m_btnWatch.GetSafeHwnd())
		m_btnWatch.MoveWindow(10 + BUTTON_WIDTH*3, CAPTION_HEIGHT*3/2, BUTTON_WIDTH,25);

	if(m_btnAutoUpdate.GetSafeHwnd())
		m_btnAutoUpdate.MoveWindow(10 + BUTTON_WIDTH*5 + 40, CAPTION_HEIGHT*3/2, BUTTON_WIDTH,25);
}

bool CSTTDlg::GetApplicationInfo(LPCSTR lpszAppName, APPLICATION_INFO& app_info)
{
	STTDoc* pDoc = GetDocument();

	if(pDoc == NULL)
		return false;

	return pDoc->GetApplicationInfo(lpszAppName, app_info);
}

bool CSTTDlg::GetApplicationNameList(CArray<CString, CString&>& appNameList)
{
	STTDoc* pDoc = GetDocument();

	if(pDoc == NULL)
		return false;

	return pDoc->GetApplicationNameList(appNameList);
}

void CSTTDlg::OnBnClickedNew()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	return OnProcessRegister();
}

void CSTTDlg::OnBnClickedStart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	

	BeginWaitCursor( ); 

	int count = m_listctrlProcess.GetItemCount();
	CString msg = "";

	THREAD_PARAM_LIST* pThreadParamList = new THREAD_PARAM_LIST;

	for(int i=0; i < count; i++)
	{
		int check = m_listctrlProcess.GetCheck(i);

		if (check)
		{
			CString status = m_listctrlProcess.GetItemText(i, 2);

			if( status.CompareNoCase("Inactive") == 0 )
			{
				THREAD_PARAM thread_param;
				thread_param.appId			= m_listctrlProcess.GetItemText(i, 1);
				thread_param.directive		= "start";

				pThreadParamList->Add(thread_param);

				m_listctrlProcess.SetCheck(i, FALSE);
			}
			else
			{
				CString app_id = m_listctrlProcess.GetItemText(i, 1);

				CString tmp;
#ifdef AFX_TARG_KOR
				tmp.Format("[ %s ] 는 %s 상태입니다.\r\n", app_id, status);
#else
				tmp.Format("[ %s ] is %s\r\n", app_id, status);
#endif

				msg += tmp;
			}
		}
	}

	if(pThreadParamList->GetSize() > 0)
		AfxBeginThread(ThreadCallFunc, pThreadParamList);
	else
		delete pThreadParamList;

	EndWaitCursor();

	if(msg.GetLength() > 0) {
		//AfxMessageBox(msg, MB_ICONWARNING);
		utvERROR((msg));
	}
}

void CSTTDlg::OnBnClickedStop()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString msg = "";
/* skpark
#ifdef AFX_TARG_KOR
	if( AfxMessageBox("프로세스 중지 작업을 계속 하시겠습니까?", MB_YESNO | MB_ICONQUESTION) != IDYES) return;
#else
	if( AfxMessageBox("Do you work process stop continuously?", MB_YESNO | MB_ICONQUESTION) != IDYES) return;
#endif
*/
	BeginWaitCursor( ); 

	int count = m_listctrlProcess.GetItemCount();
	
	THREAD_PARAM_LIST* pThreadParamList = new THREAD_PARAM_LIST;

	for(int i=0; i<count; i++)
	{
		int check = m_listctrlProcess.GetCheck(i);
		
		if (check)
		{
			CString status = m_listctrlProcess.GetItemText(i, 2);

			if( status.CompareNoCase("Active") == 0 || status.CompareNoCase("Starting") == 0)
			{
				THREAD_PARAM thread_param;
				thread_param.appId			= m_listctrlProcess.GetItemText(i, 1);
				thread_param.directive		= "stop";

				pThreadParamList->Add(thread_param);
			}
			else
			{
				CString app_id = m_listctrlProcess.GetItemText(i, 1);

				CString tmp;
#ifdef AFX_TARG_KOR
				tmp.Format("[ %s ] 는 %s 상태입니다.\r\n", app_id, status);
#else
				tmp.Format("[ %s ] is %s\r\n", app_id, status);
#endif
				msg += tmp;
			}
		}
	}

	if(pThreadParamList->GetSize() > 0)
		AfxBeginThread(ThreadCallFunc, pThreadParamList);
	else
		delete pThreadParamList;

	EndWaitCursor(); // Remove the hourglass cursor.

	if(msg.GetLength() > 0) {
		SttMessageBox(msg, 2000);
	}

}

void CSTTDlg::OnBnClickedWatch()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString msg = "";

	BeginWaitCursor( ); 

	int count = m_listctrlProcess.GetItemCount();
	
	THREAD_PARAM_LIST* pThreadParamList = new THREAD_PARAM_LIST;

	for(int i=0; i<count; i++)
	{
		int check = m_listctrlProcess.GetCheck(i);
		
		if (check)
		{
			THREAD_PARAM thread_param;
			thread_param.appId			= m_listctrlProcess.GetItemText(i, 1);
			thread_param.directive		= "watch";

			pThreadParamList->Add(thread_param);
		}
	}

	if(pThreadParamList->GetSize() > 0)
		AfxBeginThread(ThreadCallFunc, pThreadParamList);
	else
		delete pThreadParamList;

	EndWaitCursor(); // Remove the hourglass cursor.

	if(msg.GetLength() > 0) {
		SttMessageBox(msg, 2000);
	}
}

void CSTTDlg::OnBnClickedAutoUpdate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	return OnTrayUpdate();
}

void CSTTDlg::OnNMRclickListProcess(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	CMenu menu;
	menu.LoadMenu(IDR_POPUP_MENU);

	CMenu* sub_menu = menu.GetSubMenu(0);

	if(sub_menu)
	{
		CPoint point;
		GetCursorPos(&point);

		sub_menu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}
}

void CSTTDlg::OnProcessRegister()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
/*
#ifdef _COP_USE_PROPERTIES_
	ciString cfgFilename = ciEnv::newEnv("PROJECT_CODE");           
	cfgFilename += ".properties";  
#else
	ciString cfgFilename = "starter.cfg";
#endif
	CString errMsg;
	errMsg.Format("프로세스 등록이 허용되지 않습니다.\n%s 파일을 수정해주세요.", cfgFilename.c_str());
	SttMessageBox(errMsg, 3000);
	return;
	*/

	ProcessDlg dlg(ProcessDlg::AD_TYPE_ADD, this);

	if( dlg.DoModal() == IDOK )
	{ // no job
		InsertItem(dlg.m_info);
		// 도큐먼트에 추가
		STTDoc *pDoc = GetDocument();
		// add application info & send udp packet
		pDoc->AddApplicationInfo(dlg.m_info, true);
	}
}

void CSTTDlg::OnProcessModify()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.

	STTDoc* pDoc = GetDocument();

	if(pDoc == NULL)
		return;

	int index = m_listctrlProcess.GetSelectionMark();

	if(index < 0)
		return;

	CString status		= m_listctrlProcess.GetItemText(index, 2);
	CString app_id		= m_listctrlProcess.GetItemText(index, 1);

	if(status.CompareNoCase("Active") == 0)
	{
#ifdef AFX_TARG_KOR
		SttMessageBox("먼저 프로세스를 중지하여 주시기 바랍니다.", 2000);
#else
		SttMessageBox("First. Stop the process.", 2000);	
#endif
		return;
	} 
	/*else {
		if (app_id.Compare("AGENT") == 0) {
#ifdef _COP_USE_PROPERTIES_
			ciString cfgFilename = ciEnv::newEnv("PROJECT_CODE");           
			cfgFilename += ".properties";  
#else
			ciString cfgFilename = "starter.cfg";
#endif
			CString errMsg;
			errMsg.Format("프로세스 정보를 수정하더라도 %s는 수정되지 않습니다.", cfgFilename.c_str());
			SttMessageBox(errMsg, 2000);
		}		
	}*/

	ProcessDlg dlg(ProcessDlg::AD_TYPE_MODIFY, this, app_id);

	if( dlg.DoModal() == IDOK )
	{
		// set application info & send udp packet
		pDoc->SetApplicationInfo(dlg.m_info, true);
		UpdateProcess(dlg.m_info);
	}
}

void CSTTDlg::OnProcessDelete()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
/*
#ifdef _COP_USE_PROPERTIES_
	ciString cfgFilename = ciEnv::newEnv("PROJECT_CODE");           
	cfgFilename += ".properties";  
#else
	ciString cfgFilename = "starter.cfg";
#endif
	CString errMsg;
	errMsg.Format("프로세스 삭제가 허용되지 않습니다.\n%s 파일을 수정해주세요.", cfgFilename.c_str());
	SttMessageBox(errMsg, 2000);
	return;
	*/

	STTDoc* pDoc = GetDocument();

	if(pDoc == NULL)
		return;

	int index = m_listctrlProcess.GetSelectionMark();

	if(index < 0)
		return;

	CString app_id		= m_listctrlProcess.GetItemText(index, 1);

	ProcessDlg dlg(ProcessDlg::AD_TYPE_DELETE, this, app_id);

	if( dlg.DoModal() == IDOK )
	{
		// delete application info & send udp packet
		pDoc->DeleteApplicationInfo(app_id.GetBuffer(0), true);
		ReloadProcessInfo();
	}
}

void CSTTDlg::OnProcessDetailInfo()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.

	int index = m_listctrlProcess.GetSelectionMark();

	if(index < 0)
		return;

	CString app_id		= m_listctrlProcess.GetItemText(index, 1);

	ProcessDlg dlg(ProcessDlg::AD_TYPE_DETAIL_INFO, this, app_id);

	if( dlg.DoModal() == IDOK )
	{ // no job
	}
}

LRESULT CSTTDlg::OnRefresh(WPARAM wParam, LPARAM lParam)
{
	ReloadProcessInfo();
	return true;
}

BOOL CSTTDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	STTDoc* pDoc = GetDocument();
	if (pDoc) {		
		switch(pCopyDataStruct->dwData)
		{		
		case 0: // Inactive
			{
				CString appId = (LPCSTR)pCopyDataStruct->lpData;
				pDoc->SetApplicationInfo(appId, "Inactive");
				ReloadProcessInfo();
				break;
			}
		case 1: // Active
			{
				CString appId = (LPCSTR)pCopyDataStruct->lpData;
				ciTime current;
				pDoc->SetApplicationInfo(appId, "Active", current.getTimeString().c_str());
				ReloadProcessInfo();
				break;
			}
		case 100: // APP INFO REFRESH
			{					
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("Recv Data...[%s]", strData.GetBuffer(0)));
				ciStringTokenizer token(strData.GetBuffer(), "&", ciTrue /*NULLTOKEN*/);
				ciString directive = token.nextToken();
				APPLICATION_INFO app_info;
				if (directive == "DELETE") {
					app_info.appId = token.nextToken().c_str();
				} else {
					int size = token.countTokens();				
					if (size != 15) {
						utvERROR(("Invalid Data...[%s:%d]", strData.GetBuffer(0), size));
						break;
					}
					app_info.appId = token.nextToken().c_str();
					app_info.argument = token.nextToken().c_str();
					CString strBool = token.nextToken().c_str();
					app_info.autoStartUp = (strBool == "TRUE") ? 1:0;
					app_info.binaryName = token.nextToken().c_str();
					app_info.debug = token.nextToken().c_str();
					app_info.initSleepSecond = atoi(token.nextToken().c_str());
					strBool = token.nextToken().c_str();
					app_info.isPrimitive = (strBool == "TRUE") ? 1:0;
					strBool = token.nextToken().c_str();
					app_info.monitored = (strBool == "TRUE") ? 1:0;
					app_info.pid = atoi(token.nextToken().c_str());
					app_info.preRunningApp = token.nextToken().c_str();
					app_info.properties = token.nextToken().c_str();
					app_info.runningType = token.nextToken().c_str();
					strBool = token.nextToken().c_str();
					app_info.runTimeUpdate = (strBool == "TRUE") ? 1:0;
					app_info.startTime = token.nextToken().c_str();
					app_info.status = token.nextToken().c_str();
					app_info.adminState = true; // ON
				}

				if (directive == "ADD") {
					pDoc->AddApplicationInfo(app_info, true, false);
				} else if (directive == "SET") {
					pDoc->SetApplicationInfo(app_info, true, false);
				} else if (directive == "DELETE") {
					pDoc->DeleteApplicationInfo(app_info.appId.GetBuffer(0), true, false);
				} else {
					utvERROR(("Invalid Direction[%s]", directive.c_str()));
					break;
				}
				ReloadProcessInfo();
				break;
			}
		case 200: // Update FTP Info, Timer Info, ScreenShot FTP Info
			{
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("Recv Data...[%s]", strData.GetBuffer(0)));

				ciStringTokenizer token(strData.GetBuffer(0), "&", ciTrue /*NULLTOKEN*/);
				ciString entry = token.nextToken();
				
				if (entry == "UPDATE") {
					ciString directive = token.nextToken();
					if (directive == "DELETE") {
						pDoc->DeleteUpdateFtpInfo();
						break;
					}

					if (token.countTokens() != 3 && token.countTokens() != 4) {
						utvERROR(("Invalid Data...[%s]", strData.GetBuffer(0)));
						break;
					}
					ciString ipAddress = token.nextToken();
					ciString id = token.nextToken();
					ciString password = token.nextToken();
					ciUShort port = 21;
					if (token.hasMoreTokens()) {
						port = atoi(token.nextToken().c_str());
					}

					if (directive == "ADD") {
						pDoc->AddUpdateFtpInfo(ipAddress.c_str(), id.c_str(), password.c_str(), port);
					} else if (directive == "SET") {
						pDoc->SetUpdateFtpInfo(ipAddress.c_str(), id.c_str(), password.c_str(), port);
					} else {
						utvERROR(("Invalid Directive...[%s]", directive.c_str()));
					}
					break;
				} else if (entry == "TIMER") {
					if (token.countTokens() == 6) {
						ciString strTemp = token.nextToken();
						int agentAppMonitor = atoi(strTemp.c_str());
						strTemp = token.nextToken();
						int agentHeartbeat = atoi(strTemp.c_str());
						strTemp = token.nextToken();
						int agentHostMonitor = atoi(strTemp.c_str());
						strTemp = token.nextToken();
						int agentScreenShot = atoi(strTemp.c_str());
						strTemp = token.nextToken();
						int autoUpdate = atoi(strTemp.c_str());
						strTemp = token.nextToken();
						int starterAppMonitor = atoi(strTemp.c_str());
						pDoc->SetTimerPeriodInfo(agentAppMonitor, 
												agentHeartbeat, 
												agentHostMonitor,
												agentScreenShot, 
												autoUpdate, 
												starterAppMonitor);
					}
					break;
				} else if (entry == "SCREENSHOT") {
					ciString directive = token.nextToken();
					if (directive == "DELETE") {
						pDoc->DeleteScreenShotFtpInfo();
						break;
					}

					if (token.countTokens() != 3 && token.countTokens() != 4) {
						utvERROR(("Invalid Data...[%s]", strData.GetBuffer(0)));
						break;
					}
					ciString ipAddress = token.nextToken();
					ciString id = token.nextToken();
					ciString password = token.nextToken();
					ciUShort port = 21;
					if (token.hasMoreTokens()) {
						port = atoi(token.nextToken().c_str());
					}

					if (directive == "ADD") {
						pDoc->AddScreenShotFtpInfo(ipAddress.c_str(), id.c_str(), password.c_str(), port);
					} else if (directive == "SET") {
						pDoc->SetScreenShotFtpInfo(ipAddress.c_str(), id.c_str(), password.c_str(), port);
					} else {
						utvERROR(("Invalid Directive...[%s]", directive.c_str()));
					}
					break;
				} else {
					utvERROR(("Invalid Data[%s]", entry.c_str()));
				}
				break;
			}
		case 300 :  // SetProperty
			{
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("SetProperty...[%s]", strData));
				ciStringTokenizer token(strData, "=", ciTrue /*NULLTOKEN*/);
				
				ciString name;
				if(token.hasMoreTokens()) {
					name = token.nextToken();
				}else{
					utvERROR(("Invalid Data[%s]", strData.GetBuffer(0)));
					break;
				}
				ciString value = "";
				if(token.hasMoreTokens()) {
					value = token.nextToken();
				}
				//pDoc->SetProperty("ROOT", name.c_str(), value.c_str());
				pDoc->SetProperty(name.c_str(), value.c_str());
				break;
			}
		case 400 :  // DelProperty
			{
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("DelProperty...[%s]", strData.GetBuffer(0)));
				pDoc->DelProperty(strData);
				break;
			}
		case 500 :  // SetProperties
			{
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("SetProperties...[...]"));
				if(strData == "Lock") {
					pDoc->Lock();
				}else if(strData == "Reload") {
					pDoc->Reload();
				}else if(strData == "UnLock") {
					pDoc->UnLock();
				}	
				break;
			}
		case 600 :  // doAutoUpdate
			{
				utvDEBUG(("doAutoUpdate"));
				::AfxBeginThread((AFX_THREADPROC)doAutoUpdate, 0);
				break;
			}
		case 610 :  // SetAutoUpdateFlag
			{
				utvDEBUG(("SetAutoUpdateFlag"));
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				if(strData == "TRUE") {
					pDoc->SetAutoUpdateFlag(true);
				} else if(strData == "FALSE") {
					pDoc->SetAutoUpdateFlag(false);
				}
				utvDEBUG(("SetAutoUpdateFlag[%s]", strData));
				break;
			}
		case 700 : // stop process
			{
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("Window Message 700 : Stop process %s", strData));
				AppMonitor::getInstance()->stop(strData);
				break;
			}
		case 710 : // Dont start again
			{
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("Window Message 710 : Dont Start again %s", strData));
				AppMonitor::getInstance()->dontStartAgain(strData);
				break;
			}
		case 800 : // start process
			{
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				AppMonitor::getInstance()->start(strData);
				break;
			}
		case 801 : // set brw adminState
			{
				CString strData = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("Window Message 801 : SET BRW AdminState=%s", strData));
				AppMonitor::getInstance()->setBrwAdmin(atoi(strData));
				break;
			}
		case 900 : // Show Window
			{
				//HWND sttHWnd = ::FindWindow(NULL, STARTER_WINDOWSNAME);
				HWND sttHWnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
				::ShowWindow(sttHWnd, SW_SHOWNORMAL);
					
				CRect rcWnd;
				GetWindowRect(rcWnd);
				SetWindowPos(&wndTopMost, rcWnd.left, rcWnd.top, rcWnd.Width(), 
								rcWnd.Height(), SWP_NOOWNERZORDER); 

			}
		case 4002:
			{
				AppMonitor* monitor = AppMonitor::getInstance();
				if(monitor)
				{
					CString appId = (LPCSTR)pCopyDataStruct->lpData;
					utvDEBUG(("Window Message 4002 : App Start (%s)", appId));
					if( !monitor->start(appId) )
					{
						utvWARN(("App Start (%s) Fail !!!", appId));
					}
				}
			}
			break;
		case 4003:
			{
				AppMonitor* monitor = AppMonitor::getInstance();
				if(monitor)
				{
					CString appId = (LPCSTR)pCopyDataStruct->lpData;
					utvDEBUG(("Window Message 4003 : App Stop (%s)", appId));
					if( !monitor->stop(appId) )
					{
						utvWARN(("App Stop (%s) Fail !!!", appId));
					}
				}
			}
			break;
		default:
			break;
		}
	}
	utvDEBUG(("STT::OnCopyData() end"));

	BOOL retval = CDialog::OnCopyData(pWnd, pCopyDataStruct);

	utvDEBUG(("CDialog::OnCopyData() end"));
	return retval;
}

UINT CSTTDlg::doAutoUpdate(LPVOID pParam)
{
	//utvDEBUG(("Before GUARD CSTTDlg::OnCopyData()"));
	ciGuard aGuard(AutoUpdateDlg::m_sLock);
	//utvDEBUG(("After GUARD CSTTDlg::OnCopyData()"));

	UINT ret = 0;
	
	AutoUpdateDlg dlg;
	dlg.SetDocument(theApp.getDoc());
	//dlg.SetAutoUpdateFlag(TRUE); // FirmWareView 호출시 메시지창 띄움
	dlg.SetShowMessageBoxFlag(TRUE); // FirmWareView 호출시 메시지창 띄움

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{

	}
	else if (nResponse == IDCANCEL)
	{

	}
	return TRUE;
}

LONG CSTTDlg::TrayIconMessage(WPARAM wParam,LPARAM lParam)
{
	switch (lParam)
	{
	case WM_RBUTTONDOWN: //트레이아이콘을 한 번클릭시
		{
			//메뉴를 로드한다.
			CMenu menu, *pMenu;
			CPoint pt;

			menu.LoadMenu(IDR_MENU_TRAY);
			pMenu = menu.GetSubMenu(0); //첫번째 주메뉴를 호출한다.
			if (!m_hasUpdate) {
				pMenu->EnableMenuItem(ID_MENU_UPDATE, MF_GRAYED); // MF_ENABLED
			}
			
			// IGNORE_COP_DEBUG 환경변수를 체크해서, 체크 처리한다.
			const char*  ignoreDebugEnv = ciEnv::newEnv("IGNORE_COP_DEBUG");
			if(ignoreDebugEnv==0 || ignoreDebugEnv[0] != '1'){
				utvDEBUG(("IGNORE_COP_DEBUG==0"));
				pMenu->CheckMenuItem(ID__DEBUG, MF_CHECKED);
			}else{
				utvDEBUG(("IGNORE_COP_DEBUG==1"));
				pMenu->CheckMenuItem(ID__DEBUG, MF_UNCHECKED);
			}

			GetCursorPos(&pt);

			//트레이아이콘의 메뉴를 X,Y위치에 뛰운다.
			pMenu->TrackPopupMenu(TPM_RIGHTALIGN,pt.x,pt.y,this);
			break;
		}


		//트레이 아이콘을 더블클릭했을때 윈도우가 보여지게 한다.
	case WM_LBUTTONDBLCLK:
		{
			//ShowWindow(SW_SHOW); //윈도우를 보여준다.
			ShowWindow(SW_NORMAL);
			SetForegroundWindow();
			SetFocus();
			break;
		}
	}

	return 1;
}

LONG CSTTDlg::TrayIconUpdate(WPARAM wParam, LPARAM lParam)
{
	if(lParam == UPDATE_READY && !m_hasUpdate)
	{
		utvDEBUG(("업데이트준비 이벤트 수신"));
		// UPDATE플래그 설정, 아이콘 변경
		m_hasUpdate = true;

		NOTIFYICONDATA    nid;
		::ZeroMemory(&nid, sizeof(nid));
		nid.cbSize    = sizeof(nid);
		nid.uID        = IDI_TRAY;
		nid.hWnd    = m_hWnd;
		nid.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP; 
		nid.uCallbackMessage = UM_TRAYICON;
		nid.hIcon = AfxGetApp()->LoadIcon(IDI_TRAY_UPDATE);

		lstrcpy(nid.szTip, "업데이트가 존재합니다."); // 툴팁

		// 버튼 활성화
		m_btnAutoUpdate.EnableWindow(TRUE);

		::Shell_NotifyIcon(NIM_MODIFY, &nid);
	}
	else if(lParam == UPDATE_COMPLETE && m_hasUpdate)
	{
		utvDEBUG(("업데이트완료 이벤트 수신"));
		// UPDATE플래그 해제, 아이콘 원복
		m_hasUpdate = false;

		NOTIFYICONDATA    nid;
		::ZeroMemory(&nid, sizeof(nid));
		nid.cbSize    = sizeof(nid);
		nid.uID        = IDI_TRAY;
		nid.hWnd    = m_hWnd;
		nid.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP;
		nid.uCallbackMessage = UM_TRAYICON;
		nid.hIcon = AfxGetApp()->LoadIcon(IDI_TRAY);

		lstrcpy(nid.szTip, STARTER_WINDOWSNAME); // 툴팁

		// 버튼 비활성화
		m_btnAutoUpdate.EnableWindow(FALSE);

		::Shell_NotifyIcon(NIM_MODIFY, &nid);
	}
	return 1;
}

void CSTTDlg::OnTrayOpen()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	ShowWindow(SW_SHOW); //윈도우를 보여준다.
}

void CSTTDlg::OnTrayClose()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	PostQuitMessage(0); // 프로그램 종료
}

void CSTTDlg::OnTrayUpdate()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	AutoUpdateDlg dlg;
	dlg.SetDocument(m_pDocument);
	dlg.SetForcedUpdateFlag(true); // 프로세스들을 재기동한다.
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// OK 처리
	}
	else if (nResponse == IDCANCEL)
	{
		// CANCEL 처리
	}

	// 어플리케이션이 종료 됐을지도 모르므로 watch 실행

	AppMonitor::getInstance()->watchAll();
}

void CSTTDlg::OnTrayAbout()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	CAboutDlg dlg;

	if( dlg.DoModal() == IDOK )
	{
	}
}

UINT CSTTDlg::ThreadCallFunc(LPVOID pParam)
{
	THREAD_PARAM_LIST *pThreadParamList = (THREAD_PARAM_LIST*)pParam;

	int size = pThreadParamList->GetSize();

	for(int i=0; i<size; i++)
	{
		THREAD_PARAM& thread_param = pThreadParamList->GetAt(i);

		CString appId		= thread_param.appId;
		CString directive	= thread_param.directive;
	
		BOOL result = false;

		// 프로세스 작업

		AppMonitor* monitor = AppMonitor::getInstance();
		if (directive == "start") {
			result = monitor->start(appId);
		} else if (directive == "stop") {
			result = monitor->stop(appId);
		} else if (directive == "watch") {
			result = monitor->watch(appId);
		}
		
		if( !result )
		{
			CString msg;
			msg.Format("[ %s ] 의 %s 작업에 실패하였습니다.", appId, directive);

			//HWND sttHWnd = ::FindWindow(NULL, STARTER_WINDOWSNAME);
			HWND sttHWnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
			SttMessageBoxH(sttHWnd, msg, 2000);
		}

		::Sleep(1000);
	}

	delete pThreadParamList;

	return 0;
}

void CSTTDlg::OnDebugOnOff()
{
#ifdef _COP_MSC_
	utvDEBUG(("OnDebugOnOff"));
	const char*  ignoreDebugEnv = ciEnv::newEnv("IGNORE_COP_DEBUG");

	if(ignoreDebugEnv){
		utvDEBUG(("IGNORE_COP_DEBUG={%s}",ignoreDebugEnv));
	}else{
		utvDEBUG(("IGNORE_COP_DEBUG={%s}","NULL"));
	}


	if(ignoreDebugEnv==0 || ignoreDebugEnv[0] != '1'){
		scEnv::getInstance()->setSysEnv("IGNORE_COP_DEBUG","1");
		ciEnv::setEnv("IGNORE_COP_DEBUG","1");
		utvDEBUG(("IGNORE_COP_DEBUG=1"));
	}else{
		scEnv::getInstance()->setSysEnv("IGNORE_COP_DEBUG","0");
		ciEnv::setEnv("IGNORE_COP_DEBUG","0");
		utvDEBUG(("IGNORE_COP_DEBUG=0"));
	}
#else
	// NO IMPLEMENT
#endif
}

void CSTTDlg::OnUpdateDebugOnOff(CCmdUI *pCmdUI)
{
	/*
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	pCmdUI->Enable(TRUE);
	int nCheck = 1;
		// TODO: 여기에 명령 처리기 코드를 추가합니다.
#ifdef _COP_MSC_
	utvDEBUG(("OnUpdateDebugOnOff"));
	const char*  ignoreDebugEnv = ciEnv::newEnv("IGNORE_COP_DEBUG");
	if(ignoreDebugEnv==0 || ignoreDebugEnv[0] != '1'){
		nCheck = 0;
	}else{
		nCheck = 1;
	}
#else
	// NO IMPLEMENT
#endif
	utvDEBUG(("OnUpdateDebugOnOff %d", nCheck));
	pCmdUI->SetCheck(nCheck);
	*/
}

void CSTTDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == TIMER_ID_CONFIG_VARIABLES)
	{
		KillTimer(TIMER_ID_CONFIG_VARIABLES);
		if(CheckConfigVariables() == FALSE)
		{
			SetTimer(TIMER_ID_CONFIG_VARIABLES, TIMER_TIME_CONFIG_VARIABLES, NULL);
		}
	}
}

CString CSTTDlg::m_strCustomerId = "";
CString CSTTDlg::m_strScope = "";

bool CSTTDlg::CheckConfigVariables()
{
	// <------ update ConfigVariables
	CConfigVariables cv(m_strCustomerId, m_strScope);
	CConfigVariables::RETURN_TYPE ret_val = cv.Apply();
	if(ret_val == CConfigVariables::SUCCESS_EXIST_CHANGE)
	{
		utvDEBUG(("Exist to change from ConfigVariables!!!"));
		CMapStringToPtr* cv_map = cv.GetValueMapPtr();
		CString key;
		CV_ITEM* pItem;
		for(POSITION pos=cv_map->GetStartPosition(); pos != NULL; )
		{
			cv_map->GetNextAssoc(pos, key, (void*&)pItem);

			utvDEBUG(("Scope:%s,FN:%s,APP:%s,KEY:%s,OLD_VAL:%s,NEW_VAL:%s", 
				pItem->scope, pItem->fileName, pItem->appName, pItem->keyName, pItem->old_value, pItem->value));
		}
		utvDEBUG(("ubc must reboot-system!!!"));

		if(m_strScope == "Server" || m_strScope == "Manager" || m_strScope == "Studio")
		{
			int ret_val = ::MessageBox(NULL, "UBC System Configuration is changed.\r\n\r\nYou must reboot.\r\n\r\nReady to reboot?", "UBC Updater", MB_ICONWARNING | MB_YESNO);
			if(ret_val == IDYES)
				ShellExecute( NULL, "open", "rebootSystem.bat", NULL, NULL, SW_HIDE );
		}
		else
		{
			ShellExecute( NULL, "open", "rebootSystem.bat", NULL, NULL, SW_HIDE );
		}
		return TRUE;
	}
	if(ret_val == CConfigVariables::SUCCESS_NO_CHANGE)
	{
		utvDEBUG(("Nothing to change from ConfigVariables"));
		return TRUE;
	}
	else
	{
		utvDEBUG(("Fail to load from ConfigVariables !!! (%d)", ret_val));
		return FALSE;
	}
	// ------>
}
