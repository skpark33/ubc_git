#include "stdafx.h"
#include "FtpUtil.h"
#include "LogTrace.h"
#include "COP/ciStringTokenizer.h"

#if defined(_WIN32)
#   include <winsock2.h>
//  WinSock DLL을 사용할 버전
#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0
#   include <time.h>
#else
#   include <sys/socket.h>       /* for socket(), connect(), send(), and recv() */
#   include <arpa/inet.h>        /* for sockaddr_in and inet_addr() */
#endif

/////////////////////////////////////////////////////////////////////////
//Added by jwh184 2002-0305
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 기본 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
FtpUtil::FtpUtil()
: m_strAddress(_T(""))
, m_strAddress2(_T(""))
, m_strName(_T(""))
, m_strPassword(_T(""))
, m_port(21)
, m_pFTPClient(NULL)
, m_passiveFlag(false)
{
}
/////////////////////////////////////////////////////////////////////////

FtpUtil::FtpUtil(LPCSTR user, LPCSTR pass, LPCSTR address,  LPCSTR address2, unsigned short port)
{
	m_strAddress  = address;
	m_strAddress2  = address2;
	m_strName	  = user;
	m_strPassword = pass;
	m_port = port;

	m_pFTPClient = NULL;
	m_passiveFlag = false;
}
FtpUtil::~FtpUtil()
{
	if(!(m_pFTPClient == NULL))
	{
		delete m_pFTPClient;
		m_pFTPClient = NULL;
	}

	m_strPassword = "";
	m_strAddress  = "";
	m_strAddress2  = "";
	m_strName     = "";
	m_port = 0;
}

/////////////////////////////////////////////////////////////////////////
//Added by jwh184 2002-0305
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// FTP 연결을 위한 계정등 설정함수 \n
/// @param ((LPCSTR) user : (in) 계정 id
/// @param (LPCSTR) pass : (in) 계정 비밀번호
/// @param (LPCSTR) address : (in) ftp 주소
/// @param (LPCSTR) address2 : (in) 두번째 ftp 주소
/// @param (unsigned) short port : (in) ftp 포트 번호
/////////////////////////////////////////////////////////////////////////////////
void FtpUtil::SetAccount(LPCSTR user, LPCSTR pass, LPCSTR address,LPCSTR address2, unsigned short port)
{
	m_strAddress	= address;
	m_strAddress2	= address2;
	m_strName		= user;
	m_strPassword	= pass;
	m_port			= port;
}
/////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------
// FTP 에 접속한다
// CFTPConnection 과 CFTPFileFind 객체를 셋팅한다.
//-------------------------------------------------------------------------
BOOL FtpUtil::Connect(int order)
{
	CString address = m_strAddress;
	if(order==2){
		address = m_strAddress2;
	}

#ifdef _WIN32
    WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
    WSADATA       WsaData;            // receives data from WSAStartup

    // Load WinSock DLL
    if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {
        return FALSE;
    }
#endif

	// 이전 Connection 체크
	if(m_pFTPClient !=NULL)
	{
		m_pFTPClient->Logout();
		delete m_pFTPClient;
		m_pFTPClient = NULL;
	}

	//IP, 아이디, 패스워드 는 Default 값이 있으므로 값이 없을리 없다 
	if(m_strPassword == "" || address == "" || m_strName == "")
	{	
		//AfxMessageBox("IP, 아이디, 패스워드를 확인하여 주십시오.");
		utvERROR(("IP, 아이디, 패스워드를 확인하여 주십시오."));
		return FALSE;

	}else{
		// 타임아웃 10초, 세 번 재시도
		m_pFTPClient = new CFTPClient(nsSocket::CreateDefaultBlockingSocketInstance(), 10, 2048, 0, 2);
		// 접속정보 설정
		CLogonInfo loginInfo(tstring(address.GetBuffer(0)), m_port, 
			tstring(m_strName.GetBuffer(0)),tstring(m_strPassword.GetBuffer(0)), 
			tstring(m_strName.GetBuffer(0)));
		// 로그인
		if (!m_pFTPClient->Login(loginInfo)) {
			utvERROR(("FTP Login failed"));
			return FALSE;
		}
		m_pFTPClient->SetResumeMode(FALSE);
	}
	
	// 연결 성공 실패 체크
	if(!m_pFTPClient->IsConnected())
	{
		//AfxMessageBox("ftp서버에 접속할 수 없습니다.\n FTP 서버 접속 정보를 확인하여 주십시오.",MB_OK);
		utvERROR(("ftp서버에 접속할 수 없습니다. FTP 서버 접속 정보를 확인하여 주십시오."));
		delete m_pFTPClient;
		m_pFTPClient = NULL;
		return FALSE;
	}
	return TRUE;
}

//-------------------------------------------------------------------------
// FTP 에 접속을 끊는다
// CFTPConnection 과 CFTPFileFind 객체를 제거한다.
//-------------------------------------------------------------------------
void FtpUtil::Disconnect()
{
	// 접속을 끊고 객체 삭제 (connection 객체)
	if (m_pFTPClient) {
		m_pFTPClient->Logout();
		delete m_pFTPClient;
		m_pFTPClient = 0;
	}
#ifdef _WIN32
	WSACleanup();
#endif
}
//------------------------------------------------------------------------------
// 파일을 다운로드 한다. 
//------------------------------------------------------------------------------
BOOL FtpUtil::FileDownload(LPCSTR remoteFile, LPCSTR localFile, LPCSTR localDir)
{
	utvDEBUG(("FileDownload: PATH[%s] FILE[%s] DIR[%s]",
		remoteFile, localFile, localDir));
	tstring strPathname = remoteFile;
	tstring filename = localFile;
	/*
	if (string::npos != filename.rfind("/")) {
		int locate = filename.rfind("/");
		filename = filename.substr(locate+1, filename.size()-locate-1); 
	}*/
	//strFilename.Replace("\/", "\\");
	tstring strdirname  = localDir;
	
	// 로컬 디렉토리 생성 (없으면 만들어 질수 있게 함)
	CreateDirectory( strdirname.c_str(), NULL );
	BOOL ret = m_pFTPClient->DownloadFile(strPathname,
									filename, 
									CRepresentation(CType::Image()), 
									m_passiveFlag);

	DWORD errCode = GetLastError();
	utvDEBUG(("FileDownload: GET RESULT[%u]", errCode));

	return ret;
}

BOOL FtpUtil::FileUpload(LPCSTR localFile, LPCSTR remoteFile)
{
	utvDEBUG(("FileUpload: LOCAL[%s] REMOTE[%s]", localFile, remoteFile));

	BOOL ret = m_pFTPClient->UploadFile(localFile, 
		remoteFile, 
		false, 
		CRepresentation(CType::Image()), 
		m_passiveFlag);

	DWORD errCode = GetLastError();
	utvDEBUG(("FileUpload: GET RESULT[%u]", errCode));

	return ret;
}

void FtpUtil::Passive(BOOL passiveFlag) 
{
	m_passiveFlag = passiveFlag;
}

BOOL FtpUtil::ChangeWorkingDirectory(LPCSTR pathname)
{
	utvDEBUG(("ChangeWorkingDirectory[%s]", pathname));
	int result = m_pFTPClient->ChangeWorkingDirectory(pathname);
	if (result != FTP_OK) {
		ciStringTokenizer token(pathname, "/\\");
		ciBoolean retVal;
		do {
			retVal = _ChangeWorkingDirectory(token.nextToken().c_str());
		} while (token.hasMoreTokens() && retVal == ciTrue);
		if (retVal) return TRUE;
		ciERROR(("ChangeWorkingDirectory failed.."));
		return FALSE;
	}
	return TRUE;
}

BOOL FtpUtil::_ChangeWorkingDirectory(LPCSTR pathname)
{
	utvDEBUG(("_ChangeWorkingDirectory[%s]", pathname));

	int result = m_pFTPClient->MakeDirectory(pathname);
	if (result != FTP_OK) {
		utvDEBUG(("MakeDirectory[%s] failed"), pathname);
		//return FALSE;
	}
	result = m_pFTPClient->ChangeWorkingDirectory(pathname);
	if (result != FTP_OK) return FALSE;

	return TRUE;
}

BOOL FtpUtil::FileFind(LPCSTR srcname)
{
	if (FileSize(srcname) != -1) {
		return TRUE;
	}
	return FALSE;
}

long FtpUtil::FileSize(LPCSTR srcname)
{
	if (m_pFTPClient->IsConnected()) {
		long fileSize = 0;
		m_pFTPClient->RepresentationType(CType::Image());
		if (m_pFTPClient->FileSize(tstring(srcname), fileSize) == FTP_OK) 
			return fileSize;
	}
	return -1;
}

void FtpUtil::AttachObserver(CFTPClient::CNotification* pObserver)
{
	if (m_pFTPClient) {
		m_pFTPClient->AttachObserver(pObserver);
	}
}

void FtpUtil::DetachObserver(CFTPClient::CNotification* pObserver)
{
	if (m_pFTPClient) {
		m_pFTPClient->DetachObserver(pObserver);
	}
}
