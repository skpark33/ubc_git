#ifndef FTP_OBSERVER_H
#define FTP_OBSERVER_H

#pragma once

#include "libFTPClient/FTPClient.h"

/// @brief Shows communication between server and client
class FtpObserver : public nsFTP::CFTPClient::CNotification
{
public:
	FtpObserver();
	virtual ~FtpObserver();

	unsigned long GetBytesReceived();

protected:
	virtual void OnInternalError(const tstring& strErrorMsg, const tstring& strFileName, DWORD dwLineNr);
	virtual void OnSendCommand(const tstring& strCommand);
	virtual void OnResponse(const nsFTP::CReply& response);
	virtual void OnBytesReceived(const nsFTP::TByteVector& /*vBuffer*/, long /*lReceivedBytes*/);
	void WriteLine(const char* cszLine);

private:
	unsigned long m_bytesReceived;
};

#endif // FTP_OBSERVER_H
