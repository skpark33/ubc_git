// STTDoc.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "STT.h"
#include "STTDlg.h"
#include "STTDoc.h"
#include "LogTrace.h"
#include "COP/ciStringUtil.h"
#include "COP/ciStringTokenizer.h"
#include "COP/ciEnv.h"
#include "UBC/ubcIni.h"
#include "UpdateMonitor.h"

#define DEFAULT_SERVER_INI_PATH		_COP_CD("C:\\Project\\ubc\\config\\data")
#define DEFAULT_SETTOP_INI_PATH		_COP_CD("C:\\SQISoft\\UTV1.0\\execute\\data")

// STTDoc

IMPLEMENT_DYNCREATE(STTDoc, CDocument)

STTDoc::STTDoc()
:	m_strID(""), m_strPassword(""), m_strIpAddr(""), m_strIpAddr2(""),m_port(21), m_strVersion(""),
	m_autoUpdate(300), m_appMonitor(60),
	m_strIniPath(DEFAULT_SETTOP_INI_PATH), m_serverFlag(false),
	m_hostId("")
	,m_sHttpPort(8080)
{
}

BOOL STTDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

STTDoc::~STTDoc()
{
}


BEGIN_MESSAGE_MAP(STTDoc, CDocument)
END_MESSAGE_MAP()


// STTDoc 진단입니다.

#ifdef _DEBUG
void STTDoc::AssertValid() const
{
	CDocument::AssertValid();
}

#ifndef _WIN32_WCE
void STTDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif
#endif //_DEBUG

#ifndef _WIN32_WCE
// STTDoc serialization입니다.

void STTDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}
#endif


// STTDoc 명령입니다.

bool STTDoc::init()
{
	//_config.setEnvTranslateFlag(false); // 환경변수값을 변환하지 않는다.

	_initFTPInfo();
	_initTimerPeriod();
	_initHostInfo();

	return _initConfig();
}

void STTDoc::SetServerFlag(bool serverFlag)
{
	if (serverFlag) {
		m_strIniPath = DEFAULT_SERVER_INI_PATH;
	} else {
		m_strIniPath = DEFAULT_SETTOP_INI_PATH;
	}
	m_serverFlag = serverFlag;
}

bool STTDoc::GetServerFlag()
{
	return m_serverFlag;
}

void STTDoc::_initFTPInfo()
{
	ubcConfig aIni("UBCConnect", m_strIniPath);

	aIni.get("UBCCENTER","FTP_IP", m_strIpAddr);
	if(m_strIpAddr.IsEmpty()){
		utvERROR(("[UBCCENTER] FTP_IP get failed."));
		aIni.get("UBCCENTER","IP", m_strIpAddr, "211.232.57.202");
		m_strIpAddr2 = m_strIpAddr;
	}else{
		aIni.get("UBCCENTER","IP", m_strIpAddr2, "211.232.57.202");
	}
	utvDEBUG(("FIRST FTP IP=%s", m_strIpAddr));
	utvDEBUG(("SECOND FTP IP=%s", m_strIpAddr2));

	/*
	if (!aIni.get("UBCCENTER","IP", m_strIpAddr, "211.232.57.202")) {
			utvERROR(("[UBCCENTER]IP get failed."));
			m_strIpAddr = "211.232.57.202";
	}
	*/
	utvDEBUG((" - IP[%s]", m_strIpAddr));
/*
	if (!aIni.get("UBCCENTER","FTPID", m_strID, "cbu")) {
		utvERROR(("[UBCCENTER]FTPID get failed."));
		m_strID = "cbu";
	} 
	*/
	if (!aIni.get("UBCCENTER","FTPID", m_strID, "ent_center")) {
		utvERROR(("[UBCCENTER]FTPID get failed."));
		m_strID = "ent_center";
	} 

	utvDEBUG((" - USER[%s]", m_strID));

	if (!aIni.get("UBCCENTER","FTPPASSWD", m_strPassword, "rjtlrl2009")) {
		utvERROR(("[UBCCENTER]FTPPASSWD get failed."));
		m_strPassword = "rjtlrl2009";	
	} 
	//utvDEBUG((" - PASSWORD[%s]", m_strPassword));
	utvDEBUG((" - PASSWORD[****]"));

	m_port = 21;
	if (!aIni.get("UBCCENTER","FTPPORT", m_port)) {
		utvERROR(("[UBCCENTER]FTPPORT get failed."));
	} 
	if (m_port == 0) {
		utvERROR(("[UBCCENTER]PORT get failed."));
		m_port = 21;
	}
	utvDEBUG((" - PORT[%d]", m_port));

	//////////////////////////////////////////////////////////////////////
	//Added by jwh184 2002-03-06
	m_sHttpPort = 0;
	aIni.get("UBCCENTER","HTTPPORT", m_sHttpPort);
	if (m_sHttpPort == 0)
	{
		utvERROR(("[UBCCENTER]HTTPPORT get failed."));
		m_sHttpPort = 8080;
	}//if
	//////////////////////////////////////////////////////////////////////

	if (m_serverFlag) {
		if (!aIni.get("UBCCENTER","VERSION", m_strVersion)) {
			utvERROR(("[UBCCENTER]VERSION get failed."));
			m_strVersion = "";
		} 
	} else {
		if (!aIni.get("UBCCENTER","VERSION", m_strVersion, "UTV1.0")) {
			utvERROR(("[UBCCENTER]VERSION get failed."));
			m_strVersion = "UTV1.0";
		} 
	}
	utvDEBUG((" - VERSION[%s]", m_strVersion));
}

void STTDoc::_initTimerPeriod()
{
	ubcConfig aIni("UBCVariables", m_strIniPath);
	if (!aIni.get("ROOT","AutoUpdatePeriod", m_autoUpdate)) {
		utvERROR(("[ROOT]AutoUpdatePeriod get failed."));
		m_autoUpdate = 300;
	}
	if (m_autoUpdate < 60*60*3 ) m_autoUpdate =  60*60*3;
	utvDEBUG((" - AutoUpdatePeriod[%u]", m_autoUpdate));

	if (!aIni.get("ROOT","ApplicationMonitorPeriod", m_appMonitor)) {
		utvERROR(("[ROOT]ApplicationMonitorPeriod get failed."));
		m_appMonitor = 10;
	}
	if (m_appMonitor < 5) m_appMonitor = 5;
	utvDEBUG((" - ApplicationMonitorPeriod[%u]", m_appMonitor));
}

bool STTDoc::_initConfig()
{
	// starter.cfg

	ubcConfig aIni("UBCStarter", m_strIniPath);
	ciString strAppList;
	if (!aIni.get("ROOT",ubcConfig::getAppListName(), strAppList)) {
		utvERROR(("[ROOT]APP_LIST get failed."));
		return false;
	}

	ciStringTokenizer appToken(strAppList, ",");
	while (appToken.hasMoreTokens())
	{
		ciString procName = appToken.nextToken();
		utvDEBUG( ("[%s]", procName.c_str()));

		// Load Process Info
		_initLoadProcess(procName.c_str());
	}
	return true;
}

bool
STTDoc::_initLoadProcess(LPCSTR procName)
{
	utvDEBUG(("_initLoadProcess(%s)", procName));

	APPLICATION_INFO procInfo(procName);

	ubcConfig aIni("UBCStarter", m_strIniPath);
	/*
	ciBoolean monitored = ciFalse;
	if (!aIni.get(procName,"MONITORED", monitored)) 
	{
		utvERROR(("[%s]MONITORED get failed.", procName));
		return false;
	}
	if (!monitored) {
		utvDEBUG(("%s is not monitoring application", procName));
		return false;
	}*/
	aIni.get(procName,"MONITORED",		procInfo.monitored);

	aIni.get(procName,"BINARY_NAME",	procInfo.binaryName);
	ciString binName = ::GetBinaryName(procInfo.binaryName);
	ciString starterName = STARTER_PROCESSNAME;
	ciStringUtil::toUpper(binName);
	ciStringUtil::toUpper(starterName);
	if (binName == starterName) {
		utvDEBUG(("It's me. return - BINARY_NAME[%s]", STARTER_PROCESSNAME));
		return false;
	}

	aIni.get(procName,"ARGUMENT",		procInfo.argument);
	aIni.get(procName,"PROPERTIES",		procInfo.properties);
	aIni.get(procName,"DEBUG",			procInfo.debug);
	aIni.get(procName,"START_TIME",		procInfo.startTime );
	aIni.get(procName,"RUNNING_TYPE",	procInfo.runningType);
	aIni.get(procName,"AUTO_STARTUP",	procInfo.autoStartUp);
	aIni.get(procName,"RUNTIME_UPDATE",	procInfo.runTimeUpdate);	
	aIni.get(procName,"PRERUNNING_APP",	procInfo.preRunningApp);
	aIni.get(procName,"IS_PRIMITIVE",	procInfo.isPrimitive);	
	aIni.get(procName,"INIT_SLEEP_SECOND",procInfo.initSleepSecond);	
	aIni.get(procName,"PID",			procInfo.pid);
	aIni.get(procName,"STATUS",			procInfo.status);
	
	printf(" - APP_ID[%s]\n",			procInfo.appId);
	printf(" - BINARY_NAME[%s]\n",		procInfo.binaryName);
	printf(" - ARGUMENT[%s]\n",			procInfo.argument);
	printf(" - PROPERTIES[%s]\n",		procInfo.properties);
	printf(" - DEBUG[%s]\n",			procInfo.debug);
	printf(" - START_TIME[%s]\n",		procInfo.startTime);
	printf(" - RUNNING_TYPE[%s]\n",		procInfo.runningType);
	printf(" - AUTO_STARTUP[%s]\n",		procInfo.autoStartUp ? "TRUE" : "FALSE");
	printf(" - RUNTIME_UPDATE[%s]\n",	procInfo.runTimeUpdate ? "TRUE" : "FALSE");
	printf(" - ADMINSTATE[%d]\n",		procInfo.adminState);
	
	printf(" - PRERUNNING_APP[%s]\n",	procInfo.preRunningApp);
	printf(" - IS_PRIMITIVE[%s]\n",		procInfo.isPrimitive ? "TRUE":"FALSE");
	printf(" - MONITORED[%s]\n",		procInfo.monitored ? "TRUE":"FALSE");
	printf(" - INIT_SLEEP_SECOND[%d]\n",procInfo.initSleepSecond);
	
	printf(" - PID[%d]\n",				procInfo.pid);
	printf(" - STATUS[%s]\n",			procInfo.status);

	AddApplicationInfo(procInfo);

	return true;
}

void STTDoc::_initHostInfo()
{
	ciString hostId, macAddress, edition;
	scratchUtil* sUtil = scratchUtil::getInstance();
	if (!sUtil->readAuthFile(hostId, macAddress, edition)) {
		utvERROR(("scratchUtil::readAuthFile() failed."));
	} else {
		m_hostId = hostId.c_str();
	}	
}

bool STTDoc::ReloadApplicationInfo()
{
	CI_GUARD(_mutex, "ReloadApplicationInfo()");

	m_applicationList.RemoveAll();

	BeginWaitCursor();

	//CCIApplication cciApp;
	//bool ret_value = cciApp.GetAllApplicationsInfo(m_applicationList, m_domainMap);
	bool ret_value = false;

	EndWaitCursor();

	return ret_value;
}

bool	
STTDoc::GetTimerPeriodInfo( short& agentAppMonitor, 
						    short& agentHeartbeat, 
						    short& agentHostMonitor, 
						    short& agentScreenShot, 
						    short& autoUpdate, 
						    short& starterAppMonitor)
{
	CI_GUARD(_mutex, "GetTimerPeriodInfo()");

	agentAppMonitor=5; 
	agentHeartbeat=1; 
	agentHostMonitor=60; 
	agentScreenShot=300; 
	autoUpdate=60*60*3;
	starterAppMonitor=5;

	ubcConfig aIni("UBCVariables", m_strIniPath);
	//aIni.get("ROOT","AGENT_APP_MONITOR", agentAppMonitor);
	//if (agentAppMonitor < 5) agentAppMonitor = 5;

	//aIni.get("ROOT","AGENT_HEARTBEAT",agentHeartbeat);
	//if (agentHeartbeat < 1) agentHeartbeat = 1;

	aIni.get("ROOT","AgentConnectionPeriod",agentHostMonitor);
	if (agentHostMonitor < 60) agentHostMonitor = 60;

	aIni.get("ROOT","ScreenShotPeriod",agentScreenShot);
	if (agentScreenShot && agentScreenShot < 120) agentScreenShot = 120;

	aIni.get("ROOT","AutoUpdatePeriod",autoUpdate);
	if (autoUpdate < 60*60*3) autoUpdate = 60*60*3;

	aIni.get("ROOT","ApplicationMonitorPeriod",starterAppMonitor);
	if (starterAppMonitor < 5) starterAppMonitor = 5;

	return ciTrue;
}

bool	
STTDoc::SetTimerPeriodInfo( short agentAppMonitor, 
						    short agentHeartbeat, 
						    short agentHostMonitor, 
						    short agentScreenShot, 
						    short autoUpdate, 
						    short starterAppMonitor)
{
	CI_GUARD(_mutex, "SetTimerPeriodInfo()");

	// 0 이상의 입력 값만 세팅함

	ubcConfig aIni("UBCVariables", m_strIniPath);
/*
	if(agentAppMonitor>=0) {
		aIni.set("ROOT","AGENT_APP_MONITOR", agentAppMonitor);
	}
	if(agentHeartbeat>=0) {
		aIni.set("ROOT","AGENT_HEARTBEAT", agentHeartbeat);
	}*/
	if(agentHostMonitor>=0) {
		aIni.set("ROOT","AgentConnectionPeriod", agentHostMonitor);
	}
	if(agentScreenShot>=0) {
		aIni.set("ROOT","ScreenShotPeriod", agentScreenShot);
	}
	if(autoUpdate>=0) {
		aIni.set("ROOT","AutoUpdatePeriod", autoUpdate);
	}
	if(starterAppMonitor>=0) {
		aIni.set("ROOT","ApplicationMonitorPeriod", starterAppMonitor);
	}

	return ciTrue;
}

bool	
STTDoc::GetUBCCenterInfo(ciString& ip, ciUShort& port)
{
	CI_GUARD(_mutex, "GetUBCCenterInfo()");

	ubcConfig aIni("UBCConnect", m_strIniPath);
	if (!aIni.get("UBCCENTER","IP", ip)) {
		utvERROR(("[UBCCENTER]IP get failed."));
		return false;
	}
	utvDEBUG((" - IP[%s]", ip.c_str()));

	if (!aIni.get("UBCCENTER","PORT", port)) {
		utvERROR(("[UBCCENTER]FTPPORT get failed."));
		return false;
	} 
	utvDEBUG((" - PORT[%d]", port));

	return true;
}

bool	
STTDoc::GetUpdateFtpInfo(ciString& ip, ciString& user, ciString& password, ciUShort& port)
{
	CI_GUARD(_mutex, "GetUpdateFtpInfo()");

	ubcConfig aIni("UBCConnect", m_strIniPath);
	if (!aIni.get("UBCCENTER","IP", ip, "211.232.57.202")) {
		utvERROR(("[UBCCENTER]IP get failed."));
		ip = "211.232.57.202";
	}
	utvDEBUG((" - IP[%s]", ip.c_str()));
/*
	if (!aIni.get("UBCCENTER","FTPID", user, "cbu")) {
		utvERROR(("[UBCCENTER]FTPID get failed."));
		user = "cbu";
	} 
	*/
	if (!aIni.get("UBCCENTER","FTPID", user, "ent_center")) {
		utvERROR(("[UBCCENTER]FTPID get failed."));
		user = "ent_center";
	} 

	utvDEBUG((" - USER[%s]", user.c_str()));

	if (!aIni.get("UBCCENTER","FTPPASSWD", password, "rjtlrl2009")) {
		utvERROR(("[UBCCENTER]FTPPASSWD get failed."));
		password = "rjtlrl2009";	
	} 
	//utvDEBUG((" - PASSWORD[%s]", password.c_str()));
	utvDEBUG((" - PASSWORD[****]"));

	if (!aIni.get("UBCCENTER","FTPPORT", port)) {
		utvERROR(("[UBCCENTER]FTPPORT get failed."));
		port = 21;
	} 
	if (port == 0) {
		utvERROR(("[UBCCENTER]FTPPORT get failed."));
		port = 21;
	}
	utvDEBUG((" - PORT[%d]", port));

	return true;
}

bool	
STTDoc::SetUpdateFtpInfo(LPCSTR ip, LPCSTR user, LPCSTR password, ciUShort port)
{
	CI_GUARD(_mutex, "SetUpdateFtpInfo()");

	ubcConfig aIni("UBCConnect", m_strIniPath);

	aIni.set("UBCCENTER","IP",ip);
	aIni.set("UBCCENTER","FTPID",user);
	aIni.set("UBCCENTER","FTPPASSWD",password);
	aIni.set("UBCCENTER","FTPPORT",port);

	return ciTrue;
}

bool	
STTDoc::AddUpdateFtpInfo(LPCSTR ip, LPCSTR user, LPCSTR password, ciUShort port)
{
	CI_GUARD(_mutex, "AddUpdateFtpInfo()");

	ubcConfig aIni("UBCConnect", m_strIniPath);

	aIni.set("UBCCENTER","IP",ip);
	aIni.set("UBCCENTER","FTPID",user);
	aIni.set("UBCCENTER","FTPPASSWD",password);
	aIni.set("UBCCENTER","FTPPORT",port);

	return ciTrue;
}

bool
STTDoc::DeleteUpdateFtpInfo()
{
	utvERROR(("ini write error"));
	return ciFalse;
}

bool	
STTDoc::GetScreenShotFtpInfo(ciString& ip, ciString& user, ciString& password, ciUShort& port)
{
	//CI_GUARD(_mutex, "GetScreenShotFtpInfo()");

	// ScreenShot FTP 정보는 Agent Client 로부터 가져오며
	// ini 파일로 저장되지 않음
	ciERROR(("ScreenShot FTP information get failed."));

	return ciFalse;
}

bool	
STTDoc::SetScreenShotFtpInfo(LPCSTR ip, LPCSTR user, LPCSTR password, ciUShort port)
{
	//CI_GUARD(_mutex, "SetScreenShotFtpInfo()");

	// ScreenShot FTP 정보는 Agent Client 로부터 가져오며
	// ini 파일로 저장되지 않음
	ciERROR(("ScreenShot FTP information set failed."));

	return ciFalse;
}

bool	
STTDoc::AddScreenShotFtpInfo(LPCSTR ip, LPCSTR user, LPCSTR password, ciUShort port)
{
	//CI_GUARD(_mutex, "AddScreenShotFtpInfo()");

	// ScreenShot FTP 정보는 Agent Client 로부터 가져오며
	// ini 파일로 저장되지 않음
	ciERROR(("ScreenShot FTP information set failed."));

	return ciFalse;
}

bool
STTDoc::DeleteScreenShotFtpInfo()
{
	utvERROR(("ini write error"));
	return ciFalse;
}

bool STTDoc::AddApplicationInfo(APPLICATION_INFO& app_info, BOOL write, BOOL send_udp)
{
	CI_GUARD(_mutex, "AddApplicationInfo()");

	m_applicationList.Add(app_info);
	if (write) _WriteAddAppInfo(app_info);
	if (send_udp) SendApplicationInfo("ADD", app_info);

	return true;
}

bool STTDoc::GetApplicationInfo(LPCSTR lpszAppName, APPLICATION_INFO& app_info)
{
	CI_GUARD(_mutex, "GetApplicationInfo()");
	
	bool ret_value = false;
	int count = m_applicationList.GetSize();

	for(int i=0; i<count; i++)
	{
		APPLICATION_INFO& info = m_applicationList.GetAt(i);

		if(info.appId.CompareNoCase(lpszAppName)==0 )
		{
			app_info = info;
			ret_value = true;
			break;
		}
	}
	return ret_value;
}

bool STTDoc::GetApplicationsInfo(APPLICATION_LIST& app_list)
{
	CI_GUARD(_mutex, "GetApplicationsInfo()");
	
	app_list.RemoveAll();
	app_list.Copy(m_applicationList);

	return true;
}

bool STTDoc::GetApplicationNameList(CArray<CString, CString&>& appNameList)
{
	CI_GUARD(_mutex, "GetApplicationNameList()");

	bool ret_value = false;
	int count = m_applicationList.GetSize();

	for(int i=0; i<count; i++)
	{
		APPLICATION_INFO& info = m_applicationList.GetAt(i);	
		appNameList.Add(info.appId);
	}
	return ret_value;
}

bool STTDoc::SetApplicationInfo(APPLICATION_INFO& app_info, BOOL write, BOOL send_udp)
{
	CI_GUARD(_mutex, "SetApplicationInfo()");
	//utvDEBUG(("SetApplicationInfo(...)"));
	bool ret_value = false;
	int count = m_applicationList.GetSize();

	for(int i=0; i<count; i++)
	{
		APPLICATION_INFO& info = m_applicationList.GetAt(i);

		if(info.appId.CompareNoCase(app_info.appId)==0 )
		{
			//CString app_status = info.status;
			//CString start_time = info.startTime;

			info = app_info;

			//info.status = app_status;
			//info.startTime = start_time;

			if (write) _WriteSetAppInfo(app_info);
			// send to AGENT
			if (send_udp) SendApplicationInfo("SET", app_info);

			ret_value = true;

			break;
		}
	}
	return ret_value;
}

#include <ace/SOCK_Dgram.h>

bool STTDoc::SendApplicationInfo(LPCSTR directive, APPLICATION_INFO& app_info)
{
	utvDEBUG(("SendApplicationInfo(%s,%s)", directive, app_info.appId));

	ciString strTemp;
	ciString msgData = directive;
	msgData += "&";
	msgData += app_info.appId;

	CString strDir = directive;
	if (strDir.CompareNoCase("DELETE") != 0) {
		msgData += "&";
		msgData += app_info.argument;
		msgData += "&";
		msgData += app_info.autoStartUp ? "TRUE" : "FALSE";
		msgData += "&";
		msgData += app_info.binaryName;
		msgData += "&";
		msgData += app_info.debug;
		msgData += "&";
		ciStringUtil::itoa(app_info.initSleepSecond, strTemp);
		msgData += strTemp;
		msgData += "&";
		msgData += app_info.isPrimitive ? "TRUE" : "FALSE";
		msgData += "&";
		msgData += app_info.monitored ? "TRUE" : "FALSE";
		msgData += "&";
		ciStringUtil::itoa(app_info.pid, strTemp);
		msgData += strTemp;
		msgData += "&";
		msgData += app_info.preRunningApp;
		msgData += "&";
		msgData += app_info.properties;
		msgData += "&";
		msgData += app_info.runningType;
		msgData += "&";
		msgData += app_info.runTimeUpdate ? "TRUE" : "FALSE";
		msgData += "&";
		msgData += app_info.startTime;
		msgData += "&";
		msgData += app_info.status;
	}

	ACE_INET_Addr serverAddr;
	serverAddr.set(50020, "localhost");

    ACE_INET_Addr localAddr;
    ACE_SOCK_Dgram udp(localAddr);
	if (udp.send(msgData.c_str(), msgData.size(), serverAddr) == -1)
    {
		utvERROR(("UDP SEND ERROR. [%s]", msgData.c_str()));
		return false;
    }
	udp.close();
	return true;
}

bool STTDoc::SetApplicationInfo(LPCSTR lpszAppName, LPCSTR lpszAppStatus, LPCSTR lpszStartTime)
{
	CI_GUARD(_mutex, "SetApplicationInfo()");
	
	bool ret_value = false;
	int count = m_applicationList.GetSize();

	for(int i=0; i<count; i++)
	{
		APPLICATION_INFO& info = m_applicationList.GetAt(i);

		if(info.appId.CompareNoCase(lpszAppName)==0 )
		{
			info.status = lpszAppStatus;
			if(lpszStartTime)
				info.startTime = lpszStartTime;
			_WriteSetAppInfo(lpszAppName, lpszAppStatus, lpszStartTime);
			ret_value = true;

			break;
		}
	}
	return ret_value;
}

void STTDoc::SetProcessStatus(LPCSTR lpszAppName, LPCSTR lpszStatus)
{
	CI_GUARD(_mutex, "SetProcessStatus()");

	int count = m_applicationList.GetSize();

	for(int i=0; i<count; i++)
	{
		APPLICATION_INFO& info = m_applicationList.GetAt(i);

		if( info.appId.CompareNoCase(lpszAppName)==0 )
		{
			info.status = lpszStatus;
			_WriteSetAppInfo(lpszAppName, lpszStatus);

			break;
		}
	}
}

bool STTDoc::DeleteApplicationInfo(LPCSTR lpszAppName, BOOL write, BOOL send_udp)
{
	CI_GUARD(_mutex, "DeleteApplicationInfo()");

	bool ret_value = false;
	int count = m_applicationList.GetSize();

	int i=0;
	for(i=0; i<count; i++)
	{
		APPLICATION_INFO& info = m_applicationList.GetAt(i);

		if( info.appId.CompareNoCase(lpszAppName)==0 )
		{		
			ret_value = true;
			break;
		}
	}
	if (ret_value) {
		m_applicationList.RemoveAt(i);
		if (write) _WriteDeleteAppInfo(lpszAppName);
		if (send_udp) {
			APPLICATION_INFO aInfo;
			aInfo.appId = lpszAppName;
			SendApplicationInfo("DELETE", aInfo);			
		}
	}
	return ret_value;
}

bool STTDoc::_WriteSetAppInfo(APPLICATION_INFO& app_info)
{
	ubcConfig aIni("UBCStarter", m_strIniPath);	
	ciString prefix = app_info.appId;
	
	ciString arg = app_info.argument;
	ciStringUtil::rightTrim(arg);

	utvDEBUG(("DEBUG=%s", app_info.debug));

	aIni.set(prefix.c_str(),"BINARY_NAME",		app_info.binaryName);
	aIni.set(prefix.c_str(),"ARGUMENT",			arg.c_str());
	aIni.set(prefix.c_str(),"PROPERTIES",		app_info.properties);
	aIni.set(prefix.c_str(),"DEBUG",			app_info.debug);
	aIni.set(prefix.c_str(),"STATUS",			app_info.status);
	aIni.set(prefix.c_str(),"START_TIME",		app_info.startTime);
	aIni.set(prefix.c_str(),"RUNNING_TYPE",		app_info.runningType);
	aIni.set(prefix.c_str(),"AUTO_STARTUP",		app_info.autoStartUp);
	aIni.set(prefix.c_str(),"RUNTIME_UPDATE",	app_info.runTimeUpdate);
	aIni.set(prefix.c_str(),"PRERUNNING_APP",	app_info.preRunningApp);
	aIni.set(prefix.c_str(),"IS_PRIMITIVE",		app_info.isPrimitive);
	aIni.set(prefix.c_str(),"MONITORED",		app_info.monitored);
	aIni.set(prefix.c_str(),"INIT_SLEEP_SECOND",app_info.initSleepSecond);
	aIni.set(prefix.c_str(),"PID",				app_info.pid);

	/*
	ciBoolean write_flag = ciFalse;
	{
		ciString name=(prefix+".BINARY_NAME").c_str();
		ciString newVal = app_info.binaryName;
		ciString oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal.c_str());	
		}
	}
	{
		ciString name=(prefix+".ARGUMENT").c_str();
		ciString newVal = app_info.argument;
		ciString oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal.c_str());	
		}
	}
	{
		ciString name=(prefix+".PROPERTIES").c_str();
		ciString newVal = app_info.properties;
		ciString oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal.c_str());	
		}
	}
	{
		ciString name=(prefix+".DEBUG").c_str();
		ciString newVal = app_info.debug;
		ciString oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal.c_str());	
		}
	}
	
	{
		ciString name=(prefix+".STATUS").c_str();
		ciString newVal = app_info.status;
		ciString oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			//write_flag=ciTrue;
			config->set(name.c_str(), newVal.c_str());	
		}
	}
	{
		ciString name=(prefix+".START_TIME").c_str();
		ciString newVal = app_info.startTime;
		ciString oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			//write_flag=ciTrue;
			config->set(name.c_str(), newVal.c_str());	
		}
	}
	
	{
		ciString name=(prefix+".RUNNING_TYPE").c_str();
		ciString newVal = app_info.runningType;
		ciString oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal.c_str());	
		}
	}
	{
		ciString name=(prefix+".AUTO_STARTUP").c_str();
		ciBoolean newVal = app_info.autoStartUp;
		ciBoolean oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal);	
		}
	}
	{
		ciString name=(prefix+".RUNTIME_UPDATE").c_str();
		ciBoolean newVal = app_info.runTimeUpdate;
		ciBoolean oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal);	
		}
	}
	{
		ciString name=(prefix+".PRERUNNING_APP").c_str();
		ciString newVal = app_info.preRunningApp;
		ciString oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal.c_str());	
		}
	}
	{
		ciString name=(prefix+".IS_PRIMITIVE").c_str();
		ciBoolean newVal = app_info.isPrimitive;
		ciBoolean oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal);	
		}
	}
	{
		ciString name=(prefix+".MONITORED").c_str();
		ciBoolean newVal = app_info.monitored;
		ciBoolean oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal);	
		}
	}
	{
		ciString name=(prefix+".INIT_SLEEP_SECOND").c_str();
		ciShort newVal = app_info.initSleepSecond;
		ciShort oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			write_flag=ciTrue;
			config->set(name.c_str(), newVal);	
		}
	}
	{
		ciString name=(prefix+".PID").c_str();
		ciULong newVal = app_info.pid;
		ciULong oldVal; config->get(name.c_str(), oldVal);
		if(oldVal!=newVal){
			//write_flag=ciTrue;
			config->set(name.c_str(), newVal);	
		}
	}
	if(write_flag) {
		utvDEBUG(("write file!!!"));
		if (!config->write()) {
			utvERROR(("xconfig write error"));
			return ciFalse;
		}
	}
	*/
	return ciTrue;
}

bool STTDoc::_WriteSetAppInfo(LPCSTR lpszAppName, 
								   LPCSTR lpszAppStatus, 
								   LPCSTR lpszStartTime)
{
	ubcConfig aIni("UBCStarter", m_strIniPath);	
	ciString prefix = lpszAppName;

	aIni.set(prefix.c_str(), "STATUS", lpszAppStatus);
	if (!lpszStartTime || strlen(lpszStartTime)) {
		aIni.set(prefix.c_str(), "START_TIME", lpszStartTime);
	}

	/* skpark 20081103 */
	/* Don't write status info,  we don't need to wirte this */
	/*
	if (!config->write()) {
		utvERROR(("xconfig write error"));
		return ciFalse;
	}
	*/
	return ciTrue;
}

bool STTDoc::_WriteAddAppInfo(APPLICATION_INFO& app_info) 
{
	ubcConfig aIni("UBCStarter", m_strIniPath);
	
	ciString appList;
	aIni.get("ROOT", ubcConfig::getAppListName(), appList);
	if (appList.empty()) {
		appList = app_info.appId;
	} else {
		ciStringTokenizer token(appList.c_str(), ",");
		while(token.hasMoreTokens()) {
			if (token.nextToken() == (const char*)app_info.appId) {
				utvERROR(("%s is already exist.", app_info.appId));
				return false;
			}
		}
		appList += ",";
		appList += app_info.appId;
	}
	aIni.set("ROOT", ubcConfig::getAppListName(), appList.c_str());

	ciString prefix = app_info.appId;

	ciString arg = app_info.argument;
	ciStringUtil::rightTrim(arg);

	utvDEBUG(("DEBUG=%s", app_info.debug));

	aIni.set(prefix.c_str(),"BINARY_NAME",		app_info.binaryName);
	aIni.set(prefix.c_str(),"ARGUMENT",			arg.c_str());
	aIni.set(prefix.c_str(),"PROPERTIES",		app_info.properties);
	aIni.set(prefix.c_str(),"DEBUG",			app_info.debug);
	aIni.set(prefix.c_str(),"STATUS",			app_info.status);
	aIni.set(prefix.c_str(),"START_TIME",		app_info.startTime);
	aIni.set(prefix.c_str(),"RUNNING_TYPE",		app_info.runningType);
	aIni.set(prefix.c_str(),"AUTO_STARTUP",		app_info.autoStartUp);
	aIni.set(prefix.c_str(),"RUNTIME_UPDATE",	app_info.runTimeUpdate);
	aIni.set(prefix.c_str(),"PRERUNNING_APP",	app_info.preRunningApp);
	aIni.set(prefix.c_str(),"IS_PRIMITIVE",		app_info.isPrimitive);
	aIni.set(prefix.c_str(),"MONITORED",		app_info.monitored);
	aIni.set(prefix.c_str(),"INIT_SLEEP_SECOND",app_info.initSleepSecond);
	aIni.set(prefix.c_str(),"PID",				app_info.pid);
	
	return ciTrue;
}

bool STTDoc::_WriteDeleteAppInfo(LPCSTR appName)
{
	ubcConfig aIni("UBCStarter", m_strIniPath);
	ciString appList;
	if (!aIni.get("ROOT", ubcConfig::getAppListName(), appList) || 
		appList.empty())
	{
		utvERROR(("[ROOT]APP_LIST %s is not found.", appName));
		return false;
	}

	ciString newAppList = "";
	ciStringTokenizer token(appList, ",");
	while(token.hasMoreTokens()) {
		ciString aAppName = token.nextToken();
		if (aAppName == appName) continue;
		newAppList += aAppName;
		newAppList += ",";
	}
	if (!newAppList.empty()) 
		newAppList = newAppList.substr(0, newAppList.size()-1);
	aIni.set("ROOT", ubcConfig::getAppListName(), newAppList.c_str());

	return true;
}

bool	
STTDoc::SetProperty(LPCSTR name, LPCSTR value)
{
	utvDEBUG(("SetProperty(%s=%s)", name, value));
	CI_GUARD(_mutex, "SetProperty()");

	if(!ubcConfig::lvc_setProperty(name, value)) {
		utvERROR(("set(%s=%s) error", name, value));
		return ciFalse;
	}
	utvDEBUG(("SetProperty(%s=%s) succeed",name, value));

	_updateAppInfo(name,value);
	return ciTrue;
}

bool	
STTDoc::DelProperty(LPCSTR name)
{
	utvDEBUG(("DelProperty(%s)", name));
	CI_GUARD(_mutex, "DelProperty()");

	_deleteAppInfo(name);

	ubcConfig aIni("UBCStarter", m_strIniPath);
	ciString appList;
	if (!aIni.get("ROOT", ubcConfig::getAppListName(), appList) || 
		appList.empty())
	{
		ciERROR(("[ROOT]APP_LIST %s is not found.", name));
		return false;
	}

	ciString newAppList = "";
	ciStringTokenizer token(appList, ",");
	while(token.hasMoreTokens()) {
		ciString appName = token.nextToken();
		if (appName == name) continue;
		newAppList += appName;
		newAppList += ",";
	}
	if (!newAppList.empty()) 
		newAppList = newAppList.substr(0, newAppList.size()-1);
	aIni.set("ROOT", ubcConfig::getAppListName(), newAppList.c_str());

	utvDEBUG(("DelProperty(%s) succeed", name));
	return ciTrue;
}

bool
STTDoc::_updateAppInfo(LPCSTR name, LPCSTR value)
{
	utvDEBUG(("_updateAppInfo(%s,%s)", name,value));

	ciStringTokenizer aTokenizer(name,".");
	if(aTokenizer.countTokens() < 3) {
		return ciTrue;
	}

	aTokenizer.nextToken();
	ciString buf = aTokenizer.nextToken();
	ciString appId = buf.substr(4);
	ciString attr = aTokenizer.nextToken();
	
	utvDEBUG(("_updateAppInfo(%s,%s,%s)", appId.c_str(), attr.c_str(),value));
		
	bool ret_value = false;
	int count = m_applicationList.GetSize();

	int i=0;
	for(i=0; i<count; i++)
	{
		APPLICATION_INFO& info = m_applicationList.GetAt(i);
		if( info.appId.CompareNoCase(appId.c_str())==0 )
		{		
			return _updateInfoValue(info,attr,value);
		}
	}
	utvDEBUG(("_updateAppInfo(%s not found)", appId.c_str()));
	
	APPLICATION_INFO info(appId.c_str());
	if(_updateInfoValue(info,attr,value)) {
		m_applicationList.Add(info);
		return ciTrue;
	}

	return ciFalse;		
}

bool
STTDoc::_deleteAppInfo(LPCSTR name)
{
	utvDEBUG(("_deleteAppInfo(%s)", name));

	if(ciStringUtil::simpleMatch(name,"ROOT.APP_LIST.*")) {
		
		ciStringTokenizer aTokenizer(name,".");
		if(aTokenizer.countTokens() < 3) {
			return ciTrue;
		}

		aTokenizer.nextToken();
		aTokenizer.nextToken();
		ciString appId = aTokenizer.nextToken();
		
		bool ret_value = false;
		int count = m_applicationList.GetSize();

		int i=0;
		for(i=0; i<count; i++)
		{
			APPLICATION_INFO& info = m_applicationList.GetAt(i);

			if( info.appId.CompareNoCase(appId.c_str())==0 )
			{		
				ret_value = true;
				break;
			}
		}
		if (ret_value) {
			m_applicationList.RemoveAt(i);
		}
		return ret_value;
	}

	if(!ciStringUtil::simpleMatch(name,"STARTER.APP_*")) {
		return ciTrue;
	}

	ciStringTokenizer aTokenizer(name,".");
	if(aTokenizer.countTokens() < 3) {
		return ciTrue;
	}

	aTokenizer.nextToken();
	ciString buf = aTokenizer.nextToken();
	ciString appId = buf.substr(4);
	ciString attr = aTokenizer.nextToken();
	
	utvDEBUG(("_deleteAppInfo(%s,%s)", appId.c_str(), attr.c_str()));
		
	bool ret_value = false;
	int count = m_applicationList.GetSize();

	int i=0;
	for(i=0; i<count; i++)
	{
		APPLICATION_INFO& info = m_applicationList.GetAt(i);
		if( info.appId.CompareNoCase(appId.c_str())==0 )
		{		
			return _updateInfoValue(info,attr,"");	
		}
	}
	utvDEBUG(("_deleteAppInfo(%s not found)", appId.c_str()));
	return ciFalse;		
}

bool
STTDoc::_updateInfoValue(APPLICATION_INFO& info, ciString& attr, const char* value)
{
	utvDEBUG(("_updateInfoValue(%s,%s)", attr.c_str(),value));

	if(attr=="APP_ID")				{info.appId=value; return ciTrue;}
	if(attr=="BINARY_NAME")			{info.binaryName=value; return ciTrue;}
	if(attr=="ARGUMENT")			{info.argument=value; return ciTrue;}
	if(attr=="PROPERTIES")			{info.properties=value; return ciTrue;}
	if(attr=="DEBUG")				{info.debug=value; return ciTrue;}
	if(attr=="START_TIME")			{info.startTime=value; return ciTrue;}
	if(attr=="RUNNING_TYPE")		{info.runningType=value; return ciTrue;}
	if(attr=="AUTO_STARTUP")		{info.autoStartUp=ciStringUtil::atob(value); return ciTrue;}
	if(attr=="RUNTIME_UPDATE")		{info.runTimeUpdate=ciStringUtil::atob(value); return ciTrue;}
	if(attr=="ADMINSTATE")			{info.adminState=atoi(value); return ciTrue;}
	
	if(attr=="PRERUNNING_APP")		{info.preRunningApp=value; return ciTrue;}
	if(attr=="IS_PRIMITIVE")		{info.isPrimitive=ciStringUtil::atob(value); return ciTrue;}
	if(attr=="MONITORED")			{info.monitored=ciStringUtil::atob(value); return ciTrue;}
	if(attr=="INIT_SLEEP_SECOND")	{info.initSleepSecond=atoi(value); return ciTrue;}
	
	if(attr=="PID")					{info.pid=atol(value); return ciTrue;}
	if(attr=="STATUS")				{info.status=value; return ciTrue;}

	utvERROR(("_updateInfoValue(%s,%s) failed", attr.c_str(),value));
	return ciFalse;
}

// UBCVariables.ini 파일에서 AutoUpdateFlag 값을 가져온다.
bool	
STTDoc::GetAutoUpdateFlag()
{
	bool autoUpdateFlag = true; // Default is TRUE
	ubcConfig aIni("UBCVariables", m_strIniPath);
	if (!aIni.get("ROOT","AutoUpdateFlag", autoUpdateFlag)) 
	{
		utvWARN(("[ROOT]AutoUpdateFlag not found."));
	}
	utvDEBUG(("[ROOT]AutoUpdateFlag is %s", autoUpdateFlag ? "TRUE":"FALSE"));
	return autoUpdateFlag;
}

// UBCVariables.ini 파일에 AutoUpdateFlag 값을 설정한다.
// UpdateMonitor 타이머를 기동/중지 시킨다.
bool	
STTDoc::SetAutoUpdateFlag(bool updateFlag)
{
	// 주석처리: ini 파일쓰기는 Agent 에서 한다.
	//bool autoUpdateFlag = updateFlag;
	//ubcConfig aIni("UBCVariables", m_strIniPath);
	//if (!aIni.set("ROOT","AutoUpdateFlag", autoUpdateFlag))
	//{
	//	utvERROR(("[ROOT]AutoUpdateFlag Set failed."));
	//	return ciFalse;
	//}
	UpdateMonitor* updator = UpdateMonitor::getInstance();
	if (updateFlag) {
		updator->initTimer("AutoUpdate", m_autoUpdate);
		updator->SetDocument(this);
		updator->startTimer();
	} else {
		updator->stopTimer();
	}
	//utvDEBUG(("[ROOT]AutoUpdateFlag is %s", autoUpdateFlag ? "TRUE":"FALSE"));
	return ciTrue;
}

void
STTDoc::Lock()
{
	utvDEBUG(("Lock(...)"));
	_mutex.lock();
}
void	
STTDoc::UnLock()
{
	utvDEBUG(("UnLock(...)"));
	_mutex.unlock();
}
bool	
STTDoc::Reload()
{
	utvDEBUG(("Reload(...)"));
	m_applicationList.RemoveAll();
	return _initConfig();
}
