// ProcessDlg.cpp : 구현 파일입니다.
//

#include "StdAfx.h"
#include "STT.h"
#include "STTDlg.h"
#include "ProcessDlg.h"
#include "COP/ciStringUtil.h"
#include "MsgBox.h"


// ProcessDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(ProcessDlg, CDialog)

ProcessDlg::ProcessDlg(AD_TYPE type, CSTTDlg* sttDlg, LPCSTR lpszAppName, CWnd* pParent /*=NULL*/)
	: CDialog(ProcessDlg::IDD, pParent)
	, m_sttDlg(sttDlg)
	, m_type (type)
	, m_strAppName (lpszAppName)
{

}

ProcessDlg::~ProcessDlg()
{
}

void ProcessDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_APP_ID, m_appId);
	DDX_Control(pDX, IDC_EDIT_PROCESS, m_binaryName);
	DDX_Control(pDX, IDC_EDIT_PARAM, m_argument);
	DDX_Control(pDX, IDC_EDIT_PROP, m_properties);
	DDX_Control(pDX, IDC_EDIT_DEBUG, m_debug);
	DDX_Control(pDX, IDC_EDIT_STARTTIME, m_startTime);
	DDX_Control(pDX, IDC_EDIT_PID, m_pid);
	DDX_Control(pDX, IDC_COMBO_RUNNINGTYPE, m_cbxRunningType);
	DDX_Control(pDX, IDC_COMBO_AUTOSTART, m_cbxAutoStart);
	DDX_Control(pDX, IDC_COMBO_RUNTIMEUPDATE, m_cbxRuntimeUpdate);
	DDX_Control(pDX, IDC_COMBO_PRERUNNINGAPP, m_cbxPreRunningApp);
	DDX_Control(pDX, IDC_COMBO_ISPRIMITIVE, m_cbxIsPrimitive);
	DDX_Control(pDX, IDC_COMBO_MONITORED, m_cbxMonitored);
	DDX_Control(pDX, IDC_COMBO_ADMINSTATE, m_cbxAdminState);
	DDX_Control(pDX, IDC_EDIT_INITSLEEPSECOND, m_initSleepSecond);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(ProcessDlg, CDialog)
END_MESSAGE_MAP()


// ProcessDlg 메시지 처리기입니다.

BOOL ProcessDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	InitComboBox();

	m_btnCancel.LoadBitmap(IDB_BTN_CANCEL, BTN_TRANSPARENT_COLOR);

	switch(m_type)
	{
	case AD_TYPE_ADD:
#ifdef AFX_TARG_KOR
		SetWindowText("프로세스 추가");
#else
		SetWindowText("NEW PROCESS");
#endif

		m_btnOK.LoadBitmap(IDB_BTN_REGISTRATION, BTN_TRANSPARENT_COLOR);
		m_startTime.SetReadOnly();
		m_pid.SetReadOnly();
		m_cbxAdminState.EnableWindow(FALSE);

		break;

	case AD_TYPE_MODIFY:
#ifdef AFX_TARG_KOR
		SetWindowText("프로세스 수정");
#else
		SetWindowText("MODIFY PROCESS");
#endif

		m_btnOK.LoadBitmap(IDB_BTN_MODIFY, BTN_TRANSPARENT_COLOR);

		LoadApplicationInfo(m_strAppName);
		m_appId.SetReadOnly();
		m_startTime.SetReadOnly();
		m_pid.SetReadOnly();

		break;

	case AD_TYPE_DELETE:
#ifdef AFX_TARG_KOR
		SetWindowText("프로세스 삭제");
#else
		SetWindowText("DELETE PROCESS");
#endif

		m_btnOK.LoadBitmap(IDB_BTN_DELETE, BTN_TRANSPARENT_COLOR);

		LoadApplicationInfo(m_strAppName);

		m_appId.SetReadOnly();
		m_binaryName.SetReadOnly();
		m_argument.SetReadOnly();
		m_properties.SetReadOnly();
		m_debug.SetReadOnly();
		m_startTime.SetReadOnly();
		m_pid.SetReadOnly();
		m_cbxRunningType.EnableWindow(FALSE);
		m_cbxAutoStart.EnableWindow(FALSE);
		m_cbxRuntimeUpdate.EnableWindow(FALSE);
		m_cbxPreRunningApp.EnableWindow(FALSE);
		m_cbxIsPrimitive.EnableWindow(FALSE);
		m_cbxMonitored.EnableWindow(FALSE);
		m_initSleepSecond.SetReadOnly();
		m_cbxAdminState.EnableWindow(FALSE);

		break;

	case AD_TYPE_DETAIL_INFO:
#ifdef AFX_TARG_KOR
		SetWindowText("프로세스 상세 정보");
#else
		SetWindowText("PROCESS DETAIL INFOMATION");
#endif

		m_btnOK.LoadBitmap(IDB_BTN_CLOSE, BTN_TRANSPARENT_COLOR);

		LoadApplicationInfo(m_strAppName);

		m_appId.SetReadOnly();
		m_binaryName.SetReadOnly();
		m_argument.SetReadOnly();
		m_properties.SetReadOnly();
		m_debug.SetReadOnly();
		m_startTime.SetReadOnly();
		m_pid.SetReadOnly();
		m_cbxRunningType.EnableWindow(FALSE);
		m_cbxAutoStart.EnableWindow(FALSE);
		m_cbxRuntimeUpdate.EnableWindow(FALSE);
		m_cbxPreRunningApp.EnableWindow(FALSE);
		m_cbxIsPrimitive.EnableWindow(FALSE);
		m_cbxMonitored.EnableWindow(FALSE);
		m_initSleepSecond.SetReadOnly();
		m_cbxAdminState.EnableWindow(FALSE);

		break;
	}

	// 필드 최대 크기 설정
	m_appId.SetLimitText(255);
	m_binaryName.SetLimitText(255);
	m_properties.SetLimitText(255);
	m_debug.SetLimitText(255);
	m_argument.SetLimitText(1023);
	m_initSleepSecond.SetLimitText(5);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

bool ProcessDlg::InitComboBox()
{
	// running type
	m_cbxRunningType.AddString("Background");
	m_cbxRunningType.AddString("Foreground");
	m_cbxRunningType.SetCurSel(0);

	// auto start
	m_cbxAutoStart.AddString("True");
	m_cbxAutoStart.AddString("False");
	m_cbxAutoStart.SetCurSel(1);

	// runtime update
	m_cbxRuntimeUpdate.AddString("True");
	m_cbxRuntimeUpdate.AddString("False");
	m_cbxRuntimeUpdate.SetCurSel(0);

	// prerunning application
	m_cbxPreRunningApp.AddString(""); // EMPTY
	CArray<CString, CString&> appNameList;
	m_sttDlg->GetApplicationNameList(appNameList);
	int count = appNameList.GetCount();
	for (int i=0; i < count; ++i) {
		CString appName = appNameList.GetAt(i);
		m_cbxPreRunningApp.AddString(appName.GetBuffer(0));
	}
	m_cbxPreRunningApp.SetCurSel(0);

	// is primitive
	m_cbxIsPrimitive.AddString("True");
	m_cbxIsPrimitive.AddString("False");
	m_cbxIsPrimitive.SetCurSel(1);

	// monitored
	m_cbxMonitored.AddString("True");
	m_cbxMonitored.AddString("False");
	m_cbxMonitored.SetCurSel(0);

	// adminState
	m_cbxAdminState.AddString("True");
	m_cbxAdminState.AddString("False");
	m_cbxAdminState.SetCurSel(0);

	return true;
}

bool ProcessDlg::LoadApplicationInfo(LPCSTR lpszAppName)
{
	if (!m_sttDlg) {
		//AfxMessageBox("부모 대화상자를 찾을 수 없습니다.");
#ifdef AFX_TARG_KOR
		SttMessageBox("부모 대화상자를 찾을 수 없습니다.", 2000);
#else
		SttMessageBox("Can not find parents dialogue box.", 2000);
#endif
		return false;
	}

	APPLICATION_INFO info;

	BeginWaitCursor();

	// get Application Info	
	m_sttDlg->GetApplicationInfo(lpszAppName, info);

	m_appId.SetWindowText(info.appId);
	m_binaryName.SetWindowText(info.binaryName);
	m_argument.SetWindowText(info.argument);
	m_properties.SetWindowText(info.properties);
	m_debug.SetWindowText(info.debug);
	m_startTime.SetWindowText(info.startTime);
	ciString strPid;
	ciStringUtil::itoa(info.pid, strPid);
	m_pid.SetWindowText(strPid.c_str());
	m_cbxRunningType.SetCurSel( 
		((info.runningType.CompareNoCase("bg")==0) || 
		(info.runningType.GetLength()==0)) ? 0 : 1);
	m_cbxAutoStart.SetCurSel(info.autoStartUp ? 0:1);
	m_cbxRuntimeUpdate.SetCurSel(info.runTimeUpdate ? 0:1);

	m_cbxPreRunningApp.SetText(info.preRunningApp);
	m_cbxIsPrimitive.SetCurSel(info.isPrimitive ? 0:1);
	m_cbxMonitored.SetCurSel(info.monitored ? 0:1);
	ciString strSleep;
	ciStringUtil::itoa(info.initSleepSecond, strSleep);
	m_initSleepSecond.SetWindowTextA(strSleep.c_str());
	m_cbxAdminState.SetCurSel(info.adminState ? 0:1);
	//

	EndWaitCursor();

	return true;
}

bool ProcessDlg::CheckField()
{
	if(m_info.appId.GetLength() == 0)
	{
		//AfxMessageBox("프로세스 이름을 입력하여 주십시오.", MB_ICONWARNING);
#ifdef AFX_TARG_KOR
		SttMessageBox("프로세스 이름을 입력하여 주십시오.", 2000);
#else
		SttMessageBox("Please input APPLICATION ID.", 2000);
#endif
		m_appId.SetFocus();
		return false;
	}

	if(m_info.binaryName.GetLength() == 0)
	{
#ifdef AFX_TARG_KOR
		SttMessageBox("바이너리 이름을 입력하여 주십시오.", 2000);
#else
		SttMessageBox("Please input BINARY NAME.", 2000);
#endif
		m_binaryName.SetFocus();
		return false;
	}
	
	if(m_cbxRunningType.GetCurSel() < 0)
	{
#ifdef AFX_TARG_KOR
		SttMessageBox("실행 타입을 선택하여 주십시오.", 2000);
#else
		SttMessageBox("Please select RUNNING TYPE.", 2000);
#endif
		m_cbxRunningType.SetFocus();
		return false;
	}

	if(m_cbxAutoStart.GetCurSel() < 0)
	{
#ifdef AFX_TARG_KOR
		SttMessageBox("자동 실행 여부를 선택하여 주십시오.", 2000);
#else
		SttMessageBox("Please select AUTO STARTUP.", 2000);
#endif
		m_cbxAutoStart.SetFocus();
		return false;
	}

	if(m_cbxRuntimeUpdate.GetCurSel() < 0)
	{
#ifdef AFX_TARG_KOR
		SttMessageBox("실시간 업데이트 여부을 선택하여 주십시오.", 2000);
#else
		SttMessageBox("Please select RUNTIME UPDATE.", 2000);
#endif
		m_cbxRuntimeUpdate.SetFocus();
		return false;
	}

	return true;
}

void ProcessDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	m_info.appId			= m_appId.GetValueString();
	m_info.binaryName		= m_binaryName.GetValueString();
	m_info.argument			= m_argument.GetValueString();
	m_info.properties		= m_properties.GetValueString();
	m_info.debug			= m_debug.GetValueString();
	m_info.startTime		= m_startTime.GetValueString();
	m_info.status			= "Inactive";
	m_info.pid				= m_pid.GetValueInt();
	m_info.runningType		= (m_cbxRunningType.GetCurSel() == 0) ? "BG" : "FG";
	m_info.autoStartUp		= (m_cbxAutoStart.GetCurSel() == 0);
	m_info.runTimeUpdate	= (m_cbxRuntimeUpdate.GetCurSel() == 0);
	m_info.preRunningApp	= m_cbxPreRunningApp.GetText();
	m_info.isPrimitive		= (m_cbxIsPrimitive.GetCurSel() == 0);
	m_info.monitored		= (m_cbxMonitored.GetCurSel() == 0);
	m_info.initSleepSecond	= m_initSleepSecond.GetValueInt();
	m_info.adminState		= (m_cbxAdminState.GetCurSel() == 0);

	switch(m_type)
	{
	case AD_TYPE_ADD:
		if( CheckField() )
		{
			BeginWaitCursor();

			//추가
			m_sttDlg->UpdateProcess(m_info);

			EndWaitCursor();
		}
		else
			return;
		break;

	case AD_TYPE_MODIFY:
		if( CheckField() )
		{
			BeginWaitCursor();

			// 수정
			m_sttDlg->UpdateProcess(m_info);

			EndWaitCursor();
		}
		else
			return;
		break;

	case AD_TYPE_DELETE:
		{
			BeginWaitCursor();

			// 삭제
			m_sttDlg->DeleteItem(m_info.appId);

			EndWaitCursor();
		}
		break;
	}

	CDialog::OnOK();
}
