﻿// AutoUpdateDlg.cpp : implementation file
//

#include "StdAfx.h"
#include "AutoUpdateDlg.h"
#include "LogTrace.h"
#include "AppMonitor.h"
#include "COP/ciArgParser.h"
#include "COP/ciEnv.h"
#include "COP/ciFileUtil.h"
#include "MsgBox.h"
#include "FtpObserver.h"
#include "common/libCrc32/Crc32Static.h"
#include "CustomerSpecific.h"

#include <winsvc.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define AUTOUPDATE_COMPLETE   7 //오토업데이트가 끝날때 네비게이터에게 날린다.
BOOL AutoUpdateDlg::ms_FTPDownloadActive = FALSE;
ciMutex AutoUpdateDlg::m_sLock;

UINT   nAutoUpdateMsg;
/////////////////////////////////////////////////////////////////////////////
// AutoUpdateDlg dialog
AutoUpdateDlg::AutoUpdateDlg(CWnd* pParent /*=NULL*/)
: CDialog(AutoUpdateDlg::IDD, pParent)
, m_pHttpFile(NULL)
{
	//{{AFX_DATA_INIT(AutoUpdateDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bNewMainGUI = FALSE;

	//--012. 자기자신이 업데이트 대상인지 판별한다.
	m_bSelfUpdate = FALSE;
	m_bSelfUpdateInclude =  FALSE;

	m_pDocument = NULL;
	m_ForcedUpdateFlag = FALSE;

	m_passiveFlag = true;
	// FTP 서버가 Passive 모드가 아닐경우
	if(ciArgParser::getInstance()->isSet("+nopassive")) {
		m_passiveFlag = false;
	}

	m_update_only = false;
	m_showMessageBox = false;
	m_isServer = false;
	m_isManager = false;

	////////////////////////////////////////////////
	//Added by jwh184 2012-06-27
	m_bHttpMode = GetUseHttp();
	////////////////////////////////////////////////
}

void AutoUpdateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AutoUpdateDlg)
	DDX_Control(pDX, IDC_PROGRESSBAR_ALL, m_ProgressBarAll);
	DDX_Control(pDX, IDC_STATIC_TRANS_STATE, m_TransState);
	DDX_Control(pDX, IDC_STATIC_TRANS_FILENAME, m_TransFileName);
	DDX_Control(pDX, IDC_PROGRESS_BAR, m_ProgressBar);
	DDX_Control(pDX, IDC_STATIC_TRANS_FILENAME2, m_TransFileName2);
	DDX_Control(pDX, IDC_LIST_LOG, m_listLog);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(AutoUpdateDlg, CDialog)
	//{{AFX_MSG_MAP(AutoUpdateDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_SETWINDOWTEXT, OnSetWindowText)
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
END_MESSAGE_MAP()
//	ON_WM_NCHITTEST()

/////////////////////////////////////////////////////////////////////////////
// AutoUpdateDlg message handlers
//---------------------------FTP 작업쓰레드 생성------------------------//
UINT AutoUpdateDlg::FTPRunThread(LPVOID pParam)
{
	UINT ret = 0;

	AutoUpdateDlg *pDlg = (AutoUpdateDlg*)pParam;
	// 현재 윈도우를 최상위로 가져온다.
	pDlg->SetForegroundWindow(); 	
	pDlg->SetWindowPos(&wndTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);

	////////////////////////////////////////////////////
	//Added by jwh184 2002-03-02
	//http file service를 위한 COM 초기화
	//if(GetUseHttp())
	if(pDlg->m_bHttpMode)
	{
		CoInitialize(NULL);
	}//if
	////////////////////////////////////////////////////

	// FTP 로 다운로드 한다.
	pDlg->AutoUpdate();
	utvDEBUG(("AutoUpdate() End"));
	pDlg->EndUpdate(0);
	utvDEBUG(("EndUpdate() End"));

	// 업데이트 완료되었음을 알림
	//HWND sttHWnd = ::FindWindow(NULL, STARTER_WINDOWSNAME);
	HWND sttHWnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
	::SendMessage(sttHWnd, WM_UPDATE, NULL, UPDATE_COMPLETE);
	utvDEBUG(("UPDATE_COMPLETE Message Send"));

	::Sleep(3000);
	// 다이얼로그 창 자동으로 닫기
	pDlg->SendMessage(WM_CLOSE);
	utvDEBUG(("WND [%X], HWND [%X] SendMessage(WM_CLOSE)", pDlg, pDlg->GetSafeHwnd()));

	// +update_only option 이 셋팅되엇을 경우, update 가 끝났음을 화면에 표시한다.
	if(pDlg->m_update_only) {
		utvDEBUG(("update only case"));
		//HWND sttHWnd = ::FindWindow(NULL, STARTER_WINDOWSNAME);
		HWND sttHwnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
		if(sttHWnd){
			SttMessageBoxH(sttHWnd,"S/W Update Complete", 2000);
		}
	}
	utvDEBUG(("FTPRunThread() End"));

	////////////////////////////////////////////////////
	//Added by jwh184 2002-03-02
	//http file service를 위한 COM 초기화
	//if(GetUseHttp())
	if(pDlg->m_bHttpMode)
	{
		CoUninitialize();
	}//if
	////////////////////////////////////////////////////
	return TRUE;
}

STTDoc* AutoUpdateDlg::GetDocument() const
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(STTDoc)));
	return (STTDoc*)m_pDocument;
}

void AutoUpdateDlg::SetDocument(STTDoc* document)
{
	ASSERT(document);
	m_pDocument = document;
}

void AutoUpdateDlg::SetForcedUpdateFlag(BOOL forcedUpdateFlag)
{
	m_ForcedUpdateFlag = forcedUpdateFlag;
}

/*-------------------------------------------------------------------------*/
//  서버로부터 version.txt 파일을 가져온다.                                //
//  version.txt.tmp 로 로컬에 저장한다.                                    //
/*-------------------------------------------------------------------------*/
BOOL AutoUpdateDlg::GetVersionFile(CString& strVersionTmpPath)
{
	utvDEBUG(("GetVersionFile()"));

	CString versionSrcName;
	if (m_strVersion.IsEmpty()) {
		versionSrcName.Format("/%s", m_strVersionSrcName.GetBuffer(0));
	} else {
		versionSrcName.Format("/%s/%s", m_strVersion.GetBuffer(0), m_strVersionSrcName.GetBuffer(0));
	}

	CString releaseSrcName;
	if (m_strVersion.IsEmpty()) {
		releaseSrcName.Format("/%s", m_strReleaseSrcName.GetBuffer(0));
	} else {
		releaseSrcName.Format("/%s/%s", m_strVersion.GetBuffer(0), m_strReleaseSrcName.GetBuffer(0));
	}

	utvDEBUG(("Download versionSrcName = %s, releaseSrcName = %s", versionSrcName, releaseSrcName));

	//////////////////////////////////////////////////////////////////
	//Modified by jwh184 2002-03-02
	//if(!GetUseHttp())
	if(!m_bHttpMode)
	{
		utvDEBUG(("It's not HTTP mode"));
		FtpUtil FtpObj(m_strID.GetBuffer(0), m_strPassword.GetBuffer(0), m_strIpAddr.GetBuffer(0), m_strIpAddr2.GetBuffer(0),m_port);
		if (m_passiveFlag) FtpObj.Passive(true); // Passive 모드로 전환

		if(FtpObj.Connect(1) == FALSE) {
			utvERROR(("Ftp Connect failed. [server:%s,id:%s]", m_strIpAddr, m_strID));
			if(FtpObj.Connect(2) == FALSE) {
				utvERROR(("Ftp Connect failed. [server:%s,id:%s]", m_strIpAddr2, m_strID));
				return FALSE;
			}else{
				utvDEBUG(("Ftp Connect succeed. [server:%s,id:%s]", m_strIpAddr2, m_strID));
			}
		}else{
			utvDEBUG(("Ftp Connect succeed. [server:%s,id:%s]", m_strIpAddr, m_strID));
		}

		BOOL _envOK    = FtpObj.FileDownload(versionSrcName.GetBuffer(0), 
			(LPSTR)(LPCSTR)(m_strDST_DIR+"/"+m_strVersionDstTmpName), (LPSTR)(LPCSTR)m_strDST_DIR);

		if(_envOK){
			FtpObj.FileDownload(releaseSrcName.GetBuffer(0), 
				(LPSTR)(LPCSTR)(m_strDST_DIR+"/"+m_strReleaseDstTmpName), (LPSTR)(LPCSTR)m_strDST_DIR);
		}

		FtpObj.Disconnect();

		if(!_envOK) {
			//AfxMessageBox("자동 업데이트를 위한 version.txt을 다운로드 할 수 없습니다.");
			//utvERROR(("자동 업데이트를 위한 version.txt을 다운로드 할 수 없습니다."));
			utvERROR(("Fail to download version.txt file."));
			return FALSE;
		}
	}
	else
	{
		utvDEBUG(("It's HTTP mode"));
		if(!m_pHttpFile)
		{
			return FALSE;
		}//if

		strVersionTmpPath = m_strDST_DIR+"/"+m_strVersionDstTmpName;
		CString strReleaseTmpPath = m_strDST_DIR+"/"+m_strReleaseDstTmpName;
		utvDEBUG(("strVersionTmpPath = %s, strReleaseTmpPath = %s", strVersionTmpPath, strReleaseTmpPath));

		//버젼파일은 다운로드를 성공해야하지만, 릴리즈파일은 다운로드를 실패하여도 TRUE를 리턴...
		if(!m_pHttpFile->GetFile(versionSrcName, strVersionTmpPath, 0, NULL))		//Version 파일 다운로드
		{
			//utvERROR(("자동 업데이트를 위한 version.txt을 다운로드 할 수 없습니다."));
			utvERROR(("Fail to download version.txt file."));
			utvDEBUG(("%s", m_pHttpFile->GetErrorMsg()));
			return FALSE;
		}//if

		if(!m_pHttpFile->GetFile(releaseSrcName, strReleaseTmpPath, 0, NULL))
		{
			utvWARN(("Fail to download release file(%s)", releaseSrcName));
			utvDEBUG(("%s", m_pHttpFile->GetErrorMsg()));
		}//if

		return TRUE;
	}//if
	//////////////////////////////////////////////////////////////////
	
	return TRUE;
}

/*-------------------------------------------------------------------------*/
// 서버로부터 AutoUpdateDisableList.txt 파일을 가져와서 
// 업데이트 제한 리스트에 자신이 포함되어 있는지 확인한다.
// TRUE : AutoUpdate 가능(제한된 경우를 제외하고 모두 가능)
// FALSE: AutoUpdate 제한됨(FTP 접속 실패, 제한된 호스트)
/*-------------------------------------------------------------------------*/
BOOL 
AutoUpdateDlg::IsAutoUpdatePossible()
{
	// AUTOUPDATE_DISABLE_CHECKLIST 파일 다운로드

	utvDEBUG(("IsAutoUpdatePossible()"));

	CString remotePath;
	if (m_strVersion.IsEmpty()) {
		remotePath.Format("/%s", AUTOUPDATE_DISABLE_CHECKLIST);
	} else {
		remotePath.Format("/%s/%s", m_strVersion, AUTOUPDATE_DISABLE_CHECKLIST);
	}

	CString localPath;
	localPath.Format("%s\\%s", m_strDST_DIR, AUTOUPDATE_DISABLE_CHECKLIST);

	utvDEBUG(("Download %s, RemotePath = %s, LocalPath = %s", AUTOUPDATE_DISABLE_CHECKLIST, remotePath, localPath));

	///////////////////////////////////////////////////////////////////////
	//Modified by jwh184 2002-03-02
	//if(!GetUseHttp())
	if(!m_bHttpMode)
	{
		utvDEBUG(("It's not HTTP mode"));
		FtpUtil FtpObj(m_strID, m_strPassword,m_strIpAddr,m_strIpAddr2,m_port);
		if (m_passiveFlag) FtpObj.Passive(true); // Passive 모드로 전환

		if(FtpObj.Connect(1) == FALSE) {
			utvERROR(("Ftp Connect failed. [server:%s,id:%s]", m_strIpAddr, m_strID));
			if(FtpObj.Connect(2) == FALSE) {
				utvERROR(("Ftp Connect failed. [server:%s,id:%s]", m_strIpAddr2, m_strID));
				return FALSE;
			}else{
				utvDEBUG(("Ftp Connect succeed. [server:%s,id:%s]", m_strIpAddr2, m_strID));
			}
		}else{
			utvDEBUG(("Ftp Connect succeed. [server:%s,id:%s]", m_strIpAddr, m_strID));
		}


		BOOL _envOK    = FtpObj.FileDownload(remotePath, 
			(LPSTR)(LPCSTR)localPath, (LPSTR)(LPCSTR)m_strDST_DIR);
		FtpObj.Disconnect();

		if(!_envOK) {
			utvWARN(("%s을 다운로드 할 수 없습니다.", AUTOUPDATE_DISABLE_CHECKLIST));
			return TRUE;
		}
	}
	else
	{
		//해당 주소로 연결이 안된다면 오류이므로 FALSE를 리턴하지만,
		//AutoUpdateDisableList.txt을 서버에서 다운로드 받지 못했다고 오류는 아니다!!!(TRUE 리턴)
		if(!m_pHttpFile)
		{
			return FALSE;
		}//if

		if(!m_pHttpFile->GetFile(remotePath, localPath, 0, NULL))
		{
			utvWARN(("GetFile %s failed (%s)", remotePath, m_pHttpFile->GetErrorMsg()));
			return TRUE;	//해당 파일이 없어도 된다?
		}//if
	}//if
		
	utvDEBUG(("GetFile %s succeed",localPath));

	// HostId 가져옴

	CString hostId = m_pDocument->GetHostID();
	if (hostId.IsEmpty()) {
		utvERROR(("Host ID is empty."));
		DeleteFile(localPath);
		return TRUE;
	}

	// 업데이트 제한 리스트에 자신이 포함되어 있는지 확인

	CFileException fileException;
	CString strExtracted;
	CStdioFile updateLimitFile;
	long	TotalFileLen;
	long    tmpFilePos;
	try
	{
		if( updateLimitFile.Open(localPath, CStdioFile::modeRead, &fileException ) == 0 ) 
		{
			utvERROR(("Can't open file[%s]", localPath));
			DeleteFile(localPath);
			return TRUE;
		}

		updateLimitFile.Seek( 0, CStdioFile::begin);

		TotalFileLen = updateLimitFile.GetLength();
		tmpFilePos   = updateLimitFile.GetPosition();

		if(TotalFileLen != 0)
		{
			for(;;)
			{
				updateLimitFile.ReadString(strExtracted);
				tmpFilePos = updateLimitFile.GetPosition();
				//------------------ compact set ----------------------//
				bool matched = FALSE;
				if (strExtracted.Right(1) == "*") {
					CString subExtracted = strExtracted.Left(strExtracted.GetLength()-1);
					if (hostId.GetLength() >= subExtracted.GetLength())
					{
						CString subHostId = hostId.Left(subExtracted.GetLength());
						if (subHostId == subExtracted) {
							matched = TRUE;
						}
					}						
				} else {
					if (hostId == strExtracted) {
						matched = TRUE;
					}
				}
				if (matched) {
					utvDEBUG(("It is a limited host to autoupdate by server.[%s,%s]", 
						hostId, strExtracted));
					updateLimitFile.Close();
					DeleteFile(localPath);
					return FALSE;
				}
				//-----------------------------------------------------//
				if(tmpFilePos == TotalFileLen)
					break;
			}
		}
		updateLimitFile.Close();
		DeleteFile(localPath);
		// 제한된 호스트 리스트에 해당되지 않음
		return TRUE;
	}
	catch(CException *e)
	{
		utvERROR(("파일(%s)을 확인하십시오. Error = 1", AUTOUPDATE_DISABLE_CHECKLIST));
		e->Delete();
	}
	catch(...)
	{
		utvERROR(("파일(%s)을 확인하십시오. Error = 2",AUTOUPDATE_DISABLE_CHECKLIST));
	}
	DeleteFile(localPath);
	return TRUE;
}

BOOL AutoUpdateDlg::OnInitDialog()
{
	////////////////////////////////////////////////
	//Added by jwh184 2012-03-12
	//if(GetUseHttp())
	if(m_bHttpMode)
	{
		HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
		if(hr != S_OK)
		{
			utvDEBUG(("Fail to CoInitializeEx"));
		}
		else
		{
			utvDEBUG(("Success CoInitializeEx"));
		}//if
	}//if
	////////////////////////////////////////////////

	utvDEBUG(("WND [%X], HWND [%X] 업데이트 체크", this, GetSafeHwnd()));

	// 랜덤한 짧은 시간동안 Sleep (서버의 순간동시접속을 줄이기 위함)
	srand((unsigned int)time(NULL));
	unsigned int aU = (unsigned int) (rand() * _getpid()) % 1000;
	utvDEBUG(("Sleep %u ms", aU));
	::Sleep(aU);

	/*
	// ftp check  하기 전으로 옮김 
	if(	ciArgParser::getInstance()->isSet("+studio")	|| 
		ciArgParser::getInstance()->isSet("+studioWedding")	||
		ciArgParser::getInstance()->isSet("+studioEE")	||
		ciArgParser::getInstance()->isSet("+manager")	){
		::AfxBeginThread((AFX_THREADPROC)ShowCheckUpdateMsg, this);
	}
	*/

	// config/config.ini 파일로 부터 ftp 접속 정보 가져옴
	GetFTPInfo();

	SetWindowText(AUTOUPDATE_WINDOWSNAME); 
	nAutoUpdateMsg = RegisterWindowMessage(AUTOUPDATE_WINDOWSNAME);

	m_hThis = GetSafeHwnd();

	m_brA.CreateSolidBrush(RGB(192, 192, 192));
	m_brB.CreateSolidBrush(RGB(192, 192, 192));

	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	CRect rect;
	m_listLog.GetClientRect(&rect);
	int nColInterval = rect.Width()/2;

#ifdef AFX_TARG_KOR
	m_listLog.InsertColumn(0, _T("파일명"), LVCFMT_LEFT, nColInterval+60);
	m_listLog.InsertColumn(1, _T("다운로드 성공 여부"), LVCFMT_LEFT, nColInterval-60);
#else
	m_listLog.InsertColumn(0, _T("FILE NAME"), LVCFMT_LEFT, nColInterval+60);
	m_listLog.InsertColumn(1, _T("DOWNLOAD RESULT"), LVCFMT_LEFT, nColInterval-60);
#endif

	m_strDST_DIR = GetDstDir();

	// 레지스트리에 path가 없으면 디폴트 패스를 잡는다.
	if(m_strDST_DIR == "") {
		m_strDST_DIR = ciEnv::newEnv("PROJECT_HOME");
		if (m_strDST_DIR.IsEmpty()) {
			m_strDST_DIR = _COP_CD("C:\\Project\\UTV");			
		}
	}

	// AutoUpdate 다이얼로그 컨트롤 업데이트
	// WM_CLOSE 메시지에도 대화창이 닫지 않는 문제가 있어서 위치 조정함
#ifdef AFX_TARG_KOR
	::PostMessage(m_hThis, WM_SETWINDOWTEXT, (WPARAM)&m_TransState, (LPARAM)_T("전송상태 : 파일전송시작..."));
#else
	::PostMessage(m_hThis, WM_SETWINDOWTEXT, (WPARAM)&m_TransState, (LPARAM)_T("Tran. Status : File transfer beginning..."));
#endif

	if(ciArgParser::getInstance()->isSet("+firmwareView"))
	{
		SetShowMessageBoxFlag(TRUE);
	}

/*	/////////////////////////////////////////////////////////
	//Added by jwh184 2012-03-07 
	if(!HttpConnectTest())
	{
		if(m_update_only)
		{
			utvDEBUG(("+update_only case"));
		}//if

		if(m_showMessageBox)
		{
			utvDEBUG(("ShowMessageBox case"));	
			SttMessageBox("Connection to S/W Update Center failed or S/W Update not allowed 1\n", 2000);
		}//if
		SendMessage(WM_CLOSE);
		return FALSE;
	}//if
	/////////////////////////////////////////////////////////
*/
	// 서버로부터 AutoUpdate 제한 여부 확인
	if(!m_pDocument->GetServerFlag() && !IsAutoUpdatePossible())
	{
		//utvDEBUG(("Can't update by server's policy"));
		// +update_only option 이 있을 경우에는 "Update 할것이 없다" 라는 메시지를 보여준다.
		if(m_update_only) {
			utvDEBUG(("+update_only case"));
		}
		if(m_showMessageBox)
		{
			utvDEBUG(("ShowMessageBox case"));	
			SttMessageBox(
					"Connection to S/W Update Center failed or S/W Update not allowed 2\n", 2000);
		}
		SendMessage(WM_CLOSE);
		return FALSE;
	}
/*
	if(	ciArgParser::getInstance()->isSet("+studio")	|| 
		ciArgParser::getInstance()->isSet("+studioWedding")	||
		ciArgParser::getInstance()->isSet("+studioEE")	||
		ciArgParser::getInstance()->isSet("+manager")	){
		::AfxBeginThread((AFX_THREADPROC)ShowCheckUpdateMsg, this);
	}
*/
	if( ciArgParser::getInstance()->isSet("+manager") ){
		this->SetIsManager(true);
	}

	// 서버로부터 version.txt 파일 다운로드 -> version.txt.tmp
	CString strVersionTmpPath;
	if(!GetVersionFile(strVersionTmpPath)) {
		SendMessage(WM_CLOSE);
		return FALSE;
	}

	if(!m_pDocument->GetServerFlag()){
		// Customer 별로 다른 파일을 version.tmp.txt 파일에 합침
		CCustomerSpecific* aSpecific = CCustomerSpecific::getInstance();
		aSpecific->Init(this);
		if(aSpecific->Download()){
			aSpecific->AppendVersion(strVersionTmpPath);
		}
	}


	// version.txt.tmp 파일에 쓰여있는 내용을 리스트로 저장 (한줄씩) 그후, version.txt.tmp 제거
	m_listUpdateEnable.RemoveAll();
	if(AnalysisVersionFile(m_strDST_DIR+"/"+m_strVersionDstTmpName, &m_listUpdateEnable) == FALSE)
	{
		SendMessage(WM_CLOSE);
		return FALSE;
	}

	// version.txt 파일에 쓰여있는 내용을 리스트로 저장 (한줄씩)
	VERSIONLIST oOldList;
	AnalysisVersionFileLocal(m_strDST_DIR+"\\"+m_strVersionDstName, &oOldList);

	// 업데이트 해야할 것만 리스트로 저장( m_listUpdateEnable )
	DiscardOldVersionOnObtainList(&oOldList);

	int updateFileCount = m_listUpdateEnable.GetSize();
	if( updateFileCount <= 0 ) 
	{
		EndUpdate(0);
		//utvDEBUG(("업데이트가 없습니다(%d)",m_update_only ));
		utvDEBUG(("Nothing to update(%d)",m_update_only ));
		// +update_only option 이 있을 경우에는 "Update 할것이 없다" 라는 메시지를 보여준다.
		if(m_update_only) {
			utvDEBUG(("+update_only case"));
		}
		if(m_showMessageBox)
		{
			utvDEBUG(("ShowMessageBox case"));
			SttMessageBox("It is the latest version.", 2000);
		}

		SendMessage(WM_CLOSE);
		return TRUE;
	}

	// 기동중인 업데이트 대상 파일을 확인
	//utvDEBUG(("업데이트가 존재합니다."));
	utvDEBUG(("Exist update."));


	// skpark ConfirmDialog 필요


	if(	ciArgParser::getInstance()->isSet("+studio")	|| 
		ciArgParser::getInstance()->isSet("+studioWedding")	||
		ciArgParser::getInstance()->isSet("+studioEE")	||
		ciArgParser::getInstance()->isSet("+manager")	){


		ciString newVersion;
		scratchUtil::getInstance()->getVersionTmp(newVersion,ciFalse);

		ciString oldVersion;
		scratchUtil::getInstance()->getVersion(oldVersion,ciFalse);

		int msgAnswer=IDNO;
		
		ciString quest_kr = "새로운 버전이 존재합니다.  새로운 버전으로 업데이트 하시겠습니까?";
		ciString quest_en = "The new version exists, Would you like to update the new version?";
		ciString quest_jp = "新しいバジョンが存在します。新しいバジョンに更新しますか？";

		ciString quest = quest_en.c_str();
		if(scratchUtil::getInstance()->getLang()=="kr"){
			quest = quest_kr.c_str();
		}else
		if(scratchUtil::getInstance()->getLang()=="jp"){
			quest = quest_jp.c_str();
		}
		
		CString quest_str;
		quest_str.LoadString(IDS_VERSION_QUEST);
		quest = quest_str;

		quest += "\n\n\tOLD VERSION = ";
		quest += oldVersion.c_str();
		quest += "\n\tNEW VERSION = ";
		quest += newVersion.c_str();
		quest += "\n";

		msgAnswer = AfxMessageBox(quest.c_str(), MB_YESNO | MB_ICONQUESTION);
		if(msgAnswer!=IDYES) {
			SendMessage(WM_CLOSE);
			return TRUE;
		}
	}


	// 등록된 어플리케이션 정보들을 획득
	APPLICATION_LIST appList;
	if (m_pDocument && !m_pDocument->GetApplicationsInfo(appList)) {
		utvDEBUG(("Application List size is zero."));
	}

	// 런타임 업데이트 플래그가 설정된 프로세스 리스트 확보
	// m_ForcedUpdateFlag 가 설정되어 있다면 무조건 런타임 업데이트 가능
	int count = appList.GetSize();
	CStringList appRTU; // runtime update passible
	CStringList appNRTU; // runtime update impassible
	for(int i=0; i<count; ++i)
	{
		APPLICATION_INFO& info = appList.GetAt(i);
		if (info.runTimeUpdate) {
			appRTU.AddTail(::GetBinaryName(info.binaryName));
		} else {
			appNRTU.AddTail(::GetBinaryName(info.binaryName));
		}
	}

	// 업데이트 대상 파일중에 DLL, config, ini 파일이 포함되어있는지 확인
	// evt_restart 파일이 포함되었다면 모든 관리 프로세스 재기동

	m_ocxList.clear();
	BOOL serverRestartFlag = false;
	BOOL rerunFlag = false;
	for (int i=0; i<updateFileCount; ++i)
	{
		// 1. exe 파일의 업데이트라면 해당 프로세스만 재기동
		// 2. config, init 파일의 업데이트라면 모든 프로세스 종료
		CString strExt = m_listUpdateEnable[i].keyname.Mid(0,3);
		if ( strExt == "dll" || strExt == "ini" || strExt == "cfg" || strExt == "ocx")
		{
			if(strExt == "ocx"){
				m_ocxList.push_back(m_listUpdateEnable[i].dstname.GetBuffer());
			}
			rerunFlag = true;
			//break; // serverRestartFlag 때문에 주석처리
		} else if (strExt == "evt") {
			if (strncmp(m_listUpdateEnable[i].keyname.GetBuffer(), "evt_restart", 11)==0) 
			{
				serverRestartFlag = true;
				break;
			}
		}
		// Server 환경에서 all.idl 이 업데이트 되면 전체 재기동
		else if (m_pDocument->GetServerFlag() 
			&& strExt == "idl")
		{
			if (strncmp(m_listUpdateEnable[i].keyname.GetBuffer(), "idl_all", 7)==0)
			{
				serverRestartFlag = true;
				break;
			}
		}
	}

	// skpark 2010.07.07 
	// 프로세스를 여기서 죽이지 않고, FTP 가 완료된 후에 죽이기 위해, 필요한 정보만 기억해둔다.
	// 이하는 주석으로 막아둔다.
	m_KilledProcessInfo.appNRTU.AddTail(&appNRTU);
	m_KilledProcessInfo.appRTU.AddTail(&appRTU);
	m_KilledProcessInfo.serverRestartFlag = serverRestartFlag;
	m_KilledProcessInfo.rerunFlag = rerunFlag;
	m_KilledProcessInfo.updateFileCount = updateFileCount;
	//if(TerminateProcess(serverRestartFlag,rerunFlag,appRTU,appNRTU,updateFileCount)){
	//	return true; // 더이상 진행하지 않음.
	//}
	/*
	if (serverRestartFlag) {

		// 런타임 업데이트 플래그에 상관없이 모든 관리 프로세스 종료
		POSITION pos = appRTU.GetHeadPosition();
		while(pos != NULL)
		{
			CString processName = appRTU.GetNext(pos);
			if (IsAliveProcess(processName)) TerminateProcess(processName);
		}
		POSITION npos = appNRTU.GetHeadPosition();
		while(npos != NULL)
		{
			CString processName = appNRTU.GetNext(npos);
			if (IsAliveProcess(processName)) TerminateProcess(processName);
		}

	} else {

		// 런타임 업데이트 플래그가 설정되지 않은 프로세스 점검

		CStringList appTerminate;
		POSITION pos = appNRTU.GetHeadPosition();
		while(pos != NULL)
		{		
			CString processName = appNRTU.GetNext(pos);
			if (!IsAliveProcess(processName)) continue;

			for (int i=0; i<updateFileCount; ++i)
			{				
				CString binaryName = 
					m_listUpdateEnable[i].srcname.Mid(m_listUpdateEnable[i].srcname.ReverseFind('/')+1);
				if ( rerunFlag || binaryName == processName )
				{
					if (m_ForcedUpdateFlag) {
						appTerminate.AddTail(processName);
						break;
					} else {
						// 업데이트가 존재한다는 이벤트를 발행하고 업데이트 진행 종료

						//HWND sttHWnd = ::FindWindow(NULL, STARTER_WINDOWSNAME);
						HWND sttHWnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
						::SendMessage(sttHWnd, WM_UPDATE, NULL, UPDATE_READY);

						EndUpdate(0);
						//AfxMessageBox("대상 프로세스가 기동중입니다. 업데이트를 진행할 수 없습니다.");
						SendMessage(WM_CLOSE);
						return true;
					}
				}
			}
		}

		// 프로세스를 종료시킬 것인지 확인
		if (m_ForcedUpdateFlag && appTerminate.GetSize() > 0) 
		{
			// 종료할 것인지 확인
#ifdef AFX_TARG_KOR
			int select = MessageBox("업데이트를 위해서 실행중인 프로세스를 종료합니다.\n"
				"업데이트를 진행 하시겠습니까?\n", "자동업데이트", MB_OKCANCEL);
#else
			int select = MessageBoxA("Kill executing process for update.\n"
				"Do you progress update?\n", "AutoUpdate", MB_OKCANCEL);
#endif

			//업데이트 안하겠다고 하면, 자신만 종료
			if(select == IDCANCEL)
			{
				EndUpdate(1);		//자신 종료	
				SendMessage(WM_CLOSE);
				return true;
			}
			// 업데이트 하겠다고 하면, 대상 프로세스들을 종료
			else{
				POSITION posT = appTerminate.GetHeadPosition();
				while(posT != NULL) {
					CString processName = appTerminate.GetNext(posT);
					TerminateProcess(processName);
				}
			}
		}

		// 재기동이 필요하고 런타임 업데이트 플래그가 설정된 프로세스 종료
		for (int i=0; i<updateFileCount; ++i)
		{
			if ( rerunFlag )
			{
				// 모든 프로세스 종료
				POSITION pos = appRTU.GetHeadPosition();
				while(pos != NULL)
				{
					CString processName = appRTU.GetNext(pos);
					if (IsAliveProcess(processName)) TerminateProcess(processName);
				}
				break;
			} else {				
				CString binaryName = 
					m_listUpdateEnable[i].srcname.Mid(m_listUpdateEnable[i].srcname.ReverseFind('/')+1);
				if (!IsAliveProcess(binaryName)) 
					continue;

				POSITION pos = appRTU.GetHeadPosition();
				while(pos != NULL)
				{
					CString processName = appRTU.GetNext(pos);
					if (binaryName == processName) TerminateProcess(processName);
				}
			}
		}

	} // serverRestartFlag
	*/
	// AutoUpdate 다이얼로그 컨트롤 업데이트
	//::PostMessage(m_hThis, WM_SETWINDOWTEXT, (WPARAM)&m_TransState, (LPARAM)_T("전송상태 : 파일전송시작..."));

	//기존 프로세스 죽이고 업데이트 한다.
	utvDEBUG(("FTPRunThread() Before"));
	::AfxBeginThread((AFX_THREADPROC)FTPRunThread, this);
	utvDEBUG(("FTPRunThread() After"));

	SetTimer(TIMER_CHECK_MINIMIZE, 100, NULL);

	return true;
}

UINT AutoUpdateDlg::ShowCheckUpdateMsg(LPVOID pParam)
{
	utvDEBUG(("ShowCheckUpdateMsg()"));

	ciString quest_kr = "최신 버전 여부를 확인하고 있습니다";
	ciString quest_en = "Latest version checking...";
	ciString quest_jp = "最新のバジョンを確認しています。";

	ciString quest = quest_en.c_str();
	if(scratchUtil::getInstance()->getLang()=="kr"){
		quest = quest_kr.c_str();
	}else
	if(scratchUtil::getInstance()->getLang()=="jp"){
		quest = quest_jp.c_str();
	}

	//CString quest;
	//quest.LoadString(IDS_VERSION_CHECK);

	AutoUpdateDlg *pDlg = (AutoUpdateDlg*)pParam;

	CMsgBox obj(pDlg);
	obj.MessageBox(quest.c_str(), "UTV Starter Message", 3000, MB_OK | MB_ICONINFORMATION | MB_TOPMOST);

	return 1;

}

BOOL AutoUpdateDlg::TerminateProcess(	BOOL& serverRestartFlag, 	
										BOOL& rerunFlag,
									 	CStringList& appRTU, // runtime update passible
										CStringList& appNRTU, // runtime update impassible
										int& updateFileCount		) 
{
	utvDEBUG(("TerminateProcess(serverRestartFlag=%d,rerunFlag=%d,updateFileCount=%d)",
				serverRestartFlag,rerunFlag,updateFileCount));

	if (serverRestartFlag) {

		// 런타임 업데이트 플래그에 상관없이 모든 관리 프로세스 종료
		POSITION pos = appRTU.GetHeadPosition();
		while(pos != NULL)
		{
			CString processName = appRTU.GetNext(pos);
			if (IsAliveProcess(processName)) TerminateProcess(processName);
		}
		POSITION npos = appNRTU.GetHeadPosition();
		while(npos != NULL)
		{
			CString processName = appNRTU.GetNext(npos);
			if (IsAliveProcess(processName)) TerminateProcess(processName);
		}

	} else {

		// 런타임 업데이트 플래그가 설정되지 않은 프로세스 점검

		CStringList appTerminate;
		POSITION pos = appNRTU.GetHeadPosition();
		while(pos != NULL)
		{		
			CString processName = appNRTU.GetNext(pos);
			if (!IsAliveProcess(processName)) continue;

			for (int i=0; i<updateFileCount; ++i)
			{				
				CString binaryName = 
					m_listUpdateEnable[i].srcname.Mid(m_listUpdateEnable[i].srcname.ReverseFind('/')+1);
				if ( rerunFlag || binaryName == processName )
				{
					if (m_ForcedUpdateFlag) {
						appTerminate.AddTail(processName);
						break;
					} else {
						// 업데이트가 존재한다는 이벤트를 발행하고 업데이트 진행 종료

						//HWND sttHWnd = ::FindWindow(NULL, STARTER_WINDOWSNAME);
						HWND sttHWnd = scratchUtil::getInstance()->getWHandle("UTV_starter.exe");
						::SendMessage(sttHWnd, WM_UPDATE, NULL, UPDATE_READY);

						EndUpdate(0);
						//AfxMessageBox("대상 프로세스가 기동중입니다. 업데이트를 진행할 수 없습니다.");
						SendMessage(WM_CLOSE);
						return true;
					}
				}
			}
		}

		// 프로세스를 종료시킬 것인지 확인
		if (m_ForcedUpdateFlag && appTerminate.GetSize() > 0) 
		{
			// 종료할 것인지 확인
#ifdef AFX_TARG_KOR
			int select = MessageBox("업데이트를 위해서 실행중인 프로세스를 종료합니다.\n"
				"업데이트를 진행 하시겠습니까?\n", "자동업데이트", MB_OKCANCEL);
#else
			int select = MessageBoxA("Kill executing process for update.\n"
				"Do you progress update?\n", "AutoUpdate", MB_OKCANCEL);
#endif

			//업데이트 안하겠다고 하면, 자신만 종료
			if(select == IDCANCEL)
			{
				EndUpdate(1);		//자신 종료	
				SendMessage(WM_CLOSE);
				return true;
			}
			// 업데이트 하겠다고 하면, 대상 프로세스들을 종료
			else{
				POSITION posT = appTerminate.GetHeadPosition();
				while(posT != NULL) {
					CString processName = appTerminate.GetNext(posT);
					TerminateProcess(processName);
				}
			}
		}

		// 재기동이 필요하고 런타임 업데이트 플래그가 설정된 프로세스 종료
		for (int i=0; i<updateFileCount; ++i)
		{
			if ( rerunFlag )
			{
				// 모든 프로세스 종료
				POSITION pos = appRTU.GetHeadPosition();
				while(pos != NULL)
				{
					CString processName = appRTU.GetNext(pos);
					if (IsAliveProcess(processName)) TerminateProcess(processName);
				}
				break;
			} else {				
				CString binaryName = 
					m_listUpdateEnable[i].srcname.Mid(m_listUpdateEnable[i].srcname.ReverseFind('/')+1);
				if (!IsAliveProcess(binaryName)) 
					continue;

				POSITION pos = appRTU.GetHeadPosition();
				while(pos != NULL)
				{
					CString processName = appRTU.GetNext(pos);
					if (binaryName == processName) TerminateProcess(processName);
				}
			}
		}

	} // serverRestartFlag
	return false;
}

void AutoUpdateDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR AutoUpdateDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

UINT AutoUpdateDlg::OnNcHitTest(CPoint point) 
{
	UINT ret = CDialog::OnNcHitTest(point);

	if(ret==HTCLIENT)
		ret = HTCAPTION;

	return ret;
}

void AutoUpdateDlg::AutoUpdate()
{
	utvDEBUG(("1. AutoUpdate를 시작"));

	BOOL	canceled = FALSE;
	CString remoteDirectoryName;
	FtpUtil FtpRegular;
	CFileServiceWrap httpFile;

	/////////////////////////////////////////////////////////////////////////
	//Modified by jwh184 2002-0305
	//if(!GetUseHttp())
	if(!m_bHttpMode)
	{
		utvDEBUG(("2. FTP 서버에 접속"));

		//FtpUtil FtpRegular(m_strID.GetBuffer(0), m_strPassword.GetBuffer(0),m_strIpAddr.GetBuffer(0), m_strIpAddr2.GetBuffer(0),m_port);
		FtpRegular.SetAccount(m_strID.GetBuffer(0), m_strPassword.GetBuffer(0),m_strIpAddr.GetBuffer(0), m_strIpAddr2.GetBuffer(0),m_port);
		if (m_passiveFlag) FtpRegular.Passive(true); // Passive 모드로 전환

		if(FtpRegular.Connect(1) == FALSE) {
			utvERROR(("Ftp Connect failed. [server:%s,id:%s]", m_strIpAddr, m_strID));
			if(FtpRegular.Connect(2) == FALSE) {
				utvERROR(("Ftp Connect failed. [server:%s,id:%s]", m_strIpAddr2, m_strID));
				SendMessage(WM_CLOSE);
				return;
			}else{
				utvDEBUG(("Ftp Connect succeed. [server:%s,id:%s]", m_strIpAddr2, m_strID));
			}
		}else{
			utvDEBUG(("Ftp Connect succeed. [server:%s,id:%s]", m_strIpAddr, m_strID));
		}
	}//if
	/////////////////////////////////////////////////////////////////////////

	utvDEBUG(("3. 자신이 업데이트 대상에 포함되어 있는지 확인"));

	CString strFailMsg = "실패 항목\n-------------------------------\n";
	BOOL bFailure = FALSE;
	
	int count, iSize;
	int nStartIdx = 0;
	iSize = m_listUpdateEnable.GetSize();	// m_listUpdateEnable : 다운로드 해야할 파일 리스트

	//skpark 2011.10.21
	VERSIONLIST aCompleteList;
	ciStringList aCopyFailList;

	for(count = 0; count < iSize; count++)
	{
		VERSION_INFO _tmpinfo = m_listUpdateEnable[count];

		// UTV_starter.exe 파일만 내려받기 위해 아래처럼 세팅
		if( _tmpinfo.procname == "autoupdate")
		{
			nStartIdx		= count;
			m_bSelfUpdate	= TRUE;
			iSize			=	1;
			break;
		}
		else
			nStartIdx = 0;

	}

	// DOWNLOAD_TMP_DIRECTORY 사용 코드
	utvDEBUG(("3-1. 임시 다운로드 폴더 생성"));

	CString downloadDirectory;
	downloadDirectory.Format("%s\\%s", m_strDST_DIR.GetBuffer(), DOWNLOAD_TMP_DIRECTORY);
	::CreateDirectory(downloadDirectory.GetBuffer(), NULL);

	utvDEBUG(("4. 업데이트 대상 파일들 루프를 돌며 확인"));

	//--012. 
	//전체상태 프로그레스바 초기화
	m_ProgressBarAll.SetShowText(TRUE);
	m_ProgressBarAll.SetRange(0, iSize);

	// 윈도우 서비스로 등록된 프로세스를 죽였다 살리기 위한 플래그
	BOOL isServiceTerminate=false;
	BOOL hasAlterTable=false;
	BOOL wedding_changed=false;
	BOOL downSuccess = FALSE;

	for(int i = nStartIdx; i < iSize + nStartIdx ; i++) 
	{
		VERSION_INFO _versioninfo = m_listUpdateEnable[i];

		//Http 웹서비스에서 서버경로의 첫번째 경로에 '/' 나 '\\'가 없으면 오류가 난다...
		if(_versioninfo.srcname.GetAt(0) != '/'
			|| _versioninfo.srcname.GetAt(0) != '\\')
		{
			CString strTmpSrc = "/";
			strTmpSrc += _versioninfo.srcname;
			_versioninfo.srcname = strTmpSrc;
		}//if

		CString strTargetFileName = _versioninfo.srcname.Mid(_versioninfo.srcname.ReverseFind('/')+1);	//파일명 추출
		CString strExt = strTargetFileName.Mid(strTargetFileName.Find(".")+1);							//확장자 추출

		utvDEBUG(("[%s] 업데이트 시작", strTargetFileName.GetBuffer()));

		// 업데이트 대상파일에 UTV_starter.exe 파일이 포함되어 있는지 찾는다.
		// 포함되어 있으면 UTV_starter.exe 파일만 먼저 내려받고 난후 
		// 변경된 UTV_starter.exe로 다른 파일들을 내려받는다.
		CString tempFile = strTargetFileName;
		tempFile.MakeUpper();
/* version 파일을 비교하는곳에서 처리함
#ifdef _HYUNDAI_KIA_		
		if(!m_isManager && strncmp(tempFile,"UBCMANAGER", strlen("UBCMANAGER"))==0){
			// 매니저가 아니면, 매니저를 업데이트할 필요가 없다.
			utvDEBUG(("[%s] 파일은 업데이트 하지 않는다.", strTargetFileName.GetBuffer()));
			continue;
		}
#endif
*/
		if(m_isServer && tempFile == "ALTERTABLE.SQL"){
			hasAlterTable = ciTrue;
			// Alter table 할것이 있으면, ALTER TABLE 해주어야 한다.
			utvDEBUG(("[%s] EXIST", strTargetFileName.GetBuffer()));
		}
		if(tempFile == "PLUGINS_WEDDING.SWF"){
			wedding_changed = ciTrue;
			// WEDDING 플래쉬가 바뀌면 copy 해주여야 한다.
			utvDEBUG(("[%s] EXIST", strTargetFileName.GetBuffer()));
		}

		if(tempFile == "SELFAUTOUPDATE.EXE")
		{			
			m_bSelfUpdateInclude = TRUE;
		}


		if(tempFile == "UTV_STARTER.EX_")
		{			
			m_bSelfUpdate = TRUE;
		}
		else
		{			
			//skpark 2010.7.7  이곳에서 프로세스를 죽이지 않고, FTP 완료된 후에 죽인다.
			m_KilledProcessInfo.strTargetFileNameList.AddTail(strTargetFileName);
			/*
			// 다운로드파일의 프로세스가 떠 있으면 종료시킨다.
			// skpark
			// 이때 만약, 해당 프로세스가, 윈도우즈 서비스로 등록되어 있다면, 서비스도 내려야 한다.
			// 단, 서비스에 등록된 프로세스가 여러개라 하더라도, 서비스는 한번만 내리면 된다.
			if(isServiceTerminate==false && isService(strTargetFileName)){
				isServiceTerminate = TerminateService();
			}else{
				TerminateProcess(strTargetFileName);
			}
			
			TerminateProcess(strTargetFileName);
			*/

			m_bSelfUpdate = FALSE;
		}

		utvDEBUG(("skpark : Sleep !!!!!!!!"));
		::Sleep(1000);

		CString _wndtextStatus;
#ifdef AFX_TARG_KOR
		if(m_bSelfUpdate)
			_wndtextStatus.Format("전송상태 : %d / %d", 1, iSize);
		else
			_wndtextStatus.Format("전송상태 : %d / %d", i+1, iSize);
		CString _wndtextSrcName = "전송파일 : " + _versioninfo.srcname;
		CString _wndtestDstName = "대상파일 : " + _versioninfo.dstname;
#else
		if(m_bSelfUpdate)
			_wndtextStatus.Format("Tran. Status : %d / %d", 1, iSize);
		else
			_wndtextStatus.Format("Tran. Status : %d / %d", i+1, iSize);
		CString _wndtextSrcName = "Source File  : " + _versioninfo.srcname;
		CString _wndtestDstName = "Target File  : " + _versioninfo.dstname;
#endif

		::SendMessage(m_hThis, WM_SETWINDOWTEXT, reinterpret_cast<WPARAM>(&this->m_TransState), 
			(LPARAM)(LPCSTR)_wndtextStatus);		
		::SendMessage(m_hThis, WM_SETWINDOWTEXT, reinterpret_cast<WPARAM>(&this->m_TransFileName), 
			(LPARAM)(LPCSTR)_wndtextSrcName);		
		::SendMessage(m_hThis, WM_SETWINDOWTEXT, reinterpret_cast<WPARAM>(&this->m_TransFileName2), 
			(LPARAM)(LPCSTR)_wndtestDstName);

		utvDEBUG((" - [%s] Download start.", strTargetFileName.GetBuffer()));

		// 리스트 컨트롤 파일 정보 입력
		int index = m_listLog.GetItemCount();
		m_listLog.InsertItem(index, "");
		m_listLog.SetItemData(index, index);
		m_listLog.SetItemText(index, 0, _versioninfo.dstname);	
		// 리스트 컨트롤 자동 스크롤
		int nCount = m_listLog.GetItemCount();
		m_listLog.EnsureVisible(nCount-1, FALSE); 

		//--012. 다운로드 받을 파일의 핸들을 얻는다.
		//--디렉토리경로까지 지정해줘야함.
		/////////////////////////////////////////////////////////////////////////
		//Modified by jwh184 2002-0305
		//if(!GetUseHttp())
		if(!m_bHttpMode)
		{
			utvDEBUG(("FTP file download (%s)", _versioninfo.srcname));
			downSuccess = aFileDownload(&FtpRegular, _versioninfo);
		}
		else
		{
			utvDEBUG(("HTTP file download (%s)", _versioninfo.srcname));
			downSuccess = HttpFileDownload(m_pHttpFile, _versioninfo);
		}//if
		utvDEBUG((" - [%s] Download complete.", strTargetFileName));

		// 전체파일 다운로드 현황.
		CString str;
		if(m_bSelfUpdate)
			str.Format("%d%%", (100/iSize));
		else
			str.Format("%d%%", ((i+1)*100)/iSize);

		utvDEBUG((" - 전체중 얼만큼 % 다운받았는지 표시"));

		m_ProgressBarAll.SetWindowText(str);
		m_ProgressBarAll.StepIt();

		//--012.다운로드가 실패했을경우 메시지 박스를 띄운다.
		if(!downSuccess)
		{
			bFailure = TRUE;
			strFailMsg += _versioninfo.srcname;
			strFailMsg += _T("\n");
			m_listLog.SetItemText(index, 1, "FAILURE");
		} 
		// 다운로드가 성공시 로컬의 version.txt 버전 갱신
		else 
		{
			// CRC 체크
			BOOL crcResult = TRUE;
			if (_versioninfo.crc.GetLength()) {
				CString dstName = m_strDST_DIR+"\\"+DOWNLOAD_TMP_DIRECTORY+_versioninfo.dstname;
				DWORD dwCrc32, dwErrorCode = NO_ERROR;
				dwErrorCode = CCrc32Static::FileCrc32Win32(dstName, dwCrc32);
				CString strResult;
				if(dwErrorCode == NO_ERROR)
					strResult.Format(_T("0x%08x"), dwCrc32);
				else
					strResult.Format(_T("Error: [0x%08x]"), dwErrorCode);
				if (strResult != _versioninfo.crc) {
					utvERROR(("%s CRC Check failed.[%s][%s]", dstName, _versioninfo.crc, strResult));
					//DeleteFile(dstName);
					crcResult = FALSE;
					bFailure = TRUE; // TMP 폴더 복사하지 않음
				}
			}

			if (crcResult) {
				// 다운로드 성공한 것에 대해서는 VERSION_DST_NAME_TMP 파일에 업데이트.
				//skpark 2011.10.21
				//ModifyVersionInfo(&_versioninfo);
				_versioninfo.updateComplete = true;
				aCompleteList.Add(_versioninfo);
				utvDEBUG((" - [%s] 다운로드 성공하였으므로 version.txt 파일 갱신",
					strTargetFileName));
				m_listLog.SetItemText(index, 1, "SUCCEED");

				CCustomerSpecific::getInstance()->SetDownloadResult(_versioninfo.keyname,TRUE);

			} else {
				m_listLog.SetItemText(index, 1, "CRC FAILURE");
			}
		}
		m_listLog.RedrawItems(index, index);
	}

	/////////////////////////////////////////////////////////////////////////
	//Modified by jwh184 2002-0305
	//if(!GetUseHttp())
	if(!m_bHttpMode)
	{
		utvDEBUG(("5. FTP disconnected."));
		FtpRegular.Disconnect();
	}//if
	/////////////////////////////////////////////////////////////////////////

	utvDEBUG(("6. 업데이트 결과 확인"));
	strFailMsg += _T(" 관련 파일의 다운로드를 실패하였습니다.");
	if(bFailure)	
	{
		//AfxMessageBox(strFailMsg);
		utvERROR(("%s", strFailMsg.GetBuffer(0)));
		/*////////////////////////////////////////////////////////////////////////////
		//For test
		CString strSrcDir = m_strDST_DIR + "\\" + DOWNLOAD_TMP_DIRECTORY;
		CString strTrgDir = m_strDST_DIR + "\\" + "TestBin";
		if (!ciFileUtil::MoveDir(strSrcDir.GetBuffer(), strTrgDir.GetBuffer(), aCopyFailList))
		{
			utvERROR(("MoveDir(%s->%s) failed...", strSrcDir.GetBuffer(), strTrgDir.GetBuffer()));
		} 
		////////////////////////////////////////////////////////////////////////*/
	}

	// DOWNLOAD_TMP_DIRECTORY 사용 코드
	else 
	{
		//skpark 2010.7.7 move 하기 직전에 죽인다.
		if(this->TerminateProcess(	m_KilledProcessInfo.serverRestartFlag,
									m_KilledProcessInfo.rerunFlag,
									m_KilledProcessInfo.appRTU,
									m_KilledProcessInfo.appNRTU,
									m_KilledProcessInfo.updateFileCount)){
			utvWARN(("Update aborted by user"));
			return;
		}
		POSITION pos = m_KilledProcessInfo.strTargetFileNameList.GetHeadPosition();
		while(pos != NULL)
		{		
			CString strTargetFileName = m_KilledProcessInfo.strTargetFileNameList.GetNext(pos);
			if(isServiceTerminate==false && isService(strTargetFileName)){
					isServiceTerminate = TerminateService();
			}
			TerminateProcess(strTargetFileName);
		}
		::Sleep(1000);  // browser 등의 프로세스가 죽을 여유를 준다.
		//skpark 2010.7.7 end

		utvDEBUG(("6-1. 임시 다운로드 파일 이동 및 폴더 삭제"));
		//CString strSrcDir = m_strDST_DIR + "\\" + DOWNLOAD_TMP_DIRECTORY + "\\*";
		CString strSrcDir = m_strDST_DIR + "\\" + DOWNLOAD_TMP_DIRECTORY;
		CString strTrgDir = m_strDST_DIR;

		if (!ciFileUtil::MoveDir(strSrcDir.GetBuffer(), strTrgDir.GetBuffer(), aCopyFailList))
		{
			utvERROR(("MoveDir(%s->%s) failed...", strSrcDir.GetBuffer(), strTrgDir.GetBuffer()));
			//Modified by jwh184
			//MoveDir이 실패하면 aCompleteList를 비워준다.
			aCompleteList.RemoveAll();

		} /*else {
			if (!ciFileUtil::DeleteDir(downloadDirectory.GetBuffer())) {
				utvERROR(("DeleteDir(%s) failed...", downloadDirectory.GetBuffer()));
			}
		}*/
	}

	// skpark 2011.10.21
	// skpark : copy 가 실패한 파일이 다운로드가 완료된것으로 표시되는 것을 막기위해
	int completeSize = aCompleteList.GetSize();
	for(int i=0;i<completeSize;i++){
		PVERSION_INFO aVerInfo = &aCompleteList[i];
		aVerInfo->updateComplete = _isCopySucceed(aCopyFailList, aVerInfo->srcname);
		if(aVerInfo->updateComplete==false){
			continue;
		}
		ModifyVersionInfo(aVerInfo);
	}

	utvDEBUG(("7. 버전 파일 최종 갱신"));
	//version.txt 파일 최종갱신
	if (iSize) {		
		RenewVersionFile();
	}

	// skpark
	if (hasAlterTable){
		// Alter table 할것이 있으면, ALTER TABLE 해주어야 한다.
		ciString command = "UTV_sample.exe +server +ubc +noES +noxlog +util +alterTable";
		if(system(command.c_str())!=0){
			utvERROR(("%s failed", command.c_str()));
		}else{
			utvDEBUG(("%s succeed", command.c_str()));
		}
	}
	// skpark
	if (wedding_changed){
		// plugins_wedding.swf 파일이 변경되었다면 copy 해주어야 한다.
		ciString command = "C:\\Windows\\system32\\xcopy.exe /D /Y "; 
		command += _COP_CD("C:\\SQISoft\\UTV1.0\\execute\\flash\\plugins_wedding.swf ");
		command += _COP_CD("C:\\SQISoft\\Contents\\ENC\\.");
		if(system(command.c_str())!=0){
			utvERROR(("%s failed", command.c_str()));
		}else{
			utvDEBUG(("%s succeed", command.c_str()));
		}
	}

	// 만약 서비스를 내렸다면, 다시 시작시킨다.
	
	if(isServiceTerminate){
		RunService();
	}
	
	utvDEBUG(("8. AutoUpdate complete."));
}

BOOL
AutoUpdateDlg::_isCopySucceed(ciStringList& failList, CString& srcname)
{
	ciStringList::iterator itr;
	for(itr=failList.begin();itr!=failList.end();itr++){
		ciString filename = srcname.Mid(srcname.ReverseFind('/')+1);	//파일명 추출
		if(stricmp(filename.c_str(), itr->c_str()) == 0){
			utvDEBUG(("ERROR :: %s file copy failed", filename.c_str()));
			return false;
		}
	}
	return true;
}

//-------------------------------------------------------------//
// 갱신될 version.txt.temp  --> version.txt 로 최종 갱신       //
//-------------------------------------------------------------//
void AutoUpdateDlg::RenewVersionFile()
{
	CStdioFile DefConfigFile;   //version.txt
	CStdioFile tempConfigFile;  //version.txt.tmp
	CFileException fileException;

	if( DefConfigFile.Open(m_strDST_DIR+"/"+m_strVersionDstName, CFile::modeRead | CFile::shareDenyNone, &fileException) == 0)
	{
		char debug[1024];
		fileException.GetErrorMessage(debug, 1024);
		TRACE("debug = %s\n",debug);
		// version.txt 파일이 없으면 업데이트 받은 후에 다운 받은 tmp 파일을 version.txt 로 변경
		try{
			CFile::Rename(m_strDST_DIR+"/"+m_strVersionDstTmpName, m_strDST_DIR+"/"+m_strVersionDstName);
		}
		catch(CException *e)
		{
			char debug[1024];
			e->GetErrorMessage(debug , 1024);
			TRACE("debug = %s\n",debug);

			//AfxMessageBox("version.txt.temp -> version.txt rename 시 오류발생");
			utvERROR(("version.txt.temp -> version.txt rename 시 오류발생"));
		}
		return ; 
	}

	//다운로드 받을 파일이 없는 경우 version.txt.tmp 가 안만들어 졌을것임(고려)
	if( tempConfigFile.Open(m_strDST_DIR+"/"+m_strVersionDstTmpName, CFile::modeReadWrite | CFile::shareDenyNone, &fileException) ==0)
	{
		char debug[1024];
		fileException.GetErrorMessage(debug, 1024);
		TRACE("debug = %s\n",debug);
		return ; 
	}

	CString originalStr;
	CString renewalStr;

	while(1)
	{
		if(!DefConfigFile.ReadString(originalStr))
			break;

		CString orgProcName = "";
		orgProcName= originalStr.Left(originalStr.Find(','));

		BOOL bFind = TRUE;
		while(1)
		{
			if(!tempConfigFile.ReadString(renewalStr))
			{
				bFind = FALSE;
				break;
			}
			CString& renewProcName = renewalStr.Left(renewalStr.Find(','));
			if(::stricmp(orgProcName, renewProcName) == 0)
			{
				break;

			}else{
				continue;
			}
		}
		if(!bFind)
		{
			if(orgProcName != "")   //기존 version.txt 에 쓰여있던 가비지 문자 제거를 위해.
			{
				tempConfigFile.Seek(0, CFile::end);
				tempConfigFile.WriteString(originalStr+"\n");
				tempConfigFile.Seek(0, CFile::begin);
			}
		}
	}
	DefConfigFile.Close();  	
	tempConfigFile.Close(); 

	try
	{
		CFile::Remove(m_strDST_DIR+"/"+m_strVersionDstName);
	}
	catch(CException *e)
	{
		char debug[1024];
		e->GetErrorMessage(debug , 1024);
		TRACE("debug = %s\n",debug);

		//AfxMessageBox("이전 version.txt 삭제시 오류발생");
		utvERROR(("이전 version.txt 삭제시 오류발생"));
	}

	try{
		CFile::Rename(m_strDST_DIR+"/"+m_strVersionDstTmpName, m_strDST_DIR+"/"+m_strVersionDstName);
	}
	catch(CException *e)
	{
		char debug[1024];
		e->GetErrorMessage(debug , 1024);
		TRACE("debug = %s\n",debug);

		//AfxMessageBox("version.txt.temp -> version.txt rename 시 오류발생");
		utvERROR(("version.txt.temp -> version.txt rename 시 오류발생"));
	}
}

BOOL AutoUpdateDlg::AnalysisVersionFile(LPCSTR lpszFileName, PVERSIONLIST pVersionList)
{
	CFileException fileException;

	CString strExtracted;
	CStdioFile DefConfigFile;

	long	TotalFileLen;
	long    tmpFilePos;

	try{
		if( DefConfigFile.Open(lpszFileName, CStdioFile::modeRead, &fileException ) == 0 ) {
			pVersionList->RemoveAll();
			return FALSE;
		}

		DefConfigFile.Seek( 0, CStdioFile::begin);

		TotalFileLen = DefConfigFile.GetLength();
		tmpFilePos   = DefConfigFile.GetPosition();

		if(TotalFileLen != 0)
		{
			for(;;)
			{
				DefConfigFile.ReadString(strExtracted);
				tmpFilePos = DefConfigFile.GetPosition();

				//------------------ compact set ----------------------//
				VERSION_INFO oVersionInfo;
				this->ParsEnvFileLine((LPSTR)(LPCSTR)strExtracted, &oVersionInfo);

				pVersionList->Add(oVersionInfo);
				//-----------------------------------------------------//
				if(tmpFilePos == TotalFileLen)
					break;
			}
		}
		DefConfigFile.Close();

	}catch(CException *e)
	{
		//AfxMessageBox("환경 파일(version.txt)을 확인하십시오. Error = 1");
		utvERROR(("환경 파일(version.txt)을 확인하십시오. Error = 1"));
		e->Delete();
		return FALSE;
	}
	catch(...)
	{
		//AfxMessageBox("환경 파일(version.txt)을 확인하십시오. Error = 2");
		utvERROR(("환경 파일(version.txt)을 확인하십시오. Error = 2"));
		return FALSE;
	}

	try
	{
		CFile::Remove(lpszFileName);  //version.txt.temp 파일은 제거한다.
	}
	catch(CException *e)
	{
		char debug[1024];
		e->GetErrorMessage(debug , 1024);
		TRACE("debug = %s\n",debug);
		utvERROR(("이전 version.txt.temp 삭제시 오류발생"));
	}

	return TRUE;
}

void AutoUpdateDlg::AnalysisVersionFileLocal(LPCSTR lpszFileName, PVERSIONLIST pVersionList)
{
	CFileException fileException;

	CString strExtracted;
	CStdioFile DefConfigFile;

	long	TotalFileLen;
	long    tmpFilePos;

	try
	{
		if( DefConfigFile.Open(lpszFileName, CStdioFile::modeRead, &fileException ) == 0 ) {
			pVersionList->RemoveAll();
			return ;
		}

		DefConfigFile.Seek( 0, CStdioFile::begin);

		TotalFileLen = DefConfigFile.GetLength();
		tmpFilePos   = DefConfigFile.GetPosition();

		if(TotalFileLen != 0)
		{

			for(;;)
			{
				DefConfigFile.ReadString(strExtracted);
				tmpFilePos = DefConfigFile.GetPosition();

				//------------------ compact set ----------------------//
				VERSION_INFO oVersionInfo;
				this->ParsEnvFileLine((LPSTR)(LPCSTR)strExtracted, &oVersionInfo);

				pVersionList->Add(oVersionInfo);
				//-----------------------------------------------------//
				if(tmpFilePos == TotalFileLen)
					break;
			}
		}

		DefConfigFile.Close();
	}
	catch(CException *e)
	{
		//AfxMessageBox("환경 파일(version.txt)을 확인하십시오. Error = 1");
		utvERROR(("환경 파일(version.txt)을 확인하십시오. Error = 1"));
		e->Delete();
	}
	catch(...)
	{
		//AfxMessageBox("환경 파일(version.txt)을 확인하십시오. Error = 2");
		utvERROR(("환경 파일(version.txt)을 확인하십시오. Error = 2"));
	}
}

/*------------------------------------------------
파일(filelist.dat)을 읽어서 파싱.
src name, dest name, dest dir name 을 get.
--------------------------------------------------*/
void AutoUpdateDlg::ParsEnvFileLine(LPSTR envline, PVERSION_INFO pVersionInfo)
{
	static int _linecount_;

	VERSION_INFO _versioninfo;

	CString token;
	CString strData = envline;

	int nFind = -1;

	for(int i=0; i<ITEM_NUM_AT_LINE; i++)
	{
		nFind = strData.Find(',');
		if( nFind == -1 ) {
			token = strData;
			strData = "";
		} else {
			token   = strData.Left(nFind);
			strData = strData.Right(strData.GetLength() - (nFind+1));
		}

		switch(i)
		{
		case 0: pVersionInfo->procname = token; break;
		case 1: pVersionInfo->keyname = token; break;
		case 2: pVersionInfo->version = token; break; 
		case 3: pVersionInfo->srcname = token; break;
		case 4: pVersionInfo->dstname = token; break;
		case 5: pVersionInfo->dirname = token; break;
		case 6: pVersionInfo->crc = token; break;
		}
	}
	_linecount_++;
	pVersionInfo->linenum = _linecount_;
}

void AutoUpdateDlg::EndUpdate(int updateFlag)
{
	// Update 완료 후 동작을 추가합니다.
	if(updateFlag) return;

	/* 2011.7.12 comment out 스타터가 분리되었기 때문에  SelfAutoUpdate 를 또 실행하면 안된다!
	if(m_bSelfUpdate || m_bSelfUpdateInclude)
	{
		CString strProcess;
		strProcess.Format("%s", SELFAUTOUPDATE_EXE);
		for(int i=1; i<__argc; ++i) {
			const char* strArg = __argv[i];
			strProcess += " ";
			strProcess += strArg;
		}
		strProcess += " +byAutoUpdate";
		utvDEBUG(("SELFAUTOUPDATE_EXE Start"));
		WinExec(strProcess.GetBuffer(), SW_SHOW);
		utvDEBUG(("Exit..."));

		scratchUtil::getInstance()->removeIconTray("UTV Starter",169);
		//exit(0);
		EndDialog(IDOK);  // 스타터를 죽인다....근데..잘안죽는듯...
	}
	*/
	if(m_ocxList.size()){
		utvDEBUG(("ocx exist"));
		ciString ubcHome = _COP_CD("C:\\SQISoft\\UTV1.0");
		ciString regsvr32 = "C:\\Windows\\system32\\regsvr32.exe  /s  ";
		ciString winDir = ciEnv::getenv("windir");
		winDir += "\\system32";


		ciStringList::iterator itr;
		for(itr=m_ocxList.begin();itr!=m_ocxList.end();itr++){
			ciString ocxFile = (*itr);
			size_t j;
			for (;( j = ocxFile.find( "/" )) != string::npos;){
				ocxFile.replace( j, 1, "\\");
			}

			ciString srcFile = ubcHome + ocxFile;
			ciString destFile = winDir + ocxFile.substr(strlen("\\execute"));

			ciString command = regsvr32;

			if(::CopyFile(srcFile.c_str(), destFile.c_str(), ciFalse)){
				utvDEBUG(("COPY %s %s succeed", srcFile.c_str(), destFile.c_str()));		
				command += destFile;
			}else{
				utvDEBUG(("COPY %s %s failed", 	srcFile.c_str(), destFile.c_str()));		
				command += srcFile;
			}

			utvDEBUG(("ocx regist command : %s", command.c_str()));
			WinExec(command.c_str(), SW_NORMAL);
		}
	}

	utvDEBUG(("AfterUpdate.bat Start"));

	CCustomerSpecific::getInstance()->Execute();
	//system(_UBC_CD("C:\\SQISoft\\UTV1.0\\execute\\AfterUpdate.bat"));
}

HBRUSH AutoUpdateDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if(nCtlColor == CTLCOLOR_DLG)
	{
		hbr = m_brA;
	}

	if(nCtlColor == CTLCOLOR_STATIC)
	{
		pDC->SetBkColor(RGB(206, 215, 231));
		pDC->SetTextColor(RGB(0, 0, 0));

		pDC->SetBkMode(TRANSPARENT);
		hbr = m_brB;
	}

	return hbr;
}

CString AutoUpdateDlg::GetCurDir()
{
	CString strDir;
	char	szBuf[_MAX_PATH] = { 0x00 };

	::GetModuleFileName(NULL, szBuf, _MAX_PATH);
	strDir.Format("%s", szBuf);
	strDir.MakeReverse();
	int nIndex = strDir.Find("\\");
	if (nIndex < 0) return "";

	if (strDir.GetLength() < (nIndex+1)) return "";

	strDir = strDir.Mid(nIndex + 1);
	strDir.MakeReverse();

	return strDir;
}

CString AutoUpdateDlg::GetDstDir()
{
	// 레지스트리를 읽지 않고, 현재 AutoUpdate.exe 가 있는 위치의 한단계 위를 destnation 폴더로 한다.
	CString currentDir = GetCurDir();

	currentDir.MakeReverse();
	int nIndex = currentDir.Find("\\");
	if (nIndex < 0) return "";

	if (currentDir.GetLength() < (nIndex+1)) return "";

	CString destDir;
	destDir = currentDir.Mid(nIndex + 1);
	destDir.MakeReverse();

	return destDir;
}

BOOL AutoUpdateDlg::StilAliveProcess(CString processName, DWORD processID)
{
	CArray<DWORD,DWORD> processIDs;
	if(IsAliveProcess(processName, &processIDs)){
		for(int i=0; i < processIDs.GetSize(); i++)
		{
			DWORD pID = processIDs.GetAt(i);
			if(pID == processID) {
				utvERROR(("PROCESS %s stil alive", processName));
				return ciTrue;
			}
		}	
	}
	return false;
}

BOOL AutoUpdateDlg::IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs)
{
	utvDEBUG(("IsAliveProcess : %s 1873", processName.GetBuffer()));

	CString newName = "";

	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);

	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(PROCESSENTRY32);

	BOOL bIsAlive = FALSE;

	while(Process32Next(hSnapShot,&pEntry))
	{	
		// 1. 프로세스명 얻기
		CString newName = pEntry.szExeFile;

		newName.MakeUpper();
		processName.MakeUpper();

		// 2. 프로세스명 비교
		if( processName.Find(newName) >= 0 )
		{
			if(processIDs != NULL)
				processIDs->Add(pEntry.th32ProcessID);
			bIsAlive = TRUE;
		}
	}

	CloseHandle(hSnapShot);

	return bIsAlive;
}

void AutoUpdateDlg::TerminateProcess(CString processName)
{
	utvDEBUG(("TerminateProcess(%s) 1908", processName.GetBuffer()));
	CArray<DWORD,DWORD> processIDs;

	if(IsAliveProcess(processName, &processIDs))  //살아있으면
	{
		utvDEBUG(("IsAliveProcess(%s) 1913", processName.GetBuffer()));
		for(int i=0; i < processIDs.GetSize(); i++)
		{
			DWORD processID = processIDs.GetAt(i);
			//gracefully kill
			if (processName == "UTV_brwClient2.exe" || processName == "UTV_brwClient2_UBC1.exe") {
				int result = scratchUtil::getInstance()->killGracefully(processID,3000);
				if(result <= 0){
					utvDEBUG(("browser does not terminated yet.  It will be killed"));
					KillProcess(processID);
				}
			}else{
				utvDEBUG(("KillProcess()"));
				KillProcess(processID);
			}
			int result = 1;
			for(int i=0;i<3;i++){				
				::Sleep(1000);
				utvDEBUG(("IsAliveProcess(%s) 1931", processName.GetBuffer()));
				if(StilAliveProcess(processName, processID)){
					result = -1;
				}else{
					result = 1;
					break;
				}
			}
			if(result <= 0){
				utvDEBUG(("%s does not terminated yet.  It will be killed", processName));
				KillProcess(processID);
			}
		}
	}
	// HardCoding for notifymanager
	if (processName == "notifymanager.exe") {
		TerminateProcess(CString("notifyserver.exe"));
	}
}

LRESULT AutoUpdateDlg::OnSetWindowText(WPARAM wParam, LPARAM lParam)
{
	CWnd* pWnd = reinterpret_cast<CWnd*>(wParam);
	LPCSTR lpszText = reinterpret_cast<LPCSTR>(lParam);

	pWnd->SetWindowText(lpszText);

	return 0;
}


/*------------------------------------------------
레지스트리의 현재 GUI 버전정보를 읽어 새로운 버전 정보와 비교하여 구버전일 경우 FTP Get 리스트에서
제거한다.
--------------------------------------------------*/
VOID AutoUpdateDlg::DiscardOldVersionOnObtainList(PVERSIONLIST pOldList)
{
#define BUF_SIZE 128
	DWORD dwSizeNameBuf = BUF_SIZE;
	DWORD dwSizeDataBuf = BUF_SIZE;
	BYTE lpszData[128];
	::ZeroMemory(lpszData, BUF_SIZE);
	LONG lRetValue;
	HKEY  hKey;

	CString strParentKey(_T("SOFTWARE\\SQISOFT\\UTV\\PROCESS\\"));
	CString strName(_T(""));

	int iSize = m_listUpdateEnable.GetSize();
	for(int i = 0; i < iSize; i ++) {	// file 리스트에서 upper version 이 아닐 경우 제거
		CString strProcName = m_listUpdateEnable[i].procname;
		CString strKeyName = m_listUpdateEnable[i].keyname;
		CString strVersion = m_listUpdateEnable[i].version;

		// 1. 버전 정보를 얻음
		CString strOldVersion(_T(""));
		int iOldSize = pOldList->GetSize();
		for(int j = 0; j < iOldSize; j ++) {
			CString& strOldVerName = pOldList->GetAt(j).procname;
			if(strOldVerName == strProcName ) {
				strOldVersion = pOldList->GetAt(j).version;
				break;
			}
		}

		// 버전 체크 해서 obtain list remove, 프로세스 Kill 을 결정
		// 1) New O, mainNew O -> remove X, kill O
		// 2) New O, mainNew X -> remove X, kill O
		// 3) New X, mainNew O -> remove O, kill O
		// 4) New X, mainNew X -> remove O, kill X
		BOOL bNewVersion = FALSE;
		if( strOldVersion == _T("")) {
			bNewVersion = TRUE;
		}else {
			bNewVersion = IsNewVersion(m_listUpdateEnable[i].version, (LPCSTR)strOldVersion);
		}
		// Manager 는 Player 에서 update 할 필요가 없다
		{
			VERSION_INFO _versioninfo = m_listUpdateEnable[i];
			CString strTargetFileName = _versioninfo.srcname.Mid(_versioninfo.srcname.ReverseFind('/')+1);	//파일명 추출
			strTargetFileName.MakeUpper();
			if(!m_isManager && strncmp(strTargetFileName,"UBCMANAGER", strlen("UBCMANAGER"))==0){
				// 매니저가 아니면, 매니저를 업데이트할 필요가 없다.
				utvDEBUG(("[%s] 파일은 업데이트 하지 않는다.", strTargetFileName.GetBuffer()));
				bNewVersion = FALSE;
			}
		}

		////////////////////////////////////////////////////////////////////////////
		// Modified by jwh184 2012-05-08
		// 4월 28일 릴리즈에 문제가 있어서 UTV_brwClient2.exe, copTaoCI_NODB.dll 2개의 파일이 업데이트가 안되는 문제가 발생
		// 실제 경로에서 검색하여 2개의 파일의 변경 날자가 4월 28일 이전이면 강제로 2개의 파일을 업데이트 하여야 한다.
		// 따라서, 실제 경로의 변경날자를 비교하고 변경날자가 4월 28일 이전이면 m_listUpdateEnable에서 삭제하지 않는다.

		if(!bNewVersion)
		{
			if(!IsForceUpdate(m_listUpdateEnable[i]))	//강제 업데이트 하지말아야 할것만 리스트에서 삭제
			{
				m_listUpdateEnable.RemoveAt(i);
				i --;
				iSize --;
			}//if

			//여기는 레거시코드 예외처리용으로 있던것 같다...
			if( ! m_bNewMainGUI )
			{
				continue;
			}//if
		}//if
		////////////////////////////////////////////////////////////////////////////

		CString strKey = strParentKey + strKeyName;
		lRetValue=::RegOpenKey(HKEY_CURRENT_USER, strKey, &hKey);
		if(lRetValue != ERROR_SUCCESS)		continue;

		// 2. 프로세스 ID를 얻고 Kill
		TCHAR lpszName[128];
		long retCode = ERROR_SUCCESS;
		for (int j = 0; retCode == ERROR_SUCCESS; ++j) { 
			dwSizeNameBuf = BUF_SIZE;
			dwSizeDataBuf = BUF_SIZE;

			lpszName[0] = '\0'; 
			lpszData[0] = '\0'; 

			retCode = RegEnumValue(hKey, j, lpszName, 
				&dwSizeNameBuf, 
				NULL, 
				NULL,					// &dwType, 
				lpszData,				// &bData, 
				&dwSizeDataBuf);   // &bcData 

			if ( retCode == (DWORD)ERROR_SUCCESS && ::strnicmp(lpszName, _T("ProcessId_"), 10) == 0 ) {
				// 현재 작동중인 프로세스 종료
				DWORD dwPId = *(LPDWORD)lpszData;
				this->KillProcess(dwPId);
				::RegDeleteValue(hKey, (LPCSTR) lpszName); // ProcessId_ + # 를 제거한다.
			}
		}

		::RegCloseKey(hKey);
	} 
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 강제로 업데이트를 해야하는 지 판단한다. \n
/// @param (VERSION_INFO&) infoVersion : (in/out) 버젼정보
/// @return <형: bool> \n
///			<true: 강제 업데이트 하여야 함> \n
///			<false: 강제 업데이트 하지 않음> \n
/////////////////////////////////////////////////////////////////////////////////
bool AutoUpdateDlg::IsForceUpdate(VERSION_INFO& infoVersion)
{
	CString strProcName = infoVersion.procname;
	if(strProcName.CompareNoCase("exe_UTV_brwClient2") != 0
		&& strProcName.CompareNoCase("dll_copTaoCI_NODB") != 0)
	{
		//UTV_brwClient2.exe, copTaoCI_NODB.dll 2개의 파일만 강제 업데이트 대상
		ciDEBUG(1, ("It is not target file for force update : %s", strProcName));
		return false;
	}//if

	ciDEBUG(1, ("Check force update : %s", strProcName));

	char szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(szModule, cDrive, cPath, cFilename, cExt);

	CString strDestPath, strDstName, strDirName;
	//dstname	"/execute/UTV_brwClient2_UBC1.exe"
	//dirname	"/execute/"
	//위와 같은 형식으로 버젼정보에 값이 들어 있으므로 실제 프로그램 명만 얻어오기 위해...
	strDirName = infoVersion.dirname;
	strDstName = infoVersion.dstname;
	strDstName.Replace(strDirName, "");
	strDestPath.Format("%s%s%s", cDrive, cPath, strDstName);

	ciDEBUG(1, ("Check force update path : %s", strDestPath));

	//대상 파일의 변경된 날자를 얻어온다.
	CFileStatus fs;
	if(CFile::GetStatus(strDestPath, fs) == FALSE)
	{
		//파일의 정보를 얻어오지 못했다면 업데이트...
		ciERROR(("Fial to get file status : %s", strDestPath));
		return true;
	}//if

	CTime tmForce(2012, 04, 25, 0, 0, 0);		//강제 업데이트 기준시간(여유있게...)
	CString strTmForce, strTmFile;
	strTmForce = tmForce.Format("%Y-%m-%d %H:%M:%S");
	strTmFile = fs.m_mtime.Format("%Y-%m-%d %H:%M:%S");
	ciDEBUG(1, ("Force date : %s", strTmForce));
	ciDEBUG(1, ("File modified date : %s", strTmFile));
	
	if(fs.m_mtime > tmForce)
	{
		//2012-04-25 일 이후에 수정되었다면 업데이트 할 필요 없음.
		ciDEBUG(1, ("Not need force update : %s", strDestPath));
		return false;
	}//if

	//2012-04-25 일 이전에 수정되었다면 업데이트 해야함.
	ciDEBUG(1, ("Need force update : %s", strDestPath));
	return true;
}


inline BOOL AutoUpdateDlg::IsNewVersion(LPCSTR strNewVersion, LPCSTR strCurrent)
{
	int nNew, nOld;

	nNew = atoi(strNewVersion);
	nOld = atoi(strCurrent);

	if (nNew > nOld)
		return TRUE;
	else 
		return FALSE;

	//	return ::lstrcmp(strNewVersion, strCurrent) > 0 ? TRUE : FALSE;
}

/*
프로세스 ID를 이용하여 프로세스를 종료한다.
*/
inline VOID AutoUpdateDlg::KillProcess(DWORD dwProcessId)
{

	HANDLE hProcessGUI = ::OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId);

	if( hProcessGUI ) 
	{
		::TerminateProcess(hProcessGUI, 0);
		//AppMonitor::SafeTerminateProcess(hProcessGUI, 0);
		::CloseHandle(hProcessGUI);
	}
}

//lpszFileName : version.txt
// pVerInfo : 파일에 써야할 한 라인.
void AutoUpdateDlg::ModifyVersionInfo(PVERSION_INFO pVerInfo)
{
	CFileException fileException;

	CString strExtracted;
	CStdioFile tempConfigFile;

	CString strChangedInfo="";
	if (pVerInfo->crc.GetLength()) {
		strChangedInfo.Format("%s,%s,%s,%s,%s,%s,%s\n", 
			pVerInfo->procname, 
			pVerInfo->keyname, 
			pVerInfo->version, 
			pVerInfo->srcname, 
			pVerInfo->dstname,
			pVerInfo->dirname,
			pVerInfo->crc);
	} else {
		strChangedInfo.Format("%s,%s,%s,%s,%s,%s\n", 
			pVerInfo->procname, 
			pVerInfo->keyname, 
			pVerInfo->version, 
			pVerInfo->srcname, 
			pVerInfo->dstname,
			pVerInfo->dirname);
	}

	try
	{
		if( tempConfigFile.Open(m_strDST_DIR+"/"+m_strVersionDstTmpName, 
			CFile::modeCreate | CFile::modeNoTruncate | CFile::modeReadWrite | CFile::shareDenyNone, &fileException) ==0)
		{
			char debug[1024];
			fileException.GetErrorMessage(debug, 1024);
			TRACE("debug = %s\n",debug);
			return ; 

		}

		tempConfigFile.Seek(0, CFile::end);
		tempConfigFile.WriteString(strChangedInfo);
		tempConfigFile.Close();

	}
	catch(CException *e)
	{
		//AfxMessageBox("환경 파일(version.txt)을 확인하십시오. Error = 1");
		utvERROR(("환경 파일(version.txt)을 확인하십시오. Error = 1"));
		char debug[1024];
		e->GetErrorMessage(debug,1024);
		TRACE("exception 메세지 = %s\n", debug);
		e->Delete();
	}
	catch(...)
	{
		//AfxMessageBox("환경 파일(version.txt)을 확인하십시오. Error = 2");
		utvERROR(("환경 파일(version.txt)을 확인하십시오. Error = 2"));
	}
}

/*----------------------------------------------------------------------------*/
//   config/config.ini 파일로 부터 ftp 접속 정보 가져옴.                      //
/*----------------------------------------------------------------------------*/
void AutoUpdateDlg::GetFTPInfo()
{
	/////////////////////////////////////////////////////////////////////
	//Modified by jwh184 2002-03-06
	CString ipAddress, userId, passwd, version, ipAddress2;
	ciUShort port=21, sHttpPort=8080;
	m_pDocument->GetFTPInfo(userId, passwd, ipAddress, ipAddress2, port, version, sHttpPort);

	m_strID = userId;
	m_strPassword = passwd;
	m_strIpAddr = ipAddress;	
	m_strIpAddr2 = ipAddress2;	
	m_strVersion = version;
	//if(!GetUseHttp())
	if(!m_bHttpMode)
	{
		utvDEBUG(("It's not HTTP mode, Port = %d", m_port));
		m_port = port;
	}
	else
	{
		utvDEBUG(("It's HTTP mode, Port = %d", m_port));
		m_port = sHttpPort;

		//Http 접속테스트
		/////////////////////////////////////////////////////////
		//Added by jwh184 2012-06-27 Http 모드임에도 연결이 실패하면 ftp로 다운로드 하도록 함.
		if(!HttpConnectTest())
		{
			utvDEBUG(("Http connect fail ==> Change to FTP mode"));
			m_bHttpMode = false;
			m_port = port;
		}//if
		/////////////////////////////////////////////////////////
	}//if
	/////////////////////////////////////////////////////////////////////

	// version 파일경로 설정
	if (m_pDocument->GetServerFlag()) {
		m_strVersionSrcName = VERSION_SERVER_SRC_NAME;
		m_strVersionDstName = VERSION_SERVER_DST_NAME;
		m_strVersionDstTmpName = VERSION_SERVER_DST_NAME_TMP;

		m_strReleaseSrcName = RELEASE_SERVER_SRC_NAME;
		m_strReleaseDstName = RELEASE_SERVER_DST_NAME;
		m_strReleaseDstTmpName = RELEASE_SERVER_DST_NAME_TMP;
	} else {
		m_strVersionSrcName = VERSION_SRC_NAME;
		m_strVersionDstName = VERSION_DST_NAME;
		m_strVersionDstTmpName = VERSION_DST_NAME_TMP;

		m_strReleaseSrcName = RELEASE_SRC_NAME;
		m_strReleaseDstName = RELEASE_DST_NAME;
		m_strReleaseDstTmpName = RELEASE_DST_NAME_TMP;
	}
}

BOOL AutoUpdateDlg::aFileDownload(FtpUtil *pFtpUtil, VERSION_INFO _versioninfo, int nErrorTime/* = 1000*600 */)
{
	// 파일한개 다운로드 받는데 10분이상 걸리면 에러로 생각하고 종료하는 타이머 생성
	SetTimer(TIMER_ERROR, nErrorTime, NULL);

	BOOL	retval = false;
	TCHAR szErr[1024];

	retval = _aFileDownload(pFtpUtil,_versioninfo);

	KillTimer(TIMER_ERROR);
	return retval;
}

BOOL AutoUpdateDlg::_aFileDownload(FtpUtil *pFtpUtil, VERSION_INFO _versioninfo)
{
	if(!pFtpUtil->FileFind(_versioninfo.srcname.GetBuffer())){
		utvERROR(("원격지 파일을 찾을 수 없습니다."));
		return false;
	}
	utvDEBUG((" - _versioninfo.srcname = [%s]", _versioninfo.srcname));

	DWORD fileSize = pFtpUtil->FileSize(_versioninfo.srcname.GetBuffer());

	CString fileFullPath  = m_strDST_DIR + _versioninfo.dstname;
	CString strDestDir = m_strDST_DIR + _versioninfo.dirname;	

	// 해당 폴더가 없으면 폴더 생성.
	CFileFind ff;
	if(!ff.FindFile(strDestDir)) {
		ciFileUtil::CreateDir(strDestDir.GetBuffer(0));
	}

	if(fileSize<=0){
		utvWARN(("fileSize 가 0 입니다.[%s]",_versioninfo.srcname));
		return ciTrue;
	}

	m_ProgressBar.SetShowText(TRUE);
	m_ProgressBar.SetRange(0, 100);

	FtpObserver observer;
	pFtpUtil->AttachObserver(&observer);

	UINT	nByteRead = 0;
	DWORD	nSumOfnByteRead = 0;
	BOOL bDownloadOK = false;
/*1
	if (pFtpUtil->FileDownload(_versioninfo.srcname.GetBuffer(), 
						(LPSTR)(LPCSTR)(m_strDST_DIR + _versioninfo.dstname), 
						strDestDir.GetBuffer()))
	{
		utvDEBUG((" - File download success [%s]",_versioninfo.srcname.GetBuffer()));
		bDownloadOK = true;
	} else {
		utvERROR((" - File download failed [%s]",_versioninfo.srcname.GetBuffer()));
	}
/*2*/
	FTP_THREAD_PARAM* pThread_param = new FTP_THREAD_PARAM;
	pThread_param->srcname = _versioninfo.srcname;

	// DOWNLOAD_TMP_DIRECTORY 사용 코드
	// 해당 폴더가 없으면 폴더 생성.
	CString strTempDestDir = m_strDST_DIR+"\\"+DOWNLOAD_TMP_DIRECTORY+_versioninfo.dirname;
	CFileFind tempFF;
	if(!tempFF.FindFile(strTempDestDir)) {
		ciFileUtil::CreateDir(strTempDestDir.GetBuffer());
	}
	pThread_param->dstname = m_strDST_DIR+"\\"+DOWNLOAD_TMP_DIRECTORY+_versioninfo.dstname;

	//pThread_param->dstname = m_strDST_DIR + _versioninfo.dstname;
	pThread_param->dirname = strDestDir;
	pThread_param->util = pFtpUtil;
	pThread_param->bComplete = false;

	CWinThread* ftpThread = AfxBeginThread(FTPDownloadThread, pThread_param);
	ftpThread->m_bAutoDelete = FALSE; // 스레드가 자동으로 지워지지 않게 설정

	ms_FTPDownloadActive = TRUE;
	while(ms_FTPDownloadActive && nSumOfnByteRead < fileSize) 
	{
		::Sleep(10);
		nSumOfnByteRead = observer.GetBytesReceived();

		// 하나의 파일을 100% 으로 보았을때 현재까지의 다운로드 %
		CString str;
		int nPos = nSumOfnByteRead * 100 / fileSize;
		if(nPos > 100) nPos =100;
		str.Format("%d%%", nPos);
		m_ProgressBar.SetWindowText(str);
		m_ProgressBar.SetPos(nPos);

		if(nSumOfnByteRead >= fileSize)
		{
			bDownloadOK = true;
			break;
		}		
	}
	ms_FTPDownloadActive = FALSE;
	//DWORD dwStartTick = ::GetTickCount();
	while(!pThread_param->bComplete)
	{
		//if (::WaitForSingleObject(ftpThread->m_hThread, 5000) == WAIT_TIMEOUT)
		if (::WaitForSingleObject(ftpThread->m_hThread, 500) == WAIT_TIMEOUT)
		{
			//utvWARN(("WAIT_TIMEOUT in FTP Download Thread!!!"));
			utvWARN(("Waiting complete FTP Download Thread!!!"));
		}//if
	}//while
	//DWORD dwEndTick = ::GetTickCount() - dwStartTick;
	//utvDEBUG(("Thread end wait %d tic", dwEndTick));
	delete ftpThread; // 스레드를 수동으로 지움
/*e*/
	pFtpUtil->DetachObserver(&observer);
	
	if(!bDownloadOK){
		utvERROR(("file broken!!! [%s]",_versioninfo.srcname));
	}
	return bDownloadOK;
}

UINT AutoUpdateDlg::FTPDownloadThread(LPVOID pParam)
{
	FTP_THREAD_PARAM* pThreadParam = (FTP_THREAD_PARAM*)pParam;

	// 프로세스 작업
	FtpUtil* util = pThreadParam->util;
	if (util->FileDownload(pThreadParam->srcname, 
						pThreadParam->dstname, 
						pThreadParam->dirname))
	{
		utvDEBUG((" - File download success [%s]",pThreadParam->srcname));
	} else {
		utvERROR((" - File download failed [%s]",pThreadParam->srcname));
		// 파일 전송량 체크 루프에서 벗어남
		ms_FTPDownloadActive = FALSE;
	}

	pThreadParam->bComplete = true;
	delete pThreadParam;
	return 0;
}


/////////////////////////////////////////////////////////////////////////
//Added by jwh184 2002-0305
/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Http를 이용하여 파일을 다운로드 하는 함수 \n
/// @param (CFileServiceWrap) *pHttpFile : (in) Http 파일전송 객체 포인터
/// @param (VERSION_INFO) _versioninfo : (in) 다운로드할 파일의 정보
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
BOOL AutoUpdateDlg::HttpFileDownload(CFileServiceWrap *pHttpFile, VERSION_INFO _versioninfo)
{
	//파일의 존재여부
	if(pHttpFile == NULL || !pHttpFile->IsExist(_versioninfo.srcname))
	{
		utvERROR(("원격지 파일을 찾을 수 없습니다."));
		return FALSE;
	}//if

	utvDEBUG((" - _versioninfo.srcname = [%s]", _versioninfo.srcname));

	//파일의 크기
	CFileServiceWrap::SFileInfo info;
	if(!pHttpFile->FileInfo(_versioninfo.srcname, info))
	{
		utvERROR(("Can't get file[%s] size : %s", _versioninfo.srcname, pHttpFile->GetErrorMsg()));
		return FALSE;
	}//if

	if(_atoi64(info.strLength) <= 0)
	{
		utvWARN(("fileSize 가 0 입니다.[%s]", _versioninfo.srcname));
		return TRUE;
	}//if

	m_ProgressBar.SetShowText(TRUE);
	m_ProgressBar.SetRange(0, 100);

	CString strLocalPath = m_strDST_DIR + "\\" + DOWNLOAD_TMP_DIRECTORY + _versioninfo.dstname;
	utvDEBUG(("Src file = %s, Local file = %s", _versioninfo.srcname, strLocalPath)); 
	if(!pHttpFile->GetFile(_versioninfo.srcname, strLocalPath, 0, this))
	{
		utvERROR(("Fail to download file[%s] : %s", _versioninfo.srcname, pHttpFile->GetErrorMsg()));
		return FALSE;
	}//if
	
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Http FileService 진행상태 표시 \n
/// @param (CFileServiceWrap::FS_PROGRESS_FLAG) nFlag : (in) 파일전송 상태 값
/// @param (ULONGLONG) uCurrentBytes : (in) 현재 진행된 파일의 사이즈
/// @param (ULONGLONG) uTotalBytes : (in) 현재 진행중인 파일의 전체 사이즈
/// @param (CString) strLocalPath : (in/) 현재 징행중인 파일의 로컬 경로
/////////////////////////////////////////////////////////////////////////////////
void AutoUpdateDlg::ProgressEvent(CFileServiceWrap::FS_PROGRESS_FLAG nFlag, ULONGLONG uCurrentBytes, ULONGLONG uTotalBytes, CString strLocalPath)
{
	if(nFlag != CFileServiceWrap::FS_PROGRESS_FLAG::FS_DOING)
	{
		return;
	}//if

	// 하나의 파일을 100% 으로 보았을때 현재까지의 다운로드 %
	int nPos = uCurrentBytes * 100 / uTotalBytes;
	if(nPos > 100)
	{
		nPos = 100;
	}//if

	CString strText;
	strText.Format("%d%%", nPos);

	m_ProgressBar.SetWindowText(strText);
	m_ProgressBar.SetPos(nPos);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 두개의 주소로 웹서버에 연결 테스트를 한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool AutoUpdateDlg::HttpConnectTest()
{
	//if(!GetUseHttp())
	if(!m_bHttpMode)
	{
		return true;
	}//if

	//첫번째주소로 웹서버에 연결테스트...
	m_pHttpFile = new CFileServiceWrap(m_strIpAddr, m_port);
	if(m_pHttpFile->Login(m_strID, m_strPassword))
	{
		if(m_pHttpFile->Test())
		{
			utvDEBUG(("Http test  success on first address(%s), port(%d)", m_strIpAddr, m_port));
			return true;
		}//if
		utvDEBUG(("Fail to http test on first address(%s), port(%d) : %s", m_strIpAddr, m_port, m_pHttpFile->GetErrorMsg()));
	}
	else
	{
		utvDEBUG(("Fail to http login on first address(%s), port(%d) : %s", m_strIpAddr, m_port, m_pHttpFile->GetErrorMsg()));
	}//if

	//두번째 주소로 웹서버 연결테스트...
	if(m_pHttpFile->SetHttpServerInfo(m_strIpAddr2, m_port))
	{
		if(m_pHttpFile->Login(m_strID, m_strPassword))
		{
			if(m_pHttpFile->Test())
			{
				utvDEBUG(("Http test  success on second address(%s), port(%d)", m_strIpAddr2, m_port));
				return true;
			}//if
		}
		else
		{
			utvDEBUG(("Fail to http login on second address(%s), port(%d)", m_strIpAddr2, m_port));
		}//if
	}//if
	utvDEBUG(("Fail to http test on second address(%s), port(%d)", m_strIpAddr2, m_port));

	return false;
}
/////////////////////////////////////////////////////////////////////////

void AutoUpdateDlg::OnTimer(UINT nIDEvent)
{
	switch(nIDEvent)
	{
		case TIMER_ERROR:
			KillTimer(TIMER_ERROR);
			//AfxMessageBox("서버와의 통신상태가 양호하지 않습니다. \n잠시 후 다시 실행 해 주십시요.");
			//utvERROR(("서버와의 통신상태가 양호하지 않습니다. \n잠시 후 다시 실행 해 주십시요."));
			utvERROR(("Network conditions are too poor - Terminate update"));
			exit(0);
			//utvERROR(("서버와의 통신상태가 양호하지 않습니다."));
			break;

		case TIMER_CHECK_MINIMIZE:
			KillTimer(TIMER_CHECK_MINIMIZE);
			{
				HWND hwnd1 = scratchUtil::getInstance()->getWHandle("UTV_brwClient2.exe");
				HWND hwnd2 = scratchUtil::getInstance()->getWHandle("UTV_brwClient2_UBC1.exe");

				bool topmost = true;
				if( hwnd1 && (GetWindowLong(hwnd1, GWL_STYLE) & WS_CAPTION) == NULL )
				{
					topmost = false;
				}
				if( hwnd1 && (GetWindowLong(hwnd1, GWL_STYLE) & WS_CAPTION) == NULL )
				{
					topmost = false;
				}

				if( topmost )
				{
					SetWindowPos(&wndTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);
				}
				else
				{
					SetWindowPos(&wndNoTopMost, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE);

					if( hwnd1 ) ::SetForegroundWindow(hwnd1);
					if( hwnd2 ) ::SetForegroundWindow(hwnd2);
				}
			}
			SetTimer(TIMER_CHECK_MINIMIZE, 1000, NULL);  // next 1sec
			break;
	}

	CDialog::OnTimer(nIDEvent);
}


BOOL
AutoUpdateDlg::isService(CString& filename)
{
	if(filename.Compare("UBCFirmwareView.exe")==0 || filename.Compare("Firmware_v7.swf")==0 ){
		utvDEBUG(("isService(%s)==UBCFirmwareView.exe", filename));
		return TRUE;
	}
	return FALSE;
	/*
	// 해당 프로세스가 XYNTService 로 등록되어있는가 살핀다.
	utvDEBUG(("isService(%s)", filename));

	// XYNTService.ini file 에 있다면, 등록되어 있는 것이다.

	CString ini=GetDstDir();
	ini.Append("\\execute\\XYNTService.ini");

	for(int i=0;;i++){

		char entryName[20];
		memset(entryName,0x00,20);
		sprintf(entryName,"Process%d", i);

		char buf[1024];
		memset(buf,0x00,1024);

		GetPrivateProfileString(entryName,
								"CommandLine",
								"",
								buf,
								1024,
								ini);

		if(strlen(buf)==0) break;
		CString Cbuf = buf;
		CString strTargetFileName = Cbuf.Mid(Cbuf.ReverseFind('\\')+1);	//파일명 추출
		if(strncmp(filename,strTargetFileName,filename.GetLength())==0){
			utvDEBUG(("%s is Widows Service", filename));
			return true;
		}
	}
	return false;
	*/
}

BOOL
AutoUpdateDlg::TerminateService()
{
	// XYNTService 를 내린다.
	utvDEBUG(("TerminateService()"));

	// WinExec 는 무조건 성공하기 때문에 실제로 죽였는지 알수 없다.
	// 따라서 먼저 떠 있는지 체크한다.

	unsigned long pid = scratchUtil::getInstance()->getPid("UBCFirmwareView.exe");
	if(pid<=0){
		utvDEBUG(("UBCFirmwareView is not alive"));
		return ciFalse;
	}

	CString strExecute="kill UBCFirmwareView";
	int nRet = WinExec(strExecute, SW_NORMAL);
	if(nRet < 31) {
		utvDEBUG(("%s run fail", strExecute));
		return false;
	}
	utvDEBUG(("%s run succeed", strExecute));
	return true;
	/*
	return  KillService("UBCService");
	*/
}

BOOL
AutoUpdateDlg::RunService()
{
	// XYNTService 를 가동한다.

	
	utvDEBUG(("RunService()"));
	CString strExecute="UBCFirmwareView.exe";
	int nRet = WinExec(strExecute, SW_NORMAL);
	if(nRet < 31) {
		utvDEBUG(("%s run fail", strExecute));
		return false;
	}
	utvDEBUG(("%s run succeed", strExecute));
	return true;

	/*
	::Sleep(5000);
	return RunService("UBCService",0,NULL);
	*/
	//BounceProcess("UBCService",0);
	//BounceProcess("UBCService",1);

	return ciTrue;
}

BOOL AutoUpdateDlg::RunService(char* pName, int nArg, char** pArg) 
{ 
	// run service with given name
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS); 
	if (schSCManager==0) 
	{
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "OpenSCManager failed, error code = %d", nError);
		utvDEBUG((pTemp));
	}
	else
	{
		// open the service
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0) 
		{
			long nError = GetLastError();
			char pTemp[121];
			sprintf(pTemp, "OpenService failed, error code = %d", nError);
			utvDEBUG((pTemp));
		}
		else
		{
			// call StartService to run the service
			if(StartService(schService,nArg,(const char**)pArg))
			{
				CloseServiceHandle(schService); 
				CloseServiceHandle(schSCManager); 
				utvDEBUG(("Start Service succeed"));
				return TRUE;
			}
			else
			{
				long nError = GetLastError();
				char pTemp[121];
				sprintf(pTemp, "StartService failed, error code = %d", nError);
				utvDEBUG((pTemp));
			}
			CloseServiceHandle(schService); 
		}
		CloseServiceHandle(schSCManager); 
	}
	return FALSE;
}


BOOL 
AutoUpdateDlg::BounceProcess(char* pName, int nIndex) 
{ 
	// bounce the process with given index
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS); 
	if (schSCManager==0) 
	{
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "OpenSCManager failed, error code = %d", nError);
		utvDEBUG((pTemp));
	}
	else
	{
		// open the service
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0) 
		{
			long nError = GetLastError();
			char pTemp[121];
			sprintf(pTemp, "OpenService failed, error code = %d", nError); 
			utvDEBUG((pTemp));
		}
		else
		{
			// call ControlService to invoke handler
			SERVICE_STATUS status;
			if(nIndex>=0&&nIndex<128)
			{
				if(ControlService(schService,(nIndex|0x80),&status))
				{
					CloseServiceHandle(schService); 
					CloseServiceHandle(schSCManager); 
					utvDEBUG(("BOUCE SUCCEED"));
					return TRUE;
				}
				long nError = GetLastError();
				char pTemp[121];
				sprintf(pTemp, "ControlService failed, error code = %d", nError); 
				utvDEBUG((pTemp));
			}
			else
			{
				char pTemp[121];
				sprintf(pTemp, "Invalid argument to BounceProcess: %d", nIndex); 
				utvDEBUG((pTemp));
			}
			CloseServiceHandle(schService); 
		}
		CloseServiceHandle(schSCManager); 
	}
	return FALSE;
}

BOOL 
AutoUpdateDlg::KillService(char* pName) 
{ 
	// kill service with given name
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS); 
	if (schSCManager==0) 
	{
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "OpenSCManager failed, error code = %d", nError);
		utvDEBUG((pTemp));
	}
	else
	{
		// open the service
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0) 
		{
			long nError = GetLastError();
			char pTemp[121];
			sprintf(pTemp, "OpenService failed, error code = %d", nError);
			utvDEBUG((pTemp));
		}
		else
		{
			// call ControlService to kill the given service
			SERVICE_STATUS status;
			if(ControlService(schService,SERVICE_CONTROL_STOP,&status))
			{
				CloseServiceHandle(schService); 
				CloseServiceHandle(schSCManager); 
				utvDEBUG(("BOUNCE SUCCEED"));
				return TRUE;
			}
			else
			{
				long nError = GetLastError();
				char pTemp[121];
				sprintf(pTemp, "ControlService failed, error code = %d", nError);
				utvDEBUG((pTemp));
			}
			CloseServiceHandle(schService); 
		}
		CloseServiceHandle(schSCManager); 
	}
	return FALSE;
}

BOOL AutoUpdateDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	if (m_pDocument) {		
		switch(pCopyDataStruct->dwData)
		{		
		case 10: // File AutoUpdate Succeed
			{
				CString fileName = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("OnCopyData Succeed(%s)", fileName.GetBuffer(0)));
				if (fileName == STARTER_INI_NAME) {
					// STARTER_INI_NAME 파일을 리로딩
					utvDEBUG(("%s reloading...", STARTER_INI_NAME));
					m_pDocument->Reload();
				}
				break;
			}
		case 20: // File AutoUpdate Failed
			{
				CString fileName = (LPCSTR)pCopyDataStruct->lpData;
				utvDEBUG(("OnCopyData Failed(%s)", fileName.GetBuffer(0)));
				// 등록된 어플리케이션 정보들을 획득
				APPLICATION_LIST appList;
				if (m_pDocument && !m_pDocument->GetApplicationsInfo(appList)) {
					utvDEBUG(("Application List size is zero."));
					// 등록된 어플리케이션 정보가 없을 경우 해당 프로세스만 종료
					if (IsAliveProcess(fileName)) TerminateProcess(fileName);
					break;
				}

				// 런타임 업데이트 플래그가 설정된 프로세스 리스트 확보
				// m_ForcedUpdateFlag 가 설정되어 있다면 무조건 런타임 업데이트 가능
				int count = appList.GetSize();
				CStringList appRTU; // runtime update passible
				for(int i=0; i<count; ++i)
				{
					APPLICATION_INFO& info = appList.GetAt(i);
					appRTU.AddTail(::GetBinaryName(info.binaryName));
				}

				// 실행파일이라면 해당 프로세스 종료, DLL이라면 모든 프로세스 종료
				// 업데이트 대상 파일중에 DLL, config, ini 파일이 포함되어있는지 확인

				// 1. exe 파일의 업데이트라면 해당 프로세스만 재기동
				// 2. config, init 파일의 업데이트라면 모든 프로세스 종료
				CString strExt = fileName.Mid(fileName.GetLength()-3,3);
				if ( strExt == "dll" || strExt == "ini" || strExt == "cfg" || strExt == "ocx")
				{
					// 런타임 업데이트 플래그에 상관없이 모든 관리 프로세스 종료
					POSITION pos = appRTU.GetHeadPosition();
					while(pos != NULL)
					{
						CString processName = appRTU.GetNext(pos);
						if (IsAliveProcess(processName)) TerminateProcess(processName);
					}
				} else {
					if (IsAliveProcess(fileName)) TerminateProcess(fileName);
				}
				break;
			}	
		default:
			break;
		}
	}
	return CDialog::OnCopyData(pWnd, pCopyDataStruct);
}



void AutoUpdateDlg::OnDestroy()
{
	if(m_pHttpFile)
	{
		delete m_pHttpFile;
		m_pHttpFile = NULL;
	}//if

	////////////////////////////////////////////////
	//Added by jwh184 2012-03-12
	//if(GetUseHttp())
	if(m_bHttpMode)
	{
		CoUninitialize();
	}//if
	////////////////////////////////////////////////

	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}
