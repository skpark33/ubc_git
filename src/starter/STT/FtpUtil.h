
#ifndef __TFTP_DEF__
#define __TFTP_DEF__

#include "libFTPClient/FTPClient.h"
using namespace nsFTP;

class FtpUtil
{
public:

	
	/////////////////////////////////////////////////////////////////////////
	//Added by jwh184 2002-0305
	FtpUtil();				///<기본 생성자
	/////////////////////////////////////////////////////////////////////////
	FtpUtil(LPCSTR user, LPCSTR pass, LPCSTR address,LPCSTR address2, unsigned short port = 21);
	virtual ~FtpUtil();

public:
	/////////////////////////////////////////////////////////////////////////
	//Added by jwh184 2002-0305
	void SetAccount(LPCSTR user, LPCSTR pass, LPCSTR address,LPCSTR address2, unsigned short port = 21);	///<FTP 연결을 위한 계정등 설정함수
	/////////////////////////////////////////////////////////////////////////

	BOOL Connect(int ftpOrder);
	void Disconnect();
	BOOL FileDownload(LPCSTR remoteFile, LPCSTR localFile, LPCSTR localDir);
	BOOL FileUpload(LPCSTR localFile, LPCSTR remoteFile);
	void Passive(BOOL passiveFlag = true);

	BOOL ChangeWorkingDirectory(LPCSTR pathname);
	BOOL _ChangeWorkingDirectory(LPCSTR pathname);
	BOOL FileFind(LPCSTR srcname);
	long FileSize(LPCSTR srcname);
	void AttachObserver(CFTPClient::CNotification* pObserver);
	void DetachObserver(CFTPClient::CNotification* pObserver);

public:
	CFTPClient*       m_pFTPClient;

	CString m_strAddress;
	CString m_strAddress2;
	CString m_strName;
	CString m_strPassword;
	unsigned short m_port;

	BOOL m_passiveFlag;
};


#endif
