/*! \class ApplicationInfo
 *  Copyright ⓒ 2002, SQI soft. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#ifndef _ApplicationInfo_h_
#define _ApplicationInfo_h_

#include "stdafx.h"
#include "COP/ciSyncUtil.h"

class ApplicationInfo
{
public:
	ApplicationInfo();
	ApplicationInfo(const char* procName);
	virtual ~ApplicationInfo();

	//bool fromMap(ciXPropertyMap& procMap);

	CString&	get_appId() { ciGuard aGuard(_lock);	return _appId; }
	CString&	get_binaryName() { ciGuard aGuard(_lock);	return _binaryName; }
	CString&	get_argument() { ciGuard aGuard(_lock);	return _argument; }
	CString&	get_properties() { ciGuard aGuard(_lock);	return _properties; }
	CString&	get_debug() { ciGuard aGuard(_lock);	return _debug; }
	CString&	get_status() { ciGuard aGuard(_lock);	return _status; }
	CString&	get_startTime() { ciGuard aGuard(_lock);	return _startTime; }
	CString&	get_runningType() { ciGuard aGuard(_lock);	return _runningType; }
	unsigned long&	get_pid() { ciGuard aGuard(_lock);	return _pid; }
	bool&		get_autoStartUp() { ciGuard aGuard(_lock);	return _autoStartUp; }
	bool&		get_runTimeUpdate() { ciGuard aGuard(_lock);	return _runTimeUpdate; }
	CString&	get_preRunningApp() { ciGuard aGuard(_lock);	return _preRunningApp; }
	bool&		get_isPrimitive() { ciGuard aGuard(_lock);	return _isPrimitive; }
	bool&		get_monitored() { ciGuard aGuard(_lock);	return _monitored; }
	short&		get_initSleepSecond() { ciGuard aGuard(_lock);	return _initSleepSecond; }
	bool&		get_adminState() { ciGuard aGuard(_lock);	return _adminState; }

	void set_appId( CString p_appId) 			{ ciGuard aGuard(_lock);_appId=p_appId;}
	void set_binaryName( CString p_binaryName) 		{ ciGuard aGuard(_lock);_binaryName=p_binaryName;}
	void set_argument( CString p_argument) 			{ ciGuard aGuard(_lock);_argument=p_argument;}
	void set_properties( CString p_properties) 		{ ciGuard aGuard(_lock);_properties=p_properties;}
	void set_debug( CString p_debug) 			{ ciGuard aGuard(_lock);_debug=p_debug;}
	void set_status( CString p_status) 			{ ciGuard aGuard(_lock);_status=p_status;}
	void set_startTime( CString p_startTime) 		{ ciGuard aGuard(_lock);_startTime=p_startTime;}
	void set_runningType( CString p_runningType) 		{ ciGuard aGuard(_lock);_runningType=p_runningType;}
	void set_pid( unsigned long  p_pid) 			{ ciGuard aGuard(_lock);  unsigned long  _pid=p_pid;}
	void set_autoStartUp( bool p_autoStartUp) 		{ ciGuard aGuard(_lock);  _autoStartUp=p_autoStartUp;}
	void set_runTimeUpdate( bool p_runTimeUpdate) 		{ ciGuard aGuard(_lock);  _runTimeUpdate=p_runTimeUpdate;}
	void set_preRunningApp( CString p_preRunningApp) 	{ ciGuard aGuard(_lock);_preRunningApp=p_preRunningApp;}
	void set_isPrimitive( bool p_isPrimitive) 		{ ciGuard aGuard(_lock);  _isPrimitive=p_isPrimitive;}
	void set_monitored( bool p_monitored) 			{ ciGuard aGuard(_lock);  _monitored=p_monitored;}
	void set_initSleepSecond( short p_initSleepSecond) 	{ ciGuard aGuard(_lock);  _initSleepSecond=p_initSleepSecond;}
	void set_adminState( bool p_adminState) 		{ ciGuard aGuard(_lock);  _adminState=p_adminState;}

protected:

	CString			_appId;			// 어플리케이션 아이디
	CString			_binaryName;		// 실행 파일 이름
	CString			_argument;		// 입력 인수
	CString			_properties;		// CORBA 인수
	CString			_debug;			// 디버깅 인수
	CString			_status;			// 어플리케이션 상태
	CString			_startTime;		// 기동시각
	CString			_runningType;	// 실행 방법
	unsigned long   _pid;			// 프로세스 아이디
	bool			_autoStartUp;	// 자동 재시동 여부
	bool			_runTimeUpdate;	// 자동 업데이트 여부
	CString			_preRunningApp;	// 선행되어야 하는 어플리케이션 아이디
	bool			_isPrimitive;	// 운용상 반드시 필요한 어플리케이션
	bool			_monitored;		// 모니터링 여부
	short			_initSleepSecond;// 기동전 대기 시간
	bool			_adminState;		// 관리자 On, Off 플래그

	ciMutex			_lock;		



};

#endif //_ApplicationInfo_h_

