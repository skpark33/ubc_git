// STTDlg.h : 헤더 파일
//

#pragma once
#include "COP/ciAceType.h"

#include "afxwin.h"
#include "afxcmn.h"
#include "Control/CheckListCtrl.h"
#include "Control/HoverButton.h"
#include "Control/GradientStatic.h"
#include "STTDoc.h"

// CSTTDlg 대화 상자
class CSTTDlg : public CDialog
{

// 생성입니다.
public:
	CSTTDlg(BOOL update = false, CWnd* pParent = NULL);	// 표준 생성자입니다.
	virtual ~CSTTDlg(); // 소멸자

// 대화 상자 데이터입니다.
	enum { IDD = IDD_STT_DIALOG };

	STTDoc* GetDocument() const;
	void	SetDocumnet(STTDoc* document);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.
	virtual BOOL PreTranslateMessage(MSG* pMsg);

// 구현입니다.
	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	//afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	static UINT ThreadCallFunc(LPVOID pParam); // 스레드 함수
	static UINT doAutoUpdate(LPVOID pParam);


	bool _init();
	void _initTray();

protected:
	HICON m_hIcon;

	CHoverButton	m_btnRegister;
	CHoverButton	m_btnStart;
	CHoverButton	m_btnStop;
	CHoverButton	m_btnWatch;
	CHoverButton	m_btnAutoUpdate;
	CGradientStatic m_staticTitle;
	CCheckListCtrl	m_listctrlProcess;

	STTDoc* m_pDocument;
	BOOL	m_hasUpdate;
	BOOL	m_update; // 자동업데이트 기능을 사용

public:	
	bool	InsertItem(APPLICATION_INFO& info, int index=-1);
	bool	DeleteItem(LPCSTR lpszAppName);
	bool	UpdateProcess(APPLICATION_INFO& info);
	void	RePosControl(int cx, int cy);

	bool	ReloadProcessInfo();
	bool	GetApplicationInfo(LPCSTR lpszAppName, APPLICATION_INFO& app_info);
	bool	GetApplicationNameList(CArray<CString, CString&>& appNameList);

	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedNew();
	afx_msg void OnBnClickedStart();
	afx_msg void OnBnClickedStop();
	afx_msg void OnBnClickedWatch();
	afx_msg void OnBnClickedAutoUpdate();
	afx_msg void OnNMRclickListProcess(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnProcessRegister();
	afx_msg void OnProcessModify();
	afx_msg void OnProcessDelete();
	afx_msg void OnProcessDetailInfo();
	afx_msg LRESULT OnRefresh(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg LONG TrayIconMessage(WPARAM wParam,LPARAM lParam);
	afx_msg LONG TrayIconUpdate(WPARAM wParam,LPARAM lParam);
	afx_msg void OnTrayOpen();
	afx_msg void OnTrayClose();
	afx_msg void OnTrayAbout();	
	afx_msg void OnTrayUpdate();
	afx_msg void OnDebugOnOff();
	afx_msg void OnUpdateDebugOnOff(CCmdUI *pCmdUI);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	static	CString m_strCustomerId;
	static	CString m_strScope;

	static	bool CheckConfigVariables();
};
