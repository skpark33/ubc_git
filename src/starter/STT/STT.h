// STT.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.
#include "STTDoc.h"

// CSTTApp:
// 이 클래스의 구현에 대해서는 STT.cpp을 참조하십시오.
//


class CSTTApp : public CWinApp
{
public:
	CSTTApp();

// 재정의입니다.
	public:
	virtual BOOL InitInstance();
	virtual BOOL InitAutoUpdate(STTDoc* document);
	BOOL Authentication();


	STTDoc*	getDoc() { return _doc; }

// 구현입니다.

	DECLARE_MESSAGE_MAP()

	STTDoc* _doc;
	BOOL _update_only;
	BOOL _isServer;

protected:
	unsigned long _getpid(const char* exename);
	unsigned long _getProcessCount(const char* exename);
	void	_fixSiteIdBugInStarterArg();
public:
	virtual int ExitInstance();
};

extern CSTTApp theApp;