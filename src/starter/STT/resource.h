//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by STT.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_STT_DIALOG                  102
#define IDS_VERSION_QUEST               102
#define IDD_AUTOUPDATE_DIALOG           103
#define IDS_VERSION_CHECK               103
#define IDI_ADMIN                       110
#define IDI_NAVIGATOR                   111
#define IDR_MAINFRAME                   128
#define IDR_STT                         128
#define IDI_HOST                        129
#define IDI_PROCESS                     130
#define IDB_BTN_UPPER                   131
#define IDB_BTN_ADD                     132
#define IDB_BTN_APPLY                   133
#define IDB_BTN_BOTTOM                  135
#define IDB_BTN_CANCEL                  136
#define IDB_BTN_CHANGE                  137
#define IDB_BTN_CLOSE                   138
#define IDB_BTN_DELETE                  139
#define IDB_BTN_EDITING                 140
#define IDB_BTN_HIGHRANK                144
#define IDB_BTN_LOWRANK                 146
#define IDB_BTN_MODIFY                  148
#define IDB_BTN_NEW_ADD                 149
#define IDB_BTN_OK                      150
#define IDB_BTN_PARAM                   151
#define IDB_BTN_REGISTRATION            152
#define IDB_BTN_SAVE                    154
#define IDB_BTN_SELECTION               156
#define IDB_BTN_SERVER                  157
#define IDB_BTN_START                   158
#define IDB_BTN_STATUS                  159
#define IDB_BTN_STOP                    160
#define IDB_PROCESSTREE                 163
#define IDB_CHECK_BOXES                 164
#define IDB_NAMENOTI                    165
#define IDD_PROCESS                     166
#define IDR_POPUP_MENU                  167
#define IDR_MENU_TRAY                   168
#define IDI_TRAY                        169
#define IDD_DIALOG_SPLASH               172
#define IDI_TRAY_UPDATE                 173
#define IDB_BITMAP1                     174
#define IDB_BTN_AUTOUPDATE              174
#define IDC_LIST_PROCESS                1004
#define IDC_START                       1005
#define IDC_WATCH                       1006
#define IDC_STOP                        1015
#define IDC_STATIC_VER                  1017
#define IDC_EDIT_PROCESS                1018
#define IDC_EDIT_PARAM                  1019
#define IDC_EDIT_PROP                   1020
#define IDC_COMBO_RUNTIMEUPDATE         1021
#define IDC_EDIT_DEBUG                  1022
#define IDC_COMBO_RUNNINGTYPE           1023
#define IDC_COMBO_AUTOSTART             1024
#define IDC_EDIT_STARTTIME              1025
#define IDC_NEW                         1026
#define IDC_EDIT_APPLICATION_ID         1026
#define IDC_EDIT_APP_ID                 1026
#define IDC_EDIT_PID                    1027
#define IDC_STATIC_TITLE                1033
#define IDC_BUTTON1                     1034
#define IDC_AUTOUPDATE                  1034
#define IDC_COMBO_PRERUNNINGAPP         1035
#define IDC_STATIC_VERSION              1035
#define IDC_COMBO_ISPRIMITIVE           1036
#define IDC_COMBO_MONITORED             1037
#define IDC_EDIT1                       1038
#define IDC_EDIT_INITSLEEPSECOND        1038
#define IDC_COMBO_ADMINFLAG             1039
#define IDC_COMBO_ADMINSTATE            1039
#define IDC_LOG_LIST                    1039
#define IDC_LIST_LOG                    1039
#define IDC_PROGRESS_BAR                1041
#define IDC_STATIC_TRANS_STATE          1042
#define IDC_STATIC_TRANS_FILENAME       1043
#define IDC_STATIC_TRANS_FILENAME2      1044
#define IDC_PROGRESSBAR_ALL             1046
#define ID_PROCESS_REGISTER             3278
#define ID_PROCESS_MODIFY               32782
#define ID_PROCESS_DELETE               32783
#define ID_PROCESS_DETAIL_INFO          32784
#define ID_TRAY_OPEN                    32787
#define ID_TRAY_CLOSE                   32788
#define ID__UTVSTARTER32789             32789
#define ID_MENU_ABOUT                   32790
#define ID_MENU_UPDATE                  32792
#define ID__DEBUG                       32795
#define ID_DEBUG_ON                     32796
#define ID__DEBUGON                     32797
#define ID__DEBUGON32798                32798
#define ID__CANCEL                      32799
#define ID__CANCEL32800                 32800
#define ID__32801                       32801

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        176
#define _APS_NEXT_COMMAND_VALUE         32802
#define _APS_NEXT_CONTROL_VALUE         1044
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
