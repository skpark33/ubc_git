// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// STT.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"


COLORREF DarkenColor(COLORREF col,double factor)
{
	if(factor>0.0&&factor<=1.0){
		BYTE red,green,blue,lightred,lightgreen,lightblue;
		red = GetRValue(col);
		green = GetGValue(col);
		blue = GetBValue(col);
		lightred = (BYTE)(red-(factor*red));
		lightgreen = (BYTE)(green-(factor*green));
		lightblue = (BYTE)(blue-(factor*blue));
		col = RGB(lightred,lightgreen,lightblue);
	}
	return(col);
}

COLORREF BrightenColor(COLORREF col,double factor)
{
	if(factor>0.0&&factor<=1.0){
		BYTE red,green,blue,lightred,lightgreen,lightblue;
		red = GetRValue(col);
		green = GetGValue(col);
		blue = GetBValue(col);
		lightred = (BYTE)(red+(factor*red));
		lightgreen = (BYTE)(green+(factor*green));
		lightblue = (BYTE)(blue+(factor*blue));
		col = RGB(lightred,lightgreen,lightblue);
	}
	return(col);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// Http를 사용하는지 여부를 구한다. \n
/// @return <형: bool> \n
///			<true: http 사용> \n
///			<false: http 사용하지 않음> \n
/////////////////////////////////////////////////////////////////////////////////
bool GetUseHttp()
{
	char szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(szModule, cDrive, cPath, cFilename, cExt);

	//Http 사용 여부
	CString strPath;
	strPath.Format(_T("%s%s\\data\\UBCVariables.ini"), cDrive, cPath);
	char cBuffer[256] = { 0x00 };
	::GetPrivateProfileString(_T("ROOT"), _T("HTTP_ONLY"), _T(""), cBuffer, 256, strPath);
	CString strVal = cBuffer;
	//if(strcmp(cBuffer, "CLIENT") == 0)
	if(strVal.CompareNoCase("CLIENT") == 0)
	{
		return true;
	}//if

	return false;
}

CString GetBinaryName(LPCSTR lpszFullPath)
{
	char dir[MAX_PATH]={0}, path[MAX_PATH]={0}, fn[MAX_PATH]={0}, ext[MAX_PATH]={0};
	_splitpath(lpszFullPath, dir, path, fn, ext);

	CString ret;
	ret.Format("%s%s", fn, ext);

	return ret;
}

CString GetPath(LPCSTR lpszFullPath)
{
	char dir[MAX_PATH]={0}, path[MAX_PATH]={0}, fn[MAX_PATH]={0}, ext[MAX_PATH]={0};
	_splitpath(lpszFullPath, dir, path, fn, ext);

	CString ret;
	ret.Format("%s%s", dir, path);

	return ret;
}
