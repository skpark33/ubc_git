// CustomerSpecific.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AutoUpdateDlg.h"
#include "LogTrace.h"

#include "CustomerSpecific.h"

#include "COP/ciStringTokenizer.h"
#include "COP/ciStringUtil.h"
#include "UBC/ubcIni.h"
#include  <io.h>
// CCustomerSpecific

#define CUSTOMER_SPECIFIC_INI  "CustomerSpecific.ini"
#define CUSTOMER_SPECIFIC_LOG_INI  "CustomerSpecificLog.ini"
#define CUSTOMER_SPECIFIC_VERSION  "CustomerSpecificVersion.txt"
#define PROJECT_HOME			_COP_CD("C:\\SQISoft\\")
#define EXE_HOME			_COP_CD("C:\\SQISoft\\UTV1.0\\execute")

CCustomerSpecific* CCustomerSpecific::_instance = 0;
ciMutex CCustomerSpecific::_sMutex;

CCustomerSpecific*	CCustomerSpecific::getInstance()
{
	if(_instance == 0)
	{
		CI_GUARD(_sMutex, "GetInstance()");
		if(_instance == 0)
			_instance = new CCustomerSpecific;
	}
	return _instance;
}

void CCustomerSpecific::clearInstance()
{
	if(_instance)
	{
		CI_GUARD(_sMutex, "ClearInstance()");
		if (_instance) {
			delete _instance;
			_instance = NULL;
		}
	}
}


CCustomerSpecific::CCustomerSpecific()
:	m_strCustomer ( "" ), m_pDlg(0), m_hasSpecific(FALSE)
{
}
CCustomerSpecific::~CCustomerSpecific()
{
	Clear();
}
void CCustomerSpecific::Init(AutoUpdateDlg* dlg)
{
	utvDEBUG(("CCustomerSpecific::Init()"));

	ASSERT(dlg);	

	m_pDlg = dlg; 

	m_strHostId = m_pDlg->m_pDocument->GetHostID();
	if(m_strHostId.IsEmpty()){
		ubcIni aIni("UBCVariables",EXE_HOME,"data");
		aIni.get("CUSTOMER_INFO","NAME", m_strHostId);
		m_strCustomer = m_strHostId;
	}else{
		m_strCustomer = m_strHostId.Mid(0,m_strHostId.Find('-'));
	}

}

void CCustomerSpecific::Clear()
{
	ciGuard aGuard(m_mapLock);
	SpecificEntryMap::iterator itr;
	for(itr=m_map.begin();itr!=m_map.end();itr++) {
		delete itr->second;
	}
	m_map.clear();
}
BOOL CCustomerSpecific::Download()
{
	CString localIni;
	if(!_Download(CUSTOMER_SPECIFIC_INI, localIni)){
		return FALSE;
	}
	if(!_LoadIni(localIni)) {
		//DeleteFile(localIni);
		return FALSE;
	}
	//DeleteFile(localIni);

	CString localVersion;
	if(!_Download(CUSTOMER_SPECIFIC_VERSION, localVersion)){
		return FALSE;
	}
	if(!_LoadVersion(localVersion)) {
		return FALSE;
	}
	return TRUE;
}

BOOL CCustomerSpecific::_Download(LPCSTR filename, CString& localPath)
{
	utvDEBUG(("CCustomerSpecific::_Download(%s)", filename));

	if (m_strHostId.IsEmpty()) {
		utvERROR(("CCustomerSpecific::Host ID is empty."));
		return FALSE;
	}

	CString remotePath;
	if (m_pDlg->m_strVersion.IsEmpty()) {
		remotePath.Format("/%s", filename);
	} else {
		remotePath.Format("/%s/%s", m_pDlg->m_strVersion, filename);
	}

	localPath.Format("%s\\%s", m_pDlg->m_strDST_DIR, filename);

	utvDEBUG(("CCustomerSpecific::Download %s, RemotePath = %s, LocalPath = %s", filename, remotePath, localPath));

	///////////////////////////////////////////////////////////////////////
	//Modified by jwh184 2002-03-02
	//if(!GetUseHttp())
	if(!m_pDlg->m_bHttpMode)
	{
		return FALSE;
	}

	if(!m_pDlg->m_pHttpFile)
	{
		return FALSE;
	}//if

	if(!m_pDlg->m_pHttpFile->GetFile(remotePath, localPath, 0, NULL))
	{
		utvWARN(("CCustomerSpecific::GetFile %s failed (%s)", remotePath, m_pDlg->m_pHttpFile->GetErrorMsg()));
		return FALSE;	
	}//if
		
	utvDEBUG(("CCustomerSpecific::GetFile %s succeed",localPath));

	return TRUE;
}

BOOL CCustomerSpecific::_LoadIni(LPCSTR localPath)
{
	/*  CustomerSpecific.ini 구조
		[<cust>]
		specificHost=*,<hostid*>,,,,
		specificEntry=<s_entry_name>,,,,

		[s_entiry_name]
		targetPath=
		movePath=
		beforeMove=
		afterMove=
	*/
	m_hasSpecific = FALSE;
	ubcIni aIni(CUSTOMER_SPECIFIC_INI, PROJECT_HOME, "UTV1.0");

	ciString hostListStr;
	aIni.get(m_strCustomer, "specificHost", hostListStr);
	if(hostListStr.empty()) {
		utvDEBUG(("CCustomerSpecific::%s specificHost is empty", m_strCustomer));
		return FALSE;
	}
	utvDEBUG(("CCustomerSpecific::hostListStr=[%s]", hostListStr.c_str()));

	if(hostListStr[0] != '*') {
		ciStringTokenizer aTokens(hostListStr.c_str(),",");
		while(aTokens.hasMoreTokens()){
			ciString sHostId = aTokens.nextToken();
			if(m_strHostId.Find(sHostId.c_str()) >= 0) {
				m_hasSpecific = TRUE;
				break;
			}
		}
	}else{
		m_hasSpecific = TRUE;
	}
	
	if(!m_hasSpecific) {
		utvDEBUG(("CCustomerSpecific::%s host does not match with specificHost", m_strHostId));
		return FALSE;
	}

	utvDEBUG(("CCustomerSpecific::%s host match with specificHost", m_strHostId));

	ciString entryListStr;
	aIni.get(m_strCustomer, "specificEntry", entryListStr);
	utvDEBUG(("CCustomerSpecific::entryListStr=[%s]", entryListStr.c_str()));

	if(entryListStr.empty()) {
		utvDEBUG(("CCustomerSpecific::%s host does not has special download file entry", m_strHostId));
		return FALSE;
	}

	ciStringList entryList;
	ciStringTokenizer aTokens(entryListStr.c_str(),",");
	while(aTokens.hasMoreTokens()){
		entryList.push_back(aTokens.nextToken());
	}

	ciGuard aGuard(m_mapLock);
	int count = 0;
	ciStringList::iterator itr;
	for(itr=entryList.begin();itr!=entryList.end();itr++){
		SpecificEntry* aEntry = new SpecificEntry;
		aIni.get(itr->c_str(),"targetPath", aEntry->targetPath);
		if(!aEntry->targetPath.empty()) {
			aIni.get(itr->c_str(),"movePath", aEntry->movePath);
			aIni.get(itr->c_str(),"beforeMove", aEntry->beforeMove);
			aIni.get(itr->c_str(),"afterMove", aEntry->afterMove);
			m_map.insert(SpecificEntryMap::value_type(itr->c_str(),aEntry));
			utvDEBUG(("CCustomerSpecific::special entry %s founded", itr->c_str()));
			count++;
		}
	}
	if(count == 0) {
		utvDEBUG(("CCustomerSpecific::%s host does not has special download file entry", m_strHostId));
		return FALSE;
	}


	utvDEBUG(("CCustomerSpecific::%dth special file founded", count));
	return TRUE;
}

BOOL CCustomerSpecific::_LoadVersion(LPCSTR localPath)
{
	FILE* fp = fopen(localPath,"r");
	if(fp==0){
		utvERROR(("CCustomerSpecific::%s file open failed", localPath));
		return FALSE;
	}

	char buf[2048];
	memset(buf,0x00,2048);
	while(fgets(buf,2048,fp)){
		CString line = buf;
		CString key = line.Mid(0,line.Find(','));
		SpecificEntryMap::iterator itr = m_map.find(key);
		if(itr!=m_map.end()) {
			SpecificEntry* ele = itr->second;
			ele->versionLine = line;
			utvDEBUG(("CCustomerSpecific::%s line founed", line));
		}
	}
	fclose(fp);
	return TRUE;
}

BOOL CCustomerSpecific::AppendVersion(LPCSTR localPath)
{
	FILE* fp = fopen(localPath,"a");
	if(fp==0){
		utvERROR(("CCustomerSpecific::%s file open failed", localPath));
		return FALSE;
	}
	SpecificEntryMap::iterator itr;
	for(itr=m_map.begin();itr!=m_map.end();itr++) {
		SpecificEntry* ele = itr->second;
		if(!ele->versionLine.empty()) {
			fprintf(fp,ele->versionLine.c_str());
			utvDEBUG(("CCustomerSpecific::%s line appended", ele->versionLine.c_str()));
		}
	}
	fclose(fp);
	return TRUE;
}

BOOL CCustomerSpecific::SetDownloadResult(CString& target, BOOL result)
{
	if(!this->m_hasSpecific) {
		return FALSE;
	}

	ciGuard aGuard(m_mapLock);
	SpecificEntryMap::iterator itr = m_map.find(target);
	if(itr!=m_map.end()) {
		itr->second->downloadSucceed = result;
		if(result) {
			utvDEBUG(("CCustomerSpecific::%s update succeed", target));
		}else{
			utvDEBUG(("CCustomerSpecific::%s update failed", target));
		}
		return TRUE;
	}
	return FALSE;
}


BOOL CCustomerSpecific::Execute()
{
	utvDEBUG(("CCustomerSpecific::Execute()"));

	ubcIni logIni(CUSTOMER_SPECIFIC_LOG_INI, EXE_HOME, "data");

	SpecificEntryMap::iterator itr;
	for(itr=m_map.begin();itr!=m_map.end();itr++) {
		SpecificEntry* ele = itr->second;
		ciString moveResult = "TRUE";
		logIni.get(itr->first, "moveResult",moveResult,"TRUE");
		utvDEBUG(("CCustomerSpecific::moveResult of %s = %s", itr->first, moveResult.c_str()));
		if(ele->downloadSucceed || moveResult == "FALSE") {
			ele->isExecuted = ele->Execute();
		}
	}
	return _WriteLog();
}
BOOL CCustomerSpecific::_WriteLog()
{
	ubcIni logIni(CUSTOMER_SPECIFIC_LOG_INI, EXE_HOME, "data");

	ciTime now;
	ciString timeStr = now.getTimeString();
	SpecificEntryMap::iterator itr;
	for(itr=m_map.begin();itr!=m_map.end();itr++) {
		SpecificEntry* ele = itr->second;
		if(ele->isExecuted) {
			ele->WriteLog(&logIni,itr->first,timeStr.c_str());
		}
	}
	return FALSE;
}

ciBoolean SpecificEntry::Execute()
{
	ciBoolean isChanged;
	if(!this->beforeMove.empty()) {
		if(::system(beforeMove.c_str())==0){
			beforeMoveResult = ciTrue;
		}else{
			beforeMoveResult = ciFalse;
		}
		::Sleep(1000);
		isChanged = ciTrue;
		utvDEBUG(("CCustomerSpecific::%s executed =%d", beforeMove.c_str(), beforeMoveResult));
	}
	if(!this->movePath.empty()) {
		if(beforeMoveResult == ciTrue){
			ciString source = PROJECT_HOME;
			source += this->targetPath;
			ciStringUtil::stringReplace(source,"/","\\");
			if(access(source.c_str(),0x00)!=0) {
				if(access(this->movePath.c_str(),0x00)==0) {
					// source 가 없고, target 만 있다면, 이미 move 된것.
					moveResult = ciTrue;
					isChanged = ciTrue;
					utvDEBUG(("CCustomerSpecific::%s --> %s already done", source.c_str(), movePath.c_str()));
				}else{
					//source 가 없고, target 도 없다.
					moveResult = ciFalse;
				}
			}else{
				moveResult = ::MoveFileEx(source.c_str(),this->movePath.c_str(), MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING);
				if(moveResult) {
					utvDEBUG(("CCustomerSpecific::%s --> %s move succeed", source.c_str(), movePath.c_str()));
					this->isMoved=ciTrue;
				}else{
					utvERROR(("CCustomerSpecific::%s --> %s move failed %d", source.c_str(), movePath.c_str(),GetLastError()));
				}
				isChanged = ciTrue;
			}
		}
	}
	if(!this->afterMove.empty()) {
		if(beforeMoveResult == ciTrue){
			if(::system(afterMove.c_str())==0){
				afterMoveResult = ciTrue;
			}else{
				afterMoveResult = ciFalse;
			}
			utvDEBUG(("CCustomerSpecific::%s executed =%d", afterMove.c_str(), afterMoveResult));
		}
		isChanged = ciTrue;
	}
	return isChanged;
}
void SpecificEntry::WriteLog(ubcIni* logIni, const char* key, const char* nowStr)
{
	logIni->set(key,"lastUpdateTime", nowStr);
	if(isMoved){
		logIni->set(key,"lastMoveTime", nowStr);
	}
	logIni->set(key,"beforeMove", this->beforeMove.c_str());
	logIni->set(key,"beforeMoveResult", this->beforeMoveResult);
	logIni->set(key,"afterMove", this->afterMove.c_str());
	logIni->set(key,"afterMoveResult", this->afterMoveResult);
	logIni->set(key,"movePath", this->movePath.c_str());
	logIni->set(key,"moveResult", this->moveResult);
}