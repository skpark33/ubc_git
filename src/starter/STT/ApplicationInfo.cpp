/*! \class ApplicationInfo
 *  Copyright ⓒ 2002, SQI soft. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#include "StdAfx.h"
#include "LogTrace.h"
#include "ApplicationInfo.h"
#include "COP/ciStringUtil.h"
#include "COP/ciStringTokenizer.h"



ApplicationInfo::ApplicationInfo()
{
	utvDEBUG(("ApplicationInfo()"));
	_runningType = "BG";
	_autoStartUp = false;
	_runTimeUpdate = false;
	_isPrimitive = false;
	_monitored = true; // 디폴트가 참
	_initSleepSecond = 0;
	_pid = 0;
	_adminState=true;
}

ApplicationInfo::ApplicationInfo(const char* procName) 
{
	utvDEBUG(("ApplicationInfo()"));

	_appId = procName; 
	_runningType = "BG";
	_autoStartUp = false;
	_runTimeUpdate = false;
	_isPrimitive = false;
	_monitored = true; // 디폴트가 참
	_initSleepSecond = 0;
	_pid = 0;
	_adminState=true;
}
/*
bool 
ApplicationInfo::fromMap(ciXPropertyMap& procMap)
{
	ciGuard aGuard(_lock);

	procMap.get("MONITORED",		_monitored);
	if (!_monitored) {
		utvDEBUG(("%s is not monitoring application", _appId));
		return false;
	}

	procMap.get("BINARY_NAME",		_binaryName);
	ciString binName = _binaryName;;
	ciString starterName = STARTER_PROCESSNAME;
	ciStringUtil::toUpper(binName);
	ciStringUtil::toUpper(starterName);
	if (binName == starterName) {
		utvDEBUG(("It's me. return - BINARY_NAME[%s]", STARTER_PROCESSNAME));
		return false;
	}

	procMap.get("ARGUMENT",			_argument);
	procMap.get("PROPERTIES",		_properties);
	procMap.get("DEBUG",			_debug);
	procMap.get("START_TIME",		_startTime );
	procMap.get("RUNNING_TYPE",		_runningType);
	procMap.get("AUTO_STARTUP",		_autoStartUp);
	procMap.get("RUNTIME_UPDATE",	_runTimeUpdate);
	
	procMap.get("PRERUNNING_APP",	_preRunningApp);
	procMap.get("IS_PRIMITIVE",		_isPrimitive);
	
	procMap.get("INIT_SLEEP_SECOND",_initSleepSecond);
	
	procMap.get("PID",				_pid);
	procMap.get("STATUS",			_status);
	
	printf(" - APP_ID[%s]\n",			_appId.GetBuffer());
	printf(" - BINARY_NAME[%s]\n",		_binaryName.GetBuffer());
	printf(" - ARGUMENT[%s]\n",			_argument.GetBuffer());
	printf(" - PROPERTIES[%s]\n",		_properties.GetBuffer());
	printf(" - DEBUG[%s]\n",			_debug.GetBuffer());
	printf(" - START_TIME[%s]\n",		_startTime.GetBuffer());
	printf(" - RUNNING_TYPE[%s]\n",		_runningType.GetBuffer());
	printf(" - AUTO_STARTUP[%s]\n",		_autoStartUp ? "TRUE" : "FALSE");
	printf(" - RUNTIME_UPDATE[%s]\n",	_runTimeUpdate ? "TRUE" : "FALSE");
	printf(" - ADMINSTATE[%d]\n",		_adminState);
	
	printf(" - PRERUNNING_APP[%s]\n",	_preRunningApp.GetBuffer());
	printf(" - IS_PRIMITIVE[%s]\n",		_isPrimitive ? "TRUE":"FALSE");
	printf(" - MONITORED[%s]\n",		_monitored ? "TRUE":"FALSE");
	printf(" - INIT_SLEEP_SECOND[%d]\n",_initSleepSecond);
	
	printf(" - PID[%d]\n",				_pid);
	printf(" - STATUS[%s]\n",			_status.GetBuffer());
	
	return true;
}
*/

ApplicationInfo::~ApplicationInfo()
{
	utvDEBUG(("~ApplicationInfo()"));
}

