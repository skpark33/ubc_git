///////////////////////////////////////////////////////////////////////
//  LogTrace.cpp -- Implementation of the CLogTrace class


#include "stdafx.h"
#include "LogTrace.h"


CLogTrace* 	CLogTrace::_instance = 0; 
ciMutex 	CLogTrace::_instanceLock;

CLogTrace*	
CLogTrace::getInstance() {
	if(!_instance) {
		ciGuard aGuard(_instanceLock);
		if(!_instance) {
			_instance = new CLogTrace;
		}
	}
	return _instance;
}

void	
CLogTrace::clearInstance() {
	if(_instance) {
		ciGuard aGuard(_instanceLock);
		if(_instance) {
			delete _instance;
			_instance =0;
		}
	}
}



CLogTrace::CLogTrace()
{
	m_bActive = FALSE;
	m_bTimeStamp = TRUE;
}


CLogTrace::~CLogTrace()
{
	if(m_bActive) m_f.Close();
}

void CLogTrace::logOn(LPCTSTR szFileName,BOOL bTimeStamp)
{
	m_strFileName.Format("%s", szFileName);
	m_bTimeStamp = bTimeStamp;
	
	if (m_strFileName.IsEmpty()) return;	
	
	CFileException fe;
	try
	{
		if (m_f.Open(m_strFileName, CFile::modeCreate | CFile::shareDenyNone , &fe) == FALSE){
			return;
		}
	} catch (CException* e) {
		e->Delete();
		return;
	}

	m_bActive = TRUE;
	
	WriteLine("\n\n******************************************\n\n");
	CString s;
	s.Format("Log Trace %s\n\n",  COleDateTime::GetCurrentTime().Format());
	WriteLine(s);
}

void CLogTrace::WriteLine(LPCTSTR szLine)
{
	if (m_bActive == FALSE) return;

	CString s;
	if (m_bTimeStamp){
		s.Format("%s\t%s\n", COleDateTime::GetCurrentTime().Format(),	szLine);
	}else{
		s.Format("%s\n", szLine);
	}
	m_f.WriteString(s);
		
}

