/*! \class AppMonitor
 *  Copyright ⓒ 2002, SQI soft. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#ifndef _AppMonitor_h_
#define _AppMonitor_h_

#include "COP/ciTimer.h"
#include "COP/ciSyncUtil.h"
#include "STTDoc.h"
#ifdef _TAO
#	include <winsock2.h>
#endif

class AppMonitor :  public  ciPollable
{
public:
	static AppMonitor*	getInstance();
	static void clearInstance();

	virtual ~AppMonitor();

	ciBoolean init(STTDoc* document, int period);

protected:
	AppMonitor();
	STTDoc* _doc;
	HWND _sttHWnd;

	ciInt _watch(APPLICATION_INFO& appInfo, ciBoolean preRunningApp = ciFalse); // Active:PID, InActive: 0, 실패: -1
	ciUInt _start(APPLICATION_INFO& appInfo); // 성공:PID, 실패:0
	ciBoolean _stop(APPLICATION_INFO& appInfo);

	ciUInt _createProcess(const char *command, const char *argument, ciBoolean minFlag = ciFalse, ciBoolean bgFlag=ciFalse);
	ciInt _killProcess(const char *command, ciUInt pid = 0);

#ifdef _TAO
	//ciBoolean _getExternalIP(ciString& oExtIP);
	//int recv_timeout(SOCKET Socket, void *pvBuffer, int len, int second);
#endif
	BOOL _getIdfromArgument(const char* argument, ciString& id);

	ciMutex _appMutex;

	static ciMutex _sMutex;
	static AppMonitor* _instance;

	ciBoolean	_isMonitored;
	ciString _customer;
	ciString _serverId;
	ciString _serverIp;

public:
    virtual void processExpired(ciString name, int counter, int interval);

	void watchAll(/*skpark int counter*/);
	
	ciBoolean watch(APPLICATION_INFO& appInfo);
	ciBoolean start(APPLICATION_INFO& appInfo);
	ciBoolean stop(APPLICATION_INFO& appInfo);

	void	 setBrwAdmin(int val);
	ciBoolean watch(CString& appId);
	ciBoolean start(CString& appId);
	ciBoolean conditionalStart(CString& appId);
	ciBoolean stop(CString& appId);
	ciBoolean dontStartAgain(CString& appId);

	// Utility method
	static BOOL SafeTerminateProcess(HANDLE hProcess, UINT uExitCode);

	int	adminState_of_brw1;
};

#endif //_AppMonitor_h_

