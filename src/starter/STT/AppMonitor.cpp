/*! \class AppMonitor
 *  Copyright ⓒ 2002, SQI soft. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#include "StdAfx.h"
#include <tlhelp32.h>
#include "COP/ciArgParser.h"
#include "COP/ciStringUtil.h"
#include "COP/ciStringTokenizer.h"
#include "COP/ciTime.h"
#include "COP/ciEnv.h"
#include "LogTrace.h"
#include "AppMonitor.h"
#include "MsgBox.h"
#include "AutoUpdateDlg.h"
#include "libServerProcess/ServerProcess.h"
#include <UBC/ubcIni.h>

// static var
AppMonitor* AppMonitor::_instance = 0;
ciMutex AppMonitor::_sMutex;

AppMonitor*	AppMonitor::getInstance()
{
	if(_instance == 0)
	{
		CI_GUARD(_sMutex, "AppMonitorGetInstance()");
		if(_instance == 0)
			_instance = new AppMonitor;
	}
	return _instance;
}

void AppMonitor::clearInstance()
{
	if(_instance)
	{
		CI_GUARD(_sMutex, "AppMonitorGetInstance()");
		if (_instance) {
			_instance->stopTimer();
			delete _instance;
			_instance = NULL;
		}
	}
}

AppMonitor::AppMonitor()
: _doc(0), _sttHWnd(0),_isMonitored(ciFalse)
{
	utvDEBUG(("AppMonitor()"));
	adminState_of_brw1 = -1;
}

AppMonitor::~AppMonitor()
{
	utvDEBUG(("~AppMonitor()"));
	_doc = NULL;
	_sttHWnd = NULL;
}

ciBoolean
AppMonitor::init(STTDoc* document, int period)
{
	utvDEBUG(("init(%d)", period));

	//if(ciArgParser::getInstance()->isSet("+monitor")){
	//	_isMonitored = ciTrue;
	//}
	const char* configroot = ciEnv::newEnv("CONFIGROOT");
	if(!configroot){
		utvERROR(("CONFIGROOT NOT SET"));
		return ciFalse;
	}


	ubcIni aIni("UBCVariables",configroot, "data");
	aIni.get("ROOT","ServerMonitor", _isMonitored);
	utvDEBUG(("ServerMonitor=%d", _isMonitored));
	if(_isMonitored){
		ubcIni bIni("UBCServerMonitor2",configroot, "data");
		bIni.get("ROOT","ENTERPRISE", _customer);
		if(_customer.empty()){
			utvERROR(("get customer failed"));
			if(_customer.empty()) {
				aIni.get("CUSTOMER_INFO","NAME", _customer);
				_customer = "UNKNOWN";
			}
		}
		if(!scratchUtil::getInstance()->getIpAddress(_serverIp)){
			utvERROR(("getIpAddress failed"));
			const char* my_ip = getenv("MY_IP");
			if(my_ip){
				_serverIp = my_ip;
			}else{
				_serverIp = "UNKNOWN";
			}
		}
		char ac[MAX_COMPUTERNAME_LENGTH+1];
		DWORD len=MAX_COMPUTERNAME_LENGTH;
		//if (gethostname(ac, sizeof(ac)) == -1) {
		if (GetComputerName(ac, &len) == FALSE) {
			const char* computerName = getenv("COMPUTERNAME");
			if(!computerName){
				_serverId = "UNKNOWN";
			}else{
				_serverId = computerName;
			}
		}else{
			_serverId = ac;
		}
		utvDEBUG(("Customer=%s", _customer.c_str()));
		utvDEBUG(("Server Ip=%s", _serverIp.c_str()));
		utvDEBUG(("Server Id=%s", _serverId.c_str()));

		// UBCServerAgent로 이동
		//CServerProcess::getInstance()->InitCommand(_customer.c_str(), _serverId.c_str());
	}
	

	// set default=60
	if (period < 10) {
		utvWARN(("Period minimum time is 10s."));
		period = 10;
	}
	utvDEBUG(("init : AppMonitor Timer(%d)", period));

	if (!document) {
		utvERROR(("Document is null."));
		return ciFalse;
	}
	_doc = document;

	_sttHWnd = ::FindWindow(NULL, STARTER_WINDOWSNAME);
	if (!_sttHWnd) {
		utvERROR(("STTDlg Handle is not found."));
		return ciFalse;
	}
	//initTimer("AppMonitor", period);
	// Debug 를 위해서 initTimer 를 호출하지 않고, 직접 initTimer내부 코드를 수행한다.

	utvDEBUG(("init : AppMonitor Timer(%d) -- 1", period));
	if(_timer) {
		utvDEBUG(("init : AppMonitor Timer(%d) -- 2", period));
		stopTimer();
		utvDEBUG(("init : AppMonitor Timer(%d) -- 3", period));
		delete _timer; 
	}
	utvDEBUG(("init : AppMonitor Timer(%d) -- 4", period));
	_timer = new ciTimer("AppMonitor", this);
	if(_timer==0){
		utvERROR(("Fatal : memory new failed(ciTimer) !!!"));
		return ciFalse;
	}
	utvDEBUG(("init : AppMonitor Timer(%d) -- 5", period));
    _timer->setInterval(period);




	return ciTrue;
}

void
AppMonitor::processExpired(ciString name, int counter, int interval)
{
	utvDEBUG(("processExpired : name(%s), counter(%d), interval(%d)"
		, name.c_str()
		, counter
		, interval
	));

	// 애플리케이션 모니터링
	utvDEBUG(("Before GUARD AppMonitor::processExpired()"));
	ciGuard aGuard(AutoUpdateDlg::m_sLock);
	utvDEBUG(("After GUARD AppMonitor::processExpired()"));
	watchAll(/* skpark counter*/);
}

void
AppMonitor::watchAll(/*skpark int counter*/)
{
	APPLICATION_LIST appList;
	if (!_doc->GetApplicationsInfo(appList)) {
		utvDEBUG(("Application List size is zero."));
		return;
	}

	// skpark 2011.12.13
	// ServerMonitor 옵션이 있는 경우는 ubc center 에 http 로 프로세스 상태를 보고한다.
	// watch 는 10초 마다 한번씩 호출되기 때문에,  이를 1분에 한번 호출되도록 수정해주어야 한다.
	// ApplicationMonitorPeriod=60 으로 바꾸어주는 것을 잊지 않는다.
	//

	// CServerProcess를 UBCServerAgent로 이동 (2013.04.12)
/*
	CServerProcessInfo spi;
	if(this->_isMonitored){
		spi.SetServerInfo(_customer.c_str(), _serverId.c_str(), _serverIp.c_str());
	}
*/
	int count = appList.GetSize();
	for(int i=0; i<count; i++)
	{
		APPLICATION_INFO& info = appList.GetAt(i);

		if(info.appId.CompareNoCase("BROWSER")==0 ){
			if(adminState_of_brw1 != -1) {
				info.adminState = adminState_of_brw1;
				utvDEBUG(("setBrwAdmin 1"));
				adminState_of_brw1 = -1;
				_doc->SetApplicationInfo(info);
			}
		}

		watch(info);
/*
		if(this->_isMonitored){
			ciTime startTime(info.startTime);

			SERVERPROCESS_INFO  spiEle;

			spiEle.appId		= info.appId;
			spiEle.argument		= info.argument;
			spiEle.binaryName	= info.binaryName;
			spiEle.debug		= info.debug;
			spiEle.properties	= info.properties;
			spiEle.startUpTime	= startTime;
			spiEle.status		= info.status;
	
			utvDEBUG(("%s Process Status add", info.appId));
			spi.AddServerProcessInfo(spiEle);
		}
*/
	}

/*
	if(this->_isMonitored){
		utvDEBUG(("%s Process Status Sent to UBC Center !!!", _serverId.c_str()));
		ciBoolean sendResult  = CServerProcess::getInstance()->SendStatus(spi);
		if(sendResult){
			utvDEBUG(("%s Process Status Sent to UBC Center Succeed!!!", _serverId.c_str()));
			ciBoolean receiveResult = CServerProcess::getInstance()->GetCommandList(spi);
			int commandCount=0;
			if(receiveResult) {
				utvDEBUG(("%s Process GetCommandList Succeed!!!", _serverId.c_str()));
				spi.GetFirstServerProcessInfo();
				SERVERPROCESS_INFO* info;
				while((info=spi.GetNextServerProcessInfo())!=NULL){
					utvDEBUG(("%s Command Exist!!!", _serverId.c_str()));
					utvDEBUG(("APPID=%s", info->appId.c_str()));
					utvDEBUG(("BIN=%s", info->binaryName.c_str()));
					utvDEBUG(("STATUS=%d", info->status));
					utvDEBUG(("COMMAND=%s", info->command.c_str()));
					CString appId = info->appId.c_str();
					if(info->command == "Starting"){
						this->conditionalStart(appId);
					}else 
					if(info->command == "Stopping"){
						this->stop(appId);
					}
					commandCount++;
				}
			}else{
				utvDEBUG(("%s Process GetCommandList Fail!!!", _serverId.c_str()));
			}
			if(commandCount == 0){
				utvDEBUG(("No Command from UBC Center"));
			}
		}else{
			utvERROR(("%s Process Status Sent to UBC Center Failed!!!", _serverId.c_str()));
		}
	}
*/
}

ciBoolean 
AppMonitor::watch(APPLICATION_INFO& appInfo) 
{
	//utvDEBUG(("watch"));
	CI_GUARD(_appMutex, "watch()");
	//utvDEBUG(("watch"));

	if (_watch(appInfo) >= 0) {
		return ciTrue;
	}
	return ciFalse;
}

ciBoolean 
AppMonitor::start(APPLICATION_INFO& appInfo) 
{
	CI_GUARD(_appMutex, "start()");
	utvDEBUG(("start()"));

/*
	if (appInfo.isPrimitive || !appInfo.monitored)
	{
		//AfxMessageBox("제어할 수 없는 어플리케이션입니다.");
#ifdef AFX_TARG_KOR
		SttMessageBoxH(_sttHWnd, "제어할 수 없는 어플리케이션입니다.", 2000);
#else
		SttMessageBoxH(_sttHWnd, "It is not controlable application.", 2000);
#endif
		return ciFalse;
	}*/

	ciUInt pid = _start(appInfo);
	if (pid == 0) {
		utvERROR(("Application start failed."));
		return ciFalse;
	}
	return ciTrue;
}

ciBoolean 
AppMonitor::stop(APPLICATION_INFO& appInfo)
{
	utvDEBUG(("stop[appInfo]"));
	CI_GUARD(_appMutex, "stop()");
/*
	if (appInfo.isPrimitive || !appInfo.monitored)
	{
		//AfxMessageBox("제어할 수 없는 어플리케이션입니다.");
#ifdef AFX_TARG_KOR
		SttMessageBoxH(_sttHWnd, "제어할 수 없는 어플리케이션입니다.", 2000);
#else
		SttMessageBoxH(_sttHWnd, "It is not controlable application.", 2000);
#endif
		return ciFalse;
	}*/

	if (!_stop(appInfo)) {
		utvERROR(("Application stop failed."));
		return ciFalse;
	}
	return ciTrue;
}

void  
AppMonitor::setBrwAdmin(int val) 
{
	utvDEBUG(("setBrwAdmin(%d)",val));
	adminState_of_brw1 = val;
}


ciBoolean  
AppMonitor::watch(CString& appId) 
{
	utvDEBUG(("watch(%s)", appId));
	APPLICATION_INFO appInfo;
	if (!_doc->GetApplicationInfo(appId, appInfo)) {
		utvDEBUG(("Application Info not found."));
		return ciFalse;
	}
	appInfo.adminState = true; // adminState ON
	return watch(appInfo);
}

ciBoolean  
AppMonitor::start(CString& appId)
{
	utvDEBUG(("start[%s]", appId.GetBuffer(0)));

	APPLICATION_INFO appInfo;
	if (!_doc->GetApplicationInfo(appId, appInfo)) {
		utvDEBUG(("Application Info not found."));
		return ciFalse;
	}
	appInfo.adminState = true; // adminState ON
	return start(appInfo);
}
ciBoolean  
AppMonitor::conditionalStart(CString& appId)
{
	utvDEBUG(("conditionalStart[%s]", appId.GetBuffer(0)));

	APPLICATION_INFO appInfo;
	if (!_doc->GetApplicationInfo(appId, appInfo)) {
		utvDEBUG(("Application Info not found."));
		return ciFalse;
	}
	if(appInfo.status == "Active"){
		utvDEBUG(("%s is Already Active", appId.GetBuffer(0)));
		return ciFalse;
	}
	appInfo.adminState = true; // adminState ON
	return start(appInfo);
}
ciBoolean  
AppMonitor::stop(CString& appId)
{
	utvDEBUG(("stop[%s]", appId.GetBuffer(0)));
	
	APPLICATION_INFO appInfo;
	if (!_doc->GetApplicationInfo(appId, appInfo)) {
		utvDEBUG(("[%s]어플리케이션 정보를 찾을 수 없습니다.", appId.GetBuffer(0)));
		return ciFalse;
	}
	appInfo.adminState = false; // adminState OFF
	return stop(appInfo);
}
ciBoolean  
AppMonitor::dontStartAgain(CString& appId)
{
	utvDEBUG(("dontStartAgain[%s]", appId.GetBuffer(0)));
	
	APPLICATION_INFO appInfo;
	if (!_doc->GetApplicationInfo(appId, appInfo)) {
		utvDEBUG(("[%s]어플리케이션 정보를 찾을 수 없습니다.", appId.GetBuffer(0)));
		return ciFalse;
	}
	appInfo.adminState = false; // adminState OFF
	_doc->SetApplicationInfo(appInfo);
	return ciTrue;
}
ciInt 
AppMonitor::_watch(APPLICATION_INFO& appInfo, ciBoolean preRunningApp) 
{
	//
	//utvDEBUG(("0--watch(%s,%d)", appInfo.binaryName,appInfo.adminState));
	//ciString appName = appInfo->appId;
	ciString binaryName = ::GetBinaryName(appInfo.binaryName);

	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ( hSnapshot == INVALID_HANDLE_VALUE ) {
		utvERROR(("HANDLE을 생성할 수 없습니다."));
		return -1;
	}

	PROCESSENTRY32 pe32 ;
	pe32.dwSize = sizeof(PROCESSENTRY32);

	BOOL bContinue = false;
	BOOL bActive = false;
	char strProcessName[512];

	//utvDEBUG(("프로세스 검색을 시작합니다. watch(%s)", binaryName.c_str()) );
	if ( !Process32First ( hSnapshot, &pe32 ) )
	{
		utvERROR(("Process32First failed."));
		return -1;
	}
	//utvDEBUG(("1--watch(%s,%d)", appInfo.binaryName,appInfo.adminState));

	do
	{
		memset(strProcessName, 0, sizeof(strProcessName));

		size_t stringLength = strlen(pe32.szExeFile);
		for(int i=0; i<stringLength; i++) // 대문자로 전환
			strProcessName[i] = toupper( pe32.szExeFile[i] );

		if( stricmp(strProcessName, binaryName.c_str()) == 0 )
		{	
			// ID 비교

			BOOL equalIdFlag = TRUE;
			if (!appInfo.argument.IsEmpty()) {
				// Get Command-line arguments
				ciString cmdLineArgument;
				scratchUtil::getInstance()->extractProcessCommandLine(pe32.th32ProcessID, cmdLineArgument);

				ciString peStrId, appStrId;				
				if (_getIdfromArgument(cmdLineArgument.c_str(), peStrId) 
					&& _getIdfromArgument(appInfo.argument.GetBuffer(0), appStrId)
					&& peStrId != appStrId)
				{
					equalIdFlag = FALSE;
				}
			}

			// Document 에 기록

			if (equalIdFlag) {
				if (appInfo.status != "Active" || appInfo.pid != pe32.th32ProcessID)
				{
					appInfo.status = "Active";
					appInfo.pid = pe32.th32ProcessID;
					ciTime current;
					appInfo.startTime = current.getTimeString().c_str();
					if (!appInfo.adminState) {
						utvDEBUG(("%s is Active. Set AdminState to True.", appInfo.appId.GetBuffer()));
						appInfo.adminState = true;
					}

					// Windows Message 로 전송
					_doc->SetApplicationInfo(appInfo);
					//utvDEBUG(("watch Window after Active"));
					::PostMessage(_sttHWnd, WM_REFRESH, NULL, NULL);
				}
				bActive = true;
				break;
			}
		}
		bContinue = Process32Next ( hSnapshot, &pe32 );
	}
	while ( bContinue );
	//utvDEBUG( ("프로세스 검색이 완료되었습니다. watch(%s)", binaryName.c_str()) );

	CloseHandle( hSnapshot );

	utvDEBUG(("2--watch(%s,%d)", appInfo.binaryName,appInfo.adminState));

	ciInt ret_pid = 0;
	if (bActive) {
		ret_pid = appInfo.pid;
	} else {
		if (appInfo.status != "Inactive") {
			// Document 에 기록

			appInfo.status = "Inactive";
			appInfo.pid = 0;

			// Windows Message 로 전송
			_doc->SetApplicationInfo(appInfo);
			//utvDEBUG(("Refresh Window after Inactive"));
			::PostMessage(_sttHWnd, WM_REFRESH, NULL, NULL);
		}		
		if (appInfo.autoStartUp 
			|| (preRunningApp 
				&& appInfo.isPrimitive 
				&& !appInfo.monitored) // idl2ir, nbgen 같은 APP.
			)
		{
			// 자동기동 설정이 되어있고 AdminState 값이 True
			if (appInfo.adminState) {
				ret_pid = _start(appInfo);
			} else {
				utvDEBUG(("[%s]AdminState is FALSE. "
					"Set AdminState to TRUE if you want to auto start."
					, appInfo.appId.GetBuffer()));
			}
		}
	}
	return ret_pid;
}

ciUInt 
AppMonitor::_start(APPLICATION_INFO& appInfo) 
{
	utvDEBUG(("_start()"));

	// 선행 어플리케이션을 확인한다.
	if (!appInfo.preRunningApp.IsEmpty()) {
		APPLICATION_INFO preApp;
		if (_doc->GetApplicationInfo(appInfo.preRunningApp, preApp)) {
			if (_watch(preApp, ciTrue) <= 0) {
				CString errMsg;
				errMsg.Format("%s 프로세스를 먼저 기동해주세요.", 
					appInfo.preRunningApp);
				//AfxMessageBox(errMsg);
				SttMessageBoxH(_sttHWnd, errMsg, 2000);
				return 0;
			} else {
				// skpark 2009.8.27 프로세스간 동기화.
				// preRunningApp 가 단지 살아있음을 체크하는데 그치지 않고
				// 특정 작업까지 마쳤는지 확인한뒤, 본 프로세스를 띠우기 위한 조치이다.
				// _watch 를 통해서 preRunningApp 프로세스가 살아있음을 확인한뒤,
				// preRunningApp 가 ::CreateMutex 를 할때까지 기다린다.
				// 그러나 initSleepSecond 이상으로 기다리지는 않는다. 
				// 따라서, 이러한 동기화하는 프로세스의 경우에는 initSleepSecond 를 충분히
				// 큰 값으로 주어야 한다.
				// initSleepSecond 가 0 인 경우는 끝까지 기다린다. 
				// 
				// 
				utvDEBUG(("%s's preRunningApp[%s] executed", appInfo.appId,appInfo.preRunningApp));
				if (_doc->GetServerFlag()==ciFalse) {
					int counter = 0;
					while(1){
						if(appInfo.initSleepSecond > 0){
							if(counter > appInfo.initSleepSecond) { 
								break;
							}
						}
						HANDLE hMutex = ::OpenMutex(MUTEX_ALL_ACCESS,FALSE,appInfo.preRunningApp);
						if(hMutex) {
							utvDEBUG(("%s's preRunningApp[%s] have created mutex", appInfo.appId,appInfo.preRunningApp));
							if(_doc->GetApplicationInfo(appInfo.appId, appInfo)){
								utvDEBUG(("%s's information reloaded", appInfo.appId));
							}else{
								utvWARN(("%s's information reloading fail", appInfo.appId));
							}
							break;
						}
						utvDEBUG(("wait %s's preRunningApp[%s] to create mutex", appInfo.appId,appInfo.preRunningApp));
						Sleep(1000);
						counter++;
						// 무한루프 방지 2010.01.12
						if (counter > 300) {
							utvERROR(("Failed %s's preRunningApp[%s] to create mutex", 
								appInfo.appId,appInfo.preRunningApp));
							break;
						}
					}
				}else{
					Sleep(appInfo.initSleepSecond*1000);
				}
							
			}
		} else {
			utvERROR(("%s is not found", appInfo.preRunningApp.GetBuffer(0)));
		}
	}

	ciString command = appInfo.binaryName;
	ciString argument = "";
	if (!appInfo.argument.IsEmpty()) {
		argument += " ";
		argument += appInfo.argument;
	}
	if (!appInfo.properties.IsEmpty()) {
		argument += " ";
		argument += appInfo.properties;
	}
	if (!appInfo.debug.IsEmpty()) {
		argument += " ";
		argument += appInfo.debug;
	}
	ciBoolean minFlag = ciFalse;
	if (appInfo.runningType == "BG") {
		minFlag = ciTrue;
	}

	ciBoolean bgFlag = ciFalse;
	if(	(appInfo.argument.Find("+bg") >= 0) ||
		(appInfo.properties.Find("+bg") >= 0) ||
		(appInfo.debug.Find("+bg") >= 0) ){
		bgFlag = ciTrue;
	}
#ifdef _TAO
	/*
	ciString extIPAddr;
	if (_getExternalIP(extIPAddr)) {
		command += " -ORBEndpoint iiop:///hostname_in_ior=";
		command += extIPAddr;
	}
	*/
#endif
	// skpark 2009.8.27 preRunningApp 가 있는 경우에는 프로세스간 동기화 기능에서
	// initSleep 하므로 여기서는 하지 않는다.
	if (appInfo.initSleepSecond > 0 && appInfo.preRunningApp.IsEmpty()) {
		::Sleep(1000 * appInfo.initSleepSecond); // 기동전 대기 시간동안 sleep
	}

	// 환경변수 변환
	ciString runCommand;
	ciStringUtil::envTranslate(command.c_str(), runCommand);

	ciString runArgument;
	ciStringUtil::envTranslate(argument.c_str(), runArgument);

	ciUInt pid = _createProcess(runCommand.c_str(), runArgument.c_str(), minFlag, bgFlag);
	utvDEBUG(("%ul=_createProcess(%s)",pid, runCommand.c_str()));

	if (pid) {
		// Windows Message 로 전송
	
		appInfo.pid = pid;
		appInfo.status = "Starting";
		ciTime current;
		appInfo.startTime = current.getTimeString().c_str();

		// skpark 090413 starting 으로 바뀔때는 굳이 화일에 쓰지 않는다.
		//utvDEBUG(("SetApplicationInfo after createProcess"));
		_doc->SetApplicationInfo(appInfo,false);
		//utvDEBUG(("Refresh Window after createProcess"));
		::PostMessage(_sttHWnd, WM_REFRESH, NULL, NULL);
		//utvDEBUG(("Refresh Window end"));
	}
	return pid;
}

ciBoolean 
AppMonitor::_stop(APPLICATION_INFO& appInfo)
{
	utvDEBUG(("_stop[appInfo]"));

	ciString command = ::GetBinaryName(appInfo.binaryName);
//	ciUInt pid = appInfo.pid;
//	if(pid<=0) {
	utvDEBUG(("get pid again"));
	ciUInt	pid = scratchUtil::getInstance()->getPid(command.c_str());
//	}
	ciInt result = 0;
	// gracefully kill
	if (command == "UTV_brwClient2.exe" || command == "UTV_brwClient2_UBC1.exe"  ) {

		result = scratchUtil::getInstance()->killGracefully(pid,3000);
		if(result <= 0){
			//pid = scratchUtil::getInstance()->waitBeforeKill(command.c_str(),3000);
			//if(pid){
				result = _killProcess(command.c_str(), pid);
			//}
		}
	}else{
		result = _killProcess(command.c_str(), pid);
	}
	// result = _killProcess(command.c_str(), pid);

	if (result == 1 || result == -1) {
		// Windows Message 로 전송

		appInfo.pid = 0;
		//appInfo.status = "Stopping";
		appInfo.status = "Inactive";

		//utvDEBUG(("SetApplicationInfo after stopProcess"));
		_doc->SetApplicationInfo(appInfo);
		//utvDEBUG(("Refresh Window after stopProcess"));
		::PostMessage(_sttHWnd, WM_REFRESH, NULL, NULL);

		if (result == -1) {
			CString msg;
			msg.Format("[ %s ] 프로세스를 찾을 수 없습니다.", 
				appInfo.appId.GetBuffer(0));
			//AfxMessageBox(msg, MB_ICONSTOP);
			// skpark
			//SttMessageBoxH(_sttHWnd, msg, 2000);
		}
	}
	// HardCoding for notifymanager
	if (command == "notifymanager.exe") {
		_killProcess("notifyserver.exe");
	}
	return result;
}

ciUInt 
AppMonitor::_createProcess(const char *command, const char *argument, ciBoolean minFlag/*=false*/, ciBoolean bgFlag/*=false*/)
{
	utvDEBUG(("_createProcess : command(%s),argument(%s),min(%d),bg(%d)", command, argument, minFlag, bgFlag) );

	CString str_path = ::GetPath(command);
	char* current_path = NULL;
	if( str_path.GetLength() > 0 )
	{
		current_path = new char[MAX_PATH];
		::ZeroMemory(current_path, MAX_PATH);
		strcpy(current_path, str_path);
	}

	ciString cmd = command;
	if( argument ) cmd += argument;

	STARTUPINFO             si;
	PROCESS_INFORMATION     pi;
	ZeroMemory(&si, sizeof(si));
	DWORD dwCreationFlag = NORMAL_PRIORITY_CLASS;
	if (minFlag) {
		si.dwFlags = STARTF_USESHOWWINDOW;
		if(bgFlag) {
			si.wShowWindow = SW_HIDE;
			dwCreationFlag = DETACHED_PROCESS;
		}else{
			si.wShowWindow = SW_MINIMIZE;
		}
	}

	BOOL bRun = CreateProcess(
		NULL, 
		(LPSTR)cmd.c_str(), 
		NULL, 
		NULL, 
		FALSE, 
		dwCreationFlag, 
		NULL, 
		current_path, 
		&si, 
		&pi
		);

	DWORD err_code = ::GetLastError();
	CloseHandle( pi.hThread );
	CloseHandle( pi.hProcess );

	if( current_path ) delete[]current_path;

	if (bRun)
	{
		utvDEBUG(("createProcess : pid(%d)", pi.dwProcessId) );
		return pi.dwProcessId;
	}
	utvWARN( ("createProcess : fail to CreateProcess (ErrCode:%u)", err_code) );
	return 0;
}


ciInt 
AppMonitor::_killProcess(const char *command, ciUInt pid) 
{
	utvDEBUG(("killProcess : command(%s), pid(%d)", command, pid) );

	ciString strCommand = command;
	ciString lpExeName;
	ciStringUtil::toUpper(strCommand, lpExeName);

	HANDLE hSnapshot = CreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ((int)hSnapshot == -1)
	{
		utvWARN( ("killProcess : fail to CreateToolhelp32Snapshot") );
		return 0;
	}

	utvDEBUG(("killProcess : success to CreateToolhelp32Snapshot") );

	PROCESSENTRY32 pe32 ;
	pe32.dwSize=sizeof(PROCESSENTRY32);
	BOOL bContinue ;
	ciString strProcessName;

	if (!Process32First(hSnapshot, &pe32)) {
		utvWARN( ("killProcess : fail to killProcess") );
		return 0;
	}

	ciInt result = -1;
	do
	{
		//------------------------------------
		// 대문자로 전환하여 처리한다.
		ciString strExe = pe32.szExeFile;
		ciStringUtil::toUpper(strExe, strProcessName);
		//------------------------------------

		// pid 값이 0이면 모든 command 프로세스 종료한다.
		// pid 값이 0이 아니면 pid 값을 갖는 command 프로세스만 종료
		if( (strProcessName == lpExeName.c_str()) && (pe32.th32ProcessID == pid || pid == 0) )
		{
			HANDLE hProcess = OpenProcess( PROCESS_ALL_ACCESS, 0, pe32.th32ProcessID );
			if( hProcess )
			{
				DWORD   dwExitCode;
				GetExitCodeProcess( hProcess, &dwExitCode );
				::TerminateProcess( hProcess, dwExitCode );
				//SafeTerminateProcess( hProcess, dwExitCode );
				CloseHandle( hProcess );
				result = 1;

				utvDEBUG(("killProcess : success to Closehandle") );
				if (pid) {
					CloseHandle( hSnapshot );
					return result; // pid 값이 0이 아니면 리턴
				}
			}
		}
		bContinue = Process32Next ( hSnapshot, &pe32 );

	} while ( bContinue );

	CloseHandle( hSnapshot );
	utvDEBUG(("killProcess : Process is not found.") );
	return result;
}

BOOL AppMonitor::SafeTerminateProcess(HANDLE hProcess, UINT uExitCode)
{
	DWORD dwTID, dwCode, dwErr = 0;
	HANDLE hProcessDup = INVALID_HANDLE_VALUE;
	HANDLE hRT = NULL;
	HINSTANCE hKernel = GetModuleHandle("Kernel32");

	BOOL bSuccess = FALSE;
	BOOL bDup = DuplicateHandle(GetCurrentProcess(), 
		hProcess, 
		GetCurrentProcess(), 
		&hProcessDup, 
		PROCESS_ALL_ACCESS, 
		FALSE, 
		0);

	if ( GetExitCodeProcess((bDup) ? hProcessDup : hProcess, &dwCode) 
		&& (dwCode == STILL_ACTIVE) )
	{
		FARPROC pfnExitProc;
		pfnExitProc = GetProcAddress(hKernel, "ExitProcess");
		hRT = CreateRemoteThread((bDup) ? hProcessDup : hProcess, 
			NULL, 
			0, 
			(LPTHREAD_START_ROUTINE)pfnExitProc,
			(PVOID)uExitCode, 0, &dwTID);
		if ( hRT == NULL ) dwErr = GetLastError();
	}
	else 
	{
		dwErr = ERROR_PROCESS_ABORTED;
	}
	if ( hRT )
	{
		WaitForSingleObject((bDup) ? hProcessDup : hProcess, INFINITE);
		CloseHandle(hRT);
		bSuccess = TRUE;
	}
	if ( bDup ) 
		CloseHandle(hProcessDup);
	if ( !bSuccess )
		SetLastError(dwErr);

	return bSuccess;
}

BOOL AppMonitor::_getIdfromArgument(const char* argument, ciString& id)
{
	ciStringTokenizer argToken(argument);
	if (argToken.countTokens() <= 2) {
		return FALSE;
	}
	while (argToken.hasMoreTokens()) {
		ciString argValue = argToken.nextToken();
		if (argValue == "+id" && argToken.hasMoreTokens()) {
			id = argToken.nextToken();
			return TRUE;
		}
	}
	return FALSE;
}

/*
#ifdef _TAO
// WinSock DLL을 사용할 버전
#	define VERSION_MAJOR         2 
#	define VERSION_MINOR         0

ciBoolean AppMonitor::_getExternalIP(ciString& oExtIP)
{
	// Get Config

	//ciXProperties* config = ciXProperties::getInstance();

	ciString _ipServerAddress;
	if(!config->get("COP.IP_SERVER.IP", _ipServerAddress)){
		utvERROR(("COP.IP_SERVER.IP loading error"));
		return ciFalse;
	}
	utvDEBUG((" - IP_SERVER_ADDR[%s]", _ipServerAddress.c_str()));

	ciULong _ipServerPort = 0;
	if(!config->get("COP.IP_SERVER.PORT", _ipServerPort)){
		utvERROR(("COP.IP_SERVER.IP loading error"));
		return ciFalse;
	}
	utvDEBUG((" - IP_SERVER_PORT[%ld]", _ipServerPort));

	// Socket Source

	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA       WsaData;            // receives data from WSAStartup 
	struct sockaddr_in server_addr;	// socket address
	SOCKET socket_fd;		// socket handle
	char buf[128+1];
	int msg_size;

	// Load WinSock DLL 
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {		
		return ciFalse;
	}

	// Create a reliable, stream socket using TCP
	if ((socket_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		utvERROR(("Can't open socket."));
		return ciFalse;
	}

	// Construct the server address structure
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(_ipServerAddress.c_str());
	server_addr.sin_port = htons(_ipServerPort);

	// Connect
	if (connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
		utvERROR(("Can't connect ip server.[%s:%d]", 
			_ipServerAddress.c_str(), _ipServerPort));
		return ciFalse;
	}

	// Receive Data
	if ((msg_size = recv_timeout(socket_fd, buf, 128, 5)) <= 0) {
		utvERROR(("Can't recv from server.[%s]", _ipServerAddress.c_str()));
		return ciFalse;
	}

	// Print Data
	buf[msg_size] = '\0';
	oExtIP = buf;	

	// Clean Socket
	closesocket(socket_fd);
	WSACleanup();		// Unload WinSock DLL

	utvDEBUG(("Local IP Address is %s", oExtIP.c_str()));

	return ciTrue;
}

int 
AppMonitor::recv_timeout(SOCKET Socket, void *pvBuffer, int len, int second)
{
	int     iRC            = 0;
	int     iSendStatus    = 0;
	timeval ReceiveTimeout;
	int		iReceiveStatus = -1;
	char    *pBuffer       = static_cast<char *>(pvBuffer);

	// Set timeout
	ReceiveTimeout.tv_sec  = second;
	ReceiveTimeout.tv_usec = 0;

	fd_set fds;

	FD_ZERO(&fds);
	FD_SET(Socket, &fds);

	// As long we need to send bytes...

	iRC = select(0, &fds, NULL, NULL, &ReceiveTimeout);

	// Timeout
	if(!iRC) {
		ciERROR(("TimeOut!!![%s]", _ipServerAddress.c_str()));
		return -1;
	}

	// Error
	if(iRC < 0) {
		return WSAGetLastError();
	}

	// Receive some bytes
	iReceiveStatus = recv(Socket, pBuffer, len, 0);

	return iReceiveStatus;
}
#endif
*/

