/*! \class UpdateMonitor
 *  Copyright �� 2002, SQI soft. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#ifndef _UpdateMonitor_h_
#define _UpdateMonitor_h_

#include "COP/ciTimer.h"
#include "COP/ciSyncUtil.h"
#include "STTDoc.h"

class UpdateMonitor :  public  ciPollable
{
public:
	static UpdateMonitor*	getInstance();
	static void clearInstance();

	virtual ~UpdateMonitor();

	ciBoolean init(int period);
	void SetDocument(STTDoc* document);

protected:
	UpdateMonitor();
	HWND _sttHWnd;
	STTDoc* m_pDocument;

	ciMutex _uptMutex;

	static ciMutex _sMutex;
	static UpdateMonitor* _instance;

public:
    virtual void processExpired(ciString name, int counter, int interval);
	
	ciBoolean update();
};

#endif //_UpdateMonitor_h_

