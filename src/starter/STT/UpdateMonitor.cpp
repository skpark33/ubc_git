/*! \class UpdateMonitor
 *  Copyright ⓒ 2002, SQI soft. All rights reserved.
 *
 *  \brief
 *  \author
 *  \version
 *  \date
 */

#include "StdAfx.h"
#include <tlhelp32.h>
#include "COP/ciTime.h"
#include "LogTrace.h"
#include "AutoUpdateDlg.h"
#include "AppMonitor.h"
#include "UpdateMonitor.h"

// static var
UpdateMonitor* UpdateMonitor::_instance = 0;
ciMutex UpdateMonitor::_sMutex;

UpdateMonitor*	UpdateMonitor::getInstance()
{
	if(_instance == 0)
	{
		CI_GUARD(_sMutex, "UpdateMonitor::getInstance()");
		if(_instance == 0)
			_instance = new UpdateMonitor;
	}
	return _instance;
}

void UpdateMonitor::clearInstance()
{
	if(_instance)
	{
		CI_GUARD(_sMutex, "UpdateMonitor::getInstance()");
		if (_instance) {
			_instance->stopTimer();
			delete _instance;
			_instance = NULL;
		}
	}
}

UpdateMonitor::UpdateMonitor()
: _sttHWnd(0), m_pDocument(0)
{
	utvDEBUG(("UpdateMonitor()"));
}

UpdateMonitor::~UpdateMonitor()
{
	utvDEBUG(("~UpdateMonitor()"));
	_sttHWnd = NULL;
}

ciBoolean
UpdateMonitor::init(int period)
{
	utvDEBUG(("init(%d)", period));

	// set default=600
	if (period < 3600*3) {
		utvWARN(("Period minimum time is 3600*3s."));
		period = 3600*3;
	}
	utvDEBUG(("init : UpdateMonitor Timer(%d)", period));

	_sttHWnd = ::FindWindow(NULL, STARTER_WINDOWSNAME);
	if (!_sttHWnd) {
		utvERROR(("STTDlg Handle is not found."));
		return ciFalse;
	}

	initTimer("UpdateMonitor", period);

	return ciTrue;
}

void 
UpdateMonitor::SetDocument(STTDoc* document)
{
	ASSERT(document);
	m_pDocument = document;
}

void
UpdateMonitor::processExpired(ciString name, int counter, int interval)
{
	//utvDEBUG(("processExpired : name(%s), counter(%d), interval(%d)"
	//	, name.c_str()
	//	, counter
	//	, interval
	//));

	if (counter == 1) return; // 첫 타임은 스킵.

	//utvDEBUG(("Before GUARD UpdateMonitor::processExpired()"));
	ciGuard aGuard(AutoUpdateDlg::m_sLock);
	//utvDEBUG(("After GUARD UpdateMonitor::processExpired()"));

	//	Update 모니터링

	ciDEBUG(1,("Update Monitor Timer Expired"));
	update();

	// 어플리케이션이 종료 됐을지도 모르므로 watch 실행

	AppMonitor::getInstance()->watchAll();
}

ciBoolean
UpdateMonitor::update()
{
	AutoUpdateDlg dlg;
	dlg.SetDocument(m_pDocument);
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// OK 처리
	}
	else if (nResponse == IDCANCEL)
	{
		// CANCEL 처리
	}
	return ciTrue;
}
