/*
 *  Copyright ⓒ 2002 SQISSoft Inc.
 *  All Rights Reserved.
 *
 *  Created by  : 
 *  Modified by :
 *  Last updae  : 2005/12/05
 *  File name   : ubcIni.h
 */

#ifndef _ubcIni_h_
#define _ubcIni_h_

//----------------------------------------------------------------------------
//	Include Header File
//----------------------------------------------------------------------------

#include "COP/ciBaseType.h"
#include "COP/ciTime.h"
#include "COP/ciEnv.h"
#include "common/libScratch/scratchUtil.h"

#define DEFAULT_CONFIG_PATH		_COP_CD("C:\\SQISoft\\UTV1.0\\execute\\config")
#define DEFAULT_DATA_PATH		_COP_CD("C:\\SQISoft\\UTV1.0\\execute\\data")

/* 
  ubcIni : Property 의 데이터 구조 class
*/

class ubcIni
{
public:
	ubcIni(const char* filename, const char* path="", const char* sub="");
	virtual	~ubcIni() {}
	
	virtual boolean		get(const char* section, const char* key, ciString& pValue, const char* defaltValue="");
	virtual boolean		get(const char* section, const char* key, CString& pValue, const char* defaltValue="");
	virtual boolean		get(const char* section, const char* key, ciShort& pValue);
	virtual boolean		get(const char* section, const char* key, ciLong& pValue);
	virtual boolean		get(const char* section, const char* key, ciUShort& pValue);
	virtual boolean		get(const char* section, const char* key, ciULong& pValue);
	virtual boolean		get(const char* section, const char* key, ciTime& pValue);
	virtual boolean		get(const char* section, const char* key, ciBoolean& pValue);

	virtual boolean		set(const char* section, const char* key, const char* pValue);
	virtual boolean		set(const char* section, const char* key, ciShort pValue);
	virtual boolean		set(const char* section, const char* key, ciLong pValue);
	virtual boolean		set(const char* section, const char* key, ciUShort pValue);
	virtual boolean		set(const char* section, const char* key, ciULong pValue);
	virtual boolean		set(const char* section, const char* key, ciTime pValue);
	virtual boolean		set(const char* section, const char* key, ciBoolean pValue);

protected:
	virtual boolean		_get(const char* section, const char* key, int& pValue);

	string _fullpath;
};

class ubcConfig : public ubcIni {
public:
	ubcConfig(const char* filename, 
		const char* path=DEFAULT_DATA_PATH, 
		const char* sub="")	
		: ubcIni(filename,path,sub)
		{}
	virtual ~ubcConfig() {};

	// lvc is low version compatibility
	static boolean lvc_setProperty(const char* name, const char* value);
	static boolean lvc_getProperty(const char* name, ciString& value);

	static const char* getAppListName();
	static void getSlaveName(ciString& slaveName);
	static ciString _appListName;
};

#endif 

