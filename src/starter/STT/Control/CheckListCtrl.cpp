// CheckListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "CheckListCtrl.h"
#include "STT.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCheckListCtrl

CCheckListCtrl::CCheckListCtrl() : m_blInited(FALSE)
, m_nSortCol(0)
, m_bAscend(true)
, m_bCheckBoxType(false)
{
}

CCheckListCtrl::~CCheckListCtrl()
{
}


BEGIN_MESSAGE_MAP(CCheckListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CCheckListCtrl)
	ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, OnItemChanged)		
	//}}AFX_MSG_MAP
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnNMCustomdraw)
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnLvnColumnclick)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCheckListCtrl message handlers
void CCheckListCtrl::OnItemChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMLISTVIEW* pNMLV = (NMLISTVIEW*)pNMHDR;
	*pResult = 0;

	if ( m_blInited && LVIF_STATE == pNMLV->uChanged && m_bCheckBoxType)
	{
		BOOL blAllChecked = TRUE;
		int nCount = GetItemCount();
		for(int nItem = 0; nItem < nCount; nItem++)
		{
			if ( !ListView_GetCheckState(GetSafeHwnd(), nItem) )
			{
				blAllChecked = FALSE;
				break;
			}
		}
		
		HDITEM hdItem;
		hdItem.mask = HDI_IMAGE;
		if (blAllChecked)
			hdItem.iImage = 2;
		else
			hdItem.iImage = 1;
		VERIFY( m_checkHeadCtrl.SetItem(0, &hdItem) );
	}
}

BOOL CCheckListCtrl::Initialize(bool bIsCheckBoxType, bool bIsRowColorType)
{
	m_bRowColorType = bIsRowColorType;

	if (m_blInited)
		return TRUE;

	if(bIsCheckBoxType)
	{
		m_nSortCol = 1;

		CHeaderCtrl* pHeadCtrl = this->GetHeaderCtrl();
		ASSERT(pHeadCtrl->GetSafeHwnd());

		VERIFY( m_checkHeadCtrl.SubclassWindow(pHeadCtrl->GetSafeHwnd()) );
		VERIFY( m_checkImgList.Create(IDB_CHECK_BOXES, 16, 5, RGB(255,0,255)) );

		int i = m_checkImgList.GetImageCount();
		m_checkHeadCtrl.SetImageList(&m_checkImgList);
		
		HDITEM hdItem;
		hdItem.mask = HDI_IMAGE | HDI_FORMAT;
		VERIFY( m_checkHeadCtrl.GetItem(0, &hdItem) );
		hdItem.iImage = 1;
		hdItem.fmt |= HDF_IMAGE;
		
		VERIFY( m_checkHeadCtrl.SetItem(0, &hdItem) );

	}
	else
	{
		CHeaderCtrl* pHeadCtrl = this->GetHeaderCtrl();
		ASSERT(pHeadCtrl->GetSafeHwnd());

		VERIFY( m_checkImgList.Create(IDB_CHECK_BOXES, 16, 5, RGB(255,0,255)) );

		int i = m_checkImgList.GetImageCount();
		pHeadCtrl->SetImageList(&m_checkImgList);
	}

	m_bCheckBoxType = bIsCheckBoxType;
	m_blInited = TRUE;

	return TRUE;
}

void CCheckListCtrl::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );

    *pResult = 0;

	switch(pLVCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:

        *pResult = CDRF_NOTIFYITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT:
		if(m_bRowColorType)
		{
			COLORREF crText=RGB(0,0,0);
			COLORREF clrBk=RGB(255,255,255);

			int nItem = static_cast<int>( pLVCD->nmcd.dwItemSpec );

			CString tmpSection1;
			tmpSection1	= GetItemText(nItem, 2); // app status

			CString tmpSection2;
			tmpSection2	= GetItemText(nItem, 2); // host status

			if(tmpSection1 == "Active" )
			{
				crText = ACTIVE_TEXT_COLOR;
				clrBk  = ACTIVE_BACK_COLOR;
			}
			else if(tmpSection1 == "Inactive" )
			{
				crText = INACTIVE_TEXT_COLOR;
				clrBk  = INACTIVE_BACK_COLOR;
			}
			else if(tmpSection1 == "Starting")
			{
				crText = STARTING_TEXT_COLOR;
				clrBk  = STARTING_BACK_COLOR;
			}
			else if(tmpSection1 == "Stopping")
			{
				crText = STOPPING_TEXT_COLOR;
				clrBk  = STOPPING_BACK_COLOR;
			}
			else if(tmpSection1 == "OverMemory" || tmpSection2 == "Warning" )
			{
				crText = OVERMEMORY_TEXT_COLOR;
				clrBk  = OVERMEMORY_BACK_COLOR;
			}

			pLVCD->clrText   = crText;
			pLVCD->clrTextBk = clrBk;
			
			*pResult = CDRF_DODEFAULT;
		}
		else
		{
			if(pLVCD->iSubItem < m_listTextColor.GetCount())
				pLVCD->clrText = m_listTextColor.GetAt(pLVCD->iSubItem);
			if(pLVCD->iSubItem < m_listBackColor.GetCount())
				pLVCD->clrTextBk = m_listBackColor.GetAt(pLVCD->iSubItem);

			*pResult = CDRF_NOTIFYSUBITEMDRAW;
		}
		break;

	case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:

		pLVCD->clrText = m_listTextColor.GetAt(pLVCD->iSubItem);
		pLVCD->clrTextBk = m_listBackColor.GetAt(pLVCD->iSubItem);

		*pResult = CDRF_NEWFONT;
		break;

	default:

		*pResult = CDRF_DODEFAULT;
		break;
	}
}

void CCheckListCtrl::OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	// 눌려진 컬럼인덱스
	int	nSortCol = pNMLV->iSubItem;

	if(m_bCheckBoxType && nSortCol == 0)
		return;

	// 헤더 콘트롤의 포인터를 찾는다.
	CHeaderCtrl* pHeader = GetHeaderCtrl();

	// 이전 컬럼의 상태에서 아이콘을 제거한다.
	HDITEM    itemOld;
	itemOld.mask = HDI_FORMAT | HDI_IMAGE;

	if(m_nSortCol >= 0)
	{
		pHeader->GetItem(m_nSortCol, &itemOld);
		itemOld.mask = HDI_FORMAT | HDI_IMAGE;
		itemOld.iImage = 0;
		itemOld.fmt = itemOld.fmt & ~(HDF_IMAGE);
		pHeader->SetItem(m_nSortCol, &itemOld);
	}

	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = nSortCol;
	sort_param.bAscend = (m_nSortCol != nSortCol) ? true : !m_bAscend;
	sort_param.bIsStringType = (m_listSortType.GetAt(nSortCol) == 0) ? false : true;

	// 현재 정렬기준 컬럼과 정렬모드를 저장한다.
	m_nSortCol = nSortCol;
	m_bAscend = sort_param.bAscend;

	// 정렬한다.
    SortItems(&SortColumn, (LPARAM)&sort_param);

	// 아이콘을 표시한다.
	HDITEM    itemNew;
	itemNew.mask = HDI_FORMAT | HDI_IMAGE;

	pHeader->GetItem(m_nSortCol, &itemNew);
	itemNew.mask = HDI_FORMAT | HDI_IMAGE;
	itemNew.iImage = (m_bAscend == true) ? 3 : 4;
	itemNew.fmt = itemNew.fmt | HDF_IMAGE;
	pHeader->SetItem(m_nSortCol, &itemNew);
}

int CALLBACK CCheckListCtrl::SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam)
{
	SORT_PARAM*	ptrSortParam = (SORT_PARAM *)lSortParam;
	CListCtrl*	pListCtrl = (CListCtrl *)CWnd::FromHandle(ptrSortParam->hWnd);

	LVFINDINFO	lvFind;
	lvFind.flags = LVFI_PARAM;
	lvFind.lParam = lParam1;
	int nIndex1 = pListCtrl->FindItem(&lvFind);

	//lvFind2.flags = LVFI_PARAM;
	lvFind.lParam = lParam2;
	int nIndex2 = pListCtrl->FindItem(&lvFind);

	CString strItem1 = pListCtrl->GetItemText(nIndex1, ptrSortParam->nCol);
	CString	strItem2 = pListCtrl->GetItemText(nIndex2, ptrSortParam->nCol);

	CString		strItemPadding1;
	CString		strItemPadding2;

	if( ptrSortParam->bIsStringType )	// 문자 타입이면
	{
		strItemPadding1 = strItem1;
		strItemPadding2 = strItem2;
	}
	else // 숫자 타입이면
	{
		double i1 = atof(strItem1.GetBuffer(0));
		double i2 = atof(strItem2.GetBuffer(0));
		strItem1.ReleaseBuffer();
		strItem2.ReleaseBuffer();

		strItemPadding1.Format("%020.5f", i1);
		strItemPadding2.Format("%020.5f", i2);
	}

	if (ptrSortParam->bAscend == true)
		return strItemPadding1.Compare(strItemPadding2);
	else
		return strItemPadding2.Compare(strItemPadding1);
}

BOOL CCheckListCtrl::Sort(int col, bool ascend)
{
	if(col<0)
	{
		col = m_nSortCol;
		ascend = m_bAscend;
	}
	else
	{
		m_nSortCol = col;
		m_bAscend = ascend;
	}

	//
	SORT_PARAM	sort_param;
	sort_param.hWnd = GetSafeHwnd();
	sort_param.nCol = col;
	sort_param.bAscend = ascend;
	sort_param.bIsStringType = (m_listSortType.GetAt(col) == 0) ? false : true;

	//
	CHeaderCtrl* pHeader = GetHeaderCtrl();

	HDITEM    itemNew;
	itemNew.mask = HDI_FORMAT | HDI_IMAGE;

	pHeader->GetItem(col, &itemNew);
	itemNew.mask = HDI_FORMAT | HDI_IMAGE;
	itemNew.iImage = (ascend == true) ? 3 : 4;
	itemNew.fmt = itemNew.fmt | HDF_IMAGE;
	pHeader->SetItem(col, &itemNew);


    return SortItems(&SortColumn, (LPARAM)&sort_param);
}

void CCheckListCtrl::SetMaxColumn(int n)
{
	for(int i=0; i<=n; i++)
	{
		m_listSortType.Add(1);
		m_listTextColor.Add(RGB(0,0,0));
		m_listBackColor.Add(RGB(255,255,255));
	}
}

void CCheckListCtrl::SetColumnType(int index, int nSortType, COLORREF crText, COLORREF crBack)
{
	m_listSortType.SetAt(index, nSortType);
	m_listTextColor.SetAt(index, crText);
	m_listBackColor.SetAt(index, crBack);
}

BOOL CCheckListCtrl::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

/*	if(m_bReadOnly)
	{
		CRect rcClient;
		GetClientRect( &rcClient );

		pDC->FillRect(rcClient, &CBrush(::GetSysColor(COLOR_BTNFACE)));
		return TRUE;
	}
	else*/
	{
		CListCtrl::OnEraseBkgnd(pDC);

		CRect rect;
		GetClientRect(rect);

		SCROLLINFO si;
		ZeroMemory(&si, sizeof(SCROLLINFO));
		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_POS;
		GetScrollInfo(SB_HORZ, &si);
		rect.left -= si.nPos;

		for(int i=0; i<=GetHeaderCtrl()->GetItemCount();i++)
		{
			rect.right = rect.left+GetColumnWidth(i);

			CBrush brush(m_listBackColor.GetAt(i));
			pDC->FillRect(&rect, &brush);

			rect.left += GetColumnWidth(i);
		}

		return FALSE;
		//return CListCtrl::OnEraseBkgnd(pDC);
	}
	//return CListCtrl::OnEraseBkgnd(pDC);
}
