#if !defined(AFX_CHECKLISTCTRL_H__B02295B1_44F0_46AE_985A_6067632408FB__INCLUDED_)
#define AFX_CHECKLISTCTRL_H__B02295B1_44F0_46AE_985A_6067632408FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CheckListCtrl.h : header file
//
#include "CheckHeadCtrl.h"
/////////////////////////////////////////////////////////////////////////////
// CCheckListCtrl window

class CCheckListCtrl : public CListCtrl
{
// Construction
public:
	CCheckListCtrl();

// Attributes
public:

// Operations
private:

protected:
	CCheckHeadCtrl	m_checkHeadCtrl;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCheckListCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCheckListCtrl();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	// Generated message map functions

protected:
	//{{AFX_MSG(CCheckListCtrl)
	afx_msg void OnItemChanged(NMHDR* pNMHDR, LRESULT* pResult);		
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

	BOOL		m_blInited;
	CImageList	m_checkImgList;

	int			m_nSortCol;
	bool		m_bAscend;

	static int CALLBACK SortColumn(LPARAM lParam1, LPARAM lParam2, LPARAM lSortParam);

	bool		m_bCheckBoxType;
	bool		m_bRowColorType;

	CUIntArray	m_listSortType;
	CUIntArray	m_listTextColor;
	CUIntArray	m_listBackColor;

public:

	BOOL Initialize(bool bCheckBoxType=true, bool bIsRowColorType=true);

	BOOL	Sort(int col=-1, bool ascend=true);
	void	SetMaxColumn(int n);
	void	SetColumnType(int index, int nSortType, COLORREF crText=RGB(0,0,0), COLORREF crBack=RGB(255,255,255));

	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHECKLISTCTRL_H__B02295B1_44F0_46AE_985A_6067632408FB__INCLUDED_)
