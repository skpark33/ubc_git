// EditEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "EditEx.h"


// CEditEx

IMPLEMENT_DYNAMIC(CEditEx, CEdit)
CEditEx::CEditEx()
{
}

CEditEx::~CEditEx()
{
}


BEGIN_MESSAGE_MAP(CEditEx, CEdit)
END_MESSAGE_MAP()



// CEditEx 메시지 처리기입니다.


LPCSTR CEditEx::GetValueString()
{
	CEdit::GetWindowText(m_str);

	return m_str;
}

int CEditEx::GetValueInt()
{
	CEdit::GetWindowText(m_str);

	m_str.Replace(",", "");

	return atoi(m_str);
}


