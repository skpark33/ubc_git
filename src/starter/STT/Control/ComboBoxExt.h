#pragma once


// CComboBoxExt

class CComboBoxExt : public CComboBox
{
	DECLARE_DYNAMIC(CComboBoxExt)

public:
	CComboBoxExt();
	virtual ~CComboBoxExt();

	void		RemoveString(LPCTSTR lpszText);

	void		SetText(LPCSTR lpszText);

	void		GetText(CString& str);
	LPCSTR		GetText();
	LPCSTR		GetWindowText();
	int 		GetWindowValue();

protected:
	DECLARE_MESSAGE_MAP()

	CString m_str;
};


