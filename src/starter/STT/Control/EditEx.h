#pragma once


// CEditEx

class CEditEx : public CEdit
{
	DECLARE_DYNAMIC(CEditEx)

public:
	CEditEx();
	virtual ~CEditEx();

	LPCSTR	GetValueString();
	int		GetValueInt();

protected:
	DECLARE_MESSAGE_MAP()

	CString	m_str;
};


