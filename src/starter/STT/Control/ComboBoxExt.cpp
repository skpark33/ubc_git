// ComboBoxExt.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ComboBoxExt.h"

// CComboBoxExt

IMPLEMENT_DYNAMIC(CComboBoxExt, CComboBox)
CComboBoxExt::CComboBoxExt()
{
}

CComboBoxExt::~CComboBoxExt()
{
}


BEGIN_MESSAGE_MAP(CComboBoxExt, CComboBox)
END_MESSAGE_MAP()

// CComboBoxExt 메시지 처리기입니다.

void CComboBoxExt::RemoveString(LPCTSTR lpszText)
{
	int cur_sel = GetCurSel();
	int count = CComboBox::GetCount();

	for(int i=0; i<count; i++)
	{
		CString str;
		GetLBText(i, str);

		if(str.Compare(lpszText) == 0)
		{
			CComboBox::DeleteString(i);
			break;
		}
	}
}

void CComboBoxExt::SetText(LPCSTR lpszText)
{
	int count = GetCount();

	for(int i=0; i<count; i++)
	{
		CString str;
		GetLBText(i, str);

		if(str.Compare(lpszText) == 0)
		{
			SetCurSel(i);
			return;
		}
	}

	AddString(lpszText);

	for(int i=0; i<count+1; i++)
	{
		CString str;
		GetLBText(i, str);

		if(str.Compare(lpszText) == 0)
		{
			SetCurSel(i);
			return;
		}
	}
}

LPCSTR CComboBoxExt::GetWindowText()
{
	CComboBox::GetWindowText(m_str);

	return m_str;
}

int CComboBoxExt::GetWindowValue()
{
	CComboBox::GetWindowText(m_str);

	return atoi(m_str);
}

void CComboBoxExt::GetText(CString& str)
{
	int index = GetCurSel();

	GetLBText(index, m_str);

	str = m_str;
}

LPCSTR CComboBoxExt::GetText()
{
	int index = GetCurSel();

	GetLBText(index, m_str);

	return m_str;
}

