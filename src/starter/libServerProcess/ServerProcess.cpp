// ServerCheck.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#ifdef _LINK_STARTER_
#include <UBC/ubcIni.h>
#include "common/libScratch/scratchUtil.h"
#include "common/libHttpRequest/HttpRequest.h"

#define ciDEBUG(x,msg)	scratchUtil::getInstance()->myDEBUG msg
#define ciSET_DEBUG(x,y)

#else
#include <ci/libDebug/ciDebug.h>
//#include "CMN/libCommon/ubcIniMux.h"
#endif
#include "ServerProcess.h"


#define		DEFAULT_UBC_CENTER_IP			"ubccenter.sqisoft.com"
#define		DEFAULT_UBC_CENTER_PORT			8080


ciSET_DEBUG(10, "CServerProcess");

///////////////////////////////////////////////////
CServerProcess* CServerProcess::_instance = NULL; 

CServerProcess* CServerProcess::getInstance()
{
	if(!_instance)
	{
		_instance = new CServerProcess;
	}
	return _instance;
}

void CServerProcess::clearInstance()
{
	if(_instance)
	{
		delete _instance;
		_instance = NULL;
	}
}
///////////////////////////////////////////////////


// CServerCheck

CServerProcess::CServerProcess()
{
	//
	//ubcConfig aIni("UBCConnect");
	//ubcIni aIni("UBCConnect", _COP_CD("C:\\Project\\ubc\\config"), "data");
	ubcIni aIni("UBCConnect", "CONFIGROOT", "data");

	//
	string center_ip="";
	aIni.get("UBCCENTER","IP", center_ip);
	m_strCenterIP = ( center_ip.length()>0 ? center_ip.c_str() : DEFAULT_UBC_CENTER_IP );

	//
	string center_port="";
	aIni.get("UBCCENTER","WEBPORT", center_port);
	m_nCenterPort = ( center_port.length()>0 ? atoi(center_port.c_str()) : DEFAULT_UBC_CENTER_PORT );

	ciDEBUG(10, ("CENTER_IP=%s", m_strCenterIP.c_str()) );
	ciDEBUG(10, ("CENTER_PORT=%d", m_nCenterPort) );
}

CServerProcess::~CServerProcess()
{
}


// CServerCheck 메시지 처리기입니다.

int CServerProcess::SendStatus(CServerProcessInfo& info)
{
	//
	ciString customer, serverId, serverIp;
	info.GetServerInfo(customer, serverId, serverIp);

	ciDEBUG(10, ("Customer=%s", customer.c_str()) );
	ciDEBUG(10, ("ServerId=%s", serverId.c_str()) );
	ciDEBUG(10, ("ServerIp=%s", serverIp.c_str()) );

	//
	info.GetFirstServerProcessInfo();
	SERVERPROCESS_INFO*	p_info;

	int count = 0;
	int success_count = 0;
	while( (p_info=info.GetNextServerProcessInfo()) != NULL)
	{
		ciDEBUG(10, ("%d:AppId=%s",			count, p_info->appId.c_str()) );
		ciDEBUG(10, ("%d:BinaryName=%s",	count, p_info->binaryName.c_str()) );
		ciDEBUG(10, ("%d:Argument=%s",		count, p_info->argument.c_str()) );
		ciDEBUG(10, ("%d:Properties=%s",	count, p_info->properties.c_str()) );
		ciDEBUG(10, ("%d:Debug=%s",			count, p_info->debug.c_str()) );
		ciDEBUG(10, ("%d:Status=%s",		count, p_info->status.c_str()) );
		ciDEBUG(10, ("%d:StartUpTime=%s",	count, p_info->startUpTime.getTimeString().c_str()) );
		count++;

		// make url for connecting ubc-center
		CString url;
		url.Format(_T("http://%s:%d/UBC_Server/set_server_process_status.asp"), m_strCenterIP.c_str(), m_nCenterPort);

		// sending status-info.
		CString send_msg;
		send_msg.Format("customer=%s&serverId=%s&appId=%s&serverIp=%s&binaryName=%s&argument=%s&properties=%s&debug=%s&status=%s&startUpTime=%s", 
			CHttpRequest::ToEncodingString(customer.c_str()),
			CHttpRequest::ToEncodingString(serverId.c_str()),
			CHttpRequest::ToEncodingString(p_info->appId.c_str()),
			CHttpRequest::ToEncodingString(serverIp.c_str()),
			CHttpRequest::ToEncodingString(p_info->binaryName.c_str()),
			CHttpRequest::ToEncodingString(p_info->argument.c_str()),
			CHttpRequest::ToEncodingString(p_info->properties.c_str()),
			CHttpRequest::ToEncodingString(p_info->debug.c_str()),
			CHttpRequest::ToEncodingString(p_info->status.c_str()),
			CHttpRequest::ToEncodingString(p_info->startUpTime.getTimeString().c_str()) );

		// connect to ubc-center
		CString response = "";
		CHttpRequest http_request;
		BOOL ret_val = http_request.RequestPost(url, send_msg, response);

		if(ret_val)
		{
			if(response == "OK")
			{
				// all ok
				success_count++;
			}
		}
	}

	return success_count;
}

BOOL CServerProcess::InitCommand(const char* lpszCustomer, const char* lpszServerId)
{
	//
	if(lpszCustomer == NULL || lpszServerId == NULL)
		return FALSE;

	// make url for connecting ubc-center
	CString url;
	url.Format(_T("http://%s:%d/UBC_Server/init_server_process_command_list.asp"), m_strCenterIP.c_str(), m_nCenterPort);

	// receiving status-change.
	CString send_msg;
	send_msg.Format("customer=%s&serverId=%s", 
		CHttpRequest::ToEncodingString(lpszCustomer),
		CHttpRequest::ToEncodingString(lpszServerId) );

	// connect to ubc-center
	CString out_msg = "";
	CHttpRequest http_request;
	BOOL ret_val = http_request.RequestPost(url, send_msg, out_msg);

	if(!ret_val) return FALSE;

	if(out_msg == "OK" || out_msg == "Fail") return TRUE;

	return FALSE;
}

BOOL CServerProcess::GetCommand(const char* lpszCustomer, const char* lpszServerId, const char* lpszAppId, ciString& command)
{
	//
	if(lpszCustomer == NULL || lpszServerId == NULL || lpszAppId == NULL)
		return FALSE;

	//
	SERVERPROCESS_INFOLIST info_list;
	BOOL ret_val = GetCommandList(lpszCustomer, lpszServerId, lpszAppId, info_list);

	//
	SERVERPROCESS_INFOLIST::iterator itr = info_list.begin();
	SERVERPROCESS_INFOLIST::iterator end = info_list.end();

	if(ret_val)
	{
		//
		SERVERPROCESS_INFO* info = (SERVERPROCESS_INFO*)(*itr);
		if(info != NULL)
		{
			ciDEBUG(10, ("command=%s", info->command.c_str()) );
			command = info->command;
		}
		else
			ret_val = FALSE;
	}

	//
	for(; itr!=end; itr++)
	{
		SERVERPROCESS_INFO* info = (SERVERPROCESS_INFO*)(*itr);
		if(info != NULL) delete info;
	}
	info_list.clear();

	//
	return ret_val;
}

BOOL CServerProcess::GetCommandList(const char* lpszCustomer, const char* lpszServerId, const char* lpszAppId, SERVERPROCESS_INFOLIST& infoList)
{
	//
	ciDEBUG(10, ("Customer=%s",	lpszCustomer) );
	ciDEBUG(10, ("ServerId=%s",	lpszServerId) );
	ciDEBUG(10, ("AppId=%s",	(lpszAppId==NULL) ? "NULL" : lpszAppId) );

	// make url for connecting ubc-center
	CString url;
	url.Format(_T("http://%s:%d/UBC_Server/get_server_process_command_list.asp"), m_strCenterIP.c_str(), m_nCenterPort);

	// receiving status-change.
	CString send_msg;
	send_msg.Format("customer=%s&serverId=%s", 
		CHttpRequest::ToEncodingString(lpszCustomer),
		CHttpRequest::ToEncodingString(lpszServerId) );
	if(lpszAppId != NULL)
	{
		send_msg += "&appId=";
		send_msg += lpszAppId;
	}

	// connect to ubc-center
	CString out_msg = "";
	CHttpRequest http_request;
	BOOL ret_val = http_request.RequestPost(url, send_msg, out_msg);

	if(!ret_val) return FALSE;

	CString response = out_msg;
	CStringArray line_list;

	//
	int pos = 0;
	CString token = response.Tokenize(_T("\r\n"), pos);
	while(token != _T(""))
	{
		line_list.Add(token);
		token = response.Tokenize(_T("\r\n"), pos);
	}

	//
	if(line_list.GetCount() > 0 && line_list.GetAt(0) == "OK")
	{
		ciDEBUG(1,("Response=Exist") );

		SERVERPROCESS_INFO* info = NULL;

		for(int i=1; i<line_list.GetCount(); i++)
		{
			const CString& line = line_list.GetAt(i);

			int pos = 0;
			CString key = line.Tokenize(_T("="), pos);
			CString value = ( (key=="") ? "" : line.Tokenize(_T("="),pos) );

			if(key == "appId")
			{
				//
				if(info)
				{
					ciDEBUG(1,("appId : %s", info->appId.c_str()));
					ciDEBUG(1,("command : %s", info->command.c_str()));
				}

				//
				info = new SERVERPROCESS_INFO;
				infoList.push_back(info);

				//
				info->appId = value;
			}
			else if(key == "command" && info)
				info->command = value;
		}

		if(info)
		{
			ciDEBUG(1,("appId : %s", info->appId.c_str()));
			ciDEBUG(1,("command : %s", info->command.c_str()));
		}

		return TRUE;
	}
	else if(line_list.GetCount() > 0 && line_list.GetAt(0) == "Fail")
	{
		// not exist
		ciDEBUG(1,("Response=NotExist") );
	}
	else
	{
		// connection fail
		ciDEBUG(1,("Response=Unknown") );
	}

	return FALSE;
}

BOOL CServerProcess::GetCommandList(CServerProcessInfo& spi)
{
	ciString customer, serverId, serverIp;
	spi.GetServerInfo(customer, serverId, serverIp);

	SERVERPROCESS_INFOLIST info_list;
	BOOL ret_val = GetCommandList(customer.c_str(), serverId.c_str(), NULL, info_list);

	if(ret_val)
	{
		SERVERPROCESS_INFOLIST::iterator itr = info_list.begin();
		SERVERPROCESS_INFOLIST::iterator end = info_list.end();

		for(; itr!=end; itr++)
		{
			SERVERPROCESS_INFO* info = (SERVERPROCESS_INFO*)(*itr);
			if(info != NULL)
			{
				spi.AddServerProcessInfo(*info);
				delete info;
			}
		}
		info_list.clear();
	}

	return ret_val;
}

/*

// Test URL

// send status
http://ubccenter.sqisoft.com:8080/UBC_Server/set_server_process_status.asp?customer=1&serverId=2&serverIp=3&appId=4&binaryName=5&argument=6&properties=7&debug=8&status=9&startUpTime=2011-12-05%2012:13:14

// get command list
http://ubccenter.sqisoft.com:8080/UBC_Server/get_server_process_command_list.asp?customer=1&serverId=2

// init command
http://ubccenter.sqisoft.com:8080/UBC_Server/init_server_process_command_list.asp?customer=1&serverId=2

*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CServerProcessInfo::CServerProcessInfo()
{
	m_customer = "";
	m_serverId = "";
	m_serverIp = "";
}

CServerProcessInfo::CServerProcessInfo(const char* lpszCustomer, const char* lpszServerId, const char* lpszServerIp)
{
	SetServerInfo(lpszCustomer, lpszServerId, lpszServerIp);
}

CServerProcessInfo::~CServerProcessInfo()
{
	GetFirstServerProcessInfo();

	SERVERPROCESS_INFO* info;
	while( (info=GetNextServerProcessInfo()) != NULL )
	{
		delete info;
	}

	m_listServerProcess.clear();
}

void CServerProcessInfo::GetFirstServerProcessInfo()
{
	m_itr = m_listServerProcess.begin();
}

SERVERPROCESS_INFO* CServerProcessInfo::GetNextServerProcessInfo()
{
	if(m_itr == m_listServerProcess.end()) return NULL;
	return (SERVERPROCESS_INFO*)*m_itr++;
}

void CServerProcessInfo::GetServerInfo(ciString& customer, ciString& serverId, ciString& serverIp)
{
	customer = m_customer;
	serverId = m_serverId;
	serverIp = m_serverIp;
}

void CServerProcessInfo::SetServerInfo(const char* lpszCustomer, const char* lpszServerId, const char* lpszServerIp)
{
	m_customer = lpszCustomer;
	m_serverId = lpszServerId;
	m_serverIp = lpszServerIp;
}

void CServerProcessInfo::AddServerProcessInfo(SERVERPROCESS_INFO& info)
{
	SERVERPROCESS_INFO* p_info = new SERVERPROCESS_INFO;

	*p_info = info;

	m_listServerProcess.push_back(p_info);
}

void CServerProcessInfo::AddServerProcessInfo(	const char* lpszAppId,			// 어플리케이션ID
												const char* lpszBinaryName,		// 실행 파일 이름
												const char* lpszArgument,		// 입력 인수
												const char* lpszProperties,		// CORBA 인수
												const char* lpszDebug,			// 디버깅 인수
												const char* lpszStatus,			// 어플리케이션 상태
												ciTime	tmStartUpTime)
{
	SERVERPROCESS_INFO* p_info = new SERVERPROCESS_INFO;

	p_info->appId		= lpszAppId;
	p_info->binaryName	= lpszBinaryName;
	p_info->argument	= lpszArgument;
	p_info->properties	= lpszProperties;
	p_info->debug		= lpszDebug;
	p_info->status		= lpszStatus;
	p_info->startUpTime	= tmStartUpTime;

	m_listServerProcess.push_back(p_info);
}
