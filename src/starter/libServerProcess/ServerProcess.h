#pragma once
#ifdef _LINK_STARTER_
#include <COP/ciBaseType.h>
#include <COP/ciTime.h>
#else
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libBase/ciTime.h>
#endif


#define		SERVERPROCESS_INFO_STARTING		"Starting"
#define		SERVERPROCESS_INFO_STOPPING		"Stopping"


// SERVERPROCESS_INFO
typedef struct {
	ciString	appId;			// 어플리케이션ID
	ciString	binaryName;		// 실행 파일 이름
	ciString	argument;		// 입력 인수
	ciString	properties;		// CORBA 인수
	ciString	debug;			// 디버깅 인수
	ciString	status;			// 어플리케이션 상태
	ciTime		startUpTime;	// 기동 시각
	ciString	command;		// SERVERPROCESS_INFO_STARTING / SERVERPROCESS_INFO_STOPPING
} SERVERPROCESS_INFO;
typedef		list<SERVERPROCESS_INFO*>	SERVERPROCESS_INFOLIST;


class CServerProcessInfo;

/////////////////////////////////////////////////////////////////////////////////////////////////////
// CServerProcess
class CServerProcess
{
public:
	static CServerProcess* getInstance();
	static void	clearInstance();
protected:
	static CServerProcess*	_instance;


public:
	virtual ~CServerProcess();
protected:
	CServerProcess();

	ciString	m_strCenterIP;
	DWORD		m_nCenterPort;

	BOOL		RequestPost(const char* lpUrl, const char* lpszSendMsg, ciString& strOutMsg);
	ciString	ConvertToURLString(const char* lpszStr, bool bReturnNullString=false);

public:
	void		GetCenterInfo(ciString& strCenterIP, DWORD& nCenterPort) { strCenterIP=m_strCenterIP; nCenterPort=m_nCenterPort; };

	int 		SendStatus(CServerProcessInfo& spInfo);		// return success-count

	BOOL		InitCommand(								// 서버프로세스 command 초기화
								const char* lpszCustomer, 
								const char* lpszServerId);

	BOOL		GetCommand(									// 서버프로세스 하나만 command 수신. TRUE=command있음, FALSE=없음.
								const char* lpszCustomer, 
								const char* lpszServerId, 
								const char* lpszAppId, 
								ciString& command);

	BOOL		GetCommandList(								// 서버프로세스 command 리스트 수신. TRUE=command있음, FALSE=없음.
								const char* lpszCustomer, 
								const char* lpszServerId, 
								const char* lpszAppId,		// lpszAppId=NULL 일 경우, 해당서버 전체 command수신;
								SERVERPROCESS_INFOLIST& infoList); // 사용후 반드시(!!!) infoList 의 내부객체 delete. 귀찮으면 아래 함수를 사용하세요.

	BOOL		GetCommandList(CServerProcessInfo& spi);	// 서버프로세스 변경정보 리스트 수신. TRUE=command있음, FALSE=없음.
															// spi.SetServerInfo(...)로 customer, serverid 필드에 정보를 채워줘야함.
};


/////////////////////////////////////////////////////////////////////////////////////////////////////
// CServerProcessInfo
class CServerProcessInfo
{
public:
	CServerProcessInfo();
	CServerProcessInfo(const char* lpszCustomer, const char* lpszServerId, const char* lpszServerIp);
	virtual ~CServerProcessInfo();

protected:
	ciString	m_customer;		// HYUNDAI, KIA, ...
	ciString	m_serverId;		// 12, 34, ...
	ciString	m_serverIp;		// 서버IP주소

	SERVERPROCESS_INFOLIST		m_listServerProcess;
	SERVERPROCESS_INFOLIST::iterator	m_itr;

public:
/////////////////////////////////////////////////////////////////////////////////////////////////////
	void				GetFirstServerProcessInfo();	// 정보get 전에 초기화
	SERVERPROCESS_INFO*	GetNextServerProcessInfo();		// 정보를 get (마지막=>NULL 리턴)

/////////////////////////////////////////////////////////////////////////////////////////////////////
	void	GetServerInfo(ciString& customer, ciString& serverId, ciString& serverIp);
	void	SetServerInfo(const char* lpszCustomer, const char* lpszServerId, const char* lpszServerIp);

	void	AddServerProcessInfo(SERVERPROCESS_INFO& info);
	void	AddServerProcessInfo(const char* lpszAppId,			// 어플리케이션ID
								const char* lpszBinaryName,		// 실행 파일 이름
								const char* lpszArgument,		// 입력 인수
								const char* lpszProperties,		// CORBA 인수
								const char* lpszDebug,			// 디버깅 인수
								const char* lpszStatus,			// 어플리케이션 상태
								ciTime	tmStartUpTime);			// 기동 시각
};
