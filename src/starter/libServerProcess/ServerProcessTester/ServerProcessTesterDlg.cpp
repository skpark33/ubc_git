// ServerProcessTesterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ServerProcessTester.h"
#include "ServerProcessTesterDlg.h"

#include "../ServerProcess.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CServerProcessTesterDlg 대화 상자




CServerProcessTesterDlg::CServerProcessTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServerProcessTesterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CServerProcessTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_CENTER_IP, m_editCenterIP);
	DDX_Control(pDX, IDC_EDIT_CENTER_PORT, m_editCenterPort);
	DDX_Control(pDX, IDC_EDIT_CUSTOMER, m_editCustomer);
	DDX_Control(pDX, IDC_EDIT_SERVERID, m_editServerID);
	DDX_Control(pDX, IDC_EDIT_APPID, m_editAppID);
	DDX_Control(pDX, IDC_EDIT_SERVERIP, m_editServerIP);
	DDX_Control(pDX, IDC_EDIT_BINARYNAME, m_editBinaryName);
	DDX_Control(pDX, IDC_EDIT_ARGUMENT, m_editArgument);
	DDX_Control(pDX, IDC_EDIT_PROPERTIES, m_editProperties);
	DDX_Control(pDX, IDC_EDIT_DEBUG, m_editDebug);
	DDX_Control(pDX, IDC_EDIT_STATUS, m_editStatus);
	DDX_Control(pDX, IDC_DATE_STARTUPTIME, m_dcStartUpTime);
	DDX_Control(pDX, IDC_TIME_STARTUPTIME, m_tcStartUpTime);
}

BEGIN_MESSAGE_MAP(CServerProcessTesterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_GET_COMMAND_LIST, &CServerProcessTesterDlg::OnBnClickedButtonGetCommandList)
	ON_BN_CLICKED(IDC_BUTTON_INIT_COMMAND_LIST, &CServerProcessTesterDlg::OnBnClickedButtonInitCommandList)
END_MESSAGE_MAP()


// CServerProcessTesterDlg 메시지 처리기

bool getIpAddress(ciString& strAddr)
{
    strAddr = "";
    char ac[80];

    if (gethostname(ac, sizeof(ac)) == -1) {
        return false;
    }

    struct hostent* phe = gethostbyname(ac);
    if ( phe == 0 ) {
        return false;
    }

    if ( phe->h_addr_list[0] != 0 ) {
        struct in_addr addr;
        memcpy(&addr, phe->h_addr_list[0], sizeof(struct in_addr));
        strAddr = inet_ntoa(addr);
    }

    return true;
}

BOOL CServerProcessTesterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	//
	ciString center_ip;
	DWORD center_port; CString str_center_port;
	CServerProcess::getInstance()->GetCenterInfo(center_ip, center_port);
	str_center_port.Format("%d", center_port);

	//
	ciString ip_addr;
	getIpAddress(ip_addr);

	//
	m_editCenterIP.SetWindowText(center_ip.c_str());
	m_editCenterPort.SetWindowText(str_center_port);

	m_editCustomer.SetWindowText("SQISOFT");
	m_editServerID.SetWindowText("12");
	m_editAppID.SetWindowText("SP_TESTER");
	m_editServerIP.SetWindowText(ip_addr.c_str());
	m_editBinaryName.SetWindowText("ServerProcessTester.exe");
	m_editArgument.SetWindowText("+argument");
	m_editProperties.SetWindowText("+properties");
	m_editDebug.SetWindowText("+debug");
	m_editStatus.SetWindowText("ACTIVE");

	CTime cur_tm = CTime::GetCurrentTime();
	m_dcStartUpTime.SetTime(&cur_tm);
	m_tcStartUpTime.SetTime(&cur_tm);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CServerProcessTesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CServerProcessTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CServerProcessTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CServerProcessTesterDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();

	//
	CString	str_customer;
	m_editCustomer.GetWindowText(str_customer);

	CString	str_serverId;
	m_editServerID.GetWindowText(str_serverId);

	CString	str_appId;
	m_editAppID.GetWindowText(str_appId);

	CString	str_serverIp;
	m_editServerIP.GetWindowText(str_serverIp);

	CString	str_binaryName;
	m_editBinaryName.GetWindowText(str_binaryName);

	CString	str_argument;
	m_editArgument.GetWindowText(str_argument);

	CString	str_properties;
	m_editProperties.GetWindowText(str_properties);

	CString	str_debug;
	m_editDebug.GetWindowText(str_debug);

	CString	str_status;
	m_editStatus.GetWindowText(str_status);

	CTime date_startuptime, time_startuptime;
	m_dcStartUpTime.GetTime(date_startuptime);
	m_tcStartUpTime.GetTime(time_startuptime);
	CTime dt_startuptime(
		date_startuptime.GetYear(),
		date_startuptime.GetMonth(),
		date_startuptime.GetDay(),
		time_startuptime.GetHour(),
		time_startuptime.GetMinute(),
		time_startuptime.GetSecond() );

	//
	SERVERPROCESS_INFO info;

	info.appId		= str_appId;
	info.binaryName	= str_binaryName;
	info.argument	= str_argument;
	info.properties	= str_properties;
	info.debug		= str_debug;
	info.status		= str_status;
	info.startUpTime.set(dt_startuptime.GetTime());

	//
	CServerProcessInfo spi(str_customer, str_serverId, str_serverIp);
	spi.AddServerProcessInfo(info);

	info.appId += "2";
	spi.AddServerProcessInfo(info);

	//
	int success_count = CServerProcess::getInstance()->SendStatus(spi);
	if( success_count )
	{
		CString msg;
		msg.Format("OK (Success_Count:%d)", success_count);
		AfxMessageBox(msg, MB_ICONINFORMATION);
	}
	else
		AfxMessageBox("FAIL !!!", MB_ICONSTOP);
}

void CServerProcessTesterDlg::OnBnClickedButtonGetCommandList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//
	CString	str_customer;
	m_editCustomer.GetWindowText(str_customer);

	CString	str_serverId;
	m_editServerID.GetWindowText(str_serverId);

	CString	str_serverIp = "";

	//
	CServerProcessInfo spi(str_customer, str_serverId, str_serverIp);

	//
	BOOL ret_val = CServerProcess::getInstance()->GetCommandList(spi);
	if( ret_val )
	{
		CString msg;

		spi.GetFirstServerProcessInfo();

		SERVERPROCESS_INFO* info;
		while( (info=spi.GetNextServerProcessInfo()) != NULL )
		{
			msg += "\r\nAppId=";
			msg += info->appId.c_str();
			msg += ",Command=";
			msg += info->command.c_str();
		}

		AfxMessageBox(msg, MB_ICONINFORMATION);
	}
	else
		AfxMessageBox("FAIL or Nothing !!!", MB_ICONSTOP);
}

void CServerProcessTesterDlg::OnBnClickedButtonInitCommandList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//
	CString	str_customer;
	m_editCustomer.GetWindowText(str_customer);

	CString	str_serverId;
	m_editServerID.GetWindowText(str_serverId);

	//
	BOOL ret_val = CServerProcess::getInstance()->InitCommand(str_customer, str_serverId);
	if( ret_val )
		AfxMessageBox("INIT OK !!!", MB_ICONINFORMATION);
	else
		AfxMessageBox("ERROR !!!", MB_ICONSTOP);
}
