//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ServerProcessTester.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SERVERPROCESSTESTER_DIALOG  102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_EDIT_CENTER_IP              1000
#define IDC_EDIT_CENTER_PORT            1001
#define IDC_EDIT_CUSTOMER               1002
#define IDC_EDIT_SERVERID               1003
#define IDC_EDIT_APPID                  1004
#define IDC_EDIT_SERVERIP               1005
#define IDC_EDIT_BINARYNAME             1006
#define IDC_EDIT_ARGUMENT               1007
#define IDC_EDIT_PROPERTIES             1008
#define IDC_EDIT_DEBUG                  1009
#define IDC_EDIT_STATUS                 1010
#define IDC_DATE_STARTUPTIME            1013
#define IDC_TIME_STARTUPTIME            1014
#define IDC_BUTTON_GET_COMMAND_LIST     1015
#define IDC_BUTTON1                     1016
#define IDC_BUTTON_INIT_COMMAND_LIST    1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
