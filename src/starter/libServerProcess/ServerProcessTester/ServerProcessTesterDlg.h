// ServerProcessTesterDlg.h : 헤더 파일
//

#pragma once

#include "afxwin.h"
#include "afxdtctl.h"


// CServerProcessTesterDlg 대화 상자
class CServerProcessTesterDlg : public CDialog
{
// 생성입니다.
public:
	CServerProcessTesterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SERVERPROCESSTESTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()

public:
	CEdit m_editCenterIP;
	CEdit m_editCenterPort;

	CEdit m_editCustomer;
	CEdit m_editServerID;
	CEdit m_editAppID;
	CEdit m_editServerIP;
	CEdit m_editBinaryName;
	CEdit m_editArgument;
	CEdit m_editProperties;
	CEdit m_editDebug;
	CEdit m_editStatus;
	CDateTimeCtrl m_dcStartUpTime;
	CDateTimeCtrl m_tcStartUpTime;

	afx_msg void OnBnClickedButtonGetCommandList();
	afx_msg void OnBnClickedButtonInitCommandList();
};
