 /*! \file releaserWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _releaserWorld_h_
#define _releaserWorld_h_


#include <ci/libWorld/ciWorld.h> 

class releaserWorld : public ciWorld {
public:
	releaserWorld();
	~releaserWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

protected:	

};
		
#endif //_releaserWorld_h_
