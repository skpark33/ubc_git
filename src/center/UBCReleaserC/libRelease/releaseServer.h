#ifndef _releaseServer_h_
#define _releaseServer_h_


#include <UBCReleaserC/libRelease/releaseInfo.h>


class releaseServer : public virtual releaseManager {
public:
	static releaseServer*	getInstance();
	static void	clearInstance();
	virtual ~releaseServer() ;

	virtual int			makeVersionFile();

	virtual void		setMainRelease();
	virtual void		setPreRelease();

protected:
	releaseServer();

	virtual int				navigateDir(FILE* fd,float release_no,const char* root, const char* subdir);
	virtual void			searchPath(const char* filename, ciString& outPath);


	static releaseServer*	_instance;
};

#endif // _releaseServer_h_
