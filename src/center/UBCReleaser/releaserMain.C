#include "releaserWorld.h"
#include "ci/libDebug/ciArgParser.h"

int main(int argc, char** argv)
{
    
	//ciEnv::defaultEnv(ciFalse, "utv1");

	releaserWorld* world = new releaserWorld();

    if(!world) {
        cerr<<"Unable to create releaserWorld"<<endl;
        exit(1);
    }

	ciArgParser::initialize(argc, argv);

    if(world->init(argc, argv) == ciFalse) {
       // cerr<<"Init world error"<<endl;
        world->fini(2);
    }


    
	//world->run();
    world->fini(0);
	return 0;
}

