#ifndef _releaseUI_h_
#define _releaseUI_h_

#include <UBCReleaser/libRelease/releaseInfo.h>


class releaseUI : public virtual releaseManager {
public:

	static releaseUI*	getInstance();
	static void	clearInstance();
	virtual ~releaseUI() ;

protected:
	releaseUI();


	static releaseUI*	_instance;

};

#endif // _releaseUI_h_
