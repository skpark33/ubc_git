#ifndef _releaseInfo_h_
#define _releaseInfo_h_

#include <cstdio>
#include <string>
#include <list>
#include <map>
#include <fstream>

#include <ci/libBase/ciTime.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libBase/ciStringTokenizer.h>


#define RELEASE_DELI ","
#define PADDING		 64

class releaseInfo {
public:
	releaseInfo() : shouldUpdate(ciTrue), releaseNo(2.3), ver(9090101) {}
	~releaseInfo() {};

	ciBoolean	shouldUpdate;
	ciFloat		releaseNo;
	ciULong		ver;
	ciString	target;
	ciString	source;
	ciString	targetModifiedTime;

	ciString	sourceFilename;
	ciString	targetFilename;

	void	toString(ciString& outval);
};

typedef list<releaseInfo*>	ReleaseInfoList;


class releaseManager {
public:
	virtual ~releaseManager() { clear();}

	virtual int			init(float release_no);
	virtual int			release(float release_no, ciBoolean interactive=ciFalse, const char* filter="");
	virtual int			showRelease(float release_no);
	virtual int			showChanged();
	virtual int			makeVersionFile();
	virtual ciBoolean	createLine(float release_no, ciBoolean shuoldUpdate, const char* source, const char* target);
	virtual ciBoolean	removeLine(float release_no,const char* target,ciBoolean interactive);

	virtual void		setMainRelease();
	virtual void		setPreRelease();
	virtual void		setMainWeddingRelease();
	virtual void		setPreWeddingRelease();
	ciBoolean			isMainRelease() { return _isMainRelease;}

protected:
	releaseManager() : _isMainRelease(ciTrue) {}

	virtual void			clear();
	virtual int				load();
	virtual void			write(float release_no);
	virtual void			write(FILE* fd);

	virtual ciULong			genVersion(ciULong oldValue=0L);
	virtual releaseInfo*	findFileInfo(const char* filename);

	virtual ciBoolean		isUpdated(ciString& source, ciString& target, ciString& outValue, ciBoolean silent=ciFalse);
	virtual ciBoolean		isSelfUpdated(ciString& source, ciString& inoutValue, ciBoolean silent=ciFalse);

	virtual ciBoolean		wouldYou();
	virtual ciBoolean		copy(const char* source, const char* target);

	virtual void			getFilename(const char* fullpath, ciString& filename);
	virtual int				getExt(const char* filename ,ciString& ext);

	virtual ciBoolean		createReleaseNote(float release_no, ciBoolean interactive);

	virtual int				navigateDir(FILE* fd,float release_no,const char* root, const char* subdir);
	virtual void			searchPath(const char* filename, ciString& outPath);

	ciBoolean				getSoftPath(const char* key, const char* path, ciString& outVal);

protected:
	virtual void	_init();

	void			_openIni(const char* filename, const char* configPath, const char* sub);
	ciBoolean		_readIni(const char* section,const char* key, ciString& value, const char* defaultValue);

	ciStringSet _lines;
	ReleaseInfoList	_list;
	ciMutex			_lock;

	ciString	_releaseFile;
	ciString _releaseRoot;
	ciString _installRoot;
	ciString _ftpRoot;
	ciString _configFile;

	ciBoolean	_isMainRelease;
};


#endif // _releaseInfo_h_
