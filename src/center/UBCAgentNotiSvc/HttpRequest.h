#pragma once

#include <afxmt.h>
#include <afxinet.h>
#include <afxtempl.h>

// CHttpRequest

class CHttpRequest
{
protected:

	class CBufferDeleter
	{
	public:
		CBufferDeleter(char* buf) { Init(); m_buf=buf; };
		CBufferDeleter(wchar_t* wbuf) { Init(); m_wbuf=wbuf; };
		CBufferDeleter(CHttpFile* hf) { Init(); m_httpfile=hf; };
		CBufferDeleter(CHttpConnection* hc) { Init(); m_httpconn=hc; };
		~CBufferDeleter() { Clear(); };

	protected:
		char*		m_buf;
		wchar_t*	m_wbuf;
		CHttpFile*	m_httpfile;
		CHttpConnection*	m_httpconn;

		void	Init()
					{
						m_buf = NULL;
						m_wbuf = NULL;
						m_httpfile = NULL;
						m_httpconn = NULL;
					};
		void	Clear()
					{
						if(m_buf) delete[]m_buf;
						if(m_wbuf) delete[]m_wbuf;
						if(m_httpfile) { m_httpfile->Close(); delete m_httpfile; }
						if(m_httpconn) { m_httpconn->Close(); delete m_httpconn; }
					};
	};

////////////////////////////////////////////////////////////////////////////////
public:
	CHttpRequest(LPCTSTR lpszIP=NULL, UINT nPort=0);
	virtual ~CHttpRequest();

protected:
//	static	CString	m_strCenterIP;
//	static	UINT	m_nCenterPort;

	static	CCriticalSection	m_lockUseSSL;
	static	BOOL	m_nUseSSL;

	static	CCriticalSection	m_lockIpToDomain;
	static	CMapStringToString	m_mapIpToDomain;

	CString	m_strServerIP;
	UINT	m_nServerPort;

	CString	m_strRequestURL;
	CString	m_strErrorMsg;

	BOOL	Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg);
	void	GetLineList(CString& str, CStringArray& line_list);

	BOOL	HttpToHttps(const char* http, CString& https);
	BOOL	IpToDomainName(const char* strHttpServerIp, CString& domain);
	BOOL	isUseSSL();

	CString	GetINIPath();

public:
//	BOOL	Init(CONNECTION_TYPE conType, LPCTSTR lpszServerIP=NULL, UINT nServerPort=0);
			// CONNECTION_CENTER, CONNECTION_SERVER => connect to UBC-center or UBC-server
			// CONNECTION_USER_DEFINED => connect to user defined server,port

	BOOL	RequestGet(LPCTSTR lpUrl, CString &strOutMsg);
	BOOL	RequestGet(LPCTSTR lpUrl, CStringArray &strOutMsgList);
	BOOL	RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg);
	BOOL	RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CStringArray& strOutMsgList);

	static CString	ToEncodingString(CString str, bool bReturnNullToString=false);
	static CString	ToString(int nValue);

	void	GetServerInfo(CString& strIP, UINT& nPort) { strIP=m_strServerIP; nPort=m_nServerPort; };
	LPCTSTR	GetErrorMsg() { return (LPCTSTR)m_strErrorMsg; };
	LPCTSTR	GetRequestURL() { return (LPCTSTR)m_strRequestURL; };
};
