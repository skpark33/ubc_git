// HttpRequest.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "HttpRequest.h"

#include <afxinet.h>


#define		DEFAULT_CENTER_IP			_T("ubccenter.sqisoft.com")
#define		DEFAULT_CENTER_WEBPORT		(8080)

#define		HTTP_SESSION_TIMEOUT		(60*1000)	// 1min


// CHttpRequest
//CString	CHttpRequest::m_strCenterIP = _T("");
//UINT	CHttpRequest::m_nCenterPort = 0;

CHttpRequest::CHttpRequest(LPCTSTR lpszIP, UINT nPort)
{
	if(lpszIP==NULL || _tcslen(lpszIP)==0)
	{
		m_strServerIP = DEFAULT_CENTER_IP;
	}
	else
	{
		m_strServerIP = lpszIP;
	}

	if(nPort == 0)
	{
		m_nServerPort = DEFAULT_CENTER_WEBPORT;
	}
	else
	{
		m_nServerPort = nPort;
	}
}

CHttpRequest::~CHttpRequest()
{
}

BOOL CHttpRequest::Request(bool isGet, LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg)
{
	m_strErrorMsg = _T("");

	//
	if( _tcsnicmp( lpUrl, _T("http://"), 7) )
	{
		m_strRequestURL.Format(_T("http://%s:%d%s%s"), 
			m_strServerIP,
			m_nServerPort,
			(lpUrl[0] == _T('/') ? "" : _T("/")),
			ToEncodingString(lpUrl)
		);
	}
	else
		m_strRequestURL = lpUrl;

	//
	bool is_https = false;
	CString https;
	if(HttpToHttps(m_strRequestURL, https))
	{
		is_https = true;
		m_strRequestURL = https;
	}

	//
	DWORD dwSearviceType;
	CString strServer, strObject;
	INTERNET_PORT nPort;

	if(!AfxParseURL(m_strRequestURL, dwSearviceType, strServer, strObject, nPort))
	{
		m_strErrorMsg.Format(_T("Invalid URL : %s"), m_strRequestURL);
		return FALSE;
	}

	CInternetSession Session("UBCAgentNotiSvc");
	Session.SetOption(INTERNET_OPTION_CONNECT_TIMEOUT,			HTTP_SESSION_TIMEOUT);
	Session.SetOption(INTERNET_OPTION_CONTROL_RECEIVE_TIMEOUT,	HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_CONTROL_SEND_TIMEOUT,		HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT,		HTTP_SESSION_TIMEOUT );
	Session.SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT,		HTTP_SESSION_TIMEOUT );

	BOOL bRet = FALSE;
	DWORD dwReadSize;

	try
	{
		CHttpConnection* pServer = Session.GetHttpConnection(strServer, nPort);
		if(pServer == NULL)
		{
			m_strErrorMsg = _T("Session.GetHttpConnection is NULL");
			return FALSE;
		}
		CBufferDeleter hc_deleter(pServer);

		DWORD flag;
		if(isUseSSL() && is_https)
			flag = INTERNET_FLAG_EXISTING_CONNECT | INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID | INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
		else
			flag = INTERNET_FLAG_EXISTING_CONNECT;

		CHttpFile *pFile = NULL;
		if(isGet)
		{
			CString strHeader = "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\nAccept: */*\r\n";
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_GET, strObject, NULL, 1, NULL, NULL, flag);
			if(pFile==NULL)
			{
				m_strErrorMsg = _T("HttpConnection.OpenRequest is NULL");
				return FALSE;
			}
			pFile->SendRequest(strHeader, (LPVOID)(LPCTSTR)strHeader, strHeader.GetLength());
		}
		else
		{
			CString strHeader = "Content-Type: application/x-www-form-urlencoded\r\nUser-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
			pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject, NULL, 1, NULL, NULL, flag);
			if(pFile==NULL)
			{
				m_strErrorMsg = _T("HttpConnection.OpenRequest is NULL");
				return FALSE;
			}
			pFile->SendRequest(strHeader, (LPVOID)lpszSendMsg, _tcslen(lpszSendMsg));
		}
		CBufferDeleter hf_deleter(pFile);

		//
		char szLen[32]="";
		DWORD dwLenSize = sizeof(szLen);
		pFile->QueryInfo( HTTP_QUERY_CONTENT_LENGTH, szLen, &dwLenSize );
		int length = atoi( szLen);

		TCHAR* buf = new TCHAR[length+1];
		CBufferDeleter buf_deleter(buf);
		::ZeroMemory(buf, sizeof(TCHAR)*(length+1));
		dwReadSize = pFile->Read(buf, length);

		//
		if(dwReadSize != strlen(buf) || buf[0] == '<')
		{
			bRet = FALSE;
			m_strErrorMsg = buf;
		}
		else
		{
			const char* utf8_idx = strstr(buf, "CodePage=utf-8");

			if( utf8_idx != NULL )
			{
				//std::string out_msg;
				//UTF8toA(buf, out_msg);
				//strOutMsg = out_msg.c_str();
			}
			else
				strOutMsg = buf;

			bRet = TRUE;
		}
	}
	catch (CInternetException* e)
	{
		TCHAR szError[255];
		e->GetErrorMessage(szError, 255);
		e->Delete();
		return bRet;
	}
	catch (CException* e)
	{
		TCHAR szError[255];
		e->GetErrorMessage(szError, 255);
		e->Delete();
		return bRet;
	}
	catch (...)
	{
		return bRet;
	}

	return bRet;
}

BOOL CHttpRequest::RequestGet(LPCTSTR lpUrl, CString &strOutMsg)
{
	return Request(true, lpUrl, NULL, strOutMsg);
}

BOOL CHttpRequest::RequestGet(LPCTSTR lpUrl, CStringArray &strOutMsgList)
{
	CString strOutMsg;
	BOOL ret_val = Request(true, lpUrl, NULL, strOutMsg);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

BOOL CHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CString& strOutMsg)
{
	return Request(false, lpUrl, lpszSendMsg, strOutMsg);
}

BOOL CHttpRequest::RequestPost(LPCTSTR lpUrl, LPCTSTR lpszSendMsg, CStringArray& strOutMsgList)
{
	CString strOutMsg;
	BOOL ret_val = Request(false, lpUrl, lpszSendMsg, strOutMsg);

	GetLineList(strOutMsg, strOutMsgList);

	return ret_val;
}

CString CHttpRequest::ToEncodingString(CString str, bool bReturnNullToString)
{
	if(str.GetLength() == 0) return (bReturnNullToString ? _T("(null)") : _T(""));
	str.Replace(_T("%"), _T("%25"));
	str.Replace(_T(" "), _T("%20"));
	str.Replace(_T("&"), _T("%26"));
	str.Replace(_T("="), _T("%3d"));
	str.Replace(_T("+"), _T("%2b"));
	str.Replace(_T("?"), _T("%3f"));
	return str;
}

CString CHttpRequest::ToString(int nValue)
{
	TCHAR buf[16] = {0};
	_stprintf(buf, _T("%d"), nValue);

	return buf;
}

void CHttpRequest::GetLineList(CString& str, CStringArray& line_list)
{
	int pos = 0;
	CString token = str.Tokenize(_T("\r\n"), pos);
	while(token != _T(""))
	{
		line_list.Add(token);
		token = str.Tokenize(_T("\r\n"), pos);
	}
}

////////////////////////////////////////////////////////////////////////////////////////

CCriticalSection CHttpRequest::m_lockUseSSL;
int CHttpRequest::m_nUseSSL = -1;

BOOL CHttpRequest::isUseSSL()
{
	LOCK(m_lockUseSSL);

	if(m_nUseSSL < 0)
	{
		CString str_use_ssl;
		GetPrivateProfileString(_T("ROOT"), _T("USE_SSL"), str_use_ssl, _T("0"), _T("..\\config\\data\\UBCVariables.ini"));

		m_nUseSSL = _ttoi(str_use_ssl);
	}

	return m_nUseSSL;
}

CCriticalSection CHttpRequest::m_lockIpToDomain;
CMapStringToString CHttpRequest::m_mapIpToDomain;

BOOL CHttpRequest::IpToDomainName(const char* strHttpServerIp, CString& domain)
{
	LOCK(m_lockIpToDomain);

	CString https_domain;
	if( m_mapIpToDomain.Lookup(strHttpServerIp, https_domain) )
	{
		if(https_domain.GetLength() > 0)
		{
			domain = https_domain;
			return TRUE;
		}
		return FALSE;
	}

	GetPrivateProfileString(_T("SSLDomainNameList"), strHttpServerIp, https_domain, _T("..\\config\\data\\UBCVariables.ini"));
	m_mapIpToDomain.SetAt(strHttpServerIp, https_domain);
	if(https_domain.GetLength() > 0)
	{
		domain = https_domain;
		return TRUE;
	}

	return FALSE;
}

BOOL CHttpRequest::HttpToHttps(const char* http, CString& https)
{
	if(isUseSSL() == false) //
	{
		https = http;
		return false;
	}
	if(strncmp(http, "http://", 7) != 0)
	{
		https = http;
		return false;
	}

	char* ip_end_ptr = (char*)http+7; // ip-pointer after of "http://"
	while(*ip_end_ptr != ':' && *ip_end_ptr != '/' && *ip_end_ptr != 0) // find end char (:, /, NULL)
		ip_end_ptr++;
	char ip_end_char = *ip_end_ptr;

	*ip_end_ptr = 0;
	CString ip = http+7;

	*ip_end_ptr = ip_end_char;
	CString url = ip_end_ptr;

	CString domainName;
	if(IpToDomainName(ip, domainName))
	{
		https = "https://";
		https += domainName;
		https += url;

		return true;
	}
	else
	{
		https = http;
	}

	return false;
}
