#include "stdAfx.h"
#include "common/libCrc32/Crc32Static.h"

int main(int argc, char** argv)
{
    
	if(argc <= 1)
	{
		printf("Usage : GetCRC target_file_full_path\n");
		exit(1);
	}

	char fullname[256];
	sprintf(fullname,"%s", argv[1]);
	DWORD dwCrc32, dwErrorCode = NO_ERROR;
	dwErrorCode = CCrc32Static::FileCrc32Win32(fullname, dwCrc32);
	if(dwErrorCode != NO_ERROR) {
		dwCrc32=dwErrorCode;
		printf("GetCRC ERROR : %ld\n", dwCrc32);
		exit(1);
	}
	printf("0x%08x\n",dwCrc32);
	return 0;
}

