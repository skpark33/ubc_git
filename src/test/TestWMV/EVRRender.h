/************************************************************************************/
/*! @file EVRRender.h
	@brief VMR을 이용한 동영상 재생 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/01/06\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 정운형)
	-# <2010/01/06:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include "BasicInterface.h"

#include <Evr.h>

#pragma comment( lib, "strmiids.lib" )
#pragma comment( lib, "Quartz.lib" )
//#pragma comment( lib, "d3d9.lib" )
//#pragma comment( lib, "d3dx9.lib" )

//#ifndef SAFE_RELEASE
//	#define SAFE_RELEASE(i) {if (i) i->Release(); i = NULL;}
//#endif
//
//#ifndef JIF
//	#define JIF( x ) if( FAILED(hr = ( x )) ) { return hr; }
//#endif


//! VMR을 이용한 동영상 재생 클래스
/*!
*/
class CEVRRender : public CBasicInterface
{
protected:
	// Encapsulate VMR functionality in this class
    //IGraphBuilder				*m_pGrapgBuilder;		///<그래프빌더 인터페이스
    IMediaControl				*m_pMediaControl;		///<미디어 컨트롤 인터페이스
    IMediaSeeking				*m_pMediaSeeking;		///<미디어 seeking 인터페이스
	IBasicAudio					*m_pBasicAudio;				///<Basic audio
	IBaseFilter					*m_pFilter;				///<랜더링 필터
	IMFVideoDisplayControl		*m_pWindowlessControl;
//    IVMRWindowlessControl		*m_pWindowlessControl;	///<VMR7 windowless 컨트롤 인터페이스
	//IVMRAspectRatioControl		*m_pAspectRatio;		///<VMR7 aspectration 컨트로 인터페이스
	IVideoWindow				*m_pVideoWindow;		///<Video window 인터페이스
	IMediaEventEx				*m_pMediaEventEx;		///<Media event 인터페이스
	//IVMRWindowlessControl9	*m_WindowlessControl9;	///<VMR9 windowless 컨트롤 인터페이스

	DWORD						m_dwRotId;				///<Rot 테이블에 등록되는 아이디
	//HWND						m_hWnd;					///<동영상이 그려질 창의 윈도우 핸들
	RECT						m_rcWnd;				///<동영상이 그려질 창의 영역
	//CString					m_strFile;				///<동영상 파일의 경로
	int							m_nMode;				///<랜더링 모드
	bool						m_bOpen;				///<파일이 열린상태

public: 
	CEVRRender(CWnd* pWnd);												///<생성자
	//CEVRRender(HWND hwnd, int nMode = E_VMR7_WINDOWED);				///<생성자
	virtual ~CEVRRender(void);											///<소멸자

	virtual bool	Open(LPCTSTR lpszFilename);							///<동영상 파일을 오픈한다.
	virtual bool	Play(void);											///<동영상 파일을 재생한다.
	virtual bool	Pause(void);										///<동영상 파일의 재생을 일시 중지 한다.
	virtual bool	Stop(void);											///<동영상 파일의 재생을 중지 한다.
	virtual void	Close(void);										///<동영상 파일을 닫는다.
	virtual bool	SetVolume(int nVol);								///<동영상의 음량을 설정한다.

	virtual double	GetTotalPlayTime(void);								///<동영상 파일의 총 재생시간을 반환한다.
	virtual double	GetCurrentPlayTime(void);							///<동영상 파일의 현재 재생시간을 반환한다.
	virtual void    SetPosition(double pos);							///<동영상의 지정된 위치로 이동한다.
	virtual void	SeekToStart(void);									///<동영상의 처음 위치로 이동한다.

	virtual bool	SetVideoRect(LPRECT pRect);							///<동영상의 영역을 설정한다.
	virtual bool	GetVideoRect(LPRECT pRect);							///<동영상의 원본영역을 반한한다.

	virtual bool	RePaintVideo(/*HDC hdc*/);								///<동영상을 다시 그려준다.
	virtual void	DisplayChanged(void) {};							///<모니터가 변경되었을때 처리를 해 준다.
	virtual bool	SetAspectRatioMode(ASPECT_RATIO_MODE mode);			///<동영상의 재생시에 화면의 비율을 유지하는지를 설정한다.

	virtual void	ReleaseAllInterfaces(void);							///<그래프빌더의 모든 필터와 인터페이스들을 제거한다.
	virtual void	SetRenderMode(int nMode) { m_nMode = nMode; }		///<Video render의 type을 설정한다.

protected:
	bool			SetVMR7WindowLess(void);							///<VMR7 windowless 필터를 랜더필터로 설정한다.
	bool			SetVMR7Windowed(void);								///<VMR7 windowed 필터를 랜더필터로 설정한다.
	bool			SetVMR9WindowLess(void);							///<VMR9 windowless 필터를 랜더필터로 설정한다.
	bool			SetDefaultWindowLess(void);							///<System default windowless 필터를 랜더필터로 설정한다.
	bool			SetDefaultWindowed(void);							///<System default windowed 필터를 랜더필터로 설정한다.
	bool			SetEVR(void);							///<System default windowed 필터를 랜더필터로 설정한다.

	bool			SetRenderFilter(void);								///<비디오 랜더를 설정한다.

	HRESULT			AddToRot(IUnknown *pUnkGraph);						///<Running time object 테이블에 등록을 한다.
	void			RemoveFromRot(void);								///<Running time object 테이블에서 제거한다.

	bool			IsWindowsMediaFile(LPCTSTR lpszFilename);
};