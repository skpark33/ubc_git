/************************************************************************************/
/*! @file VMRRender.h
	@brief VMR을 이용한 동영상 재생 클래스 선언파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/01/06\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 정운형)
	-# <2010/01/06:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include "BasicInterface.h"

//! VMR을 이용한 동영상 재생 클래스
/*!
*/
class CVMRRender : public CBasicInterface
{
public: 
	CVMRRender(CWnd* pWnd);												///<생성자
	virtual ~CVMRRender(void);											///<소멸자

	virtual void	ReleaseAllInterfaces(void);							///<그래프빌더의 모든 필터와 인터페이스들을 제거한다.

	virtual bool	SetAspectRatioMode(ASPECT_RATIO_MODE mode);			///<동영상의 재생시에 화면의 비율을 유지하는지를 설정한다.
	virtual bool	SetVideoRect(LPRECT pRect);							///<동영상의 영역을 설정한다.
	virtual bool	GetVideoRect(LPRECT pRect);							///<동영상의 원본영역을 반한한다.

	virtual bool	Open(LPCTSTR lpszFilename);							///<동영상 파일을 오픈한다.

	virtual bool	RePaintVideo(void);									///<동영상을 다시 그려준다.
	virtual void	DisplayChanged(void);								///<모니터가 변경되었을때 처리를 해 준다.

protected:
	bool			SetVMR7WindowLess(void);							///<VMR7 windowless 필터를 랜더필터로 설정한다.
	bool			SetVMR7Windowed(void);								///<VMR7 windowed 필터를 랜더필터로 설정한다.
	bool			SetVMR9WindowLess(void);							///<VMR9 windowless 필터를 랜더필터로 설정한다.
	bool			SetDefaultWindowed(void);							///<System default windowed 필터를 랜더필터로 설정한다.

	bool			SetRenderFilter(void);								///<비디오 랜더를 설정한다.
	bool			SetWindowsMediaFilters(LPCWSTR lpszFileName);		///<윈도우 미디어 파일을 재생하기위한 필터들을 구성한다.
	bool			IsWindowsMediaFile(LPCTSTR lpszFilename);			///<윈도우 미디어 파일인지를 판단한다.
	IBaseFilter*	CreateEncodec(CString inFriendlyName);				///<코덱의 이름으로 필터를 찾아준다
};