//
// CWMVPlayer.h
//

#pragma once


#include <DShow.h>
#include <D3D9.h>
#include <vmr9.h>

#include "dshowutil.h"


// Filter graph notification to the specified window
#define WM_GRAPHNOTIFY  (WM_USER+20)


class CWMVPlayer
{
public:
	CWMVPlayer();
	virtual ~CWMVPlayer();

public:
	HRESULT CreateGraph(HWND hWnd);

	HRESULT RenderMovieFile(LPCTSTR lpszFilename);

	bool RunMovies(void);
	bool StopMovies(void);
	bool PauseMovies(void);

	HRESULT SetAspectRatio(BOOL bAspectRatio);

	HRESULT GetNativeMovieSize(CRect & pos);
	HRESULT SetMoviePosition(CRect & pos);
	HRESULT GetMoviePosotion(CRect & pos);

	HRESULT GetCurrentPosition(double * outPosition);
	HRESULT GetStopPosition(double * outPosition);
	HRESULT SetCurrentPosition(double inPosition);
	HRESULT SetStartStopPosition(double inStart, double inStop);
	HRESULT GetDuration(double * outDuration);
	HRESULT SetPlaybackRate(double inRate);

	HRESULT SetNotifyWindow(HWND inWindows);
	void    HandleEvent(WPARAM wParam, LPARAM lParam);

	IMediaEventEx * GetEventHandle() const;
	OAFilterState GetCurrentState();

private:
	inline void SafeRelease(IUnknown * filter)
	{
		if (NULL != filter)
		{
			filter->Release();
			filter = NULL;
		}
	}

	HRESULT UsedVideoMixingRenderer9();
	BOOL IsWindowsMediaFile(LPCTSTR lpszFilename);
	HRESULT GetUnconnectedPin(IBaseFilter* pFilter,  PIN_DIRECTION PinDir, IPin **ppPin);
	HRESULT RenderMoivesToVMR9(IGraphBuilder* pGB, LPCTSTR lpszFilename);

	//HRESULT AddToRot(IUnknown *pUnkGraph, DWORD *pdwRegister);
	//void RemoveFromRot(DWORD pdwRegister);

	void ShowMsg(TCHAR *szFormat, ...);

	IBaseFilter* CreateEncodec(CString inFriendlyName);
	HRESULT CreateFilter(REFCLSID clsid, IBaseFilter **ppFilter);

private:
	IGraphBuilder  *  pGraphBuilder;
	IMediaControl  *  pMediaControl;
	IMediaSeeking  *  pMediaSeeking;
	IMediaEventEx  *  pMediaEventEx;

	IBaseFilter    * pVMR9;
	IVMRWindowlessControl9 * pVMR9Control;

	DWORD dwRegister;
	HWND mPlayerHandle;
};
