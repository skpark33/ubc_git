// TestWMVDlg.h : 헤더 파일
//

#pragma once

#include "afxwin.h"

//#include "WMVPlayer.h"
#include "BasicInterface.h"
#include "ReposControl.h"

// CTestWMVDlg 대화 상자
class CTestWMVDlg : public CDialog
{
// 생성입니다.
public:
	CTestWMVDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TESTWMV_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//CWMVPlayer*		m_player;
	CBasicInterface*		m_player;
	CReposControl	m_repos;

public:
	afx_msg void OnBnClickedButtonOpen();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnBnClickedButtonPlay();
	afx_msg void OnBnClickedButtonPause();
	CFont m_font;
	CEdit m_editResult;
protected:
	virtual void OnCancel();

	afx_msg LRESULT OnAddResult(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedRadioAspectRatio();
	afx_msg void OnBnClickedRadioRender();

	CButton m_btnOpen;
	CButton m_btnPlay;
	CButton m_btnPause;
	int m_nAspectRatio;
	CButton m_radioFullscreen;
	CButton m_radioAspectRatio;
	int m_nRender;
	CButton m_radioVMR7;
	CButton m_radioVMR9;
	CButton m_radioEVR;

	CWnd	m_wnd;
	CStatic m_groupMode;
	CStatic m_groupScreen;
	CButton m_btnClose;

	CString	m_strMediaFullPath;
	double	m_iTotalPlayTime;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};


/*
#define VOLUME_FULL     0L
#define VOLUME_SILENCE  -10000L

// File filter for OpenFile dialog
#define FILE_FILTER_TEXT \
    TEXT("Windows Media Files (*.asf; *.wma; *.wmv)\0*.asf; *.wma; *.wmv\0") \
    TEXT("All Files (*.*)\0*.*;\0\0")

// Begin default media search at root directory
#define DEFAULT_MEDIA_PATH  TEXT("\\\0")

// Defaults used with audio-only files
#define DEFAULT_AUDIO_WIDTH     240
#define DEFAULT_AUDIO_HEIGHT    120
#define DEFAULT_VIDEO_WIDTH     320
#define DEFAULT_VIDEO_HEIGHT    240
#define MINIMUM_VIDEO_WIDTH     200
#define MINIMUM_VIDEO_HEIGHT    120

#define APPLICATIONNAME TEXT("DSPlay Sample\0")
#define CLASSNAME       TEXT("DSPlayWMSample\0")

#define WM_GRAPHNOTIFY  WM_USER+20

enum PLAYSTATE {Stopped, Paused, Running, Init};

//
// Macros
//
#define SAFE_RELEASE(x) { if (x) x->Release(); x = NULL; }

#define JIF(x) if (FAILED(hr=(x))) \
    {Msg(TEXT("FAILED(hr=0x%x) in ") TEXT(#x) TEXT("\n\0"), hr); return hr;}

#define LIF(x) if (FAILED(hr=(x))) \
    {Msg(TEXT("FAILED(hr=0x%x) in ") TEXT(#x) TEXT("\n\0"), hr);}

*/