// TestWMVDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "TestWMV.h"
#include "TestWMVDlg.h"

#include "OverlayInterface.h"
#include "VMRRender.h"
#include "EVRRender.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		USE_BASICINTERFACE



#define		PLAY_CHECK_TIME		100



// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CTestWMVDlg 대화 상자




CTestWMVDlg::CTestWMVDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestWMVDlg::IDD, pParent)
	, m_repos(this)
	, m_nAspectRatio(0)
	, m_nRender (0)
	, m_iTotalPlayTime ( 0.0f )
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_player = NULL;
}

void CTestWMVDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_RESULT, m_editResult);
	DDX_Control(pDX, IDC_BUTTON_OPEN, m_btnOpen);
	DDX_Control(pDX, IDC_BUTTON_PLAY, m_btnPlay);
	DDX_Control(pDX, IDC_BUTTON_PAUSE, m_btnPause);
	DDX_Radio(pDX, IDC_RADIO_FULLSCREEN, m_nAspectRatio);
	DDX_Control(pDX, IDC_RADIO_FULLSCREEN, m_radioFullscreen);
	DDX_Control(pDX, IDC_RADIO_ASPECTRATIO, m_radioAspectRatio);
	DDX_Radio(pDX, IDC_RADIO_VMR7, m_nRender);
	DDX_Control(pDX, IDC_RADIO_VMR7, m_radioVMR7);
	DDX_Control(pDX, IDC_RADIO_VMR9, m_radioVMR9);
	DDX_Control(pDX, IDC_RADIO_EVR,  m_radioEVR);
	DDX_Control(pDX, IDC_STATIC_MODE, m_groupMode);
	DDX_Control(pDX, IDC_STATIC_SCREEn, m_groupScreen);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
}

BEGIN_MESSAGE_MAP(CTestWMVDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_OPEN, &CTestWMVDlg::OnBnClickedButtonOpen)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CTestWMVDlg::OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_PLAY, &CTestWMVDlg::OnBnClickedButtonPlay)
	ON_BN_CLICKED(IDC_BUTTON_PAUSE, &CTestWMVDlg::OnBnClickedButtonPause)
	ON_MESSAGE(WM_ADD_RESULT, OnAddResult)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_RADIO_ASPECTRATIO, &CTestWMVDlg::OnBnClickedRadioAspectRatio)
	ON_BN_CLICKED(IDC_RADIO_FULLSCREEN, &CTestWMVDlg::OnBnClickedRadioAspectRatio)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CTestWMVDlg 메시지 처리기


BOOL CTestWMVDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	m_repos.AddControl(&m_btnOpen, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_btnClose, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_btnPlay, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_btnPause, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_editResult, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_repos.AddControl(&m_radioFullscreen, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_radioAspectRatio, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_radioVMR7, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_radioVMR9, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_radioEVR, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);

	CRect rect;
	GetClientRect(rect);
	rect.right -= 180;

	m_wnd.Create(NULL, _T("view"), WS_CHILD | WS_VISIBLE, rect, this, 0xfeff);

	m_repos.AddControl(&m_wnd, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);

	m_repos.AddControl(&m_groupMode, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);
	m_repos.AddControl(&m_groupScreen, REPOS_MOVE, REPOS_FIX, REPOS_MOVE, REPOS_FIX);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CTestWMVDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CTestWMVDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CTestWMVDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


/*
#include <strmif.h>
#include <vfwmsgs.h>
#include <uuids.h>
#include "control.h"
#include "evcode.h"
#include <stdio.h>
#include <tchar.h>
#include <crtdbg.h>
#include <stddef.h> 
#include <strsafe.h>
#include <atlconv.h>
#include <wmsdkidl.h>
#include "resource.h"
#include "nserror.h"
#include "D3D9.h"
#include "vmr9.h"



#ifndef NUMELMS
   #define NUMELMS(aa) (sizeof(aa)/sizeof((aa)[0]))
#endif

const int AUDIO=1, VIDEO=2; // Used for enabling playback menu items

#pragma warning(disable: 4100)

//
// Global data
//
HWND      ghApp=0;
HMENU     ghMenu=0;
HINSTANCE ghInst=0;
TCHAR     g_szFileName[MAX_PATH]={0};
BOOL      g_bAudioOnly=FALSE, g_bFullscreen=FALSE;
LONG      g_lVolume=VOLUME_FULL;
PLAYSTATE g_psCurrent=Stopped;

// DirectShow interfaces
IGraphBuilder *pGB = NULL;
IBaseFilter   *pFilter = NULL;
IVMRFilterConfig9* pConfig = NULL;
IMediaControl *pMC = NULL;
IMediaEventEx *pME = NULL;
IVideoWindow  *pVW = NULL;
IBasicAudio   *pBA = NULL;
IBasicVideo   *pBV = NULL;
IMediaSeeking *pMS = NULL;

//------------------------------------------------------------------------------
// Name: Msg()
// Desc: Displays the specified message in a Message Box.
//------------------------------------------------------------------------------
void Msg(__in LPCTSTR szFormat, ...)
{
    TCHAR szBuffer[1024];  // Large buffer for long filenames or URLs
    
    // Format the input string
    va_list pArgs;
    va_start(pArgs, szFormat);

    // Use a bounded buffer size to prevent buffer overruns.  Limit count to
    // character size minus one to allow for a NULL terminating character.
    (void)StringCchVPrintf(szBuffer, NUMELMS(szBuffer), szFormat, pArgs);
            
    va_end(pArgs);

    // Display a message box with the formatted string
    MessageBox(NULL, szBuffer, TEXT("DSPlay Sample"), MB_OK);
}
//------------------------------------------------------------------------------
// Name: CheckVisibility()
// Desc: Set global values for presence of video window.
//------------------------------------------------------------------------------
void CheckVisibility(void)
{
    long lVisible;
    HRESULT hr;

    if ((!pVW) || (!pBV))
    {
        // Audio-only files have no video interfaces.  This might also
        // be a file whose video component uses an unknown video codec.
        g_bAudioOnly = TRUE;
        return;
    }
    else
    {
        // Clear the global flag
        g_bAudioOnly = FALSE;
    }

    hr = pVW->get_Visible(&lVisible);
    if (FAILED(hr))
    {
        // If this is an audio-only clip, get_Visible() won't work.
        if (hr == E_NOINTERFACE)
        {
            g_bAudioOnly = TRUE;
        }
        else
        {
            Msg(TEXT("Failed(%08lx) in pVW->get_Visible()!\r\n"), hr);
        }
    }
}

//------------------------------------------------------------------------------
// Name: InitVideoWindow()
// Desc: Sets the window size and position.
//
// nMultiplier, nDivider: These parameters give the ratio by which to stretch or
// shrink the video. For example, for 2X size, use (2,1). For 1/2 size, use (1,2).
//------------------------------------------------------------------------------
HRESULT InitVideoWindow(int nMultiplier, int nDivider)
{
    LONG lHeight, lWidth;
    HRESULT hr = S_OK;
    RECT rect;

    if (!pBV)
        return S_OK;

    // Read the default video size
    hr = pBV->GetVideoSize(&lWidth, &lHeight);
    if (hr == E_NOINTERFACE)
        return S_OK;

    // Enable all playback menu items
//    EnablePlaybackMenu(TRUE, VIDEO);

    // Account for requests of normal, half, or double size
    lWidth  = lWidth  * nMultiplier / nDivider;
    lHeight = lHeight * nMultiplier / nDivider;

    int nTitleHeight  = GetSystemMetrics(SM_CYCAPTION);
    int nBorderWidth  = GetSystemMetrics(SM_CXBORDER);
    int nBorderHeight = GetSystemMetrics(SM_CYBORDER);

    // Account for size of title bar and borders for exact match
    // of window client area to default video size
    SetWindowPos(ghApp, NULL, 0, 0, lWidth + 2*nBorderWidth,
            lHeight + nTitleHeight + 2*nBorderHeight,
            SWP_NOMOVE | SWP_NOOWNERZORDER);

    // Move the video size/position to within the application window
    GetClientRect(ghApp, &rect);
    JIF(pVW->SetWindowPosition(rect.left, rect.top, rect.right, rect.bottom));

    return hr;
}

//------------------------------------------------------------------------------
// Name: InitPlayerWindow()
// Desc: Sets the window size and position when there is no video.
//------------------------------------------------------------------------------
HRESULT InitPlayerWindow(void)
{
    // Reset to a default size for audio and after closing a clip
    SetWindowPos(ghApp, NULL, 0, 0,
                 DEFAULT_AUDIO_WIDTH,
                 DEFAULT_AUDIO_HEIGHT,
                 SWP_NOMOVE | SWP_NOOWNERZORDER);

    // Check the default 'normal size' menu item
//    CheckSizeMenu(ID_FILE_SIZE_NORMAL);

    // Disable the playback menu items
//    EnablePlaybackMenu(FALSE, 0);

    return S_OK;
}

HRESULT PlayMovieInWindow(__in_ecount(MAX_PATH) LPTSTR szFile)
{
    USES_CONVERSION;
    WCHAR wFile[MAX_PATH];
    WCHAR * pszFile = NULL;
    HRESULT hr;

    // Check input string
    if (!szFile)
        return E_POINTER;

    // Clear open dialog remnants before calling RenderFile()
    UpdateWindow(ghApp);

    // Convert filename to wide character string
    pszFile = T2W(szFile);
    if (!pszFile)
        return E_POINTER;
    (void)StringCchCopyW(wFile, NUMELMS(wFile), pszFile);

    // Get the interface for DirectShow's GraphBuilder
    JIF(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, 
                         IID_IGraphBuilder, (void **)&pGB));

    JIF(CoCreateInstance(CLSID_VideoMixingRenderer9, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&pFilter));

	JIF(pFilter->QueryInterface(IID_IVMRFilterConfig9, reinterpret_cast<void**>(&pConfig)));
  
    JIF(pConfig->SetRenderingMode( VMR9Mode_Renderless ));
  
	JIF(pConfig->SetNumberOfStreams(2));

//	JIF(SetAllocatorPresenter( pFilter, window ));
  
	JIF(pGB->AddFilter(pFilter, L"Video Mixing Renderer 9"));

    // Get the media event interface before building the graph
    JIF(pGB->QueryInterface(IID_IMediaEventEx, (void **)&pME));

    // Have the graph builder construct the appropriate graph automatically
    JIF(pGB->RenderFile(wFile, NULL));

    if( SUCCEEDED( hr ) )
    {
        // QueryInterface for DirectShow interfaces
        JIF(pGB->QueryInterface(IID_IMediaControl, (void **)&pMC));
        JIF(pGB->QueryInterface(IID_IMediaSeeking, (void **)&pMS));

        // Query for video interfaces, which may not be relevant for audio files
        JIF(pGB->QueryInterface(IID_IVideoWindow, (void **)&pVW));
        JIF(pGB->QueryInterface(IID_IBasicVideo,  (void **)&pBV));

        // Query for audio interfaces, which may not be relevant for video-only files
        JIF(pGB->QueryInterface(IID_IBasicAudio, (void **)&pBA));

        // Have the graph signal event via window callbacks for performance
        JIF(pME->SetNotifyWindow((OAHWND)ghApp, WM_GRAPHNOTIFY, 0));

        // Is this an audio-only file (no video component)?
        CheckVisibility();

        if (!g_bAudioOnly)
        {
            // Setup the video window
            JIF(pVW->put_Owner((OAHWND)ghApp));
            JIF(pVW->put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS | WS_CLIPCHILDREN));
            JIF(InitVideoWindow(1, 1));
        }
        else
        {
            // Initialize the default player window and enable playback menu items
            // that don't involve manipulating video size
            JIF(InitPlayerWindow());
//            EnablePlaybackMenu(TRUE, AUDIO);
        }

        // Complete window initialization
//        CheckSizeMenu(ID_FILE_SIZE_NORMAL);
        ShowWindow(ghApp, SW_SHOWNORMAL);
        UpdateWindow(ghApp);
        SetForegroundWindow(ghApp);
//        UpdateMainTitle();
        g_bFullscreen = FALSE;

        // Run the graph to play the media file
        JIF(pMC->Run());

        g_psCurrent = Running;
        SetFocus(ghApp);
    }

    return hr;
}

*/
void CTestWMVDlg::OnBnClickedButtonOpen()
{
	// TODO: Add your control notification handler code here

	UpdateData();

	CFileDialog dlg(TRUE);
	if(dlg.DoModal() == IDOK)
	{
		m_strMediaFullPath = dlg.GetPathName();

		//SendMessage(WM_ADD_RESULT, (WPARAM)(LPCTSTR)m_strMediaFullPath);

		//
		if(m_player != NULL)
		{
#ifdef USE_BASICINTERFACE
			m_player->Close();
#endif
			delete m_player;
		}

		//
		CRect rect;
		GetClientRect(rect);
		rect.right -= 180;

#ifndef USE_BASICINTERFACE
		//
		m_player = new CWMVPlayer();   
		m_player->CreateGraph(GetSafeHwnd());
		m_player->RenderMovieFile((LPCTSTR)fullpath);
		m_player->SetMoviePosition(rect);
		m_player->SetNotifyWindow(GetSafeHwnd());   
		m_player->SetAspectRatio(m_nAspectRatio);
#else
		switch(m_nRender)
		{
		case 0:
			m_player = new CVMRRender(&m_wnd);
			m_player->SetRenderType(E_VMR7_WINDOWLESS);
			break;
		case 1:
			m_player = new CVMRRender(&m_wnd);
			m_player->SetRenderType(E_VMR9_WINDOWLESS);
			break;
		case 2:
			m_player = new CEVRRender(&m_wnd);
			break;
		default:
			m_player = new COverlayInterface(&m_wnd);
			break;
		}
		m_player->SetParentWindow(m_wnd.GetSafeHwnd());
		m_player->Open((LPCTSTR)m_strMediaFullPath);
		m_player->SetAspectRatioMode(m_nAspectRatio ? ASPECT_RATIO_MODE::LETTER_BOX : ASPECT_RATIO_MODE::STRETCHED);
		m_player->SetVideoRect(rect);
#endif

		//SendMessage(WM_ADD_RESULT, (WPARAM)"");

		m_radioVMR7.EnableWindow(FALSE);
		m_radioVMR9.EnableWindow(FALSE);
		m_radioEVR.EnableWindow(FALSE);

		m_iTotalPlayTime = m_player->GetTotalPlayTime();
		SetTimer(1020, PLAY_CHECK_TIME, NULL);
	}
}

void CTestWMVDlg::OnBnClickedButtonClose()
{
	// TODO: Add your control notification handler code here

	if(m_player)
	{
		KillTimer(1020);
		m_strMediaFullPath = _T("");

		m_player->Close();
		delete m_player;
		m_player = NULL;

		m_radioVMR7.EnableWindow(TRUE);
		m_radioVMR9.EnableWindow(TRUE);
		m_radioEVR.EnableWindow(TRUE);

		Invalidate(TRUE);
		m_wnd.Invalidate(TRUE);
	}
}

void CTestWMVDlg::OnBnClickedButtonPlay()
{
	// TODO: Add your control notification handler code here

	if(m_player != NULL)
#ifndef USE_BASICINTERFACE
		m_player->RunMovies();
#else
		m_player->Play();
#endif
}

void CTestWMVDlg::OnBnClickedButtonPause()
{
	// TODO: Add your control notification handler code here

	if(m_player != NULL)
#ifndef USE_BASICINTERFACE
		m_player->PauseMovies();
#else
		m_player->Pause();
#endif
}

void CTestWMVDlg::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

	if(m_player != NULL)
	{
#ifndef USE_BASICINTERFACE
		m_player->StopMovies();
#else
		m_player->Stop();
		m_player->Close();
#endif
		delete m_player;
	}

	CDialog::OnCancel();
}

LRESULT CTestWMVDlg::OnAddResult(WPARAM wParam, LPARAM lParam)
{
	LPCTSTR result = (LPCTSTR)wParam;

	m_editResult.SetSel(-1, -1);
	m_editResult.ReplaceSel(result, FALSE);
	m_editResult.ReplaceSel(_T("\r\n"), FALSE);

	return 0;
}

void CTestWMVDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here

	m_repos.MoveControl();
	Invalidate();

	if(m_player != NULL)
	{
		CRect rect;
		GetClientRect(rect);
		rect.right -= 180;
#ifndef USE_BASICINTERFACE
		m_player->SetMoviePosition(rect);
#else
		m_player->SetVideoRect(rect);
#endif
	}
}

void CTestWMVDlg::OnBnClickedRadioAspectRatio()
{
	// TODO: Add your control notification handler code here

	UpdateData();

	if(m_player != NULL)
#ifndef USE_BASICINTERFACE
		m_player->SetAspectRatio(m_nAspectRatio);
#else
		m_player->SetAspectRatioMode(m_nAspectRatio ? ASPECT_RATIO_MODE::LETTER_BOX : ASPECT_RATIO_MODE::STRETCHED);
#endif
}

void CTestWMVDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == 1020 && m_strMediaFullPath.GetLength() > 0)
	{
		if(m_iTotalPlayTime > 0.0f && m_player != NULL)
		{
			double cur_tm = m_player->GetCurrentPlayTime();
			if(cur_tm >= m_iTotalPlayTime)
			{
		//		KillTimer(1020);

		//		if(m_player != NULL)
		//		{
		//#ifdef USE_BASICINTERFACE
		//			m_player->Close();
		//#endif
		//			delete m_player;
		//		}

		//		//
		//		CRect rect;
		//		GetClientRect(rect);
		//		rect.right -= 180;

		//#ifndef USE_BASICINTERFACE
		//		//
		//		m_player = new CWMVPlayer();   
		//		m_player->CreateGraph(GetSafeHwnd());
		//		m_player->RenderMovieFile((LPCTSTR)fullpath);
		//		m_player->SetMoviePosition(rect);
		//		m_player->SetNotifyWindow(GetSafeHwnd());   
		//		m_player->SetAspectRatio(m_nAspectRatio);
		//#else
		//		switch(m_nRender)
		//		{
		//		case 0:
		//			m_player = new CVMRRender(&m_wnd);
		//			m_player->SetRenderType(E_VMR7_WINDOWLESS);
		//			break;
		//		case 1:
		//			m_player = new CVMRRender(&m_wnd);
		//			m_player->SetRenderType(E_VMR9_WINDOWLESS);
		//			break;
		//		case 2:
		////			m_player = new CEVRRender(&m_wnd);
		//			break;
		//		default:
		//			m_player = new COverlayInterface(&m_wnd);
		//			break;
		//		}
		//		m_player->SetParentWindow(m_wnd.GetSafeHwnd());
		//		m_player->Open((LPCTSTR)m_strMediaFullPath);
		//		m_player->SetAspectRatioMode(m_nAspectRatio ? ASPECT_RATIO_MODE::LETTER_BOX : ASPECT_RATIO_MODE::STRETCHED);
		//		m_player->SetVideoRect(rect);
		//#endif

		//		//SendMessage(WM_ADD_RESULT, (WPARAM)"");

		//		m_radioVMR7.EnableWindow(FALSE);
		//		m_radioVMR9.EnableWindow(FALSE);
		//		m_radioEVR.EnableWindow(FALSE);

		//		m_iTotalPlayTime = m_player->GetTotalPlayTime();
		//		SetTimer(1020, PLAY_CHECK_TIME, NULL);

				m_player->SetPosition(0.f);
				m_player->Play();
			}
		}
	}
}
