
#define	INT_ONLY_VERSION


#define FCCRIFF			0x46464952
#define FCCAVI			0x20495641
#define FCCLIST			0x5453494C
#define FCCidx1			0x31786469
#define FCChdrl			0x6C726468
#define FCCmovi			0x69766F6D
#define FCCstrh			0x68727473
#define FCCstrf			0x66727473
#define FCCindx			0x78646E69
#define FCCvids			0x73646976
#define FCCauds			0x73647561
#define FCCstrl			0x6C727473
#define FCCDIVX			0x58564944
#define FCCdivx			0x78766964
#define FCCDX50			0x30355844
#define FCCdx50			0x30357864
#define FCCDIV3			0x33564944
#define FCCdiv3			0x33766964
#define FCCDIV4			0x34564944
#define FCCdiv4			0x34766964
#define FCCXVID			0x44495658
#define FCCxvid			0x64697678
#define FCCMP43			0x3334504D
#define FCCmp43			0x3334706D
#define FCCWMV3			0x33564D57
#define FCCwmv3			0x33766D77

#define AVI_INDEX_OF_INDEXES	0x00
#define AVI_INDEX_OF_CHUNKS		0x01
#define AVI_INDEX_2FIELD		0x03
#define AVIIF_KEYFRAME			0x00000010
#define AVIIF_NO_TIME			0x00000100

#define AUD_FORMAT_PCM			0x0001
#define AUD_FORMAT_MSADPCM		0x0002
#define AUD_FORMAT_MP3			0x0055
#define AUD_FORMAT_WMA1			0x0160
#define AUD_FORMAT_WMA2			0x0161
#define AUD_FORMAT_AC3			0x2000
#define AUD_FORMAT_DTS			0x2001

#define AVIINFO_OK				0
#define AVIINFO_IOERR				-1
#define AVIINFO_INVALIDFILE			-2
#define AVIINFO_THREADERR			-3
#define AVIINFO_TIMEOUT				-4
#define AVIINFO_PARAMERR			-5
#define AVIINFO_NEED_MORE_DATA		-6

#define AVIINFO_MORESTREAM		1
#define MAX_STREAM			0x100
#define AVIINFO_ISERROR(x)			((x) < 0)

typedef struct {
	DWORD	fccType;
	DWORD	fccHandler;
	DWORD	dwFlags;
	WORD	wPriority;
	WORD	wLanguage;
	DWORD	dwInitialFrames;
	DWORD	dwScale;
	DWORD	dwRate;
	DWORD	dwStart;
	DWORD	dwLength;
	DWORD	dwSuggestedBufferSize;
	DWORD	dwQuality;
	DWORD	dwSampleSize;
	short	rcFrameLeft;
	short	rcFrameTop;
	short	rcFrameRight;
	short	rcFrameBottom;
} STREAMHEADER;

typedef struct {
	DWORD	biSize;
	LONG	biWidth;
	LONG	biHeight;
	WORD	biPlanes;
	WORD	biBitCount;
	DWORD	biCompression;
	DWORD	biSizeImage;
	LONG	biXPelsPerMeter;
	LONG	biYPelsPerMeter;
	DWORD	biClrUsed;
	DWORD	biClrImportant;
} VIDSTREAMFORMAT;

typedef struct {
	WORD	wFormatTag;
	WORD	nChannels;
	DWORD	nSamplesPerSec;
	DWORD	nAvgBytesPerSec;
	WORD	nBlockAlign;
} AUDBASICSTREAMFORMAT;

typedef struct {
	unsigned short	wLongsPerEntry;
	unsigned char	bIndexSubType;
	unsigned char	bIndexType;
	DWORD			nEntriesInUse;
	DWORD			dwChunkId;
	INT64			qwBaseOffset;
	DWORD			dwReserved3;
} STREAMINDXHDR;

typedef struct {
	INT64	qwOffset;
	DWORD	dwSize;
	DWORD	dwDuration;
} SUPERINDEXENTRY;

struct AudioCodecInfo
{
	unsigned long	dwCodec;
	unsigned long	dwCodecEx;					// AudioDecoder_CodecEx_type
	unsigned long	dwChannelCount;
	unsigned long	dwFreq;
	unsigned long	dwBitPerSample;
};


struct AviVideoCodecInfo
{
	unsigned long	dwCodec;
	unsigned long	dwCodecEx;					// VideoDecoder_CodecEx_type
	unsigned long   nWidth;
	unsigned long   nHeight;
};

struct AviCodecInfo
{
	struct AudioCodecInfo audioCodec;
	struct AviVideoCodecInfo videoCodec;
}; 

typedef enum { aistVideo, aistAudio } AIStreamType;
typedef enum { aivcUnknown, aivcDivX3LowM, aivcDivX3FastM, aivcDivX4, aivcDivX5, aivcXviD, aivcMSMPEG4V3, aivcWMV9 } AIVideoCodec;
typedef enum { aiacUnknown, aiacPCM, aiacMSADPCM, aiacMP3, aiacWMA1, aiacWMA2, aiacAC3, aiacDTS } AIAudioCodec;

typedef struct {
	AIStreamType	type;
#if defined (INT_ONLY_VERSION)
	__int64			msDur;
#else
	double			secDur;
#endif
	union {
		struct {
			AIVideoCodec	codec;
			unsigned long	codecEx;
			long			nWid, nHgt;
#if defined (INT_ONLY_VERSION)
			__int64			fps_mulby1000;
#else
			double			fps;
#endif
		} videoInfo;
		struct {
			AIAudioCodec	codec;
			unsigned short	codecEx;
			unsigned long	nBitRate, nSampleRate;
			unsigned short	nChannels;
		} audioInfo;
	} avSpecific;
} AVIStreamInfo;



int AVIInfo_GetAVIInfo(char* pData, int dataLen, AVIStreamInfo *pInfo, int *pnStrm);
int divx_GetCodecInfoFromData(char* pData, int dataLen, struct AviCodecInfo *codec);
int GetDivxInfo(LPCTSTR pFilename, struct AviCodecInfo *codec);
