/************************************************************************************/
/*! @file EVRRender.cpp
	@brief VMR을 이용한 동영상 재생 클래스 구현파일
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2010/01/07\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# 2010/01/07:정운형:최초작성.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#include "stdafx.h"

//#include <reftime.h>
#include <math.h>
#include <io.h>

#include "dshowutil.h"
#include "EVRRender.h"

#ifndef __DEBUG__
	#define __DEBUG__(x, y)
#endif

#ifndef _NULL
	#define _NULL
#endif


#define		SEND_LOG(x)		::AfxGetMainWnd()->SendMessage(WM_ADD_RESULT, (WPARAM)x);


void ShowMsg(TCHAR* szFormat, ...)
{
	TCHAR szBuffer[1024];  // Large buffer for long filenames or URLs
	const size_t NUMCHARS = sizeof(szBuffer) / sizeof(szBuffer[0]);
	const int LASTCHAR = NUMCHARS - 1;

	// Format the input string
	va_list pArgs;
	va_start(pArgs, szFormat);

	// Use a bounded buffer size to prevent buffer overruns.  Limit count to
	// character size minus one to allow for a NULL terminating character.
	(void)StringCchVPrintf(szBuffer, NUMCHARS - 1, szFormat, pArgs);
	va_end(pArgs);

	// Ensure that the formatted string is NULL-terminated
	szBuffer[LASTCHAR] = TEXT('\0');

	// Display a message box with the formatted string
	MessageBox(NULL, szBuffer, TEXT("WMVPlayer"), MB_OK);
}

//static HRESULT SetRenderingMode(IBaseFilter* pBaseFilter, VMRMode mode)
//{
//    // Test VMRConfig, VMRMonitorConfig
//    IVMRFilterConfig* pConfig;
//
//    HRESULT hr = pBaseFilter->QueryInterface(IID_IVMRFilterConfig, (LPVOID *)&pConfig);
//    if(SUCCEEDED(hr))
//    {
//        hr = pConfig->SetRenderingMode(mode);
//        hr = pConfig->SetRenderingPrefs(RenderPrefs_ForceOverlays|RenderPrefs_AllowOverlays);
//        pConfig->Release();
//    }//if
//    return hr;
//}//if


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/// @param (HWND) hwnd : (in) 동영상을 재생할 창의 핸들
/// @param (int) nMode : (in) 동영상을 랜더링 할 모드
/////////////////////////////////////////////////////////////////////////////////
CEVRRender::CEVRRender(CWnd* pWnd)
: CBasicInterface(pWnd)
{
	m_pGraphBuilder			= NULL;
	m_pMediaControl			= NULL;
	m_pMediaSeeking			= NULL;
	m_pBasicAudio				= NULL;
	m_pFilter				= NULL;
	m_pWindowlessControl	= NULL;
//	m_pAspectRatio			= NULL;
	m_pVideoWindow			= NULL;
	m_pMediaEventEx			= NULL;
	//m_WindowlessControl9(NULL)
	m_dwRotId				= -1;
	m_pParentWnd			= pWnd;
	m_strFilePath			= "";
	m_nMode					= E_EVR;
	m_bOpen					= false;
	m_hParentWnd			= pWnd->m_hWnd;
	::GetClientRect(m_hParentWnd, &m_rcWnd);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CEVRRender::~CEVRRender()
{
	if(m_bOpen)
	{
		Stop();
		Close();
	}//if

	m_hParentWnd = NULL;
}

HRESULT CreateFilter(REFCLSID clsid, IBaseFilter **ppFilter)
{
	HRESULT hr;

	hr = CoCreateInstance(clsid, NULL, CLSCTX_INPROC_SERVER,
		IID_IBaseFilter,
		(void **) ppFilter);

	if(FAILED(hr))
	{
		//MessageBox(TEXT("CreateFilter: Failed to create filter!  hr=0x%x\n"));
		if (ppFilter)
			*ppFilter = NULL;
		return hr;
	}

	return S_OK;
}

IBaseFilter* CreateEncodec(CString inFriendlyName)
{
	HRESULT hr = NOERROR;
	ICreateDevEnum * enumHardware =NULL;
	IBaseFilter    * hardwareFilter = NULL;

	hr = CoCreateInstance(CLSID_SystemDeviceEnum,NULL,CLSCTX_INPROC_SERVER,
		IID_ICreateDevEnum,(void **)&enumHardware);

	if(FAILED(hr))
	{
		return NULL;
	}

	IEnumMoniker * enumMoniker = NULL;

	hr = enumHardware->CreateClassEnumerator(CLSID_LegacyAmFilterCategory, &enumMoniker,0);

	if(FAILED(hr))
	{
		return NULL;
	}

	if(enumMoniker)
	{
		enumMoniker->Reset();

		IMoniker * moniker = NULL;
		char friendlyName[256];
		ZeroMemory(friendlyName,256);

		while(S_OK == enumMoniker->Next(1,&moniker,0))
		{
			if(moniker)
			{
				IPropertyBag * proBag = NULL;
				VARIANT    name;

				hr = moniker->BindToStorage(NULL,NULL,IID_IPropertyBag,(void **)&proBag);

				if(SUCCEEDED(hr))
				{
					name.vt = VT_BSTR;
					proBag->Read(L"FriendlyName",&name,NULL);
				}

				if(SUCCEEDED(hr))
				{
					WideCharToMultiByte(CP_ACP, 0, name.bstrVal, -1,friendlyName, 256,
						NULL, NULL);
					CString str = (CString)friendlyName;
					if(inFriendlyName == str)
					{
						moniker->BindToObject(NULL,NULL,IID_IBaseFilter,
							(void **)&hardwareFilter);
					}
				}
				if(proBag)
				{
					proBag->Release();
					proBag = NULL;
				}
				moniker->Release();
			}
		}
		enumMoniker->Release();
	}
	enumHardware->Release();

	return hardwareFilter;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 오픈한다. \n
/// @param (LPCSTR) lpszFilename : (in) 재생하려는 파일의 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::Open(LPCTSTR lpszFilename)
{
	Close();
	m_strFilePath = lpszFilename;
	if(m_strFilePath.GetLength() == 0)
	{
		__DEBUG__("Invlid file path", m_strFilePath);
		return false;
	}//if

	//USES_CONVERSION;
	HRESULT hres;
	WCHAR szFileName[MAX_PATH] = { 0x00 };

#ifdef UNICODE
	wcsncpy(szFileName, (LPCWSTR)m_strFilePath, NUMELMS(szFileName));
#else
	MultiByteToWideChar(CP_ACP, 0, (LPCTSTR)m_strFilePath, -1, szFileName, MAX_PATH);
#endif

	HRESULT result = CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);

	hres = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder, (LPVOID *)&m_pGraphBuilder);
	if(FAILED(hres))
	{
		SEND_LOG(_T("-IGraphBuilder"));
		return false;
	}//if

	SEND_LOG(_T("+IGraphBuilder"));

	//Setting render
	if(!SetRenderFilter())
	{
		Close();
		return false;
	}//if

	//Get media control interface
	hres = m_pGraphBuilder->QueryInterface(IID_IMediaControl, (LPVOID *)&m_pMediaControl);
	if(FAILED(hres))
	{
		Close();
		SEND_LOG(_T("-IMediaControl"));
		return false;
	}//if

	SEND_LOG(_T("+IMediaControl"));

	//Get audio control interface
	hres = m_pGraphBuilder->QueryInterface(IID_IBasicAudio, (LPVOID*)&m_pBasicAudio);
	if(FAILED(hres))
	{
		Close();
		SEND_LOG(_T("-IBasicAudio"));
		return false;
	}//if

	SEND_LOG(_T("+IBasicAudio"));

	//Get media seeking control interface
	hres = m_pGraphBuilder->QueryInterface(IID_IMediaSeeking, (LPVOID *)&m_pMediaSeeking);
	if(FAILED(hres))
	{
		Close();
		SEND_LOG(_T("-IMediaSeeking"));
		return false;
	}//if

	SEND_LOG(_T("+IMediaSeeking"));

	if (!IsWindowsMediaFile(lpszFilename))
	{
		//Render file
		hres = m_pGraphBuilder->RenderFile(szFileName, NULL);
		if(FAILED(hres))
		{
			Close();
			SEND_LOG(_T("-RenderFile"));
			return false;
		}//if
	}
	else
	{
		CComPtr <IBaseFilter> pSource;
		CComPtr <IFileSourceFilter> pFileSource;
		CComPtr <IPin> pOutputPin;
		CComPtr <IPin> pInputPin;
		CComPtr <IBaseFilter> pAudioRenderer;

		// Load the improved ASF reader filter by CLSID
		HRESULT hr = CreateFilter(CLSID_WMAsfReader, &pSource);
		if(FAILED(hr))
		{
			CString msg;
			msg.Format(TEXT("Failed to create WMAsfWriter filter!  hr=0x%x\0"), hr);
			SEND_LOG((LPCTSTR)msg);
			return hr;
		}

		// Add the ASF reader filter to the graph.  For ASF/WMV/WMA content,
		// this filter is NOT the default and must be added explicitly.
		hr |= m_pGraphBuilder->AddFilter(pSource, L"WM ASF Reader");
		if(FAILED(hr))
		{
			CString msg;
			msg.Format(TEXT("Failed to add ASF reader filter to graph!  hr=0x%x\0"), hr);
			SEND_LOG((LPCTSTR)msg);
			return hr;
		}

		SEND_LOG(_T("+WM ASF Reader"));

		// Set its source filename
		hr |= pSource->QueryInterface(IID_IFileSourceFilter, (void **) &pFileSource);

		// Attempt to load this file
		hr |= pFileSource->Load(lpszFilename, NULL);

		if (FAILED(hr))
		{
			CString msg;
			msg.Format(TEXT("Failed to load file in source filter (pFileSource->Load())!  hr=0x%x\0"), hr);
			SEND_LOG((LPCTSTR)msg);
			return hr;
		}

		SEND_LOG(_T("+IFileSourceFilter"));

		ULONG count = 0;
		CountTotalFilterPins(pSource, &count);

		for (ULONG i = 0; i < count; i ++)
		{
			pOutputPin = GetOutPin(pSource, i);
			PIN_INFO info;
			pOutputPin->QueryPinInfo(&info);

			CComPtr<IBaseFilter> Codec;

			if (wcsstr(info.achName, L"Video"))
			{
				Codec = CreateEncodec(L"WMVideo Decoder DMO");
				if(Codec == NULL)
				{
					SEND_LOG(_T("-WMVideo Decoder DMO"));
				}
				else if (NULL != m_pGraphBuilder)
				{
					m_pGraphBuilder->AddFilter(Codec, L"WMVideo Decoder DMO");
					pInputPin = GetInPin(Codec, 0);

					hr |= m_pGraphBuilder->Connect(pOutputPin, pInputPin);
					if (FAILED(hr))
					{
						CString msg;
						msg.Format(_T("Video Connected Failed (Codec-Source).  hr=0x%x\0"), hr);
						SEND_LOG((LPCTSTR)msg);
						return hr;
					}

					pOutputPin = GetOutPin(Codec, 0);
					pInputPin = GetInPin(m_pFilter, 0);

					hr |= m_pGraphBuilder->Connect(pOutputPin, pInputPin);
					if (FAILED(hr))
					{
						CString msg;
						msg.Format(_T("Video Connected Failed (Codec-Render).  hr=0x%x\0"), hr);
						SEND_LOG((LPCTSTR)msg);
						return hr;
					}

					SEND_LOG(_T("+WMVideo Decoder DMO"));
				}
			}
			else if (wcsstr(info.achName, L"Audio"))
			{
				Codec = CreateEncodec(_T("WMAudio Decoder DMO"));
				if(Codec == NULL)
				{
					SEND_LOG(_T("-WMAudio Decoder DMO"));
				}
				else if (NULL != m_pGraphBuilder)
				{
					m_pGraphBuilder->AddFilter(Codec, L"WMAudio Decoder DMO");
					pInputPin = GetInPin(Codec, 0);

					hr |= m_pGraphBuilder->Connect(pOutputPin, pInputPin);
					if (FAILED(hr))
					{
						CString msg;
						msg.Format(_T("Audio Connected Failed (Codec-Source).  hr=0x%x\0"), hr);
						SEND_LOG((LPCTSTR)msg);
						return hr;
					}

					pOutputPin = GetOutPin(Codec, 0);

					if (NULL == pAudioRenderer)
					{
						if (SUCCEEDED(CoCreateInstance(CLSID_DSoundRender, NULL, CLSCTX_INPROC_SERVER,
							IID_IBaseFilter, (void **)&pAudioRenderer)))
						{
							// The audio renderer was successfully created, so add it to the graph
							hr |= m_pGraphBuilder->AddFilter(pAudioRenderer, L"Audio Renderer");
							if (FAILED(hr))
							{
								SEND_LOG(_T("-Audio Renderer"));
								return hr;
							}

							SEND_LOG(_T("+CLSID_DSoundRender"));
						}
					}

					pInputPin = GetInPin(pAudioRenderer, 0);

					hr |= m_pGraphBuilder->Connect(pOutputPin, pInputPin);
					if (FAILED(hr))
					{
						CString msg;
						msg.Format(_T("Audio Connected Failed (Codec-Render).  hr=0x%x\0"), hr);
						SEND_LOG((LPCTSTR)msg);
						return hr;
					}

					SEND_LOG(_T("+WMAudio Decoder DMO"));
				}
			}
		}
	}

	m_pMediaControl->StopWhenReady();
	m_bOpen = true;
	AddToRot(m_pGraphBuilder);

//	SetAspectRatioMode(CBasicInterface::ASPECT_RATIO_MODE::STRETCHED);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 비디오 랜더를 설정한다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::SetRenderFilter()
{
	if(!m_pGraphBuilder)
	{
		ShowMsg(TEXT("m_pGraphBuilder is NULL"), NULL);
		return false;
	}//if

	bool bRet = false;
	switch(m_nMode)
	{
	case E_EVR:
		{
			//ShowMsg(TEXT("Default windowless mode"), NULL);
			SEND_LOG(_T("Enhanced Video Renderer mode"));
			bRet = SetEVR();
		}
		break;

	default :
		{
			ShowMsg(TEXT("NO mode"), NULL);
		}
	}//switch

	return bRet;
}

bool CEVRRender::SetEVR()
{
	HRESULT hres = AddFilterByCLSID(m_pGraphBuilder, CLSID_EnhancedVideoRenderer, &m_pFilter, L"EVR");

	if(SUCCEEDED(hres))
	{
		SEND_LOG(_T("+CLSID_EnhancedVideoRenderer"));
	}
	else
	{
		SEND_LOG(_T("-CLSID_EnhancedVideoRenderer"));
		return false;
	}

    IMFGetService *pService = NULL;
	IMFVideoDisplayControl *pDisplay = NULL;

    HRESULT hr = m_pFilter->QueryInterface(__uuidof(IMFGetService), (void**)&pService); 
    if (SUCCEEDED(hr)) 
    { 
        hr = pService->GetService(
            MR_VIDEO_RENDER_SERVICE, 
            __uuidof(IMFVideoDisplayControl),
            (void**)&pDisplay
            );

		SEND_LOG(_T("+IMFVideoDisplayControl"));
    }
	else
	{
		SEND_LOG(_T("-IMFVideoDisplayControl"));
		return false;
	}

	// Set the clipping window.
	if (SUCCEEDED(hr))
	{
		hr = pDisplay->SetVideoWindow(m_hParentWnd);
	}

	if (SUCCEEDED(hr))
	{
		m_pWindowlessControl = pDisplay;
		m_pWindowlessControl->AddRef();
	}

	SAFE_RELEASE(pService);
	SAFE_RELEASE(pDisplay);

	return SUCCEEDED(hr); 
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 닫는다. \n
/////////////////////////////////////////////////////////////////////////////////
void CEVRRender::Close()
{
	ReleaseAllInterfaces();
	RemoveFromRot();

	m_bOpen  = false;
	m_strFilePath = "";
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 그래프빌더의 모든 필터와 인터페이스들을 제거한다. \n
/// @return <형: BOOL> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
void CEVRRender::ReleaseAllInterfaces()
{
	if(!m_bOpen)
	{
		//ShowMsg(TEXT("Video file not opened"), NULL);
		return;
	}//if

	HRESULT hr = m_pGraphBuilder->Abort();
	if(!SUCCEEDED(hr))
	{
		//ShowMsg(TEXT("Graph builder abort fail"), NULL);
	}//if

	//그래프를 정지한다.
	Stop();

	//그래프의 필터를 열거한다.
	IEnumFilters *pEnum = NULL;
	hr = m_pGraphBuilder->EnumFilters(&pEnum);
	if(!SUCCEEDED(hr))
	{
		//ShowMsg(TEXT("Enum filters fail"), NULL);
		return;
	}//if

	IBaseFilter *pFilter = NULL;
	while(S_OK == pEnum->Next(1, &pFilter, NULL))
	{
		//필터를 삭제한다.
		hr = m_pGraphBuilder->RemoveFilter(pFilter);
		if(!SUCCEEDED(hr))
		{
			ShowMsg(TEXT("Remove filter fail"), NULL);
		}//if

		//열거자를 리셋한다.
		pEnum->Reset();
		pFilter->Release();
	}//while
	pEnum->Release();

	if(m_pVideoWindow)
	{
		m_pVideoWindow->put_Visible(OAFALSE);		//윈도우를 비표시함
		m_pVideoWindow->put_Owner(NULL);			//기존의 부모 윈도우를 삭제
	}//if
	SAFE_RELEASE(m_pVideoWindow);

	//이벤트 통지를 처리하는 윈도우등록 해제
	if(m_pMediaEventEx)
	{
		m_pMediaEventEx->SetNotifyWindow((OAHWND)NULL, 0, 0);
	}//if
	SAFE_RELEASE(m_pMediaEventEx)

	SAFE_RELEASE(m_pWindowlessControl);
//	SAFE_RELEASE(m_pAspectRatio);
	SAFE_RELEASE(m_pVideoWindow);
	SAFE_RELEASE(m_pMediaEventEx);
	SAFE_RELEASE(m_pMediaControl);
	SAFE_RELEASE(m_pMediaSeeking);
	SAFE_RELEASE(m_pBasicAudio);
	//SAFE_RELEASE(m_pFilter);
	SAFE_RELEASE(m_pGraphBuilder);

	return;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일을 재생한다. \n
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<S_FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::Play()
{
	HRESULT hr = S_FALSE;
	if(!m_bOpen)
	{
		//ShowMsg(TEXT("Video file not opened"), NULL);
		return hr;
	}//if

	hr = m_pMediaControl->Run();
	if(SUCCEEDED(hr))
	{
		return hr;
	}//if

	ShowMsg(TEXT("Fail to IMediaControl Run"), NULL);
	return hr;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 재생을 일시 중지 한다. \n
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<S_FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::Pause()
{
	HRESULT hr = S_FALSE;
	if(!m_bOpen)
	{
		//ShowMsg(TEXT("Video file not opened"), NULL);
		return hr;
	}//if

	hr = m_pMediaControl->Pause();
	if(SUCCEEDED(hr))
	{
		return hr;
	}//if

	ShowMsg(TEXT("Fail to IMediaControl Pause"), NULL);
	return hr;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 재생을 중지 한다. \n
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<S_FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::Stop()
{
	HRESULT hr = S_FALSE;
	if(!m_bOpen)
	{
		//ShowMsg(TEXT("Video file not opened"), NULL);
		return hr;
	}//if

	hr = m_pMediaControl->Stop();
	if(SUCCEEDED(hr))
	{
		SeekToStart();
		return hr;
	}//if

	ShowMsg(TEXT("Fail to IMediaControl Stop"), NULL);
	return hr;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 볼륨을 설정한다. \n
/// @param (int) nVolume : (in) 0~100 까지의 볼륨 값
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<S_FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::SetVolume(int nVolume)
{
	HRESULT hr = S_FALSE;
	if(!m_bOpen)
	{
		//ShowMsg(TEXT("Video file not opened"), NULL);
		return hr;
	}//if

	// 볼륨조절
	double lf_cur_sound = ((1 - pow(10.0f, -10)) * ((double)nVolume / 100)) + pow(10.0f, -10);// 핵심코드
	lf_cur_sound = 10 * log10(lf_cur_sound) * 100;
	long l_cur_sound = (long)lf_cur_sound;

	hr = m_pBasicAudio->put_Volume(l_cur_sound);
	if(SUCCEEDED(hr))
	{
		return hr;
	}//if
	
	ShowMsg(TEXT("Faile to set volume"), nVolume);
	return hr;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 총 재생시간을 반환한다. \n
/// @return <형: double> \n
///			<double: 동영상 파일의 총 재생시간> \n
/////////////////////////////////////////////////////////////////////////////////
double CEVRRender::GetTotalPlayTime()
{
	if(!m_bOpen)
	{
		//ShowMsg(TEXT("Video file not opened"), NULL);
		return false;
	}//if

	REFTIME tm = 0.0f;
    LONGLONG lDuration;
	HRESULT hr = S_OK;
    hr = m_pMediaSeeking->GetDuration(&lDuration);
    if(SUCCEEDED(hr))
    {
        return double(lDuration) / UNITS;
    }//if
  
	ShowMsg(TEXT("Fail to get duration"), NULL);
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상 파일의 현재 재생시간을 반환한다. \n
/// @return <형: double> \n
///			<double: 동영상 파일의 현재 재생시간> \n
/////////////////////////////////////////////////////////////////////////////////
double CEVRRender::GetCurrentPlayTime()
{
	if(!m_bOpen)
	{
		//ShowMsg(TEXT("Video file not opened"), NULL);
		return false;
	}//if

	REFTIME rt = (REFTIME) 0;
	HRESULT hr = S_OK;
	LONGLONG lPosition;
	hr = m_pMediaSeeking->GetPositions(&lPosition, NULL);
	if(SUCCEEDED(hr))
	{
		return double(lPosition) / UNITS;
	}//if

	ShowMsg(TEXT("Fail to get position"), NULL);
	return rt;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 지정된 위치로 이동한다. \n
/// @param (double) pos : (in) 이동하려는 동영상의 시간
/////////////////////////////////////////////////////////////////////////////////
void CEVRRender::SetPosition(double pos)
{
	if(!m_bOpen)
	{
		//ShowMsg(TEXT("Video file not opened"), NULL);
	}//if

	HRESULT hr = S_OK;
    LONGLONG llTime = LONGLONG(pos * double(UNITS));

    //FILTER_STATE fs;
    //hr = pMediaControl->GetState(100, (OAFilterState *)&fs);
    hr = m_pMediaSeeking->SetPositions(&llTime, AM_SEEKING_AbsolutePositioning, NULL, 0);
	//m_pMediaControl->Pause();

    // This gets new data through to the renderers
    //if(fs == State_Stopped && bFlushData)
    //{
    //    hr = pMediaControl->Pause();
    //    hr = pMediaControl->GetState(INFINITE, (OAFilterState *)&fs);
    //    hr = pMediaControl->Stop();
    //}//if

    if(!SUCCEEDED(hr))
    {
        ShowMsg(TEXT("Fail to set position"), NULL);
    }//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 처음 위치로 이동한다. \n
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
void CEVRRender::SeekToStart()
{
	SetPosition(0);
	return;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상을 다시 그려준다. \n
/// @param (HDC) hdc : (in/out) 설명
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::RePaintVideo(/*HDC hdc*/)
{
	//CWnd* parent_wnd = CWnd::FromHandle(m_hWnd);
	CDC* dc = m_pParentWnd->GetDC();
	HDC hdc = dc->GetSafeHdc();

	bool bRet = FALSE;
	switch(m_nMode)
	{
	case E_EVR:
		{
			if(m_pWindowlessControl)
			{
				HRESULT hr = m_pWindowlessControl->RepaintVideo();
				if(SUCCEEDED(hr))
				{
					bRet = TRUE;
				}
				else
				{
					ShowMsg(TEXT("Fail to RepaintVideo"), NULL);
					bRet = FALSE;
				}//if
			}
			else
			{
				ShowMsg(TEXT("IWindowlessControl is NULL"), NULL);
				bRet = FALSE;
			}//if
		}
		break;

	default:
		{
			bRet = TRUE;
			ShowMsg(TEXT("Not support mode"), NULL);
		}
	}//switch

	return bRet;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// let the graph instance be accessible from graphedit \n
/// @param (IUnknown) *pUnkGraph : (in) Grapheditor interface pointer
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<HRESULT: 실패 코드> \n
/////////////////////////////////////////////////////////////////////////////////
HRESULT CEVRRender::AddToRot(IUnknown *pUnkGraph) 
{
	if(m_dwRotId != -1)
	{
		//ShowMsg(TEXT("Registerd filtergraph in ROT"), (int)m_dwRotId);
	}//if

	if(pUnkGraph == NULL)
	{
		ShowMsg(TEXT("Graphbuilder is NULL"), NULL);
		return E_INVALIDARG;
	}//if

    IMoniker * pMoniker;
    IRunningObjectTable *pROT;
    if(FAILED(GetRunningObjectTable(0, &pROT)))
	{
		//ShowMsg(TEXT("Fail to get ROT"), NULL);
        return E_FAIL;
    }//if

    WCHAR wsz[256];
    wsprintfW(wsz, L"FilterGraph %08x pid %08x", (DWORD_PTR)pUnkGraph, GetCurrentProcessId());
    HRESULT hr = CreateItemMoniker(L"!", wsz, &pMoniker);
    if(SUCCEEDED(hr))
	{
        hr = pROT->Register(0, pUnkGraph, pMoniker, &m_dwRotId);
		if(!SUCCEEDED(hr))
		{
			m_dwRotId = -1;
			ShowMsg(TEXT("Fail to register filtergraph"), NULL);
		}
		else
		{
			//ShowMsg(TEXT("Success in register filtergraph"), (int)m_dwRotId);
		}//if
    }
	else
	{
		m_dwRotId = -1;
		ShowMsg(TEXT("Fail to create moniker"), NULL);
	}//if

	pMoniker->Release();
    pROT->Release();
    return hr;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// remove the graph instance accessibility from graphedit \n
/////////////////////////////////////////////////////////////////////////////////
void CEVRRender::RemoveFromRot()
{
	if(m_dwRotId != -1)
	{
		IRunningObjectTable *pROT;
		if(SUCCEEDED(GetRunningObjectTable(0, &pROT)))
		{
			HRESULT hr = pROT->Revoke(m_dwRotId);
			if(SUCCEEDED(hr))
			{
				//ShowMsg(TEXT("Success in revok"), (int)m_dwRotId);
				m_dwRotId = -1;
				pROT->Release();
			}
			else
			{
				ShowMsg(TEXT("Fail to revok"), (int)m_dwRotId);
			}//if
		}
		else
		{
			ShowMsg(TEXT("Fail to get ROT"), (int)m_dwRotId);
		}//if
	}
	else
	{
		//ShowMsg(TEXT("Nothing to remove from ROT"), (int)m_dwRotId);
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 영역을 설정한다. \n
/// @param (LPRECT) pRect : (in) 설정하려는 동영상의 영역
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<S_FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::SetVideoRect(LPRECT pRect)
{
	HRESULT hr = S_FALSE;
	switch(m_nMode)
	{
	case E_EVR:
			if(m_pWindowlessControl)
			{
				hr = m_pWindowlessControl->SetVideoPosition(NULL, pRect);
			}
		break;

	default:
		{
			ShowMsg(TEXT("Unknown mode"), NULL);
		}
	}//switch

	return hr;
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 원본영역을 반한한다. \n
/// @param (LPRECT) pRect : (out) 동영상의 원본 영역
/// @return <형: HRESULT> \n
///			<S_OK: 성공> \n
///			<S_FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::GetVideoRect(LPRECT pRect)
{
	HRESULT hr = S_FALSE;
	switch(m_nMode)
	{
	case E_EVR:
		{
			if(m_pWindowlessControl)
			{
				pRect->left = 0;
				pRect->top = 0;
				SIZE size;
				HRESULT hr = m_pWindowlessControl->GetNativeVideoSize(&size, NULL);
				pRect->right = size.cx;
				pRect->bottom= size.cy;
				RECT rtSrc, rtDest;
				hr = m_pWindowlessControl->GetVideoPosition(NULL, &rtDest);
				if(!SUCCEEDED(hr))
				{
					ShowMsg(TEXT("Fail to GetVideoRect"), NULL);
				}//if
			}
			else
			{
				ShowMsg(TEXT("IWindowlessControl is NULL"), NULL);
			}//if
		}
		break;

	default:
		{
			ShowMsg(TEXT("Unknown mode"), NULL);
		}
	}//switch

	return hr;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 동영상의 재생시에 화면의 비율을 유지하는지를 설정한다. \n
/// @param (ASPECT_RATIO_MODE) mode : (in) STRETCHED, LETTER_BOX, ....
/// @return <형: BOOL> \n
///			<TRUE: 성공> \n
///			<FALSE: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CEVRRender::SetAspectRatioMode(ASPECT_RATIO_MODE mode)
{
	bool bRet = FALSE;
	HRESULT hres;

	switch(m_nMode)
	{
	case E_EVR:
		{
			if(m_pWindowlessControl)
			{
				hres = m_pWindowlessControl->SetAspectRatioMode(mode); // MFVideoARMode_None(stretch)=0, MFVideoARMode_PreservePicture(letterbox)=1
				if(SUCCEEDED(hres))
				{
					bRet = TRUE;
				}
				else
				{
					bRet = FALSE;

					CString msg;
					msg.Format(_T("-SetAspectRatioMode(%d).  hr=0x%x\0"), mode, hres);
					SEND_LOG((LPCTSTR)msg);
				}//if
			}
			else
			{
			//	ShowMsg(TEXT("IWindowlessControl is NULL"), NULL);
				bRet = FALSE;
			}//if
		}
		break;

	default:
		{
			ShowMsg(TEXT("Unknown mode"), NULL);
			bRet = TRUE;
		}
	}//switch

	return bRet;
}

#include "aviinfo.h"

bool CEVRRender::IsWindowsMediaFile(LPCTSTR lpszFilename)
{
	AviCodecInfo codec;

	GetDivxInfo(lpszFilename, &codec);

	return (codec.videoCodec.dwCodec == FCCWMV3);
}


