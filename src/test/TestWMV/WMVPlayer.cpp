// CWMVPlayer.cpp
//

#include "StdAfx.h"
#include "WMVPlayer.h"


#define		SEND_LOG(x)		::AfxGetMainWnd()->SendMessage(WM_ADD_RESULT, (WPARAM)x);


CWMVPlayer::CWMVPlayer()
{
	dwRegister = 0;

	pGraphBuilder = NULL;
	pMediaControl = NULL;
	pMediaSeeking = NULL;
	pVMR9Control  = NULL;
	pMediaEventEx = NULL;
	pVMR9         = NULL;
}

CWMVPlayer::~CWMVPlayer()
{
//#ifdef _DEBUG
//	if(dwRegister) RemoveFromRot(dwRegister);
//#endif

	SafeRelease(pMediaSeeking);
	SafeRelease(pMediaControl);
	SafeRelease(pVMR9Control);
	SafeRelease(pMediaEventEx);
	if(pVMR9)
		SafeRelease(pVMR9);
	else
		SafeRelease(pGraphBuilder);

	CoUninitialize();
}

HRESULT CWMVPlayer::CreateGraph(HWND hWnd)
{
	HRESULT hr = NOERROR;
	mPlayerHandle = hWnd;

	HRESULT result = CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);
	//if(S_OK != result)
	//{
	//	return E_FAIL;
	//}

	if (NULL == pGraphBuilder)
	{
		hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER,
			                  IID_IGraphBuilder, (void **)&pGraphBuilder);

		if (SUCCEEDED(hr))
		{
			SEND_LOG(_T("CLSID_FilterGraph"));

			hr |= pGraphBuilder->QueryInterface(IID_IMediaControl, (void **)&pMediaControl);
			hr |= pGraphBuilder->QueryInterface(IID_IMediaSeeking, (void **)&pMediaSeeking);
			hr |= pGraphBuilder->QueryInterface(IID_IMediaEventEx, (void **)&pMediaEventEx);

			if (FAILED(hr))
			{
				return hr;
			}

			SEND_LOG(_T("IID_IMediaControl"));
			SEND_LOG(_T("IID_IMediaSeeking"));
			SEND_LOG(_T("IID_IMediaEventEx"));
		}
	}

	UsedVideoMixingRenderer9();

	return hr;
}

HRESULT CWMVPlayer::RenderMovieFile(LPCTSTR lpszFilename)
{
	if (NULL != lpszFilename)
	{
		if (NULL == pGraphBuilder)
		{
			MessageBox(NULL, _T("The IGraphBuilder is NULL"), _T("Error Information"), S_OK);
		}

		HRESULT hr = RenderMoivesToVMR9(pGraphBuilder, lpszFilename);

		CRect rc;
		SetRect(&rc, 0, 0, 0, 0);
		hr |= GetNativeMovieSize(rc);
		hr |= SetMoviePosition(rc);

		if (SUCCEEDED(hr))
		{
			return hr;
		}
	}
//
//#ifdef _DEBUG
//	AddToRot(pGraphBuilder, &dwRegister);
//#endif

	return E_FAIL;
}

bool CWMVPlayer::RunMovies()
{
	if (NULL != pGraphBuilder && NULL != pMediaControl)
	{
		if (State_Paused == GetCurrentState() || State_Stopped == GetCurrentState())
		{
			if (SUCCEEDED(pMediaControl->Run()))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	return false;
}

bool CWMVPlayer::StopMovies()
{
	if (NULL != pGraphBuilder && NULL != pMediaControl)
	{
		if (State_Running == GetCurrentState())
		{
			if (SUCCEEDED(pMediaControl->Stop()))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	return false;
}

bool CWMVPlayer::PauseMovies()
{
	if (NULL != pGraphBuilder && NULL != pMediaControl)
	{
		if (State_Running == GetCurrentState())
		{
			if (SUCCEEDED(pMediaControl->Pause()))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	return false;
}

HRESULT CWMVPlayer::SetAspectRatio(BOOL bAspectRatio)
{
	HRESULT hr = NOERROR;

	if (NULL != pVMR9Control)
	{
		if(bAspectRatio)
			hr = pVMR9Control->SetAspectRatioMode(VMR9ARMode_LetterBox);
		else
			hr = pVMR9Control->SetAspectRatioMode(VMR9ARMode_None);

		if (SUCCEEDED(hr))
		{
			return hr;
		}
	}

	return E_FAIL;
}

HRESULT CWMVPlayer::GetNativeMovieSize(CRect& pos)
{
	HRESULT hr = NOERROR;

	if (NULL != pVMR9Control)
	{
		hr = pVMR9Control->GetNativeVideoSize(&pos.right, &pos.bottom, NULL, NULL);

		if (SUCCEEDED(hr))
		{
			return hr;
		}

		SetRect(&pos, 0, 0, 0, 0);
	}

	return E_FAIL;
}

HRESULT CWMVPlayer::GetMoviePosotion(CRect & pos)
{
	HRESULT hr = NOERROR;
	CRect src;

	if (NULL != pVMR9Control)
	{
		hr = pVMR9Control->GetVideoPosition(&src, &pos);

		if (SUCCEEDED(hr))
		{
			return hr;
		}

		SetRect(&pos, 0, 0, 0, 0);
	}

	return E_FAIL;
}

HRESULT CWMVPlayer::SetMoviePosition(CRect & pos)
{
	HRESULT hr = NOERROR;

	if (NULL != pVMR9Control)
	{
		hr = pVMR9Control->SetVideoPosition(NULL, &pos);

		if (SUCCEEDED(hr))
		{
			return hr;
		}
	}

	return E_FAIL;
}

HRESULT CWMVPlayer::GetCurrentPosition(double * outPosition)
{
	HRESULT hr = E_FAIL;

	if (NULL != pMediaSeeking)
	{
		LONGLONG position = (__int64)0.0;

		hr = pMediaSeeking->GetCurrentPosition(&position);
		if (SUCCEEDED(hr))
		{
			*outPosition = ((double)position) / 10000000.0;
			return hr;
		}
	}

	return hr;
}

HRESULT CWMVPlayer::GetStopPosition(double *outPosition)
{

	HRESULT hr = E_FAIL;

	if (NULL != pMediaSeeking)
	{
		LONGLONG position = (LONGLONG)0.0;

		hr = pMediaSeeking->GetStopPosition(&position);
		if (SUCCEEDED(hr))
		{
			*outPosition = ((double)position) / 10000000.0;
			return hr;
		}
	}

	return hr;
}

HRESULT CWMVPlayer::SetCurrentPosition(double inPosition)
{
	HRESULT hr = E_FAIL;

	if (NULL != pMediaSeeking)
	{
		LONGLONG position = (LONGLONG)(10000000 * inPosition);

		hr = pMediaSeeking->SetPositions(&position, AM_SEEKING_AbsolutePositioning |
			                             AM_SEEKING_SeekToKeyFrame, 0, AM_SEEKING_NoPositioning);

		if (SUCCEEDED(hr))
		{
			return hr;
		}
	}

	return hr;
}

HRESULT CWMVPlayer::SetStartStopPosition(double inStart, double inStop)
{
	HRESULT hr = E_FAIL;

	if (NULL != pMediaSeeking)
	{
		LONGLONG start = (LONGLONG)(10000000 * inStart);
		LONGLONG stop  = (LONGLONG)(10000000 * inStop);

		hr = pMediaSeeking->SetPositions(&start, AM_SEEKING_AbsolutePositioning | AM_SEEKING_SeekToKeyFrame,
			                             &stop, AM_SEEKING_AbsolutePositioning | AM_SEEKING_SeekToKeyFrame);

		if (SUCCEEDED(hr))
		{
			return hr;
		}
	}

	return hr;
}

HRESULT CWMVPlayer::GetDuration(double * outDuration)
{
	HRESULT hr = E_FAIL;

	if (NULL != pMediaSeeking)
	{
		LONGLONG length = (LONGLONG)0.0;

		hr = pMediaSeeking->GetDuration(&length);

		if (SUCCEEDED(hr))
		{
			*outDuration = ((double)length / 10000000.0);

			return hr;
		}
	}

	return hr;
}

HRESULT CWMVPlayer::SetPlaybackRate(double inRate)
{
	HRESULT hr = E_FAIL;

	if (NULL != pMediaSeeking)
	{
		hr = pMediaSeeking->SetRate(inRate);

		if (SUCCEEDED(hr))
		{
			return hr;
		}
	}

	return hr;
}

HRESULT CWMVPlayer::SetNotifyWindow(HWND inWindows)
{
	HRESULT hr = E_FAIL;

	if (NULL != pMediaEventEx)
	{
		hr = pMediaEventEx->SetNotifyWindow((OAHWND)inWindows, WM_GRAPHNOTIFY, 0);

		if (SUCCEEDED(hr))
		{
			return hr;
		}
	}

	return hr;
}

void CWMVPlayer::HandleEvent(WPARAM wParam, LPARAM lParam)
{
	if (NULL != pMediaEventEx)
	{
		LONG eventCode = 0;
		LONG_PTR eventParam1 = 0;
		LONG_PTR eventParam2 = 0;

		while(SUCCEEDED(pMediaEventEx->GetEvent(&eventCode, &eventParam1, &eventParam2, 0)))
		{
			pMediaEventEx->FreeEventParams(eventCode, eventParam1, eventParam2);
			switch (eventCode)
			{
			case EC_COMPLETE:
				break;

			case EC_USERABORT:
			case EC_ERRORABORT:
				break;

			default:
				break;
			}
		}
	}
}

IMediaEventEx * CWMVPlayer::GetEventHandle() const
{
	if (NULL != pMediaEventEx)
	{
		return pMediaEventEx;
	}

	return NULL;
}

HRESULT CWMVPlayer::UsedVideoMixingRenderer9()
{
	HRESULT hr = NOERROR;

	hr |= CoCreateInstance(CLSID_VideoMixingRenderer9, NULL, CLSCTX_INPROC,
		                   IID_IBaseFilter, (void **)&pVMR9);

	if (SUCCEEDED(hr))
	{
		SEND_LOG(_T("CLSID_VideoMixingRenderer9"));

		hr |= pGraphBuilder->AddFilter(pVMR9, L"Video Mixing Render 9");

		if (SUCCEEDED(hr))
		{
			SEND_LOG(_T("Video Mixing Render 9"));

			IVMRFilterConfig9 * pConfig9 = NULL;
			hr |= pVMR9->QueryInterface(IID_IVMRFilterConfig9, (void **)&pConfig9);

			if (SUCCEEDED(hr))
			{
				SEND_LOG(_T("IID_IVMRFilterConfig9"));

				hr |= pConfig9->SetNumberOfStreams(2);
				hr |= pConfig9->SetRenderingMode(VMR9Mode_Windowless);
				hr |= pConfig9->SetRenderingPrefs(RenderPrefs_AllowOverlays);

				pConfig9->Release();
			}
			pConfig9 = NULL;

			IVMRMonitorConfig9 * pMonitorConfig = NULL;
			hr |= pVMR9->QueryInterface(IID_IVMRMonitorConfig9, (void **)&pMonitorConfig);
			if(SUCCEEDED(hr))
			{
				SEND_LOG(_T("IID_IVMRMonitorConfig9"));

				UINT iCurrentMonitor;
				hr |= pMonitorConfig->GetMonitor(&iCurrentMonitor);
				pMonitorConfig->Release();
			}
			pMonitorConfig = NULL;

			hr |= pVMR9->QueryInterface(IID_IVMRWindowlessControl9, (void **)&pVMR9Control);

			if (SUCCEEDED(hr))
			{
				SEND_LOG(_T("IID_IVMRWindowlessControl9"));
			}
		}
	}

	SafeRelease(pVMR9);

	if (SUCCEEDED(hr))
	{
		hr |= pVMR9Control->SetVideoClippingWindow(mPlayerHandle);
		hr |= pVMR9Control->SetAspectRatioMode(VMR_ARMODE_LETTER_BOX);
	}
	else
	{
		SafeRelease(pVMR9Control);
	}

	return hr;
}

HRESULT CWMVPlayer::RenderMoivesToVMR9(IGraphBuilder * pGB, LPCTSTR lpszFilename)
{
	HRESULT hr = NOERROR;
	CComPtr <IPin> pOutputPin;
	CComPtr <IPin> pInputPin;
	CComPtr <IBaseFilter> pSource;
	CComPtr <IBaseFilter> pAudioRenderer;
	CComPtr <IFilterGraph2> pFG;
	CComPtr <IFileSourceFilter> pFileSource;

	//
	if (SUCCEEDED(CoCreateInstance(CLSID_DSoundRender, NULL, CLSCTX_INPROC_SERVER,
		IID_IBaseFilter, (void **)&pAudioRenderer)))
	{
		SEND_LOG(_T("CLSID_DSoundRender"));

		// The audio renderer was successfully created, so add it to the graph
		hr |= pGB->AddFilter(pAudioRenderer, L"Audio Renderer");
		if (FAILED(hr))
		{
			SEND_LOG(_T("No Audio Render !!!"));
			//return hr;
		}
		else
			SEND_LOG(_T("Audio Renderer"));
	}

	// Add a file source filter for this media file
	if (!IsWindowsMediaFile(lpszFilename))
	{
		// Add the source filter to the graph
		if (FAILED(hr |= pGB->AddSourceFilter(lpszFilename, L"SOURCE", &pSource)))
		{
			USES_CONVERSION;
			TCHAR szMsg[MAX_PATH + 128] = {0};

			hr = StringCchPrintf(szMsg, NUMELMS(szMsg), TEXT("Failed to add the source filter to the graph!  hr=0x%x\r\n\r\n")
				TEXT("Filename: %s\0"), hr, lpszFilename);
			MessageBox(NULL, szMsg, TEXT("Failed to render file to VMR9"), MB_OK | MB_ICONERROR);

			return hr;
		}

		hr |= GetUnconnectedPin(pSource, PINDIR_OUTPUT, &pOutputPin);
		if (FAILED(hr))
		{
			return hr;
		}

		// Get an IFilterGraph2 interface to assist in building the
		// multifile graph with the non-default VMR9 renderer
		hr |= pGB->QueryInterface(IID_IFilterGraph2, (void **)&pFG);
		if (FAILED(hr))
		{
			return hr;
		}

		SEND_LOG(_T("IID_IFilterGraph2"));

		// Render the output pin, using the VMR9 as the specified renderer.  This is
		// necessary in case the GraphBuilder needs to insert a Color Space convertor,
		// or if multiple filters insist on using multiple allocators.
		// The audio renderer will also be used, if the media file contains audio.
		hr |= pFG->RenderEx(pOutputPin, AM_RENDEREX_RENDERTOEXISTINGRENDERERS, NULL);
		if (FAILED(hr))
		{
			return hr;
		}

		// If this media file does not contain an audio stream, then the
		// audio renderer that we created will be unconnected.  If left in the
		// graph, it could interfere with rate changes and timing.
		// Therefore, if the audio renderer is unconnected, remove it from the graph.
		if (pAudioRenderer != NULL)
		{
			IPin *pUnconnectedPin=0;

			// Is the audio renderer's input pin connected?
			HRESULT hrPin = GetUnconnectedPin(pAudioRenderer, PINDIR_INPUT, &pUnconnectedPin);

			// If there is an unconnected pin, then remove the unused filter
			if (SUCCEEDED(hrPin) && (pUnconnectedPin != NULL))
			{
				// Release the returned IPin interface
				pUnconnectedPin->Release();

				// Remove the audio renderer from the graph
				hrPin = pGB->RemoveFilter(pAudioRenderer);
			}
		}
	}
	else
	{
		hr = pGB->RenderFile(lpszFilename, NULL);
		if(FAILED(hr))
		{
			ShowMsg(TEXT("Failed to create WMAsfWriter filter!  hr=0x%x\0"), hr);
		}
		return hr;

		// Load the improved ASF reader filter by CLSID
		hr |= CreateFilter(CLSID_WMAsfReader, &pSource);
		if(FAILED(hr))
		{
			ShowMsg(TEXT("Failed to create WMAsfWriter filter!  hr=0x%x\0"), hr);
			return hr;
		}

		// Add the ASF reader filter to the graph.  For ASF/WMV/WMA content,
		// this filter is NOT the default and must be added explicitly.
		hr |= pGB->AddFilter(pSource, L"WM ASF Reader");
		if(FAILED(hr))
		{
			ShowMsg(TEXT("Failed to add ASF reader filter to graph!  hr=0x%x\0"), hr);
			return hr;
		}

		SEND_LOG(_T("WM ASF Reader"));

		// Set its source filename
		hr |= pSource->QueryInterface(IID_IFileSourceFilter, (void **) &pFileSource);

		// Attempt to load this file
		hr |= pFileSource->Load(lpszFilename, NULL);

		if (FAILED(hr))
		{
			ShowMsg(TEXT("Failed to load file in source filter (pFileSource->Load())!  hr=0x%x\0"), hr);
			return hr;
		}

		SEND_LOG(_T("IID_IFileSourceFilter"));

		ULONG count = 0;
		CountTotalFilterPins(pSource, &count);

		for (ULONG i = 0; i < count; i ++)
		{
			pOutputPin = GetOutPin(pSource, i);
			PIN_INFO info;
			pOutputPin->QueryPinInfo(&info);

			CComPtr<IBaseFilter> Codec;

			if (wcsstr(info.achName, L"Video"))
			{
				Codec = CreateEncodec(_T("WMVideo Decoder DMO"));
				if (NULL != pGraphBuilder)
				{
					pGraphBuilder->AddFilter(Codec, L"WMVideo Decoder DMO");
					pInputPin = GetInPin(Codec, 0);

					hr |= pGraphBuilder->Connect(pOutputPin, pInputPin);
					if (FAILED(hr))
					{
						ShowMsg(_T("Video Connected Failed.  hr=0x%x\0"), hr);
						return hr;
					}

					SEND_LOG(_T("WMVideo Decoder DMO"));

					pOutputPin = GetOutPin(Codec, 0);
					pInputPin = GetInPin(pVMR9, 0);

					hr |= pGraphBuilder->Connect(pOutputPin, pInputPin);
					if (FAILED(hr))
					{
						ShowMsg(_T("Video Connected Failed.  hr=0x%x\0"), hr);
						return hr;
					}
				}
			}
			else if (wcsstr(info.achName, L"Audio"))
			{
				Codec = CreateEncodec(_T("WMAudio Decoder DMO"));
				if (NULL != pGraphBuilder)
				{
					pGraphBuilder->AddFilter(Codec, L"WMAudio Decoder DMO");
					pInputPin = GetInPin(Codec, 0);

					hr |= pGraphBuilder->Connect(pOutputPin, pInputPin);
					if (FAILED(hr))
					{
						ShowMsg(_T("Audio Connected Failed.  hr=0x%x\0"), hr);
						return hr;
					}

					SEND_LOG(_T("WMAudio Decoder DMO"));

					pOutputPin = GetOutPin(Codec, 0);

					if (NULL == pAudioRenderer)
					{
						if (SUCCEEDED(CoCreateInstance(CLSID_DSoundRender, NULL, CLSCTX_INPROC_SERVER,
							IID_IBaseFilter, (void **)&pAudioRenderer)))
						{
							// The audio renderer was successfully created, so add it to the graph
							hr |= pGB->AddFilter(pAudioRenderer, L"Audio Renderer");
							if (FAILED(hr))
							{
								return hr;
							}

							SEND_LOG(_T("CLSID_DSoundRender"));
						}
					}

					pInputPin = GetInPin(pAudioRenderer, 0);

					hr |= pGraphBuilder->Connect(pOutputPin, pInputPin);
					if (FAILED(hr))
					{
						ShowMsg(_T("Audio Connected Failed.  hr=0x%x\0"), hr);
						return hr;
					}
				}
			}
		}
	}

	return hr;
}

BOOL CWMVPlayer::IsWindowsMediaFile(LPCTSTR lpszFilename)
{
	USES_CONVERSION;
	TCHAR szFilename[MAX_PATH] = {0};

	// Copy the file name to a local string and convert to lowercase
	(void)StringCchCopy(szFilename,NUMELMS(szFilename), lpszFilename);
	szFilename[MAX_PATH-1] = 0;
	_tcslwr_s(szFilename, 260);

	if (_tcsstr(szFilename, TEXT(".asf")) ||
		_tcsstr(szFilename, TEXT(".wma")) ||
		_tcsstr(szFilename, TEXT(".wmv")))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

OAFilterState CWMVPlayer::GetCurrentState()
{
	OAFilterState states = 0;

	if (NULL != pGraphBuilder && NULL != pMediaControl)
	{
		pMediaControl->GetState(INFINITE, &states);
	}

	return states;
}

//
////Register the filter graph to Rot
//HRESULT CWMVPlayer::AddToRot(IUnknown *pUnkGraph, DWORD *pdwRegister)
//{
//	HRESULT hr = NOERROR;
//	IMoniker * pMoniker = NULL;
//	IRunningObjectTable * pROT = NULL;
//	if(FAILED(GetRunningObjectTable(0,&pROT)))
//	{
//		return E_FAIL;
//	}
//
//	WCHAR wsz[256];
//	::memset(wsz,0,256);
//
//	hr = StringCchPrintfW(wsz, NUMELMS(wsz), L"FilterGraph %08x pid %08x",
//		(DWORD_PTR)pUnkGraph,GetCurrentProcessId());
//
//	hr = CreateItemMoniker(L"!",wsz,&pMoniker);
//	if(SUCCEEDED(hr))
//	{
//		hr = pROT->Register(0,pUnkGraph,pMoniker,pdwRegister);
//		pMoniker->Release();
//	}
//	pROT->Release();
//	return hr;
//}
//
//void CWMVPlayer::RemoveFromRot(DWORD pdwRegister)
//{
//	IRunningObjectTable *pROT = NULL;
//	if(SUCCEEDED(GetRunningObjectTable(0,&pROT)))
//	{
//		pROT->Revoke(pdwRegister);
//		pROT->Release();
//	}
//}

HRESULT CWMVPlayer::GetUnconnectedPin(
									   IBaseFilter *pFilter,   // Pointer to the filter.
									   PIN_DIRECTION PinDir,   // Direction of the pin to find.
									   IPin **ppPin)           // Receives a pointer to the pin.
{
	IEnumPins *pEnum = 0;
	IPin *pPin = 0;

	if (!ppPin)
		return E_POINTER;
	*ppPin = 0;

	// Get a pin enumerator
	HRESULT hr = pFilter->EnumPins(&pEnum);
	if (FAILED(hr))
		return hr;

	// Look for the first unconnected pin
	while (pEnum->Next(1, &pPin, NULL) == S_OK)
	{
		PIN_DIRECTION ThisPinDir;

		pPin->QueryDirection(&ThisPinDir);
		if (ThisPinDir == PinDir)
		{
			IPin *pTmp = 0;

			hr = pPin->ConnectedTo(&pTmp);
			if (SUCCEEDED(hr))  // Already connected, not the pin we want.
			{
				pTmp->Release();
			}
			else  // Unconnected, this is the pin we want.
			{
				pEnum->Release();
				*ppPin = pPin;
				return S_OK;
			}
		}
		pPin->Release();
	}

	// Release the enumerator
	pEnum->Release();

	// Did not find a matching pin
	return E_FAIL;
}

IBaseFilter* CWMVPlayer::CreateEncodec(CString inFriendlyName)
{
	HRESULT hr = NOERROR;
	ICreateDevEnum * enumHardware =NULL;
	IBaseFilter    * hardwareFilter = NULL;

	hr = CoCreateInstance(CLSID_SystemDeviceEnum,NULL,CLSCTX_INPROC_SERVER,
		IID_ICreateDevEnum,(void **)&enumHardware);

	if(FAILED(hr))
	{
		return NULL;
	}

	IEnumMoniker * enumMoniker = NULL;

	hr = enumHardware->CreateClassEnumerator(CLSID_LegacyAmFilterCategory, &enumMoniker,0);

	if(FAILED(hr))
	{
		return NULL;
	}

	if(enumMoniker)
	{
		enumMoniker->Reset();

		IMoniker * moniker = NULL;
		char friendlyName[256];
		ZeroMemory(friendlyName,256);

		while(S_OK == enumMoniker->Next(1,&moniker,0))
		{
			if(moniker)
			{
				IPropertyBag * proBag = NULL;
				VARIANT    name;

				hr = moniker->BindToStorage(NULL,NULL,IID_IPropertyBag,(void **)&proBag);

				if(SUCCEEDED(hr))
				{
					name.vt = VT_BSTR;
					proBag->Read(L"FriendlyName",&name,NULL);
				}

				if(SUCCEEDED(hr))
				{
					WideCharToMultiByte(CP_ACP, 0, name.bstrVal, -1,friendlyName, 256,
						NULL, NULL);
					CString str = (CString)friendlyName;
					if(inFriendlyName == str)
					{
						moniker->BindToObject(NULL,NULL,IID_IBaseFilter,
							(void **)&hardwareFilter);
					}
				}
				if(proBag)
				{
					proBag->Release();
					proBag = NULL;
				}
				moniker->Release();
			}
		}
		enumMoniker->Release();
	}
	enumHardware->Release();

	return hardwareFilter;
}

HRESULT CWMVPlayer::CreateFilter(REFCLSID clsid, IBaseFilter **ppFilter)
{
	HRESULT hr;

	hr = CoCreateInstance(clsid, NULL, CLSCTX_INPROC_SERVER,
		IID_IBaseFilter,
		(void **) ppFilter);

	if(FAILED(hr))
	{
		//MessageBox(TEXT("CreateFilter: Failed to create filter!  hr=0x%x\n"));
		if (ppFilter)
			*ppFilter = NULL;
		return hr;
	}

	return S_OK;
}

void CWMVPlayer::ShowMsg(TCHAR *szFormat, ...)
{
	TCHAR szBuffer[1024];  // Large buffer for long filenames or URLs
	const size_t NUMCHARS = sizeof(szBuffer) / sizeof(szBuffer[0]);
	const int LASTCHAR = NUMCHARS - 1;

	// Format the input string
	va_list pArgs;
	va_start(pArgs, szFormat);

	// Use a bounded buffer size to prevent buffer overruns.  Limit count to
	// character size minus one to allow for a NULL terminating character.
	(void)StringCchVPrintf(szBuffer, NUMCHARS - 1, szFormat, pArgs);
	va_end(pArgs);

	// Ensure that the formatted string is NULL-terminated
	szBuffer[LASTCHAR] = TEXT('\0');

	// Display a message box with the formatted string
	MessageBox(NULL, szBuffer, TEXT("Porsche HD Player"), MB_OK);
}