 /*! \file sampleWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _sampleWorld_h_
#define _sampleWorld_h_
#pragma once


#include <cci/libWorld/cciORBWorld.h> 
#include <ci/libThread/ciMutex.h> 
#include <ci/libThread/ciThread.h> 




class sampleWorld : public cciORBWorld {
public:
	sampleWorld();
	~sampleWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

	static  int password();

	static int modifyBRWProperties(char* arg);
	static int modifyEdition(char* arg);
	static ciBoolean hostAutoUpdate();

protected:	
	static int sha256Test(char* arg);
	static int encodingTest(char* arg);
	static int getSiteId(char* arg);
	static int loginEnt();
	static int replace_contents(char* arg);
	static int gather_contents(char* arg);
	static int changeServerIP(char* arg);
	static int setServerEnv(char* arg);

	static int pro2ini(char* arg);
	static int hostAutoUpdate(char* arg);
	static int getScreenShot(char* arg);
	static int getAppliedHostList(char* arg);
	static int getReservedHostList(char* arg);


	static int listEnt(char* arg);
	static int createEnt(char* arg);
	static int deleteEnt(char* arg);
	static int setEnt(char* arg);
	static int loginEnt(ciString& id);

	static int listKey(char* arg);
	static int listIp(char* arg);
	static int createKey(char* arg);
	static int createKey1(char* arg);
	static int deleteKey(char* arg);
	static int resetKey(char* arg);
	static int createSite(char* arg);
	static int deleteSite(char* arg);
	static int listSite(char* arg);
	static int sortSite(char* arg);


	static int ubcMux(char* arg);
	static int setComputerName(char* arg);
	static int getBrowserInfo(char* arg);
	static int wbs(char* arg);
	static int sample(char* arg);
	static int importNotify(char* arg);
	static int ping(char* arg);
	
	static int writeAuth(char* arg);
	static int writeLimit(char* arg);
	static int setSiteId(char* arg);
	static int readAuth(char* arg);
	static int readLimit(char* arg);
	static int auth(char* arg);
	static int drm1(char* arg);
	static int setRegistry1(char* arg);
	static int updateStudioLink(char* arg);
	static int socketAgent(char* arg);
	static int socketAgent2(char* arg);
	static int vncBack(char* arg);
	static int printRegi(char* arg);
	static int fileTest(char* arg);
	static int powerOn(char* arg);

	static int doAddJob(char* arg);
	static int showBalloonTip(char* arg);
	static int toggleTaskbar(char* arg);
	static int getVersion(char* arg);

	static int sample_call(char* arg);
	static int sample_prop(char* arg);
	static int sample_post(char* arg);
	static int sample_register(char* arg);
	static int sample_deregister(char* arg);

	static int addOrbConn(char* arg);
	static int delOrbConn(char* arg);
	static int modOrbConn(char* arg);
	static int getCurOrbConn(char* arg);
	static int setCurOrbConn(char* arg);
	static int getOrbConnList(char* arg);
	static int alterTable(char* arg);
	static int test(char* arg);
	static int start(char* arg);
	static int stop(char* arg);
	static int runPlan(char* arg);
	static int announce(char* arg);
	static int printWLog(char* arg);
	static int filterGet(char* arg);
	static int filterTest(char* arg);

	static int toMSSQL(char* arg);
	static int toMYSQL(char* arg);
	static int httpTest(char* arg);
	static int getClassInfo(char* arg);
	static int crc(char* arg);
	static int dbInstall(char* arg);
	static int dbInstall_ms(char* arg);
	static int installNas(char* arg);

	static int pjLinkOn(char* arg);
	static int pjLinkOff(char* arg);
	static int pjLinkGet(char* arg);
	
	static int AsciiToBase64(string strAscii, string& outVal);
	static int axisTest(char* arg);
	static int axisTest2(char* arg);


	static void do_pw(ciString& query, int counter, ciString& priority,ciString& status,	ciString& important,
			ciString& category,	ciString& program,ciString& r_category,
		ciString& work,	ciString& worker,ciString& e_start,	ciString& e_end,
		ciString& effort,ciString& r_start,	ciString& r_end,ciString& comment, ciString& workerid);

	static void do_pw_detail(ciString& query,int counter, ciString& priority,ciString& status,	ciString& important,
			ciString& category,	ciString& program,ciString& r_category,
		ciString& work,	ciString& worker,ciString& e_start,	ciString& e_end,
		ciString& effort,ciString& r_start,	ciString& r_end,ciString& comment, ciString& workerid);
	
	static void mylog(ofstream& out, const char *pFormat,...) ;
	static ciLong _eventId;

	static int	serialReceiver(char* arg);
	static ciBoolean _getCurrentSerialServerIP(ciString& currentSerialServerIp);

public:
	void lockTest(const char* name);
	ciMutex _lock;
	
};

class testThread2 : public ciThread
{
public:
	testThread2(sampleWorld* p, const char* n) { world = p; name=n;}
	void run() { 
		while(1) {
			world->lockTest(name.c_str());
		}
	}
	sampleWorld* world;
	ciString name;
};

#endif //_sampleWorld_h_
