#include "JsonBase.h"

#include <ci/libDebug/ciDebug.h>
ciSET_DEBUG(5, "JsonBase");
///////////////////////////////////////////////////////////////////////
//
// JsonValue implementations
//
JsonValue* JsonValue::createFromFile(const char *fullpath)
{
	FILE*	fd = fopen(fullpath, "rb"); 
	if(fd == 0) {
		ciERROR(("%s file open error", fullpath));
		return 0;
	}

	ciString aStr;
	char buf[1024];
	int read_size = 0;
	do {
		memset(buf,0x00,1024);
		read_size = fread(buf, 1, 1024-1, fd);
		aStr += buf;
	} while (read_size == 1024-1);
	fclose(fd);

	if(aStr.size() == 0){
		ciERROR(("%s file read error", fullpath));
		return 0;
	}
	return create(aStr.c_str(),0, (int) aStr.size());
}

JsonValue* JsonValue::create(const char *szSrc, int nStart, int nLen, int *pNextPos)
{
    JsonValue *p = NULL;
    int i = findExcluding(szSrc, nStart, nLen, JSON_S_IGNORE_CHARSET);
    if (i < 0) {
		//ciERROR(("findExcluding(%s,%d,%d,%s) is Failed", szSrc, nStart,nLen, JSON_S_IGNORE_CHARSET));
        return p;
	}

    switch (szSrc[i]) {
    case JSON_N_OBJECT_HEAD:
        p = new JsonObject();
        break;
    case JSON_N_ARRAY_HEAD:
        p = new JsonArray();
        break;
    case JSON_N_STRING_TIE:
        p = new JsonString();
        break;
	case JSON_N_NULL_HEAD:
		p = new JsonNull();
		break;
    default:
        p = new JsonNumber();
        break;
    }

    if (p) {
        int nNextPos = p->parse(szSrc, i, nLen);
        if (nNextPos < 0) {
			//ciERROR(("Value[%d]'s parse(szSrc[size:%d], i[%d], nLen[%d]) is Failed with szSrc[%d~]:%s",
			//				p->getType(), strlen(szSrc), i, nLen, i, szSrc+i));
            delete p;
            p = NULL;
        }
        else if (pNextPos) {
            *pNextPos = nNextPos;
        }
    }

    return p;
}

int JsonValue::findChar(const char *szSrc, int nStart, int nLen, char nFindChar, char nEscapeChar)
{
    for (int i = nStart; i < nLen; i++) {
#ifdef JSON_DEF_EXCLUDE_UTF8
        if (szSrc[i] < 0) {
            i++;
            continue;
        }
#endif
        if (szSrc[i] == nEscapeChar) {
            i++;
#ifdef JSON_DEF_EXCLUDE_UTF8
            if (i < nLen && szSrc[i] < 0)
                i++;
#endif
            continue;
        }
        if (szSrc[i] == nFindChar)
            return i;
    }

    return -1;
}

int JsonValue::findIncluding(const char *szSrc, int nStart, int nLen, const char *szFindCharSet)
{
    int nCharSetLen = (int) strlen(szFindCharSet);

    for (int nPos = nStart; nPos < nLen; nPos++) {
#ifdef JSON_DEF_EXCLUDE_UTF8
        if (szSrc[nPos] < 0) {
            nPos++;
            continue;
        }
#endif
        for (int i = 0; i < nCharSetLen; i++) {
            if (szSrc[nPos] == szFindCharSet[i]) {
                return nPos;
            }
        }
    }

    return -1;
}

int JsonValue::findExcluding(const char *szSrc, int nStart, int nLen, const char *szFindCharSet)
{
    int nCharSetLen = (int) strlen(szFindCharSet);

    for (int nPos = nStart; nPos < nLen; nPos++) {
        if (szSrc[nPos] < 0) {
            return nPos;
        }
        bool bFound = false;
        for (int i = 0; i < nCharSetLen; i++) {
            if (szSrc[nPos] == szFindCharSet[i]) {
                bFound = true;
                break;
            }
        }
        if (bFound == false)
            return nPos;
    }

    return -1;
}

JsonValue::JsonValue()
{
}

JsonValue::~JsonValue()
{
}

///////////////////////////////////////////////////////////////////////
//
// JsonNull implementations
//
JsonNull::JsonNull()
{
    _nType = TYPE_NULL;
}

JsonNull::~JsonNull()
{

}

int JsonNull::parse(const char *szSrc, int nStart, int nLen)
{
	int i = nStart;

	if(szSrc[i] == 'n'
		&& szSrc[i+1] == 'u'
		&& szSrc[i+2] == 'l'
		&& szSrc[i+3] == 'l') {
		return (i+4);
	}
    return nLen;
}

void* JsonNull::getValue()
{
	return NULL;
}

const char* JsonNull::getString()
{
	return "";
}

string JsonNull::toString()
{
    return string(JSON_S_NULL);
}

int JsonNull::toString(string& retStr)
{
	retStr = JSON_S_NULL;
	return (int) retStr.size();
}

void JsonNull::debugPrint(int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;
    printf("%s%s%s%s\n", szIndent, szPrefix ? szPrefix:"NULL", JSON_S_NULL, szSuffix);
}

void JsonNull::debugPrint2(FILE* fp, int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;
    fprintf(fp, "%s%s%s%s", szIndent, szPrefix ? szPrefix:"NULL", JSON_S_NULL, szSuffix);
}

///////////////////////////////////////////////////////////////////////
//
// JsonString implementations
//
JsonString::JsonString()
{
    _nType = TYPE_STRING;
}
    
JsonString::JsonString(const char *value)
{
    _nType = TYPE_STRING;

    _value = value;
    string str = setEscapeChar(value, 0, (int) strlen(value));

    _strValue = JSON_S_STRING_TIE + str + JSON_S_STRING_TIE;
}
    
JsonString::JsonString(const char *value, int nLen)
{
    _nType = TYPE_STRING;

    string str;
    str.assign(value, nLen);
    _value = str.c_str();

    str = setEscapeChar(_value.c_str(), 0, nLen);

    _strValue = JSON_S_STRING_TIE + str + JSON_S_STRING_TIE;
}

JsonString::~JsonString()
{
	//ciDEBUG(10,("~JsonString() with Data[%s]", getString()));
}

int JsonString::parse(const char *szSrc, int nStart, int nLen)
{
    if (szSrc[nStart] == JSON_N_STRING_TIE) {
        int nPos = findChar(szSrc, nStart + 1, nLen, JSON_N_STRING_TIE, JSON_N_ESCAPE_CHAR);
        if (nPos < 0)
            return -1;

        _strValue.assign(szSrc + nStart, nPos - nStart + 1);
        _value = removeEscapeChar(szSrc, nStart + 1, nPos);
        return nPos + 1;
    }
    else {
        int nPos = findIncluding(szSrc, nStart, nLen, JSON_S_TOKEN_STOPPER);
        if (nPos < 0)
            return -1;

        if (nPos == 0) {
            _value    = "";
            _strValue = JSON_S_STRING_TIE JSON_S_STRING_TIE;
            return 0;
        }
        else {
            _value.assign(szSrc + nStart, nPos - nStart);
            _strValue = JSON_S_STRING_TIE + _value + JSON_S_STRING_TIE;
            return nPos;
        }
    }
}

string JsonString::toString()
{
    return _strValue;
}

int JsonString::toString(string& str)
{
	str = _strValue;
	return (int) str.size();
}

void JsonString::debugPrint(int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;
    printf("%s%s%s%s\n", szIndent, szPrefix ? szPrefix:"NULL", _strValue.c_str(), szSuffix);
}

void JsonString::debugPrint2(FILE* fp, int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;
    fprintf(fp, "%s%s%s%s", szIndent, szPrefix ? szPrefix:"NULL", _strValue.c_str(), szSuffix);
}

string JsonString::setEscapeChar(const char *szSrc, int nStart, int nLen)
{
    string str = "";
    for (int i = 0; i < nLen && szSrc[i]; i++) {
#ifdef JSON_DEF_EXCLUDE_UTF8
        if (szSrc[i] < 0) {
            str += szSrc[i];
            i++;
            if (i < nLen && szSrc[i])
                str += szSrc[i];
        }
#endif
#ifdef JSON_DEF_NO_STANDARD_ESCAPE
    #ifdef JSON_DEF_EXCLUDE_UTF8
        else if (szSrc[i] == JSON_N_ESCAPE_CHAR || szSrc[i] == JSON_N_STRING_TIE) {
    #else
        if (szSrc[i] == JSON_N_ESCAPE_CHAR || szSrc[i] == JSON_N_STRING_TIE) {
    #endif
            str += JSON_N_ESCAPE_CHAR;
            str += szSrc[i];
        }
        else 
            str += szSrc[i];
#else
    #ifdef JSON_DEF_EXCLUDE_UTF8
        else {
    #else
        {
    #endif
            switch (szSrc[i]) {
            case JSON_N_ESCAPE_CHAR:
            case JSON_N_STRING_TIE:
                str += JSON_N_ESCAPE_CHAR;
                str += szSrc[i];
                break;
/*
            case '/':
                str += "\\/";
                break;
*/
            case '\n':  // new line
                str += "\\n";
                break;
            case '\r':  // carriage return
                str += "\\r";
                break;
            case '\b':  // back space
                str += "\\b";
                break;
            case '\f':  // form feed
                str += "\\f";
                break;
            case '\t':  // tab
                str += "\\t";
                break;
            default:
                str += szSrc[i];
                break;
            }
        }
#endif
    }

    return str;
}

string JsonString::removeEscapeChar(const char *szSrc, int nStart, int nLen)
{
    string str = "";
    for (int i = nStart; i < nLen && szSrc[i]; i++) {
#ifdef JSON_DEF_NO_STANDARD_ESCAPE
    #ifdef JSON_DEF_EXCLUDE_UTF8
        if (szSrc[i] < 0) {
            str += szSrc[i];
            i++;
        }
        else if (szSrc[i] == JSON_N_ESCAPE_CHAR && i + 1 < nLen && (szSrc[i + 1] == JSON_N_STRING_TIE || szSrc[i + 1] == JSON_N_ESCAPE_CHAR)) {
    #else
        if (szSrc[i] == JSON_N_ESCAPE_CHAR && i + 1 < nLen && (szSrc[i + 1] == JSON_N_STRING_TIE || szSrc[i + 1] == JSON_N_ESCAPE_CHAR)) {
    #endif
            i++;
        }
        if (i < nLen && szSrc[i])
            str += szSrc[i];
#else
    #ifdef JSON_DEF_EXCLUDE_UTF8
        if (szSrc[i] < 0) {
            str += szSrc[i];
            i++;
            if (i < nLen && szSrc[i])
                str += szSrc[i];
        }
        else if (szSrc[i] == JSON_N_ESCAPE_CHAR) {
    #else
        if (szSrc[i] == JSON_N_ESCAPE_CHAR) {
    #endif
            i++;
            if (i < nLen && szSrc[i]) {
                switch (szSrc[i]) {
                case JSON_N_ESCAPE_CHAR:
                case JSON_N_STRING_TIE:
                case '/':
                    str += szSrc[i];
                    break;
                case 'n':  // new line
                    str += '\n';
                    break;
                case 'r':  // carriage return
                    str += '\r';
                    break;
                case 'b':  // back space
                    str += '\b';
                    break;
                case 'f':  // form feed
                    str += '\f';
                    break;
                case 't':  // tab
                    str += '\t';
                    break;
                default:
                    str += JSON_N_ESCAPE_CHAR;
                    str += szSrc[i];
                    break;
                }
            }
            else {
                str += JSON_N_ESCAPE_CHAR;
            }
        }
        else {
            str += szSrc[i];
        }
#endif
    }

    return str;
}

///////////////////////////////////////////////////////////////////////
//
// JsonNumber implementations
//
JsonNumber::JsonNumber()
{
    _nType = TYPE_NUMBER;
}

JsonNumber::JsonNumber(int nValue)
{
    _nType = TYPE_NUMBER;
    char szValue[20];
    sprintf(szValue, "%d", nValue);
    _value = szValue;
}

JsonNumber::JsonNumber(unsigned int nValue)
{
    _nType = TYPE_NUMBER;
    char szValue[21];
    sprintf(szValue, "%d", nValue);
    _value = szValue;
}

JsonNumber::JsonNumber(const char* szValue)
{
    _nType = TYPE_NUMBER;
	_value = szValue;
}

JsonNumber::~JsonNumber()
{
	//ciDEBUG(10,("~JsonNumber() with Data[%s]", getString()));
}

int JsonNumber::parse(const char *szSrc, int nStart, int nLen)
{
    string strResult = "";
    for (int i = nStart; i < nLen; i++) {
        if ((szSrc[i] < '0' || szSrc[i] > '9') && szSrc[i] != '.') {
			if(i > nStart || szSrc[i] != '-') {
				_value = strResult;
				return i;
			}
        }
        strResult += szSrc[i];
    }

    _value = strResult;
    return nLen;
}

int JsonNumber::getInt()
{
	return atoi(_value.c_str());
}

unsigned int JsonNumber::getUInt()
{
	return strtoul(_value.c_str(),NULL,0);
}

float JsonNumber::getFloat()
{
	return (float) atof(_value.c_str());
}

long int JsonNumber::getLongInt()
{
	return atol(_value.c_str());
}

string JsonNumber::toString()
{
    return _value;
}

int JsonNumber::toString(string& retStr)
{
	retStr = _value;
	return (int) retStr.size();
}

void JsonNumber::debugPrint(int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;
    printf("%s%s%s%s\n", szIndent, szPrefix ? szPrefix:"NULL", _value.c_str(), szSuffix);
}

void JsonNumber::debugPrint2(FILE* fp, int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;
    fprintf(fp, "%s%s%s%s", szIndent, szPrefix ? szPrefix:"NULL", _value.c_str(), szSuffix);
}

///////////////////////////////////////////////////////////////////////
//
// JsonArray implementations
//
JsonArray::JsonArray()
{
    _nType = TYPE_ARRAY;
}

JsonArray::~JsonArray()
{
	//ciDEBUG(10, ("~JsonArray()"));

    for (int i = 0; i < (int) _value.size(); i++) {
        JsonValue *p = _value[i];
        delete p;
    }
    _value.resize(0);
}

int JsonArray::parse(const char *szSrc, int nStart, int nLen)
{
    nStart++;
    while (true) {
        nStart = findExcluding(szSrc, nStart, nLen, JSON_S_IGNORE_CHARSET);
        if (nStart < 0)
            return -1;
        if (szSrc[nStart] == JSON_N_ARRAY_TAIL)
            return nStart + 1;

        JsonValue *p = JsonValue::create(szSrc, nStart, nLen, &nStart);
        if (p == NULL)
            return -1;

        _value.resize(_value.size() + 1);
        _value[_value.size() - 1] = p;

        if (nStart >= nLen)
            return -1;

        nStart = findExcluding(szSrc, nStart, nLen, JSON_S_IGNORE_CHARSET);
        if (nStart < 0)
            return -1;
        if (szSrc[nStart] == JSON_N_ARRAY_TAIL)
            return nStart + 1;
        if (szSrc[nStart] != JSON_N_TOKENER)
            return -1;

        nStart++;
    }
}

string JsonArray::toString()
{
    string str(JSON_S_ARRAY_HEAD);

    for (int i = 0; i < (int) _value.size(); i++) {
        JsonValue *p = _value[i];
        if (i > 0)
            str += JSON_N_TOKENER;
        str += p->toString();
    }
    str += JSON_N_ARRAY_TAIL;

    return str;
}

int JsonArray::toString(string& str)
{
    str = JSON_S_ARRAY_HEAD;

    for (int i = 0; i < (int) _value.size(); i++) {
        JsonValue *p = _value[i];
        if (i > 0)
            str += JSON_N_TOKENER;
        str += p->toString();
    }
    str += JSON_N_ARRAY_TAIL;

	return (int) str.size();
}

void JsonArray::debugPrint(int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;

    printf("%s%s%c\n", szIndent, szPrefix ? szPrefix:"NULL", JSON_N_ARRAY_HEAD);
    for (int i = 0; i < (int) _value.size(); i++) {
        JsonValue *p = _value[i];
        if (i + 1 == _value.size())
            p->debugPrint(nIndent + 1);
        else
            p->debugPrint(nIndent + 1, "", JSON_S_TOKENER);
    }
    printf("%s%c%s\n", szIndent, JSON_N_ARRAY_TAIL, szSuffix);
}

void JsonArray::debugPrint2(FILE* fp, int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;

    fprintf(fp, "%s%s%c", szIndent, szPrefix ? szPrefix:"NULL", JSON_N_ARRAY_HEAD);
    for (int i = 0; i < (int) _value.size(); i++) {
        JsonValue *p = _value[i];
		if(p->getType() < JsonValue::TYPE_OBJECT) {
			if (i + 1 == _value.size())
				p->debugPrint2(fp, 0);
			else
				p->debugPrint2(fp, 0, "", JSON_S_TOKENER);
		} else {
			if (i + 1 == _value.size())
				p->debugPrint2(fp, nIndent + 1);
			else
				p->debugPrint2(fp, nIndent + 1, "", JSON_S_TOKENER);
		}
    }
    fprintf(fp, "%s%c%s\n", szIndent, JSON_N_ARRAY_TAIL, szSuffix);
}

int JsonArray::add(JsonValue *pValue)
{
    _value.resize(_value.size() + 1);
    _value[_value.size() - 1] = pValue;
    return 1;
}

int JsonArray::add(const char *szValue)
{
   return add(new JsonString(szValue));
}

int JsonArray::add(int nValue)
{
   return add(new JsonNumber(nValue));
}

int JsonArray::add(unsigned int nValue)
{
   return add(new JsonNumber(nValue));
}

///////////////////////////////////////////////////////////////////////
//
// JsonObject implementations
//
JsonObject::JsonObject()
{
    _nType = TYPE_OBJECT;
}

JsonObject::~JsonObject()
{
	//ciDEBUG(10, ("~JsonObject()"));

	// modified by jhchoi 2011.11.29
    //while (_value.size() > 0) {
    while (!_value.empty()) {
        JSON_T_OBJECT_ELEMENT *p = _value.front();
		if (p->name) { delete p->name ; p->name = 0; }
		if (p->value) { delete p->value; p->value = 0; }
        delete p; p = 0;
        _value.pop_front();
    }
}

int JsonObject::parse(const char *szSrc, int nStart, int nLen)
{
    nStart++;
    while (true) {
        nStart = findExcluding(szSrc, nStart, nLen, JSON_S_IGNORE_CHARSET);
        if (nStart < 0)
            return -1;

        if (szSrc[nStart] == JSON_N_OBJECT_TAIL)
            return nStart + 1;

        JsonValue *pName = JsonValue::create(szSrc, nStart, nLen, &nStart);
        if (pName == NULL)
            return -1;

        if (pName->getType() != TYPE_STRING) {
            delete pName;
            return -1;
        }

        nStart = findExcluding(szSrc, nStart, nLen, JSON_S_IGNORE_CHARSET);
        if (nStart < 0) {
            delete pName;
            return -1;
        }

        if (szSrc[nStart] != JSON_N_NAMEVALUE_TOKENER) {
            delete pName;
            return -1;
        }
        nStart++;

        if (nStart >= nLen) {
            delete pName;
            return -1;
        }

        JsonValue *pValue = JsonValue::create(szSrc, nStart, nLen, &nStart);
        if (pValue == NULL) {
            delete pName;
            return -1;
        }

        JSON_T_OBJECT_ELEMENT *obj = new JSON_T_OBJECT_ELEMENT;
        obj->name  = (JsonString*) pName;
        obj->value = pValue;
        _value.push_back(obj);

        if (nStart >= nLen)
            return -1;

        nStart = findExcluding(szSrc, nStart, nLen, JSON_S_IGNORE_CHARSET);
        if (nStart < 0)
            return -1;
        if (szSrc[nStart] == JSON_N_OBJECT_TAIL)
            return nStart + 1;
        if (szSrc[nStart] != JSON_N_TOKENER) 
            return -1;

        nStart++;
    }
}

string JsonObject::toString()
{
    string str(JSON_S_OBJECT_HEAD);

    list<JSON_T_OBJECT_ELEMENT*>::iterator itr;
    int i = 0;
    for (itr = _value.begin(); itr != _value.end(); itr++, i++) {
        JSON_T_OBJECT_ELEMENT *p = *itr;
        if (i > 0)
            str += JSON_N_TOKENER;
        str += p->name->toString();
        str += JSON_N_NAMEVALUE_TOKENER + p->value->toString();
    }

    str += JSON_N_OBJECT_TAIL;

    return str;
}

int JsonObject::toString(string& str)
{
	str = JSON_S_OBJECT_HEAD;

    list<JSON_T_OBJECT_ELEMENT*>::iterator itr;
    int i = 0;
    for (itr = _value.begin(); itr != _value.end(); itr++, i++) {
        JSON_T_OBJECT_ELEMENT *p = *itr;
        if (i > 0)
            str += JSON_N_TOKENER;
        str += p->name->toString();
        str += JSON_N_NAMEVALUE_TOKENER + p->value->toString();
    }

    str += JSON_N_OBJECT_TAIL;

	return (int) str.size();
}

void JsonObject::debugPrint(int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;

    printf("%s%s%c\n", szIndent, szPrefix ? szPrefix:"NULL", JSON_N_OBJECT_HEAD);
    list<JSON_T_OBJECT_ELEMENT*>::iterator itr;
    int i = 1;
    for (itr = _value.begin(); itr != _value.end(); itr++, i++) {
        JSON_T_OBJECT_ELEMENT *p = *itr;
        if (i == _value.size())
            p->value->debugPrint(nIndent + 1, (p->name->toString() + "\t" JSON_S_NAMEVALUE_TOKENER).c_str());
        else
            p->value->debugPrint(nIndent + 1, (p->name->toString() + "\t" JSON_S_NAMEVALUE_TOKENER).c_str(), JSON_S_TOKENER);
    }
    printf("%s%c%s\n", szIndent, JSON_N_OBJECT_TAIL, szSuffix);

    JSON_T_OBJECT_VALUE o;
}

void JsonObject::debugPrint2(FILE* fp, int nIndent, const char *szPrefix, const char *szSuffix)
{
    char szIndent[100] = { '\t' };
    memset(szIndent, '\t', sizeof(szIndent));
    szIndent[nIndent] = 0;

    fprintf(fp, "%s%s%c", szIndent, szPrefix ? szPrefix:"NULL", JSON_N_OBJECT_HEAD);
    list<JSON_T_OBJECT_ELEMENT*>::iterator itr;
    int i = 1;
    for (itr = _value.begin(); itr != _value.end(); itr++, i++) {
        JSON_T_OBJECT_ELEMENT *p = *itr;
		if (p->value->getType() < JsonValue::TYPE_OBJECT) {
			if (i == _value.size())
				p->value->debugPrint2(fp, 0, (p->name->toString() + JSON_S_NAMEVALUE_TOKENER).c_str());
			else
				p->value->debugPrint2(fp, 0, (p->name->toString() + JSON_S_NAMEVALUE_TOKENER).c_str(), JSON_S_TOKENER);
		} else {
			if (i == _value.size())
				p->value->debugPrint2(fp, nIndent + 1, (p->name->toString() + JSON_S_NAMEVALUE_TOKENER).c_str());
			else
				p->value->debugPrint2(fp, nIndent + 1, (p->name->toString() + JSON_S_NAMEVALUE_TOKENER).c_str(), JSON_S_TOKENER);
		}
    }
    fprintf(fp, "%s%c%s\n", szIndent, JSON_N_OBJECT_TAIL, szSuffix);

    JSON_T_OBJECT_VALUE o;
}

JsonValue* JsonObject::get(const char *szName)
{
    list<JSON_T_OBJECT_ELEMENT*>::iterator itr;
    for (itr = _value.begin(); itr != _value.end(); itr++) {
        JSON_T_OBJECT_ELEMENT *p = *itr;
        if (strcmp(p->name->getString(), szName) == 0)
            return p->value;
    }
    return NULL;
}


// added by jhchoi 2011.12.02
JsonValue* JsonObject::pop(const char *szName)
{
	// get and remove only pointer

    list<JSON_T_OBJECT_ELEMENT*>::iterator itr;
    for (itr = _value.begin(); itr != _value.end(); itr++) {
        JSON_T_OBJECT_ELEMENT *p = *itr;
        if (strcmp(p->name->getString(), szName) == 0) {
			_value.erase(itr);
			JsonValue* retValue = p->value;
			delete p->name;
			delete p;
            return retValue;
		}
    }
    return NULL;
}

int JsonObject::add(const char *szName, JsonValue *pValue)
{
    JSON_T_OBJECT_ELEMENT *obj = new JSON_T_OBJECT_ELEMENT;
    obj->name  = new JsonString(szName);
    obj->value = pValue;
    _value.push_back(obj);
    return 1;
}

int JsonObject::add(const char *szName, const char *szValue)
{
    return add(szName, new JsonString(szValue));
}

int JsonObject::add(const char *szName, int nValue)
{
    return add(szName, new JsonNumber(nValue));
}

int JsonObject::add(const char *szName, unsigned int nValue)
{
    return add(szName, new JsonNumber(nValue));
}

JsonValueIntMap::JsonValueIntMap()
{
}

JsonValueIntMap::~JsonValueIntMap()
{
	Release(false);
}

void JsonValueIntMap::Release(bool deepRelease)
{
	JsonValueIntMap::iterator itr;
	for(itr = begin(); itr != end(); itr++) {
		JsonValue* value = itr->second;
		delete value;
	}
	clear();
}

JsonValueStringMap::JsonValueStringMap()
{
}

JsonValueStringMap::~JsonValueStringMap()
{
	Release(false);
}

void JsonValueStringMap::Release(bool deepRelease)
{
	JsonValueStringMap::iterator itr;
	for(itr = begin(); itr != end(); itr++) {
		JsonValue* value = itr->second;
		delete value;
	}
	clear();
}
