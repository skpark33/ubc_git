/*! \class TcpStreamServer
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_TCPSTREAMSERVER_H
#define PLAYER_BEAPOTAGENT_TCPSTREAMSERVER_H

#include <ci/libBase/ciBaseType.h>
#include <ci/libThread/ciThread.h>
#include <ci/libThread/ciSyncUtil.h>
#include "SocketBase.h"
#include "TcpStreamChannel.h"

class TcpStreamServer : public ciThread
{
public:
    TcpStreamServer(void);
    TcpStreamServer(ServiceAccessWorker* p_worker);

    virtual ~TcpStreamServer(void);

    void Start(int n_listen_port, string channel_type);
    void Stop();
    
	void run();	

protected:
    const char* CreateChannelId_();

protected:
    //ciTimer               timer_;      
    bool                    started_;
    int                     listen_port_;
    string                  channel_type_;

    SocketBase              socket_;
    TcpStreamChannel*       p_channel;
    ServiceAccessWorker*    p_sap_worker;
};

#endif //PLAYER_BEAPOTAGENT_TCPSTREAMSERVER_H