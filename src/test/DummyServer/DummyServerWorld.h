 /*! \file DummyServerWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief Dummy Tcp Socket Server World
 *
 *  \author detect8
 *  \version 1.0
 *  \date 2015/11/04 11:01:00
 */

#ifndef _DummyServerWorld_h_
#define _DummyServerWorld_h_


#include <ci/libWorld/ciWorld.h> 

class DummyServerWorld : public ciWorld {
public:
	DummyServerWorld();
	~DummyServerWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

    void    DummyServerStart();

protected:

};
		
#endif //_DummyServerWorld_h_
