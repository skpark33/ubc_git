//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by socketReceiver.rc
//
#define BT_SEND                         3
#define BT_CLEARALL                     4
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SOCKETRECEIVER_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_EDIT1                       1000
#define IDC_EDIT2                       1001
#define IDC_EDIT3                       1002
#define IDC_EDIT4                       1003
#define IDC_EDIT5                       1004
#define IDC_EDIT6                       1005
#define IDC_EDIT7                       1006
#define IDC_EDIT8                       1007
#define IDC_EDIT9                       1008
#define IDC_EDIT10                      1009
#define IDC_EDIT11                      1010
#define IDC_EDIT12                      1011
#define IDC_EDIT13                      1012
#define IDC_EDIT14                      1013
#define IDC_EDIT15                      1014
#define IDC_RADIO1                      1015
#define IDC_RADIO2                      1016
#define IDC_RADIO3                      1017
#define IDC_RADIO4                      1018
#define IDC_RADIO5                      1019
#define IDC_RADIO6                      1020
#define IDC_RADIO7                      1021
#define IDC_RADIO8                      1022
#define IDC_RADIO9                      1023
#define IDC_RADIO10                     1024
#define IDC_RADIO11                     1025
#define IDC_RADIO12                     1026
#define IDC_RADIO13                     1027
#define IDC_RADIO14                     1028
#define IDC_RADIO15                     1029
#define IDC_SHOW_EDIT1                  1030
#define IDC_SHOW_EDIT2                  1031
#define IDC_SHOW_EDIT3                  1032
#define IDC_CHECK1                      1033
#define IDC_CHECK2                      1034
#define IDC_CHECK3                      1035
#define IDC_BUTTON2                     1037
#define IDC_SET                         1037

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1038
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
