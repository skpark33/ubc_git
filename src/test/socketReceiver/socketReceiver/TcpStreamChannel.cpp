#include "stdafx.h"
//#include <ci/libDebug/ciDebug.h>
//#include "ci/libDebug/ciArgParser.h"

#include "TcpStreamChannel.h"

//ciSET_DEBUG(3, "TcpStreamChannel");

TcpStreamChannel::TcpStreamChannel()
{
    //ciDEBUG(3, ("TcpStreamChannel()"));
    p_sap_worker_ = NULL;
    channel_type_ = CONNECT_TCP_ONETRANSACTION_CHANNEL;
}


TcpStreamChannel::~TcpStreamChannel(void)
{
    //ciDEBUG(3, ("~TcpStreamChannel()"));
 
}


int TcpStreamChannel::StartConnect(string str_ip, int port, string channel_type)
{
    //ciDEBUG(3, ("StartConnect(%s, %d)\n", str_ip.c_str(), port));

    channel_type_ = channel_type;

    int ret = socket_.Create(0); //SOCKET_BASE_TCP = 0
    if (ret < 1 ) {
        //ciDEBUG(3, ("StartConnect Create Failed\n"));
        return -1;
    }
    //if (socket_.SetNonblockMode() < 0) {
    //    //ciDEBUG(3, ("SetNonblockMode  Failed\n"));
    //    Close();
    //    return -2;
    //}
    ret = socket_.Connect(socket_.GetFD(), str_ip.c_str(), port);
    if (ret < 0 ) {
        //ciDEBUG(3, ("StartConnect Connect Failed\n"));
        return -3;
    }
    //ciDEBUG(3, ("StartConnect Connect Success!!!\n"));
    return 1;
}


int TcpStreamChannel::SendMessage(const char* p_send_msg, int length)
{
    //ciDEBUG(3, ("SendMessage()"));
    return socket_.Sendn(p_send_msg, length);
}

void TcpStreamChannel::Close()
{
    //ciDEBUG(3, ("Close()"));
    
    SetLingerMode(true);
    socket_.Close();
}

const char* TcpStreamChannel::GetChannelType()
{
    return channel_type_.c_str();
}

const char* TcpStreamChannel::GetChannelId()
{
    return channel_id_.c_str();
}

//////////////////////////////////////////////////
// 
void TcpStreamChannel::SendConnectAck_()
{
    //ciDEBUG(3, ("SendConnectAck_()"));
    
    //[ 향후 Protocol 처리에서 Connect Ack가 필요한 경우만 보내도록 이 블럭에 코딩한다.

    //]

    // 현재는 하드코딩
    if (socket_.GetFD() > 0) {
        // server에서 생성시 channel을 close 한다.

        string str_ack("~AGNTTEXT0000000002CHANNELACK");
        char send_buf[50];
        int  send_ret = 0;
        memset(send_buf, 0x00, 50);
        sprintf(send_buf, "%s", str_ack.c_str());        

        send_ret = socket_.Send(send_buf, (int) strlen(send_buf));
        if (send_ret > 0) {
            //ciDEBUG(3, ("SendConnectAck_ success [%s]", str_ack.c_str()));
        } else {
            //ciDEBUG(3, ("SendConnectAck_ failed! [%s]", str_ack.c_str()));
            Close();
        }   
    }
}

void TcpStreamChannel::SendSyncResponse_()
{
    //ciDEBUG(3, ("SendSyncResponse_()"));
    
    //[ 향후 Protocol 처리에서 Connect Ack가 필요한 경우만 보내도록 이 블럭에 코딩한다.

    //]

    // 현재는 하드코딩
    if (socket_.GetFD() > 0) {
        // server에서 생성시 channel을 close 한다.

        string str_ack("~AGNTTEXT0000000002CHANNELACK");
        char send_buf[50];
        int  send_ret = 0;
        memset(send_buf, 0x00, 50);
        sprintf(send_buf, "%s", str_ack.c_str());        

        send_ret = socket_.Send(send_buf, (int) strlen(send_buf));
        if (send_ret > 0) {
            //ciDEBUG(3, ("SendConnectAck_ success [%s]", str_ack.c_str()));
        } else {
            //ciDEBUG(3, ("SendConnectAck_ failed! [%s]", str_ack.c_str()));
            Close();
        }

        RecvStream_();
        Close();
    }
}

// 향후 Protocol 객체 적용하여 모든 프레임에 대한 Stream을 송수신하도록 수정
int TcpStreamChannel::RecvStream_()
{
    //ciDEBUG(3, ("RecvPacket_()"));

    string str_recv_msg("");    
    char buf[4096];
    char* p_buf = buf;
    memset(buf, 0x00, 4096);
    int n_recv = socket_.Recv(buf, 4096);

    if (n_recv > 1) {
        // recv ok
        if (channel_type_ == ACCEPT_TCP_DUMMY_CHANNEL) {
            //ciWARN2( ("Recv\n[\n%s\n]", buf ));
        } else {
            //ciDEBUG2(1, ("Recv\n[\n%s\n]", buf));
        }
        
        // Header 검증 후 Msg 생성
        str_recv_msg = p_buf + 20;

        // Packet Header 떼고 Msg만 ServiceAccessPoint로 전송
        // SAP 가 NULL이면 전송하지 않는다.
        if (p_sap_worker_) {
            p_sap_worker_->RecvFromChannel(str_recv_msg);
        }
        return n_recv;
        
    } else {
        // recv error socket broken!
        //ciDEBUG(3, ("Recv error socket broken! channel_id[%s]", channel_id_.c_str()));
        Close();
        return -1;
    }

    return 0;
}


bool TcpStreamChannel::SetLingerMode(bool Mode) 
{
    //ciDEBUG(3, ("SetLingerMode(%d) ", Mode));

    int flag;
    linger lingval;

    if (socket_.GetFD() == -1) {
        return false;
    }

    lingval.l_onoff = Mode ? 1 : 0;
    lingval.l_linger = 0;

    if (setsockopt(socket_.GetFD(), SOL_SOCKET, SO_LINGER, (char*) &lingval, sizeof (lingval)) < 0) {
        //ciERROR(("can't set SO_LINGER socket option!!!"));
        return false;
    }

    return true;
}