#include "stdafx.h"
#include "SocketBase.h"

#include <stdio.h>
#include <string.h>
#ifndef _WIN32
    #include <unistd.h>
    #include <fcntl.h>
    #include <sys/time.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <errno.h>
    #include <stdarg.h>
    #define GetLastError() errno
    #define WSAGetLastError() errno
    #define WSAEINTR        EINTR
    #define WSAEINPROGRESS  EINPROGRESS
    #define WSAEWOULDBLOCK  EWOULDBLOCK
#else
    #include <winsock2.h>
#endif

#define DEF_NONBLOCK_EXPERIMENT

#ifdef _WIN32

class WindowsSocketInitializer {
public:
    WindowsSocketInitializer()
	{
		WORD    wVersionRequested;
		WSADATA wsaData;
		wVersionRequested = MAKEWORD( 2, 2 );
		WSAStartup(wVersionRequested, &wsaData);
	}

    ~WindowsSocketInitializer()
	{
		WSACleanup();
	}
};

static WindowsSocketInitializer s_sockInitializer;

#endif

///////////////////////////////////////////////////////////////////////
//
// SocketBase implementations
//
//ciSET_DEBUG(3, "SocketBase");

SocketBase::SocketBase()
{
    //ciDEBUG(5, ("SocketBase()"));    

    _nFD = -1;
}

SocketBase::~SocketBase()
{
    //ciDEBUG(5, ("~SocketBase()"));    

    Close();
}

int SocketBase::Connect(int nFD, const char *szIP, int nPort)
{
    //ciDEBUG(5, ("~Connect(%d, %s, %d)", nFD, szIP, nPort));    

	struct sockaddr_in sockAddr;
	memset(&sockAddr, 0, sizeof(sockAddr));
	sockAddr.sin_family      = AF_INET;
	sockAddr.sin_addr.s_addr = inet_addr(szIP);
	sockAddr.sin_port        = htons((u_short) nPort);
    
    if (::connect(nFD, (struct sockaddr*) &sockAddr, sizeof(sockAddr)) < 0) {        
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }

    return 0;
}

int SocketBase::Open(const char *szIP, int nPort, int nMaxConnectRetry, int nConnectWaitMillisec, const char *szMyIP, int nMyPort, int bSetReuse)
{
    //ciDEBUG(5, ("Open(%s, %d, %d, %d)", szIP, nPort, nMaxConnectRetry, nConnectWaitMillisec));

    Close();

	struct sockaddr_in sockAddr;
	memset(&sockAddr, 0, sizeof(sockAddr));
	sockAddr.sin_family      = AF_INET;
	sockAddr.sin_addr.s_addr = inet_addr(szIP);
	sockAddr.sin_port        = htons((u_short) nPort);

    if (nMaxConnectRetry < 1) {
        _nFD = (int) ::socket(AF_INET, SOCK_STREAM, 0);
        if (_nFD < 0) {
            //ciERROR(("%d", WSAGetLastError()));
            return -1;
        }

        if (bSetReuse)
            SetReuse(1);

        if (szMyIP && szMyIP[0] || nMyPort > 0) {
            if (Bind(_nFD, szMyIP, nMyPort) < 0) {
                //ciERROR(("%d", WSAGetLastError()));
                Close();
                return -1;
            }
        }

        if (::connect(_nFD, (struct sockaddr*) &sockAddr, sizeof(sockAddr)) < 0) {
            //ciERROR(("%d", WSAGetLastError()));
            Close();
            return -1;
        }

        return 0;
    }

    for (int nRetry = 0; nRetry < nMaxConnectRetry; nRetry++) {
        _nFD = (int) ::socket(AF_INET, SOCK_STREAM, 0);
        if (_nFD < 0) {
            //ciERROR(("%d", WSAGetLastError()));
            continue;
        }

        if (szMyIP) {
            if (szMyIP[0]) {
                if (Bind(_nFD, szMyIP, 0) < 0) {
                    Close();
                    continue;
                }
            }
        }

        if (nConnectWaitMillisec < 0) {
            if (::connect(_nFD, (struct sockaddr*) &sockAddr, sizeof(sockAddr)) < 0) {
                //ciERROR(("%d", WSAGetLastError()));
                Close();
                continue;
            }

            return 0;
        }

        if (SetNonblockMode() < 0) {
            Close();
            continue;
        }
        
        if (::connect(_nFD, (struct sockaddr*) &sockAddr, sizeof(sockAddr)) >= 0) {
            SetBlockMode();
            return 0;
        }

        int nErr = WSAGetLastError();
        if (nErr != WSAEINPROGRESS && nErr != WSAEWOULDBLOCK && nErr != 0) {
            //ciERROR(("%d", nErr));
            Close();
            continue;
        }

        {
	        struct timeval timeWait;
	        fd_set rfds, wfds;
            int nSelect;

	        timeWait.tv_sec  = nConnectWaitMillisec / 1000;
	        timeWait.tv_usec = (nConnectWaitMillisec % 1000) * 1000;

	        FD_ZERO(&rfds);
	        FD_ZERO(&wfds);
	        FD_SET(_nFD, &rfds);
	        FD_SET(_nFD, &wfds);

            while (1) {
#ifdef _WIN32   /* the completion of the connection request by checking to see if the socket is writeable */
                nSelect = select(_nFD + 1, NULL, &wfds, NULL, &timeWait);
                if (nSelect > 0) {
                    SetBlockMode();
                    return 0;
                }
#else
                nSelect = select(_nFD + 1, &rfds, &wfds, NULL, &timeWait);
#ifdef LINUX
                if (nSelect > 0) {
                    SetBlockMode();
                    return 0;
                }
#else
                if (nSelect > 0)
                    break;
#endif
#endif
                if (nSelect < 0) {
                    if (WSAGetLastError() == WSAEINTR)
                        continue;

                    //ciERROR(("%d", WSAGetLastError()));
                    break;
                }

                if (nSelect == 0)
                    //ciDEBUG(1, ("Open >> select timeout!!"));
                break;
            }

            if (nSelect <= 0) {
                Close();
                continue;
            }

            {
                char buf[10];
                if (::recv(_nFD, buf, 0, 0) >= 0) {
                    SetBlockMode();
                    return 0;
                }

                //ciERROR(("%d", WSAGetLastError()));
                Close();
                continue;
            }
        }
    }
    return -1;
}

int SocketBase::Listen(int nPort, int backlog, const char *szBindIP)
{
    //ciDEBUG(5, ("Listen(port = %d) backlog = %d, bind ip = %s", nPort, backlog, szBindIP ? szBindIP : ""));

    Close();

    _nFD = (int) ::socket(AF_INET, SOCK_STREAM, 0);
    if (_nFD < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }

    SetReuse(1);

    if (Bind(_nFD, szBindIP, nPort) < 0) {
        Close();
        return -2;
    }
    if (::listen(_nFD, backlog) < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        Close();
        return -3;
    }

    return 1;
}

int SocketBase::Accept(char *szClientIP, int *pClientPort)
{
    //ciDEBUG(5, ("Accept"));

    struct sockaddr_in addrCli;
#if (defined(__SOCKLEN_T) || defined(SOCKLEN_T) || defined(__socklen_t_defined))
    socklen_t nLen = sizeof(addrCli);
#else
    int nLen = sizeof(addrCli);
#endif
    memset(&addrCli, 0x00, nLen);
    SetNonblockMode();
    int nSocket = (int) ::accept(_nFD, (struct sockaddr*) &addrCli, &nLen);
	if (nSocket < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        Close();
        return -1;
	}
    SetBlockMode();
    
    unsigned char* pIP   = (unsigned char*) &addrCli.sin_addr;
    int            nPort = ::ntohs(addrCli.sin_port);
    char szIP[100];
    sprintf(szIP, "%d.%d.%d.%d", pIP[0], pIP[1], pIP[2], pIP[3]);
    if (szClientIP)
        strcpy(szClientIP, szIP);
    if (pClientPort)
        *pClientPort = nPort;

    //ciDEBUG(5, ("Accept >> new client %s:%d fd=%d", szIP, nPort, nSocket));

    return nSocket;
}

int SocketBase::Send(const char *pData, int nLen)
{
    //ciDEBUG(5, ("Send(len = %d), fd = %d", nLen, _nFD));

    if (_nFD < 0) {
        //ciERROR(("Send >> invalid socket"));
        return -1;
    }
    if (nLen < 1) {
        //ciERROR(("Sendn >> no data to send, Datalen = %d", nLen));
        return 0;
    }

    int nSend = ::send(_nFD, pData, nLen, 0);
    if (nSend < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
    //ciDEBUG(10, ("Send >> send len = %d", nSend));
    //TraceHex(20, pData, nSend);

    return nSend;
}

int SocketBase::Sendn(const char *pData, int nLen)
{
    //ciDEBUG(5, ("Sendn(len = %d), fd = %d", nLen, _nFD));

    if (_nFD < 0) {
        //ciERROR(("Sendn >> invalid socket"));
        return -1;
    }
    if (nLen < 1) {
        //ciERROR(("Sendn >> no data to send, Datalen = %d", nLen));
        return 0;
    }

    int         nRemain = nLen ;
    const char *p       = pData;
    while (nRemain > 0) {
        int nSend = ::send(_nFD, p, nRemain, 0);
        if (nSend < 0) {
            //ciERROR(("%d", WSAGetLastError()));
            break;
        }
        nRemain -= nSend;
        p       += nSend;
    }
    //ciDEBUG(10, ("Sendn >> send len = %d", nLen - nRemain));
    //TraceHex(20, pData, nLen - nRemain);

    return (nLen - nRemain);
}

int SocketBase::Sendn(const char *pData, int nLen, int nWaitMilliSec)
{
    //ciDEBUG(5, ("Sendn(len = %d), fd = %d", nLen, _nFD));

    if (_nFD < 0) {
        //ciERROR(("Sendn >> invalid socket"));
        return -1;
    }
    if (nLen < 1) {
        //ciERROR(("Sendn >> no data to send"));
        return 0;
    }

    int         nRemain = nLen ;
    const char *p       = pData;
    while (nRemain > 0) {
        if (WaitSend(nWaitMilliSec) <= 0)
            break;

        int nSend = ::send(_nFD, p, nRemain, 0);
        if (nSend < 0) {
            //ciERROR(("%d", WSAGetLastError()));
            break;
        }
        nRemain -= nSend;
        p       += nSend;
    }
    //ciDEBUG(10, ("Sendn >> send len = %d", nLen - nRemain));
    //TraceHex(20, pData, nLen - nRemain);

    return (nLen - nRemain);
}

int SocketBase::Recv(char *buffer, int nSize)
{
    //ciDEBUG(5, ("Recv >> fd = %d", _nFD));
    if (_nFD < 0) {
        //ciERROR(("_nFD < 0 [%d]", _nFD));
        return -1;
    }

    int nRecv = ::recv(_nFD, buffer, nSize, 0);
    if (nRecv < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
    if (nRecv == 0) {
        //ciERROR(("lost connection fd = %d", _nFD));
        return -1;
    }
    //ciDEBUG(5, ("Recv >> recv len = %d", nRecv));
    //TraceHex(20, buffer, nRecv);

    return nRecv;
}

int SocketBase::Recvn(char *buffer, int nSize)
{
    //ciDEBUG(5, ("Recvn >> fd = %d", _nFD));
    if (_nFD < 0) {
        //ciERROR(("_nFD < 0 [%d]", _nFD));
        return -1;
    }

    int   nRemain = nSize ;
    char *p       = buffer;
    while (nRemain > 0) {
        int nRecv = ::recv(_nFD, p, nRemain, 0);
        if (nRecv < 0) {
            //ciERROR(("%d", WSAGetLastError()));
            break;
        }
        if (nRecv == 0) {
            //ciERROR(("lost connection fd = %d", _nFD));
            break;
        }
        nRemain -= nRecv;
        p       += nRecv;
    }
    //ciDEBUG(5, ("Recv >> recv len = %d", nSize - nRemain));
    //TraceHex(20, buffer, nSize - nRemain);

    return (nSize - nRemain);
}

int SocketBase::Recvn(char *buffer, int nSize, int nWaitMilliSec)
{
    if (_nFD < 0) {
        //ciERROR(("_nFD < 0 [%d]", _nFD));
        return -1;
    }

    int   nRemain = nSize ;
    char *p       = buffer;
    while (nRemain > 0) {
        if (WaitRecv(nWaitMilliSec) <= 0)
            break;

        int nRecv = ::recv(_nFD, p, nRemain, 0);
        if (nRecv < 0) {
            //ciERROR(("%d", WSAGetLastError()));
            break;
        }
        if (nRecv == 0) {
            //ciERROR(("lost connection fd = %d", _nFD));
            break;
        }
        nRemain -= nRecv;
        p       += nRecv;
    }
    //ciDEBUG(5, ("Recv >> recv len = %d", nSize - nRemain));
    //TraceHex(20, buffer, nSize - nRemain);

    return (nSize - nRemain);
}

int SocketBase::WaitRecv(int nWaitMilliSec)
{
    //ciDEBUG(5, ("WaitRecv 1 >> fd = %d", _nFD));
    if (_nFD < 0) {
        //ciERROR(("_nFD < 0 [%d]", _nFD));
        return -1;
    }

	struct timeval timeWait;
	timeWait.tv_sec  = nWaitMilliSec / 1000;
    timeWait.tv_usec = (nWaitMilliSec % 1000) * 1000;

	fd_set read_fds;
	FD_ZERO(&read_fds);
    FD_SET(_nFD, &read_fds);

	int nSelect = ::select(_nFD + 1, &read_fds, NULL, NULL, &timeWait);
    if (nSelect < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }

    //ciDEBUG(5, ("WaitRecv >> fd = %d, select result = %d", _nFD, nSelect));

    return nSelect;
}

int SocketBase::WaitRecv(int nWaitMilliSec, int nFD1, ...)
{
	fd_set fds;
	FD_ZERO(&fds);
	int nMaxFD = -1;
	
    va_list argList;
    va_start(argList, nFD1);
    int nFD = nFD1;
    while (nFD) {
		if (nFD > 0) {
			FD_SET(nFD, &fds);
			if (nFD > nMaxFD)
				nMaxFD = nFD;
		}
        nFD = va_arg(argList, int);
    }
    va_end(argList);

    //ciDEBUG(5, ("WaitRecv 2 >> max fd = %d", nMaxFD));

    if (nMaxFD < 0) {
        //ciERROR(("nMaxFD < 0 [%d]", nMaxFD));
        return -1;
    }

	struct timeval timeWait;
	timeWait.tv_sec  = nWaitMilliSec / 1000;
    timeWait.tv_usec = (nWaitMilliSec % 1000) * 1000;

	int nSelect = ::select(nMaxFD + 1, &fds, NULL, NULL, &timeWait);
    if (nSelect < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }

    ////ciDEBUG(5, ("WaitRecv >> max fd = %d, select result = %d", nMaxFD, nSelect);

    return nSelect;
}

int SocketBase::WaitSend(int nWaitMilliSec)
{
    //ciDEBUG(5, ("WaitSend >> fd = %d", _nFD));
    if (_nFD < 0) {
        //ciERROR(("_nFD < 0 [%d]", _nFD));
        return -1;
    }

	struct timeval timeWait;
	timeWait.tv_sec  = nWaitMilliSec / 1000;
    timeWait.tv_usec = (nWaitMilliSec % 1000) * 1000;

	fd_set fds;
	FD_ZERO(&fds);
    FD_SET(_nFD, &fds);

	int nSelect = ::select(_nFD + 1, NULL, &fds, NULL, &timeWait);
    if (nSelect < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }

    //ciDEBUG(5, ("WaitSend >> fd = %d, select result = %d", _nFD, nSelect));

    return nSelect;
}

void SocketBase::SetReuse(int nReuse)
{
#ifdef SO_REUSEPORT
    setsockopt(_nFD, SOL_SOCKET, SO_REUSEPORT, (const char*) &nReuse, sizeof(int));
#endif
#ifdef SO_REUSEADDR
    #ifdef _WIN32
        setsockopt(_nFD, SOL_SOCKET, SO_REUSEADDR, (const char*) &nReuse, sizeof(int));
    #else
        setsockopt(_nFD, SOL_SOCKET, SO_REUSEADDR, &nReuse, sizeof(int));
    #endif
#endif
}

int SocketBase::SetNonblockMode()
{
    //ciDEBUG(5, ("SetNonblockMode >> fd = %d", _nFD));
#ifdef _WIN32
#ifdef DEF_NONBLOCK_EXPERIMENT
    u_long nFlag = 1;
    if (ioctlsocket(_nFD, FIONBIO, &nFlag) != 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
#else
    BOOL bFlag = TRUE;
    setsockopt(_nFD, IPPROTO_TCP, TCP_NODELAY, (const char*) &bFlag, sizeof(bFlag));
#endif
#else
    int nFlag;
    if ((nFlag = fcntl(_nFD, F_GETFL, 0)) < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }

    nFlag &= ~O_NDELAY;
    nFlag |= O_NONBLOCK;

    if ((nFlag = fcntl(_nFD, F_SETFL, nFlag)) < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
#endif

    return 0;
}

int SocketBase::SetBlockMode()
{
    //ciDEBUG(5, ("SocketBase::setBlockMode >> fd = %d", _nFD));
#ifdef _WIN32
#ifdef DEF_NONBLOCK_EXPERIMENT
    u_long nFlag = 0;
    if (ioctlsocket(_nFD, FIONBIO, &nFlag) != 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
#else
    BOOL bFlag = FALSE;
    setsockopt(_nFD, IPPROTO_TCP, TCP_NODELAY, (const char*) &bFlag, sizeof(bFlag));
#endif
#else
    int nFlag;
    if ((nFlag = fcntl(_nFD, F_GETFL, 0)) < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }

    nFlag &= ~(O_NONBLOCK | O_NDELAY);

    if ((nFlag = fcntl(_nFD, F_SETFL, nFlag)) < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
#endif

    return 0;
}

void SocketBase::Close()
{
    //ciDEBUG(5, ("Close"));
    if (_nFD >= 0) {
        //ciDEBUG(5, ("Close fd = %d", _nFD));

        Close(_nFD);
        _nFD = -1;
    }
}

void SocketBase::Close(int nFD)
{
#ifdef _WIN32
    ::shutdown(nFD, SD_BOTH);
    ::closesocket(nFD);
#else
    ::shutdown(nFD, SHUT_RDWR);
    ::close(nFD);
#endif
}

int SocketBase::Bind(const char *szIP, int nPort)
{
    //ciDEBUG(5, ("Bind(%s, %d)", szIP, nPort));
    return Bind(_nFD, szIP, nPort);
}

int SocketBase::Bind(int nFD, const char *szIP, int nPort)
{
    //ciDEBUG(5, ("Bind(%d, %s, %d)", nFD, szIP, nPort));

    struct sockaddr_in sockAddr;
    memset(&sockAddr, 0, sizeof(sockAddr));
    if (szIP && szIP[0]) {
        sockAddr.sin_family      = AF_INET;
        sockAddr.sin_addr.s_addr = inet_addr(szIP);
        sockAddr.sin_port        = htons((u_short) nPort);
    }
    else {
        sockAddr.sin_family      = AF_INET;
	    sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	    sockAddr.sin_port        = htons((u_short) nPort);
    }

    if (::bind(nFD, (struct sockaddr*) &sockAddr, sizeof(sockAddr)) < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        //ciERROR(("Bind >> fail!! fd = %d, ip = %s, port = %d", nFD, szIP, nPort));
        return -1;
    }

    return 0;
}

int SocketBase::Create(int nType)
{
    //ciDEBUG(5, ("Create(%d)", nType));

    if (_nFD >= 0) {
        //ciERROR(("_nFD < 0 [%d]", _nFD));
        return 0;
    }

    if (nType == SOCKET_BASE_TCP)
        _nFD = (int) ::socket(AF_INET, SOCK_STREAM, 0);
    else if (nType == SOCKET_BASE_UDP)
        _nFD = (int) ::socket(AF_INET, SOCK_DGRAM, 0);
    else {
        //ciERROR(("Unknown Socket Type [%d]", nType));
        return -1;
    }
    if (_nFD < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
    return 1;
}

int SocketBase::SendTo(const char *szIP, int nPort, const char *pData, int nLen)
{
    //ciDEBUG(5, ("SendTo(%s:%d len = %d), fd = %d", szIP, nPort, nLen, _nFD));

    if (_nFD < 0) {
        //ciERROR(("SendTo >> invalid socket"));
        return -1;
    }
    if (nLen < 1) {
        //ciERROR(("Sendto >> no data to send"));
        return 0;
    }

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family      = AF_INET;
	addr.sin_addr.s_addr = inet_addr(szIP);
    addr.sin_port        = htons((u_short) nPort);

    int nSend = ::sendto(_nFD, pData, nLen, 0, (struct sockaddr*) &addr, sizeof(addr));
    if (nSend < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
    //ciDEBUG(5, ("SendTo >> send len = %d", nSend));
    //TraceHex(20, pData, nSend);

    return nSend;
}

int SocketBase::RecvFrom(char *buffer, int nSize, char *szPeerIP, int *pPeerPort)
{
    //ciDEBUG(5, ("RecvFrom >> fd = %d", _nFD));
    if (_nFD < 0) {
        //ciERROR(("RecvFrom >> invalid socket"));
        return -1;
    }

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
#if (defined(__SOCKLEN_T) || defined(SOCKLEN_T) || defined(__socklen_t_defined))
    socklen_t nAddrlen = sizeof(addr);
#else
    int nAddrlen = sizeof(addr);
#endif
    int nRecv = ::recvfrom(_nFD, buffer, nSize, 0, (struct sockaddr*) &addr, &nAddrlen);
    if (nRecv < 0) {
        //ciERROR(("%d", WSAGetLastError()));
        return -1;
    }
    if (nRecv == 0) {
        //ciERROR(("lost connection fd = %d", _nFD));
        return -1;
    }
    //ciDEBUG(5, ("RecvFrom >> recv len = %d", nRecv));
    //TraceHex(20, buffer, nRecv);
    if (szPeerIP) {
        unsigned char* pIP = (unsigned char*) &addr.sin_addr;
        sprintf(szPeerIP, "%d.%d.%d.%d", pIP[0], pIP[1], pIP[2], pIP[3]);
    }
    if (pPeerPort) {
        *pPeerPort = ::ntohs(addr.sin_port);
    }

    return nRecv;
}

