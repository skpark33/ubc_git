// socketReceiverDlg.cpp : 구현 파일
//
#include "stdafx.h"
#include "JsonBase.h"

#include "socketReceiver.h"
#include "socketReceiverDlg.h"
#include "winsock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CsocketReceiverDlg 대화 상자




CsocketReceiverDlg::CsocketReceiverDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CsocketReceiverDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CsocketReceiverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit[0]);
	DDX_Control(pDX, IDC_EDIT2, m_edit[1]);
	DDX_Control(pDX, IDC_EDIT3, m_edit[2]);
	DDX_Control(pDX, IDC_EDIT4, m_edit[3]);
	DDX_Control(pDX, IDC_EDIT5, m_edit[4]);
	DDX_Control(pDX, IDC_EDIT6, m_edit[5]);
	DDX_Control(pDX, IDC_EDIT7, m_edit[6]);
	DDX_Control(pDX, IDC_EDIT8, m_edit[7]);
	DDX_Control(pDX, IDC_EDIT9, m_edit[8]);
	DDX_Control(pDX, IDC_EDIT10, m_edit[9]);
	DDX_Control(pDX, IDC_EDIT11, m_edit[10]);
	DDX_Control(pDX, IDC_EDIT12, m_edit[11]);
	DDX_Control(pDX, IDC_EDIT13, m_edit[12]);
	DDX_Control(pDX, IDC_EDIT14, m_edit[13]);
	DDX_Control(pDX, IDC_EDIT15, m_edit[14]);

	DDX_Control(pDX, IDC_RADIO1, m_radio[0]);
	DDX_Control(pDX, IDC_RADIO2, m_radio[1]);
	DDX_Control(pDX, IDC_RADIO3, m_radio[2]);
	DDX_Control(pDX, IDC_RADIO4, m_radio[3]);
	DDX_Control(pDX, IDC_RADIO5, m_radio[4]);
	DDX_Control(pDX, IDC_RADIO6, m_radio[5]);
	DDX_Control(pDX, IDC_RADIO7, m_radio[6]);
	DDX_Control(pDX, IDC_RADIO8, m_radio[7]);
	DDX_Control(pDX, IDC_RADIO9, m_radio[8]);
	DDX_Control(pDX, IDC_RADIO10, m_radio[9]);
	DDX_Control(pDX, IDC_RADIO11, m_radio[10]);
	DDX_Control(pDX, IDC_RADIO12, m_radio[11]);
	DDX_Control(pDX, IDC_RADIO13, m_radio[12]);
	DDX_Control(pDX, IDC_RADIO14, m_radio[13]);
	DDX_Control(pDX, IDC_RADIO15, m_radio[14]);

	DDX_Control(pDX, IDC_SHOW_EDIT1, m_showEdit[0]);
	DDX_Control(pDX, IDC_SHOW_EDIT2, m_showEdit[1]);
	DDX_Control(pDX, IDC_SHOW_EDIT3, m_showEdit[2]);

	DDX_Control(pDX, IDC_CHECK1, m_check[0]);
	DDX_Control(pDX, IDC_CHECK2, m_check[1]);
	DDX_Control(pDX, IDC_CHECK3, m_check[2]);

}

BEGIN_MESSAGE_MAP(CsocketReceiverDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CsocketReceiverDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CsocketReceiverDlg::OnBnClickedCancel)

	ON_BN_CLICKED(BT_SEND, &CsocketReceiverDlg::OnBnClickedSend)
	ON_BN_CLICKED(BT_CLEARALL, &CsocketReceiverDlg::OnBnClickedClearall)
	ON_BN_CLICKED(IDC_SET, &CsocketReceiverDlg::OnBnClickedSet)
END_MESSAGE_MAP()


// CsocketReceiverDlg 메시지 처리기

BOOL CsocketReceiverDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	//

	loadData();



	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CsocketReceiverDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CsocketReceiverDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CsocketReceiverDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CsocketReceiverDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnCancel();
}

void CsocketReceiverDlg::loadData()
{
	TCHAR cBuffer[1024] = { 0x00 };

	CString ini = "C:\\SQISoft\\utv1.0\\execute\\data\\hanger.ini";

	for(int i=0;i<15;i++) {
		SimulInfo *ele = new SimulInfo;

		char buf[10];
		sprintf(buf,"hanger_%d",i+1);

		memset(cBuffer,0x00,1024);
		GetPrivateProfileString(buf, "beaconId", _T(""), cBuffer, 1024, ini);
		ele->beaconId = cBuffer;

		memset(cBuffer,0x00,1024);
		GetPrivateProfileString(buf, "goodsId", _T(""), cBuffer, 1024, ini);
		m_edit[i].SetWindowText(cBuffer);
		ele->goodsId = cBuffer;

		memset(cBuffer,0x00,1024);
		GetPrivateProfileString(buf, "NAME", _T(""), cBuffer, 1024, ini);
		ele->NAME = cBuffer;

		memset(cBuffer,0x00,1024);
		GetPrivateProfileString(buf, "PRICE", _T(""), cBuffer, 1024, ini);
		ele->PRICE = cBuffer;

		memset(cBuffer,0x00,1024);
		GetPrivateProfileString(buf, "SIZE", _T(""), cBuffer, 1024, ini);
		ele->SIZE = cBuffer;

		memset(cBuffer,0x00,1024);
		GetPrivateProfileString(buf, "COLOR", _T(""), cBuffer, 1024, ini);
		ele->COLOR = cBuffer;

		memset(cBuffer,0x00,1024);
		GetPrivateProfileString(buf, "COUNT", _T(""), cBuffer, 1024, ini);
		ele->COUNT = cBuffer;

		_dataVector.push_back(ele);
	}
	for(int k=0;k<3;k++) {
		selectedVector[k] = -1;
	}

	//this->MessageBox("Load data end");

    // [CONNECT] FLASH_IP, FLASH_PORT
	memset(cBuffer,0x00,1024);
	GetPrivateProfileString("CONNECT", "FLASH_IP", _T(""), cBuffer, 1024, ini);
	flash_ip = cBuffer;

	memset(cBuffer,0x00,1024);
	GetPrivateProfileString("CONNECT", "FLASH_PORT", _T(""), cBuffer, 1024, ini);
	flash_port = atoi(cBuffer);

}


void CsocketReceiverDlg::OnBnClickedSend()
{
    goodsInfoList goods_info_list;
    goodsInfo goods_info;
    std::string send_xml_msg;

	for(int k=0;k<3;k++) {
		int m = selectedVector[k];

        CString EditText;
        m_showEdit[k].GetWindowText(EditText);
        std::string str_show_edit = (char*) EditText.GetBuffer();           

		if(m >= 0) {	
            goods_info.beaconId = LPSTR(LPCTSTR(_dataVector[m]->beaconId)) ;
            goods_info.goodsId  = LPSTR(LPCTSTR(_dataVector[m]->goodsId));
            goods_info.NAME     = LPSTR(LPCTSTR(_dataVector[m]->NAME));
            goods_info.PRICE    = LPSTR(LPCTSTR(_dataVector[m]->PRICE));
            goods_info.SIZE     = LPSTR(LPCTSTR(_dataVector[m]->SIZE));
            goods_info.COLOR    = LPSTR(LPCTSTR(_dataVector[m]->COLOR));
            goods_info.COUNT    = LPSTR(LPCTSTR(_dataVector[m]->COUNT));
        } else {
            goods_info.goodsId  = "NULL";
        }
        goods_info_list.push_back(goods_info);
	}
    send_xml_msg = CreateXmlMsg(goods_info_list);
    SendMessageFlash(send_xml_msg);
}

void CsocketReceiverDlg::OnBnClickedOk()
{
	// TODO: Clear();
	bool  tempVector[3];
	for(int i=0;i<3;i++){
		if(m_check[i].GetCheck()) {	
			m_showEdit[i].SetWindowTextA(_T(""));
			tempVector[i] = true;
		}else{
			tempVector[i] = false;
		}
	}

	for(int k=0;k<3;k++) {
		if(tempVector[k]){;
			int m = selectedVector[k];
			if(m >= 0) {	
				selectedVector[k] = -1;
			}
		}
	}

}


void CsocketReceiverDlg::OnBnClickedClearall()
{
	for(int i=0;i<3;i++){
		m_showEdit[i].SetWindowTextA(_T(""));

	}
	for(int k=0;k<3;k++) {
		int m = selectedVector[k];
		if(m >= 0) {	
			selectedVector[k] = -1;
		}
	}

}


void
CsocketReceiverDlg::createMsg(CString& beaconId, int x, int y, int z, CString& outMessage)
{
	outMessage = MakeBeaconPacket(beaconId,x,y,z).c_str();
}


#   define VERSION_MAJOR         2
#   define VERSION_MINOR         0

bool
CsocketReceiverDlg::sendMsg(CString& msg)
{
	if(msg.IsEmpty()){
		return false;
	}

 	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA       WsaData;            
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {		
		WSACleanup();	
		return false;
		this->MessageBox("socket fail 1");
	}

	SOCKET socket_fd;		
	if ((socket_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		return false;
		this->MessageBox("socket fail 2");
	}

	struct sockaddr_in server_addr;	
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr("192.168.0.35");
	server_addr.sin_port = htons(10000);
	
	if (connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
		closesocket(socket_fd);
		WSACleanup();	
		this->MessageBox("socket fail 3");
		return false;
	}
	
	int msg_size;
	if ((msg_size = send(socket_fd, msg, msg.GetLength(), 0)) <= 0) {
		WSACleanup();	
		this->MessageBox("socket fail 4");
		closesocket(socket_fd);
		return false;
	}

	char buf[128+1];
	memset(buf,0x00, sizeof(buf));
	if ((msg_size = recv(socket_fd, buf, 3, 0)) <= 0) {
		closesocket(socket_fd);
		WSACleanup();	
		this->MessageBox("socket fail 5");
		return false;
	}
	
	closesocket(socket_fd);
	WSACleanup();		

	return true;
}
using namespace std;

string CsocketReceiverDlg::MakeBeaconPacket(const char* beacon_id,  int xiro_x, int xiro_y, int xiro_z )
{
	string scanner_id = beacon_id; 

	// sample msg : "~SIMLJSON00000000004DATA"
    string str_msg("~SIMLJSON0");
    string str_json("");
    char sz_no_buf[33];
    
    string str_rssi    = "-60";
    string str_battery = "60";
    string str_xiro_x  = itoa(xiro_x, sz_no_buf, 10);
    string str_xiro_y  = itoa(xiro_y, sz_no_buf, 10);
    string str_xiro_z  = itoa(xiro_z, sz_no_buf, 10);
    string str_ctime("");

    time_t	m_time;
    struct tm	m_tm;
	char	buf[20];

    time(&m_time);
	tm* local_time = localtime(&m_time);
	if(local_time == 0){
		m_time = MAXLONG;
		local_time = localtime(&m_time);
	}
	m_tm = *local_time;

    sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d",
			m_tm.tm_year + 1900, m_tm.tm_mon + 1, m_tm.tm_mday,
			m_tm.tm_hour, m_tm.tm_min, m_tm.tm_sec);
	str_ctime = buf;


    char sz_buf[11];
    memset(sz_buf, 0x0, 11);

    // JSON CREATE
    JsonObject* root        = new JsonObject();
    JsonObject* xiro_beacon = new JsonObject();
    JsonArray*  xiro_xyz_array = new JsonArray();

    root->add("XIRO_BEACON", xiro_beacon);
    xiro_beacon->add("ACTION_TYPE", "PING");

    xiro_xyz_array->add(str_xiro_x.c_str());
    xiro_xyz_array->add(str_xiro_y.c_str());
    xiro_xyz_array->add(str_xiro_z.c_str());
    xiro_beacon->add("XIRO", xiro_xyz_array);

    xiro_beacon->add("SCANNERID", scanner_id.c_str());
    xiro_beacon->add("BEACONID", beacon_id);
    xiro_beacon->add("RSSI", str_rssi.c_str());
    xiro_beacon->add("BATTERY", str_battery.c_str());
    xiro_beacon->add("CTIME", str_ctime.c_str());

    str_json = root->toString();
    //

    // Length write
    sprintf(sz_buf, "%010d", (int) str_json.size());

    str_msg += ciString(sz_buf);
    str_msg += str_json;

    return str_msg;

}

void CsocketReceiverDlg::OnBnClickedSet()
{
	int j=0;
	for(int i=0;i<15 && j < 3;i++) {
		if(m_radio[i].GetCheck()) {
			CString goodsId;
			m_edit[i].GetWindowText(goodsId);
			for(int p=j;p<3;p++) {
				if(selectedVector[p] < 0) {
					m_showEdit[p].SetWindowText(goodsId);
					selectedVector[p] = i;
					j++;
					m_radio[i].SetCheck(0);
					break;
				}
			}
		}
	}

}


void CsocketReceiverDlg::SendMessageFlash(std::string& str_send_msg)
{
    flash_channel_.StartConnect(flash_ip, flash_port, CONNECT_TCP_ONETRANSACTION_CHANNEL);
    int send_len = flash_channel_.SendMessage(str_send_msg.c_str(), (int) str_send_msg.size());
    flash_channel_.Close();

    if (send_len <= 0) {
        char buf[512] = { 0x00 };
        sprintf(buf, "Flash SendMessage FAILED!!! send_len[%d] msg_size[%d] ", send_len, str_send_msg.size());
        this->MessageBox(buf);
    } else {
        char buf[512] = { 0x00 };
        sprintf(buf, "Flash SendMessage SUCCESS!!! send_len[%d] msg_size[%d] ", send_len, str_send_msg.size());
        this->MessageBox(buf);
    }

}


//<?xml version=\"1.0\" encoding=\"utf-8\"?>
//<ELIGA>
//<BEACON>
//<PICKUP>
    //<NO1>
    //<GOODSID>LFCS51101LB</GOODSID>
    //<NAME>hanger_1</NAME>
    //<PRICE>10001</PRICE>
    //<SIZE>101</SIZE>
    //<COLOR>RED</COLOR>
    //<COUNT>100</COUNT>
    //</NO1>
    //<NO2>
    //<GOODSID>LFTS50201OW</GOODSID>
    //<NAME>hanger_2</NAME>
    //<PRICE>10002</PRICE>
    //<SIZE>102</SIZE>
    //<COLOR>RED2</COLOR>
    //<COUNT>102</COUNT>
    //</NO2>
    //<NO3>
    //<GOODSID>LFPS50101BK</GOODSID>
    //<NAME>hanger_3</NAME>
    //<PRICE>10003</PRICE>
    //<SIZE>103</SIZE>
    //<COLOR>RED3</COLOR>
    //<COUNT>103</COUNT>
    //</NO3>
//</PICKUP>
//</BEACON>
//</ELIGA>

std::string	CsocketReceiverDlg::CreateXmlMsg(goodsInfoList& goods_info_list)
{
    // sample msg : "~AGNTXML_00000000004DATA"
    //string str_msg("~AGNTXML_0");
    string str_msg("");
    string str_xml("");
    string str_no_xml("");
    char sz_buf[11];
    memset(sz_buf, 0x0, 11);

    //[ XML create
	str_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	str_xml += "<ELIGA>\n";
	str_xml += "<BEACON>\n";
    str_xml += "<PICKUP>\n"; // MIRROR_ACCESS, PICKUP
    
    char sz_no_buf[33];
    int goods_no = 0;
    int i = 0;    
    goodsInfoList::iterator itr;
    for (itr = goods_info_list.begin(); itr != goods_info_list.end(); itr++) {
        if (i >= 3) break; // 3개만 flash에 보낸다.

        str_no_xml = ciString("<NO") + itoa(i+1, sz_no_buf, 10) + ciString(">\n");
        //sprintf(sz_buf, "%s%d", str_no_xml.c_str(), i);
        str_xml += str_no_xml;

        goodsInfo goods_info = *itr;
    
        str_xml += "<GOODSID>" + goods_info.goodsId   + "</GOODSID>\n";
        if(goods_info.goodsId != "NULL") {
            str_xml += "<NAME>"    + goods_info.NAME  + "</NAME>\n";
            str_xml += "<PRICE>"   + goods_info.PRICE + "</PRICE>\n";
            str_xml += "<SIZE>"    + goods_info.SIZE  + "</SIZE>\n";
            str_xml += "<COLOR>"   + goods_info.COLOR + "</COLOR>\n";
            str_xml += "<COUNT>"   + goods_info.COUNT + "</COUNT>\n";
            goods_no++;
        }

        str_no_xml = ciString("</NO") + itoa(i+1, sz_no_buf, 10) + ciString(">\n");
        str_xml += str_no_xml;
        i++;
    }

    str_xml += "</PICKUP>\n"; // MIRROR_ACCESS, PICKUP
    str_xml += "</BEACON>\n";
    str_xml += "</ELIGA>";
    //]

    // Length write
    //sprintf(sz_buf, "%010d", (int) str_xml.size());

    //str_msg += ciString(sz_buf);
    str_msg += str_xml;

    //ciDEBUG(1,("CreateGoodsInfoMsg_(%s, %d, RealGoodsNo:%d) XML_MSG[%s]", action_type.c_str(), goods_info_list.size(), goods_no, str_msg.c_str() ));
    return str_msg;
}

