//#include "ci/libDebug/ciArgParser.h"
#include "stdafx.h"
#include "TcpStreamServer.h"

//ciSET_DEBUG(1, "TcpStreamServer");

TcpStreamServer::TcpStreamServer(void)
{
    //ciDEBUG(3, ("TcpStreamServer()"));
    started_ = false;
    p_channel = NULL;
    p_sap_worker = NULL;
}

TcpStreamServer::TcpStreamServer(ServiceAccessWorker* p_worker)
{
    //ciDEBUG(3, ("TcpStreamServer()"));
    started_ = false;
    p_channel = NULL;
    p_sap_worker = p_worker;
}

TcpStreamServer::~TcpStreamServer(void)
{
    //ciDEBUG(3, ("~TcpStreamServer()"));
    started_ = false;
}

void TcpStreamServer::Start(int n_listen_port, string channel_type)
{
    //ciDEBUG(3, ("Start()"));

    Stop();
    listen_port_ = n_listen_port;   
    channel_type_ = channel_type;

    //ciDEBUG(3, ("before start()"));
    started_ = true;
    start();
    //ciDEBUG(3, ("after start()"));
}

void TcpStreamServer::Stop()
{
    //ciDEBUG(3, ("Stop()"));

    delete p_channel;
    p_channel = NULL;
    
    socket_.Close();
	if(started_) {
		started_ = false;
		stop(); 
	}
}

void TcpStreamServer::run()
{
    //ciDEBUG(3, ("run()"));

    bool b_error = false;
    int n_ret = 0;
	while(started_) {
        if (b_error) {
            sleep(5);
            b_error = false;
        }
        if (socket_.GetFD() < 0) {
            socket_.Listen(listen_port_, 100);
            //ciDEBUG(3, ("Server listen port[%d]", listen_port_));
        }

        if (socket_.GetFD() < 0) {
            //ciDEBUG(3, ("Listen socket open error[%d]", n_ret));
            b_error = true;
            continue;
        }

        int n_wait = socket_.WaitRecv(1000);
        ////ciDEBUG(3, ("Server WaitRecv(1000) [%d]", n_wait));
        if (n_wait < 0) {
            b_error = true;
            //ciDEBUG(3, ("WaitRecv error(%d)", b_error));
            continue;
        }
        if (n_wait > 0) {
            int n_fd = socket_.Accept();
            //ciDEBUG(3, ("Accept FD[%d]", n_fd));
            if (n_fd < 0) {
                b_error = true;
            }
            else {               
                // TcpStreamChannel Add socket enqueue
                if (!p_channel) {
                    string str_channel_id = CreateChannelId_();
                    p_channel = new TcpStreamChannel(this);
                    if (p_sap_worker) {
                        p_sap_worker->RegisterTcpChannel(str_channel_id.c_str(), p_channel);
                    }
                    p_channel->StartServer(n_fd, str_channel_id.c_str(), false, channel_type_);
                } else {
                    p_channel->Restart(n_fd);
                }
            }
        }
	}
}

const char* TcpStreamServer::CreateChannelId_()
{
    static int s_nIndex = 0;
    string channel_id("");
     
    if (s_nIndex < 0x7FFFFFFF)
        s_nIndex++;
    else
        s_nIndex = 1;
    char szID[20];
    sprintf(szID, "%08X_%08X", (0xFFFFFFFF & time(NULL)), s_nIndex);

    channel_id = szID;

    return channel_id.c_str();
}
