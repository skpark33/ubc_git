/*! \class SocketBase
 *  Copyright �� 2002, COP. All rights reserved.
 *
 *  \brief
 *  
 *  \author detect8
 *  \version 1.0
 *  \purpose create and config all object of the Beapot Agent
 *  \date 2015/10/06 11:01:00
 */

#ifndef PLAYER_BEAPOTAGENT_SOCKETBASE_H
#define PLAYER_BEAPOTAGENT_SOCKETBASE_H

//#include <ci/libBase/ciBaseType.h>
//#include "ci/libThread/ciSyncUtil.h"

class SocketBase {
public:
    SocketBase();
    virtual ~SocketBase();

    enum {
        SOCKET_BASE_TCP = 0,
        SOCKET_BASE_UDP
    };
    virtual int  Open(const char *szIP, int nPort, int nMaxConnectRetry = 3, int nConnectWaitMillisec = 100, const char *szMyIP = 0L, int nMyPort = 0, int bSetReuse = 1);
    static  int  Connect(int nFD, const char *szIP, int nPort);
    virtual int  Listen(int nPort, int backlog = 5, const char *szBindIP = 0L);
    virtual int  Accept(char *szClientIP = 0L, int *pClientPort = 0L);

    virtual int  Send(const char *pData, int nLen);
    virtual int  Sendn(const char *pData, int nLen);
    virtual int  Sendn(const char *pData, int nLen, int nWaitMilliSec);
    virtual int  Recv(char *buffer, int nSize);
    virtual int  Recvn(char *buffer, int nSize);
    virtual int  Recvn(char *buffer, int nSize, int nWaitMilliSec);
    virtual int  WaitRecv(int nWaitMilliSec);
    static  int  WaitRecv(int nWaitMilliSec, int nFD1, ...);
    virtual int  WaitSend(int nWaitMilliSec);

    virtual int  SetNonblockMode();
    virtual int  SetBlockMode();
    virtual void SetReuse(int nReuse);
    virtual void Close();
    virtual int  Bind(const char *szIP, int nPort);
    static  void Close(int nFD);
    static  int  Bind(int nFD, const char *szIP, int nPort);
    virtual int  GetFD() { return _nFD; }

    void Attach(int nFD) { Close(); _nFD = nFD; }
    int  Detach()        { int nFD = _nFD; _nFD = -1; return nFD; }
    int  Create(int nType = SOCKET_BASE_TCP);

    virtual int  SendTo(const char *szIP, int nPort, const char *pData, int nLen);
    virtual int  RecvFrom(char *buffer, int nSize, char *szPeerIP = 0L, int *pPeerPort = 0L);

protected:
    int   _nFD;
};

#endif //PLAYER_BEAPOTAGENT_SOCKETBASE_H