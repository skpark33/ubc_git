// socketReceiverDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include <vector>
#include <string>

#include "TcpStreamChannel.h"

class SimulInfo
{
public:
	CString beaconId;
	CString goodsId;
	CString NAME;
	CString PRICE;
    CString SIZE;
    CString COLOR;
    CString COUNT;
};
typedef std::vector<SimulInfo*>	SimulInfoVector;

class goodsInfo
{
public:
	std::string beaconId;
    std::string goodsId;
	std::string NAME;
	std::string PRICE;
	std::string SIZE;
    std::string COLOR;
    std::string COUNT;
};
typedef std::list<goodsInfo>	goodsInfoList;

// CsocketReceiverDlg 대화 상자
class CsocketReceiverDlg : public CDialog
{
// 생성입니다.
public:
	CsocketReceiverDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SOCKETRECEIVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	void	loadData();
	void	createMsg(CString& beaconId, int x, int y, int z, CString& outMessage);
	bool	sendMsg(CString& msg);

    std::string	CreateXmlMsg(goodsInfoList& goods_info_list);
    void        SendMessageFlash(std::string& str_send_msg);

protected:
    TcpStreamChannel         flash_channel_;
    std::string              flash_ip;
    int                      flash_port;

public:
	afx_msg void OnBnClickedSend();
	afx_msg void OnBnClickedClearall();

	CButton m_check2;
	CButton m_check3;

	CEdit m_edit[15];
	CButton m_radio[15];

	CEdit m_showEdit[3];
	CButton m_check[3];

	SimulInfoVector  _dataVector;
	int  selectedVector[3];

	std::string MakeBeaconPacket(const char* beacon_id,  int xiro_x, int xiro_y, int xiro_z );
	afx_msg void OnBnClickedSet();
};
