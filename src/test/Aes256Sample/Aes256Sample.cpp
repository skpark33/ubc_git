// Aes256Sample.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <string>
#include "AES256.h"
#include "elCryptoAes256.h"

#define DUMP(s, buf, sz)  {printf(s);                   \
                              for (int i = 0; i < (sz);i++)    \
                                  printf("%02X ", (unsigned char) buf[i]); \
                              printf("\n");}



static
bool
test( AES256::Chaining_Mode   mode,
      std::string     const & key,
      std::string     const & iv,
      std::string     const & plaintext
       )
{
    AES256 aes( key, mode );
    aes.set_IV( iv );

    std::string res("");
    std::string decodetext;

    std::string ciphertext = aes.encrypt( plaintext, false );
    // Remove the IV at the very start

    if ( mode != AES256::ECB )
        ciphertext = ciphertext.substr( 16 );

    //if ( res != ciphertext )
    //    return false;

    // All modes except ECG need the IV to be prepended.

    if ( mode != AES256::ECB )
        res = iv + ciphertext;
    else {
        res = ciphertext;
    }
    // planetext가 16배수 길이 이면 ( 16 32 48 64 80 96 102 128 길이) 이면 
    // decodetext = aes.decrypt( res, false ); 는 죽는다. decodetext = aes.decrypt( res, true );는 안죽는다.
    // => 해결 : aes.encrypt( plaintext, false ); 이면 aes.decrypt( res, false );로 셋팅한다.
    // => 해결 : aes.encrypt( plaintext, true ); 이면 aes.decrypt( res, true );로 셋팅한다.
    decodetext = aes.decrypt( res, false );

    printf("#################################\n");
    printf("KEY         (%s)\n", key.c_str());
    printf("IV          (%s)\n", iv.c_str());
    printf("ENCODE TEXT (%s)\n", ciphertext.c_str());
    DUMP("ENCODE DUMP: ", ciphertext.c_str(), ciphertext.size());
    printf("RES TEXT    (%s)\n", res.c_str());
    printf("DECODE TEXT (%s)\n", decodetext.c_str());
    DUMP("DECODE DUMP: ", decodetext.c_str(), decodetext.size());
    printf("PLAIN TEXT  (%s)\n", plaintext.c_str());
    printf("#################################\n");

    return decodetext == plaintext;
}

void testLegacy(std::string plaintext)
{
    AES256::Chaining_Mode mode;

    std::string key("JYT20140315197402171994082501234");
    std::string iv("JYT2014031519740");
    //std::string iv("");

    int count = 0;
    char buf[ 1024 ];
    int fail = 0;

    mode = AES256::CBC;
    //mode = AES256::ECB;

	// When we've found all necessary data run the test
    //if (    ! key.empty( )
    //     && ( mode == AES256::ECB || ! iv.empty( ) )
    //     && ! plaintext.empty( )
    //     && ! ciphertext.empty( ) )
    {
        if ( ! test( mode, key, iv, plaintext ) )
            fail += 1;
        key.clear( );
        iv.clear( );
        plaintext.clear( );
    }

    if ( fail )
        std::cout << plaintext.c_str() << " => " << fail << " out of " << count
                  << " failed" << std::endl;
    else
        std::cout << plaintext.c_str() << " => success" << std::endl;

}

int main(int argc, char* argv[])
{
    if ( argc < 2 )
    {
        std::cerr << "A plane text must be supplied" << std::endl;
        return EXIT_FAILURE;
    }

    // Legacy Test
    testLegacy(argv[ 1 ]);


    // elCryptoAes256 Test
    std::string key("JYT20140315197402171994082501234");
    std::string iv("JYT2014031519740");

    elCryptoAes256 aes256;
    if (!aes256.Init(key, iv)) {
        printf("Failed aes256.Init!");
        exit(1);
    }

    std::string cipher_text = aes256.Encrypt(argv[1]);
    std::string decipher_text = aes256.Decrypt(cipher_text);

    printf("\n############################################\n");
    printf("CipherText : \n");
    printf("%s\n", cipher_text.c_str());
    DUMP  ("CipherText DUMP: ", cipher_text.c_str(), cipher_text.size());
    printf("DecipherText(%s)\n", decipher_text.c_str());
    DUMP  ("DecipherText DUMP: ", decipher_text.c_str(), decipher_text.size());
    printf("############################################\n");

    return 0;
}
