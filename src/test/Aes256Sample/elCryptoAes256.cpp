#include "elCryptoAes256.h"

elCryptoAes256::elCryptoAes256(void)
{
    aes256_ = NULL;
}

elCryptoAes256::~elCryptoAes256(void)
{
    if (aes256_) {
        delete aes256_;
        aes256_ = NULL;
    }
}

bool elCryptoAes256::Init(std::string key32, std::string iv16, AES256::Chaining_Mode mode)
{
    if (key32.size() != 32) {
        printf("KEY size=%d, (%s)", key32.size(), key32.c_str());
        return false;
    }
    if (iv16.size() != 16) {
        printf("IV size=%d, (%s)", iv16.size(), iv16.c_str());
        return false;
    }

    key32_ = key32;
    iv16_ = iv16;

    if (aes256_) {
        delete aes256_;
        aes256_ = NULL;
    }
    aes256_ = new AES256(key32, mode);
    aes256_->set_IV(iv16);
    
    return true;
}

std::string elCryptoAes256::Encrypt(std::string plain_text)
{
    if (!aes256_) {
        return std::string("");
    }

    // 블록패딩을 뺀다. no_padding_block = false 
    std::string cipher_text("");
    if (block_cipher_mode_ != AES256::ECB) {
        cipher_text = aes256_->encrypt(plain_text, false ).substr(16);
    } else {
        cipher_text = aes256_->encrypt(plain_text, false );
    }
    return cipher_text;
}

std::string elCryptoAes256::Decrypt(std::string cipher_text)
{
    if (!aes256_) {
        return std::string("");
    }

    std::string res("");

    if ( block_cipher_mode_ != AES256::ECB )
        res = iv16_ + cipher_text;
    else {
        res = cipher_text;
    }

    // 블록패딩을 뺀다. no_padding_block = false 
    return aes256_->decrypt( res, false );
}
