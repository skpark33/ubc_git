// UBCMediaInfo.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>

int _tmain(int argc, _TCHAR* argv[])
{
	std::string source = _T("C:\\SQISoft\\Contents\\Enc\\movie.flv");
	if(argc < 2) {
	}else{
		source = argv[1];
		if(source == _T("/?") || source == _T("-help") || source == _T("/h") || source == _T("--help") || source == _T("-h")) {
			std::wcout << _T("Usage : UBCAutoEncoding [source_file_full_path] [batch_file_full_path] [target_full_path]") << std::endl;
			std::wcout << _T("Example : UBCAutoEncoding C:\\SQISoft\\Contents\\Enc\\movie.flv C:\\SQISoft\\UTV1.0\\execute\\x264_mp4_aac_org C:\\SQISoft\\Contents\\Enc\\movie.mp4") << std::endl;
			return 1;
		}
	}
	std::string batchFileName = _T("C:\\SQISoft\\UTV1.0\\execute\\x264_mp4_aac");
	if(argc >= 3) {
		batchFileName = argv[2];
	}
	std::string targetFullPath = _T("C:\\SQISoft\\Contents\\Enc\\movie.mp4");
	if(argc >= 4) {
		targetFullPath = argv[3];
	}


	std::string video, audio, errStr;
	int bitRate = getBitRate(source, video, audio,errStr);
	if(bitRate == 0){
		std::wcout << _T("ERROR: ") << errStr.c_str() << std::endl;
		return 1;
	}
    std::wcout  << _T("BITRATE=[") << bitRate << _T("] KByte") << std::endl;


	wchar_t drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
	_wsplitpath(source.c_str(), drive, path, filename, ext);

	std::string tag;
	std::string extention = ext;
	/*
	if(extention == _T(".mp4")) {
		std::wcout  << _T("same extention") << std::endl;
		// audio, video codec 이 모두 같다면, 인코딩 하지 않는다.
		if(video == _T("AVC") || audio == _T("AAC")) {
			std::wcout  << _T("same codec, no encoding required !") << std::endl;
			return 0;
		}
		tag = _T("_");
	}
	*/
	/*
	std::string tmpName = _T("C:\\Temp\\");
	tmpName += filename;
	tmpName += tag;
	tmpName += _T(".mp4");
	*/
	std::string tmpName = targetFullPath;
	tmpName += _T(".tmp");
	/*
	std::string converted = drive;
	converted += path;
	converted += filename;
	converted += tag;
	converted += _T(".mp4");
	*/
	std::string converted = targetFullPath;

	std::wcout  << tmpName.c_str() << std::endl;
	std::wcout  << converted.c_str() << std::endl;

	std::string command = batchFileName;
	//command += _T(".bat ");
	command += _T(" ");
	command += source;
	command += _T(" ");
	//command += converted;
	command += tmpName;
	command += _T(" ");

	wchar_t buf[100];
	wsprintf(buf, _T("%d"), bitRate); 

	command += buf;

    std::wcout  << command << std::endl;
	
	int retval = _wsystem(command.c_str());
	if(retval != 0) {
	    std::wcout  << _T("encoding failed !!!") << std::endl;
	}

	if(!MoveFileEx(tmpName.c_str(), converted.c_str(), MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
	{
		std::wcout << _T("Fail to move ") << tmpName.c_str() << " to " << converted.c_str() << std::endl;
		return 1;
	}

	return retval;
}


