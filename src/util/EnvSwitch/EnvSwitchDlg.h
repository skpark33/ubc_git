// EnvSwitchDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"

#define UTV_STAND_ALONE		100
#define UTV_NETWORK			200

#define REGISTRY_ADDRESS "Environment"
#define START_PROGRAM_ADDRESS "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"

// CEnvSwitchDlg 대화 상자
class CEnvSwitchDlg : public CDialog
{
// 생성입니다.
public:
	CEnvSwitchDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ENVSWITCH_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	void RegWrite(const char* regItemName, const char* value);
	void RegRead(const char* regItemName, CString& outValue);
	void RegStartWrite(const char* regItemName, const char* value);
	BOOL RegStartProgram(int utvType);

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_CopHome;
	CEdit m_ProjectHome;
	CEdit m_ConfigRoot;
	CEdit m_LogRoot;
	CEdit m_Path;
	CEdit m_TrdPartyRoot;
	CEdit m_VbrokerHome;
	CEdit m_BesDefaultDir;
	CEdit m_BesDir;
	CButton m_Network;
	CButton m_StandAlone;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedNetworkRadio();
	afx_msg void OnBnClickedStandaloneRadio();
};
