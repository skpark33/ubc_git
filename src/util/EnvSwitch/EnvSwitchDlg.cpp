// EnvSwitchDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "EnvSwitch.h"
#include "EnvSwitchDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CEnvSwitchDlg 대화 상자




CEnvSwitchDlg::CEnvSwitchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEnvSwitchDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CEnvSwitchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COPHOME_EDIT, m_CopHome);
	DDX_Control(pDX, IDC_PROJECTHOME_EDIT, m_ProjectHome);
	DDX_Control(pDX, IDC_CONFIGROOT_EDIT, m_ConfigRoot);
	DDX_Control(pDX, IDC_LOGROOT_EDIT, m_LogRoot);
	DDX_Control(pDX, IDC_PATH_EDIT, m_Path);
	DDX_Control(pDX, IDC_TRDPARTYROOT_EDIT, m_TrdPartyRoot);
	DDX_Control(pDX, IDC_VBROKERHOME_EDIT, m_VbrokerHome);
	DDX_Control(pDX, IDC_BESLICDEFAULTDIR_EDIT, m_BesDefaultDir);
	DDX_Control(pDX, IDC_BESLICDIR_EDIT, m_BesDir);
	DDX_Control(pDX, IDC_NETWORK_RADIO, m_Network);
	DDX_Control(pDX, IDC_STANDALONE_RADIO, m_StandAlone);
}

BEGIN_MESSAGE_MAP(CEnvSwitchDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CEnvSwitchDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_NETWORK_RADIO, &CEnvSwitchDlg::OnBnClickedNetworkRadio)
	ON_BN_CLICKED(IDC_STANDALONE_RADIO, &CEnvSwitchDlg::OnBnClickedStandaloneRadio)
END_MESSAGE_MAP()


// CEnvSwitchDlg 메시지 처리기

BOOL CEnvSwitchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	// 레지스트리 검색
	CString copHome, projHome;
	CString configRoot, logRoot, path;
	CString trdPartyRoot, vbrokerHome, besDefDir, besDir;

	RegRead("COP_HOME", copHome);
	RegRead("PROJECT_HOME", projHome);
	RegRead("CONFIGROOT", configRoot);
	RegRead("LOGROOT", logRoot);
	RegRead("PATH", path);
	RegRead("TRDPARTYROOT", trdPartyRoot);
	RegRead("VBROKER_HOME", vbrokerHome);
	RegRead("BES_LIC_DEFAULT_DIR", besDefDir);
	RegRead("BES_LIC_DIR", besDir);

	m_CopHome.SetWindowText(copHome.GetBuffer());
	m_ProjectHome.SetWindowText(projHome.GetBuffer());
	m_ConfigRoot.SetWindowText(configRoot.GetBuffer());
	m_LogRoot.SetWindowText(logRoot.GetBuffer());
	m_Path.SetWindowText(path.GetBuffer());
	m_TrdPartyRoot.SetWindowText(trdPartyRoot.GetBuffer());
	m_VbrokerHome.SetWindowText(vbrokerHome.GetBuffer());
	m_BesDefaultDir.SetWindowText(besDefDir.GetBuffer());
	m_BesDir.SetWindowText(besDir.GetBuffer());

	BOOL isStandAlone = false;
	CButton *pCheck;
	if (strstr(projHome.GetBuffer(), "StandAlone")) {
		pCheck = (CButton *)GetDlgItem(IDC_STANDALONE_RADIO);
		isStandAlone = true;
	} else {
		pCheck = (CButton *)GetDlgItem(IDC_NETWORK_RADIO);
	}
	pCheck->SetCheck(TRUE);

	if (!isStandAlone) {
		m_TrdPartyRoot.SetReadOnly();
		m_VbrokerHome.SetReadOnly();
		m_BesDefaultDir.SetReadOnly();
		m_BesDir.SetReadOnly();
	}

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CEnvSwitchDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CEnvSwitchDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CEnvSwitchDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// 레지스트리 관련 유틸 함수

void CEnvSwitchDlg::RegWrite(const char* regItemName, const char* value)
{
	// 값이 없으면 변경하지 않는다.
	if (strlen(value) <= 0) return;

	HKEY  m_hRegsKey; 
	RegCreateKey(HKEY_CURRENT_USER, REGISTRY_ADDRESS, &m_hRegsKey);

	RegSetValueEx(m_hRegsKey, // subkey handle 
		regItemName, // value name 
		0, // must be zero 
		REG_SZ, // value type canbe REG_DWORD, 
		(LPBYTE) value, // pointer to value data 
		strlen(value) + 1); // length of value data 
	RegCloseKey(m_hRegsKey);
}

void CEnvSwitchDlg::RegRead(const char* regItemName, CString& strOutValue)
{
	HKEY  m_hRegsKey; 
	RegCreateKey(HKEY_CURRENT_USER, REGISTRY_ADDRESS, &m_hRegsKey);
	//DWORD size = MAX_PATH+1;
	DWORD size = 2048;
	unsigned long ValueType = REG_SZ; 
	char outValue[2048];
	memset(outValue, 0x00, 2048);
	RegQueryValueEx(m_hRegsKey, regItemName, 
		NULL, &ValueType, (unsigned char *)outValue, &size);
	strOutValue = outValue;
	RegCloseKey(m_hRegsKey);
}

void CEnvSwitchDlg::RegStartWrite(const char* regItemName, const char* value)
{
	// 값이 없으면 해당 변수를 제거한다.
	HKEY  m_hRegsKey; 
	RegCreateKey(HKEY_LOCAL_MACHINE, START_PROGRAM_ADDRESS, &m_hRegsKey);

	if (strlen(value)) {
		RegSetValueEx(m_hRegsKey, // subkey handle 
			regItemName, // value name 
			0, // must be zero 
			REG_SZ, // value type canbe REG_DWORD, 
			(LPBYTE) value, // pointer to value data 
			strlen(value) + 1); // length of value data 
	} else {
		RegDeleteValue(m_hRegsKey, regItemName);
	}
	RegCloseKey(m_hRegsKey);
}

void CEnvSwitchDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString copHome, projHome;
	CString configRoot, logRoot, path;
	CString trdPartyRoot, vbrokerHome, besDefDir, besDir;

	m_CopHome.GetWindowText(copHome);
	m_ProjectHome.GetWindowText(projHome);
	m_ConfigRoot.GetWindowText(configRoot);
	m_LogRoot.GetWindowText(logRoot);
	m_Path.GetWindowText(path);

	m_TrdPartyRoot.GetWindowText(trdPartyRoot);
	m_VbrokerHome.GetWindowText(vbrokerHome);
	m_BesDefaultDir.GetWindowText(besDefDir);
	m_BesDir.GetWindowText(besDir);

	RegWrite("COP_HOME", copHome.GetBuffer());
	RegWrite("PROJECT_HOME", projHome.GetBuffer());
	RegWrite("CONFIGROOT", configRoot.GetBuffer());
	RegWrite("LOGROOT", logRoot.GetBuffer());
	RegWrite("PATH", path.GetBuffer());
	RegWrite("TRDPARTYROOT", trdPartyRoot.GetBuffer());
	RegWrite("VBROKER_HOME", vbrokerHome.GetBuffer());
	RegWrite("BES_LIC_DEFAULT_DIR", besDefDir.GetBuffer());
	RegWrite("BES_LIC_DIR", besDir.GetBuffer());

	// 시작 프로그램 등록
	if (strstr(projHome.GetBuffer(), "StandAlone")) {
		RegStartProgram(UTV_STAND_ALONE);
	} else if (strstr(projHome.GetBuffer(), "Network")) {
		RegStartProgram(UTV_NETWORK);
	} else {
		// 시작 프로그램 모두 제거
		RegStartProgram(-1);
	}

	// 환경이 변경되었다는 것을 시스템에 방송한다.
	CString strEnv = "Environment";
	::SendMessage(HWND_BROADCAST,WM_SETTINGCHANGE,0, (LPARAM)strEnv.GetBuffer());

	AfxMessageBox("사용자 환경변수값이 수정되었습니다.\n프로세스들을 재기동해주세요.");

	//OnOK();
}

void CEnvSwitchDlg::OnBnClickedNetworkRadio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CButton *pCheck;
	pCheck = (CButton *)GetDlgItem(IDC_NETWORK_RADIO);
	if (!pCheck->GetCheck()) {
		return;
	}

	CString copHome, projHome;
	CString configRoot, logRoot, path;

	m_CopHome.GetWindowText(copHome);
	m_ProjectHome.GetWindowText(projHome);
	m_ConfigRoot.GetWindowText(configRoot);
	m_LogRoot.GetWindowText(logRoot);
	m_Path.GetWindowText(path);

	copHome.Replace("StandAlone", "Network");
	projHome.Replace("StandAlone", "Network");
	configRoot.Replace("StandAlone", "Network");
	logRoot.Replace("StandAlone", "Network");
	path.Replace("StandAlone", "Network");

	m_CopHome.SetWindowText(copHome.GetBuffer());
	m_ProjectHome.SetWindowText(projHome.GetBuffer());
	m_ConfigRoot.SetWindowText(configRoot.GetBuffer());
	m_LogRoot.SetWindowText(logRoot.GetBuffer());
	m_Path.SetWindowText(path.GetBuffer());

	m_TrdPartyRoot.SetReadOnly();
	m_VbrokerHome.SetReadOnly();
	m_BesDefaultDir.SetReadOnly();
	m_BesDir.SetReadOnly();
}

void CEnvSwitchDlg::OnBnClickedStandaloneRadio()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CButton *pCheck;
	pCheck = (CButton *)GetDlgItem(IDC_STANDALONE_RADIO);
	if (!pCheck->GetCheck()) {
		return;
	}

	CString copHome, projHome;
	CString configRoot, logRoot, path;

	m_CopHome.GetWindowText(copHome);
	m_ProjectHome.GetWindowText(projHome);
	m_ConfigRoot.GetWindowText(configRoot);
	m_LogRoot.GetWindowText(logRoot);
	m_Path.GetWindowText(path);

	copHome.Replace("Network", "StandAlone");
	projHome.Replace("Network", "StandAlone");
	configRoot.Replace("Network", "StandAlone");
	logRoot.Replace("Network", "StandAlone");
	path.Replace("Network", "StandAlone");

	m_CopHome.SetWindowText(copHome.GetBuffer());
	m_ProjectHome.SetWindowText(projHome.GetBuffer());
	m_ConfigRoot.SetWindowText(configRoot.GetBuffer());
	m_LogRoot.SetWindowText(logRoot.GetBuffer());
	m_Path.SetWindowText(path.GetBuffer());

	m_TrdPartyRoot.SetReadOnly(false);
	m_VbrokerHome.SetReadOnly(false);
	m_BesDefaultDir.SetReadOnly(false);
	m_BesDir.SetReadOnly(false);
}

BOOL CEnvSwitchDlg::RegStartProgram(int utvType)
{
	if (utvType == UTV_STAND_ALONE) {
		RegStartWrite("UTV_Network", "");
		RegStartWrite("UTV_StandAlone", "copRun.bat base");
	} else if (utvType == UTV_NETWORK) {
		RegStartWrite("UTV_StandAlone", "");
		RegStartWrite("UTV_Network", "SelfAutoUpdate.exe +log +start");
	} else {
		RegStartWrite("UTV_StandAlone", "");
		RegStartWrite("UTV_Network", "");
	}
	
	return true;
}
