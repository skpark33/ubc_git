//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EnvSwitch.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ENVSWITCH_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_COPHOME_EDIT                1000
#define IDC_PROJECTHOME_EDIT            1001
#define IDC_CONFIGROOT_EDIT             1002
#define IDC_LOGROOT_EDIT                1003
#define IDC_PATH_EDIT                   1004
#define IDC_TRDPARTYROOT_EDIT           1005
#define IDC_VBROKERHOME_EDIT            1006
#define IDC_BESLICDEFAULTDIR_EDIT       1007
#define IDC_BESLICDIR_EDIT              1008
#define IDC_NETWORK_RADIO               1009
#define IDC_RADIO2                      1010
#define IDC_STANDALONE_RADIO            1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
