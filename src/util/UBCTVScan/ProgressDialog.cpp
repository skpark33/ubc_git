// ProgressDialog.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "UBCTVScan.h"
#include "ProgressDialog.h"


// CProgressDialog 대화 상자입니다.

IMPLEMENT_DYNAMIC(CProgressDialog, CDialog)

CProgressDialog::CProgressDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDialog::IDD, pParent)
{

}

CProgressDialog::~CProgressDialog()
{
}

void CProgressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_CURRENT, m_pcCurrent);
	DDX_Control(pDX, IDC_PROGRESS_TOTAL, m_pcTotal);
}


BEGIN_MESSAGE_MAP(CProgressDialog, CDialog)
END_MESSAGE_MAP()


// CProgressDialog 메시지 처리기입니다.

void CProgressDialog::SetCurrentCount(int nCount)
{
	m_pcCurrent.SetRange32(0, nCount);
}

void CProgressDialog::SetTotalCount(int nCount)
{
	m_pcTotal.SetRange32(0, nCount);
}

void CProgressDialog::SetCurrentStep(int nStep)
{
	m_pcCurrent.SetPos(nStep);
}

void CProgressDialog::SetTotalStep(int nStep)
{
	m_pcTotal.SetPos(nStep);
}

