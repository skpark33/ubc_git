//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCTVScan.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_UBCTVSCAN_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_PROGRESS             129
#define IDI_ICON_EXPAND                 130
#define IDI_ICON_REDUCE                 131
#define IDC_COMBO_COUNTRY               1000
#define IDC_COMBO_MODULATION            1001
#define IDC_PROGRESS_SCAN               1003
#define IDC_CHECK_ANALOG_AIR            1004
#define IDC_CHECK_DIGITAL_AIR           1005
#define IDC_CHECK_ANALOG_CATV           1006
#define IDC_CHECK_DIGITAL_CATV          1007
#define IDC_LIST_CHANNEL                1008
#define IDC_BUTTON2                     1010
#define IDC_BUTTON_SCAN                 1010
#define IDC_STATIC_PREVIEW              1011
#define IDC_PROGRESS_CURRENT            1012
#define IDC_PROGRESS2                   1013
#define IDC_PROGRESS_TOTAL              1013
#define IDC_STATIC_FOLDING              1013
#define IDC_STATIC_VERT                 1014
#define IDC_BUTTON1                     1015
#define IDC_BUTTON_SAVE                 1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
