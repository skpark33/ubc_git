// TVInterface.cpp
//

#include "stdafx.h"
#include "TVInterface.h"


#define		INI_FILENAME		_T("UBCTVScan.ini")


TVCHANNEL_ITEMLIST	CTVInterface::m_ChannelList;


CTVInterface::CTVInterface()
:	m_nCurrentChannelIndex ( -1 )
,	m_pParentWnd ( NULL )
,	m_nStartupDelay ( 0 )
{
	LoadChannelList();
}

CTVInterface::~CTVInterface()
{
}

LPCTSTR CTVInterface::GetAppPath()
{
	static CString	_mainDirectory = _T("");

	if(_mainDirectory.GetLength() == 0)
	{
		TCHAR str[MAX_PATH];
		::ZeroMemory(str, MAX_PATH);
		::GetModuleFileName(NULL, str, MAX_PATH);
		int length = _tcslen(str) - 1;
		while( (length > 0) && (str[length] != _T('\\')) )
			str[length--] = 0;

		_mainDirectory = str;
	}

	return _mainDirectory;
}

BOOL CTVInterface::DeleteINIFile()
{
	CString strPath = GetAppPath();
	strPath += _T("data\\");
	strPath += INI_FILENAME;

	return ::DeleteFile(strPath);
}

int CTVInterface::GetINIValueInt(LPCTSTR lpszFileName, LPCTSTR lpszSection, LPCTSTR lpszKey)
{
	return _ttoi(GetINIValueString(lpszFileName, lpszSection, lpszKey));
}

CString CTVInterface::GetINIValueString(LPCTSTR lpszFileName, LPCTSTR lpszSection, LPCTSTR lpszKey)
{
	TCHAR cBuffer[1024] = { 0x00 };
	CString strPath = GetAppPath();
	strPath += _T("data\\");
	strPath += lpszFileName;
	GetPrivateProfileString(lpszSection, lpszKey, _T(""), cBuffer, 1024, strPath);

	return CString(cBuffer);
}

void CTVInterface::WriteINIValueInt(LPCTSTR lpszFileName, LPCTSTR lpszSection, LPCTSTR lpszKey, int nValue)
{
	CString buf;
	buf.Format(_T("%d"), nValue);

	WriteINIValueString(lpszFileName, lpszSection, lpszKey, buf);
}

void CTVInterface::WriteINIValueString(LPCTSTR lpszFileName, LPCTSTR lpszSection, LPCTSTR lpszKey, LPCTSTR lpszValue)
{
	CString strPath = GetAppPath();
	strPath += _T("data\\");
	::CreateDirectory(strPath, NULL);
	strPath += lpszFileName;
	WritePrivateProfileString(lpszSection, lpszKey, lpszValue, strPath);
}

int CTVInterface::GetPrevChannelIndex(TV_MODE eTVMode)
{
	int count = m_ChannelList.GetCount();
	if(m_nCurrentChannelIndex >= count) return -1;

	if(m_nCurrentChannelIndex < 0)
	{
		for(int i=0; i<count; i++)
		{
			TVCHANNEL_ITEM& item = m_ChannelList.GetAt(i);

			if(item.eTVMode == eTVMode)
			{
				return i;
			}
		}
	}
	else
	{
		int prev_index = m_nCurrentChannelIndex-1;
		while(1)
		{
			if(prev_index < 0) prev_index = count-1;
			if(m_nCurrentChannelIndex == prev_index) return m_nCurrentChannelIndex;
			
			TVCHANNEL_ITEM& item = m_ChannelList.GetAt(prev_index);

			if(item.eTVMode == eTVMode)
			{
				return prev_index;
			}
			prev_index--;
		}
	}
	return -1;
}

int CTVInterface::GetNextChannelIndex(TV_MODE eTVMode)
{
	int count = m_ChannelList.GetCount();
	if(m_nCurrentChannelIndex >= count) return -1;

	if(m_nCurrentChannelIndex < 0)
	{
		for(int i=0; i<count; i++)
		{
			TVCHANNEL_ITEM& item = m_ChannelList.GetAt(i);

			if(item.eTVMode == eTVMode)
			{
				return i;
			}
		}
	}
	else
	{
		int next_index = m_nCurrentChannelIndex+1;
		while(1)
		{
			if(next_index >= count) next_index = 0;
			if(m_nCurrentChannelIndex == next_index) return m_nCurrentChannelIndex;
			
			TVCHANNEL_ITEM& item = m_ChannelList.GetAt(next_index);

			if(item.eTVMode == eTVMode)
			{
				return next_index;
			}
			next_index++;
		}
	}
	return -1;
}

BOOL CTVInterface::ClearChannelList(bool bDataDelete, bool bFileDelete)
{
	if(bFileDelete)
	{
		if(!DeleteINIFile())
			return FALSE;
	}

	if(bDataDelete)
		m_ChannelList.RemoveAll();

	return TRUE;
}

BOOL CTVInterface::LoadChannelList()
{
	if(m_ChannelList.GetCount() == 0)
	{
		m_nStartupDelay = GetINIValueInt(INI_FILENAME, _T("ROOT"), _T("StartupDelay"));
		if(m_nStartupDelay == 0) m_nStartupDelay = 3000;
		WriteINIValueInt(INI_FILENAME, _T("ROOT"), _T("StartupDelay"), m_nStartupDelay);

		int count = GetINIValueInt(INI_FILENAME, _T("ROOT"), _T("ChannelCount"));

		for(int i=0; i<count; i++)
		{
			CString key;
			key.Format(_T("CHANNEL%d"), i);

			TVCHANNEL_ITEM item;
			item.eTVMode = (TV_MODE)GetINIValueInt(INI_FILENAME, key, _T("TVMode"));
			item.nChannel = GetINIValueInt(INI_FILENAME, key, _T("Channel"));
			item.strName = GetINIValueString(INI_FILENAME, key, _T("Name"));

			m_ChannelList.Add(item);
		}
	}

	return TRUE;
}

BOOL CTVInterface::SaveChannelList()
{
	int count = m_ChannelList.GetCount();

	WriteINIValueInt(INI_FILENAME, _T("ROOT"), _T("ChannelCount"), count);

	for(int i=0; i<count; i++)
	{
		TVCHANNEL_ITEM& item = m_ChannelList.GetAt(i);

		CString key;
		key.Format(_T("CHANNEL%d"), i);

		WriteINIValueInt(INI_FILENAME, key, _T("TVMode"), (int)item.eTVMode);
		WriteINIValueInt(INI_FILENAME, key, _T("Channel"), item.nChannel);
		WriteINIValueString(INI_FILENAME, key, _T("Name"), item.strName);
	}

	return TRUE;
}

BOOL CTVInterface::AddChannel(TVCHANNEL_ITEM* pItem)
{
	if(pItem == NULL) return FALSE;

	int count = m_ChannelList.GetCount();
	for(int i=0; i<count; i++)
	{
		TVCHANNEL_ITEM& item = m_ChannelList.GetAt(i);

		if(item.eTVMode == pItem->eTVMode &&
			item.nChannel == pItem->nChannel)
		{
			return FALSE;
		}
	}

	TVCHANNEL_ITEM item = *pItem;
	m_ChannelList.Add(item);

	return TRUE;
}

BOOL CTVInterface::DeleteChannel(TVCHANNEL_ITEM* pItem)
{
	if(pItem == NULL) return FALSE;

	int count = m_ChannelList.GetCount();
	for(int i=0; i<count; i++)
	{
		TVCHANNEL_ITEM& item = m_ChannelList.GetAt(i);

		if(item.eTVMode == pItem->eTVMode &&
			item.nChannel == pItem->nChannel)
		{
			m_ChannelList.RemoveAt(i);
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CTVInterface::GetTvChannelList(TVCHANNEL_ITEMLIST& listChannel, TV_MODE mode)
{
	if(mode == modeAll)
	{
		listChannel.Copy(m_ChannelList);
	}
	else
	{
		int count = m_ChannelList.GetCount();
		for(int i=0; i<count; i++)
		{
			TVCHANNEL_ITEM& item = m_ChannelList.GetAt(i);
			if(item.eTVMode == mode)
				listChannel.Add(item);
		}
	}
	return TRUE;
}
