#pragma once

// 컴퓨터에서 Microsoft Visual C++를 사용하여 생성한 IDispatch 래퍼 클래스입니다.

// 참고: 이 파일의 내용을 수정하지 마십시오. Microsoft Visual C++에서
//  이 클래스를 다시 생성할 때 수정한 내용을 덮어씁니다.

/////////////////////////////////////////////////////////////////////////////
// CDvicoTV103 래퍼 클래스입니다.

class CDvicoTV103 : public CWnd
{
protected:
	DECLARE_DYNCREATE(CDvicoTV103)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0x25C2F186, 0xCE24, 0x4F4F, { 0x9B, 0xAA, 0x48, 0xB8, 0x72, 0xE7, 0xB, 0x61 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle,
						const RECT& rect, CWnd* pParentWnd, UINT nID, 
						CCreateContext* pContext = NULL)
	{ 
		return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); 
	}

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, 
				UINT nID, CFile* pPersist = NULL, BOOL bStorage = FALSE,
				BSTR bstrLicKey = NULL)
	{ 
		return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); 
	}

// 특성입니다.
public:
enum
{
    spaceAnalogAir = 0,
    spaceAnalogCatv = 1,
    spaceDigitalAir = 2,
    spaceDigitalCatv = 3,
    spaceMobile = 4,
    spaceSatellite = 5
}TxTuneSpace;
enum
{
    qualVcd = 0,
    qualSvcd = 1,
    qualDvd = 2,
    qualDivx = 3,
    qualMpeg1 = 4,
    qualMpeg2 = 5,
    qualStandard = 6,
    qualMidrange = 7,
    qualHigh = 8
}TxQuality;
enum
{
    tsNative = 0,
    tsParalellStream = 1,
    tsSerialStream = 2,
    tsParallelNoStream = 3,
    tsSerialNoStream = 4
}TxTsInterface;
enum
{
    sourceSvhs = 0,
    sourceVideo = 1,
    sourceTuner = 2,
    sourceFile = 3,
    sourceDvhs = 4
}TxSource;
enum
{
    recordNone = 0,
    recordFile = 1,
    recordDvhs = 2,
    recordFileDvhs = 3
}TxRecTarget;
enum
{
    previewNone = 0,
    previewVga = 1,
    previewDvhs = 2,
    previewVgaDvhs = 3
}TxViewTarget;
enum
{
    motionStop = 0,
    motionPlaying = 1,
    motionPause = 2,
    motionRecording = 3,
    motionPlayingAndRecording = 4,
    motionFilePlaying = 5
}TxMotion;
enum
{
    waveout = 0,
    directsound = 1
}TxAudioRenderer;
enum
{
    formatNTSC = 0,
    formatPAL = 1,
    formatSECAM = 2,
    formatATSC = 3,
    formatDVBT = 4,
    formatDVBS = 5,
    formatDMB = 6,
    formatDVBC = 7
}TxTvFormat;
enum
{
    resFullD1 = 0,
    res66percentD1 = 1,
    resHalfD1 = 2,
    resQuarterD1 = 3,
    resCIF = 4,
    res1080i = 5,
    res720p = 6,
    res720i = 7,
    res480p = 8,
    res480i = 9,
    resAnaD1 = 10,
    res576p = 11,
    res576i = 12,
    res720x480 = 13,
    res960x540 = 14,
    res1280x720 = 15
}TxResolution;
enum
{
    modeCBR = 0,
    modeVBR = 1
}TxEncodingMode;
enum
{
    typeMpeg1 = 0,
    typeMpeg2 = 1,
    typeDvd = 2,
    typeVcd = 3,
    typeSvcd = 4,
    typeDivx = 5
}TxDiscFormat;
enum
{
    audioRateNone = 0,
    audioRateAC3 = 1,
    audioRateAAC = 2,
    audioRateMPEG1 = 3,
    audioRateMPEG2 = 4,
    audioRateAC3_6Ch = 5,
    audioRateAAC_6CH = 6,
    audioRateMPEG2_6CH = 7,
    audioRate64KHz = 8,
    audioRate96KHz = 9,
    audioRate192KHz = 10,
    audioRate224Khz = 11,
    audioRate256KHz = 12,
    audioRate320KHz = 13,
    audioRate384KHz = 14
}TxAudioRate;
enum
{
    sample44K = 0,
    sample48K = 1,
    sample32K = 2
}TxAudioSamplingRate;
enum
{
    stillBmp = 0,
    stillPng = 1,
    stillJpg = 2,
    stillGif = 3,
    stillTif = 4
}TxStillFormat;
enum
{
    oemFusionPci = 0,
    oemBluebirdUsb = 1,
    oemUltraviewPci = 2,
    oemUnspecified = 3
}TxOemDevice;
enum
{
    tunerNone = 0,
    tunerPhilipsNtsc = 1,
    tunerPhilipsPalBG = 2,
    tunerPhilipsPalI = 3,
    tunerPhilipsPalDK = 4,
    tunerLgNtsc = 5,
    tunerLgPalBG = 6,
    tunerLgPalI = 7,
    tunerLgPalDK = 8,
    tunerReserved = 9
}TxTunerType;
enum
{
    errNone = 0,
    errObjectAlreadyCreate = 1,
    errDirectShow = 2,
    errDevice = 3,
    errOutOfMemory = 4,
    errTargetFileNotFound = 5,
    errParam = 6,
    errHardwareNotFound = 7,
    errFunctionCall = 8,
    errOutOfSpace = 9,
    errScanChannelFail = 10,
    errChannelNotValid = 11,
    errEtc = 12,
    errFilterCreationFailed = 13,
    errInvalidHwnd = 14,
    errIntefaceCreationFailed = 15,
    errFilterAdditionFailed = 16,
    errGetKsProperty = 17,
    errDecoderRenderingFailed = 18,
    errGraphCreationFailed = 19,
    errVideoWindowCreationFailed = 20,
    errMotionNotApplicable = 21,
    errDirectShowStart = 22,
    errDirectShowFailed = 23,
    errDirectShowCannotRender = 24,
    errQueryFilterInterfaceFailed = 25,
    errInitTimeDelayFailed = 26,
    errInitProxyFailed = 27,
    errUsbFilterFailed = 28,
    errInitDumpFailed = 29,
    errConnectToTimeDelayFailed = 30,
    errConnectPinToFilterFailed = 31,
    errCannotFindPin = 32,
    errCannotFindPinSubTypes = 33,
    errStreamNoVsync = 34,
    errUnknownStreamError = 35,
    errRegValueAccessFailed = 36,
    errRegKeyOpenFailed = 37,
    errSoundCardNotFound = 38,
    errSoundCardAlreadyUsed = 39,
    errAudioRenderingFailed = 40,
    errOverlayConnectFailed = 41,
    errWinDvdOrPowerDvdNotFound = 42,
    errWinDvdNotFound = 43,
    errPowerDvdNotFound = 44,
    errAuthenticationFailed = 45,
    errLowDxVersion = 46
}TxErrorCode;
enum
{
    encEventMasterQuality = 0,
    encEventVideoRate = 1,
    encEventResolution = 2,
    encEventEncodingMode = 3,
    encEventVariance = 4,
    encEventAudioRate = 5,
    encEventTvFormat = 6,
    encEventFile = 7,
    encEventAudioSamplingRate = 8
}TxEncEventCode;
enum
{
    signalCatv = 0,
    signalAir = 1,
    signalTDmb = 2
}TxSignalType;
enum
{
    tmodeAnalogTv = 0,
    tmodeATSC = 1
}TxTunerMode;
enum
{
    vchmodNone = 0,
    vchmodAnalog = 1,
    vchmod64QAM = 2,
    vchmod256QAM = 3,
    vchmod8Vsb = 4,
    vchmod16VSB = 5,
    vchmodAll = 6
}TxDtvModulation;
enum
{
    anaDeinterlaceNone = 0,
    anaDeinterlaceSoft = 1,
    anaDeinterlaceSharp = 2
}TxAnalogDeinterlace;
enum
{
    mtsMono = 0,
    mtsStereo = 1,
    mtsLangA = 2,
    mtsLangB = 3,
    mtsLangC = 4
}TxMultiSound;
enum
{
    ftypeTS = 0,
    ftypePS = 1,
    ftypeAVI = 2,
    ftypeWMV = 3,
    ftypeDvrms = 4
}TxFileType;
enum
{
    ratioStretched = 0,
    ratioOriginal = 1,
    ratio16x9Fixed = 2,
    ratio4x3Fixed = 3,
    ratio4x3PanScan = 4,
    ratio16x10Fixed = 5
}TxAspectRatio;
enum
{
    avProfMPEG1 = 0,
    avProfMPEG2 = 1,
    avProfVcdNTSC = 2,
    avProfVcdPAL = 3,
    avProfVcdFLIM = 4,
    avProfNA1 = 5,
    avProfDvdNTSC = 6,
    avProfDvdPAL = 7,
    avProfSvcdNTSC = 8,
    avProfSvcdPAL = 9,
    avProfDivxNTSC = 10,
    avProfDivxPAL = 11,
    avProfNone = 12,
    avProfWMV = 13,
    avProfAVI = 14
}TxAvProfile;
enum
{
    avFr24_1001 = 0,
    avFr24_1000 = 1,
    avFr25_1000 = 2,
    avFr30_1001 = 3,
    avFr30_1000 = 4
}TxAvFrameRate;
enum
{
    doutStereo = 0,
    dout51ch = 1,
    doutSpdifDSound = 2,
    doutSpdifWaveOut = 3
}TxDigitalAudioOutput;
enum
{
    deinterlaceBob = 0,
    deinterlaceWeave = 1,
    deinterlaceAuto = 2,
    deinterlaceAuto1 = 3,
    deinterlaceNone1 = 4,
    deinterlaceBob1 = 5,
    deinterlaceSoftware1 = 6
}TxDeinterlace;
enum
{
    decNormal = 0,
    decHigh = 1,
    decHighest = 2
}TxDecoderQuality;
enum
{
    pip = 0,
    windows4 = 1,
    all_view = 2
}TxMultiviewType;
enum
{
    right_bottom = 0,
    left_bottom = 1,
    left_top = 2,
    right_top = 3
}TxPipArrangedPosition;
enum
{
    sizeLarge = 0,
    sizeNormal = 1,
    sizeSmall = 2,
    sizeVerySmall = 3
}TxPipArrangedSize;
enum
{
    htKeyword = 0,
    htContext = 1
}TxHelpType;


// 작업입니다.
public:

// IPvrHD103

// Functions
//

	long get_TuneSpace()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	CString get_AnalogAirChannels()
	{
		CString result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_AnalogCatvChannels()
	{
		CString result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_DigitalAirChannels()
	{
		CString result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_DigitalCatvChannels()
	{
		CString result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long get_NumOfVirtualChannels()
	{
		long result;
		InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_PmtPid()
	{
		long result;
		InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PmtPid(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_WallPaper()
	{
		BOOL result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_WallPaper(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	unsigned long get_AudioChannel()
	{
		unsigned long result;
		InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	void put_SpdifOutput(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ColorReverse()
	{
		BOOL result;
		InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ColorReverse(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_RestoreDisplay(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_RefreshRate(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_ActiveMovieWindow()
	{
		long result;
		InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ActiveMovieWindow(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_MultiPipWindow()
	{
		long result;
		InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DisplayFrameWindow(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long GetQualifiedBytesPerSecond(long quality1)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms, quality1);
		return result;
	}
	long GetMaxRecordSeconds(long quality1)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms, quality1);
		return result;
	}
	void Postroll(long sleepMilli)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms, sleepMilli);
	}
	void InitVideoParameters(long brightness1, long contrast1, long hue1, long saturation1, long sharpness1, long gamma1)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, parms, brightness1, contrast1, hue1, saturation1, sharpness1, gamma1);
	}
	void StopLiveRecord()
	{
		InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	long GetMaxVariance(long videoKbps1)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms, videoKbps1);
		return result;
	}
	void UnFreeze()
	{
		InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ClearErrorCode()
	{
		InvokeHelper(0x1b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	CString GetQualifiedExtension(long quality1)
	{
		CString result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x21, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, quality1);
		return result;
	}
	BOOL IsChannelSync(long channel1)
	{
		BOOL result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x25, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, channel1);
		return result;
	}
	BOOL IsValidChannel(long channel1)
	{
		BOOL result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x26, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, channel1);
		return result;
	}
	void SetAvPid(long videoPid, long audioPid)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x27, DISPATCH_METHOD, VT_EMPTY, NULL, parms, videoPid, audioPid);
	}
	void SetPremapAvPid(long videoPid, long audioPid)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x28, DISPATCH_METHOD, VT_EMPTY, NULL, parms, videoPid, audioPid);
	}
	void GetAvPid(long * videoPid, long * audioPid)
	{
		static BYTE parms[] = VTS_PI4 VTS_PI4 ;
		InvokeHelper(0x29, DISPATCH_METHOD, VT_EMPTY, NULL, parms, videoPid, audioPid);
	}
	void SetTeletextPid(long ttxPid, long subPid)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x2a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, ttxPid, subPid);
	}
	void StartSignalStrength(long channel1)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x2b, DISPATCH_METHOD, VT_EMPTY, NULL, parms, channel1);
	}
	void StopSignalStrength()
	{
		InvokeHelper(0x2c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void GetStationPid(long channel1, long * videoPid, long * audioPid)
	{
		static BYTE parms[] = VTS_I4 VTS_PI4 VTS_PI4 ;
		InvokeHelper(0x2d, DISPATCH_METHOD, VT_EMPTY, NULL, parms, channel1, videoPid, audioPid);
	}
	void SaveMappedAvPid(BOOL isAir, long channel1, long avPid)
	{
		static BYTE parms[] = VTS_BOOL VTS_I4 VTS_I4 ;
		InvokeHelper(0x2e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, isAir, channel1, avPid);
	}
	long GetMappedAvPid(long channel1)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x2f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, channel1);
		return result;
	}
	void ClearMappedPidArray(BOOL isAir)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x30, DISPATCH_METHOD, VT_EMPTY, NULL, parms, isAir);
	}
	void HandleVirtualChannel()
	{
		InvokeHelper(0x38, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	unsigned long ReadTunerProductId()
	{
		unsigned long result;
		InvokeHelper(0x43, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	void SetTsInterface(long tsIface)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x44, DISPATCH_METHOD, VT_EMPTY, NULL, parms, tsIface);
	}
	BOOL AccessEeprom64k(BOOL eraseIt, unsigned short productId, unsigned short productCode, unsigned long serialNumber)
	{
		BOOL result;
		static BYTE parms[] = VTS_BOOL VTS_UI2 VTS_UI2 VTS_UI4 ;
		InvokeHelper(0x45, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, eraseIt, productId, productCode, serialNumber);
		return result;
	}
	unsigned long GetDeviceIndex()
	{
		unsigned long result;
		InvokeHelper(0x46, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	void SetDeviceIndex(unsigned long currIndex)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x47, DISPATCH_METHOD, VT_EMPTY, NULL, parms, currIndex);
	}
	BOOL GetIsExistMsVDecorder()
	{
		BOOL result;
		InvokeHelper(0x48, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	unsigned long GetZulu2DeviceCapabilitesExt()
	{
		unsigned long result;
		InvokeHelper(0x49, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	BOOL GetDxva()
	{
		BOOL result;
		InvokeHelper(0x4a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void SetDxva(BOOL dxva)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x4b, DISPATCH_METHOD, VT_EMPTY, NULL, parms, dxva);
	}
	unsigned long GetCodecCapabilities(long videoDecoderIndex, long dxva)
	{
		unsigned long result;
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x4c, DISPATCH_METHOD, VT_UI4, (void*)&result, parms, videoDecoderIndex, dxva);
		return result;
	}
	unsigned long GetVideoRenderer()
	{
		unsigned long result;
		InvokeHelper(0x4d, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	void SetD3dDevice(unsigned long d3dDevice)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x4e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, d3dDevice);
	}
	void SetMessageId(unsigned long messageId)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x4f, DISPATCH_METHOD, VT_EMPTY, NULL, parms, messageId);
	}
	unsigned long GetAllDeviceInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0x50, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long GetMixerInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0x51, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long GetVirtualChannelInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0x52, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long GetWmvProfileInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0x53, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long GetVideoCompressorInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0x54, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long GetAudioCompressorInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0x55, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	BOOL GetHonestechEncoderExist()
	{
		BOOL result;
		InvokeHelper(0x56, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	unsigned long GetOemFilter1Ptr()
	{
		unsigned long result;
		InvokeHelper(0x57, DISPATCH_METHOD, VT_UI4, (void*)&result, NULL);
		return result;
	}
	void ResetPlayback(BOOL reset)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x58, DISPATCH_METHOD, VT_EMPTY, NULL, parms, reset);
	}
	BOOL get_IsDmbStreamFormat()
	{
		BOOL result;
		InvokeHelper(0x5b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsSBEEngine()
	{
		BOOL result;
		InvokeHelper(0x5c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DmbService(unsigned long newValue)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x5d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_DmbReset(unsigned long newValue)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x5e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_PreviewWindow()
	{
		long result;
		InvokeHelper(0x5f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PreviewWindow(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x5f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_NotifyWindow()
	{
		long result;
		InvokeHelper(0x60, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_NotifyWindow(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x60, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Source()
	{
		long result;
		InvokeHelper(0x61, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Source(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x61, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_RecTarget()
	{
		long result;
		InvokeHelper(0x62, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_RecTarget(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x62, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_ViewTarget()
	{
		long result;
		InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ViewTarget(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x63, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_CursorHidden()
	{
		BOOL result;
		InvokeHelper(0x64, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_CursorHidden(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x64, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_OverlayRunning()
	{
		BOOL result;
		InvokeHelper(0x65, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_Channel()
	{
		long result;
		InvokeHelper(0x66, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Channel(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x66, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Channel2Go()
	{
		long result;
		InvokeHelper(0x67, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Channel2Go(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x67, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_Enabled()
	{
		BOOL result;
		InvokeHelper(DISPID_ENABLED, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_Enabled(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(DISPID_ENABLED, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Motion()
	{
		long result;
		InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_Rate()
	{
		long result;
		InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Rate(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_AudioChangeSmapling(unsigned long newValue)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_AudioCompresionMode(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_OverlayAvailable()
	{
		BOOL result;
		InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_XpServicePackVersion()
	{
		long result;
		InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsVMRRenderer()
	{
		BOOL result;
		InvokeHelper(0x6e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_IsVMRRenderer(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x6e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AudioRenderer()
	{
		long result;
		InvokeHelper(0x6f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AudioRenderer(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x6f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_CancelAlarm(unsigned long newValue)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x70, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_TvFormat()
	{
		long result;
		InvokeHelper(0x71, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_TvFormat(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x71, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_MasterQuality()
	{
		long result;
		InvokeHelper(0x72, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_MasterQuality(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x72, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_VideoResolution()
	{
		long result;
		InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_VideoResolution(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_EncodingMode()
	{
		long result;
		InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_EncodingMode(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_VideoKbps()
	{
		long result;
		InvokeHelper(0x75, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_VideoKbps(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x75, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_VideoVariance()
	{
		long result;
		InvokeHelper(0x76, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_VideoVariance(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x76, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_DiscFormat()
	{
		long result;
		InvokeHelper(0x77, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DiscFormat(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x77, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AudioRate()
	{
		long result;
		InvokeHelper(0x78, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AudioRate(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x78, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AudioSamplingRate()
	{
		long result;
		InvokeHelper(0x79, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AudioSamplingRate(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x79, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AudioInputGain()
	{
		long result;
		InvokeHelper(0x7a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AudioInputGain(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x7a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_BytesPerSecond()
	{
		long result;
		InvokeHelper(0x7b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_MaxKbps()
	{
		long result;
		InvokeHelper(0x7c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_Mpeg1OptionalVideoKbps()
	{
		long result;
		InvokeHelper(0x7d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Mpeg1OptionalVideoKbps(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x7d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Mpeg1OptionalAudioKbps()
	{
		long result;
		InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Mpeg1OptionalAudioKbps(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_StillFormat()
	{
		long result;
		InvokeHelper(0x7f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_StillFormat(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x7f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_StillExtension()
	{
		CString result;
		InvokeHelper(0x80, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsAlive()
	{
		BOOL result;
		InvokeHelper(0x81, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsMPEG1()
	{
		BOOL result;
		InvokeHelper(0x82, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsNonTrickableFile()
	{
		BOOL result;
		InvokeHelper(0x83, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_Oem()
	{
		long result;
		InvokeHelper(0x84, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Oem(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x84, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsUsb()
	{
		BOOL result;
		InvokeHelper(0x85, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_IsUsb(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x85, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsHighSpeedUsb()
	{
		BOOL result;
		InvokeHelper(0x86, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_TunerType()
	{
		long result;
		InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	BOOL get_TunerExists()
	{
		BOOL result;
		InvokeHelper(0x88, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsLive()
	{
		BOOL result;
		InvokeHelper(0x89, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsFilePlay()
	{
		BOOL result;
		InvokeHelper(0x8a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsRecording()
	{
		BOOL result;
		InvokeHelper(0x8b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsRecordingOnly()
	{
		BOOL result;
		InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsLivePlaying()
	{
		BOOL result;
		InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsLivePlaybackOnly()
	{
		BOOL result;
		InvokeHelper(0x8e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsPreview()
	{
		BOOL result;
		InvokeHelper(0x8f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_IsPreview(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x8f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsFastRecordable()
	{
		BOOL result;
		InvokeHelper(0x90, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_DllVersion()
	{
		long result;
		InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	CString get_FirmwareVersion()
	{
		CString result;
		InvokeHelper(0x92, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_DriverVersion()
	{
		CString result;
		InvokeHelper(0x93, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long get_PrevSource()
	{
		long result;
		InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PrevSource(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_WinVersion()
	{
		long result;
		InvokeHelper(0x95, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsDigitalMode()
	{
		BOOL result;
		InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsSupportQAM()
	{
		BOOL result;
		InvokeHelper(0x97, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	CString get_serialNumber()
	{
		CString result;
		InvokeHelper(0x98, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_IsFullScreen(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x99, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsNativeMpeg()
	{
		BOOL result;
		InvokeHelper(0x9a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsDependent()
	{
		BOOL result;
		InvokeHelper(0x9b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_IsDependent(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x9b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Event()
	{
		long result;
		InvokeHelper(0x9c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_ErrorCode()
	{
		long result;
		InvokeHelper(0x9d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_DetailedErrorCode()
	{
		long result;
		InvokeHelper(0x9e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_ScanEventCode()
	{
		long result;
		InvokeHelper(0x9f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	unsigned long get_PvrEventCode()
	{
		unsigned long result;
		InvokeHelper(0xa0, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	long get_EncEventCode()
	{
		long result;
		InvokeHelper(0xa1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_StreamLength()
	{
		long result;
		InvokeHelper(0xa2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_StreamPosition()
	{
		long result;
		InvokeHelper(0xa3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_StreamPosition(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xa3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_LastStreamLength()
	{
		long result;
		InvokeHelper(0xa4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_LastStreamPosition()
	{
		long result;
		InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_StreamLengthKbytes()
	{
		long result;
		InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_StreamPosKbytes()
	{
		long result;
		InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_StreamPosKbytes(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_StreamGapKbytes()
	{
		long result;
		InvokeHelper(0xa8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_AbsStartPosition()
	{
		long result;
		InvokeHelper(0xa9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_AbsEndPosition()
	{
		long result;
		InvokeHelper(0xaa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_AbsStreamPosition()
	{
		long result;
		InvokeHelper(0xab, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AbsStreamPosition(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xab, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_PausedStreamPosition()
	{
		long result;
		InvokeHelper(0xac, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PausedStreamPosition(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xac, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsPositionPaused()
	{
		BOOL result;
		InvokeHelper(0xad, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsScrubMode()
	{
		BOOL result;
		InvokeHelper(0xae, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_IsScrubMode(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xae, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsAirSignal()
	{
		BOOL result;
		InvokeHelper(0xaf, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_IsAirSignal(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xaf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_SignalType()
	{
		long result;
		InvokeHelper(0xb0, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_SignalType(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xb0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_CountryCode()
	{
		long result;
		InvokeHelper(0xb1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_CountryCode(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xb1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_MaxChannel()
	{
		long result;
		InvokeHelper(0xb2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_MinChannel()
	{
		long result;
		InvokeHelper(0xb3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_TunerMode()
	{
		long result;
		InvokeHelper(0xb4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_TunerMode(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xb4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsDTV()
	{
		BOOL result;
		InvokeHelper(0xb5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_SignalStrength()
	{
		long result;
		InvokeHelper(0xb6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_LastSignalStrength()
	{
		long result;
		InvokeHelper(0xb7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_SignalSnrDecibel()
	{
		long result;
		InvokeHelper(0xb8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_TunerFrequency()
	{
		long result;
		InvokeHelper(0xb9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_TunerFrequency(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xb9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_TunerBandwidth()
	{
		long result;
		InvokeHelper(0xba, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ScanModulations(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xbb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AnalogDeinterlace()
	{
		long result;
		InvokeHelper(0xbc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AnalogDeinterlace(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xbc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AudioMixerIndex()
	{
		long result;
		InvokeHelper(0xbd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AudioMixerIndex(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xbd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_AudioMixerMute(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xbe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_TimedMixerMute(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xbf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_RouteMute(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xc0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_AudioMixerAttenuation(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xc1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_Support88xAudio(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xc2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_AudioCable(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xc3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_MtsCapability()
	{
		long result;
		InvokeHelper(0xc4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_TvMultiSoundAvailables()
	{
		long result;
		InvokeHelper(0xc5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_TvMultiSound()
	{
		long result;
		InvokeHelper(0xc6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_TvMultiSound(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xc6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_RepeatFilePlay(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xc7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_AudioInfoVolumeRestore()
	{
		BOOL result;
		InvokeHelper(0xc8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_AudioInfoVolumeRestore(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xc8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ScanNow()
	{
		BOOL result;
		InvokeHelper(0xc9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ScanNow(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xc9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsScanFinished()
	{
		BOOL result;
		InvokeHelper(0xca, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_CurrentScanningChannel()
	{
		long result;
		InvokeHelper(0xcb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsChannelLocked()
	{
		BOOL result;
		InvokeHelper(0xcc, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_ScanSignalStrength()
	{
		long result;
		InvokeHelper(0xcd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_EpgScanChannel(unsigned long newValue)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0xce, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_EpgScan()
	{
		BOOL result;
		InvokeHelper(0xcf, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_EpgScan(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xcf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_ScanChannel(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_RecMbytesPerFile()
	{
		long result;
		InvokeHelper(0xd1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_RecMbytesPerFile(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_PauseBufferSize()
	{
		long result;
		InvokeHelper(0xd2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PauseBufferSize(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_PauseFileName(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xd3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_PauseBufferMinutes()
	{
		long result;
		InvokeHelper(0xd4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PauseBufferMinutes(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_PauseBufferSeconds()
	{
		long result;
		InvokeHelper(0xd5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PauseBufferSeconds(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_PauseBufferPath()
	{
		CString result;
		InvokeHelper(0xd6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_PauseBufferPath(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0xd6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_PlayFileName()
	{
		CString result;
		InvokeHelper(0xd7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_RecFileName()
	{
		CString result;
		InvokeHelper(0xd8, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long get_FileSrcType()
	{
		long result;
		InvokeHelper(0xd9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_FileSrcType(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_FileRecType()
	{
		long result;
		InvokeHelper(0xda, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_FileRecType(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xda, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	double get_RecPrerollTime()
	{
		double result;
		InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
		return result;
	}
	long get_DiskFreeKBytes()
	{
		long result;
		InvokeHelper(0xdc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	CString get_RecExtension()
	{
		CString result;
		InvokeHelper(0xdd, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_LstExtension()
	{
		CString result;
		InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long get_Brightness()
	{
		long result;
		InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Brightness(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Contrast()
	{
		long result;
		InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Contrast(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Saturation()
	{
		long result;
		InvokeHelper(0xe1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Saturation(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Hue()
	{
		long result;
		InvokeHelper(0xe2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Hue(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Sharpness()
	{
		long result;
		InvokeHelper(0xe3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Sharpness(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Gamma()
	{
		long result;
		InvokeHelper(0xe4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Gamma(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AspectRatio()
	{
		long result;
		InvokeHelper(0xe5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AspectRatio(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_DesktopResolutionChange(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xe6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Volume()
	{
		long result;
		InvokeHelper(0xe7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Volume(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_Mute()
	{
		BOOL result;
		InvokeHelper(0xe8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_Mute(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xe8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AvRecProfile()
	{
		long result;
		InvokeHelper(0xe9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AvRecProfile(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AvRecProfileOrg()
	{
		long result;
		InvokeHelper(0xea, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_AvRecBitrate()
	{
		long result;
		InvokeHelper(0xeb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AvRecBitrate(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xeb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AvRecFrameRate()
	{
		long result;
		InvokeHelper(0xec, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AvRecFrameRate(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xec, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AvRecQuality()
	{
		long result;
		InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AvRecQuality(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AvRecWidth()
	{
		long result;
		InvokeHelper(0xee, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AvRecWidth(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xee, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AvRecHeight()
	{
		long result;
		InvokeHelper(0xef, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AvRecHeight(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xef, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_AvAudioRecordLevel(double newValue)
	{
		static BYTE parms[] = VTS_R8 ;
		InvokeHelper(0xf0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AvAudioRecordBitrate()
	{
		long result;
		InvokeHelper(0xf1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AvAudioRecordBitrate(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xf1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_HonestechEncoderExist()
	{
		BOOL result;
		InvokeHelper(0xf2, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsHonestechEncoder()
	{
		BOOL result;
		InvokeHelper(0xf3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	unsigned long get_WmvProfileInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0xf4, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	long get_WwvProfileIndex()
	{
		long result;
		InvokeHelper(0xf5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_WwvProfileIndex(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xf5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	unsigned long get_VideoCompressorInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0xf6, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	long get_VideoCompressorIndex()
	{
		long result;
		InvokeHelper(0xf7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_VideoCompressorIndex(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xf7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	unsigned long get_AudioCompressorInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0xf8, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	long get_AudioCompressorIndex()
	{
		long result;
		InvokeHelper(0xf9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AudioCompressorIndex(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xf9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_DtvModulation()
	{
		long result;
		InvokeHelper(0xfa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DtvModulation(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xfa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_RfAttenuation(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xfb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_DigitalAudioOutput()
	{
		long result;
		InvokeHelper(0xfc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DigitalAudioOutput(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xfc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_DigitalAudioLanguage()
	{
		long result;
		InvokeHelper(0xfd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DigitalAudioLanguage(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xfd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_NumOfAudioLanguages()
	{
		long result;
		InvokeHelper(0xfe, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_DigitalDeinterlace()
	{
		long result;
		InvokeHelper(0xff, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DigitalDeinterlace(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xff, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_DvhsExists()
	{
		BOOL result;
		InvokeHelper(0x100, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_SpdifExists()
	{
		BOOL result;
		InvokeHelper(0x101, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_ProgramNumber()
	{
		long result;
		InvokeHelper(0x102, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ProgramNumber(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x102, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_SubChannelRecord(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x103, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_CaptureNativeMpeg(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x104, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_LastProgramNumberParam()
	{
		long result;
		InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PreProgramNumber(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_MinorCh(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x107, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_ProgramRecordStart(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x108, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_ProgramRecordStop(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x109, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_RecordFilename(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_ProgramAbsStartPosition()
	{
		long result;
		InvokeHelper(0x10b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_ProgramAbsEndPosition()
	{
		long result;
		InvokeHelper(0x10c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_videoDecoderIndex()
	{
		long result;
		InvokeHelper(0x10d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_videoDecoderIndex(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x10d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AudioDecoderIndex()
	{
		long result;
		InvokeHelper(0x10e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AudioDecoderIndex(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x10e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_QualityOfFullSwDecoder(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x10f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_AudioDecoderType(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x110, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_VideoDecoderResolution(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x111, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_TeletextWindow(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x112, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_TeletextBackColor(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x113, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_TeletextEnabled(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x114, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_TeletextBoldFontEnabled(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x115, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_TeletextAlphaBlending(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x116, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_TeletextInitialPage()
	{
		long result;
		InvokeHelper(0x117, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_TeletextPage()
	{
		long result;
		InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_TeletextPage(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_TeletextTimeout(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x119, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_SubtitleWindow(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x11a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_SubtitleBackColor(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x11b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_SubtitleEnabled(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x11c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_SubtitlePage()
	{
		long result;
		InvokeHelper(0x11d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_SubtitlePage(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x11d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_SubTitleDelay(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x11e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_TeletextSubType()
	{
		BOOL result;
		InvokeHelper(0x11f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_MultiviewType()
	{
		long result;
		InvokeHelper(0x120, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_MultiviewType(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x120, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_MultiviewOn()
	{
		BOOL result;
		InvokeHelper(0x121, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_MultiviewOn(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x121, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsMultiUseable()
	{
		BOOL result;
		InvokeHelper(0x122, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_MultiviewNextPage(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x123, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_PipProgramNumber()
	{
		long result;
		InvokeHelper(0x124, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PipProgramNumber(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x124, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_PipArrangedPos(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x125, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_PipArrangedSize(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x126, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_PipToggle(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x127, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	unsigned long get_DeviceCapabilitesExt()
	{
		unsigned long result;
		InvokeHelper(0x128, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long get_Zulu2DeviceCapabilitesExt()
	{
		unsigned long result;
		InvokeHelper(0x129, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	BOOL get_dxva()
	{
		BOOL result;
		InvokeHelper(0x12a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_dxva(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x12a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	unsigned long get_DeviceIndex()
	{
		unsigned long result;
		InvokeHelper(0x12b, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	void put_DeviceIndex(unsigned long newValue)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x12b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsExistMsVDecorder()
	{
		BOOL result;
		InvokeHelper(0x12c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	unsigned long get_VideoRenderer()
	{
		unsigned long result;
		InvokeHelper(0x12d, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	void put_d3dDevice(unsigned long newValue)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x12e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_messageId(unsigned long newValue)
	{
		static BYTE parms[] = VTS_UI4 ;
		InvokeHelper(0x12f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Capabilities()
	{
		long result;
		InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_OemDevice(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x131, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	unsigned long get_DevicePtr()
	{
		unsigned long result;
		InvokeHelper(0x132, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long get_MixerInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0x133, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long get_VirtualChannelInfoPtr()
	{
		unsigned long result;
		InvokeHelper(0x134, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	unsigned long get_OemFilter1Ptr()
	{
		unsigned long result;
		InvokeHelper(0x135, DISPATCH_PROPERTYGET, VT_UI4, (void*)&result, NULL);
		return result;
	}
	void put_reset(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x136, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long Activate()
	{
		long result;
		InvokeHelper(0x137, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	void Deactivate()
	{
		InvokeHelper(0x138, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Play()
	{
		InvokeHelper(0x139, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Pause()
	{
		InvokeHelper(0x13a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Stop()
	{
		InvokeHelper(0x13b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void StartRecord(LPCTSTR fileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13c, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fileName);
	}
	void PlayAndRecord(LPCTSTR fileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13d, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fileName);
	}
	void PlayFile(LPCTSTR fileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x13e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fileName);
	}
	void PlayMpegFile(LPCTSTR fileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x140, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fileName);
	}
	void PlayAviFile(LPCTSTR fileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x141, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fileName);
	}
	void PlayWmvFile(LPCTSTR fileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x142, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fileName);
	}
	void PlayDvrmsFile(LPCTSTR fileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x143, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fileName);
	}
	void StillCapture(LPCTSTR fileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x144, DISPATCH_METHOD, VT_EMPTY, NULL, parms, fileName);
	}
	void GetDshowEventParam(unsigned long * param1, unsigned long * param2)
	{
		static BYTE parms[] = VTS_PUI4 VTS_PUI4 ;
		InvokeHelper(0x145, DISPATCH_METHOD, VT_EMPTY, NULL, parms, param1, param2);
	}
	void SetVideoRect(long left, long top, long width, long height)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x146, DISPATCH_METHOD, VT_EMPTY, NULL, parms, left, top, width, height);
	}
	void SetCropRect(long left, long top, long right, long bottom)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x147, DISPATCH_METHOD, VT_EMPTY, NULL, parms, left, top, right, bottom);
	}
	void SetFullScreenRect(long left1, long top1, long right1, long bottom1)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x148, DISPATCH_METHOD, VT_EMPTY, NULL, parms, left1, top1, right1, bottom1);
	}
	void SetSatelliteInfo(long symbolRate, long inversionType, long lnbLoFreq, long lnbHiFreq, long lnbSwitchFreq, long lnbPolarity, long lnbToneOn)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x149, DISPATCH_METHOD, VT_EMPTY, NULL, parms, symbolRate, inversionType, lnbLoFreq, lnbHiFreq, lnbSwitchFreq, lnbPolarity, lnbToneOn);
	}
	void SetDiseqcPort(long portNum, long lnbPolarity, long lnbToneOn)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x14b, DISPATCH_METHOD, VT_EMPTY, NULL, parms, portNum, lnbPolarity, lnbToneOn);
	}
	void UpdateTeletext(long bcdMagazine, long bcdPage)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x14e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, bcdMagazine, bcdPage);
	}
	void FlushTeletext()
	{
		InvokeHelper(0x14f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SelectTeletextByPosition(long x, long y)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x150, DISPATCH_METHOD, VT_EMPTY, NULL, parms, x, y);
	}
	void UpdateSubtitle(long bcdComposition, long bcdAncillary)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x151, DISPATCH_METHOD, VT_EMPTY, NULL, parms, bcdComposition, bcdAncillary);
	}
	void SetSourceRect(long left1, long top1, long right1, long bottom1)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4 ;
		InvokeHelper(0x152, DISPATCH_METHOD, VT_EMPTY, NULL, parms, left1, top1, right1, bottom1);
	}
	BOOL get_DoubleBuffered()
	{
		BOOL result;
		InvokeHelper(0x153, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DoubleBuffered(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x153, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_AlignDisabled()
	{
		BOOL result;
		InvokeHelper(0x154, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_VisibleDockClientCount()
	{
		long result;
		InvokeHelper(0x155, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long DrawTextBiDiModeFlagsReadingOnly()
	{
		long result;
		InvokeHelper(0x157, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
		return result;
	}
	void InitiateAction()
	{
		InvokeHelper(0x158, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	BOOL IsRightToLeft()
	{
		BOOL result;
		InvokeHelper(0x159, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL UseRightToLeftReading()
	{
		BOOL result;
		InvokeHelper(0x15c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL UseRightToLeftScrollBar()
	{
		BOOL result;
		InvokeHelper(0x15d, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_Visible()
	{
		BOOL result;
		InvokeHelper(0x15e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_Visible(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x15e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	short get_Cursor()
	{
		short result;
		InvokeHelper(0x15f, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
		return result;
	}
	void put_Cursor(short newValue)
	{
		static BYTE parms[] = VTS_I2 ;
		InvokeHelper(0x15f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_HelpType()
	{
		long result;
		InvokeHelper(0x160, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_HelpType(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x160, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_HelpKeyword()
	{
		CString result;
		InvokeHelper(0x161, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_HelpKeyword(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x161, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void SetSubComponent(BOOL IsSubComponent)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x163, DISPATCH_METHOD, VT_EMPTY, NULL, parms, IsSubComponent);
	}

// Properties
//



};
