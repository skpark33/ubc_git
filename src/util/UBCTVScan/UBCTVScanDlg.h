// UBCTVScanDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


#include "TVPlayer.h"
#include "ProgressDialog.h"


// CUBCTVScanDlg 대화 상자
class CUBCTVScanDlg : public CDialog
{
// 생성입니다.
public:
	CUBCTVScanDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCTVSCAN_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()

	CTVPlayer			m_player;
	CProgressDialog		m_wndProgress;

	int		m_nOriginalWidth;
	int		m_nExpandWidth;
	bool	m_bExpand;

	bool	m_bScanning;
	bool	m_bChannelChanged;

	CUIntArray	m_listIncompleteScanSignal;
	CUIntArray	m_listCompleteScanSignal;
	int			m_nTotalChannelCount;
	TV_MODE		m_eScanningSignal;

	BOOL		CreateTVPlayer();

	void		ResetChannelList();

	void		NextScanSignal();
	int			GetCurrentChannelCount();
	int			GetCompleteChannelCount();

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedCheckScanSignal();
	afx_msg void OnBnClickedButtonScan();
	afx_msg void OnBnClickedButtonSave();

	afx_msg void OnStnClickedStaticFolding();
	afx_msg void OnNMDblclkListChannel(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownListChannel(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg LRESULT OnAddChannelInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeChannelInfo(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnChangeScanChannel(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCompleteScanChannel(WPARAM wParam, LPARAM lParam);

	CComboBox	m_cbxCountry;
	CComboBox	m_cbxModulation;
	CButton		m_chkAnalogAir;
	CButton		m_chkDigitalAir;
	CButton		m_chkAnalogCatv;
	CButton		m_chkDigitalCatv;
	CListCtrl	m_lcChannel;
	CButton		m_btnScan;
	CButton		m_btnSave;
	CButton		m_btnClose;

	BOOL		LoadChannelList();

};
