// UBCTVScanDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "UBCTVScan.h"
#include "UBCTVScanDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



#define		TIMER_NEXT_SCAN_SIGNAL_ID			1028
#define		TIMER_NEXT_SCAN_SIGNAL_TIME			(100)

#define		TIMER_EXPAND_ID						1029
#define		TIMER_EXPAND_TIME					(10)

#define		TIMER_REDUCE_ID						1030
#define		TIMER_REDUCE_TIME					(10)

#define		EXPAND_REDUCE_STEP					25


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CUBCTVScanDlg 대화 상자




CUBCTVScanDlg::CUBCTVScanDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCTVScanDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bScanning = false;

	m_bExpand = false;

	m_bChannelChanged = false;
}

void CUBCTVScanDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_COUNTRY, m_cbxCountry);
	DDX_Control(pDX, IDC_COMBO_MODULATION, m_cbxModulation);
	DDX_Control(pDX, IDC_CHECK_ANALOG_AIR, m_chkAnalogAir);
	DDX_Control(pDX, IDC_CHECK_DIGITAL_AIR, m_chkDigitalAir);
	DDX_Control(pDX, IDC_CHECK_ANALOG_CATV, m_chkAnalogCatv);
	DDX_Control(pDX, IDC_CHECK_DIGITAL_CATV, m_chkDigitalCatv);
	DDX_Control(pDX, IDC_LIST_CHANNEL, m_lcChannel);
	DDX_Control(pDX, IDC_BUTTON_SCAN, m_btnScan);
	DDX_Control(pDX, IDC_BUTTON_SAVE, m_btnSave);
	DDX_Control(pDX, IDCANCEL, m_btnClose);
}


BEGIN_MESSAGE_MAP(CUBCTVScanDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CHECK_ANALOG_AIR, &CUBCTVScanDlg::OnBnClickedCheckScanSignal)
	ON_BN_CLICKED(IDC_CHECK_ANALOG_CATV, &CUBCTVScanDlg::OnBnClickedCheckScanSignal)
	ON_BN_CLICKED(IDC_CHECK_DIGITAL_AIR, &CUBCTVScanDlg::OnBnClickedCheckScanSignal)
	ON_BN_CLICKED(IDC_CHECK_DIGITAL_CATV, &CUBCTVScanDlg::OnBnClickedCheckScanSignal)
	ON_BN_CLICKED(IDC_BUTTON_SCAN, &CUBCTVScanDlg::OnBnClickedButtonScan)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CUBCTVScanDlg::OnBnClickedButtonSave)
	ON_MESSAGE(WM_ADD_CHANNEL_INFO, OnAddChannelInfo)
	ON_MESSAGE(WM_CHANGE_CHANNEL_INFO, OnChangeChannelInfo)
	ON_MESSAGE(WM_CHANGE_SCAN_CHANNEL, OnChangeScanChannel)
	ON_MESSAGE(WM_COMPLETE_SCAN_CHANNEL, OnCompleteScanChannel)
	ON_STN_CLICKED(IDC_STATIC_FOLDING, &CUBCTVScanDlg::OnStnClickedStaticFolding)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CHANNEL, &CUBCTVScanDlg::OnNMDblclkListChannel)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_CHANNEL, &CUBCTVScanDlg::OnLvnKeydownListChannel)
END_MESSAGE_MAP()


// CUBCTVScanDlg 메시지 처리기

void CUBCTVScanDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCTVScanDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

void CUBCTVScanDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnTimer(nIDEvent);

	if(nIDEvent == TIMER_NEXT_SCAN_SIGNAL_ID)
	{
		KillTimer(TIMER_NEXT_SCAN_SIGNAL_ID);

		NextScanSignal();
	}
	else if(nIDEvent == TIMER_EXPAND_ID)
	{
		CRect rect;
		GetWindowRect(rect);
		rect.right += EXPAND_REDUCE_STEP;
		if(rect.Width() > m_nExpandWidth)
		{
			rect.right = rect.left + m_nExpandWidth;
			KillTimer(TIMER_EXPAND_ID);
		}
		MoveWindow(rect);
	}
	else if(nIDEvent == TIMER_REDUCE_ID)
	{
		CRect rect;
		GetWindowRect(rect);
		rect.right -= EXPAND_REDUCE_STEP;
		if(rect.Width() < m_nOriginalWidth)
		{
			rect.right = rect.left + m_nOriginalWidth;
			KillTimer(TIMER_REDUCE_ID);
		}
		MoveWindow(rect);
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCTVScanDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CUBCTVScanDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	//
	m_wndProgress.Create(IDD_DIALOG_PROGRESS, this);

	//
	m_cbxCountry.AddString("Australia");
	m_cbxCountry.AddString("France");
	m_cbxCountry.AddString("Germany");
	m_cbxCountry.AddString("Japan");
	m_cbxCountry.AddString("Korea");
	m_cbxCountry.AddString("Singapore");
	m_cbxCountry.AddString("Spain");
	m_cbxCountry.AddString("U.K.");
	m_cbxCountry.AddString("U.S.");
	m_cbxCountry.AddString("Vietnam");
	m_cbxCountry.SetCurSel(4);

	//
	m_cbxModulation.AddString("NTSC");
	m_cbxModulation.AddString("PAL");
	m_cbxModulation.AddString("SECAM");
	m_cbxModulation.SetCurSel(0);

	//
	m_chkAnalogAir.SetCheck(BST_CHECKED);
	m_chkDigitalAir.SetCheck(BST_CHECKED);

	//
	m_btnSave.EnableWindow(FALSE);

	//
	m_lcChannel.SetExtendedStyle(m_lcChannel.GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	m_lcChannel.InsertColumn(0, _T("Channel"), 0, 65);
	m_lcChannel.InsertColumn(1, _T("Name"), 0, 130);

	//
	CRect client_rect;
	GetClientRect(client_rect);
	m_nOriginalWidth = client_rect.Width() + ::GetSystemMetrics(SM_CXDLGFRAME)*2;

	CreateTVPlayer();

	//
	LoadChannelList();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CUBCTVScanDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if(m_bChannelChanged)
	{
		int ret_value = ::MessageBox(GetSafeHwnd(), _T("Channel list changed and not been saved.      \r\n\t\nWould you like to save?"), _T("UBC TV Scan"), MB_ICONEXCLAMATION | MB_YESNOCANCEL);
		if(ret_value == IDCANCEL)
			return;
		else if(ret_value == IDYES)
		{
			m_player.ClearChannelList(false);
			m_player.SaveChannelList();
		}
	}

	m_player.Destroy();
	ResetChannelList();

	CDialog::OnCancel();
}

void CUBCTVScanDlg::OnBnClickedCheckScanSignal()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if( m_chkAnalogAir.GetCheck() == BST_UNCHECKED &&
		m_chkDigitalAir.GetCheck() == BST_UNCHECKED &&
		m_chkAnalogCatv.GetCheck() == BST_UNCHECKED &&
		m_chkAnalogAir.GetCheck() == BST_UNCHECKED )
	{
		m_btnScan.EnableWindow(FALSE);
	}
	else
	{
		m_btnScan.EnableWindow(TRUE);
	}
}

void CUBCTVScanDlg::OnBnClickedButtonScan()
{
	// TODO: 

	m_bScanning = !m_bScanning;

	if(m_bScanning)
	{
		// start scan
		m_bChannelChanged = true;

		m_cbxCountry.EnableWindow(FALSE);
		m_cbxModulation.EnableWindow(FALSE);
		m_chkAnalogAir.EnableWindow(FALSE);
		m_chkDigitalAir.EnableWindow(FALSE);
		m_chkAnalogCatv.EnableWindow(FALSE);
		m_chkDigitalCatv.EnableWindow(FALSE);
		m_btnScan.SetWindowText(_T("Stop"));
		m_btnSave.EnableWindow(FALSE);
		m_btnClose.EnableWindow(FALSE);

		m_nTotalChannelCount = 0;
		m_listIncompleteScanSignal.RemoveAll();
		m_listCompleteScanSignal.RemoveAll();
		ResetChannelList();

		if(m_chkAnalogAir.GetCheck() == BST_CHECKED)	{ m_listIncompleteScanSignal.Add(modeAnalogAir);	m_nTotalChannelCount += 69; }
		if(m_chkDigitalAir.GetCheck() == BST_CHECKED)	{ m_listIncompleteScanSignal.Add(modeDigitalAir);	m_nTotalChannelCount += 69; }
		if(m_chkAnalogCatv.GetCheck() == BST_CHECKED)	{ m_listIncompleteScanSignal.Add(modeAnalogCatv);	m_nTotalChannelCount += 136; }
		if(m_chkDigitalCatv.GetCheck() == BST_CHECKED)	{ m_listIncompleteScanSignal.Add(modeDigitalCatv);	m_nTotalChannelCount += 136; }

		m_wndProgress.SetTotalCount(m_nTotalChannelCount);
		m_wndProgress.SetTotalStep(0);
		m_wndProgress.SetCurrentStep(0);

		m_wndProgress.CenterWindow();
		m_wndProgress.ShowWindow(SW_SHOW);
		m_wndProgress.SetWindowText(_T("Preparing..."));

		m_player.ClearChannelList(true, false);

		SetTimer(TIMER_NEXT_SCAN_SIGNAL_ID, TIMER_NEXT_SCAN_SIGNAL_TIME, NULL);
	}
	else
	{
		// stop scan
		BeginWaitCursor();

		m_player.StopScan();

		m_player.Destroy(true);

		CreateTVPlayer();

		m_cbxCountry.EnableWindow(TRUE);
		m_cbxModulation.EnableWindow(TRUE);
		m_chkAnalogAir.EnableWindow(TRUE);
		m_chkDigitalAir.EnableWindow(TRUE);
		m_chkAnalogCatv.EnableWindow(TRUE);
		m_chkDigitalCatv.EnableWindow(TRUE);
		m_btnScan.SetWindowText(_T("Scan"));
		m_btnSave.EnableWindow(TRUE);
		m_btnClose.EnableWindow(TRUE);

		m_wndProgress.ShowWindow(SW_HIDE);

		EndWaitCursor();
	
		::MessageBox(GetSafeHwnd(), _T("Channel Scan Complete   "), _T("UBC TV Scan"), MB_ICONINFORMATION);
	}
}

void CUBCTVScanDlg::OnBnClickedButtonSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_bChannelChanged = false;
	m_btnSave.EnableWindow(FALSE);

	m_player.ClearChannelList(false);
	m_player.SaveChannelList();
}

void CUBCTVScanDlg::OnStnClickedStaticFolding()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	m_bExpand = !m_bExpand;

	if(m_bExpand)
	{
		CStatic* wnd = (CStatic*)GetDlgItem(IDC_STATIC_FOLDING);
		HICON ico = ::AfxGetApp()->LoadIcon(IDI_ICON_REDUCE);
		wnd->SetIcon(ico);
		KillTimer(TIMER_REDUCE_ID);
		SetTimer(TIMER_EXPAND_ID, TIMER_EXPAND_TIME, NULL);
	}
	else
	{
		CStatic* wnd = (CStatic*)GetDlgItem(IDC_STATIC_FOLDING);
		HICON ico = ::AfxGetApp()->LoadIcon(IDI_ICON_EXPAND);
		wnd->SetIcon(ico);
		KillTimer(TIMER_EXPAND_ID);
		SetTimer(TIMER_REDUCE_ID, TIMER_REDUCE_TIME, NULL);
	}
}

void CUBCTVScanDlg::OnNMDblclkListChannel(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(m_bScanning) return;

	LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE)pNMHDR;

	int idx = lpnmitem->iItem;
	if(idx < 0) return;

	BeginWaitCursor();

	TVCHANNEL_ITEM* item = (TVCHANNEL_ITEM*)m_lcChannel.GetItemData(idx);

	m_player.Stop();
	m_player.SetTVMode(item->eTVMode);
	m_player.SetChannel(item->nChannel);
	m_player.Play();

	if(!m_bExpand) OnStnClickedStaticFolding();

	EndWaitCursor();
}

void CUBCTVScanDlg::OnLvnKeydownListChannel(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	if(pLVKeyDow->wVKey == VK_DELETE)
	{
		if(m_bScanning) return;

		// delete channel
		int count = m_lcChannel.GetItemCount();
		for(int i=count-1; i>=0; i--)
		{
			UINT state = m_lcChannel.GetItemState(i, LVIS_SELECTED);
			if(state == LVIS_SELECTED)
			{
				TVCHANNEL_ITEM* item = (TVCHANNEL_ITEM*)m_lcChannel.GetItemData(i);

				if(m_player.DeleteChannel(item))
				{
					m_bChannelChanged = true;
					m_btnSave.EnableWindow(TRUE);

					delete item;
					m_lcChannel.DeleteItem(i);
				}
			}
		}
	}
}

BOOL CUBCTVScanDlg::CreateTVPlayer()
{
	CRect rect(0,0,1,1);
	CWnd* vert_wnd = GetDlgItem(IDC_STATIC_VERT);
	if(vert_wnd)
	{
		vert_wnd->GetWindowRect(rect);
		ScreenToClient(&rect);
		m_nExpandWidth = rect.Height() * 4 / 3;
		rect.left += 16;
		rect.right = rect.left + rect.Height() * 4 / 3;
	}
	CWnd* preview_wnd = GetDlgItem(IDC_STATIC_PREVIEW);
	if(preview_wnd)
	{
		preview_wnd->MoveWindow(rect);
		rect.DeflateRect(1,1);
	}
	m_nExpandWidth += m_nOriginalWidth + 13;

	return m_player.Create(this, rect, true);
}

void CUBCTVScanDlg::ResetChannelList()
{
	for(int i=0; i<m_lcChannel.GetItemCount(); i++)
	{
		TVCHANNEL_ITEM* change_item = (TVCHANNEL_ITEM*)m_lcChannel.GetItemData(i);
		if(change_item) delete change_item;
	}
	m_lcChannel.DeleteAllItems();
}

void CUBCTVScanDlg::NextScanSignal()
{
	if(m_listIncompleteScanSignal.GetCount() == 0)
	{
		if(m_bScanning) OnBnClickedButtonScan();
	}
	else
	{
		m_eScanningSignal = (TV_MODE)m_listIncompleteScanSignal.GetAt(0);

		m_wndProgress.SetCurrentCount(GetCurrentChannelCount());
		m_wndProgress.SetCurrentStep(0);

		BeginWaitCursor();

		m_player.SetTVMode(m_eScanningSignal);
		m_player.SetVolume(0);
		m_player.SetMute(TRUE);
		m_player.StartScan();

		EndWaitCursor();
	}
}

int CUBCTVScanDlg::GetCurrentChannelCount()
{
	if(m_listIncompleteScanSignal.GetCount() == 0) return 0;

	switch(m_eScanningSignal)
	{
	case modeAnalogAir:
	case modeDigitalAir:
		return 69;
		break;
	case modeAnalogCatv:
	case modeDigitalCatv:
		return 136;
		break;
	}
	return 0;
}

int CUBCTVScanDlg::GetCompleteChannelCount()
{
	int channel_count = 0;
	int count = m_listCompleteScanSignal.GetCount();

	for(int i=0; i<count; i++)
	{
		switch(m_listCompleteScanSignal.GetAt(i))
		{
		case modeAnalogAir:
		case modeDigitalAir:
			channel_count += 69;
			break;
		case modeAnalogCatv:
		case modeDigitalCatv:
			channel_count += 136;
			break;
		}
	}

	return channel_count;
}

LRESULT CUBCTVScanDlg::OnAddChannelInfo(WPARAM wParam, LPARAM lParam)
{
	if(wParam == NULL) return 0;

	int idx = m_lcChannel.GetItemCount();

	TVCHANNEL_ITEM* item = (TVCHANNEL_ITEM*)wParam;

	TVCHANNEL_ITEM* new_item = new TVCHANNEL_ITEM;
	*new_item = *item;

	CString buf;
	buf.Format(_T("%d"), item->nChannel);

	m_lcChannel.InsertItem(idx, buf);
	m_lcChannel.SetItemText(idx, 1, item->strName);
	m_lcChannel.SetItemData(idx, (DWORD)new_item);

	return 0;
}

LRESULT CUBCTVScanDlg::OnChangeChannelInfo(WPARAM wParam, LPARAM lParam)
{
	if(wParam == NULL) return 0;

	TVCHANNEL_ITEM* item = (TVCHANNEL_ITEM*)wParam;

	int count = m_lcChannel.GetItemCount();
	for(int i=0; i<count; i++)
	{
		TVCHANNEL_ITEM* change_item = (TVCHANNEL_ITEM*)m_lcChannel.GetItemData(i);

		if(item->eTVMode == change_item->eTVMode && 
			item->nChannel == change_item->nChannel)
		{
			*change_item = *item;

			CString buf;
			buf.Format(_T("%d"), item->nChannel);

			m_lcChannel.SetItemText(i, 0, buf);
			m_lcChannel.SetItemText(i, 1, item->strName);

			break;
		}
	}
	return 0;
}

LRESULT CUBCTVScanDlg::OnChangeScanChannel(WPARAM wParam, LPARAM lParam)
{
	int count = GetCurrentChannelCount();
	int percent = 0;
	if(count > 0)
		percent = wParam * 100 / count;

	CString buf;
	switch(m_eScanningSignal)
	{
	case modeAnalogAir:
		buf.Format(_T("Scanning Analog Air...(%d%%)"), percent);
		break;
	case modeDigitalAir:
		buf.Format(_T("Scanning Digital Air...(%d%%)"), percent);
		break;
	case modeAnalogCatv:
		buf.Format(_T("Scanning Analog Catv...(%d%%)"), percent);
		break;
	case modeDigitalCatv:
		buf.Format(_T("Scanning Digital Catv...(%d%%)"), percent);
		break;
	}
	m_wndProgress.SetWindowText(buf);
	m_wndProgress.SetCurrentStep(wParam);
	m_wndProgress.SetTotalStep(GetCompleteChannelCount() + wParam);

	return 0;
}

LRESULT CUBCTVScanDlg::OnCompleteScanChannel(WPARAM wParam, LPARAM lParam)
{
	m_player.StopScan();

	m_listCompleteScanSignal.Add(m_eScanningSignal);
	m_listIncompleteScanSignal.RemoveAt(0);

	m_wndProgress.SetCurrentStep(0);
	m_wndProgress.SetWindowText(_T("Preparing..."));

	SetTimer(TIMER_NEXT_SCAN_SIGNAL_ID, TIMER_NEXT_SCAN_SIGNAL_TIME, NULL);

	return 0;
}

BOOL CUBCTVScanDlg::LoadChannelList()
{
	TVCHANNEL_ITEMLIST channel_list;

	m_player.GetTvChannelList(channel_list);

	int count = channel_list.GetCount();
	for(int i=0; i<count; i++)
	{
		TVCHANNEL_ITEM& item = channel_list.GetAt(i);

		TVCHANNEL_ITEM* new_item = new TVCHANNEL_ITEM;
		*new_item = item;

		CString buf;
		buf.Format(_T("%d"), item.nChannel);

		m_lcChannel.InsertItem(i, buf);
		m_lcChannel.SetItemText(i, 1, item.strName);
		m_lcChannel.SetItemData(i, (DWORD)new_item);
	}

	return TRUE;
}
