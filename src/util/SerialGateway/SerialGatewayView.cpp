// SerialGatewayView.cpp : CSerialGatewayView 클래스의 구현
//

#include "stdafx.h"
#include "SerialGateway.h"

#include "SerialGatewayDoc.h"
#include "SerialGatewayView.h"

#include "SerialMFC.h"

#include "ShortcutKeySettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define		WM_RECEIVE_DATA		(WM_USER+100)


// CSerialGatewayView

IMPLEMENT_DYNCREATE(CSerialGatewayView, CFormView)

BEGIN_MESSAGE_MAP(CSerialGatewayView, CFormView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_COMMAND(ID_SEND_INIT, &CSerialGatewayView::OnSendInit)
	ON_COMMAND(ID_SEND_TEST, &CSerialGatewayView::OnSendTest)
	ON_WM_SERIAL(OnSerialMsg)
	ON_MESSAGE(WM_RECEIVE_DATA, OnReceiveData)
	ON_COMMAND(ID_SHORTCUTKEY_SETTINGS, &CSerialGatewayView::OnShortcutkeySettings)
END_MESSAGE_MAP()

// CSerialGatewayView 생성/소멸

int CSerialGatewayView::m_nComPort = 1;
bool CSerialGatewayView::m_bTypePushbutton = false;


CSerialGatewayView::CSerialGatewayView()
	: CFormView(CSerialGatewayView::IDD)
{
	m_pSerial = new CSerialMFC;
	m_nColumnCount = 0;
	m_pThread = NULL;
}

CSerialGatewayView::~CSerialGatewayView()
{
	//
	if(m_pSerial) delete m_pSerial;

	//
	if(m_pThread)
	{
		m_bRunThread = false;

		try
		{
			DWORD dwExitCode;

			::GetExitCodeThread(m_pThread->m_hThread, &dwExitCode);

			if(dwExitCode == STILL_ACTIVE)
			{
				DWORD timeout = ::WaitForSingleObject(m_pThread->m_hThread, 1000);
				if(timeout == WAIT_TIMEOUT)
				{
					//__WARNING__(_T("Timeout !!!"), _NULL);
				}
				else
				{
				}
			}

			delete m_pThread;
		}
		catch(...)
		{
			//__WARNING__(_T("Exception in DeleteThread !!!"), _NULL);
		}

		m_pThread = NULL;
	}
}

void CSerialGatewayView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_lcLog);
}

BOOL CSerialGatewayView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CSerialGatewayView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//
	m_lcLog.SetExtendedStyle(m_lcLog.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_lcLog.InsertColumn(0, _T("Count"), LVCFMT_RIGHT, 0);
	m_lcLog.InsertColumn(1, _T("Time"), 0, 120);
	m_lcLog.InsertColumn(2, _T("Status"), 0, 210);
	m_lcLog.InsertColumn(3, _T("Receive Data Packet"), 0, 350);
	m_lcLog.InsertColumn(4, _T("Data Size"), 0, 60);

	//
	SetTimer(1025, 1000, NULL);

	//
	RefreshShortcutKey();
}


// CSerialGatewayView 진단

#ifdef _DEBUG
void CSerialGatewayView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSerialGatewayView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CSerialGatewayDoc* CSerialGatewayView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSerialGatewayDoc)));
	return (CSerialGatewayDoc*)m_pDocument;
}
#endif //_DEBUG


// CSerialGatewayView 메시지 처리기

void CSerialGatewayView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	if( GetSafeHwnd() && m_lcLog.GetSafeHwnd() )
	{
		m_lcLog.MoveWindow(0, 0, cx, cy);
	}
}

void CSerialGatewayView::OnTimer(UINT_PTR nIDEvent)
{
	long errCode = ERROR_SUCCESS;
	CString str_comport;

	switch(nIDEvent)
	{
	case 1025:
		KillTimer(1025);
		InsertItem((LPBYTE)"Open ComPort", MAXDWORD, "INFO");

		// Attempt to open the _serial port (COM1)
		str_comport.Format(_T("COM%d"), (m_nComPort==0 ? 1 : m_nComPort) );
		errCode = m_pSerial->Open(str_comport, this);
		if (errCode != ERROR_SUCCESS)
		{
			//_errMsg = "Unable to open COM-port";
			//printf("code=%d,msg=%s\n", errCode,_errMsg.c_str());
			CString str_msg;
			str_msg.Format("Fail to Open %s-Port.", str_comport);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			return;
		}

		// Setup the _serial port (9600,8N1, which is the default setting)
		errCode = m_pSerial->Setup(	(CSerial::EBaudrate)CSerial::EBaud9600,
									(CSerial::EDataBits)CSerial::EData8,
									(CSerial::EParity)CSerial::EParNone,
									(CSerial::EStopBits)CSerial::EStop1		);

		if (errCode != ERROR_SUCCESS)
		{
			//_errMsg = "Unable to set COM-port setting";
			//printf("code=%d,msg=%s\n",errCode,_errMsg.c_str());
			CString str_msg;
			str_msg.Format("Fail to Setup %s-Port.", str_comport);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			return;
		}

		// Register only for the receive event
		errCode = m_pSerial->SetMask(CSerial::EEventBreak |
									CSerial::EEventCTS   |
									CSerial::EEventDSR   |
									CSerial::EEventError |
									CSerial::EEventRing  |
									CSerial::EEventRLSD  |
									CSerial::EEventRecv);
		
		if (errCode != ERROR_SUCCESS)
		{
			//_errMsg = "Unable to set COM-port event mask";
			//printf("code=%d,msg=%s\n",errCode,_errMsg.c_str());
			CString str_msg;
			str_msg.Format("Fail to SetMask %s-Port.", str_comport);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			return;
		}
		
		// Use 'non-blocking' reads, because we don't know how many bytes
		// will be received. This is normally the most convenient mode
		// (and also the default mode for reading data).
		errCode = m_pSerial->SetupReadTimeouts(CSerial::EReadTimeoutNonblocking);
		if (errCode != ERROR_SUCCESS)
		{
			//_errMsg = "Unable to set COM-port read timeout.";
			//printf("code=%d,msg=%s\n",errCode,_errMsg.c_str());
			CString str_msg;
			str_msg.Format("Fail to SetupReadTimeouts %s-Port.", str_comport);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			return;
		}

		m_pThread = ::AfxBeginThread(CheckSerialDataThread, (LPVOID)this, THREAD_PRIORITY_IDLE, 0, CREATE_SUSPENDED);
		m_pThread->m_bAutoDelete = FALSE;
		m_bRunThread = true;
		m_pThread->ResumeThread();

		SetTimer(1026, 1000, NULL);
		break;


	case 1026:
		KillTimer(1026);
		InsertItem((LPBYTE)"Run WatchDog", MAXDWORD, "INFO");
		{
			BYTE buf[16] = { 0x10, 0x2A, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x29 };
			LONG ret_val = m_pSerial->Write(buf, 16);
			if(ret_val)
			{
				CString str_msg;
				str_msg.Format("Fail to write (%d)", ret_val);
				InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			}
		}
		SetTimer(1027, 1000, NULL);
		break;

	case 1027:
		KillTimer(1027);
		InsertItem((LPBYTE)"Init", MAXDWORD, "INFO");
		{
			BYTE buf[16] = { 0x10, 0x21, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F };
			LONG ret_val = m_pSerial->Write(buf, 16);
			if(ret_val)
			{
				CString str_msg;
				str_msg.Format("Fail to write (%d)", ret_val);
				InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "ERROR");
			}
		}
		break;
	}

	CFormView::OnTimer(nIDEvent);
}

void CSerialGatewayView::OnSendInit()
{
	SetTimer(1026, 10, NULL);
}

void CSerialGatewayView::OnSendTest()
{
	BYTE buf[16] = { 0x10, 0x2A, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x28 };
	m_pSerial->Write(buf, 16);
}

void CSerialGatewayView::InsertItem(LPBYTE pData, DWORD dwSize, LPCTSTR lpszStatus)
{
	m_lcLog.SetRedraw(FALSE);

	TCHAR buf[1024];

	_stprintf(buf, _T("%I64u"), m_nColumnCount);
	m_lcLog.InsertItem(0, buf);

	m_lcLog.SetItemText(0, 1, CTime::GetCurrentTime().Format("%Y-%m-%d %H:%M:%S"));
	m_lcLog.SetItemText(0, 2, lpszStatus);

	if(dwSize == MAXDWORD)
	{
		LPTSTR str_msg = (LPTSTR)pData;
		m_lcLog.SetItemText(0, 3, str_msg);
	}
	else if(dwSize > 0)
	{
		CString str_data;
		for(int i=0; i<dwSize; i++)
		{
			_stprintf(buf, _T("%02X"), pData[i]);

			if( str_data.GetLength() > 0 )
				str_data += _T(", ");
			str_data += buf;
		}
		m_lcLog.SetItemText(0, 3, str_data);

		_stprintf(buf, _T("%u"), dwSize);
		m_lcLog.SetItemText(0, 4, buf);
	}

	m_nColumnCount++;

	CheckListCount();

	m_lcLog.SetRedraw(TRUE);
}

void CSerialGatewayView::CheckListCount()
{
	int count = m_lcLog.GetItemCount();
	if(count > 1000)
	{
		m_lcLog.SetRedraw(FALSE);
		for(int i=count-1; i>900; i--)
		{
			m_lcLog.DeleteItem(i);
		}
		m_lcLog.SetRedraw(TRUE);
	}
}

LRESULT CSerialGatewayView::OnSerialMsg(WPARAM wParam, LPARAM lParam)
{
	CSerial::EEvent eEvent = CSerial::EEvent(LOWORD(wParam));
	CSerial::EError eError = CSerial::EError(HIWORD(wParam));

	BYTE buf[1024] = {0};
	DWORD read_size = 0;

	// lParam: User-defined 32-bit integer (type LPARAM)
	switch (eEvent)
	{
	case CSerialMFC::EEventRecv:
		m_pSerial->Read(buf, 1024, &read_size);
		if(read_size > 0)
		{
			m_csData.Lock();

			int old_size = m_arrData.GetSize();
			int new_size = old_size + read_size;
			m_arrData.SetSize(new_size);

			int idx=0;
			for(int i=old_size; i<new_size; i++, idx++)
			{
				m_arrData.SetAt(i, buf[idx]);
			}

			m_dwLastUpdateTick = ::GetTickCount();
			m_csData.Unlock();
		}
		break;

	default:
		{
			CString str_msg;
			str_msg.Format("Unknown Event (%d)", eEvent);
			InsertItem((LPBYTE)(LPCTSTR)str_msg, MAXDWORD, "WARN");
		}
		break;
	}

	// Return successful
	return 0;
}

LRESULT CSerialGatewayView::OnReceiveData(WPARAM wParam, LPARAM lParam)
{
	LPBYTE data = (LPBYTE)wParam;
	int size = (int)lParam;

	if(size > 0)
	{
		static int prev_short_pin = -1;

		CString str_status;
		if(data[1] == 0x23 && data[4] == 0x07)
		{
			// open
			str_status.Format(_T("Open"));

			//
			HWND brwHwnd = getWHandle(getPid("UTV_brwClient2.exe"));
			if(brwHwnd && m_bTypePushbutton==false)
			{
				if( m_strShortcutKey[0].GetLength() > 0 && 
					( prev_short_pin != 0 || m_bSendSameShortcutKey ) )
				{
					prev_short_pin = 0;

					COPYDATASTRUCT appInfo;
					appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
					appInfo.lpData = (char*)(LPCSTR)m_strShortcutKey[0];
					appInfo.cbData = m_strShortcutKey[0].GetLength() + 1;
					::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				}
			}
		}
		else if(data[1] == 0x23)
		{
			// short
			BYTE data_inv = ~(data[4]);
			BYTE filter = 0x01;
			CString str;
			for(int i=0; i<3; i++, filter<<=1)
			{
				if(data_inv & filter)
				{
					if(str.GetLength() > 0) str+=_T(",");
					TCHAR buf[2]={0};
					buf[0] = _T('1') + i;
					str+=buf;
				}
			}
			str_status.Format(_T("Short (%s)"), str);

			//
			HWND brwHwnd = getWHandle(getPid("UTV_brwClient2.exe"));
			if(brwHwnd)
			{
				int short_pin = data_inv & 0x07;

				if( m_strShortcutKey[short_pin].GetLength() > 0 && 
					( prev_short_pin != short_pin || m_bSendSameShortcutKey ) )
				{
					prev_short_pin = short_pin;

					COPYDATASTRUCT appInfo;
					appInfo.dwData = 1000; // UBC_WM_KEYBOARD_EVENT;
					appInfo.lpData = (char*)(LPCSTR)m_strShortcutKey[short_pin];
					appInfo.cbData = m_strShortcutKey[short_pin].GetLength() +1 ;
					::SendMessage(brwHwnd, WM_COPYDATA, NULL, (LPARAM)&appInfo);
				}
			}
		}
		else if(data[1] == 0x2A)
		{
			// watch-dog
			if(data[2] == 0x00 && data[3] == 0x01)
			{
				// watch-dog start
				str_status.Format(_T("WatchDog Start (Reset-Count: %d)"), data[4]);
			}
			else if(data[2] == 0x00 && data[3] == 0x00)
			{
				// watch-dog stop
				str_status.Format(_T("WatchDog Stop (Reset-Count: %d)"), data[4]);
			}
			else if(data[2] == 0x00 && data[3] == 0x02)
			{
				// watch-dog test
				str_status.Format(_T("WatchDog Test (Reset-Count: %d)"), data[4]);
			}
			else if(data[2] == 0x01 && data[3] == 0x00)
			{
				// watch-dog reset(stop)
				str_status.Format(_T("WatchDog Reset-Stop (Reset-Count: %d)"), data[4]);
			}
			else if(data[2] == 0x01 && data[3] == 0x01)
			{
				// watch-dog reset(start)
				str_status.Format(_T("WatchDog Reset-Start (Reset-Count: %d)"), data[4]);
			}
			else if(data[2] == 0x01 && data[3] == 0x02)
			{
				// watch-dog reset(test)
				str_status.Format(_T("WatchDog Reset-Test (Reset-Count: %d)"), data[4]);
			}
		}
		else
		{
			str_status = _T("Unknown");
		}

		InsertItem(data, size, str_status);
	}

	delete[]data;

	return 0;
}

DWORD GetTickDiff(DWORD dwStart, DWORD dwEnd)
{
	if(dwStart > dwEnd)
		return (MAXDWORD - dwStart) + dwEnd;
	return dwEnd - dwStart;
}

UINT CheckSerialDataThread(LPVOID pParam)
{
	CSerialGatewayView* parent_wnd = (CSerialGatewayView*)pParam;

	while(parent_wnd->m_bRunThread)
	{
		parent_wnd->m_csData.Lock();

		DWORD cur_tick = ::GetTickCount();
		int size = parent_wnd->m_arrData.GetCount();

		if( size > 0 &&
			GetTickDiff(parent_wnd->m_dwLastUpdateTick, cur_tick) > 50 )
		{
			int end;
			for(int i=0; i<size; i+=16)
			{
				end = i+16;
				if(end > size) end = size;
				int part_size = end-i;

				LPBYTE part_data = new BYTE[size];
				memcpy(part_data, parent_wnd->m_arrData.GetData() + i, part_size);
				parent_wnd->PostMessage(WM_RECEIVE_DATA, (WPARAM)part_data, (LPARAM)part_size);
			}
			parent_wnd->m_arrData.RemoveAll();
		}

		parent_wnd->m_csData.Unlock();
		::Sleep(1);
	}

	return 0;
}

void CSerialGatewayView::RefreshShortcutKey()
{
	char buf[1024];

	::GetPrivateProfileString("ShortcutKey", "Open", "Ctrl+i", buf, 1024, ::GetINIPath());
	m_strShortcutKey[0] = buf;

	::GetPrivateProfileString("ShortcutKey", "Short1", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_strShortcutKey[1] = buf;

	::GetPrivateProfileString("ShortcutKey", "Short2", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_strShortcutKey[2] = buf;

	::GetPrivateProfileString("ShortcutKey", "Short3", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_strShortcutKey[4] = buf;

	::GetPrivateProfileString("ShortcutKey", "Short12", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_strShortcutKey[3] = buf;

	::GetPrivateProfileString("ShortcutKey", "Short13", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_strShortcutKey[5] = buf;

	::GetPrivateProfileString("ShortcutKey", "Short23", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_strShortcutKey[6] = buf;

	::GetPrivateProfileString("ShortcutKey", "Short123", "Ctrl+h", buf, 1024, ::GetINIPath());
	m_strShortcutKey[7] = buf;

	::GetPrivateProfileString("ShortcutKey", "SendSameShortcutKey", "0", buf, 1024, ::GetINIPath());
	m_bSendSameShortcutKey = atoi(buf);

	//
	for(int i=0; i<8; i++)
	{
		m_strShortcutKey[i].Trim();
	}
}

void CSerialGatewayView::OnShortcutkeySettings()
{
	CShortcutKeySettingsDlg dlg;
	if( dlg.DoModal() == IDOK )
	{
		RefreshShortcutKey();
	}
}
