#pragma once
#include "afxwin.h"


// CShortcutKeySettingsDlg 대화 상자입니다.

class CShortcutKeySettingsDlg : public CDialog
{
	DECLARE_DYNAMIC(CShortcutKeySettingsDlg)

public:
	CShortcutKeySettingsDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CShortcutKeySettingsDlg();
	virtual BOOL OnInitDialog();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SHORTCUTKEY_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()
public:
	CEdit	m_editOpen;
	CEdit	m_editShort1;
	CEdit	m_editShort2;
	CEdit	m_editShort3;
	CEdit	m_editShort12;
	CEdit	m_editShort13;
	CEdit	m_editShort23;
	CEdit	m_editShort123;
	CButton	m_chkSendSameShortcutKey;

	CString	m_strShortcutKeyOpen;
	CString	m_strShortcutKeyShort1;
	CString	m_strShortcutKeyShort2;
	CString	m_strShortcutKeyShort3;
	CString	m_strShortcutKeyShort12;
	CString	m_strShortcutKeyShort13;
	CString	m_strShortcutKeyShort23;
	CString	m_strShortcutKeyShort123;
	int		m_bSendSameShortcutKey;
};
