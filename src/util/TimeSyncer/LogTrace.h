//  LogTrace.cpp -- Interface for the CLogTrace class
//  A class to do debug logging


#ifndef __LOGTRACE_H__
#define __LOGTRACE_H__

#include "ci/libDebug/ciDebug.h"
#include "ci/libThread/ciSyncUtil.h"

#define utvDEBUG(msg)	CLogTrace::getInstance()->debug msg
#define utvERROR(msg)	CLogTrace::getInstance()->error msg
#define utvWARN(msg)	CLogTrace::getInstance()->warn msg

class CLogTrace
{
// Construction/Destruction
public:

	static CLogTrace*	getInstance() ;
	static void			clearInstance();
		

	CLogTrace();
	~CLogTrace();


// Attributes
public:
	CString m_strAppName;

protected:
	BOOL m_bActive;
	CString m_strFileName;
	BOOL m_bTimeStamp;

// Operations
public:
	void WriteLine(LPCTSTR szLine);
	void WriteLine(LPCTSTR szFormat, LPCTSTR szAddInfo);
	void WriteLine(LPCTSTR szFormat, int nAddInfo);
	void ResetFile();
	void OnStartup(BOOL bActive, BOOL bTimeStamp);
	void SetFileName(LPCTSTR szFileName);

	void	debug(const char* format,...);
	void	error(const char* format,...);
	void	warn(const char* format,...);

protected:

	ciMutex	_lock;
	static CLogTrace*	_instance;
	static ciMutex		_instanceLock;

// Inlines
public:
	inline void SetActive(BOOL bSet)
	{
		m_bActive = bSet;
	}
	inline CString GetFileName()
	{
		return m_strFileName;
	}
};


#endif // __LOGTRACE_H__</PRE>