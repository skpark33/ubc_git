// TimeSyncerDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "ci/libBase/ciTime.h"
#include "TimeSyncTimer.h"


// CTimeSyncerDlg 대화 상자
class CTimeSyncerDlg : public CDialog
{
// 생성입니다.
public:
	CTimeSyncerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TIMESYNCER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	BOOL ORBinit();

public:
	afx_msg void OnBnClickedBtnSync();
	afx_msg void OnBnClickedBtnEdit();
	afx_msg LONG OnUpdateTime(WPARAM wParam,LPARAM lParam);

	// 엔터키 입력시 종료 방지
	BOOL PreTranslateMessage(MSG* pMsg);

	CString m_strPeriod;
	CString m_Message;
	CString m_strLastUpdateTime;
	CString m_strNextUpdateTime;

	int m_Period;
	ciTime m_LastUpdateTime;
	TimeSyncTimer m_Timer;
	afx_msg void OnClose();
};
