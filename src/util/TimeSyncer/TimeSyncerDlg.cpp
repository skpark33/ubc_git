// TimeSyncerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "TimeSyncer.h"
#include "TimeSyncTimer.h"
#include "TimeSyncerDlg.h"

#include "LogTrace.h"
#include "ci/libDebug/ciArgParser.h"
#include "cci/libUtil/cciCorbaCommon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTimeSyncerDlg 대화 상자

CTimeSyncerDlg::CTimeSyncerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTimeSyncerDlg::IDD, pParent)
	, m_Period(300)
	, m_strPeriod(_T(""))
	, m_Message(_T(""))
	, m_strLastUpdateTime(_T(""))
	, m_strNextUpdateTime(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTimeSyncerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PERIOD, m_strPeriod);
	DDX_Text(pDX, IDC_STATIC_MSG, m_Message);
	DDV_MaxChars(pDX, m_Message, 50);
	DDX_Text(pDX, IDC_EDIT2, m_strLastUpdateTime);
	DDX_Text(pDX, IDC_EDIT3, m_strNextUpdateTime);
}

BEGIN_MESSAGE_MAP(CTimeSyncerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_SYNC, &CTimeSyncerDlg::OnBnClickedBtnSync)
	ON_BN_CLICKED(IDC_BTN_EDIT, &CTimeSyncerDlg::OnBnClickedBtnEdit)
	ON_MESSAGE(UM_UPDATE_TIME,OnUpdateTime)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CTimeSyncerDlg 메시지 처리기

BOOL CTimeSyncerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	//
	// COP 초기화
	//

	ciArgParser::initialize(__argc,__argv);

	if(ciArgParser::getInstance()->isSet("+log")) {
		CLogTrace* log = CLogTrace::getInstance();
		log->m_strAppName = TIMESYNCER;
		ciString logFileName = getenv("PROJECT_HOME");
		if (logFileName.empty()) {
			logFileName = "C:\\Project\\COP2005\\cop\\log\\DEBUG_TimeSyncer.log";			
		} else {
			logFileName += "\\log\\DEBUG_TimeSyncer.log";
		}
		log->SetFileName(logFileName.c_str());
		log->OnStartup(TRUE, TRUE);
	}

	// ORB 초기화 및 TimeOut 설정
	if (!ORBinit()) {
		return FALSE;
	}

	// ACE 초기화
	ACE::init();

	utvDEBUG(("====== TimeSyncer Aplication 이 시작됩니다. ======"));

	m_Message.Format("시간 동기화 주기는 %d 초 입니다.", m_Period);
	UpdateData(false);

	// 타이머 시작
	m_Timer.initTimer(TIMESYNCER, m_Period);
	m_Timer.startTimer();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CTimeSyncerDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
		
	// ACE 해제
	ACE::fini();

	CDialog::OnClose();
}

void CTimeSyncerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CTimeSyncerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTimeSyncerDlg::OnBnClickedBtnSync()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	// Time Sync
	TimeSyncTimer::timeSync("*",TIMESYNCER);
}

void CTimeSyncerDlg::OnBnClickedBtnEdit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(true);

	int period = atoi(m_strPeriod.GetBuffer());
	if (period < 60) {
		AfxMessageBox("주기는 60초 이상이어야 합니다.");
		return;
	}
	m_Period = period;

	// 타이머 시간 변경
	m_Timer.setInterval(m_Period);

	// 화면 업데이트
	m_Message.Format("시간 동기화 주기는 %d 초 입니다.", m_Period);

	ciString strDate;
	ciTime next = m_LastUpdateTime + m_Period;
	next.getTimeString_r(strDate);
	m_strNextUpdateTime = strDate.c_str();

	UpdateData(false);
}

LONG CTimeSyncerDlg::OnUpdateTime(WPARAM wParam,LPARAM lParam)
{
	ciTime current;
	m_LastUpdateTime = current;
	ciString strDate;
	current.getTimeString_r(strDate);
	m_strLastUpdateTime = strDate.c_str();

	ciTime next = current + m_Period;
	strDate = "";
	next.getTimeString_r(strDate);
	m_strNextUpdateTime = strDate.c_str();

	UpdateData(false);

	return 1;
}

// 엔터키 입력시 종료 방지
BOOL CTimeSyncerDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	CEdit* pWnd = (CEdit*)GetDlgItem(IDC_EDIT_PERIOD);
	if(pWnd->GetSafeHwnd() != NULL)
	{
		if(pMsg->message == WM_KEYDOWN)
		{
			if(pWnd->GetSafeHwnd() == pMsg->hwnd && pMsg->wParam == VK_RETURN)
				return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


BOOL CTimeSyncerDlg::ORBinit()
{
	// timeout arguments
	ciString timeOutStr;
	ciUInt timeOutVal = 2; // default 2 seconds
	if (ciArgParser::getInstance()->getArgValue("+timeout", timeOutStr) ) {
		timeOutVal = atoi(timeOutStr.c_str());
	} 
	utvDEBUG(("+timeout argument is set by [%d]", timeOutVal));	
	if ( timeOutVal < 1 ) timeOutVal = 1; // minimum 1 seconds

	try {
		CORBA::ORB_var orb = CORBA::ORB_init(__argc, __argv);
		if(CORBA::is_nil(orb)) {
			utvERROR(("====== ORB init failed ======"));
			return FALSE;
		}

		// 1 second (TimeT has 100 nanosecond resolution).
		TimeBase::TimeT timeout = 10000000 * timeOutVal;
		CORBA::Any timeout_value;
		timeout_value <<= timeout;

		CORBA::Policy_var policy;
		// QoSExt::RELATIVE_CONN_TIMEOUT_POLICY_TYPE    - VBROKER
		// TAO::CONNECTION_TIMEOUT_POLICY_TYPE          - TAO
		// Messaging::RELATIVE_REQ_TIMEOUT_POLICY_TYPE  - ALL
		// Messaging::RELATIVE_RT_TIMEOUT_POLICY_TYPE   - ALL
#ifdef _VBROKER
		policy = orb->create_policy(
			Messaging::RELATIVE_RT_TIMEOUT_POLICY_TYPE
			, timeout_value
			);
#else //_TAO
		policy = orb->create_policy(
			Messaging::RELATIVE_RT_TIMEOUT_POLICY_TYPE
			, timeout_value
			);
#endif

		CORBA::PolicyList policies;
		policies.length(1);
		policies[0] = CORBA::Policy::_duplicate(policy);

		// Install the policy at the orb level.
		CORBA::Object_var pmobj = orb->resolve_initial_references("ORBPolicyManager");
		CORBA::PolicyManager_var orb_mgr = CORBA::PolicyManager::_narrow(pmobj);

		orb_mgr->set_policy_overrides(policies, CORBA::SET_OVERRIDE);

	} catch(CORBA::Exception& e) {
		utvERROR(("Catch Exception[%s] in setting to orb's PolicyManager", e._name()));
		return FALSE;
	}
	utvDEBUG(("TimeOut is set %d seconds.", timeOutVal));
	return TRUE;
}
