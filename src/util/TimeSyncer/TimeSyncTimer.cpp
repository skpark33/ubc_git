#include "StdAfx.h"
#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciTime.h>

#include "TimeSyncTimer.h"
#include "LogTrace.h"

ciSET_DEBUG(1, "TimeSyncTimer");

HWND TimeSyncTimer::_tsHwnd = 0;

TimeSyncTimer::TimeSyncTimer() 
: _interval(0)
{
	ciDEBUG(3, ("TimeSyncTimer()"));
}

TimeSyncTimer::~TimeSyncTimer() {
	ciDEBUG(3, ("~TimeSyncTimer()"));
	_tsHwnd = NULL;
}

void 
TimeSyncTimer::initTimer(const char* pname, int interval) {
	ciDEBUG(3, ("initTimer(%s,%d)", pname, interval));

	if (!_tsHwnd) {
		_tsHwnd = ::FindWindow(NULL, TIMESYNCER);
		if (!_tsHwnd) {
			ciERROR(("TimeSyncer Handle is not found."));
		}
	}

	_interval = interval;
	_timer = new ciTimer(pname, this);
	_timer->setInterval(interval);
}

void
TimeSyncTimer::processExpired(ciString name, int counter, int interval)
{
	utvDEBUG(("processExpired(%s,%d,%d)", 
		name.c_str(), counter, interval));

	// Time Sync
	timeSync("*",TIMESYNCER);

}

ciBoolean 
TimeSyncTimer::timeSync(const char* agentId,
				   const char* operatorId)
{
	cciEntity entity("%s=%s", TIMEAGENT, agentId);

	cciCall call("time");
	call.setEntity(entity);

	cciTime current;
	call.addItem("time", current.get());
	call.addItem("operatorId", operatorId);

	utvDEBUG((call.toString()));

	try
	{
		if(!call.call())
		{
			utvERROR(("call이 실패하여 시간을 변경하지 못하였습니다."));
		} else {
			cciReply ret;
			while(call.getReply(ret))
			{	
				// null loop
				//utvDEBUG((ret.toString()));
			}
		}
	}
	catch(CORBA::Exception& ex)
	{
		utvERROR((ex._name()));
	}

	::SendMessage(TimeSyncTimer::_tsHwnd, UM_UPDATE_TIME, NULL, NULL);

	utvDEBUG(("call complete"));
	return ciTrue;
}
