#ifndef _TimeSyncTimer_H_
#define _TimeSyncTimer_H_

#include <ci/libTimer/ciTimer.h>

class TimeSyncTimer : public ciPollable {
protected:
	int _interval;
	ciString _sttId;
	static HWND _tsHwnd;

public:
	TimeSyncTimer();
	~TimeSyncTimer();

	virtual void initTimer(const char* pname, int interval = 60);
	virtual void processExpired(ciString name, int counter, int interval);

	static ciBoolean timeSync(
		const char* agentId, 
		const char* operatorId);

};

#endif //_TimeSyncTimer_H_
