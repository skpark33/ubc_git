 /*! \file scheduleSaverWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _scheduleSaverWorld_h_
#define _scheduleSaverWorld_h_


#include <ci/libWorld/ciWorld.h> 


class scheduleSaverWorld : public ciWorld {
public:
	scheduleSaverWorld();
	~scheduleSaverWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

protected:	
	ciBoolean _getHostList(ciStringList& outList);
	void	doIt(ciStringList& beforeList, ciStringList& after, const char* tId);
};
		
#endif //_scheduleSaverWorld_h_
