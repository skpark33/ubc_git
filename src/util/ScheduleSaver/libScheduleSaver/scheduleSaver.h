#ifndef _scheduleSaver_h_
#define _scheduleSaver_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>


#include <CMN/libCommon/utvMacro.h>

class scheduleSaver  {
public:
	static scheduleSaver*	getInstance();
	static void	clearInstance();

	virtual ~scheduleSaver() ;
	
	ciBoolean	save(const char* hostId);

protected:
	scheduleSaver();

	void		_saveList(FILE* fd, const char* name, ciStringList& inList, const char* prefix);
	ciBoolean	_saveHost(FILE* fd, const char* hostId, ciString& outVal);
	ciBoolean	_getTemplateList(const char* hostId, const char* veiwName, ciStringList& outList);
	ciBoolean	_saveTemplate(FILE* fd, const char* templateId);
	ciBoolean	_getFrameList(const char* templateId, ciStringList& outList);
	ciBoolean	_saveFrame(FILE* fd, const char* templateId, const char* frameId);
	ciBoolean	_getScheduleList(const char* viewName, const char* hostId,
									const char* templateId,const char* frameId,
									ciStringList& outList);
	ciBoolean	_saveDefaultSchedule(FILE* fd, 
								const char* siteId,
								const char* hostId, 
								 const char* templateId,
								 const char* frameId,
								 const char* scheduleId);

	ciBoolean	_saveSchedule(FILE* fd, 
							const char* siteId,
							const char* hostId, 
							 const char* templateId,
							 const char* frameId,
							 const char* scheduleId);


	static scheduleSaver*	_instance;
	static ciMutex			_instanceLock;


};

#endif // _scheduleSaver_h_
