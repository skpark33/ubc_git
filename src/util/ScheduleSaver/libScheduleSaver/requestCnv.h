#ifndef _requestCnv_h_
#define _requestCnv_h_

#include <idl/CCI/CCITypes.h>
#include <idl/CCI/CCI_s.h>

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciListType.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libThread/ciSyncUtil.h>
#include <cci/libPS/cciPS.h>
#include <cci/libWrapper/cciEvent.h>
#include <cci/libWrapper/cciEventHandler.h>

#define KTF_COMMON_1	"07101+07103+07105"			// 07101, 07103, 07105
#define KTF_COMMON_2	"07107+07109+07111"			// 07107, 07109, 07111
#define KTF_BUMGYE		"07101"			// 07101
#define KTF_GILDONG		"07103"			// 07103
#define KTF_MACHUN		"07105"			// 07105
#define KTF_SS			"07107"			// 07107
#define KTF_SAMSUNG		"07109"			// 07109
#define KTF_KTEC		"07111"			// 07111
#define CINUS_DEMO		"06001"

class requestCnv  {
public:
	static requestCnv*	getInstance();
	static void	clearInstance();

	virtual ~requestCnv() ;
	
	ciBoolean	cnv(const char* before, const char* after, const char* tId, FILE* fp);

protected:
	requestCnv();

	ciBoolean	_getPid(const char* oldId, const char* tId, ciString& newId);
	ciBoolean	_getTid(const char* hostIdList, ciString& tId);


	static requestCnv*	_instance;
	static ciMutex			_instanceLock;


};

#endif // _requestCnv_h_
