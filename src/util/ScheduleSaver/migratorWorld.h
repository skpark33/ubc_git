 /*! \file migratorWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _migratorWorld_h_
#define _migratorWorld_h_


#include <ci/libWorld/ciWorld.h> 


class migratorWorld : public ciWorld {
public:
	migratorWorld();
	~migratorWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

	void	migStage();
	void	createStage(const char* sosName, const char* templateId, 
						ciBoolean isVisible, int playOrder);

	ciBoolean getSOSId(const char* sosName, ciString& outVal);
	ciBoolean getPOTId(const char* sosId, ciString& outVal);

protected:	

	ciStringMap	_sosMap;
	ciStringMap _potMap;

};
		
#endif //_migratorWorld_h_
