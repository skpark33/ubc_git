#ifndef _serialSocketSession_h_
#define _serialSocketSession_h_

#include <ci/libThread/ciMutex.h>
#include "serialSocketHandler.h"

class serialSocketSession : public serialSocketHandler {
public:
	serialSocketSession();
	virtual ~serialSocketSession();

	 int	svc();
	
	 ciBoolean  readline(ciString& outVal);
	 ciBoolean	writeline(ciString& inVal);

protected:	
	ciMutex		_sockLock;
};

#endif
