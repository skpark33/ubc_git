 /*! \file serialGWWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _serialGWWorld_h_
#define _serialGWWorld_h_


#include <ci/libWorld/ciWorld.h> 
#include <ci/libThread/ciMutex.h> 
#include <ci/libBase/ciListType.h> 
#include "serialSocketAcceptor.h"

class CHangerDebugDlg;

class serialGWWorld : public ciWorld {
public:
	serialGWWorld();
	~serialGWWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	virtual ciBoolean fini(long p_finiCode);

	ciBoolean accept();
	void setDlg(CHangerDebugDlg* dlg) { _acceptor.setDlg(dlg); }

protected:	

	serialSocketAcceptor	_acceptor;
};
		
#endif //_serialGWWorld_h_
