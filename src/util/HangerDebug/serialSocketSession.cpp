#include "resource.h"
#include "HangerDebugDlg.h"
#include <ci/libWorld/ciWorld.h>
#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciAceType.h>
#include <ci/libBase/ciStringUtil.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libConfig/ciConfig.h>
#include <common/libCommon/ubcIni.h>
#include "serialSocketSession.h"

ciSET_DEBUG(10,"serialSocketSession");

//
// serialSocketData Class
//

serialSocketSession::serialSocketSession() {
	ciDEBUG(1,("serialSocketSession"));
}

serialSocketSession::~serialSocketSession() {
	ciDEBUG(1,("~serialSocketSession"));
}

int
serialSocketSession::svc() {
	ciDEBUG(1,("svc()"));

	ciString key = "";
	ACE_INET_Addr addr;
	peer().get_remote_addr(addr);
	const char* ip = addr.get_host_addr();
	
	while(1) {
		ciString name="";
		if(!readline(name)){
			ciDEBUG(1,("recv error"));
			break;
		}
		if(name.empty()){
			ciDEBUG(1,("recv name error"));
			break;
		}		

		if(_dlg)
		{
			ciString outval;
			_dlg->Put(name, outval);
			writeline(outval);
		}

	}// receive loop while

	ciDEBUG(1,("Client : connection close....\n"));
	return 0;
}

ciBoolean
serialSocketSession::writeline(ciString& inVal)
{
	ciDEBUG(9,("Write to socket(%s)", inVal.c_str()));

	ciGuard aGuard(_sockLock);

	inVal += "\0";

	int aSendLen = peer().send_n(inVal.c_str(), (int)inVal.length()+1);	
	if (aSendLen <= 0) {
			ciERROR(("socket() write error"));
			return ciFalse;
	}
	ciDEBUG(9,("tcpSocketSession (%s) sent", inVal.c_str()));
	return ciTrue;
}

ciBoolean
serialSocketSession::readline(ciString& outVal)
{
	ciDEBUG(9,("readline()"));

	ciGuard aGuard(_sockLock);

	ciBoolean eof = ciFalse;

	while(1) {
		char recvBuf[2];
		memset(recvBuf, 0x00, sizeof(recvBuf));
		int nread = peer().recv_n(recvBuf,1);
		if(nread < 0) {
			ciERROR(("socket read error"));
			return ciFalse;
		}
		if(nread == 0) {
			ciDEBUG(1,("unexpected socket EOF"));
			return ciFalse;
		}
		if(!eof && (recvBuf[0] == '\n')) {  // '0'
			eof = ciTrue;
			ciDEBUG(8,("tcpSocketSession received(%s)",outVal.c_str()));
			return ciTrue;
		}
		outVal += recvBuf;
	}
	return ciTrue;
}


