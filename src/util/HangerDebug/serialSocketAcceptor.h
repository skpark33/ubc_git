#ifndef _serialSocketAcceptor_h_
#define _serialSocketAcceptor_h_

#include <ace/SOCK_Acceptor.h>
#include <ace/Acceptor.h>
#include "serialSocketSession.h"

class CHangerDebugDlg;

class serialSocketAcceptor : public ACE_Acceptor<serialSocketSession, ACE_SOCK_ACCEPTOR> {
public:
	serialSocketAcceptor();
	virtual ~serialSocketAcceptor();

	void setDlg(CHangerDebugDlg* dlg) { _dlg = dlg; }
		

    virtual int make_svc_handler(serialSocketSession*& pSvcHandler);
    virtual int activate_svc_handler(serialSocketSession* pSvcHandler);

protected:
	typedef ACE_Acceptor<serialSocketSession, ACE_SOCK_ACCEPTOR> inherited;

	CHangerDebugDlg* _dlg;
};

#endif
