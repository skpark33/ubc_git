// HangerDebugDlg.h : 헤더 파일
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "serialSocketAcceptor.h"
#include "CheckListCtrl.h"

// CHangerDebugDlg 대화 상자
class CHangerDebugDlg : public CDialog
{
// 생성입니다.
public:
	CHangerDebugDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_HANGERDEBUG_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CCheckListCtrl m_listctrl;
	CStatic m_static_threshold;
	afx_msg void OnBnClickedOk();

	static UINT StartServer(LPVOID pParam);

protected:
	void	InitList();
	void	SetFont();
	//ciBoolean	Accept();

	CFont	m_font;
	serialSocketAcceptor	_acceptor;

	int m_threshold ;

	class BeaconInfo
	{
	public:
		BeaconInfo() : rtime_t(0) {}
		ciString	status;
		ciString	beaconId;
		ciString	rtime;
		time_t		rtime_t;
	};

	typedef vector<BeaconInfo*> BeaconVector;
	BeaconVector m_beacon_array;

public:
	ciBoolean Put(ciString& input, ciString& output);
	ciBoolean Parsing(ciString& input, ciString& beaconId, ciString& rtime,ciString& x,ciString& y,ciString& z,
		ciString& xy,ciString& yz,ciString& zx, ciString& status);

	void Clear();


	afx_msg void OnHdnItemdblclickBeaconList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkBeaconList(NMHDR *pNMHDR, LRESULT *pResult);
};
