/*! \file serialGWWorld.C
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author 
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
*/

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libDebug/ciDebug.h>
#include <ci/libBase/ciTime.h>
#include <ci/libDebug/ciArgParser.h>
#include <ci/libConfig/ciConfig.h>
#include <ci/libConfig/ciEnv.h>
#include <ci/libConfig/ciXProperties.h>
#include <common/libScratch/scratchUtil.h>
#include <common/libCommon/ubcIni.h>
#include "serialGWWorld.h"

ciSET_DEBUG(10, "serialGWWorld");

serialGWWorld::serialGWWorld() {
	ciDEBUG(3, ("serialGWWorld()"));
}

serialGWWorld::~serialGWWorld() {
	ciDEBUG(3, ("~serialGWWorld()"));
}	

ciBoolean 
serialGWWorld::init(int& p_argc, char** &p_argv) 
{
	ciDEBUG(5, ("serialGWWorld::init(%d)", p_argc));	
	
	if(ciWorld::init(p_argc,p_argv) == ciFalse) {
		ciERROR(("ciWorld::init() is FAILED"));
		return ciFalse;
	}
	ciDebug::setDebugOn();
	ciWorld::ylogOn();

	ciDEBUG(5, ("serialGWWorld::init(%d) end", p_argc));	

	accept();

	return ciTrue;
}


ciBoolean 
serialGWWorld::fini(long p_finiCode)
{
	ciDEBUG(1,("fini()"));
	_acceptor.close();
	return ciWorld::fini(p_finiCode); 
}

ciBoolean
serialGWWorld::accept()
{
	int port=15003;
	ciString portStr;
	if(ciArgParser::getInstance()->getArgValue("+port", portStr)){
		port = atoi(portStr.c_str());
	}
	ACE_INET_Addr rAddr(port);
	if( _acceptor.open(rAddr) < 0 ) {
		ciERROR(("session's open() for Port[%d] is FAILED", port ));
		return ciFalse;
	}
	ciDEBUG(1,("Socket accepted(%ld) for read", port));
	return ciTrue;
}


