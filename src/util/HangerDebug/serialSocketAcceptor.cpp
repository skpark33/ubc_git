#include "resource.h"
#include "HangerDebugDlg.h"
#include <ci/libDebug/ciDebug.h>
#include "serialSocketAcceptor.h"

ciSET_DEBUG(10, "serialSocketAcceptor");

serialSocketAcceptor::serialSocketAcceptor() 
{
	_dlg = 0;
}

serialSocketAcceptor::~serialSocketAcceptor() {
}

int
serialSocketAcceptor::make_svc_handler(serialSocketSession*& pSvcHandler)
{
    ciDEBUG(1,("make_svc_handler(SvcHandler)"));


    int retval= inherited::make_svc_handler(pSvcHandler);
	if(_dlg && pSvcHandler) {
		pSvcHandler->setDlg(_dlg);
	}
	return retval;
}

int
serialSocketAcceptor::activate_svc_handler(serialSocketSession* pSvcHandler)
{
    ciDEBUG(1,("activate_svc_handler(SvcHandler)"));

	return inherited::activate_svc_handler(pSvcHandler);
}
