// HangerDebugDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "HangerDebug.h"
#include "HangerDebugDlg.h"
#include <ci/libDebug/ciArgParser.h>
#include "ci/libBase/ciStringTokenizer.h"
#include "ci/libBase/ciTime.h"

#include "serialGWWorld.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CHangerDebugDlg 대화 상자

ciSET_DEBUG(10, "CHangerDebugDlg");


CHangerDebugDlg::CHangerDebugDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHangerDebugDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_threshold = 500;
}

void CHangerDebugDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BEACON_LIST, m_listctrl);
	DDX_Control(pDX, IDC_STATIC_THR, m_static_threshold);
}

BEGIN_MESSAGE_MAP(CHangerDebugDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CHangerDebugDlg::OnBnClickedOk)
	ON_NOTIFY(HDN_ITEMDBLCLICK, 0, &CHangerDebugDlg::OnHdnItemdblclickBeaconList)
	ON_NOTIFY(NM_DBLCLK, IDC_BEACON_LIST, &CHangerDebugDlg::OnNMDblclkBeaconList)
END_MESSAGE_MAP()


// CHangerDebugDlg 메시지 처리기

BOOL CHangerDebugDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	SetFont();

	m_listctrl.InsertColumn(0, "", LVCFMT_LEFT, 28);
	m_listctrl.InsertColumn(1, _T("BEACONID"), LVCFMT_LEFT, 170);
	m_listctrl.InsertColumn(2, _T("STATUS"), LVCFMT_LEFT, 100);
	m_listctrl.InsertColumn(3, _T("TIME"), LVCFMT_LEFT, 175);
	m_listctrl.InsertColumn(4, _T("X"), LVCFMT_LEFT, 60);
	m_listctrl.InsertColumn(5, _T("Y"), LVCFMT_LEFT, 60);
	m_listctrl.InsertColumn(6, _T("Z"), LVCFMT_LEFT, 60);
	m_listctrl.InsertColumn(7, _T("XY"), LVCFMT_LEFT, 60);
	m_listctrl.InsertColumn(8, _T("YZ"), LVCFMT_LEFT, 60);
	m_listctrl.InsertColumn(9, _T("ZX"), LVCFMT_LEFT, 60);
	m_listctrl.InsertColumn(10, _T("MSG"), LVCFMT_LEFT, 300);

 	m_listctrl.SetMaxColumn(11);
    m_listctrl.Initialize();

	m_listctrl.SetExtendedStyle(
        m_listctrl.GetExtendedStyle() | LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	InitList();

	BringWindowToTop();
	SetForegroundWindow();
	SetFocus();
	::SetWindowPos(this->m_hWnd, HWND_TOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED | SWP_SHOWWINDOW);

	::AfxBeginThread((AFX_THREADPROC)StartServer, this);



	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CHangerDebugDlg::SetFont()
{
	  m_font.CreateFont( 16,                          // nHeight
						  8,                          // nWidth
						   0,                          // nEscapement
						   0,                          // nOrientation
						   1,                           // nWeight
						   0,                          // bItalic
						   0,                          // bUnderline 
						   0,                          // cStrikeOut 
						   0,                          // nCharSet
						 OUT_DEFAULT_PRECIS,           // nOutPrecision 
						   0,                          // nClipPrecision 
						 DEFAULT_QUALITY,              // nQuality
						 DEFAULT_PITCH | FF_DONTCARE,  // nPitchAndFamily 
						 "Tahoma" );	  // lpszFacename
	  
	m_listctrl.SetFont(&m_font);
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CHangerDebugDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CHangerDebugDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CHangerDebugDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//OnOK();

	m_listctrl.DeleteAllItems();
	InitList();

}

void CHangerDebugDlg::Clear()
{
	BeaconVector::iterator itr;
	for(itr=m_beacon_array.begin();itr!=m_beacon_array.end();itr++)
	{
		BeaconInfo* ele = *itr;
		if(ele) delete ele;
	}
	m_beacon_array.clear();

}

void CHangerDebugDlg::InitList()
{
	ciDEBUG(1,("InitList() 1"));
	FILE* fp = fopen("C:\\SQISoft\\Contents\\Enc\\beaconList.txt","rb");
	if(fp==NULL)
	{
		return;
	}
	Clear();	

	ciDEBUG(1,("InitList() 2"));
	int index = 0;
	int line = 0;
	char buf[1024];
	memset(buf,0x00,1024);
	while(fgets(buf,1024,fp))
	{
		//if(index == 0)
		//{
		//	m_listctrl.InsertItem(index, "");
		//	m_listctrl.SetItemData(index, index);
		//}
		
		line++;
		if(line==1) 
		{
			m_threshold = atoi(buf);
			ciString strThreshold = "임계치 : ";
			strThreshold += buf;
			this->m_static_threshold.SetWindowText(strThreshold.c_str());
			memset(buf,0x00,1024);
			continue;
		}
		
		int len = strlen(buf);
		if(len < 5)
		{
			memset(buf,0x00,1024);
			continue;
		}
		buf[len-2]=0;  // remove \r\n
		m_listctrl.InsertItem(index, "");
		m_listctrl.SetItemText(index, 1, buf);	
		m_listctrl.RedrawItems(index, index);

		BeaconInfo* aEle = new BeaconInfo();
		aEle->beaconId = buf;

		m_beacon_array.push_back(aEle);
		
		memset(buf,0x00,1024);
		index++;
	}
	fclose(fp);
	ciDEBUG(1,("InitList() 3"));
}

UINT CHangerDebugDlg::StartServer(LPVOID pParam)
{
	CHangerDebugDlg *pDlg = (CHangerDebugDlg*)pParam;
	
	serialGWWorld* world = new serialGWWorld();
    if(!world) {
        cerr<<"Unable to create serialGWWorld"<<endl;
        exit(1);
    }

	world->setDlg(pDlg);

	if(world->init(__argc, __argv) == ciFalse) {
        cerr<<"Init world error"<<endl;
        world->fini(2);
    }
	world->run();
    world->fini(0);
}
/*
ciBoolean
CHangerDebugDlg::Accept()
{
	int port=15003;
	ciString portStr;
	if(ciArgParser::getInstance()->getArgValue("+port", portStr)){
		port = atoi(portStr.c_str());
	}

	ACE_INET_Addr rAddr(port);
	_acceptor.setDlg(this);
	if( _acceptor.open(rAddr) < 0 ) {
		ciERROR(("session's open() for Port[%d] is FAILED", port ));
		return ciFalse;
	}
	ciDEBUG(1,("Socket accepted(%ld) for read", port));

	return ciTrue;
}
*/

ciBoolean
CHangerDebugDlg::Parsing(ciString& input, ciString& beaconId, ciString& rtime,ciString& x,ciString& y,ciString& z, 
						 ciString& xy,ciString& yz,ciString& zx,ciString& status)
{
	ciStringTokenizer aTokens(input.c_str(),",\r\n");

	// <R|C|P>,<beaconId>,<time>,x,y,z,xy,yz,zx

	if(aTokens.countTokens() < 9)
	{
		ciERROR(("Invalid input(%s)", input.c_str()));
		status = "Invalid";
		return ciFalse;
	}

	ciString key = aTokens.nextToken();

	if(key == "R") {
		status = "Received";
	} else if(key == "C") {
		status = "Moved";
	} else if(key == "P") {
		status = "Pickuped";
	} else if(key == "E") {
		status = "Error";
	} else	{
		ciERROR(("Invalid input(%s)", input.c_str()));
		status = "Invalid";
		return ciFalse;
	}

	beaconId = aTokens.nextToken();
	rtime = aTokens.nextToken();
	x = aTokens.nextToken();
	y = aTokens.nextToken();
	z = aTokens.nextToken();
	xy = aTokens.nextToken();
	yz = aTokens.nextToken();
	zx = aTokens.nextToken();

	return ciTrue;
}


ciBoolean
CHangerDebugDlg::Put(ciString& input, ciString& output)
{
	ciStringTokenizer aTokens(input.c_str(),",\r\n");

	ciString beaconId;
	ciString rtime,x,y,z,xy,yz,zx, status;

	if(Parsing(input,beaconId,rtime,x,y,z,xy,yz,zx,status))
	{
		output = "OK\n";
	}else{
		output = "NO\n";
	}


	int index = -1;
	int len = m_beacon_array.size();

	for(int i=0;i<len;i++){
		if(m_beacon_array[i]->beaconId == beaconId)
		{
			index = i;

			ciTime now(rtime.c_str());
			time_t now_t = now.getTime();

			ciDEBUG(1,("becaonId(%s,%s) founded", beaconId.c_str(), rtime.c_str()));


			if(m_beacon_array[i]->status == "Pickuped" && now_t - m_beacon_array[i]->rtime_t < 3)
			{
				m_listctrl.SetItemText(index, 10, (status + "/" + rtime).c_str());	
				m_listctrl.RedrawItems(index, index);
				return true;
			}
			else
			{
				m_beacon_array[i]->status = status;
				m_beacon_array[i]->rtime = rtime;
				m_beacon_array[i]->rtime_t = now_t;
				m_listctrl.SetItemText(index, 10, "");	
			}
			
			break;
		}
	}
	
	if(index < 0 || index >= len)
	{
		ciERROR(("becaonId(%s) not founded", beaconId.c_str()));
		index = len;
		if(m_listctrl.GetItemCount() < index+1) {
			m_listctrl.InsertItem(index, "");
			ciDEBUG(1,("index=%d",index));
		}
		m_listctrl.SetItemText(index, 1, beaconId.c_str());	
		status = "Unknown";
	}

	if(status == "Error")
	{
		m_listctrl.SetItemText(index, 10, x.c_str());	
		x = "";
	}

	m_listctrl.SetItemText(index, 2, status.c_str());	
	m_listctrl.SetItemText(index, 3, rtime.c_str());
	m_listctrl.SetItemText(index, 4, x.c_str());	
	m_listctrl.SetItemText(index, 5, y.c_str());	
	m_listctrl.SetItemText(index, 6, z.c_str());	
	m_listctrl.SetItemText(index, 7, xy.c_str());	
	m_listctrl.SetItemText(index, 8, yz.c_str());	
	m_listctrl.SetItemText(index, 9, zx.c_str());	

	m_listctrl.RedrawItems(index, index);

	return ciTrue;
}

void CHangerDebugDlg::OnHdnItemdblclickBeaconList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.


	*pResult = 0;
}

void CHangerDebugDlg::OnNMDblclkBeaconList(NMHDR *pNMHDR, LRESULT *pResult)
{
	::SetWindowPos(this->m_hWnd, HWND_NOTOPMOST, -1, -1, -1, -1, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
	::ShellExecute(NULL, "open", "iexplore.exe", "http://localhost:8000/api/chart", "", SW_SHOW);
	*pResult = 0;
}
