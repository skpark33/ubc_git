// UBCCleaner.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "UBCCleaner.h"
#include <ci/libDebug/ciDebug.h>
#include <cci/libWrapper/cciCall.h>
#include <cci/libValue/cciAny.h>
#include "SCGWorld.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 유일한 응용 프로그램 개체입니다.

CWinApp theApp;

using namespace std;

#include <libUBCCleaner/UBCCleaner.h>


//int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
int main(int argc, char* argv[])
{
	SCGWorldThread* pworldThread = new SCGWorldThread();
	//ciDEBUG(10, ("SCGWorldThread init"));
	pworldThread->init();
	//ciDEBUG(10, ("SCGWorldThread before start"));
	pworldThread->start();
	//ciDEBUG(10, ("SCGWorldThread after start"));

	int nRetCode = 0;
	ciDebug::setDebugOn();
	ciDebug::logOpen("UBCCleaner.log");

	// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		_tprintf(_T("심각한 오류: MFC를 초기화하지 못했습니다.\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 응용 프로그램의 동작은 여기에서 코딩합니다.
		cout << "\r\n======================================================\r\n";

		CUBCCleaner clsCleaner;
		list<string> listContentsPackage;

		cout << "Get contentspackage list from server...\r\n";

		if(!clsCleaner.GetContentsPackageList(listContentsPackage))
		{
			cout << "\r\nFail to get contents package list !\r\n";
			system("pause");
			return 0;
		}//if

		cout << "Contentspackage count = " << listContentsPackage.size() << endl;

		ULONGLONG ullSize = clsCleaner.Clean(listContentsPackage, true);

		cout << "\r\nTotal deleted size = " << ullSize << endl;
		cout << "\r\n======================================================\r\n";
		system("pause");
		cout << "\r\nGood bye... \r\n";
	}

	return nRetCode;
}
