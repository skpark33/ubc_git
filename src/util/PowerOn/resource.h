//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by PowerOn.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_POWERON_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_MAC_1                       1000
#define IDC_MAC_2                       1001
#define IDC_MAC_3                       1002
#define IDC_MAC_4                       1003
#define IDC_MAC_5                       1004
#define IDC_MAC_6                       1005
#define IDC_POWER_ON                    1006
#define IDC_PORT                        1007
#define IDC_SECUREON_ENABLE             1008
#define IDC_PASS_1                      1009
#define IDC_PASS_2                      1010
#define IDC_PASS_3                      1011
#define IDC_PASS_4                      1012
#define IDC_PASS_5                      1013
#define IDC_PASS_6                      1014
#define IDC_PASS_TXT                    1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
