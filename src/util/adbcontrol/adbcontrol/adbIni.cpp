#include "stdafx.h"
#include "adbIni.h"
#include "common/ubcdefine.h"
#include "common\TraceLog.h"



adbIni::adbIni(const char* iniName)
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	m_path.Format(_T("%s%sdata\\%s"), szDrive, szPath, iniName);

	TraceLog(("adbIni path=%s", m_path));

	adbCommand= getValue("adbcontrol","adbCommand","");
	screenshotDelay = getValue("adbcontrol","screenshotDelay",1000);
	localImageFilePath = getValue("adbcontrol","localImageFilePath","");
	phoneImageFilePath = getValue("adbcontrol","phoneImageFilePath","");

	if(adbCommand.IsEmpty()){
		adbCommand.Format(_T("%s%s\\adb.exe"), szDrive, szPath);
	}
	if(localImageFilePath.IsEmpty()){
		localImageFilePath.Format(_T("%s\\SQISoft\\ScreenShot"), szDrive);
	}
	if(phoneImageFilePath.IsEmpty()){
		phoneImageFilePath = "/mnt/sdcard/adbcontrol_screenshot.png";
	}

	TraceLog(("adbCommand=%s", adbCommand));
	TraceLog(("screenshotDelay=%d", screenshotDelay));
	TraceLog(("localImageFilePath=%s", localImageFilePath));
	TraceLog(("phoneImageFilePath=%s", phoneImageFilePath));

}


int	
adbIni::getValue(const char* entry, const char* name, int defaultVal)
{
	m_intVal = defaultVal;
	m_intVal = ::GetPrivateProfileInt(entry, name, defaultVal, m_path);
	return m_intVal;
}

CString 
adbIni::getValue(const char* entry, const char* name, const char* defaultVal)
{
	char buf1[1024];
	m_strVal = defaultVal;
	::GetPrivateProfileString(entry, name, defaultVal, buf1, 1024, m_path);
	m_strVal = buf1;
	return m_strVal;

}




