#include "stdafx.h"
#include "adbHelper.h"
#include "common/TraceLog.h"
#include <stdio.h>

bool 
CadbHelper::connect(CString& ip)
{
	CString cmd = "connect";
	cmd += " ";
	cmd += ip;
	if(executeCommand_popen(cmd)) {
	//if(executeCommand(cmd)) {
		m_connected = true;
	}else{
		m_connected = false;
	}
	return m_connected;
}

bool 
CadbHelper::disconnect(CString& ip)
{
	if(m_connected) {
		CString cmd = "disconnect";
		cmd += " ";
		cmd += ip;
		if(executeCommand_popen(cmd)) {
		//if(executeCommand(cmd)) {
			m_connected = false;
		}
	}
	return (!m_connected);

}

bool 
CadbHelper::executeCommand_popen(CString& cmd,bool checkResult)
{
	time_t now = time(NULL);
	CString errFile;
	if(checkResult) {
		errFile.Format("%s\\%s_%ld.log",
			config->localImageFilePath , config->ipAddress ,now);

		::DeleteFile(errFile);
	}
	m_errMsg = "";
	m_resultMsg = "";
	CString cmdLine = config->adbCommand + " " + cmd + " 2> " + errFile;
	//CString cmdLine = config->adbCommand + " " + cmd ;

	TraceLog(("executeCommand(%s)", cmdLine));

	FILE *pipein_fp;
	if( (pipein_fp=_popen(cmdLine, "r")) == NULL )
	{
		TraceLog(("popen error !!! %s", cmdLine));
		m_errMsg = cmdLine ;
		m_errMsg += " failed";
		return false;
	}

	/*반복 처리*/
	char readbuf[1024] = {0};
	while( fread(readbuf, 1, sizeof(readbuf)-1, pipein_fp) )
	{
		m_resultMsg += readbuf;
		TraceLog((readbuf));
		memset(readbuf, 0, sizeof(readbuf));
	}

	/*파이프를 닫는다*/
	_pclose(pipein_fp);
	if(checkResult) {

		FILE* errFile_fp = fopen(errFile,"r");
		if(errFile_fp) {
			while( fread(readbuf, 1, sizeof(readbuf)-1, pipein_fp) )
			{
				m_errMsg += readbuf;
				TraceLog((readbuf));
				memset(readbuf, 0, sizeof(readbuf));
			}
			if(!m_errMsg.IsEmpty()){
				TraceLog(("%s error(%s)", cmdLine, m_errMsg));
				return false;
			}
			fclose(errFile_fp);
			//::DeleteFile(errFile);
		}
	}
	TraceLog(("popen succeed !!! %s", cmdLine));
	if(m_resultMsg.IsEmpty()){
		m_resultMsg = "succeed";
	}
	return true;
}

bool 
CadbHelper::executeCommand(CString& cmd)
{
	m_errMsg = "";
	m_resultMsg = "";

	HINSTANCE nRet = ShellExecute(NULL, NULL, config->adbCommand, cmd, NULL, SW_HIDE);
	if(int(nRet) > 32)
	{
		m_resultMsg.Format("%s succeed(%d)",  cmd , int(nRet));
		return true;
	}
	m_errMsg.Format("%s failed(%d)",  cmd , int(nRet));
	return false;
}

bool 
CadbHelper::sendClick(int x, int y)
{
	//System.out.println("Click at: " + x + "/" + y);
	CString buf;
	buf.Format("input tap %d %d", x, y);
	return executeShellCommand(buf);
}

bool 
CadbHelper::sendText(CString& text)
{
	CString buf = "input text ";
	buf += text;
	return executeShellCommand(buf);
}

bool 
CadbHelper::sendKey(androidKey& key)
{
	CString buf;
	buf.Format("input keyevent %d",key.getCode());
	return executeShellCommand(buf); 
}

bool 
CadbHelper::screenshot(CString& outpath)
{
	CString fileName = config->phoneImageFilePath;
	CString buf = "screencap -p ";
	buf += fileName;

	CString outString;
	if(executeShellCommand(buf)) {
		outpath = config->localImageFilePath + "\\adbcontrol_" + config->ipAddress + ".png";

		buf = "pull ";
		buf += fileName;
		buf += " ";
		buf += outpath;

		outString.Empty();
		return executeCommand(buf);
	}
	return false;
}

bool 
CadbHelper::sendSwipe(int downX, int downY, int upX, int upY)
{
	//System.out.println("Swipe from " + downX + "/" + downY + " to " + upX + "/" + upY);
	CString buf;
	buf.Format("input swipe %d %d %d %d", downX, downY, upX, upY);
	return executeShellCommand(buf);
}

