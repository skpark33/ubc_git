#pragma once


#include <afxtempl.h>


/////////////////////////////////////////////////////////////////////////////////////////
typedef struct {
	CWnd*	pWnd;
	CRect	rect;
	int		nLeft;
	int		nTop;
	int		nRight;
	int		nBottom;
	int		nHorzCount;
	int		nVertCount;
}	REPOS_CONTROL_INFO;

typedef CArray<REPOS_CONTROL_INFO, REPOS_CONTROL_INFO&>		REPOS_CONTROL_INFO_LIST;


/////////////////////////////////////////////////////////////////////////////////////////
class CReposControl
{
public:
	CReposControl(CWnd*	pParent);
	virtual ~CReposControl();

protected:
	CWnd*	m_pParent;
	CRect	m_rectParent;
	CSize	m_MinSize;

	bool	m_bInit;

	REPOS_CONTROL_INFO_LIST		m_ReposControlInfoList;

public:
	void	SetParentRect(CRect& rect);
	void	AddControl(CWnd* pControl, int nLeft, int nTop, int nRight, int nBottom, int nHorzCount=1, int nVertCount=1);
	void	Move(int cx=0, int cy=0);
	void	Move(int x, int y, int cx, int cy);
	void	RemoveAll();
	void	Remove(CWnd* );
	void	SetMinSize(int cx, int cy);
};


/////////////////////////////////////////////////////////////////////////////////////////
#define		REPOS_FIX				-1
#define		REPOS_MOVE				0
#define		REPOS_RESIZE_DIV2		1
#define		REPOS_RESIZE_DIV3		2
#define		REPOS_RESIZE_DIV4		3
#define		REPOS_RESIZE_DIV5		5
#define		REPOS_RESIZE_DIV6		6
#define		REPOS_RESIZE_DIV7		7
#define		REPOS_RESIZE_DIV8		8
#define		REPOS_RESIZE_DIV9		9
#define		REPOS_RESIZE_DIV10		10

