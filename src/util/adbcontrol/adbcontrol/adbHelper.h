#pragma once
#include "adbIni.h"
#include "androidKey.h"

class CadbHelper
{
public:
	adbIni* config;
protected:
	CString m_errMsg;
	CString m_resultMsg;
	bool m_connected;


public:
	CadbHelper(adbIni* config)
	{
		this->config = config;
		m_connected = false;
	}

public:	
	bool connect(CString& ip);
	bool disconnect(CString& ip);
	bool executeCommand(CString& cmd);
	bool executeCommand_popen(CString& cmd, bool checkResult=true);
	bool executeShellCommand(CString& cmd)	{	return executeCommand("shell " + cmd);	}

	bool sendClick(int x, int y);
	bool sendText(CString& text);
	bool sendKey(androidKey& key);
	bool screenshot(CString& outpath);
	bool sendSwipe(int downX, int downY, int upX, int upY);


	CString getError() { return m_errMsg; }
	CString getResult() { return m_resultMsg; }
};
