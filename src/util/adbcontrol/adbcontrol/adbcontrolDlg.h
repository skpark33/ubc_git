// adbcontrolDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "adbcontrolPanel.h"
#include "adbHelper.h"
#include "reposcontrol.h"

class adbIni;

// CadbcontrolDlg 대화 상자
class CadbcontrolDlg : public CDialog
{
// 생성입니다.
public:
	CadbcontrolDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ADBCONTROL_DIALOG };

	void setIP(CString& ipAddress, bool auto_connect, unsigned long port1=5555, unsigned long port2 =0)
	{ m_ipAddress = ipAddress; m_auto_connect = auto_connect; m_port1 = port1;m_port2 = port2; 
		if(m_config)m_config->ipAddress = ipAddress; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


	bool	m_isRunning;
	adbIni*			m_config;

// 구현입니다.
protected:
	HICON m_hIcon;
	CReposControl m_Reposition;

	CString		m_ipAddress;
	unsigned long	m_port1;
	unsigned long   m_port2;
	bool		m_auto_connect;

	int			m_monitorWidth;
	int			m_monitorHeight;

	int			m_curRatio;

	CadbHelper*	m_adbHelper;
	CWinThread* m_pThread;

	void InitPosition(CRect rc);
	void ShowSize();
	void Resize(int ratio);

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CadbcontrolPanel m_picCtrl;
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedReconnect();
	afx_msg void OnBnClickedDisconnect();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedFullsize();
	afx_msg void OnBnClickedHalf();
	afx_msg void OnBnClickedQuarter();
	afx_msg void OnBnClickedRotateL();
	afx_msg void OnBnClickedRotateR();


	void	makeScreenshot();
	void	loadImage(CString& screenshotFile);
	void	showError();
	void	showResult();
	void	showMsg(const char* msg);

	bool start();
	bool stop();

	static UINT ThreadProc(LPVOID param);


	CEdit m_edit_pic_size;
};
