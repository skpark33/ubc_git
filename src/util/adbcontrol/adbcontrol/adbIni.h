#pragma once


class adbIni
{
public:
	adbIni(const char* iniName);
	virtual ~adbIni() {}
	
	int	getValue(const char* entry, const char* name, int defaultVal);
	CString getValue(const char* entry, const char* name, const char* defaultValue);

protected:
	CString m_path;
	
	CString	m_strVal;
	int		m_intVal;

public:
	CString adbCommand;
	int screenshotDelay;
	CString localImageFilePath;
	CString phoneImageFilePath;

	CString ipAddress;

};