
#include "StdAfx.h"
#include "ReposControl.h"


CReposControl::CReposControl(CWnd*	pParent)
:	m_pParent (pParent)
,	m_bInit(false)
,	m_MinSize(0,0)
{
}

CReposControl::~CReposControl()
{
}

void CReposControl::AddControl(CWnd* pControl, int nLeft, int nTop, int nRight, int nBottom, int nHorzCount, int nVertCount)
{
	if(m_bInit == false)
	{
		m_bInit = true;
		if(m_pParent->GetSafeHwnd())
			m_pParent->GetClientRect(m_rectParent);
	}

	if(pControl != NULL && pControl->GetSafeHwnd())
	{
		//
		int caption_height	= ::GetSystemMetrics(SM_CYSIZE);
		int frame_width		= ::GetSystemMetrics(SM_CXFRAME);
		int frame_height	= ::GetSystemMetrics(SM_CYFRAME);

		//
		REPOS_CONTROL_INFO info;

		info.pWnd		= pControl;
		info.nLeft		= nLeft;
		info.nTop		= nTop;
		info.nRight		= nRight;
		info.nBottom	= nBottom;
		info.nHorzCount	= nHorzCount;
		info.nVertCount	= nVertCount;
		pControl->GetWindowRect(info.rect);

//		info.rect.OffsetRect(-frame_width, -(caption_height*2+frame_height));

		m_pParent->ScreenToClient(info.rect);

		m_ReposControlInfoList.Add(info);
	}
}

void CReposControl::Move(int cx, int cy)
{
	if(m_pParent->GetSafeHwnd())
	{
		//
		CRect parent_rect;
		m_pParent->GetClientRect(parent_rect);

		(cx<m_MinSize.cx)?cx=m_MinSize.cx:cx=cx;
		(cy<m_MinSize.cy)?cy=m_MinSize.cy:cy=cy;

		int width_diff  = (cx == 0 ? parent_rect.Width() : cx) - m_rectParent.Width();//(cx == 0 ? parent_rect.Width() - m_rectParent.Width() : cx);
		int height_diff = (cy == 0 ? parent_rect.Height() : cy) - m_rectParent.Height();//(cy == 0 ? parent_rect.Height() - m_rectParent.Height() : cy);

		//
		int count = m_ReposControlInfoList.GetCount();

		for(int i=0; i<count; i++)
		{
			REPOS_CONTROL_INFO& info = m_ReposControlInfoList.GetAt(i);

			if(info.pWnd->GetSafeHwnd())
			{
				CRect rect = info.rect;

				if(info.nLeft   == 0) rect.left   += width_diff;	else if(info.nLeft   > 0) rect.left   += (width_diff  * info.nLeft   / info.nHorzCount);
				if(info.nRight  == 0) rect.right  += width_diff;	else if(info.nRight  > 0) rect.right  += (width_diff  * info.nRight  / info.nHorzCount);
				if(info.nTop    == 0) rect.top    += height_diff;	else if(info.nTop    > 0) rect.top    += (height_diff * info.nTop    / info.nVertCount);
				if(info.nBottom == 0) rect.bottom += height_diff;	else if(info.nBottom > 0) rect.bottom += (height_diff * info.nBottom / info.nVertCount);

				info.pWnd->MoveWindow(rect, FALSE);
			}
		}

		m_pParent->Invalidate(FALSE);
	}
}

void CReposControl::Move(int x, int y, int cx, int cy)
{
	if(m_pParent->GetSafeHwnd())
	{
		//
		CRect parent_rect;
		m_pParent->GetClientRect(parent_rect);

		(cx<m_MinSize.cx)?cx=m_MinSize.cx:cx=cx;
		(cy<m_MinSize.cy)?cy=m_MinSize.cy:cy=cy;

		int width_diff  = (cx == 0 ? parent_rect.Width() : cx) - m_rectParent.Width();//(cx == 0 ? parent_rect.Width() - m_rectParent.Width() : cx);
		int height_diff = (cy == 0 ? parent_rect.Height() : cy) - m_rectParent.Height();//(cy == 0 ? parent_rect.Height() - m_rectParent.Height() : cy);

		//
		int count = m_ReposControlInfoList.GetCount();

		for(int i=0; i<count; i++)
		{
			REPOS_CONTROL_INFO& info = m_ReposControlInfoList.GetAt(i);

			if(info.pWnd->GetSafeHwnd())
			{
				CRect rect = info.rect;

				if(info.nLeft   == 0) rect.left   += width_diff;	else if(info.nLeft   > 0) rect.left   += (width_diff  * info.nLeft   / info.nHorzCount);
				if(info.nRight  == 0) rect.right  += width_diff;	else if(info.nRight  > 0) rect.right  += (width_diff  * info.nRight  / info.nHorzCount);
				if(info.nTop    == 0) rect.top    += height_diff;	else if(info.nTop    > 0) rect.top    += (height_diff * info.nTop    / info.nVertCount);
				if(info.nBottom == 0) rect.bottom += height_diff;	else if(info.nBottom > 0) rect.bottom += (height_diff * info.nBottom / info.nVertCount);

				rect.left += x;
				rect.right += x;
				rect.top += y;
				rect.bottom += y;

				info.pWnd->MoveWindow(rect, FALSE);
			}
		}

		m_pParent->Invalidate(FALSE);
	}
}

void CReposControl::SetParentRect(CRect& rect)
{
	m_bInit = true;
	m_rectParent = rect;
}

void CReposControl::RemoveAll()
{
	m_ReposControlInfoList.RemoveAll();
}

void CReposControl::Remove(CWnd* pWnd)
{
	if(pWnd == NULL){
		RemoveAll();
		return;
	}

	int count = m_ReposControlInfoList.GetCount();

	for(int i=0; i<count; i++)
	{
		REPOS_CONTROL_INFO& info = m_ReposControlInfoList.GetAt(i);

		if(info.pWnd == pWnd){
			m_ReposControlInfoList.RemoveAt(i);
			return;
		}
	}
}

void CReposControl::SetMinSize(int cx, int cy)
{
	m_MinSize.cx = cx;
	m_MinSize.cy = cy;
}

/*
	// Vertical		4-resize
	m_pos.AddControl(&m_static1, REPOS_FIX, REPOS_FIX,			REPOS_MOVE, REPOS_RESIZE_DIV2,	1, 4);
	m_pos.AddControl(&m_static2, REPOS_FIX, REPOS_RESIZE_DIV2,	REPOS_MOVE, REPOS_RESIZE_DIV3,	1, 4);
	m_pos.AddControl(&m_static3, REPOS_FIX, REPOS_RESIZE_DIV3,	REPOS_MOVE, REPOS_RESIZE_DIV4,	1, 4);
	m_pos.AddControl(&m_static4, REPOS_FIX, REPOS_RESIZE_DIV4,	REPOS_MOVE, REPOS_MOVE,			1, 4);

	// Horizontal	1-fix,2-resize,1-fix
	m_pos.AddControl(&m_static1, REPOS_FIX,			REPOS_FIX, REPOS_FIX,			REPOS_MOVE, 2, 1);
	m_pos.AddControl(&m_static2, REPOS_FIX	,		REPOS_FIX, REPOS_RESIZE_DIV2,	REPOS_MOVE, 2, 1);
	m_pos.AddControl(&m_static3, REPOS_RESIZE_DIV2,	REPOS_FIX, REPOS_MOVE,			REPOS_MOVE, 2, 1);
	m_pos.AddControl(&m_static4, REPOS_MOVE,		REPOS_FIX, REPOS_MOVE,			REPOS_MOVE, 2, 1);
*/

