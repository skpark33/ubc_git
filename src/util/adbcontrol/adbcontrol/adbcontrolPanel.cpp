#include "stdafx.h"
#include "resource.h"
#include "common\TraceLog.h"
#include "adbcontrolPanel.h"
//#include "Enviroment.h"


CadbcontrolPanel::CadbcontrolPanel()
{
	m_expand = false;
	m_bgColor = NODE_COLOR_WHITE;
	m_scr_width = 270;
	m_scr_height = 480;
}

CadbcontrolPanel::~CadbcontrolPanel()
{
}

BEGIN_MESSAGE_MAP(CadbcontrolPanel, CStatic)
	//{{AFX_MSG_MAP(CadbcontrolPanel)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CadbcontrolPanel::SetSize()
{
	CRect rect;
	GetClientRect(rect);
	m_scr_width = rect.Width();
	m_scr_height = rect.Height();
}
void CadbcontrolPanel::OnPaint() 
{
	TraceLog(("OnPaint()"));
	CRect rect;
	GetClientRect(rect);
	m_scr_width = rect.Width();
	m_scr_height = rect.Height();

	if(m_bgImage.IsValid())
	{
		CPaintDC dc(this);

		//dc.FillSolidRect(rect, RGB(255,255,255));
		dc.FillSolidRect(rect, m_bgColor);
		TraceLog(("OnPaint(%d,%d)", m_scr_width, m_scr_height));

		if(!m_expand){
			double scale;

			int image_width = m_bgImage.GetWidth();
			int image_height = m_bgImage.GetHeight();


			int width, height;
			width = m_scr_width;
			height = image_width * m_scr_height / m_scr_width;
			if(height < image_height)
			{
				height = m_scr_height;
				width = image_height * m_scr_width / m_scr_height;

				scale = ((double)m_scr_height) / ((double)image_height);
			}
			else
			{
				scale = ((double)m_scr_width) / ((double)image_width);
			}

			rect.right = rect.left + m_bgImage.GetWidth() * scale;
			rect.bottom = rect.top + m_bgImage.GetHeight() * scale;
		}
		m_bgImage.Draw2(dc.GetSafeHdc(), rect);
	}
	CStatic::OnPaint();
	//CButton::OnPaint();
}

void CadbcontrolPanel::OnDestroy() 
{
	CStatic::OnDestroy();
	//CButton::OnDestroy();
	UnLoad();
}


bool CadbcontrolPanel::LocalLoad(CString& strMediaFullPath) 
{
	TraceLog(("LocalLoad(%s)", strMediaFullPath));

	CString strExt = FindExtension(strMediaFullPath);
	int nImgType = CxImage::GetTypeIdFromName(strExt);
	
	UnLoad();

	bool bRet = m_bgImage.Load(strMediaFullPath, nImgType);
	if(bRet){
		TraceLog(("%s file loaded from local", strMediaFullPath));
		return true;
	}
	TraceLog(("%s file not exist", strMediaFullPath));
	return bRet;
}
void CadbcontrolPanel::UnLoad()
{
	if(m_bgImage.IsValid())
	{
		m_bgImage.Destroy();
	}
}

void CadbcontrolPanel::RotateLeft()
{
	if(m_bgImage.IsValid())
	{
		m_bgImage.RotateLeft();
	}
}
void CadbcontrolPanel::RotateRight()
{
	if(m_bgImage.IsValid())
	{
		m_bgImage.RotateRight();
	}
}
/*
//#define DEFAULT_BORDER_COLOR    RGB(127, 157, 185)
#define DEFAULT_BORDER_COLOR    RGB(255, 0, 0)

LRESULT CadbcontrolPanel::WindowProc(UINT message,WPARAM wParam,LPARAM lParam)    
{        
	switch (message)        
	{            
		case WM_NCPAINT:   {       
			CStatic::WindowProc(message, wParam, lParam);    
		
			// convert to client coordinates                
			CRect rect;                
			GetWindowRect(&rect);                
			rect.OffsetRect(-rect.left, -rect.top);
						
			// Draw a single line around the outside                
			CWindowDC dc(this);                
			dc.Draw3dRect(&rect, DEFAULT_BORDER_COLOR, DEFAULT_BORDER_COLOR);                
			return 0; // Handled.            
		}        
	}        
	return CStatic::WindowProc(message, wParam, lParam);
}
*/