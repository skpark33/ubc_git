// adbcontrolDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "adbcontrol.h"
#include "adbcontrolDlg.h"
#include "adbIni.h"
#include "common/ubcdefine.h"
#include "common/TraceLog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define WIDTH_OFFSET   56
#define HEIGHT_OFFSET  61
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CadbcontrolDlg 대화 상자




CadbcontrolDlg::CadbcontrolDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CadbcontrolDlg::IDD, pParent)
	,	m_Reposition(this)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_port1 = 5555;
	m_port2 = 0;
	m_curRatio = 4;
	m_config = new adbIni(UBCVARS_INI);

	m_monitorWidth = ::GetSystemMetrics(SM_CXSCREEN);						// 모니터의 해상도 x
	m_monitorHeight = ::GetSystemMetrics(SM_CYSCREEN);						// 모니터의 해상도 y
	
	m_adbHelper = new CadbHelper(m_config);

	m_pThread = 0;
	m_isRunning = false;
	m_auto_connect = false;

}

void CadbcontrolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PIC_CTRL, m_picCtrl);
	DDX_Control(pDX, IDC_EDIT_WIDTH, m_edit_pic_size);
}

BEGIN_MESSAGE_MAP(CadbcontrolDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_BN_CLICKED(ID_RECONNECT, &CadbcontrolDlg::OnBnClickedReconnect)
	ON_BN_CLICKED(ID_DISCONNECT, &CadbcontrolDlg::OnBnClickedDisconnect)
	ON_BN_CLICKED(IDCANCEL, &CadbcontrolDlg::OnBnClickedCancel)
	ON_BN_CLICKED(ID_FULLSIZE, &CadbcontrolDlg::OnBnClickedFullsize)
	ON_BN_CLICKED(ID_HALF, &CadbcontrolDlg::OnBnClickedHalf)
	ON_BN_CLICKED(ID_QUARTER, &CadbcontrolDlg::OnBnClickedQuarter)
	ON_BN_CLICKED(ID_ROTATE_L, &CadbcontrolDlg::OnBnClickedRotateL)
	ON_BN_CLICKED(ID_ROTATE_R, &CadbcontrolDlg::OnBnClickedRotateR)
END_MESSAGE_MAP()


// CadbcontrolDlg 메시지 처리기

BOOL CadbcontrolDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CRect rcClient;
	GetClientRect(&rcClient);



	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	CString title = "adbcontrol : " + this->m_ipAddress;
	this->SetWindowText(title);

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	InitPosition(rcClient);

	showMsg("Press Connect button");

	OnBnClickedHalf();
	//ShowSize();

	//if(this->m_auto_connect) {
	//	OnBnClickedReconnect();
	//}
	
	this->RedrawWindow();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CadbcontrolDlg::makeScreenshot()
{
	CString screenshotFile;
	if(m_adbHelper->screenshot(screenshotFile)) {
		loadImage(screenshotFile);
	}else{
		showError();
	}
}

void CadbcontrolDlg::loadImage(CString& screenshotFile)
{
	m_picCtrl.LocalLoad(screenshotFile);
	m_picCtrl.ShowWindow(SW_NORMAL);
}
void CadbcontrolDlg::showError()
{
	CString msg = "ERROR :";
	msg += this->m_adbHelper->getError();
	m_edit_pic_size.SetWindowText(msg);
}
void CadbcontrolDlg::showResult()
{
	CString msg = "RESULT :";
	msg += this->m_adbHelper->getResult();
	m_edit_pic_size.SetWindowText(msg);
}
void CadbcontrolDlg::showMsg(const char* msg)
{
	m_edit_pic_size.SetWindowText(msg);
}

void CadbcontrolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CadbcontrolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CadbcontrolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CadbcontrolDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnClose();
}

void CadbcontrolDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	TraceLog(("OnSize(%d,%d)", cx,cy));

	m_Reposition.Move();
	this->m_picCtrl.RedrawWindow();
	//this->ShowSize();
	this->RedrawWindow();
}
void CadbcontrolDlg::ShowSize()
{
	int c_width = this->m_picCtrl.GetWidth();
	int c_height = this->m_picCtrl.GetHeight();
	int s_width = this->m_picCtrl.GetScrWidth();
	int s_height = this->m_picCtrl.GetScrHeight();
	
	double ratio_w = 0.0;
	if(s_width > 0 ){
		ratio_w = double(s_width)/double(c_width);
		ratio_w *= 100.0;
	}
	double ratio_h = 0.0;
	if(s_height > 0){
		ratio_h = double(s_height)/double(c_height);
		ratio_h *= 100.0;
	}

	char buf_size[200];
	sprintf(buf_size, "picture size = %dx%d, view size = %dx%d (%d%%x%d%%)", 
		c_width, c_height, s_width, s_height, int(ratio_w), int(ratio_h));

	m_edit_pic_size.SetWindowText(buf_size);
}

void CadbcontrolDlg::InitPosition(CRect rc)
{
	m_Reposition.SetParentRect(rc);
	m_Reposition.RemoveAll();

	m_Reposition.AddControl(&m_picCtrl, REPOS_FIX, REPOS_FIX, REPOS_MOVE, REPOS_MOVE);
	m_Reposition.AddControl(&m_edit_pic_size, REPOS_FIX, REPOS_MOVE, REPOS_MOVE, REPOS_MOVE);
}
void CadbcontrolDlg::OnBnClickedReconnect()
{
	if(!m_adbHelper->connect(this->m_ipAddress)) {
		showError();
		return;
	}
	showResult();
	start();
}

void CadbcontrolDlg::OnBnClickedDisconnect()
{
	stop();
	if(m_adbHelper->disconnect(this->m_ipAddress)) {
		showResult();
	}else{
		showError();
	}

}

void CadbcontrolDlg::OnBnClickedCancel()
{
	OnBnClickedDisconnect();
	OnCancel();
}

void CadbcontrolDlg::Resize(int ratio)
{
	int w = this->m_picCtrl.GetWidth()/ratio;
	int h = this->m_picCtrl.GetHeight()/ratio;

	if(w==0) {
		w = 270;
	}
	if(h==0) {
		h = 480;
	}


	m_curRatio = ratio;
		
	int width = w+WIDTH_OFFSET;
	int height = h+HEIGHT_OFFSET;

	if(width > m_monitorWidth) {
		height = double(height) * double(m_monitorWidth)/double(width); 
		width = m_monitorWidth;
	}
	if(height > m_monitorHeight) {
		width = double(width) * double(m_monitorHeight)/double(height); 
		height = m_monitorHeight;
	}
	
	CRect rc;
	GetWindowRect(&rc);

	MoveWindow(rc.left,rc.top,width,height);
	this->m_picCtrl.SetSize();

}
void CadbcontrolDlg::OnBnClickedFullsize()
{
	this->Resize(1);
}

void CadbcontrolDlg::OnBnClickedHalf()
{
	this->Resize(2);
}
void CadbcontrolDlg::OnBnClickedQuarter()
{
	this->Resize(4);
}

void CadbcontrolDlg::OnBnClickedRotateL()
{
	this->m_picCtrl.RotateLeft();
	this->Resize(m_curRatio);
	this->RedrawWindow();
}

void CadbcontrolDlg::OnBnClickedRotateR()
{
	this->m_picCtrl.RotateRight();
	this->Resize(m_curRatio);
	this->RedrawWindow();
}


bool 
CadbcontrolDlg::start()
{
	m_isRunning = true;
	m_pThread = AfxBeginThread(ThreadProc, (LPVOID)this);
	
	return true;
}

bool 
CadbcontrolDlg::stop()
{
	while(m_isRunning) {
		m_isRunning = false;
		::Sleep(this->m_config->screenshotDelay*3);
		//AfxEndThread(0);
	}
	return true;
}

UINT CadbcontrolDlg::ThreadProc(LPVOID param)
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	TraceLog(("CadbHelper::ThreadProc"));

	CadbcontrolDlg* dlg = (CadbcontrolDlg*)param;
	if(dlg)
	{
		while(dlg->m_isRunning) {
			dlg->makeScreenshot();
			::Sleep(dlg->m_config->screenshotDelay);
		}

	}
	::CoUninitialize();

//	AfxEndThread(0);

	return 0;
}
