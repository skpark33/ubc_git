#pragma once
#include "ximage.h"

#define	NODE_COLOR_GRAY		RGB(238,238,238)
#define NODE_COLOR_YELLOW	RGB(0,255,255)
#define NODE_COLOR_WHITE	RGB(255,255,255)

class CadbcontrolPanel : public CStatic
{
protected:
	CxImage	m_bgImage;
	bool	m_expand;
	CString m_siteId;

	int		m_scr_width;
	int		m_scr_height;

	COLORREF	m_bgColor;

	//virtual LRESULT WindowProc(UINT message,WPARAM wParam,LPARAM lParam) ;

	

public:
	CadbcontrolPanel();
	virtual ~CadbcontrolPanel();
	void SetSiteId(CString& siteId, bool expand) {	m_expand = expand;m_siteId = siteId; }
	void SetBGColor(COLORREF bgColor) { m_bgColor = bgColor; }

	afx_msg void OnDestroy();
	afx_msg void OnPaint();



	DECLARE_MESSAGE_MAP()

public:
	virtual bool LocalLoad(CString& localLocation);
	virtual void UnLoad();

	virtual int GetWidth() { return m_bgImage.GetWidth(); }
	virtual int GetHeight() { return m_bgImage.GetHeight(); }

	void SetSize();
	int GetScrWidth() { return m_scr_width; }
	int GetScrHeight() { return m_scr_height; }

	void RotateLeft();
	void RotateRight();

};