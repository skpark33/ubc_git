//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by adbcontrol.rc
//
#define ID_RECONNECT                    3
#define ID_RECONNECT2                   4
#define ID_DISCONNECT                   4
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ADBCONTROL_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_PIC_CTRL                    1000
#define ID_ROTATE_R                     1001
#define ID_FULLSIZE                     1002
#define ID_ROTATE_L                     1003
#define ID_HALF                         1004
#define ID_QUARTER                      1005
#define IDC_EDIT_WIDTH                  1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
