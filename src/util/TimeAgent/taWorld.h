 /*! \file taWorld.h
 *  Copyright �� 2007, SQIsoft. All rights reserved.
 *
 *  \brief Time Agent World
 *  (Environment: VBroker 7.0, Windows 2003 SP2)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2007/07/05 11:01:00
 */

#ifndef _taWorld_h_
#define _taWorld_h_

#include <aci/libCall/aciCallWorld.h> 

class taWorld : public aciCallWorld {
public:
	taWorld();
	~taWorld();
 	
	//[
	//	From aciCallWorld
	//
	virtual aciCall*    newCallImpl(const char* pname);
	ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean fini(long p_finiCode = 0);
	//
	//]

protected:
	ciBoolean _startUp();
	ciBoolean _sendWindowMessage(ciBoolean isActive);

};
		
#endif //_taWorld_h_
