/*! \class taCallImpl
 *  Copyright �� 2007, COP. All rights reserved.
 *
 *  \brief UTV_Agt Implement
 *  (Environment: Visibroker 7.0, Windows XP)
 *
 *  \author 
 *  \version 1.0
 *  \date 2007/07/05 19:01:00
 */

#ifndef _taCallImpl_h_
#define _taCallImpl_h_

#include "ci/libBase/ciBaseType.h"
#include "cci/libValue/cciEntity.h"

#include "aci/libCall/aciCall.h"

class taCallImpl : virtual public  aciCall {
public:
	taCallImpl(const char* pname) : aciCall(pname) { _taId = pname; } ;
	virtual ~taCallImpl() {} ;

	//
	//[ From CCI_BaseMO
	//
	virtual CORBA::Boolean call(
		const char* directive, 
		const CCI::CCI_Entity& entity, 
		const CCI::CCI_Request& request, 
		const CCI::CCI_Filter& filter, 
		const CCI::CCI_ScopeInfo& scope, 
		CCI::CCI_CallReply_out reply
		);

	virtual void cancelCallReply(CCI::CCI_CallReply* reply);
	virtual void destroyCallReply(CCI::CCI_CallReply* reply);
	// 
	//]

	virtual aciReply*	newCallReply(cciEntity& pEntity, const char* directive);
protected:
	PortableServer::POA_ptr _taGetCallReplyPOA();
	CCI::CCI_CallReply_ptr _taGetCallReplyObjRef(PortableServer::Servant pServant);
	PortableServer::POA_ptr _createServantPOA(
		const char *pPOAName,
		PortableServer::POA_ptr pParentPOA,
		PortableServer::POAManager_ptr pPOAManager,
		CORBA::Boolean pIsPersistent = false,
		CORBA::Boolean pIsMultiThread = true
		);
	ciBoolean _deleteServant(PortableServer::Servant servant);

protected:
	ciString _taId;
};	

#endif //_taCallImpl_h_
