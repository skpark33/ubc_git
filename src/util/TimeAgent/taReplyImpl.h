/*! \class taReplyImpl
 *  Copyright ⓒ 2007, COP. All rights reserved.
 *
 *  \brief Implement
 *  (Environment: VBroker 7.0, Windows 2003 SP2)
 *
 *  \author 
 *  \version 1.0
 *  \date 2007/07/05 19:01:00
 */

#ifndef _taReplyImpl_h_
#define _taReplyImpl_h_

#include <ci/libBase/ciBaseType.h>
#include <ci/libDebug/ciDebug.h>
#include <aci/libCall/aciReply.h>

class taReplyImpl : public aciReply {
public:
	taReplyImpl();
	taReplyImpl(
		const char* className,
		const char* psType=PS_AUTO,
		const char* psName="",
		ciBoolean   psOwner = ciTrue,
		int memsize=100
		);
	virtual ~taReplyImpl();

protected:
    virtual ciBoolean _execute();
    virtual ciBoolean _initObject();

	virtual ciBoolean _get();
	virtual ciBoolean _time();

	virtual ciBoolean setTime(
				CCI::CCI_Time time,			// 변경되는 시간
				const char* operatorId		// 운용자
				);

protected:
	ciString			_taId;
};


#endif // _taReplyImpl_h_
