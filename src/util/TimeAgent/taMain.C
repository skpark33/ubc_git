 /*! \file taMain.h
 *  Copyright �� 2007, SQIsoft. All rights reserved.
 *
 *  \brief Time Agent World
 *  (Environment: VBroker 7.0, Windows 2003 SP2)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2007/07/05 11:01:00
 */

#include "taWorld.h"

int main(int argc, char** argv)
{
    taWorld* world = new taWorld();

    if(!world) {
        cerr<<"Unable to create taWorld"<<endl;
        exit(1);
    }

    if(world->init(argc, argv) == ciFalse) {
        cerr<<"Init world error"<<endl;
        world->fini(2);
		return 2;
    }
    world->run();

    world->fini(0);
	return 0;
}
