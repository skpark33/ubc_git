

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxsock.h>		// MFC socket extensions

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#define MAGIC_PACKET_LENGTH	108

UINT HexStrToInt(CString hexStr)
{
	//printf("HexStrToInt(%s)\n", hexStr);

	char *stop;
	char  num[3];
	UINT res = 0;

	if (hexStr.GetLength()>2) {
		printf("(Length) Invalid Input!\n");
		return 0;		//or perhaps exit;
	}

	memset(num,'\0',3);

	strcpy_s(num,hexStr);
	res = strtol(num,&stop,16);

	printf("HexStrToInt(%s-%ld)\n", hexStr, res);

	//if (res==LONG_MAX || res==LONG_MIN || res==0) {
	//	printf("(OverFlow) Invalid Input!\n");
	//	return 0;
	//}
	return res;
}


int powerOn(const char* pMac, long pPort)
{
	printf("powerOn(%s,%ld)\n", pMac, pPort);
	
	CString macAddr = pMac;
	BYTE magicP[MAGIC_PACKET_LENGTH];

	printf("macAddr=(%s)\n", macAddr);

	for (int i=0;i<6;i++) { 										
		magicP[i] = 0xff;										
	}

	for (int i=0;i<6;i++) {
		magicP[i+6] = HexStrToInt(macAddr.Mid(i*2,2));
	}
	
	for (int i=0;i<15;i++) {
		memcpy(&magicP[(i+2)*6],&magicP[6],6);
	}

	CAsyncSocket s;
	s.Create(pPort,SOCK_DGRAM);
																
	BOOL bOptVal = TRUE;
	if (s.SetSockOpt(SO_BROADCAST,(char*)&bOptVal,sizeof(BOOL))==SOCKET_ERROR) {
		return 1;
	}
	s.SendTo(magicP,MAGIC_PACKET_LENGTH,pPort);

	printf("Magic packet sent.Remote computer should turn on now.\n");
	s.Close();

	return 0;
}

int main(int argc, const char** argv)
{

	if(argc<3) {
		printf("Usage: wol macaddress port\n");
		return 1;
	}

	const char* mac = argv[1];
	long port = atol(argv[2]);

	if(!AfxSocketInit()) {
		printf("Socket initialization failed");
		return 1;
	}



	printf("wol.exe %s %ld\n", mac, port);

	return powerOn(mac,port);

}
