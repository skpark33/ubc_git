// DeleteOrphan.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "DeleteOrphan.h"
#include <ci/libDebug/ciDebug.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 유일한 응용 프로그램 개체입니다.

CWinApp theApp;

using namespace std;

//ciSET_DEBUG(10, "DeleteOrphanConsole");

//int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
int main(int argc, char* argv[])
{
	int nRetCode = 0;

	//ciArgParser::initialize(__argc,__argv);
	//ciEnv::defaultEnv(ciFalse, "utv1");
	ciDebug::setDebugOn();
	ciDebug::logOpen("DeleteOrphan.log");
	

	// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		_tprintf(_T("심각한 오류: MFC를 초기화하지 못했습니다.\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 응용 프로그램의 동작은 여기에서 코딩합니다.
		cout << "\r\n======================================================\r\n";
		int nInput = 0;
		while(nInput != 4)
		{
			cout << " Select contenst folder location\r\n";
			cout << "   1 : content\r\n";
			cout << "   2 : local_content\r\n";
			cout << "   3 : All (contents & local_content)\r\n";
			cout << "   4 : Exit\r\n";
			cout << " ==> ";

			cin >> nInput;

			CDeleteOrphan* pOrphan = NULL;
			switch(nInput)
			{
			case 1:
				{
					pOrphan = CDeleteOrphan::getInstance();
					int nCount = pOrphan->DeleteContents(CDeleteOrphan::E_CONTENTS);
					cout << "\r\n" << "  ==> " << nCount << "  contents folder deleted\r\n\r\n";
					cout << "======================================================\r\n\r\n";
					pOrphan->clearInstance();
				}
				break;
			case 2:
				{
					pOrphan = CDeleteOrphan::getInstance();
					int nCount = pOrphan->DeleteContents(CDeleteOrphan::E_LOCAL_CONTENTS);
					cout << "\r\n" << "  ==> " << nCount << "  contents folder deleted\r\n\r\n";
					cout << "======================================================\r\n\r\n";
					pOrphan->clearInstance();
				}
				break;
			case 3:
				{
					pOrphan = CDeleteOrphan::getInstance();
					int nCount = pOrphan->DeleteContents(CDeleteOrphan::E_ALL_CONTENTS);
					cout << "\r\n" << "  ==> " << nCount << "  contents folder deleted\r\n\r\n";
					cout << "======================================================\r\n";
					pOrphan->clearInstance();
				}
				break;
			case 4:
				{
					cout << "\r\nGood bye... \r\n";
					cout << "======================================================\r\n\r\n";
					system("pause");

					return nRetCode;
				}
				break;
			default:
				{
					cout << "\r\n Invalid selection\r\n\r\n";
					cout << "======================================================\r\n\r\n";
				}
			}//switch
		}//while
	}//if

	return nRetCode;
}