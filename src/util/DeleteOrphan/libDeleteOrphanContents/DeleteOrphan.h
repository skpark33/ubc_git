/************************************************************************************/
/*! @file DeleteOrphan.h
	@brief 서버에서 컨텐츠 패키지 파일들에서 더 이상 사용되지 않는 컨텐츠 파일들을 검색하여 지워준다.
	@remarks
	▶ 작성자: 정운형\n
	▶ 작성일: 2012/03/22\n

************************************************************************************
  - @b 추가 @b 및 @b 변경사항
************************************************************************************
	@b 작성)
	-# <2012/03/22:정운형:최초작성>.

************************************************************************************
Copyright (c) 2008 by SQI Soft
*/
/***********************************************************************************/

#pragma once

#include <afxmt.h>



//! 사용되지 않는 컨텐츠 파일들을 삭제하는 클래스
/*!
	컨텐츠 패키지 파일들을 조회하여 더 이상 사용되지 않는 \n
	컨텐츠 파일들을 삭제한다. \n
*/
class CDeleteOrphan
{
public:
	static	CDeleteOrphan*		getInstance(void);			///<싱글톤 인스턴스 생성
	static	void				clearInstance(void);		///<싱글톤 인스턴스 삭제

	virtual ~CDeleteOrphan(void);							///<소멸자

	enum EN_DELETE_OPTION
	{
		E_CONTENTS = 0,			///<contents 폴더 정리
		E_LOCAL_CONTENTS,		///<local_contents 폴더 정리
		E_ALL_CONTENTS,			///<contents와 local_contents 폴더 정리
		E_OPTION_MAX
	};

protected:
	CDeleteOrphan(void);									///<생성자

	static	CDeleteOrphan*		m_Instance;					///<싱글톤 인스턴스
	static	CCriticalSection	m_InstanceLock;				///<싱글톤 인스턴스 생성을 위한 동기화 객체

	CString						m_strConfigPath;			///<컨텐츠 패키지 경로
	CString						m_strContentsPath;			///<컨텐츠 경로
	CString						m_strLocalContentsPath;		///<로컬 컨텐츠 경로
	CMapStringToString			m_mapUseContents;			///<사용하고 있는 콘텐츠 파일들 맵
	CMapStringToString			m_mapExistContents;			///<컨텐츠 폴더에 존재하는 콘텐츠 파일들 맵
	CMapStringToString			m_mapExistLocalContents;	///<로컬 컨텐츠 폴더에 존재하는 콘텐츠 파일들 맵
	int							m_nDeletedCount;			///<삭제된 컨텐츠 폴더 개수
	EN_DELETE_OPTION			m_nOption;					///<정리를 실행할 옵션


public:
	void	ClearData(void);										///<멤버 데이터 정리
	int		DeleteContents(CDeleteOrphan::EN_DELETE_OPTION nOption);///<지정된 폴더이름의 컨텐츠 파일들을 삭제한다.
	bool	MakeUseContentsMap(void);								///<사용하는 컨텐츠 맵을 만든다.
	void	MakeExistContentsMap(void);								///<컨텐츠 폴더에 존재하는 컨텐츠 맵을 만든다.
	void	MakeExistLocalContentsMap(void);						///<로컬 컨텐츠 폴더에 존재하는 컨텐츠 맵을 만든다.
	void	DeleteOrphans(void);									///<사용하지 않는 컨텐츠 폴더들을 삭제한다.
	void	DeleteLocalOrphans(void);								///<사용하지 않는 로컬 컨텐츠 폴더들을 삭제한다.
	bool	DeleteDirectory(LPCTSTR lpDirPath);						///<지정된 폴더와 내부의 폴더, 파일을 삭제한다.
	CString	GetExtension(const CString& strFileName);				///<파일명에서 확장자를 분리하여 반환
};
