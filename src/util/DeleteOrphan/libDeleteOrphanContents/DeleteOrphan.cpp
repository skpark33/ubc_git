#include "StdAfx.h"
#include "DeleteOrphan.h"
#include <ci/libDebug/ciDebug.h>
#include <libProfileManager/ProfileManager.h>

#define CONFIG_PATH				("..\\config")
#define CONTENTS_PATH			("..\\contents")
#define LOCAL_CONTENTS_PATH		("..\\local_contents")
#define	READ_PACKAGE			(mngProfile.GetProfileString)

ciSET_DEBUG(10, "DeleteOrphan");


CCriticalSection CDeleteOrphan::m_InstanceLock;
CDeleteOrphan* CDeleteOrphan::m_Instance = NULL; 


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 싱글톤 인스턴스 생성 \n
/// @return <형: CDeleteOrphan*> \n
///			<싱글톤 인스턴스> \n
/////////////////////////////////////////////////////////////////////////////////
CDeleteOrphan* CDeleteOrphan::getInstance()
{
	if(!m_Instance) 
	{
		m_InstanceLock.Lock();
		if(!m_Instance) 
		{
			m_Instance = new CDeleteOrphan;
		}//if
		m_InstanceLock.Unlock();
	}//if

	return m_Instance;
}

/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 싱글톤 인스턴스 삭제 \n
/////////////////////////////////////////////////////////////////////////////////
void CDeleteOrphan::clearInstance()
{
	if(m_Instance) 
	{
		m_InstanceLock.Lock();
		if(m_Instance) 
		{
			delete m_Instance;
			m_Instance = NULL;
		}//if
		m_InstanceLock.Unlock();
	}//if
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 생성자 \n
/////////////////////////////////////////////////////////////////////////////////
CDeleteOrphan::CDeleteOrphan()
: m_strConfigPath("")
, m_strContentsPath("")
, m_strLocalContentsPath("")
, m_nDeletedCount(0)
, m_nOption(E_CONTENTS)
{
	char szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	char cDrive[MAX_PATH], cPath[MAX_PATH], cFilename[MAX_PATH], cExt[MAX_PATH];
	_splitpath(szModule, cDrive, cPath, cFilename, cExt);

	m_strConfigPath.Format("%s%s%s", cDrive, cPath, CONFIG_PATH);
	m_strContentsPath.Format("%s%s%s", cDrive, cPath, CONTENTS_PATH);
	m_strLocalContentsPath.Format("%s%s%s", cDrive, cPath, LOCAL_CONTENTS_PATH);
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 소멸자 \n
/////////////////////////////////////////////////////////////////////////////////
CDeleteOrphan::~CDeleteOrphan()
{
	ClearData();
}



/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 멤버 데이터 정리 \n
/////////////////////////////////////////////////////////////////////////////////
void CDeleteOrphan::ClearData()
{
	m_nDeletedCount = 0;
	m_mapUseContents.RemoveAll();
	m_mapExistContents.RemoveAll();
	m_mapExistLocalContents.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 폴더이름의 컨텐츠 파일들을 삭제한다. \n
/// @param (CDeleteOrphan::EN_DELETE_OPTION) nOption : (in) 정리할 폴더 옵션
/// @return <형: int> \n
///			<삭제된 컨텐츠 폴더 개수> \n
/////////////////////////////////////////////////////////////////////////////////
int CDeleteOrphan::DeleteContents(CDeleteOrphan::EN_DELETE_OPTION nOption)
{
	if(nOption >= E_OPTION_MAX)
	{
		ciWARN(("invalid delete option = %d", nOption));
		return 0;
	}//if

	m_nOption = nOption;
	ClearData();

	//사용하고있는 컨텐츠 파일들 맵 생성
	if(!MakeUseContentsMap())
	{
		ciERROR(("Fail to make MakeUseContentsMap"));
		return 0;
	}//if
	ciDEBUG(1,("Success MakeUseContentsMap"));

	CFileStatus status;
	CFile::GetStatus(m_strConfigPath, status);
	m_strConfigPath = status.m_szFullName;

	CFile::GetStatus(m_strContentsPath, status);
	m_strContentsPath = status.m_szFullName;

	CFile::GetStatus(m_strLocalContentsPath, status);
	m_strLocalContentsPath = status.m_szFullName;

	ciDEBUG(1,("Config path = %s", m_strConfigPath));
	ciDEBUG(1,("Contents path = %s", m_strContentsPath));
	ciDEBUG(1,("Local Contents path = %s", m_strLocalContentsPath));

	//컨텐츠 폴더에 존재하는 컨텐츠 파일들 맵 생성
	switch(m_nOption)
	{
	case E_CONTENTS:
		{
			MakeExistContentsMap();
		}
		break;
	case E_LOCAL_CONTENTS:
		{
			MakeExistLocalContentsMap();
		}
		break;
	case E_ALL_CONTENTS:
		{
			MakeExistContentsMap();
			MakeExistLocalContentsMap();
		}
		break;
	}//switch

	if(m_mapExistContents.GetCount() == 0 
		&& m_mapExistLocalContents.GetCount() == 0)
	{
		ciWARN(("Exist contents map size is 0"));
		return 0;
	}//if

	//사용하지 않는 컨텐츠 폴더들 삭제
	switch(m_nOption)
	{
	case E_CONTENTS:
		{
			DeleteOrphans();
		}
		break;
	case E_LOCAL_CONTENTS:
		{
			DeleteLocalOrphans();
		}
		break;
	case E_ALL_CONTENTS:
		{
			DeleteOrphans();
			DeleteLocalOrphans();
		}
		break;
	}//switch

	ciDEBUG(1,("Totla delete count = %d", m_nDeletedCount));
	
	return m_nDeletedCount;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용하는 컨텐츠 맵을 만든다. \n
/// @return <형: bool> \n
///			<true: 성공> \n
///			<fasle: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDeleteOrphan::MakeUseContentsMap()
{
	CString strPackage, strContentsList, strETCList;
	CProfileManager mngProfile;
	CFileFind ff;
	BOOL bFind = ff.FindFile(m_strConfigPath + "\\*.*");
	while(bFind)
	{
		bFind = ff.FindNextFile();

		if(ff.IsDots()) continue;
		if(ff.IsDirectory()) continue;

		if(!mngProfile.Read(ff.GetFilePath()))
		{
			ciERROR(("Fail to read package file = %s", strPackage));
			continue;		//파일 읽기 오류
		}//if

		strContentsList = READ_PACKAGE(_T("Host"), _T("ContentsList"));

		int nPos = 0;
		int nLen = 0;
		CString strName, strID;
		ULONGLONG ulVolume = 0;
		CString token = strContentsList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strName = READ_PACKAGE(token, _T("filename"));
			ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));
			strID = READ_PACKAGE(token, _T("contentsid"));

			if(strName.GetLength() != 0 && ulVolume != 0 && strID.GetLength() != 0)
			{
				//대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
				strID.MakeLower();
				m_mapUseContents.SetAt(strID, strID);	//맵에 추가
			}//if

			token = strContentsList.Tokenize(_T(","), nPos);
		}//while

		//ETCList
		strETCList = READ_PACKAGE(_T("Host"), _T("EtcList"));
		nPos = 0;
		token = strETCList.Tokenize(_T(","), nPos);
		while(token != _T(""))
		{
			token.Trim();

			strName = READ_PACKAGE(token, _T("filename"));
			ulVolume = (ULONGLONG)_atoi64(READ_PACKAGE(token, _T("volume")));
			strID = READ_PACKAGE(token, _T("contentsid"));

			if(strName.GetLength() != 0 && ulVolume != 0 && strID.GetLength() != 0)
			{
				//대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
				strID.MakeLower();
				m_mapUseContents.SetAt(strID, strID);	//맵에 추가
			}//if

			token = strETCList.Tokenize(_T(","), nPos);
		}//while
	}//while
	ff.Close();

	if(m_mapUseContents.GetCount() == 0)
	{
		ciWARN(("Using contents map size is 0"));
		return false;
	}//if

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 컨텐츠 폴더에 존재하는 컨텐츠 맵을 만든다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDeleteOrphan::MakeExistContentsMap()
{
	CString strPackage, strContentsList;

	CFileFind ff;
	BOOL bFind = ff.FindFile(m_strContentsPath + "\\*.*");
	CString strKey;
	while(bFind)
	{
		bFind = ff.FindNextFile();

		if(ff.IsDots()) continue;
		if(!ff.IsDirectory()) continue;

		//폴더인 경우만 고려....
		strKey = ff.GetFileTitle();
		//대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
		strKey.MakeLower();
		m_mapExistContents.SetAt(strKey, ff.GetFilePath());
	}//while
	ff.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 로컬 컨텐츠 폴더에 존재하는 컨텐츠 맵을 만든다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDeleteOrphan::MakeExistLocalContentsMap()
{
	CString strPackage, strContentsList;

	CFileFind ff;
	BOOL bFind = ff.FindFile(m_strLocalContentsPath + "\\*.*");
	CString strKey;
	while(bFind)
	{
		bFind = ff.FindNextFile();

		if(ff.IsDots()) continue;
		if(!ff.IsDirectory()) continue;

		//폴더인 경우만 고려....
		strKey = ff.GetFileTitle();
		//대소문자를 구분하지 않기위해 키값은 소문자로 치환한다.
		strKey.MakeLower();
		m_mapExistLocalContents.SetAt(strKey, ff.GetFilePath());
	}//while
	ff.Close();
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용하지 않는 컨텐츠 폴더들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDeleteOrphan::DeleteOrphans()
{
	POSITION pos;
	CString strId, strPath, strTmp;

	//콘텐츠 폴더
	for(pos = m_mapExistContents.GetStartPosition(); pos != NULL; )
	{
		m_mapExistContents.GetNextAssoc(pos, strId, strPath);
		if(!m_mapUseContents.Lookup(strId, strTmp))	//존재하는 컨텐츠가 사용중이지 않는다면 삭제한다.
		{
			ciDEBUG(1,("Trying remove in contents folder = %s", strPath));
			if(DeleteDirectory(strPath))
			{
				ciDEBUG(1,("Success remove in contents folder = %s", strPath));
				m_nDeletedCount++;
			}
			else
			{
				ciERROR(("Fail to remove in contents folder = %s", strPath));
			}//if
		}//if
	}//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 사용하지 않는 로컬 컨텐츠 폴더들을 삭제한다. \n
/////////////////////////////////////////////////////////////////////////////////
void CDeleteOrphan::DeleteLocalOrphans()
{
	POSITION pos;
	CString strId, strPath, strTmp;

	//로컬 콘텐츠 폴더
	 for(pos = m_mapExistLocalContents.GetStartPosition(); pos != NULL; )
	 {
		 m_mapExistLocalContents.GetNextAssoc(pos, strId, strPath);
		 if(!m_mapUseContents.Lookup(strId, strTmp))	//존재하는 컨텐츠가 사용중이지 않는다면 삭제한다.
		 {
			 ciDEBUG(1,("Trying remove in local_contents folder = %s", strPath));
			 if(DeleteDirectory(strPath))
			 {
				 ciDEBUG(1,("Success remove local_contents folder = %s", strPath));
				 m_nDeletedCount++;
			 }
			 else
			 {
				 ciERROR(("Fail to remove local_contents folder = %s", strPath));
			 }//if
		 }//if
	 }//for
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 지정된 폴더와 내부의 폴더, 파일을 삭제한다. \n
/// @param (LPCTSTR) lpDirPath : (in) 정리하려는 폴더 경로
/// @return <형: bool> \n
///			<true: 성공> \n
///			<false: 실패> \n
/////////////////////////////////////////////////////////////////////////////////
bool CDeleteOrphan::DeleteDirectory(LPCTSTR lpDirPath)
{
	if(lpDirPath == NULL)// 경로가 없는 경우 되돌아간다.
	{
		ciERROR(("Empty folder path"));
		return false;
	}//if

	BOOL bRval = FALSE;
	int nRval = 0;
	CString strRoot;
	CString strPath = lpDirPath;
	strPath.Append("\\*.*");
	CFileFind find;

	// 폴더가 존재 하는 지 확인 검사 
	bRval = find.FindFile(strPath);
	if(bRval == FALSE)
	{
		ciERROR(("Fail to found path = %s", strPath));
		return false;
	}//if

	while(bRval)
	{
		bRval = find.FindNextFile();
		// . or .. 인 경우 무시 한다.
		if(find.IsDots() == TRUE)
		{
			continue;
		}//if

		// Directory 일 경우
		if(find.IsDirectory())             
		{
			// Recursion function 호출            
			DeleteDirectory(find.GetFilePath());            
		}
		else	// file일 경우
		{
			// 파일 삭제
			if(!::DeleteFile(find.GetFilePath()))
			{
				ciERROR(("Fail to delete file = %s", find.GetFilePath()));
			}//if
		}
	}//while

	strRoot = find.GetRoot();     
	find.Close(); 
	if(!::RemoveDirectory(strRoot))	//Root 폴더 삭제
	{
		//ciERROR(("Fail to remove folder = %s", strRoot));
		return false;
	}//if

	//ciDEBUG(1,("Success remove folder = %s", strRoot));
	return true; 
}


/////////////////////////////////////////////////////////////////////////////////
/// @remarks
/// 파일명에서 확장자를 분리하여 반환 \n
/// @param (const CString&) strFileName : (in) 파일명
/// @return <형: CString> \n
///			<파일의 확장자> \n
/////////////////////////////////////////////////////////////////////////////////
CString	CDeleteOrphan::GetExtension(const CString& strFileName)
{
	int nlen = strFileName.GetLength();
	int i;
	for (i = nlen-1; i >= 0; i--)
	{
		if (strFileName[i] == '.')
		{
			return strFileName.Mid(i+1);
		}//if
	}//if

	return CString(_T(""));
}