// UBCMediaInfo.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "io.h"
#include "MediaInfoDLL.h"
#include <iostream>
#include <iomanip>
using namespace MediaInfoDLL;

int getBitRate(String& fullpath, String& video_format, String& audio_format, String& errStr)
{
	if(access(fullpath.c_str(), 4)!=0){
		errStr = fullpath;
		errStr += _T(" acccess failed");
		return 0;
	}


    MediaInfo MI;
	size_t ret = MI.Open(fullpath);
	if(ret==0){
		errStr = fullpath;
		errStr += _T(" open failed .....");
		return 0;
	}
    errStr = MI.Get(Stream_General, 0, _T("OverallBitRate"), Info_Text, Info_Name);
	unsigned long bitRate = strtoul(errStr.c_str(),NULL,10);
	std::cout  << bitRate << std::endl;
	int retval = int(bitRate/1024);

    video_format = MI.Get(Stream_General, 0, _T("Video_Format_List"), Info_Text, Info_Name);
	std::cout << _T("Video Format = [") << video_format.c_str() << _T("]") << std::endl;

    audio_format = MI.Get(Stream_General, 0, _T("Audio_Format_List"), Info_Text, Info_Name);
	std::cout << _T("Audio Format = [") << audio_format.c_str() << _T("]") <<std::endl;

	MI.Close();
	return retval;
}



int _tmain(int argc, char* argv[])
{
	String source = _T("C:\\SQISoft\\Contents\\Enc\\movie.flv");
	if(argc < 2) {
	}else{
		source = argv[1];
		if(source == _T("/?") || source == _T("-help") || source == _T("/h") || source == _T("--help") || source == _T("-h")) {
			std::cout << _T("Usage : UBCAutoEncoding [source_file_full_path] [batch_file_full_path] [target_full_path]") << std::endl;
			std::cout << _T("Example : UBCAutoEncoding C:\\SQISoft\\Contents\\Enc\\movie.flv C:\\SQISoft\\UTV1.0\\execute\\x264_mp4_aac_org C:\\SQISoft\\Contents\\Enc\\movie.mp4") << std::endl;
			return 1;
		}
	}
	String batchFileName = _T("C:\\SQISoft\\UTV1.0\\execute\\x264_mp4_aac");
	if(argc >= 3) {
		batchFileName = argv[2];
	}
	String targetFullPath = _T("C:\\SQISoft\\Contents\\Enc\\movie.mp4");
	if(argc >= 4) {
		targetFullPath = argv[3];
	}

	std::cout  << _T("source=[") << source.c_str() << _T("]") << std::endl;
	std::cout  << _T("batch=[") << batchFileName.c_str() << _T("]") << std::endl;
	std::cout  << _T("target=[") << targetFullPath.c_str() << _T("]") << std::endl;



	String video, audio, errStr;
	int bitRate = getBitRate(source, video, audio,errStr);
	if(bitRate == 0){
		std::cout << _T("ERROR: ") << errStr.c_str() << std::endl;
		return 1;
	}
    std::cout  << _T("BITRATE=[") << bitRate << _T("] KByte") << std::endl;

	/*
	wchar_t drive[_MAX_PATH], path[_MAX_PATH], filename[_MAX_PATH], ext[_MAX_PATH];
	_wsplitpath(source.c_str(), drive, path, filename, ext);

	String tag;
	String extention = ext;

	if(extention == _T(".mp4")) {
		std::cout  << _T("same extention") << std::endl;
		// audio, video codec 이 모두 같다면, 인코딩 하지 않는다.
		if(video == _T("AVC") || audio == _T("AAC")) {
			std::cout  << _T("same codec, no encoding required !") << std::endl;
			return 0;
		}
		tag = _T("_");
	}
	*/
	/*
	String tmpName = _T("C:\\Temp\\");
	tmpName += filename;
	tmpName += tag;
	tmpName += _T(".mp4");
	*/
	String tmpName = targetFullPath;
	tmpName += _T(".tmp");
	/*
	String converted = drive;
	converted += path;
	converted += filename;
	converted += tag;
	converted += _T(".mp4");
	*/
	String converted = targetFullPath;

	std::cout  << _T("tmpName = ") << tmpName.c_str() << std::endl;
	std::cout  << _T("lastName = ") << converted.c_str() << std::endl;

	String command = batchFileName;
	//command += _T(".bat ");
	command += _T(" \"");
	command += source;
	command += _T("\" \"");
	//command += converted;
	command += tmpName;
	command += _T("\" ");

	//char buf[100];
	//sprintf(buf, _T("%d"), bitRate); 

	command += errStr;

	std::cout  << _T("command = ") << command.c_str() << std::endl;
	
	int retval = system(command.c_str());
	if(retval != 0) {
	    std::cout  << _T("encoding failed !!!") << std::endl;
	}

	if(!MoveFileEx(tmpName.c_str(), converted.c_str(), MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING))
	{
		std::cout << _T("Fail to move ") << tmpName.c_str() << " to " << converted.c_str() << std::endl;
		return 1;
	}

	return retval;
}


