// Crc32Util.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CCrc32UtilApp:
// See Crc32Util.cpp for the implementation of this class
//

class CCrc32UtilApp : public CWinApp
{
public:
	CCrc32UtilApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CCrc32UtilApp theApp;