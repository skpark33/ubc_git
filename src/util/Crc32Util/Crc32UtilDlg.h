// Crc32UtilDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#define ITEM_NUM_AT_LINE		7	// 2010.01.08 crc �߰�
#define VERSION_TXT_PATH		"D:\\Project\\UBC\\utv1"

typedef struct __EnvVersionInfo
{
	CString procname;
	CString keyname;
	CString version;
	CString srcname;
	CString dstname;
	CString dirname;
	CString crc;
	int linenum;
}VERSION_INFO, *PVERSION_INFO;

// CCrc32UtilDlg dialog
class CCrc32UtilDlg : public CDialog
{
// Construction
public:
	CCrc32UtilDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CRC32UTIL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void InitValue();
	CString GetCurDir();
	BOOL CheckVersionTxt();
	void _CheckVersionTxt(LPSTR strBuffer);
	CString todayStr();
	CString date2Str(time_t tm);
	static DWORD CheckFile(LPCSTR filePath, CString& strResult);


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	CEdit m_versionPath;
	CEdit m_ftpDstPath;
	CListCtrl m_listLog;

	static UINT RunThread(LPVOID pParam);
	CEdit m_iniPath;
};
