// Crc32UtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Crc32Util.h"
#include "Crc32UtilDlg.h"
#include "common/libCrc32/Crc32Static.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCrc32UtilDlg dialog

UINT CCrc32UtilDlg::RunThread(LPVOID pParam)
{
	CCrc32UtilDlg* crc32Dlg = (CCrc32UtilDlg*)pParam;

	crc32Dlg->CheckVersionTxt();

	//::Sleep(3000);
	//crc32Dlg->SendMessage(WM_CLOSE); // CheckVersionTxt 함수 실행후 바로 종료

	return 0;
}


CCrc32UtilDlg::CCrc32UtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCrc32UtilDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCrc32UtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_VERSIONPATH, m_versionPath);
	DDX_Control(pDX, IDC_EDIT_DESTPATH, m_ftpDstPath);
	DDX_Control(pDX, IDC_LIST_LOG, m_listLog);
	DDX_Control(pDX, IDC_EDIT_INIPATH, m_iniPath);
}

BEGIN_MESSAGE_MAP(CCrc32UtilDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CCrc32UtilDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_ESCAPE) return TRUE;
		if(pMsg->wParam == VK_RETURN) return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

// CCrc32UtilDlg message handlers

BOOL CCrc32UtilDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	CRect rect;
	m_listLog.GetClientRect(&rect);
	int nColInterval = rect.Width()/2;
	m_listLog.InsertColumn(0, _T("FILE NAME"), LVCFMT_LEFT, nColInterval+50);
	m_listLog.InsertColumn(1, _T("CRC32 RESULT"), LVCFMT_LEFT, nColInterval-50);

	InitValue();

	AfxBeginThread(RunThread, this);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCrc32UtilDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCrc32UtilDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCrc32UtilDlg::InitValue()
{
	CString iniPath;
	iniPath.Format("%s\\Crc32Util.ini", GetCurDir());

	m_iniPath.SetWindowText(iniPath);

	char	szValue[2048];
	memset(szValue, 0x00, sizeof(szValue));
	GetPrivateProfileString("ROOT","VersionTxtPath",VERSION_TXT_PATH,szValue,sizeof(szValue),iniPath);
	//m_versionPath = szValue;
	m_versionPath.SetWindowText(szValue);

	memset(szValue, 0x00, sizeof(szValue));
	GetPrivateProfileString("ROOT","DestinationPath",VERSION_TXT_PATH,szValue,sizeof(szValue),iniPath);
	//m_ftpDstPath = szValue;
	m_ftpDstPath.SetWindowText(szValue);
}

CString CCrc32UtilDlg::GetCurDir()
{
	CString strDir;
	char	szBuf[_MAX_PATH] = { 0x00 };

	::GetModuleFileName(NULL, szBuf, _MAX_PATH);
	strDir.Format("%s", szBuf);
	strDir.MakeReverse();
	int nIndex = strDir.Find("\\");
	if (nIndex < 0) return "";

	if (strDir.GetLength() < (nIndex+1)) return "";

	strDir = strDir.Mid(nIndex + 1);
	strDir.MakeReverse();

	return strDir;
}

BOOL CCrc32UtilDlg::CheckVersionTxt()
{
	DWORD tStart, tEnd;
	tStart = GetTickCount();
	tEnd = tStart;

	CString iFileName;
	m_versionPath.GetWindowText(iFileName);
	iFileName += "\\version.txt";
	CString oFileName;
	m_versionPath.GetWindowText(oFileName);
	oFileName += "\\version_crc.txt";

	// 1. 입력 파일 열기
	FILE* stream = fopen(iFileName, "r");
	if (!stream) {
		//ciERROR(("File[%s] open failed.", iFileName));
		return FALSE;	
	}
	
	// 2. 출력 파일 열기
	FILE* oStream = fopen(oFileName, "w");
	if (!oStream) {
		//ciERROR(("File[%s] open failed.", oFileName));
		return FALSE;	
	}

	// 3. INSERT PlayLog Table
	char buffer[1024];
	while(!feof(stream)) {
		memset(buffer, 0x00, 1024);
		fgets(buffer, 1024, stream);
		_CheckVersionTxt(buffer);
		fprintf(oStream, "%s", buffer);
	}

	// 4. 입력 파일 닫기
	fclose(stream);

	// 5. 출력 파일 닫기
	fclose(oStream);

	// 6. 출력 파일명을 입력 파일명으로 변경(백업후 덮어쓰기)
	CString bFileName = iFileName + "." + todayStr();
	if(::CopyFile(iFileName, bFileName, FALSE)) {
		::CopyFile(oFileName, iFileName, FALSE);
		::DeleteFile(oFileName);
	}

	tEnd = GetTickCount();
	//ciDEBUG(9, ("ElapsedTime %f", (tEnd-tStart)/1000.0f));
	return TRUE;
}

void CCrc32UtilDlg::_CheckVersionTxt(LPSTR strBuffer)
{
	CString token;
	CString strData = strBuffer;
	strData.Remove('\r');strData.Remove('\n');
	CString dstName;

	VERSION_INFO versioninfo;

	int nFind = -1;
	for(int i=0; i<ITEM_NUM_AT_LINE; i++)
	{
		nFind = strData.Find(',');
		if( nFind == -1 ) {
			token = strData;
			strData = "";
		} else {
			token   = strData.Left(nFind);
			strData = strData.Right(strData.GetLength() - (nFind+1));
		}

		switch(i)
		{
		case 0: versioninfo.procname = token; break;
		case 1: versioninfo.keyname = token; break;
		case 2: versioninfo.version = token; break; 
		case 3: versioninfo.srcname = token; break;
		case 4: versioninfo.dstname = token; break;
		case 5: versioninfo.dirname = token; break;
		case 6: versioninfo.crc = token; break;
		}
	}
	if (versioninfo.dstname.IsEmpty()) return;

	// 리스트 컨트롤 파일 정보 입력
	int index = m_listLog.GetItemCount();
	m_listLog.InsertItem(index, "");
	m_listLog.SetItemData(index, index);
	m_listLog.SetItemText(index, 0, versioninfo.dstname);	
	// 리스트 컨트롤 자동 스크롤
	int nCount = m_listLog.GetItemCount();
	m_listLog.EnsureVisible(nCount-1, FALSE); 

	CString filePath;
	m_ftpDstPath.GetWindowText(filePath);
	filePath += versioninfo.dstname;
	DWORD dwCrc32, dwErrorCode = NO_ERROR;
	dwErrorCode = CCrc32Static::FileCrc32Win32(filePath, dwCrc32);
	CString strResult;
	if(dwErrorCode == NO_ERROR) {
		strResult.Format(_T("0x%08x"), dwCrc32);
		sprintf(strBuffer, "%s,%s,%s,%s,%s,%s,%s\n",
			versioninfo.procname, 
			versioninfo.keyname, 
			versioninfo.version, 
			versioninfo.srcname, 
			versioninfo.dstname,
			versioninfo.dirname,
			strResult);
	} else {
		//strResult.Format(_T("Error: [0x%08x]"), dwErrorCode);
		;
	}

	m_listLog.SetItemText(index, 1, strResult);
}

DWORD CCrc32UtilDlg::CheckFile(LPCSTR filePath, CString& strResult)
{
	DWORD dwCrc32, dwErrorCode = NO_ERROR;
	dwErrorCode = CCrc32Static::FileCrc32Win32(filePath, dwCrc32);
	CString strRslt;
	if(dwErrorCode == NO_ERROR) {
		strRslt.Format(_T("0x%08x"), dwCrc32);
	} else {
		strRslt.Format(_T("Error: [0x%08x]"), dwErrorCode);
	}
	strResult = strRslt;
	return dwErrorCode;
}

CString CCrc32UtilDlg::todayStr() {
	time_t the_time;
	time(&the_time);
	return date2Str(the_time);
}

CString CCrc32UtilDlg::date2Str(time_t tm) {
	TCHAR* str;
	struct tm aTM;
#ifdef WIN32
	struct tm *temp = localtime(&tm);
	if (temp) {
		aTM = *temp;
	} else {
		memset((void*)&aTM, 0x00, sizeof(aTM));
	}
#else
	localtime_r(&tm, &aTM);
#endif

	str = (TCHAR*)malloc(sizeof(TCHAR)*20);
	_stprintf(str,_T("%4d%02d%02d"),
		aTM.tm_year+1900,aTM.tm_mon+1,aTM.tm_mday);
	CString strTime = str;
	free(str);
	return strTime;
}
