#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <string>

#define BUF_LEN 128

// WinSock DLL을 사용할 버전
#define VERSION_MAJOR         2 
#define VERSION_MINOR         0 


#include <ci/libDebug/ciDebug.h>
#include <ci/libDebug/ciArgParser.h>
#include <CMN/libPJLink/PJLink.h>

ciSET_DEBUG(10,"LDPSimulator");


ciBoolean
_writeline(SOCKET fd, const char* buf)
{
	ciDEBUG(1,("Write try : %s\n", buf));

	int msg_size = strlen(buf);
	if(send(fd, buf, msg_size, 0)<=0) {
		ciERROR(("send error"));
		return ciFalse;
	};

	ciDEBUG(1,("Write end : %s\n", buf));
	return ciTrue;

}

ciBoolean
_readline(SOCKET fd, ciString& outVal)
{
	ciDEBUG(1,("read try : "));

	char buf[BUF_LEN+1];
	int msg_size;

	memset(buf, 0x00, sizeof(buf));
	msg_size = recv(fd, buf, sizeof(buf), 0);

	if(msg_size <= 0) {
		ciERROR(("receive error"));
		return ciFalse;
	}

	ciDEBUG(1,("read end : %s\n", buf));
	
	outVal =  buf;
	return ciTrue;
}


int main(int argc, char* argv[])
{
	ciArgParser::initialize(argc, argv);
	ciDebug::handleDebugOption();

	// Load WinSock DLL 
	WORD VersionRequested = MAKEWORD(VERSION_MAJOR, VERSION_MINOR);
	WSADATA       WsaData;            // receives data from WSAStartup 
	if ( WSAStartup(VersionRequested, &WsaData) != 0 ) {		
		return -1;
	}

	// socket 생성
	SOCKET server_fd = socket( PF_INET, SOCK_STREAM, 0 );

	struct sockaddr_in server_addr;
	memset( &server_addr, 0, sizeof(struct sockaddr) );
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	//server_addr.sin_port = htons(atoi(argv[1]));
	server_addr.sin_port = htons(5352);

	// bind() 호출
	if( bind(server_fd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) < 0 ){
		ciDEBUG(1,("Server: Can't bind local address.\n"));
		exit(0);
	}

	// 소켓을 수동 대기모드로 세팅
	listen(server_fd, 5);
	ciDEBUG(1,("IP Address Server start.\n\n"));

	// iterative  echo 서비스 수행
	while(1) {
		ciDEBUG(1,("Server : waiting connection request.\n"));
		
		struct sockaddr_in client_addr;		
		int len = sizeof(client_addr);

		// 연결요청을 기다림
		SOCKET client_fd = accept(server_fd, (struct sockaddr *)&client_addr, &len);
		if(client_fd < 0) {
 			ciDEBUG(1,("Server: accept failed.\n"));
 			exit(0);
		}

		ciDEBUG(1,("Server : A client connected. %s\n", inet_ntoa(client_addr.sin_addr)));

		_writeline(client_fd, "PJLINK 0\r");

		ciString buf ="";
		_readline(client_fd,buf);

		if(buf=="%1POWR 1\r") {
			Sleep(2*990);
			_writeline(client_fd, "%1POWR=OK\r");
		}else	
		if(buf=="%1POWR 0\r") {
			Sleep(3*990);
			_writeline(client_fd, "%1POWR=ERR2\r");
		}else	
		if(buf=="%1POWR ?\r") {
			Sleep(2*990);
			_writeline(client_fd, "%1POWR=1\r");
		}else{	
			ciDEBUG(1,("Not supported command. %s\n", buf));
		}

		closesocket(client_fd);
	}
	closesocket(server_fd);

	WSACleanup();		// Unload WinSock DLL
	return 0;
}


