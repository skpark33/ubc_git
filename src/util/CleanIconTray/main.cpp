#include "stdafx.h"

int main()
{
  USES_CONVERSION;

	if (__argc < 3)
	{
		printf("Usage: CleanIconTray tray_id window_title\n");
		return 1;
	}


	CString windowName;
	int tray_id = atoi(__argv[1]);
	for(int i=2;i<__argc;i++){
		if(i>2){
			windowName.Append(" ");
		}
		windowName.Append(__argv[i]);
	}

	HWND m_hWnd  = ::FindWindow(NULL,windowName);
	if(m_hWnd){
		printf("Remove %s from ICON TRAY\n", windowName.GetBuffer());
		NOTIFYICONDATA nid;
		::ZeroMemory(&nid, sizeof(nid));
		nid.cbSize = sizeof(nid);
		
		nid.hWnd = m_hWnd;
		nid.uID = tray_id; //IDI_TRAY;
		Shell_NotifyIcon(NIM_DELETE,&nid);//삭제플래그를 준다.
		return 0;
	}
	printf("Remove %s from ICON TRAY Failed\n",windowName.GetBuffer());

	return 1;
}
