 /*! \file hostAutoUpdateWorld.h
 *  Copyright �� 2002, SQIsoft. All rights reserved.
 *
 *  \brief CLI World
 *  (Environment: SORBA 2.3, COMPAQ Tru64 5.1)
 *
 *  \author jhchoi
 *  \version 1.0
 *  \date 2002/09/27 11:01:00
 */

#ifndef _hostAutoUpdateWorld_h_
#define _hostAutoUpdateWorld_h_
#pragma once
#include <cci/libWorld/cciORBWorld.h> 

class hostAutoUpdateWorld : public cciORBWorld {
public:
	hostAutoUpdateWorld();
	~hostAutoUpdateWorld();
 	
	virtual ciBoolean init(int& p_argc, char** &p_argv);
	ciBoolean hostAutoUpdate();

protected:	
public:
	
};
#endif
