// UBCADBDownloaderDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "common/TraceLog.h"
#include "UBCADBDownloader.h"
#include "UBCADBDownloaderDlg.h"
#include "UbcCommon/WaitMessageBox.h"
#include <ci/libBase/ciStringTokenizer.h>
#include <ci/libAes256/ciElCryptoAes256.h>

#include <util/libEncoding/Encoding.h>
#include "io.h"
#include "atlenc.h"
#include <sys/stat.h>

#include <tlhelp32.h>
#include <shlwapi.h>
#include <winternl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define COLOR_BLACK			RGB(0,0,0)
#define COLOR_WHITE			RGB(255,255,255)
#define COLOR_LILAC			RGB(182,151,253)
#define COLOR_YELLOW		RGB(255,255,0)
#define COLOR_GRAY			RGB(200,200,200)
#define COLOR_DOWNLOAD		RGB(135,206,235)		// ::GetSysColor(COLOR_ACTIVECAPTION)
#define COLOR_FAIL			RGB(176,0,0)

/*
// jslee
adb pull mnt/external_sd/SQISoft/custom_contents/filelist.txt .

192.168.202.154 에만 적용되어 있습니다.
adb connect 192.168.202.154
데이터 순서는
contentsName, fullpath, contentsType, playTime, startDate, endDate

*/

// CUBCADBDownloaderDlg 대화 상자

#define		PEEK_MSG_TIMER		1004

void ProcessWindowMessage()
{
   MSG msg;
   while(::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
   {
      ::SendMessage(msg.hwnd, msg.message, msg.wParam, msg.lParam);
   }
}


CUBCADBDownloaderDlg::CUBCADBDownloaderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUBCADBDownloaderDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
	m_bRunning = FALSE;
	m_connected = FALSE;

}

void CUBCADBDownloaderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IPADDRESS1, m_editIP);
	DDX_Control(pDX, IDC_LIST1, m_lscTerminal);
	DDX_Control(pDX, ID_DOWNLOAD, m_btDownload);
	DDX_Control(pDX, IDC_EDIT_HOSTNAME, m_editHostName);
	DDX_Control(pDX, IDC_STATIC_MSG, m_staticMsg);
	DDX_Control(pDX, ID_DOWNLOAD_STOP, m_btStopDownload);
}

BEGIN_MESSAGE_MAP(CUBCADBDownloaderDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(ID_DOWNLOAD, &CUBCADBDownloaderDlg::OnBnClickedDownload)
	ON_BN_CLICKED(ID_OPEN_FOLDER, &CUBCADBDownloaderDlg::OnBnClickedOpenFolder)
	ON_BN_CLICKED(IDCANCEL, &CUBCADBDownloaderDlg::OnBnClickedCancel)
	ON_BN_CLICKED(ID_SAVE_IP, &CUBCADBDownloaderDlg::OnBnClickedSaveIp)
	ON_BN_CLICKED(ID_SETTINGS, &CUBCADBDownloaderDlg::OnBnClickedSettings)
	ON_BN_CLICKED(ID_DEL_CONTENTS, &CUBCADBDownloaderDlg::OnBnClickedDelContents)
	ON_BN_CLICKED(ID_INIT_IP, &CUBCADBDownloaderDlg::OnBnClickedInitIp)
	ON_BN_CLICKED(ID_DOWNLOAD_STOP, &CUBCADBDownloaderDlg::OnBnClickedDownloadStop)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CUBCADBDownloaderDlg 메시지 처리기

BOOL CUBCADBDownloaderDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	GetCurrentDir();

	GetDebug();

	GetContentsPath();

	GetIp();

	this->m_staticMsg.ShowWindow((m_debug?SW_SHOW:SW_HIDE));
	this->m_btStopDownload.EnableWindow(FALSE);

	InitList();
	//SetTimer(PEEK_MSG_TIMER, 2000, NULL);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CUBCADBDownloaderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CUBCADBDownloaderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CUBCADBDownloaderDlg::OnBnClickedDownloadStop()
{
	m_bRunning = FALSE;
}


void CUBCADBDownloaderDlg::OnBnClickedDownload()
{
	if(this->m_connected == false)
	{
		UbcMessageBox("먼저 우체국명을 입력하고 [상품홍보 연계]버튼을 누르세요", MB_ICONSTOP);
		return;	
	}

	this->m_btDownload.EnableWindow(FALSE);

	CWaitMessageBox wait;

	if(this->ReadFileList() <= 0) {
		this->m_btDownload.EnableWindow(TRUE);
		return;
	}

	if(this->CheckFiles() <= 0) {
		UbcMessageBox("다운로드할 파일이 없습니다");
		this->m_btDownload.EnableWindow(TRUE);
		return;
	}

	this->m_btStopDownload.EnableWindow(TRUE);

	int count = DownloadFiles();

	SetList();

	CString msg;
	msg.Format("%d 개 파일을 다운로드하였습니다.  다운로드된 파일을 상품홍보에 활용하시기 바랍니다", count);
	this->m_btDownload.EnableWindow(TRUE);
	this->m_btStopDownload.EnableWindow(FALSE);
	UbcMessageBox(msg);

}

int CUBCADBDownloaderDlg::ReadFileList()
{
	std::string filelist = m_currentPath;
	filelist += "filelist.ini";

	::DeleteFile(filelist.c_str());
	
	CString pull_txt = "pull /mnt/external_sd/SQISoft/custom_contents/filelist.txt filelist.ini";
	m_bRunning = TRUE;
	CString msg = RunADB(pull_txt);
	m_bRunning = FALSE;

	int nPos = 0;
	CString first = msg.Tokenize(" \t",nPos); 
	int byte = atoi(first);
	if(byte <= 0)
	{
		UbcMessageBox("다운로드할 파일 목록을 가져오지 못했습니다", MB_ICONSTOP);		
		return 0;
	}


	FILE* fp = fopen("filelist.ini","rb");
	if(!fp)
	{
		UbcMessageBox("다운로드할 파일 목록을 가져오지 못했습니다. 상품홍보 셋톱박스 전원상태 혹은 네트워크를 점검해 주십시오");
		return 0;
	}

	this->ClearInfo();

	char buf[MAX_PATH+1];
	while(fgets(buf,MAX_PATH,fp)>0)
	{
		ciString ansi_str;
		CEncoding::getInstance()->UTF8ToAnsi((const char*)buf, ansi_str);

		ContentsInfo* ele = new ContentsInfo();
		ciStringTokenizer aTokens(ansi_str.c_str(), ",");	
		int tokCount = aTokens.countTokens();
		if(tokCount < 6) {
			continue;
		}
		
		ele->contentsName = aTokens.nextToken().c_str();
		ele->contentsName.Trim("\"");

		ciString fullpath = "C:";
		fullpath += aTokens.nextToken().c_str();

		SplitPath(fullpath.c_str(), ele->contentsPath, ele->contentsFile );

		//ele->contentsFile = aTokens.nextToken().c_str();

		ele->contentsType = atoi(aTokens.nextToken().c_str());
		ele->playTime = atoi(aTokens.nextToken().c_str());
		ele->startDate = strtoul(aTokens.nextToken().c_str(),0,10);
		ele->endDate = strtoul(aTokens.nextToken().c_str(),0,10);

		if(tokCount == 7)
		{
			ele->volume = strtoul(aTokens.nextToken().c_str(),0,10);
		}

		this->m_infoList.push_back(ele);
	}
	fclose(fp);

	return m_infoList.size();	

}

void CUBCADBDownloaderDlg::OnBnClickedOpenFolder()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	::ShellExecute(NULL, "open", m_contentsPath, NULL, NULL, SW_SHOW);

}

void CUBCADBDownloaderDlg::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bRunning = FALSE;
	SetIp();

	//adb.exe 를 죽여주는 것이 좋다.
	_TerminateProcess("adb.exe");

	OnCancel();
}


void CUBCADBDownloaderDlg::InitList()
{
    // Font 변경
    HFONT hNewFont;    
    hNewFont=CreateFont( 11,0,0,0,0,0,0,0,HANGEUL_CHARSET,3,2,1,        
                                   VARIABLE_PITCH | FF_MODERN,"돋움체");
    m_lscTerminal.SendMessage( WM_SETFONT, (WPARAM)hNewFont, (LPARAM)TRUE);


	m_lscTerminal.SetTextFont("고딕체",10);

	m_lscTerminal.SetExtendedStyle(m_lscTerminal.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_INFOTIP| m_lscTerminal.GetExtendedStyle());
	//m_lscTerminal.SetExtendedStyle(m_lscTerminal.GetExtendedStyle() | LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_HEADERDRAGDROP|LVS_EX_INFOTIP| m_lscTerminal.GetExtendedStyle());

	//m_lscTerminal.SetExtendedStyle(m_lscTerminal.GetExtendedStyle()|LVS_EX_SUBITEMIMAGES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
/*
	CBitmap bmCheck;
	bmCheck.LoadBitmap(IDB_HOST_LIST);
	m_ilHostList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 0, 1);
	m_ilHostList.Add(&bmCheck, RGB(255, 255, 255));
	m_lscTerminal.SetImageList(&m_ilHostList, LVSIL_SMALL);
*/
	//m_lscTerminal.InsertColumn(eCheck,	"선택",		LVCFMT_LEFT, 22);
	m_lscTerminal.InsertColumn(eContentsName,	"제목",		LVCFMT_LEFT, 200);
	m_lscTerminal.InsertColumn(eContentsFile,	"파일명", LVCFMT_LEFT, 240);
	m_lscTerminal.InsertColumn(eContentsType,	"종류",			LVCFMT_LEFT, 60);
	m_lscTerminal.InsertColumn(ePlayTime,		"러닝타임",		LVCFMT_LEFT, 50);
	m_lscTerminal.InsertColumn(eStartDate,		"상영시작",		LVCFMT_LEFT, 90);
	m_lscTerminal.InsertColumn(eEndDate,		"상영종료",		LVCFMT_LEFT, 90);
	m_lscTerminal.InsertColumn(eFileSize,		"파일크기",		LVCFMT_LEFT, 90);
	m_lscTerminal.InsertColumn(eDownloadState,		"상태",		LVCFMT_LEFT, 130);
	m_lscTerminal.InsertColumn(eDownloadTime,		"파일받은시간",		LVCFMT_LEFT, 160);

	//m_lscTerminal.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_CHECKBOX);
	m_lscTerminal.InitHeader(IDB_LISTCTRLEX_HEADER, CUTBListCtrlEx::LS_NONE);
}

void CUBCADBDownloaderDlg::ClearInfo()
{
	for(ContentsInfoList::iterator Iter = m_infoList.begin(); Iter != m_infoList.end(); Iter++)
	{
		delete (*Iter);
	}
	m_infoList.clear();
}

void CUBCADBDownloaderDlg::SetList()
{
	m_lscTerminal.DeleteAllItems();

	int nRow=0;
	for(ContentsInfoList::iterator Iter = m_infoList.begin(); Iter != m_infoList.end(); Iter++)
	{
		ContentsInfo* ele = (*Iter);
		SetList(nRow,ele);
		nRow++;
	}

}

void CUBCADBDownloaderDlg::SetList(int nRow, ContentsInfo* ele)
{
	m_lscTerminal.InsertItem(nRow, "");

	m_lscTerminal.SetItemText(nRow,eContentsName,	ele->contentsName);
	m_lscTerminal.SetItemText(nRow,eContentsFile,	ele->contentsFile);

	CString typeStr;
	switch(ele->contentsType)
	{
		case	0 : typeStr = "동영상";		break;
		case	2 :	typeStr = "그림";		break;
		case   15 :	typeStr = "PPT";		break;
		case	7 :	typeStr = "URL";		break;
		default	  : typeStr = "Unknown";	break;
	}

	m_lscTerminal.SetItemText(nRow,eContentsType,	typeStr);

	CString playTimeStr;
	playTimeStr.Format("%d", ele->playTime);

	m_lscTerminal.SetItemText(nRow,ePlayTime,		playTimeStr);

	CString startDateStr;
	startDateStr.Format("%04d/%02d/%02d", 
		ele->startDate.GetYear(), ele->startDate.GetMonth(), ele->startDate.GetDay());

	m_lscTerminal.SetItemText(nRow,eStartDate,		startDateStr);	

	CString endDateStr;
	endDateStr.Format("%04d/%02d/%02d", 
		ele->endDate.GetYear(), ele->endDate.GetMonth(), ele->endDate.GetDay());

	m_lscTerminal.SetItemText(nRow,eEndDate,		endDateStr);
	
	CString fileSizeStr;
	
	double mbyte = (ele->volume/1000000.0);
	fileSizeStr.Format("%5.2f MB", mbyte);
	m_lscTerminal.SetItemText(nRow,eFileSize,		fileSizeStr);
	
	CString stateStr = "다운로드하세요";
	switch(ele->downloadState)
	{
		case 1: stateStr = "최신다운로드 완료"; break;
		case 2: stateStr = "이미다운로드"; break;
		case 3: stateStr = "다운로드에러"; break;
	}
	m_lscTerminal.SetItemText(nRow,eDownloadState,		stateStr);

	COLORREF clrBack = COLOR_GRAY;
	if(ele->downloadState == 1) {
		clrBack = COLOR_DOWNLOAD;
	}else
	if(ele->downloadState == 0 || ele->downloadState == 3) {
		clrBack = COLOR_YELLOW;
	}
	m_lscTerminal.SetRowColor(nRow,clrBack,m_lscTerminal.GetTextColor());

	CString downloadTimeStr;
	downloadTimeStr.Format("%04d/%02d/%02d %02d:%02d:%02d", 
		ele->downloadTime.GetYear(), ele->downloadTime.GetMonth(), ele->downloadTime.GetDay(),
		ele->downloadTime.GetHour(), ele->downloadTime.GetMinute(), ele->downloadTime.GetSecond()
		);
	m_lscTerminal.SetItemText(nRow,eDownloadTime,		downloadTimeStr);
}


int CUBCADBDownloaderDlg::CheckFiles()
{
	// 현재, Contents Directory 에 동일이름 동일 사이즈 파일이 있는지 살펴본다.
	// 있는 놈은 제외시킨다.

	int nRow=0;
	for(ContentsInfoList::iterator Iter = m_infoList.begin(); Iter != m_infoList.end(); Iter++)
	{
		ContentsInfo* ele = (*Iter);
		CString localPath = m_contentsPath;
		localPath += "\\";
		localPath += ele->contentsFile;

		if(::access(localPath,0)==0){
			// 로컬에 파일이 있음
			struct _stati64 stat = {0};
			_stati64((LPCSTR)localPath, &stat);

			size_t localSize = stat.st_size;

			if(localSize > 0  && localSize == ele->volume) {
				ele->downloadState = 2; // 이미있음으로 표시함.
				ele->downloadTime = stat.st_mtime;
				continue;
			}else{
				ele->downloadState = 0; 
			}
		}else{
			ele->downloadState = 0;
			ele->downloadTime = 0;
		}
		nRow++;	
	}
	return nRow;
}

int CUBCADBDownloaderDlg::MoveFiles(ciString& prev_folder)
{
	// prev_folder 에 있는 파일들을 m_contentsPath 로 move한다.

	if(m_debug) UbcMessageBox(prev_folder.c_str());


	int nRow=0;
	for(ContentsInfoList::iterator Iter = m_infoList.begin(); Iter != m_infoList.end(); Iter++)
	{
		ContentsInfo* ele = (*Iter);
		CString targetPath = m_contentsPath;
		targetPath += "\\";
		targetPath += ele->contentsFile;

		CString sourcePath = prev_folder.c_str();
		sourcePath += "\\";
		sourcePath += ele->contentsFile;

		if(m_debug)
		{
			CString msg;
			msg.Format("MoveFileEx(src=%s, des=%s)", sourcePath, targetPath);
			this->m_staticMsg.SetWindowText(msg);
		}

		if(::access(sourcePath,0)==0){
			// prev_folder에 파일이 있음
			if(::MoveFileEx(sourcePath,targetPath, MOVEFILE_WRITE_THROUGH // 파일이 실제로 완전히 이동되기 전에는 리턴하지 않는다. 파일 이동 후 이동된 새 파일을 곧바로 사용하고자 할 때는 이 플래그를 사용해야 한다.
												| MOVEFILE_COPY_ALLOWED)) // 파일이 다른 드라이브간에 이동될 때는 CopyFile, DeleteFile 함수 호출을 시뮬레이트하도록 한다);
			{
				nRow++;	
			}else{
				if(m_debug)
				{
					CString msg;
					DWORD dwErr = GetLastError();
					msg.Format("MoveFileEx fail : src=%s, des=%s, errCode=%d", sourcePath, targetPath, dwErr);
					this->m_staticMsg.SetWindowText(msg);
				}

			}
		}else{
			if(m_debug)
			{
				CString msg;
				msg.Format("access fail(src=%s, des=%s)", sourcePath, targetPath);
				this->m_staticMsg.SetWindowText(msg);
			}
		}

	}
	return nRow;
}



int CUBCADBDownloaderDlg::DownloadFiles()
{

	m_bRunning = TRUE;

	CTime now = CTime::GetCurrentTime();
	int nRow=0;
	int total_byte = 0;
	for(ContentsInfoList::iterator Iter = m_infoList.begin(); Iter != m_infoList.end(); Iter++)
	{
		if(m_bRunning == FALSE) 
		{
			break;
		}
		ContentsInfo* ele = (*Iter);
		if(ele->downloadState==2 || ele->downloadState==1){
			continue;
		}

		CString downloadCommand = "pull ";
		downloadCommand += ele->contentsPath;
		downloadCommand += ele->contentsFile;
		downloadCommand += " \"";
		downloadCommand += m_contentsPath;
		downloadCommand += "\"";
		
		//UbcMessageBox(downloadCommand);

		CString msg = RunADB(downloadCommand);

		int nPos = 0;
		CString first = msg.Tokenize(" \t\n",nPos); 
		int byte = atoi(first);
		if(byte > 0)
		{
			// 실제로 파일이 있는지 다시 한번 체크해준다.
			CString downloadFile = this->m_contentsPath;
			downloadFile += "\\";
			downloadFile += ele->contentsFile;
			if(::access(downloadFile,0)==0)
			{
				ele->downloadState = 1;
				ele->downloadTime = now;
				nRow++;
			}
		}
		// 윈도 메시지 한번 날려준다. (응답없음에 빠지지 않기 위해)
		//ProcessWindowMessage();
	}
	m_bRunning = FALSE;

	return nRow;
}



CString CUBCADBDownloaderDlg::RunADB(LPCTSTR parameter)
{
	CString adb_command = m_currentPath + "adb.exe";
	TCHAR szParam[MAX_PATH*4];
	sprintf(szParam, _T("%s %s"), adb_command, parameter);
	//sprintf(szParam, _T("%s %s 2>&1"), adb_command, parameter);
/*
	CString msg;
	FILE* fp = _popen(szParam, "r");
	if( fp != NULL )
	{
		char buf[1024+1];
		memset(buf, 0x00, sizeof(buf));
		while( fgets(buf, 1024, fp) != 0 )
		{
			if( strlen(buf) > 0 )
			{
				msg += buf;
			}
			memset(buf, 0x00, sizeof(buf));
		}
		_pclose(fp);
	}
	//UbcMessageBox(msg);
	return msg;
*/

	if(m_debug)
	{
		CString msg;
		msg.Format("[%s]", szParam);
		this->m_staticMsg.SetWindowText(msg);
	}

	SECURITY_ATTRIBUTES sa = { 0 };
	sa.nLength = sizeof(sa);
	sa.bInheritHandle = TRUE;
	TCHAR szOutputFilePath[MAX_PATH];
	lstrcpy(szOutputFilePath, TEXT("stdout.txt"));
	HANDLE hFile = CreateFile(szOutputFilePath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, &sa, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	STARTUPINFO si = { 0 };
	si.cb = sizeof(si);
	si.dwFlags = STARTF_USESTDHANDLES;
	si.hStdInput = stdin;
	si.hStdOutput = hFile;
	si.hStdError = hFile;

/*
	STARTUPINFO si = {0};
	si.cb = sizeof(STARTUPINFO);
	si.wShowWindow = SW_HIDE;
	si.dwFlags = STARTF_USESHOWWINDOW;	// 콘솔창이 보이지 않도록 한다
*/

	PROCESS_INFORMATION pi;

	if(!CreateProcess((LPSTR)(LPCTSTR)adb_command 
					, (LPSTR)(LPCTSTR)szParam 
					, NULL
					, NULL
					, TRUE
					, CREATE_NO_WINDOW
					, NULL
					, NULL
					, &si
					, &pi
					) )
	{
		DWORD dwErrNo = GetLastError();

		LPVOID lpMsgBuf = NULL;
		FormatMessage(  FORMAT_MESSAGE_ALLOCATE_BUFFER
					  | FORMAT_MESSAGE_IGNORE_INSERTS 
					  | FORMAT_MESSAGE_FROM_SYSTEM     
					  ,	NULL
					  , dwErrNo
					  , 0
					  , (LPTSTR)&lpMsgBuf
					  , 0
					  , NULL );

		CString strErrMsg;
		if(!lpMsgBuf)
		{
			strErrMsg.Format(_T("Unknow Error - CreateProcess [%d]"), dwErrNo);
		}
		else
		{
			strErrMsg.Format(_T("%s [%d]"), (LPCTSTR)lpMsgBuf, dwErrNo);
		}
	
		LocalFree( lpMsgBuf );
		UbcMessageBox(strErrMsg);
		return "0 ";
	}
	
	while(::WaitForSingleObject( pi.hProcess, 0))
	{
		if(!m_bRunning)
		{
			UbcMessageBox("Createprocess break!!!");
			TerminateProcess(pi.hProcess, 0);
			::WaitForSingleObject( pi.hProcess, 5000);
			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
			return "0 ";
		}
		//if(m_debug)	UbcMessageBox("processWindowMessage");

		ProcessWindowMessage();
		Sleep(1000);
	}
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	CloseHandle(hFile);
/*
	CString msg = "1 ";
	FILE* fp = fopen(szOutputFilePath, "r");
	if( fp != NULL )
	{
		char buf[1024+1];
		memset(buf, 0x00, sizeof(buf));
		while( fgets(buf, 1024, fp) != 0 )
		{
			if( strlen(buf) > 0 )
			{
				msg += buf;
			}
			memset(buf, 0x00, sizeof(buf));
		}
		fclose(fp);
	}
	return msg;
	*/
	return "1 ";
}

void CUBCADBDownloaderDlg::GetCurrentDir()
{
	TCHAR szModule[MAX_PATH];
	::ZeroMemory(szModule, MAX_PATH);
	::GetModuleFileName(NULL, szModule, MAX_PATH);

	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(szModule, szDrive, szPath, szFilename, szExt);

	m_currentPath = szDrive;
	m_currentPath += szPath;

	m_contentsPath = szDrive;
	m_contentsPath += "\\SQISoft_adb\\Contents";

	return;
}

void CUBCADBDownloaderDlg::SplitPath(LPCTSTR input, CString& path, CString& filename)
{
	TCHAR szDrive[MAX_PATH], szPath[MAX_PATH], szFilename[MAX_PATH], szExt[MAX_PATH];
	_tsplitpath(input, szDrive, szPath, szFilename, szExt);

	path = szPath;
	filename = szFilename;
	filename += szExt;

}

BOOL CUBCADBDownloaderDlg::ADBConnect()
{
	if(m_ip == "0.0.0.0") {
		UbcMessageBox("먼저 상품홍보 셋톱박스의 IP 를 입력하세요", MB_ICONSTOP);
		return FALSE;
	}

	if(m_ip == "255.255.255.255"){ //직접연결테스트를 위한 케이스임
		m_connected = true;
		return TRUE;
	}

	CString connect = "connect ";
	connect += m_ip;

	m_connected = false;
	
	m_bRunning = TRUE;
	RunADB(connect);
	m_bRunning = FALSE;

	// connect test
	int line_no =0;
	CString devices;
	CString adb_connect_test = m_currentPath + "\\adb.exe devices";
	FILE* fp = _popen(adb_connect_test, "r");
	if( fp != NULL )
	{
		char buf[1024+1];
		memset(buf, 0x00, sizeof(buf));
		while( fgets(buf, 1024, fp) != 0 )
		{
			if( strlen(buf) > 0 )
			{
				line_no++;
				devices += buf;
			}
			memset(buf, 0x00, sizeof(buf));
		}
		_pclose(fp);
	}

	if(line_no < 3)
	{
		//UbcMessageBox("먼저 상품홍보 셋톱박스와 연결에 실패하였습니다. 네트웍 또는 IP 를 다시 확인하십시오", MB_ICONSTOP);
		return FALSE;
	}


	m_connected = true;
	return TRUE;
}


BOOL CUBCADBDownloaderDlg::PingTest()
{
	if(m_ip == "0.0.0.0") {
		UbcMessageBox("먼저 상품홍보 셋톱박스의 IP 를 입력하세요", MB_ICONSTOP);
		return FALSE;
	}

	if(m_ip == "255.255.255.255"){ //직접연결테스트를 위한 케이스임
		m_connected = true;
		return TRUE;
	}

	CString ping_test = m_currentPath + "\\cping.exe ";
	ping_test += m_ip;
	FILE* fp = _popen(ping_test, "r");
	if( fp != NULL )
	{
		char buf[1024+1];
		memset(buf, 0x00, sizeof(buf));
		while( fgets(buf, 1024, fp) != 0 )
		{
			if( strncmp(buf,"succeed",strlen("succeed"))==0 )
			{
				_pclose(fp);
				return TRUE;
			}
			memset(buf, 0x00, sizeof(buf));
		}
		_pclose(fp);
	}

	return FALSE;
}

BOOL CUBCADBDownloaderDlg::FindIP()
{
	int retval = _FindIP(m_hostname);
	if(retval == 1) 
	{
		return TRUE;
	}
	if(retval == -1)
	{
		UbcMessageBox("상품홍보 셋탑박스의 IP가 자동검색되지 않았습니다. 우체국명을 올바르게 입력하였는지 다시 한번 확인하여 주십시오. 우체국명을 올바르게 넣었는데도 연결이 되지 않는다면, 상품홍보 시스템 운영실에 IP를 문의하여 셋톱박스 IP 란에 입력하고 사용하여 주시기 바랍니다", MB_ICONSTOP);
		return FALSE;
	}
	
	CString new_name = m_hostname;
	new_name += "2";

	retval = _FindIP(new_name);
	if(retval == 1) 
	{
		return TRUE;
	}

	UbcMessageBox("상품홍보 셋탑박스와 네트워크 연결이 되지 않습니다.  현재 사용중인 PC 가 우편망에 연결된 PC 인지 확인하고, 상품홍보 셋톱박스의 전원 혹은 네트워크를 다시 한번 확인하여 주시기바랍니다.  ", MB_ICONSTOP);
	return FALSE;

}

int CUBCADBDownloaderDlg::_FindIP(CString& hostname)
{
	CString ini = this->m_currentPath;
	ini += "profile.ini";

	char enc[64] = {0};
	GetPrivateProfileString("ENC", "ENC", "", enc, 64, ini);

	BOOL encrypted = TRUE;
	if(enc==0 || strlen(enc) == 0 || enc[0] != '1'){
		encrypted = this->EncryptDataFile(ini);
	}
	
	if(!this->DecryptDataFile(ini)) {
		UbcMessageBox("Decrypt fail");
		return -1;
	}


	IP_MAP::iterator itr = this->_ip_map.find(ciString(hostname));
	if(itr == this->_ip_map.end())
	{
		return -1;
	}

	m_ip = itr->second.c_str();

	if(!PingTest()){
		return -2;
	}


	if(m_debug) {
		CString msg;
		msg.Format("ip founded %s=[%s]", hostname, m_ip);
		m_staticMsg.SetWindowText(msg);
	}
	return 1;
	
/*
	char ip[64] = {0};
	GetPrivateProfileString("ENC", m_hostname, "", ip, 64, ini);


	if(ip == 0 || strlen(ip)==0)
	{
		return FALSE;
	}

	//CString msg;
	//msg.Format("ip founded %s=%s", m_hostname, ip);
	//UbcMessageBox(msg);

	if(encrypted){
		m_ip = Decrypt(ip);
	}else{
		m_ip = ip;
	}
	//msg.Format("ip founded %s=%s", m_hostname, m_ip);
	//UbcMessageBox(msg);

	//this->m_editIP.SetWindowText(m_ip);
	return TRUE;
*/
}

void CUBCADBDownloaderDlg::OnBnClickedSaveIp()
{
	OnBnClickedConnect(true);
}

void CUBCADBDownloaderDlg::SetIp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString ini = this->m_currentPath;
	ini += "UBCADBDownloader.ini";

	if(m_is_input_ip)
	{
		this->m_editIP.GetWindowText(m_ip);

		CString enc_ip = AsciiToBase64(m_ip);

		WritePrivateProfileString("SETTOP", "IP", enc_ip, ini);
	}

	this->m_editHostName.GetWindowText(m_hostname);
	if(!m_hostname.IsEmpty()){
		CheckHostName();
		WritePrivateProfileString("SETTOP", "HostName", m_hostname, ini);
	}
}



void CUBCADBDownloaderDlg::GetIp()
{
	CString ini = this->m_currentPath;
	ini += "UBCADBDownloader.ini";

	char ip[64] = {0};
	memset(ip,0x00,64);
	GetPrivateProfileString("SETTOP", "IP", "MC4wLjAuMA==", ip, 64, ini);
	CString dec_ip = Base64ToAscii(ip);
	this->m_editIP.SetWindowText(dec_ip);
	m_ip = dec_ip;

	if(m_ip.IsEmpty() || m_ip == "0.0.0.0") 
	{
		m_is_input_ip = false;
	}else{
		m_is_input_ip = true;
	}

	char hostname[64] = {0};
	GetPrivateProfileString("SETTOP", "HostName", "", hostname, 64, ini);
	this->m_editHostName.SetWindowText(hostname);
	m_hostname = hostname;

}

void CUBCADBDownloaderDlg::GetDebug()
{
	CString ini = this->m_currentPath;
	ini += "UBCADBDownloader.ini";

	char debug[64] = {0};
	GetPrivateProfileString("SETTOP", "DEBUG", "0", debug, 64, ini);
	
	m_debug = BOOL(atoi(debug));

}


void CUBCADBDownloaderDlg::GetContentsPath()
{
	CString ini = this->m_currentPath;
	ini += "UBCADBDownloader.ini";

	char contentsPath[MAX_PATH+1] = {0};
	GetPrivateProfileString("SETTOP", "CONTENTS_PATH", this->m_contentsPath, contentsPath, MAX_PATH, ini);

	this->m_contentsPath = contentsPath;
}

void CUBCADBDownloaderDlg::SetContentsPath()
{
	CString ini = this->m_currentPath;
	ini += "UBCADBDownloader.ini";

	//UbcMessageBox("aaaaa");
	//UbcMessageBox(this->m_contentsPath);


	WritePrivateProfileString("SETTOP", "CONTENTS_PATH", this->m_contentsPath, ini);
}


static int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	switch(uMsg)
	{
	case BFFM_INITIALIZED:
		{
			SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)lpData);
		}
		break;
	}
	return 0; 
}

void CUBCADBDownloaderDlg::OnBnClickedSettings()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// 저장 폴더 설정
	

	std::string prev_folder = this->m_contentsPath;

	ITEMIDLIST        *pidlBrowse;
	char    pszPathname[MAX_PATH];
	memset(pszPathname,0x00,MAX_PATH);
	
	BROWSEINFO BrInfo;
	BrInfo.hwndOwner = NULL; //GetSafeHwnd();
	BrInfo.pidlRoot = NULL;
	memset( &BrInfo, 0, sizeof(BrInfo) );
	BrInfo.pszDisplayName = (LPSTR)(LPCTSTR)this->m_contentsPath;
	BrInfo.lpszTitle = "콘텐츠가 저장될 디렉토리를 선택하세요";
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS;

	BrInfo.lpfn = BrowseCallbackProc;
	//BrInfo.lParam = (LPARAM)this->m_contentsPath.GetBuffer(this->m_contentsPath.GetLength());
	BrInfo.lParam = (LPARAM)(LPSTR)(LPCTSTR)this->m_contentsPath;

	if(m_debug) UbcMessageBox(prev_folder.c_str());
	

	// 다이얼로그를 띄우기
	pidlBrowse = ::SHBrowseForFolder(&BrInfo);   
	if( pidlBrowse != NULL)
	{
	 // 패스를 얻어옴
		 ::SHGetPathFromIDList(pidlBrowse, pszPathname);   
		 this->m_contentsPath = pszPathname;
		/*
		 if(this->m_contentsPath.Find(' ') >= 0 || this->m_contentsPath.Find('\t') >= 0) 
		 {
			UbcMessageBox("저장 폴더명에는 공란(빈칸)을 사용할 수 없습니다, 다른 폴더를 사용하십시오");
			this->m_contentsPath = prev_folder;
			return ;
		 }
		*/
		 if(prev_folder != ciString(this->m_contentsPath)) {
			SetContentsPath();

			MoveFiles(prev_folder);
			CheckFiles();
			SetList();
		 
		 }


	}

}


void CUBCADBDownloaderDlg::OnBnClickedDelContents()
{
	bool bCheck = false;
	for(int nRow = 0; nRow < m_lscTerminal.GetItemCount(); nRow++)
	{
		if(m_lscTerminal.GetCheck(nRow))
		{
			bCheck = true;
			break;
		}
	}

	if(!bCheck)
	{
		UbcMessageBox("먼저 삭제할 콘텐츠를 선택하세요", MB_ICONINFORMATION);
		return;
	}

	CWaitMessageBox wait;
	int counter = 0;

	/*
	CList<ContentsInfo>  lsList;
	for(ContentsInfoList::iterator Iter = m_infoList.begin(); Iter != m_infoList.end(); Iter++)
	{
		ContentsInfo* ele = (*Iter);
		
		ContentsInfo newEle;
		newEle.contentsFile = ele->contentsFile;
		newEle.contentsPath = ele->contentsPath;

		lsList.AddTail(newEle);
	}
	*/

	for(int nRow = m_lscTerminal.GetItemCount()-1; 0 <= nRow; nRow--)
	{
		// 선택되어 있지 않으면 스킵
		if(!m_lscTerminal.GetCheck(nRow)) continue;

		CString filename = m_lscTerminal.GetItemText(nRow,eContentsFile);

		// 파일 삭제
		CString fullpath = this->m_contentsPath + "\\" + filename;

		if(::DeleteFile(fullpath))
		{
			// 선택해제
			m_lscTerminal.SetCheck(nRow, FALSE);
			m_lscTerminal.DeleteItem(nRow);
			counter++;
		}
	}

	CString msg;
	msg.Format("%d 콘텐츠가 삭제되었습니다.", counter);
	UbcMessageBox(msg, MB_ICONINFORMATION);



}

/*
BOOL CUBCADBDownloaderDlg::EncryptDataFile(CString& inipath)
{
	UbcMessageBox("Encrypt file");
	FILE* fp = fopen(inipath,"r");
	if(!fp) {
		return FALSE;
	}

	_ip_map.clear();

	char buf[MAX_PATH+1];
	while(fgets(buf,MAX_PATH,fp)>0)
	{
		CString tok(buf);
		int nPos = 0;
		std::string name = tok.Tokenize("=",nPos); 
		if(name == "ENC") continue;
		if(name.length() >= 5 && name.substr(0,5) == "[ENC]") continue;

		CString value = tok.Tokenize("=",nPos); 
		std::string enc_value = AsciiToBase64(value);
		_ip_map.insert(IP_MAP::value_type(name,enc_value));
	}
	fclose(fp);

	FILE* wfp = fopen(inipath,"w");
	fprintf(wfp,"[ENC]\nENC=1\n");
	fclose(wfp);

	IP_MAP::iterator itr;
	for(itr=_ip_map.begin();itr!=_ip_map.end();itr++)
	{
		WritePrivateProfileString("ENC", itr->first.c_str(), itr->second.c_str(), inipath);
	}
	return TRUE;
}
*/

BOOL CUBCADBDownloaderDlg::EncryptDataFile(CString& inipath)
{
	UbcMessageBox("Encrypt file");

	FILE* fp = fopen(inipath,"r");
	if(!fp) {
		return FALSE;
	}

	_ip_map.clear();

	char buf[MAX_PATH+1];
	while(fgets(buf,MAX_PATH,fp)>0)
	{
		CString tok(buf);
		int nPos = 0;
		std::string name = tok.Tokenize("=",nPos); 
		if(name == "ENC") continue;
		if(name.length() >= 5 && name.substr(0,5) == "[ENC]") continue;

		std::string name2 = name + "2";
		
		CString value = tok.Tokenize("=",nPos); 

		std::string cipher_name = ciAes256Util::Encrypt(name,0);
		std::string enc_name = AsciiToBase64(cipher_name.c_str());

		std::string cipher_name2 = ciAes256Util::Encrypt(name2,0);
		std::string enc_name2 = AsciiToBase64(cipher_name2.c_str());

		std::string cipher_value = ciAes256Util::Encrypt(ciString(value),0);
		std::string enc_value = AsciiToBase64(cipher_value.c_str());

		IP_MAP::iterator itr = this->_ip_map.find(enc_name);
		if(itr == this->_ip_map.end())
		{	// 없으면 name 을 그대로 넣는다.
			_ip_map.insert(IP_MAP::value_type(enc_name,enc_value));
		}
		else
		{
			// 이미 있으면 name2 로넣는다.
			_ip_map.insert(IP_MAP::value_type(enc_name2,enc_value));
		}
	}
	fclose(fp);

	FILE* wfp = fopen(inipath,"w");
	fprintf(wfp,"[ENC]\nENC=1\n");

	int nRow=0;
	IP_MAP::iterator itr;
	for(itr=_ip_map.begin();itr!=_ip_map.end();itr++)
	{
		nRow++;
		fprintf(wfp,"%d=%s,%s\n", nRow,itr->first.c_str(), itr->second.c_str());
	}

	fclose(wfp);
	return TRUE;
}

BOOL CUBCADBDownloaderDlg::DecryptDataFile(CString& inipath)
{
	FILE* fp = fopen(inipath,"r");
	if(!fp) {
		return FALSE;
	}

	_ip_map.clear();

	int nRow = 0;
	char buf[MAX_PATH+1];
	while(fgets(buf,MAX_PATH,fp)>0)
	{
		CString tok(buf);

		if(tok.GetLength() >= 5)
		{
			if(tok.Mid(0,5) == "[ENC]" || tok.Mid(0,3) == "ENC")	continue;
		}

		int pos = tok.Find('='); 
		if(pos <= 0) continue;

		CString value = tok.Mid(pos+1);

		int iPos = 0;
		CString real_name = value.Tokenize(",", iPos);
		CString real_value = value.Tokenize(",", iPos);


		std::string dec_value =Base64ToAscii(real_value);
		std::string dec_name = Base64ToAscii(real_name);

		std::string plain_name = ciAes256Util::Decrypt(dec_name);
		std::string plain_value = ciAes256Util::Decrypt(dec_value);
			
		ciStringUtil::removeSpace(plain_value); // \n trim

		_ip_map.insert(IP_MAP::value_type(plain_name,plain_value));

		nRow++;
	}
	fclose(fp);
	return TRUE;
}



CString CUBCADBDownloaderDlg::Decrypt(TCHAR* ip)
{
	return Base64ToAscii(ip);
}


CString CUBCADBDownloaderDlg::AsciiToBase64(CString strAscii)
{
	int nDestLen = Base64EncodeGetRequiredLength(strAscii.GetLength());

	CString strBase64;
	Base64Encode((const BYTE*)(LPCSTR)strAscii, strAscii.GetLength(),strBase64.GetBuffer(nDestLen), &nDestLen);
	strBase64.ReleaseBuffer(nDestLen);

	return strBase64;
}

CString CUBCADBDownloaderDlg::Base64ToAscii(CString strBase64)
{
	int nDecLen = Base64DecodeGetRequiredLength(strBase64.GetLength());

	CString strAscii;
	Base64Decode(strBase64, strBase64.GetLength(), (BYTE*)strAscii.GetBuffer(nDecLen), &nDecLen);
	strAscii.ReleaseBuffer(nDecLen);
	return strAscii;
}
void CUBCADBDownloaderDlg::OnBnClickedInitIp()
{
	OnBnClickedConnect(false);
}


void CUBCADBDownloaderDlg::OnBnClickedConnect(bool hostname_button)
{
	this->_TerminateProcess("adb.exe");

	if(m_debug == TRUE) {
		UbcMessageBox("Debug Mode");
	}


	if(hostname_button) {
		this->m_editHostName.GetWindowText(m_hostname);
		if(m_hostname.IsEmpty())
		{
			UbcMessageBox("먼저 우체국명을 입력해주십시요", MB_ICONSTOP);
			return;
		}
		CheckHostName();

		if(!FindIP())
		{
			//UbcMessageBox("상품홍보 셋탑박스의 IP가 자동검색되지 않았습니다. 우체국명을 올바르게 입력하였는지 다시 한번 확인하여 주십시오. 우체국명을 올바르게 넣었는데도 연결이 되지 않는다면, 상품홍보 시스템 운영실에 IP를 문의하여 셋톱박스 IP 란에 입력하고 사용하여 주시기 바랍니다", MB_ICONSTOP);
			return;
		}
		m_is_input_ip = false;
	}
	else
	{
		this->m_editIP.GetWindowText(m_ip);
		if((m_ip.IsEmpty() || m_ip == "0.0.0.0"))
		{
			UbcMessageBox("상품홍보 셋탑박스의 IP를 입력해주십시요", MB_ICONSTOP);
			return;
	
		}
		m_is_input_ip = true;
	}

	CWaitMessageBox wait;

	m_lscTerminal.DeleteAllItems();

	if(ADBConnect()) 
	{

		int count = ReadFileList();
		if(count==0){
			UbcMessageBox("다운로드받을 파일이 없습니다.", MB_ICONSTOP);
			return ;
		}

		CheckFiles();
		SetList();

		SetIp();

	}else{
		UbcMessageBox("상품홍보 셋톱박스와의 연결에 실패하였습니다. 상품홍보 셋톱박스의 전원/네트워크 상태를 확인하십시오. 계속 연결이 안될경우, 상품홍보 시스템 운영실에 연락하시기 바랍니다.", MB_ICONSTOP);
		
		CString ini = this->m_currentPath;
		ini += "UBCADBDownloader.ini";
		this->m_editHostName.GetWindowText(m_hostname);
		if(!m_hostname.IsEmpty()){
			CheckHostName();
			WritePrivateProfileString("SETTOP", "HostName", m_hostname, ini);
		}
	}

}


void CUBCADBDownloaderDlg::_TerminateProcess(CString processName)
{
	CArray<DWORD,DWORD> processIDs;

	if(_IsAliveProcess(processName, &processIDs))  //살아있으면
	{
		for(int i=0; i < processIDs.GetSize(); i++)
		{
			DWORD processID = processIDs.GetAt(i);
			_KillProcess(processID);
		}
	}
}

BOOL CUBCADBDownloaderDlg::_IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs)
{
	CString newName = "";

	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);

	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(PROCESSENTRY32);

	BOOL bIsAlive = FALSE;

	while(Process32Next(hSnapShot,&pEntry))
	{	
		// 1. 프로세스명 얻기
		CString newName = pEntry.szExeFile;

		newName.MakeUpper();
		processName.MakeUpper();

		// 2. 프로세스명 비교
		if( processName.Find(newName) >= 0 )
		{
			if(processIDs != NULL)
				processIDs->Add(pEntry.th32ProcessID);
			bIsAlive = TRUE;
		}
	}

	CloseHandle(hSnapShot);

	return bIsAlive;
}

void CUBCADBDownloaderDlg::_KillProcess(DWORD dwProcessId)
{
	HANDLE hProcessGUI = ::OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId);

	if( hProcessGUI ) 
	{
		::TerminateProcess(hProcessGUI, 0);
		::CloseHandle(hProcessGUI);
	}
}
void CUBCADBDownloaderDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	static int pickCounter=0;

	CDialog::OnTimer(nIDEvent);
	if(nIDEvent == PEEK_MSG_TIMER)
	{ //3초 타임머
		if(this->m_bRunning)
		{
			// 윈도 메시지 한번 날려준다. (응답없음에 빠지지 않기 위해)
			if(m_debug)
			{
				CString msg;
				msg.Format("PEEKED(%d)", pickCounter++);
				this->m_staticMsg.SetWindowText(msg);
			}
			ProcessWindowMessage();

		}
	}
}

void CUBCADBDownloaderDlg::CheckHostName()
{
	if(m_hostname.Find("우체국") < 0 && 
		m_hostname.Find("우정") < 0 && 
		m_hostname.Find("TEST") < 0 && 
		m_hostname.Find("사업") < 0 && 
		m_hostname.Find("본부") < 0){
		m_hostname += "우체국";
		this->m_editHostName.SetWindowText(m_hostname);
	}
}