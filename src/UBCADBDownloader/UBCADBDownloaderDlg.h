// UBCADBDownloaderDlg.h : 헤더 파일
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "common\utblistctrlex.h"
#include <list>
#include <map>

class ContentsInfo
{
public:
	ContentsInfo() : contentsType(0), playTime(0), volume(0), downloadState(0) {}
	CString	contentsName;
	CString	contentsPath;
	CString	contentsFile;
	int	contentsType;
	int	playTime;	
	CTime	startDate;
	CTime	endDate;
	size_t	volume;

	int		downloadState;
	CTime	downloadTime;
};

typedef std::list<ContentsInfo*>	ContentsInfoList;


// CUBCADBDownloaderDlg 대화 상자
class CUBCADBDownloaderDlg : public CDialog
{
// 생성입니다.
public:
	CUBCADBDownloaderDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_UBCADBDOWNLOADER_DIALOG };
	enum { /*eCheck,*/
			eContentsName,
		   eContentsFile,
		   eContentsType,
		   ePlayTime,	
		   eStartDate,	
		   eEndDate,
		   eFileSize,
			eDownloadState,
			eDownloadTime};	

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	void _TerminateProcess(CString processName);
	BOOL _IsAliveProcess(CString processName, CArray<DWORD,DWORD> *processIDs);
	void _KillProcess(DWORD dwProcessId);

public:

	CIPAddressCtrl m_editIP;
	afx_msg void OnBnClickedDownload();
	afx_msg void OnBnClickedOpenFolder();
	afx_msg void OnBnClickedCancel();


	void    ClearInfo();
	void	InitList();
	void	SetList();
	void	SetList(int nRow, ContentsInfo* ele);

	int		DownloadFiles();
	int		CheckFiles();
	int		MoveFiles(std::string& prev_folder);

	void	SetIp();
	void	GetIp();
	void	GetDebug();
	void	SetContentsPath();
	void	GetContentsPath();
	
	void	GetCurrentDir();

	void	CheckHostName();
	BOOL	PWDCheck();
	BOOL	FindIP();
	int		_FindIP(CString& hostname);
	BOOL	ADBConnect();
	BOOL	PingTest();
	CString	RunADB(LPCTSTR parameter);
	int		ReadFileList();

	BOOL	EncryptDataFile(CString& inipath);
	BOOL	DecryptDataFile(CString& inipath);
	CString	Decrypt(TCHAR* ip);

	void	SplitPath(LPCTSTR input, CString& path, CString& filename);

	CString AsciiToBase64(CString strAscii);
	CString Base64ToAscii(CString strBase64);

	void OnBnClickedConnect(bool hostname_button);

	CUTBListCtrlEx m_lscTerminal;
	CImageList m_ilHostList;

	//CListCtrl m_lscTerminal;
	CString m_currentPath;
	CString m_contentsPath;
	ContentsInfoList m_infoList;
	CString m_hostname;
	CString m_ip;

	BOOL m_bRunning;
	BOOL m_connected;

	afx_msg void OnBnClickedSaveIp();
	CButton m_btDownload;
	afx_msg void OnBnClickedSettings();
	afx_msg void OnBnClickedDelContents();
	CEdit m_editHostName;

	typedef std::map<std::string,std::string>	IP_MAP;
	IP_MAP _ip_map;

	BOOL m_is_input_ip;
	BOOL m_debug;

	afx_msg void OnBnClickedInitIp();
	CStatic m_staticMsg;
	CButton m_btStopDownload;
	afx_msg void OnBnClickedDownloadStop();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
