//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UBCADBDownloader.rc
//
#define IDD_UBCADBDOWNLOADER_DIALOG     102
#define IDB_LISTCTRLEX_HEADER           129
#define IDI_ICON1                       131
#define IDB_HOST_LIST                   157
#define IDC_IPADDRESS1                  1001
#define ID_OPEN_FOLDER                  1002
#define ID_DOWNLOAD                     1003
#define IDC_LIST1                       1004
#define ID_SAVE_IP                      1005
#define ID_SETTINGS                     1006
#define ID_DEL_CONTENTS                 1007
#define IDC_EDIT_HOSTNAME               1008
#define ID_DOWNLOAD_STOP                1009
#define ID_NETWORK_TEST                 1010
#define IDC_STATIC_MSG                  1012
#define ID_INIT_IP                      1013
#define IDC_STATIC_MSG2                 1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
