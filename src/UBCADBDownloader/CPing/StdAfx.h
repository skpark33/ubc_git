#define VC_EXTRALEAN		

#include <afxwin.h>     
#include <afxext.h>     

#ifdef CPING_USE_WINSOCK2
#include <winsock2.h>
#endif
#ifdef CPING_USE_ICMP
#include <winsock.h>
#endif

#include <afxsock.h>
#include <afxdisp.h>
#include <afxpriv.h>

