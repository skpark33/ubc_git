#include "stdafx.h"

#include <ci/libBase/ciBaseType.h>
#include <ci/libBase/ciAceType.h>
#include <ci/libDebug/ciDebug.h>
#include "ciIcmpPing.h"
#include <ace/OS.h>
#if defined(_COP_GCC_)
#	include <sys/socket.h>
#endif

ciSET_DEBUG(10, ("ciIcmpPing"));

/*
ciIcmpPing::ciIcmpPing() 
{
    ciDEBUG(8, ("ciIcmpPing()"));
    
}
*/

ciIcmpPing::ciIcmpPing(int icmpType)
{
    ciDEBUG(5, ("ciIcmpPing(%d)", icmpType));
    m_Ident      = 0x00;
    m_Rrt        = 0x00;
    m_Socket     = 0x00;
	m_sent       = 0;
    //m_TimeOut    = DEF_TIMEOUT;

	char* tVal = NULL;
    tVal = getenv("PINGTIMEOUT");
    m_TimeOut = ( (tVal) ? atoi(tVal) : DEF_TIMEOUT); 
    m_IcmpType   = icmpType;
}

ciIcmpPing::~ciIcmpPing()
{
    ciDEBUG(5, ("ciIcmpPing destructor"));
    if( m_Socket != 0x00 )
	{
#ifdef WIN32
		closesocket(m_Socket);
#else
		close(m_Socket);
#endif	//WIN32
	}
}

int 
ciIcmpPing::_createSocket(int domain, int type, int protocol )
{
    int aSocket = 0;
    
    ciDEBUG(5, ("_createSocket()"));
    
    if(( aSocket = socket( domain, type, protocol )) < 0 ){
        ciERROR(("Can't create socket "));
        return -1;
    }
    ciDEBUG(5, ("socket(%d) created~!", aSocket));

    return aSocket;
}

int 
ciIcmpPing::_inCheckSum(u_short *buf, int len)
{
    register long sum = 0;
    u_short  answer = 0;

    while( len > 1 ){
        sum += *buf++;
        len -= 2;
    }

    if( len == 1 ){
        *( u_char* )( &answer ) = *( u_char* )buf;
        sum += answer;
    }
    sum = ( sum >> 16 ) + ( sum & 0xffff );
    sum += ( sum >> 16 );     
    answer = ~sum;     

    return ( answer );    
}

int
ciIcmpPing::_sendPing( const char* pHost, struct sockaddr_in *pAddr )
{
    int len;
    int ss;
    unsigned char buf[ HDRLEN + DATALEN ];
    struct hostent*   hp;
    struct icmp*      icmp;
    unsigned short    last;
    
    len = HDRLEN + DATALEN;

	memset(buf, 0x00, sizeof(buf));
    
    if(( hp = gethostbyname( pHost )) != NULL ){
        memcpy( &pAddr->sin_addr, hp->h_addr_list[0], sizeof( pAddr->sin_addr ));
        pAddr->sin_port = 0;
        pAddr->sin_family = AF_INET;
    }
    else if( inet_aton( pHost, &pAddr->sin_addr ) == 0 ) {
        ciERROR(("inet_aton error"));        
        return -1;
    }
    
    // 브로드 캐스팅 에서는 필요없는 로직이므로 모듈화해야 함.
    last = ntohl( pAddr->sin_addr.s_addr ) & 0xFF;
    if(( last == 0x00 ) || ( last == 0xFF )){
        ciERROR(("ntohl error"));        
        return -1;        
    }

    
    icmp = (struct icmp*)buf;
    icmp->icmp_type  = ICMP_ECHO; //((m_IcmpType == 0) ? ICMP_ECHO : ICMP_MASKREQ);
    icmp->icmp_code  = 0;
    icmp->icmp_cksum = 0;
    icmp->icmp_id    = getpid() & 0xFFFF;
	icmp->icmp_seq   = m_sent++;
    icmp->icmp_cksum = _inCheckSum((u_short*)icmp, len );

	ciDEBUG(5, ("+++++++++++++++++ICMP ID(%d)", icmp->icmp_id));    
    if(( ss = sendto( m_Socket, buf, sizeof( buf ), 0,
                       (struct sockaddr*)pAddr, sizeof( *pAddr ))) < 0 ){
        ciERROR(("sendto error "));
        return -2;
    }

    if( ss != len ){
        ciERROR(("malformed packet"));
        return -2;
    }

    return 0;
}

int
ciIcmpPing::_recvPing( struct sockaddr_in* pAddr )
{
    // ciDEBUG(8, ("_recvPing()"));
    
    int                response;
    int                len;
    int                from;
    int                nf, cc;
    unsigned char      buf[ HDRLEN + DATALEN ];
    struct icmp        *icmp;
    struct sockaddr_in faddr;
    struct timeval     to;
    fd_set             readset, writeset;

    to.tv_sec = m_TimeOut / 100000;
    to.tv_usec = ( m_TimeOut - ( to.tv_sec * 100000 ) ) * 10;

    FD_ZERO( &readset );
    FD_ZERO( &writeset );
    FD_SET( m_Socket, &readset );
    /* we use select to see if there is any activity
       on the sock.  If not, then we've requested an
       unreachable network and we'll time out here. */
    if(( nf = select( m_Socket + 1, &readset, &writeset, NULL, &to )) < 0 ){
        ciERROR(("error select"));
        exit( EXIT_FAILURE );
    }
    if( nf == 0 ){ 
        return -1; 
    }

    len = HDRLEN + DATALEN;
    from = sizeof( faddr ); 

	ciDEBUG(5, ("recvfrom socket(%d)", m_Socket));
#if defined(_COP_LINUX_) || defined(_COP_AIX5_)
    cc = recvfrom( m_Socket, buf, len, 0, (struct sockaddr*)&faddr, (socklen_t *)&from );
#else 
    cc = recvfrom( m_Socket, buf, len, 0, (struct sockaddr*)&faddr, &from );
#endif
    if( cc < 0 ){
        ciERROR(("recvfrom error"));
        //exit( EXIT_FAILURE );
    }

    //ciDEBUG(5, ("all of recvfrom : from(%s) cc(%d)", inet_ntoa(faddr.sin_addr), cc));
    
    icmp = (struct icmp *)(buf + HDRLEN + DATALEN );

    if( faddr.sin_addr.s_addr != pAddr->sin_addr.s_addr ){
		if( icmp->icmp_id != m_Ident) {
			ciDEBUG(5, ("It's not mine : id(%d) this(%d)", icmp->icmp_id, m_Ident));
			return 1;
		}
       // return 1;
    }
/*
	if( icmp->icmp_id != m_Ident) {
		ciDEBUG(5, ("id(%d) this(%d)", icmp->icmp_id, m_Ident));
		return 1;
	}
*/
    ciDEBUG(5, ("on success: to(%s) from(%s)", inet_ntoa(pAddr->sin_addr) , inet_ntoa(faddr.sin_addr)) );
	ciDEBUG(5, ("on success: id(%d) this(%d)", icmp->icmp_id, m_Ident));
    return 0;
}

int
ciIcmpPing::_myPing( const char *hostName, int pTime )
{
    ciDEBUG(5, ("_myPing()"));
    
    //int timeo;
    int err;
    struct sockaddr_in sockA;
    struct timeval myTime;
 
    m_Ident = getpid() & 0xFFFF;
	m_sent  = 0;

    if( pTime == 0 ) m_TimeOut = DEF_TIMEOUT; 
    else         m_TimeOut = pTime;

    (void) gettimeofday( &myTime, (struct timezone *)NULL);

//[    alter code ,because of sorba's bug(2002/12/12)
//    if( m_Socket != 0 ) close(m_Socket);

	if( m_Socket == 0 ) {

    if (int i =geteuid()) {
        ciERROR(("This program can only be run by root, or it must be setuid root.(%d)",i));
        return -3;
    }

    struct protoent *proto = getprotobyname("icmp");
    if(!proto) {
       ciERROR(("icmp: unknown protocol"));
        return -3;
    }

        //if( (m_Socket = _createSocket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0 ) {
        if( (m_Socket = _createSocket(AF_INET, SOCK_RAW, proto->p_proto)) < 0 ) {
             ciERROR(("error : socket create"));
             return m_Socket;
        }
    }
//] end of add code

    if(( err = _sendPing( hostName, &sockA )) < 0 ){
        ciERROR(("_sendPing error : result(%d)", err));
//        close( m_Socket );
        return err;
    }
    do{
        if(( m_Rrt = _elapsedTime( &myTime )) > m_TimeOut * 1000 ){
 //           close( m_Socket );
            ciDEBUG(5, ("ElapsedTime(%d)", m_Rrt));           
            return 0;
        }
    } while( _recvPing( &sockA ));

	//[    alter code ,because of sorba's bug(2002/12/12)
    //close( m_Socket ); 

    return 1;    
}

int
ciIcmpPing::ping( const char* hostName, int pTime, int retries )
{
    ciDEBUG(5, ("ping to %s with time(%d),retries(%d)", hostName, pTime, retries));

	while(1) {
		retries--;

		int retval = _myPing( hostName, pTime );
		if(retval) {
			return retval;
		}

		if(retries <= 0) {
			return retval;
		}
	}

    //return _myPing( hostName, pTime );
}


int
ciIcmpPing::rttPing( const char* hostName, int pTime)
{
    ciDEBUG(5, ("rttPing()"));
    
    int result = 0x00;
    
    if(( result = _myPing( hostName, pTime )) > 0 )
        return m_Rrt;
    else
        return result;
}

int
ciIcmpPing::_elapsedTime( struct timeval* startTime )
{
//    ciDEBUG(5, ("_elapsedTime()"));
    
    int elapsed = 0x00;
    struct timeval *newTime;

    newTime = (struct timeval*)malloc( sizeof(struct timeval));
    gettimeofday(newTime,NULL);

    if(( newTime->tv_usec - startTime->tv_usec) > 0 ){
        elapsed += (newTime->tv_usec - startTime->tv_usec)/1000 ;
    }
    else{
        elapsed += ( 1000000 + newTime->tv_usec - startTime->tv_usec ) /1000;
        newTime->tv_sec--;
    }
    if(( newTime->tv_sec - startTime->tv_sec ) > 0 ){
        elapsed += 1000 * ( newTime->tv_sec - startTime->tv_sec );
    }
    free(newTime);
    return( elapsed );    
}

void
ciIcmpPing::setTimeout(int pTimeout)
{
    m_TimeOut = pTimeout;
}
