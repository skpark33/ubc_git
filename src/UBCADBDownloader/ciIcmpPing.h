#ifndef __PINGCMD_H__
#define __PINGCMD_H__

#include <stdio.h>

#ifdef WIN32
#	include <winsock2.h>
#else
#	include <unistd.h>
#	include <ctime>
#	include <netinet/in_systm.h>
#	include <netinet/in.h>
//#	include <netinet/udp.h>
#	include <sys/socket.h>
#	include <arpa/inet.h>
#	include <netdb.h>
#	include <netinet/ip.h>
#	include <netinet/ip_icmp.h>
#endif	// WIN32

#include <cstdlib>

#define  MAXPACKET   65535
#define  PKTSIZE     64 
#ifdef WIN32
#define  HDRLEN      8
#else
#define  HDRLEN      ICMP_MINLEN
#endif // WIN32

#define  DATALEN     (PKTSIZE-HDRLEN)
#define  MAXDATA     (MAXPKT-HDRLEN-TIMLEN)
#define  DEF_TIMEOUT 2

/*
//! format of a (udp) probe packet
struct aPacket 
{
    struct ip      ip;
    struct udphdr  udp;
    u_char         seq;
    u_char         ttl;
    struct timeval tv;
};
*/

class ciIcmpPing 
{
 public:
    ciIcmpPing(int icmpType = 0);
    ~ciIcmpPing();

    //! If the host reply, return 1
    int        ping( const char* hostName, int pTime = 0, int retries = 1);
    void       setTimeout(int pTimeout);
	int        getTimeout() { return m_TimeOut; }    

    //! On Success, return round trip time(milisecond)
    int        rttPing( const char* hostName, int pTime = 0 );

    
 protected:
    int        _createSocket(int domain = AF_INET, int type = SOCK_RAW,
                             int protocol = IPPROTO_ICMP);

    int        _sendPing( const char* pHost, struct sockaddr_in *pAddr );
    int        _recvPing( struct sockaddr_in * pAddr );
    int        _elapsedTime( struct timeval* startTime );
    int        _myPing( const char* hostName, int pTime );
	int        _inCheckSum(u_short *buf, int len);
    
 private:
    int        m_Ident;
    int        m_TimeOut;
    int        m_Rrt;
    int        m_Socket;
    int        m_IcmpType;
	int        m_sent;
    
};

#endif //__PINGCMD_H__
