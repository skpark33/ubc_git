package ubc.app.UBCAnnouncementLauncher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;

public class UBCAnnouncementLauncher extends Activity implements DialogInterface.OnClickListener {
	WebView browser;
	private Menu theMenu = null;	// 하단 메뉴 객체
	private static EditText editText;	// 대화창의 ip입력 텍스트박스
	final String filename = "ipAddress.txt";	// 서버ip값 저장 파일이름 
	
	private static String ipAddress = "211.232.57.218";		// 서버ip

	// 하단 메뉴
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		theMenu = menu;
		new MenuInflater(getApplication()).inflate(R.menu.menu, menu);
		return (super.onCreateOptionsMenu(menu));
	}
	
	// 서버ip입력 대화창 표시
	public static void showTextDialog(final Activity activity, String title, String text, String hint, DialogInterface.OnClickListener listener) { 
		editText = new EditText(activity); 
		editText.setText(text); 
		editText.setHint(hint); 
		AlertDialog.Builder ad = new AlertDialog.Builder(activity); 
		ad.setTitle(title); 
		ad.setView(editText); 
		ad.setPositiveButton("OK", listener); 
		ad.show();
	} 
	
	// 하단 메뉴에서 서버ip설정 클릭시
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.no_icon) {
			showTextDialog(this, "서버 IP",ipAddress," 여기에 서버IP를 입력", this);
			return (true);
		}
		return (super.onOptionsItemSelected(item));
	}
	
	// 대화창에서 ok확인시
	public void onClick(DialogInterface dialog, int which){ 
		ipAddress = editText.getText().toString();
		
		// ipaddress.txt 파일에 설정값 저장
		PrintWriter writer;
		try
		{
			writer = new PrintWriter(openFileOutput(filename, MODE_PRIVATE));
			writer.printf(ipAddress);
			writer.close();
		} catch(FileNotFoundException e) {
		}

		// 서버 접속 메시지 표시
		Toast.makeText(this, "Connecting to " + ipAddress, Toast.LENGTH_LONG).show();

		// 서버 접속 스레드 생성
        GetHttpSrc thread = new GetHttpSrc();
        thread.parent = this;
        thread.execute("Test");
	} 
	
	// 서버 접속 스레드
	class GetHttpSrc extends AsyncTask<String, Void, Drawable> {
	    @Override
		protected Drawable doInBackground(String... params) {
	    	// 잠시 대기
	    	try {
		    	Thread.sleep(1000);
	    	} catch (InterruptedException e)
	    	{
	    	}
	    	
	    	// 서버 연결 테스트
	    	Drawable result = null;
	    	try {
/*
				// 좀더 테스트 필요
				// 서버로부터 웹페이지 소스를 get
 	    		HttpClient httpclient = new DefaultHttpClient();
	    		String ipaddr = "http://" + ipAddress + ":8090/ubcmini/";
	    		HttpGet httpget = new HttpGet(ipaddr);
	    		HttpResponse response = httpclient.execute(httpget);
	    		HttpEntity entity = response.getEntity();
	    		if(entity != null)
	    		{
	    			// 웹페이지 소스 가져옴
	    			InputStream instream = entity.getContent();
	    			int i;
	    			String text = "";
	    			byte[] buf = new byte[2048];
	    			byte[] tmp = new byte[2048];

	    			Arrays.fill(buf, (byte)0);
	    			Arrays.fill(tmp, (byte)0);
	    			boolean first = true;
	    			while((i=instream.read(tmp)) != -1)
	    			{
	    				if(first == true)
	    				{
	    					// utf-8 헤더 제거
	    					for(int x=0; x<i; x++)
	    						buf[x] = tmp[x+3];
		    				text += new String(buf);
		    				first = false;
	    				}
	    				else
	    				{
	    					// utf-8 헤더후
	    					for(int x=0; x<i; x++)
	    						buf[x] = tmp[x];
		    				text += new String(buf);
	    				}
		    			Arrays.fill(buf, (byte)0);
		    			Arrays.fill(tmp, (byte)0);
	    			}
	    			
	    			CharSequence seq1 = "%";
	    			CharSequence seq2 = "&#37;";
	    			
	    			String text2 = text.replace(seq1, seq2);
	    			System.out.println(text2);

	    			// 로그인 페이지인지 확인
	    			if(text2.indexOf("login") < 0)
		    			parent.MoveNotConnPage();
	    			else
	    				parent.MoveMainpage();
	    		}
	    		else
	    		{
	    			parent.MoveNotConnPage();
	    		}
*/	    		
				parent.MoveMainpage();
	    	}
	    	catch(final Exception e)
	    	{
    			parent.MoveErrorPage();
	    	}
	    	return result;
	    }
	    public UBCAnnouncementLauncher parent;
	}
	
	// ipaddress.txt 파일로부터 서버ip값 가져옴
	void loadFromStream(InputStream is)
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		try {
			ipAddress = reader.readLine();
			//Toast.makeText(this, ipAddress, Toast.LENGTH_LONG).show();
			reader.close();
		} catch(IOException e) {
		}
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // ipaddress.txt로부터 서버ip값 가져옴
        try {
        	loadFromStream(openFileInput(filename));
        } catch(FileNotFoundException e) {
        }

        // 웹뷰 설정
        browser = (WebView)findViewById(R.id.webkit);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setCacheMode(2);
        
        // 초기화면(Now Connecting...) 보여줌
        String text ="<html><body><table width='100&#37;' height='100&#37;'><tr><td><center>Now Connecting...</center></td></tr></table></body></html>"; 
        browser.loadData(text, "text/html", "utf-8");
        
        // 서버 접속 스레드 생성
        GetHttpSrc thread = new GetHttpSrc();
        thread.parent = this;
        thread.execute("Test");
    }

    // 로그인 페이지로 이동
    public void MoveMainpage()
    {
		//Toast.makeText(this, "http://" + ipAddress + ":8090/ubcmini/", Toast.LENGTH_LONG).show();
        browser.loadUrl("http://" + ipAddress + ":8090/ubcmini/");
    }
    
    // 연결 안될시 에러메시지 표시
    public void MoveNotConnPage()
    {
        String text ="<html><body><table width='100&#37;' height='100&#37;'><tr><td><center>Can't Connect to server.<br>Check Network Settings or Server IP Address.</center></td></tr></table></body></html>"; 
        browser.loadData(text, "text/html", "utf-8");
    }

    // 에러 발생시 에러메시지 표시
    public void MoveErrorPage()
    {
        String text ="<html><body><table width='100&#37;' height='100&#37;'><tr><td><center>ERROR !!!</center></td></tr></table></body></html>"; 
        browser.loadData(text, "text/html", "utf-8");
    }
}
