CREATE VIEW dbo.ubc_defaultscheduleview
AS
SELECT     d.mgrId, d.siteId, d.programId, d.templateId, d.frameId, d.scheduleId, d.requestId, d.contentsId, d.startDate, d.endDate, d.playOrder, d.openningFlag, 
                      d.timeScope, d.priority, d.castingState, d.castingStateTime, d.castingStateHistory, d.result, d.operationalState, d.adminState, d.parentScheduleId, 
                      d.touchTime, d.phoneNumber, d.serialNo, c.contentsName, c.location, c.filename, c.contentsType, c.contentsState, c.volume, d.runningTime, c.bgColor, 
                      c.fgColor, c.font, c.fontSize, c.playSpeed, c.soundVolume, c.comment1, c.comment2, c.comment3, c.comment4, c.comment5, c.comment6, c.comment7, 
                      c.comment8, c.comment9, c.comment10, c.promotionValueList, c.direction, c.align, c.isUsed, c.width, c.height, c.currentComment, c.resolution, 
                      c.requester, c.verifyMembers, c.verifyTime, c.registerType, c.contentsCategory, c.purpose, c.hostType, c.vertical, 
                      c.validationDate AS validationDat
FROM         dbo.ubc_defaultschedule AS d LEFT OUTER JOIN
                      dbo.ubc_contents AS c ON d.mgrId = c.mgrId AND d.siteId = c.siteId AND d.programId = c.programId AND d.contentsId = c.contentsId
;


CREATE VIEW dbo.ubc_hostView
AS 
SELECT h.*, s.siteName,s.chainNo,s.businessCode,s.businessType, s.phoneNo1 
from (dbo.ubc_host h left join dbo.ubc_site s on((h.siteId = s.siteId)));


CREATE VIEW dbo.ubc_nodeView AS SELECT
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.hostType as objectType,
d.hostName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.soundVolume as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_host d
WHERE n.objectClass = 'Host' and  n.objectId = d.hostId
UNION SELECT 
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.monitorType as objectType,
d.monitorName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.monitorUseTime as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_monitor d
WHERE n.objectClass = 'Monitor' and  n.objectId = d.monitorId
UNION SELECT 
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.lightType as objectType,
d.lightName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.brightness as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_light d
WHERE n.objectClass = 'Light' and  n.objectId = d.lightId;


CREATE VIEW dbo.ubc_reservationview
AS
SELECT     h.mgrId, h.siteId, h.hostId, h.displayCounter, h.currentSchedule1, h.currentSchedule2, h.autoSchedule1, h.autoSchedule2, h.lastSchedule1, 
                      h.lastSchedule2, h.lastScheduleTime1, h.lastScheduleTime2, h.networkUse1, h.networkUse2, h.scheduleApplied1, h.scheduleApplied2, r.reservationId, 
                      r.programId AS programid, r.side, r.fromTime, r.toTime
FROM         dbo.ubc_reservation AS r LEFT OUTER JOIN
                      dbo.ubc_host AS h ON r.mgrId = h.mgrId AND r.siteId = h.siteId AND r.hostId = h.hostId;

CREATE VIEW dbo.ubc_scheduleview
AS
SELECT     d.mgrId, d.siteId, d.programId, d.templateId, d.frameId, d.scheduleId, d.requestId, d.contentsId, d.startDate, d.endDate, d.startTime, d.endTime, 
                      d.toTime, d.fromTime, d.openningFlag, d.priority, d.castingState, d.castingStateTime, d.castingStateHistory, d.result, d.operationalState, d.adminState, 
                      d.phoneNumber, d.isOrigin, d.serialNo, c.contentsName, c.location, c.filename, c.contentsType, c.contentsState, c.volume, d.runningTime, c.bgColor, 
                      c.fgColor, c.font, c.fontSize, c.playSpeed, c.soundVolume, c.comment1, c.comment2, c.comment3, c.comment4, c.comment6, c.comment5, c.comment7, 
                      c.comment8, c.comment9, c.comment10, c.promotionValueList, c.direction, c.align, c.isUsed, c.width, c.height, c.currentComment, c.resolution, 
                      c.requester, c.verifyMembers, c.verifyTime, c.registerType, c.contentsCategory, c.purpose, c.hostType, c.vertical, c.validationDate
FROM         dbo.ubc_schedule AS d LEFT OUTER JOIN
                      dbo.ubc_contents AS c ON d.mgrId = c.mgrId AND d.siteId = c.siteId AND d.programId = c.programId AND d.contentsId = c.contentsId;


