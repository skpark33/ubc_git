-- remove user
exec sp_revokedbaccess 'ubc' 
go 
exec sp_droplogin 'ubc' 
go

-- create user
exec sp_addlogin 'ubc', 'sqicop'  
go 
use ubc; 
exec sp_grantdbaccess 'ubc' 
exec sp_addrolemember 'db_owner', 'ubc'  -- db_owner role �ο� 
go 

USE [ubc]
GO
/****** Object:  User [ubc]    Script Date: 07/24/2012 19:50:42 ******/
CREATE USER [ubc] FOR LOGIN [ubc] WITH DEFAULT_SCHEMA=[dbo]
GO
