/****** Object:  Default [DF__em_banlis__send___503BEA1C]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_banlist] ADD  DEFAULT ('N') FOR [send_yn]
GO
/****** Object:  Default [DF__em_banlis__ban_s__51300E55]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_banlist] ADD  DEFAULT ('Y') FOR [ban_status_yn]
GO
/****** Object:  Default [DF__em_banlis__reg_d__5224328E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_banlist] ADD  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF__em_banlis__updat__531856C7]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_banlist] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [update_date]
GO
/****** Object:  Default [DF__em_mmt_cl__msg_s__681373AD]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_client] ADD  DEFAULT ('1') FOR [msg_status]
GO
/****** Object:  Default [DF__em_mmt_cl__count__690797E6]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_client] ADD  DEFAULT ('82') FOR [country_code]
GO
/****** Object:  Default [DF__em_mmt_fi__reg_d__6DCC4D03]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_file] ADD  CONSTRAINT [DF__em_mmt_fi__reg_d__6DCC4D03]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF__em_mmt_tr__prior__5BAD9CC8]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('S') FOR [priority]
GO
/****** Object:  Default [DF__em_mmt_tr__msg_c__5CA1C101]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('1') FOR [msg_class]
GO
/****** Object:  Default [DF__em_mmt_tr__date___5D95E53A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [date_client_req]
GO
/****** Object:  Default [DF__em_mmt_tr__conte__5E8A0973]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ((0)) FOR [content_type]
GO
/****** Object:  Default [DF__em_mmt_tr__attac__5F7E2DAC]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ((0)) FOR [attach_file_group_key]
GO
/****** Object:  Default [DF__em_mmt_tr__broad__607251E5]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('N') FOR [broadcast_yn]
GO
/****** Object:  Default [DF__em_mmt_tr__msg_s__6166761E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('1') FOR [msg_status]
GO
/****** Object:  Default [DF__em_mmt_tr__count__625A9A57]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('82') FOR [country_code]
GO
/****** Object:  Default [DF__em_mmt_tr__crypt__634EBE90]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('Y') FOR [crypto_yn]
GO
/****** Object:  Default [DF__em_smt_cl__msg_s__16CE6296]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_client] ADD  DEFAULT ('1') FOR [msg_status]
GO
/****** Object:  Default [DF__em_smt_cl__count__17C286CF]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_client] ADD  DEFAULT ('82') FOR [country_code]
GO
/****** Object:  Default [DF__em_smt_tr__prior__0D44F85C]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('S') FOR [priority]
GO
/****** Object:  Default [DF__em_smt_tr__date___0E391C95]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [date_client_req]
GO
/****** Object:  Default [DF__em_smt_tr__broad__0F2D40CE]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('N') FOR [broadcast_yn]
GO
/****** Object:  Default [DF__em_smt_tr__msg_s__10216507]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('1') FOR [msg_status]
GO
/****** Object:  Default [DF__em_smt_tr__count__11158940]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('82') FOR [country_code]
GO
/****** Object:  Default [DF__em_smt_tr__crypt__1209AD79]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('Y') FOR [crypto_yn]
GO
/****** Object:  Default [DF__em_statis__stat___338A9CD5]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_statistics_d] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [stat_regdate]
GO
/****** Object:  Default [DF__em_statis__stat___2EC5E7B8]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_statistics_m] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [stat_regdate]
GO
/****** Object:  Default [DF__em_status__reg_d__2A01329B]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_status] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [reg_date]
GO
/****** Object:  Default [DF_ts_cronjob_operationalStatus]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ts_cronjob] ADD  CONSTRAINT [DF_ts_cronjob_operationalStatus]  DEFAULT ((1)) FOR [operationalStatus]
GO
/****** Object:  Default [DF_ubc_announce_playSpeed]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_announce] ADD  CONSTRAINT [DF_ubc_announce_playSpeed]  DEFAULT ((1000)) FOR [playSpeed]
GO
/****** Object:  Default [DF_ubc_bp_zorder]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_bp] ADD  CONSTRAINT [DF_ubc_bp_zorder]  DEFAULT ('0') FOR [zorder]
GO
/****** Object:  Default [DF_ubc_bp_state]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_bp] ADD  CONSTRAINT [DF_ubc_bp_state]  DEFAULT ((0)) FOR [state]
GO
/****** Object:  Default [DF_ubc_bp_retryCount]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_bp] ADD  CONSTRAINT [DF_ubc_bp_retryCount]  DEFAULT ((0)) FOR [retryCount]
GO
/****** Object:  Default [DF_ubc_bp_retryGap]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_bp] ADD  CONSTRAINT [DF_ubc_bp_retryGap]  DEFAULT ((0)) FOR [retryGap]
GO
/****** Object:  Default [DF_ubc_cframe_grade]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_grade]  DEFAULT ((2)) FOR [grade]
GO
/****** Object:  Default [DF_ubc_cframe_isPIP]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_isPIP]  DEFAULT ((0)) FOR [isPIP]
GO
/****** Object:  Default [DF_ubc_cframe_borderStyle]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_borderStyle]  DEFAULT ('solid') FOR [borderStyle]
GO
/****** Object:  Default [DF_ubc_cframe_borderThickness]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_borderThickness]  DEFAULT ((0)) FOR [borderThickness]
GO
/****** Object:  Default [DF_ubc_cframe_borderColor]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_borderColor]  DEFAULT ('#FFFFFF') FOR [borderColor]
GO
/****** Object:  Default [DF_ubc_cframe_cornerRadius]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_cornerRadius]  DEFAULT ((0)) FOR [cornerRadius]
GO
/****** Object:  Default [DF_ubc_cframe_alpha]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_alpha]  DEFAULT ((0)) FOR [alpha]
GO
/****** Object:  Default [DF_ubc_cframe_isTV]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_isTV]  DEFAULT ((0)) FOR [isTV]
GO
/****** Object:  Default [DF_ubc_code_dorder]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_code] ADD  CONSTRAINT [DF_ubc_code_dorder]  DEFAULT ((0)) FOR [dorder]
GO
/****** Object:  Default [DF_ubc_code_visible]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_code] ADD  CONSTRAINT [DF_ubc_code_visible]  DEFAULT ((1)) FOR [visible]
GO
/****** Object:  Default [DF_ubc_connectionstatelog_operationalState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_connectionstatelog] ADD  CONSTRAINT [DF_ubc_connectionstatelog_operationalState]  DEFAULT ((0)) FOR [operationalState]
GO
/****** Object:  Default [DF_ubc_contents_isRaw]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_isRaw]  DEFAULT ((0)) FOR [isRaw]
GO
/****** Object:  Default [DF_ubc_contents_contentsState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_contentsState]  DEFAULT ((0)) FOR [contentsState]
GO
/****** Object:  Default [DF_ubc_contents_runningTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_runningTime]  DEFAULT ((10)) FOR [runningTime]
GO
/****** Object:  Default [DF_ubc_contents_currentComment]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_currentComment]  DEFAULT ((1)) FOR [currentComment]
GO
/****** Object:  Default [DF_ubc_contents_playSpeed]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_playSpeed]  DEFAULT ((1000)) FOR [playSpeed]
GO
/****** Object:  Default [DF_ubc_contents_soundVolume]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_soundVolume]  DEFAULT ((50)) FOR [soundVolume]
GO
/****** Object:  Default [DF_ubc_contents_isPublic]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_isPublic]  DEFAULT ((0)) FOR [isPublic]
GO
/****** Object:  Default [DF_ubc_contents_tvInputType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_tvInputType]  DEFAULT ((0)) FOR [tvInputType]
GO
/****** Object:  Default [DF_ubc_contents_direction]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_direction]  DEFAULT ((0)) FOR [direction]
GO
/****** Object:  Default [DF_ubc_contents_align]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_align]  DEFAULT ((5)) FOR [align]
GO
/****** Object:  Default [DF_ubc_contents_isUsed]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_isUsed]  DEFAULT ((1)) FOR [isUsed]
GO
/****** Object:  Default [DF_ubc_contents_contentsCategory]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_contentsCategory]  DEFAULT ((0)) FOR [contentsCategory]
GO
/****** Object:  Default [DF_ubc_contents_purpose]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_purpose]  DEFAULT ((0)) FOR [purpose]
GO
/****** Object:  Default [DF_ubc_contents_hostType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_hostType]  DEFAULT ((0)) FOR [hostType]
GO
/****** Object:  Default [DF_ubc_contents_vertical]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_vertical]  DEFAULT ((0)) FOR [vertical]
GO
/****** Object:  Default [DF_ubc_contents_resolution]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_resolution]  DEFAULT ((0)) FOR [resolution]
GO
/****** Object:  Default [DF_ubc_contents_adminState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_adminState]  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF_ubc_ctemplate_bgColor]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_ctemplate] ADD  CONSTRAINT [DF_ubc_ctemplate_bgColor]  DEFAULT ('#c0c0c0') FOR [bgColor]
GO
/****** Object:  Default [DF__ubc_custome__gmt__4999D985]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_customer] ADD  DEFAULT ((9999)) FOR [gmt]
GO
/****** Object:  Default [DF_ubc_defaultschedule_openningFlag]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_openningFlag]  DEFAULT ((1)) FOR [openningFlag]
GO
/****** Object:  Default [DF_ubc_defaultschedule_timeScope]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_timeScope]  DEFAULT ((0)) FOR [timeScope]
GO
/****** Object:  Default [DF_ubc_defaultschedule_priority]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_priority]  DEFAULT ((0)) FOR [priority]
GO
/****** Object:  Default [DF_ubc_defaultschedule_operationalState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_operationalState]  DEFAULT ((1)) FOR [operationalState]
GO
/****** Object:  Default [DF_ubc_defaultschedule_adminState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_adminState]  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF_ubc_defaultschedule_runningTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_runningTime]  DEFAULT ((0)) FOR [runningTime]
GO
/****** Object:  Default [DF_ubc_downloadstate_brwId]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_brwId]  DEFAULT ((0)) FOR [brwId]
GO
/****** Object:  Default [DF_ubc_downloadstate_result]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_result]  DEFAULT ((0)) FOR [result]
GO
/****** Object:  Default [DF_ubc_downloadstate_downState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_downState]  DEFAULT ((0)) FOR [downState]
GO
/****** Object:  Default [DF_ubc_downloadstate_programState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_programState]  DEFAULT ((0)) FOR [programState]
GO
/****** Object:  Default [DF_ubc_downloadstate_progress]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_progress]  DEFAULT ('0/0') FOR [progress]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_brwId]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_brwId]  DEFAULT ((0)) FOR [brwId]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_result]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_result]  DEFAULT ((0)) FOR [result]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_downState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_downState]  DEFAULT ((0)) FOR [downState]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_programState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_programState]  DEFAULT ((0)) FOR [programState]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_progress]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_progress]  DEFAULT ('0/0') FOR [progress]
GO
/****** Object:  Default [DF__ubc_downl__brwId__45A94D10]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstateSummary] ADD  DEFAULT ((0)) FOR [brwId]
GO
/****** Object:  Default [DF__ubc_downl__progr__469D7149]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstateSummary] ADD  DEFAULT ((0)) FOR [programState]
GO
/****** Object:  Default [DF__ubc_downl__progr__47919582]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstateSummary] ADD  DEFAULT ('0/0') FOR [progress]
GO
/****** Object:  Default [DF_ubc_fault_counter]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_fault] ADD  CONSTRAINT [DF_ubc_fault_counter]  DEFAULT ((1)) FOR [counter]
GO
/****** Object:  Default [DF_ubc_faultlog_counter]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_faultlog] ADD  CONSTRAINT [DF_ubc_faultlog_counter]  DEFAULT ((1)) FOR [counter]
GO
/****** Object:  Default [DF_ubc_fmo_duration]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_fmo] ADD  CONSTRAINT [DF_ubc_fmo_duration]  DEFAULT ((7)) FOR [duration]
GO
/****** Object:  Default [DF_ubc_frame_grade]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_grade]  DEFAULT ((2)) FOR [grade]
GO
/****** Object:  Default [DF_ubc_frame_isPIP]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_isPIP]  DEFAULT ((0)) FOR [isPIP]
GO
/****** Object:  Default [DF_ubc_frame_borderStyle]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_borderStyle]  DEFAULT ('solid') FOR [borderStyle]
GO
/****** Object:  Default [DF_ubc_frame_borderThickness]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_borderThickness]  DEFAULT ((0)) FOR [borderThickness]
GO
/****** Object:  Default [DF_ubc_frame_borderColor]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_borderColor]  DEFAULT ('#FFFFFF') FOR [borderColor]
GO
/****** Object:  Default [DF_ubc_frame_cornerRadius]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_cornerRadius]  DEFAULT ((0)) FOR [cornerRadius]
GO
/****** Object:  Default [DF_ubc_frame_zindex]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_zindex]  DEFAULT ((0)) FOR [zindex]
GO
/****** Object:  Default [DF_ubc_frame_alpha]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_alpha]  DEFAULT ((0)) FOR [alpha]
GO
/****** Object:  Default [DF_ubc_frame_isTV]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_isTV]  DEFAULT ((0)) FOR [isTV]
GO
/****** Object:  Default [DF__ubc_host__period__2744C181]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((60)) FOR [period]
GO
/****** Object:  Default [DF__ubc_host__adminS__2838E5BA]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF__ubc_host__operat__292D09F3]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [operationalState]
GO
/****** Object:  Default [DF__ubc_host__autoPo__2A212E2C]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [autoPowerFlag]
GO
/****** Object:  Default [DF__ubc_host__powerC__2B155265]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [powerControl]
GO
/****** Object:  Default [DF__ubc_host__cpuUse__2C09769E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0.00)) FOR [cpuUsed]
GO
/****** Object:  Default [DF__ubc_host__screen__2CFD9AD7]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((5)) FOR [screenshotPeriod]
GO
/****** Object:  Default [DF__ubc_host__displa__2DF1BF10]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [displayCounter]
GO
/****** Object:  Default [DF__ubc_host__networ__2EE5E349]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [networkUse1]
GO
/****** Object:  Default [DF__ubc_host__networ__2FDA0782]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [networkUse2]
GO
/****** Object:  Default [DF__ubc_host__schedu__30CE2BBB]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [scheduleApplied1]
GO
/****** Object:  Default [DF__ubc_host__schedu__31C24FF4]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [scheduleApplied2]
GO
/****** Object:  Default [DF__ubc_host__soundD__32B6742D]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [soundDisplay]
GO
/****** Object:  Default [DF__ubc_host__vncSta__33AA9866]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [vncState]
GO
/****** Object:  Default [DF__ubc_host__cpuTem__349EBC9F]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [cpuTemp]
GO
/****** Object:  Default [DF__ubc_host__create__3592E0D8]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [createTime]
GO
/****** Object:  Default [DF__ubc_host__autoUp__36870511]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [autoUpdateFlag]
GO
/****** Object:  Default [DF__ubc_host__playLo__377B294A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((15)) FOR [playLogDayLimit]
GO
/****** Object:  Default [DF__ubc_host__hddThr__386F4D83]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((90)) FOR [hddThreshold]
GO
/****** Object:  Default [DF__ubc_host__playLo__396371BC]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('00:00') FOR [playLogDayCriteria]
GO
/****** Object:  Default [DF__ubc_host__render__3A5795F5]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [renderMode]
GO
/****** Object:  Default [DF__ubc_host__starte__3B4BBA2E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [starterState]
GO
/****** Object:  Default [DF__ubc_host__browse__3C3FDE67]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [browserState]
GO
/****** Object:  Default [DF__ubc_host__browse__3D3402A0]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [browser1State]
GO
/****** Object:  Default [DF__ubc_host__firmwa__3E2826D9]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [firmwareViewState]
GO
/****** Object:  Default [DF__ubc_host__preDow__3F1C4B12]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [preDownloaderState]
GO
/****** Object:  Default [DF__ubc_host__downlo__40106F4B]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [download1State]
GO
/****** Object:  Default [DF__ubc_host__downlo__41049384]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [download2State]
GO
/****** Object:  Default [DF__ubc_host__progre__41F8B7BD]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('0/0') FOR [progress1]
GO
/****** Object:  Default [DF__ubc_host__progre__42ECDBF6]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('0/0') FOR [progress2]
GO
/****** Object:  Default [DF__ubc_host__monito__43E1002F]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [monitoroff]
GO
/****** Object:  Default [DF__ubc_host__hostTy__44D52468]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [hostType]
GO
/****** Object:  Default [DF__ubc_host__monito__45C948A1]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((-1)) FOR [monitorState]
GO
/****** Object:  Default [DF__ubc_host__monito__46BD6CDA]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((-1)) FOR [monitorUseTime]
GO
/****** Object:  Default [DF__ubc_host__gmt__47B19113]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  CONSTRAINT [DF__ubc_host__gmt__47B19113]  DEFAULT ((9999)) FOR [gmt]
GO
/****** Object:  Default [DF__ubc_host__disk1A__6CE315C2]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((-1)) FOR [disk1Avail]
GO
/****** Object:  Default [DF__ubc_host__disk2A__6DD739FB]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((-1)) FOR [disk2Avail]
GO
/****** Object:  Default [DF__ubc_host__protoc__1308BEAA]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('C') FOR [protocolType]
GO
/****** Object:  Default [DF__ubc_host___perio__161A357F]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((60)) FOR [period]
GO
/****** Object:  Default [DF__ubc_host___admin__170E59B8]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF__ubc_host___opera__18027DF1]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [operationalState]
GO
/****** Object:  Default [DF__ubc_host___autoP__18F6A22A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((0)) FOR [autoPowerFlag]
GO
/****** Object:  Default [DF__ubc_host___power__19EAC663]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((0)) FOR [powerControl]
GO
/****** Object:  Default [DF__ubc_host___scree__1ADEEA9C]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((5)) FOR [screenshotPeriod]
GO
/****** Object:  Default [DF__ubc_host___displ__1BD30ED5]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [displayCounter]
GO
/****** Object:  Default [DF__ubc_host___netwo__1CC7330E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [networkUse1]
GO
/****** Object:  Default [DF__ubc_host___netwo__1DBB5747]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [networkUse2]
GO
/****** Object:  Default [DF__ubc_host___sched__1EAF7B80]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [scheduleApplied1]
GO
/****** Object:  Default [DF__ubc_host___sched__1FA39FB9]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [scheduleApplied2]
GO
/****** Object:  Default [DF__ubc_host___sound__2097C3F2]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [soundDisplay]
GO
/****** Object:  Default [DF__ubc_host___vncSt__218BE82B]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((0)) FOR [vncState]
GO
/****** Object:  Default [DF__ubc_host___cpuTe__22800C64]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((0)) FOR [cpuTemp]
GO
/****** Object:  Default [DF__ubc_inter__keywo__270FB757]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_interactivelog] ADD  DEFAULT (NULL) FOR [keyword1]
GO
/****** Object:  Default [DF__ubc_inter__keywo__2803DB90]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_interactivelog] ADD  DEFAULT (NULL) FOR [keyword2]
GO
/****** Object:  Default [DF__ubc_inter__keywo__28F7FFC9]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_interactivelog] ADD  DEFAULT (NULL) FOR [keyword3]
GO
/****** Object:  Default [DF__ubc_inter__count__29EC2402]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_interactivelog] ADD  DEFAULT ('0') FOR [counter]
GO
/****** Object:  Default [DF_ubc_lmo_duration]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_duration]  DEFAULT ((3)) FOR [duration]
GO
/****** Object:  Default [DF_ubc_lmo_powerState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_powerState]  DEFAULT ((0)) FOR [powerState]
GO
/****** Object:  Default [DF_ubc_lmo_connectionState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_connectionState]  DEFAULT ((0)) FOR [connectionState]
GO
/****** Object:  Default [DF_ubc_lmo_scheduleState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_scheduleState]  DEFAULT ((0)) FOR [scheduleState]
GO
/****** Object:  Default [DF_ubc_lmo_faultState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_faultState]  DEFAULT ((0)) FOR [faultState]
GO
/****** Object:  Default [DF_ubc_lmo_loginState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_loginState]  DEFAULT ((0)) FOR [loginState]
GO
/****** Object:  Default [DF_ubc_lmo_estimateState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_estimateState]  DEFAULT ((0)) FOR [estimateState]
GO
/****** Object:  Default [DF_ubc_lmo_downloadState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_downloadState]  DEFAULT ((0)) FOR [downloadState]
GO
/****** Object:  Default [DF__ubc_lmo_s__mgrId__12B3B8EF]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo_solution] ADD  DEFAULT ('') FOR [mgrId]
GO
/****** Object:  Default [DF__ubc_lmo_s__logTy__13A7DD28]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo_solution] ADD  DEFAULT ('0') FOR [logType]
GO
/****** Object:  Default [DF__ubc_lmo_s__durat__149C0161]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo_solution] ADD  DEFAULT ('3') FOR [duration]
GO
/****** Object:  Default [DF__ubc_lmo_s__admin__1590259A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo_solution] ADD  DEFAULT ('0') FOR [adminState]
GO
/****** Object:  Default [DF_ubc_menuauth_menuType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_menuType]  DEFAULT ('S') FOR [menuType]
GO
/****** Object:  Default [DF_ubc_menuauth_cAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_cAuth]  DEFAULT ((0)) FOR [cAuth]
GO
/****** Object:  Default [DF_ubc_menuauth_rAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_rAuth]  DEFAULT ((0)) FOR [rAuth]
GO
/****** Object:  Default [DF_ubc_menuauth_uAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_uAuth]  DEFAULT ((0)) FOR [uAuth]
GO
/****** Object:  Default [DF_ubc_menuauth_dAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_dAuth]  DEFAULT ((0)) FOR [dAuth]
GO
/****** Object:  Default [DF_ubc_menuauth_aAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_aAuth]  DEFAULT ((0)) FOR [aAuth]
GO
/****** Object:  Default [DF__ubc_mir__indexOr__3805392F]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_mir] ADD  DEFAULT ((-1)) FOR [indexOrder]
GO
/****** Object:  Default [DF_ubc_playlog_playCount]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_playlog] ADD  CONSTRAINT [DF_ubc_playlog_playCount]  DEFAULT ((0)) FOR [playCount]
GO
/****** Object:  Default [DF_ubc_playlog_playTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_playlog] ADD  CONSTRAINT [DF_ubc_playlog_playTime]  DEFAULT ((0)) FOR [playTime]
GO
/****** Object:  Default [DF_ubc_playlog_planTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_playlog] ADD  CONSTRAINT [DF_ubc_playlog_planTime]  DEFAULT ((0)) FOR [planTime]
GO
/****** Object:  Default [DF_ubc_playlog_failCount]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_playlog] ADD  CONSTRAINT [DF_ubc_playlog_failCount]  DEFAULT ((0)) FOR [failCount]
GO
/****** Object:  Default [DF__ubc_pmo__ftpPort__75235608]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_pmo] ADD  DEFAULT ((21)) FOR [ftpPort]
GO
/****** Object:  Default [DF__ubc_pmo__current__76177A41]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_pmo] ADD  DEFAULT ((-1)) FOR [currentConnectionCount]
GO
/****** Object:  Default [DF__ubc_pmo__lastUpd__770B9E7A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_pmo] ADD  DEFAULT (getdate()) FOR [lastUpdateTime]
GO
/****** Object:  Default [DF__ubc_pmo__adminst__77FFC2B3]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_pmo] ADD  DEFAULT ((1)) FOR [adminstate]
GO
/****** Object:  Default [DF_ubc_program_contentsCategory]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_contentsCategory]  DEFAULT ((0)) FOR [contentsCategory]
GO
/****** Object:  Default [DF_ubc_program_purpose]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_purpose]  DEFAULT ((0)) FOR [purpose]
GO
/****** Object:  Default [DF_ubc_program_hostType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_hostType]  DEFAULT ((0)) FOR [hostType]
GO
/****** Object:  Default [DF_ubc_program_vertical]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_vertical]  DEFAULT ((0)) FOR [vertical]
GO
/****** Object:  Default [DF_ubc_program_resolution]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_resolution]  DEFAULT ((0)) FOR [resolution]
GO
/****** Object:  Default [DF_ubc_program_isPublic]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_isPublic]  DEFAULT ((0)) FOR [isPublic]
GO
/****** Object:  Default [DF_ubc_program_isVerify]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_isVerify]  DEFAULT ((1)) FOR [isVerify]
GO
/****** Object:  Default [DF__ubc_progr__volum__7484378A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  DEFAULT ((0)) FOR [volume]
GO
/****** Object:  Default [DF_ubc_rolemap_menuType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_rolemap] ADD  CONSTRAINT [DF_ubc_rolemap_menuType]  DEFAULT ('S') FOR [menuType]
GO
/****** Object:  Default [DF_ubc_schedule_openningFlag]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_openningFlag]  DEFAULT ((1)) FOR [openningFlag]
GO
/****** Object:  Default [DF_ubc_schedule_priority]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_priority]  DEFAULT ((0)) FOR [priority]
GO
/****** Object:  Default [DF_ubc_schedule_castingState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_castingState]  DEFAULT ((0)) FOR [castingState]
GO
/****** Object:  Default [DF_ubc_schedule_operationalState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_operationalState]  DEFAULT ((1)) FOR [operationalState]
GO
/****** Object:  Default [DF_ubc_schedule_adminState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_adminState]  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF_ubc_schedule_isOrigin]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_isOrigin]  DEFAULT ((1)) FOR [isOrigin]
GO
/****** Object:  Default [DF_ubc_schedule_runningTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_runningTime]  DEFAULT ((0)) FOR [runningTime]
GO
/****** Object:  Default [DF_ubc_smtplog_wdate]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_smtplog] ADD  CONSTRAINT [DF_ubc_smtplog_wdate]  DEFAULT (getdate()) FOR [wdate]
GO
/****** Object:  Default [DF_ubc_template_bgColor]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_template] ADD  CONSTRAINT [DF_ubc_template_bgColor]  DEFAULT ('#c0c0c0') FOR [bgColor]
GO
/****** Object:  Default [DF__ubc_test__monito__0C90CB45]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_test] ADD  DEFAULT ((-1)) FOR [monitorState]
GO
/****** Object:  Default [DF_ubc_th_side]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_th] ADD  CONSTRAINT [DF_ubc_th_side]  DEFAULT ((1)) FOR [side]
GO
/****** Object:  Default [DF_ubc_tp_zorder]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_tp] ADD  CONSTRAINT [DF_ubc_tp_zorder]  DEFAULT ('0') FOR [zorder]
GO
/****** Object:  Default [DF_ubc_tp_state]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_tp] ADD  CONSTRAINT [DF_ubc_tp_state]  DEFAULT ((0)) FOR [state]
GO
/****** Object:  Default [DF_ubc_user_userType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_userType]  DEFAULT ((3)) FOR [userType]
GO
/****** Object:  Default [DF_ubc_user_useEmail]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_useEmail]  DEFAULT ((0)) FOR [useEmail]
GO
/****** Object:  Default [DF_ubc_user_useSms]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_useSms]  DEFAULT ((0)) FOR [useSms]
GO
/****** Object:  ForeignKey [FK_ubc_defaultschedule_ubc_defaultschedule]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule]  WITH CHECK ADD  CONSTRAINT [FK_ubc_defaultschedule_ubc_defaultschedule] FOREIGN KEY([serialNo])
REFERENCES [dbo].[ubc_defaultschedule] ([serialNo])
GO
ALTER TABLE [dbo].[ubc_defaultschedule] CHECK CONSTRAINT [FK_ubc_defaultschedule_ubc_defaultschedule]
GO
