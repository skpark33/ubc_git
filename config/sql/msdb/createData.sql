-- ----------------------------------------------------------------------
-- SQL data bulk transfer script generated by the MySQL Migration Toolkit
-- ----------------------------------------------------------------------

use ubc;
INSERT INTO dbo.ubc_pmo(mgrId, pmId, ipAddress, ftpId, ftpPasswd, ftpPort, socketPort, ipAddress1, ipAddress2, ftpAddress, ftpAddress1, ftpAddress2, ftpMirror, ftpMirror1, ftpMirror2, currentConnectionCount, lastUpdateTime, adminstate)
VALUES 
  ('OD', '1', '211.232.57.215', 'server', 'rjtlrl2009', 21, 14161, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2012-07-18 20:28:22', 1),
  ('OD', '2', '211.232.57.215', 'server', 'rjtlrl2009', 21, 14162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2012-07-18 20:28:22', 1);

INSERT INTO dbo.ubc_customer(mgrId, customerId, url, description, gmt, language, copyUrl)
VALUES 
  ('OD', 'SQI', '', NULL, -540, 'Korean', NULL);

INSERT INTO dbo.ubc_site(mgrId, siteId, siteName, phoneNo1, phoneNo2, mobileNo, faxNo, franchizeType, chainNo, businessType, businessCode, ceoName, siteLevel, shopOpenTime, shopCloseTime, holiday, comment1, comment2, comment3, zipCode, addr1, addr2, addr3, addr4, parentId)
VALUES 
  ('1', 'SQI', 'SQI', NULL, NULL, NULL, NULL, 'SQI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL);

INSERT INTO dbo.ubc_user(mgrId, siteId, userId, password, userName, sid, mobileNo, phoneNo, zipCode, addr1, addr2, email, registerTime, userType, siteList, hostList, probableCauseList, useEmail, useSms, roleId, validationDate)
VALUES 
  ('1', 'SQI', 'super', 'c3VwZXI=', 'super', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, 0, '1', '2030-12-31 00:00:00'),
  ('1', 'SQI', 'SQImanager', 'S0lBbWFuYWdlcg==', 'SQImanager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 0, 0, '1', '2030-12-31 00:00:00'),
  ('1', 'SQI', 'SQIstaff', 'S0lBc3RhZmY=', 'SQIstaff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, 0, 0, '1', '2030-12-31 00:00:00');

INSERT INTO dbo.ubc_key_announceid(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_bpid(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_downloadid(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_faultid(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_hostautoupdateid(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_id(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_reservationid(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_thid(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_tpid(keyval) VALUES (1);
INSERT INTO dbo.ubc_key_zorder(keyval) VALUES (1);

INSERT INTO dbo.ubc_lmo_solution(mgrId, lmId, description, tableName, timeFieldName, logType, duration, adminState)
VALUES 
  ('1', 'ApplyLog', '', 'UBC_ApplyLog', 'applyTime', 0, 30, 1),
  ('1', 'ConnectionStateLog', '', 'UBC_ConnectionStateLog', 'eventTime', 0, 30, 1),
  ('1', 'DownloadState', '', 'UBC_DownloadState', 'startTime', 0, 30, 1),
  ('1', 'FaultLog', '', 'UBC_FaultLog', 'eventTime', 0, 30, 1),
  ('1', 'InteractiveLog', '', 'UBC_InteractiveLog', 'eventTime', 0, 30, 1),
  ('1', 'PlayLog', '', 'UBC_PlayLog', 'playDate', 0, 30, 1),
  ('1', 'PowerStateLog', '', 'UBC_POWERSTATELOG', 'eventTime', 0, 30, 1),
  ('1', 'ScheduleStateLog', '', 'UBC_ScheduleStateLog', 'eventTime', 0, 30, 1),
  ('1', 'ScreenShot', '', 'ScreenShot', '', 1, 30, 1),
  ('1', 'UserLog', '', 'UBC_UserLog', 'loginTime', 0, 30, 1);

INSERT INTO dbo.ubc_role(mgrId, roleId, roleName, roleDesc, customer)
VALUES 
  ('OD', '1', '시스템관리자', '', 'SQI'),
  ('OD', '2', '컨텐츠제작자', '', 'SQI'),
  ('OD', '3', '방송편집장', '', 'SQI');

INSERT INTO dbo.ubc_rolemap(mgrId, rolemapId, roleId, menuId, menuType, customer)
VALUES 
  ('OD', '1BDEL', '1', 'BDEL', 'S', 'SQI'),
  ('OD', '1BDTL', '1', 'BDTL', 'S', 'SQI'),
  ('OD', '1BMOD', '1', 'BMOD', 'S', 'SQI'),
  ('OD', '1BQRY', '1', 'BQRY', 'S', 'SQI'),
  ('OD', '1BREG', '1', 'BREG', 'S', 'SQI'),
  ('OD', '1BXPT', '1', 'BXPT', 'S', 'SQI'),
  ('OD', '1CQRY', '1', 'CQRY', 'S', 'SQI'),
  ('OD', '1FDEL', '1', 'FDEL', 'S', 'SQI'),
  ('OD', '1FQRY', '1', 'FQRY', 'S', 'SQI'),
  ('OD', '1HBRS', '1', 'HBRS', 'S', 'SQI'),
  ('OD', '1HCLR', '1', 'HCLR', 'S', 'SQI'),
  ('OD', '1HCRS', '1', 'HCRS', 'S', 'SQI'),
  ('OD', '1HDEL', '1', 'HDEL', 'S', 'SQI'),
  ('OD', '1HDNV', '1', 'HDNV', 'S', 'SQI'),
  ('OD', '1HDTL', '1', 'HDTL', 'S', 'SQI'),
  ('OD', '1HEXL', '1', 'HEXL', 'S', 'SQI'),
  ('OD', '1HGMD', '1', 'HGMD', 'S', 'SQI'),
  ('OD', '1HMOD', '1', 'HMOD', 'S', 'SQI'),
  ('OD', '1HMPW', '1', 'HMPW', 'S', 'SQI'),
  ('OD', '1HOFF', '1', 'HOFF', 'S', 'SQI'),
  ('OD', '1HPNO', '1', 'HPNO', 'S', 'SQI'),
  ('OD', '1HQRY', '1', 'HQRY', 'S', 'SQI'),
  ('OD', '1HRBT', '1', 'HRBT', 'S', 'SQI'),
  ('OD', '1HREG', '1', 'HREG', 'S', 'SQI'),
  ('OD', '1HRMC', '1', 'HRMC', 'S', 'SQI'),
  ('OD', '1HSCR', '1', 'HSCR', 'S', 'SQI'),
  ('OD', '1HUSE', '1', 'HUSE', 'S', 'SQI'),
  ('OD', '1HVNC', '1', 'HVNC', 'S', 'SQI'),
  ('OD', '1LQRY', '1', 'LQRY', 'S', 'SQI'),
  ('OD', '1MQRY', '1', 'MQRY', 'S', 'SQI'),
  ('OD', '1PCHG', '1', 'PCHG', 'S', 'SQI'),
  ('OD', '1PDEL', '1', 'PDEL', 'S', 'SQI'),
  ('OD', '1PMOD', '1', 'PMOD', 'S', 'SQI'),
  ('OD', '1PQRY', '1', 'PQRY', 'S', 'SQI'),
  ('OD', '1PREG', '1', 'PREG', 'S', 'SQI'),
  ('OD', '1PRSV', '1', 'PRSV', 'S', 'SQI'),
  ('OD', '1PSAV', '1', 'PSAV', 'S', 'SQI'),
  ('OD', '1PSND', '1', 'PSND', 'S', 'SQI'),
  ('OD', '1SBRD', '1', 'SBRD', 'S', 'SQI'),
  ('OD', '1SCOD', '1', 'SCOD', 'S', 'SQI'),
  ('OD', '1SGRP', '1', 'SGRP', 'S', 'SQI'),
  ('OD', '1SQRY', '1', 'SQRY', 'S', 'SQI'),
  ('OD', '1SUSR', '1', 'SUSR', 'S', 'SQI'),
  ('OD', '1TDEL', '1', 'TDEL', 'S', 'SQI'),
  ('OD', '1TMOD', '1', 'TMOD', 'S', 'SQI'),
  ('OD', '1TQRY', '1', 'TQRY', 'S', 'SQI'),
  ('OD', '1TREG', '1', 'TREG', 'S', 'SQI'),
  ('OD', '1UMOD', '1', 'UMOD', 'S', 'SQI'),
  ('OD', '1UQRY', '1', 'UQRY', 'S', 'SQI'),
  ('OD', '2CQRY', '2', 'CQRY', 'S', 'SQI'),
  ('OD', '2HEXL', '2', 'HEXL', 'S', 'SQI'),
  ('OD', '2HQRY', '2', 'HQRY', 'S', 'SQI'),
  ('OD', '2PMOD', '2', 'PMOD', 'S', 'SQI'),
  ('OD', '2PQRY', '2', 'PQRY', 'S', 'SQI'),
  ('OD', '2PREG', '2', 'PREG', 'S', 'SQI'),
  ('OD', '2PSAV', '2', 'PSAV', 'S', 'SQI'),
  ('OD', '2PSND', '2', 'PSND', 'S', 'SQI'),
  ('OD', '3BDEL', '3', 'BDEL', 'S', 'SQI'),
  ('OD', '3BDTL', '3', 'BDTL', 'S', 'SQI'),
  ('OD', '3BMOD', '3', 'BMOD', 'S', 'SQI'),
  ('OD', '3BQRY', '3', 'BQRY', 'S', 'SQI'),
  ('OD', '3BREG', '3', 'BREG', 'S', 'SQI'),
  ('OD', '3BXPT', '3', 'BXPT', 'S', 'SQI'),
  ('OD', '3CQRY', '3', 'CQRY', 'S', 'SQI'),
  ('OD', '3HCRS', '3', 'HCRS', 'S', 'SQI'),
  ('OD', '3HDNV', '3', 'HDNV', 'S', 'SQI'),
  ('OD', '3HEXL', '3', 'HEXL', 'S', 'SQI'),
  ('OD', '3HPNO', '3', 'HPNO', 'S', 'SQI'),
  ('OD', '3HQRY', '3', 'HQRY', 'S', 'SQI'),
  ('OD', '3HSCR', '3', 'HSCR', 'S', 'SQI'),
  ('OD', '3PCHG', '3', 'PCHG', 'S', 'SQI'),
  ('OD', '3PDEL', '3', 'PDEL', 'S', 'SQI'),
  ('OD', '3PMOD', '3', 'PMOD', 'S', 'SQI'),
  ('OD', '3PQRY', '3', 'PQRY', 'S', 'SQI'),
  ('OD', '3PREG', '3', 'PREG', 'S', 'SQI'),
  ('OD', '3PRSV', '3', 'PRSV', 'S', 'SQI'),
  ('OD', '3PSAV', '3', 'PSAV', 'S', 'SQI'),
  ('OD', '3PSND', '3', 'PSND', 'S', 'SQI'),
  ('OD', '3TDEL', '3', 'TDEL', 'S', 'SQI'),
  ('OD', '3TMOD', '3', 'TMOD', 'S', 'SQI'),
  ('OD', '3TQRY', '3', 'TQRY', 'S', 'SQI'),
  ('OD', '3TREG', '3', 'TREG', 'S', 'SQI'),
  ('OD', '3UMOD', '3', 'UMOD', 'S', 'SQI'),
  ('OD', '3UQRY', '3', 'UQRY', 'S', 'SQI'),
  ('OD', '4FDEL', '4', 'FDEL', 'S', 'SQI'),
  ('OD', '4FQRY', '4', 'FQRY', 'S', 'SQI'),
  ('OD', '4HBRS', '4', 'HBRS', 'S', 'SQI'),
  ('OD', '4HCLR', '4', 'HCLR', 'S', 'SQI'),
  ('OD', '4HCRS', '4', 'HCRS', 'S', 'SQI'),
  ('OD', '4HDTL', '4', 'HDTL', 'S', 'SQI'),
  ('OD', '4HEXL', '4', 'HEXL', 'S', 'SQI'),
  ('OD', '4HMOD', '4', 'HMOD', 'S', 'SQI'),
  ('OD', '4HMPW', '4', 'HMPW', 'S', 'SQI'),
  ('OD', '4HOFF', '4', 'HOFF', 'S', 'SQI'),
  ('OD', '4HQRY', '4', 'HQRY', 'S', 'SQI'),
  ('OD', '4HRBT', '4', 'HRBT', 'S', 'SQI'),
  ('OD', '4HSCR', '4', 'HSCR', 'S', 'SQI'),
  ('OD', '4LQRY', '4', 'LQRY', 'S', 'SQI'),
  ('OD', '4MQRY', '4', 'MQRY', 'S', 'SQI'),
  ('OD', '4PCHG', '4', 'PCHG', 'S', 'SQI'),
  ('OD', '4SQRY', '4', 'SQRY', 'S', 'SQI');

INSERT INTO dbo.ubc_menuauth(mgrId, menuId, menuName, menuType, cAuth, rAuth, uAuth, dAuth, aAuth, customer)
VALUES 
  ('OD', 'BDEL', '방송계획-삭제', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'BDTL', '방송계획-속성', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'BMOD', '방송계획-변경', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'BQRY', '방송계획-목록조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'BREG', '방송계획-추가', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'BXPT', '방송계획-내보내기', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'CQRY', '콘텐츠관리조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'FDEL', '장애-삭제', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'FQRY', '장애-목록조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HBRS', '단말-브라우저재가동', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HCLR', '단말-단말청소', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HCRS', '단말-컨텐츠재전송', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HDEL', '단말-삭제', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HDNV', '배포 결과 보기', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HDTL', '단말목록-속성', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HEXL', '단말-목록 ExcelExport', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HGMD', '단말-그룹변경', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HMOD', '단말-수정', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HMPW', '단말-모니터전원', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HOFF', '단말-끄기', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HPNO', '단말-패키지해제', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HQRY', '단말-목록조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HRBT', '단말-다시시작', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HREG', '단말-등록', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HRMC', '단말-원격관리', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HSCR', '단말스크린샷조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HUSE', '단말목록-미사용', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'HVNC', '단말-원격접속', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'LQRY', '로그-조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'MQRY', '프로세스감시-조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'PCHG', '패키지-즉시변경', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'PDEL', '패키지-삭제', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'PMOD', '패키지-편집', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'PQRY', '패키지-목록조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'PREG', '패키지-추가', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'PRSV', '패키지-예약', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'PSAV', '패키지-저장', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'PSND', '패키지-전송', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'SBRD', '공지사항관리', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'SCOD', '코드관리', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'SGRP', '조직관리', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'SQRY', '통계-조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'SUSR', '사용자관리', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'TDEL', '긴급공지-삭제', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'TMOD', '긴급공지-수정', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'TQRY', '긴급공지-목록조회', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'TREG', '긴급공지-추가', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'UMOD', 'S/W 업데이트-변경', 'S', 0, 0, 0, 0, 0, 'SQI'),
  ('OD', 'UQRY', 'S/W 업데이트 센터', 'S', 0, 0, 0, 0, 0, 'SQI');

select count(*) pmo from dbo.ubc_pmo;
select count(*) customer from dbo.ubc_customer;
select count(*) site from dbo.ubc_site;
select count(*) users from dbo.ubc_user;
select count(*) lmo_solution from dbo.ubc_lmo_solution;
select count(*) role from dbo.ubc_role;
select count(*) rolemap from dbo.ubc_rolemap;
select count(*) menuauth from dbo.ubc_menuauth;

-- End of script
