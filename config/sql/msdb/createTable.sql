USE [ubc]
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_log_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_log_create]
    @p_log_table varchar(6) 
AS
    DECLARE @dsql nvarchar(4000)

    IF @p_log_table <> ''
    BEGIN

        IF NOT EXISTS (
			SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_smt_log_'+@p_log_table
		)
        BEGIN
 
            select @dsql = '
                CREATE TABLE em_smt_log_' + @p_log_table + ' (
                    mt_pr int NOT NULL,
                    mt_seq int NOT NULL,
                    mt_refkey varchar(20) COLLATE Korean_Wansung_CS_AS,
                    priority char(2) COLLATE Korean_Wansung_CS_AS NOT NULL default ''S'',
                    date_client_req datetime NOT NULL,
                    content varchar(255) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    callback varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    broadcast_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL ,
                    msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default ''1'',
                    recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS,
                    change_word1 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word2 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word3 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word4 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word5 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    date_mt_sent datetime,
                    date_rslt datetime,
                    date_mt_report datetime,
                    mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
                    mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
                    carrier int,
                    rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
                    recipient_net int,
                    recipient_npsend char(1) COLLATE Korean_Wansung_CS_AS,
                    country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default ''82'',
                    charset varchar(20) COLLATE Korean_Wansung_CS_AS,
                    msg_type int,
                    crypto_yn char(1) COLLATE Korean_Wansung_CS_AS default ''Y''
                )'
    
            EXEC (@dsql)
    
            EXEC ('ALTER TABLE em_smt_log_'+@p_log_table+' ADD PRIMARY KEY ( mt_pr, mt_seq )')

            EXEC ('CREATE INDEX idx_em_smt_log_'+@p_log_table+'_1 ON em_smt_log_'+@p_log_table+' (date_client_req, recipient_num)')
            EXEC ('CREATE INDEX idx_em_smt_log_'+@p_log_table+'_2 ON em_smt_log_'+@p_log_table+' (date_mt_report, mt_report_code_ib)')
            EXEC ('CREATE INDEX idx_em_smt_log_'+@p_log_table+'_3 ON em_smt_log_'+@p_log_table+' (msg_status)')

        END
    END


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_create]
AS
    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_smt_tran'
	)
    BEGIN
        CREATE TABLE em_smt_tran (
            mt_pr int identity(1,1) NOT NULL ,
            mt_refkey varchar(20) COLLATE Korean_Wansung_CS_AS,
            priority char(2) COLLATE Korean_Wansung_CS_AS NOT NULL default 'S',
            date_client_req datetime NOT NULL default '1970-01-01 00:00:00',
            content varchar(255) COLLATE Korean_Wansung_CS_AS NOT NULL,
            callback varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
            service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            broadcast_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default 'N',
            msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default '1',
            recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS,
            date_mt_sent datetime,
            date_rslt datetime,
            date_mt_report datetime,
            mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
            mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
            carrier int,
            rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
            recipient_net int,
            recipient_npsend varchar(1) COLLATE Korean_Wansung_CS_AS,
            country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default '82',
            charset varchar(20) COLLATE Korean_Wansung_CS_AS,
            msg_type int,
            crypto_yn char(1) COLLATE Korean_Wansung_CS_AS default 'Y'
        )
        
        ALTER TABLE em_smt_tran  ADD PRIMARY KEY ( mt_pr )
        
        CREATE INDEX idx_em_smt_tran_1 ON em_smt_tran (msg_status, date_client_req)

        CREATE INDEX idx_em_smt_tran_2 ON em_smt_tran (recipient_num)

    END


    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_smt_client'
	)
    BEGIN
        CREATE TABLE em_smt_client (
            mt_pr int NOT NULL,
            mt_seq int NOT NULL,
            msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default '1',
            recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
            change_word1 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word2 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word3 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word4 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word5 varchar(20) COLLATE Korean_Wansung_CS_AS,
            date_mt_sent datetime,
            date_rslt datetime,
            date_mt_report datetime,
            mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
            mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
            carrier int,
            rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
            recipient_net int,
            recipient_npsend varchar(1) COLLATE Korean_Wansung_CS_AS,
            country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default '82'
        )
        
        ALTER TABLE em_smt_client  ADD PRIMARY KEY ( mt_pr, mt_seq )

        CREATE INDEX idx_em_smt_client_1 ON em_smt_client (recipient_num)

        CREATE INDEX idx_em_smt_client_2 ON em_smt_client (msg_status)
    END


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_stat_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_stat_create]
AS
    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_statistics_m' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_statistics_m (
            stat_date varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL,
            stat_servicetype char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
			stat_payment_code varchar(20) COLLATE Korean_Wansung_CS_AS NOT NULL,
			stat_carrier int NOT NULL,
            stat_success int,
			stat_failure int,
			stat_invalid int,
			stat_invalid_ib int,
			stat_remained int,
            stat_regdate datetime NOT NULL default '1970-01-01 00:00:00'
        )
        
        ALTER TABLE em_statistics_m  ADD PRIMARY KEY(stat_date, stat_servicetype, stat_payment_code, stat_carrier)

    END


    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_statistics_d' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_statistics_d (
            stat_date varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL,
            stat_servicetype char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
			stat_payment_code varchar(20) COLLATE Korean_Wansung_CS_AS NOT NULL,
			stat_carrier int NOT NULL,
            stat_fail_code varchar(10) COLLATE Korean_Wansung_CS_AS NOT NULL,
            stat_fail_cnt int,
            stat_regdate datetime NOT NULL default '1970-01-01 00:00:00'
        )
        
        ALTER TABLE em_resultcode  ADD PRIMARY KEY(stat_date, stat_servicetype, stat_payment_code, stat_carrier, stat_fail_code)

    END

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_update]
    @p_table_divi               char(1),
    @p_update_all_yn            char(1),
    @p_mt_pr                    int,
    @p_mt_seq                   int,
    @p_msg_status               char(1),
    @p_mt_report_code_ib        char(4),
    @p_mt_report_code_ibtype    char(1)
AS
    DECLARE @dsql nvarchar(1000)
	DECLARE @v_params as nvarchar(200)

    IF @p_table_divi = 'M'
        SET @dsql = ' UPDATE em_smt_tran SET  '
    ELSE
        SET @dsql = ' UPDATE em_smt_client SET '
 
    SET @dsql = @dsql + '
        msg_status            = @pp_msg_status,
        mt_report_code_ib     = @pp_mt_report_code_ib,
        mt_report_code_ibtype = @pp_mt_report_code_ibtype,
        date_mt_sent		  = getdate() '

    IF @p_msg_status = '3'
        SET @dsql = @dsql + ' , date_rslt  = getdate() '

    SET @dsql = @dsql + ' WHERE mt_pr  = @pp_mt_pr '

    IF ( @p_table_divi = 'D' AND @p_update_all_yn = 'N' )
        SET @dsql = @dsql + ' AND mt_seq  = ' + convert(varchar(11), @p_mt_seq) 

	SET @v_params = '@pp_msg_status char(1),
					 @pp_mt_report_code_ib char(4),
					 @pp_mt_report_code_ibtype char(1),
					 @pp_mt_pr int'
      


	EXECUTE sp_executesql @dsql, @v_params, 
								 @pp_msg_status = @p_msg_status,
								 @pp_mt_report_code_ib = @p_mt_report_code_ib,
								 @pp_mt_report_code_ibtype = @p_mt_report_code_ibtype,
								 @pp_mt_pr = @p_mt_pr


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_tran_select]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_tran_select]
    @p_service_type varchar(20),
    @p_priority char(2),
    @p_ttl int,
    @p_emma_id char(2),
    @p_bancheck_yn char(1)
  
AS
    DECLARE @dsql nvarchar(4000)
	DECLARE @v_params as nvarchar(500)

    SET @dsql = '
        SELECT TOP 300
            A.mt_pr             AS mt_pr,
            A.mt_refkey         AS mt_refkey,
            A.content           AS content,
            A.priority          AS priority,
            A.broadcast_yn      AS broadcast_yn,
            A.callback          AS callback,
            A.recipient_num     AS recipient_num,
            A.recipient_net     AS recipient_net,
            A.recipient_npsend  AS recipient_npsend,
            A.country_code      AS country_code,
            A.date_client_req   AS date_client_req,
            A.charset           AS charset,
            A.msg_type          AS msg_type,
            A.crypto_yn         AS crypto_yn,
            A.service_type      AS service_type,
            B.ban_type          AS ban_type,
            B.send_yn           AS send_yn
        FROM em_smt_tran A 
        LEFT OUTER JOIN em_banlist B 
        ON  A.recipient_num = B.content
        AND A.service_type = B.service_type
        AND B.ban_type = @pp_ban_type
        AND B.ban_status_yn = @pp_ban_status_yn
        WHERE A.priority = @pp_priority
        AND A.msg_status = @pp_msg_status 
        AND A.date_client_req BETWEEN (getdate() - @pp_ttl/24/60) AND getdate() '

    IF @p_service_type = 'SMT'
        SET @dsql = @dsql + ' AND   A.service_type = ''0'' '
    
    IF @p_service_type = 'URL'
        SET @dsql = @dsql + ' AND   A.service_type = ''1'' '

    SET @v_params = '
		@pp_ban_type char(1),
		@pp_ban_status_yn char(1),
		@pp_priority char(2),
		@pp_msg_status char(1),
		@pp_ttl int'

    EXECUTE sp_executesql @dsql, @v_params, 
								 @pp_ban_type = 'R',
								 @pp_ban_status_yn = 'Y',
								 @pp_priority = @p_priority,
								 @pp_msg_status = '1',
								 @pp_ttl = @p_ttl


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_create]
AS
    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_mmt_tran'
	)
    BEGIN
        CREATE TABLE em_mmt_tran (
            mt_pr int identity(1,1) NOT NULL ,
            mt_refkey varchar(20) COLLATE Korean_Wansung_CS_AS,
            priority char(2) COLLATE Korean_Wansung_CS_AS NOT NULL default 'S',
            msg_class char(1) default '1',
            date_client_req datetime NOT NULL default '1970-01-01 00:00:00',
            subject varchar(40) COLLATE Korean_Wansung_CS_AS NOT NULL,
			content_type int default 0,
            content varchar(4000) COLLATE Korean_Wansung_CS_AS NOT NULL,
            attach_file_group_key int default 0,
            callback varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
            service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            broadcast_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default 'N',
            msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default '1',
            recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS,
            date_mt_sent datetime,
            date_rslt datetime,
            date_mt_report datetime,
            mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
            mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
            carrier int,
            rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
            recipient_net int,
            recipient_npsend varchar(1) COLLATE Korean_Wansung_CS_AS,
            country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default '82',
            charset varchar(20) COLLATE Korean_Wansung_CS_AS,
            msg_type int,
            crypto_yn char(1) COLLATE Korean_Wansung_CS_AS default 'Y'
        )
        
        ALTER TABLE em_mmt_tran  ADD PRIMARY KEY ( mt_pr )
        
        CREATE INDEX idx_em_mmt_tran_1 ON em_mmt_tran (msg_status, date_client_req)

        CREATE INDEX idx_em_mmt_tran_2 ON em_mmt_tran (recipient_num)

        CREATE INDEX idx_em_mmt_tran_3 ON em_mmt_tran (attach_file_group_key)

    END


    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_mmt_client'
	)
    BEGIN
        CREATE TABLE em_mmt_client (
            mt_pr int NOT NULL,
            mt_seq int NOT NULL,
            msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default '1',
            recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
            change_word1 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word2 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word3 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word4 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word5 varchar(20) COLLATE Korean_Wansung_CS_AS,
            date_mt_sent datetime,
            date_rslt datetime,
            date_mt_report datetime,
            mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
            mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
            carrier int,
            rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
            recipient_net int,
            recipient_npsend varchar(1) COLLATE Korean_Wansung_CS_AS,
            country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default '82'
        )
        
        ALTER TABLE em_mmt_client  ADD PRIMARY KEY ( mt_pr, mt_seq )

        CREATE INDEX idx_em_mmt_client_1 ON em_mmt_client(recipient_num)

        CREATE INDEX idx_em_mmt_client_2 ON em_mmt_client (msg_status)

    END


    /* create em_mmt_file table, file detail */
	IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_mmt_file'
	)
    BEGIN
        CREATE TABLE em_mmt_file (
            attach_file_group_key int NOT NULL,
            attach_file_group_seq int NOT NULL,
            attach_file_seq int NOT NULL,
            attach_file_subpath varchar(64) COLLATE Korean_Wansung_CS_AS,
            attach_file_name varchar(64) COLLATE Korean_Wansung_CS_AS NOT NULL,
            attach_file_carrier int,
            reg_date datetime NOT NULL default '1970-01-01 00:00:00'
        )
        
        ALTER TABLE em_mmt_file  ADD PRIMARY KEY ( attach_file_group_key, attach_file_group_seq )

        CREATE INDEX idx_em_mmt_file_1 ON em_mmt_file(attach_file_seq)

        CREATE INDEX idx_em_mmt_file_2 ON em_mmt_file(attach_file_name)

    END


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_log_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_log_create]
    @p_log_table varchar(6) 
AS
    DECLARE @dsql nvarchar(4000)

    IF @p_log_table <> ''
    BEGIN
		IF NOT EXISTS (
			SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_mmt_log_'+@p_log_table
		)
        BEGIN
 
            select @dsql = '
                CREATE TABLE em_mmt_log_' + @p_log_table + ' (
                    mt_pr int NOT NULL,
                    mt_seq int NOT NULL,
                    mt_refkey varchar(20) COLLATE Korean_Wansung_CS_AS,
                    priority char(2) COLLATE Korean_Wansung_CS_AS NOT NULL default ''S'',
                    msg_class char(1) COLLATE Korean_Wansung_CS_AS default ''1'',
                    date_client_req datetime NOT NULL,
                    subject varchar(40) COLLATE Korean_Wansung_CS_AS NOT NULL,
					content_type int default 0,
                    content varchar(4000) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    attach_file_group_key int,
                    callback varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    broadcast_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL ,
                    msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default ''1'',
                    recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS,
                    change_word1 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word2 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word3 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word4 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word5 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    date_mt_sent datetime,
                    date_rslt datetime,
                    date_mt_report datetime,
                    mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
                    mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
                    carrier int,
                    rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
                    recipient_net int,
                    recipient_npsend char(1) COLLATE Korean_Wansung_CS_AS,
                    country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default ''82'',
                    charset varchar(20) COLLATE Korean_Wansung_CS_AS,
                    msg_type int,
                    crypto_yn char(1) COLLATE Korean_Wansung_CS_AS default ''Y''
                )'
    
            EXEC (@dsql)
    
            EXEC ('ALTER TABLE em_mmt_log_'+@p_log_table+' ADD PRIMARY KEY ( mt_pr, mt_seq )')

            EXEC ('CREATE INDEX idx_em_mmt_log_'+@p_log_table+'_1 ON em_mmt_log_'+@p_log_table+' (date_client_req, recipient_num)')
            EXEC ('CREATE INDEX idx_em_mmt_log_'+@p_log_table+'_2 ON em_mmt_log_'+@p_log_table+' (date_mt_report, mt_report_code_ib)')
            EXEC ('CREATE INDEX idx_em_mmt_log_'+@p_log_table+'_3 ON em_mmt_log_'+@p_log_table+' (msg_status)')

        END
    END


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mon_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mon_create]
AS
    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_status'
	)
    BEGIN
        CREATE TABLE em_status (
			emma_id char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            process_name varchar(40) COLLATE Korean_Wansung_CS_AS NOT NULL,
            pid varchar(20) COLLATE Korean_Wansung_CS_AS NOT NULL,
			service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
			cnt_today int,
			cnt_total int,
			cnt_resent_1 int,
			cnt_resent_10 int,
			que_size int,
			conn_time varchar(20) COLLATE Korean_Wansung_CS_AS ,
			update_time varchar(20) COLLATE Korean_Wansung_CS_AS ,
			conn_gw_info varchar(40) COLLATE Korean_Wansung_CS_AS ,
			reg_date datetime NOT NULL default '1970-01-01 00:00:00'
        )
        
        ALTER TABLE em_status  ADD PRIMARY KEY(emma_id, process_name, pid, service_type)

    END

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_update]
    @p_table_divi               char(1),
    @p_update_all_yn            char(1),
    @p_mt_pr                    int,
    @p_mt_seq                   int,
    @p_msg_status               char(1),
    @p_mt_report_code_ib        char(4),
    @p_mt_report_code_ibtype    char(1)
AS
    DECLARE @dsql nvarchar(1000)
	DECLARE @v_params as nvarchar(200)

    IF @p_table_divi = 'M'
        SET @dsql = ' UPDATE em_mmt_tran SET  '
    ELSE
        SET @dsql = ' UPDATE em_mmt_client SET '
 
    SET @dsql = @dsql + '
        msg_status            = @pp_msg_status,
        mt_report_code_ib     = @pp_mt_report_code_ib,
        mt_report_code_ibtype = @pp_mt_report_code_ibtype,
        date_mt_sent = getdate() '

    IF @p_msg_status = '3'
        SET @dsql = @dsql + ' , date_rslt  = getdate() '

    SET @dsql = @dsql + ' WHERE mt_pr  = @pp_mt_pr '

    IF ( @p_table_divi = 'D' AND @p_update_all_yn = 'N' )
        SET @dsql = @dsql + ' AND mt_seq  = ' + convert(varchar(11), @p_mt_seq)    

	SET @v_params = '@pp_msg_status char(1),
					 @pp_mt_report_code_ib char(4),
					 @pp_mt_report_code_ibtype char(1),
					 @pp_mt_pr int'

	EXECUTE sp_executesql @dsql, @v_params, 
								 @pp_msg_status = @p_msg_status,
								 @pp_mt_report_code_ib = @p_mt_report_code_ib,
								 @pp_mt_report_code_ibtype = @p_mt_report_code_ibtype,
								 @pp_mt_pr = @p_mt_pr

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_common_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_common_create]
AS
    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_banlist' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_banlist (
            service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            ban_seq int NOT NULL,
            ban_type char(1) COLLATE Korean_Wansung_CS_AS NOT NULL,
            content varchar(45) COLLATE Korean_Wansung_CS_AS NOT NULL,
            send_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default 'N',
            ban_status_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default 'Y',
            reg_date datetime NOT NULL default getdate(),
            reg_user varchar(20) COLLATE Korean_Wansung_CS_AS,
            update_date datetime NOT NULL default '1970-01-01 00:00:00',
            update_user varchar(20) COLLATE Korean_Wansung_CS_AS
        )
        
        ALTER TABLE em_banlist  ADD PRIMARY KEY(service_type, ban_seq)
        CREATE INDEX idx_em_banlist_1 ON em_banlist(ban_type, service_type, ban_status_yn)
		CREATE INDEX idx_em_banlist_2 ON em_banlist(content)

    END


    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_resultcode' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_resultcode (
            service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            rslt_code varchar(4) COLLATE Korean_Wansung_CS_AS NOT NULL,
            rslt_type char(1) COLLATE Korean_Wansung_CS_AS NOT NULL,
            rslt_name varchar(80) COLLATE Korean_Wansung_CS_AS NOT NULL,
            rslt_pname varchar(80) COLLATE Korean_Wansung_CS_AS
        )
        
        ALTER TABLE em_resultcode  ADD PRIMARY KEY(service_type, rslt_code)
        
    END

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_common_checkprivilege]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_common_checkprivilege]
AS
    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_temp' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_temp (a char(1))

		DROP TABLE em_temp
    END

RETURN
GO
/****** Object:  Table [dbo].[ubc_host_test]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_host_test](
	[mgrId] [varchar](255) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[hostName] [varchar](255) NULL,
	[domainName] [varchar](255) NULL,
	[vendor] [varchar](255) NULL,
	[model] [varchar](255) NULL,
	[os] [nvarchar](max) NULL,
	[serialNo] [decimal](10, 0) IDENTITY(1,1) NOT NULL,
	[period] [decimal](5, 0) NULL,
	[asOperator] [varchar](255) NULL,
	[asOperatorTel] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[soundVolume] [decimal](5, 0) NULL,
	[mute] [decimal](1, 0) NULL,
	[resolution] [varchar](50) NULL,
	[defaultTemplate] [varchar](50) NULL,
	[ipAddress] [varchar](255) NULL,
	[adminState] [decimal](1, 0) NULL,
	[operationalState] [decimal](1, 0) NULL,
	[autoPowerFlag] [decimal](1, 0) NULL,
	[startupTime] [varchar](255) NULL,
	[shutdownTime] [varchar](255) NULL,
	[powerControl] [decimal](5, 0) NULL,
	[amtPassword] [varchar](50) NULL,
	[macAddress] [varchar](50) NULL,
	[wolPort] [decimal](5, 0) NULL,
	[edition] [varchar](255) NULL,
	[version] [varchar](255) NULL,
	[lastUpdateTime] [datetime] NULL,
	[cpuUsed] [decimal](5, 2) NULL,
	[realMemoryUsed] [decimal](10, 0) NULL,
	[realMemoryTotal] [decimal](10, 0) NULL,
	[virtualMemoryUsed] [decimal](10, 0) NULL,
	[virtualMemoryTotal] [decimal](10, 0) NULL,
	[fileSystem] [varchar](1024) NULL,
	[vncPort] [decimal](10, 0) NULL,
	[zipCode] [varchar](255) NULL,
	[addr1] [varchar](1024) NULL,
	[addr2] [varchar](255) NULL,
	[addr3] [varchar](255) NULL,
	[addr4] [varchar](255) NULL,
	[templatePlayList] [varchar](2048) NULL,
	[lastScreenshotFile] [varchar](255) NULL,
	[screenshotPeriod] [decimal](5, 0) NULL,
	[sosId] [varchar](255) NULL,
	[templatePlayList3] [ntext] NULL,
	[authDate] [datetime] NULL,
	[displayCounter] [decimal](5, 0) NULL,
	[autoSchedule1] [varchar](255) NULL,
	[autoSchedule2] [varchar](255) NULL,
	[currentSchedule1] [varchar](255) NULL,
	[currentSchedule2] [varchar](255) NULL,
	[lastSchedule1] [varchar](255) NULL,
	[lastSchedule2] [varchar](255) NULL,
	[lastScheduleTime1] [datetime] NULL,
	[lastScheduleTime2] [datetime] NULL,
	[networkUse1] [tinyint] NOT NULL,
	[networkUse2] [tinyint] NOT NULL,
	[scheduleApplied1] [tinyint] NOT NULL,
	[scheduleApplied2] [tinyint] NOT NULL,
	[holiday] [varchar](255) NULL,
	[bootUpTime] [datetime] NULL,
	[reservedScheduleList1] [text] NULL,
	[reservedScheduleList2] [ntext] NULL,
	[soundDisplay] [tinyint] NOT NULL,
	[vncState] [tinyint] NOT NULL,
	[cpuTemp] [tinyint] NOT NULL,
 CONSTRAINT [PK_ubc_host_test] PRIMARY KEY CLUSTERED 
(
	[serialNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__ubc_host__97B6C6053EC74557] UNIQUE NONCLUSTERED 
(
	[siteId] ASC,
	[hostId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_host]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_host](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[hostName] [varchar](255) NULL,
	[domainName] [varchar](255) NULL,
	[vendor] [varchar](255) NULL,
	[model] [varchar](255) NULL,
	[os] [varchar](255) NULL,
	[serialNo] [decimal](10, 0) IDENTITY(1,1) NOT NULL,
	[period] [decimal](5, 0) NULL,
	[asOperator] [varchar](255) NULL,
	[asOperatorTel] [nvarchar](255) NULL,
	[description] [varchar](255) NULL,
	[soundVolume] [decimal](5, 0) NULL,
	[mute] [decimal](1, 0) NULL,
	[resolution] [varchar](50) NULL,
	[defaultTemplate] [varchar](50) NULL,
	[ipAddress] [varchar](255) NULL,
	[adminState] [decimal](1, 0) NULL,
	[operationalState] [tinyint] NULL,
	[autoPowerFlag] [decimal](1, 0) NULL,
	[startupTime] [varchar](255) NULL,
	[shutdownTime] [varchar](255) NULL,
	[powerControl] [decimal](5, 0) NULL,
	[amtPassword] [varchar](50) NULL,
	[macAddress] [varchar](50) NULL,
	[wolPort] [decimal](5, 0) NULL,
	[edition] [varchar](255) NULL,
	[version] [varchar](255) NULL,
	[lastUpdateTime] [datetime] NULL,
	[cpuUsed] [float] NULL,
	[realMemoryUsed] [decimal](10, 0) NULL,
	[realMemoryTotal] [decimal](10, 0) NULL,
	[virtualMemoryUsed] [decimal](10, 0) NULL,
	[virtualMemoryTotal] [decimal](10, 0) NULL,
	[fileSystem] [varchar](1024) NULL,
	[vncPort] [decimal](10, 0) NULL,
	[zipCode] [varchar](255) NULL,
	[addr1] [varchar](1024) NULL,
	[addr2] [varchar](255) NULL,
	[addr3] [varchar](255) NULL,
	[addr4] [varchar](255) NULL,
	[templatePlayList] [varchar](2048) NULL,
	[lastScreenshotFile] [varchar](255) NULL,
	[screenshotPeriod] [decimal](5, 0) NULL,
	[sosId] [varchar](255) NULL,
	[templatePlayList3] [text] NULL,
	[authDate] [datetime] NULL,
	[displayCounter] [decimal](5, 0) NULL,
	[autoSchedule1] [varchar](255) NULL,
	[autoSchedule2] [varchar](255) NULL,
	[currentSchedule1] [varchar](255) NULL,
	[currentSchedule2] [varchar](255) NULL,
	[lastSchedule1] [varchar](255) NULL,
	[lastSchedule2] [varchar](255) NULL,
	[lastScheduleTime1] [datetime] NULL,
	[lastScheduleTime2] [datetime] NULL,
	[networkUse1] [tinyint] NOT NULL,
	[networkUse2] [tinyint] NOT NULL,
	[scheduleApplied1] [tinyint] NOT NULL,
	[scheduleApplied2] [tinyint] NOT NULL,
	[holiday] [varchar](255) NULL,
	[bootUpTime] [datetime] NULL,
	[reservedScheduleList1] [text] NULL,
	[reservedScheduleList2] [text] NULL,
	[soundDisplay] [tinyint] NOT NULL,
	[vncState] [tinyint] NOT NULL,
	[cpuTemp] [tinyint] NOT NULL,
	[createTime] [datetime] NOT NULL,
	[autoUpdateFlag] [tinyint] NULL,
	[playLogDayLimit] [decimal](10, 0) NULL,
	[hddThreshold] [tinyint] NOT NULL,
	[playLogDayCriteria] [varchar](10) NOT NULL,
	[baseSchedule1] [varchar](255) NULL,
	[baseSchedule2] [varchar](255) NULL,
	[renderMode] [tinyint] NOT NULL,
	[category] [varchar](255) NULL,
	[contentsDownloadTime] [varchar](10) NULL,
	[starterState] [tinyint] NOT NULL,
	[browserState] [tinyint] NOT NULL,
	[browser1State] [tinyint] NOT NULL,
	[firmwareViewState] [tinyint] NOT NULL,
	[preDownloaderState] [tinyint] NOT NULL,
	[bootDownTime] [datetime] NULL,
	[download1State] [tinyint] NOT NULL,
	[download2State] [tinyint] NOT NULL,
	[progress1] [varchar](10) NOT NULL,
	[progress2] [varchar](10) NOT NULL,
	[monitoroff] [tinyint] NOT NULL,
	[monitoroffList] [text] NULL,
	[hostType] [smallint] NOT NULL,
	[mmsCount] [varchar](45) NULL,
	[monitorState] [int] NULL,
	[winPassword] [varchar](50) NULL,
	[monitorUseTime] [int] NOT NULL,
	[downloadGroup] [varchar](255) NULL,
	[gmt] [int] NOT NULL,
	[weekShutdownTime] [varchar](100) NULL,
	[hostMsg1] [varchar](255) NULL,
	[hostMsg2] [varchar](255) NULL,
	[disk1Avail] [decimal](10, 2) NOT NULL,
	[disk2Avail] [decimal](10, 2) NOT NULL,
	[protocolType] [varchar](255) NOT NULL,
	[hostType] [smallint] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_ubc_host] PRIMARY KEY CLUSTERED 
(
	[hostId] ASC,
	[siteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__ubc_host__97B6C6053BEAD8AC] UNIQUE NONCLUSTERED 
(
	[siteId] ASC,
	[hostId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_frame]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_frame](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](128) NOT NULL,
	[programId] [varchar](128) NOT NULL,
	[templateId] [varchar](128) NOT NULL,
	[frameId] [varchar](128) NOT NULL,
	[grade] [decimal](5, 0) NULL,
	[width] [decimal](5, 0) NULL,
	[height] [decimal](5, 0) NULL,
	[x] [decimal](5, 0) NULL,
	[y] [decimal](5, 0) NULL,
	[isPIP] [decimal](1, 0) NULL,
	[borderStyle] [varchar](50) NULL,
	[borderThickness] [decimal](5, 0) NULL,
	[borderColor] [varchar](50) NULL,
	[cornerRadius] [decimal](5, 0) NULL,
	[description] [varchar](255) NULL,
	[comment1] [varchar](255) NULL,
	[comment2] [varchar](255) NULL,
	[comment3] [varchar](255) NULL,
	[serialNo] [decimal](10, 0) IDENTITY(1,1) NOT NULL,
	[zindex] [tinyint] NULL,
	[alpha] [tinyint] NULL,
	[isTV] [tinyint] NULL,
 CONSTRAINT [PK_ubc_frame_1] PRIMARY KEY CLUSTERED 
(
	[serialNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[programId] ASC,
	[templateId] ASC,
	[frameId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[programId] ASC,
	[templateId] ASC,
	[frameId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_fmo]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_fmo](
	[mgrId] [varchar](255) NOT NULL,
	[fmId] [varchar](255) NOT NULL,
	[duration] [int] NULL,
	[probableCauseList] [text] NULL,
	[excludeSource] [text] NULL,
	[includeSource] [text] NULL,
 CONSTRAINT [PK_ubc_fmo_1] PRIMARY KEY CLUSTERED 
(
	[fmId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_faultlog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_faultlog](
	[mgrId] [varchar](255) NOT NULL,
	[faultId] [varchar](255) NOT NULL,
	[eventTime] [datetime] NULL,
	[source] [varchar](255) NULL,
	[severity] [decimal](3, 0) NULL,
	[probableCause] [varchar](255) NULL,
	[additionalText] [varchar](255) NULL,
	[counter] [int] NULL,
	[updateTime] [datetime] NULL,
	[siteId] [varchar](255) NULL,
	[hostId] [varchar](235) NULL,
 CONSTRAINT [PK_ubc_faultlog_1] PRIMARY KEY CLUSTERED 
(
	[faultId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_fault]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_fault](
	[mgrId] [varchar](255) NOT NULL,
	[faultId] [varchar](255) NOT NULL,
	[eventTime] [datetime] NULL,
	[source] [varchar](255) NULL,
	[severity] [decimal](3, 0) NULL,
	[probableCause] [varchar](255) NULL,
	[additionalText] [varchar](255) NULL,
	[counter] [int] NULL,
	[updateTime] [datetime] NULL,
	[siteId] [varchar](255) NULL,
	[hostId] [varchar](235) NULL,
 CONSTRAINT [PK_ubc_fault_1] PRIMARY KEY CLUSTERED 
(
	[faultId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_eventlog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_eventlog](
	[eventType] [varchar](255) NOT NULL,
	[eventKey] [varchar](255) NULL,
	[eventTime] [datetime] NOT NULL,
	[eventBody] [varchar](max) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_event]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_event](
	[eventType] [varchar](255) NOT NULL,
	[eventIndex] [bigint] IDENTITY(1,1) NOT NULL,
	[domain] [varchar](255) NULL,
	[entity] [varchar](255) NULL,
	[eventTime] [datetime] NOT NULL,
	[eventBody] [varchar](max) NULL,
 CONSTRAINT [PK_ubc_event] PRIMARY KEY CLUSTERED 
(
	[eventType] ASC,
	[eventIndex] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_downloadstateSummary]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_downloadstateSummary](
	[mgrId] [varchar](255) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[downloadId] [varchar](255) NOT NULL,
	[brwId] [tinyint] NULL,
	[programId] [varchar](255) NULL,
	[programState] [tinyint] NULL,
	[programStartTime] [datetime] NULL,
	[programEndTime] [datetime] NULL,
	[progress] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_downloadstatelog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_downloadstatelog](
	[mgrId] [varchar](255) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[downloadId] [varchar](255) NOT NULL,
	[brwId] [tinyint] NULL,
	[programId] [varchar](255) NULL,
	[contentsId] [varchar](255) NOT NULL,
	[contentsName] [varchar](255) NULL,
	[contentsType] [decimal](3, 0) NULL,
	[location] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[volume] [decimal](10, 0) NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[currentVolume] [decimal](10, 0) NULL,
	[result] [tinyint] NULL,
	[reason] [decimal](5, 0) NULL,
	[downState] [tinyint] NULL,
	[programState] [tinyint] NULL,
	[programStartTime] [datetime] NULL,
	[programEndTime] [datetime] NULL,
	[progress] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_downloadstate]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_downloadstate](
	[mgrId] [varchar](255) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[downloadId] [varchar](255) NOT NULL,
	[brwId] [tinyint] NULL,
	[programId] [varchar](255) NULL,
	[contentsId] [varchar](255) NOT NULL,
	[contentsName] [varchar](255) NULL,
	[contentsType] [decimal](3, 0) NULL,
	[location] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[volume] [decimal](10, 0) NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[currentVolume] [decimal](10, 0) NULL,
	[result] [tinyint] NULL,
	[reason] [decimal](5, 0) NULL,
	[downState] [tinyint] NULL,
	[programState] [tinyint] NULL,
	[programStartTime] [datetime] NULL,
	[programEndTime] [datetime] NULL,
	[progress] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_defaultschedule]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_defaultschedule](
	[mgrId] [varchar](128) NOT NULL,
	[siteId] [varchar](128) NOT NULL,
	[programId] [varchar](128) NOT NULL,
	[templateId] [varchar](128) NOT NULL,
	[frameId] [varchar](128) NOT NULL,
	[scheduleId] [varchar](128) NOT NULL,
	[requestId] [varchar](255) NULL,
	[contentsId] [varchar](255) NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[playOrder] [decimal](5, 0) NULL,
	[openningFlag] [decimal](1, 0) NULL,
	[timeScope] [decimal](5, 0) NULL,
	[priority] [decimal](5, 0) NULL,
	[castingState] [decimal](3, 0) NULL,
	[castingStateTime] [timestamp] NOT NULL,
	[castingStateHistory] [varchar](255) NULL,
	[result] [varchar](255) NULL,
	[operationalState] [decimal](1, 0) NULL,
	[adminState] [decimal](1, 0) NULL,
	[phoneNumber] [varchar](255) NULL,
	[comment1] [varchar](255) NULL,
	[comment2] [varchar](255) NULL,
	[comment3] [varchar](255) NULL,
	[touchTime] [varchar](45) NULL,
	[parentScheduleId] [varchar](255) NULL,
	[serialNo] [decimal](10, 0) IDENTITY(1,1) NOT NULL,
	[runningTime] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_defaultschedule_1] PRIMARY KEY CLUSTERED 
(
	[serialNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[programId] ASC,
	[templateId] ASC,
	[frameId] ASC,
	[scheduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_customer]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_customer](
	[mgrId] [varchar](50) NOT NULL,
	[customerId] [varchar](255) NOT NULL,
	[url] [text] NULL,
	[description] [varchar](255) NULL,
	[gmt] [int] NOT NULL,
	[language] [varchar](50) NULL,
	[copyUrl] [varchar](max) NULL,
 CONSTRAINT [PK_ubc_customer] PRIMARY KEY CLUSTERED 
(
	[customerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_ctemplate]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_ctemplate](
	[mgrId] [varchar](10) NOT NULL,
	[templateId] [varchar](255) NOT NULL,
	[width] [decimal](5, 0) NULL,
	[height] [decimal](5, 0) NULL,
	[bgColor] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[registerTime] [timestamp] NOT NULL,
	[registerId] [varchar](255) NULL,
	[customer] [varchar](45) NULL,
 CONSTRAINT [PK_ubc_ctemplate] PRIMARY KEY CLUSTERED 
(
	[templateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_contents]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_contents](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NOT NULL,
	[contentsId] [varchar](255) NOT NULL,
	[category] [varchar](255) NULL,
	[promotionId] [varchar](255) NULL,
	[contentsName] [varchar](255) NULL,
	[contentsType] [decimal](3, 0) NULL,
	[parentsId] [varchar](255) NULL,
	[registerId] [varchar](255) NULL,
	[registerTime] [datetime] NULL,
	[isRaw] [decimal](1, 0) NULL,
	[contentsState] [decimal](3, 0) NULL,
	[contentsStateTime] [datetime] NULL,
	[contentsStateHistory] [varchar](255) NULL,
	[location] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[width] [decimal](5, 0) NULL,
	[height] [decimal](5, 0) NULL,
	[volume] [decimal](10, 0) NULL,
	[runningTime] [decimal](10, 0) NULL,
	[verifier] [varchar](255) NULL,
	[duration] [decimal](5, 0) NULL,
	[videoCodec] [varchar](255) NULL,
	[audioCodec] [varchar](255) NULL,
	[encodingInfo] [varchar](255) NULL,
	[encodingTime] [datetime] NULL,
	[description] [varchar](255) NULL,
	[comment1] [text] NULL,
	[comment2] [text] NULL,
	[comment3] [text] NULL,
	[comment4] [text] NULL,
	[comment5] [text] NULL,
	[comment6] [text] NULL,
	[comment7] [text] NULL,
	[comment8] [text] NULL,
	[comment9] [text] NULL,
	[comment10] [text] NULL,
	[currentComment] [decimal](5, 0) NULL,
	[bgColor] [varchar](255) NULL,
	[fgColor] [varchar](255) NULL,
	[font] [varchar](255) NULL,
	[fontSize] [decimal](5, 0) NULL,
	[playSpeed] [decimal](5, 0) NULL,
	[soundVolume] [decimal](5, 0) NULL,
	[promotionValueList] [varchar](1024) NULL,
	[snapshotFile] [varchar](255) NULL,
	[wizardFiles] [text] NULL,
	[isPublic] [decimal](1, 0) NULL,
	[tvInputType] [decimal](10, 0) NULL,
	[tvChannel] [varchar](45) NULL,
	[tvPortType] [decimal](10, 0) NULL,
	[direction] [tinyint] NULL,
	[align] [tinyint] NULL,
	[isUsed] [tinyint] NOT NULL,
	[wizardXML] [text] NULL,
	[contentsCategory] [smallint] NULL,
	[purpose] [smallint] NULL,
	[hostType] [smallint] NULL,
	[vertical] [smallint] NULL,
	[resolution] [smallint] NULL,
	[validationDate] [datetime] NULL,
	[requester] [varchar](255) NULL,
	[verifyMembers] [varchar](255) NULL,
	[verifyTime] [datetime] NULL,
	[registerType] [varchar](10) NULL,
	[serialNo] [decimal](10, 0) NULL,
	[adminState] [decimal](1, 0) NOT NULL,
UNIQUE NONCLUSTERED 
(
	[programId] ASC,
	[contentsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_connectionstatelog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_connectionstatelog](
	[mgrId] [varchar](255) NOT NULL,
	[logId] [varchar](255) NOT NULL,
	[eventTime] [datetime] NULL,
	[siteId] [varchar](255) NULL,
	[hostId] [varchar](255) NULL,
	[probableCause] [varchar](255) NULL,
	[additionalText] [varchar](255) NULL,
	[operationalState] [decimal](1, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_code]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_code](
	[mgrId] [varchar](255) NOT NULL,
	[codeId] [varchar](255) NOT NULL,
	[categoryName] [varchar](255) NOT NULL,
	[enumString] [varchar](255) NOT NULL,
	[enumNumber] [decimal](10, 0) NOT NULL,
	[dorder] [tinyint] NOT NULL,
	[visible] [tinyint] NOT NULL,
	[customer] [varchar](45) NOT NULL,
 CONSTRAINT [PK_ubc_code_1] PRIMARY KEY CLUSTERED 
(
	[codeId] ASC,
	[categoryName] ASC,
	[customer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_cframe]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_cframe](
	[mgrId] [varchar](10) NOT NULL,
	[templateId] [varchar](128) NOT NULL,
	[frameId] [varchar](128) NOT NULL,
	[grade] [decimal](5, 0) NULL,
	[width] [decimal](5, 0) NULL,
	[height] [decimal](5, 0) NULL,
	[x] [decimal](5, 0) NULL,
	[y] [decimal](5, 0) NULL,
	[isPIP] [decimal](1, 0) NULL,
	[borderStyle] [varchar](50) NULL,
	[borderThickness] [decimal](5, 0) NULL,
	[borderColor] [varchar](50) NULL,
	[cornerRadius] [decimal](5, 0) NULL,
	[description] [varchar](255) NULL,
	[comment1] [varchar](255) NULL,
	[comment2] [varchar](255) NULL,
	[comment3] [varchar](255) NULL,
	[alpha] [tinyint] NULL,
	[isTV] [tinyint] NULL,
	[customer] [varchar](45) NULL,
 CONSTRAINT [PK_ubc_cframe] PRIMARY KEY CLUSTERED 
(
	[templateId] ASC,
	[frameId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_bpmon]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_bpmon](
	[mgrId] [varchar](255) NOT NULL,
	[bpMonId] [varchar](255) NOT NULL,
	[bpId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NOT NULL,
	[reportTime] [datetime] NULL,
	[phone] [varchar](max) NULL,
	[email] [varchar](max) NULL,
	[reportWay] [tinyint] NULL,
	[downloadFailLimit] [tinyint] NULL,
	[testHostId] [varchar](255) NULL,
	[testStartTime] [datetime] NULL,
	[testEndTime] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_bpLog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_bpLog](
	[mgrId] [varchar](255) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[bpId] [varchar](255) NOT NULL,
	[bpLogId] [varchar](255) NULL,
	[touchTime] [datetime] NULL,
	[who] [varchar](255) NULL,
	[action] [varchar](50) NULL,
	[bpName] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_bp]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_bp](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[bpId] [varchar](255) NOT NULL,
	[bpName] [varchar](255) NULL,
	[createTime] [datetime] NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[weekInfo] [varchar](20) NULL,
	[exceptDay] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[zorder] [varchar](256) NOT NULL,
	[state] [tinyint] NOT NULL,
	[registerId] [varchar](255) NULL,
	[downloadTime] [datetime] NULL,
	[retryCount] [tinyint] NULL,
	[retryGap] [tinyint] NULL,
 CONSTRAINT [PK_ubc_bp] PRIMARY KEY CLUSTERED 
(
	[siteId] ASC,
	[bpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[siteId] ASC,
	[bpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_applylog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_applylog](
	[mgrId] [varchar](50) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[applyId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NULL,
	[applyTime] [datetime] NULL,
	[how] [varchar](50) NULL,
	[who] [varchar](255) NULL,
	[why] [varchar](255) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[applyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_announce]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_announce](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[announceId] [varchar](255) NOT NULL,
	[serialNo] [decimal](10, 0) NULL,
	[title] [varchar](255) NULL,
	[creator] [varchar](255) NULL,
	[createTime] [datetime] NULL,
	[hostIdList] [varchar](1024) NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[position] [decimal](3, 0) NULL,
	[height] [decimal](5, 0) NULL,
	[comment1] [text] NULL,
	[comment2] [text] NULL,
	[comment3] [text] NULL,
	[comment4] [text] NULL,
	[comment5] [text] NULL,
	[comment6] [text] NULL,
	[comment7] [text] NULL,
	[comment8] [text] NULL,
	[comment9] [text] NULL,
	[comment10] [text] NULL,
	[font] [varchar](255) NULL,
	[fontSize] [decimal](5, 0) NULL,
	[bgColor] [varchar](255) NULL,
	[fgColor] [varchar](255) NULL,
	[bgLocation] [varchar](255) NULL,
	[bgFile] [varchar](255) NULL,
	[playSpeed] [decimal](5, 0) NULL,
	[alpha] [decimal](5, 0) NULL,
UNIQUE NONCLUSTERED 
(
	[siteId] ASC,
	[announceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_agtserverlog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_agtserverlog](
	[mgrId] [varchar](255) NOT NULL,
	[agtserverlogId] [varchar](255) NULL,
	[actionKind] [varchar](255) NULL,
	[actionDirective] [varchar](255) NULL,
	[actionTime] [datetime] NOT NULL,
	[actionObject] [varchar](255) NULL,
	[actionComment] [varchar](255) NULL,
	[additionalInfo] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ts_cronjob]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ts_cronjob](
	[cronJobId] [varchar](100) NOT NULL,
	[exeTime] [varchar](50) NULL,
	[exeType] [varchar](20) NULL,
	[entity] [varchar](200) NULL,
	[directive] [varchar](64) NULL,
	[attributes] [varchar](1000) NULL,
	[operationalStatus] [decimal](1, 0) NULL,
 CONSTRAINT [PK_ts_cronjob] PRIMARY KEY CLUSTERED 
(
	[cronJobId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[Find_n_Right]    Script Date: 07/24/2012 19:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		gwangsoo
-- Create date: 2012-06-20
-- Description:	입력받은 문자열과 같은 부분이후의 문자열을 리턴
-- =============================================
CREATE FUNCTION [dbo].[Find_n_Right] 
(
	@findStr varchar(255), @fullStr varchar(255)
)
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @resultStr varchar(255)
select @resultStr = RIGHT(@fullStr, LEN(@fullStr) - (CHARINDEX(@findStr, @fullStr)+LEN(@findStr)-1))
return @resultStr
END;
GO
/****** Object:  Table [dbo].[event_postbox]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[event_postbox](
	[EVENTID] [int] IDENTITY(1,1) NOT NULL,
	[DOMAIN] [varchar](64) NULL,
	[ENTITY] [varchar](64) NULL,
	[EVENTTYPE] [varchar](64) NULL,
	[EVENTVALUE] [varchar](1024) NULL,
	[CREATETIME] [datetime] NULL,
	[SENDTIME] [datetime] NULL,
	[CHECKFLAG] [decimal](1, 0) NULL,
	[SENDFLAG] [decimal](1, 0) NULL,
	[ERRORFLAG] [decimal](1, 0) NULL,
	[REMARKS] [varchar](64) NULL,
 CONSTRAINT [PK_event_postbox] PRIMARY KEY CLUSTERED 
(
	[EVENTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_status]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_status](
	[emma_id] [char](2) NOT NULL,
	[process_name] [varchar](40) NOT NULL,
	[pid] [varchar](20) NOT NULL,
	[service_type] [char](2) NOT NULL,
	[cnt_today] [int] NULL,
	[cnt_total] [int] NULL,
	[cnt_resent_1] [int] NULL,
	[cnt_resent_10] [int] NULL,
	[que_size] [int] NULL,
	[conn_time] [varchar](20) NULL,
	[update_time] [varchar](20) NULL,
	[conn_gw_info] [varchar](40) NULL,
	[reg_date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[emma_id] ASC,
	[process_name] ASC,
	[pid] ASC,
	[service_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_statistics_m]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_statistics_m](
	[stat_date] [varchar](8) NOT NULL,
	[stat_servicetype] [char](2) NOT NULL,
	[stat_payment_code] [varchar](20) NOT NULL,
	[stat_carrier] [int] NOT NULL,
	[stat_success] [int] NULL,
	[stat_failure] [int] NULL,
	[stat_invalid] [int] NULL,
	[stat_invalid_ib] [int] NULL,
	[stat_remained] [int] NULL,
	[stat_regdate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[stat_date] ASC,
	[stat_servicetype] ASC,
	[stat_payment_code] ASC,
	[stat_carrier] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_statistics_d]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_statistics_d](
	[stat_date] [varchar](8) NOT NULL,
	[stat_servicetype] [char](2) NOT NULL,
	[stat_payment_code] [varchar](20) NOT NULL,
	[stat_carrier] [int] NOT NULL,
	[stat_fail_code] [varchar](10) NOT NULL,
	[stat_fail_cnt] [int] NULL,
	[stat_regdate] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_smt_tran]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_smt_tran](
	[mt_pr] [int] IDENTITY(1,1) NOT NULL,
	[mt_refkey] [varchar](20) NULL,
	[priority] [char](2) NOT NULL,
	[date_client_req] [datetime] NOT NULL,
	[content] [varchar](255) NOT NULL,
	[callback] [varchar](25) NOT NULL,
	[service_type] [char](2) NOT NULL,
	[broadcast_yn] [char](1) NOT NULL,
	[msg_status] [char](1) NOT NULL,
	[recipient_num] [varchar](25) NULL,
	[date_mt_sent] [datetime] NULL,
	[date_rslt] [datetime] NULL,
	[date_mt_report] [datetime] NULL,
	[mt_report_code_ib] [char](4) NULL,
	[mt_report_code_ibtype] [char](1) NULL,
	[carrier] [int] NULL,
	[rs_id] [varchar](20) NULL,
	[recipient_net] [int] NULL,
	[recipient_npsend] [varchar](1) NULL,
	[country_code] [varchar](8) NOT NULL,
	[charset] [varchar](20) NULL,
	[msg_type] [int] NULL,
	[crypto_yn] [char](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[mt_pr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_smt_client]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_smt_client](
	[mt_pr] [int] NOT NULL,
	[mt_seq] [int] NOT NULL,
	[msg_status] [char](1) NOT NULL,
	[recipient_num] [varchar](25) NOT NULL,
	[change_word1] [varchar](20) NULL,
	[change_word2] [varchar](20) NULL,
	[change_word3] [varchar](20) NULL,
	[change_word4] [varchar](20) NULL,
	[change_word5] [varchar](20) NULL,
	[date_mt_sent] [datetime] NULL,
	[date_rslt] [datetime] NULL,
	[date_mt_report] [datetime] NULL,
	[mt_report_code_ib] [char](4) NULL,
	[mt_report_code_ibtype] [char](1) NULL,
	[carrier] [int] NULL,
	[rs_id] [varchar](20) NULL,
	[recipient_net] [int] NULL,
	[recipient_npsend] [varchar](1) NULL,
	[country_code] [varchar](8) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[mt_pr] ASC,
	[mt_seq] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_resultcode]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_resultcode](
	[service_type] [char](2) NOT NULL,
	[rslt_code] [varchar](4) NOT NULL,
	[rslt_type] [char](1) NOT NULL,
	[rslt_name] [varchar](80) NOT NULL,
	[rslt_pname] [varchar](80) NULL,
PRIMARY KEY CLUSTERED 
(
	[service_type] ASC,
	[rslt_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_mmt_tran]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_mmt_tran](
	[mt_pr] [int] IDENTITY(1,1) NOT NULL,
	[mt_refkey] [varchar](20) NULL,
	[priority] [char](2) NOT NULL,
	[msg_class] [char](1) NULL,
	[date_client_req] [datetime] NOT NULL,
	[subject] [varchar](40) NOT NULL,
	[content_type] [int] NULL,
	[content] [varchar](4000) NOT NULL,
	[attach_file_group_key] [int] NULL,
	[callback] [varchar](25) NOT NULL,
	[service_type] [char](2) NOT NULL,
	[broadcast_yn] [char](1) NOT NULL,
	[msg_status] [char](1) NOT NULL,
	[recipient_num] [varchar](25) NULL,
	[date_mt_sent] [datetime] NULL,
	[date_rslt] [datetime] NULL,
	[date_mt_report] [datetime] NULL,
	[mt_report_code_ib] [char](4) NULL,
	[mt_report_code_ibtype] [char](1) NULL,
	[carrier] [int] NULL,
	[rs_id] [varchar](20) NULL,
	[recipient_net] [int] NULL,
	[recipient_npsend] [varchar](1) NULL,
	[country_code] [varchar](8) NOT NULL,
	[charset] [varchar](20) NULL,
	[msg_type] [int] NULL,
	[crypto_yn] [char](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[mt_pr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_mmt_file]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_mmt_file](
	[attach_file_group_key] [int] NOT NULL,
	[attach_file_group_seq] [int] NOT NULL,
	[attach_file_seq] [int] NOT NULL,
	[attach_file_subpath] [varchar](64) NULL,
	[attach_file_name] [varchar](64) NOT NULL,
	[attach_file_carrier] [int] NULL,
	[reg_date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[attach_file_group_key] ASC,
	[attach_file_group_seq] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_mmt_client]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_mmt_client](
	[mt_pr] [int] NOT NULL,
	[mt_seq] [int] NOT NULL,
	[msg_status] [char](1) NOT NULL,
	[recipient_num] [varchar](25) NOT NULL,
	[change_word1] [varchar](20) NULL,
	[change_word2] [varchar](20) NULL,
	[change_word3] [varchar](20) NULL,
	[change_word4] [varchar](20) NULL,
	[change_word5] [varchar](20) NULL,
	[date_mt_sent] [datetime] NULL,
	[date_rslt] [datetime] NULL,
	[date_mt_report] [datetime] NULL,
	[mt_report_code_ib] [char](4) NULL,
	[mt_report_code_ibtype] [char](1) NULL,
	[carrier] [int] NULL,
	[rs_id] [varchar](20) NULL,
	[recipient_net] [int] NULL,
	[recipient_npsend] [varchar](1) NULL,
	[country_code] [varchar](8) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[mt_pr] ASC,
	[mt_seq] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[em_banlist]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[em_banlist](
	[service_type] [char](2) NOT NULL,
	[ban_seq] [int] NOT NULL,
	[ban_type] [char](1) NOT NULL,
	[content] [varchar](45) NOT NULL,
	[send_yn] [char](1) NOT NULL,
	[ban_status_yn] [char](1) NOT NULL,
	[reg_date] [datetime] NOT NULL,
	[reg_user] [varchar](20) NULL,
	[update_date] [datetime] NOT NULL,
	[update_user] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[service_type] ASC,
	[ban_seq] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[call_postbox]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[call_postbox](
	[CALLID] [int] IDENTITY(1,1) NOT NULL,
	[DIRECTIVE] [varchar](64) NULL,
	[ENTITY] [varchar](64) NULL,
	[VALUESTRING] [varchar](4096) NULL,
	[CALLER] [varchar](64) NULL,
	[CREATETIME] [datetime] NULL,
	[CALLTIME] [datetime] NULL,
	[CHECKFLAG] [int] NULL,
	[CALLFLAG] [int] NULL,
	[ERRORFLAG] [int] NULL,
	[REMARKS] [varchar](64) NULL,
 CONSTRAINT [PK_call_postbox] PRIMARY KEY CLUSTERED 
(
	[CALLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[GetClassValue]    Script Date: 07/24/2012 19:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetClassValue]
(@inString varchar(255), @index int)
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @resultString varchar(255), @tempString varchar(255)
declare @oPos int, @nPos int, @curIndex int

set @oPos = 1 -- 구분문자 검색을 시작할 위치
set @nPos = 1 -- 구분문자 위치
set @curIndex = 1

while(@nPos > 0)
begin
  set @nPos = charindex('/', @inString, @oPos)
  
  if @nPos = 0
    set @tempString = right(@inString, len(@inString) - @oPos+1)
  else
    set @tempString = substring(@inString, @oPos, @nPos - @oPos)
    
  if @index = @curIndex
    set @resultString = @tempString
  
  set @curIndex = @curIndex + 1;
  set @oPos = @nPos + 1;
end

if len(@resultString) > 0
  set @resultString = right(@resultString, len(@resultString) - (charindex('=', @resultString)))

return @resultString
END
GO
/****** Object:  Table [dbo].[reply_postbox]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reply_postbox](
	[CALLID] [int] NOT NULL,
	[REPLYID] [int] NOT NULL,
	[ENTITY] [varchar](64) NULL,
	[VALUESTRING] [varchar](4096) NULL,
	[CREATETIME] [datetime] NULL,
	[CHECKFLAG] [int] NULL,
	[ERRORFLAG] [int] NULL,
	[REMARKS] [varchar](64) NULL,
 CONSTRAINT [PK_reply_postbox] PRIMARY KEY CLUSTERED 
(
	[CALLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[Password_Encode]    Script Date: 07/24/2012 19:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--'*********************************************************************************    
--'프로시저명 : dbo.Password_Encode
--'프로시저기능설명 : base64암호화 
--'작성자 : ?
--'작성일 : 2007-03-28
--'수정자 : 임유석
--'수정일 : 2011-08-01
--'수정내용 :

--'*********************************************************************************  

CREATE FUNCTION [dbo].[Password_Encode]
(
 @InputStrings varchar(50)
)
RETURNS varchar(50)
AS
BEGIN
 DECLARE  @ConvertTable varchar(128)
  ,@ReturnStrings varchar(50)
  ,@InputBinary varbinary(50)
  ,@InputSize int
  ,@Count  int

  ,@Before1 binary(1)
  ,@Before2 binary(1)
  ,@Before3 binary(1)
  ,@After1 int
  ,@After2 int
  ,@After3 int
  ,@After4 int
 
  SET @ConvertTable = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    + 'abcdefghijklmnopqrstuvwxyz'
    + '0123456789+/='
  SET @InputBinary = CAST(@InputStrings AS varbinary(50))
  SET @InputSize = DATALENGTH(@InputBinary)
  SET @Count = 1
  SET @ReturnStrings = ''

IF @InputStrings <>'' or @InputStrings is not null
BEGIN

  WHILE (0=0) BEGIN
   IF @Count > @InputSize BREAK
 
   SET @Before1 = SUBSTRING(@InputBinary, @Count, 1)
 
   IF @Count + 1 > @InputSize BEGIN
    SET @Before2 = NULL
   END
   ELSE BEGIN
    SET @Before2 = SUBSTRING(@InputBinary, @Count + 1, 1)
   END
 
   IF @Count + 2 > @InputSize BEGIN
    SET @Before3 = NULL
   END
   ELSE BEGIN
    SET @Before3 = SUBSTRING(@InputBinary, @Count + 2, 1)
   END
 
   SET @After1 = (@Before1 & 252) /  4
 
   SET @After2 = (@Before1 &   3) * 16
    + (ISNULL(@Before2, 0x00) & 240) / 16
 
   SET @After3 = (@Before2 &  15) *  4
    + (ISNULL(@Before3, 0x00) & 192) / 64
 
   SET @After4 = (ISNULL(@Before3, 0x00) &  63) *  1
 
   SET @ReturnStrings = @ReturnStrings
    + SUBSTRING(@ConvertTable, @After1 + 1, 1)
    + SUBSTRING(@ConvertTable, @After2 + 1, 1)
    + CASE WHEN @Before2 IS NULL AND @Before3 IS NULL
     THEN '='
     ELSE SUBSTRING(@ConvertTable, @After3 + 1, 1)
      END
    + CASE WHEN @Before3 IS NULL
     THEN '='
     ELSE SUBSTRING(@ConvertTable, @After4 + 1, 1)
      END
 
   SET @Count = @Count + 3
  END
 

END

 RETURN @ReturnStrings

END
GO
/****** Object:  UserDefinedFunction [dbo].[Password_Decode]    Script Date: 07/24/2012 19:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--'*********************************************************************************    
--'프로시저명 : dbo.Password_Decode
--'프로시저기능설명 : base64암호화 해석
--'작성자 : 강동수
--'작성일 : 2007-03-28
--'수정자 : 임유석
--'수정일 : 2011-08-01
--'수정내용 :

--'*********************************************************************************  

CREATE      FUNCTION [dbo].[Password_Decode]
(
 @InputStrings varchar(50)
)
RETURNS varchar(50)
AS
BEGIN

 DECLARE  @ConvertTable binary(100)
  ,@ReturnStrings varchar(50)
  ,@InputSize int
  ,@Count  int
  ,@Before1 varchar(100)
  ,@Before2 varchar(100)
  ,@Before3 varchar(100)
  ,@After1 int
  ,@After2 int
  ,@After3 int
  ,@After4 int
  ,@cut_data varchar(4)


IF @InputStrings <>'' or @InputStrings is not null
BEGIN

  SET @ConvertTable = CAST('ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    + 'abcdefghijklmnopqrstuvwxyz'
    + '0123456789+/=' AS binary(100))
 
 
  SET @InputSize = DATALENGTH(@InputStrings)
  SET @Count = 1
  SET @ReturnStrings = ''
 
  WHILE (0=0) BEGIN
	IF @Count > @InputSize BREAK

	SET @cut_data = SUBSTRING(@InputStrings, @Count, 4)
	SET @After1 = CHARINDEX( CAST(SUBSTRING(@cut_data, 1, 1) as binary(1)),@ConvertTable) - 1
	SET @After2 = CHARINDEX( CAST(SUBSTRING(@cut_data, 2, 1) as binary(1)),@ConvertTable) - 1
	SET @After3 = CHARINDEX( CAST(SUBSTRING(@cut_data, 3, 1) as binary(1)),@ConvertTable) - 1
	SET @After4 = CHARINDEX( CAST(SUBSTRING(@cut_data, 4, 1) as binary(1)),@ConvertTable) - 1
	SET @Before1 = NCHAR(((@After2 & 48) / 16) | (@After1 * 4) & 255)
	SET @Before2 = NCHAR(((@After3 & 60) / 4) | (@After2 * 16) & 255)
	SET @Before3 = NCHAR((((@After3 & 3) * 64) & 255) | (@After4 & 63))
	
	--SET @ReturnStrings = @ReturnStrings + @Before1 + @Before2 + @Before3
	IF @Before1 <> '' and @Before1 is not null
	BEGIN
		SET @ReturnStrings = @ReturnStrings + @Before1
	END
	IF @Before2 <> '' and @Before2 is not null
	BEGIN
		SET @ReturnStrings = @ReturnStrings + @Before2
	END
	IF @Before3 <> '' and @Before3 is not null
	BEGIN
		SET @ReturnStrings = @ReturnStrings + @Before3
	END

	SET @Count = @Count + 4
  END

END

  RETURN @ReturnStrings

END
GO
/****** Object:  Table [dbo].[ubc_web_menu_middle]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_web_menu_middle](
	[COMP_CODE] [varchar](20) NOT NULL,
	[MIDDLE_CODE] [varchar](20) NOT NULL,
	[MENU_NAME] [varchar](100) NULL,
	[PROG_TYPE] [varchar](20) NULL,
	[SORT_NUM] [decimal](3, 0) NULL,
	[PROG_ID] [varchar](100) NULL,
	[LARGE_CODE] [varchar](20) NULL,
	[USE_FLAG] [varchar](20) NULL,
	[VISIBLE_FLAG] [varchar](1) NULL,
	[LANG_FLAG] [varchar](2) NOT NULL,
	[USER_TYPE] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_web_menu_large]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_web_menu_large](
	[COMP_CODE] [varchar](20) NOT NULL,
	[LARGE_CODE] [varchar](20) NOT NULL,
	[MENU_NAME] [varchar](100) NULL,
	[PROG_TYPE] [varchar](20) NULL,
	[SORT_NUM] [int] NULL,
	[PROG_ID] [varchar](100) NULL,
	[USE_FLAG] [varchar](20) NULL,
	[VISIBLE_FLAG] [varchar](1) NULL,
	[LANG_FLAG] [varchar](2) NOT NULL,
	[USER_TYPE] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_web_boardAttach]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_web_boardAttach](
	[attachId] [int] IDENTITY(1,1) NOT NULL,
	[boardId] [bigint] NULL,
	[filePath] [nvarchar](200) NULL,
	[fileName] [nvarchar](500) NULL,
	[size] [varchar](20) NULL,
	[createDate] [datetime] NULL,
 CONSTRAINT [PK_ubc_web_boardAttach] PRIMARY KEY CLUSTERED 
(
	[attachId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_web_board]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_web_board](
	[boardId] [bigint] IDENTITY(1,1) NOT NULL,
	[boardMasterId] [int] NULL,
	[thread] [int] NULL,
	[depth] [int] NULL,
	[userId] [nvarchar](100) NULL,
	[title] [nvarchar](500) NULL,
	[contents] [nvarchar](max) NULL,
	[contentCd] [nvarchar](10) NULL,
	[topYn] [bit] NULL,
	[popYn] [bit] NULL,
	[readCount] [int] NULL,
	[useYn] [bit] NULL,
	[ip] [nvarchar](50) NULL,
	[compType] [varchar](1) NULL,
	[createDate] [datetime] NULL,
	[updateDate] [datetime] NULL,
 CONSTRAINT [PK_ubc_web_board] PRIMARY KEY CLUSTERED 
(
	[boardId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_userLog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_userLog](
	[mgrId] [varchar](255) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[userId] [varchar](255) NOT NULL,
	[userLogId] [varchar](255) NULL,
	[loginTime] [datetime] NULL,
	[via] [varchar](50) NULL,
	[result] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_user]    Script Date: 07/24/2012 19:50:40 ******/
USE [ubc]
GO

/****** Object:  Table [dbo].[ubc_user]    Script Date: 01/18/2013 14:51:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ubc_user](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[userId] [varchar](255) NOT NULL,
	[password] [varchar](50) NULL,
	[userName] [varchar](50) NULL,
	[sid] [varchar](50) NULL,
	[mobileNo] [varchar](50) NULL,
	[phoneNo] [varchar](50) NULL,
	[zipCode] [varchar](50) NULL,
	[addr1] [varchar](255) NULL,
	[addr2] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[registerTime] [datetime] NULL,
	[userType] [decimal](5, 0) NULL,
	[siteList] [text] NULL,
	[hostList] [text] NULL,
	[probableCauseList] [text] NULL,
	[useEmail] [tinyint] NOT NULL,
	[useSms] [tinyint] NOT NULL,
	[roleId] [varchar](255) NULL,
	[validationDate] [datetime] NULL,
	[lastLoginTime] [datetime] NULL,
	[lastPWChangeTime] [datetime] NULL,
	[loginFailCount] [tinyint] NULL,
	[adminstate] [tinyint] NULL,
 CONSTRAINT [PK_ubc_user_1] PRIMARY KEY CLUSTERED 
(
	[siteId] ASC,
	[userId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_userType]  DEFAULT ((3)) FOR [userType]
GO

ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_useEmail]  DEFAULT ((0)) FOR [useEmail]
GO

ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_useSms]  DEFAULT ((0)) FOR [useSms]
GO

ALTER TABLE [dbo].[ubc_user] ADD  DEFAULT ((0)) FOR [loginFailCount]
GO

ALTER TABLE [dbo].[ubc_user] ADD  DEFAULT ((1)) FOR [adminstate]
GO

/****** Object:  Table [dbo].[ubc_tp]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_tp](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[bpId] [varchar](255) NOT NULL,
	[tpId] [varchar](255) NOT NULL,
	[createTime] [datetime] NULL,
	[startTime] [varchar](20) NULL,
	[endTime] [varchar](20) NULL,
	[programId] [varchar](255) NULL,
	[zorder] [varchar](128) NULL,
	[state] [tinyint] NOT NULL,
 CONSTRAINT [PK_ubc_tp_1] PRIMARY KEY CLUSTERED 
(
	[tpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[siteId] ASC,
	[bpId] ASC,
	[tpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_th]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_th](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[bpId] [varchar](255) NOT NULL,
	[thId] [varchar](255) NOT NULL,
	[targetHost] [varchar](255) NOT NULL,
	[side] [tinyint] NOT NULL,
 CONSTRAINT [PK_ubc_th] PRIMARY KEY CLUSTERED 
(
	[siteId] ASC,
	[bpId] ASC,
	[thId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_test]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_test](
	[mgrId] [varchar](50) NOT NULL,
	[customerId] [varchar](255) NOT NULL,
	[url] [text] NULL,
	[description] [varchar](255) NULL,
	[monitorState] [int] NOT NULL,
	[winPassword] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[customerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_template]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_template](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NOT NULL,
	[templateId] [varchar](255) NOT NULL,
	[width] [decimal](5, 0) NULL,
	[height] [decimal](5, 0) NULL,
	[bgColor] [varchar](255) NULL,
	[bgImage] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[shortCut] [varchar](45) NULL,
	[registerTime] [timestamp] NULL,
	[serialNo] [decimal](10, 0) IDENTITY(1,1) NOT NULL,
	[bgType] [int] NULL,
	[bgSerialNo] [decimal](10, 0) NULL,
 CONSTRAINT [PK_ubc_template_1] PRIMARY KEY CLUSTERED 
(
	[serialNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[programId] ASC,
	[templateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_smtplog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_smtplog](
	[mailid] [bigint] IDENTITY(1,1) NOT NULL,
	[company] [varchar](255) NULL,
	[send_email] [varchar](255) NULL,
	[receive_email] [varchar](max) NULL,
	[title] [varchar](255) NULL,
	[contents] [varchar](max) NULL,
	[success_yn] [char](1) NULL,
	[wdate] [datetime] NULL,
 CONSTRAINT [PK_ubc_smtplog] PRIMARY KEY CLUSTERED 
(
	[mailid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_site]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_site](
	[mgrId] [varchar](255) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[siteName] [varchar](255) NULL,
	[phoneNo1] [varchar](255) NULL,
	[phoneNo2] [varchar](255) NULL,
	[mobileNo] [varchar](255) NULL,
	[faxNo] [varchar](255) NULL,
	[franchizeType] [varchar](255) NOT NULL,
	[chainNo] [varchar](255) NULL,
	[businessType] [varchar](255) NULL,
	[businessCode] [varchar](255) NULL,
	[ceoName] [varchar](255) NULL,
	[siteLevel] [decimal](5, 0) NULL,
	[shopOpenTime] [varchar](255) NULL,
	[shopCloseTime] [varchar](255) NULL,
	[holiday] [varchar](255) NULL,
	[comment1] [varchar](255) NULL,
	[comment2] [varchar](255) NULL,
	[comment3] [varchar](255) NULL,
	[zipCode] [varchar](255) NULL,
	[addr1] [varchar](255) NULL,
	[addr2] [varchar](255) NULL,
	[addr3] [varchar](255) NULL,
	[addr4] [varchar](255) NULL,
	[parentId] [varchar](255) NULL,
 CONSTRAINT [PK_ubc_site_1] PRIMARY KEY CLUSTERED 
(
	[siteId] ASC,
	[franchizeType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_screenshot]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_screenshot](
	[hostId] [varchar](255) NOT NULL,
	[createTime] [varchar](20) NOT NULL,
	[fileName] [varchar](255) NOT NULL,
	[url] [varchar](45) NOT NULL,
 CONSTRAINT [PK_ubc_screenshot] PRIMARY KEY CLUSTERED 
(
	[fileName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_schedulestatelog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_schedulestatelog](
	[mgrId] [varchar](255) NOT NULL,
	[logId] [varchar](255) NOT NULL,
	[eventTime] [datetime] NULL,
	[siteId] [varchar](255) NULL,
	[hostId] [varchar](255) NULL,
	[probableCause] [varchar](255) NULL,
	[additionalText] [varchar](255) NULL,
	[autoSchedule1] [varchar](255) NULL,
	[autoSchedule2] [varchar](255) NULL,
	[currentSchedule1] [varchar](255) NULL,
	[currentSchedule2] [varchar](255) NULL,
	[lastSchedule1] [varchar](255) NULL,
	[lastSchedule2] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_schedule]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_schedule](
	[mgrId] [varchar](128) NOT NULL,
	[siteId] [varchar](128) NOT NULL,
	[programId] [varchar](128) NOT NULL,
	[templateId] [varchar](128) NOT NULL,
	[frameId] [varchar](128) NOT NULL,
	[scheduleId] [varchar](128) NOT NULL,
	[requestId] [varchar](255) NULL,
	[contentsId] [varchar](255) NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[startTime] [varchar](50) NULL,
	[endTime] [varchar](50) NULL,
	[toTime] [datetime] NULL,
	[fromTime] [datetime] NULL,
	[openningFlag] [decimal](1, 0) NULL,
	[priority] [decimal](5, 0) NULL,
	[castingState] [decimal](3, 0) NULL,
	[castingStateTime] [datetime] NULL,
	[castingStateHistory] [varchar](255) NULL,
	[result] [varchar](255) NULL,
	[operationalState] [decimal](1, 0) NULL,
	[adminState] [decimal](1, 0) NULL,
	[phoneNumber] [varchar](255) NULL,
	[isOrigin] [decimal](1, 0) NULL,
	[comment1] [varchar](255) NULL,
	[comment2] [varchar](255) NULL,
	[comment3] [varchar](255) NULL,
	[serialNo] [decimal](10, 0) IDENTITY(1,1) NOT NULL,
	[runningTime] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_schedule] PRIMARY KEY CLUSTERED 
(
	[serialNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[programId] ASC,
	[templateId] ASC,
	[frameId] ASC,
	[scheduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_rolemap]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_rolemap](
	[mgrId] [varchar](255) NOT NULL,
	[rolemapId] [varchar](255) NOT NULL,
	[roleId] [varchar](255) NOT NULL,
	[menuId] [varchar](255) NOT NULL,
	[menuType] [varchar](10) NOT NULL,
	[customer] [varchar](45) NOT NULL,
 CONSTRAINT [PK_ubc_rolemap_1] PRIMARY KEY CLUSTERED 
(
	[rolemapId] ASC,
	[customer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_role]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_role](
	[mgrId] [varchar](255) NOT NULL,
	[roleId] [varchar](255) NOT NULL,
	[roleName] [varchar](255) NOT NULL,
	[roleDesc] [varchar](255) NULL,
	[customer] [varchar](45) NOT NULL,
 CONSTRAINT [PK_ubc_role_1] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC,
	[customer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_reservation]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_reservation](
	[mgrId] [varchar](10) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[reservationId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NULL,
	[side] [tinyint] NULL,
	[fromTime] [datetime] NULL,
	[toTime] [datetime] NULL,
	[tpEntity] [varchar](255) NULL,
	[thEntity] [varchar](255) NULL,
 CONSTRAINT [PK_ubc_reservation_1] PRIMARY KEY CLUSTERED 
(
	[siteId] ASC,
	[hostId] ASC,
	[reservationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_programlocation]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_programlocation](
	[mgrId] [varchar](255) NOT NULL,
	[programLocationId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NOT NULL,
	[pmId] [varchar](255) NOT NULL,
 CONSTRAINT [PK_ubc_programlocation_1] PRIMARY KEY CLUSTERED 
(
	[programLocationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_program]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_program](
	[mgrId] [varchar](255) NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NOT NULL,
	[lastUpdateTime] [datetime] NULL,
	[lastUpdateId] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[templatePlayList] [text] NULL,
	[serialNo] [decimal](10, 0) IDENTITY(1,1) NOT NULL,
	[contentsCategory] [smallint] NULL,
	[purpose] [smallint] NULL,
	[hostType] [smallint] NULL,
	[vertical] [smallint] NULL,
	[resolution] [smallint] NULL,
	[isPublic] [smallint] NULL,
	[validationDate] [datetime] NULL,
	[isVerify] [smallint] NULL,
	[volume] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_ubc_program_1] PRIMARY KEY CLUSTERED 
(
	[serialNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_powerstatelog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_powerstatelog](
	[mgrId] [varchar](10) NOT NULL,
	[logId] [varchar](255) NOT NULL,
	[eventTime] [datetime] NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[probableCause] [varchar](255) NULL,
	[additionalText] [varchar](255) NULL,
	[bootUpTime] [datetime] NOT NULL,
	[bootDownTime] [datetime] NOT NULL,
 CONSTRAINT [PK__ubc_powe__54C464F2095F58DF] PRIMARY KEY NONCLUSTERED 
(
	[siteId] ASC,
	[hostId] ASC,
	[bootUpTime] ASC,
	[bootDownTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_pmo]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_pmo](
	[mgrId] [varchar](255) NOT NULL,
	[pmId] [varchar](255) NOT NULL,
	[ipAddress] [varchar](255) NOT NULL,
	[ftpId] [varchar](255) NULL,
	[ftpPasswd] [varchar](255) NULL,
	[ftpPort] [decimal](10, 0) NOT NULL,
	[socketPort] [decimal](10, 0) NULL,
	[ipAddress1] [varchar](45) NULL,
	[ipAddress2] [varchar](45) NULL,
	[ftpAddress] [varchar](45) NULL,
	[ftpAddress1] [varchar](45) NULL,
	[ftpAddress2] [varchar](45) NULL,
	[ftpMirror] [varchar](45) NULL,
	[ftpMirror1] [varchar](45) NULL,
	[ftpMirror2] [varchar](45) NULL,
	[currentConnectionCount] [int] NULL,
	[lastUpdateTime] [datetime] NULL,
	[adminstate] [tinyint] NULL,
 CONSTRAINT [PK_ubc_pmo_1] PRIMARY KEY CLUSTERED 
(
	[pmId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_playlog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_playlog](
	[playDate] [smalldatetime] NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NOT NULL,
	[contentsId] [varchar](255) NOT NULL,
	[contentsName] [varchar](255) NULL,
	[filename] [varchar](255) NOT NULL,
	[playCount] [decimal](10, 0) NOT NULL,
	[playTime] [decimal](10, 0) NOT NULL,
	[planTime] [decimal](10, 0) NOT NULL,
	[failCount] [decimal](10, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_mir]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_mir](
	[classScopedName] [varchar](255) NOT NULL,
	[attrName] [varchar](255) NOT NULL,
	[attrType] [varchar](255) NOT NULL,
	[className] [varchar](255) NULL,
	[indexOrder] [int] NOT NULL,
 CONSTRAINT [PK_ubc_mir] PRIMARY KEY CLUSTERED 
(
	[classScopedName] ASC,
	[attrName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_menuauth]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_menuauth](
	[mgrId] [varchar](255) NOT NULL,
	[menuId] [varchar](255) NOT NULL,
	[menuName] [varchar](255) NOT NULL,
	[menuType] [varchar](10) NOT NULL,
	[cAuth] [tinyint] NOT NULL,
	[rAuth] [tinyint] NOT NULL,
	[uAuth] [tinyint] NOT NULL,
	[dAuth] [tinyint] NOT NULL,
	[aAuth] [tinyint] NOT NULL,
	[customer] [varchar](45) NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[menuId] ASC,
	[customer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_lmo_solution]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_lmo_solution](
	[mgrId] [varchar](255) NOT NULL,
	[lmId] [varchar](255) NOT NULL,
	[description] [varchar](255) NULL,
	[tableName] [varchar](255) NULL,
	[timeFieldName] [varchar](255) NULL,
	[logType] [decimal](1, 0) NULL,
	[duration] [decimal](5, 0) NULL,
	[adminState] [decimal](1, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[lmId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_lmo]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_lmo](
	[mgrId] [varchar](10) NOT NULL,
	[lmId] [varchar](255) NOT NULL,
	[duration] [decimal](5, 0) NULL,
	[powerState] [decimal](1, 0) NULL,
	[connectionState] [decimal](1, 0) NULL,
	[scheduleState] [decimal](1, 0) NULL,
	[faultState] [decimal](1, 0) NULL,
	[loginState] [decimal](1, 0) NULL,
	[estimateState] [decimal](1, 0) NULL,
	[downloadState] [decimal](1, 0) NULL,
 CONSTRAINT [PK_ubc_lmo] PRIMARY KEY CLUSTERED 
(
	[lmId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_lastscreenshot]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_lastscreenshot](
	[hostId] [varchar](255) NOT NULL,
	[fullstring] [varchar](255) NOT NULL,
 CONSTRAINT [PK_ubc_lastscreenshot] PRIMARY KEY CLUSTERED 
(
	[hostId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_key_zorder]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_zorder](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_zorder] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_tpid]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_tpid](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_tpid] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_thid]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_thid](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_thid] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_reservationid]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_reservationid](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_reservationid] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_id]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_id](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_id] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_hostautoupdateid]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_hostautoupdateid](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_hostautoupdateid] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_faultid]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_faultid](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_faultid] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_downloadid]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_downloadid](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_downloadid] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_bpid]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_bpid](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_bpid] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_key_announceid]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ubc_key_announceid](
	[keyval] [decimal](10, 0) NOT NULL,
 CONSTRAINT [PK_ubc_key_announceid] PRIMARY KEY CLUSTERED 
(
	[keyval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ubc_interactivelog]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_interactivelog](
	[playDate] [datetime] NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NOT NULL,
	[contentsId] [varchar](255) NOT NULL,
	[keyword1] [varchar](255) NULL,
	[keyword2] [varchar](255) NULL,
	[keyword3] [varchar](255) NULL,
	[counter] [int] NOT NULL,
	[keyword4] [varchar](255) NULL,
	[keyword5] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ubc_interactive_backup]    Script Date: 07/24/2012 19:50:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ubc_interactive_backup](
	[playDate] [date] NOT NULL,
	[siteId] [varchar](255) NOT NULL,
	[hostId] [varchar](255) NOT NULL,
	[programId] [varchar](255) NOT NULL,
	[contentsId] [varchar](255) NOT NULL,
	[keyword1] [varchar](255) NULL,
	[keyword2] [varchar](255) NULL,
	[keyword3] [varchar](255) NULL,
	[counter] [int] NOT NULL,
	[keyword4] [varchar](255) NULL,
	[keyword5] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[ubc_hostView]    Script Date: 07/24/2012 19:50:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ubc_hostView] AS SELECT h.*, s.siteName,s.chainNo,s.businessCode,s.businessType, s.phoneNo1 from (dbo.ubc_host h left join dbo.ubc_site s on((h.siteId = s.siteId)));
GO
/****** Object:  UserDefinedFunction [dbo].[GetSiteName]    Script Date: 07/24/2012 19:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetSiteName]
(@siteId varchar(255))
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @siteName varchar(255)
select @siteName = siteName from ubc_site where siteId = @siteId
return @siteName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetHostName]    Script Date: 07/24/2012 19:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetHostName]
(@hostId varchar(255))
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @hostName varchar(255)
select @hostName = hostName from ubc_host where hostId = @hostId
return @hostName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetContentsName]    Script Date: 07/24/2012 19:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetContentsName]
(@contentsId varchar(255))
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @contentsName varchar(255)
select @contentsName = contentsName from ubc_contents where contentsId = @contentsId
return @contentsName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetChildSite]    Script Date: 07/24/2012 19:50:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetChildSite]
(@franchizeType varchar(255), @siteId varchar(255))
RETURNS table
AS
return (
with cte (mgrId, siteName, parentId, siteId, lvl, name_path, id_path, franchizeType)
as
(
select 1 as mgrId, siteName, parentId, siteId, 1 as lvl, convert(varchar(255), '/' + siteName) name_path, convert(varchar(255), '/' + siteId) id_path, franchizeType
from ubc_site
--where parentId is null
where siteId = @siteId and franchizeType = @franchizeType
union all
select 1 as mgrId, a.siteName, a.parentId, a.siteId, lvl + 1, convert(varchar(255),  name_path + '/' + a.siteName) name_path, convert(varchar(255),  id_path + '/' + a.siteId) id_path, a.franchizeType
from ubc_site a, cte b
where a.parentId = b.siteId and a.franchizeType = b.franchizeType
)
select mgrId, siteName, siteId, parentId, lvl, name_path, id_path, franchizeType from cte
--order by lvl
)
GO
/****** Object:  StoredProcedure [dbo].[sp_em_stat_delete]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_stat_delete]
    @p_service_type char(2),
    @p_prc_date     varchar(8)
AS 

    EXEC sp_em_stat_create

	--BEGIN TRAN

    /* delete em_statistics_m */
	DELETE FROM em_statistics_m
	WHERE stat_date = @p_prc_date
	AND stat_servicetype = @p_service_type;

    if @@error != 0
    begin
        --rollback tran
        return
    end

    /* delete em_statistics_d */
	DELETE FROM em_statistics_d
	WHERE stat_date = @p_prc_date
	AND stat_servicetype = @p_service_type;

    if @@error != 0
    begin
        --rollback tran
        return
    end

    --COMMIT TRAN

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_stat_mo_insert]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_stat_mo_insert]
    @p_service_type char(2)
AS
    DECLARE @v_prc_date		varchar(8),
			@v_table_name	varchar(20),
            @dsql			nvarchar(4000)
   
	SELECT @v_prc_date = CONVERT(varchar, getdate(), 112)	

	IF @p_service_type = '4'
		SELECT @v_table_name = 'em_mo_log_' + substring(@v_prc_date, 1, 6)
	ELSE IF @p_service_type = '5'
		SELECT @v_table_name = 'em_mo_log_' + substring(@v_prc_date, 1, 6)

	EXEC sp_em_stat_create

	IF EXISTS (
		SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @v_table_name
	)

	BEGIN

	/** insert em_statistics_m */
    SELECT  @dsql = '
        INSERT INTO em_statistics_m (
			stat_date,
			stat_servicetype,
			stat_payment_code,
			stat_carrier,
			stat_success,
			stat_failure,
			stat_invalid,
			stat_invalid_ib,
			stat_remained,
			stat_regdate
		) SELECT 
			'''+@v_prc_date+''',
			'''+@p_service_type+''',
			''NONE'',
			carrier,
			SUM(CASE WHEN msg_status = ''3'' THEN 1 ELSE 0 END) AS success,
			SUM(CASE WHEN msg_status <> ''3'' THEN 1 ELSE 0 END) AS failure,
			0 AS invalid,
			0 AS invalid_ib,
			0 AS remained,
			getdate()
		FROM '+@v_table_name+'
		WHERE convert(varchar, date_mo_recv, 112) = '''+@v_prc_date+'''
		AND service_type = '''+@p_service_type+'''
		GROUP BY carrier '

	EXEC (@dsql)

	END

RETURN
GO
/****** Object:  StoredProcedure [dbo].[usp_User_Update_notPass]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_User_Update_notPass]
(

	@userId                varchar(255) ,
	@siteId                varchar(255) , 
	@userName			   varchar(50)  ,
	@mobileNo              varchar(50)  , 
	@email                 varchar(255) ,
	@userType              decimal(5,0) ,
	@roleId                varchar(255) ,
	@validationDate        datetime ,
	@useEmail              tinyint  ,
	@useSms                tinyint  ,
	@probableCauseList     text ,
	@siteList              text ,
	@hostList              text
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
 
 
UPDATE  ubc_user
  SET
    userName	  	  =	@userName  
  , mobileNo          =	@mobileNo         
  , email             =	@email            
  , userType          =	@userType         
  , roleId            =	@roleId           
  , validationDate    =	@validationDate   
  , useEmail          =	@useEmail         
  , useSms            =	@useSms           
  , probableCauseList =	@probableCauseList
  , siteList          =	@siteList         
  , hostList          =	@hostList         
  WHERE  
       siteId = @siteId 
  AND  userId = @userId 
 
 
 
 
 
 
-- DECLARE @dsql  varchar(1000)
-- DECLARE @passSql  varchar(1000)
-- DECLARE @v_params as  varchar(200)

--	IF @password = ''
--        SET @passSql = ' '
--    ELSE
--        SET @passSql = ' , password = @Ppassword '

 
--SET @dsql = 
--' UPDATE  ubc_user
--  SET
--    userName	  	  =	@PuserName 
--' 
--  +@passSql+
--' , mobileNo          =	@PmobileNo         
--  , email             =	@Pemail            
--  , userType          =	@PuserType         
--  , roleId            =	@ProleId           
--  , validationDate    =	@PvalidationDate   
--  , useEmail          =	@vuseEmail         
--  , useSms            =	@PuseSms           
--  , probableCauseList =	@PprobableCauseList
--  , siteList          =	@PsiteList         
--  , hostList          =	@PhostList         
--  WHERE  
--       siteId = @PsiteId 
--  AND  userId = @PuserId
--'

--SET @v_params = 
--   '@PuserId                varchar(255) ,
--	@PsiteId                varchar(255) ,
--	@Ppassword			   varchar(50)  ,
--	@PuserName			   varchar(50)  ,
--	@PmobileNo              varchar(50)  , 
--	@Pemail                 varchar(255) ,
--	@PuserType              decimal(5,0) ,
--	@ProleId                varchar(255) ,
--	@PvalidationDate        datetime ,
--	@PuseEmail              tinyint  ,
--	@PuseSms                tinyint  ,
--	@PprobableCauseList     text ,
--	@PsiteList              text ,
--	@PhostList              text
--   '	 
			
--EXECUTE sp_executesql @dsql
--, @v_params, 
--    @PuserId              =     @userId     ,        
--	@PsiteId              =  	@siteId      ,       
--	@Ppassword			  =  	@password			,     
--	@PuserName			  =  	@userName			 ,    
--	@PmobileNo            =  	@mobileNo        ,   
--	@Pemail               =  	@email            ,  
--	@PuserType            =  	@userType          , 
--	@ProleId              =  	@roleId             ,
--	@PvalidationDate      =  	@validationDate     ,
--	@PuseEmail            =  	@useEmail           ,
--	@PuseSms              =  	@useSms             ,
--	@PprobableCauseList   =  	@probableCauseList  ,
--	@PsiteList            =  	@siteList           ,
--	@PhostList            =  	@hostList            
			
			
			
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_User_Update]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_User_Update]
(

	@userId                varchar(255) ,
	@siteId                varchar(255) ,
	@password			   varchar(50)  ,
	@userName			   varchar(50)  ,
	@mobileNo              varchar(50)  , 
	@email                 varchar(255) ,
	@userType              decimal(5,0) ,
	@roleId                varchar(255) ,
	@validationDate        datetime ,
	@useEmail              tinyint  ,
	@useSms                tinyint  ,
	@probableCauseList     text ,
	@siteList              text ,
	@hostList              text
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
 
 
UPDATE  ubc_user
  SET
    userName	  	  =	@userName 
  , password          = dbo.Password_Encode( @password )
  , mobileNo          =	@mobileNo         
  , email             =	@email            
  , userType          =	@userType         
  , roleId            =	@roleId           
  , validationDate    =	@validationDate   
  , useEmail          =	@useEmail         
  , useSms            =	@useSms           
  , probableCauseList =	@probableCauseList
  , siteList          =	@siteList         
  , hostList          =	@hostList         
  WHERE  
       siteId = @siteId 
  AND  userId = @userId 
 
 
 
 
 
 
-- DECLARE @dsql  varchar(1000)
-- DECLARE @passSql  varchar(1000)
-- DECLARE @v_params as  varchar(200)

--	IF @password = ''
--        SET @passSql = ' '
--    ELSE
--        SET @passSql = ' , password = @Ppassword '

 
--SET @dsql = 
--' UPDATE  ubc_user
--  SET
--    userName	  	  =	@PuserName 
--' 
--  +@passSql+
--' , mobileNo          =	@PmobileNo         
--  , email             =	@Pemail            
--  , userType          =	@PuserType         
--  , roleId            =	@ProleId           
--  , validationDate    =	@PvalidationDate   
--  , useEmail          =	@vuseEmail         
--  , useSms            =	@PuseSms           
--  , probableCauseList =	@PprobableCauseList
--  , siteList          =	@PsiteList         
--  , hostList          =	@PhostList         
--  WHERE  
--       siteId = @PsiteId 
--  AND  userId = @PuserId
--'

--SET @v_params = 
--   '@PuserId                varchar(255) ,
--	@PsiteId                varchar(255) ,
--	@Ppassword			   varchar(50)  ,
--	@PuserName			   varchar(50)  ,
--	@PmobileNo              varchar(50)  , 
--	@Pemail                 varchar(255) ,
--	@PuserType              decimal(5,0) ,
--	@ProleId                varchar(255) ,
--	@PvalidationDate        datetime ,
--	@PuseEmail              tinyint  ,
--	@PuseSms                tinyint  ,
--	@PprobableCauseList     text ,
--	@PsiteList              text ,
--	@PhostList              text
--   '	 
			
--EXECUTE sp_executesql @dsql
--, @v_params, 
--    @PuserId              =     @userId     ,        
--	@PsiteId              =  	@siteId      ,       
--	@Ppassword			  =  	@password			,     
--	@PuserName			  =  	@userName			 ,    
--	@PmobileNo            =  	@mobileNo        ,   
--	@Pemail               =  	@email            ,  
--	@PuserType            =  	@userType          , 
--	@ProleId              =  	@roleId             ,
--	@PvalidationDate      =  	@validationDate     ,
--	@PuseEmail            =  	@useEmail           ,
--	@PuseSms              =  	@useSms             ,
--	@PprobableCauseList   =  	@probableCauseList  ,
--	@PsiteList            =  	@siteList           ,
--	@PhostList            =  	@hostList            
			
			
			
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_User_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_User_List]
(
 
  	 @siteId	varchar(255) ,
  	 @ROOT_SITE_ID  varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

 

SELECT 
  A.mgrId
  ,A.siteId
  ,A.userId,dbo.Password_Decode(A.password),A.userName,A.sid,A.mobileNo,A.phoneNo
 ,A.zipCode,A.addr1,A.addr2,A.email,A.registerTime,A.userType,A.siteList,A.hostList
 ,A.probableCauseList,A.useEmail,A.useSms,A.roleId,A.validationDate
 ,b.siteName
FROM
ubc_user as A 
LEFT JOIN
ubc_site   as B
on 
 A.siteId = B.siteId  
 WHERE 
 A.siteId = @siteId
and A.userType <> -1
--siteId IN ('HYUNDAI','KIA')

--SELECT 
--		  A.mgrId
--		, A.siteId
--		, A.userId
--		, dbo.Password_Decode(A.password)
--		, A.userName
--		, A.sid
--		, A.mobileNo
--		, A.phoneNo
--		, A.zipCode
--		, A.addr1
--		, A.addr2
--		, A.email
--		, A.registerTime
--		, A.userType
--		, A.siteList
--		, A.hostList
--		, A.probableCauseList
--		, A.useEmail
--		, A.useSms
--		, A.roleId
--		, A.validationDate
--		, b.siteName
--FROM ubc_user as A 
--LEFT JOIN ubc_site as B on A.siteId = B.siteId and B.franchizeType = @ROOT_SITE_ID
--WHERE A.siteId = @siteId
--and A.userType <> -1
	 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_User_Insert]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_User_Insert]
(

	@userId                varchar(255) ,
	@siteId                varchar(255) ,
	@password			   varchar(50)  ,
	@userName			   varchar(50)  ,
	@mobileNo              varchar(50)  , 
	@email                 varchar(255) ,
	@userType              decimal(5,0) ,
	@roleId                varchar(255) ,
	@validationDate        datetime ,
	@useEmail              tinyint  ,
	@useSms                tinyint  ,
	@probableCauseList     text ,
	@siteList              text ,
	@hostList              text
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

DECLARE @mgrIdTemp  varchar(255) 
select  @mgrIdTemp = mgrId  from ubc_site where siteId = @siteId



INSERT INTO   ubc_user
(
mgrId , siteId , userId , password , userName , mobileNo , email , userType , roleId , validationDate
, useEmail , useSms , probableCauseList , siteList , hostList , registerTime 
)
VALUES
(
@mgrIdTemp , @siteId , @userId , dbo.Password_Encode(@password) , @userName , @mobileNo , @email , @userType , @roleId , @validationDate
, @useEmail , @useSms , @probableCauseList , @siteList , @hostList , GETDATE() 
) 

	 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_User_Dup]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_User_Dup]
(
	@userId  varchar(255),
	@siteId  varchar(255)
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	
select COUNT(userId) as userId  from ubc_user 
where  
userId = @userId  
	
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_User_Detail]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_User_Detail]
(
	 @siteId 	varchar(255)  , 
  	 @userId	varchar(255)  
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


 

select mgrId,siteId,userId,dbo.Password_Decode(password),userName,sid,mobileNo,phoneNo,zipCode,addr1,addr2,email,registerTime,userType,siteList,hostList,probableCauseList,useEmail,useSms,roleId,validationDate
from ubc_user where  siteId = @siteId and  userId = @userId


	 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_User_Delete]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_User_Delete]
(
	 @siteId 	varchar(255)  , 
  	 @userId	varchar(255)  
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


 
delete from ubc_user
where  siteId = @siteId and  userId = @userId
 

	 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_InterActive_Keyword]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-12-08
-- Description:	상호작용 통계의 메뉴 조회(keyword1,keyword2,keyword3)
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_InterActive_Keyword]
(
	@franchizeType		varchar(50),
	@totalVisitorString	varchar(50),
	@keyword1			varchar(50) = '',
	@keyword2			varchar(50) = '',
	@keyword3			varchar(50) = '',
	@keyword4			varchar(50) = ''
	
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


	IF @keyword1 = ''

		-- 1차 메뉴조회
		SELECT   keyword1 AS code
			   , keyword1 AS name
		  FROM ubc_interactivelog AS A
		 INNER JOIN ubc_site B 
					ON A.siteid = B.siteId AND B.franchizeType = @franchizeType
		 WHERE A.keyword1 <> @totalVisitorString
		 GROUP BY A.keyword1
		
	ELSE IF @keyword2 = ''
			
		-- 2차 메뉴조회	
		SELECT   keyword2 AS code
			   , keyword2 AS name
		  FROM ubc_interactivelog AS A
		 INNER JOIN ubc_site B 
					ON A.siteid = B.siteId AND B.franchizeType = @franchizeType
		 WHERE A.keyword1 = @keyword1
		   AND A.keyword2 IS NOT NULL
		   AND A.keyword2 <> ''
		 GROUP BY A.keyword1, A.keyword2				

	ELSE IF @keyword3 = ''
		-- 3차 메뉴조회
		SELECT   keyword3 AS code
			   , keyword3 AS name
		  FROM ubc_interactivelog AS A
		 INNER JOIN ubc_site B 
					ON A.siteid = B.siteId AND B.franchizeType = @franchizeType
		 WHERE A.keyword1 = @keyword1
		   AND A.keyword2 = @keyword2
		   AND A.keyword3 IS NOT NULL
		   AND A.keyword3 <> ''
		 GROUP BY A.keyword1, A.keyword2, A.keyword3
		
	ELSE IF @keyword4 = ''
			
		-- 4차 메뉴조회	
		SELECT   keyword4 AS code
			   , keyword4 AS name
		  FROM ubc_interactivelog AS A
		 INNER JOIN ubc_site B 
					ON A.siteid = B.siteId AND B.franchizeType = @franchizeType
		 WHERE A.keyword1 = @keyword1
		   AND A.keyword2 = @keyword2
		   AND A.keyword3 = @keyword3
		   AND A.keyword4 IS NOT NULL
		   AND A.keyword4 <> ''
		 GROUP BY A.keyword1, A.keyword2, A.keyword3, A.keyword4
		 
	ELSE
	
		-- 5차 메뉴조회	
		SELECT   keyword5 AS code
			   , keyword5 AS name
		  FROM ubc_interactivelog AS A
		 INNER JOIN ubc_site B 
					ON A.siteid = B.siteId AND B.franchizeType = @franchizeType
		 WHERE A.keyword1 = @keyword1
		   AND A.keyword2 = @keyword2
		   AND A.keyword3 = @keyword3
		   AND A.keyword4 = @keyword4
		   AND A.keyword5 IS NOT NULL
		   AND A.keyword5 <> ''
		 GROUP BY A.keyword1, A.keyword2, A.keyword3, A.keyword4, A.keyword5

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Smtp_Insert]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		 
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Smtp_Insert]
(
	 
	@company		varchar(255) , 
	@send_email		varchar(255) , 
	@receive_email	varchar(max) , 
	@title			varchar(255) , 
    @contents		varchar(max) , 
    @success_yn     char(1) 
	  
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

   
    
	INSERT INTO ubc_smtplog
	( company , send_email , receive_email , title , contents , success_yn )
	VALUES
	( @company , @send_email , @receive_email , @title , @contents , @success_yn  )	
		
	 
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Sms_Send]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Sms_Send]
( 
      @send_phone	        varchar(255)
    , @recive_phone			varchar(255)
    , @contents			    varchar(255)  
	
	
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

 
 
 -- service_type = 0 = SMS
insert into em_smt_tran 
(date_client_req, content, callback, service_type, broadcast_yn, msg_status, recipient_num) 
values
(getdate(), @contents , @send_phone, '0', 'N', '1', @recive_phone);

 
  
 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Site_User_Excel_Insert]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date: 2011-08-10 임유석 수정(인자추가)
-- Description:	사용자 엑셀등록
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Site_User_Excel_Insert]
(

	@userId                varchar(255) ,
	@siteId                varchar(255) ,
	@password			   varchar(50)  ,
	@userName			   varchar(50)  , 
  	@mobileNo              varchar(50)  , -- 2011-08-10 임유석 수정(인자추가)
	@email                 varchar(255) , -- 2011-08-10 임유석 수정(인자추가)
	@userType              decimal(5,0) , -- 2011-08-10 임유석 수정(인자추가)
	@roleId                varchar(255)   -- 2011-08-10 임유석 수정(인자추가)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

DECLARE @mgrIdTemp  varchar(255) 
select  @mgrIdTemp = mgrId  from ubc_site where siteId = @siteId



INSERT INTO ubc_user
			( mgrId 
			, siteId 
			, userId 
			, password 
			, userName
			, mobileNo 
			, email 
			, userType 
			, roleId 
			, validationDate 
			, useEmail 
			, useSms  
			, registerTime 
			)
VALUES		( @mgrIdTemp 
			, @siteId 
			, @userId 
			, dbo.Password_Encode(@password)
			, @userName
			, @mobileNo 
			, @email 
			, @userType 
			, @roleId 
			, DATEADD(YY, 10, GETDATE())
			, 0 
			, 0 
			, GETDATE() 
			) 

	 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Site_User_Excel_Dup]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Site_User_Excel_Dup]
(
 
  	 @siteId	varchar(255) ,
  	 @userId	varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


 
select * from ubc_user 
where siteId = @siteId
 and  userId = @userId

	 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Site_User_Excel]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  2011-08-11 임유석 인자추가
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Site_User_Excel]
(
 
  	 @siteId		varchar(255),
  	 @franchizeType varchar(255) -- 2011-08-11 임유석 수정(인자추가)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

-- 조직이 존재하는지 여부를 알기위한 것이므로 top 1 해줌
 

select top 1 * 
  from ubc_site 
 where siteId = @siteId
   and franchizeType = @franchizeType
	 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Site_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Site_List]
 

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	select distinct(siteId) from ubc_site where siteId <> '' and siteId is not null
	 and siteId in ('HYUNDAI' , 'KIA')
	 
	 

			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Site_Factory_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Site_Factory_List]
   ( 
  
@ROOT_SITE_ID  varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
 
select siteId,siteName,businessCode,businessType,parentId 
from  ubc_site  where businessType='공장'	
and franchizeType= @ROOT_SITE_ID  order by siteName 		
			
			
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Site_Child_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date:  2011-07-14
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Site_Child_List]
 (
	@parentId varchar(255),
	@franchizeType  varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
 
IF @parentId = ''

	BEGIN
		SELECT  siteId
			  , siteName
			  , businessCode
			  , businessType
			  , parentId
		  FROM ubc_site
		 WHERE parentId      IS NULL 
		   AND franchizeType = @franchizeType
		 ORDER BY siteName
	END

ELSE

	BEGIN 
		SELECT  siteId
			  , siteName
			  , businessCode
			  , businessType
			  , parentId
		  FROM ubc_site
		 WHERE parentId      = @parentId 
		   AND franchizeType = @franchizeType
		 ORDER BY siteName	
	END
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Role_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Role_List]
(
	@customer  varchar(45)
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	
select roleId , roleName  from ubc_role where customer = @customer



	
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Pop_Board_List_Service]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		lys
-- Create date: 2011-06-16
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Pop_Board_List_Service]
(
	@boardMasterId		int
	, @siteId			NVARCHAR(100) = ''
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	
	select top 3
	       b.filename -- , a.topYn , b.attachId
	  from ubc_web_board as a
	       right join ubc_web_boardAttach as b
	       on a.boardId = b.boardId
	 where a.boardMasterId = @boardMasterId
	   and a.compType = 'k'
	   and a.useYn = 1
	   and ( a.contents = 'kia' or a.contents = @siteId )
	 order by a.topYn desc, b.attachId
	
	 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Pop_Board_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		lys
-- Create date: 2011-06-15
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Pop_Board_List]
(
	@boardMasterId		int
	, @siteId			NVARCHAR(100) = ''
	, @userId			NVARCHAR(100) = ''
	, @compType			varchar(1) = 'K'
	, @page				int			 = 1	
	, @pageSize			int			 = 10	
	, @totalRows		int	=0	OUTPUT 
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	DECLARE @firstRownum INT
	DECLARE @lastRownum INT
	DECLARE @totlaRows INT

	SET @firstRownum = ( @Page-1) * @PageSize + 1
	SET @lastRownum = @PageSize+(@Page-1)*@PageSize

	SELECT	
		@TotalRows = COUNT(*)	
	FROM	
		dbo.ubc_web_board AS A
	INNER JOIN
		dbo.ubc_user AS B
	ON
		A.userId = B.userId AND B.siteId = @siteId --AND B.mgrId=2
	WHERE	
		boardMasterId = @boardMasterId
		AND useYn = 1
		AND compType = @compType
		AND contents = @siteId
		
	SELECT
		*
		, userName
		, CD.enumString AS contentNm
		, T3.attachYn
	FROM
		(		
		SELECT
			*
		FROM
		(
			SELECT	
				row_number() OVER(ORDER BY TopYn desc , thread desc ) row_num	
				, boardId		
				, Title
				, thread
				, depth
				, contentCd
				, ReadCount
				, topYn
				, popYn
				, CreateDate
				, A.userId
				, B.userName
			FROM	
				dbo.ubc_web_board AS A
			INNER JOIN
				dbo.ubc_user AS B
			ON
				A.userId = B.userId AND B.siteId = @siteId --AND B.mgrId=2
			WHERE	
				boardMasterId = @boardMasterId
				AND useYn = 1
				AND compType = @compType
				AND contents = @siteId
		) AS T1
	) AS T2
	OUTER APPLY
	(
		SELECT TOP 1 'Y' AS attachYn FROM dbo.ubc_web_boardAttach WHERE boardId = T2.boardId
	) AS T3
	LEFT JOIN
		ubc_code AS CD
	ON
		 T2.contentCd = CD.codeId	
	WHERE 
		row_num BETWEEN @firstRownum AND @lastRownum
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mms_Site_Phone]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Mms_Site_Phone]
(
@siteId varchar(255)
) 

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	select ISNULL(mobileNo, phoneNo1)  from ubc_site  where siteId = @siteId
		
		
		
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Mms_Send]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Mms_Send]
( 
      @phoneSend	varchar(255)
    , @title			varchar(255)
    , @contents			varchar(255)
	, @franchizeType    varchar(255)
	, @siteId 			varchar(255)
	, @phone 			varchar(255)
	, @fn 			    varchar(255) 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

/*
//select comment2   from ubc_site  where franchizeType='KIA' and siteId = 'GB1'
//if(comment2 <= 0  )  
//{
//    // 남은 건수가 0이거나 음수이면 -1을 안해줌
//    // 남은 건수가 없습니다.
//} else {
//    //남은 건수가 0이상이면, MMS 모듈에 insert 해주고, ubc_site 에서   -1해줌
//    update   ubc_site  set comment2 = comment2 -1   where franchizeType='KIA' and siteId = 'GB1'
//}
*/
declare @comment2Val int 
declare @maxFile         int 

select @comment2Val = comment2   from ubc_site  where franchizeType=@franchizeType and siteId = @siteId

--  comment2 컬럼이 null 일수있다. 왜... 값을 안넣을수있으므로.
--  따라서, @comment2Val가 null 일때 아래의 IF 문장( @comment2Val <= 0  )은 false 가 된다.
if (@comment2Val <= 0 )
begin

	return 0

end
else
begin

   
    -- em_mmt_file 에서 맥스값+1 가져와서 
    -- em_mmt_file의 attach_file_group_key  , em_mmt_tran의 attach_file_group_key 에 넣기
    select @maxFile = max(attach_file_group_key)+1     from em_mmt_file
   
    
    -- 첨부파일 insert
    insert into em_mmt_file 
	(attach_file_group_key, attach_file_group_seq, attach_file_seq, attach_file_subpath, attach_file_name) 
	Values
	(@maxFile, 1, 1, NULL ,   @fn );
  
    -- MMS 메인 insert 
	insert  into  em_mmt_tran 
	(mt_refkey , date_client_req, subject, content, attach_file_group_key , callback, service_type, broadcast_yn, msg_status, recipient_num) 
	values
	(@siteId, getdate() , @title , @contents, @maxFile,  @phoneSend , '2', 'N', '1', @phone );
 
    -- 건수 빼기  
	update   ubc_site  set comment2 = comment2 -1   where franchizeType=@franchizeType and siteId = @siteId
	
	return 2

end


 
 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Member_ValidateUser]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-08-09
-- Description:	사용자 인증(기존에 클라이언트에 있던 쿼리를 프로시저로 옮김)
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Member_ValidateUser]
(
	 
	 @mgrId		varchar(10) ,
	 @userId	varchar(255),  
	 @siteId	varchar(255),  
	 @password	varchar(50)
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

		
	SELECT 1    
	  FROM dbo.ubc_user
     WHERE userId = @userId
       AND dbo.Password_Decode(password) = @password
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Member_UpdatePassword]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-08-08
-- Description:	분실 비밀번호 이메일 인증후 비밀번호 변경
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Member_UpdatePassword]
(
	 @userId		varchar(255),  
	 @email 		varchar(255),
	 @oldPassword	varchar(50) ,
	 @newPassword	varchar(50)
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

		
	update ubc_user 
	   set password =  dbo.Password_Encode(@newPassword)
	 where userId = @userId 
	   and email = @email
	   and dbo.Password_Decode(password) = @oldPassword
			
	select 1
	  from ubc_user		
	 where userId = @userId 
	   and email  = @email
	   and dbo.Password_Decode(password) = @newPassword	
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Member_GetUserInfo]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-08-09
-- Description:	사용자 정보 조회(기존에 클라이언트에 있던 쿼리를 프로시저로 옮김)
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Member_GetUserInfo]
(	 
	 @mgrId		varchar(10) ,	 
	 @siteId	varchar(255),
	 @userId	varchar(255)
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

		
	SELECT   userName
           , mobileNo   
           , siteId 
           , userType
      FROM dbo.ubc_user
     WHERE userId = @userId
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Member_GetNewPassword]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-08-08
-- Description:	비밀번호 분실시 새로운 비밀번호 부여
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Member_GetNewPassword]
(
	 @userId	varchar(255),  
	 @email 	varchar(255)
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

		
	update ubc_user 
	   set password =  dbo.Password_Encode(NEWID())
	 where userId = @userId 
	   and email = @email   
	
	
	select dbo.Password_Decode(password)
	  from ubc_user		
	 where userId = @userId 
	   and email = @email		
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Manager_Org_Upd]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Manager_Org_Upd]
(
	@mgrId				varchar(255)
	, @siteId			varchar(255)
	, @siteName			varchar(255)
	, @phoneNo1			varchar(255)
	, @mobileNo			varchar(255)
	, @businessType		varchar(255)
	, @businessCode		varchar(255)
	, @shopOpenTime		varchar(255)
	, @shopCloseTime	varchar(255)
	, @holiday			varchar(255)
	, @comment1			varchar(255)
	, @comment2			varchar(255)
	, @ROOT_SITE_ID  varchar(255)
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


	UPDATE 
		ubc_site
	SET
		siteName		=	@siteName
		, phoneNo1		=	@phoneNo1
		, mobileNo      =	@mobileNo
		, businessType  =	@businessType
		, businessCode	=	@businessCode
		, shopOpenTime	=	@shopOpenTime
		, shopCloseTime	=	@shopCloseTime
		, holiday		=	@holiday
		, comment1		=	@comment1
        , comment2		=	@comment2
	WHERE
		mgrId = @mgrId
		AND siteId = @siteId
		and franchizeType = @ROOT_SITE_ID
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Manager_Org_Sel]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 
-- Description:	
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Manager_Org_Sel]
(
	@siteId	nvarchar(30) = ''
	,@ROOT_SITE_ID  varchar(255) = ''
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT
		mgrId
		, siteId
		, siteName
		, phoneNo1
		, businessType
		, businessCode
		, shopOpenTime
		, shopCloseTime
		, holiday
		, chainNo
		, parentId
	FROM	
		dbo.ubc_site
	WHERE
		siteId = @siteId
		and franchizeType = @ROOT_SITE_ID
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Manager_Org_Reg]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Manager_Org_Reg]
(
	@mgrId				varchar(255)
	, @siteId			varchar(255)
	, @siteName			varchar(255)
	, @phoneNo1			varchar(255)
	, @mobileNo			varchar(255)
	, @businessType		varchar(255)
	, @businessCode		varchar(255)
	, @shopOpenTime		varchar(255)
	, @shopCloseTime	varchar(255)
	, @holiday			varchar(255)
	, @comment1			varchar(255)
	, @comment2			varchar(255)
	, @parentId			varchar(255)
	, @ROOT_SITE_ID  varchar(255)
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


    -- ubc_pmo 에서 adminstate=1 것중, pmId 들중 랜덤으로 가져오기.
	SELECT TOP 1 @mgrId = pmId FROM ubc_pmo WHERE adminstate=1
	ORDER BY NEWID()

	INSERT	ubc_site
			(
			mgrId
			, siteId
			, siteName
			, phoneNo1
			, mobileNo
			, businessType
			, businessCode
			, shopOpenTime
			, shopCloseTime
			, holiday
			, comment1	 
			, comment2	 
			, parentId
			,franchizeType 
			)        
	VALUES 
			(
			@mgrId
			, @siteId
			, @siteName
			, @phoneNo1
			, @mobileNo
			, @businessType
			, @businessCode
			, @shopOpenTime
			, @shopCloseTime
			, @holiday
			, @comment1
			, @comment2
			, @parentId
			,@ROOT_SITE_ID
			)        	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Manager_Org_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2011-06-28
-- Description:	2011-10-25 임유석 수정 (businessType 인자 삭제)
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Manager_Org_List]
(
	@parentId	nvarchar(30) = ''
	, @ROOT_SITE_ID  varchar(255) = ''
	
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	
	SELECT
		mgrId
		, siteId
		, siteName
		, phoneNo1
		, mobileNo
		, businessType
		, businessCode
		, shopOpenTime
		, shopCloseTime
		, holiday
		, chainNo
		, comment1
		, comment2 
		, parentId
	FROM	
		dbo.ubc_site
	WHERE
		parentId = @parentId
		and franchizeType = @ROOT_SITE_ID
	ORDER BY
		siteName		
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Manager_Org_Del]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Manager_Org_Del]
(
	@mgrId				varchar(255)
	, @siteId			varchar(255)
	, @ROOT_SITE_ID  varchar(255)
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

    -- 조직이 지워지면  다음 17가지의 테이블에서도 지워진다.
	DELETE
		ubc_site
	WHERE
		mgrId = @mgrId
		AND siteId = @siteId
	    and franchizeType = @ROOT_SITE_ID
			
	-- 17 테이블
	--    ubc_LM
	Delete from ubc_Host				WHERE   siteId = @siteId
	Delete from ubc_PowerStateLog		WHERE   siteId = @siteId
	Delete from ubc_ConnectionStateLog  WHERE   siteId = @siteId
	Delete from ubc_ScheduleStateLog	WHERE   siteId = @siteId
	--    ubc_FM
	Delete from ubc_Fault				WHERE   siteId = @siteId
	Delete from ubc_FaultLog			WHERE   siteId = @siteId
	--    ubc_PM
	Delete from ubc_BP					WHERE   siteId = @siteId
	Delete from ubc_TP					WHERE   siteId = @siteId
	Delete from ubc_TH					WHERE   siteId = @siteId
	Delete from ubc_User				WHERE   siteId = @siteId
	Delete from ubc_Reservation			WHERE   siteId = @siteId
	Delete from ubc_DownloadState		WHERE   siteId = @siteId
	Delete from ubc_DownloadStateLog	WHERE   siteId = @siteId
	Delete from ubc_Announce			WHERE   siteId = @siteId
	Delete from ubc_Program				WHERE   siteId = @siteId
	Delete from ubc_Contents			WHERE   siteId = @siteId
    			 
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Manager_Org_Code]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 
-- Description:	
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Manager_Org_Code]
(
	@parentId varchar(255),
	@franchizeType  varchar(255)
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	IF @parentId = ''

		SELECT  siteId AS code
			  , siteName AS name
		  FROM ubc_site
		 WHERE parentId      IS NULL 
		   AND franchizeType = @franchizeType
		 ORDER BY siteName
	

ELSE

	
		SELECT  siteId AS code
			  , siteName AS name
		  FROM ubc_site
		 WHERE parentId      = @parentId 
		   AND franchizeType = @franchizeType
		 ORDER BY siteName	
	
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Manage_Site_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Manage_Site_List]
 (
 @ROOT_SITE_ID  varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

select siteId , siteName from ubc_site  where siteId <> '' 
and franchizeType= @ROOT_SITE_ID
order by siteName 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Manage_Host_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Manage_Host_List]
 

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

 
			
select hostId , hostName from ubc_host 
order by hostname
			
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Setup_Update]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Setup_Update]
 (
	--@lmId		varchar(255)   , 
	@duration          decimal(5,0) , 
	@powerState        decimal(1,0) , 
	@connectionState   decimal(1,0) , 
	@scheduleState     decimal(1,0) ,
    @faultState		   decimal(1,0) ,
	@loginState		   decimal(1,0) ,
	@estimateState	   decimal(1,0) ,
	@downloadState	   decimal(1,0) 
 
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
 

UPDATE ubc_lmo
SET
duration		= @duration			, 
powerState		= @powerState		, 
connectionState = @connectionState	, 
scheduleState	= @scheduleState    ,
faultState      = @faultState		,
loginState      = @loginState       ,
estimateState   = @estimateState	,
downloadState   = @downloadState

--WHERE lmId = @lmId
 
		
		  
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Setup_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Setup_List]
 --(
	--@lmId		varchar(255)  
	--, @fromDate	varchar(255) 
	--, @toDate	    varchar(255)
 --)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

declare @cnt int

select  @cnt = COUNT(*)
from ubc_lmo
--WHERE lmId = @lmId

if @cnt = 0
 insert into ubc_lmo
 (mgrId,lmId,duration,powerState,connectionState,scheduleState , faultState , loginState , estimateState , downloadState  )
 values
 ('1','1', 0 ,  0,0,0 ,0 ,0 ,0 ,0 )



select mgrId,lmId,duration,powerState,connectionState,scheduleState ,  faultState , loginState , estimateState , downloadState
from ubc_lmo
--WHERE lmId = @lmId

			 
--select 
--A.mgrId,A.siteId,b.siteName ,  a.hostId,
--A.downloadId,A.brwId , A.programId,A.contentsId,A.contentsName,A.contentsType,
--A.location,A.filename,A.volume,A.startTime,A.endTime,A.currentVolume,A.result,
--A.reason,A.downState,A.programState,A.programStartTime,A.programEndTime,A.progress
 
--from ubc_downloadstatelog as A
--LEFT JOIN
--ubc_site   as B
--on 
-- A.siteId = B.siteId  
--where 
--A.hostId like @hostId +'%'
----and   ( convert( varchar ,A.eventTime , 112) between  @fromDate and  @toDate  )
--order by  A.hostId --, A.eventTime
		
		
		
		 
		
		
		
		
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Schedule_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Schedule_List]
 (
	@hostId		varchar(255) , 
	@fromDate	varchar(255) , 
	@toDate	    varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

--A.mgrId , A.logId , A.eventTime , A.siteId ,B.siteName , A.hostId , A.probableCause , A.additionalText , A.autoSchedule1 , A.autoSchedule2 , A.currentSchedule1 , A.currentSchedule2 , A.lastSchedule1 , A.lastSchedule2 
			 
select  CONVERT(VARCHAR, A.eventTime, 120) AS eventTime   
		, A.siteId 
		, B.siteName 
		, A.hostId  
		, A.autoSchedule1 
		, A.currentSchedule1   
		, A.lastSchedule1 
		, A.lastSchedule2 
from ubc_schedulestatelog as A
INNER JOIN ubc_site as B on A.siteId = B.siteId  
where A.hostId like '%' + @hostId +'%'
and   ( convert( varchar ,A.eventTime , 112) between  @fromDate and  @toDate  )
order by  A.siteId, A.hostId , A.eventTime desc  
		
		
		
		 
		
		
		
		
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Power_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	전원 on/off 로그
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Power_List]
 (
	@hostId		varchar(255) , 
	@fromDate	varchar(255) , 
	@toDate	    varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

--select  CONVERT(VARCHAR, A.eventTime, 120) AS eventTime  
--		, A.siteId 
--		, B.siteName 
--		, A.hostId  
--		, CONVERT(VARCHAR, A.bootUpTime, 120) AS bootUpTime   
--		, CONVERT(VARCHAR, A.bootDownTime, 120) AS bootDownTime  
--from ubc_powerstatelog as A
--INNER JOIN ubc_site as B on A.siteId = B.siteId  
--where A.hostId like '%' + @hostId +'%'
--and   ( convert( varchar ,A.eventTime , 112) between  @fromDate and  @toDate  )
--order by A.siteId, A.hostId , A.eventTime desc
		
		
select  A.siteId 
		, B.siteName 
		, A.hostId  
		, CONVERT(VARCHAR, A.bootUpTime, 120) AS bootTime   
		, 'On' AS status  
from ubc_powerstatelog as A
INNER JOIN ubc_site as B on A.siteId = B.siteId  
where A.hostId like '%' + @hostId +'%'
and   ( convert( varchar ,A.bootUpTime , 112) between  @fromDate and  @toDate  )
union
select  A.siteId 
		, B.siteName 
		, A.hostId  
		, CONVERT(VARCHAR, A.bootDownTime, 120) AS bootTime  
		, 'Off' AS status
from ubc_powerstatelog as A
INNER JOIN ubc_site as B on A.siteId = B.siteId  
where A.hostId like '%' + @hostId +'%'
and   ( convert( varchar ,A.bootDownTime , 112) between  @fromDate and  @toDate  )
order by A.siteId, A.hostId , bootTime desc
		 
		
		
		
		
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Play_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date:	2011-07-15
-- Description:	플레이어 오류
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Play_List]
 (
	@hostId		varchar(255) , 
	@fromDate	varchar(255) , 
	@toDate	    varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
	
	SELECT 
			  A.siteId
			, B.siteName
			, A.hostId
			, CONVERT(VARCHAR, A.playDate, 111) AS playDate   
			, A.programId
			, A.contentsId
			, A.contentsName
			, replace(convert(varchar,convert(money, A.playCount),1),'.00','')
			  AS playCount
			, replace(convert(varchar,convert(money, A.playTime),1),'.00','') 
			  AS playTime
			, replace(convert(varchar,convert(money, A.failCount),1),'.00','') 
			  AS failCount
	FROM ubc_playlog AS A
	INNER JOIN ubc_site AS B ON A.siteId = B.siteId
	WHERE A.hostId LIKE '%' + @hostId + '%'
	AND (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
	ORDER BY A.siteId, A.hostId, A.playDate desc, A.programId, A.contentsId		 
					
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Login_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Login_List]
 (
	@userId		varchar(255) , 
	@fromDate	varchar(255) , 
	@toDate	    varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

			 
select  A.mgrId 
		, A.siteId 
		, B.siteName 
		, A.userId 
		, A.userLogId 
		, CONVERT(VARCHAR, A.loginTime, 120) AS loginTime   
		, A.via 
		, A.result
from ubc_userLog as A
INNER JOIN ubc_site as B on A.siteId = B.siteId  
where A.userId like '%' + @userId +'%'
and   ( convert( varchar ,A.loginTime , 112) between  @fromDate and  @toDate  )
order by  A.siteId , A.loginTime desc, A.userId
				 
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Fault_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	장애로그
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Fault_List]
 (
	@hostId		varchar(255) , 
	@fromDate	varchar(255) , 
	@toDate	    varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
		 
select	CONVERT(VARCHAR, A.eventTime, 120) AS eventTime
		, A.siteId 
		, B.siteName 
		, A.hostId  
		, A.faultId  
		, replace(convert(varchar,convert(money, A.counter),1),'.00','')
			AS counter
		, case A.probableCause when '' then '기타' else A.probableCause end
			AS probableCause 
		, A.severity 
		, A.source 
		, CONVERT(VARCHAR, A.updateTime, 120) AS updateTime
from ubc_faultlog as A
INNER JOIN ubc_site as B on A.siteId = B.siteId  
where A.hostId like '%' + @hostId +'%'
and   ( convert( varchar ,A.eventTime , 112) between  @fromDate and  @toDate  )
order by  A.siteId, A.hostId, A.eventTime desc
		
		
		
		 
		
		
		
		
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Download_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Download_List]
 (
	@hostId		varchar(255)  
	, @fromDate	varchar(255) 
	, @toDate	varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
	 
SELECT 
		 A.mgrId
		,A.siteId
		,b.siteName
		,a.hostId
		,A.downloadId
		,A.brwId
		,A.programId
		,A.programState
		, CONVERT(VARCHAR, A.programStartTime, 120) AS programStartTime 
		, CONVERT(VARCHAR, A.programEndTime, 120) AS programEndTime 
		,A.progress
FROM ubc_downloadstateSummary AS A
INNER JOIN ubc_site AS B ON A.siteId = B.siteId
WHERE A.hostId LIKE '%' + @hostId +'%'
and (  
		( convert( varchar ,A.programStartTime , 112) between  @fromDate and  @toDate )
		
	)
ORDER BY A.siteId, A.hostId, A.programStartTime desc
		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Connection_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Connection_List]
 (
	@hostId		varchar(255) , 
	@fromDate	varchar(255) , 
	@toDate	    varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

select 	 CONVERT(VARCHAR, A.eventTime, 120) AS eventTime 
		 , A.siteId 
		 , B.siteName 
		 , A.hostId  
		 , case A.operationalState 
		   when '0' then 'Disconnect' 
		   else 'Connect'
		   end	as operationalState		   
from ubc_connectionstatelog as A
INNER JOIN ubc_site as B on A.siteId = B.siteId  
where A.hostId like '%' + @hostId + '%'
and  ( convert( varchar ,A.eventTime , 112) between  @fromDate and  @toDate  )
order by  A.siteId , A.hostId , A.eventTime desc
		
		
		
		 
		
		
		
		
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Bp_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date:	2011-07-15
-- Description:	스케쥴 생성/변경 접근자 및 작업 히스토리
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Bp_List]
 (
	@siteId		varchar(255) , 
	@fromDate	varchar(255) , 
	@toDate	    varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

SELECT 
	  A.siteId
	, B.siteName
	, A.bpName
	, CONVERT(VARCHAR, A.touchTime, 120) AS touchTime
	, A.action
	, A.who
FROM ubc_bpLog AS A
INNER JOIN ubc_site AS B ON A.siteId = B.siteId
WHERE A.siteId LIKE '%' + @siteId +'%' 
AND (CONVERT(VARCHAR,A.touchTime, 112) BETWEEN @fromDate AND @toDate)
ORDER BY A.siteId, A.touchTime desc, A.bpId, A.Action	 
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Log_Apply_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date:	2011-07-15
-- Description:	컨텐츠 패키지 변경명령 로그
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Log_Apply_List]
 (
	@hostId		varchar(255) , 
	@fromDate	varchar(255) , 
	@toDate	    varchar(255)
 )

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

SELECT 
	  A.siteId
	, B.siteName
	, A.hostId
	, A.programId
	, CONVERT(VARCHAR, A.applyTime, 120) AS applyTime
	, A.how
	, A.who
FROM ubc_applylog AS A
INNER JOIN ubc_site AS B ON A.siteId = B.siteId
WHERE A.hostId LIKE '%' + @hostId +'%' 
AND (CONVERT(VARCHAR,A.applyTime, 112) BETWEEN @fromDate AND @toDate)
ORDER BY A.siteId, A.hostId, A.applyTime desc, A.programId
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_generalSettings_CopyUrl_Upd]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-11-28
-- Description:	고객정보: 언어설정 업데이트
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_generalSettings_CopyUrl_Upd]
(
      @customerId	nvarchar(255)
	, @copyUrl text
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


	UPDATE ubc_customer
	SET   copyUrl = @copyUrl
	WHERE customerId = @customerId
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Customer_Url_Upd]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-11-14
-- Description:	고객정보의 URL 업데이트
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Customer_Url_Upd]
(
	  @customerId		varchar(255)
	, @url				text
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


	UPDATE ubc_customer
	SET   url = @url
	WHERE customerId = @customerId
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Customer_Url_Sel]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		임유석
-- Create date: 2011-11-14
-- Description:	고객정보 셀렉트
-- =============================================
CREATE PROCEDURE [dbo].[usp_Customer_Url_Sel]
@customerId varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

    SELECT mgrId, customerId, url, description, gmt, language, copyUrl
      FROM ubc_customer
     WHERE customerId = @customerId
    
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Upd]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_Upd]
(
	@boardId			bigint
	, @userId			nvarchar(100)
	, @title			nvarchar(500)=''
	, @contents			nvarchar(MAX)=''
	, @contentCd		nvarchar(10)=''
	, @topYn			bit
	, @popYn			bit = null
	, @ip				nvarchar(50)
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
	UPDATE	
		ubc_web_board
	SET
		title		= @title
		, contents	= @contents
		, contentCd	= @contentCd
		, topYn		= @topYn
		, popYn		= @popYn
		, ip		= @ip	
		, updateDate= GETDATE()	
	WHERE
		boardId = @boardId
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Reply]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_Reply]
(
	@boardMasterId		int
	, @boardId			bigint	OUTPUT
	, @thread			int
    , @depth			int
	, @userId			nvarchar(100)=''
	, @title			nvarchar(500)=''
	, @contents			nvarchar(MAX)=''
	, @contentCd		nvarchar(10)=''
	, @ip				nvarchar(50)
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	UPDATE 
		ubc_web_board 
    SET 
		thread = thread - 1
    WHERE 
		boardMasterId = @boardMasterId
		AND thread < @thread and thread > (@thread-1)/1000 * 1000
    
        
	INSERT	ubc_web_board
			(
			boardMasterId
			, thread
			, depth
			, userId
			, title
			, contents
			, contentCd		
			, readCount
			, useYn
			, ip
			, createDate
			, updateDate
			)        
	VALUES 
			(
			@boardMasterId
			, @thread-1
			, @depth + 1
			, @userId
			, @title
			, @contents
			, @contentCd		
			, 0
			, 1
			, @ip
			, GETDATE()
			, GETDATE()
			)        

	SET @boardId = SCOPE_IDENTITY()
	
	
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Reg]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_Reg]
(
	@boardMasterId		int
	, @boardId			bigint = 0 	OUTPUT
	, @userId			nvarchar(100)
	, @title			nvarchar(500)=''
	, @contents			nvarchar(MAX)=''
	, @contentCd		nvarchar(10)=''
	, @topYn			bit
	, @popYn			bit = null
	, @ip				nvarchar(50)
	, @compType			varchar(1) = 'K'
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	DECLARE @NewThread int
	SELECT	@NewThread = ISNULL(MAX(thread),0) + 1000 FROM ubc_web_board WHERE boardMasterId = @boardMasterId

	INSERT	ubc_web_board
			(
			boardMasterId
			, thread
			, depth
			, userId
			, title
			, contents
			, contentCd
			, topYn
			, popYn
			, readCount
			, useYn
			, ip
			, compType
			, createDate
			, updateDate
			)        
	VALUES 
			(
			@boardMasterId
			, @NewThread
			, 0
			, @userId
			, @title
			, @contents
			, @contentCd
			, @topYn
			, @popYn
			, 0
			, 1
			, @ip
			, @compType
			, GETDATE()
			, GETDATE()
			)        

	SET @boardId = SCOPE_IDENTITY()
	
	
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_ReadCount_Upd]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_ReadCount_Upd]
(
	@boardId			bigint
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
	UPDATE	
		ubc_web_board
	SET
		readCount = readCount + 1
	WHERE
		boardId = @boardId
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Pop]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_Pop]
(
	@siteId		nvarchar(100) = ''
	, @compType varchar(1) = 'K'

)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	
		TOP 1
		boardId
		, boardMasterId
		, thread
		, T1.userId
		, depth
		, title
		, contents
		, readCount
		, useYn
		, topYn
		, ISNULL(popYn, 0) AS popYn 
		, ip
		, createDate
		, updateDate	
	FROM	
		dbo.ubc_web_board AS T1
	WHERE
		popYn = 1
		AND compType =  'K'--@siteId
	ORDER BY 
		updateDate DESC
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Community_Board_List]
(
	@boardMasterId		int
	, @siteId			NVARCHAR(100) = ''
	, @userId			NVARCHAR(100) = ''
	, @searchColumn		NVARCHAR(100) = ''
	, @searchValue		NVARCHAR(100) = ''
	, @compType			varchar(1) = 'K'
	, @page				int			 = 1	
	, @pageSize			int			 = 10	
	, @totalRows		int	=0	OUTPUT 
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	DECLARE @firstRownum INT
	DECLARE @lastRownum INT
	DECLARE @totlaRows INT

	SET @firstRownum = ( @Page-1) * @PageSize + 1
	SET @lastRownum = @PageSize+(@Page-1)*@PageSize

	SELECT	
		@TotalRows = COUNT(*)	
	FROM	
		dbo.ubc_web_board AS A
	LEFT JOIN
		dbo.ubc_user AS B
	ON
		A.userId = B.userId --AND B.siteId = @siteId --AND B.mgrId=1
	WHERE	
		boardMasterId = @boardMasterId
		AND useYn = 1
		AND compType = @compType
		AND (@searchColumn = '' 
			OR ((@searchColumn = 'title' AND title LIKE '%' + @searchValue + '%' ))
			OR ((@searchColumn = 'contents' AND contents LIKE '%'+ @searchValue + '%' ))
			OR ((@searchColumn = 'userNm' AND userName LIKE '%'+ @searchValue + '%' ))
			OR ((@searchColumn = 'contentCd' AND (contentCd = @searchValue or @searchValue = '')  ))
			)
		
	SELECT
		*
		, userName
		, CD.enumString AS contentNm
		, T3.attachYn
	FROM
		(		
		SELECT
			*
		FROM
		(
			SELECT	
				row_number() OVER(ORDER BY TopYn desc , thread desc ) row_num	
				, boardId		
				, Title
				, thread
				, depth
				, contentCd
				, ReadCount
				, topYn
				, popYn
				, CreateDate
				, A.userId
				, B.userName
				, A.boardMasterId
			FROM	
				dbo.ubc_web_board AS A
			LEFT JOIN
				dbo.ubc_user AS B
			ON
				A.userId = B.userId --AND B.siteId = @siteId --AND B.mgrId=1
			WHERE	
				boardMasterId = @boardMasterId
				AND useYn = 1
				AND compType = @compType
				AND (@searchColumn = '' 
					OR ((@searchColumn = 'title' AND title LIKE '%' + @searchValue + '%' ))
					OR ((@searchColumn = 'contents' AND contents LIKE '%'+ @searchValue + '%' ))
					OR ((@searchColumn = 'userNm' AND userName LIKE '%'+ @searchValue + '%' ))
					OR ((@searchColumn = 'contentCd' AND (contentCd = @searchValue or @searchValue = '')  ))
					)
		) AS T1
	) AS T2
	OUTER APPLY
	(
		SELECT TOP 1 'Y' AS attachYn FROM dbo.ubc_web_boardAttach WHERE boardId = T2.boardId
	) AS T3
	LEFT JOIN
		ubc_code AS CD
	ON
		 CAST(T2.contentCd as int) = CD.enumNumber and CD.categoryName = 'PROGRAM TYPE' and T2.boardMasterId in (2,3,4)
	WHERE 
		row_num BETWEEN @firstRownum AND @lastRownum
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Intro_Attach_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 
-- Description:	
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Community_Board_Intro_Attach_List]
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
	SELECT	
		A.boardMasterId
		, B.attachId
		, A.contentCd
		, A.boardId		
		, filePath
		, fileName
		, A.updateDate
	FROM	
		dbo.ubc_web_board AS A
	INNER JOIN
		dbo.ubc_web_boardAttach AS B
	ON
		A.boardId = B.boardId
	WHERE	
		useYn = 1
		AND boardMasterId in (2,3,4)		
	ORDER BY
		updateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Detail]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_Detail]
(
	@boardId	bigint
	, @siteId	nvarchar(100) = ''
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	
		boardId
		, boardMasterId
		, thread
		, T1.userId
		, UU.userName
		, depth
		, title
		, contents
		, contentCd
		, CD.enumString AS contentNm
		, readCount
		, useYn
		, topYn
		, ISNULL(popYn, 0) AS popYn 
		, ip
		, createDate
		, updateDate	
	FROM	
		dbo.ubc_web_board AS T1
	INNER JOIN
		ubc_user AS UU
	ON
		T1.userId = UU.userId --AND UU.siteId = @siteId --AND UU.mgrId=1
	LEFT JOIN
		ubc_code AS CD
	ON
		 cast(T1.contentCd as int) = CD.enumNumber and CD.categoryName = 'PROGRAM TYPE' and T1.boardMasterId in (2,3,4)
	WHERE
		boardId = @boardId
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Del]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_Del]
(
	@boardId			bigint
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
	UPDATE
		ubc_web_board
	SET
		useYn = 0
	WHERE
		boardId = @boardId
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Attach_Sel]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 
-- Description:	
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Community_Board_Attach_Sel]
(
	@attachId	int
	 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
    SELECT 
		attachId
		, boardId
		, filePath
		, fileName
		, size
    FROM	
		ubc_web_boardAttach
	WHERE
		attachId = @attachId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Attach_Reg]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_Attach_Reg]
(
	@boardId		bigint
	, @filePath		nvarchar(200)
	, @fileName		nvarchar(500)
	, @size			nvarchar(10)
	 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
	INSERT	ubc_web_boardAttach
			(
			boardId
			, filePath
			, fileName
			, size
			, createDate
			)
	VALUES
			(
			@boardId
			, @filePath
			, @fileName
			, @size
			, GETDATE()
			)				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Attach_List]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 
-- Description:	
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Community_Board_Attach_List]
(
	@boardId		bigint
	 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
    SELECT 
		attachId
		, boardId
		, filePath
		, fileName
		, size
    FROM	
		ubc_web_boardAttach
	WHERE
		boardId = @boardId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Community_Board_Attach_Del]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 
-- Description:	
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Community_Board_Attach_Del]
(
	@attachId	int = null
	, @boardId	bigint = null
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
    IF @boardId IS NULL
		DELETE
			ubc_web_boardAttach
		WHERE
			attachId = @attachId
	
	IF @attachId IS NULL
		DELETE
			ubc_web_boardAttach
		WHERE
			boardId = @boardId
		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CommonCode_Sel]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		bjw
-- Create date: 2010-06-28
-- Description:	
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_CommonCode_Sel]
(
	@mgrId	varchar(255)
	, @categoryName varchar(255)
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	
		  enumNumber AS codeId	
		, enumString AS codeNm
	FROM	
		dbo.ubc_Code
	WHERE
		mgrId = @mgrId
		AND categoryName = @categoryName
		AND visible = 1
	ORDER BY
		dorder, enumString, enumNumber
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Code_Update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석 
-- Create date: 2011-08-04
-- Description:	카테고리 수정
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Code_Update]
(
	@customer		varchar(255), 
	@codeId			varchar(100),
	@categoryName	varchar(100)	 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
    DECLARE @categoryName_old varchar(255)
    
    -- 이전 카테고리명 저장해놓고
    SELECT @categoryName_old = enumString
    FROM ubc_code     
    WHERE codeId = @codeId
	AND customer = @customer
	AND categoryName = 'UBC_CODE_GROUP'
    
    -- 새로운 카테고리명 업데이트
    UPDATE ubc_code
	SET enumString = @categoryName	 
	WHERE codeId = @codeId
	and customer = @customer
	AND categoryName = 'UBC_CODE_GROUP'
	
	-- 이전 카테고리에 속했던 코드들의 카테고리명 모두 업데이트
	UPDATE ubc_code
	SET categoryName = @categoryName	 
	WHERE categoryName = @categoryName_old
	and customer = @customer	
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Code_List]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-08-04 
-- Description:	코드그룹조회
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Code_List]
(
	@customer  varchar(255)
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
 
	select   codeId    as codeId
	       , enumString as categoryName  
	from dbo.ubc_code
	where customer  = @customer
	and categoryName = 'UBC_CODE_GROUP'
	order by dorder, enumString, enumNumber
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Code_Insert]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석 
-- Create date: 2011-08-04
-- Description:	카테고리 추가
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Code_Insert]
(
	 @customer		varchar(255) , 
	 @categoryName	varchar(100) 	 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	INSERT INTO [ubc].[dbo].[ubc_code]
           ([mgrId]
           ,[codeId]
           ,[categoryName]
           ,[enumString]
           ,[enumNumber]
           ,[dorder]
           ,[visible]
           ,[customer])
     VALUES
           ('OD', newid(), 'UBC_CODE_GROUP', @categoryName, 0, 99, 1, @customer);
	 				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Code_Detail_Update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석 
-- Create date: 2011-08-04
-- Description:	공통코드 수정
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Code_Detail_Update]
(
	@customer		varchar(255) , 
	@codeId			varchar(255) ,
	@enumString		varchar(255) ,
	@dorder			int ,
	@visible		int
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;    
    
    
	UPDATE ubc_code
	SET enumString  = @enumString ,
		dorder      = @dorder     , 
		visible		= @visible
	WHERE codeId = @codeId
	AND customer = @customer	  
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Code_Detail_List]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-08-04
-- Description:	카테고리별 상세코드조회
-- =============================================*/


CREATE PROCEDURE [dbo].[usp_Code_Detail_List]
(
	@customer		varchar(255) , 
	@categoryName	varchar(100)
)

AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  mgrId
			, codeId
			, categoryName
			, enumString
			, enumNumber
			, dorder
			, visible
	FROM ubc_code 
	WHERE categoryName = @categoryName
	AND customer = @customer
	order by dorder, enumString, enumNumber
			
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Code_Detail_Insert]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석 
-- Create date: 2011-08-04
-- Description:	공통코드 등록
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Code_Detail_Insert]
(
	 @customer		varchar(255),  
	 @categoryName 	varchar(255), 
	 @enumString	varchar(255),
	 @dorder		int,
	 @visible		int	 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	DECLARE @enumNumber int;
    
    select @enumNumber = isnull(max(enumNumber)+1, 0)
    from ubc_code
    WHERE categoryName = @categoryName
	AND customer = @customer
	
	INSERT INTO [ubc].[dbo].[ubc_code]
           ([mgrId]
           ,[codeId]
           ,[categoryName]
           ,[enumString]
           ,[enumNumber]
           ,[dorder]
           ,[visible]
           ,[customer])
     VALUES
           ( 'OD'
            , newid()
            , @categoryName
            , @enumString
            , @enumNumber
            , @dorder
            , @visible
            , @customer
           );

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Code_Detail_Delete]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-08-04
-- Description:	공통코드 삭제
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Code_Detail_Delete]
(
	@customer	varchar(255) , 
	@codeId		varchar(100) 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
	DELETE FROM dbo.ubc_code
	WHERE codeId = @codeId
	AND customer = @customer
		 
		
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Code_Delete]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-08-04
-- Description:	카테고리 삭제
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Code_Delete]
(
	@customer	varchar(255) , 
	@codeId		varchar(100) 
)
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;
    
    DECLARE @categoryName varchar(255)
    
    -- 카테고리명 가져오기
    SELECT @categoryName = enumString
    FROM ubc_code     
    WHERE codeId = @codeId
	AND customer = @customer
	AND categoryName = 'UBC_CODE_GROUP'
	
	begin tran
	
    -- 카테고리삭제
	DELETE FROM dbo.ubc_code
	WHERE codeId = @codeId
	and   customer = @customer
	and   categoryName = 'UBC_CODE_GROUP'
	
	-- 카테고리에 속해있는 코드 삭제
	DELETE FROM dbo.ubc_code
	WHERE categoryName = @categoryName
	AND   customer = @customer
	
	commit
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mon_status_delete_all]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mon_status_delete_all]
	@p_emma_id		 		char(2)
AS
	DELETE FROM em_status
	WHERE emma_id = @p_emma_id

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mon_status_delete]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mon_status_delete]
	@p_emma_id		 			char(2),
    @p_process_name				varchar(40),
	@p_pid 						varchar(20),
    @p_service_type 			char(2)
AS
	/* delete record */
	DELETE FROM em_status
	WHERE emma_id		= @p_emma_id 
	AND process_name	= @p_process_name
	AND pid				= @p_pid
	AND service_type	= @p_service_type

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_common_banlist]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_common_banlist]
    @p_service_type char(2)
AS
    
	SELECT 
		service_type,
		ban_type,
		content,
		send_yn
	FROM em_banlist
	WHERE service_type = @p_service_type
	AND ban_type  <> 'R'
	AND ban_status_yn = 'Y'


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_tran_select]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_tran_select]
    @p_priority char(2),
    @p_ttl int,
    @p_emma_id char(2),
    @p_bancheck_yn char(1)
AS
    SELECT TOP 300
        A.mt_pr             AS mt_pr,
        A.mt_refkey         AS mt_refkey,
        A.subject           AS subject,
		A.content_type		AS content_type,
        A.content           AS content,
        A.priority          AS priority,
        A.broadcast_yn      AS broadcast_yn,
        A.callback          AS callback,
        A.recipient_num     AS recipient_num,
        A.recipient_net     AS recipient_net,
        A.recipient_npsend  AS recipient_npsend,
        A.country_code      AS country_code,
        A.date_client_req   AS date_client_req,
        A.charset           AS charset,
        A.msg_class         AS msg_class, 
        A.attach_file_group_key AS attach_file_group_key,
        A.msg_type          AS msg_type,
        A.crypto_yn         AS crypto_yn,
        A.service_type      AS service_type,
        B.ban_type          AS ban_type,
        B.send_yn           AS send_yn
    FROM em_mmt_tran A 
    LEFT OUTER JOIN em_banlist B 
    ON  A.recipient_num = B.content
    AND A.service_type = B.service_type
    AND B.ban_type = 'R'
    AND B.ban_status_yn = 'Y'
    WHERE A.priority = @p_priority
    AND A.msg_status = '1' 
    AND A.date_client_req BETWEEN ( getdate() -  ( @p_ttl/24/60 ) ) AND getdate()

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_tran_rslt_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_tran_rslt_update]
    @p_mt_report_code_ib        char(4),
    @p_mt_report_code_ibtype    char(1),
    @p_rs_id                    varchar(20),
    @p_client_msg_key           int,
    @p_recipient_order          int,
    @p_carrier                  int,
    @p_date_rslt                datetime
AS
    UPDATE em_mmt_tran SET
        msg_status              = '3',
        date_rslt               = @p_date_rslt,
        date_mt_report          = getdate() ,
        mt_report_code_ib       = @p_mt_report_code_ib,
        mt_report_code_ibtype   = @p_mt_report_code_ibtype,  
        carrier                 = @p_carrier,     
        rs_id                   = @p_rs_id     
    WHERE mt_pr                 = @p_client_msg_key


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_tran_rslt_delete]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_tran_rslt_delete]
AS

    DELETE FROM em_mmt_tran
    WHERE mt_pr IN ( SELECT TOP 1000 A.mt_pr FROM em_mmt_tran A
					 WHERE (A.msg_status = 2 OR A.msg_status = 3)
					 AND A.broadcast_yn = 'Y'
					 AND ( SELECT COUNT(*) FROM em_mmt_client WHERE mt_pr = A.mt_pr) = 0
					)	


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_tran_log_move_past]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_tran_log_move_past]
    @p_emma_id char(2)
AS
	SET NOCOUNT ON      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       

    DECLARE @dsql nvarchar(2000),
			@v_date_client_req varchar(8),
			@v_log_table varchar(6)

    -- set up cursor 
    DECLARE csr_obj cursor local for
        SELECT top 5
            convert(varchar, date_client_req, 112) AS date_client_req 
        FROM em_mmt_tran WITH(NOLOCK)
        WHERE date_client_req < getdate() - 10
		AND msg_status <> '3'
        AND (broadcast_yn = 'N' OR broadcast_yn is NULL)
        GROUP BY convert(varchar, date_client_req, 112)

    open csr_obj
    FETCH next FROM csr_obj into @v_date_client_req

    while (@@fetch_status = 0)
    begin
		SET @v_log_table = substring(@v_date_client_req, 1, 6)

		SELECT TOP 2000 
			mt_pr,
			0 AS mt_seq,
			mt_refkey,
			priority,
			msg_class,
			date_client_req,
			subject,
			content_type,
			content,
			attach_file_group_key,
			callback,
			service_type,
			broadcast_yn,
			msg_status,
			recipient_num,
			NULL AS change_word1,
			NULL AS change_word2,
			NULL AS change_word3,
			NULL AS change_word4,
			NULL AS change_word5,
			date_mt_sent,
			date_rslt, 
			date_mt_report, 
			mt_report_code_ib,
			mt_report_code_ibtype,
			carrier, 
			rs_id,
			recipient_net,
			recipient_npsend,
			country_code,
			charset,
			msg_type,
			crypto_yn
		INTO #em_mmt_log_temp_past
		FROM em_mmt_tran WITH(NOLOCK) 
		WHERE convert(varchar, date_client_req, 112) = @v_date_client_req
		AND msg_status <> '3'
		AND (broadcast_yn = 'N' OR broadcast_yn is NULL)

		SELECT  @dsql = '
        INSERT em_mmt_log_'+@v_log_table+' 
        SELECT
            mt_pr,
			mt_seq,
			mt_refkey,
			priority,
			msg_class,
			date_client_req,
			subject,
			content_type,
			content,
			attach_file_group_key,
			callback,
			service_type,
			broadcast_yn,
			msg_status,
			recipient_num,
			change_word1,
			change_word2,
			change_word3,
			change_word4,
			change_word5,
			date_mt_sent,
			date_rslt, 
			date_mt_report, 
			mt_report_code_ib,
			mt_report_code_ibtype,
			carrier, 
			rs_id,
			recipient_net,
			recipient_npsend,
			country_code,
			charset,
			msg_type,
			crypto_yn
        FROM #em_mmt_log_temp_past WITH(NOLOCK) '

		EXEC sp_em_mmt_log_create @v_log_table

		BEGIN TRAN

		EXEC sp_executesql @dsql

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		DELETE em_mmt_tran FROM em_mmt_tran A, #em_mmt_log_temp_past B
		WHERE A.mt_pr = B.mt_pr

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		COMMIT TRAN

		DROP TABLE #em_mmt_log_temp_past

        fetch next from csr_obj into @v_date_client_req

    end
    close csr_obj
    deallocate csr_obj


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_tran_log_move]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_tran_log_move]
    @p_emma_id char(2)
AS
	SET NOCOUNT ON      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       

    DECLARE @dsql nvarchar(2000),
			@v_date_client_req varchar(8),
			@v_log_table varchar(6)

    -- set up cursor 
    DECLARE csr_obj cursor local for
        SELECT top 5
            convert(varchar, date_client_req, 112) AS date_client_req 
        FROM em_mmt_tran WITH(NOLOCK)
        WHERE msg_status = '3'
        AND (broadcast_yn = 'N' OR broadcast_yn is NULL)
        GROUP BY convert(varchar, date_client_req, 112)

    open csr_obj
    FETCH next FROM csr_obj into @v_date_client_req

    while (@@fetch_status = 0)
    begin
		SET @v_log_table = substring(@v_date_client_req, 1, 6)

		SELECT TOP 2000 
			mt_pr,
			0 AS mt_seq,
			mt_refkey,
			priority,
			msg_class,
			date_client_req,
			subject,
			content_type,
			content,
			attach_file_group_key,
			callback,
			service_type,
			broadcast_yn,
			msg_status,
			recipient_num,
			NULL AS change_word1,
			NULL AS change_word2,
			NULL AS change_word3,
			NULL AS change_word4,
			NULL AS change_word5,
			date_mt_sent,
			date_rslt, 
			date_mt_report, 
			mt_report_code_ib,
			mt_report_code_ibtype,
			carrier, 
			rs_id,
			recipient_net,
			recipient_npsend,
			country_code,
			charset,
			msg_type,
			crypto_yn
		INTO #em_mmt_log_temp
		FROM em_mmt_tran WITH(NOLOCK) 
		WHERE msg_status = '3'
		AND (broadcast_yn = 'N' OR broadcast_yn is NULL)
		AND convert(varchar, date_client_req, 112) = @v_date_client_req

		SELECT  @dsql = '
        INSERT em_mmt_log_'+@v_log_table+' 
        SELECT
            mt_pr,
			mt_seq,
			mt_refkey,
			priority,
			msg_class,
			date_client_req,
			subject,
			content_type,
			content,
			attach_file_group_key,
			callback,
			service_type,
			broadcast_yn,
			msg_status,
			recipient_num,
			change_word1,
			change_word2,
			change_word3,
			change_word4,
			change_word5,
			date_mt_sent,
			date_rslt, 
			date_mt_report, 
			mt_report_code_ib,
			mt_report_code_ibtype,
			carrier, 
			rs_id,
			recipient_net,
			recipient_npsend,
			country_code,
			charset,
			msg_type,
			crypto_yn
        FROM #em_mmt_log_temp WITH(NOLOCK) '

		EXEC sp_em_mmt_log_create @v_log_table

		BEGIN TRAN

		EXEC sp_executesql @dsql

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		DELETE em_mmt_tran FROM em_mmt_tran A, #em_mmt_log_temp B
		WHERE A.mt_pr = B.mt_pr

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		COMMIT TRAN

		DROP TABLE #em_mmt_log_temp

        fetch next from csr_obj into @v_date_client_req

    end
    close csr_obj
    deallocate csr_obj


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_file_select]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_file_select]
    @p_attach_file_group_key int
AS

    SELECT TOP 50
		attach_file_group_key,
		attach_file_group_seq,
		attach_file_seq,
		attach_file_subpath,
		attach_file_name,
		attach_file_carrier
	 FROM em_mmt_file
    WHERE attach_file_group_key = @p_attach_file_group_key

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_client_select]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_client_select]
    @p_mt_pr int,
    @p_service_type char(2),
    @p_bancheck_yn char(1)
AS

	SELECT top 300
        A.mt_pr             AS mt_pr,
        A.mt_seq            AS mt_seq,
        A.recipient_num     AS recipient_num,
        A.recipient_net     AS recipient_net,
        A.recipient_npsend  AS recipient_npsend,    
        A.country_code      AS country_code,
        A.change_word1      AS change_word1,
        A.change_word2      AS change_word2,
        A.change_word3      AS change_word3,
        A.change_word4      AS change_word4,
        A.change_word5      AS change_word5,
        B.ban_type          AS ban_type,
        B.send_yn           AS send_yn
    FROM em_mmt_client A 
    LEFT OUTER JOIN em_banlist B 
    ON  A.recipient_num = B.content
    AND B.service_type = @p_service_type
    AND B.ban_type = 'R'
    AND B.ban_status_yn = 'Y'
    WHERE A.mt_pr = @p_mt_pr
    AND A.msg_status = '1'

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_client_rslt_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_client_rslt_update]
    @p_mt_report_code_ib        char(4),
    @p_mt_report_code_ibtype    char(1),
    @p_rs_id                    varchar(20),
    @p_client_msg_key           int,
    @p_recipient_order          int,
    @p_carrier                  int,
    @p_date_rslt                datetime
AS

    UPDATE em_mmt_client SET
        msg_status              = '3',
        date_rslt               = @p_date_rslt,
        date_mt_report          = getdate() ,
        mt_report_code_ib       = @p_mt_report_code_ib,
        mt_report_code_ibtype   = @p_mt_report_code_ibtype,  
        carrier                 = @p_carrier,     
        rs_id                   = @p_rs_id     
    WHERE mt_pr                 = @p_client_msg_key
      AND mt_seq                = @p_recipient_order


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_client_log_move_past]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_client_log_move_past]
    @p_emma_id char(2)
AS
	SET NOCOUNT ON      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       

    DECLARE @dsql nvarchar(4000),
			@v_date_client_req varchar(8),
            @v_mt_pr int,
            @v_cnt int,
			@v_log_table varchar(6)
    SELECT  @v_cnt = 1000


    -- set up cursor 
    DECLARE csr_obj cursor local for
        SELECT top 100
            convert(varchar, A.date_client_req, 112) AS date_client_req, 
            A.mt_pr as mt_pr, 
            count(B.mt_pr) as cnt
        FROM em_mmt_tran A WITH(NOLOCK), em_mmt_client B WITH(NOLOCK)
        WHERE A.date_client_req < getdate() - 10
        AND A.broadcast_yn = 'Y'
        AND B.msg_status <> '3'
        AND A.mt_pr = B.mt_pr
        GROUP BY convert(varchar, A.date_client_req, 112), A.mt_pr

    OPEN csr_obj
    FETCH next FROM csr_obj INTO @v_date_client_req, @v_mt_pr, @v_cnt

    while (@@fetch_status = 0)
    begin
		SET @v_log_table = substring(@v_date_client_req, 1, 6)

		SELECT TOP 1000 
                B.mt_pr AS mt_pr,
                B.mt_seq AS mt_seq,
                A.mt_refkey AS mt_refkey,
                A.priority AS priority,
				A.msg_class as msg_class,
                A.date_client_req AS date_client_req,
				A.subject AS subject,
				A.content_type AS content_type,
                A.content AS content,
				A.attach_file_group_key as attach_file_group_key,
                A.callback AS callback,
                A.service_type AS service_type,
                A.broadcast_yn AS broadcast_yn,
                B.msg_status AS msg_status,
                B.recipient_num AS recipient_num,
                B.change_word1 AS change_word1,
                B.change_word2 AS change_word2,
                B.change_word3 AS change_word3,
                B.change_word4 AS change_word4,
                B.change_word5 AS change_word5,
                B.date_mt_sent AS date_mt_sent,
                B.date_rslt AS date_rslt, 
                B.date_mt_report AS date_mt_report, 
                B.mt_report_code_ib AS mt_report_code_ib,
                B.mt_report_code_ibtype AS mt_report_code_ibtype,
                B.carrier AS carrier, 
                B.rs_id AS rs_id,
                B.recipient_net AS recipient_net,
                B.recipient_npsend AS recipient_npsend,
                B.country_code AS country_code,
                A.charset AS charset,
                A.msg_type AS msg_type,
                A.crypto_yn AS crypto_yn
			INTO #em_mmt_log_temp_past
            FROM em_mmt_tran A WITH(NOLOCK), em_mmt_client B WITH(NOLOCK)
            WHERE A.mt_pr = B.mt_pr
			AND B.mt_pr = @v_mt_pr
			AND B.msg_status <> '3'
            AND A.broadcast_yn = 'Y'


		SELECT  @dsql = '
			INSERT em_mmt_log_'+@v_log_table+' 
			SELECT
				mt_pr,
				mt_seq,
				mt_refkey,
				priority,
				msg_class,
				date_client_req,
				subject,
				content_type,
				content,
				attach_file_group_key,
				callback,
				service_type,
				broadcast_yn,
				msg_status,
				recipient_num,
				change_word1,
				change_word2,
				change_word3,
				change_word4,
				change_word5,
				date_mt_sent,
				date_rslt, 
				date_mt_report, 
				mt_report_code_ib,
				mt_report_code_ibtype,
				carrier, 
				rs_id,
				recipient_net,
				recipient_npsend,
				country_code,
				charset,
				msg_type,
				crypto_yn
			FROM #em_mmt_log_temp_past WITH(NOLOCK) '

		EXEC sp_em_mmt_log_create @v_log_table
		
		BEGIN TRAN
		
		EXEC sp_executesql @dsql

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		DELETE em_mmt_client FROM em_mmt_client A, #em_mmt_log_temp_past B
		WHERE A.mt_pr = @v_mt_pr
		AND A.mt_pr = B.mt_pr
		AND A.mt_seq = B.mt_seq

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		COMMIT TRAN

		DROP TABLE #em_mmt_log_temp_past

        fetch next from csr_obj into @v_date_client_req, @v_mt_pr, @v_cnt

    end
    close csr_obj
    deallocate csr_obj


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_client_log_move]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_client_log_move]
    @p_emma_id char(2)
AS
	SET NOCOUNT ON      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       

    DECLARE @dsql nvarchar(4000),
			@v_date_client_req varchar(8),
            @v_mt_pr int,
            @v_cnt int,
			@v_log_table varchar(6)
    SELECT  @v_cnt = 1000


    -- set up cursor 
    DECLARE csr_obj cursor local for
        SELECT top 100
            convert(varchar, A.date_client_req, 112) AS date_client_req, 
            A.mt_pr as mt_pr, 
            count(B.mt_pr) as cnt
        FROM em_mmt_tran A WITH(NOLOCK), em_mmt_client B WITH(NOLOCK)
        WHERE A.msg_status = '2'
        AND A.broadcast_yn = 'Y'
        AND B.msg_status = '3'
        AND A.mt_pr = B.mt_pr
        GROUP BY convert(varchar, A.date_client_req, 112), A.mt_pr

    OPEN csr_obj
    FETCH next FROM csr_obj INTO @v_date_client_req, @v_mt_pr, @v_cnt

    while (@@fetch_status = 0)
    begin
		SET @v_log_table = substring(@v_date_client_req, 1, 6)

		SELECT TOP 1000 
                B.mt_pr AS mt_pr,
                B.mt_seq AS mt_seq,
                A.mt_refkey AS mt_refkey,
                A.priority AS priority,
				A.msg_class as msg_class,
                A.date_client_req AS date_client_req,
				A.subject AS subject,
				A.content_type AS content_type,
                A.content AS content,
				A.attach_file_group_key as attach_file_group_key,
                A.callback AS callback,
                A.service_type AS service_type,
                A.broadcast_yn AS broadcast_yn,
                B.msg_status AS msg_status,
                B.recipient_num AS recipient_num,
                B.change_word1 AS change_word1,
                B.change_word2 AS change_word2,
                B.change_word3 AS change_word3,
                B.change_word4 AS change_word4,
                B.change_word5 AS change_word5,
                B.date_mt_sent AS date_mt_sent,
                B.date_rslt AS date_rslt, 
                B.date_mt_report AS date_mt_report, 
                B.mt_report_code_ib AS mt_report_code_ib,
                B.mt_report_code_ibtype AS mt_report_code_ibtype,
                B.carrier AS carrier, 
                B.rs_id AS rs_id,
                B.recipient_net AS recipient_net,
                B.recipient_npsend AS recipient_npsend,
                B.country_code AS country_code,
                A.charset AS charset,
                A.msg_type AS msg_type,
                A.crypto_yn AS crypto_yn
			INTO #em_mmt_log_temp
            FROM em_mmt_tran A WITH(NOLOCK), em_mmt_client B WITH(NOLOCK)
            WHERE A.mt_pr = B.mt_pr
			AND B.mt_pr = @v_mt_pr
			AND B.msg_status = '3'
            AND A.broadcast_yn = 'Y'


		SELECT  @dsql = '
			INSERT em_mmt_log_'+@v_log_table+' 
			SELECT
				mt_pr,
				mt_seq,
				mt_refkey,
				priority,
				msg_class,
				date_client_req,
				subject,
				content_type,
				content,
				attach_file_group_key,
				callback,
				service_type,
				broadcast_yn,
				msg_status,
				recipient_num,
				change_word1,
				change_word2,
				change_word3,
				change_word4,
				change_word5,
				date_mt_sent,
				date_rslt, 
				date_mt_report, 
				mt_report_code_ib,
				mt_report_code_ibtype,
				carrier, 
				rs_id,
				recipient_net,
				recipient_npsend,
				country_code,
				charset,
				msg_type,
				crypto_yn
			FROM #em_mmt_log_temp WITH(NOLOCK) '

		EXEC sp_em_mmt_log_create @v_log_table
		
		BEGIN TRAN
		
		EXEC sp_executesql @dsql

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		DELETE em_mmt_client FROM em_mmt_client A, #em_mmt_log_temp B
		WHERE A.mt_pr = @v_mt_pr
		AND A.mt_pr = B.mt_pr
		AND A.mt_seq = B.mt_seq

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		COMMIT TRAN

		DROP TABLE #em_mmt_log_temp

        fetch next from csr_obj into @v_date_client_req, @v_mt_pr, @v_cnt

    end
    close csr_obj
    deallocate csr_obj


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_tran_rslt_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_tran_rslt_update]
    @p_mt_report_code_ib        char(4),
    @p_mt_report_code_ibtype    char(1),
    @p_rs_id                    varchar(20),
    @p_client_msg_key           int,
    @p_recipient_order          int,
    @p_carrier                  int,
    @p_date_rslt                datetime
AS
    UPDATE em_smt_tran SET
        msg_status              = '3',
        date_rslt               = @p_date_rslt,
        date_mt_report          = getdate() ,
        mt_report_code_ib       = @p_mt_report_code_ib,
        mt_report_code_ibtype   = @p_mt_report_code_ibtype,  
        carrier                 = @p_carrier,     
        rs_id                   = @p_rs_id     
    WHERE mt_pr                 = @p_client_msg_key


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_tran_rslt_delete]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_tran_rslt_delete]
AS


	DELETE FROM em_smt_tran
    WHERE mt_pr IN ( SELECT TOP 1000 A.mt_pr FROM em_smt_tran A
					 WHERE (A.msg_status = 2 OR A.msg_status = 3)
					 AND A.broadcast_yn = 'Y'
					 AND ( SELECT COUNT(*) FROM em_smt_client WHERE mt_pr = A.mt_pr) = 0
					)	


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_tran_log_move_past]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_tran_log_move_past]
    @p_emma_id char(2)
AS
	SET NOCOUNT ON      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       

    DECLARE @dsql nvarchar(2000),
			@v_date_client_req varchar(8),
			@v_log_table varchar(6)

    -- set up cursor 
    DECLARE csr_obj cursor local for
        SELECT top 5
            convert(varchar, date_client_req, 112) AS date_client_req 
        FROM em_smt_tran WITH(NOLOCK)
        WHERE date_client_req < getdate() - 10
		AND msg_status <> '3'
        AND (broadcast_yn = 'N' OR broadcast_yn is NULL)
        GROUP BY convert(varchar, date_client_req, 112)

    open csr_obj
    FETCH next FROM csr_obj into @v_date_client_req

    while (@@fetch_status = 0)
    begin
		SET @v_log_table = substring(@v_date_client_req, 1, 6)

		SELECT TOP 2000  
			mt_pr,
			0 as mt_seq,
			mt_refkey,
			priority,
			date_client_req,
			content,
			callback,
			service_type,
			broadcast_yn,
			msg_status,
			recipient_num,
			NULL AS change_word1,
			NULL AS change_word2,
			NULL AS change_word3,
			NULL AS change_word4,
			NULL AS change_word5,
			date_mt_sent,
			date_rslt,
			date_mt_report,
			mt_report_code_ib,
			mt_report_code_ibtype,
			carrier,
			rs_id,
			recipient_net,
			recipient_npsend,
			country_code,
			charset,
			msg_type,
			crypto_yn
		INTO #em_smt_log_temp_past
		FROM em_smt_tran WITH(NOLOCK) 
		WHERE convert(varchar, date_client_req, 112) = @v_date_client_req
		AND msg_status <> '3'
		AND (broadcast_yn = 'N' OR broadcast_yn is NULL)

		SELECT  @dsql = '
			INSERT em_smt_log_'+@v_log_table+'
			SELECT
				mt_pr,
				mt_seq,
				mt_refkey,
				priority,
				date_client_req,
				content,
				callback,
				service_type,
				broadcast_yn,
				msg_status,
				recipient_num,  
				change_word1,
				change_word2,
				change_word3,
				change_word4,
				change_word5,
				date_mt_sent,
				date_rslt,
				date_mt_report,
				mt_report_code_ib,
				mt_report_code_ibtype,
				carrier,
				rs_id,
				recipient_net,
				recipient_npsend,
				country_code,
				charset,
				msg_type,
				crypto_yn
			FROM #em_smt_log_temp_past WITH(NOLOCK) '

		EXEC sp_em_smt_log_create @v_log_table

		BEGIN TRAN

		EXEC sp_executesql @dsql

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		DELETE em_smt_tran FROM em_smt_tran A, #em_smt_log_temp_past B
		WHERE A.mt_pr = B.mt_pr


		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		COMMIT TRAN

		DROP TABLE #em_smt_log_temp_past

        fetch next from csr_obj into @v_date_client_req

    end
    close csr_obj
    deallocate csr_obj


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_tran_log_move]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_tran_log_move]
    @p_emma_id char(2)
AS
	SET NOCOUNT ON      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       

    DECLARE @dsql nvarchar(2000),
			@v_date_client_req varchar(8),
			@v_log_table varchar(6)

    -- set up cursor 
    DECLARE csr_obj cursor local for
        SELECT top 5
            convert(varchar, date_client_req, 112) AS date_client_req 
        FROM em_smt_tran WITH(NOLOCK)
        WHERE msg_status = '3'
        AND (broadcast_yn = 'N' OR broadcast_yn is NULL)
        GROUP BY convert(varchar, date_client_req, 112)

    open csr_obj
    FETCH next FROM csr_obj into @v_date_client_req

    while (@@fetch_status = 0)
    begin
		SET @v_log_table = substring(@v_date_client_req, 1, 6)

		SELECT TOP 2000  
			mt_pr,
			0 as mt_seq,
			mt_refkey,
			priority,
			date_client_req,
			content,
			callback,
			service_type,
			broadcast_yn,
			msg_status,
			recipient_num,
			NULL AS change_word1,
			NULL AS change_word2,
			NULL AS change_word3,
			NULL AS change_word4,
			NULL AS change_word5,
			date_mt_sent,
			date_rslt,
			date_mt_report,
			mt_report_code_ib,
			mt_report_code_ibtype,
			carrier,
			rs_id,
			recipient_net,
			recipient_npsend,
			country_code,
			charset,
			msg_type,
			crypto_yn
		INTO #em_smt_log_temp
		FROM em_smt_tran WITH(NOLOCK) 
		WHERE msg_status = '3'
		AND (broadcast_yn = 'N' OR broadcast_yn is NULL)
		AND convert(varchar, date_client_req, 112) = @v_date_client_req

		SELECT  @dsql = '
			INSERT em_smt_log_'+@v_log_table+'
			SELECT
				mt_pr,
				mt_seq,
				mt_refkey,
				priority,
				date_client_req,
				content,
				callback,
				service_type,
				broadcast_yn,
				msg_status,
				recipient_num,  
				change_word1,
				change_word2,
				change_word3,
				change_word4,
				change_word5,
				date_mt_sent,
				date_rslt,
				date_mt_report,
				mt_report_code_ib,
				mt_report_code_ibtype,
				carrier,
				rs_id,
				recipient_net,
				recipient_npsend,
				country_code,
				charset,
				msg_type,
				crypto_yn
			FROM #em_smt_log_temp WITH(NOLOCK) '

		EXEC sp_em_smt_log_create @v_log_table

		BEGIN TRAN

		EXEC sp_executesql @dsql

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		DELETE em_smt_tran FROM em_smt_tran A, #em_smt_log_temp B
		WHERE A.mt_pr = B.mt_pr


		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		COMMIT TRAN

		DROP TABLE #em_smt_log_temp

        fetch next from csr_obj into @v_date_client_req

    end
    close csr_obj
    deallocate csr_obj


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_client_select]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_client_select]
    @p_mt_pr int,
    @p_service_type char(2),
    @p_bancheck_yn char(1)
AS

    SELECT top 300
        A.mt_pr             AS mt_pr,
        A.mt_seq            AS mt_seq,
        A.recipient_num     AS recipient_num,
        A.recipient_net     AS recipient_net,
        A.recipient_npsend  AS recipient_npsend,    
        A.country_code      AS country_code,
        A.change_word1      AS change_word1,
        A.change_word2      AS change_word2,
        A.change_word3      AS change_word3,
        A.change_word4      AS change_word4,
        A.change_word5      AS change_word5,
        B.ban_type          AS ban_type,
        B.send_yn           AS send_yn
    FROM em_smt_client A 
    LEFT OUTER JOIN em_banlist B 
    ON  A.recipient_num = B.content
    AND B.service_type = @p_service_type
    AND B.ban_type = 'R'
    AND B.ban_status_yn = 'Y'
    WHERE A.mt_pr = @p_mt_pr
    AND A.msg_status = '1'

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_client_rslt_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_client_rslt_update]
    @p_mt_report_code_ib        char(4),
    @p_mt_report_code_ibtype    char(1),
    @p_rs_id                    varchar(20),
    @p_client_msg_key           int,
    @p_recipient_order          int,
    @p_carrier                  int,
    @p_date_rslt                datetime
AS

    UPDATE em_smt_client SET
        msg_status              = '3',
        date_rslt               = @p_date_rslt,
        date_mt_report          = getdate(),
        mt_report_code_ib       = @p_mt_report_code_ib,
        mt_report_code_ibtype   = @p_mt_report_code_ibtype,  
        carrier                 = @p_carrier,     
        rs_id                   = @p_rs_id
    WHERE mt_pr                 = @p_client_msg_key
      AND mt_seq                = @p_recipient_order


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_client_log_move_past]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_client_log_move_past]
    @p_emma_id char(2)
AS

	SET NOCOUNT ON      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       

    DECLARE @dsql nvarchar(4000),
			@v_date_client_req varchar(8),
            @v_mt_pr int,
            @v_cnt int,
			@v_log_table varchar(6)
    SELECT  @v_cnt = 1000


    -- set up cursor 
    DECLARE csr_obj cursor local for
        SELECT top 100
            convert(varchar, A.date_client_req, 112) AS date_client_req, 
            A.mt_pr as mt_pr,
            count(B.mt_pr) as cnt
        FROM em_smt_tran A WITH(NOLOCK), em_smt_client B WITH(NOLOCK)
        WHERE A.date_client_req < getdate() - 10
        AND A.broadcast_yn = 'Y'
        AND B.msg_status <> '3'
        AND A.mt_pr = B.mt_pr
        GROUP BY convert(varchar, A.date_client_req, 112), A.mt_pr

    OPEN csr_obj
    FETCH next FROM csr_obj INTO @v_date_client_req, @v_mt_pr, @v_cnt

    while (@@fetch_status = 0)
    begin
		SET @v_log_table = substring(@v_date_client_req, 1, 6)

		SELECT TOP 1000 
                B.mt_pr AS mt_pr,
                B.mt_seq AS mt_seq,
                A.mt_refkey AS mt_refkey,
                A.priority AS priority,
                A.date_client_req AS date_client_req,
                A.content AS content,
                A.callback AS callback,
                A.service_type AS service_type,
                A.broadcast_yn AS broadcast_yn,
                B.msg_status AS msg_status,
                B.recipient_num AS recipient_num,
                B.change_word1 AS change_word1,
                B.change_word2 AS change_word2,
                B.change_word3 AS change_word3,
                B.change_word4 AS change_word4,
                B.change_word5 AS change_word5,
                B.date_mt_sent AS date_mt_sent,
                B.date_rslt AS date_rslt, 
                B.date_mt_report AS date_mt_report, 
                B.mt_report_code_ib AS mt_report_code_ib,
                B.mt_report_code_ibtype AS mt_report_code_ibtype,
                B.carrier AS carrier, 
                B.rs_id AS rs_id,
                B.recipient_net AS recipient_net,
                B.recipient_npsend AS recipient_npsend,
                B.country_code AS country_code,
                A.charset AS charset,
                A.msg_type AS msg_type,
                A.crypto_yn AS crypto_yn
			INTO #em_smt_log_temp_past
            FROM em_smt_tran A WITH(NOLOCK), em_smt_client B WITH(NOLOCK)
            WHERE A.mt_pr = B.mt_pr
			AND B.mt_pr = @v_mt_pr
			AND B.msg_status <> '3'
            AND A.broadcast_yn = 'Y'


		SELECT  @dsql = '
			INSERT em_smt_log_'+@v_log_table+'
			SELECT
				mt_pr,
				mt_seq,
				mt_refkey,
				priority,
				date_client_req,
				content,
				callback,
				service_type,
				broadcast_yn,
				msg_status,
				recipient_num,
                change_word1,
				change_word2,
				change_word3,
				change_word4,
				change_word5,
				date_mt_sent,
				date_rslt,
				date_mt_report, 
				mt_report_code_ib, 
				mt_report_code_ibtype, 
				carrier, 
				rs_id, 
				recipient_net,
				recipient_npsend,
				country_code,
				charset,
				msg_type,
				crypto_yn
			FROM #em_smt_log_temp_past WITH(NOLOCK) '

		EXEC sp_em_smt_log_create @v_log_table
		
		BEGIN TRAN
		
		EXEC sp_executesql @dsql

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end


		DELETE em_smt_client FROM em_smt_client A, #em_smt_log_temp_past B
		WHERE A.mt_pr = @v_mt_pr
		AND A.mt_pr = B.mt_pr
		AND A.mt_seq = B.mt_seq


		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		COMMIT TRAN

		DROP TABLE #em_smt_log_temp_past

        fetch next from csr_obj into @v_date_client_req, @v_mt_pr, @v_cnt

    end
    close csr_obj
    deallocate csr_obj

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_client_log_move]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_client_log_move]
    @p_emma_id char(2)
AS

	SET NOCOUNT ON      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       

    DECLARE @dsql nvarchar(4000),
			@v_date_client_req varchar(8),
            @v_mt_pr int,
            @v_cnt int,
			@v_log_table varchar(6)
    SELECT  @v_cnt = 1000


    -- set up cursor 
    DECLARE csr_obj cursor local for
        SELECT top 100
            convert(varchar, A.date_client_req, 112) AS date_client_req, 
            A.mt_pr as mt_pr, 
            count(B.mt_pr) as cnt
        FROM em_smt_tran A WITH(NOLOCK), em_smt_client B WITH(NOLOCK)
        WHERE A.msg_status = '2'
        AND A.broadcast_yn = 'Y'
        AND B.msg_status = '3'
        AND A.mt_pr = B.mt_pr
        GROUP BY convert(varchar, A.date_client_req, 112), A.mt_pr

    OPEN csr_obj
    FETCH next FROM csr_obj INTO @v_date_client_req, @v_mt_pr, @v_cnt

    while (@@fetch_status = 0)
    begin
		SET @v_log_table = substring(@v_date_client_req, 1, 6)

		SELECT TOP 1000 
                B.mt_pr AS mt_pr,
                B.mt_seq AS mt_seq,
                A.mt_refkey AS mt_refkey,
                A.priority AS priority,
                A.date_client_req AS date_client_req,
                A.content AS content,
                A.callback AS callback,
                A.service_type AS service_type,
                A.broadcast_yn AS broadcast_yn,
                B.msg_status AS msg_status,
                B.recipient_num AS recipient_num,
                B.change_word1 AS change_word1,
                B.change_word2 AS change_word2,
                B.change_word3 AS change_word3,
                B.change_word4 AS change_word4,
                B.change_word5 AS change_word5,
                B.date_mt_sent AS date_mt_sent,
                B.date_rslt AS date_rslt, 
                B.date_mt_report AS date_mt_report, 
                B.mt_report_code_ib AS mt_report_code_ib,
                B.mt_report_code_ibtype AS mt_report_code_ibtype,
                B.carrier AS carrier, 
                B.rs_id AS rs_id,
                B.recipient_net AS recipient_net,
                B.recipient_npsend AS recipient_npsend,
                B.country_code AS country_code,
                A.charset AS charset,
                A.msg_type AS msg_type,
                A.crypto_yn AS crypto_yn
			INTO #em_smt_log_temp
            FROM em_smt_tran A WITH(NOLOCK), em_smt_client B WITH(NOLOCK)
            WHERE A.mt_pr = B.mt_pr
			AND B.mt_pr = @v_mt_pr
			AND B.msg_status = '3'
            AND A.broadcast_yn = 'Y'


		SELECT  @dsql = '
			INSERT em_smt_log_'+@v_log_table+'
			SELECT
				mt_pr,
				mt_seq,
				mt_refkey,
				priority,
				date_client_req,
				content,
				callback,
				service_type,
				broadcast_yn,
				msg_status,
				recipient_num,
                change_word1,
				change_word2,
				change_word3,
				change_word4,
				change_word5,
				date_mt_sent,
				date_rslt,
				date_mt_report, 
				mt_report_code_ib, 
				mt_report_code_ibtype, 
				carrier, 
				rs_id, 
				recipient_net,
				recipient_npsend,
				country_code,
				charset,
				msg_type,
				crypto_yn
			FROM #em_smt_log_temp WITH(NOLOCK) '

		EXEC sp_em_smt_log_create @v_log_table
		
		BEGIN TRAN
		
		EXEC sp_executesql @dsql

		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end


		DELETE em_smt_client FROM em_smt_client A, #em_smt_log_temp B
		WHERE A.mt_pr = @v_mt_pr
		AND A.mt_pr = B.mt_pr
		AND A.mt_seq = B.mt_seq


		if @@error != 0
		begin
			rollback tran
			close csr_obj
			deallocate csr_obj
			return
		end

		COMMIT TRAN

		DROP TABLE #em_smt_log_temp

        fetch next from csr_obj into @v_date_client_req, @v_mt_pr, @v_cnt

    end
    close csr_obj
    deallocate csr_obj

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mon_status_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mon_status_update]
	@p_emma_id		 			char(2),
    @p_process_name				varchar(40),
	@p_pid 						varchar(20),
    @p_service_type 			char(2),
    @p_cnt_today 				int,
    @p_cnt_total 				int,
	@p_cnt_resent_1 			int,
	@p_cnt_resent_10 			int,
	@p_que_size 				int
AS
     UPDATE em_status SET
        cnt_today               = @p_cnt_today,
        cnt_total               = @p_cnt_total,
        cnt_resent_1            = @p_cnt_resent_1,
        cnt_resent_10         	= @p_cnt_resent_10,
        que_size     			= @p_que_size,
        update_time             = getdate()
    WHERE emma_id				= @p_emma_id
	  AND process_name          = @p_process_name
	  AND pid 					= @p_pid
	  AND service_type			= @p_service_type


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mon_status_insert]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mon_status_insert]
	@p_emma_id		 			char(2),
    @p_process_name				varchar(40),
	@p_pid 						varchar(20),
    @p_service_type 			char(2),
    @p_conn_time 				varchar(20),
    @p_conn_gw_info 		    varchar(40)
AS
	
	/** delete previous entry */
	EXEC sp_em_mon_status_delete @p_emma_id, @p_process_name, @p_pid, @p_service_type

	INSERT INTO em_status (
		emma_id,
		process_name,
		pid,
		service_type,
		cnt_today,
		cnt_total,
		cnt_resent_1,
		cnt_resent_10,
		que_size,
		conn_time,
		update_time,
		conn_gw_info,
		reg_date
	) values ( 
		@p_emma_id,
		@p_process_name, 
		@p_pid,
		@p_service_type,
		0,
		0,
		0,
		0,
		0,
		@p_conn_time,
		convert(varchar, getdate(), 20),
		@p_conn_gw_info,
		getdate()
	)

RETURN
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_InterActive_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-21
-- Description:	상호작용통계 그래프
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_InterActive_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate datetime ,
     @toDate   datetime ,
  	 @siteId   varchar(255),
  	 @programName varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	TOP 10
			'[' + MAX(keyword1) + ']-[' + max(keyword2) + ']-[' + max(keyword3) + ']'
									AS click_category
			, max(A.keyword1)		AS category1
			, max(A.keyword2)		AS category2
			, max(A.keyword3)		AS category3
			, max(A.keyword4)		AS category4
			, max(A.keyword5)		AS category5
		    , sum(A.counter)		AS click_count			
	FROM ubc_interactivelog AS A
	WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
	AND A.siteId in ( select siteid 
	                    from GetChildSite(@franchizeType, @franchizeType) 
	                   where id_path like @siteId + '%' )
	and A.keyword1 <> '총방문자'
	GROUP BY A.keyword1, A.keyword2, A.keyword3, A.keyword4, A.keyword5
	ORDER BY click_count desc
			                   		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_FaultStatistics_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  2011-07-16
-- Description:	 장애통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_FaultStatistics_List]
(
	 @franchizeType varchar(255),	 
	 @fromDate datetime ,
     @toDate   datetime ,
     @siteId	varchar(255), 
     @probableCause	varchar(255)
  	 
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


	SELECT	  max(B.siteName)			AS siteName
			, max(A.hostId)				AS hostId
			--, max(A.probableCause)		AS probableCause
			, case max(A.probableCause) when '' then '기타' else max(A.probableCause) end
										AS probableCause
			, replace(convert(varchar,convert(money,sum(A.counter)),1),'.00','') probableCause_count		
			, CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
										AS s_date
	  FROM ubc_faultlog A  
	  LEFT JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.eventTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.probableCause like '%' + @probableCause + '%'
	group by A.siteId, A.hostId, A.probableCause
	order by A.siteId, A.hostId, A.probableCause
				
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_FaultStatistics_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  2011-07-16
-- Description:	 장애통계 그래프
--               2011-07-20 임유석 수정
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_FaultStatistics_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate datetime ,
     @toDate   datetime ,     
  	 @siteId	varchar(255) ,
  	 @probableCause	varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	
	SELECT	 TOP 10
			 case max(A.probableCause) when '' then '기타' else max(A.probableCause) end
										AS probableCause
			,replace(convert(varchar,convert(money,sum(A.counter)),1),'.00','') 
										AS probableCause_count		
	  FROM ubc_faultlog A  
	  LEFT JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.eventTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.probableCause like '%' + @probableCause + '%'
	group by A.probableCause
	order by probableCause_count desc			
			
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ContentsUsed_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-16
-- Description:	컨텐츠 사용통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ContentsUsed_List]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),
  	 @contentsName	varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
									AS s_date
			, max(B.siteName)		AS siteName
			, replace(convert(varchar,convert(money,count(A.contentsId)),1),'.00','') contentsId_count
			, max(A.contentsName)	AS contentsName
	  FROM ubc_contents A  
	  INNER JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.registerTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.contentsName like '%' + @contentsName + '%'
	group by  A.siteId, A.contentsId
	order by A.siteId, A.contentsId
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ContentsUsed_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-16
-- Description:	컨텐츠 사용통계 그래프
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Statistics_ContentsUsed_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),
  	 @contentsName	varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  TOP 10
			  count(A.contentsId)	AS contentsId_count
			, max(A.contentsName)	AS contentsName		
	  FROM ubc_contents A  
	  LEFT JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.registerTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.contentsName like '%' + @contentsName + '%'
	group by A.contentsId
	order by contentsId_count desc

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ContentsReg_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-16
-- Description:	컨텐츠 등록통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ContentsReg_List]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),
  	 @contentsName	varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
									AS s_date
			, max(B.siteName)		AS siteName
			, replace(convert(varchar,convert(money,count(A.contentsId)),1),'.00','') contentsId_count
			, max(A.contentsName)	AS contentsName
	  FROM ubc_contents A  
	  INNER JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.registerTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.contentsName like '%' + @contentsName + '%'
	group by  A.siteId, A.contentsId
	order by A.siteId, A.contentsId

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ContentsReg_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-16
-- Description:	컨텐츠 등록통계 그래프
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ContentsReg_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),
  	 @contentsName	varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  TOP 10
			  count(A.contentsId)	AS contentsId_count
			, max(A.contentsName)	AS contentsName		
	  FROM ubc_contents A  
	  LEFT JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.registerTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.contentsName like '%' + @contentsName + '%'
	group by A.contentsId
	order by contentsId_count desc

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ContentsBroadcast_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-16
-- Description:	컨텐츠 방송통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ContentsBroadcast_List]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),
  	 @hostId		varchar(255),
  	 @contentsName  varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


SELECT	 CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
												AS s_date  
		,max(B.siteName)						AS siteName
		,max(A.hostId)							AS hostId
		,max(A.contentsName)					AS contentsName
		,replace(convert(varchar,convert(money,sum(A.playCount)),1),'.00','') playCount
		,replace(convert(varchar,convert(money,sum(A.playTime) ),1),'.00','') playTime
		,replace(convert(varchar,convert(money,sum(A.failCount)),1),'.00','') failCount
  FROM ubc_playlog A  
  INNER JOIN ubc_site B ON A.siteId = B.siteId
WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
  AND A.contentsName like '%' + @contentsName + '%'
  AND A.hostId like '%' + @hostId + '%'
group by A.siteId, A.hostId, A.contentsName
order by A.siteId, A.hostId, A.contentsName
		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ContentsBroadcast_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-16
-- Description:	컨텐츠 사용 통계 그래프
-- =============================================*/

CREATE PROCEDURE [dbo].[usp_Statistics_ContentsBroadcast_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
     @siteId		varchar(255),
     @hostId		varchar(255),
  	 @contentsName  varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

SELECT	  TOP 10
          max(A.contentsName) AS contentsName
		, sum(A.playCount)	  AS playCount	
  FROM ubc_playlog A
WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
  AND A.contentsName like '%' + @contentsName + '%'
  AND A.hostId like '%' + @hostId + '%'
group by A.contentsName
order by playCount desc
	
			
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ContentsAccess_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-25
-- Description:	콘텐츠 접근
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ContentsAccess_List]
(
	 @franchizeType		varchar(255),	 
	 @fromDate			datetime ,
     @toDate			datetime ,
  	 @siteId			varchar(255),
  	 @selectedSiteId	varchar(255),
  	 @searchType		varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;


-- 로그가 있는 지역본부,서비스센터,출고센터 리스트 가져옴
IF @searchType = '1'

BEGIN

	
	
  SELECT      MAX(v.siteName)	AS siteName
			, MAX(v.siteId)		AS siteId
			, CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
								AS s_date
			, SUM(cnt_visit)	AS cnt_visit
			, SUM(cnt_view)		AS cnt_view 
	FROM (	
			-- 지역본부
			SELECT  CASE MAX(B.lvl) 
					WHEN 3 THEN MAX(b.siteId)
					WHEN 4 THEN MAX(b.parentId)
					END						AS parentId
				  , MAX(A.siteId)			AS siteId
				  , SUM(A.counter)			AS cnt_visit
				  , 0						AS cnt_view
			FROM ubc_interactivelog AS A  
			INNER JOIN GetChildSite(@franchizeType, '지역본부') B ON A.siteId = B.siteId
			WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
			--AND A.programId LIKE '%' + @programName + '%'	  
			AND A.siteId IN ( select siteId from GetChildSite(@franchizeType, @siteId) )
			AND A.keyword1 = '총방문자'
			GROUP BY A.siteId
			union
			SELECT  CASE MAX(B.lvl) 
					WHEN 3 THEN MAX(b.siteId)
					WHEN 4 THEN MAX(b.parentId)
					END						AS parentId
				  , MAX(A.siteId)			AS siteId
				  , 0						AS cnt_visit
				  , SUM(A.counter)			AS cnt_view
			FROM ubc_interactivelog AS A  
			INNER JOIN GetChildSite(@franchizeType, '지역본부') B ON A.siteId = B.siteId
			WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
			--AND A.programId LIKE '%' + @programName + '%'	  
			AND A.siteId IN ( select siteId from GetChildSite(@franchizeType, @siteId) )
			AND A.keyword1 <> '총방문자'
			GROUP BY A.siteId	
									
			UNION 
			
			-- 서비스센터
			SELECT  MAX(B.siteId)			AS parentId
				  , MAX(A.siteId)			AS siteId
				  , SUM(A.counter)			AS cnt_visit
				  , 0						AS cnt_view
			FROM ubc_interactivelog AS A  
			INNER JOIN GetChildSite(@franchizeType, '서비스센터') B ON A.siteId = B.siteId
			WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
			--AND A.programId LIKE '%' + @programName + '%'  
			AND A.siteId IN ( select siteId from GetChildSite(@franchizeType, @siteId) )
			AND A.keyword1 = '총방문자'
			GROUP BY A.siteId
			union
			-- 서비스센터
			SELECT  MAX(B.siteId)			AS parentId
				  , MAX(A.siteId)			AS siteId
				  , 0						AS cnt_visit
				  , SUM(A.counter)			AS cnt_view
			FROM ubc_interactivelog AS A  
			INNER JOIN GetChildSite(@franchizeType, '서비스센터') B ON A.siteId = B.siteId
			WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
			--AND A.programId LIKE '%' + @programName + '%'  
			AND A.siteId IN ( select siteId from GetChildSite(@franchizeType, @siteId) )
			AND A.keyword1 <> '총방문자'
			GROUP BY A.siteId
			
			UNION 
			
			-- 출고센타
			SELECT  MAX(B.siteId)			AS parentId
				  , MAX(A.siteId)			AS siteId
				  , SUM(A.counter)			AS cnt_visit
				  , 0						AS cnt_view
			FROM ubc_interactivelog AS A  
			INNER JOIN GetChildSite(@franchizeType, '출고센타') B ON A.siteId = B.siteId
			WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
			--AND A.programId LIKE '%' + @programName + '%'
			AND A.siteId IN ( SELECT siteId from GetChildSite(@franchizeType, @siteId) )			
			AND A.keyword1 = '총방문자'
			GROUP BY A.siteId
			union
			-- 출고센타
			SELECT  MAX(B.siteId)			AS parentId
				  , MAX(A.siteId)			AS siteId
				  , 0						AS cnt_visit
				  , SUM(A.counter)			AS cnt_view
			FROM ubc_interactivelog AS A  
			INNER JOIN GetChildSite(@franchizeType, '출고센타') B ON A.siteId = B.siteId
			WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
			--AND A.programId LIKE '%' + @programName + '%'
			AND A.siteId IN ( SELECT siteId from GetChildSite(@franchizeType, @siteId) )			
			AND A.keyword1 <> '총방문자'
			GROUP BY A.siteId
						
			--UNION 
			
			---- 양재동 본사
			--SELECT  MAX(B.siteId)			AS parentId
			--	  , MAX(A.siteId)			AS siteId
			--	  , SUM(A.counter)			AS cnt_visit
			--	  , 0						AS cnt_view
			--FROM ubc_interactivelog AS A  
			--INNER JOIN GetChildSite(@franchizeType, 'KIA') B ON A.siteId = B.siteId and B.siteId = 'KIA'
			--WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
			----AND A.programId LIKE '%' + @programName + '%'
			--AND A.siteId IN ( SELECT siteId from GetChildSite(@franchizeType, @siteId) )
			--AND A.keyword1 = '총방문자'
			--GROUP BY A.siteId	
			--union
			--SELECT  MAX(B.siteId)			AS parentId
			--	  , MAX(A.siteId)			AS siteId
			--	  , 0						AS cnt_visit
			--	  , SUM(A.counter)			AS cnt_view
			--FROM ubc_interactivelog AS A  
			--INNER JOIN GetChildSite(@franchizeType, 'KIA') B ON A.siteId = B.siteId and B.siteId = 'KIA'
			--WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
			----AND A.programId LIKE '%' + @programName + '%'
			--AND A.siteId IN ( SELECT siteId from GetChildSite(@franchizeType, @siteId) )
			--AND A.keyword1 <> '총방문자'
			--GROUP BY A.siteId	
			
	) R
	LEFT join ubc_site S ON S.siteId = R.parentId
	LEFT join ubc_site V ON V.siteId = S.parentId 		
	GROUP by v.siteId	
	ORDER by siteId, siteName
	
END

-- 총방문자, 총페이지뷰 (선택된 조직만 하위조직 모두 조회)
ELSE IF @searchType = '2'

BEGIN

	
	SELECT   siteId
		   , siteName
		   , cnt_visit
		   , cnt_view		   
		   , CONVERT(VARCHAR,@fromDate, 112)	AS fromDate 
		   , CONVERT(VARCHAR,@toDate  , 112)	AS toDate
	from
	(
		SELECT   'total'		AS siteId
			   , '지점 Total'	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId  and B.businessType = '지점'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) )
					group by A.siteId
					union
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId  and B.businessType = '지점'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) )
					GROUP BY A.siteId
				) M
		GROUP BY M.total
		
		union all
		
		SELECT   max(siteId)	AS siteId
			   , max(siteName)	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '지점'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) )
					group by A.siteId
					union
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId and B.businessType = '지점'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) )
					GROUP BY A.siteId
				) M
		GROUP BY M.siteId	
		
		union all
			
		SELECT   'total'			AS siteId
			   , '대리점 Total'		AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId  and B.businessType = '대리점'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) )
					group by A.siteId
					union
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId  and B.businessType = '대리점'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) )
					GROUP BY A.siteId
				) M
		GROUP BY M.total
		
		union all
			
		SELECT   max(siteId)	AS siteId
			   , max(siteName)	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '대리점'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) )
					group by A.siteId
					union
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId and B.businessType = '대리점'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) )
					GROUP BY A.siteId
				) M
		GROUP BY M.siteId
		
		union all
			
		SELECT   'total'				AS total
			   , '서비스센터 Total'		AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId  and B.businessType = '서비스센터'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) )
					group by A.siteId
					union
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId  and B.businessType = '서비스센터'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) )
					GROUP BY A.siteId
				) M
		GROUP BY M.total
		
		union all
			
		SELECT   max(siteId)	AS siteId
			   , max(siteName)	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '서비스센터'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) )
					group by A.siteId
					union
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId and B.businessType = '서비스센터'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) )
					GROUP BY A.siteId
				) M
		GROUP BY M.siteId
		
		union all
			
		SELECT   'total'				AS total
			   , '출고센터 Total'		AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId  and B.businessType = '출고센터'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) )
					group by A.siteId
					union
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId  and B.businessType = '출고센터'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) )
					GROUP BY A.siteId
				) M
		GROUP BY M.total
		
		union all
			
		SELECT   max(siteId)	AS siteId
			   , max(siteName)	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '출고센터'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) )
					group by A.siteId
					union
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId and B.businessType = '출고센터'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) )
					GROUP BY A.siteId
				) M
		GROUP BY M.siteId
				
		
		
		
	) T

END


-- 총방문자, 총페이지뷰 (선택된 조직만 조회)
ELSE IF @searchType = '2-1'

BEGIN

	SELECT   siteId
		   , siteName
		   , cnt_visit
		   , cnt_view		   
		   , CONVERT(VARCHAR,@fromDate, 112)	AS fromDate 
		   , CONVERT(VARCHAR,@toDate  , 112)	AS toDate
	from
	(
		SELECT   'total'		AS siteId
			   , '지점 Total'	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '지점'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId)
					group by A.siteId
					union
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId  and B.businessType = '지점'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					GROUP BY A.siteId
				) M
		GROUP BY M.total
		
		union all
		
		SELECT   max(siteId)	AS siteId
			   , max(siteName)	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '지점'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					group by A.siteId
					union
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId and B.businessType = '지점'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					GROUP BY A.siteId
				) M
		GROUP BY M.siteId	
		
		union all
			
		SELECT   'total'			AS siteId
			   , '대리점 Total'		AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId  and B.businessType = '대리점'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					group by A.siteId
					union
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId  and B.businessType = '대리점'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					GROUP BY A.siteId
				) M
		GROUP BY M.total
		
		union all
			
		SELECT   max(siteId)	AS siteId
			   , max(siteName)	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '대리점'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					group by A.siteId
					union
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId and B.businessType = '대리점'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					GROUP BY A.siteId
				) M
		GROUP BY M.siteId
		
		union all
			
		SELECT   'total'				AS total
			   , '서비스센터 Total'		AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId  and B.businessType = '서비스센터'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					group by A.siteId
					union
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId  and B.businessType = '서비스센터'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					GROUP BY A.siteId
				) M
		GROUP BY M.total
		
		union all
			
		SELECT   max(siteId)	AS siteId
			   , max(siteName)	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '서비스센터'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					group by A.siteId
					union
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId and B.businessType = '서비스센터'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					GROUP BY A.siteId
				) M
		GROUP BY M.siteId
		
		union all
			
		SELECT   'total'				AS total
			   , '출고센터 Total'		AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId  and B.businessType = '출고센터'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					group by A.siteId
					union
					SELECT  'total'					AS total
						  , max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId  and B.businessType = '출고센터'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					GROUP BY A.siteId
				) M
		GROUP BY M.total
		
		union all
			
		SELECT   max(siteId)	AS siteId
			   , max(siteName)	AS siteName
			   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
								AS cnt_visit
			   , replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
								AS cnt_view	
		  FROM (
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , sum(counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B on A.siteId = B.siteId and B.businessType = '출고센터'
					where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId LIKE '%' + @programName + '%'
					AND A.keyword1 = '총방문자' 
					AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					group by A.siteId
					union
					SELECT  max(A.siteId)			AS siteId
						  , max(B.siteName)			AS siteName
						  , 0						AS cnt_visit
						  , sum(counter)			AS cnt_view
					FROM ubc_interactivelog A 
					INNER JOIN ubc_site B ON A.siteId = B.siteId and B.businessType = '출고센터'
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					--AND A.programId like '%' + @programName + '%'
					AND A.keyword1 <> '총방문자' 
					AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @siteId) where siteId = @selectedSiteId )
					GROUP BY A.siteId
				) M
		GROUP BY M.siteId
		
	) T

END


-- 페이지뷰 상세보기
ELSE IF @searchType = '3'

BEGIN

	SELECT max(A.siteId)		AS siteId
		 , max(A.keyword1)		AS category1
		 , max(A.keyword2)		AS category2
		 , max(A.keyword3)		AS category3
		 , replace(convert(varchar,convert(money,sum(A.counter) ),1),'.00','') 
								AS touch_cnt	
	FROM ubc_interactivelog AS A
	WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
	--AND A.programId LIKE '%' + @programName + '%'
	and A.keyword1 <> '총방문자' 	
	and A.siteId = @siteId
	GROUP BY A.keyword1, A.keyword2, A.keyword3

END


END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ContentsAccess_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-21
-- Description:	콘텐츠 접근 통계 그래프
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ContentsAccess_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate datetime ,
     @toDate   datetime ,
  	 @siteId   varchar(255),
  	 @programName varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	TOP 10
			'[' + MAX(keyword1) + ']-[' + max(keyword2) + ']-[' + max(keyword3) + ']'
									AS click_category
			, max(A.keyword1)		AS category1
			, max(A.keyword2)		AS category2
			, max(A.keyword3)		AS category3
		    , sum(A.counter)		AS click_count			
	FROM ubc_interactivelog AS A
	WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
	AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	and A.keyword1 <> '총방문자'
	GROUP BY A.keyword1, A.keyword2, A.keyword3
	ORDER BY click_count desc
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_BpStatistics_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-18
-- Description:	스케쥴 생성/변경 통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_BpStatistics_List]
(
	 @franchizeType varchar(255),	 
	 @fromDate datetime ,
     @toDate   datetime ,
  	 @siteId   varchar(255),
  	 @bpName varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	 CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
									AS s_date
			--A.siteId				AS siteID
			, max(B.siteName)		AS siteName
			, max(A.bpName)			AS bpName
			--, max(A.bpId)			AS bpId
			, max(A.action)			AS action
			, replace(convert(varchar,convert(money,count(A.action) ),1),'.00','') AS action_count		
			--, max(A.who)			AS who		
	  FROM ubc_Bplog A  
	  INNER JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.touchTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.bpName like '%' + @bpName + '%'
	group by A.siteId, A.bpId, A.action
	order by A.siteId, A.bpId, A.action

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_BpStatistics_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-18
-- Description:	스케쥴 생성/변경 통계 그래프
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_BpStatistics_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate datetime ,
     @toDate   datetime ,
  	 @siteId   varchar(255),
  	 @bpName   varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

SELECT	  TOP 10
		  max(B.siteName)	AS siteName
		, count(A.bpId)		AS bpId_count
  FROM ubc_Bplog A  
  LEFT JOIN ubc_site B ON A.siteId = B.siteId
WHERE (CONVERT(VARCHAR,A.touchTime, 112) BETWEEN @fromDate AND @toDate)
  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
  AND A.bpName like '%' + @bpName + '%'
group by A.siteId
order by bpId_count desc

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ApplyStatistics_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-18
-- Description:	패키지 배포 통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ApplyStatistics_List]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),
  	 @programName	varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
									AS s_date
			, max(B.siteName)		AS siteName
			, max(A.programId)		AS programId
			, max(A.how)			AS how
			, replace(convert(varchar,convert(money,count(A.programId) ),1),'.00','') 
									AS programId_count			
	  FROM ubc_applylog A  
	  LEFT JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.applyTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.programId like '%' + @programName + '%'
	group by A.siteId, A.how, A.programId
	order by A.siteId, A.how, A.programId

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ApplyStatistics_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-18
-- Description:	배포 통계 그래프
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ApplyStatistics_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),
  	 @programName	varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  TOP 10
			  max (A.programId)		AS programName
			, count(A.programId)	AS programId_count			
	  FROM ubc_applylog A  
	WHERE (CONVERT(VARCHAR,A.applyTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.programId like '%' + @programName + '%'
	group by A.programId
	order by programId_count desc

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ProgramUsed_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-18
-- Description:	패키지 사용통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ProgramUsed_List]
(
	 @franchizeType varchar(255),
	 @fromDate datetime ,
     @toDate   datetime ,
  	 @siteId   varchar(255),
  	 @programName varchar(255)

)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
									AS s_date
			, max(B.siteName)		AS siteName
			, max(A.programId)		AS programId
			, max(A.how)			AS how
			, replace(convert(varchar,convert(money,count(A.programId) ),1),'.00','') 
									AS programId_count			
	  FROM ubc_applylog A  
	  LEFT JOIN ubc_site B ON A.siteId = B.siteId
	WHERE (CONVERT(VARCHAR,A.applyTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.programId like '%' + @programName + '%'
	group by A.siteId, A.how, A.programId
	order by A.siteId, A.how, A.programId
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ProgramUsed_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-18
-- Description:	패키지 사용통계 그래프
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ProgramUsed_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate datetime ,
	 @toDate   datetime ,
	 @siteId   varchar(255),
	 @programName varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	SELECT	  TOP 10
			  max (A.programId)		AS programName
			, count(A.programId)	AS programId_count			
	  FROM ubc_applylog A  
	WHERE (CONVERT(VARCHAR,A.applyTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.programId like '%' + @programName + '%'
	group by A.programId
	order by programId_count desc
	

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ProgramReg_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-17
-- Description:	패키지 등록통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ProgramReg_List]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),  	 
  	 @programName   varchar(255),
  	 @hostType		varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

IF @hostType <> ''

BEGIN
	
	SELECT	  CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
															AS s_date
			, max(B.siteName)								AS siteName
			, isnull(max(C.enumString), '기타')				AS hostTypeName
			, max(A.programId)								AS programId	
			, replace(convert(varchar,convert(money,count(A.programId)),1),'.00','') programId_count
	  FROM ubc_program A  
	  INNER JOIN ubc_site B ON A.siteId = B.siteId
	  INNER JOIN ubc_code C 
		on A.hostType = C.enumNumber 
		and C.categoryName = '단말타입'
		and C.customer = @franchizeType
	WHERE (CONVERT(VARCHAR,A.lastUpdateTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.programId like '%' + @programName + '%'
	  AND A.hostType = @hostType
	group by A.siteId, A.hostType, A.programId
	order by A.siteId, A.hostType, A.programId

END

ELSE
	
BEGIN
	
	SELECT	  CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
															AS s_date
			, max(B.siteName)								AS siteName
			, isnull(max(C.enumString), '기타')				AS hostTypeName
			, max(A.programId)								AS programId	
			, replace(convert(varchar,convert(money,count(A.programId)),1),'.00','') programId_count
	  FROM ubc_program A  
	  INNER JOIN ubc_site B ON A.siteId = B.siteId
	  INNER JOIN ubc_code C 
		on A.hostType = C.enumNumber 
		and C.categoryName = '단말타입'
		and C.customer = @franchizeType		
	WHERE (CONVERT(VARCHAR,A.lastUpdateTime, 112) BETWEEN @fromDate AND @toDate)
	  AND A.siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND A.programId like '%' + @programName + '%'
	group by A.siteId, A.hostType, A.programId
	order by A.siteId, A.hostType, A.programId

END


END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_ProgramReg_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-17
-- Description:	패키지 등록통계 그래프
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_ProgramReg_Graph]
(
	 @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
  	 @siteId		varchar(255),
  	 @programName   varchar(255),
  	 @hostType		varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

IF @hostType <> ''

BEGIN

	SELECT	  TOP 10
			  max  (programId)	AS programName
			, count(programId)	AS programId_count
	  FROM ubc_program 
	WHERE (CONVERT(VARCHAR,lastUpdateTime, 112) BETWEEN @fromDate AND @toDate)
	  AND siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND programId like '%' + @programName + '%'
	  AND hostType = @hostType
	group by programId
	order by programId_count desc

END

ELSE
	
BEGIN
	
	SELECT	  TOP 10
			  max  (programId)	AS programName
			, count(programId)	AS programId_count
	  FROM ubc_program 
	WHERE (CONVERT(VARCHAR,lastUpdateTime, 112) BETWEEN @fromDate AND @toDate)
	  AND siteId in ( select siteid from GetChildSite(@franchizeType, @siteId) )
	  AND programId like '%' + @programName + '%'
	group by programId
	order by programId_count desc
	
END

END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_MessageUsage_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		rjh
-- Create date:  2011-07-18
-- Description:	 전송이용 통계
--               2011-07-20 임유석 수정
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_MessageUsage_List]
( 
     @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
     @siteId		varchar(255), 
  	 @mobileSend	varchar(255)  
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

-- @mobileSend 은 SMS 혹은 MMS 이다.

-- em_statistics_d 와 em_statistics_m 테이블은 인포뱅크에서 만들어준것임.
-- em_statistics_d 은 실패 저장 테이블
-- em_statistics_m 은 성공 저장 테이블
-- 위 테이블 둘다 공통 : stat_servicetype 컬럼이 0 이면 SMS   , 컬럼이 2 이면 MMS 로 구분 
-- 위 테이블 둘다 공통 : stat_payment_code 컬럼은 조직코드 이다.

-- COLLATE DATABASE_DEFAULT 라는 이상한 문장이 있는데 이것은 인포뱅크의 테이블과의 문자 충돌을 회피하기위한것이다. 다음과 같이 쓴다.
--  where stat_servicetype   = '0'          을 아래와 같이 쓴다 .(WHERE 절에 만 해당)
--  where stat_servicetype COLLATE DATABASE_DEFAULT = '0'   COLLATE DATABASE_DEFAULT
						  
 
IF @mobileSend = 'MMS'

	BEGIN
	
		SELECT	  max(B.siteName)		as send_siteName
				, max(A.send_status)	as send_status
				, sum(A.cnt)			AS send_count
				, CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
										AS s_date
		FROM 
				(
					 select '실패'					as send_status 
							,  stat_date			as send_date
							,  stat_payment_code	as siteId
							,  stat_fail_cnt		as cnt  
					 From em_statistics_d 
					 where stat_servicetype COLLATE DATABASE_DEFAULT = '2' COLLATE DATABASE_DEFAULT
					 union all
					 select '성공'					as send_status
							,  stat_date			as send_date
							,  stat_payment_code	as siteId
							,  stat_success			as cnt
					 From em_statistics_m 
					 where stat_servicetype COLLATE DATABASE_DEFAULT = '2'   COLLATE DATABASE_DEFAULT			  
				) as A
		LEFT JOIN ubc_site as B on A.siteId COLLATE DATABASE_DEFAULT = B.siteId COLLATE DATABASE_DEFAULT
		WHERE (convert(varchar ,A.send_date , 112) between @fromDate and  @toDate )
		AND   A.siteId  COLLATE DATABASE_DEFAULT in ( select siteid  COLLATE DATABASE_DEFAULT from GetChildSite(@franchizeType, @siteId) )
		GROUP BY A.siteId, A.send_status 
		ORDER BY A.siteId, A.send_status
		
	END
	
ELSE

	BEGIN
	
		SELECT	  max(B.siteName)		as send_siteName
				, max(A.send_status)	as send_status
				, sum(A.cnt)			AS send_count
				, CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
										AS s_date
		FROM 
				(
					 select '실패'					as send_status 
							,  stat_date			as send_date
							,  stat_payment_code	as siteId
							,  stat_fail_cnt		as cnt  
					 From em_statistics_d 
					 where stat_servicetype COLLATE DATABASE_DEFAULT = '0' COLLATE DATABASE_DEFAULT
					 union all
					 select '성공'					as send_status
							,  stat_date			as send_date
							,  stat_payment_code	as siteId
							,  stat_success			as cnt
					 From em_statistics_m 
					 where stat_servicetype COLLATE DATABASE_DEFAULT = '0'   COLLATE DATABASE_DEFAULT			  
				) as A
		LEFT JOIN ubc_site as B on A.siteId COLLATE DATABASE_DEFAULT = B.siteId COLLATE DATABASE_DEFAULT
		WHERE (convert(varchar ,A.send_date , 112) between @fromDate and  @toDate )
		AND   A.siteId  COLLATE DATABASE_DEFAULT in ( select siteid  COLLATE DATABASE_DEFAULT from GetChildSite(@franchizeType, @siteId) )
		GROUP BY A.siteId, A.send_status 
		ORDER BY A.siteId, A.send_status
 
 	END
 		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_MessageUsage_Graph]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		 임유석
-- Create date:  2011-07-20
-- Description:	 전송이용 통계 그래프
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_MessageUsage_Graph]
( 
     @franchizeType varchar(255),	 
	 @fromDate		datetime ,
     @toDate		datetime ,
     @siteId		varchar(255), 
  	 @mobileSend	varchar(255)  
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

-- @mobileSend 은 SMS 혹은 MMS 이다.

-- em_statistics_d 와 em_statistics_m 테이블은 인포뱅크에서 만들어준것임.
-- em_statistics_d 은 실패 저장 테이블
-- em_statistics_m 은 성공 저장 테이블
-- 위 테이블 둘다 공통 : stat_servicetype 컬럼이 0 이면 SMS   , 컬럼이 2 이면 MMS 로 구분 
-- 위 테이블 둘다 공통 : stat_payment_code 컬럼은 조직코드 이다.

-- COLLATE DATABASE_DEFAULT 라는 이상한 문장이 있는데 이것은 인포뱅크의 테이블과의 문자 충돌을 회피하기위한것이다. 다음과 같이 쓴다.
--  where stat_servicetype   = '0'          을 아래와 같이 쓴다 .(WHERE 절에 만 해당)
--  where stat_servicetype COLLATE DATABASE_DEFAULT = '0'   COLLATE DATABASE_DEFAULT
						  
 
IF @mobileSend = 'MMS'

	BEGIN
	
		SELECT	  TOP 10
				  max(B.siteName)		AS send_siteName
				, sum(A.stat_success)	AS send_count
		FROM em_statistics_m AS A
		LEFT JOIN ubc_site AS B on A.stat_payment_code COLLATE DATABASE_DEFAULT = B.siteId COLLATE DATABASE_DEFAULT
		WHERE A.stat_servicetype COLLATE DATABASE_DEFAULT = '2' COLLATE DATABASE_DEFAULT
		AND A.stat_payment_code COLLATE DATABASE_DEFAULT in ( select siteid  COLLATE DATABASE_DEFAULT from GetChildSite(@franchizeType, @siteId) )
		AND (convert(varchar, A.stat_date, 112) between @fromDate and  @toDate)
		GROUP BY A.stat_payment_code
		ORDER BY send_count desc
		
	END
	
ELSE

	BEGIN
	
		SELECT	  TOP 10
				  max(B.siteName)		AS send_siteName
				, sum(A.stat_success)	AS send_count
		FROM em_statistics_m AS A
		LEFT JOIN ubc_site AS B on A.stat_payment_code COLLATE DATABASE_DEFAULT = B.siteId COLLATE DATABASE_DEFAULT
		WHERE A.stat_servicetype COLLATE DATABASE_DEFAULT = '0' COLLATE DATABASE_DEFAULT
		AND A.stat_payment_code COLLATE DATABASE_DEFAULT in ( select siteid COLLATE DATABASE_DEFAULT from GetChildSite(@franchizeType, @siteId) )
		AND (convert(varchar, A.stat_date, 112) between @fromDate and  @toDate)
		GROUP BY A.stat_payment_code
		ORDER BY send_count desc
		
 	END
 		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_Statistics_InterActive_List]    Script Date: 07/24/2012 19:50:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		임유석
-- Create date: 2011-07-21
-- Description:	상호작용 통계
-- =============================================*/
CREATE PROCEDURE [dbo].[usp_Statistics_InterActive_List]
(
	 @franchizeType		varchar(255),	 
	 @fromDate			datetime ,
     @toDate			datetime ,
  	 @siteIdPath		varchar(255),
  	 @parentSiteId		varchar(255),
  	 @searchType		varchar(255)
)	 
AS
BEGIN

 	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	declare @searchSiteId varchar(255) = '';

	-- 지역본부,서비스센터,출고센터의 총방문자수 총페이지수 조회
	IF @searchType = '1'

		BEGIN
				
			SELECT    MAX(v.siteName)	AS siteName
					, MAX(v.siteId)		AS siteId
					, CONVERT(VARCHAR,@fromDate, 111) + ' - '  + CONVERT(VARCHAR,@toDate, 111)
										AS s_date
					, replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') 
										AS cnt_visit
					, replace(convert(varchar,convert(money,sum(cnt_view) ),1),'.00','') 
										AS cnt_view								
			FROM (	
					-- 지역본부
					SELECT  CASE MAX(B.lvl) 
							WHEN 3 THEN MAX(b.siteId)
							WHEN 4 THEN MAX(b.parentId)
							END						AS parentId
						  , MAX(A.siteId)			AS siteId
						  , SUM(A.counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog AS A  
					INNER JOIN GetChildSite(@franchizeType, '지역본부') B ON A.siteId = B.siteId
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
					AND A.siteId IN ( select siteId 
										from GetChildSite(@franchizeType, @franchizeType) 
									   where id_path like @siteIdPath + '%' )
					AND A.keyword1 = '총방문자'
					GROUP BY A.siteId
					UNION ALL
					SELECT  CASE MAX(B.lvl) 
							WHEN 3 THEN MAX(b.siteId)
							WHEN 4 THEN MAX(b.parentId)
							END						AS parentId
						  , MAX(A.siteId)			AS siteId
						  , 0						AS cnt_visit
						  , SUM(A.counter)			AS cnt_view
					FROM ubc_interactivelog AS A  
					INNER JOIN GetChildSite(@franchizeType, '지역본부') B ON A.siteId = B.siteId
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
					AND A.siteId IN ( select siteId 
										from GetChildSite(@franchizeType, @franchizeType) 
									   where id_path like @siteIdPath + '%' )
					GROUP BY A.siteId	
											
					UNION ALL 
					
					-- 서비스센터
					SELECT  MAX(B.siteId)			AS parentId
						  , MAX(A.siteId)			AS siteId
						  , SUM(A.counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog AS A  
					INNER JOIN GetChildSite(@franchizeType, '서비스센터') B ON A.siteId = B.siteId
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
					AND A.siteId IN ( select siteId 
										from GetChildSite(@franchizeType, @franchizeType) 
									   where id_path like @siteIdPath + '%' )
					AND A.keyword1 = '총방문자'
					GROUP BY A.siteId
					UNION ALL
					-- 서비스센터
					SELECT  MAX(B.siteId)			AS parentId
						  , MAX(A.siteId)			AS siteId
						  , 0						AS cnt_visit
						  , SUM(A.counter)			AS cnt_view
					FROM ubc_interactivelog AS A  
					INNER JOIN GetChildSite(@franchizeType, '서비스센터') B ON A.siteId = B.siteId
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
					AND A.siteId IN ( select siteId 
										from GetChildSite(@franchizeType, @franchizeType) 
									   where id_path like @siteIdPath + '%' )
					GROUP BY A.siteId
					
					UNION ALL 
					
					-- 출고센타
					SELECT  MAX(B.siteId)			AS parentId
						  , MAX(A.siteId)			AS siteId
						  , SUM(A.counter)			AS cnt_visit
						  , 0						AS cnt_view
					FROM ubc_interactivelog AS A  
					INNER JOIN GetChildSite(@franchizeType, '출고센타') B ON A.siteId = B.siteId
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					AND A.siteId IN ( select siteId 
										from GetChildSite(@franchizeType, @franchizeType) 
									   where id_path like @siteIdPath + '%' )
					AND A.keyword1 = '총방문자'
					GROUP BY A.siteId
					UNION ALL
					-- 출고센타
					SELECT  MAX(B.siteId)			AS parentId
						  , MAX(A.siteId)			AS siteId
						  , 0						AS cnt_visit
						  , SUM(A.counter)			AS cnt_view
					FROM ubc_interactivelog AS A  
					INNER JOIN GetChildSite(@franchizeType, '출고센타') B ON A.siteId = B.siteId
					WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
					AND A.siteId IN ( select siteId 
										from GetChildSite(@franchizeType, @franchizeType) 
									   where id_path like @siteIdPath + '%' )
					GROUP BY A.siteId
					
			) R
			LEFT join ubc_site S ON S.siteId = R.parentId
			LEFT join ubc_site V ON V.siteId = S.parentId 		
			GROUP by v.siteId	
			ORDER by siteName, siteId 
			
		END

	-- 조직별 통계
	ELSE IF @searchType = '2'

		BEGIN
				
			-- 입력받은 @siteIdPath, @parentSiteId 두개의 SITE ID 값중 
			-- 레벨이 더 낮은 SITE ID 하나를 찾아낸다 
			SELECT top 1 @searchSiteId = t.siteid 
			  FROM (
						SELECT siteId, lvl
						  FROM GetChildSite(@franchizeType, @franchizeType)  	
						 WHERE id_path = @siteIdPath
						 UNION ALL
						SELECT siteId, lvl
						  FROM GetChildSite(@franchizeType, @franchizeType)  	
						 WHERE siteId = @parentSiteId
					) t
			 ORDER BY lvl desc 

			SELECT    siteId
					, siteName
					, cnt_visit
					, cnt_view		   
					, CONVERT(VARCHAR,@fromDate, 112)	AS fromDate 
					, CONVERT(VARCHAR,@toDate  , 112)	AS toDate
			  FROM
				   (
					-- GetChildSite 의 lvl = '5' : 지점
					SELECT   'total'		AS siteId
						   , '지점 Total'	AS siteName
						   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','')  AS cnt_visit
						   , replace(convert(varchar,convert(money,sum(cnt_view ) ),1),'.00','')  AS cnt_view	
					  FROM (
								SELECT      'total'			AS siteId
										  , '지점 Total'	AS siteName
										  , sum(counter)	AS cnt_visit
										  , 0				AS cnt_view
								  FROM ubc_interactivelog A 
								 INNER JOIN  GetChildSite(@franchizeType, @franchizeType) B 
											on A.siteId = B.siteId and B.lvl = '5'
								 WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								   AND A.keyword1 = '총방문자' 
								   AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @searchSiteId) )					
								 GROUP BY A.siteId
								 UNION
								SELECT      'total'			AS siteId
										  , '지점 Total'	AS siteName
										  , 0				AS cnt_visit
										  , sum(counter)	AS cnt_view
								  FROM ubc_interactivelog A 
								 INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '5'
								 WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								   AND A.siteId IN ( SELECT siteId FROM GetChildSite(@franchizeType, @searchSiteId) )					
								 GROUP BY A.siteId
							) M
					GROUP BY M.siteId	
					UNION ALL		
					SELECT   max(siteId)	AS siteId
						   , max(siteName)	AS siteName
						   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') AS cnt_visit
						   , replace(convert(varchar,convert(money,sum(cnt_view ) ),1),'.00','') AS cnt_view	
					  FROM (
								SELECT  max(A.siteId)			AS siteId
									  , max(B.siteName)			AS siteName
									  , sum(counter)			AS cnt_visit
									  , 0						AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '5'
								where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.keyword1 = '총방문자' 
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								group by A.siteId
								union
								SELECT  max(A.siteId)			AS siteId
									  , max(B.siteName)			AS siteName
									  , 0						AS cnt_visit
									  , sum(counter)			AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '5'
								WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	 
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								GROUP BY A.siteId
							) M
					GROUP BY M.siteId

					UNION ALL
						
					-- GetChildSite 의 lvl = '6' : 대리점
					SELECT   'total'			AS siteId
						   , '대리점 Total'		AS siteName
						   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') AS cnt_visit
						   , replace(convert(varchar,convert(money,sum(cnt_view ) ),1),'.00','') AS cnt_view	
					  FROM (
								SELECT  'total'					AS siteId
									  , '대리점 Total'			AS siteName
									  , sum(counter)			AS cnt_visit
									  , 0						AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '6'
								where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.keyword1 = '총방문자' 
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								group by A.siteId
								union
								SELECT  'total'					AS siteId
									  , '대리점 Total'			AS siteName
									  , 0						AS cnt_visit
									  , sum(counter)			AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '6'
								WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								GROUP BY A.siteId
							) M
					GROUP BY M.siteId		
					UNION ALL			
					SELECT   max(siteId)	AS siteId
						   , max(siteName)	AS siteName
						   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') AS cnt_visit
						   , replace(convert(varchar,convert(money,sum(cnt_view ) ),1),'.00','') AS cnt_view	
					  FROM (
								SELECT  max(A.siteId)			AS siteId
									  , max(B.siteName)			AS siteName
									  , sum(counter)			AS cnt_visit
									  , 0						AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '6'
								where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.keyword1 = '총방문자' 
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								group by A.siteId
								union
								SELECT  max(A.siteId)			AS siteId
									  , max(B.siteName)			AS siteName
									  , 0						AS cnt_visit
									  , sum(counter)			AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '6'
								WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								GROUP BY A.siteId
							) M
					GROUP BY M.siteId
								 
					UNION ALL

					-- GetChildSite 의 lvl = '4' : 서비스센터 or 출고센터
					SELECT   'total'				    AS total
						   , max(m.siteName) + ' Total'	AS siteName
						   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') AS cnt_visit
						   , replace(convert(varchar,convert(money,sum(cnt_view ) ),1),'.00','') AS cnt_view	
					  FROM (
								SELECT  'total'					AS siteId
									  , max(c.siteName)			AS siteName
									  , sum(counter)			AS cnt_visit
									  , 0						AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '4'
								INNER JOIN ubc_site C on b.parentId = C.siteId
								where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.keyword1 = '총방문자' 
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								group by A.siteId
								union
								SELECT  'total'					AS siteId
									  , max(c.siteName)			AS siteName
									  , 0						AS cnt_visit
									  , sum(counter)			AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '4'
								INNER JOIN ubc_site C on b.parentId = C.siteId
								WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								GROUP BY A.siteId
							) M
					GROUP BY M.siteId		
					UNION ALL			
					SELECT   max(siteId)	AS siteId
						   , max(siteName)	AS siteName
						   , replace(convert(varchar,convert(money,sum(cnt_visit) ),1),'.00','') AS cnt_visit
						   , replace(convert(varchar,convert(money,sum(cnt_view ) ),1),'.00','') AS cnt_view	
					  FROM (
								SELECT  max(A.siteId)			AS siteId
									  , max(B.siteName)			AS siteName
									  , sum(counter)			AS cnt_visit
									  , 0						AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '4'
								where (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.keyword1 = '총방문자' 
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								group by A.siteId
								union
								SELECT  max(A.siteId)			AS siteId
									  , max(B.siteName)			AS siteName
									  , 0						AS cnt_visit
									  , sum(counter)			AS cnt_view
								FROM ubc_interactivelog A 
								INNER JOIN  GetChildSite(@franchizeType, @franchizeType) b on A.siteId = B.siteId and b.lvl = '4'
								WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)	  
								AND A.siteId IN  ( select siteId from GetChildSite(@franchizeType, @searchSiteId) )					
								GROUP BY A.siteId
							) M
					GROUP BY M.siteId	
			) T
		END


	-- 조직별 통계 (메뉴별상세보기)
	ELSE IF @searchType = '3'

		BEGIN

			SELECT max(A.siteId)		AS siteId
				 , max(A.keyword1)		AS category1
				 , max(A.keyword2)		AS category2
				 , max(A.keyword3)		AS category3
				 , max(A.keyword4)		AS category4
				 , max(A.keyword5)		AS category5
				 , replace(convert(varchar,convert(money,sum(A.counter) ),1),'.00','') 
										AS touch_cnt	
			FROM ubc_interactivelog AS A
			WHERE (CONVERT(VARCHAR,A.playDate, 112) BETWEEN @fromDate AND @toDate)
			--AND A.programId LIKE '%' + @programName + '%'
			and A.keyword1 <> '총방문자' 	
			and A.siteId = @siteIdPath
			GROUP BY A.keyword1, A.keyword2, A.keyword3, A.keyword4, A.keyword5
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_em_stat_mt_insert_day]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_stat_mt_insert_day]
    @p_service_type char(2),
    @p_prc_date     varchar(8),
    @p_table_name	varchar(20)
AS
    DECLARE @dsql nvarchar(4000)

    EXEC sp_em_stat_create

    --BEGIN TRAN

	EXEC sp_em_stat_delete @p_service_type, @p_prc_date

	if @@error != 0
    begin
        --rollback tran
        return
    end


	SELECT  @dsql = '
        INSERT INTO em_statistics_m (
			stat_date,
			stat_servicetype,
			stat_payment_code,
			stat_carrier,
			stat_success,
			stat_failure,
			stat_invalid,
			stat_invalid_ib,
			stat_remained,
			stat_regdate
		) SELECT 
			'''+@p_prc_date+''',
			'''+@p_service_type+''',

			CASE WHEN mt_refkey IS NULL THEN ''NONE'' ELSE mt_refkey END AS payment_code, 

			carrier,

			SUM(CASE WHEN mt_report_code_ib =''1000'' THEN 1 ELSE 0 END) AS success,
			SUM(CASE WHEN mt_report_code_ib >=''2000'' AND mt_report_code_ib < ''3000'' THEN 1 ELSE 0 END) AS failure,
			SUM(CASE WHEN mt_report_code_ib >=''3000'' AND mt_report_code_ib < ''4000'' OR mt_report_code_ib >=''5000'' AND mt_report_code_ib < ''6000'' THEN 1 ELSE 0 END) AS invalid,
			SUM(CASE WHEN mt_report_code_ib >=''1001'' AND mt_report_code_ib < ''2000'' OR mt_report_code_ib >=''4000'' AND mt_report_code_ib < ''5000'' OR mt_report_code_ib >=''E900'' AND mt_report_code_ib < ''E999'' THEN 1 ELSE 0 END) AS invalid_ib,
			SUM(CASE WHEN msg_status=''2'' THEN 1 ELSE 0 END) AS remained,

			getdate()
		FROM '+@p_table_name+'
		WHERE convert(varchar, date_mt_report, 112) = '''+@p_prc_date+'''
		AND service_type = '''+@p_service_type+'''
		GROUP BY mt_refkey, carrier '

    EXEC (@dsql)

    if @@error != 0
    begin
        --rollback tran
        return
    end

    SELECT  @dsql = '
        INSERT INTO em_statistics_d (
			stat_date,
			stat_servicetype,
			stat_payment_code,
			stat_carrier,
			stat_fail_code,
			stat_fail_cnt,
			stat_regdate
		) SELECT 
			'''+@p_prc_date+''',
			'''+@p_service_type+''',

			CASE WHEN mt_refkey IS NULL THEN ''NONE'' ELSE mt_refkey END AS payment_code, 

			carrier,
			mt_report_code_ib,
			count(mt_report_code_ib),
			getdate()
		FROM '+@p_table_name+'
		WHERE convert(varchar, date_mt_report, 112) = '''+@p_prc_date+'''
		AND service_type = '''+@p_service_type+'''
		AND mt_report_code_ib <> ''1000'' 
		GROUP BY mt_refkey, carrier, mt_report_code_ib '

    EXEC (@dsql)

    if @@error != 0
    begin
        --rollback tran
        return
    end

    --commit tran

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_stat_mt_insert]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_stat_mt_insert]
    @p_service_type char(2)
AS
    DECLARE @v_prc_date		varchar(8),			
			@v_table_name	varchar(20),
			@v_cnt			int,
			@i				int

	/** set processing date count */
	IF (@p_service_type = '2' OR @p_service_type = '3')
		SET @v_cnt = 5
	ELSE
		SET @v_cnt = 2

	SET @i=0;
	
	while (@i<@v_cnt)
	begin

		/** set process date */
		SELECT @v_prc_date = CONVERT(varchar, DATEADD(DAY, -@i, GETDATE()), 112) 

		/*SET @v_prc_date = '20090918'*/

		/** set table name */
		IF (@p_service_type = '2' OR @p_service_type = '3')
			SET @v_table_name = 'em_mmt_log_' + substring(@v_prc_date,1,6)
		ELSE
			SET @v_table_name = 'em_smt_log_' + substring(@v_prc_date,1,6)
		

		/** call insert */
		EXEC sp_em_stat_mt_insert_day @p_service_type, @v_prc_date, @v_table_name

		SET @i=@i+1;
	
	end

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_stat_execute]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_stat_execute]
    @p_smt_use_yn	char(1),
    @p_url_use_yn	char(1),
    @p_mmt_use_yn	char(1),
    @p_smo_use_yn	char(1),
    @p_mmo_use_yn	char(1)

AS
    
	/** SMS MT */
    IF @p_smt_use_yn = 'Y'
		EXEC sp_em_stat_mt_insert '0'

	/** CALLBACK URL MT */
    IF @p_url_use_yn = 'Y'
		EXEC sp_em_stat_mt_insert '1'

	/** MMS MT */
    IF @p_mmt_use_yn = 'Y'
		EXEC sp_em_stat_mt_insert '2'
    
    /** MMS MT */
    IF @p_mmt_use_yn = 'Y'
		EXEC sp_em_stat_mt_insert '3'

    /** SMS MO */
    IF @p_smo_use_yn = 'Y'
		EXEC sp_em_stat_mo_insert '4'

    /** MMS MO */
    IF @p_mmo_use_yn = 'Y'
		EXEC sp_em_stat_mo_insert '5'

RETURN
GO
/****** Object:  Default [DF__em_banlis__send___503BEA1C]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_banlist] ADD  DEFAULT ('N') FOR [send_yn]
GO
/****** Object:  Default [DF__em_banlis__ban_s__51300E55]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_banlist] ADD  DEFAULT ('Y') FOR [ban_status_yn]
GO
/****** Object:  Default [DF__em_banlis__reg_d__5224328E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_banlist] ADD  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF__em_banlis__updat__531856C7]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_banlist] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [update_date]
GO
/****** Object:  Default [DF__em_mmt_cl__msg_s__681373AD]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_client] ADD  DEFAULT ('1') FOR [msg_status]
GO
/****** Object:  Default [DF__em_mmt_cl__count__690797E6]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_client] ADD  DEFAULT ('82') FOR [country_code]
GO
/****** Object:  Default [DF__em_mmt_fi__reg_d__6DCC4D03]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_file] ADD  CONSTRAINT [DF__em_mmt_fi__reg_d__6DCC4D03]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF__em_mmt_tr__prior__5BAD9CC8]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('S') FOR [priority]
GO
/****** Object:  Default [DF__em_mmt_tr__msg_c__5CA1C101]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('1') FOR [msg_class]
GO
/****** Object:  Default [DF__em_mmt_tr__date___5D95E53A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [date_client_req]
GO
/****** Object:  Default [DF__em_mmt_tr__conte__5E8A0973]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ((0)) FOR [content_type]
GO
/****** Object:  Default [DF__em_mmt_tr__attac__5F7E2DAC]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ((0)) FOR [attach_file_group_key]
GO
/****** Object:  Default [DF__em_mmt_tr__broad__607251E5]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('N') FOR [broadcast_yn]
GO
/****** Object:  Default [DF__em_mmt_tr__msg_s__6166761E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('1') FOR [msg_status]
GO
/****** Object:  Default [DF__em_mmt_tr__count__625A9A57]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('82') FOR [country_code]
GO
/****** Object:  Default [DF__em_mmt_tr__crypt__634EBE90]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_mmt_tran] ADD  DEFAULT ('Y') FOR [crypto_yn]
GO
/****** Object:  Default [DF__em_smt_cl__msg_s__16CE6296]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_client] ADD  DEFAULT ('1') FOR [msg_status]
GO
/****** Object:  Default [DF__em_smt_cl__count__17C286CF]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_client] ADD  DEFAULT ('82') FOR [country_code]
GO
/****** Object:  Default [DF__em_smt_tr__prior__0D44F85C]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('S') FOR [priority]
GO
/****** Object:  Default [DF__em_smt_tr__date___0E391C95]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [date_client_req]
GO
/****** Object:  Default [DF__em_smt_tr__broad__0F2D40CE]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('N') FOR [broadcast_yn]
GO
/****** Object:  Default [DF__em_smt_tr__msg_s__10216507]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('1') FOR [msg_status]
GO
/****** Object:  Default [DF__em_smt_tr__count__11158940]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('82') FOR [country_code]
GO
/****** Object:  Default [DF__em_smt_tr__crypt__1209AD79]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_smt_tran] ADD  DEFAULT ('Y') FOR [crypto_yn]
GO
/****** Object:  Default [DF__em_statis__stat___338A9CD5]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_statistics_d] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [stat_regdate]
GO
/****** Object:  Default [DF__em_statis__stat___2EC5E7B8]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_statistics_m] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [stat_regdate]
GO
/****** Object:  Default [DF__em_status__reg_d__2A01329B]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[em_status] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [reg_date]
GO
/****** Object:  Default [DF_ts_cronjob_operationalStatus]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ts_cronjob] ADD  CONSTRAINT [DF_ts_cronjob_operationalStatus]  DEFAULT ((1)) FOR [operationalStatus]
GO
/****** Object:  Default [DF_ubc_announce_playSpeed]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_announce] ADD  CONSTRAINT [DF_ubc_announce_playSpeed]  DEFAULT ((1000)) FOR [playSpeed]
GO
/****** Object:  Default [DF_ubc_bp_zorder]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_bp] ADD  CONSTRAINT [DF_ubc_bp_zorder]  DEFAULT ('0') FOR [zorder]
GO
/****** Object:  Default [DF_ubc_bp_state]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_bp] ADD  CONSTRAINT [DF_ubc_bp_state]  DEFAULT ((0)) FOR [state]
GO
/****** Object:  Default [DF_ubc_bp_retryCount]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_bp] ADD  CONSTRAINT [DF_ubc_bp_retryCount]  DEFAULT ((0)) FOR [retryCount]
GO
/****** Object:  Default [DF_ubc_bp_retryGap]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_bp] ADD  CONSTRAINT [DF_ubc_bp_retryGap]  DEFAULT ((0)) FOR [retryGap]
GO
/****** Object:  Default [DF_ubc_cframe_grade]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_grade]  DEFAULT ((2)) FOR [grade]
GO
/****** Object:  Default [DF_ubc_cframe_isPIP]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_isPIP]  DEFAULT ((0)) FOR [isPIP]
GO
/****** Object:  Default [DF_ubc_cframe_borderStyle]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_borderStyle]  DEFAULT ('solid') FOR [borderStyle]
GO
/****** Object:  Default [DF_ubc_cframe_borderThickness]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_borderThickness]  DEFAULT ((0)) FOR [borderThickness]
GO
/****** Object:  Default [DF_ubc_cframe_borderColor]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_borderColor]  DEFAULT ('#FFFFFF') FOR [borderColor]
GO
/****** Object:  Default [DF_ubc_cframe_cornerRadius]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_cornerRadius]  DEFAULT ((0)) FOR [cornerRadius]
GO
/****** Object:  Default [DF_ubc_cframe_alpha]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_alpha]  DEFAULT ((0)) FOR [alpha]
GO
/****** Object:  Default [DF_ubc_cframe_isTV]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_cframe] ADD  CONSTRAINT [DF_ubc_cframe_isTV]  DEFAULT ((0)) FOR [isTV]
GO
/****** Object:  Default [DF_ubc_code_dorder]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_code] ADD  CONSTRAINT [DF_ubc_code_dorder]  DEFAULT ((0)) FOR [dorder]
GO
/****** Object:  Default [DF_ubc_code_visible]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_code] ADD  CONSTRAINT [DF_ubc_code_visible]  DEFAULT ((1)) FOR [visible]
GO
/****** Object:  Default [DF_ubc_connectionstatelog_operationalState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_connectionstatelog] ADD  CONSTRAINT [DF_ubc_connectionstatelog_operationalState]  DEFAULT ((0)) FOR [operationalState]
GO
/****** Object:  Default [DF_ubc_contents_isRaw]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_isRaw]  DEFAULT ((0)) FOR [isRaw]
GO
/****** Object:  Default [DF_ubc_contents_contentsState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_contentsState]  DEFAULT ((0)) FOR [contentsState]
GO
/****** Object:  Default [DF_ubc_contents_runningTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_runningTime]  DEFAULT ((10)) FOR [runningTime]
GO
/****** Object:  Default [DF_ubc_contents_currentComment]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_currentComment]  DEFAULT ((1)) FOR [currentComment]
GO
/****** Object:  Default [DF_ubc_contents_playSpeed]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_playSpeed]  DEFAULT ((1000)) FOR [playSpeed]
GO
/****** Object:  Default [DF_ubc_contents_soundVolume]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_soundVolume]  DEFAULT ((50)) FOR [soundVolume]
GO
/****** Object:  Default [DF_ubc_contents_isPublic]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_isPublic]  DEFAULT ((0)) FOR [isPublic]
GO
/****** Object:  Default [DF_ubc_contents_tvInputType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_tvInputType]  DEFAULT ((0)) FOR [tvInputType]
GO
/****** Object:  Default [DF_ubc_contents_direction]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_direction]  DEFAULT ((0)) FOR [direction]
GO
/****** Object:  Default [DF_ubc_contents_align]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_align]  DEFAULT ((5)) FOR [align]
GO
/****** Object:  Default [DF_ubc_contents_isUsed]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_isUsed]  DEFAULT ((1)) FOR [isUsed]
GO
/****** Object:  Default [DF_ubc_contents_contentsCategory]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_contentsCategory]  DEFAULT ((0)) FOR [contentsCategory]
GO
/****** Object:  Default [DF_ubc_contents_purpose]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_purpose]  DEFAULT ((0)) FOR [purpose]
GO
/****** Object:  Default [DF_ubc_contents_hostType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_hostType]  DEFAULT ((0)) FOR [hostType]
GO
/****** Object:  Default [DF_ubc_contents_vertical]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_vertical]  DEFAULT ((0)) FOR [vertical]
GO
/****** Object:  Default [DF_ubc_contents_resolution]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_resolution]  DEFAULT ((0)) FOR [resolution]
GO
/****** Object:  Default [DF_ubc_contents_adminState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_contents] ADD  CONSTRAINT [DF_ubc_contents_adminState]  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF_ubc_ctemplate_bgColor]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_ctemplate] ADD  CONSTRAINT [DF_ubc_ctemplate_bgColor]  DEFAULT ('#c0c0c0') FOR [bgColor]
GO
/****** Object:  Default [DF__ubc_custome__gmt__4999D985]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_customer] ADD  DEFAULT ((9999)) FOR [gmt]
GO
/****** Object:  Default [DF_ubc_defaultschedule_openningFlag]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_openningFlag]  DEFAULT ((1)) FOR [openningFlag]
GO
/****** Object:  Default [DF_ubc_defaultschedule_timeScope]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_timeScope]  DEFAULT ((0)) FOR [timeScope]
GO
/****** Object:  Default [DF_ubc_defaultschedule_priority]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_priority]  DEFAULT ((0)) FOR [priority]
GO
/****** Object:  Default [DF_ubc_defaultschedule_operationalState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_operationalState]  DEFAULT ((1)) FOR [operationalState]
GO
/****** Object:  Default [DF_ubc_defaultschedule_adminState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_adminState]  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF_ubc_defaultschedule_runningTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule] ADD  CONSTRAINT [DF_ubc_defaultschedule_runningTime]  DEFAULT ((0)) FOR [runningTime]
GO
/****** Object:  Default [DF_ubc_downloadstate_brwId]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_brwId]  DEFAULT ((0)) FOR [brwId]
GO
/****** Object:  Default [DF_ubc_downloadstate_result]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_result]  DEFAULT ((0)) FOR [result]
GO
/****** Object:  Default [DF_ubc_downloadstate_downState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_downState]  DEFAULT ((0)) FOR [downState]
GO
/****** Object:  Default [DF_ubc_downloadstate_programState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_programState]  DEFAULT ((0)) FOR [programState]
GO
/****** Object:  Default [DF_ubc_downloadstate_progress]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstate] ADD  CONSTRAINT [DF_ubc_downloadstate_progress]  DEFAULT ('0/0') FOR [progress]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_brwId]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_brwId]  DEFAULT ((0)) FOR [brwId]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_result]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_result]  DEFAULT ((0)) FOR [result]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_downState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_downState]  DEFAULT ((0)) FOR [downState]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_programState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_programState]  DEFAULT ((0)) FOR [programState]
GO
/****** Object:  Default [DF_ubc_downloadstatelog_progress]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstatelog] ADD  CONSTRAINT [DF_ubc_downloadstatelog_progress]  DEFAULT ('0/0') FOR [progress]
GO
/****** Object:  Default [DF__ubc_downl__brwId__45A94D10]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstateSummary] ADD  DEFAULT ((0)) FOR [brwId]
GO
/****** Object:  Default [DF__ubc_downl__progr__469D7149]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstateSummary] ADD  DEFAULT ((0)) FOR [programState]
GO
/****** Object:  Default [DF__ubc_downl__progr__47919582]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_downloadstateSummary] ADD  DEFAULT ('0/0') FOR [progress]
GO
/****** Object:  Default [DF_ubc_fault_counter]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_fault] ADD  CONSTRAINT [DF_ubc_fault_counter]  DEFAULT ((1)) FOR [counter]
GO
/****** Object:  Default [DF_ubc_faultlog_counter]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_faultlog] ADD  CONSTRAINT [DF_ubc_faultlog_counter]  DEFAULT ((1)) FOR [counter]
GO
/****** Object:  Default [DF_ubc_fmo_duration]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_fmo] ADD  CONSTRAINT [DF_ubc_fmo_duration]  DEFAULT ((7)) FOR [duration]
GO
/****** Object:  Default [DF_ubc_frame_grade]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_grade]  DEFAULT ((2)) FOR [grade]
GO
/****** Object:  Default [DF_ubc_frame_isPIP]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_isPIP]  DEFAULT ((0)) FOR [isPIP]
GO
/****** Object:  Default [DF_ubc_frame_borderStyle]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_borderStyle]  DEFAULT ('solid') FOR [borderStyle]
GO
/****** Object:  Default [DF_ubc_frame_borderThickness]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_borderThickness]  DEFAULT ((0)) FOR [borderThickness]
GO
/****** Object:  Default [DF_ubc_frame_borderColor]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_borderColor]  DEFAULT ('#FFFFFF') FOR [borderColor]
GO
/****** Object:  Default [DF_ubc_frame_cornerRadius]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_cornerRadius]  DEFAULT ((0)) FOR [cornerRadius]
GO
/****** Object:  Default [DF_ubc_frame_zindex]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_zindex]  DEFAULT ((0)) FOR [zindex]
GO
/****** Object:  Default [DF_ubc_frame_alpha]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_alpha]  DEFAULT ((0)) FOR [alpha]
GO
/****** Object:  Default [DF_ubc_frame_isTV]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_frame] ADD  CONSTRAINT [DF_ubc_frame_isTV]  DEFAULT ((0)) FOR [isTV]
GO
/****** Object:  Default [DF__ubc_host__period__2744C181]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((60)) FOR [period]
GO
/****** Object:  Default [DF__ubc_host__adminS__2838E5BA]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF__ubc_host__operat__292D09F3]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [operationalState]
GO
/****** Object:  Default [DF__ubc_host__autoPo__2A212E2C]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [autoPowerFlag]
GO
/****** Object:  Default [DF__ubc_host__powerC__2B155265]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [powerControl]
GO
/****** Object:  Default [DF__ubc_host__cpuUse__2C09769E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0.00)) FOR [cpuUsed]
GO
/****** Object:  Default [DF__ubc_host__screen__2CFD9AD7]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((5)) FOR [screenshotPeriod]
GO
/****** Object:  Default [DF__ubc_host__displa__2DF1BF10]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [displayCounter]
GO
/****** Object:  Default [DF__ubc_host__networ__2EE5E349]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [networkUse1]
GO
/****** Object:  Default [DF__ubc_host__networ__2FDA0782]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [networkUse2]
GO
/****** Object:  Default [DF__ubc_host__schedu__30CE2BBB]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [scheduleApplied1]
GO
/****** Object:  Default [DF__ubc_host__schedu__31C24FF4]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [scheduleApplied2]
GO
/****** Object:  Default [DF__ubc_host__soundD__32B6742D]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [soundDisplay]
GO
/****** Object:  Default [DF__ubc_host__vncSta__33AA9866]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [vncState]
GO
/****** Object:  Default [DF__ubc_host__cpuTem__349EBC9F]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [cpuTemp]
GO
/****** Object:  Default [DF__ubc_host__create__3592E0D8]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [createTime]
GO
/****** Object:  Default [DF__ubc_host__autoUp__36870511]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [autoUpdateFlag]
GO
/****** Object:  Default [DF__ubc_host__playLo__377B294A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((15)) FOR [playLogDayLimit]
GO
/****** Object:  Default [DF__ubc_host__hddThr__386F4D83]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((90)) FOR [hddThreshold]
GO
/****** Object:  Default [DF__ubc_host__playLo__396371BC]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('00:00') FOR [playLogDayCriteria]
GO
/****** Object:  Default [DF__ubc_host__render__3A5795F5]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((1)) FOR [renderMode]
GO
/****** Object:  Default [DF__ubc_host__starte__3B4BBA2E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [starterState]
GO
/****** Object:  Default [DF__ubc_host__browse__3C3FDE67]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [browserState]
GO
/****** Object:  Default [DF__ubc_host__browse__3D3402A0]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [browser1State]
GO
/****** Object:  Default [DF__ubc_host__firmwa__3E2826D9]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [firmwareViewState]
GO
/****** Object:  Default [DF__ubc_host__preDow__3F1C4B12]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [preDownloaderState]
GO
/****** Object:  Default [DF__ubc_host__downlo__40106F4B]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [download1State]
GO
/****** Object:  Default [DF__ubc_host__downlo__41049384]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [download2State]
GO
/****** Object:  Default [DF__ubc_host__progre__41F8B7BD]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('0/0') FOR [progress1]
GO
/****** Object:  Default [DF__ubc_host__progre__42ECDBF6]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('0/0') FOR [progress2]
GO
/****** Object:  Default [DF__ubc_host__monito__43E1002F]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [monitoroff]
GO
/****** Object:  Default [DF__ubc_host__hostTy__44D52468]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((0)) FOR [hostType]
GO
/****** Object:  Default [DF__ubc_host__monito__45C948A1]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((-1)) FOR [monitorState]
GO
/****** Object:  Default [DF__ubc_host__monito__46BD6CDA]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((-1)) FOR [monitorUseTime]
GO
/****** Object:  Default [DF__ubc_host__gmt__47B19113]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  CONSTRAINT [DF__ubc_host__gmt__47B19113]  DEFAULT ((9999)) FOR [gmt]
GO
/****** Object:  Default [DF__ubc_host__disk1A__6CE315C2]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((-1)) FOR [disk1Avail]
GO
/****** Object:  Default [DF__ubc_host__disk2A__6DD739FB]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ((-1)) FOR [disk2Avail]
GO
/****** Object:  Default [DF__ubc_host__protoc__1308BEAA]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host] ADD  DEFAULT ('C') FOR [protocolType]
GO
/****** Object:  Default [DF__ubc_host___perio__161A357F]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((60)) FOR [period]
GO
/****** Object:  Default [DF__ubc_host___admin__170E59B8]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF__ubc_host___opera__18027DF1]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [operationalState]
GO
/****** Object:  Default [DF__ubc_host___autoP__18F6A22A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((0)) FOR [autoPowerFlag]
GO
/****** Object:  Default [DF__ubc_host___power__19EAC663]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((0)) FOR [powerControl]
GO
/****** Object:  Default [DF__ubc_host___scree__1ADEEA9C]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((5)) FOR [screenshotPeriod]
GO
/****** Object:  Default [DF__ubc_host___displ__1BD30ED5]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [displayCounter]
GO
/****** Object:  Default [DF__ubc_host___netwo__1CC7330E]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [networkUse1]
GO
/****** Object:  Default [DF__ubc_host___netwo__1DBB5747]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [networkUse2]
GO
/****** Object:  Default [DF__ubc_host___sched__1EAF7B80]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [scheduleApplied1]
GO
/****** Object:  Default [DF__ubc_host___sched__1FA39FB9]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [scheduleApplied2]
GO
/****** Object:  Default [DF__ubc_host___sound__2097C3F2]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((1)) FOR [soundDisplay]
GO
/****** Object:  Default [DF__ubc_host___vncSt__218BE82B]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((0)) FOR [vncState]
GO
/****** Object:  Default [DF__ubc_host___cpuTe__22800C64]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_host_test] ADD  DEFAULT ((0)) FOR [cpuTemp]
GO
/****** Object:  Default [DF__ubc_inter__keywo__270FB757]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_interactivelog] ADD  DEFAULT (NULL) FOR [keyword1]
GO
/****** Object:  Default [DF__ubc_inter__keywo__2803DB90]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_interactivelog] ADD  DEFAULT (NULL) FOR [keyword2]
GO
/****** Object:  Default [DF__ubc_inter__keywo__28F7FFC9]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_interactivelog] ADD  DEFAULT (NULL) FOR [keyword3]
GO
/****** Object:  Default [DF__ubc_inter__count__29EC2402]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_interactivelog] ADD  DEFAULT ('0') FOR [counter]
GO
/****** Object:  Default [DF_ubc_lmo_duration]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_duration]  DEFAULT ((3)) FOR [duration]
GO
/****** Object:  Default [DF_ubc_lmo_powerState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_powerState]  DEFAULT ((0)) FOR [powerState]
GO
/****** Object:  Default [DF_ubc_lmo_connectionState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_connectionState]  DEFAULT ((0)) FOR [connectionState]
GO
/****** Object:  Default [DF_ubc_lmo_scheduleState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_scheduleState]  DEFAULT ((0)) FOR [scheduleState]
GO
/****** Object:  Default [DF_ubc_lmo_faultState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_faultState]  DEFAULT ((0)) FOR [faultState]
GO
/****** Object:  Default [DF_ubc_lmo_loginState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_loginState]  DEFAULT ((0)) FOR [loginState]
GO
/****** Object:  Default [DF_ubc_lmo_estimateState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_estimateState]  DEFAULT ((0)) FOR [estimateState]
GO
/****** Object:  Default [DF_ubc_lmo_downloadState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo] ADD  CONSTRAINT [DF_ubc_lmo_downloadState]  DEFAULT ((0)) FOR [downloadState]
GO
/****** Object:  Default [DF__ubc_lmo_s__mgrId__12B3B8EF]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo_solution] ADD  DEFAULT ('') FOR [mgrId]
GO
/****** Object:  Default [DF__ubc_lmo_s__logTy__13A7DD28]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo_solution] ADD  DEFAULT ('0') FOR [logType]
GO
/****** Object:  Default [DF__ubc_lmo_s__durat__149C0161]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo_solution] ADD  DEFAULT ('3') FOR [duration]
GO
/****** Object:  Default [DF__ubc_lmo_s__admin__1590259A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_lmo_solution] ADD  DEFAULT ('0') FOR [adminState]
GO
/****** Object:  Default [DF_ubc_menuauth_menuType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_menuType]  DEFAULT ('S') FOR [menuType]
GO
/****** Object:  Default [DF_ubc_menuauth_cAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_cAuth]  DEFAULT ((0)) FOR [cAuth]
GO
/****** Object:  Default [DF_ubc_menuauth_rAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_rAuth]  DEFAULT ((0)) FOR [rAuth]
GO
/****** Object:  Default [DF_ubc_menuauth_uAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_uAuth]  DEFAULT ((0)) FOR [uAuth]
GO
/****** Object:  Default [DF_ubc_menuauth_dAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_dAuth]  DEFAULT ((0)) FOR [dAuth]
GO
/****** Object:  Default [DF_ubc_menuauth_aAuth]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_menuauth] ADD  CONSTRAINT [DF_ubc_menuauth_aAuth]  DEFAULT ((0)) FOR [aAuth]
GO
/****** Object:  Default [DF__ubc_mir__indexOr__3805392F]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_mir] ADD  DEFAULT ((-1)) FOR [indexOrder]
GO
/****** Object:  Default [DF_ubc_playlog_playCount]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_playlog] ADD  CONSTRAINT [DF_ubc_playlog_playCount]  DEFAULT ((0)) FOR [playCount]
GO
/****** Object:  Default [DF_ubc_playlog_playTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_playlog] ADD  CONSTRAINT [DF_ubc_playlog_playTime]  DEFAULT ((0)) FOR [playTime]
GO
/****** Object:  Default [DF_ubc_playlog_planTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_playlog] ADD  CONSTRAINT [DF_ubc_playlog_planTime]  DEFAULT ((0)) FOR [planTime]
GO
/****** Object:  Default [DF_ubc_playlog_failCount]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_playlog] ADD  CONSTRAINT [DF_ubc_playlog_failCount]  DEFAULT ((0)) FOR [failCount]
GO
/****** Object:  Default [DF__ubc_pmo__ftpPort__75235608]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_pmo] ADD  DEFAULT ((21)) FOR [ftpPort]
GO
/****** Object:  Default [DF__ubc_pmo__current__76177A41]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_pmo] ADD  DEFAULT ((-1)) FOR [currentConnectionCount]
GO
/****** Object:  Default [DF__ubc_pmo__lastUpd__770B9E7A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_pmo] ADD  DEFAULT (getdate()) FOR [lastUpdateTime]
GO
/****** Object:  Default [DF__ubc_pmo__adminst__77FFC2B3]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_pmo] ADD  DEFAULT ((1)) FOR [adminstate]
GO
/****** Object:  Default [DF_ubc_program_contentsCategory]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_contentsCategory]  DEFAULT ((0)) FOR [contentsCategory]
GO
/****** Object:  Default [DF_ubc_program_purpose]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_purpose]  DEFAULT ((0)) FOR [purpose]
GO
/****** Object:  Default [DF_ubc_program_hostType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_hostType]  DEFAULT ((0)) FOR [hostType]
GO
/****** Object:  Default [DF_ubc_program_vertical]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_vertical]  DEFAULT ((0)) FOR [vertical]
GO
/****** Object:  Default [DF_ubc_program_resolution]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_resolution]  DEFAULT ((0)) FOR [resolution]
GO
/****** Object:  Default [DF_ubc_program_isPublic]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_isPublic]  DEFAULT ((0)) FOR [isPublic]
GO
/****** Object:  Default [DF_ubc_program_isVerify]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  CONSTRAINT [DF_ubc_program_isVerify]  DEFAULT ((1)) FOR [isVerify]
GO
/****** Object:  Default [DF__ubc_progr__volum__7484378A]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_program] ADD  DEFAULT ((0)) FOR [volume]
GO
/****** Object:  Default [DF_ubc_rolemap_menuType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_rolemap] ADD  CONSTRAINT [DF_ubc_rolemap_menuType]  DEFAULT ('S') FOR [menuType]
GO
/****** Object:  Default [DF_ubc_schedule_openningFlag]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_openningFlag]  DEFAULT ((1)) FOR [openningFlag]
GO
/****** Object:  Default [DF_ubc_schedule_priority]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_priority]  DEFAULT ((0)) FOR [priority]
GO
/****** Object:  Default [DF_ubc_schedule_castingState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_castingState]  DEFAULT ((0)) FOR [castingState]
GO
/****** Object:  Default [DF_ubc_schedule_operationalState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_operationalState]  DEFAULT ((1)) FOR [operationalState]
GO
/****** Object:  Default [DF_ubc_schedule_adminState]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_adminState]  DEFAULT ((1)) FOR [adminState]
GO
/****** Object:  Default [DF_ubc_schedule_isOrigin]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_isOrigin]  DEFAULT ((1)) FOR [isOrigin]
GO
/****** Object:  Default [DF_ubc_schedule_runningTime]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_schedule] ADD  CONSTRAINT [DF_ubc_schedule_runningTime]  DEFAULT ((0)) FOR [runningTime]
GO
/****** Object:  Default [DF_ubc_smtplog_wdate]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_smtplog] ADD  CONSTRAINT [DF_ubc_smtplog_wdate]  DEFAULT (getdate()) FOR [wdate]
GO
/****** Object:  Default [DF_ubc_template_bgColor]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_template] ADD  CONSTRAINT [DF_ubc_template_bgColor]  DEFAULT ('#c0c0c0') FOR [bgColor]
GO
/****** Object:  Default [DF__ubc_test__monito__0C90CB45]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_test] ADD  DEFAULT ((-1)) FOR [monitorState]
GO
/****** Object:  Default [DF_ubc_th_side]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_th] ADD  CONSTRAINT [DF_ubc_th_side]  DEFAULT ((1)) FOR [side]
GO
/****** Object:  Default [DF_ubc_tp_zorder]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_tp] ADD  CONSTRAINT [DF_ubc_tp_zorder]  DEFAULT ('0') FOR [zorder]
GO
/****** Object:  Default [DF_ubc_tp_state]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_tp] ADD  CONSTRAINT [DF_ubc_tp_state]  DEFAULT ((0)) FOR [state]
GO
/****** Object:  Default [DF_ubc_user_userType]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_userType]  DEFAULT ((3)) FOR [userType]
GO
/****** Object:  Default [DF_ubc_user_useEmail]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_useEmail]  DEFAULT ((0)) FOR [useEmail]
GO
/****** Object:  Default [DF_ubc_user_useSms]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_user] ADD  CONSTRAINT [DF_ubc_user_useSms]  DEFAULT ((0)) FOR [useSms]
GO
/****** Object:  ForeignKey [FK_ubc_defaultschedule_ubc_defaultschedule]    Script Date: 07/24/2012 19:50:40 ******/
ALTER TABLE [dbo].[ubc_defaultschedule]  WITH CHECK ADD  CONSTRAINT [FK_ubc_defaultschedule_ubc_defaultschedule] FOREIGN KEY([serialNo])
REFERENCES [dbo].[ubc_defaultschedule] ([serialNo])
GO
ALTER TABLE [dbo].[ubc_defaultschedule] CHECK CONSTRAINT [FK_ubc_defaultschedule_ubc_defaultschedule]
GO

CREATE TABLE  ubc_actionlog (
  logDate  varchar(50) NOT NULL,
  logTime  varchar(50) NOT NULL,
  logId varchar(50) NOT NULL,
  logIp varchar(50) DEFAULT NULL,
  result  varchar(50),
  windowName varchar(256) DEFAULT NULL,
  actionType varchar(256) DEFAULT NULL,
  fileName varchar(256) DEFAULT NULL,
  fileSzie varchar(256) DEFAULT NULL
)
GO

CREATE TABLE  ubc_loginlog (
  logDate  varchar(50) NOT NULL,
  logTime  varchar(50) NOT NULL,
  logId varchar(50) NOT NULL,
  logIp varchar(50) DEFAULT NULL,
  result  varchar(50)
)
GO

CREATE TABLE  ubc_authchangelog (
  logDate  varchar(50) NOT NULL,
  logTime  varchar(50) NOT NULL,
  logId varchar(50) NOT NULL,
  changeType varchar(50) DEFAULT NULL,
  changeString varchar(255) 
)
GO

CREATE TABLE  ubc_connectionDailyStat (
  statDate datetime NOT NULL,
  siteId varchar(255) NOT NULL,
  hostId varchar(255) NOT NULL,
  disconnectCount bigInt NULL,
  disconnectSec  bigInt NULL
) 
GO
