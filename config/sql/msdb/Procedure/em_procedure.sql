USE [ubc]
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_log_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_log_create]
    @p_log_table varchar(6) 
AS
    DECLARE @dsql nvarchar(4000)

    IF @p_log_table <> ''
    BEGIN

        IF NOT EXISTS (
			SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_smt_log_'+@p_log_table
		)
        BEGIN
 
            select @dsql = '
                CREATE TABLE em_smt_log_' + @p_log_table + ' (
                    mt_pr int NOT NULL,
                    mt_seq int NOT NULL,
                    mt_refkey varchar(20) COLLATE Korean_Wansung_CS_AS,
                    priority char(2) COLLATE Korean_Wansung_CS_AS NOT NULL default ''S'',
                    date_client_req datetime NOT NULL,
                    content varchar(255) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    callback varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    broadcast_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL ,
                    msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default ''1'',
                    recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS,
                    change_word1 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word2 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word3 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word4 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word5 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    date_mt_sent datetime,
                    date_rslt datetime,
                    date_mt_report datetime,
                    mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
                    mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
                    carrier int,
                    rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
                    recipient_net int,
                    recipient_npsend char(1) COLLATE Korean_Wansung_CS_AS,
                    country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default ''82'',
                    charset varchar(20) COLLATE Korean_Wansung_CS_AS,
                    msg_type int,
                    crypto_yn char(1) COLLATE Korean_Wansung_CS_AS default ''Y''
                )'
    
            EXEC (@dsql)
    
            EXEC ('ALTER TABLE em_smt_log_'+@p_log_table+' ADD PRIMARY KEY ( mt_pr, mt_seq )')

            EXEC ('CREATE INDEX idx_em_smt_log_'+@p_log_table+'_1 ON em_smt_log_'+@p_log_table+' (date_client_req, recipient_num)')
            EXEC ('CREATE INDEX idx_em_smt_log_'+@p_log_table+'_2 ON em_smt_log_'+@p_log_table+' (date_mt_report, mt_report_code_ib)')
            EXEC ('CREATE INDEX idx_em_smt_log_'+@p_log_table+'_3 ON em_smt_log_'+@p_log_table+' (msg_status)')

        END
    END


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_create]
AS
    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_smt_tran'
	)
    BEGIN
        CREATE TABLE em_smt_tran (
            mt_pr int identity(1,1) NOT NULL ,
            mt_refkey varchar(20) COLLATE Korean_Wansung_CS_AS,
            priority char(2) COLLATE Korean_Wansung_CS_AS NOT NULL default 'S',
            date_client_req datetime NOT NULL default '1970-01-01 00:00:00',
            content varchar(255) COLLATE Korean_Wansung_CS_AS NOT NULL,
            callback varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
            service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            broadcast_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default 'N',
            msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default '1',
            recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS,
            date_mt_sent datetime,
            date_rslt datetime,
            date_mt_report datetime,
            mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
            mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
            carrier int,
            rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
            recipient_net int,
            recipient_npsend varchar(1) COLLATE Korean_Wansung_CS_AS,
            country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default '82',
            charset varchar(20) COLLATE Korean_Wansung_CS_AS,
            msg_type int,
            crypto_yn char(1) COLLATE Korean_Wansung_CS_AS default 'Y'
        )
        
        ALTER TABLE em_smt_tran  ADD PRIMARY KEY ( mt_pr )
        
        CREATE INDEX idx_em_smt_tran_1 ON em_smt_tran (msg_status, date_client_req)

        CREATE INDEX idx_em_smt_tran_2 ON em_smt_tran (recipient_num)

    END


    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_smt_client'
	)
    BEGIN
        CREATE TABLE em_smt_client (
            mt_pr int NOT NULL,
            mt_seq int NOT NULL,
            msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default '1',
            recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
            change_word1 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word2 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word3 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word4 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word5 varchar(20) COLLATE Korean_Wansung_CS_AS,
            date_mt_sent datetime,
            date_rslt datetime,
            date_mt_report datetime,
            mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
            mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
            carrier int,
            rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
            recipient_net int,
            recipient_npsend varchar(1) COLLATE Korean_Wansung_CS_AS,
            country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default '82'
        )
        
        ALTER TABLE em_smt_client  ADD PRIMARY KEY ( mt_pr, mt_seq )

        CREATE INDEX idx_em_smt_client_1 ON em_smt_client (recipient_num)

        CREATE INDEX idx_em_smt_client_2 ON em_smt_client (msg_status)
    END


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_stat_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_stat_create]
AS
    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_statistics_m' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_statistics_m (
            stat_date varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL,
            stat_servicetype char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
			stat_payment_code varchar(20) COLLATE Korean_Wansung_CS_AS NOT NULL,
			stat_carrier int NOT NULL,
            stat_success int,
			stat_failure int,
			stat_invalid int,
			stat_invalid_ib int,
			stat_remained int,
            stat_regdate datetime NOT NULL default '1970-01-01 00:00:00'
        )
        
        ALTER TABLE em_statistics_m  ADD PRIMARY KEY(stat_date, stat_servicetype, stat_payment_code, stat_carrier)

    END


    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_statistics_d' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_statistics_d (
            stat_date varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL,
            stat_servicetype char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
			stat_payment_code varchar(20) COLLATE Korean_Wansung_CS_AS NOT NULL,
			stat_carrier int NOT NULL,
            stat_fail_code varchar(10) COLLATE Korean_Wansung_CS_AS NOT NULL,
            stat_fail_cnt int,
            stat_regdate datetime NOT NULL default '1970-01-01 00:00:00'
        )
        
        ALTER TABLE em_resultcode  ADD PRIMARY KEY(stat_date, stat_servicetype, stat_payment_code, stat_carrier, stat_fail_code)

    END

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_update]
    @p_table_divi               char(1),
    @p_update_all_yn            char(1),
    @p_mt_pr                    int,
    @p_mt_seq                   int,
    @p_msg_status               char(1),
    @p_mt_report_code_ib        char(4),
    @p_mt_report_code_ibtype    char(1)
AS
    DECLARE @dsql nvarchar(1000)
	DECLARE @v_params as nvarchar(200)

    IF @p_table_divi = 'M'
        SET @dsql = ' UPDATE em_smt_tran SET  '
    ELSE
        SET @dsql = ' UPDATE em_smt_client SET '
 
    SET @dsql = @dsql + '
        msg_status            = @pp_msg_status,
        mt_report_code_ib     = @pp_mt_report_code_ib,
        mt_report_code_ibtype = @pp_mt_report_code_ibtype,
        date_mt_sent		  = getdate() '

    IF @p_msg_status = '3'
        SET @dsql = @dsql + ' , date_rslt  = getdate() '

    SET @dsql = @dsql + ' WHERE mt_pr  = @pp_mt_pr '

    IF ( @p_table_divi = 'D' AND @p_update_all_yn = 'N' )
        SET @dsql = @dsql + ' AND mt_seq  = ' + convert(varchar(11), @p_mt_seq) 

	SET @v_params = '@pp_msg_status char(1),
					 @pp_mt_report_code_ib char(4),
					 @pp_mt_report_code_ibtype char(1),
					 @pp_mt_pr int'
      


	EXECUTE sp_executesql @dsql, @v_params, 
								 @pp_msg_status = @p_msg_status,
								 @pp_mt_report_code_ib = @p_mt_report_code_ib,
								 @pp_mt_report_code_ibtype = @p_mt_report_code_ibtype,
								 @pp_mt_pr = @p_mt_pr


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_smt_tran_select]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_smt_tran_select]
    @p_service_type varchar(20),
    @p_priority char(2),
    @p_ttl int,
    @p_emma_id char(2),
    @p_bancheck_yn char(1)
  
AS
    DECLARE @dsql nvarchar(4000)
	DECLARE @v_params as nvarchar(500)

    SET @dsql = '
        SELECT TOP 300
            A.mt_pr             AS mt_pr,
            A.mt_refkey         AS mt_refkey,
            A.content           AS content,
            A.priority          AS priority,
            A.broadcast_yn      AS broadcast_yn,
            A.callback          AS callback,
            A.recipient_num     AS recipient_num,
            A.recipient_net     AS recipient_net,
            A.recipient_npsend  AS recipient_npsend,
            A.country_code      AS country_code,
            A.date_client_req   AS date_client_req,
            A.charset           AS charset,
            A.msg_type          AS msg_type,
            A.crypto_yn         AS crypto_yn,
            A.service_type      AS service_type,
            B.ban_type          AS ban_type,
            B.send_yn           AS send_yn
        FROM em_smt_tran A 
        LEFT OUTER JOIN em_banlist B 
        ON  A.recipient_num = B.content
        AND A.service_type = B.service_type
        AND B.ban_type = @pp_ban_type
        AND B.ban_status_yn = @pp_ban_status_yn
        WHERE A.priority = @pp_priority
        AND A.msg_status = @pp_msg_status 
        AND A.date_client_req BETWEEN (getdate() - @pp_ttl/24/60) AND getdate() '

    IF @p_service_type = 'SMT'
        SET @dsql = @dsql + ' AND   A.service_type = ''0'' '
    
    IF @p_service_type = 'URL'
        SET @dsql = @dsql + ' AND   A.service_type = ''1'' '

    SET @v_params = '
		@pp_ban_type char(1),
		@pp_ban_status_yn char(1),
		@pp_priority char(2),
		@pp_msg_status char(1),
		@pp_ttl int'

    EXECUTE sp_executesql @dsql, @v_params, 
								 @pp_ban_type = 'R',
								 @pp_ban_status_yn = 'Y',
								 @pp_priority = @p_priority,
								 @pp_msg_status = '1',
								 @pp_ttl = @p_ttl


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_create]
AS
    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_mmt_tran'
	)
    BEGIN
        CREATE TABLE em_mmt_tran (
            mt_pr int identity(1,1) NOT NULL ,
            mt_refkey varchar(20) COLLATE Korean_Wansung_CS_AS,
            priority char(2) COLLATE Korean_Wansung_CS_AS NOT NULL default 'S',
            msg_class char(1) default '1',
            date_client_req datetime NOT NULL default '1970-01-01 00:00:00',
            subject varchar(40) COLLATE Korean_Wansung_CS_AS NOT NULL,
			content_type int default 0,
            content varchar(4000) COLLATE Korean_Wansung_CS_AS NOT NULL,
            attach_file_group_key int default 0,
            callback varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
            service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            broadcast_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default 'N',
            msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default '1',
            recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS,
            date_mt_sent datetime,
            date_rslt datetime,
            date_mt_report datetime,
            mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
            mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
            carrier int,
            rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
            recipient_net int,
            recipient_npsend varchar(1) COLLATE Korean_Wansung_CS_AS,
            country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default '82',
            charset varchar(20) COLLATE Korean_Wansung_CS_AS,
            msg_type int,
            crypto_yn char(1) COLLATE Korean_Wansung_CS_AS default 'Y'
        )
        
        ALTER TABLE em_mmt_tran  ADD PRIMARY KEY ( mt_pr )
        
        CREATE INDEX idx_em_mmt_tran_1 ON em_mmt_tran (msg_status, date_client_req)

        CREATE INDEX idx_em_mmt_tran_2 ON em_mmt_tran (recipient_num)

        CREATE INDEX idx_em_mmt_tran_3 ON em_mmt_tran (attach_file_group_key)

    END


    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_mmt_client'
	)
    BEGIN
        CREATE TABLE em_mmt_client (
            mt_pr int NOT NULL,
            mt_seq int NOT NULL,
            msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default '1',
            recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
            change_word1 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word2 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word3 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word4 varchar(20) COLLATE Korean_Wansung_CS_AS,
            change_word5 varchar(20) COLLATE Korean_Wansung_CS_AS,
            date_mt_sent datetime,
            date_rslt datetime,
            date_mt_report datetime,
            mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
            mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
            carrier int,
            rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
            recipient_net int,
            recipient_npsend varchar(1) COLLATE Korean_Wansung_CS_AS,
            country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default '82'
        )
        
        ALTER TABLE em_mmt_client  ADD PRIMARY KEY ( mt_pr, mt_seq )

        CREATE INDEX idx_em_mmt_client_1 ON em_mmt_client(recipient_num)

        CREATE INDEX idx_em_mmt_client_2 ON em_mmt_client (msg_status)

    END


    /* create em_mmt_file table, file detail */
	IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_mmt_file'
	)
    BEGIN
        CREATE TABLE em_mmt_file (
            attach_file_group_key int NOT NULL,
            attach_file_group_seq int NOT NULL,
            attach_file_seq int NOT NULL,
            attach_file_subpath varchar(64) COLLATE Korean_Wansung_CS_AS,
            attach_file_name varchar(64) COLLATE Korean_Wansung_CS_AS NOT NULL,
            attach_file_carrier int,
            reg_date datetime NOT NULL default '1970-01-01 00:00:00'
        )
        
        ALTER TABLE em_mmt_file  ADD PRIMARY KEY ( attach_file_group_key, attach_file_group_seq )

        CREATE INDEX idx_em_mmt_file_1 ON em_mmt_file(attach_file_seq)

        CREATE INDEX idx_em_mmt_file_2 ON em_mmt_file(attach_file_name)

    END


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_log_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_log_create]
    @p_log_table varchar(6) 
AS
    DECLARE @dsql nvarchar(4000)

    IF @p_log_table <> ''
    BEGIN
		IF NOT EXISTS (
			SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_mmt_log_'+@p_log_table
		)
        BEGIN
 
            select @dsql = '
                CREATE TABLE em_mmt_log_' + @p_log_table + ' (
                    mt_pr int NOT NULL,
                    mt_seq int NOT NULL,
                    mt_refkey varchar(20) COLLATE Korean_Wansung_CS_AS,
                    priority char(2) COLLATE Korean_Wansung_CS_AS NOT NULL default ''S'',
                    msg_class char(1) COLLATE Korean_Wansung_CS_AS default ''1'',
                    date_client_req datetime NOT NULL,
                    subject varchar(40) COLLATE Korean_Wansung_CS_AS NOT NULL,
					content_type int default 0,
                    content varchar(4000) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    attach_file_group_key int,
                    callback varchar(25) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
                    broadcast_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL ,
                    msg_status char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default ''1'',
                    recipient_num varchar(25) COLLATE Korean_Wansung_CS_AS,
                    change_word1 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word2 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word3 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word4 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    change_word5 varchar(20) COLLATE Korean_Wansung_CS_AS,
                    date_mt_sent datetime,
                    date_rslt datetime,
                    date_mt_report datetime,
                    mt_report_code_ib char(4) COLLATE Korean_Wansung_CS_AS,
                    mt_report_code_ibtype char(1) COLLATE Korean_Wansung_CS_AS,
                    carrier int,
                    rs_id varchar(20) COLLATE Korean_Wansung_CS_AS,
                    recipient_net int,
                    recipient_npsend char(1) COLLATE Korean_Wansung_CS_AS,
                    country_code varchar(8) COLLATE Korean_Wansung_CS_AS NOT NULL default ''82'',
                    charset varchar(20) COLLATE Korean_Wansung_CS_AS,
                    msg_type int,
                    crypto_yn char(1) COLLATE Korean_Wansung_CS_AS default ''Y''
                )'
    
            EXEC (@dsql)
    
            EXEC ('ALTER TABLE em_mmt_log_'+@p_log_table+' ADD PRIMARY KEY ( mt_pr, mt_seq )')

            EXEC ('CREATE INDEX idx_em_mmt_log_'+@p_log_table+'_1 ON em_mmt_log_'+@p_log_table+' (date_client_req, recipient_num)')
            EXEC ('CREATE INDEX idx_em_mmt_log_'+@p_log_table+'_2 ON em_mmt_log_'+@p_log_table+' (date_mt_report, mt_report_code_ib)')
            EXEC ('CREATE INDEX idx_em_mmt_log_'+@p_log_table+'_3 ON em_mmt_log_'+@p_log_table+' (msg_status)')

        END
    END


RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mon_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mon_create]
AS
    IF NOT EXISTS (
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'em_status'
	)
    BEGIN
        CREATE TABLE em_status (
			emma_id char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            process_name varchar(40) COLLATE Korean_Wansung_CS_AS NOT NULL,
            pid varchar(20) COLLATE Korean_Wansung_CS_AS NOT NULL,
			service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
			cnt_today int,
			cnt_total int,
			cnt_resent_1 int,
			cnt_resent_10 int,
			que_size int,
			conn_time varchar(20) COLLATE Korean_Wansung_CS_AS ,
			update_time varchar(20) COLLATE Korean_Wansung_CS_AS ,
			conn_gw_info varchar(40) COLLATE Korean_Wansung_CS_AS ,
			reg_date datetime NOT NULL default '1970-01-01 00:00:00'
        )
        
        ALTER TABLE em_status  ADD PRIMARY KEY(emma_id, process_name, pid, service_type)

    END

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_mmt_update]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_mmt_update]
    @p_table_divi               char(1),
    @p_update_all_yn            char(1),
    @p_mt_pr                    int,
    @p_mt_seq                   int,
    @p_msg_status               char(1),
    @p_mt_report_code_ib        char(4),
    @p_mt_report_code_ibtype    char(1)
AS
    DECLARE @dsql nvarchar(1000)
	DECLARE @v_params as nvarchar(200)

    IF @p_table_divi = 'M'
        SET @dsql = ' UPDATE em_mmt_tran SET  '
    ELSE
        SET @dsql = ' UPDATE em_mmt_client SET '
 
    SET @dsql = @dsql + '
        msg_status            = @pp_msg_status,
        mt_report_code_ib     = @pp_mt_report_code_ib,
        mt_report_code_ibtype = @pp_mt_report_code_ibtype,
        date_mt_sent = getdate() '

    IF @p_msg_status = '3'
        SET @dsql = @dsql + ' , date_rslt  = getdate() '

    SET @dsql = @dsql + ' WHERE mt_pr  = @pp_mt_pr '

    IF ( @p_table_divi = 'D' AND @p_update_all_yn = 'N' )
        SET @dsql = @dsql + ' AND mt_seq  = ' + convert(varchar(11), @p_mt_seq)    

	SET @v_params = '@pp_msg_status char(1),
					 @pp_mt_report_code_ib char(4),
					 @pp_mt_report_code_ibtype char(1),
					 @pp_mt_pr int'

	EXECUTE sp_executesql @dsql, @v_params, 
								 @pp_msg_status = @p_msg_status,
								 @pp_mt_report_code_ib = @p_mt_report_code_ib,
								 @pp_mt_report_code_ibtype = @p_mt_report_code_ibtype,
								 @pp_mt_pr = @p_mt_pr

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_common_create]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_common_create]
AS
    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_banlist' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_banlist (
            service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            ban_seq int NOT NULL,
            ban_type char(1) COLLATE Korean_Wansung_CS_AS NOT NULL,
            content varchar(45) COLLATE Korean_Wansung_CS_AS NOT NULL,
            send_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default 'N',
            ban_status_yn char(1) COLLATE Korean_Wansung_CS_AS NOT NULL default 'Y',
            reg_date datetime NOT NULL default getdate(),
            reg_user varchar(20) COLLATE Korean_Wansung_CS_AS,
            update_date datetime NOT NULL default '1970-01-01 00:00:00',
            update_user varchar(20) COLLATE Korean_Wansung_CS_AS
        )
        
        ALTER TABLE em_banlist  ADD PRIMARY KEY(service_type, ban_seq)
        CREATE INDEX idx_em_banlist_1 ON em_banlist(ban_type, service_type, ban_status_yn)
		CREATE INDEX idx_em_banlist_2 ON em_banlist(content)

    END


    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_resultcode' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_resultcode (
            service_type char(2) COLLATE Korean_Wansung_CS_AS NOT NULL,
            rslt_code varchar(4) COLLATE Korean_Wansung_CS_AS NOT NULL,
            rslt_type char(1) COLLATE Korean_Wansung_CS_AS NOT NULL,
            rslt_name varchar(80) COLLATE Korean_Wansung_CS_AS NOT NULL,
            rslt_pname varchar(80) COLLATE Korean_Wansung_CS_AS
        )
        
        ALTER TABLE em_resultcode  ADD PRIMARY KEY(service_type, rslt_code)
        
    END

RETURN
GO
/****** Object:  StoredProcedure [dbo].[sp_em_common_checkprivilege]    Script Date: 07/24/2012 19:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_em_common_checkprivilege]
AS
    IF NOT EXISTS (
        SELECT name FROM sysobjects 
        WHERE name = 'em_temp' 
        AND type = 'U' )
    BEGIN
        CREATE TABLE em_temp (a char(1))

		DROP TABLE em_temp
    END

RETURN
GO
