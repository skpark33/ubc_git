USE [ubc]
GO

/****** Object:  UserDefinedFunction [dbo].[GetChildSite]    Script Date: 12/09/2013 20:19:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetChildSite]
(@franchizeType varchar(255), @siteId varchar(255))
RETURNS table
AS
return (
with cte (mgrId, siteName, parentId, siteId, lvl, name_path, id_path, franchizeType)
as
(
select 1 as mgrId, siteName, parentId, siteId, 1 as lvl, convert(varchar(255), '/' + siteName) name_path, convert(varchar(255), '/' + siteId) id_path, franchizeType
from ubc_site
--where parentId is null
where siteId = @siteId and franchizeType = @franchizeType
union all
select 1 as mgrId, a.siteName, a.parentId, a.siteId, lvl + 1, convert(varchar(255),  name_path + '/' + a.siteName) name_path, convert(varchar(255),  id_path + '/' + a.siteId) id_path, a.franchizeType
from ubc_site a, cte b
where a.parentId = b.siteId and a.franchizeType = b.franchizeType
)
select mgrId, siteName, siteId, parentId, lvl, name_path, id_path, franchizeType from cte
--order by lvl
)
GO


