USE [ubc]
GO

/****** Object:  UserDefinedFunction [dbo].[Password_sha256]    Script Date: 12/09/2013 20:23:26 ******/
CREATE FUNCTION [dbo].[Password_sha256](@pwd [nvarchar](512))
RETURNS [nvarchar](512) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [Sha256dll].[fnSha256].[pwdSha256]
GO

