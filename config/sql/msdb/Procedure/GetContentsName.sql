USE [ubc]
GO

/****** Object:  UserDefinedFunction [dbo].[GetContentsName]    Script Date: 12/09/2013 20:21:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetContentsName]
(@contentsId varchar(255))
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @contentsName varchar(255)
select @contentsName = contentsName from ubc_contents where contentsId = @contentsId
return @contentsName
END
GO

