USE [ubc]
GO

/****** Object:  UserDefinedFunction [dbo].[GetHostName]    Script Date: 12/09/2013 20:22:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetHostName]
(@hostId varchar(255))
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @hostName varchar(255)
select @hostName = hostName from ubc_host where hostId = @hostId
return @hostName
END
GO

