USE [ubc]
GO

/****** Object:  UserDefinedFunction [dbo].[Find_n_Right]    Script Date: 12/09/2013 20:20:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[Find_n_Right] 
 (
 	@findStr varchar(255), @fullStr varchar(255)
 )
 RETURNS varchar(255)
 WITH EXEC AS CALLER
 AS
 BEGIN
 declare @resultStr varchar(255)
 select @resultStr = RIGHT(@fullStr, LEN(@fullStr) - (CHARINDEX(@findStr, @fullStr)+LEN(@findStr)-1))
 return @resultStr
 END;
 
GO


