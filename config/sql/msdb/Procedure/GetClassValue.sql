USE [ubc]
GO

/****** Object:  UserDefinedFunction [dbo].[GetClassValue]    Script Date: 12/09/2013 20:20:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetClassValue]
(@inString varchar(255), @index int)
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @resultString varchar(255), @tempString varchar(255)
declare @oPos int, @nPos int, @curIndex int

set @oPos = 1 -- 구분문자 검색을 시작할 위치
set @nPos = 1 -- 구분문자 위치
set @curIndex = 1

while(@nPos > 0)
begin
  set @nPos = charindex('/', @inString, @oPos)
  
  if @nPos = 0
    set @tempString = right(@inString, len(@inString) - @oPos+1)
  else
    set @tempString = substring(@inString, @oPos, @nPos - @oPos)
    
  if @index = @curIndex
    set @resultString = @tempString
  
  set @curIndex = @curIndex + 1;
  set @oPos = @nPos + 1;
end

if len(@resultString) > 0
  set @resultString = right(@resultString, len(@resultString) - (charindex('=', @resultString)))

return @resultString
END
GO


