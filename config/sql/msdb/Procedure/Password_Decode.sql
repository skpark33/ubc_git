USE [ubc]
GO

/****** Object:  UserDefinedFunction [dbo].[Password_Decode]    Script Date: 12/09/2013 20:22:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 

 

--'*********************************************************************************    
--'프로시저명 : dbo.Password_Decode
--'프로시저기능설명 : base64암호화 해석
--'작성자 : 강동수
--'작성일 : 2007-03-28
--'수정자 : 임유석
--'수정일 : 2011-08-01
--'수정내용 :

--'*********************************************************************************  

CREATE      FUNCTION [dbo].[Password_Decode]
(
 @InputStrings varchar(50)
)
RETURNS varchar(50)
AS
BEGIN

 DECLARE  @ConvertTable binary(100)
  ,@ReturnStrings varchar(50)
  ,@InputSize int
  ,@Count  int
  ,@Before1 varchar(100)
  ,@Before2 varchar(100)
  ,@Before3 varchar(100)
  ,@After1 int
  ,@After2 int
  ,@After3 int
  ,@After4 int
  ,@cut_data varchar(4)


IF @InputStrings <>'' or @InputStrings is not null
BEGIN

  SET @ConvertTable = CAST('ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    + 'abcdefghijklmnopqrstuvwxyz'
    + '0123456789+/=' AS binary(100))
 
 
  SET @InputSize = DATALENGTH(@InputStrings)
  SET @Count = 1
  SET @ReturnStrings = ''
 
  WHILE (0=0) BEGIN
	IF @Count > @InputSize BREAK

	SET @cut_data = SUBSTRING(@InputStrings, @Count, 4)
	SET @After1 = CHARINDEX( CAST(SUBSTRING(@cut_data, 1, 1) as binary(1)),@ConvertTable) - 1
	SET @After2 = CHARINDEX( CAST(SUBSTRING(@cut_data, 2, 1) as binary(1)),@ConvertTable) - 1
	SET @After3 = CHARINDEX( CAST(SUBSTRING(@cut_data, 3, 1) as binary(1)),@ConvertTable) - 1
	SET @After4 = CHARINDEX( CAST(SUBSTRING(@cut_data, 4, 1) as binary(1)),@ConvertTable) - 1
	SET @Before1 = NCHAR(((@After2 & 48) / 16) | (@After1 * 4) & 255)
	SET @Before2 = NCHAR(((@After3 & 60) / 4) | (@After2 * 16) & 255)
	SET @Before3 = NCHAR((((@After3 & 3) * 64) & 255) | (@After4 & 63))
	
	--SET @ReturnStrings = @ReturnStrings + @Before1 + @Before2 + @Before3
	IF @Before1 <> '' and @Before1 is not null
	BEGIN
		SET @ReturnStrings = @ReturnStrings + @Before1
	END
	IF @Before2 <> '' and @Before2 is not null
	BEGIN
		SET @ReturnStrings = @ReturnStrings + @Before2
	END
	IF @Before3 <> '' and @Before3 is not null
	BEGIN
		SET @ReturnStrings = @ReturnStrings + @Before3
	END

	SET @Count = @Count + 4
  END

END

  RETURN @ReturnStrings

END

GO

