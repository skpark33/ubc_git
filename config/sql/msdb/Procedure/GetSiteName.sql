USE [ubc]
GO

/****** Object:  UserDefinedFunction [dbo].[GetSiteName]    Script Date: 12/09/2013 20:22:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetSiteName]
(@siteId varchar(255))
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @siteName varchar(255)
select @siteName = siteName from ubc_site where siteId = @siteId
return @siteName
END
GO

