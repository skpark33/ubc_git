﻿DELIMITER $$

DROP FUNCTION IF EXISTS `ubc`.`gen_fileId` $$
CREATE DEFINER=`utv`@`%` FUNCTION `gen_fileId`() RETURNS int(11)
BEGIN
  DECLARE  retval int;
  SELECT keyval INTO retval FROM utv_key_fileid;
  update utv_key_fileid set keyval=LAST_INSERT_ID(keyval+1);
  return retval;
END $$

DELIMITER ;