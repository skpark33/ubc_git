-- ----------------------------------------------------------------------
-- MySQL Migration Toolkit
-- SQL Create Script
-- ----------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

CREATE DATABASE IF NOT EXISTS `ubc`
  CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ubc`;
-- -------------------------------------
-- Tables

DROP TABLE IF EXISTS `ubc`.`call_postbox`;
CREATE TABLE `ubc`.`call_postbox` (
  `CALLID` INT(10) NOT NULL AUTO_INCREMENT,
  `DIRECTIVE` VARCHAR(64) NULL,
  `ENTITY` VARCHAR(64) NULL,
  `VALUESTRING` VARCHAR(4096) NULL,
  `CALLER` VARCHAR(64) NULL,
  `CREATETIME` DATETIME NULL,
  `CALLTIME` DATETIME NULL,
  `CHECKFLAG` INT(1) NULL,
  `CALLFLAG` INT(1) NULL,
  `ERRORFLAG` INT(1) NULL,
  `REMARKS` VARCHAR(64) NULL,
  PRIMARY KEY (`CALLID`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`event_postbox`;
CREATE TABLE `ubc`.`event_postbox` (
  `EVENTID` INT(11) NOT NULL AUTO_INCREMENT,
  `DOMAIN` VARCHAR(64) NULL,
  `ENTITY` VARCHAR(64) NULL,
  `EVENTTYPE` VARCHAR(64) NULL,
  `EVENTVALUE` VARCHAR(1024) NULL,
  `CREATETIME` DATETIME NULL,
  `SENDTIME` DATETIME NULL,
  `CHECKFLAG` DECIMAL(1, 0) NULL,
  `SENDFLAG` DECIMAL(1, 0) NULL,
  `ERRORFLAG` DECIMAL(1, 0) NULL,
  `REMARKS` VARCHAR(64) NULL,
  PRIMARY KEY (`EVENTID`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`reply_postbox`;
CREATE TABLE `ubc`.`reply_postbox` (
  `CALLID` INT(10) NOT NULL,
  `REPLYID` INT(10) NOT NULL,
  `ENTITY` VARCHAR(64) NULL,
  `VALUESTRING` VARCHAR(4096) NULL,
  `CREATETIME` DATETIME NULL,
  `CHECKFLAG` INT(1) NULL,
  `ERRORFLAG` INT(1) NULL,
  `REMARKS` VARCHAR(64) NULL,
  INDEX `REPLY_POSTBOX_IDX` (`CALLID`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`ts_cronjob`;
CREATE TABLE `ubc`.`ts_cronjob` (
  `cronJobId` VARCHAR(100) NOT NULL DEFAULT '',
  `exeTime` VARCHAR(50) NULL,
  `exeType` VARCHAR(20) NULL,
  `entity` VARCHAR(200) NULL,
  `directive` VARCHAR(64) NULL,
  `attributes` VARCHAR(1000) NULL,
  `operationalStatus` DECIMAL(1, 0) NULL DEFAULT '1',
  PRIMARY KEY (`cronJobId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_adrequest`;
CREATE TABLE `ubc`.`utv_adrequest` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `requestId` VARCHAR(255) NOT NULL DEFAULT '',
  `userId` VARCHAR(255) NOT NULL DEFAULT 'system',
  `requestRoute` DECIMAL(3, 0) NULL,
  `smsFlag` DECIMAL(1, 0) NULL,
  `mobileNo` VARCHAR(255) NULL,
  `requestTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `requestState` DECIMAL(3, 0) NULL,
  `requestStateTime` DATETIME NULL,
  `requestStateHistory` VARCHAR(255) NULL,
  `deferedPay` DECIMAL(1, 0) NULL DEFAULT '0',
  `result` VARCHAR(255) NULL,
  `successRatio` DOUBLE NULL,
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `siteIdList` VARCHAR(255) NULL,
  `addr1List` VARCHAR(255) NULL,
  `addr2List` VARCHAR(255) NULL,
  `addr3List` VARCHAR(255) NULL,
  `franchizeType` VARCHAR(255) NULL,
  `hostIdList` VARCHAR(255) NULL,
  `priority` DECIMAL(5, 0) NULL DEFAULT '0',
  `templateId` VARCHAR(255) NULL,
  `frameId` VARCHAR(255) NULL,
  `contentsId` VARCHAR(255) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `startTime` VARCHAR(50) NULL,
  `endTime` VARCHAR(50) NULL,
  `iterator` DECIMAL(5, 0) NULL DEFAULT '1',
  `openningFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `phoneNumber` VARCHAR(255) NULL,
  `isTimedSchedule` DECIMAL(1, 0) NULL DEFAULT '1',
  `timeScope` DECIMAL(5, 0) NULL DEFAULT '0',
  `playOrder` DECIMAL(5, 0) NULL DEFAULT '0',
  `parentScheduleId` VARCHAR(255) NULL,
  `targetId` VARCHAR(255) NULL DEFAULT '"default"',
  PRIMARY KEY (`mgrId`, `requestId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_adrequestlog`;
CREATE TABLE `ubc`.`utv_adrequestlog` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `requestId` VARCHAR(255) NOT NULL DEFAULT '',
  `userId` VARCHAR(255) NOT NULL DEFAULT 'system',
  `requestRoute` DECIMAL(3, 0) NULL,
  `smsFlag` DECIMAL(1, 0) NULL,
  `mobileNo` VARCHAR(255) NULL,
  `requestTime` DATETIME NULL,
  `requestState` DECIMAL(3, 0) NULL,
  `requestStateTime` DATETIME NULL,
  `requestStateHistory` VARCHAR(255) NULL,
  `deferedPay` DECIMAL(1, 0) NULL DEFAULT '0',
  `result` VARCHAR(255) NULL,
  `successRatio` DOUBLE NULL,
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `siteIdList` VARCHAR(255) NULL,
  `addr1List` VARCHAR(255) NULL,
  `franchizeType` VARCHAR(255) NULL,
  `hostIdList` VARCHAR(255) NULL,
  `priority` DECIMAL(5, 0) NULL DEFAULT '0',
  `templateId` VARCHAR(255) NULL,
  `frameId` VARCHAR(255) NULL,
  `contentsIdList` VARCHAR(1024) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `startTime` VARCHAR(50) NULL,
  `endTime` VARCHAR(50) NULL,
  `iterator` DECIMAL(5, 0) NULL DEFAULT '1',
  `openningFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `phoneNumber` VARCHAR(255) NULL,
  `isTimedSchedule` DECIMAL(1, 0) NULL DEFAULT '1',
  `timeScope` DECIMAL(5, 0) NULL DEFAULT '0',
  `playOrder` DECIMAL(5, 0) NULL DEFAULT '0',
  PRIMARY KEY (`mgrId`, `requestId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_adtarget`;
CREATE TABLE `ubc`.`utv_adtarget` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `targetId` VARCHAR(255) NOT NULL DEFAULT '',
  `siteIdList` VARCHAR(255) NULL,
  `addr1List` VARCHAR(255) NULL,
  `addr2List` VARCHAR(255) NULL,
  `addr3List` VARCHAR(255) NULL,
  `franchizeType` VARCHAR(255) NULL,
  `hostIdList` VARCHAR(255) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  PRIMARY KEY (`mgrId`, `targetId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_contents`;
CREATE TABLE `ubc`.`utv_contents` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `contentsId` VARCHAR(255) NOT NULL DEFAULT '',
  `category` VARCHAR(255) NULL,
  `promotionId` VARCHAR(255) NULL,
  `contentsName` VARCHAR(255) NULL,
  `contentsType` DECIMAL(3, 0) NULL,
  `parentsId` VARCHAR(255) NULL,
  `registerId` VARCHAR(255) NULL,
  `registerTime` DATETIME NULL,
  `isRaw` DECIMAL(1, 0) NULL DEFAULT '0',
  `contentsState` DECIMAL(3, 0) NULL DEFAULT '0',
  `contentsStateTime` DATETIME NULL,
  `contentsStateHistory` VARCHAR(255) NULL,
  `location` VARCHAR(255) NULL,
  `filename` VARCHAR(255) NULL,
  `width` DECIMAL(5, 0) NULL,
  `height` DECIMAL(5, 0) NULL,
  `volume` DECIMAL(10, 0) NULL,
  `runningTime` DECIMAL(10, 0) NULL DEFAULT '10',
  `verifier` VARCHAR(255) NULL,
  `duration` DECIMAL(5, 0) NULL,
  `videoCodec` VARCHAR(255) NULL,
  `audioCodec` VARCHAR(255) NULL,
  `encodingInfo` VARCHAR(255) NULL,
  `encodingTime` DATETIME NULL,
  `siteId` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `comment1` VARCHAR(255) NULL,
  `comment2` VARCHAR(255) NULL,
  `comment3` VARCHAR(255) NULL,
  `comment4` VARCHAR(255) NULL,
  `comment5` VARCHAR(255) NULL,
  `comment6` VARCHAR(255) NULL,
  `comment7` VARCHAR(255) NULL,
  `comment8` VARCHAR(255) NULL,
  `comment9` VARCHAR(255) NULL,
  `comment10` VARCHAR(255) NULL,
  `currentComment` DECIMAL(5, 0) NULL DEFAULT '1',
  `bgColor` VARCHAR(255) NULL,
  `fgColor` VARCHAR(255) NULL,
  `font` VARCHAR(255) NULL,
  `fontSize` DECIMAL(5, 0) NULL,
  `playSpeed` DECIMAL(5, 0) NULL DEFAULT '100',
  `soundVolume` DECIMAL(5, 0) NULL DEFAULT '80',
  `promotionValueList` VARCHAR(1024) NULL,
  PRIMARY KEY (`mgrId`, `contentsId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_defaultschedule`;
CREATE TABLE `ubc`.`utv_defaultschedule` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `siteId` VARCHAR(255) NOT NULL DEFAULT '',
  `hostId` VARCHAR(255) NOT NULL DEFAULT '',
  `scheduleId` VARCHAR(255) NOT NULL DEFAULT '',
  `templateId` VARCHAR(255) NULL,
  `frameId` VARCHAR(255) NULL,
  `requestId` VARCHAR(255) NULL,
  `contentsId` VARCHAR(255) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `playOrder` DECIMAL(5, 0) NULL,
  `openningFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `timeScope` DECIMAL(5, 0) NULL DEFAULT '0',
  `priority` DECIMAL(5, 0) NULL DEFAULT '0',
  `castingState` DECIMAL(3, 0) NULL,
  `castingStateTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `castingStateHistory` VARCHAR(255) NULL,
  `result` VARCHAR(255) NULL,
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `phoneNumber` VARCHAR(255) NULL,
  `comment1` VARCHAR(255) NULL,
  `comment2` VARCHAR(255) NULL,
  `comment3` VARCHAR(255) NULL,
  `touchTime` TIMESTAMP NULL,
  `parentScheduleId` VARCHAR(255) NULL,
  PRIMARY KEY (`mgrId`, `siteId`, `hostId`, `scheduleId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_dlp`;
CREATE TABLE `ubc`.`utv_dlp` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `siteId` VARCHAR(255) NOT NULL DEFAULT '',
  `hostId` VARCHAR(255) NOT NULL DEFAULT '',
  `deviceId` VARCHAR(255) NOT NULL DEFAULT '',
  `vendor` VARCHAR(255) NULL,
  `model` VARCHAR(255) NULL,
  `serialNo` VARCHAR(255) NULL,
  `period` DECIMAL(5, 0) NULL DEFAULT '300',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `description` VARCHAR(255) NULL,
  `ipAddress` VARCHAR(255) NULL,
  `port` DECIMAL(5, 0) NULL DEFAULT '4352',
  `powerState` DECIMAL(5, 0) NULL DEFAULT '0',
  PRIMARY KEY (`mgrId`, `siteId`, `hostId`, `deviceId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_fault`;
CREATE TABLE `ubc`.`utv_fault` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `faultId` VARCHAR(255) NOT NULL DEFAULT '',
  `eventTime` DATETIME NULL,
  `source` VARCHAR(255) NULL,
  `severity` DECIMAL(3, 0) NULL,
  `probableCause` VARCHAR(255) NULL,
  `additionalText` VARCHAR(255) NULL,
  PRIMARY KEY (`mgrId`, `faultId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_faultlog`;
CREATE TABLE `ubc`.`utv_faultlog` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `faultId` VARCHAR(255) NOT NULL DEFAULT '',
  `eventTime` DATETIME NULL,
  `source` VARCHAR(255) NULL,
  `severity` DECIMAL(3, 0) NULL,
  `probableCause` VARCHAR(255) NULL,
  `additionalText` VARCHAR(255) NULL,
  PRIMARY KEY (`mgrId`, `faultId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_frame`;
CREATE TABLE `ubc`.`utv_frame` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `templateId` VARCHAR(255) NOT NULL DEFAULT '',
  `frameId` VARCHAR(255) NOT NULL DEFAULT '',
  `grade` DECIMAL(5, 0) NULL,
  `width` DECIMAL(5, 0) NULL,
  `height` DECIMAL(5, 0) NULL,
  `x` DECIMAL(5, 0) NULL,
  `y` DECIMAL(5, 0) NULL,
  `isPIP` DECIMAL(1, 0) NULL DEFAULT '0',
  `borderStyle` VARCHAR(50) NULL,
  `borderThickness` DECIMAL(5, 0) NULL DEFAULT '0',
  `borderColor` VARCHAR(50) NULL,
  `cornerRadius` DECIMAL(5, 0) NULL DEFAULT '0',
  `description` VARCHAR(255) NULL,
  `comment1` VARCHAR(255) NULL,
  `comment2` VARCHAR(255) NULL,
  `comment3` VARCHAR(255) NULL,
  `siteId` VARCHAR(255) NOT NULL DEFAULT 'NULL',
  PRIMARY KEY (`mgrId`, `templateId`, `frameId`, `siteId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_host`;
CREATE TABLE `ubc`.`utv_host` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `siteId` VARCHAR(255) NOT NULL DEFAULT '',
  `hostId` VARCHAR(255) NOT NULL DEFAULT '',
  `hostName` VARCHAR(255) NULL,
  `domainName` VARCHAR(255) NULL,
  `vendor` VARCHAR(255) NULL,
  `model` VARCHAR(255) NULL,
  `os` VARCHAR(255) NULL,
  `serialNo` VARCHAR(255) NULL,
  `period` DECIMAL(5, 0) NULL DEFAULT '5',
  `asOperator` VARCHAR(255) NULL,
  `asOperatorTel` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `soundVolume` DECIMAL(5, 0) NULL,
  `mute` DECIMAL(1, 0) NULL,
  `resolution` VARCHAR(50) NULL,
  `defaultTemplate` VARCHAR(50) NULL,
  `ipAddress` VARCHAR(255) NULL,
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `autoPowerFlag` DECIMAL(1, 0) NULL DEFAULT '0',
  `startUpTime` VARCHAR(255) NULL,
  `shutdownTime` VARCHAR(255) NULL,
  `powerControl` DECIMAL(5, 0) NULL DEFAULT '0',
  `amtPassword` VARCHAR(50) NULL,
  `macAddress` VARCHAR(50) NULL,
  `wolPort` DECIMAL(5, 0) NULL,
  `hubURL` VARCHAR(255) NULL,
  `hubVendor` VARCHAR(255) NULL,
  `hubInternalIp` VARCHAR(255) NULL,
  `lastUpdateTime` DATETIME NULL,
  `cpuUsed` DECIMAL(5, 2) NULL,
  `realMemoryUsed` DECIMAL(10, 0) NULL,
  `realMemoryTotal` DECIMAL(10, 0) NULL,
  `virtualMemoryUsed` DECIMAL(10, 0) NULL,
  `virtualMemoryTotal` DECIMAL(10, 0) NULL,
  `fileSystem` VARCHAR(1024) NULL,
  `vncPort` DECIMAL(10, 0) NULL,
  `zipCode` VARCHAR(255) NULL,
  `addr1` VARCHAR(255) NULL,
  `addr2` VARCHAR(255) NULL,
  `addr3` VARCHAR(255) NULL,
  `addr4` VARCHAR(255) NULL,
  `templatePlayList` VARCHAR(1024) NULL,
  `lastScreenshotFile` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`mgrId`, `siteId`, `hostId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_key`;
CREATE TABLE `ubc`.`utv_key` (
  `requestId` DECIMAL(18, 0) NOT NULL DEFAULT '0'
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_key_contentsid`;
CREATE TABLE `ubc`.`utv_key_contentsid` (
  `keyval` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`keyval`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_key_defaultscheduleid`;
CREATE TABLE `ubc`.`utv_key_defaultscheduleid` (
  `keyval` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`keyval`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_key_faultid`;
CREATE TABLE `ubc`.`utv_key_faultid` (
  `keyval` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`keyval`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_key_requestid`;
CREATE TABLE `ubc`.`utv_key_requestid` (
  `keyval` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`keyval`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_ktfphone`;
CREATE TABLE `ubc`.`utv_ktfphone` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `model` VARCHAR(255) NOT NULL DEFAULT '',
  `vendor` VARCHAR(255) NULL,
  `nickname` VARCHAR(255) NULL,
  `filename1` VARCHAR(255) NULL,
  `filename2` VARCHAR(255) NULL,
  `filename3` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  PRIMARY KEY (`mgrId`, `model`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_mobileevent`;
CREATE TABLE `ubc`.`utv_mobileevent` (
  `id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `UserMDN` CHAR(11) NULL,
  `RegDate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `EventID` INT(10) NULL,
  `isProcessed` DECIMAL(1, 0) NULL DEFAULT '0',
  `itemNo` INT(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_mobileinf`;
CREATE TABLE `ubc`.`utv_mobileinf` (
  `mobileInfId` DECIMAL(18, 0) NOT NULL,
  `serviceCode` VARCHAR(255) NOT NULL,
  `userInput` VARCHAR(1024) NULL,
  `phoneNo` VARCHAR(32) NULL,
  `eventTIme` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isProcessed` DECIMAL(1, 0) NULL DEFAULT '0',
  `result` DECIMAL(1, 0) NULL,
  `resultString` VARCHAR(255) NULL
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_process`;
CREATE TABLE `ubc`.`utv_process` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `siteId` VARCHAR(255) NOT NULL DEFAULT '',
  `hostId` VARCHAR(255) NOT NULL DEFAULT '',
  `processId` VARCHAR(255) NOT NULL DEFAULT '',
  `binaryName` VARCHAR(255) NULL,
  `argument` VARCHAR(255) NULL,
  `properties` VARCHAR(255) NULL,
  `debug` VARCHAR(255) NULL,
  `runBackground` DECIMAL(1, 0) NULL DEFAULT '1',
  `autoStartUp` DECIMAL(1, 0) NULL DEFAULT '1',
  `runTimeUpdate` DECIMAL(1, 0) NULL,
  `period` DECIMAL(5, 0) NULL DEFAULT '300',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `startUpTime` DATETIME NULL,
  `lastHeartbeatTime` DATETIME NULL,
  `description` VARCHAR(255) NULL,
  PRIMARY KEY (`mgrId`, `siteId`, `hostId`, `processId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_progressbar`;
CREATE TABLE `ubc`.`utv_progressbar` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `contentsId` VARCHAR(255) NOT NULL DEFAULT '',
  `progressBarId` VARCHAR(255) NOT NULL DEFAULT '',
  `itemName` VARCHAR(255) NULL,
  `x` DECIMAL(5, 0) NULL,
  `y` DECIMAL(5, 0) NULL,
  `width` DECIMAL(5, 0) NULL,
  `height` DECIMAL(5, 0) NULL,
  `fgColor` VARCHAR(32) NULL,
  `bgColor` VARCHAR(32) NULL,
  `borderColor` VARCHAR(32) NULL,
  `borderThickness` DECIMAL(5, 0) NULL,
  `valueVisible` DECIMAL(1, 0) NULL,
  `font` VARCHAR(50) NULL,
  `fontColor` VARCHAR(50) NULL,
  `fontSize` DECIMAL(5, 0) NULL,
  `minValue` DECIMAL(10, 0) NULL DEFAULT '0',
  `maxValue` DECIMAL(10, 0) NULL DEFAULT '0',
  `direction` DECIMAL(5, 0) NULL DEFAULT '0',
  `valueX` DECIMAL(5, 0) NULL,
  `valueY` DECIMAL(5, 0) NULL,
  `valueWidth` DECIMAL(5, 0) NULL,
  `valueHeight` DECIMAL(5, 0) NULL,
  PRIMARY KEY (`mgrId`, `contentsId`, `progressBarId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_schedule`;
CREATE TABLE `ubc`.`utv_schedule` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `siteId` VARCHAR(255) NOT NULL DEFAULT '',
  `hostId` VARCHAR(255) NOT NULL DEFAULT '',
  `scheduleId` INT(10) NOT NULL AUTO_INCREMENT,
  `templateId` VARCHAR(255) NULL,
  `frameId` VARCHAR(255) NOT NULL,
  `requestId` VARCHAR(255) NULL,
  `contentsId` VARCHAR(255) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `startTime` VARCHAR(50) NULL,
  `endTime` VARCHAR(50) NULL,
  `toTime` DATETIME NULL,
  `fromTime` DATETIME NULL,
  `openningFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `priority` DECIMAL(5, 0) NULL DEFAULT '0',
  `castingState` DECIMAL(3, 0) NULL DEFAULT '0',
  `castingStateTime` DATETIME NULL,
  `castingStateHistory` VARCHAR(255) NULL,
  `result` VARCHAR(255) NULL,
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `phoneNumber` VARCHAR(255) NULL,
  `isOrigin` DECIMAL(1, 0) NULL DEFAULT '1',
  `comment1` VARCHAR(255) NULL,
  `comment2` VARCHAR(255) NULL,
  `comment3` VARCHAR(255) NULL,
  PRIMARY KEY (`scheduleId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_site`;
CREATE TABLE `ubc`.`utv_site` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `siteId` VARCHAR(255) NOT NULL DEFAULT '',
  `siteName` VARCHAR(255) NULL,
  `phoneNo1` VARCHAR(255) NULL,
  `phoneNo2` VARCHAR(255) NULL,
  `mobileNo` VARCHAR(255) NULL,
  `faxNo` VARCHAR(255) NULL,
  `franchizeType` VARCHAR(255) NULL,
  `chainNo` VARCHAR(255) NULL,
  `businessType` VARCHAR(255) NULL,
  `businessCode` VARCHAR(255) NULL,
  `ceoName` VARCHAR(255) NULL,
  `siteLevel` DECIMAL(5, 0) NULL,
  `shopOpenTime` VARCHAR(255) NULL,
  `shopCloseTime` VARCHAR(255) NULL,
  `holyday` VARCHAR(255) NULL,
  `comment1` VARCHAR(255) NULL,
  `comment2` VARCHAR(255) NULL,
  `comment3` VARCHAR(255) NULL,
  `zipCode` VARCHAR(255) NULL,
  `addr1` VARCHAR(255) NULL,
  `addr2` VARCHAR(255) NULL,
  `addr3` VARCHAR(255) NULL,
  `addr4` VARCHAR(255) NULL,
  PRIMARY KEY (`mgrId`, `siteId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_template`;
CREATE TABLE `ubc`.`utv_template` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `templateId` VARCHAR(255) NOT NULL DEFAULT '',
  `width` DECIMAL(5, 0) NULL,
  `height` DECIMAL(5, 0) NULL,
  `bgColor` VARCHAR(255) NULL DEFAULT '#c0c0c0',
  `bgImage` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `siteId` VARCHAR(255) NOT NULL DEFAULT 'NULL',
  `shortCut` VARCHAR(45) NULL,
  PRIMARY KEY (`mgrId`, `templateId`, `siteId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_user`;
CREATE TABLE `ubc`.`utv_user` (
  `mgrId` VARCHAR(255) NOT NULL DEFAULT '',
  `userId` VARCHAR(255) NOT NULL DEFAULT '',
  `password` VARCHAR(50) NOT NULL DEFAULT '',
  `userName` VARCHAR(50) NULL,
  `sid` VARCHAR(50) NOT NULL DEFAULT '',
  `mobileNo` VARCHAR(50) NULL,
  `phoneNo` VARCHAR(50) NULL,
  `zipCode` VARCHAR(50) NULL,
  `addr1` VARCHAR(255) NULL,
  `addr2` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  `registerTime` DATETIME NULL,
  `userType` DECIMAL(5, 0) NULL DEFAULT '1',
  `siteList` TEXT NULL,
  `hostList` TEXT NULL,
  PRIMARY KEY (`mgrId`, `userId`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ubc`.`utv_zipcode`;
CREATE TABLE `ubc`.`utv_zipcode` (
  `ZIPCODE` VARCHAR(255) NOT NULL DEFAULT '',
  `SIDO` VARCHAR(255) NOT NULL DEFAULT '',
  `GUGUN` VARCHAR(255) NOT NULL DEFAULT '',
  `DONG` VARCHAR(255) NULL,
  `BUNJI` VARCHAR(255) NULL,
  `SEQ` DOUBLE NULL,
  PRIMARY KEY (`ZIPCODE`, `SIDO`, `GUGUN`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;



SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------------------------------------------------
-- EOF

