﻿DROP TABLE IF EXISTS `ubc`.`utv_key_sosid`;
CREATE TABLE  `ubc`.`utv_key_sosid` (
  `keyval` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`keyval`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

insert into utv_key_sosid values (0);

DELIMITER $$

DROP FUNCTION IF EXISTS `ubc`.`gen_sosId` $$
CREATE FUNCTION `ubc`.`gen_sosId` () RETURNS INT
BEGIN
  DECLARE  retval int;
  SELECT keyval INTO retval FROM utv_key_sosid;
  update utv_key_sosid set keyval=LAST_INSERT_ID(keyval+1);
  return retval;
END $$

DELIMITER ;

select gen_sosId();
