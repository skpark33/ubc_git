﻿-- ----------------------------------------------------------------------
-- MySQL Migration Toolkit
-- SQL Create Script
-- ----------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

CREATE DATABASE IF NOT EXISTS `ubc`
  CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ubc`;
-- -------------------------------------
-- Tables

DROP TABLE IF EXISTS `ubc`.`utv_dlp`;
CREATE TABLE  `ubc`.`utv_dlp` (
  `mgrId` VARCHAR(256) NOT NULL,
  `siteId` varchar(256) NOT NULL default '',
  `hostId` varchar(256) default NULL,
  `deviceId` varchar(256) NOT NULL default '',
  `vendor` varchar(256) default NULL,
  `model` varchar(256) default NULL,
  `serialNo` varchar(256) default NULL,
  `period` decimal(5,0) default '300',
  `adminState` decimal(1,0) default '1',
  `operationalState` decimal(1,0) default '1',
  `description` varchar(256) default NULL,
  `ipAddress` varchar(256) default NULL,
  `port` decimal(5,0) default '4352',
  `powerState` decimal(5,0) default '0',
  PRIMARY KEY (`mgrId`,`siteId`,`hostId`,`deviceId`)
) 
ENGINE=InnoDB;

DROP TABLE IF EXISTS `ubc`.`call_postbox`;
CREATE TABLE `ubc`.`call_postbox` (
  `CALLID` INT(10) NOT NULL AUTO_INCREMENT,
  `DIRECTIVE` VARCHAR(64) NULL,
  `ENTITY` VARCHAR(64) NULL,
  `VALUESTRING` VARCHAR(4096) NULL,
  `CALLER` VARCHAR(64) NULL,
  `CREATETIME` DATETIME NULL,
  `CALLTIME` DATETIME NULL,
  `CHECKFLAG` INT(1) NULL,
  `CALLFLAG` INT(1) NULL,
  `ERRORFLAG` INT(1) NULL,
  `REMARKS` VARCHAR(64) NULL,
  PRIMARY KEY (`CALLID`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`event_postbox`;
CREATE TABLE `ubc`.`event_postbox` (
  `EVENTID` INT(11) NOT NULL AUTO_INCREMENT,
  `DOMAIN` VARCHAR(64) NULL,
  `ENTITY` VARCHAR(64) NULL,
  `EVENTTYPE` VARCHAR(64) NULL,
  `EVENTVALUE` VARCHAR(1024) NULL,
  `CREATETIME` DATETIME NULL,
  `SENDTIME` DATETIME NULL,
  `CHECKFLAG` DECIMAL(1, 0) NULL,
  `SENDFLAG` DECIMAL(1, 0) NULL,
  `ERRORFLAG` DECIMAL(1, 0) NULL,
  `REMARKS` VARCHAR(64) NULL,
  PRIMARY KEY (`EVENTID`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`reply_postbox`;
CREATE TABLE `ubc`.`reply_postbox` (
  `CALLID` INT(10) NOT NULL,
  `REPLYID` INT(10) NOT NULL,
  `ENTITY` VARCHAR(64) NULL,
  `VALUESTRING` VARCHAR(4096) NULL,
  `CREATETIME` DATETIME NULL,
  `CHECKFLAG` INT(1) NULL,
  `ERRORFLAG` INT(1) NULL,
  `REMARKS` VARCHAR(64) NULL,
  INDEX `REPLY_POSTBOX_IDX` (`CALLID`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`ts_cronjob`;
CREATE TABLE `ubc`.`ts_cronjob` (
  `cronJobId` VARCHAR(100) NOT NULL DEFAULT '',
  `exeTime` VARCHAR(50) NULL,
  `exeType` VARCHAR(20) NULL,
  `entity` VARCHAR(200) NULL,
  `directive` VARCHAR(64) NULL,
  `attributes` VARCHAR(1000) NULL,
  `operationalStatus` DECIMAL(1, 0) NULL DEFAULT '1',
  PRIMARY KEY (`cronJobId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_adrequest`;
CREATE TABLE `ubc`.`utv_adrequest` (
  `mgrId` VARCHAR(256) NOT NULL,
  `requestId` VARCHAR(256) NOT NULL DEFAULT '',
  `userId` VARCHAR(256) NOT NULL DEFAULT 'system',
  `requestRoute` DECIMAL(3, 0) NULL,
  `smsFlag` DECIMAL(1, 0) NULL,
  `mobileNo` VARCHAR(256) NULL,
  `requestTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `requestState` DECIMAL(3, 0) NULL,
  `requestStateTime` DATETIME NULL,
  `requestStateHistory` VARCHAR(255) NULL,
  `deferedPay` DECIMAL(1, 0) NULL DEFAULT '0',
  `result` VARCHAR(256) NULL,
  `successRatio` DOUBLE NULL,
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `siteIdList` VARCHAR(255) NULL,
  `addr1List` VARCHAR(255) NULL,
  `addr2List` VARCHAR(255) NULL,
  `addr3List` VARCHAR(255) NULL,
  `franchizeType` VARCHAR(255) NULL,
  `hostIdList` VARCHAR(255) NULL,
  `priority` DECIMAL(5, 0) NULL DEFAULT '0',
  `templateId` VARCHAR(256) NULL,
  `frameId` VARCHAR(256) NULL,
  `contentsIdList` VARCHAR(1024) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `startTime` VARCHAR(50) NULL,
  `endTime` VARCHAR(50) NULL,
  `iterator` DECIMAL(5, 0) NULL DEFAULT '1',
  `openningFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `phoneNumber` VARCHAR(256) NULL,
  `isTimedSchedule` DECIMAL(1, 0) NULL DEFAULT '1',
  `timeScope` DECIMAL(5, 0) NULL DEFAULT '0',
  `playOrder` DECIMAL(5, 0) NULL DEFAULT '0',
  `parentScheduleId` VARCHAR(255) NULL,
  PRIMARY KEY (`mgrId`,`requestId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_adrequestlog`;
CREATE TABLE `ubc`.`utv_adrequestlog` (
  `mgrId` VARCHAR(256) NOT NULL,
  `requestId` VARCHAR(256) NOT NULL DEFAULT '',
  `userId` VARCHAR(256) NOT NULL DEFAULT 'system',
  `requestRoute` DECIMAL(3, 0) NULL,
  `smsFlag` DECIMAL(1, 0) NULL,
  `mobileNo` VARCHAR(256) NULL,
  `requestTime` DATETIME NULL,
  `requestState` DECIMAL(3, 0) NULL,
  `requestStateTime` DATETIME NULL,
  `requestStateHistory` VARCHAR(256) NULL,
  `deferedPay` DECIMAL(1, 0) NULL DEFAULT '0',
  `result` VARCHAR(256) NULL,
  `successRatio` DOUBLE NULL,
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `siteIdList` VARCHAR(256) NULL,
  `addr1List` VARCHAR(256) NULL,
  `franchizeType` VARCHAR(256) NULL,
  `hostIdList` VARCHAR(256) NULL,
  `priority` DECIMAL(5, 0) NULL DEFAULT '0',
  `templateId` VARCHAR(256) NULL,
  `frameId` VARCHAR(256) NULL,
  `contentsIdList` VARCHAR(1024) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `startTime` VARCHAR(50) NULL,
  `endTime` VARCHAR(50) NULL,
  `iterator` DECIMAL(5, 0) NULL DEFAULT '1',
  `openningFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `phoneNumber` VARCHAR(256) NULL,
  `isTimedSchedule` DECIMAL(1, 0) NULL DEFAULT '1',
  `timeScope` DECIMAL(5, 0) NULL DEFAULT '0',
  `playOrder` DECIMAL(5, 0) NULL DEFAULT '0',
  PRIMARY KEY (`mgrId`,`requestId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_contents`;
CREATE TABLE `ubc`.`utv_contents` (
  `mgrId` VARCHAR(256) NOT NULL,
  `contentsId` VARCHAR(256) NOT NULL DEFAULT '',
  `category` VARCHAR(256) NULL,
  `promotionId` VARCHAR(256) NULL,
  `contentsName` VARCHAR(256) NULL,
  `contentsType` DECIMAL(3, 0) NULL,
  `saleContentsId` VARCHAR(256) NULL,
  `registerId` VARCHAR(256) NULL,
  `registerTime` DATETIME NULL,
  `isRaw` DECIMAL(1, 0) NULL DEFAULT '1',
  `contentsState` DECIMAL(3, 0) NULL DEFAULT '0',
  `contentsStateTime` DATETIME NULL,
  `contentsStateHistory` VARCHAR(255) NULL,
  `location` VARCHAR(255) NULL,
  `filename` VARCHAR(256) NULL,
  `width` DECIMAL(5, 0) NULL,
  `height` DECIMAL(5, 0) NULL,
  `volume` DECIMAL(10, 0) NULL,
  `runningTime` DECIMAL(10, 0) NULL,
  `verifier` VARCHAR(256) NULL,
  `duration` DECIMAL(5, 0) NULL,
  `videoCodec` VARCHAR(256) NULL,
  `audioCodec` VARCHAR(256) NULL,
  `encodingInfo` VARCHAR(256) NULL,
  `encodingTime` DATETIME NULL,
  `playerInfo` VARCHAR(256) NULL,
  `description` VARCHAR(256) NULL,
  `comment1` VARCHAR(256) NULL,
  `comment2` VARCHAR(256) NULL,
  `comment3` VARCHAR(256) NULL,
  `comment4` VARCHAR(256) NULL,
  `comment5` VARCHAR(256) NULL,
  `comment6` VARCHAR(256) NULL,
  `comment7` VARCHAR(256) NULL,
  `comment8` VARCHAR(256) NULL,
  `comment9` VARCHAR(256) NULL,
  `comment10` VARCHAR(256) NULL,
  `currentComment` DECIMAL(5, 0) NULL DEFAULT '1',
  `bgColor` VARCHAR(256) NULL,
  `fgColor` VARCHAR(256) NULL,
  `font` VARCHAR(256) NULL,
  `fontSize` DECIMAL(5, 0) NULL,
  `playSpeed` DECIMAL(5, 0) NULL,
  `soundVolume` DECIMAL(5, 0) NULL,
  `promotionValueList` VARCHAR(1024) NULL,
  PRIMARY KEY (`mgrId`,`contentsId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_defaultschedule`;
CREATE TABLE `ubc`.`utv_defaultschedule` (
  `mgrId` VARCHAR(256) NOT NULL,
  `siteId` VARCHAR(256) NOT NULL DEFAULT '',
  `hostId` VARCHAR(256) NOT NULL DEFAULT '',
  `scheduleId` VARCHAR(256) NOT NULL DEFAULT '',
  `templateId` VARCHAR(256) NULL,
  `frameId` VARCHAR(256) NULL,
  `requestId` VARCHAR(256) NULL,
  `contentsId` VARCHAR(256) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `playOrder` DECIMAL(5, 0) NULL,
  `openningFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `timeScope` DECIMAL(5, 0) NULL DEFAULT '0',
  `priority` DECIMAL(5, 0) NULL DEFAULT '0',
  `castingState` DECIMAL(3, 0) NULL,
  `castingStateTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `castingStateHistory` VARCHAR(255) NULL,
  `result` VARCHAR(256) NULL,
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `phoneNumber` VARCHAR(256) NULL,
  `comment1` VARCHAR(256) NULL,
  `comment2` VARCHAR(256) NULL,
  `comment3` VARCHAR(256) NULL,
  `touchTime` TIMESTAMP NULL,
  `parentScheduleId` VARCHAR(256) NULL,
  PRIMARY KEY (`mgrId`,`siteId`, `hostId`, `scheduleId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_fault`;
CREATE TABLE `ubc`.`utv_fault` (
  `mgrId` VARCHAR(256) NOT NULL,
  `faultId` VARCHAR(256) NOT NULL DEFAULT '',
  `eventTime` DATETIME NULL,
  `source` VARCHAR(256) NULL,
  `severity` DECIMAL(3, 0) NULL,
  `probableCause` VARCHAR(256) NULL,
  `additionalText` VARCHAR(256) NULL,
  PRIMARY KEY (`mgrId`,`faultId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_frame`;
CREATE TABLE `ubc`.`utv_frame` (
  `mgrId` VARCHAR(256) NOT NULL,
  `templateId` VARCHAR(256) NOT NULL DEFAULT '',
  `frameId` VARCHAR(256) NOT NULL DEFAULT '',
  `grade` DECIMAL(5, 0) NULL,
  `width` DECIMAL(5, 0) NULL,
  `height` DECIMAL(5, 0) NULL,
  `x` DECIMAL(5, 0) NULL,
  `y` DECIMAL(5, 0) NULL,
  `isPIP` DECIMAL(1, 0) NULL DEFAULT '0',
  `borderStyle` VARCHAR(50) NULL,
  `borderThickness` DECIMAL(5, 0) NULL DEFAULT '0',
  `borderColor` VARCHAR(50) NULL,
  `cornerRadius` DECIMAL(5, 0) NULL DEFAULT '0',
  `description` VARCHAR(256) NULL,
  `comment1` VARCHAR(256) NULL,
  `comment2` VARCHAR(256) NULL,
  `comment3` VARCHAR(256) NULL,
  PRIMARY KEY (`mgrId`,`templateId`,`frameId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_host`;
CREATE TABLE `ubc`.`utv_host` (
  `mgrId` VARCHAR(256) NOT NULL,
  `siteId` VARCHAR(256) NOT NULL DEFAULT '',
  `hostId` VARCHAR(256) NOT NULL DEFAULT '',
  `hostName` VARCHAR(256) NULL,
  `domainName` VARCHAR(256) NULL,
  `vendor` VARCHAR(256) NULL,
  `model` VARCHAR(256) NULL,
  `os` VARCHAR(256) NULL,
  `serialNo` VARCHAR(256) NULL,
  `period` DECIMAL(5, 0) NULL,
  `asOperator` VARCHAR(256) NULL,
  `asOperatorTel` VARCHAR(256) NULL,
  `description` VARCHAR(256) NULL,
  `soundVolume` DECIMAL(5, 0) NULL,
  `mute` DECIMAL(1, 0) NULL,
  `resolution` VARCHAR(50) NULL,
  `defaultTemplate` VARCHAR(50) NULL,
  `ipAddress` VARCHAR(256) NULL,
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `autoPowerFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `startUpTime` VARCHAR(256) NULL,
  `shutdownTime` VARCHAR(256) NULL,
  `powerControl` DECIMAL(5, 0) NULL DEFAULT '1',
  `amtPassword` VARCHAR(50) NULL,
  `macAddress` VARCHAR(50) NULL,
  `wolPort` DECIMAL(5, 0) NULL,
  `hubURL` VARCHAR(256) NULL,
  `hubVendor` VARCHAR(256) NULL,
  `hubInternalIp` VARCHAR(256) NULL,
  `lastUpdateTime` DATETIME NULL,
  `cpuUsed` DECIMAL(5, 2) NULL,
  `realMemoryUsed` DECIMAL(10, 0) NULL,
  `realMemoryTotal` DECIMAL(10, 0) NULL,
  `virtualMemoryUsed` DECIMAL(10, 0) NULL,
  `virtualMemoryTotal` DECIMAL(10, 0) NULL,
  `fileSystem` VARCHAR(1024) NULL,
  PRIMARY KEY (`mgrId`,`siteId`,`hostId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_key`;
CREATE TABLE `ubc`.`utv_key` (
  `requestId` DECIMAL(18, 0) NOT NULL DEFAULT '0'
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_key_contentsid`;
CREATE TABLE `ubc`.`utv_key_contentsid` (
  `keyval` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`keyval`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_key_defaultscheduleid`;
CREATE TABLE `ubc`.`utv_key_defaultscheduleid` (
  `keyval` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`keyval`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_key_faultid`;
CREATE TABLE `ubc`.`utv_key_faultid` (
  `keyval` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`keyval`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_key_requestid`;
CREATE TABLE `ubc`.`utv_key_requestid` (
  `keyval` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`keyval`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_mobileevent`;
CREATE TABLE `ubc`.`utv_mobileevent` (
  `id` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `UserMDN` CHAR(11) NULL,
  `RegDate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `EventID` INT(10) NULL,
  `isProcessed` DECIMAL(1, 0) NULL DEFAULT '0',
  `itemNo` INT(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_mobileinf`;
CREATE TABLE `ubc`.`utv_mobileinf` (
  `mobileInfId` DECIMAL(18, 0) NOT NULL,
  `serviceCode` VARCHAR(256) NOT NULL,
  `userInput` VARCHAR(1024) NULL,
  `phoneNo` VARCHAR(32) NULL,
  `eventTIme` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isProcessed` DECIMAL(1, 0) NULL DEFAULT '0',
  `result` DECIMAL(1, 0) NULL,
  `resultString` VARCHAR(256) NULL
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_process`;
CREATE TABLE `ubc`.`utv_process` (
  `mgrId` VARCHAR(256) NOT NULL,
  `siteId` VARCHAR(256) NOT NULL DEFAULT '',
  `hostId` VARCHAR(256) NOT NULL DEFAULT '',
  `processId` VARCHAR(256) NOT NULL DEFAULT '',
  `binaryName` VARCHAR(256) NULL,
  `argument` VARCHAR(256) NULL,
  `properties` VARCHAR(256) NULL,
  `debug` VARCHAR(256) NULL,
  `runBackground` DECIMAL(1, 0) NULL DEFAULT '1',
  `autoStartUp` DECIMAL(1, 0) NULL DEFAULT '1',
  `runTimeUpdate` DECIMAL(1, 0) NULL,
  `period` DECIMAL(5, 0) NULL DEFAULT '300',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `startUpTime` DATETIME NULL,
  `lastHeartbeatTime` DATETIME NULL,
  `description` VARCHAR(256) NULL,
  PRIMARY KEY (`mgrId`,`siteId`,`hostId`,`processId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_progressbar`;
CREATE TABLE `ubc`.`utv_progressbar` (
  `mgrId` VARCHAR(256) NOT NULL,
  `contentsId` VARCHAR(256) NOT NULL DEFAULT '',
  `progressBarId` VARCHAR(256) NOT NULL DEFAULT '',
  `itemName` VARCHAR(256) NULL,
  `x` DECIMAL(5, 0) NULL,
  `y` DECIMAL(5, 0) NULL,
  `width` DECIMAL(5, 0) NULL,
  `height` DECIMAL(5, 0) NULL,
  `fgColor` VARCHAR(32) NULL,
  `bgColor` VARCHAR(32) NULL,
  `borderColor` VARCHAR(32) NULL,
  `borderThickness` DECIMAL(5, 0) NULL,
  `valueVisible` DECIMAL(1, 0) NULL,
  `font` VARCHAR(50) NULL,
  `fontColor` VARCHAR(50) NULL,
  `fontSize` DECIMAL(5, 0) NULL,
  `minValue` DECIMAL(10, 0) NULL DEFAULT '0',
  `maxValue` DECIMAL(10, 0) NULL DEFAULT '0',
  `direction` DECIMAL(5, 0) NULL DEFAULT '0',
  `valueX` DECIMAL(5, 0) NULL,
  `valueY` DECIMAL(5, 0) NULL,
  `valueWidth` DECIMAL(5, 0) NULL,
  `valueHeight` DECIMAL(5, 0) NULL,
  PRIMARY KEY (`mgrId`,`contentsId`,`progressBarId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_schedule`;
CREATE TABLE `ubc`.`utv_schedule` (
  `mgrId` VARCHAR(256) NOT NULL,
  `siteId` VARCHAR(256) NOT NULL DEFAULT '',
  `hostId` VARCHAR(256) NOT NULL DEFAULT '',
  `scheduleId` INT(10) NOT NULL AUTO_INCREMENT,
  `templateId` VARCHAR(256) NULL,
  `frameId` VARCHAR(256) NOT NULL,
  `requestId` VARCHAR(256) NULL,
  `contentsId` VARCHAR(256) NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `startTime` VARCHAR(50) NULL,
  `endTime` VARCHAR(50) NULL,
  `toTime` DATETIME NULL,
  `fromTime` DATETIME NULL,
  `openningFlag` DECIMAL(1, 0) NULL DEFAULT '1',
  `priority` DECIMAL(5, 0) NULL DEFAULT '0',
  `castingState` DECIMAL(3, 0) NULL DEFAULT '0',
  `castingStateTime` DATETIME NULL,
  `castingStateHistory` VARCHAR(255) NULL,
  `result` VARCHAR(256) NULL,
  `operationalState` DECIMAL(1, 0) NULL DEFAULT '1',
  `adminState` DECIMAL(1, 0) NULL DEFAULT '1',
  `phoneNumber` VARCHAR(256) NULL,
  `isOrigin` DECIMAL(1, 0) NULL DEFAULT '1',
  `comment1` VARCHAR(256) NULL,
  `comment2` VARCHAR(256) NULL,
  `comment3` VARCHAR(256) NULL,
  PRIMARY KEY (`scheduleId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_site`;
CREATE TABLE `ubc`.`utv_site` (
  `mgrId` VARCHAR(256) NOT NULL,
  `siteId` VARCHAR(256) NOT NULL DEFAULT '',
  `siteName` VARCHAR(256) NULL,
  `zipCode` VARCHAR(256) NULL,
  `addr1` VARCHAR(256) NULL,
  `addr2` VARCHAR(256) NULL,
  `addr3` VARCHAR(256) NULL,
  `addr4` VARCHAR(256) NULL,
  `phoneNo1` VARCHAR(256) NULL,
  `phoneNo2` VARCHAR(256) NULL,
  `mobileNo` VARCHAR(256) NULL,
  `faxNo` VARCHAR(256) NULL,
  `franchizeType` VARCHAR(256) NULL,
  `chainNo` VARCHAR(256) NULL,
  `businessType` VARCHAR(256) NULL,
  `businessCode` VARCHAR(256) NULL,
  `ceoName` VARCHAR(256) NULL,
  `siteLevel` DECIMAL(5, 0) NULL,
  `shopOpenTime` VARCHAR(256) NULL,
  `shopCloseTime` VARCHAR(256) NULL,
  `holyday` VARCHAR(256) NULL,
  `comment1` VARCHAR(256) NULL,
  `comment2` VARCHAR(256) NULL,
  `comment3` VARCHAR(256) NULL,
  PRIMARY KEY (`mgrId`,`siteId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_template`;
CREATE TABLE `ubc`.`utv_template` (
  `mgrId` VARCHAR(256) NOT NULL,
  `templateId` VARCHAR(256) NOT NULL DEFAULT '',
  `width` DECIMAL(5, 0) NULL,
  `height` DECIMAL(5, 0) NULL,
  `bgColor` VARCHAR(256) NULL DEFAULT '#c0c0c0',
  `bgImage` VARCHAR(256) NULL,
  `description` VARCHAR(256) NULL,
  PRIMARY KEY (`mgrId`,`templateId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_user`;
CREATE TABLE `ubc`.`utv_user` (
  `mgrId` VARCHAR(256) NOT NULL,
  `userId` VARCHAR(256) NOT NULL DEFAULT '',
  `password` VARCHAR(50) NOT NULL DEFAULT '',
  `userName` VARCHAR(50) NULL,
  `sid` VARCHAR(50) NOT NULL DEFAULT '',
  `mobileNo` VARCHAR(50) NULL,
  `phoneNo` VARCHAR(50) NULL,
  `zipCode` VARCHAR(50) NULL,
  `addr1` VARCHAR(256) NULL,
  `addr2` VARCHAR(256) NULL,
  `email` VARCHAR(256) NULL,
  `registerTime` DATETIME NULL,
  `userType` DECIMAL(5, 0) NULL DEFAULT '1',
  `workSite` VARCHAR(256) NULL,
  PRIMARY KEY (`mgrId`,`userId`)
)
ENGINE = INNODB;

DROP TABLE IF EXISTS `ubc`.`utv_zipcode`;
CREATE TABLE `ubc`.`utv_zipcode` (
  `ZIPCODE` VARCHAR(255) NULL,
  `SIDO` VARCHAR(255) NULL,
  `GUGUN` VARCHAR(255) NULL,
  `DONG` VARCHAR(255) NULL,
  `BUNJI` VARCHAR(255) NULL,
  `SEQ` DOUBLE NULL
)
ENGINE = INNODB;



-- -------------------------------------
-- Views

DROP VIEW IF EXISTS `ubc`.`utv_defaultscheduleview`;
CREATE ALGORITHM=UNDEFINED DEFINER=`ubc`@`211.232.57.184` SQL SECURITY DEFINER VIEW `utv_defaultscheduleview` AS select `utv_defaultschedule`.`mgrId` AS `mgrId`, `utv_defaultschedule`.`siteId` AS `siteId`,`utv_defaultschedule`.`hostId` AS `hostId`,`utv_defaultschedule`.`scheduleId` AS `scheduleId`,`utv_defaultschedule`.`templateId` AS `templateId`,`utv_defaultschedule`.`frameId` AS `frameId`,`utv_defaultschedule`.`requestId` AS `requestId`,`utv_defaultschedule`.`contentsId` AS `contentsId`,`utv_defaultschedule`.`startDate` AS `startDate`,`utv_defaultschedule`.`endDate` AS `endDate`,`utv_defaultschedule`.`playOrder` AS `playOrder`,`utv_defaultschedule`.`openningFlag` AS `openningFlag`,`utv_defaultschedule`.`timeScope` AS `timeScope`,`utv_defaultschedule`.`priority` AS `priority`,`utv_defaultschedule`.`castingState` AS `castingState`,`utv_defaultschedule`.`castingStateTime` AS `castingStateTime`,`utv_defaultschedule`.`castingStateHistory` AS `castingStateHistory`,`utv_defaultschedule`.`result` AS `result`,`utv_defaultschedule`.`operationalState` AS `operationalState`,`utv_defaultschedule`.`adminState` AS `adminState`,`utv_defaultschedule`.`parentScheduleId` AS `parentScheduleId`,`utv_contents`.`location` AS `location`,`utv_contents`.`filename` AS `filename`,`utv_defaultschedule`.`phoneNumber` AS `phoneNumber`,`utv_contents`.`contentsType` AS `contentsType`,`utv_contents`.`contentsState` AS `contentsState`,`utv_contents`.`volume` AS `volume`,`utv_contents`.`runningTime` AS `runningTime`,`utv_contents`.`bgColor` AS `bgColor`,`utv_contents`.`fgColor` AS `fgColor`,`utv_contents`.`font` AS `font`,`utv_contents`.`fontSize` AS `fontSize`,`utv_contents`.`playSpeed` AS `playSpeed`,`utv_contents`.`soundVolume` AS `soundVolume`,`utv_contents`.`comment1` AS `comment1`,`utv_contents`.`comment2` AS `comment2`,`utv_contents`.`comment3` AS `comment3`,`utv_contents`.`promotionValueList` AS `promotionValueList`,`utv_contents`.`comment4` AS `comment4`,`utv_contents`.`comment5` AS `comment5`,`utv_contents`.`comment6` AS `comment6`,`utv_contents`.`comment7` AS `comment7`,`utv_contents`.`comment8` AS `comment8`,`utv_contents`.`comment9` AS `comment9`,`utv_contents`.`comment10` AS `comment10`,`utv_contents`.`contentsName` AS `contentsName`  from (`utv_defaultschedule` join `utv_contents` on((`utv_defaultschedule`.`contentsId` = `utv_contents`.`contentsId`)));

DROP VIEW IF EXISTS `ubc`.`utv_scheduleview`;
CREATE ALGORITHM=UNDEFINED DEFINER=`ubc`@`211.232.57.184` SQL SECURITY DEFINER VIEW `utv_scheduleview` AS select `utv_schedule`.`mgrId` AS `mgrId`,`utv_schedule`.`siteId` AS `siteId`,`utv_schedule`.`hostId` AS `hostId`,`utv_schedule`.`scheduleId` AS `scheduleId`,`utv_schedule`.`templateId` AS `templateId`,`utv_schedule`.`frameId` AS `frameId`,`utv_schedule`.`requestId` AS `requestId`,`utv_schedule`.`contentsId` AS `contentsId`,`utv_schedule`.`startDate` AS `startDate`,`utv_schedule`.`endDate` AS `endDate`,`utv_schedule`.`startTime` AS `startTime`,`utv_schedule`.`endTime` AS `endTime`,`utv_schedule`.`toTime` AS `toTime`,`utv_schedule`.`fromTime` AS `fromTime`,`utv_schedule`.`openningFlag` AS `openningFlag`,`utv_schedule`.`priority` AS `priority`,`utv_schedule`.`castingState` AS `castingState`,`utv_schedule`.`castingStateTime` AS `castingStateTime`,`utv_schedule`.`castingStateHistory` AS `castingStateHistory`,`utv_schedule`.`result` AS `result`,`utv_schedule`.`operationalState` AS `operationalState`,`utv_schedule`.`adminState` AS `adminState`,`utv_contents`.`location` AS `location`,`utv_contents`.`filename` AS `filename`,`utv_schedule`.`phoneNumber` AS `phoneNumber`,`utv_schedule`.`isOrigin` AS `isOrigin`,`utv_contents`.`contentsType` AS `contentsType`,`utv_contents`.`contentsState` AS `contentsState`,`utv_contents`.`volume` AS `volume`,`utv_contents`.`runningTime` AS `runningTime`,`utv_contents`.`comment1` AS `comment1`,`utv_contents`.`comment2` AS `comment2`,`utv_contents`.`comment3` AS `comment3`,`utv_contents`.`bgColor` AS `bgColor`,`utv_contents`.`fgColor` AS `fgColor`,`utv_contents`.`font` AS `font`,`utv_contents`.`fontSize` AS `fontSize`,`utv_contents`.`playSpeed` AS `playSpeed`,`utv_contents`.`soundVolume` AS `soundVolume`,`utv_contents`.`promotionValueList` AS `promotionValueList`,`utv_contents`.`comment4` AS `comment4`,`utv_contents`.`comment6` AS `comment6`,`utv_contents`.`comment5` AS `comment5`,`utv_contents`.`comment7` AS `comment7`,`utv_contents`.`comment8` AS `comment8`,`utv_contents`.`comment9` AS `comment9`,`utv_contents`.`comment10` AS `comment10`,`utv_contents`.`contentsName` AS `contentsName`   from (`utv_schedule` join `utv_contents` on((`utv_schedule`.`contentsId` = `utv_contents`.`contentsId`)));



SET FOREIGN_KEY_CHECKS = 1;


insert into utv_KEY_requestId set keyVal = (select ifnull(max(requestId),0)+1 from utv_ADRequest);
insert into utv_KEY_contentsId set keyVal = (select ifnull(max(contentsId),0)+1 from utv_Contents);
insert into utv_KEY_faultId set keyVal = (select ifnull(max(faultId),0)+1 from utv_Fault);
insert into utv_KEY_defaultScheduleId set keyVal = (select ifnull(max(scheduleId),0)+1 from utv_DefaultSchedule);

-- ----------------------------------------------------------------------
-- EOF
