﻿DROP VIEW IF EXISTS utv_requestview;
CREATE VIEW utv_requestview AS select
utv_host.siteId AS siteId,
utv_host.hostId AS hostId,
utv_pot.startDate AS startDate,
utv_pot.endDate AS endDate,
utv_pot.startTime AS startTime,
utv_pot.endTime AS endTime,
utv_pot.potType AS potType,
utv_pot.weekday AS weekday,
utv_pot.dday AS dday,
utv_request.mgrId AS mgrId,
utv_request.sosId AS sosId,
utv_request.potId AS potId,
utv_request.requestId AS scheduleId,
utv_request.stageId AS stageId,
utv_request.templateId AS templateId,
utv_request.frameId AS frameId,
utv_request.contentsId AS contentsId,
utv_request.playOrder AS playOrder,
utv_request.requestState AS castingState,
utv_request.adminState AS adminState,
utv_request.parentsId AS parentScheduleId,
utv_request.runningTime AS runningTime,
utv_contents.contentsName AS contentsName,
utv_contents.location AS location,
utv_contents.filename AS filename,
utv_contents.contentsType AS contentsType,
utv_contents.contentsState AS contentsState,
utv_contents.volume AS volume,
utv_contents.bgColor AS bgColor,
utv_contents.fgColor AS fgColor,
utv_contents.font AS font,
utv_contents.fontSize AS fontSize,
utv_contents.playSpeed AS playSpeed,
utv_contents.soundVolume AS soundVolume,
utv_contents.comment1 AS comment1,
utv_contents.comment2 AS comment2,
utv_contents.comment3 AS comment3,
utv_contents.comment4 AS comment4,
utv_contents.comment5 AS comment5,
utv_contents.comment6 AS comment6,
utv_contents.comment7 AS comment7,
utv_contents.comment8 AS comment8,
utv_contents.comment9 AS comment9,
utv_contents.comment10 AS comment10,
utv_contents.promotionValueList AS promotionValueList
from utv_request, utv_host, utv_pot, utv_contents
where  utv_request.sosId = utv_host.sosId
and utv_request.sosId = utv_pot.sosId
and utv_request.potId = utv_pot.potId 
and utv_request.contentsId = utv_contents.contentsId;


DROP VIEW IF EXISTS utv_circleScheduleView;
CREATE VIEW utv_circleScheduleView AS select * from utv_requestview where potType <> 1;

DROP VIEW IF EXISTS utv_ontimeScheduleView;
CREATE VIEW utv_ontimeScheduleView AS select * from utv_requestview where potType = 1;
