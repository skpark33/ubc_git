###
### USAGE
###
### shell> mysql -u utv -p utv < MGR.mysql.sql


drop table UTV_SITE;
create table UTV_SITE (
	siteId			VARCHAR(255) NOT NULL,
	siteName		VARCHAR(255),
	zipCode			VARCHAR(255),
	addr1			VARCHAR(255),
	addr2			VARCHAR(255),
	phoneNo1		VARCHAR(255),
	phoneNo2		VARCHAR(255),
	mobileNo		VARCHAR(255),
	faxNo			VARCHAR(255),
	franchizeType		VARCHAR(255),
	chainNo			VARCHAR(255),
	businessType		VARCHAR(255),
	businessCode		VARCHAR(255),
	ceoName			VARCHAR(255),
	siteLevel		TINYINT,
	shopOpenTime		VARCHAR(255),
	shopCloseTime		VARCHAR(255),
	onAirTime		VARCHAR(255),
	offAirTime		VARCHAR(255),
	holyday			BLOB,
	comment1		VARCHAR(255),
	comment2		VARCHAR(255),
	comment3		VARCHAR(255),
	PRIMARY KEY  (siteId)
);
###show columns in UTV_SITE;

drop table UTV_HOST;
create table UTV_HOST (
	siteId			VARCHAR(255) NOT NULL,
	hostId			VARCHAR(255) NOT NULL,
	hostName		VARCHAR(255),
	domainName		VARCHAR(255),
	vendor			VARCHAR(255),
	model			VARCHAR(255),
	os			VARCHAR(255),
	serialNo		VARCHAR(255),
	period			TINYINT,
	wolFlag			TINYINT	DEFAULT 1,
	shutdownTime		VARCHAR(255),
	startUpTime		VARCHAR(255),
	asOperator		VARCHAR(255),
	asOperatorTel		VARCHAR(255),
	description		VARCHAR(255),
	volume			TINYINT,
	adminState		TINYINT DEFAULT 1,
	operationalState	TINYINT DEFAULT 1,
	ipAddress		VARCHAR(255),
	lastUpdateTime		DATETIME,
	cpuUsed			FLOAT,
	realMemoryUsed		INT,
	realMemoryTotal		INT,
	virtualMemoryUsed	INT,
	virtualMemoryTotal	INT,
	fileSystem		BLOB,
	PRIMARY KEY  (siteId, hostId)
);
###show columns in UTV_HOST;

drop table UTV_PROCESS;
create table UTV_PROCESS  (
	siteId			VARCHAR(255) NOT NULL,
	hostId			VARCHAR(255) NOT NULL,
	processId		VARCHAR(255) NOT NULL,
	binaryName		VARCHAR(255),
	path			VARCHAR(255),
	argument		VARCHAR(255),
	startUpTime		DATETIME,
	lastHeartbeatTime	DATETIME,
	period			TINYINT,
	adminState		TINYINT DEFAULT 1,
	operationalState	TINYINT DEFAULT 1,
	descrition		VARCHAR(255),
	processState		TINYINT,
	restartAfterUpdate	TINYINT,
	PRIMARY KEY  (siteId,hostId,processId)
);
###show columns in UTV_PROCESS;


drop table UTV_BEAMPROJECTOR;
create table  UTV_BEAMPROJECTOR (
	siteId			VARCHAR(255) NOT NULL,
	deviceId		VARCHAR(255) NOT NULL,
	vendor			VARCHAR(255),
	model			VARCHAR(255),
	serialNo		VARCHAR(255),
	period			TINYINT,
	adminState		TINYINT DEFAULT 1,
	operationalState	TINYINT DEFAULT 1,
	description		VARCHAR(255),
	asOperator		VARCHAR(255),
	asOperatorTel		VARCHAR(255),
	PRIMARY KEY  (siteId,deviceId)
);
###show columns in UTV_BEAMPROJECTOR;

drop table UTV_DISPLAYDEVICE;
create table  UTV_DISPLAYDEVICE (
	siteId			VARCHAR(255) NOT NULL,
	deviceId		VARCHAR(255) NOT NULL,
	displayType		TINYINT,
	vendor			VARCHAR(255),
	model			VARCHAR(255),
	serialNo		VARCHAR(255),
	period			TINYINT,
	inch			TINYINT,
	width			TINYINT,
	height			TINYINT,
	adminState		TINYINT DEFAULT 1,
	operationalState	TINYINT DEFAULT 1,
	description		VARCHAR(255),
	asOperator		VARCHAR(255),
	asOperatorTel		VARCHAR(255),
	PRIMARY KEY  (siteId,deviceId)
);
###show columns in UTV_DISPLAYDEVICE;

drop table UTV_SPEAKER;
create table  UTV_SPEAKER (
	siteId			VARCHAR(255) NOT NULL,
	deviceId		VARCHAR(255) NOT NULL,
	vendor			VARCHAR(255),
	model			VARCHAR(255),
	serialNo		VARCHAR(255),
	period			TINYINT,
	adminState		TINYINT DEFAULT 1,
	operationalState	TINYINT DEFAULT 1,
	description		VARCHAR(255),
	asOperator		VARCHAR(255),
	asOperatorTel		VARCHAR(255),
	PRIMARY KEY  (siteId,deviceId)
);
###show columns in UTV_SPEAKER;

drop table UTV_TVCARD;
create table  UTV_TVCARD (
	siteId			VARCHAR(255) NOT NULL,
	deviceId		VARCHAR(255) NOT NULL,
	vendor			VARCHAR(255),
	model			VARCHAR(255),
	serialNo		VARCHAR(255),
	period			TINYINT,
	adminState		TINYINT DEFAULT 1,
	operationalState	TINYINT DEFAULT 1,
	description		VARCHAR(255),
	asOperator		VARCHAR(255),
	asOperatorTel		VARCHAR(255),
	channel			TINYINT,
	PRIMARY KEY  (siteId,deviceId)
);
###show columns in UTV_TVCARD;

drop table UTV_SCHEDULE;
create table UTV_SCHEDULE (
	siteId			VARCHAR(255) NOT NULL,
	hostId			VARCHAR(255) NOT NULL,
	scheduleId		VARCHAR(255) NOT NULL,
	templateId		VARCHAR(255),
	frameId			VARCHAR(255),
	requestId		VARCHAR(255),
	contentsId		VARCHAR(255),
	startDate		DATETIME,
	endDate			DATETIME,
	startTime		VARCHAR(255),
	endTime			VARCHAR(255),
	fromTime		DATETIME,
	toTime			DATETIME,
	openningFlag		TINYINT DEFAULT 1,
	priority		TINYINT,
	castingState		TINYINT,
	castingStateTime	DATETIME,
	castingStateHistory	BLOB,
	result			VARCHAR(255),
	operationalState	TINYINT DEFAULT 1,
	adminState		TINYINT DEFAULT 1,
	comment1		VARCHAR(255),
	comment2		VARCHAR(255),
	comment3		VARCHAR(255),
	PRIMARY KEY  (siteId,hostId,scheduleId)
);
###show columns in UTV_SCHEDULE;

drop table UTV_ADREQUEST;
create table UTV_ADREQUEST (
	requestId		VARCHAR(255) NOT NULL,
	userId			VARCHAR(255),
	requestRoute		TINYINT,
	smsFlag			TINYINT,
	mobileNo		VARCHAR(255),
	requestTime		DATETIME,
	requestState		TINYINT,
	requestStateTime	DATETIME,
	requestStateHistory	BLOB,
	deferedpay		TINYINT,
	result			VARCHAR(255),
	successRatio		FLOAT,
	operationalState	TINYINT DEFAULT 1,
	adminState		TINYINT DEFAULT 1,
	siteIdList		BLOB,
	addr1List		BLOB,
	franchizeType		BLOB,
	hostIdList		BLOB,
	priority		TINYINT,
	templateId		VARCHAR(255),
	frameId			VARCHAR(255),
	contentsId		VARCHAR(255),
	startDate		DATETIME,
	endDate			DATETIME,
	startDATETIME		VARCHAR(255),
	endDATETIME		VARCHAR(255),
	iterator		TINYINT DEFAULT 1,
	openningFlag		TINYINT DEFAULT 1,
	PRIMARY KEY  (requestId)
);
###show columns in UTV_ADREQUEST;

drop table UTV_CONTENTS;
create table UTV_CONTENTS (
	contentsId		VARCHAR(255) NOT NULL,
	parentContentsId	VARCHAR(255),
	requestId		VARCHAR(255),
	contentsName		VARCHAR(255),
	contentsType		TINYINT,
	saleContentsId		VARCHAR(255),
	registerId		VARCHAR(255),
	registerTime		DATETIME,
	isRaw			TINYINT DEFAULT  1,
	contentsState		TINYINT,
	contentsStateTime	DATETIME,
	contentsStateHistory	BLOB,
	location		VARCHAR(255),
	filename		VARCHAR(255),
	width			TINYINT,
	height			TINYINT,
	volume			TINYINT,
	runningTime		INT,
	verifier		VARCHAR(255),
	duration		TINYINT,
	videoCodec		VARCHAR(255),
	audioCodec		VARCHAR(255),
	encodingInfo		VARCHAR(255),
	encodingTime		DATETIME,
	playerInfo		VARCHAR(255),
	description		VARCHAR(255),
	comment1		VARCHAR(255),
	comment2		VARCHAR(255),
	comment3		VARCHAR(255),
	bgColor			VARCHAR(255),
	fgColor			VARCHAR(255),
	font			VARCHAR(255),
	fontSize		TINYINT,
	playSpeed		TINYINT,
	soundVolume		TINYINT,
	PRIMARY KEY  (contentsId)
);
###show columns in UTV_CONTENTS;

drop table UTV_TEMPLATE;
create table UTV_TEMPLATE (
	templateId		VARCHAR(255) NOT NULL,
	width			TINYINT,
	height			TINYINT,
	descrition		VARCHAR(255),
	PRIMARY KEY  (templateId)
);
###show columns in UTV_TEMPLATE;

drop table UTV_FRAME;
create table UTV_FRAME (
	templateId		VARCHAR(255) NOT NULL,
	frameId			VARCHAR(255) NOT NULL,
	grade			TINYINT,
	width			TINYINT,
	height			TINYINT,
	x			TINYINT,
	y			TINYINT,
	descrition		VARCHAR(255),
	comment1		VARCHAR(255),
	comment2		VARCHAR(255),
	comment3		VARCHAR(255),
	PRIMARY KEY  (templateId, frameId)
);
###show columns in UTV_FRAME;

drop table UTV_FAULT;
create table UTV_FAULT (
	faultId			VARCHAR(255) NOT NULL,
	requestId		VARCHAR(255),
	siteId			VARCHAR(255),
	hostId			VARCHAR(255),
	scheduleId		VARCHAR(255),
	templateId		VARCHAR(255),
	frameId			VARCHAR(255),
	contentsId		VARCHAR(255),
	Entity			VARCHAR(255),
	startTime		DATETIME,
	endTime			DATETIME,
	probableCause		VARCHAR(255),
	serverity		TINYINT,
	additionalText		VARCHAR(255),
	PRIMARY KEY  (faultId)
);
###show columns in UTV_FAULT;
