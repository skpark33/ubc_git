DROP TABLE IF EXISTS `ubc`.`utv_key_fileid`;
CREATE TABLE  `ubc`.`utv_key_fileid` (
  `keyval` int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (`keyval`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;