--4.62
CREATE TABLE dbo.ubc_customer
(
	mgrId varchar(50) NOT NULL,
	customerId varchar(255) NOT NULL,
	url text NULL,
	description varchar(255) NULL,
	PRIMARY KEY  (customerId)
);

ALTER TABLE dbo.ubc_host  ADD  monitorState int NOT NULL DEFAULT -1;
ALTER TABLE dbo.ubc_host  ADD  winPassword varchar(50) ;

-- 4.65
ALTER TABLE dbo.ubc_host  ADD  monitorUseTime int NOT NULL DEFAULT -1;
ALTER TABLE dbo.ubc_host  ADD  downloadGroup varchar(255) ;
ALTER TABLE dbo.ubc_host  ADD  gmt int NOT NULL DEFAULT 9999;

--4.70
UPDATE dbo.ubc_host SET gmt=9999 WHERE gmt=99;
ALTER TABLE dbo.ubc_customer  ADD  gmt int NOT NULL DEFAULT 9999;
ALTER TABLE dbo.ubc_customer  ADD  language varchar(50) ;

--4.80
ALTER TABLE dbo.ubc_host  ADD  weekShutdownTime varchar(100) ;


--4.86

CREATE TABLE dbo.ubc_event
	(
	eventType varchar(255) NOT NULL,
	eventIndex bigint NOT NULL identity (1,1),
	domain varchar(255) NULL,
	entity varchar(255) NULL,
	eventTime datetime NOT NULL,
	eventBody varchar(MAX) NULL
	)  ON [PRIMARY];
ALTER TABLE dbo.ubc_event ADD CONSTRAINT
	PK_ubc_event PRIMARY KEY CLUSTERED 
	(
	eventType,
	eventIndex
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

ALTER TABLE dbo.ubc_event SET (LOCK_ESCALATION = TABLE);

--4.93

ALTER TABLE dbo.ubc_host  ADD  hostMsg1 varchar(255) ;
ALTER TABLE dbo.ubc_host  ADD  hostMsg2 varchar(255) ;
CREATE TABLE dbo.ubc_bpmon
(
	mgrId varchar(255) not null,
	bpMonId varchar(255) not null,
	bpId varchar(255) not null,
	programId varchar(255) not null,
	reportTime datetime,
	phone varchar(MAX),
	email varchar(MAX),
	reportWay tinyint,
	downloadFailLimit tinyint,
	testHostId varchar(255),
	testStartTime datetime,
	testEndTime datetime  
);

--4.94

ALTER TABLE dbo.ubc_interactivelog ADD
	keyword4 varchar(255) NULL,
	keyword5 varchar(255) NULL;

ALTER TABLE dbo.ubc_interactivelog SET (LOCK_ESCALATION = TABLE);
ALTER TABLE dbo.ubc_customer  ADD  copyUrl varchar(MAX) ;

--5.00

ALTER TABLE dbo.ubc_host  ADD  disk1Avail decimal(10,2) not null default -1;
ALTER TABLE dbo.ubc_host  ADD  disk2Avail decimal(10,2) not null default -1;
ALTER TABLE dbo.ubc_program  ADD  volume decimal(18,0) not null default 0;

--program 의 volume 을 구해넣기 위해
DROP VIEW dbo.ubc_temp_programView;
CREATE VIEW dbo.ubc_temp_programView AS SELECT programId, SUM(volume) AS total_volume FROM ubc_contents GROUP BY programId;
UPDATE dbo.ubc_program 
SET volume = c.total_volume 
FROM dbo.ubc_program AS p JOIN dbo.ubc_temp_programView AS c ON p.programId = c.programId
WHERE p.volume = 0;
DROP VIEW dbo.ubc_temp_programView;

-- MIR DB
-- DROP TABLE dbo.ubc_mir;
CREATE TABLE dbo.ubc_mir
	(
	classScopedName varchar(255) NOT NULL,
	attrName varchar(255) NOT NULL,
	attrType varchar(255) NOT NULL,
	className varchar(255),
	indexOrder int NOT NULL DEFAULT -1
	)  ON [PRIMARY];
ALTER TABLE dbo.ubc_mir ADD CONSTRAINT
	PK_ubc_mir PRIMARY KEY CLUSTERED 
	(
	classScopedName,
	attrName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
ALTER TABLE dbo.ubc_mir SET (LOCK_ESCALATION = TABLE);

--5.04
--- 유효기간이 잘못들어간 놈들을 바로 잡음
update ubc_program set validationDate = CAST('2037-12-31 23:59:59' as datetime) where validationDate = CAST('1970-1-1 09:00:00' as datetime) OR validationDate = CAST('9999-12-31 23:59:59' as datetime);

CREATE TABLE dbo.ubc_eventlog
(
	eventType varchar(255) NOT NULL,
	eventKey varchar(255) NULL,
	eventTime datetime NOT NULL,
	eventBody varchar(MAX) NULL
) ;

-- beartbeat log 를 남기기위한 꽁수
insert into ubc_programLocation values ('OD','heartbeat','boolean','time');

-- DROP TABLE dbo.ubc_agtserverlog;
CREATE TABLE dbo.ubc_agtserverlog
(
	mgrId varchar(255) NOT NULL,
	agtserverlogId varchar(255) NULL,
	actionKind varchar(255) NULL,
	actionDirective varchar(255) NULL,
	actionTime datetime NOT NULL,
	actionObject varchar(255) NULL,
	actionComment varchar(255) NULL,
	additionalInfo varchar(255) NULL
) ;
--5.10
update ubc_lmo_solution set timeFieldName = 'playDate' where lmId='PlayLog';
ALTER TABLE dbo.ubc_host  ADD  protocolType varchar(255) not null default 'C';

--5.20
--PROCEDURE BEGIN
-- =============================================
-- Author:		gwangsoo
-- Create date: 2012-06-20
-- Description:	입력받은 문자열과 같은 부분이후의 문자열을 리턴
-- =============================================
CREATE FUNCTION [dbo].[Find_n_Right] 
(
	@findStr varchar(255), @fullStr varchar(255)
)
RETURNS varchar(255)
WITH EXEC AS CALLER
AS
BEGIN
declare @resultStr varchar(255)
select @resultStr = RIGHT(@fullStr, LEN(@fullStr) - (CHARINDEX(@findStr, @fullStr)+LEN(@findStr)-1))
return @resultStr
END;
--PROCEDURE END

--5.22

--5.23
CREATE TABLE dbo.ubc_host_log(
	logTime datetime,
	mgrId varchar(255) ,
	siteId varchar(255),
	siteName varchar(255),
	hostId varchar(255),
	hostName varchar(255),
	description varchar(255),
	category varchar(255),
	ipAddress varchar(255),
	hostType smallint,
	adminState decimal(1, 0),
	lastUpdateTime datetime,
	bootUpTime datetime ,
	bootDownTime datetime,
	shutdownTime varchar(255),
	weekshutdownTime varchar(255),
	holiday varchar(255)
);

--5.40
CREATE TABLE  dbo.ubc_actionlog (
  logDate varchar(50) NOT NULL,
  logTime varchar(50) NOT NULL,
  logId varchar(50) NOT NULL,
  logIp varchar(50) DEFAULT NULL,
  result varchar(50),
  windowName varchar(256) DEFAULT NULL,
  actionType varchar(256) DEFAULT NULL,
  fileName varchar(256) DEFAULT NULL,
  fileSzie varchar(256) DEFAULT NULL
);

CREATE TABLE  dbo.ubc_loginlog (
  logDate varchar(50) NOT NULL,
  logTime varchar(50) NOT NULL,
  logId varchar(50) NOT NULL,
  logIp varchar(50) DEFAULT NULL,
  result varchar(50)
);

CREATE TABLE  dbo.ubc_authchangelog (
  logDate varchar(50) NOT NULL,
  logTime varchar(50) NOT NULL,
  logId varchar(50) NOT NULL,
  changeType varchar(50) DEFAULT NULL
);
alter table ubc_authchangelog add changeString varchar(255);

ALTER TABLE ubc_user ADD lastLoginTime DATETIME;
ALTER TABLE ubc_user ADD lastPWChangeTime DATETIME;
ALTER TABLE ubc_user ADD loginFailCount TINYINT DEFAULT 0 ;
ALTER TABLE ubc_user ADD adminstate TINYINT DEFAULT 1 ;

ALTER TABLE ubc_interactivelog ADD watchSec bigint;

CREATE TABLE  ubc_connectionDailyStat (
  statDate datetime NOT NULL,
  siteId varchar(255) NOT NULL,
  hostId varchar(255) NOT NULL,
  disconnectCount bigInt NULL,
  disconnectSec  bigInt NULL
);

ALTER TABLE ubc_playlog ADD mgrId varchar(50);
ALTER TABLE ubc_playlog ADD playLogId varchar(50);

UPDATE  ubc_lmo_solution SET timeFieldName='programStartTime' WHERE lmId='DownloadStateSummary';

--5.50
update ubc_menuauth set menuId='#SQRY' where customer='SPC' and menuId='SQRY';
update ubc_menuauth set menuId='#SBRD' where customer='SPC' and menuId='SBRD';
update ubc_menuauth set menuId='#SQRY' where customer='SQI' and menuId='SQRY';
update ubc_menuauth set menuId='#SBRD' where customer='SQI' and menuId='SBRD';
update ubc_menuauth set menuId='#SQRY' where customer='HOMEPLUS' and menuId='SQRY';
update ubc_menuauth set menuId='#SBRD' where customer='HOMEPLUS' and menuId='SBRD';
update ubc_menuauth set menuId='#SQRY' where customer='KINGYOO' and menuId='SQRY';
update ubc_menuauth set menuId='#SBRD' where customer='KINGYOO' and menuId='SBRD';

ALTER table ubc_announce modify column hostIdList text;
ALTER table ubc_announce alter column hostIdList text;

set GLOBAL sql_mode = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,NO_BACKSLASH_ESCAPES';

ALTER TABLE ubc_announce ADD hostNameList text;
ALTER TABLE ubc_announce ADD contentsType smallint;
ALTER TABLE ubc_announce ADD lrMargin int;
ALTER TABLE ubc_announce ADD udMargin int;
ALTER TABLE ubc_announce ADD sourceSize int;
ALTER TABLE ubc_announce ADD soundVolume int;
ALTER TABLE ubc_announce ADD fileSize bigint;
ALTER TABLE ubc_announce ADD fileMD5 varchar(255);
ALTER TABLE ubc_announce ADD width int;
ALTER TABLE ubc_announce ADD posX int;
ALTER TABLE ubc_announce ADD posY int;
ALTER TABLE ubc_announce ADD heightRatio float;

ALTER TABLE ubc_announce alter column lrMargin int;
ALTER TABLE ubc_announce alter column udMargin int;
ALTER TABLE ubc_announce alter column sourceSize int;
ALTER TABLE ubc_announce alter column soundVolume int;
ALTER TABLE ubc_announce alter column width int;
ALTER TABLE ubc_announce alter column posX int;
ALTER TABLE ubc_announce alter column posY int;

ALTER TABLE ubc_announce modify lrMargin int;
ALTER TABLE ubc_announce modify udMargin int;
ALTER TABLE ubc_announce modify sourceSize int;
ALTER TABLE ubc_announce modify soundVolume int;
ALTER TABLE ubc_announce modify width int;
ALTER TABLE ubc_announce modify posX int;
ALTER TABLE ubc_announce modify posY int;

--5.53
Create Index idx_ubc_downloadStateSummary on ubc_downloadStateSummary (programId);

--5.54
Create Index idx_ubc_tp on ubc_tp (bpId);
Create Index idx_ubc_th on ubc_th (bpId);

--5.55

CREATE TABLE  dbo.ubc_plano (
  mgrId 		varchar(50) NOT NULL,
  siteId 		varchar(255) NOT NULL,
  planoId 		varchar(255) NOT NULL,
  backgroundFile	varchar(255),
  description 		varchar(255),
  query			text,
  expand		smallint,
  planoType		smallint,
  PRIMARY KEY  (planoId)
);

CREATE TABLE  dbo.ubc_node (
  mgrId 		varchar(50) NOT NULL,
  siteId 		varchar(255) NOT NULL,
  planoId 		varchar(255) NOT NULL,
  nodeId 		varchar(255) NOT NULL,
  objectClass 		varchar(255) ,
  objectId 		varchar(255) ,
  x			smallint,
  y			smallint,
  PRIMARY KEY  (planoId,nodeId)
);

CREATE TABLE  dbo.ubc_nodeType (
  mgrId 		varchar(50) NOT NULL,
  nodeTypeId 		varchar(255) NOT NULL,
  objectClass		varchar(32),
  objectType		int NOT NULL default 0,
  objectState		smallint NOT NULL default 0,
  ext			varchar(10),
  PRIMARY KEY  (nodeTypeId)
);

CREATE TABLE  dbo.ubc_monitor (
  mgrId 		varchar(50) NOT NULL,
  siteId 		varchar(255) NOT NULL,
  hostId 		varchar(50) NOT NULL,
  monitorId 		varchar(50) NOT NULL,
  monitorType		int NOT NULL default 0,
  monitorName		varchar(255),
  description		varchar(255),
  operationalState	smallint NOT NULL default 0,
  adminState		smallint NOT NULL default 0,
  startupTime		varchar(10),
  shutdownTime		varchar(255),
  monitorUseTime	int NOT NULL default 0,
  weekShutdownTime	varchar(255),
  holiday		varchar(255),
  PRIMARY KEY  (hostId,monitorId)
);

CREATE TABLE  dbo.ubc_light (
  mgrId 		varchar(50) NOT NULL,
  siteId 		varchar(255) NOT NULL,
  hostId 		varchar(50) NOT NULL,
  lightId 		varchar(50) NOT NULL,
  lightType		int NOT NULL default 0,
  lightName		varchar(255),
  description		varchar(255),
  operationalState	smallint NOT NULL default 0,
  adminState		smallint NOT NULL default 0,
  startupTime		varchar(10),
  shutdownTime		varchar(255),
  brightness		int NOT NULL default 0,
  weekShutdownTime	varchar(255),
  holiday		varchar(255),
  PRIMARY KEY  (hostId,lightId)
);

insert into ubc_code values ('OD','800','LightType','Type0','0','0','1','WIA');
insert into ubc_code values ('OD','801','LightType','Type1','1','0','1','WIA');
insert into ubc_code values ('OD','802','LightType','Type2','2','0','1','WIA');

insert into ubc_code values ('OD','900','MonitorType','Type0','0','0','1','WIA');
insert into ubc_code values ('OD','901','MonitorType','Type1','1','0','1','WIA');
insert into ubc_code values ('OD','902','MonitorType','Type2','2','0','1','WIA');

CREATE TABLE  dbo.ubc_contents_customer (
  mgrId 		varchar(50) NOT NULL default '1',
  customerName 		varchar(255) NOT NULL,
  customerId 		varchar(255), 
  comment		varchar(255),
  incomingTime		datetime,
  outgoingTime		datetime
);

--5.58
ALTER table dbo.ubc_downloadStateSummary modify progress varchar(50);
ALTER table dbo.ubc_downloadStateSummary alter column progress varchar(50);

--5.7
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_1_0.png','Host','1','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_1_1.png','Host','1','1','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_2_0.png','Host','2','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_2_1.png','Host','2','1','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_3_0.png','Host','3','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_3_1.png','Host','3','1','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_4_0.png','Host','4','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_4_1.png','Host','4','1','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_5_0.png','Host','5','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_5_1.png','Host','5','1','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_99_0.png','Host','99','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Host_99_1.png','Host','99','1','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Monitor_0_0.png','Monitor','0','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Monitor_0_1.png','Monitor','0','1','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Monitor_1_0.png','Monitor','1','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Monitor_1_1.png','Monitor','1','1','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Monitor_2_0.png','Monitor','2','0','png');
INSERT INTO ubc_nodeType VALUES('OD','ICON_Monitor_2_1.png','Monitor','2','1','png');

--5.90

ALTER table dbo.ubc_host drop column trashCan_;
ALTER table dbo.ubc_host drop column  trashTime_;
ALTER table dbo.ubc_host add trashId_ varchar(255);

ALTER table dbo.ubc_site drop column trashCan_;
ALTER table dbo.ubc_site drop column trashTime_;
ALTER table dbo.ubc_site add trashId_  varchar(255);

ALTER table dbo.ubc_user drop column trashCan_;
ALTER table dbo.ubc_user drop column trashTime_;
ALTER table dbo.ubc_user add trashId_ varchar(255)  ;

ALTER table dbo.ubc_bp drop column trashCan_;
ALTER table dbo.ubc_bp drop column trashTime_;
ALTER table dbo.ubc_bp add trashId_ varchar(255);

ALTER table dbo.ubc_program drop column trashCan_;
ALTER table dbo.ubc_program drop column trashTime_;
ALTER table dbo.ubc_program add trashId_ varchar(255);

ALTER table dbo.ubc_downloadstate drop column trashCan_;
ALTER table dbo.ubc_downloadstate drop column trashTime_;
ALTER table dbo.ubc_downloadstate drop column trashId_;

ALTER table dbo.ubc_downloadstatesummary drop column trashCan_;
ALTER table dbo.ubc_downloadstatesummary drop column trashTime_;
ALTER table dbo.ubc_downloadstatesummary drop column trashId_;

ALTER table dbo.ubc_applylog drop column trashCan_;
ALTER table dbo.ubc_applylog drop column trashTime_;
ALTER table dbo.ubc_applylog drop column trashId_;

ALTER table dbo.ubc_contents drop column trashCan_;
ALTER table dbo.ubc_contents drop column trashTime_;
ALTER table dbo.ubc_contents drop column trashId_;

ALTER table dbo.ubc_tp drop column trashCan_;
ALTER table dbo.ubc_tp drop column trashTime_;
ALTER table dbo.ubc_tp drop column trashId_;

ALTER table dbo.ubc_th drop column trashCan_;
ALTER table dbo.ubc_th drop column trashTime_;
ALTER table dbo.ubc_th drop column trashId_;

DROP VIEW dbo.ubc_nodeView;
CREATE VIEW dbo.ubc_nodeView AS SELECT
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.hostType as objectType,
d.hostName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.soundVolume as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_host d
WHERE n.objectClass = 'Host' and  n.objectId = d.hostId
UNION SELECT 
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.monitorType as objectType,
d.monitorName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.monitorUseTime as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_monitor d
WHERE n.objectClass = 'Monitor' and  n.objectId = d.monitorId
UNION SELECT 
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.lightType as objectType,
d.lightName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.brightness as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_light d
WHERE n.objectClass = 'Light' and  n.objectId = d.lightId;

ALTER TABLE dbo.ubc_host  ADD  monitorType smallint NOT NULL DEFAULT 0;
DROP VIEW dbo.ubc_hostView;
CREATE VIEW dbo.ubc_hostView AS SELECT h.*, s.siteName,s.chainNo,s.businessCode,s.businessType, s.phoneNo1 from (dbo.ubc_host h left join dbo.ubc_site s on((h.siteId = s.siteId)));

DROP FUNCTION [dbo].[GetChildSite];
--PROCEDURE BEGIN
CREATE FUNCTION [dbo].[GetChildSite]
(@franchizeType varchar(255), @siteId varchar(255))
RETURNS table
AS
return (
with cte (mgrId, siteName, parentId, siteId, lvl, name_path, id_path, franchizeType)
as
(
select 1 as mgrId, siteName, parentId, siteId, 1 as lvl, convert(varchar(255), '/' + siteName) name_path, convert(varchar(255), '/' + siteId) id_path, franchizeType
from ubc_site
where siteId = @siteId and franchizeType = @franchizeType 
union all
select 1 as mgrId, a.siteName, a.parentId, a.siteId, lvl + 1, convert(varchar(255),  name_path + '/' + a.siteName) name_path, convert(varchar(255),  id_path + '/' + a.siteId) id_path, a.franchizeType
from ubc_site a, cte b
where a.parentId = b.siteId and a.franchizeType = b.franchizeType
)
select mgrId, siteName, siteId, parentId, lvl, name_path, id_path, franchizeType from cte
);
--PROCEDURE END

--PROCEDURE BEGIN

DELIMITER $$

DROP PROCEDURE IF EXISTS `dbo`.`GetChildSite`$$
CREATE DEFINER=`ubc`@`%` PROCEDURE  `dbo`.`GetChildSite`(inFranchizeType varchar(255), inSiteId varchar(255))
BEGIN
  prepare stmt FROM "CREATE TEMPORARY TABLE IF NOT EXISTS dbo.TempChildSite(mgrId varchar(255), siteName varchar(255), siteId varchar(255), parentId varchar(255), lvl INT, name_path varchar(255), id_path varchar(255), franchizeType varchar(255))";
  execute stmt;
  deallocate prepare stmt;

  TRUNCATE dbo.TempChildSite;
  
  INSERT INTO dbo.TempChildSite
  SELECT mgrId, siteName, siteId, parentId, 1, CONCAT('/', siteName), CONCAT('/', siteId), inFranchizeType 
  FROM dbo.ubc_site 
  WHERE franchizeType = inFranchizeType 
  AND siteId = inSiteId;

  CALL dbo.GetChildSiteRecursive(2, inFranchizeType, inSiteId, CONCAT('/', dbo.GetSiteName(inSiteId)), CONCAT('/', inSiteId));

END $$

DELIMITER ;
--PROCEDURE END

DROP FUNCTION dbo.GetChildSiteTrash;

CREATE TABLE dbo.ubc_approve
(
	mgrId varchar(50) NOT NULL,
	approveId varchar(255) NOT NULL,
	approveState	tinyint,
	action		varchar(255),
	target		varchar(255),
	details		text,
	requestedId	varchar(255),
	requestedTime	datetime,
	approvedId	varchar(255),
	approvedTime	datetime,
	description1	varchar(255),
	description2	varchar(255),
	PRIMARY KEY  (approveId)
);

CREATE TABLE dbo.ubc_trashcan
(
	mgrId varchar(50) NOT NULL,
	trashCanId varchar(255) NOT NULL,
	siteId		varchar(50),
	entityClass	varchar(50),
	idValue		varchar(255),
	nameValue	varchar(255),
	entity		varchar(255),
	attrList	text,
	eventTime	datetime,
	userId		varchar(255),
	PRIMARY KEY  (trashCanId)
);


