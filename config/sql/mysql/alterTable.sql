use dbo;

CREATE TABLE  `dbo`.`ubc_actionlog` (
  `logDate` varchar(50) NOT NULL,
  `logTime` varchar(50)  NOT NULL,
  `logId` varchar(50) NOT NULL,
  `logIp` varchar(50) DEFAULT NULL,
  `result` varchar(50)  DEFAULT NULL,
  `windowName` varchar(256) DEFAULT NULL,
  `actionType` varchar(256) DEFAULT NULL,
  `fileName` varchar(256) DEFAULT NULL,
  `fileSzie` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `dbo`.`ubc_loginlog` (
  `logDate` varchar(50)  NOT NULL,
  `logTime` varchar(50)  NOT NULL,
  `logId` varchar(50) NOT NULL,
  `logIp` varchar(50) DEFAULT NULL,
  `result` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `dbo`.`ubc_authchangelog` (
  `logDate` varchar(50)  NOT NULL,
  `logTime` varchar(50)  NOT NULL,
  `logId` varchar(50) NOT NULL,
  `changeType` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
alter table ubc_authchangelog add changeString varchar(255);

ALTER TABLE ubc_user ADD lastLoginTime DATETIME;
ALTER TABLE ubc_user ADD lastPWChangeTime DATETIME;
ALTER TABLE ubc_user ADD loginFailCount TINYINT DEFAULT 0 ;
ALTER TABLE ubc_user ADD adminstate TINYINT DEFAULT 1 ;


CREATE TABLE  ubc_connectionDailyStat (
  statDate datetime NOT NULL,
  siteId varchar(255) NOT NULL,
  hostId varchar(255) NOT NULL,
  disconnectCount bigInt NULL,
  disconnectSec  bigInt NULL
);


ALTER TABLE ubc_playlog ADD mgrId varchar(50);
ALTER TABLE ubc_playlog ADD playLogId varchar(50);
UPDATE  ubc_lmo_solution SET timeFieldName='programStartTime' WHERE lmId='DownloadStateSummary';

--5.5
update ubc_menuauth set menuId='#SQRY' where customer='SPC' and menuId='SQRY';
update ubc_menuauth set menuId='#SBRD' where customer='SPC' and menuId='SBRD';
update ubc_menuauth set menuId='#SQRY' where customer='SQI' and menuId='SQRY';
update ubc_menuauth set menuId='#SBRD' where customer='SQI' and menuId='SBRD';
update ubc_menuauth set menuId='#SQRY' where customer='HOMEPLUS' and menuId='SQRY';
update ubc_menuauth set menuId='#SBRD' where customer='HOMEPLUS' and menuId='SBRD';
update ubc_menuauth set menuId='#SQRY' where customer='KINGYOO' and menuId='SQRY';
update ubc_menuauth set menuId='#SBRD' where customer='KINGYOO' and menuId='SBRD';
ALTER table ubc_announce modify column hostIdList text;
ALTER table ubc_announce alter column hostIdList text;

set GLOBAL sql_mode = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION,NO_BACKSLASH_ESCAPES';

ALTER TABLE ubc_announce ADD hostNameList text;
ALTER TABLE ubc_announce ADD contentsType smallint;
ALTER TABLE ubc_announce ADD lrMargin int;
ALTER TABLE ubc_announce ADD udMargin int;
ALTER TABLE ubc_announce ADD sourceSize int;
ALTER TABLE ubc_announce ADD soundVolume int;
ALTER TABLE ubc_announce ADD fileSize bigint;
ALTER TABLE ubc_announce ADD fileMD5 varchar(255);
ALTER TABLE ubc_announce ADD width int;
ALTER TABLE ubc_announce ADD posX int;
ALTER TABLE ubc_announce ADD posY int;
ALTER TABLE ubc_announce ADD heightRatio float;

ALTER TABLE ubc_announce modify lrMargin int;
ALTER TABLE ubc_announce modify udMargin int;
ALTER TABLE ubc_announce modify sourceSize int;
ALTER TABLE ubc_announce modify soundVolume int;
ALTER TABLE ubc_announce modify width int;
ALTER TABLE ubc_announce modify posX int;
ALTER TABLE ubc_announce modify posY int;

--5.53
Create Index idx_ubc_downloadStateSummary on ubc_downloadStateSummary (programId);

--5.54
Create Index idx_ubc_tp on ubc_tp (bpId);
Create Index idx_ubc_th on ubc_th (bpId);

--5.55

CREATE TABLE  dbo.ubc_plano (
  mgrId 		varchar(50) NOT NULL,
  siteId 		varchar(255) NOT NULL,
  planoId 		varchar(255) NOT NULL,
  backgroundFile	varchar(255),
  description 		varchar(255),
  query			text,
  expand		smallint,
  planoType		smallint,
  PRIMARY KEY  (planoId)
);

CREATE TABLE  dbo.ubc_node (
  mgrId 		varchar(50) NOT NULL,
  siteId 		varchar(255) NOT NULL,
  planoId 		varchar(255) NOT NULL,
  nodeId 		varchar(255) NOT NULL,
  objectClass 		varchar(255) ,
  objectId 		varchar(255) ,
  x			smallint,
  y			smallint,
  PRIMARY KEY  (planoId,nodeId)
);

CREATE TABLE  dbo.ubc_nodeType (
  mgrId 		varchar(50) NOT NULL,
  nodeTypeId 		varchar(255) NOT NULL,
  objectClass		varchar(32),
  objectType		int NOT NULL default 0,
  objectState		smallint NOT NULL default 0,
  ext			varchar(10),
  PRIMARY KEY  (nodeTypeId)
);

CREATE TABLE  dbo.ubc_monitor (
  mgrId 		varchar(50) NOT NULL,
  siteId 		varchar(255) NOT NULL,
  hostId 		varchar(50) NOT NULL,
  monitorId 		varchar(50) NOT NULL,
  monitorType		int NOT NULL default 0,
  monitorName		varchar(255),
  description		varchar(255),
  operationalState	smallint NOT NULL default 0,
  adminState		smallint NOT NULL default 0,
  startupTime		varchar(10),
  shutdownTime		varchar(255),
  monitorUseTime	int NOT NULL default 0,
  weekShutdownTime	varchar(255),
  holiday		varchar(255),
  PRIMARY KEY  (hostId,monitorId)
);

CREATE TABLE  dbo.ubc_light (
  mgrId 		varchar(50) NOT NULL,
  siteId 		varchar(255) NOT NULL,
  hostId 		varchar(50) NOT NULL,
  lightId 		varchar(50) NOT NULL,
  lightType		int NOT NULL default 0,
  lightName		varchar(255),
  description		varchar(255),
  operationalState	smallint NOT NULL default 0,
  adminState		smallint NOT NULL default 0,
  startupTime		varchar(10),
  shutdownTime		varchar(255),
  brightness		int NOT NULL default 0,
  weekShutdownTime	varchar(255),
  holiday		varchar(255),
  PRIMARY KEY  (hostId,lightId)
);

DROP VIEW dbo.ubc_nodeView;
CREATE VIEW dbo.ubc_nodeView AS SELECT
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.hostType as objectType,
d.hostName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.soundVolume as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_host d
WHERE n.objectClass = 'Host' and  n.objectId = d.hostId
UNION SELECT 
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.monitorType as objectType,
d.monitorName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.monitorUseTime as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_monitor d
WHERE n.objectClass = 'Monitor' and  n.objectId = d.monitorId
UNION SELECT 
n.mgrId as mgrId,
n.siteId as siteId,
n.planoId as planoId,
n.nodeId as nodeViewId,
n.objectClass as objectClass,
n.objectId as objectId,
n.x as x,
n.y as y,
d.lightType as objectType,
d.lightName as targetName,
d.description as description,
d.operationalState as operationalState,
d.adminState as adminState,
d.brightness as nodeValue,
d.startupTime as startupTime,
d.shutdownTime as shutdownTime,
d.hostId as hostId,
d.siteId as hostSiteId,
d.weekShutdownTime as weekShutdownTime,
d.holiday as holiday
FROM dbo.ubc_node n , dbo.ubc_light d
WHERE n.objectClass = 'Light' and  n.objectId = d.lightId;

insert into ubc_code values ('OD','800','LightType','Type0','0','0','1','WIA');
insert into ubc_code values ('OD','801','LightType','Type1','1','0','1','WIA');
insert into ubc_code values ('OD','802','LightType','Type2','2','0','1','WIA');

insert into ubc_code values ('OD','900','MonitorType','Type0','0','0','1','WIA');
insert into ubc_code values ('OD','901','MonitorType','Type1','1','0','1','WIA');
insert into ubc_code values ('OD','902','MonitorType','Type2','2','0','1','WIA');

CREATE TABLE  dbo.ubc_contents_customer (
  mgrId 		varchar(50) NOT NULL default '1',
  customerName 		varchar(255) NOT NULL,
  customerId 		varchar(255), 
  comment		varchar(255),
  incomingTime		datetime,
  outgoingTime		datetime
);
