### SHOW COLUMNS FROM user;
DELETE FROM USER WHERE user = 'ubc';
FLUSH PRIVILEGES ;
INSERT INTO user (
	Host, User, Password,
	Select_priv, Insert_priv, Update_priv, Delete_priv, Create_priv, Index_priv, 
	Alter_priv, Show_db_priv, Create_tmp_table_priv, Create_view_priv,Show_view_priv,
	ssl_cipher, x509_issuer, x509_subject, plugin, authentication_string
) VALUES (
	'%', 'ubc', password('sqicop'), 
	'Y','Y','Y','Y','Y','Y',
	'Y','Y','Y','Y','Y',
	'','',''
	,'',''
);
GRANT ALL PRIVILEGES ON *.* TO ubc IDENTIFIED BY 'sqicop'  WITH GRANT OPTION ;
FLUSH PRIVILEGES ;

SELECT concat(user, ' created') user FROM user where user='ubc';
