use dbo;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`Find_n_Right`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`Find_n_Right`(findStr varchar(255), fullStr varchar(255)) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare resultStr varchar(255);
  select RIGHT(fullStr, LENGTH(fullStr) - (LOCATE(findStr, fullStr)+LENGTH(findStr)-1)) into resultStr from dual;
  return (resultStr);
END;

 $$

DELIMITER ;

DELIMITER $$

DROP PROCEDURE IF EXISTS `dbo`.`GetChildSite`$$
CREATE DEFINER=`ubc`@`%` PROCEDURE  `dbo`.`GetChildSite`(inFranchizeType varchar(255), inSiteId varchar(255))
BEGIN

  /*
   * 임시 테이블 생성
   */
  prepare stmt FROM "CREATE TEMPORARY TABLE IF NOT EXISTS dbo.TempChildSite(mgrId varchar(255), siteName varchar(255), siteId varchar(255), parentId varchar(255), lvl 

INT, name_path varchar(255), id_path varchar(255), franchizeType varchar(255))";
  execute stmt;
  deallocate prepare stmt;

  /*
   * 임시 테이블 청소
   */
  TRUNCATE dbo.TempChildSite;
  
  /*
   * 임시 테이블에 조건에 맞는 데이터 인서트
   */
  INSERT INTO dbo.TempChildSite
  SELECT mgrId, siteName, siteId, parentId, 1, CONCAT('/', siteName), CONCAT('/', siteId), inFranchizeType 
  FROM dbo.ubc_site 
  WHERE franchizeType = inFranchizeType 
  AND siteId = inSiteId;

  /*
   * 입력된 데이터의 자식 데이터를 넣기 위해 프로시져 호출
   */
  CALL dbo.GetChildSiteRecursive(2, inFranchizeType, inSiteId, CONCAT('/', dbo.GetSiteName(inSiteId)), CONCAT('/', inSiteId));

END $$

DELIMITER ;

DELIMITER $$

DROP PROCEDURE IF EXISTS `dbo`.`GetChildSiteRecursive`$$
CREATE DEFINER=`ubc`@`%` PROCEDURE  `dbo`.`GetChildSiteRecursive`(level INT, inFranchizeType varchar(255), inParentId varchar(255), inNamePath varchar(8000), inIdPath varchar(8000))
BEGIN

  /*
   * 변수 선언
   */
  DECLARE done INT DEFAULT 0;
  DECLARE _siteName varchar(255);
  DECLARE _siteId varchar(255);
  DECLARE _level INT DEFAULT 0;
  DECLARE _id_path varchar(255);
  DECLARE _name_path varchar(255);

  /*
   * 커서 선언
   */
  DECLARE cur1 CURSOR FOR SELECT siteName, siteId, lvl, name_path, id_path from dbo.TempChildSite where franchizeType = inFranchizeType and parentId = inParentId;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

  /*
   * 입력된 데이터의 자식 데이터를 넣기 위해 프로시져 호출
   */
  INSERT INTO dbo.TempChildSite
  SELECT mgrId, siteName, siteId, parentId, level, CONCAT(inNamePath, CONCAT('/', siteName)), CONCAT(inIdPath, CONCAT('/', siteId)), inFranchizeType 
  FROM dbo.ubc_site 
  WHERE franchizeType = inFranchizeType 
  AND parentId = inParentId;
 
  /*
   * 패치 상태를 위한 변수 초기화
   */
  SET done = 0;
  
  /*
   * DB open
   */
  OPEN cur1;

  /*
   * 입력된 데이터의 자식 데이터를 넣기 위해 한건시 패치 후 프로시져 호출
   */
  REPEAT
    FETCH cur1 INTO _siteName, _siteId, _level, _name_path, _id_path;
    
    IF NOT done THEN
      CALL dbo.GetChildSiteRecursive(_level + 1, inFranchizeType, _siteId, _name_path, _id_path);
    END IF;
    
  UNTIL done END REPEAT;
  
  /*
   * DB cursor close
   */
  CLOSE cur1;
  
END $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`GetClassValue`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`GetClassValue`(inString varchar(255), idx int) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare resultString nvarchar(255);
  declare tempString nvarchar(255);
  declare oPos int;
  declare nPos int;
  declare curIndex int;

  set oPos = 1; 
  set nPos = 1; 
  set curIndex = 1;

  WHILE (nPos > 0) DO
  
    set nPos = LOCATE('/', inString, oPos);
    
    if nPos = 0 then
      set tempString = right(inString, length(inString) - oPos + 1);
    else
      set tempString = substring(inString, oPos, nPos - oPos);
    end if;
    
    if idx = curIndex then
      set resultString = tempString;
    end if;
    
    set curIndex = curIndex + 1;
    set oPos = nPos + 1;
  end while;

  if length(resultString) > 0 then
    set resultString = right(resultString, length(resultString) - (LOCATE('=', resultString)));
  end if;

  return resultString;
END;

 $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`GetContentsName`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`GetContentsName`(inId varchar(255)) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare outName varchar(255);
  select contentsName into outName from dbo.ubc_contents where contentsId = inId;
  return (outName);
END;

 $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`GetHostName`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`GetHostName`(inId varchar(255)) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare outName varchar(255);
  select hostName into outName from dbo.ubc_host where hostId = inId;
  return (outName);
END;

 $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`GetSiteName`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`GetSiteName`(inId varchar(255)) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare outName varchar(255);
  select siteName into outName from dbo.ubc_site where siteId = inId;
  return (outName);
END;

 $$

DELIMITER ;


DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`Password_Decode`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`Password_Decode`(input varchar(255)) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
	DECLARE ret varchar(255) DEFAULT '';
	DECLARE done TINYINT DEFAULT 0;

	IF input IS NULL or LENGTH(input) = 0 THEN
		RETURN NULL;
	END IF;

each_block:
	WHILE NOT done DO BEGIN
		DECLARE accum_value BIGINT UNSIGNED DEFAULT 0;
		DECLARE in_count TINYINT DEFAULT 0;
		DECLARE out_count TINYINT DEFAULT 3;

each_input_char:
		WHILE in_count < 4 DO BEGIN
			DECLARE first_char CHAR(1);
	
			IF LENGTH(input) = 0 THEN
				RETURN ret;
			END IF;
	
			SET first_char = SUBSTRING(input,1,1);
			SET input = SUBSTRING(input,2);
	
			BEGIN
				DECLARE tempval TINYINT UNSIGNED;
				DECLARE error TINYINT DEFAULT 0;
				DECLARE base64_getval CURSOR FOR SELECT val FROM dbo.base64_data WHERE c = first_char;
				DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET error = 1;
	
				OPEN base64_getval;
				FETCH base64_getval INTO tempval;
				CLOSE base64_getval;

				IF error THEN
					ITERATE each_input_char;
				END IF;

				SET accum_value = (accum_value << 6) + tempval;
			END;

			SET in_count = in_count + 1;

			IF first_char = '=' THEN
				SET done = 1;
				SET out_count = out_count - 1;
			END IF;
		END; END WHILE;
		WHILE out_count > 0 DO BEGIN
			SET ret = CONCAT(ret,CHAR((accum_value & 0xff0000) >> 16));
			SET out_count = out_count - 1;
			SET accum_value = (accum_value << 8) & 0xffffff;
		END; END WHILE;
	
	END; END WHILE;

	RETURN ret;
END;

 $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`Password_Encode`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`Password_Encode`(input varchar(255)) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
	DECLARE ret varchar(255) DEFAULT '';
	DECLARE done TINYINT DEFAULT 0;

	IF input IS NULL or LENGTH(input) = 0 THEN
		RETURN NULL;
	END IF;

each_block:
	WHILE NOT done DO BEGIN
		DECLARE accum_value BIGINT UNSIGNED DEFAULT 0;
		DECLARE in_count TINYINT DEFAULT 0;
		DECLARE out_count TINYINT;

each_input_char:
		WHILE in_count < 3 DO BEGIN
			DECLARE first_char CHAR(1);
	
			IF LENGTH(input) = 0 THEN
				SET done = 1;
				SET accum_value = accum_value << (8 * (3 - in_count));
				LEAVE each_input_char;
			END IF;
	
			SET first_char = SUBSTRING(input,1,1);
			SET input = SUBSTRING(input,2);
	
			SET accum_value = (accum_value << 8) + ASCII(first_char);

			SET in_count = in_count + 1;
		END; END WHILE;

		CASE
			WHEN in_count = 3 THEN SET out_count = 4;
			WHEN in_count = 2 THEN SET out_count = 3;
			WHEN in_count = 1 THEN SET out_count = 2;
			ELSE RETURN ret;
		END CASE;

		WHILE out_count > 0 DO BEGIN
			BEGIN
				DECLARE out_char CHAR(1);
				DECLARE base64_getval CURSOR FOR SELECT c FROM dbo.base64_data WHERE val = (accum_value >> 18);

				OPEN base64_getval;
				FETCH base64_getval INTO out_char;
				CLOSE base64_getval;

				SET ret = CONCAT(ret,out_char);
				SET out_count = out_count - 1;
				SET accum_value = accum_value << 6 & 0xffffff;
			END;
		END; END WHILE;

		CASE
			WHEN in_count = 2 THEN SET ret = CONCAT(ret,'=');
			WHEN in_count = 1 THEN SET ret = CONCAT(ret,'==');
			ELSE BEGIN END;
		END CASE;
	
	END; END WHILE;

	RETURN ret;
END;

 $$

DELIMITER ;

