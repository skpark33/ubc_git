use dbo;

DROP TABLE IF EXISTS `dbo`.`ubc_actionlog`;
CREATE TABLE  `dbo`.`ubc_actionlog` (
  `logDate` varchar(50)  NOT NULL,
  `logTime` varchar(50)  NOT NULL,
  `logId` varchar(50) NOT NULL,
  `logIp` varchar(50) DEFAULT NULL,
  `result` varchar(50)  DEFAULT NULL,
  `windowName` varchar(256) DEFAULT NULL,
  `actionType` varchar(256) DEFAULT NULL,
  `fileName` varchar(256) DEFAULT NULL,
  `fileSzie` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_loginlog`;
CREATE TABLE  `dbo`.`ubc_loginlog` (
  `logDate` varchar(50)  NOT NULL,
  `logTime` varchar(50)  NOT NULL,
  `logId` varchar(50) NOT NULL,
  `logIp` varchar(50) DEFAULT NULL,
  `result` varchar(50)  DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_authchangelog`;
CREATE TABLE  `dbo`.`ubc_authchangelog` (
  `logDate` varchar(50)  NOT NULL,
  `logTime` varchar(50)  NOT NULL,
  `logId` varchar(50) NOT NULL,
  `changeType` varchar(50) DEFAULT NULL,
  `changeString` varchar(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `dbo`.`base64_data`;
CREATE TABLE  `dbo`.`base64_data` (
  `c` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `val` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`call_postbox`;
CREATE TABLE  `dbo`.`call_postbox` (
  `CALLID` int(10) NOT NULL AUTO_INCREMENT,
  `DIRECTIVE` varchar(64) DEFAULT NULL,
  `ENTITY` varchar(64) DEFAULT NULL,
  `VALUESTRING` varchar(4096) DEFAULT NULL,
  `CALLER` varchar(64) DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  `CALLTIME` datetime DEFAULT NULL,
  `CHECKFLAG` int(10) DEFAULT NULL,
  `CALLFLAG` int(10) DEFAULT NULL,
  `ERRORFLAG` int(10) DEFAULT NULL,
  `REMARKS` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`CALLID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_banlist`;
CREATE TABLE  `dbo`.`em_banlist` (
  `service_type` char(2) NOT NULL,
  `ban_seq` int(10) NOT NULL,
  `ban_type` char(1) NOT NULL,
  `content` varchar(45) NOT NULL,
  `send_yn` char(1) NOT NULL DEFAULT 'N',
  `ban_status_yn` char(1) NOT NULL DEFAULT 'Y',
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_user` varchar(20) DEFAULT NULL,
  `update_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `update_user` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`service_type`,`ban_seq`),
  KEY `idx_em_banlist_1` (`ban_type`,`service_type`,`ban_status_yn`),
  KEY `idx_em_banlist_2` (`content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_mmt_client`;
CREATE TABLE  `dbo`.`em_mmt_client` (
  `mt_pr` int(10) NOT NULL,
  `mt_seq` int(10) NOT NULL,
  `msg_status` char(1) NOT NULL DEFAULT '1',
  `recipient_num` varchar(25) NOT NULL,
  `change_word1` varchar(20) DEFAULT NULL,
  `change_word2` varchar(20) DEFAULT NULL,
  `change_word3` varchar(20) DEFAULT NULL,
  `change_word4` varchar(20) DEFAULT NULL,
  `change_word5` varchar(20) DEFAULT NULL,
  `date_mt_sent` datetime DEFAULT NULL,
  `date_rslt` datetime DEFAULT NULL,
  `date_mt_report` datetime DEFAULT NULL,
  `mt_report_code_ib` char(4) DEFAULT NULL,
  `mt_report_code_ibtype` char(1) DEFAULT NULL,
  `carrier` int(10) DEFAULT NULL,
  `rs_id` varchar(20) DEFAULT NULL,
  `recipient_net` int(10) DEFAULT NULL,
  `recipient_npsend` varchar(1) DEFAULT NULL,
  `country_code` varchar(8) NOT NULL DEFAULT '82',
  PRIMARY KEY (`mt_pr`,`mt_seq`),
  KEY `idx_em_mmt_client_1` (`recipient_num`),
  KEY `idx_em_mmt_client_2` (`msg_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_mmt_file`;
CREATE TABLE  `dbo`.`em_mmt_file` (
  `attach_file_group_key` int(10) NOT NULL,
  `attach_file_group_seq` int(10) NOT NULL,
  `attach_file_seq` int(10) NOT NULL,
  `attach_file_subpath` varchar(64) DEFAULT NULL,
  `attach_file_name` varchar(64) NOT NULL,
  `attach_file_carrier` int(10) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`attach_file_group_key`,`attach_file_group_seq`),
  KEY `idx_em_mmt_file_1` (`attach_file_seq`),
  KEY `idx_em_mmt_file_2` (`attach_file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_mmt_tran`;
CREATE TABLE  `dbo`.`em_mmt_tran` (
  `mt_pr` int(10) NOT NULL AUTO_INCREMENT,
  `mt_refkey` varchar(20) DEFAULT NULL,
  `priority` char(2) NOT NULL DEFAULT 'S',
  `msg_class` char(1) DEFAULT '1',
  `date_client_req` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `subject` varchar(40) NOT NULL,
  `content_type` int(10) DEFAULT '0',
  `content` varchar(4000) NOT NULL,
  `attach_file_group_key` int(10) DEFAULT '0',
  `callback` varchar(25) NOT NULL,
  `service_type` char(2) NOT NULL,
  `broadcast_yn` char(1) NOT NULL DEFAULT 'N',
  `msg_status` char(1) NOT NULL DEFAULT '1',
  `recipient_num` varchar(25) DEFAULT NULL,
  `date_mt_sent` datetime DEFAULT NULL,
  `date_rslt` datetime DEFAULT NULL,
  `date_mt_report` datetime DEFAULT NULL,
  `mt_report_code_ib` char(4) DEFAULT NULL,
  `mt_report_code_ibtype` char(1) DEFAULT NULL,
  `carrier` int(10) DEFAULT NULL,
  `rs_id` varchar(20) DEFAULT NULL,
  `recipient_net` int(10) DEFAULT NULL,
  `recipient_npsend` varchar(1) DEFAULT NULL,
  `country_code` varchar(8) NOT NULL DEFAULT '82',
  `charset` varchar(20) DEFAULT NULL,
  `msg_type` int(10) DEFAULT NULL,
  `crypto_yn` char(1) DEFAULT 'Y',
  PRIMARY KEY (`mt_pr`),
  KEY `idx_em_mmt_tran_1` (`msg_status`,`date_client_req`),
  KEY `idx_em_mmt_tran_2` (`recipient_num`),
  KEY `idx_em_mmt_tran_3` (`attach_file_group_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_resultcode`;
CREATE TABLE  `dbo`.`em_resultcode` (
  `service_type` char(2) NOT NULL,
  `rslt_code` varchar(4) NOT NULL,
  `rslt_type` char(1) NOT NULL,
  `rslt_name` varchar(80) NOT NULL,
  `rslt_pname` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`service_type`,`rslt_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_smt_client`;
CREATE TABLE  `dbo`.`em_smt_client` (
  `mt_pr` int(10) NOT NULL,
  `mt_seq` int(10) NOT NULL,
  `msg_status` char(1) NOT NULL DEFAULT '1',
  `recipient_num` varchar(25) NOT NULL,
  `change_word1` varchar(20) DEFAULT NULL,
  `change_word2` varchar(20) DEFAULT NULL,
  `change_word3` varchar(20) DEFAULT NULL,
  `change_word4` varchar(20) DEFAULT NULL,
  `change_word5` varchar(20) DEFAULT NULL,
  `date_mt_sent` datetime DEFAULT NULL,
  `date_rslt` datetime DEFAULT NULL,
  `date_mt_report` datetime DEFAULT NULL,
  `mt_report_code_ib` char(4) DEFAULT NULL,
  `mt_report_code_ibtype` char(1) DEFAULT NULL,
  `carrier` int(10) DEFAULT NULL,
  `rs_id` varchar(20) DEFAULT NULL,
  `recipient_net` int(10) DEFAULT NULL,
  `recipient_npsend` varchar(1) DEFAULT NULL,
  `country_code` varchar(8) NOT NULL DEFAULT '82',
  PRIMARY KEY (`mt_pr`,`mt_seq`),
  KEY `idx_em_smt_client_1` (`recipient_num`),
  KEY `idx_em_smt_client_2` (`msg_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_smt_tran`;
CREATE TABLE  `dbo`.`em_smt_tran` (
  `mt_pr` int(10) NOT NULL AUTO_INCREMENT,
  `mt_refkey` varchar(20) DEFAULT NULL,
  `priority` char(2) NOT NULL DEFAULT 'S',
  `date_client_req` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `content` varchar(255) NOT NULL,
  `callback` varchar(25) NOT NULL,
  `service_type` char(2) NOT NULL,
  `broadcast_yn` char(1) NOT NULL DEFAULT 'N',
  `msg_status` char(1) NOT NULL DEFAULT '1',
  `recipient_num` varchar(25) DEFAULT NULL,
  `date_mt_sent` datetime DEFAULT NULL,
  `date_rslt` datetime DEFAULT NULL,
  `date_mt_report` datetime DEFAULT NULL,
  `mt_report_code_ib` char(4) DEFAULT NULL,
  `mt_report_code_ibtype` char(1) DEFAULT NULL,
  `carrier` int(10) DEFAULT NULL,
  `rs_id` varchar(20) DEFAULT NULL,
  `recipient_net` int(10) DEFAULT NULL,
  `recipient_npsend` varchar(1) DEFAULT NULL,
  `country_code` varchar(8) NOT NULL DEFAULT '82',
  `charset` varchar(20) DEFAULT NULL,
  `msg_type` int(10) DEFAULT NULL,
  `crypto_yn` char(1) DEFAULT 'Y',
  PRIMARY KEY (`mt_pr`),
  KEY `idx_em_smt_tran_1` (`msg_status`,`date_client_req`),
  KEY `idx_em_smt_tran_2` (`recipient_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_statistics_d`;
CREATE TABLE  `dbo`.`em_statistics_d` (
  `stat_date` varchar(8) NOT NULL,
  `stat_servicetype` char(2) NOT NULL,
  `stat_payment_code` varchar(20) NOT NULL,
  `stat_carrier` int(10) NOT NULL,
  `stat_fail_code` varchar(10) NOT NULL,
  `stat_fail_cnt` int(10) DEFAULT NULL,
  `stat_regdate` datetime NOT NULL DEFAULT '1970-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_statistics_m`;
CREATE TABLE  `dbo`.`em_statistics_m` (
  `stat_date` varchar(8) NOT NULL,
  `stat_servicetype` char(2) NOT NULL,
  `stat_payment_code` varchar(20) NOT NULL,
  `stat_carrier` int(10) NOT NULL,
  `stat_success` int(10) DEFAULT NULL,
  `stat_failure` int(10) DEFAULT NULL,
  `stat_invalid` int(10) DEFAULT NULL,
  `stat_invalid_ib` int(10) DEFAULT NULL,
  `stat_remained` int(10) DEFAULT NULL,
  `stat_regdate` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`stat_date`,`stat_servicetype`,`stat_payment_code`,`stat_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`em_status`;
CREATE TABLE  `dbo`.`em_status` (
  `emma_id` char(2) NOT NULL,
  `process_name` varchar(40) NOT NULL,
  `pid` varchar(20) NOT NULL,
  `service_type` char(2) NOT NULL,
  `cnt_today` int(10) DEFAULT NULL,
  `cnt_total` int(10) DEFAULT NULL,
  `cnt_resent_1` int(10) DEFAULT NULL,
  `cnt_resent_10` int(10) DEFAULT NULL,
  `que_size` int(10) DEFAULT NULL,
  `conn_time` varchar(20) DEFAULT NULL,
  `update_time` varchar(20) DEFAULT NULL,
  `conn_gw_info` varchar(40) DEFAULT NULL,
  `reg_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`emma_id`,`process_name`,`pid`,`service_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`event_postbox`;
CREATE TABLE  `dbo`.`event_postbox` (
  `EVENTID` int(10) NOT NULL AUTO_INCREMENT,
  `DOMAIN` varchar(64) DEFAULT NULL,
  `ENTITY` varchar(64) DEFAULT NULL,
  `EVENTTYPE` varchar(64) DEFAULT NULL,
  `EVENTVALUE` varchar(1024) DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  `SENDTIME` datetime DEFAULT NULL,
  `CHECKFLAG` decimal(1,0) DEFAULT NULL,
  `SENDFLAG` decimal(1,0) DEFAULT NULL,
  `ERRORFLAG` decimal(1,0) DEFAULT NULL,
  `REMARKS` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`EVENTID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`reply_postbox`;
CREATE TABLE  `dbo`.`reply_postbox` (
  `CALLID` int(10) NOT NULL,
  `REPLYID` int(10) NOT NULL,
  `ENTITY` varchar(64) DEFAULT NULL,
  `VALUESTRING` varchar(4096) DEFAULT NULL,
  `CREATETIME` datetime DEFAULT NULL,
  `CHECKFLAG` int(10) DEFAULT NULL,
  `ERRORFLAG` int(10) DEFAULT NULL,
  `REMARKS` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`CALLID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`sysdiagrams`;
CREATE TABLE  `dbo`.`sysdiagrams` (
  `name` varchar(128) NOT NULL,
  `principal_id` int(10) NOT NULL,
  `diagram_id` int(10) NOT NULL AUTO_INCREMENT,
  `version` int(10) DEFAULT NULL,
  `definition` varbinary(255) DEFAULT NULL,
  PRIMARY KEY (`diagram_id`),
  UNIQUE KEY `UK_principal_name` (`principal_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ts_cronjob`;
CREATE TABLE  `dbo`.`ts_cronjob` (
  `cronJobId` varchar(100) NOT NULL,
  `exeTime` varchar(50) DEFAULT NULL,
  `exeType` varchar(20) DEFAULT NULL,
  `entity` varchar(200) DEFAULT NULL,
  `directive` varchar(64) DEFAULT NULL,
  `attributes` varchar(1000) DEFAULT NULL,
  `operationalStatus` decimal(1,0) DEFAULT '1',
  PRIMARY KEY (`cronJobId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_agtserverlog`;
CREATE TABLE  `dbo`.`ubc_agtserverlog` (
  `mgrId` varchar(255) NOT NULL,
  `agtserverlogId` varchar(255) DEFAULT NULL,
  `actionKind` varchar(255) DEFAULT NULL,
  `actionDirective` varchar(255) DEFAULT NULL,
  `actionTime` datetime NOT NULL,
  `actionObject` varchar(255) DEFAULT NULL,
  `actionComment` varchar(255) DEFAULT NULL,
  `additionalInfo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_announce`;
CREATE TABLE  `dbo`.`ubc_announce` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `announceId` varchar(255) NOT NULL,
  `serialNo` decimal(10,0) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `hostIdList` varchar(1024) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `position` decimal(3,0) DEFAULT NULL,
  `height` decimal(5,0) DEFAULT NULL,
  `comment1` longtext,
  `comment2` longtext,
  `comment3` longtext,
  `comment4` longtext,
  `comment5` longtext,
  `comment6` longtext,
  `comment7` longtext,
  `comment8` longtext,
  `comment9` longtext,
  `comment10` longtext,
  `font` varchar(255) DEFAULT NULL,
  `fontSize` decimal(5,0) DEFAULT NULL,
  `bgColor` varchar(255) DEFAULT NULL,
  `fgColor` varchar(255) DEFAULT NULL,
  `bgLocation` varchar(255) DEFAULT NULL,
  `bgFile` varchar(255) DEFAULT NULL,
  `playSpeed` decimal(5,0) DEFAULT '1000',
  `alpha` decimal(5,0) DEFAULT NULL,
  UNIQUE KEY `UQ__ubc_anno__052E52E52BB470E3` (`siteId`,`announceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_applylog`;
CREATE TABLE  `dbo`.`ubc_applylog` (
  `mgrId` varchar(50) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `applyId` varchar(255) NOT NULL,
  `programId` varchar(255) DEFAULT NULL,
  `applyTime` datetime DEFAULT NULL,
  `how` varchar(50) DEFAULT NULL,
  `who` varchar(255) DEFAULT NULL,
  `why` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`applyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_bp`;
CREATE TABLE  `dbo`.`ubc_bp` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `bpId` varchar(255) NOT NULL,
  `bpName` varchar(255) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `weekInfo` varchar(20) DEFAULT NULL,
  `exceptDay` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `zorder` varchar(256) NOT NULL DEFAULT '0',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `registerId` varchar(255) DEFAULT NULL,
  `downloadTime` datetime DEFAULT NULL,
  `retryCount` tinyint(3) DEFAULT '0',
  `retryGap` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`siteId`,`bpId`),
  UNIQUE KEY `UQ__ubc_bp__CFA7F5AE2E90DD8E` (`siteId`,`bpId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_bplog`;
CREATE TABLE  `dbo`.`ubc_bplog` (
  `mgrId` varchar(255) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `bpId` varchar(255) NOT NULL,
  `bpLogId` varchar(255) DEFAULT NULL,
  `touchTime` datetime DEFAULT NULL,
  `who` varchar(255) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `bpName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_bpmon`;
CREATE TABLE  `dbo`.`ubc_bpmon` (
  `mgrId` varchar(255) NOT NULL,
  `bpMonId` varchar(255) NOT NULL,
  `bpId` varchar(255) NOT NULL,
  `programId` varchar(255) NOT NULL,
  `reportTime` datetime DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `reportWay` tinyint(3) DEFAULT NULL,
  `downloadFailLimit` tinyint(3) DEFAULT NULL,
  `testHostId` varchar(255) DEFAULT NULL,
  `testStartTime` datetime DEFAULT NULL,
  `testEndTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_cframe`;
CREATE TABLE  `dbo`.`ubc_cframe` (
  `mgrId` varchar(10) NOT NULL,
  `templateId` varchar(128) NOT NULL,
  `frameId` varchar(128) NOT NULL,
  `grade` decimal(5,0) DEFAULT '2',
  `width` decimal(5,0) DEFAULT NULL,
  `height` decimal(5,0) DEFAULT NULL,
  `x` decimal(5,0) DEFAULT NULL,
  `y` decimal(5,0) DEFAULT NULL,
  `isPIP` decimal(1,0) DEFAULT '0',
  `borderStyle` varchar(50) DEFAULT 'solid',
  `borderThickness` decimal(5,0) DEFAULT '0',
  `borderColor` varchar(50) DEFAULT '#FFFFFF',
  `cornerRadius` decimal(5,0) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `comment1` varchar(255) DEFAULT NULL,
  `comment2` varchar(255) DEFAULT NULL,
  `comment3` varchar(255) DEFAULT NULL,
  `alpha` tinyint(3) DEFAULT '0',
  `isTV` tinyint(3) DEFAULT '0',
  `customer` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`templateId`,`frameId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_code`;
CREATE TABLE  `dbo`.`ubc_code` (
  `mgrId` varchar(255) NOT NULL,
  `codeId` varchar(255) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `enumString` varchar(255) NOT NULL,
  `enumNumber` decimal(10,0) NOT NULL,
  `dorder` tinyint(3) NOT NULL DEFAULT '0',
  `visible` tinyint(3) NOT NULL DEFAULT '1',
  `customer` varchar(45) NOT NULL,
  PRIMARY KEY (`codeId`,`categoryName`,`customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_connectionstatelog`;
CREATE TABLE  `dbo`.`ubc_connectionstatelog` (
  `mgrId` varchar(255) NOT NULL,
  `logId` varchar(255) NOT NULL,
  `eventTime` datetime DEFAULT NULL,
  `siteId` varchar(255) DEFAULT NULL,
  `hostId` varchar(255) DEFAULT NULL,
  `probableCause` varchar(255) DEFAULT NULL,
  `additionalText` varchar(255) DEFAULT NULL,
  `operationalState` decimal(1,0) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_contents`;
CREATE TABLE  `dbo`.`ubc_contents` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `programId` varchar(255) NOT NULL,
  `contentsId` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `promotionId` varchar(255) DEFAULT NULL,
  `contentsName` varchar(255) DEFAULT NULL,
  `contentsType` decimal(3,0) DEFAULT NULL,
  `parentsId` varchar(255) DEFAULT NULL,
  `registerId` varchar(255) DEFAULT NULL,
  `registerTime` datetime DEFAULT NULL,
  `isRaw` decimal(1,0) DEFAULT '0',
  `contentsState` decimal(3,0) DEFAULT '0',
  `contentsStateTime` datetime DEFAULT NULL,
  `contentsStateHistory` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `width` decimal(5,0) DEFAULT NULL,
  `height` decimal(5,0) DEFAULT NULL,
  `volume` decimal(10,0) DEFAULT NULL,
  `runningTime` decimal(10,0) DEFAULT '10',
  `verifier` varchar(255) DEFAULT NULL,
  `duration` decimal(5,0) DEFAULT NULL,
  `videoCodec` varchar(255) DEFAULT NULL,
  `audioCodec` varchar(255) DEFAULT NULL,
  `encodingInfo` varchar(255) DEFAULT NULL,
  `encodingTime` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `comment1` longtext,
  `comment2` longtext,
  `comment3` longtext,
  `comment4` longtext,
  `comment5` longtext,
  `comment6` longtext,
  `comment7` longtext,
  `comment8` longtext,
  `comment9` longtext,
  `comment10` longtext,
  `currentComment` decimal(5,0) DEFAULT '1',
  `bgColor` varchar(255) DEFAULT NULL,
  `fgColor` varchar(255) DEFAULT NULL,
  `font` varchar(255) DEFAULT NULL,
  `fontSize` decimal(5,0) DEFAULT NULL,
  `playSpeed` decimal(5,0) DEFAULT '1000',
  `soundVolume` decimal(5,0) DEFAULT '50',
  `promotionValueList` varchar(1024) DEFAULT NULL,
  `snapshotFile` varchar(255) DEFAULT NULL,
  `wizardFiles` longtext,
  `isPublic` decimal(1,0) DEFAULT '0',
  `tvInputType` decimal(10,0) DEFAULT '0',
  `tvChannel` varchar(45) DEFAULT NULL,
  `tvPortType` decimal(10,0) DEFAULT NULL,
  `direction` tinyint(3) DEFAULT '0',
  `align` tinyint(3) DEFAULT '5',
  `isUsed` tinyint(3) NOT NULL DEFAULT '1',
  `wizardXML` longtext,
  `contentsCategory` smallint(5) DEFAULT '0',
  `purpose` smallint(5) DEFAULT '0',
  `hostType` smallint(5) DEFAULT '0',
  `vertical` smallint(5) DEFAULT '0',
  `resolution` smallint(5) DEFAULT '0',
  `validationDate` datetime DEFAULT NULL,
  `requester` varchar(255) DEFAULT NULL,
  `verifyMembers` varchar(255) DEFAULT NULL,
  `verifyTime` datetime DEFAULT NULL,
  `registerType` varchar(10) DEFAULT NULL,
  `serialNo` decimal(10,0) DEFAULT NULL,
  `adminState` decimal(1,0) NOT NULL DEFAULT '1',
  PRIMARY KEY (`programId`,`contentsId`),
  UNIQUE KEY `UQ__ubc_cont__CBCE8D91316D4A39` (`programId`,`contentsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_ctemplate`;
CREATE TABLE  `dbo`.`ubc_ctemplate` (
  `mgrId` varchar(10) NOT NULL,
  `templateId` varchar(255) NOT NULL,
  `width` decimal(5,0) DEFAULT NULL,
  `height` decimal(5,0) DEFAULT NULL,
  `bgColor` varchar(255) DEFAULT '#c0c0c0',
  `description` varchar(255) DEFAULT NULL,
  `registerTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registerId` varchar(255) DEFAULT NULL,
  `customer` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`templateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_customer`;
CREATE TABLE  `dbo`.`ubc_customer` (
  `mgrId` varchar(50) NOT NULL,
  `customerId` varchar(255) NOT NULL,
  `url` longtext,
  `description` varchar(255) DEFAULT NULL,
  `gmt` int(10) NOT NULL DEFAULT '9999',
  `language` varchar(50) DEFAULT NULL,
  `copyUrl` longtext,
  PRIMARY KEY (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_defaultschedule`;
CREATE TABLE  `dbo`.`ubc_defaultschedule` (
  `mgrId` varchar(128) NOT NULL,
  `siteId` varchar(128) NOT NULL,
  `programId` varchar(128) NOT NULL,
  `templateId` varchar(128) NOT NULL,
  `frameId` varchar(128) NOT NULL,
  `scheduleId` varchar(128) NOT NULL,
  `requestId` varchar(255) DEFAULT NULL,
  `contentsId` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `playOrder` decimal(5,0) DEFAULT NULL,
  `openningFlag` decimal(1,0) DEFAULT '1',
  `timeScope` decimal(5,0) DEFAULT '0',
  `priority` decimal(5,0) DEFAULT '0',
  `castingState` decimal(3,0) DEFAULT NULL,
  `castingStateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `castingStateHistory` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `operationalState` decimal(1,0) DEFAULT '1',
  `adminState` decimal(1,0) DEFAULT '1',
  `phoneNumber` varchar(255) DEFAULT NULL,
  `comment1` varchar(255) DEFAULT NULL,
  `comment2` varchar(255) DEFAULT NULL,
  `comment3` varchar(255) DEFAULT NULL,
  `touchTime` varchar(45) DEFAULT NULL,
  `parentScheduleId` varchar(255) DEFAULT NULL,
  `serialNo` decimal(10,0) NOT NULL,
  `runningTime` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`serialNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_downloadstate`;
CREATE TABLE  `dbo`.`ubc_downloadstate` (
  `mgrId` varchar(255) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `downloadId` varchar(255) NOT NULL,
  `brwId` tinyint(3) DEFAULT '0',
  `programId` varchar(255) DEFAULT NULL,
  `contentsId` varchar(255) NOT NULL,
  `contentsName` varchar(255) DEFAULT NULL,
  `contentsType` decimal(3,0) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `volume` decimal(10,0) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `currentVolume` decimal(10,0) DEFAULT NULL,
  `result` tinyint(3) DEFAULT '0',
  `reason` decimal(5,0) DEFAULT NULL,
  `downState` tinyint(3) DEFAULT '0',
  `programState` tinyint(3) DEFAULT '0',
  `programStartTime` datetime DEFAULT NULL,
  `programEndTime` datetime DEFAULT NULL,
  `progress` varchar(10) DEFAULT '0/0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_downloadstatelog`;
CREATE TABLE  `dbo`.`ubc_downloadstatelog` (
  `mgrId` varchar(255) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `downloadId` varchar(255) NOT NULL,
  `brwId` tinyint(3) DEFAULT '0',
  `programId` varchar(255) DEFAULT NULL,
  `contentsId` varchar(255) NOT NULL,
  `contentsName` varchar(255) DEFAULT NULL,
  `contentsType` decimal(3,0) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `volume` decimal(10,0) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `currentVolume` decimal(10,0) DEFAULT NULL,
  `result` tinyint(3) DEFAULT '0',
  `reason` decimal(5,0) DEFAULT NULL,
  `downState` tinyint(3) DEFAULT '0',
  `programState` tinyint(3) DEFAULT '0',
  `programStartTime` datetime DEFAULT NULL,
  `programEndTime` datetime DEFAULT NULL,
  `progress` varchar(10) DEFAULT '0/0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_downloadstatesummary`;
CREATE TABLE  `dbo`.`ubc_downloadstatesummary` (
  `mgrId` varchar(255) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `downloadId` varchar(255) NOT NULL,
  `brwId` tinyint(3) DEFAULT '0',
  `programId` varchar(255) DEFAULT NULL,
  `programState` tinyint(3) DEFAULT '0',
  `programStartTime` datetime DEFAULT NULL,
  `programEndTime` datetime DEFAULT NULL,
  `progress` varchar(10) DEFAULT '0/0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_event`;
CREATE TABLE  `dbo`.`ubc_event` (
  `eventType` varchar(255) NOT NULL,
  `eventIndex` bigint(19) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) DEFAULT NULL,
  `entity` varchar(255) DEFAULT NULL,
  `eventTime` datetime NOT NULL,
  `eventBody` text,
  PRIMARY KEY (`eventIndex`)
) ENGINE=InnoDB AUTO_INCREMENT=430842 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_eventlog`;
CREATE TABLE  `dbo`.`ubc_eventlog` (
  `eventType` varchar(255) NOT NULL,
  `eventKey` varchar(255) DEFAULT NULL,
  `eventTime` datetime NOT NULL,
  `eventBody` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_fault`;
CREATE TABLE  `dbo`.`ubc_fault` (
  `mgrId` varchar(255) NOT NULL,
  `faultId` varchar(255) NOT NULL,
  `eventTime` datetime DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `severity` decimal(3,0) DEFAULT NULL,
  `probableCause` varchar(255) DEFAULT NULL,
  `additionalText` varchar(255) DEFAULT NULL,
  `counter` int(10) DEFAULT '1',
  `updateTime` datetime DEFAULT NULL,
  `siteId` varchar(255) DEFAULT NULL,
  `hostId` varchar(235) DEFAULT NULL,
  PRIMARY KEY (`faultId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_faultlog`;
CREATE TABLE  `dbo`.`ubc_faultlog` (
  `mgrId` varchar(255) NOT NULL,
  `faultId` varchar(255) NOT NULL,
  `eventTime` datetime DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `severity` decimal(3,0) DEFAULT NULL,
  `probableCause` varchar(255) DEFAULT NULL,
  `additionalText` varchar(255) DEFAULT NULL,
  `counter` int(10) DEFAULT '1',
  `updateTime` datetime DEFAULT NULL,
  `siteId` varchar(255) DEFAULT NULL,
  `hostId` varchar(235) DEFAULT NULL,
  PRIMARY KEY (`faultId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_fmo`;
CREATE TABLE  `dbo`.`ubc_fmo` (
  `mgrId` varchar(255) NOT NULL,
  `fmId` varchar(255) NOT NULL,
  `duration` int(10) DEFAULT '7',
  `probableCauseList` longtext,
  `excludeSource` longtext,
  `includeSource` longtext,
  PRIMARY KEY (`fmId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_frame`;
CREATE TABLE  `dbo`.`ubc_frame` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(128) NOT NULL,
  `programId` varchar(128) NOT NULL,
  `templateId` varchar(128) NOT NULL,
  `frameId` varchar(128) NOT NULL,
  `grade` decimal(5,0) DEFAULT '2',
  `width` decimal(5,0) DEFAULT NULL,
  `height` decimal(5,0) DEFAULT NULL,
  `x` decimal(5,0) DEFAULT NULL,
  `y` decimal(5,0) DEFAULT NULL,
  `isPIP` decimal(1,0) DEFAULT '0',
  `borderStyle` varchar(50) DEFAULT 'solid',
  `borderThickness` decimal(5,0) DEFAULT '0',
  `borderColor` varchar(50) DEFAULT '#FFFFFF',
  `cornerRadius` decimal(5,0) DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `comment1` varchar(255) DEFAULT NULL,
  `comment2` varchar(255) DEFAULT NULL,
  `comment3` varchar(255) DEFAULT NULL,
  `serialNo` decimal(10,0) DEFAULT NULL,
  `zindex` tinyint(3) DEFAULT '0',
  `alpha` tinyint(3) DEFAULT '0',
  `isTV` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`siteId`,`programId`,`templateId`,`frameId`) USING BTREE,
  UNIQUE KEY `UQ__ubc_fram__DA8F15D0381A47C8` (`programId`,`templateId`,`frameId`),
  UNIQUE KEY `UQ__ubc_fram__DA8F15D056D3D912` (`programId`,`templateId`,`frameId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_host`;
CREATE TABLE  `dbo`.`ubc_host` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `hostName` varchar(255) DEFAULT NULL,
  `domainName` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `serialNo` decimal(10,0) DEFAULT NULL,
  `period` decimal(5,0) DEFAULT '60',
  `asOperator` varchar(255) DEFAULT NULL,
  `asOperatorTel` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `soundVolume` decimal(5,0) DEFAULT NULL,
  `mute` decimal(1,0) DEFAULT NULL,
  `resolution` varchar(50) DEFAULT NULL,
  `defaultTemplate` varchar(50) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `adminState` decimal(1,0) DEFAULT '1',
  `operationalState` tinyint(3) DEFAULT '1',
  `autoPowerFlag` decimal(1,0) DEFAULT '0',
  `startupTime` varchar(255) DEFAULT NULL,
  `shutdownTime` varchar(255) DEFAULT NULL,
  `powerControl` decimal(5,0) DEFAULT '0',
  `amtPassword` varchar(50) DEFAULT NULL,
  `macAddress` varchar(50) DEFAULT NULL,
  `wolPort` decimal(5,0) DEFAULT NULL,
  `edition` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `lastUpdateTime` datetime DEFAULT NULL,
  `cpuUsed` double DEFAULT '0',
  `realMemoryUsed` decimal(10,0) DEFAULT NULL,
  `realMemoryTotal` decimal(10,0) DEFAULT NULL,
  `virtualMemoryUsed` decimal(10,0) DEFAULT NULL,
  `virtualMemoryTotal` decimal(10,0) DEFAULT NULL,
  `fileSystem` varchar(1024) DEFAULT NULL,
  `vncPort` decimal(10,0) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `addr1` varchar(1024) DEFAULT NULL,
  `addr2` varchar(255) DEFAULT NULL,
  `addr3` varchar(255) DEFAULT NULL,
  `addr4` varchar(255) DEFAULT NULL,
  `templatePlayList` varchar(2048) DEFAULT NULL,
  `lastScreenshotFile` varchar(255) DEFAULT NULL,
  `screenshotPeriod` decimal(5,0) DEFAULT '5',
  `sosId` varchar(255) DEFAULT NULL,
  `templatePlayList3` longtext,
  `authDate` datetime DEFAULT NULL,
  `displayCounter` decimal(5,0) DEFAULT '1',
  `autoSchedule1` varchar(255) DEFAULT NULL,
  `autoSchedule2` varchar(255) DEFAULT NULL,
  `currentSchedule1` varchar(255) DEFAULT NULL,
  `currentSchedule2` varchar(255) DEFAULT NULL,
  `lastSchedule1` varchar(255) DEFAULT NULL,
  `lastSchedule2` varchar(255) DEFAULT NULL,
  `lastScheduleTime1` datetime DEFAULT NULL,
  `lastScheduleTime2` datetime DEFAULT NULL,
  `networkUse1` tinyint(3) NOT NULL DEFAULT '1',
  `networkUse2` tinyint(3) NOT NULL DEFAULT '1',
  `scheduleApplied1` tinyint(3) NOT NULL DEFAULT '1',
  `scheduleApplied2` tinyint(3) NOT NULL DEFAULT '1',
  `holiday` varchar(255) DEFAULT NULL,
  `bootUpTime` datetime DEFAULT NULL,
  `reservedScheduleList1` longtext,
  `reservedScheduleList2` longtext,
  `soundDisplay` tinyint(3) NOT NULL DEFAULT '1',
  `vncState` tinyint(3) NOT NULL DEFAULT '0',
  `cpuTemp` tinyint(3) NOT NULL DEFAULT '0',
  `createTime` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `autoUpdateFlag` tinyint(3) DEFAULT '1',
  `playLogDayLimit` decimal(10,0) DEFAULT '15',
  `hddThreshold` tinyint(3) NOT NULL DEFAULT '90',
  `playLogDayCriteria` varchar(10) NOT NULL DEFAULT '00:00',
  `baseSchedule1` varchar(255) DEFAULT NULL,
  `baseSchedule2` varchar(255) DEFAULT NULL,
  `renderMode` tinyint(3) NOT NULL DEFAULT '1',
  `category` varchar(255) DEFAULT NULL,
  `contentsDownloadTime` varchar(10) DEFAULT NULL,
  `starterState` tinyint(3) NOT NULL DEFAULT '0',
  `browserState` tinyint(3) NOT NULL DEFAULT '0',
  `browser1State` tinyint(3) NOT NULL DEFAULT '0',
  `firmwareViewState` tinyint(3) NOT NULL DEFAULT '0',
  `preDownloaderState` tinyint(3) NOT NULL DEFAULT '0',
  `bootDownTime` datetime DEFAULT NULL,
  `download1State` tinyint(3) NOT NULL DEFAULT '0',
  `download2State` tinyint(3) NOT NULL DEFAULT '0',
  `progress1` varchar(10) NOT NULL DEFAULT '0/0',
  `progress2` varchar(10) NOT NULL DEFAULT '0/0',
  `monitoroff` tinyint(3) NOT NULL DEFAULT '0',
  `monitoroffList` longtext,
  `hostType` smallint(5) NOT NULL DEFAULT '0',
  `mmsCount` varchar(45) DEFAULT NULL,
  `monitorState` int(10) DEFAULT '-1',
  `winPassword` varchar(50) DEFAULT NULL,
  `monitorUseTime` int(10) NOT NULL DEFAULT '-1',
  `downloadGroup` varchar(255) DEFAULT NULL,
  `gmt` int(10) NOT NULL DEFAULT '9999',
  `weekShutdownTime` varchar(100) DEFAULT NULL,
  `hostMsg1` varchar(255) DEFAULT NULL,
  `hostMsg2` varchar(255) DEFAULT NULL,
  `disk1Avail` decimal(10,2) NOT NULL DEFAULT '-1.00',
  `disk2Avail` decimal(10,2) NOT NULL DEFAULT '-1.00',
  `protocolType` varchar(255) NOT NULL DEFAULT 'C',
  `monitorType` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`hostId`,`siteId`),
  UNIQUE KEY `UQ__ubc_host__97B6C6053BEAD8AC` (`siteId`,`hostId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_interactivelog`;
CREATE TABLE  `dbo`.`ubc_interactivelog` (
  `playDate` datetime NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `programId` varchar(255) NOT NULL,
  `contentsId` varchar(255) NOT NULL,
  `keyword1` varchar(255) DEFAULT NULL,
  `keyword2` varchar(255) DEFAULT NULL,
  `keyword3` varchar(255) DEFAULT NULL,
  `counter` int(10) NOT NULL DEFAULT '0',
  `keyword4` varchar(255) DEFAULT NULL,
  `keyword5` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_connectiondailystat`;
CREATE TABLE  `dbo`.`ubc_connectiondailystat` (
  `statDate` datetime NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `disconnectCount` bigint(20) DEFAULT NULL,
  `disconnectSec` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_announceid`;
CREATE TABLE  `dbo`.`ubc_key_announceid` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_bpid`;
CREATE TABLE  `dbo`.`ubc_key_bpid` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_downloadid`;
CREATE TABLE  `dbo`.`ubc_key_downloadid` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_faultid`;
CREATE TABLE  `dbo`.`ubc_key_faultid` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_hostautoupdateid`;
CREATE TABLE  `dbo`.`ubc_key_hostautoupdateid` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_id`;
CREATE TABLE  `dbo`.`ubc_key_id` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_reservationid`;
CREATE TABLE  `dbo`.`ubc_key_reservationid` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_thid`;
CREATE TABLE  `dbo`.`ubc_key_thid` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_tpid`;
CREATE TABLE  `dbo`.`ubc_key_tpid` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_key_zorder`;
CREATE TABLE  `dbo`.`ubc_key_zorder` (
  `keyval` decimal(10,0) NOT NULL,
  PRIMARY KEY (`keyval`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_lastscreenshot`;
CREATE TABLE  `dbo`.`ubc_lastscreenshot` (
  `hostId` varchar(255) NOT NULL,
  `fullstring` varchar(255) NOT NULL,
  PRIMARY KEY (`hostId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_lmo`;
CREATE TABLE  `dbo`.`ubc_lmo` (
  `mgrId` varchar(10) NOT NULL,
  `lmId` varchar(255) NOT NULL,
  `duration` decimal(5,0) DEFAULT '3',
  `powerState` decimal(1,0) DEFAULT '0',
  `connectionState` decimal(1,0) DEFAULT '0',
  `scheduleState` decimal(1,0) DEFAULT '0',
  `faultState` decimal(1,0) DEFAULT '0',
  `loginState` decimal(1,0) DEFAULT '0',
  `estimateState` decimal(1,0) DEFAULT '0',
  `downloadState` decimal(1,0) DEFAULT '0',
  PRIMARY KEY (`lmId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_lmo_solution`;
CREATE TABLE  `dbo`.`ubc_lmo_solution` (
  `mgrId` varchar(255) NOT NULL DEFAULT '',
  `lmId` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tableName` varchar(255) DEFAULT NULL,
  `timeFieldName` varchar(255) DEFAULT NULL,
  `logType` decimal(1,0) DEFAULT '0',
  `duration` decimal(5,0) DEFAULT '3',
  `adminState` decimal(1,0) DEFAULT '0',
  PRIMARY KEY (`lmId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_menuauth`;
CREATE TABLE  `dbo`.`ubc_menuauth` (
  `mgrId` varchar(255) NOT NULL,
  `menuId` varchar(255) NOT NULL,
  `menuName` varchar(255) NOT NULL,
  `menuType` varchar(10) NOT NULL DEFAULT 'S',
  `cAuth` tinyint(3) NOT NULL DEFAULT '0',
  `rAuth` tinyint(3) NOT NULL DEFAULT '0',
  `uAuth` tinyint(3) NOT NULL DEFAULT '0',
  `dAuth` tinyint(3) NOT NULL DEFAULT '0',
  `aAuth` tinyint(3) NOT NULL DEFAULT '0',
  `customer` varchar(45) NOT NULL,
  PRIMARY KEY (`menuId`,`customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_mir`;
CREATE TABLE  `dbo`.`ubc_mir` (
  `classScopedName` varchar(255) NOT NULL,
  `attrName` varchar(255) NOT NULL,
  `attrType` varchar(255) NOT NULL,
  `className` varchar(255) DEFAULT NULL,
  `indexOrder` int(10) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`classScopedName`,`attrName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_playlog`;
CREATE TABLE  `dbo`.`ubc_playlog` (
  `playDate` datetime NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `programId` varchar(255) NOT NULL,
  `contentsId` varchar(255) NOT NULL,
  `contentsName` varchar(255) DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `playCount` decimal(10,0) NOT NULL DEFAULT '0',
  `playTime` decimal(10,0) NOT NULL DEFAULT '0',
  `planTime` decimal(10,0) NOT NULL DEFAULT '0',
  `failCount` decimal(10,0) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_pmo`;
CREATE TABLE  `dbo`.`ubc_pmo` (
  `mgrId` varchar(255) NOT NULL,
  `pmId` varchar(255) NOT NULL,
  `ipAddress` varchar(255) NOT NULL,
  `ftpId` varchar(255) DEFAULT NULL,
  `ftpPasswd` varchar(255) DEFAULT NULL,
  `ftpPort` decimal(10,0) NOT NULL DEFAULT '21',
  `socketPort` decimal(10,0) DEFAULT NULL,
  `ipAddress1` varchar(45) DEFAULT NULL,
  `ipAddress2` varchar(45) DEFAULT NULL,
  `ftpAddress` varchar(45) DEFAULT NULL,
  `ftpAddress1` varchar(45) DEFAULT NULL,
  `ftpAddress2` varchar(45) DEFAULT NULL,
  `ftpMirror` varchar(45) DEFAULT NULL,
  `ftpMirror1` varchar(45) DEFAULT NULL,
  `ftpMirror2` varchar(45) DEFAULT NULL,
  `currentConnectionCount` int(10) DEFAULT '-1',
  `lastUpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `adminstate` tinyint(3) DEFAULT '1',
  PRIMARY KEY (`pmId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_powerstatelog`;
CREATE TABLE  `dbo`.`ubc_powerstatelog` (
  `mgrId` varchar(10) NOT NULL,
  `logId` varchar(255) NOT NULL,
  `eventTime` datetime DEFAULT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `probableCause` varchar(255) DEFAULT NULL,
  `additionalText` varchar(255) DEFAULT NULL,
  `bootUpTime` datetime NOT NULL,
  `bootDownTime` datetime NOT NULL,
  PRIMARY KEY (`siteId`,`hostId`,`bootUpTime`,`bootDownTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_program`;
CREATE TABLE  `dbo`.`ubc_program` (
  `mgrId` varchar(255) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `programId` varchar(255) NOT NULL,
  `lastUpdateTime` datetime DEFAULT NULL,
  `lastUpdateId` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `templatePlayList` longtext,
  `serialNo` decimal(10,0) DEFAULT NULL,
  `contentsCategory` smallint(5) DEFAULT '0',
  `purpose` smallint(5) DEFAULT '0',
  `hostType` smallint(5) DEFAULT '0',
  `vertical` smallint(5) DEFAULT '0',
  `resolution` smallint(5) DEFAULT '0',
  `isPublic` smallint(5) DEFAULT '0',
  `validationDate` datetime DEFAULT NULL,
  `isVerify` smallint(5) DEFAULT '1',
  `volume` decimal(18,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`siteId`,`programId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_programlocation`;
CREATE TABLE  `dbo`.`ubc_programlocation` (
  `mgrId` varchar(255) NOT NULL,
  `programLocationId` varchar(255) NOT NULL,
  `programId` varchar(255) NOT NULL,
  `pmId` varchar(255) NOT NULL,
  PRIMARY KEY (`programLocationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_reservation`;
CREATE TABLE  `dbo`.`ubc_reservation` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `hostId` varchar(255) NOT NULL,
  `reservationId` varchar(255) NOT NULL,
  `programId` varchar(255) DEFAULT NULL,
  `side` tinyint(3) DEFAULT NULL,
  `fromTime` datetime DEFAULT NULL,
  `toTime` datetime DEFAULT NULL,
  `tpEntity` varchar(255) DEFAULT NULL,
  `thEntity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`siteId`,`hostId`,`reservationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_role`;
CREATE TABLE  `dbo`.`ubc_role` (
  `mgrId` varchar(255) NOT NULL,
  `roleId` varchar(255) NOT NULL,
  `roleName` varchar(255) NOT NULL,
  `roleDesc` varchar(255) DEFAULT NULL,
  `customer` varchar(45) NOT NULL,
  PRIMARY KEY (`roleId`,`customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_rolemap`;
CREATE TABLE  `dbo`.`ubc_rolemap` (
  `mgrId` varchar(255) NOT NULL,
  `rolemapId` varchar(255) NOT NULL,
  `roleId` varchar(255) NOT NULL,
  `menuId` varchar(255) NOT NULL,
  `menuType` varchar(10) NOT NULL DEFAULT 'S',
  `customer` varchar(45) NOT NULL,
  PRIMARY KEY (`rolemapId`,`customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_schedule`;
CREATE TABLE  `dbo`.`ubc_schedule` (
  `mgrId` varchar(128) NOT NULL,
  `siteId` varchar(128) NOT NULL,
  `programId` varchar(128) NOT NULL,
  `templateId` varchar(128) NOT NULL,
  `frameId` varchar(128) NOT NULL,
  `scheduleId` varchar(128) NOT NULL,
  `requestId` varchar(255) DEFAULT NULL,
  `contentsId` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `startTime` varchar(50) DEFAULT NULL,
  `endTime` varchar(50) DEFAULT NULL,
  `toTime` datetime DEFAULT NULL,
  `fromTime` datetime DEFAULT NULL,
  `openningFlag` decimal(1,0) DEFAULT '1',
  `priority` decimal(5,0) DEFAULT '0',
  `castingState` decimal(3,0) DEFAULT '0',
  `castingStateTime` datetime DEFAULT NULL,
  `castingStateHistory` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `operationalState` decimal(1,0) DEFAULT '1',
  `adminState` decimal(1,0) DEFAULT '1',
  `phoneNumber` varchar(255) DEFAULT NULL,
  `isOrigin` decimal(1,0) DEFAULT '1',
  `comment1` varchar(255) DEFAULT NULL,
  `comment2` varchar(255) DEFAULT NULL,
  `comment3` varchar(255) DEFAULT NULL,
  `serialNo` decimal(10,0) DEFAULT NULL,
  `runningTime` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`programId`,`templateId`,`frameId`,`scheduleId`),
  UNIQUE KEY `UQ__ubc_sche__07D546FE4D1564AE` (`programId`,`templateId`,`frameId`,`scheduleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_schedulestatelog`;
CREATE TABLE  `dbo`.`ubc_schedulestatelog` (
  `mgrId` varchar(255) NOT NULL,
  `logId` varchar(255) NOT NULL,
  `eventTime` datetime DEFAULT NULL,
  `siteId` varchar(255) DEFAULT NULL,
  `hostId` varchar(255) DEFAULT NULL,
  `probableCause` varchar(255) DEFAULT NULL,
  `additionalText` varchar(255) DEFAULT NULL,
  `autoSchedule1` varchar(255) DEFAULT NULL,
  `autoSchedule2` varchar(255) DEFAULT NULL,
  `currentSchedule1` varchar(255) DEFAULT NULL,
  `currentSchedule2` varchar(255) DEFAULT NULL,
  `lastSchedule1` varchar(255) DEFAULT NULL,
  `lastSchedule2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_screenshot`;
CREATE TABLE  `dbo`.`ubc_screenshot` (
  `hostId` varchar(255) NOT NULL,
  `createTime` varchar(20) NOT NULL,
  `fileName` varchar(255) NOT NULL,
  `url` varchar(45) NOT NULL,
  PRIMARY KEY (`fileName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_site`;
CREATE TABLE  `dbo`.`ubc_site` (
  `mgrId` varchar(255) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `siteName` varchar(255) DEFAULT NULL,
  `phoneNo1` varchar(255) DEFAULT NULL,
  `phoneNo2` varchar(255) DEFAULT NULL,
  `mobileNo` varchar(255) DEFAULT NULL,
  `faxNo` varchar(255) DEFAULT NULL,
  `franchizeType` varchar(255) NOT NULL,
  `chainNo` varchar(255) DEFAULT NULL,
  `businessType` varchar(255) DEFAULT NULL,
  `businessCode` varchar(255) DEFAULT NULL,
  `ceoName` varchar(255) DEFAULT NULL,
  `siteLevel` decimal(5,0) DEFAULT NULL,
  `shopOpenTime` varchar(255) DEFAULT NULL,
  `shopCloseTime` varchar(255) DEFAULT NULL,
  `holiday` varchar(255) DEFAULT NULL,
  `comment1` varchar(255) DEFAULT NULL,
  `comment2` varchar(255) DEFAULT NULL,
  `comment3` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `addr1` varchar(255) DEFAULT NULL,
  `addr2` varchar(255) DEFAULT NULL,
  `addr3` varchar(255) DEFAULT NULL,
  `addr4` varchar(255) DEFAULT NULL,
  `parentId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`siteId`,`franchizeType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_smtplog`;
CREATE TABLE  `dbo`.`ubc_smtplog` (
  `mailid` bigint(19) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) DEFAULT NULL,
  `send_email` varchar(255) DEFAULT NULL,
  `receive_email` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `contents` longtext,
  `success_yn` char(1) DEFAULT NULL,
  `wdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mailid`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_template`;
CREATE TABLE  `dbo`.`ubc_template` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `programId` varchar(255) NOT NULL,
  `templateId` varchar(255) NOT NULL,
  `width` decimal(5,0) DEFAULT NULL,
  `height` decimal(5,0) DEFAULT NULL,
  `bgColor` varchar(255) DEFAULT '#c0c0c0',
  `bgImage` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `shortCut` varchar(45) DEFAULT NULL,
  `registerTime` timestamp NULL DEFAULT NULL,
  `serialNo` decimal(10,0) DEFAULT NULL,
  `bgType` int(10) DEFAULT NULL,
  `bgSerialNo` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`siteId`,`programId`,`templateId`),
  UNIQUE KEY `UQ__ubc_temp__FEB91C9A50E5F592` (`programId`,`templateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_th`;
CREATE TABLE  `dbo`.`ubc_th` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `bpId` varchar(255) NOT NULL,
  `thId` varchar(255) NOT NULL,
  `targetHost` varchar(255) NOT NULL,
  `side` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`siteId`,`bpId`,`thId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_tp`;
CREATE TABLE  `dbo`.`ubc_tp` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `bpId` varchar(255) NOT NULL,
  `tpId` varchar(255) NOT NULL,
  `createTime` datetime DEFAULT NULL,
  `startTime` varchar(20) DEFAULT NULL,
  `endTime` varchar(20) DEFAULT NULL,
  `programId` varchar(255) DEFAULT NULL,
  `zorder` varchar(128) DEFAULT '0',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tpId`),
  UNIQUE KEY `UQ__ubc_tp__A5464DC354B68676` (`siteId`,`bpId`,`tpId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_user`;
CREATE TABLE  `dbo`.`ubc_user` (
  `mgrId` varchar(10) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `userId` varchar(255) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `sid` varchar(50) DEFAULT NULL,
  `mobileNo` varchar(50) DEFAULT NULL,
  `phoneNo` varchar(50) DEFAULT NULL,
  `zipCode` varchar(50) DEFAULT NULL,
  `addr1` varchar(255) DEFAULT NULL,
  `addr2` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `registerTime` datetime DEFAULT NULL,
  `userType` decimal(5,0) DEFAULT '3',
  `siteList` longtext,
  `hostList` longtext,
  `probableCauseList` longtext,
  `useEmail` tinyint(3) NOT NULL DEFAULT '0',
  `useSms` tinyint(3) NOT NULL DEFAULT '0',
  `roleId` varchar(255) DEFAULT NULL,
  `validationDate` datetime DEFAULT NULL,
  `lastLoginTime` datetime DEFAULT NULL,
  `lastPWChangeTime` datetime DEFAULT NULL,
  `loginFailCount` tinyint(3) DEFAULT '0',
  `adminstate` tinyint(3) DEFAULT '1',
  PRIMARY KEY (`siteId`,`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_userlog`;
CREATE TABLE  `dbo`.`ubc_userlog` (
  `mgrId` varchar(255) NOT NULL,
  `siteId` varchar(255) NOT NULL,
  `userId` varchar(255) NOT NULL,
  `userLogId` varchar(255) DEFAULT NULL,
  `loginTime` datetime DEFAULT NULL,
  `via` varchar(50) DEFAULT NULL,
  `result` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_web_board`;
CREATE TABLE  `dbo`.`ubc_web_board` (
  `boardId` bigint(19) NOT NULL AUTO_INCREMENT,
  `boardMasterId` int(10) DEFAULT NULL,
  `thread` int(10) DEFAULT NULL,
  `depth` int(10) DEFAULT NULL,
  `userId` varchar(100) DEFAULT NULL,
  `title` text,
  `contents` longtext,
  `contentCd` varchar(10) DEFAULT NULL,
  `topYn` tinyint(4) DEFAULT NULL,
  `popYn` tinyint(4) DEFAULT NULL,
  `readCount` int(10) DEFAULT NULL,
  `useYn` tinyint(4) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `compType` varchar(1) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`boardId`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_web_boardattach`;
CREATE TABLE  `dbo`.`ubc_web_boardattach` (
  `attachId` int(10) NOT NULL AUTO_INCREMENT,
  `boardId` bigint(19) DEFAULT NULL,
  `filePath` varchar(200) DEFAULT NULL,
  `fileName` varchar(500) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  PRIMARY KEY (`attachId`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_web_menu_large`;
CREATE TABLE  `dbo`.`ubc_web_menu_large` (
  `COMP_CODE` varchar(20) NOT NULL,
  `LARGE_CODE` varchar(20) NOT NULL,
  `MENU_NAME` varchar(100) DEFAULT NULL,
  `PROG_TYPE` varchar(20) DEFAULT NULL,
  `SORT_NUM` int(10) DEFAULT NULL,
  `PROG_ID` varchar(100) DEFAULT NULL,
  `USE_FLAG` varchar(20) DEFAULT NULL,
  `VISIBLE_FLAG` varchar(1) DEFAULT NULL,
  `LANG_FLAG` varchar(2) NOT NULL,
  `USER_TYPE` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `dbo`.`ubc_web_menu_middle`;
CREATE TABLE  `dbo`.`ubc_web_menu_middle` (
  `COMP_CODE` varchar(20) NOT NULL,
  `MIDDLE_CODE` varchar(20) NOT NULL,
  `MENU_NAME` varchar(100) DEFAULT NULL,
  `PROG_TYPE` varchar(20) DEFAULT NULL,
  `SORT_NUM` decimal(3,0) DEFAULT NULL,
  `PROG_ID` varchar(100) DEFAULT NULL,
  `LARGE_CODE` varchar(20) DEFAULT NULL,
  `USE_FLAG` varchar(20) DEFAULT NULL,
  `VISIBLE_FLAG` varchar(1) DEFAULT NULL,
  `LANG_FLAG` varchar(2) NOT NULL,
  `USER_TYPE` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP VIEW IF EXISTS `dbo`.`ubc_hostview`;
CREATE VIEW dbo.ubc_hostView AS SELECT h.*, s.siteName,s.chainNo,s.businessCode,s.businessType, s.phoneNo1 from (dbo.ubc_host h left join dbo.ubc_site s on((h.siteId = s.siteId)));

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`Find_n_Right`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`Find_n_Right`(findStr varchar(255), fullStr varchar(255)) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare resultStr varchar(255);
  select RIGHT(fullStr, LENGTH(fullStr) - (LOCATE(findStr, fullStr)+LENGTH(findStr)-1)) into resultStr from dual;
  return (resultStr);
END;

 $$

DELIMITER ;

DELIMITER $$

DROP PROCEDURE IF EXISTS `dbo`.`GetChildSite`$$
CREATE DEFINER=`ubc`@`%` PROCEDURE  `dbo`.`GetChildSite`(inFranchizeType varchar(255), inSiteId varchar(255))
BEGIN

  /*
   * 임시 테이블 생성
   */
  prepare stmt FROM "CREATE TEMPORARY TABLE IF NOT EXISTS dbo.TempChildSite(mgrId varchar(255), siteName varchar(255), siteId varchar(255), parentId varchar(255), lvl 

INT, name_path varchar(255), id_path varchar(255), franchizeType varchar(255))";
  execute stmt;
  deallocate prepare stmt;

  /*
   * 임시 테이블 청소
   */
  TRUNCATE dbo.TempChildSite;
  
  /*
   * 임시 테이블에 조건에 맞는 데이터 인서트
   */
  INSERT INTO dbo.TempChildSite
  SELECT mgrId, siteName, siteId, parentId, 1, CONCAT('/', siteName), CONCAT('/', siteId), inFranchizeType 
  FROM dbo.ubc_site 
  WHERE franchizeType = inFranchizeType 
  AND siteId = inSiteId;

  /*
   * 입력된 데이터의 자식 데이터를 넣기 위해 프로시져 호출
   */
  CALL dbo.GetChildSiteRecursive(2, inFranchizeType, inSiteId, CONCAT('/', dbo.GetSiteName(inSiteId)), CONCAT('/', inSiteId));

END $$

DELIMITER ;

DELIMITER $$

DROP PROCEDURE IF EXISTS `dbo`.`GetChildSiteRecursive`$$
CREATE DEFINER=`ubc`@`%` PROCEDURE  `dbo`.`GetChildSiteRecursive`(level INT, inFranchizeType varchar(255), inParentId varchar(255), inNamePath varchar(8000), inIdPath varchar(8000))
BEGIN

  /*
   * 변수 선언
   */
  DECLARE done INT DEFAULT 0;
  DECLARE _siteName varchar(255);
  DECLARE _siteId varchar(255);
  DECLARE _level INT DEFAULT 0;
  DECLARE _id_path varchar(255);
  DECLARE _name_path varchar(255);

  /*
   * 커서 선언
   */
  DECLARE cur1 CURSOR FOR SELECT siteName, siteId, lvl, name_path, id_path from dbo.TempChildSite where franchizeType = inFranchizeType and parentId = inParentId;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

  /*
   * 입력된 데이터의 자식 데이터를 넣기 위해 프로시져 호출
   */
  INSERT INTO dbo.TempChildSite
  SELECT mgrId, siteName, siteId, parentId, level, CONCAT(inNamePath, CONCAT('/', siteName)), CONCAT(inIdPath, CONCAT('/', siteId)), inFranchizeType 
  FROM dbo.ubc_site 
  WHERE franchizeType = inFranchizeType 
  AND parentId = inParentId;
 
  /*
   * 패치 상태를 위한 변수 초기화
   */
  SET done = 0;
  
  /*
   * DB open
   */
  OPEN cur1;

  /*
   * 입력된 데이터의 자식 데이터를 넣기 위해 한건시 패치 후 프로시져 호출
   */
  REPEAT
    FETCH cur1 INTO _siteName, _siteId, _level, _name_path, _id_path;
    
    IF NOT done THEN
      CALL dbo.GetChildSiteRecursive(_level + 1, inFranchizeType, _siteId, _name_path, _id_path);
    END IF;
    
  UNTIL done END REPEAT;
  
  /*
   * DB cursor close
   */
  CLOSE cur1;
  
END $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`GetClassValue`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`GetClassValue`(inString varchar(255), idx int) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare resultString nvarchar(255);
  declare tempString nvarchar(255);
  declare oPos int;
  declare nPos int;
  declare curIndex int;

  set oPos = 1; 
  set nPos = 1; 
  set curIndex = 1;

  WHILE (nPos > 0) DO
  
    set nPos = LOCATE('/', inString, oPos);
    
    if nPos = 0 then
      set tempString = right(inString, length(inString) - oPos + 1);
    else
      set tempString = substring(inString, oPos, nPos - oPos);
    end if;
    
    if idx = curIndex then
      set resultString = tempString;
    end if;
    
    set curIndex = curIndex + 1;
    set oPos = nPos + 1;
  end while;

  if length(resultString) > 0 then
    set resultString = right(resultString, length(resultString) - (LOCATE('=', resultString)));
  end if;

  return resultString;
END;

 $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`GetContentsName`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`GetContentsName`(inId varchar(255)) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare outName varchar(255);
  select contentsName into outName from dbo.ubc_contents where contentsId = inId;
  return (outName);
END;

 $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`GetHostName`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`GetHostName`(inId varchar(255)) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare outName varchar(255);
  select hostName into outName from dbo.ubc_host where hostId = inId;
  return (outName);
END;

 $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`GetSiteName`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`GetSiteName`(inId varchar(255)) RETURNS varchar(255) CHARSET utf8
BEGIN
  declare outName varchar(255);
  select siteName into outName from dbo.ubc_site where siteId = inId;
  return (outName);
END;

 $$

DELIMITER ;


DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`Password_Decode`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`Password_Decode`(input varchar(255)) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
	DECLARE ret varchar(255) DEFAULT '';
	DECLARE done TINYINT DEFAULT 0;

	IF input IS NULL or LENGTH(input) = 0 THEN
		RETURN NULL;
	END IF;

each_block:
	WHILE NOT done DO BEGIN
		DECLARE accum_value BIGINT UNSIGNED DEFAULT 0;
		DECLARE in_count TINYINT DEFAULT 0;
		DECLARE out_count TINYINT DEFAULT 3;

each_input_char:
		WHILE in_count < 4 DO BEGIN
			DECLARE first_char CHAR(1);
	
			IF LENGTH(input) = 0 THEN
				RETURN ret;
			END IF;
	
			SET first_char = SUBSTRING(input,1,1);
			SET input = SUBSTRING(input,2);
	
			BEGIN
				DECLARE tempval TINYINT UNSIGNED;
				DECLARE error TINYINT DEFAULT 0;
				DECLARE base64_getval CURSOR FOR SELECT val FROM dbo.base64_data WHERE c = first_char;
				DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET error = 1;
	
				OPEN base64_getval;
				FETCH base64_getval INTO tempval;
				CLOSE base64_getval;

				IF error THEN
					ITERATE each_input_char;
				END IF;

				SET accum_value = (accum_value << 6) + tempval;
			END;

			SET in_count = in_count + 1;

			IF first_char = '=' THEN
				SET done = 1;
				SET out_count = out_count - 1;
			END IF;
		END; END WHILE;
		WHILE out_count > 0 DO BEGIN
			SET ret = CONCAT(ret,CHAR((accum_value & 0xff0000) >> 16));
			SET out_count = out_count - 1;
			SET accum_value = (accum_value << 8) & 0xffffff;
		END; END WHILE;
	
	END; END WHILE;

	RETURN ret;
END;

 $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `dbo`.`Password_Encode`$$
CREATE DEFINER=`ubc`@`%` FUNCTION  `dbo`.`Password_Encode`(input varchar(255)) RETURNS varchar(255) CHARSET utf8
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
	DECLARE ret varchar(255) DEFAULT '';
	DECLARE done TINYINT DEFAULT 0;

	IF input IS NULL or LENGTH(input) = 0 THEN
		RETURN NULL;
	END IF;

each_block:
	WHILE NOT done DO BEGIN
		DECLARE accum_value BIGINT UNSIGNED DEFAULT 0;
		DECLARE in_count TINYINT DEFAULT 0;
		DECLARE out_count TINYINT;

each_input_char:
		WHILE in_count < 3 DO BEGIN
			DECLARE first_char CHAR(1);
	
			IF LENGTH(input) = 0 THEN
				SET done = 1;
				SET accum_value = accum_value << (8 * (3 - in_count));
				LEAVE each_input_char;
			END IF;
	
			SET first_char = SUBSTRING(input,1,1);
			SET input = SUBSTRING(input,2);
	
			SET accum_value = (accum_value << 8) + ASCII(first_char);

			SET in_count = in_count + 1;
		END; END WHILE;

		CASE
			WHEN in_count = 3 THEN SET out_count = 4;
			WHEN in_count = 2 THEN SET out_count = 3;
			WHEN in_count = 1 THEN SET out_count = 2;
			ELSE RETURN ret;
		END CASE;

		WHILE out_count > 0 DO BEGIN
			BEGIN
				DECLARE out_char CHAR(1);
				DECLARE base64_getval CURSOR FOR SELECT c FROM dbo.base64_data WHERE val = (accum_value >> 18);

				OPEN base64_getval;
				FETCH base64_getval INTO out_char;
				CLOSE base64_getval;

				SET ret = CONCAT(ret,out_char);
				SET out_count = out_count - 1;
				SET accum_value = accum_value << 6 & 0xffffff;
			END;
		END; END WHILE;

		CASE
			WHEN in_count = 2 THEN SET ret = CONCAT(ret,'=');
			WHEN in_count = 1 THEN SET ret = CONCAT(ret,'==');
			ELSE BEGIN END;
		END CASE;
	
	END; END WHILE;

	RETURN ret;
END;

 $$

DELIMITER ;

