@echo off

set DB_SERVER=%1
set SQL_SCRIPT=%2

echo "RUN %SQL_SCRIPT% SCRIPT ON DB SERVER %DB_SERVER%" 

mysql.exe -h %DB_SERVER% -u root -p mysql < %SQL_SCRIPT%.sql
pause
