select templateId from UTV_Template group by templateId having count(*) > 1;
select * from UTV_Template where templateId like 'z%';

###
### 01
###
SELECT templateId , siteId FROM utv_template where templateId='01' ;
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='01';
select hostId, templatePlayList from UTV_host where siteId='JP01' and INSTR(templatePlayList,'01/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'01/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'01/')>0;

remove RM=0/Site=tokyo/Template=01

### JP01, 01 --> z1

update UTV_Template set templateId='z1' where siteId='JP01' and  templateId='01';
update UTV_Frame set templateId='z1' where siteId='JP01' and  templateId='01';
update UTV_DefaultSchedule set templateId='z1' where siteId='JP01' and  templateId='01';

update UTV_Host  set templatePlayList=REPLACE(templatePlayList,'01/','z1/') 
where siteId='JP01' and INSTR(templatePlayList,'01/')>0;


###
### 19
###

SELECT templateId , siteId FROM utv_template where templateId='19';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='19';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'19/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'19/')>0;

### ktf2,19 --> z2

update UTV_Template set templateId='z2' where siteId='ktf2' and  templateId='19';
update UTV_Frame set templateId='z2' where siteId='ktf2' and  templateId='19';
update UTV_DefaultSchedule set templateId='z2' where siteId='ktf2' and  templateId='19';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'19/','z2/') 
where siteId='ktf2' and INSTR(templatePlayList,'19/')>0;

#update UTV_Host set templatePlayList=REPLACE(templatePlayList,'z2/','19/') 
#where siteId='ktf1' and INSTR(templatePlayList,'z2/')>0;

###
### 20
###

SELECT templateId , siteId FROM utv_template where templateId='20';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='20';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'20/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'20/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'20/')>0;
select hostId, templatePlayList from UTV_host where siteId='seoul' and INSTR(templatePlayList,'20/')>0;

remove RM=0/Site=tokyo/Template=20
remove RM=0/Site=seoul/Template=20

### ktf2,20 --> z3

update UTV_Template set templateId='z3' where siteId='ktf2' and  templateId='20';
update UTV_Frame set templateId='z3' where siteId='ktf2' and  templateId='20';
update UTV_DefaultSchedule set templateId='z3' where siteId='ktf2' and  templateId='20';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'20/','z3/') 
where siteId='ktf2' and INSTR(templatePlayList,'20/')>0;

###
### 25
###

SELECT templateId , siteId FROM utv_template where templateId='25';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='25';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'25/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'25/')>0;

### ktf2,25 --> z4

update UTV_Template set templateId='z4' where siteId='ktf2' and  templateId='25';
update UTV_Frame set templateId='z4' where siteId='ktf2' and  templateId='25';
update UTV_DefaultSchedule set templateId='z4' where siteId='ktf2' and  templateId='25';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'25/','z4/') 
where siteId='ktf2' and INSTR(templatePlayList,'25/')>0;

###
### 26
###

SELECT templateId , siteId FROM utv_template where templateId='26';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='26';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'26/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'26/')>0;

### ktf2,26 --> z5

update UTV_Template set templateId='z5' where siteId='ktf2' and  templateId='26';
update UTV_Frame set templateId='z5' where siteId='ktf2' and  templateId='26';
update UTV_DefaultSchedule set templateId='z5' where siteId='ktf2' and  templateId='26';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'26/','z5/') 
where siteId='ktf2' and INSTR(templatePlayList,'26/')>0;

###
### 27
###

SELECT templateId , siteId FROM utv_template where templateId='27';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='27';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'27/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'27/')>0;

### ktf2,27 --> z6

update UTV_Template set templateId='z6' where siteId='ktf2' and  templateId='27';
update UTV_Frame set templateId='z6' where siteId='ktf2' and  templateId='27';
update UTV_DefaultSchedule set templateId='z6' where siteId='ktf2' and  templateId='27';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'27/','z6/') 
where siteId='ktf2' and INSTR(templatePlayList,'27/')>0;

###
### 28
###

SELECT templateId , siteId FROM utv_template where templateId='28';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='28';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'28/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'28/')>0;

### ktf2,28 --> z7

update UTV_Template set templateId='z7' where siteId='ktf2' and  templateId='28';
update UTV_Frame set templateId='z7' where siteId='ktf2' and  templateId='28';
update UTV_DefaultSchedule set templateId='z7' where siteId='ktf2' and  templateId='28';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'28/','z7/') 
where siteId='ktf2' and INSTR(templatePlayList,'28/')>0;

###
### 29
###

SELECT templateId , siteId FROM utv_template where templateId='29';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='29';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'29/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'29/')>0;

### ktf2,29 --> z8

update UTV_Template set templateId='z8' where siteId='ktf2' and  templateId='29';
update UTV_Frame set templateId='z8' where siteId='ktf2' and  templateId='29';
update UTV_DefaultSchedule set templateId='z8' where siteId='ktf2' and  templateId='29';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'29/','z8/') 
where siteId='ktf2' and INSTR(templatePlayList,'29/')>0;

###
### 30
###

SELECT templateId , siteId FROM utv_template where templateId='30';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='30';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'30/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'30/')>0;

### ktf2,30 --> z9

update UTV_Template set templateId='z9' where siteId='ktf2' and  templateId='30';
update UTV_Frame set templateId='z9' where siteId='ktf2' and  templateId='30';
update UTV_DefaultSchedule set templateId='z9' where siteId='ktf2' and  templateId='30';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'30/','z9/') 
where siteId='ktf2' and INSTR(templatePlayList,'30/')>0;

###
### 31
###

SELECT templateId , siteId FROM utv_template where templateId='31';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='31';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'31/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'31/')>0;

### ktf2,31 --> za

update UTV_Template set templateId='za' where siteId='ktf2' and  templateId='31';
update UTV_Frame set templateId='za' where siteId='ktf2' and  templateId='31';
update UTV_DefaultSchedule set templateId='za' where siteId='ktf2' and  templateId='31';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'31/','za/') 
where siteId='ktf2' and INSTR(templatePlayList,'31/')>0;

###
### 32
###

SELECT templateId , siteId FROM utv_template where templateId='32';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='32';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'32/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'32/')>0;

### ktf2,32 --> zb

update UTV_Template set templateId='zb' where siteId='ktf2' and  templateId='32';
update UTV_Frame set templateId='zb' where siteId='ktf2' and  templateId='32';
update UTV_DefaultSchedule set templateId='zb' where siteId='ktf2' and  templateId='32';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'32/','zb/') 
where siteId='ktf2' and INSTR(templatePlayList,'32/')>0;

###
### 33
###

SELECT templateId , siteId FROM utv_template where templateId='33';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='33';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'33/')>0;
select hostId, templatePlayList from UTV_host where siteId='ktf2' and INSTR(templatePlayList,'33/')>0;

### ktf2,33 --> zc

update UTV_Template set templateId='zc' where siteId='ktf2' and  templateId='33';
update UTV_Frame set templateId='zc' where siteId='ktf2' and  templateId='33';
update UTV_DefaultSchedule set templateId='zc' where siteId='ktf2' and  templateId='33';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'33/','zc/') 
where siteId='ktf2' and INSTR(templatePlayList,'33/')>0;

###
### 45
###

SELECT templateId , siteId FROM utv_template where templateId='45';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='45';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'45/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'45/')>0;

### tokyo,45 --> zd

update UTV_Template set templateId='zd' where siteId='tokyo' and  templateId='45';
update UTV_Frame set templateId='zd' where siteId='tokyo' and  templateId='45';
update UTV_DefaultSchedule set templateId='zd' where siteId='tokyo' and  templateId='45';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'45/','zd/') 
where siteId='tokyo' and INSTR(templatePlayList,'45/')>0;

###
### 46
###

SELECT templateId , siteId FROM utv_template where templateId='46';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='46';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'46/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'46/')>0;

### tokyo,46 --> ze

update UTV_Template set templateId='ze' where siteId='tokyo' and  templateId='46';
update UTV_Frame set templateId='ze' where siteId='tokyo' and  templateId='46';
update UTV_DefaultSchedule set templateId='ze' where siteId='tokyo' and  templateId='46';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'46/','ze/') 
where siteId='tokyo' and INSTR(templatePlayList,'46/')>0;

###
### 47
###

SELECT templateId , siteId FROM utv_template where templateId='47';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='47';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'47/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'47/')>0;

### tokyo,47 --> zf

update UTV_Template set templateId='zf' where siteId='tokyo' and  templateId='47';
update UTV_Frame set templateId='zf' where siteId='tokyo' and  templateId='47';
update UTV_DefaultSchedule set templateId='zf' where siteId='tokyo' and  templateId='47';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'47/','zf/') 
where siteId='tokyo' and INSTR(templatePlayList,'47/')>0;

###
### 48
###

SELECT templateId , siteId FROM utv_template where templateId='48';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='48';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'48/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'48/')>0;

### tokyo,48 --> zg

update UTV_Template set templateId='zg' where siteId='tokyo' and  templateId='48';
update UTV_Frame set templateId='zg' where siteId='tokyo' and  templateId='48';
update UTV_DefaultSchedule set templateId='zg' where siteId='tokyo' and  templateId='48';

update UTV_Host set templatePlayList=REPLACE(templatePlayList,'48/','zg/') 
where siteId='tokyo' and INSTR(templatePlayList,'48/')>0;

###
### 49
###

SELECT templateId , siteId FROM utv_template where templateId='49';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='49';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'49/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'49/')>0;

remove RM=0/Site=ktf1/Template=49

###
### 60
###

SELECT templateId , siteId FROM utv_template where templateId='60';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='60';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'60/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'60/')>0;

remove RM=0/Site=tokyo/Template=60

###
### 61
###

SELECT templateId , siteId FROM utv_template where templateId='61';
select templateId, scheduleId,hostId,siteId,contentsId from utv_defaultSchedule where templateId='61';
select hostId, templatePlayList from UTV_host where siteId='ktf1' and INSTR(templatePlayList,'61/')>0;
select hostId, templatePlayList from UTV_host where siteId='tokyo' and INSTR(templatePlayList,'61/')>0;

remove RM=0/Site=tokyo/Template=61

set SM=0/Site=tokyo/Host=UBC0  attributes=[ templatePlayList="41/1,43/1,44/1,ze/1,21/1,22/1,23/1,24/1"]

###
###
###

select templateId from UTV_Template group by templateId having count(*) > 1;

