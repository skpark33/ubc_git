﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Sockets;
//using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace UBC_HTTP
{
    public class CciAddHandRcv : Receiver
    {
        public CciAddHandRcv(string inCommand, string inIp, int inPort, string inBody, string inQuery)
            : base(inCommand, inIp, inPort, inBody, inQuery)
        {   
        }

        public override CopResponse Action()
        {
            // 단말이 대량인 경우 announceExpired 이벤트를 addhand 하지 않도록 한다
            if (body == "announceExpired")
            {
                return new CopResponse(false, "[announceExpired] Event is not yet supported.", "", "");
            }

            CopResponse result = new CopResponse(true, "", "", "");

            try
            {
                // DB접속
                Database db = DatabaseFactory.CreateDatabase();

                string addtionalQuery1 = "";
                string addtionalQuery2 = "";

                // MSSQL인 경우 아래 코드 실행
                if (db.DbProviderFactory.ToString().Contains("System.Data.SqlClient"))
                {
                    db.ExecuteNonQuery(CommandType.Text, "SET DATEFORMAT YMD");
                    addtionalQuery1 = "top 1";
                }
                else if(db.DbProviderFactory.ToString().Contains("MySql.Data.MySqlClient"))
                {
                    addtionalQuery2 = "limit 0, 1";
                }

                //string newQuery = string.Format("SELECT {0} * FROM ubc_event WHERE eventType = '{1}' ORDER BY eventIndex DESC {2};"
                string newQuery = string.Format("SELECT {0} * FROM ubc_event ORDER BY eventIndex DESC {1};"
                                               , addtionalQuery1, addtionalQuery2);

                // 쿼리실행
                DbCommand Cmd = db.GetSqlStringCommand(newQuery);

                // 결과변환
                DataSet ds = db.ExecuteDataSet(Cmd);

                if (ds.Tables.Count <= 0)
                {
                    ds.Tables.Add();
                }

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    DataTable dt = ds.Tables[0];
                    //dt.Columns.Add("eventType");
                    //dt.Columns.Add("eventIndex");
                    //dt.Columns.Add("domain");
                    //dt.Columns.Add("entity");
                    //dt.Columns.Add("eventTime");
                    //dt.Columns.Add("eventBody");

                    dt.Rows.Add(body, "0", "", "", System.DateTime.Now, "");
                }
                else
                {
                    for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ds.Tables[0].Rows.RemoveAt(1);
                    }
                }

                result = new CopResponse(true, "", "XML", ds.GetXml());
            }
            catch (Exception err)
            {
                return new CopResponse(false, err.Message, "", "");
            }

            return result;
        }
    }
}
