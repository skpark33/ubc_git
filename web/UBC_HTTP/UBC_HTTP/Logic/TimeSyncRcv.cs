﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Sockets;
using System.Data.SqlClient;

namespace UBC_HTTP
{
    public class TimeSyncRcv : Receiver
    {
        public TimeSyncRcv(string inCommand, string inIp, int inPort, string inBody, string inQuery)
            : base(inCommand, inIp, inPort, inBody, inQuery)
        {
        }

        public override CopResponse Action()
        {
            CopResponse result;

            try
            {
                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                s.Connect(ip, port);

                string rcvData = "";

                byte[] buf = new byte[2048];
                int rcvsize = 0;
                while ((rcvsize = s.Receive(buf)) > 0)
                {
                    if (Constants.IS_GLOBAL) rcvData += System.Text.Encoding.UTF8.GetString(buf, 0, rcvsize);
                    else                     rcvData += System.Text.Encoding.Default.GetString(buf, 0, rcvsize);
                }
                s.Disconnect(false);
                s.Close();

                result = new CopResponse(true, "", "TEXT", rcvData);
            }
            catch (Exception err)
            {
                return new CopResponse(false, err.Message, "", "");
            }

            return result;
        }
    }
}
