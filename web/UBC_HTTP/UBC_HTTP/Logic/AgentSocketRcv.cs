﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Sockets;
using System.Data.SqlClient;

namespace UBC_HTTP
{
    public class AgentSocketRcv : Receiver
    {
        public AgentSocketRcv(string inCommand, string inIp, int inPort, string inBody, string inQuery)
            : base(inCommand, inIp, inPort, inBody, inQuery)
        {
        }

        public override CopResponse Action()
        {
            CopResponse result;

            try
            {
                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                s.Connect(ip, port);

                byte[] sndBuf = null;
                if (Constants.IS_GLOBAL) sndBuf = System.Text.Encoding.UTF8.GetBytes(body);
                else                     sndBuf = System.Text.Encoding.Default.GetBytes(body);
                s.Send(sndBuf);

                string rcvData = "";

                byte[] buf = new byte[2048];
                int rcvsize = 0;
                while ((rcvsize = s.Receive(buf)) > 0)
                {
                    string bufData = "";
                    if (Constants.IS_GLOBAL) bufData += System.Text.Encoding.UTF8.GetString(buf, 0, rcvsize);
                    else                     bufData += System.Text.Encoding.Default.GetString(buf, 0, rcvsize);

                    Log.WriteLine(Log.LOG_LEVEL.DEBUG, "", "buf", bufData);

                    rcvData += bufData;
                }
                Log.WriteLine(Log.LOG_LEVEL.DEBUG, "", "Disconnect");
                s.Disconnect(false);
                Log.WriteLine(Log.LOG_LEVEL.DEBUG, "", "Close");
                s.Close();
                Log.WriteLine(Log.LOG_LEVEL.DEBUG, "", "return");

                result = new CopResponse(true, "", "TEXT", rcvData);
            }
            catch (Exception err)
            {
                return new CopResponse(false, err.Message, "", "");
            }

            return result;
        }
    }
}
