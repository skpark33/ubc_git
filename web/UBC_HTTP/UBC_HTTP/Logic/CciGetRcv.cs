﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Sockets;
using System.Collections;
using System.Text;
//using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace UBC_HTTP
{
    public class CciGetRcv : Receiver
    {
        public CciGetRcv(string inCommand, string inIp, int inPort, string inBody, string inQuery)
            : base(inCommand, inIp, inPort, inBody, inQuery)
        {
        }

        private string classScopedName;
        private string[] entityList;

        private void InitClassScopedName(string inBody)
        {
            string directive = "";
            string entity = "";

            string[] tokens = inBody.Split(new char[] { ' ', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            if (tokens.Length >= 1) directive = tokens[0];
            if (tokens.Length >= 2) entity = tokens[1];

            classScopedName = "";
            entityList = entity.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < entityList.Length; i++)
            {
                int pos = entityList[i].IndexOf('=');
                if (pos >= 0)
                {
                    entityList[i] = entityList[i].Substring(0, pos);
                    classScopedName += "::" + entityList[i];
                }
            }

            Log.WriteLine(Log.LOG_LEVEL.DEBUG, "", "CopService", directive, entity, classScopedName);
        }

        /// <summary> 
        /// 날짜 형식의 데이타를 int형으로 형변환 
        /// </summary> 
        public static int ConvertDatetimeToInt( System.DateTime dt ) 
        {
            dt = dt.ToUniversalTime();
            System.TimeSpan ts = dt.Subtract( new System.DateTime(1970,1,1) );
            return (int)(ts.Ticks/10000000L);
        }

        /// <summary> 
        /// int형식의 데이타를 날짜형으로 형 변환 
        /// </summary> 
        public static DateTime ConvertIntToDatetime( int timeTick ) 
        { 
            System.DateTime dt = new DateTime(1970, 1,1).AddSeconds( timeTick );
            dt = dt.AddHours(9);
            return dt; 
        }

        public override CopResponse Action()
        {
            CopResponse result;

            InitClassScopedName(body);

            string mirQuery = string.Format("select * from ubc_mir where classScopedName = '{0}' order by indexOrder", classScopedName);

            try
            {
                // DB접속
                Database db = DatabaseFactory.CreateDatabase();

                // MIR쿼리실행
                DataSet dsMir = db.ExecuteDataSet(db.GetSqlStringCommand(mirQuery));

                DataSet dsResult;
                // MSSQL인 경우 아래 코드 실행
                if (db.DbProviderFactory.ToString().Contains("System.Data.SqlClient"))
                {
                    // skpark
                    string xquery = "SET DATEFORMAT YMD ; " + query;
                    //db.ExecuteNonQuery(CommandType.Text, "SET DATEFORMAT YMD");
                    // 결과변환
                    dsResult = db.ExecuteDataSet(db.GetSqlStringCommand(xquery));
                }
                else
                {
                    // 결과변환
                    dsResult = db.ExecuteDataSet(db.GetSqlStringCommand(query));
                }

              
                Hashtable copColTypes = new Hashtable();
                Hashtable copEntity = new Hashtable();

                foreach (DataTable table in dsMir.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        copColTypes.Add(row["attrName"].ToString(), row["attrType"].ToString());

                        if (row["indexOrder"].ToString() != "99")
                        {
                            copEntity.Add(row["className"].ToString(), row["attrName"].ToString());
                        }
                    }
                }

                StringBuilder copToString = new StringBuilder();

                foreach (DataTable table in dsResult.Tables)
                {
                    // Header
                    StringBuilder nameHeader = new StringBuilder();
                    StringBuilder typeHeader = new StringBuilder();
                    foreach (DataColumn colInfo in table.Columns)
                    {
                        if (copColTypes[colInfo.ColumnName] == null) continue;

                        if (nameHeader.Length > 0) nameHeader.Append(",");
                        nameHeader.Append(colInfo.ColumnName);

                        if (typeHeader.Length > 0) typeHeader.Append(",");
                        typeHeader.Append(copColTypes[colInfo.ColumnName]);
                    }

                    string copHeader = string.Format("[{0}]{1}[{2}]", nameHeader, Constants.COP_DELIM, typeHeader);

                    if (copToString.Length > 0) copToString.Append(Constants.COP_DELIM);
                    copToString.Append(copHeader);

                    // Data
                    foreach (DataRow row in table.Rows)
                    {
                        string rowEntity = "";
                        foreach(string className in entityList)
                        {
                            if(!string.IsNullOrEmpty(rowEntity)) rowEntity += "/";
                            rowEntity += className + "=" + row[copEntity[className].ToString()].ToString();
                        }

                        StringBuilder rowAttributes = new StringBuilder();

                        int index = 0;
                        foreach (DataColumn colInfo in table.Columns)
                        {
                            if (copColTypes[colInfo.ColumnName] == null) continue;

                            if (rowAttributes.Length > 0) rowAttributes.Append(",");

                            string value = row[colInfo.ColumnName].ToString();
                            if (copColTypes[colInfo.ColumnName].ToString() == "CCI_String" && !string.IsNullOrEmpty(value))
                            {
                                value = string.Format("\"{0}\"", value);
                            }
                            // SSSSSSSSSS/0,CCI_Time
                            else if (copColTypes[colInfo.ColumnName].ToString() == "CCI_Time")
                            {
                                long timeValueLong = 0;
                                if (!string.IsNullOrEmpty(value))
                                {
                                    DateTime timeValue = Convert.ToDateTime(row[colInfo.ColumnName]);
                                    timeValueLong = ConvertDatetimeToInt(timeValue);
                                }

                                value = string.Format("SSSSSSSSSS/{0}", timeValueLong);
                            }
                            else if (copColTypes[colInfo.ColumnName].ToString() == "CCI_Boolean")
                            {
                                value = (value == "1" ? "true" : "false");
                            }
                            else if (copColTypes[colInfo.ColumnName].ToString() == "CCI_Enum")
                            {
                                value = (string.IsNullOrEmpty(value) ? "0" : value);
                            }

                            string str = string.Format("{{{0},{1},T}}"
                                                     , index++
                                                     , value
                                                     );

                            rowAttributes.Append(str);
                        }

                        string copRow = string.Format("{{{0},[{{attributes,[{1}],CCI_AttributeList}}]}}", rowEntity, rowAttributes);

                        if (copToString.Length > 0) copToString.Append(Constants.COP_DELIM);
                        copToString.Append(copRow);
                        //Log.WriteLine(Log.LOG_LEVEL.ERROR, "skpark", "CopService", body, query, copRow);  //skpark temp code
                    }
                }

                //result = new CopResponse(true, "", "XML", dsResult.GetXml());
                result = new CopResponse(true, "", "TEXT", copToString.ToString());
            }
            catch (Exception err)
            {
                Log.WriteLine(Log.LOG_LEVEL.ERROR, "", "CopService", body, query, err.Message);  //skpark add
                return new CopResponse(false, err.Message, "", "");
            }

            return result;
        }
    }
}
