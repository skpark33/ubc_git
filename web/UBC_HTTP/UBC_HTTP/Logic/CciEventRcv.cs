﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Sockets;
//using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace UBC_HTTP
{
    public class CciEventRcv : Receiver
    {
        public CciEventRcv(string inCommand, string inIp, int inPort, string inBody, string inQuery)
            : base(inCommand, inIp, inPort, inBody, inQuery)
        {
        }

        public override CopResponse Action()
        {
            CopResponse result = new CopResponse(true, "", "", "");

            try
            {
                ////<PARAM><eventType>alarmSummary</eventType><eventIndex>2000</eventIndex><domain>FM</domain><entity>FM=*</entity></PARAM>
                //System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                //xmlDoc.LoadXml(body);
                //string eventType = xmlDoc.SelectSingleNode("//PARAM/eventType").FirstChild.Value;
                //string eventIndex = xmlDoc.SelectSingleNode("//PARAM/eventIndex").FirstChild.Value;
                //string domain = xmlDoc.SelectSingleNode("//PARAM/domain").FirstChild.Value;
                //string entity = xmlDoc.SelectSingleNode("//PARAM/entity").FirstChild.Value;

                //alarmSummary, 2000, FM, FM=*

                string fullQuery = "";
                foreach (string singleEvent in body.Split(new string[] { Constants.COP_DELIM }, StringSplitOptions.RemoveEmptyEntries))
                {
                    string[] param = singleEvent.Split(',');
                    if (param.Length != 4)
                    {
                        return new CopResponse(false, "Invalid params", "", "");
                    }

                    string eventType = param[0].Trim();
                    string eventIndex = param[1].Trim();
                    string domain = param[2].Trim();
                    string entity = param[3].Trim();

                    Log.WriteLine(Log.LOG_LEVEL.DEBUG, "", "CopService", eventType, eventIndex, domain, entity);

                    entity = entity.Replace("*", "%");

                    string singleQuery = "";
                    if (string.IsNullOrEmpty(entity) || entity == "=" || entity == "%=" || entity == "=%" || entity == "%=%")
                    {
                        singleQuery = string.Format("SELECT * FROM ubc_event WHERE eventType = N'{0}' and eventIndex > {1}"
                                                , eventType, eventIndex);
                    }
                    else
                    {
                        singleQuery = string.Format("SELECT * FROM ubc_event WHERE eventType = N'{0}' and eventIndex > {1} and entity like N'{2}'"
                                                , eventType, eventIndex, entity);
                    }

                    if (!string.IsNullOrEmpty(fullQuery))
                    {
                        fullQuery += " union all ";
                    }

                    fullQuery += singleQuery;
                }

                fullQuery = string.Format("SELECT DISTINCT A.* FROM ( {0} ) A ORDER BY A.eventIndex;", fullQuery);

                Log.WriteLine(Log.LOG_LEVEL.DEBUG, "", "CopService", fullQuery);

                // DB접속
                Database db = DatabaseFactory.CreateDatabase();

                // MSSQL인 경우 아래 코드 실행
                if (db.DbProviderFactory.ToString().Contains("System.Data.SqlClient"))
                {
                    db.ExecuteNonQuery(CommandType.Text, "SET DATEFORMAT YMD");
                }

                int timeOut = Constants.COP_EVENT_TIMEOUT * 1000;
                int interval = Constants.COP_EVENT_POLLING_INTERVAL;
                int startTick = System.Environment.TickCount;
                string queryResult = "<NewDataSet />";

                while ((System.Environment.TickCount - startTick) < timeOut)
                {
                    // 쿼리실행
                    DataSet ds = db.ExecuteDataSet(db.GetSqlStringCommand(fullQuery));
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        queryResult = ds.GetXml();
                        break;
                    }

                    System.Threading.Thread.Sleep(interval);
                }

                if (Constants.IS_GLOBAL)
                {
                    Byte[] temp = System.Text.Encoding.UTF8.GetBytes(queryResult);
                    queryResult = System.Text.Encoding.UTF8.GetString(temp);
                }

                result = new CopResponse(true, "", "XML", queryResult);
            }
            catch (Exception err)
            {
                string filePath = Constants.CONFIG_ROOT + "\\" + Constants.COP_PROPERTIES;
                return new CopResponse(false, err.Message + " : " + Constants.DB_CONNECTION_STRING + " : " + filePath, "", "");
            }

            return result;
        }
    }
}
