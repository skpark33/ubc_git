﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Sockets;
using System.Data.SqlClient;

namespace UBC_HTTP
{
    public class ShellRcv : Receiver
    {
        public ShellRcv(string inCommand, string inIp, int inPort, string inBody, string inQuery)
            : base(inCommand, inIp, inPort, inBody, inQuery)
        {
        }

        public override CopResponse Action()
        {
            CopResponse result;

            try
            {
                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                //int port = inPort;
                s.Connect(ip, port);

                string decodedBody = System.Text.Encoding.GetEncoding(0).GetString(Convert.FromBase64String(body));

                // make header
                byte[] sendBody = null;
                if (Constants.IS_GLOBAL) sendBody = System.Text.Encoding.UTF8.GetBytes(decodedBody);
                else                     sendBody = System.Text.Encoding.Default.GetBytes(decodedBody);
                //byte[] sendBytes = new byte[sendBody.Length + 8];
                //if (Constants.IS_GLOBAL) System.Text.Encoding.UTF8.GetBytes("SCGW").CopyTo(sendBytes, 0);
                //else                     System.Text.Encoding.Default.GetBytes("SCGW").CopyTo(sendBytes, 0);
                //BitConverter.GetBytes(sendBody.Length).CopyTo(sendBytes, 4);
                //sendBody.CopyTo(sendBytes, 8);

                //s.Send(sendBytes);

                s.Send(sendBody);

                //bool bIsValid = false;

                string rcvData = "";

                byte[] buf = new byte[2048];
                int rcvsize = 0;
                while ((rcvsize = s.Receive(buf)) > 0)
                {
                    //if (!bIsValid)
                    //{
                        //if (rcvsize < 8) throw new Exception("Shell packet header size error!!");

                        //bIsValid = (System.Text.Encoding.Default.GetString(buf, 0, 4) == "?HSR");

                        //if (!bIsValid) throw new Exception("Shell packet header code error!!");

                        //if(Constants.IS_GLOBAL) rcvData += System.Text.Encoding.UTF8.GetString(buf, 8, rcvsize - 8);
                        //else                    rcvData += System.Text.Encoding.Default.GetString(buf, 8, rcvsize - 8);
                        //continue;
                    //}

                    
                    if (Constants.IS_GLOBAL) rcvData += System.Text.Encoding.UTF8.GetString(buf, 0, rcvsize);
                    else                     rcvData += System.Text.Encoding.Default.GetString(buf, 0, rcvsize);

                }
                s.Disconnect(false);
                s.Close();
                
                // Debug
                //rcvData = rcvData.Replace("AttributeSetType", "CCI_AttributeList");

                result = new CopResponse(true, "", "TEXT", rcvData);
            }
            catch (Exception err)
            {
                return new CopResponse(false, err.Message, "", "");
            }

            return result;
        }
    }
}
