﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace UBC_HTTP
{
    /// <summary>
    /// The 'Command' abstract class
    /// </summary>
    public abstract class Command
    {
        protected Receiver receiver;

        // Constructor
        public Command(Receiver receiver)
        {
            this.receiver = receiver;
        }

        public abstract CopResponse Execute();
    }

    /// <summary>
    /// The 'ConcreteCommand' class
    /// </summary>
    public class CopCommand : Command
    {
        // Constructor
        public CopCommand(Receiver receiver) : base(receiver)
        {
        }

        public override CopResponse Execute()
        {
            return receiver.Action();
        }
    }

    /// <summary>
    /// The 'Receiver' class
    /// </summary>
    abstract public class Receiver
    {
        private string _command;
        public string command { get { return _command; } }

        private string _ip;
        public string ip { get { return _ip; } }

        private int _port;
        public int port { get { return _port; } }

        private string _body;
        public string body { get { return _body; } }

        private string _query;
        public string query { get { return _query; } }

        public Receiver(string inCommand, string inIp, int inPort, string inBody, string inQuery)
        {
            _command = inCommand;
            _ip = inIp;
            _port = inPort;
            _body = inBody;
            _query = inQuery;
        }

        public abstract CopResponse Action();
    }

    /// <summary>
    /// The 'Invoker' class
    /// </summary>
    class Invoker
    {
        private Command _command;

        public void SetCommand(Command command)
        {
            this._command = command;
        }

        public void ExecuteCommand()
        {
            _command.Execute();
        }
    }
}
