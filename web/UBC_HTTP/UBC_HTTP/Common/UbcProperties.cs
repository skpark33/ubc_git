﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;

namespace UBC_HTTP
{
    public class UbcProperties
    {
        private static UbcProperties _instance = null;
        static readonly object padlock = new object();
        public Dictionary<string, string> propertiesData = new Dictionary<string, string>();

        protected UbcProperties()
        {
            string filePath = Constants.CONFIG_ROOT + "\\" + Constants.COP_PROPERTIES;
            
            FileInfo info = new FileInfo(filePath);
            if (!info.Exists) return;

            foreach (string row in File.ReadAllLines(filePath))
            {
                row.Trim();
                if (string.IsNullOrEmpty(row)) continue;

                int idx = row.IndexOf('=');

                string key = "";
                string value = "";

                if (idx < 0) value = row;
                else
                {
                    key = row.Substring(0, idx).ToUpper();
                    value = row.Substring(idx + 1, row.Length - (idx + 1));
                }

                if (!propertiesData.ContainsKey(key))
                {
                    propertiesData.Add(key, value);
                }
            }
        }

        static public UbcProperties Instance
        {
            get
            {
                lock (padlock) // thread safe
                {
                    // Uses lazy initialization.
                    if (_instance == null)
                    {
                        _instance = new UbcProperties();
                    }

                    return _instance;
                }
            }
        }

        public bool GetValue(string key, out string value)
        {
            return propertiesData.TryGetValue(key.ToUpper(), out value);
        }
    }
}
