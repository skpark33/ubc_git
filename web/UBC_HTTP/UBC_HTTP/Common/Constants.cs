﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace UBC_HTTP
{
    public class Constants
    {
        static bool alreadyInit = false;
        static string default_path = "";
        static System.Collections.Hashtable user_path = new System.Collections.Hashtable();
        static string config_root = "";
        static string project_home = "";
        static System.Collections.Hashtable user_pwd = new System.Collections.Hashtable();
        static string ubc_variables = "";
        static string cop_properties = "";
        static string cop_get_mode = "";
        static string cop_delim = "";
        static int log_level = 4;  // WARNING Level
        static string log_filename = "";
        static string log_filename_date = "";
        static string log_delim = "";
        static int event_timeout = 25;
        static int event_polling_interval = 100;

        static System.Collections.Hashtable nat_map = new System.Collections.Hashtable(); // 공용IP --> 사설IP 변경용

        static readonly object init_lock = new object();
        //static readonly object user_path_lock = new object();
        //static readonly object user_pwd_lock = new object();
        static int files_info_limit = 0;  //skpark files_info_limit actually It should be 1000,  env variable  


        static public void Init()
        {
            lock (init_lock)
            {
                if (alreadyInit)
                {
                    return;
                }
                config_root     = System.Environment.GetEnvironmentVariable("CONFIGROOT");
                project_home    = System.Environment.GetEnvironmentVariable("PROJECT_HOME");
                default_path    = System.Configuration.ConfigurationManager.AppSettings["DEFAULT_PATH"];
                ubc_variables   = System.Configuration.ConfigurationManager.AppSettings["UBC_VARIABLES"];
                cop_properties  = System.Configuration.ConfigurationManager.AppSettings["COP_PROPERTIES"];
                cop_get_mode    = System.Configuration.ConfigurationManager.AppSettings["COP_GET_MODE"];
                cop_delim       = System.Configuration.ConfigurationManager.AppSettings["COP_DELIM"];
                log_filename    = System.Configuration.ConfigurationManager.AppSettings["LOG_FILENAME"];
                log_delim       = System.Configuration.ConfigurationManager.AppSettings["LOG_DELIM"];
                string files_info_limit_buf = System.Configuration.ConfigurationManager.AppSettings["FILES_INFO_LIMIT"];  //skpark files_info_limit
                if (!string.IsNullOrEmpty(files_info_limit_buf)) { files_info_limit = Convert.ToInt32(files_info_limit_buf); }

                string log_level_buf = System.Configuration.ConfigurationManager.AppSettings["LOG_LEVEL"];
                if (!string.IsNullOrEmpty(log_level_buf)) { log_level = Convert.ToInt32(log_level_buf); }
                
                string event_timeout_buf = System.Configuration.ConfigurationManager.AppSettings["COP_EVENT_TIMEOUT"];
                if (!string.IsNullOrEmpty(event_timeout_buf)) { event_timeout = Convert.ToInt32(event_timeout_buf); }

                string event_polling_interval_buf = System.Configuration.ConfigurationManager.AppSettings["COP_EVENT_POLLING_INTERVAL"];
                if (!string.IsNullOrEmpty(event_polling_interval_buf)) { event_polling_interval = Convert.ToInt32(event_polling_interval_buf); }

                string[] userId = new string[5];
                userId[0]="ds_ent_center";
                userId[1]="ds_ent_center_server";
                userId[2]="ent_center";
                userId[3]="ent_center_server";
                userId[4]="server";

                for(int i=0;i<5;i++){
                    // USER_PATH  
                    string userPath = System.Configuration.ConfigurationManager.AppSettings[userId[i] + "_PATH"];
                    if (string.IsNullOrEmpty(userPath)) userPath = DEFAULT_PATH;
                    user_path.Add(userId[i], userPath);

                    // USER_PWD          
                    string userPwd = System.Configuration.ConfigurationManager.AppSettings[userId[i] + "_PWD"];
                    if (string.IsNullOrEmpty(userPwd)) userPwd = "";
                    user_pwd.Add(userId[i], userPwd);
                }

                Log.WriteLine(Log.LOG_LEVEL.ERROR, "INIT NAT MAP", "", "", "", "", "", "");
                // 공용IP --> 사설IP 변경용
                for (int i=1; true; i++ )
                {
                    string nat_ip = System.Configuration.ConfigurationManager.AppSettings["NAT_IP_" + i.ToString()];
                    if (string.IsNullOrEmpty(nat_ip)) break;

                    string[] tokens = nat_ip.Split(new char[] { '|',',','/','-' }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens.Length != 2) continue;

                    string global_ip = tokens[0];
                    string internal_ip = tokens[1];

                    nat_map.Add(global_ip, internal_ip);

                    Log.WriteLine(Log.LOG_LEVEL.ERROR, "NAT", "GLOBAL_IP", global_ip, "NAT_IP", internal_ip, "", "");
                }
                alreadyInit = true;
            }
            return;
        }

        // 시스템 환경변수의 값
        static public string CONFIG_ROOT
        {
            get {return config_root;}
        }
        static public string PROJECT_HOME
        {
           // get { return System.Environment.GetEnvironmentVariable("PROJECT_HOME"); }
            get {return project_home;}
        }
        // web.config의 appsetting 값
        static public string DEFAULT_PATH
        {
            // return System.Configuration.ConfigurationManager.AppSettings["DEFAULT_PATH"]; 
            get {return default_path;}
        }
        static public string USER_PATH(string userId)
        {
            //lock (user_path_lock)
            //{
                string key = userId;
                if (user_path.ContainsKey(key))
                {
                     return user_path[key].ToString();
                }
                string userPath = System.Configuration.ConfigurationManager.AppSettings[userId + "_PATH"];
                if (string.IsNullOrEmpty(userPath)) userPath = DEFAULT_PATH;
                user_path.Add(userId, userPath);
                return userPath;
                
            //}
            /*
            string userPath = System.Configuration.ConfigurationManager.AppSettings[userId + "_PATH"];
            if (string.IsNullOrEmpty(userPath)) userPath = DEFAULT_PATH;
            return userPath;
            */
        }
        static public string USER_PWD(string userId)
        {
            /*
            return System.Configuration.ConfigurationManager.AppSettings[userId + "_PWD"];
            */
            //lock (user_pwd_lock)
        //{
            if (user_pwd.ContainsKey(userId))
            {
                 return user_pwd[userId].ToString();
            }
            string userPwd = System.Configuration.ConfigurationManager.AppSettings[userId + "_PWD"];
            if (string.IsNullOrEmpty(userPwd)) userPwd = "";
            user_pwd.Add(userId, userPwd);
            return userPwd;
                
            //}
        }
        static public string UBC_VARIABLES
        {
            get {return ubc_variables;}
        }
        static public string COP_PROPERTIES
        {
            get {return cop_properties;}
        }
        static public string COP_GET_MODE
        {
            get {return cop_get_mode;}
        }
        static public string COP_DELIM
        {
            get {return cop_delim;}
        }
        static public int LOG_LEVEL
        {
            get { return log_level; }
        }
        static public string LOG_FILENAME
        {
            get {
                string str_date = DateTime.Now.ToString("yyyyMMdd") + "_";
                log_filename_date = str_date + log_filename;
                return log_filename_date;
            }
            //return log_filename;
        }
        static public string LOG_DELIM
        {
            get {return log_delim;}
        }
        static public int COP_EVENT_TIMEOUT
        {
            get {return event_timeout;}
        }
        static public int COP_EVENT_POLLING_INTERVAL
        {
            get {return event_polling_interval;}
        }

        // ubc.properties의 값
        static public string COP_SCGW_PORT
        {
            get
            {
                string value;
                if (!UbcProperties.Instance.GetValue("COP.SCGW.PORT", out value)) return "14001";
                return value;
            }
        }

        //PS.MS_DB.COP_ODBC=211.232.57.186,1433
        //PS.MS_DB.COP_PWD=-507263a
        //PS.MS_DB.COP_TNS=ubc
        //PS.MS_DB.COP_USR=ubc
        static public string MSDB_IP
        {
            get
            {
                string value;
                if (!UbcProperties.Instance.GetValue("PS.MS_DB.COP_ODBC", out value)) return "localhost";
                string[] list = value.Split(',');
                return list[0];
            }
        }
        static public string MSDB_UID
        {
            get
            {
                string value;
                if (!UbcProperties.Instance.GetValue("PS.MS_DB.COP_USR", out value)) return "ubc";
                return value;
            }
        }
        static public string MSDB_PWD
        {
            get
            {
                string value;
                if (!UbcProperties.Instance.GetValue("PS.MS_DB.COP_PWD", out value)) return "";
                return value;
            }
        }
        static public string MSDB_DATABASE
        {
            get
            {
                string value;
                if (!UbcProperties.Instance.GetValue("PS.MS_DB.COP_TNS", out value)) return "";
                return value;
            }
        }
        static public string DB_CONNECTION_STRING
        {
            get
            {
                return string.Format("Server={0};uid={1};pwd={2};database={3};", MSDB_IP, MSDB_UID, MSDB_PWD, MSDB_DATABASE);
            }
        }

        static public bool IS_GLOBAL
        {
            get
            {
                string value;
                if (!UbcVariables.Instance.GetValue("GLOBAL", out value)) return false;
                return (value == "1");
            }
        }
        // 공용IP --> 사설IP 변경용
        static public string NAT_IP(string globalIP)
        {
            if (nat_map.Count == 0) return globalIP;
            if (nat_map.ContainsKey(globalIP))
            {
                return nat_map[globalIP].ToString();
            }
            return globalIP;
        }
        static public int FILES_INFO_LIMIT  //skpark files_info_limit;
        {
            get
            {
                return files_info_limit;
            }
        }
    }
}
