﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Generic;

namespace UBC_HTTP
{
    public class UbcVariables
    {
        private static UbcVariables _instance = null;
        static readonly object padlock = new object();
        protected Dictionary<string, string> propertiesData = new Dictionary<string, string>();

        protected UbcVariables()
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, "-----------------------< UbcVariables Instance >-----------------------");

            string filePath = Constants.CONFIG_ROOT + "\\data\\" + Constants.UBC_VARIABLES;
            Log.WriteLine(Log.LOG_LEVEL.INFO, "UBC_VARIABLES", filePath);

            FileInfo info = new FileInfo(filePath);
            if (!info.Exists)
            {
                Log.WriteLine(Log.LOG_LEVEL.ERROR, "UBC_VARIABLES is not exist");
                return;
            }

            foreach (string row in File.ReadAllLines(filePath))
            {
                row.Trim();
                if (string.IsNullOrEmpty(row)) continue;

                int idx = row.IndexOf('=');

                string key = "";
                string value = "";

                if (idx < 0) value = row;
                else
                {
                    key = row.Substring(0, idx).ToUpper();
                    value = row.Substring(idx + 1, row.Length - (idx + 1));
                }

                if (!propertiesData.ContainsKey(key))
                {
                    propertiesData.Add(key, value);
                }
            }

            string data;
            GetValue("GLOBAL", out data);
            Log.WriteLine(Log.LOG_LEVEL.INFO, "GLOBAL", data);
        }

        static public UbcVariables Instance
        {
            get
            {
                lock (padlock) // thread safe
                {
                    // Uses lazy initialization.
                    if (_instance == null)
                    {
                        _instance = new UbcVariables();
                    }

                    return _instance;
                }
            }
        }

        public bool GetValue(string key, out string value)
        {
            return propertiesData.TryGetValue(key.ToUpper(), out value);
        }
    }
}
