﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Diagnostics;

namespace UBC_HTTP
{
    /// <summary>
    ///
    /// 로그 파일은 My Documetns 폴더 내에 Logs 폴더 내에 작성하도록 하였다.
    ///
    /// WriteLine() 함수의 파라미터는 params object[] 이기 때문에
    /// 콤마로 구분하면서 무작정 나열하기만 하면 된다.
    ///
    /// 내부적으로 System.Diagnostics.Trace.WriteLine() 함수를 사용하기 때문에
    /// 비주얼 스튜디오의 Output Window 로 출력한다.
    /// 동시에 로그 파일로도 출력한다.
    ///
    /// 응용 프로그램을 여러 개 동시에 실행할 경우 각각을 구분하기 위해
    /// 파일이름에 '년월일시분초'를 포함한다.
    ///
    /// 배열 데이터는 각 항목별로 분리된 후 다시 한 줄로 출력한다.
    /// 여러 줄로 구성된 텍스트는 각 줄마다 분리된 후 다시 한 줄로 출력한다.
    ///
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough()]
    public class Log
    {
        // 로그 출력 파일.
        static private string logFilePath = "";
 
        // 일련번호
        static private long seqNo = -1;

        public enum LOG_LEVEL
        {
            NO = 0,
            ERROR = 2,
            WARNING = 4,
            INFO = 6,
            DEBUG = 8,
            FULL = 8,
        };

        //static Log()
        //{
        //    logFilePath = Constants.PROJECT_HOME + @"\log\" + Constants.LOG_FILENAME;
        //    FileInfo fi = new FileInfo(logFilePath);
        //    fi.Directory.Create();

        //    // 파일을 등록한다.
        //    System.Diagnostics.Trace.Listeners.Add(new System.Diagnostics.TextWriterTraceListener(new FileStream(logFilePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)));
        //}

        /// <summary>
        ///
        /// </summary>
        /// <param name="values"></param>
        public static void WriteLine(LOG_LEVEL nLogLevel, params object[] values)
        {
            if ((LOG_LEVEL)Constants.LOG_LEVEL < nLogLevel) return;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
 
            using (StringWriter writer = new StringWriter(sb))
            {
                System.Collections.ArrayList list = new System.Collections.ArrayList();
 
                if (values == null)
                {
                    list.Add("(null)");
                }
                else
                {
                    Split(list, values);
                }
 
                for (int i = 0; i < list.Count; ++i)
                {
                    if (i != 0)
                    {
                        writer.Write(Constants.LOG_DELIM);
                    }
 
                    writer.Write(list[i].ToString());
                }
            }

            string msg = Constants.LOG_DELIM + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + Constants.LOG_DELIM + System.Threading.Interlocked.Increment(ref seqNo).ToString("D19") + Constants.LOG_DELIM + nLogLevel.ToString() + Constants.LOG_DELIM + sb.ToString() + Constants.LOG_DELIM;
 
            // 출력
            //System.Diagnostics.Trace.WriteLine(msg);
            //System.Diagnostics.Trace.Flush();

            logFilePath = Constants.PROJECT_HOME + @"\log\" + Constants.LOG_FILENAME;
            File.AppendAllText(logFilePath, msg + "\r\n");
        }
 
        /// <summary>
        /// 배열의 항목을 분리하여 list 에 넣는다.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="array"></param>
        private static void Split(System.Collections.ArrayList list, System.Array array)
        {
            if (array.Length == 0)
            {
                list.Add("(null)");
            }
            else
            {
                foreach (object value in array)
                {
                    if (value == null)
                    {
                        list.Add("(null)");
                    }
                    else if (value is Array)
                    {
                        Split(list, value as Array);
                    }
                    else
                    {
                        string lines = value.ToString();
 
                        if (lines.Length == 0)
                        {
                            list.Add("");
                        }
                        else
                        {
                            System.Collections.ArrayList temp = new System.Collections.ArrayList();
 
                            using (StringReader reader = new StringReader(lines))
                            {
                                while (true)
                                {
                                    string line = reader.ReadLine();
 
                                    if (line == null)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        temp.Add(line);
                                    }
                                }
                            }
 
                            if (temp.Count == 1)
                                list.Add(temp[0]);
                            else
                                Split(list, temp.ToArray());
                        }
                    }
                }
            }
        }
    }
}
