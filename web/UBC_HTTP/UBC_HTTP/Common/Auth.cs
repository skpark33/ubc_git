﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Services;

namespace UBC_HTTP
{
    public class Auth
    {
        static public string CheckId(WebService webService, string id, string pw)
        {
            if (string.IsNullOrEmpty(Constants.USER_PATH(id)))
            {
                return "Login failed. (No Account)";
            }

            if (Constants.USER_PWD(id) != pw)
            {
                return "Login failed. (Password is incorrect)";
            }

            if (webService.Session["valid"] == null)
            {
                webService.Session["valid"] = id;
            }

            return null;
        }

        static public string CheckSession(WebService webService, string id, string pw)
        {
            //if (string.IsNullOrEmpty(Constants.USER_PATH(id)))
            //{
            //    return "Not logged in";
            //}

            //if (Constants.USER_PWD(id) != pw)
            //{
            //    return "Not logged in";
            //}
            /* skpark 2013.4.17 세션체크 없이 그냥 ID 체크만 한다.
             * 
            if (webService.Session["valid"] == null)
            {
                return "Not logged in";
            }
            return null;
            */
            return CheckId(webService, id, pw);
        }
    }
}
