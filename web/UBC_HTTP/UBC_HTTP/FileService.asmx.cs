﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.IO;
using System.Xml.Serialization;

namespace UBC_HTTP
{
    /// <summary>
    /// FileService의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://sqisoft.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    // [System.Web.Script.Services.ScriptService]
    public class FileService : System.Web.Services.WebService
    {
        //Log serviceLog = new Log("FileService");
        public FileService()
        {
            Constants.Init(); //skpark add
        }
 
        private string GetClientIP()
        {
            string ipAddr = Context.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
            if (ipAddr == null)
                ipAddr = Context.Request.ServerVariables.Get("REMOTE_ADDR");
            return ipAddr;
        }

        [WebMethod(Description="웹서비스가 잘 호출 되는 상황인지 테스트")]
        public string Test()
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService Test - " + Server.MachineName);
            return "FileService Test - " + this.Server.MachineName;
        }

        [WebMethod(EnableSession = true, Description = "로그인")]
        public FileResponse Login(string id, string pw)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "Login", id);

            string errorMsg = Auth.CheckId(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                return new FileResponse(false, errorMsg, "", "", null);
            }

            FileResponse response = new FileResponse(true, "", "TEXT", "OK", null);

            //if (Session["valid"] == null)
            //{
            //    Session["valid"] = id;
            //    response.resultData = "OK";
            //}
            //else
            //{
            //    response.resultData = "Loged";
            //}

            if(!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "Login", response.result, response.errorMsg);

            return response;
        }

        [WebMethod(EnableSession = true, Description = "로그아웃")]
        public void Logout()
        {
            if (Session["valid"] != null)
            {
                Session["valid"] = null;
            }
        }

        [WebMethod(EnableSession = true, Description = "파일 복사")]
        public FileResponse CopyFile(string id, string pw, string srcFile, string dstFile, bool overwrite)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "CopyFile", srcFile, dstFile, overwrite);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "CopyFile", response.result, response.errorMsg);
                return response;
            }

            try
            {
                File.Copy(Constants.USER_PATH(id) + srcFile, Constants.USER_PATH(id) + dstFile, overwrite);
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "CopyFile", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "", "", null);
        }

        [WebMethod(EnableSession = true, Description = "파일 이동")]
        public FileResponse MoveFile(string id, string pw, string srcFile, string dstFile)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "MoveFile", srcFile, dstFile);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "MoveFile", response.result, response.errorMsg);
                return response;
            }

            try
            {
                File.Move(Constants.USER_PATH(id) + srcFile, Constants.USER_PATH(id) + dstFile);
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "MoveFile", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "", "", null);
        }

        [WebMethod(EnableSession = true, Description = "파일 삭제")]
        public FileResponse DeleteFile(string id, string pw, string srcFile)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "DeleteFile", srcFile);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "DeleteFile", response.result, response.errorMsg);
                return response;
            }

            try
            {
                File.Delete(Constants.USER_PATH(id) + srcFile);
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "DeleteFile", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "", "", null);
        }

        [WebMethod(EnableSession = true, Description = "파일 목록")]
        public FileResponse FileList(string id, string pw, string path, bool topDirectoryOnly)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "FileList", path);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "FileList", response.result, response.errorMsg);
                return response;
            }

            //if (Session["valid"] == null) return new FileResponse(false, "fail", "", "", null);

            DataSet ds = new DataSet("FileSystemList");
            DataTable dtDir = ds.Tables.Add("DirectoryInfo");
            DataTable dtFile = ds.Tables.Add("FileInfo");

            try
            {
                foreach (string fileName in Directory.GetFiles(Constants.USER_PATH(id) + path))
                {
                    FileInfo fileInfo = new FileInfo(fileName);

                    if (dtFile.Columns.Count <= 0)
                    {
                        dtFile.Columns.Add("DirectoryName", fileInfo.DirectoryName.GetType());
                        dtFile.Columns.Add("FullName", fileInfo.FullName.GetType());
                        dtFile.Columns.Add("Name", fileInfo.Name.GetType());
                        dtFile.Columns.Add("Extension", fileInfo.Extension.GetType());
                        dtFile.Columns.Add("Length", fileInfo.Length.GetType());
                        dtFile.Columns.Add("CreationTime", fileInfo.CreationTime.GetType());
                        dtFile.Columns.Add("LastAccessTime", fileInfo.LastAccessTime.GetType());
                        dtFile.Columns.Add("LastWriteTime", fileInfo.LastWriteTime.GetType());
                        dtFile.Columns.Add("Attributes", fileInfo.Attributes.GetType());
                    }

                    dtFile.Rows.Add(fileInfo.DirectoryName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/') + "/"
                                  , fileInfo.FullName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/')
                                  , fileInfo.Name, fileInfo.Extension
                                  , fileInfo.Length
                                  , fileInfo.CreationTime, fileInfo.LastAccessTime, fileInfo.LastWriteTime, fileInfo.Attributes);
                }

                foreach (string dir in Directory.GetDirectories(Constants.USER_PATH(id) + path, "*", (topDirectoryOnly ? SearchOption.TopDirectoryOnly : SearchOption.AllDirectories)))
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(dir);

                    if (dtDir.Columns.Count <= 0)
                    {
                        dtDir.Columns.Add("FullName", dirInfo.FullName.GetType());
                        dtDir.Columns.Add("Name", dirInfo.Name.GetType());
                        dtDir.Columns.Add("Extension", dirInfo.Extension.GetType());
                        dtDir.Columns.Add("CreationTime", dirInfo.CreationTime.GetType());
                        dtDir.Columns.Add("LastAccessTime", dirInfo.LastAccessTime.GetType());
                        dtDir.Columns.Add("LastWriteTime", dirInfo.LastWriteTime.GetType());
                        dtDir.Columns.Add("Attributes", dirInfo.Attributes.GetType());
                    }

                    dtDir.Rows.Add(dirInfo.FullName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/') + "/"
                                 , dirInfo.Name, dirInfo.Extension
                                 , dirInfo.CreationTime, dirInfo.LastAccessTime, dirInfo.LastWriteTime, dirInfo.Attributes);

                    if (topDirectoryOnly) continue;

                    foreach (string fileName in Directory.GetFiles(dir))
                    {
                        FileInfo fileInfo = new FileInfo(fileName);

                        if (dtFile.Columns.Count <= 0)
                        {
                            dtFile.Columns.Add("DirectoryName", fileInfo.DirectoryName.GetType());
                            dtFile.Columns.Add("FullName", fileInfo.FullName.GetType());
                            dtFile.Columns.Add("Name", fileInfo.Name.GetType());
                            dtFile.Columns.Add("Extension", fileInfo.Extension.GetType());
                            dtFile.Columns.Add("Length", fileInfo.Length.GetType());
                            dtFile.Columns.Add("CreationTime", fileInfo.CreationTime.GetType());
                            dtFile.Columns.Add("LastAccessTime", fileInfo.LastAccessTime.GetType());
                            dtFile.Columns.Add("LastWriteTime", fileInfo.LastWriteTime.GetType());
                            dtFile.Columns.Add("Attributes", fileInfo.Attributes.GetType());
                        }

                        dtFile.Rows.Add(fileInfo.DirectoryName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/') + "/"
                                      , fileInfo.FullName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/')
                                      , fileInfo.Name, fileInfo.Extension
                                      , fileInfo.Length
                                      , fileInfo.CreationTime, fileInfo.LastAccessTime, fileInfo.LastWriteTime, fileInfo.Attributes);
                    }
                }
            }
            catch(Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "FileList", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "XML", ds.GetXml(), null);
        }

        [WebMethod(EnableSession = true, Description = "파일 정보")]
        public FileResponse FileInfo(string id, string pw, string fileName)
        {
            //Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "FileInfo", fileName);

            //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService start", "FileInfo", "skpark 1");

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "FileInfo", response.result, response.errorMsg);
                return response;
            }

            //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService start", "FileInfo", "skpark 2");

            //if (Session["valid"] == null) return new FileResponse(false, "fail", "", "", null);

            DataSet ds = new DataSet("FileInfoList");
            DataTable dt = ds.Tables.Add("FileInfo");

            //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService start", "FileInfo", "skpark 3");

            try
            {
                //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService start", "FileInfo", "skpark 4");
                FileInfo fileInfo = new FileInfo(Constants.USER_PATH(id) + fileName);
                //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService start", "FileInfo", "skpark 5");

                if (dt.Columns.Count <= 0)
                {
                    dt.Columns.Add("DirectoryName", fileInfo.DirectoryName.GetType());
                    dt.Columns.Add("FullName", fileInfo.FullName.GetType());
                    dt.Columns.Add("Name", fileInfo.Name.GetType());
                    dt.Columns.Add("Extension", fileInfo.Extension.GetType());
                    dt.Columns.Add("Length", fileInfo.Length.GetType());
                    dt.Columns.Add("CreationTime", fileInfo.CreationTime.GetType());
                    dt.Columns.Add("LastAccessTime", fileInfo.LastAccessTime.GetType());
                    dt.Columns.Add("LastWriteTime", fileInfo.LastWriteTime.GetType());
                    dt.Columns.Add("Attributes", fileInfo.Attributes.GetType());
                }

                //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService start", "FileInfo", "skpark 6");

                dt.Rows.Add(fileInfo.DirectoryName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/') + "/"
                          , fileInfo.FullName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/')
                          , fileInfo.Name, fileInfo.Extension
                          , fileInfo.Length
                          , fileInfo.CreationTime, fileInfo.LastAccessTime, fileInfo.LastWriteTime, fileInfo.Attributes);

                //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService start", "FileInfo", "skpark 7");

            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "FileInfo", response.result, response.errorMsg);
                return response;
            }
            //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService start", "FileInfo", "skpark 8");

            return new FileResponse(true, "", "XML", ds.GetXml(), null);
        }

        [WebMethod(EnableSession = true, Description = "파일 정보")]
        public FileResponse FilesInfo(string id, string pw, string fileList)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "FilesInfo", fileList);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "FilesInfo", response.result, response.errorMsg);
                return response;
            }

            //if (Session["valid"] == null) return new FileResponse(false, "fail", "", "", null);

            DataSet ds = new DataSet("FileInfoList");
            DataTable dt = ds.Tables.Add("FileInfo");

            try
            {
                // skpark files_info_limit filesInfo 가 과부하로 time_out 되는 것을 막기위해, 제한을 둔다.
                int files_info_limit = Constants.FILES_INFO_LIMIT;
                int limit_counter = 0;  
                foreach (string fileName in fileList.Split(new char[]{'|'}, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (!File.Exists(Constants.USER_PATH(id) + fileName)) continue;

                    FileInfo fileInfo = new FileInfo(Constants.USER_PATH(id) + fileName);

                    if (dt.Columns.Count <= 0)
                    {
                        dt.Columns.Add("DirectoryName", fileInfo.DirectoryName.GetType());
                        dt.Columns.Add("FullName", fileInfo.FullName.GetType());
                        dt.Columns.Add("Name", fileInfo.Name.GetType());
                        dt.Columns.Add("Extension", fileInfo.Extension.GetType());
                        dt.Columns.Add("Length", fileInfo.Length.GetType());
                        dt.Columns.Add("CreationTime", fileInfo.CreationTime.GetType());
                        dt.Columns.Add("LastAccessTime", fileInfo.LastAccessTime.GetType());
                        dt.Columns.Add("LastWriteTime", fileInfo.LastWriteTime.GetType());
                        dt.Columns.Add("Attributes", fileInfo.Attributes.GetType());
                    }

                    dt.Rows.Add(fileInfo.DirectoryName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/') + "/"
                              , fileInfo.FullName.Substring(Constants.USER_PATH(id).Length).Replace('\\', '/')
                              , fileInfo.Name, fileInfo.Extension
                              , fileInfo.Length
                              , fileInfo.CreationTime, fileInfo.LastAccessTime, fileInfo.LastWriteTime, fileInfo.Attributes);

                    // skpark files_info_limit filesInfo 가 과부하로 time_out 되는 것을 막기위해, 제한을 둔다.
                    limit_counter++;
                    //Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "FileService", "FilesInfo", fileInfo.Name, limit_counter);
                    if (files_info_limit > 0 && limit_counter >= files_info_limit)
                    {
                        Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService", "FilesInfo", "OVER FILES_INFO_LIMIT", Constants.FILES_INFO_LIMIT);
                        break;
                    }

                }
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "FilesInfo", response.result, response.errorMsg);
                return response;
            }
            //skpark temp debug code
            //Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService", "FilesInfo", "DATA=", ds.GetXml());
            //
            return new FileResponse(true, "", "XML", ds.GetXml(), null);
        }

        [WebMethod(EnableSession = true, Description = "파일 존재 여부")]
        public FileResponse FileExist(string id, string pw, string fileName)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "FileExist", fileName);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "FileExist", response.result, response.errorMsg);
                return response;
            }

            bool fileExist = false;
            try
            {
                fileExist = File.Exists(Constants.USER_PATH(id) + fileName);
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "FileExist", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(fileExist, "", "", "", null);
        }

        [WebMethod(EnableSession = true, Description = "디렉토리 복사")]
        public FileResponse CopyDirectory(string id, string pw, string srcPath, string dstPath, bool overwrite)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "CopyDirectory", srcPath, dstPath, overwrite);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "CopyDirectory", response.result, response.errorMsg);
                return response;
            }

            try
            {
                CopyFolder(Constants.USER_PATH(id) + srcPath, Constants.USER_PATH(id) + dstPath, overwrite);
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "CopyDirectory", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "", "", null);
        }

        public void CopyFolder(string srcPath, string dstPath, bool overwrite)
        {
            if (!Directory.Exists(dstPath))
            {
                Directory.CreateDirectory(dstPath);
            }

            string[] files = Directory.GetFiles(srcPath);
            string[] folders = Directory.GetDirectories(srcPath);

            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                string dest = Path.Combine(dstPath, name);
                File.Copy(file, dest, overwrite);
            }

            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(dstPath, name);
                CopyFolder(folder, dest, overwrite);
            }
        }

        [WebMethod(EnableSession = true, Description = "디렉토리 이동")]
        public FileResponse MoveDirectory(string id, string pw, string srcPath, string dstPath)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "MoveDirectory", srcPath, dstPath);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "MoveDirectory", response.result, response.errorMsg);
                return response;
            }

            try
            {
                Directory.Move(Constants.USER_PATH(id) + srcPath, Constants.USER_PATH(id) + dstPath);
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "MoveDirectory", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "", "", null);
        }

        [WebMethod(EnableSession = true, Description = "디렉토리 삭제")]
        public FileResponse DeleteDirectory(string id, string pw, string srcPath)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "DeleteDirectory", srcPath);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "DeleteDirectory", response.result, response.errorMsg);
                return response;
            }

            try
            {
                Directory.Delete(Constants.USER_PATH(id) + srcPath, true);
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "DeleteDirectory", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "", "", null);
        }

        [WebMethod(EnableSession = true, Description = "파일 전송")]
        public FileResponse PutFile(string id, string pw, string file, long offset, int size, byte[] context)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "PutFile", file, offset, size);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "PutFile", response.result, response.errorMsg);
                return response;
            }

            string path = Path.GetDirectoryName(Constants.USER_PATH(id) + file);
            
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                //파일처리
                using (MemoryStream ms = new MemoryStream(context))
                {
                    FileInfo fileInfo = new FileInfo(Constants.USER_PATH(id) + file);
                    if(!fileInfo.Exists)
                    {
                        using (FileStream fs = fileInfo.Create())
                        {
                            ms.WriteTo(fs);
                            ms.Close();
                            fs.Close();
                        }
                    }
                    else
                    {
                        using (FileStream fs = fileInfo.OpenWrite())
                        {
                            fs.Seek(offset, SeekOrigin.Begin);
                            ms.WriteTo(fs);

                            ms.Close();
                            fs.Close();
                        }
                    }
                }
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "PutFile", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "", "", null);
        }

        [WebMethod(EnableSession = true, Description = "파일 수신")]
        public FileResponse GetFile(string id, string pw, string file, long offset, int size)
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "FileService start", "GetFile", file, offset, size);

            string errorMsg = Auth.CheckSession(this, id, pw);
            if (!string.IsNullOrEmpty(errorMsg))
            {
                FileResponse response = new FileResponse(false, errorMsg, "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "GetFile", response.result, response.errorMsg);
                return response;
            }

            int nByteRead = 0;
            byte[] context = null;

            try
            {
                FileInfo info = new FileInfo(Constants.USER_PATH(id) + file);
                if (!info.Exists) return null;

                if (info.Length <= offset) return null;

                using (FileStream fs = info.OpenRead())
                {
                    if ((info.Length - offset) < size)
                    {
                        size = (int)(info.Length - offset);
                    }

                    context = new byte[size];
                    fs.Seek(offset, SeekOrigin.Begin);
                    nByteRead = fs.Read(context, 0, size);
                    fs.Close();
                }
            }
            catch (Exception err)
            {
                FileResponse response = new FileResponse(false, err.ToString(), "", "", null);
                if (!response.result) Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "FileService end", "GetFile", response.result, response.errorMsg);
                return response;
            }

            return new FileResponse(true, "", "BINARY", "", context);
        }
    }

    // 웹 메서드 반환 형식
    [XmlRoot("fileResponse", Namespace = "http://sqisoft.com/")]
    public class FileResponse
    {
        public FileResponse()
        {
            result = false;
            errorMsg = null;
            resultType = null;
            resultData = null;
            resultBytes = null;
        }

        public FileResponse(bool inResult, string inErrorMsg, string inResultType, string inResultData, byte[] inResultBytes)
        {
            result = inResult;
            errorMsg = inErrorMsg;
            resultType = inResultType;
            resultData = inResultData;
            resultBytes = inResultBytes;
        }

        [XmlElement("result", IsNullable = false)]
        public bool result;
        [XmlElement("errorMsg", IsNullable = true)]
        public string errorMsg;
        [XmlElement("resultType", IsNullable = true)]
        public string resultType;
        [XmlElement("resultData", IsNullable = true)]
        public string resultData;
        [XmlElement("resultBytes", IsNullable = true)]
        public byte[] resultBytes;
    }
}
