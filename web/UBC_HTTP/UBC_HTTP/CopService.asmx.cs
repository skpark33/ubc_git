﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.Text;

namespace UBC_HTTP
{
    /// <summary>
    /// CopService의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://sqisoft.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    // [System.Web.Script.Services.ScriptService]
    public class CopService : System.Web.Services.WebService
    {
        public CopService()
        {
            Constants.Init(); //skpark add
        }
        new public void Dispose() { }

        [WebMethod]
        public string Test()
        {
            Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "CopService Test - " + System.Environment.UserName + "@" + Server.MachineName);
            return "CopService Test - " + System.Environment.UserName + "@" + Server.MachineName;
        }

        //[WebMethod]
        //public string TestEnv(string key)
        //{
            
        //    return (string.IsNullOrEmpty(System.Environment.GetEnvironmentVariable(key)) ? "NULL" : System.Environment.GetEnvironmentVariable(key));
        //}

        private string GetClientIP()
        {
            string ipAddr = Context.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
            if (ipAddr == null)
                ipAddr = Context.Request.ServerVariables.Get("REMOTE_ADDR");
            return ipAddr;
        }

        [WebMethod(Description = "COP 서비스 호출")]
        public CopResponse CopServiceCall(string command, string ip, int port, string body, string query)
        {
            string internal_ip = Constants.NAT_IP(ip); // 공용IP --> 사설IP 변경용
            if (internal_ip == ip)
            {
                Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "CopService begin", command, ip, port, body, query);
            }
            else
            {
                ip = internal_ip;
                Log.WriteLine(Log.LOG_LEVEL.INFO, GetClientIP(), "CopService NAT", command, ip, port, body, query);
            }

            Command cmd = null;

            if      (command == "AGENT"  ) cmd = new CopCommand(new AgentSocketRcv(command, ip, port, body, query));
            else if (command == "TIME"   ) cmd = new CopCommand(new TimeSyncRcv(command, ip, port, body, query));
            else if (command == "CALL"   ) cmd = new CopCommand(new CciCallRcv(command, ip, port, body, query));
            else if (command == "GET")
            {
                if (Constants.COP_GET_MODE == "CALL")
                {
                    cmd = new CopCommand(new CciCallRcv(command, ip, port, body, query));
                }
                else
                {
                    cmd = new CopCommand(new CciGetRcv(command, ip, port, body, query));
                }
            }
            //skpark 2013.3.14  ENCODED_GET 은 base64로 decode해야 한다.
            else if (command == "ENCODED_GET")
            {
                //StringBuilder decodedQuery = new StringBuilder();
                //StringBuilder decodedBody = new StringBuilder();
                try
                {
                    body = System.Text.Encoding.GetEncoding(0).GetString(Convert.FromBase64String(body));
                    //byte[] body_output = Convert.FromBase64String(body);
                    //int body_len = body_output.Length;
                    //for (int i = 0; i < body_len; i++)
                    //{
                    //    decodedBody.Append(Convert.ToChar(body_output[i]));
                    //}
                    //body = decodedBody.ToString();

                    query = System.Text.Encoding.GetEncoding(0).GetString(Convert.FromBase64String(query));
                    //byte[] query_output = Convert.FromBase64String(query);
                    //int query_len = query_output.Length;
                    //for (int i = 0; i < query_len; i++)
                    //{
                    //    decodedQuery.Append(Convert.ToChar(query_output[i]));
                    //}
                    //query = decodedQuery.ToString();

                    if (body.Contains("System.Byte[]") || query.Contains("System.Byte[]"))
                    {
                        Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "CopService decoded fail", command, ip, port, body, query);
                        CopResponse decode_response = new CopResponse(false, "Decoding fail", "", "");
                        return decode_response;
                    }

                    Log.WriteLine(Log.LOG_LEVEL.WARNING, GetClientIP(), "CopService decoded succeed", command, ip, port, body, query);  //skpark temp_code
                }
                catch (Exception err)
                {
                    Log.WriteLine(Log.LOG_LEVEL.ERROR, GetClientIP(), "CopService decoded fail", command, ip, port, body, err.Message);
                    CopResponse decode_response = new CopResponse(false, "Decoding fail", "", "");
                    return decode_response;
                }
                //
                if (Constants.COP_GET_MODE == "CALL")
                {
                    cmd = new CopCommand(new CciCallRcv(command, ip, port, body, query));
                }
                else
                {
                    cmd = new CopCommand(new CciGetRcv(command, ip, port, body, query));
                }
            }
            else if (command == "ADDHAND") cmd = new CopCommand(new CciAddHandRcv(command, ip, port, body, query));
            else if (command == "EVENT") cmd = new CopCommand(new CciEventRcv(command, ip, port, body, query));
            else if (command == "HSR") cmd = new CopCommand(new ShellRcv(command, ip, port, body, query));
            
            CopResponse response = (cmd != null ? cmd.Execute() : new CopResponse(false, "Invalid command", "", ""));

            Log.WriteLine( (response.result ? Log.LOG_LEVEL.INFO : Log.LOG_LEVEL.ERROR), GetClientIP(), "CopService end", response.result, response.errorMsg, response.resultType, response.resultData);

            return response;
        }
    }
    
    // 웹 메서드 반환 형식
    [XmlRoot("copResponse", Namespace = "http://sqisoft.com/")]
    public class CopResponse
    {
        public CopResponse()
        {
            result = false;
            errorMsg = null;
            resultType = null;
            resultData = null;
        }

        public CopResponse(bool inResult, string inErrorMsg, string inResultType, string inResultData)
        {
            result = inResult;
            errorMsg = inErrorMsg;
            resultType = inResultType;
            resultData = inResultData;
        }

        [XmlElement("result", IsNullable = false)]
        public bool result;
        [XmlElement("errorMsg", IsNullable = true)]
        public string errorMsg;
        [XmlElement("resultType", IsNullable = true)]
        public string resultType;
        [XmlElement("resultData", IsNullable = true)]
        public string resultData;
    }
}
