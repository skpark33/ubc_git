@echo off
setlocal

set HOME_PATH=%CD%
for %%? in ("%~dp0..") do set HOME_PATH=%%~f?
echo HOME_PATH = [%HOME_PATH%]

set DefaultName=UBC
set DefaultSiteName="Default Web Site"
set DefaultSitePort=8080
set DefaultSitePath="%HOME_PATH%\Web\DefaultWebSite"
set ScreenShotPath="%HOME_PATH%\ScreenShot"
set PublicContentsPath="%HOME_PATH%\PublicContents"

set UbcHttpName=UBC_HTTP
set UbcHttpPath="%HOME_PATH%\Web\UBC_HTTP"
set UbcHttpAppName="Default Web Site/UBC_HTTP"

set UbcWebName=UBC_WEB
set UbcWebPort=80
set UbcWebPath="%HOME_PATH%\Web\UBC_WEB"
set UbcWebFilePath="%HOME_PATH%\Web\UBC_WEB\File"
set UbcNasUserID=ubcnasuser
set UbcNasUserPW=ubcnasuser

rem [%DefaultSiteName%] Setting

set /p installDefaultSite=[%DefaultSiteName%] Setting? (Y or N) :
if not defined installDefaultSite (goto skipDefaultSite)
if /I %installDefaultSite% NEQ Y (goto skipDefaultSite)

%systemroot%\system32\inetsrv\appcmd delete site /site.name:%DefaultSiteName%
%systemroot%\system32\inetsrv\appcmd add site /name:%DefaultSiteName% /bindings:http://*:%DefaultSitePort% /physicalPath:%DefaultSitePath%
mkdir %PublicContentsPath%
%systemroot%\system32\inetsrv\appcmd add vdir /app.name:%DefaultSiteName%/ /path:"/PublicContents" /physicalPath:%PublicContentsPath%
mkdir %ScreenShotPath%
%systemroot%\system32\inetsrv\appcmd add vdir /app.name:%DefaultSiteName%/ /path:"/ScreenShot" /physicalPath:%ScreenShotPath%
netsh advfirewall firewall delete rule name=%DefaultSiteName%
netsh advfirewall firewall add rule name=%DefaultSiteName% description=%DefaultName% dir=in protocol=TCP localport=%DefaultSitePort% action=allow
:skipDefaultSite

rem [UBC_HTTP WebService] Setting

set /p installUBC_HTTP=[UBC_HTTP WebService] Setting? (Y or N) :
if not defined installUBC_HTTP (goto skipUBC_HTTP)
if /I %installUBC_HTTP% NEQ Y (goto skipUBC_HTTP)

%systemroot%\system32\inetsrv\appcmd delete apppool /apppool.name:%UbcHttpName%
%systemroot%\system32\inetsrv\appcmd add apppool /name:%UbcHttpName% /processModel.identityType:LocalSystem /enable32BitAppOnWin64:true
%systemroot%\system32\inetsrv\appcmd delete app /app.name:%UbcHttpAppName%
%systemroot%\system32\inetsrv\appcmd add app /site.name:%DefaultSiteName% /path:/%UbcHttpName% /physicalPath:%UbcHttpPath% /applicationPool:%UbcHttpName%
:skipUBC_HTTP

rem [UBC_WEB WebSite] Setting

set /p installUBC_WEB=[UBC_WEB WebSite] Setting? (Y or N) :
if not defined installUBC_WEB (goto skipUBC_WEB)
if /I %installUBC_WEB% NEQ Y (goto skipUBC_WEB)

%systemroot%\system32\inetsrv\appcmd delete apppool /apppool.name:%UbcWebName%
%systemroot%\system32\inetsrv\appcmd add apppool /name:%UbcWebName% /processModel.identityType:LocalSystem

%systemroot%\system32\inetsrv\appcmd delete site /site.name:%UbcWebName%
%systemroot%\system32\inetsrv\appcmd add site /name:%UbcWebName% /bindings:http://*:%UbcWebPort% /physicalPath:%UbcWebPath% /applicationDefaults.applicationPool:%UbcWebName%

netsh advfirewall firewall delete rule name=%UbcWebName%
netsh advfirewall firewall add rule name=%UbcWebName% description=%DefaultName% dir=in protocol=TCP localport=%UbcWebPort% action=allow

rem [OS Account] Setting
net user %UbcNasUserID% %UbcNasUserPW% /add
cacls %UbcWebFilePath% /E /T /G %UbcNasUserID%:F
mkdir "%HOME_PATH%\Web\upload_temp"
cacls "%HOME_PATH%\Web\upload_temp" /E /T /G %UbcNasUserID%:F
:skipUBC_WEB

echo [IIS Site List]
%systemroot%\system32\inetsrv\appcmd list site
