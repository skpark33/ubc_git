﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
//using System.Collections.Specialized;

using System.Net.Mail;

public partial class guestbook : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string title = "KIA MOTORS ";
        string contents = "<html><body>";
        contents += "<hr>";
        contents += "<b>안녕하세요. 기아자동차 입니다.</b>";
        contents += "<br />";
        contents += "<b>Hello. This is from KIA MOTORS.</b>";
        contents += "<hr />";


        // 이미지 데이타가 있을경우,
        if (!string.IsNullOrEmpty(Request.Form["objByteData"]))
        {
            byte[] bteData = Convert.FromBase64String(Request.Form["objByteData"]);
            
            if (bteData != null && bteData.Length > 0)
            {
                string fileName = System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
                string SaveLocation = Server.MapPath(@"~\data\" + fileName);

                try
                {
                    File.WriteAllBytes(SaveLocation, bteData);
                }
                catch (Exception ex)
                {
                    //Response.Write(ex.Message + "<br />");
                    Response.Write("fail");
                    return;
                }

                contents += "<img src='http://" + Request.ServerVariables["SERVER_NAME"] + ":" + Request.ServerVariables["SERVER_PORT"] + "/Data/" + fileName + "' width=670px height=792px />";
            }
        }

        contents += "</body></html>";

        string returnMsg = "";
        if (sendEmail("localhost", "Brand@kia.co.kr", Request["email"], title, contents, true, out returnMsg))
        {
            //Response.Write(returnMsg);
            Response.Write("ok");
        }
        else 
        {
            Response.Write("fail");
        }
    }

    public bool sendEmail(    string server
                            , string from
                            , string to
                            , string Subject
                            , string Body
                            , bool isBodyHTML
                            , out string resultMsg
                         )
    {
        resultMsg = "";

        if ( string.IsNullOrEmpty( from ) )
        {
            resultMsg = "Sender email address is not exist";
            return false;
        }
        if (string.IsNullOrEmpty(to) ) 
        {
            resultMsg = "Recipient email address is not exist";
            return false;
        }

        Subject = string.IsNullOrEmpty( Subject ) ? "" : Subject;
        Body    = string.IsNullOrEmpty( Body    ) ? "" : Body   ;
        

        MailMessage message = new MailMessage();
        
        // 메일 제목
        message.Subject = Subject;

        // 메세지 BODY 부분처리(HTML or not)
        if (isBodyHTML == true)
        {
            message.IsBodyHtml = true;
            message.Body = Body.Replace("\r\n", "<br />");
        }
        else
        {
            message.IsBodyHtml = false;
            message.Body = Body;
        }

        // 인코딩
        message.SubjectEncoding = System.Text.Encoding.UTF8;
        message.BodyEncoding = System.Text.Encoding.UTF8;

        // 보내는주소
        to = to.Trim();
        string[] toAddresses = System.Text.RegularExpressions.Regex.Split(to, "[,;] *");
        foreach (string toAddress in toAddresses)
        {
            message.To.Add(new MailAddress(toAddress));
        }

        // 받는주소
        message.From = new MailAddress(from.Trim());

        // 서버 주소 셋팅
        SmtpClient client = new SmtpClient(string.IsNullOrEmpty(server) ? "localhost" : server);

        try
        {
            client.Send(message);
            resultMsg = "Email has sent successfully!! ";

            return true;
        }
        catch (Exception ex)
        {
            resultMsg = String.Format("Email sending has failed: {0} - {1}", ex.ToString(), ex.Message);

            return false;
        }

    }
}
