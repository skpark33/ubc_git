﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Security;

namespace DID.Common.Framework
{
    public class BasePage : System.Web.UI.Page
    {
        /// <summary>
        /// 사용자정보
        /// </summary>
        protected UserInfoContext UserInfo = null;

        protected string DOMAIN
        {
            get { return ConfigurationManager.AppSettings["DOMAIN"]; }

        }

        protected string PageClassName
        {
            get { return this.GetType().Name.Replace("_aspx", ""); }
        }

        protected string ACTION_URL
        {
            
            get { 

                return string.Format("ajax/{0},{1}.ashx"
                                    , this.PageClassName
                                    , this.GetType().Assembly.GetName().Name
                    );}
        }

        protected string FILE_PATH_NOTICE
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_NOTICE"]; }
        }

        protected string FILE_PATH_PROGRAM
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_PROGRAM"]; }
        }

        protected string FILE_PATH_VIDEO
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_VIDEO"]; }
        }

        protected string FILE_PATH_CONTENTS
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_CONTENTS"]; }
        }

        protected string FILE_PATH_COMMON
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_COMMON"]; }
        }

        // 2011.6.20 임유석 시작 - Pop 게시판 화일 저장 PATH
        protected string FILE_PATH_POP
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_POP"]; }
        }
        // 2011.06.20 임유석 끝


        // 2011.7.26 임유석 시작 - ROOT_SITE_ID
        protected string ROOT_SITE_ID
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["ROOT_SITE_ID"]; }
        }
        // 2011.7.26 임유석 끝


        /// <summary>
        /// 초기화
        /// </summary>
        protected override void OnPreInit(EventArgs e)
        {
            UserInfo = new UserInfoContext();


            base.OnPreInit(e);
        }

        protected string LANG_FLAG
        {
            get { return UserInfo.LANG_FLAG; }
        }

        /// <summary>
        /// Global Resource 조회
        /// </summary>
        protected internal string GetGResource(string key)
        {
            return this.GetGlobalResourceObject("agroResource", key).ToString();
        }

        /// <summary>
        /// 페이지 접근 권한
        /// </summary>
        protected void IsAccessPageAuth()
        {

        }

        /// <summary>
        /// 스크립트 실행
        /// </summary>
        protected void ScriptExecute(string msg)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }

        /// <summary>
        /// 스크립트 실행
        /// </summary>
        protected void ScriptExecuteGoHome(string msg)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');location.href='/';", msg), true);
        }

        /// <summary>
        /// 스크립트 실행
        /// </summary>
        protected void ScriptExecuteUrl(string msg,string url)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');location.href='{1}'", msg, url), true);
        }

        /// <summary>
        /// 스크립트 실행
        /// </summary>
        protected void ScriptExecuteAction(string msg, string func)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');{1}", msg, func), true);
        }
        /// 스크립트 실행
        /// </summary>
        protected void ScriptExecuteFunc(string func)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("{0}",func), true);
        }
        /// <summary>
        /// 페이지에서 엔터키를 눌렀을 때, 기본적으로 실행될 포스트백 스크립트를 지정한 컨트롤과 인자를 사용하여 생성한다.
        /// </summary>
        /// <param name="control">컨트롤 개체</param>
        public void SetOnEnterPressed(Control control)
        {
            this.Context.Items["__ENTER_SCRIPT__"] = this.Page.ClientScript.GetPostBackEventReference(control, string.Empty);
        }


        /// <summary>
        /// 로그인한 상태인지 아닌지 가져온다. 
        /// </summary>
        /// <returns></returns>
        public bool CheckLoginYn()
        {
            if (HttpContext.Current.Request.Cookies["UserInfo"] == null)
                return false;
            else
                return true;
        }

        /// <summary>
        /// TextBox를 readonly로..
        /// </summary>
        protected void ReadOnlyTextBox(params TextBox[] obj)
        {
            for (int i = 0; i < obj.Length; i++)
            {
                obj[i].Attributes["readonly"] = "readonly";
                obj[i].Attributes["style"] = "background-color:#f0f0f0;";
            }
        }
    }
}
