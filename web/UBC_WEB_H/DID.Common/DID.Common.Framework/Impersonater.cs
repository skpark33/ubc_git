﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace DID.Common.Framework
{

    public class Impersonater
    {
        // Fields
        private string Domain = string.Empty;
        private WindowsImpersonationContext impersonationContext;
        public const int LOGON32_LOGON_INTERACTIVE = 2;
        public const int LOGON32_PROVIDER_DEFAULT = 0;
        private string Password = string.Empty;
        private string UserName = string.Empty;

        // Methods
        public Impersonater()
        {
            this.UserName = ConfigurationManager.AppSettings["FileAuthUser"];
            this.Password = ConfigurationManager.AppSettings["FileAuthUserPwd"];
        }

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool CloseHandle(IntPtr handle);
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(IntPtr hToken, int impersonationLevel, ref IntPtr hNewToken);
        public bool ImpersonateValidUser()
        {
            if (!this.UserName.Equals(string.Empty))
            {
                IntPtr zero = IntPtr.Zero;
                IntPtr hNewToken = IntPtr.Zero;
                if ((RevertToSelf() && (LogonUserA(this.UserName, this.Domain, this.Password, 2, 0, ref zero) != 0)) && (DuplicateToken(zero, 2, ref hNewToken) != 0))
                {
                    this.impersonationContext = new WindowsIdentity(hNewToken).Impersonate();
                    if (this.impersonationContext != null)
                    {
                        CloseHandle(zero);
                        CloseHandle(hNewToken);
                        return true;
                    }
                }
                if (zero != IntPtr.Zero)
                {
                    CloseHandle(zero);
                }
                if (hNewToken != IntPtr.Zero)
                {
                    CloseHandle(hNewToken);
                }
            }
            return false;
        }

        [DllImport("advapi32.dll")]
        public static extern int LogonUserA(string lpszUserName, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RevertToSelf();
        public void UndoImpersonation()
        {
            if (!this.UserName.Equals(string.Empty) && (this.impersonationContext != null))
            {
                this.impersonationContext.Undo();
            }
        }

    }
}