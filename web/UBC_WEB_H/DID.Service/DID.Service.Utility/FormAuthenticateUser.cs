﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Data;

using DID.Common.Framework;

namespace DID.Service.Utility
{
    public class FormAuthenticateUser
    {
        UserInfoContext userInfoContext = new UserInfoContext();

        public string UserID { get; set; }
        public string UserPwd { get; set; }
        public string CompType { get; set; }
        public string LangFlag { get; set; }
        public string MgrId { get; set; }
        public string SiteId { get; set; }

        public FormAuthenticateUser() { }


        public void CreateAuthenticationTicket()
        {
            try
            {
                string userID = userInfoContext.USER_ID;

                FormsAuthenticationTicket AuthenticationTicket = new FormsAuthenticationTicket(this.UserID, false, 720);
                string EncryptedTicket = FormsAuthentication.Encrypt(AuthenticationTicket);

                HttpCookie AuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);
                HttpContext.Current.Response.Cookies.Add(AuthenticationCookie);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable CheckUserValidation()
        {
            using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
            {
                return obj.ValidateUser(this.MgrId, this.SiteId, this.UserID, this.UserPwd);
            }
        }

        public DataTable GetUserInfo()
        {
            using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
            {
                return obj.GetUserInfo(this.MgrId, this.SiteId, this.UserID);
            }
        }

        public void SetUserInfoContext(string key, string value)
        {
            HttpContext.Current.Response.Cookies["UserInfo"].Values.Set(key, value);
        }

        #region Profile을 사용하므로 본 메서드는 미사용

        //public void SetUserInfoContext()
        //{
        //    DataTable dt = null;

        //    using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
        //    {
        //        //dt = obj.Select(this.UserID);
        //    }


        //    if (dt.Rows.Count > 0)
        //    {
        //        foreach (DataColumn dcUserInfo in dt.Columns)
        //        {
        //            if (HttpContext.Current.Response.Cookies["UserInfo"] == null)
        //            {
        //                HttpContext.Current.Response.Cookies["UserInfo"][dcUserInfo.ColumnName] = HttpContext.Current.Server.UrlEncode(dt.Rows[0][dcUserInfo.ColumnName].ToString());
        //            }
        //            else
        //            {

        //                HttpContext.Current.Response.Cookies["UserInfo"].Values.Set(dcUserInfo.ColumnName, HttpContext.Current.Server.UrlEncode(dt.Rows[0][dcUserInfo.ColumnName].ToString()));
        //            }

        //        }

        //        HttpContext.Current.Response.Cookies["UserInfo"].Values.Set("COMP_TYPE", HttpContext.Current.Server.UrlEncode(this.CompType));
        //        HttpContext.Current.Response.Cookies["UserInfo"].Values.Set("LANG_FLAG", HttpContext.Current.Server.UrlEncode(this.LangFlag));
        //    }
        #endregion
    }
}
