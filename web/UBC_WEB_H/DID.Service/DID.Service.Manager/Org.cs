﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Manager
{ 
    public class Org : IDisposable 
    {
        private Database db = null;

        public Org()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() {}

        /// <summary>
        /// 
        /// </summary>
        public DataTable GetOrgInfo(string siteId, string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manager_Org_Sel");

            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 


            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        /// <summary>
        /// 
        /// </summary>
        public DataTable GetOrgList(string parentId, string businessType, string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manager_Org_List");

            db.AddInParameter(Cmd, "@parentId", DbType.String, parentId);
            db.AddInParameter(Cmd, "@businessType", DbType.String, businessType);
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 


            return db.ExecuteDataSet(Cmd).Tables[0];
        }
		
        public DataTable GetOrgCode(string businessType, string siteId , string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manager_Org_Code");

            db.AddInParameter(Cmd, "@businessType", DbType.String, businessType);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 


            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        
        // 2011-07-15 임유석 추가
        // DB 정리로 인해서 businessType 을 사용안하고 parentID만을 이용해서 조직 코드 조회하도록 함
        public DataTable GetOrgCode(string parentSiteId, string franchizeType)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manager_Org_Code");

            db.AddInParameter(Cmd, "@parentId", DbType.String, parentSiteId);
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            
            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        // 2011-07-29 임유석 추가
        // 부모조직 하위의 입력 가능한 businessType 을 가져온다
        public DataTable GetBusinessTypeCode(string parentSiteId, string franchizeType)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manager_Org_BusiType_Code");

            db.AddInParameter(Cmd, "@parentId", DbType.String, parentSiteId);
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            
            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public int Insert(string mgrId
                         , string siteId
                         , string siteName
                         , string phoneNo1		
                         , string businessType	
                         , string businessCode	
                         , string shopOpenTime	
                         , string shopCloseTime	
                         , string holiday
                         , string comment1
                         , string comment2
                         , string parentId
                         , string ROOT_SITE_ID
                        )
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manager_Org_Reg");

            db.AddInParameter(Cmd, "@mgrId", DbType.String, mgrId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@siteName", DbType.String, siteName);
            db.AddInParameter(Cmd, "@phoneNo1", DbType.String, phoneNo1);
            db.AddInParameter(Cmd, "@businessType", DbType.String, businessType);
            db.AddInParameter(Cmd, "@businessCode", DbType.String, businessCode);
            db.AddInParameter(Cmd, "@shopOpenTime", DbType.String, shopOpenTime);
            db.AddInParameter(Cmd, "@shopCloseTime", DbType.String, shopCloseTime);
            db.AddInParameter(Cmd, "@holiday", DbType.String, holiday);
            db.AddInParameter(Cmd, "@comment1", DbType.String, comment1);
            db.AddInParameter(Cmd, "@comment2", DbType.String, comment2);
            db.AddInParameter(Cmd, "@parentId", DbType.String, parentId);

            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 


            return db.ExecuteNonQuery(Cmd);
        }

        public int Update(string mgrId
                         , string siteId
                         , string siteName
                         , string phoneNo1
                         , string businessType
                         , string businessCode
                         , string shopOpenTime
                         , string shopCloseTime
                         , string holiday
                         , string comment1
                         , string comment2
                         , string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manager_Org_Upd");

            db.AddInParameter(Cmd, "@mgrId", DbType.String, mgrId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@siteName", DbType.String, siteName);
            db.AddInParameter(Cmd, "@phoneNo1", DbType.String, phoneNo1);
            db.AddInParameter(Cmd, "@businessType", DbType.String, businessType);
            db.AddInParameter(Cmd, "@businessCode", DbType.String, businessCode);
            db.AddInParameter(Cmd, "@shopOpenTime", DbType.String, shopOpenTime);
            db.AddInParameter(Cmd, "@shopCloseTime", DbType.String, shopCloseTime);
            db.AddInParameter(Cmd, "@holiday", DbType.String, holiday);
            db.AddInParameter(Cmd, "@comment1", DbType.String, comment1);
            db.AddInParameter(Cmd, "@comment2", DbType.String, comment2);

            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

            return db.ExecuteNonQuery(Cmd);
        }

        public int Delete(string mgrId, string siteId, string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manager_Org_Del");

            db.AddInParameter(Cmd, "@mgrId", DbType.String, mgrId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

            return db.ExecuteNonQuery(Cmd);
        }
    }
}
