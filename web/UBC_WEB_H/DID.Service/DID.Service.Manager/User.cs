﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Manager
{ 
    public class User : IDisposable 
    {
        private Database db = null;

        public User()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() { }


        /// <summary>
        /// 사용자 관리
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <param name="PROG_ID"></param>
        /// <returns></returns>
        public DataTable GetSiteList()   // 사이트 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_List");
            //db.AddInParameter(Cmd, "@USER_ID", DbType.String, USER_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public DataTable GetManageSiteList(string ROOT_SITE_ID)   // 관리 사이트 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manage_Site_List");
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        public DataTable GetManageHostList()   // 관리 단말 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manage_Host_List");
            //db.AddInParameter(Cmd, "@USER_ID", DbType.String, USER_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        public DataTable GetUserList(string siteId, string ROOT_SITE_ID)   // 사용자 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_List");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public DataTable GetUserDetail(string siteId, string userId)   // 사용자 디테일
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_Detail");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId); 
            db.AddInParameter(Cmd, "@userId", DbType.String, userId); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public int GetUserDelete(string siteId, string userId)   // 사용자 삭제
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_Delete");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);

            return db.ExecuteNonQuery(Cmd);
        }

        // 사용자 등록 
        //siteId , password , userName , mobileNo , email , userType , roleId , idationDate , useEmail , useSms , probableCauseList , siteList , hostList
        public int GetUserInsert(
                                  string userId
                                , string siteId
                                , string password
                                , string userName
                                , string mobileNo
                                , string email
                                , string userType
                                , string roleId
                                , string validationDate
                                , string useEmail
                                , string useSms
                                , string probableCauseList
                                , string siteList
                                , string hostList
                                )
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_insert");
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@password", DbType.String, password);
            db.AddInParameter(Cmd, "@userName", DbType.String, userName);
            db.AddInParameter(Cmd, "@mobileNo", DbType.String, mobileNo);
            db.AddInParameter(Cmd, "@email", DbType.String, email);
            db.AddInParameter(Cmd, "@userType", DbType.String, userType);
            db.AddInParameter(Cmd, "@roleId", DbType.String, roleId);
            db.AddInParameter(Cmd, "@validationDate", DbType.String, validationDate);
            db.AddInParameter(Cmd, "@useEmail", DbType.String, useEmail);
            db.AddInParameter(Cmd, "@useSms", DbType.String, useSms);
            db.AddInParameter(Cmd, "@probableCauseList", DbType.String, probableCauseList);
            db.AddInParameter(Cmd, "@siteList", DbType.String, siteList);
            db.AddInParameter(Cmd, "@hostList", DbType.String, hostList);

            return db.ExecuteNonQuery(Cmd);
        }

         
        // 사용자 수정 
        //siteId , password , userName , mobileNo , email , userType , roleId , idationDate , useEmail , useSms , probableCauseList , siteList , hostList
        public int GetUserUpdate(
                                  string userId
                                , string siteId
                                , string password
                                , string userName
                                , string mobileNo
                                , string email
                                , string userType
                                , string roleId
                                , string validationDate
                                , string useEmail
                                , string useSms
                                , string probableCauseList
                                , string siteList
                                , string hostList
                                )   
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_update");
            db.AddInParameter(Cmd, "@userId",            DbType.String, userId);
            db.AddInParameter(Cmd, "@siteId",            DbType.String, siteId);
            db.AddInParameter(Cmd, "@password",          DbType.String, password);
            db.AddInParameter(Cmd, "@userName",          DbType.String, userName);
            db.AddInParameter(Cmd, "@mobileNo",          DbType.String, mobileNo);
            db.AddInParameter(Cmd, "@email",             DbType.String, email);
            db.AddInParameter(Cmd, "@userType",          DbType.String, userType);
            db.AddInParameter(Cmd, "@roleId",            DbType.String, roleId);
            db.AddInParameter(Cmd, "@validationDate",    DbType.String, validationDate);
            db.AddInParameter(Cmd, "@useEmail",          DbType.String, useEmail);
            db.AddInParameter(Cmd, "@useSms",            DbType.String, useSms);
            db.AddInParameter(Cmd, "@probableCauseList", DbType.String, probableCauseList);
            db.AddInParameter(Cmd, "@siteList",          DbType.String, siteList);
            db.AddInParameter(Cmd, "@hostList",          DbType.String, hostList);

            return db.ExecuteNonQuery(Cmd);
        }


        // 사용자 수정 암호를 낭넣었을때에 
        //siteId , password , userName , mobileNo , email , userType , roleId , idationDate , useEmail , useSms , probableCauseList , siteList , hostList
        public int GetUserUpdateNotPass(
                                  string userId
                                , string siteId 
                                , string userName
                                , string mobileNo
                                , string email
                                , string userType
                                , string roleId
                                , string validationDate
                                , string useEmail
                                , string useSms
                                , string probableCauseList
                                , string siteList
                                , string hostList
                                )
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_update_notPass");
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId); 
            db.AddInParameter(Cmd, "@userName", DbType.String, userName);
            db.AddInParameter(Cmd, "@mobileNo", DbType.String, mobileNo);
            db.AddInParameter(Cmd, "@email", DbType.String, email);
            db.AddInParameter(Cmd, "@userType", DbType.String, userType);
            db.AddInParameter(Cmd, "@roleId", DbType.String, roleId);
            db.AddInParameter(Cmd, "@validationDate", DbType.String, validationDate);
            db.AddInParameter(Cmd, "@useEmail", DbType.String, useEmail);
            db.AddInParameter(Cmd, "@useSms", DbType.String, useSms);
            db.AddInParameter(Cmd, "@probableCauseList", DbType.String, probableCauseList);
            db.AddInParameter(Cmd, "@siteList", DbType.String, siteList);
            db.AddInParameter(Cmd, "@hostList", DbType.String, hostList);

            return db.ExecuteNonQuery(Cmd);
        }



        public DataTable GetRoleList(string customer)   // 롤 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Role_List");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }



        public DataTable GetUserIdDup(string userId , string siteId)   // 사용자중복체크
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_Dup");
            db.AddInParameter(Cmd, "@userId", DbType.String, userId); 
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }



        // 조직 시작

        // 부모 조직 키를 이용하여 자식 사이트 리스트 가져오는 함수
        // 2011-07-14 임유석 추가
        public DataTable GeChildSiteListByParentID(string parentId, string franchizeType)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_Child_List");
            db.AddInParameter(Cmd, "@parentId",      DbType.String, parentId     );
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);

            return db.ExecuteDataSet(Cmd).Tables[0];


        }
        //// 본부리스트 
        //public DataTable GetHeadList(string ROOT_SITE_ID)
        //{
        //    DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_Head_List");
        //    db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID);

        //    return db.ExecuteDataSet(Cmd).Tables[0];
        //}
        //// 지역본부리스트 
        //public DataTable GetRegionHeadList(string parentId, string ROOT_SITE_ID)   
        //{
        //    DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_RegionHead_List");
        //    db.AddInParameter(Cmd, "@parentId", DbType.String, parentId);
        //    db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

        //    return db.ExecuteDataSet(Cmd).Tables[0];
        //}
        // 서비스센터리스트 
        public DataTable GetServiceCenterList(string ROOT_SITE_ID)  
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_ServiceCenter_List");
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        // 출고센터리스트 
        public DataTable GetOutCenterList(string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_OutCenter_List");
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        // 지점리스트 
        public DataTable GetPointList(string parentId, string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_Point_List");
            db.AddInParameter(Cmd, "@parentId", DbType.String,  parentId);
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        // 대리점리스트 
        public DataTable GetAgentList(string parentId, string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_Agent_List");
            db.AddInParameter(Cmd, "@parentId", DbType.String, parentId);
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 
            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        // 공장리스트 2012.02.22 gwangsoo
        public DataTable GetFactoryList(string ROOT_SITE_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_Factory_List");
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        

        // 자바스크립트 트리 조직
        public DataTable GetSiteAllList(string current)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_All_List_EXE");
            db.AddInParameter(Cmd, "@currentval", DbType.String, current); 

            return db.ExecuteDataSet(Cmd).Tables[0];
 
        }
        
        //조직 끝










        public DataTable GetDetailCodeList(string CATEGORY_NAME)   //string USER_ID, string PROG_ID
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Detail_List");

            db.AddInParameter(Cmd, "@CATEGORY_NAME", DbType.String, CATEGORY_NAME);
            //db.AddInParameter(Cmd, "@PROG_ID", DbType.String, PROG_ID);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        public int GetCodeUpdate(string CATEGORY_NAME_OLD, string CATEGORY_NAME_NEW)   //string USER_ID, string PROG_ID
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Update");
            db.AddInParameter(Cmd, "@CATEGORY_NAME_OLD", DbType.String, CATEGORY_NAME_OLD);
            db.AddInParameter(Cmd, "@CATEGORY_NAME_NEW", DbType.String, CATEGORY_NAME_NEW);


            return db.ExecuteNonQuery(Cmd);
        }

        public int GetCodeInsert(string CODE_NAME) 
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Insert");
            db.AddInParameter(Cmd, "@CODE_NAME", DbType.String, CODE_NAME);
            


            return db.ExecuteNonQuery(Cmd);
        }
        //global_code_nameVAL, newCodeDetailNameVAL, newDorderVAL, newVisibleVAL
        public int GetCodeDetailInsert(string global_code_nameVAL, string newCodeDetailNameVAL, string newDorderVAL, string newVisibleVAL)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Detail_Insert");
            db.AddInParameter(Cmd, "@global_code_nameVAL", DbType.String, global_code_nameVAL);
            db.AddInParameter(Cmd, "@newCodeDetailNameVAL", DbType.String, newCodeDetailNameVAL);
            db.AddInParameter(Cmd, "@newDorderVAL", DbType.String, newDorderVAL);
            db.AddInParameter(Cmd, "@newVisibleVAL", DbType.String, newVisibleVAL);



            return db.ExecuteNonQuery(Cmd);
        }


        public int GetCodeUpdate(string CATEGORY_NAME, string CATEGORY_DETAIL_NAME_OLD, string CATEGORY_DETAIL_NAME_NEW, string ORDER_OLD, string ORDER_NEW, string visible)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Detail_Update");
            db.AddInParameter(Cmd, "@CATEGORY_NAME", DbType.String, CATEGORY_NAME);
            db.AddInParameter(Cmd, "@CATEGORY_DETAIL_NAME_OLD", DbType.String, CATEGORY_DETAIL_NAME_OLD);
            db.AddInParameter(Cmd, "@CATEGORY_DETAIL_NAME_NEW", DbType.String, CATEGORY_DETAIL_NAME_NEW);
            db.AddInParameter(Cmd, "@ORDER_OLD", DbType.String, ORDER_OLD);
            db.AddInParameter(Cmd, "@ORDER_NEW", DbType.String, ORDER_NEW);
            db.AddInParameter(Cmd, "@visible", DbType.String, visible);


            return db.ExecuteNonQuery(Cmd);
        }


        public int GetCodeDetailDelete(string CATEGORY_NAME, string CATEGORY_DETAIL_NAME)   //string USER_ID, string PROG_ID
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Detail_Delete");
            db.AddInParameter(Cmd, "@CATEGORY_NAME", DbType.String, CATEGORY_NAME);
            db.AddInParameter(Cmd, "@CATEGORY_DETAIL_NAME", DbType.String, CATEGORY_DETAIL_NAME);



            return db.ExecuteNonQuery(Cmd);
        }

        public int GetCodeDelete(string CATEGORY_NAME )   //string USER_ID, string PROG_ID
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Delete");
            db.AddInParameter(Cmd, "@CATEGORY_NAME", DbType.String, CATEGORY_NAME);
            

            return db.ExecuteNonQuery(Cmd);
        }

        // 사용자 엑셀 업로드- 조직 오류 체크
        public DataTable GetUserSite(string siteId, string franchizeType)    
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_User_Excel");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
         
        // 사용자 엑셀 업로드- 조직/사용자 중복  오류 체크
        public DataTable GetUserSiteDup(string siteId, string userId)    
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_User_Excel_Dup");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }



        // 사용자 엑셀 업로드-  아이디 저장
        public int UserSiteExcelInsert(   string siteId
                                        , string userId
                                        , string password 
                                        , string userName
                                        , string mobileNo
                                        , string email
                                        , string userType
                                        , string roleId
                                        )   
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_User_Excel_Insert");
            db.AddInParameter(Cmd, "@siteId",   DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId",   DbType.String, userId);
            db.AddInParameter(Cmd, "@password", DbType.String, password);
            db.AddInParameter(Cmd, "@userName", DbType.String, userName);
            db.AddInParameter(Cmd, "@mobileNo", DbType.String, mobileNo);
            db.AddInParameter(Cmd, "@email", DbType.String, email);
            db.AddInParameter(Cmd, "@userType", DbType.String, userType);
            db.AddInParameter(Cmd, "@roleId", DbType.String, roleId);
            

            return db.ExecuteNonQuery(Cmd);
        }


    }
}
