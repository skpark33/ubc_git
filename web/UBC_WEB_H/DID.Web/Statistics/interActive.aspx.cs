﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;

public partial class interActive : BasePage
{
    #region 그래프 관련 변수들

    protected string allCount = "0";
    protected string arrayTitle = "";
    protected string arrayString = "";

    #endregion

    // getSelectedSiteId 함수 인자로 사용
    protected enum SEARCH_TYPE { SEARCH_TYPE_MAIN, SEARCH_TYPE_SUB }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.InitControl();
        }
    }

    //초기화
    private void InitControl()
    {
        // fromdate 를 -7일 로 세팅한다.
        fromDate.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");

        // todate 를 오늘날짜로 세팅한다.
        toDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        // 조직콤보
        Common.BindDropDownList(ddlType, GetOrgCode(ROOT_SITE_ID), "전체");

        //// 년 콤보 셋팅
        //for (int i = 2011 ; i <= DateTime.Now.Year ; i++)
        //    ddlYear.Items.Add(new ListItem(i.ToString() + "년", i.ToString()));

        //// 월 콤보 셋팅
        //for (int i = 1; i <= 12; i++)
        //    ddlMonth.Items.Add(new ListItem(i.ToString() + "월", string.Format("{0:00}", i)));

    }


    //엑셀
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        string fileName = HttpUtility.UrlEncode("상호작용통계", new UTF8Encoding()) + ".xls";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-Disposition", "attachment;filename=" + fileName);
        Response.Charset = "";
        gvList.EnableViewState = false;
        System.IO.StringWriter tw = new System.IO.StringWriter();
        tw.WriteLine("");
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        gvList.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time. 
    }


    // 조회
    protected void btnSelect_Click(object sender, EventArgs e)
    {
        using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
        {
            // 기간
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            // 선택된 SITE ID
            string siteId = getSelectedSiteId(SEARCH_TYPE.SEARCH_TYPE_MAIN);
            
            DataTable dt = obj.GetInterActiveList(ROOT_SITE_ID, fromDateVar, toDateVar, siteId, "", "1");
            this.gvList.DataSource = dt;
            this.gvList.DataBind();

             // 데이타가 있으면
            if ( dt.Rows.Count > 0 )
            {
                // 엑셀저장 버튼 활성
                btnExcel.Enabled = true;
                btnExcel.Visible = true;

                //  그래프 그려줌
                dt = obj.GetInterActiveGraph(ROOT_SITE_ID, fromDateVar, toDateVar, siteId, "");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string category = "[" + dt.Rows[i]["category1"].ToString() + "]";

                    if (!dt.Rows[i]["category2"].ToString().IsNullOrEmpty())
                        category += "-[" + dt.Rows[i]["category2"].ToString() + "]";

                    if (!dt.Rows[i]["category3"].ToString().IsNullOrEmpty())
                        category += "-[" + dt.Rows[i]["category3"].ToString() + "]";

                    arrayString = arrayString +
                     "data.setValue(" + i + " , 0, '" + category + /*" (" + dt.Rows[i]["click_count"] + ")" +*/ "'); " +
                     "data.setValue(" + i + " , 1, " + dt.Rows[i]["click_count"] + "); ";                    
                }

                allCount = dt.Rows.Count.ToString();
                arrayTitle = "카테고리별 이용횟수(TOP10)";

                // 그래프 데이타가 있으면
                if (dt.Rows.Count > 0)
                    PanelGraph.Visible = true;
                else
                    PanelGraph.Visible = false;
            }
            else
            {
                // 그래프 안보이게
                PanelGraph.Visible = false;

                // 엑셀저장 버튼 비활성
                btnExcel.Enabled = false;
                btnExcel.Visible = false;
            }

        }

    }

    // 조직 콤보의 선택된 조직 코드 가져옴
    // SEARCH_TYPE : SEARCH_TYPE_MAIN - 메인(gridview1) 조회 호출시 사용되는 조직코드 가져옴
    //               SEARCH_TYPE_SUB  - 서브(gridview2) 조회 호출시 사용되는 조직코드 가져옴
    protected string getSelectedSiteId(SEARCH_TYPE searchType)
    {
        string siteId;

        if (ddlClass3.Visible == true)
        {
            siteId = ddlClass3.SelectedValue;
            siteId = (siteId.IsNullOrEmpty()) ? ddlClass2.SelectedValue : siteId;

        }
        else if (ddlClass2.Visible == true)
        {
            siteId = ddlClass2.SelectedValue;
            siteId = (siteId.IsNullOrEmpty()) ? ddlClass1.SelectedValue : siteId;
        }
        else if (ddlClass1.Visible == true)
        {
            siteId = ddlClass1.SelectedValue;

            if (searchType == SEARCH_TYPE.SEARCH_TYPE_MAIN)
                siteId = (siteId.IsNullOrEmpty()) ? ddlType.SelectedValue : siteId;
        }
        else
        {
            if (searchType == SEARCH_TYPE.SEARCH_TYPE_MAIN)
                siteId = ddlType.SelectedValue;
            else
                siteId = "";
        }

        if (searchType == SEARCH_TYPE.SEARCH_TYPE_MAIN)
            siteId = (siteId.IsNullOrEmpty()) ? ROOT_SITE_ID : siteId;

        return siteId;
    }

    #region 그리드뷰 gvList_RowDataBound()
    /// <summary>
    /// 그리드뷰 gvList_RowDataBound
    /// </summary>
    protected void gvList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            // 기간
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            // SITE ID            
            string siteId = drv["siteId"].ToString();

            using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
            {
                DataTable dt = new DataTable();

                // 조회할 조직 코드
                string siteIdForSubSearch = getSelectedSiteId(SEARCH_TYPE.SEARCH_TYPE_SUB );

                // 조회할 타입 (2:전체 2-2:선택된 조직 하위) 
                string searchType = (siteIdForSubSearch.IsNullOrEmpty()) ? "2" : "2-1";

                dt = obj.GetInterActiveList(ROOT_SITE_ID, fromDateVar, toDateVar, siteId, siteIdForSubSearch, searchType);                               
                             
                GridView gv = e.Row.FindControl("gvList2") as GridView;
                gv.DataSource = dt;
                gv.DataBind();
            }
        }
    }
    #endregion


    #region 그리드뷰 gvList2_RowDataBound()
    /// <summary>
    /// 그리드뷰 gvList2_RowDataBound
    /// </summary>
    protected void gvList2_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            string siteId = drv["siteId"].ToString();
           
            // 토탈 ROW 일 경우
            if (siteId == "total")
            {
                // 글씨체 bold, 셀 배경색 변경
                e.Row.BackColor = System.Drawing.Color.LightYellow;
                e.Row.Font.Bold = true;

                // 상세내역 link button 안보이게 설정
                e.Row.Cells[3].Controls[0].Visible = false;                
            }
            else
            {
                // 상세내역 링크 셋팅
                e.Row.Cells[3].Attributes.Add("onclick", "javascript:DoShowDetail('" + drv["siteId"] + "', '" + drv["siteName"] + "', '" + drv["fromDate"] + "', '" + drv["toDate"] + "')");
            }            
            
        }
    }
    #endregion


    #region SelectedIndexChanged 이벤트 핸들러
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass1.Visible = false;
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;

        ddlClass1.Items.Clear();
        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();

        if (!ddlType.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlType.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass1.Visible = true;
                Common.BindDropDownList(ddlClass1, dt, "전체");
            }
        }
        
    }
    protected void ddlClass1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;

        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();

        if (!ddlClass1.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass1.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass2.Visible = true;
                Common.BindDropDownList(ddlClass2, dt, "전체");
            }

        }

    }
    protected void ddlClass2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass3.Visible = false;

        ddlClass3.Items.Clear();

        if (!ddlClass2.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass2.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass3.Visible = true;
                Common.BindDropDownList(ddlClass3, dt, "전체");
            }
        }
    }



    #endregion


    #region 데이터 조회


    private DataTable GetOrgCode(string parentId)
    {

        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            return obj.GetOrgCode(parentId, this.ROOT_SITE_ID);
        }
    }
    #endregion


    protected void gvList_PreRender(object sender, EventArgs e)
    {

        Common.GroupColumn(gvList, 0); //그리드뷰의 ID, 병합할 컬럼

    }


}

