﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;

public partial class contentsAccess : BasePage
{
    

    #region 그래프 관련 변수들

    protected string allCount = "0";
    protected string arrayTitle = "";
    protected string arrayString = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.InitControl();
        }
    }

    //초기화
    private void InitControl()
    {
        // fromdate 를 -7일 로 세팅한다.
        fromDate.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");

        // todate 를 오늘날짜로 세팅한다.
        toDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        // 조직콤보
        Common.BindDropDownList(ddlType, GetOrgCode(ROOT_SITE_ID), "전체");

        //// 년 콤보 셋팅
        //for (int i = 2011 ; i <= DateTime.Now.Year ; i++)
        //    ddlYear.Items.Add(new ListItem(i.ToString() + "년", i.ToString()));

        //// 월 콤보 셋팅
        //for (int i = 1; i <= 12; i++)
        //    ddlMonth.Items.Add(new ListItem(i.ToString() + "월", string.Format("{0:00}", i)));

    }


    //엑셀
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        string fileName = HttpUtility.UrlEncode("콘텐츠접근통계", new UTF8Encoding()) + ".xls";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-Disposition", "attachment;filename=" + fileName);
        Response.Charset = "";
        gvList.EnableViewState = false;
        System.IO.StringWriter tw = new System.IO.StringWriter();
        tw.WriteLine("");
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        gvList.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time. 
    }


    // 조회
    protected void btnSelect_Click(object sender, EventArgs e)
    {
        using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
        {
            // 기간
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            // 선택된 SITE ID
            string siteId = (hiddenSiteIdForSearch.Value.IsNullOrEmpty()) ? ROOT_SITE_ID : hiddenSiteIdForSearch.Value;

            DataTable dt = obj.GetContentsAccessList(ROOT_SITE_ID, fromDateVar, toDateVar, siteId, "", "1");
            this.gvList.DataSource = dt;
            this.gvList.DataBind();

             // 데이타가 있으면
            if ( dt.Rows.Count > 0 )
            {
                // 엑셀저장 버튼 활성
                btnExcel.Enabled = true;
                btnExcel.Visible = true;

                //  그래프 그려줌
                dt = obj.GetContentsAccessGraph(ROOT_SITE_ID, fromDateVar, toDateVar, siteId, "");
                
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string category = "[" + dt.Rows[i]["category1"].ToString() + "]";

                    if (!dt.Rows[i]["category2"].ToString().IsNullOrEmpty())
                        category += "-[" + dt.Rows[i]["category2"].ToString() + "]";

                    if (!dt.Rows[i]["category3"].ToString().IsNullOrEmpty())
                        category += "-[" + dt.Rows[i]["category3"].ToString() + "]";

                    arrayString = arrayString +
                     "data.setValue(" + i + " , 0, '" + category + /*" (" + dt.Rows[i]["click_count"] + ")" +*/ "'); " +
                     "data.setValue(" + i + " , 1, " + dt.Rows[i]["click_count"] + "); ";                    
                }

                allCount = dt.Rows.Count.ToString();
                arrayTitle = "카테고리별 이용횟수(TOP10)";
            }
            else
            {
                // 엑셀저장 버튼 비활성
                btnExcel.Enabled = false;
                btnExcel.Visible = false;
            }

            // 그래프 데이타가 있으면
            if (dt.Rows.Count > 0)
                PanelGraph.Visible = true;
            else
                PanelGraph.Visible = false;
        }
    }


    #region 그리드뷰 gvList_RowDataBound()
    /// <summary>
    /// 그리드뷰 gvList_RowDataBound
    /// </summary>
    protected void gvList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            // 기간
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            // SITE ID            
            string siteId = drv["siteId"].ToString();

            using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
            {
                DataTable dt = new DataTable();

                if (hiddenSiteIdForSearch.Value.IsNullOrEmpty() == false
                     && hiddenSelectedSiteId.Value.IsNullOrEmpty() == false
                     && hiddenHasChildSite.Value == "0")
                {
                    // 콤보에서 선택된 조직이 최하위 조직일 경우 조회 타입을 "2-1" 프로시저 호출함.
                    // 콤보에서 선택된 조직 코드를 네번째 인자로 넘겨준다.
                    dt = obj.GetContentsAccessList(ROOT_SITE_ID, fromDateVar, toDateVar, siteId, hiddenSelectedSiteId.Value, "2-1");
                }
                else
                {
                    // 콤보에서 선택된 조직이 최하위가 아닐경우는 조회 타입 "2" 로 프로시저 호출함
                    // 이때는 콤보에서 선택된 조직코드를 넘겨주지 않고 네번째 인자를 "" 로 셋팅한다
                    dt = obj.GetContentsAccessList(ROOT_SITE_ID, fromDateVar, toDateVar, siteId, "", "2");
                }
             
                GridView gv = e.Row.FindControl("gvList2") as GridView;
                gv.DataSource = dt;
                gv.DataBind();
            }
        }
    }
    #endregion


    #region 그리드뷰 gvList2_RowDataBound()
    /// <summary>
    /// 그리드뷰 gvList2_RowDataBound
    /// </summary>
    protected void gvList2_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            string siteId = drv["siteId"].ToString();
           
            // 토탈 ROW 일 경우
            if (siteId == "total")
            {
                // 글씨체 bold, 셀 배경색 변경
                e.Row.BackColor = System.Drawing.Color.LightYellow;
                e.Row.Font.Bold = true;

                // 상세내역 link button 안보이게 설정
                e.Row.Cells[3].Controls[0].Visible = false;                
            }
            else
            {
                // 상세내역 링크 셋팅
                e.Row.Cells[3].Attributes.Add("onclick", "javascript:DoShowDetail('" + drv["siteId"] + "', '" + drv["siteName"] + "', '" + drv["fromDate"] + "', '" + drv["toDate"] + "')");
            }
            
            //// 기간
            //string fromDateVar = fromDate.Text.Replace("-", "");
            //string toDateVar = toDate.Text.Replace("-", "");

            //// SITE ID
            //string siteId = drv["siteId"].ToString();

            //using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
            //{
            //    DataTable dt = obj.GetInterActiveList(ROOT_SITE_ID, fromDateVar, toDateVar, siteId, "", "3");

            //    GridView gv = e.Row.FindControl("gvList3") as GridView;
            //    gv.DataSource = dt;
            //    gv.DataBind();
            //}
        }
    }
    #endregion


    #region SelectedIndexChanged 이벤트 핸들러
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 자식 조직이 있는지 여부 초기화 (0:없음, 1:있음)
        hiddenHasChildSite.Value = "0";

        ddlClass1.Visible = false;
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;

        ddlClass1.Items.Clear();
        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();

        if (!ddlType.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlType.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass1.Visible = true;
                Common.BindDropDownList(ddlClass1, dt, "전체");

                // 자식 조직이 있는지 여부 (0:없음, 1:있음)
                hiddenHasChildSite.Value = "1";                
            }
            else
            {
                // 자식 조직이 있는지 여부 (0:없음, 1:있음)
                hiddenHasChildSite.Value = "0";
            }
        }

        // 조회에서 사용될 선택될 조직코드 셋팅
        hiddenSiteIdForSearch.Value = ddlType.SelectedValue;

        // 실제 선택된 콤보의 siteId
        hiddenSelectedSiteId.Value = ddlType.SelectedValue;

    }
    protected void ddlClass1_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 자식 조직이 있는지 여부 초기화 (0:없음, 1:있음)
        hiddenHasChildSite.Value = "0";

        ddlClass2.Visible = false;
        ddlClass3.Visible = false;

        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();

        if (!ddlClass1.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass1.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass2.Visible = true;
                Common.BindDropDownList(ddlClass2, dt, "전체");

                // 자식 조직이 있는지 여부 (0:없음, 1:있음)
                hiddenHasChildSite.Value = "1";
            }
            else
            {
                // 자식 조직이 있는지 여부 (0:없음, 1:있음)
                hiddenHasChildSite.Value = "0";
            }

            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSiteIdForSearch.Value = ddlClass1.SelectedValue;            
        }
        else
        {
            // 조회에서 사용될 선택될 조직코드 셋팅 (전체 선택시 상위 콤보 선택값 셋팅)
            hiddenSiteIdForSearch.Value = ddlType.SelectedValue;            
        }

        // 실제 선택된 콤보의 siteId
        hiddenSelectedSiteId.Value = ddlClass1.SelectedValue;

    }
    protected void ddlClass2_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 자식 조직이 있는지 여부 초기화 (0:없음, 1:있음)
        hiddenHasChildSite.Value = "0";

        ddlClass3.Visible = false;

        ddlClass3.Items.Clear();

        if (!ddlClass2.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass2.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass3.Visible = true;
                Common.BindDropDownList(ddlClass3, dt, "전체");

                // 자식 조직이 있는지 여부 (0:없음, 1:있음)
                hiddenHasChildSite.Value = "1";
            }
            else
            {
                // 자식 조직이 있는지 여부 (0:없음, 1:있음)
                hiddenHasChildSite.Value = "0";
            }

            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSiteIdForSearch.Value = ddlClass2.SelectedValue;
            
        }
        else
        {
            // 조회에서 사용될 선택될 조직코드 셋팅 (전체 선택시 상위 콤보 선택값 셋팅)
            hiddenSiteIdForSearch.Value = ddlClass1.SelectedValue;
        }

        // 실제 선택된 콤보의 siteId
        hiddenSelectedSiteId.Value = ddlClass2.SelectedValue;
    }

    protected void ddlClass3_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 자식 조직이 있는지 여부 초기화 (0:없음, 1:있음)
        hiddenHasChildSite.Value = "0";

        if (!ddlClass3.SelectedValue.IsNullOrEmpty())
        {
            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSiteIdForSearch.Value = ddlClass3.SelectedValue;
        }
        else
        {
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSiteIdForSearch.Value = ddlClass2.SelectedValue;
        }        

        // 실제 선택된 콤보의 siteId
        hiddenSelectedSiteId.Value = ddlClass3.SelectedValue;
    }


    #endregion


    #region 데이터 조회


    private DataTable GetOrgCode(string parentId)
    {

        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            return obj.GetOrgCode(parentId, this.ROOT_SITE_ID);
        }
    }
    #endregion


    // 그리드뷰 셀병합
    protected void gvList_PreRender(object sender, EventArgs e)
    {

        Common.GroupColumn(gvList, 0); //그리드뷰의 ID, 병합할 컬럼

    }

}

