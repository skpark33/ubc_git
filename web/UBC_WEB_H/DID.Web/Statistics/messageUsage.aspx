﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="messageUsage.aspx.cs" Inherits="messageUsage" Title="제목 없음" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


<!-- 구글 파이챠트 시작-->

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load('visualization', '1', { packages: ['corechart'] });
</script>
<script type="text/javascript">      
    
    function drawVisualization() 
    {
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', '항목');
        data.addColumn('number', '갯수');
        data.addRows(<%=this.allCount %>);              
              
        <%= this.arrayString %>             
                     
        // 차트 데이타 형식
        // data.setValue(0, 0, '항목1');
        // data.setValue(0, 1, 갯수); 
        // data.setValue(1, 0, 'host_process_down');
        // data.setValue(1, 1, 2); 
        // data.setValue(2, 0, 'host_hdd_overload');
        // data.setValue(2, 1, 2);
        // 참고 : http://code.google.com/apis/ajax/playground/                      

        // Create and draw the visualization.
        new google.visualization.PieChart(document.getElementById('visualization')).draw(data, {title:"<%=this.arrayTitle %>"});
    }   

    google.setOnLoadCallback(drawVisualization);
    
</script>

<!-- 구글 파이챠트 끝-->

<script language="javascript" type="text/javascript">

    function DoSelect() {

        // 날짜 유효성 검사 시작

        var strStart_date = $("#<%= fromDate.ClientID%>").val();
        var strEnd_Date = $("#<%= toDate.ClientID%>").val();

        if (!checkDateFromTo(strStart_date, strEnd_Date)) return;        
    }

   
    
</script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
 <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
  <colgroup>
  <col width="auto" />
  <col width="50%" />
  </colgroup>
  <tr>
    <td><span class="subTitle"><span id=""><img src="/Asset/Images/body/bullet01.gif" /> 전송이용 통계</span></span> </td>
  </tr>
</table>
 
    <div class='topOptionBtns'>
        <asp:ImageButton ID="btnSelect" runat="server" ToolTip="조회" OnClick="btnSelect_Click" OnClientClick='return DoSelect()' ImageUrl="~/Asset/Images/button/btn_search.jpg"  />
        <asp:ImageButton ID="btnExcel"  runat="server" ToolTip="엑셀저장" OnClick="btnExcel_Click"  ImageUrl="~/Asset/Images/button/btn_excel.jpg" Visible=false />
    </div>    

    <table class="boardInputType mb10" cellpadding="0" cellspacing="0" border="0">
		<colgroup>		
		<col width="100px" />
		<col width="250px" />
		<col width="100px" />
		<col width="auto" />	
	</colgroup>
    <tr>
       <th><span>기간</span></th>
        <td>        
          <asp:TextBox ID="fromDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />
            ~
          <asp:TextBox ID="toDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />
        </td>   
        <th><span>조직</span></th>  
        <td>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:DropDownList ID="ddlType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass1" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass1_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass2" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass2_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass3" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass3_SelectedIndexChanged" />
                
                <asp:HiddenField ID="hiddenSelectedSiteId" runat="server" />
                    
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>                     
    </tr>    
    <tr>        
        <th><span>SMS/MMS</span></th>
        <td colspan='3'>       
            <asp:RadioButtonList ID="mobileSend"   CssClass="radioType" runat="server" RepeatDirection=Horizontal  BorderStyle=None  BorderWidth=0  >
            <asp:ListItem Value="MMS" Selected="True"  >MMS</asp:ListItem>
            <asp:ListItem Value="SMS">SMS</asp:ListItem>            
            </asp:RadioButtonList>
        </td>    
    </tr>
    </table>
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate> 
        <div class="container">
        <asp:GridView ID="gvList" runat="server" Width="100%" OnPreRender="gvList_PreRender" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false" 
              ShowHeader="true" CssClass="boardListType3">
        <Columns>  
            
            <asp:BoundField DataField="s_date" HeaderText="조회기간"  HeaderStyle-Width="170px" ItemStyle-VerticalAlign=top ItemStyle-HorizontalAlign=Center SortExpression="s_date"     >
            </asp:BoundField>
            
            <asp:BoundField DataField="send_siteName" HeaderText="소속그룹" HeaderStyle-Width="130px" ItemStyle-VerticalAlign=top ItemStyle-HorizontalAlign="Center" SortExpression="send_siteName"   >
            </asp:BoundField>            
            
            <asp:BoundField DataField="send_status" HeaderText="전송상태" ItemStyle-HorizontalAlign="Center" SortExpression="send_status"   >
            </asp:BoundField>                        

            <asp:BoundField DataField="send_count" HeaderText="전송횟수" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Right" SortExpression="send_count"    >
            </asp:BoundField>                          
             
        </Columns>
        <EmptyDataTemplate>
            <table style="width:100%;height:290px;">
                <tr>
                    <td style="text-align:center">
                    조회된 데이터가 없습니다.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        
        </asp:GridView>   
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
     
    <!-- 그래프  -->
    <asp:Panel ID="PanelGraph" runat="server" Visible=false>
        <div id="visualization"  style="width: 820px; height: 400px;"></div>
    </asp:Panel>
     
</asp:Content>


