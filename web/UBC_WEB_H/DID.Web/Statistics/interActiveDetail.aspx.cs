﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using DID.Common.Framework;

public partial class interActiveDetail : BasePage
{
    protected string title;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            title = "[" + Request["siteName"] + "] 메뉴별 상세내역";
            this.InitControl();

        }
    }


    private void InitControl()
    {
        using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
        {
            DataTable dt = obj.GetInterActiveList(ROOT_SITE_ID, Request["fromDate"], Request["toDate"], Request["siteId"], "", "3");
            this.gvList.DataSource = dt;
            this.gvList.DataBind();
        }
    }


}



