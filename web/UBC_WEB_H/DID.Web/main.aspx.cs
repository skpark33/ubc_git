﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using DID.Common.Framework;

/// <summary>
/// 메인페이지
/// </summary>
public partial class main : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        userName.Text = Profile.USER_NAME  ;
        
        if( Profile.USER_TYPE  == "1")
        {  userType.Text = "수퍼 어드민";   }
        if (Profile.USER_TYPE == "2")
        { userType.Text = "사이트 어드민"; }
        if (Profile.USER_TYPE == "3")
        { userType.Text = "일반 사용자"; }
        
        DataTable dt;

        using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
        {
             
            dt = obj.GetMenuLarge(this.Profile.COMP_TYPE, this.Profile.LANG_FLAG, this.Profile.USER_TYPE);
        }

        for (int i = 0; i < dt.Rows.Count;i++ )
        {
            DataRow dr = dt.Rows[i];

            if (i == 0)
            {
                lblTopMenu.Text += string.Format("<li class='on' id='{0}' title='menubox_05'><a href='#'>{1}</a></li>"
                                   , dr["LARGE_CODE"]
                                   , dr["MENU_NAME"]);
 
            }
            else
            {
                lblTopMenu.Text += string.Format("<li id='{0}' title='menubox_05'><a href='#'>{1}</a></li>"
                                    , dr["LARGE_CODE"]
                                    , dr["MENU_NAME"]);
            }
    
        }
    }
}
