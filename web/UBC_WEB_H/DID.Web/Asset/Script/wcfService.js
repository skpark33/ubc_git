﻿
/*
    웹서비스
*/

// 왼쪽 메뉴 가져오기

var compTypeHome ;   // 알맞은 홈을 띄우기 위한 전역변수
function Wcf_LeftMenu(compType,largeCode,langFlag) {
    var wcfService = new WebService.IWebService();
    wcfService.GetLeftMenu(compType, largeCode, langFlag, Set_LeftMenu);
    compTypeHome = compType;
    
}

// 왼쪽 메뉴 가져오기 - 결과
function Set_LeftMenu(results, userContent, methodName) {
    
    // 처음에 여기서 홈을 분기했던 곳
    var tree = '';
    var depth = '';

    if (results && results.length > 0) {
        
        results = JSON.parse(results);
        tree += "<table><tr><td height=4> </td></tr></table>";
        tree += '&nbsp;<span class="folder"><b><font size=2 color=#89561c>&nbsp;' + results[0]["treeTopName"] + '</font></b></span>';
        
        for (var i = 0; i < results.length; i++) {

            if (results[i]["menuDepth"] == '2') {

                if (depth == '2') { tree += '</li>'; }
                if (depth == '3') { tree += '</ul></li>'; }
                depth = '2';
                if (results[i]["menuUrl"] == "") {
                    tree += '<li><span class="folder">' + results[i]["menuName"] + '</span>';
                }
                else {
                    tree += '<li><span class="file" id="' + results[i]["menuUrl"] + '"><a href="Javascript:Tabs_On(\'' + results[i]["menuCode"] + '\',\'' + results[i]["menuName"] + '\',\'';
                    tree += results[i]["menuUrl"] + '\',\''+ "true" + '\');">' + results[i]["menuName"] + '</a></span>';
                }
            } else {
                if (depth == '2') { tree += '<ul>'; }
                depth = '3';
                tree += '<li><span class="file" id="' + results[i]["menuUrl"] + '"><a href="Javascript:Tabs_On(\'' + results[i]["menuCode"] + '\',\'' + results[i]["menuName"] + '\',\'';
                tree += results[i]["menuUrl"] + '\',\'' + "true" + '\');">' + results[i]["menuName"] + '</a></span></li>';
            }
        }
        if (depth == '2') { tree += '</li>'; }
        if (depth == '3') { tree += '</ul></li>'; }
    }


    $('.leftmenu ul').html(tree).treeview({ animated: "fast", collapsed: true, unique: true });
}
