﻿
var windowResizeTimeout;
var windowWidth, windowHeight;

$(document).ready(function() {

    // 사용자 브라우져 크기 만큼 화면 설정 (최소 넓이 900px)
    windowWidth = parseInt($(window).width()) - 178;
    windowHeight = parseInt($(window).height()) - 67;

    $('.leftmenu').css("height", windowHeight);                                 // leftmenu
    $('.sub_tabs').css("width", windowWidth);                                   // sub tabs
    $('.frame_box').css("width", windowWidth);                                  // frame DIV
    $('.maincontent').css("height", windowHeight).css("width", windowWidth);    // content

    // 왼쪽 메뉴 닫기 버튼 
    $('.menubutton')
        .css("height", windowHeight)
        .mouseover(function() { $(this).css("background-color", "#c5cdd7"); })
        .mouseout(function() { $(this).css("background-color", "#fff"); })
        .click(function() { Common_LeftMenu_B(); });

    windowHeight = parseInt(windowHeight) - 35;
    $('.frame_box iframe').css("width", windowWidth).css("height", windowHeight);        // iframe

    /////////    로딩시 레프트 닫기위해 처리  ////////
    //Common_LeftMenu_A();

    var newWidth = $(window).width();

    newWidth = parseInt(newWidth) - 8;

    $('.leftmenu').hide();
    $('.maincontent').css("width", newWidth);       // content
    $('.frame_box iframe').css("width", newWidth);           // iframe
    $('.sub_tabs').css("width", newWidth);          // sub tabs
    $('.frame_box').css("width", newWidth);         // frame DIV      
    $('.menubutton').css("background", "url(/Asset/Images/btn_open.gif) no-repeat 2px 50%;").attr("alt", "열기");

    $.cookie('ShowMenu', 'hide');


    /////////////////

    window.onresize = function() {

        window.clearTimeout(windowResizeTimeout);
        windowResizeTimeout = window.setTimeout(DelayedResize, 100);
    };

});

// 왼쪽 메뉴 (역반향)
function Common_LeftMenu_A() {

    var cookies = $.cookie('ShowMenu');
    var newWidth = $(window).width();

    if (cookies == 'show') {

        newWidth = parseInt(newWidth) - 178;

        $('.leftmenu').show();
        $('.maincontent').css("width", newWidth);       // content
        $('.frame_box iframe').css("width", newWidth);           // iframe
        $('.sub_tabs').css("width", newWidth);          // sub tabs
        $('.frame_box').css("width", newWidth);         // frame DIV
        $('.menubutton').css("background", "url(/Asset/Images/btn_close.gif) no-repeat left 50%;").attr("alt", "닫기");

    } else {

        newWidth = parseInt(newWidth) - 8;

        $('.leftmenu').hide();
        $('.maincontent').css("width", newWidth);       // content
        $('.frame_box iframe').css("width", newWidth);           // iframe
        $('.sub_tabs').css("width", newWidth);          // sub tabs
        $('.frame_box').css("width", newWidth);         // frame DIV      
        $('.menubutton').css("background", "url(/Asset/Images/btn_open.gif) no-repeat 2px 50%;").attr("alt", "열기");
    }
}

// 왼쪽 메뉴 (역반향)
function Common_LeftMenu_B() {

    var cookies = $.cookie('ShowMenu');
    var newWidth = $(window).width();

    if (cookies == 'show') {

        newWidth = parseInt(newWidth) - 8;

        $('.leftmenu').hide();
        $('.maincontent').css("width", newWidth);       // content
        $('.frame_box iframe').css("width", newWidth);           // iframe
        $('.sub_tabs').css("width", newWidth);          // sub tabs
        $('.frame_box').css("width", newWidth);         // frame DIV      
        $('.menubutton').css("background", "url(/Asset/Images/btn_open.gif) no-repeat 2px 50%;").attr("alt", "열기");
        $.cookie('ShowMenu', 'hide');
    } else {

        newWidth = parseInt(newWidth) - 178;

        $('.leftmenu').show();
        $('.maincontent').css("width", newWidth);       // content
        $('.frame_box iframe').css("width", newWidth);           // iframe
        $('.sub_tabs').css("width", newWidth);          // sub tabs
        $('.frame_box').css("width", newWidth);         // frame DIV
        $('.menubutton').css("background", "url(/Asset/Images/btn_close.gif) no-repeat left 50%;").attr("alt", "닫기");
        $.cookie('ShowMenu', 'show');
    }
}

function DelayedResize() {

    var newWidth = $(window).width();
    var newHeight = $(window).height();
    var cookies = $.cookie('ShowMenu');

    if (windowWidth != newWidth || windowHeight != newHeight) {

        if (cookies == 'show') {
            newWidth = parseInt(newWidth) - 178;
        } else {
            newWidth = parseInt(newWidth) - 8;
        }

        newHeight = parseInt(newHeight) - 67;

        $('.leftmenu').css("height", newHeight);            // leftmenu
        $('.menubutton').css("height", newHeight);
        $('.maincontent').css("height", newHeight).css("width", newWidth);      // content
        $('.sub_tabs').css("width", newWidth);              // sub tabs
        $('.frame_box').css("width", newWidth);             // frame DIV

        newHeight = parseInt(newHeight) - 35;
        $('.frame_box iframe').css("width", newWidth).css("height", newHeight);        // iframe       

        windowWidth = $(window).width();
        windowHeight = $(window).height();
    }
}


//function datePickerDialog(ctrName) {

//    var strd = "width:195px; height:180px; center:yes; resizable:no; status:yes; help:no; scroll:no";
//    var rs = window.open("/Common/DatePicker.aspx?ControlName=" + ctrName,'calendar', strd);

//    return -1;
//}

//function datePickerPop(ctrName) {
//    var pW = event.x + 200;
//    var pH = event.y + 200;
//    var winWidth = window.screen.width;    //해상도가로
//    var pL = pW;
//    var winHeight = window.screen.height;     //해상도세로
//    var pT = pH;
//    var sScroll = (typeof (scroll) == "undefined") ? "no" : ((scroll) ? "yes" : "no");
//    var sResizable = (typeof (scroll) == "undefined") ? "no" : ((scroll) ? "yes" : "no");
//    var nWin2 = window.open("/Common/DatePicker.aspx?ControlName=" + ctrName.id, "calendar", " width=10px , height=220,location=0, toolbar=0, menubar=no, scrollbars=no, resizable=yes, status=no, left=" + pL + ", top=" + pT);
//    nWin2.focus();
//}


// 팝업창이 화면의 가운데 위치하게 하기   
function CenterWindow(height, width) {
    var outx = screen.height;
    var outy = screen.width;
    var x = (outx - height) / 2;
    var y = (outy - width) / 2;
    dim = new Array(2);
    dim[0] = x;
    dim[1] = y;

    return dim;
}

function openPopUp(url, target, w, h, scroll) {
    var pW = parseInt(w, 10);
    var pH = parseInt(h, 10);
    var winWidth = window.screen.width;    //해상도가로
    var pL = parseInt((winWidth - pW) / 2);
    var winHeight = window.screen.height;     //해상도세로
    var pT = parseInt((winHeight - pH) / 2);
    var sScroll = (typeof (scroll) == "undefined") ? "no" : ((scroll) ? "yes" : "no");
    var sResizable = (typeof (scroll) == "undefined") ? "no" : ((scroll) ? "yes" : "no");
    var nWin2 = window.open(url, target, " location=0, toolbar=0, menubar=no, scrollbars=" + sScroll + ", resizable=" + sResizable + ", status=yes, left=" + pL + ", top=" + pT + ", width=" + w + " , height=" + h + " ");
    nWin2.focus();
}

//***************************************************************
// 주민증록번호 유효성을 체크
//***************************************************************
function IsJuminNumber(a, b) {

    var jumin = a + b;
    if (jumin.length != 13 || isNaN(jumin))
        return false;

    var mul = "234567892345";
    var sum = 0;
    for (var i = 0; i < 12; i++)
        sum += mul.charAt(i) * jumin.charAt(i);
    var chk = (11 - (sum % 11)) % 10;

    return (chk == parseInt(jumin.charAt(12), 10)) ? true : false;
}

//***************************************************************
// 사업자등록번호 유효성 체크
//***************************************************************
function IsCompanyNumber(a, b, c) {
    var no = a + b + c;

    if (no.length != 10 || isNaN(no))
        return false;

    var mul = new Array(1, 3, 7, 1, 3, 7, 1, 3);
    var sum = 0;
    for (var i = 0; i < 8; i++)
        sum += no.charAt(i) * mul[i];

    var chk = no.charAt(8) * 5;
    if (chk > 9)
        chk = sum + Math.floor(chk / 10) + Math.floor(chk % 10) + parseInt(no.charAt(9), 10);
    else
        chk += sum + parseInt(no.charAt(9), 10);

    if (chk > 99) {
        chk = (chk % 100) % 10;
        chk = Math.floor(chk);
    }
    else {
        if (chk > 9)
            chk = Math.floor(chk % 10);
    }

    return (chk == 0) ? true : false;
}

//***************************************************************
// 문자열을 Trim
//***************************************************************
function Trim(s) {
    return s.replace(/(^\s+)|(\s+$)/g, "");
}

//=================================================================================================================================
// 필드값으로 숫자만 입력받기
// Number Key Only
//=================================================================================================================================
function NumberOnly(obj) {

    var pattern = /[^0-9-.]/;

    if (pattern.test(obj.value)) {
        alert("숫자만 입력 가능합니다.");
        obj.value = "0";
        obj.focus();
        return false;
    }
}

function CheckDate(obj) {

    var regDate = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;

    if (obj.value == "") return;

    if (!regDate.test(obj.valueOf)) {
        alert("잘못된 날짜 형식입니다.\n\nYYYY-MM-DD형식으로 입력해 주십시요.");
        obj.value = "";
        obj.focus();
    }

    return false;
}



function ToZero(val) {

    return val == "" ? "0" : val;

}

//=========================================
// 년월을 입력받아 마지막 일를 반환한다(년월)
//=========================================
function LastDay(sYM) {
    if (sYM.length != 6) {
        alert("정확한 년월을 입력하십시오.");
        return;
    }

    if (!isDateYM(sYM)) {
        return;
    }

    daysArray = new makeArray(12);    // 배열을 생성한다.

    for (i = 1; i < 8; i++) {
        daysArray[i] = 30 + (i % 2);
    }

    for (i = 8; i < 13; i++) {
        daysArray[i] = 31 - (i % 2);
    }

    var sYear = sYM.substring(0, 4) * 1;
    var sMonth = sYM.substring(4, 6) * 1;

    if (((sYear % 4 == 0) && (sYear % 100 != 0)) || (sYear % 400 == 0)) {
        daysArray[2] = 29;
    } else {
        daysArray[2] = 28;
    }

    return daysArray[sMonth].toString();
}

//=========================================
// 천단위로 콤마구분
//=========================================
function Commify(n) {
    var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
    n += '';                          // 숫자를 문자열로 변환

    while (reg.test(n))
        n = n.replace(reg, '$1' + ',' + '$2');

    return n;
}

function ViewDSFolder() {
    InnoDS.ViewType = 2
}
function ViewDSFile() {
    InnoDS.ViewType = 1
}
function ViewFDFolder() {
    InnoFD.ViewType = 2
}
function ViewFDFile() {
    InnoFD.ViewType = 1
}

function PasteHtmlImage(objIr, virtualPath, objImageFileName) {

    sHTML = "<span style='color:#FF0000'><img src='" + virtualPath + "/" + $("#" + objImageFileName).val() + "' /></span>";
    oEditors.getById[objIr].exec("PASTE_HTML", [sHTML]);
}

function PopUploader(uploadType, hndImagFileName, hdnImageSave) {
    param = "UploadType=" + uploadType;
    param += "&hndImagFileName=" + hndImagFileName;
    param += "&hdnImageSave=" + hdnImageSave;

    openPopUp("/Common/Uploader.aspx?" + param, "Uploader", 490, 175, false);
}

// 파일이 제거되기 전 각각의 아이템 별로 발생하는 이벤트 입니다.
function OnBeforeRemoveItem(objName, index) {
    // 가상파일 삭제시 파일ID를 삭제정보 배열에 추가
    if (InnoDS.IsTempFile(index)) {
        delArray.push(InnoDS.GetFileID(index));
    }
}

// 사용자에 의해 파일이 제거되기전 발생하는 이벤트 입니다.
function OnRemoveItems(objName) {
    if (removeAllFlag) {
        for (var i = 0; i < InnoDS.GetFileCount(); i++) {
            if (InnoDS.IsTempFile(i)) {
                delArray.push(InnoDS.GetFileID(i));
            }
        }
    }
    removeAllFlag = true;
}


function Upload() {

    // 삭제된 파일 정보를 폼에 추가
    for (var i in delArray) {
        var oForm = document.aspnetForm;
        var oInput = document.createElement('input');
        oInput.type = 'hidden';
        oInput.name = '_innods_deleted_file';
        oInput.value = delArray[i];
        oForm.insertBefore(oInput, oForm.firstChild);
    }

    // 존재하는 파일 정보를 폼에 추가		
    for (var i = 0; i < InnoDS.GetFileCount(); i++) {
        if (InnoDS.IsTempFile(i)) {
            var oForm = document.aspnetForm;
            var oInput = document.createElement('input');
            oInput.type = 'hidden';
            oInput.name = '_innods_exist_file';
            oInput.value = InnoDS.GetFileID(i);
            oForm.insertBefore(oInput, oForm.firstChild);
        }
    }

    var addFileCount = 0;

    // 가상 파일을 제외한 실제 첨부파일 개수
    for (var i = 0; i < InnoDS.GetFileCount(); i++) {
        if (!InnoDS.IsTempFile(i)) {
            addFileCount++;
        }
    }
    // 첨부 파일이 있으면 컴포넌트로 파일전송
    if (addFileCount > 0) {

        // 서버에 저장될 서브 디렉토리 지정
        InnoDS.AddPostData("_SUB_DIR", SubDir);

        // 업로드 시작
        InnoDS.StartUpload();
    }
    else // 첨부 파일이 없으면 폼만 전송
    {
        InnoDSSubmit(document.aspnetForm);
        DoSubmit();
    }
}

function SetInnoDSInit(objName) {
    removeAllFlag = false;

    // 파일 삭제정보 전역배열
    delArray = new Array();

    // 선택된 모든 파일제거
    InnoDS.RemoveAllItems();

}


// OnUploadComplete는 파일 업로드 완료 후 발생하는 이벤트 입니다.
// 파일 업로드가 완료되면 f_write 폼 데이타와 업로드된 파일 정보를 함께 전송 합니다.
function OnUploadComplete(objName) {
    InnoDSSubmit(document.aspnetForm);
    DoSubmit();
}



/////////////////////////////////////////////////////////////////
//
// 임유석 추가 자바 스크립트
// 최초 작성일 : 2011-07-29
// 최종 변경일 : 2011-08-03
//
// 시작 
//
/////////////////////////////////////////////////////////////////

// 날짜 유효성 검사
function checkDateFromTo(startDate, endDate) {

    // 날짜 체크를 위한 정규식
    var regExp = /^\d{4}-\d{2}-\d{2}$/;

    // 시작날짜 체크
    if (!regExp.test(startDate)) {
        alert("시작일자 형식은 년(4자리)-월(2자리)-일(2자리) 이며 숫자만 입력 가능합니다.");
        return false;
    }

    // 종료날짜 체크
    if (!regExp.test(endDate)) {
        alert("종료일자 형식은 년(4자리)-월(2자리)-일(2자리) 이며 숫자만 입력 가능합니다.");
        return false;
    }

    // 시작날짜 스트링값을 date 형식으로 변환
    var yearStart = parseInt(startDate.substr(0, 4), 10);
    var monthStart = parseInt(startDate.substr(5, 2), 10);
    var dayStart = parseInt(startDate.substr(8), 10);

    if (!isValidMonth(monthStart)) {
        alert("입력하신 시작일자의 월(月)이 유효하지 않습니다.");
        return false;
    }

    if (!isValidDay(yearStart, monthStart, dayStart)) {
        alert("입력하신 시작일자의 일(日)이 유효하지 않습니다.");
        return false;
    }

    // 종료날짜 스트링값을 date 형식으로 변환
    var yearEnd = parseInt(endDate.substr(0, 4), 10);
    var monthEnd = parseInt(endDate.substr(5, 2), 10);
    var dayEnd = parseInt(endDate.substr(8), 10);

    if (!isValidMonth(monthEnd)) {
        alert("입력하신 종료일자의 월(月)이 유효하지 않습니다.");
        return false;
    }

    if (!isValidDay(yearEnd, monthEnd, dayEnd)) {
        alert("입력하신 종료일자의 일(日)이 유효하지 않습니다.");
        return false;
    }

    // 날짜 비교
    var dStart = new Date(yearStart, monthStart, dayStart);
    var dEnd = new Date(yearEnd, monthEnd, dayEnd);
    if (dStart > dEnd) {
        alert("종료일자가 시작일자보다 앞설 수 없습니다.");
        return false;
    }

    return true;
}

// 유효한(존재하는) 월(月)인지 체크
function isValidMonth(mm) {
    var m = parseInt(mm, 10);
    return (m >= 1 && m <= 12);
}

// 유효한(존재하는) 일(日)인지 체크
function isValidDay(yyyy, mm, dd) {
    var m = parseInt(mm, 10) - 1;
    var d = parseInt(dd, 10);

    var end = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    if ((yyyy % 4 == 0 && yyyy % 100 != 0) || yyyy % 400 == 0) {
        end[1] = 29;
    }

    return (d >= 1 && d <= end[m]);
}

// E-mail 유효성 검사
// 2011-08-03 추가
function checkEmail(email, msg) {

    // E-mail 위한 정규식
    var regExp = /^[a-z0-9_+.-]+@([a-z0-9-]+\.)+[a-z0-9]{2,4}$/;

    // 시작날짜 체크
    if (!regExp.test(email)) {
        // 메세지가 있을겨우 보여줌
        if (msg != "") alert(msg);
        return false;
    }

    return true;
}

/////////////////////////////////////////////////////////////////
//
// 끝 
// 임유석 추가 자바 스크립트
//
/////////////////////////////////////////////////////////////////