﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using DID.Common.Framework;

public partial class bpLog : BasePage
{ 
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            ddlhostId.Focus();
            this.InitControl(); 
        }
    }

        //초기화
    private void InitControl()
    {
        // fromdate 를 -3일 로 세팅한다.
        fromDate.Text = DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd");

        // todate 를 오늘날짜로 세팅한다.
        toDate.Text   = DateTime.Now.ToString("yyyy-MM-dd");
    }
     

    //엑셀
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        string fileName = HttpUtility.UrlEncode("bpLog", new UTF8Encoding()) + ".xls";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-Disposition", "attachment;filename=" + fileName );
        Response.Charset = "";
        gvList.EnableViewState = false;
        System.IO.StringWriter tw = new System.IO.StringWriter();
        tw.WriteLine("");
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        gvList.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time. 
    }
     

    // 조회
    protected void btnSelect_Click(object sender, EventArgs e)
    {
        getDateList();
    }


    private void getDateList()
    {
        using (DID.Service.Log.Log obj = new DID.Service.Log.Log())
        {
            DataTable dt = null;
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            dt = obj.GetBpLogList(ddlhostId.Text, fromDateVar, toDateVar);
            this.gvList.DataSource = dt;
            this.gvList.DataBind();

            // 데이타 여부에 따라 엑셀저장 버튼 활성/비활성
            if (dt.Rows.Count > 0)
            {
                btnExcel.Enabled = true;
                btnExcel.Visible = true;
            }
            else
            {
                btnExcel.Enabled = false;
                btnExcel.Visible = false;
            }
        }
    }




    #region 그리드뷰 gvList_RowDataBound()
    /// <summary>
    /// 그리드뷰 gvList_RowDataBound
    /// </summary>
    protected void gvList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        DataRowView drv = (DataRowView)e.Row.DataItem;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }
    }
    #endregion


//    protected void gvList_Sorting(object sender, GridViewSortEventArgs e)
//    {

   
//// Ascending
//// descending

        

//        //this.ScriptExecute(e.SortDirection.ToString() );
//        //this.ScriptExecute(e.SortExpression);
       

//        using (DID.Service.Log.Log obj = new DID.Service.Log.Log())
//        {
//            DataTable dt = null;
//            string fromDateVar = fromDate.Text.Replace("-", "");
//            string toDateVar = toDate.Text.Replace("-", "");

//            dt = obj.GetScheduleLogList(ddlhostId.Text, fromDateVar, toDateVar);

//            e.SortDirection = SortDirection.Descending;
//            this.gvList.Sort(e.SortExpression, e.SortDirection);

//            this.gvList.DataSource = dt;
//            this.gvList.DataBind();
//        } 
         
         
//    }

    // 그리드뷰 셀병합
    protected void gvList_PreRender(object sender, EventArgs e)
    {

        Common.GroupColumn(gvList, 0); //그리드뷰의 ID, 병합할 컬럼
        Common.GroupColumn(gvList, 1, 0); //그리드뷰의 ID, 병합할 컬럼

    }
}
