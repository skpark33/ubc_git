﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/PopupMasterPage.master" AutoEventWireup="true" CodeFile="log_setup.aspx.cs" Inherits="Log_log_setup" Title="로그설정" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language=javascript>

    function duration()
    { 
        var durationDay = $("#<%=duration.ClientID %>").val();
        durationDay = durationDay.replace(' ','').replace(/s*$/, ''); 

        if( durationDay == "" )
        {
            alert("로그보존기간을 입력하세요.");
            $("#<%=duration.ClientID %>").focus();
            $("#<%=duration.ClientID %>").val(0);
            return false ;
        }   
         
        var pattern = /[^0-9-.]/;
        if (pattern.test(durationDay)) {
            alert("숫자만 입력 가능합니다.");
            $("#<%=duration.ClientID %>").focus();
            $("#<%=duration.ClientID %>").val(0);
            return false;
        }
 
    }


</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 
<table   class="boardInputType mb10"  cellpadding="0" cellspacing="0" border="0" >
<tr>
<td align=center>로그보존기간</td>
<td align=center>
    <asp:TextBox ID="duration" runat="server" MaxLength=4   Width=40></asp:TextBox>일

 <%--   <asp:ListBox ID="duration" runat="server" Rows="1" >            
    </asp:ListBox>  --%>
</td>
</tr>

<tr>
<td align=right>콘텐츠패키지 변경결과 로그기록</td>
<td align=center>
    <asp:CheckBox ID="chkSchedule" runat="server" />
</td>
</tr>
<tr>
<td align=right>콘텐츠패키지 변경명령 로그기록</td>
<td align=center>
    <asp:CheckBox ID="chkApply" runat="server" />
</td>
</tr>
<tr>
<td align=right>전원ON/OFF 로그기록</td>
<td align=center>
    <asp:CheckBox ID="chkPower" runat="server" />
</td> 
</tr>
<tr>
<td align=right>연결상태 로그기록</td>
<td align=center>
    <asp:CheckBox ID="chkConnection" runat="server" />
</td>
</tr>
<tr>
<td align=right>장애 로그기록</td>
<td align=center>
    <asp:CheckBox ID="chkFault" runat="server" />
</td>
</tr>
<tr>
<td align=right>콘텐츠 배포 로그기록</td>
<td align=center>
    <asp:CheckBox ID="chkDownload" runat="server" />
</td>
</tr>
<tr> 
<td align=right>로그인 로그기록</td>
<td align=center>
    <asp:CheckBox ID="chkLogin" runat="server" />
</td>
</tr>
<tr>
<td align=right>간단견적 로그기록</td>
<td align=center>
    <asp:CheckBox ID="chkEstimate" runat="server" />
</td>
</tr>


<tr>
<td colspan=2 align=center >
<br />
 <asp:Button ID="btnSave" runat="server" Text="저장" onclick="btnSave_Click"   OnClientClick="return duration();"    />
 <asp:Button ID="btnClose" runat="server" Text="닫기" OnClientClick="self.close();" /> 
<br />
</td> 
</tr>
</table>

     
    
</asp:Content>

