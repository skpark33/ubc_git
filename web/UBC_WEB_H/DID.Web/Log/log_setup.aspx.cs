﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using DID.Common.Framework;

public partial class Log_log_setup : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        { 
            this.InitControl();

        }
    }


    private void InitControl()
    {
        ////duration 0 부터 366 까지  
        //for (int i = 0; i < 366; i++)
        //{
        //    ListItem addList = new ListItem();
        //    addList.Value = i.ToString();
        //    addList.Text  = i.ToString();
        //    duration.Items.Add(addList);
        //} 


        // 로그인한 siteId 의 로그보관주기와 체크박스들을 디비의 값으로 정해준다.
        using (DID.Service.Log.Log obj = new DID.Service.Log.Log())
        {

            DataTable dt = null;
            // Profile.SITE_ID 이 ubc_lmo 테이블에 없으면, 해당아이디로 insert후 셀렉트한다.
            dt = obj.GetLogSetupList();   


            // 로그보존기간
            duration.Text  = dt.Rows[0]["duration"].ToString();


            //전원ON/OFF 로그기록
            if (dt.Rows[0]["powerState"].ToString() == "1")
            {   chkPower.Checked = true;  }
            else
            {   chkPower.Checked = false ; }
            //연결상태 로그기록
            if (dt.Rows[0]["connectionState"].ToString() == "1")
            { chkConnection.Checked = true; }
            else
            { chkConnection.Checked = false; }

            //콘텐츠패키지 변경 로그기록
            if (dt.Rows[0]["scheduleState"].ToString() == "1")
            { chkSchedule.Checked = true; }
            else
            { chkSchedule.Checked = false; }


            //장애 로그기록
            if (dt.Rows[0]["faultState"].ToString() == "1")
            { chkFault.Checked = true; }
            else
            { chkFault.Checked = false; }

            //로그인 로그기록
            if (dt.Rows[0]["loginState"].ToString() == "1")
            { chkLogin.Checked = true; }
            else
            { chkLogin.Checked = false; }

            //간단견적 로그기록
            if (dt.Rows[0]["estimateState"].ToString() == "1")
            { chkEstimate.Checked = true; }
            else
            { chkEstimate.Checked = false; }

            //콘텐츠 배포 로그기록
            if (dt.Rows[0]["downloadState"].ToString() == "1")
            { chkDownload.Checked = true; }
            else
            { chkDownload.Checked = false; }

              
        }




    }



    
    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        // 업데이트만 한다.
        using (DID.Service.Log.Log  obj = new DID.Service.Log.Log ())
        {   
            
            string chkPowerVal;
            string chkConnectionVal;
            string chkScheduleVal;

            string chkFaultVal;
            string chkLoginVal;
            string chkEstimateVal;
            string chkDownloadVal;


            //전원ON/OFF 로그기록
            if(chkPower.Checked)
            {    chkPowerVal = "1"; }
            else
            {    chkPowerVal = "0"; }

            //연결상태 로그기록
            if(chkConnection.Checked)
            {    chkConnectionVal = "1"; }
            else
            {    chkConnectionVal = "0"; }
             
            //콘텐츠패키지 변경 로그기록
            if(chkSchedule.Checked)
            {    chkScheduleVal = "1"; }
            else
            {    chkScheduleVal = "0"; }


            //장애 로그기록
            if (chkFault.Checked)
            { chkFaultVal = "1"; }
            else
            { chkFaultVal = "0"; }
            //로그인 로그기록
            if (chkLogin.Checked)
            { chkLoginVal = "1"; }
            else
            { chkLoginVal = "0"; }
            //간단견적 로그기록
            if (chkEstimate.Checked)
            { chkEstimateVal = "1"; }
            else
            { chkEstimateVal = "0"; }
            //콘텐츠 배포 로그기록
            if (chkDownload.Checked)
            { chkDownloadVal = "1"; }
            else
            { chkDownloadVal = "0"; }

            obj.GetLogSetupUpdate(duration.Text, chkPowerVal, chkConnectionVal, chkScheduleVal, chkFaultVal, chkLoginVal, chkEstimateVal, chkDownloadVal);

            this.ScriptExecute("설정 되었습니다.");
        
        }



    }









}
