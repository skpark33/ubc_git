﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="playLog.aspx.cs" Inherits="playLog" Title="제목 없음" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoExcel() {
        $("#<%= btnExcel.ClientID%>").click();
    }

    function DoSetup() {
        window.open("./log_setup.aspx", "log_setup", "toolbar=0,scrollbars=0,location=0,directories=0,status=0,menubar=0,resizable=no,width=480,height=340");
    }

    function DoSelect() {

        // 날짜 유효성 검사 시작

        var strStart_date = $("#<%= fromDate.ClientID%>").val();
        var strEnd_Date = $("#<%= toDate.ClientID%>").val();

        if (!checkDateFromTo(strStart_date, strEnd_Date)) 
            return false;

        return true;
      
    }

    
     
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
   <%-- <asp:Button ID="btnSelect" runat="server" OnClick="btnSelect_Click" />
    <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_Click" style="display:none" />
  --%>
 
 <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
  <colgroup>
  <col width="auto" />
  <col width="50%" />
  </colgroup>
  <tr>
    <td><span class="subTitle"><span id=""><img src="/Asset/Images/body/bullet01.gif" /> 플레이어 오류 로그</span></span> </td>
  </tr>
</table>
    <div class='topOptionBtns'>
        <%--<span class='btnType btnSearch'><span><a href='javascript:void(0)' onclick='javascript:DoSelect();'>
        조회</a></span></span>    
        <span class='btnType btnSearch'><span><a href='javascript:void(0)' onclick='javascript:DoExcel();'>
        엑셀</a></span></span>        
        <span class='btnType btnModify'><span><a href='javascript:void(0)' onclick='javascript:DoSetup();'>
        설정</a></span></span>  --%>      
        
        <asp:ImageButton ID="btnSelect" runat="server" ToolTip="조회" OnClick="btnSelect_Click" OnClientClick='return DoSelect()' ImageUrl="~/Asset/Images/button/btn_search.jpg"  />
        <asp:ImageButton ID="btnExcel"  runat="server" ToolTip="엑셀저장" OnClick="btnExcel_Click"  ImageUrl="~/Asset/Images/button/btn_excel.jpg" Visible=false />
        <%--<asp:ImageButton ID="btnSetup"  runat="server" ToolTip="설정" OnClientClick="DoSetup()"  ImageUrl="~/Asset/Images/button/btn_setup.jpg"  /> --%>
        
    </div>    

    <table class="boardInputType mb10" cellpadding="0" cellspacing="0" border="0">
		<colgroup>
		<col width="100px" />
		<col width="auto" />
		<col width="100px" />
		<col width="auto" />		
	</colgroup>
    <tr>
        <th><span>단말ID</span></th>
        <td>    
            <asp:TextBox ID="ddlhostId" runat="server"  CssClass="textType"></asp:TextBox>
        </td>
        <th><span>기간</span></th>
        <td>        
          <asp:TextBox ID="fromDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />
            ~
          <asp:TextBox ID="toDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />
        </td>                
    </tr>
    </table>
    
    
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate> 
        <div class="container" >
        <!--   CssClass="boardListType" AllowSorting=true  onsorting="gvList_Sorting"   -->
        <asp:GridView ID="gvList" runat="server" OnRowDataBound="gvList_RowDataBound" OnPreRender="gvList_PreRender" 
        AutoGenerateColumns="false" ShowHeader="true" CssClass="boardListType2"  >
        <Columns>  
        
            <asp:BoundField DataField="siteId"          HeaderText="소속그룹ID" HeaderStyle-Width="120px" ItemStyle-HorizontalAlign=Center SortExpression="siteId" ItemStyle-VerticalAlign=Top />
            <asp:BoundField DataField="siteName"        HeaderText="소속그룹"   HeaderStyle-Width="120px" ItemStyle-HorizontalAlign=Center SortExpression="siteName" ItemStyle-VerticalAlign=Top  />
            <asp:BoundField DataField="hostId"          HeaderText="단말ID"     HeaderStyle-Width="120px" ItemStyle-HorizontalAlign=Center SortExpression="hostId" ItemStyle-VerticalAlign=Top />
            <asp:BoundField DataField="playDate"        HeaderText="방송일"     HeaderStyle-Width="120px" ItemStyle-HorizontalAlign=Center SortExpression="playDate" ItemStyle-VerticalAlign=Top />                       
            <asp:BoundField DataField="programId"       HeaderText="패키지명"   HeaderStyle-Width="150px" ItemStyle-HorizontalAlign=Left   SortExpression="programId" />
            <%--<asp:BoundField DataField="contentsId"      HeaderText="콘텐츠ID"                             ItemStyle-HorizontalAlign=Left   SortExpression="contentsId" />--%>
            <asp:BoundField DataField="contentsName"    HeaderText="콘텐츠명"                             ItemStyle-HorizontalAlign=Left   SortExpression="contentsName" />
            <asp:BoundField DataField="playCount"       HeaderText="실행횟수"   HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign=Right  SortExpression="playCount" />
            <asp:BoundField DataField="failCount"       HeaderText="방송실패수" HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign=Right  SortExpression="failCount" />
            
        </Columns>
        <EmptyDataTemplate>
            <table style="width:100%;height:290px;">
                <tr>
                    <td style="text-align:center">
                        조회된 데이터가 없습니다.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        
        </asp:GridView>   
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

