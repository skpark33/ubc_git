﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" EnableSessionState="False" ValidateRequest="false" CodeFile="Contents_Write.aspx.cs" Inherits="Contents_Write" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/default.css" rel="stylesheet" type="text/css" />
<link href="/Asset/Editor/css/style.css" rel="stylesheet" type="text/css" />
<link href="/Asset/Editor/css/maggot.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">

    var oEditors = [];
    var delArray = [];


    $(function() {
        nhn.husky.EZCreator.createInIFrame({
            oAppRef: oEditors,
            elPlaceHolder: "<%=ir1.ClientID %>",
            sSkinURI: "/Asset/Editor/SEditorSkin.html",
            fCreator: "createSEditorInIFrame"
        });

        var inlineProgressBar = NeatUploadPB.prototype.Bars["<%= inlineProgressBar.ClientID%>"];
        var origDisplay = inlineProgressBar.Display;
        inlineProgressBar.Display = function() {

            var elem = document.getElementById(this.ClientID);
            elem.parentNode.style.display = "block";
            origDisplay.call(this);
        }
        inlineProgressBar.EvalOnClose = "NeatUploadMainWindow.document.getElementById('" + inlineProgressBar.ClientID + "').parentNode.style.display = \"none\";";
    });

    function ValidateForm() {
        oEditors.getById["<%=ir1.ClientID %>"].exec("UPDATE_IR_FIELD", []);        
        if (Trim($("#<%= txtTitle.ClientID%>").val()) == "") {
            alert("제목을 입력하십시요.");
            $("#<%= txtTitle.ClientID%>").focus();
            return false;
        }
        if (oEditors.getById["<%=ir1.ClientID %>"].getIR().length == 0) {
            alert("내용을 입력하십시요.");
            oEditors.getById["<%=ir1.ClientID %>"].exec("FOCUS", []); 
            return false;
        }
        return true;
    }

    function DoSave() {
        if (ValidateForm()) {
            $("#<%= hdnContents.ClientID%>").val($("#<%=ir1.ClientID %>").val());
            $("#<%= btnSave.ClientID%>").click();
        }
    }

    function DoList() {
        location.href = "Contents_List.aspx";
    }

</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnContents" runat="server"  />
    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="display:none" />
    <input type="hidden" id="hndImagFileName" name="hndImagFileName" />
    <input type="button" id="hdnImageSave" name="hdnImageSave" style="display:none" value="ffff" onclick="PasteHtmlImage('<%= ir1.ClientID%>','<%=this.FILE_PATH_COMMON%>','hndImagFileName');" />
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <asp:Literal ID="lblSubTitle" runat="server" Text="" /></span>
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn("SAVE#LIST") %>
    </div>    
    
    <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
    <colgroup>
	    <col width="13%" />
	    <col width="auto" /> 
	    <col width="13%" />
	    <col width="auto" /> 
	    <col width="13%" />
	    <col width="auto" /> 
    </colgroup>
    <tr>
        <th>제목<font color="red"> *</font></th>
        <td colspan="5"> 
            <asp:TextBox ID="txtTitle" runat="server"  CssClass="textType" Width="660px" />
        </td>
    </tr> 
    <tr>
        <th>목록 상단 위치</th>
        <td colspan="5">
            <asp:RadioButtonList ID="rdoTopYn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radioType"  >
                <asp:ListItem Text="예&nbsp;" Value="Y"  />
                <asp:ListItem Text="아니오" Value="N" Selected="True" />
            </asp:RadioButtonList>
        </td>
    </tr> 
    <tr>
        <th>내용<font color="red"> *</font></th>
        <td align="right" colspan="5">
            <span onclick="PopUploader('image','hndImagFileName','hdnImageSave');" style="cursor:pointer">
            <img src="/Asset/Images/body/img.gif" />
            이미지 업로드</span>
            <textarea name="ir1" id="ir1" runat="server" style="width:100%;height:300px" ></textarea>
        </td>
    </tr> 
    <tr id="trFileView" runat="server" visible="false">
        <th>첨부된 파일</th>
        <td style="text-align:left" colspan="5">
            <asp:Repeater ID="rptFile" runat="server">
            <ItemTemplate>
                <input type="checkbox" value="<%# Eval("attachId") %>|<%# Util.DB2HTML(Eval("fileName").ToString()) %>" name="_delete_file" /><%# Util.DB2HTML(Eval("fileName").ToString())%><br />        
            </ItemTemplate>
            </asp:Repeater><p></p>
            <span style="color:#8b0000">◆ 삭제하려면 해당 파일을 체크하고 저장하면 됩니다.</span> 
        </td>
    </tr>
    <tr>
        <th>파일첨부</th>
        <td align="left" colspan="5">
        
			<p>File(s) to upload:</P> 
			<Upload:MultiFile id="multiFile" runat="server" useFlashIfAvailable="false">
				<asp:Button id="multiFileButton" Text="찾아보기" Enabled="<%# multiFile.Enabled %>" runat="server"/>
			</Upload:MultiFile>

			<div style="display:none;">
			<Upload:ProgressBar id="inlineProgressBar" runat="server" Inline="true" height="50px" Width="630px" Triggers="btnSave">
			<asp:Label id="label" runat="server" Text="Check Progress"/>
			</Upload:ProgressBar>
			</div>

        </td>
    </tr> 
    </table> 
    

</asp:Content>

