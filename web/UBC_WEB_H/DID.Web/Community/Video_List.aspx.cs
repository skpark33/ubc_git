﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

/// <summary>
/// 목록 페이지
/// </summary>
public partial class Video_List : BasePage
{
    /// <summary>
    /// 페이지당 목록 수
    /// </summary>
    protected int PAGE_SIZE = 10;
    /// <summary>
    /// 현재 페이지
    /// </summary>
    protected int CURRENT_PAGE
    {
        get { return Pager.CurrentPageIndex; }
        set { Pager.CurrentPageIndex = value; }
    }
    /// <summary>
    /// 목록수
    /// </summary>
    protected int TOTAL_ROW_COUNT { get; set; }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.InitControl();
            this.DataBindControl();
        }
    }
    #region 컨트롤 초기화 - InitControl()
    /// <summary>
    /// 컨트롤 초기화
    /// </summary>
    private void InitControl()
    {
        if (!string.IsNullOrEmpty(this.Pgn))
            this.CURRENT_PAGE = Convert.ToInt32(this.Pgn);
    }
    #endregion

    #region 페이징 이벤트 - PagerPaging(object sender, EventArgs e)
    /// <summary>
    /// 페이징 이벤트
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PagerPaging(object sender, EventArgs e)
    {
        this.DataBindControl();
    }
    #endregion

    #region 데이터 바인딩 - DataBindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void DataBindControl()
    {
        DataTable dt = this.GetBoardList();

        this.dList.DataSource = dt;
        this.dList.DataBind();
        this.pnlMsg.Visible = dt.Rows.Count == 0;

        this.Pager.PageSize = this.PAGE_SIZE;
        this.Pager.TotalRowCount = this.TOTAL_ROW_COUNT;
        this.Pager.DataBind();
    }
    #endregion

    #region 게시물 데이터 조회 - GetBoardList()
    /// <summary>
    /// 게시물 데이터 조회
    /// </summary>
    /// <returns></returns>
    private DataTable GetBoardList()
    {
        int TotalRows = 0;
        DataTable dt = null;

        using (DID.Service.Community.Board obj = new DID.Service.Community.Board())
        {
            dt = obj.GetList((int)Common.BoardMasterID.Video
                            , this.Profile.SITE_ID
                            , ""
                            , ddlSearchKind.SelectedValue
                            , txtSearchWord.Text.Trim()
                            , Profile.COMP_TYPE
                            , this.CURRENT_PAGE
                            , this.PAGE_SIZE
                            , out TotalRows);
        }

        this.TOTAL_ROW_COUNT = TotalRows;

        return dt;

    }
    #endregion

    protected void dList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        string[] imgExtention = {".jpg",".png" ,".gif"};

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)(e.Item.DataItem);

            using(DID.Service.Community.Video obj = new DID.Service.Community.Video())
            {
                DataTable dt = obj.GetAttachList(Convert.ToInt64(drv["boardId"]));

                foreach (string ext in imgExtention)
                {
                    dt.DefaultView.RowFilter = string.Format("fileName like '%{0}'",ext);
                    if (dt.DefaultView.Count > 0)
                    {
                        Image img = e.Item.FindControl("img") as Image;
                        img.ImageUrl = this.FILE_PATH_VIDEO + "/" + dt.DefaultView[0]["fileName"].ToString();
                        return;
                    }
                }
            }
        }

    }

    #region 검색 버튼 클릭 - btnSearch_click(object sender, EventArgs e)
    /// <summary>
    /// 검색 버튼 클릭 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_click(object sender, EventArgs e)
    {
        this.DataBindControl();
    }
    #endregion
}
