﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Faq_List.aspx.cs" Inherits="Faq_List" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<style type="text/css"> 

#items div.arrow { background:transparent url(arrows.png) no-repeat scroll 0px -16px; width:16px; height:16px; display:block;}
#items div.up { background-position:0px 0px;}
</style> 

<script language="javascript" type="text/javascript">

    function DoReg() {
        location.href = "Faq_Write.aspx";
    }

    $(document).ready(function() {

    $('.boardListType tr:even').addClass("alt");

        $("#items tr.itemDetail").hide();
        $("#items tr.data td.clickable").click(function() {
            $(this).parent().next("tr").toggle().toggleClass('highlight');
            if ($(this).parent().hasClass('alt')) {
                $(this).parent().next("tr").addClass('alt');
            } else {
                $(this).parent().next("tr").removeClass('alt');
            }
            $(this).parent().find(".arrow").toggleClass("up");
            $('#items tr.data td').css('border-collapse', 'collapse');
        });

    });    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <table style="width:100%;height:100%" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
    <colgroup>
        <col width="auto" />
        <col width="50%" />
    </colgroup>
    <tr>
        <td>
            <span class="subTitle"><span>FAQ</span></span>
        </td>
    </tr>
    </table>
        
    <div class='topOptionBtns'>
        <span class='btnType btnRegister'><span><a href='javascript:void(0)' onclick='javascript:DoReg();'>등록</a></span></span>        
    </div>    
    
    <table style="width:100%;margin-top:10px" class="boardListType" >
    <colgroup>
        <col width="20%" />
        <col width="auto" />
    </colgroup>
    <tr>
        <th>구분</th>    
        <th>FAQ</th>    
    </tr>
    <asp:Repeater ID="rptList" runat="server">
    <ItemTemplate>
    <tr>
        <td class="clickable" ><%# Eval("faqCd") %>aa</td>
        <td class="clickable" ><%# Util.DB2HTML(Eval("contents").ToString()) %></td>
    </tr>
    <tr>
        <td co class="clickable" ><%# Util.DB2HTML(Eval("contents_reply").ToString()) %></td>
    </tr>
    </ItemTemplate>
    </asp:Repeater>
    </table>
                
    <div id="pagenum2" style="margin-top:15px;display:none">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>     
       	
</asp:Content>

