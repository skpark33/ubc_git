﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Faq_Write.aspx.cs" Inherits="Faq_Write" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Innorix/InnoDS/InnoDS.js"></script>

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/default.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">

    var oEditors = [];
    var delArray = [];
    

    $(function() {
        nhn.husky.EZCreator.createInIFrame({
            oAppRef: oEditors,
            elPlaceHolder: "<%=ir1.ClientID %>",
            sSkinURI: "/Asset/Editor/SEditorSkin.html",
            fCreator: "createSEditorInIFrame"
        });
        
        nhn.husky.EZCreator.createInIFrame({
            oAppRef: oEditors,
            elPlaceHolder: "<%=ir2.ClientID %>",
            sSkinURI: "/Asset/Editor/SEditorSkin.html",
            fCreator: "createSEditorInIFrame"
        });
        
    });


    function ValidateForm() {
        oEditors.getById["<%=ir1.ClientID %>"].exec("UPDATE_IR_FIELD", []);        
        oEditors.getById["<%=ir2.ClientID %>"].exec("UPDATE_IR_FIELD", []);        
        
        if (oEditors.getById["<%=ir1.ClientID %>"].getIR().length == 0) {
            alert("질문을 입력하십시요.");
            oEditors.getById["<%=ir1.ClientID %>"].exec("FOCUS", []); 
            return false;
        }
        if (oEditors.getById["<%=ir2.ClientID %>"].getIR().length == 0) {
            alert("답변을 입력하십시요.");
            oEditors.getById["<%=ir2.ClientID %>"].exec("FOCUS", []); 
            return false;
        }
        return true;
    }

    function DoSave() {
        if (ValidateForm()) {

            $("#<%= hdnContents.ClientID%>").val($("#<%=ir1.ClientID %>").val());
            $("#<%= hdnContentsReply.ClientID%>").val($("#<%=ir2.ClientID %>").val());

           DoSubmit();
        }        
    }

    function DoList() {
        location.href = "Board_List.aspx";
    }

    function DoSubmit(){
        $("#<%= btnSave.ClientID%>").click();
    }

</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnContents" runat="server"  />
    <asp:HiddenField ID="hdnContentsReply" runat="server"  />

    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="display:none" />

    <input type="hidden" id="hndImagFileName" name="hndImagFileName" />
    <input type="button" id="hdnImageSave" name="hdnImageSave" style="display:none" value="ffff" onclick="PasteHtmlImage('<%= ir1.ClientID%>','hndImagFileName');" />
    <input type="hidden" id="hndImagFileName2" name="hndImagFileName2" />
    <input type="button" id="hdnImageSave2" name="hdnImageSave2" style="display:none" value="ffff" onclick="PasteHtmlImage('<%= ir2.ClientID%>','hndImagFileName2');" />
    
    <table style="width:100%;height:100%" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><asp:Label ID="lblSubTitle" runat="server" Text=""></asp:Label></span>
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <span class='btnType btnRegister'><span><a href='javascript:void(0)' onclick='javascript:DoSave();'>저장</a></span></span>        
        <span class='btnType btnSearch'><span><a href='javascript:void(0)' onclick='javascript:DoList();'>목록</a></span></span>        
    </div>    
    
    <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
	    <colgroup>
	    <col width="15%" />
	    <col width="auto" /> 
    </colgroup>
    <tr>
        <th>구분</th>
        <td> 
            <asp:DropDownList ID="rdoFaqCd" runat="server"></asp:DropDownList>
        </td>
    </tr> 
    <tr>
        <th>질문</th>
        <td align="right">
            <span onclick="PopUploader('image','hndImagFileName','hdnImageSave');" style="cursor:pointer">이미지 업로드</span>
            <textarea name="ir1" id="ir1" runat="server" style="width:100%;height:200px" ></textarea>
        </td>
    </tr> 
    <tr>
        <th>답변</th>
        <td align="right">
            <span onclick="PopUploader('image','hndImagFileName2','hdnImageSave2');" style="cursor:pointer">이미지 업로드</span>
            <textarea name="ir2" id="ir2" runat="server" style="width:100%;height:200px" ></textarea>
        </td>
    </tr> 
    </table> 
</asp:Content>

