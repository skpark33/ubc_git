﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Contents_View.aspx.cs" Inherits="Contents_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Innorix/InnoFD/InnoFD.js"></script>

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/Common/jwplayer.js"></script>
<script type="text/javascript" src="/Common/FLVPDF.js"></script>

<script language="javascript" type="text/javascript">
    function DoList() {
        location.href = "Contents_List.aspx?pgn=<%=this.Pgn%>";
    }
    function DoUpdate() {
        location.href = "Contents_Write.aspx?boardId=<%=this.BoardID%>&pgn=<%=this.Pgn%>";
    }
    function DoDelete() {
        if(confirm('삭제하시겠습니까?')){
            $("#<%=btnDelete.ClientID%>").click();
        }
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:HiddenField ID="hdnBoardId" runat="server" />
    <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" style="display:none" />
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><span><img src="/Asset/Images/body/bullet01.gif" /> 컨텐츠 조회</span></span>
                
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn("LIST#UPD#DEL")%>
    </div>    

    <asp:FormView ID="fvBoard" runat="server" style="width:100%">
    <ItemTemplate>
        <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
        <colgroup>
	        <col width="15%" />
	        <col width="auto" />
	        <col width="15%" />
	        <col width="auto" />  
        </colgroup>
        <tr>
        <th>제목</th>
            <td colspan="3">
                <%# Util.DB2HTML(Eval("title").ToString()) %>
            </td> 
        </tr> 
        <tr>
            <th>등록자</th>
            <td>
                <%# Util.DB2HTML(Eval("userName").ToString())%>
            </td>
            <th>등록일</th>
            <td>
                <%# Eval("createDate" ,"{0:yyyy-MM-dd}") %>
            </td>
        </tr>
        <tr>
            <th>목록 상단 위치</th>
            <td>
                <%# Convert.ToBoolean(Eval("topYn")) ? "예" : "아니오" %>
            </td>
            <th>조회수</th>
            <td>
                <%# Eval("readCount" ,"{0:n0}") %>
            </td>
        </tr> 
        <tr>
            <th>첨부 파일</th>
            <td colspan="3" align="left">
                <asp:Repeater ID="rptFile" runat="server">
                <ItemTemplate>
        			<img src="/Asset/Images/attach.gif">
        			<span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%# Eval("attachId") %>&boardMasterId=<%=(int)Common.BoardMasterID.Contents %>'">
                        <%# Util.DB2HTML(Eval("fileName").ToString())%></span><br />        
                </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr> 
        <tr>
            <th>내용</th>
            <td colspan="3" >
                <div class="smartOutput">
                <%# Util.DB2HTML(Eval("contents").ToString()) %>
                </div>
            </td>
        </tr> 
        </table>
    </ItemTemplate> 
    </asp:FormView>
        
</asp:Content>

