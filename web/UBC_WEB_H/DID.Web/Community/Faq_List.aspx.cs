﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

/// <summary>
/// 목록 페이지
/// </summary>
public partial class Faq_List : BasePage
{
    /// <summary>
    /// 페이지당 목록 수
    /// </summary>
    protected int PAGE_SIZE = 10;
    /// <summary>
    /// 현재 페이지
    /// </summary>
    protected int CURRENT_PAGE
    {
        get { return Pager.CurrentPageIndex; }
        set { Pager.CurrentPageIndex = value; }
    }
    /// <summary>
    /// 목록수
    /// </summary>
    protected int TOTAL_ROW_COUNT { get; set; }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.InitControl();
            this.DataBindControl();
        }
    }
    #region 컨트롤 초기화 - InitControl()
    /// <summary>
    /// 컨트롤 초기화
    /// </summary>
    private void InitControl()
    {
        if (!string.IsNullOrEmpty(this.Pgn))
            this.CURRENT_PAGE = Convert.ToInt32(this.Pgn);
    }
    #endregion

    #region 페이징 이벤트 - PagerPaging(object sender, EventArgs e)
    /// <summary>
    /// 페이징 이벤트
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PagerPaging(object sender, EventArgs e)
    {
        this.DataBindControl();
    }
    #endregion

    #region 데이터 바인딩 - DataBindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void DataBindControl()
    {
        DataTable dt = this.GetBoardList();

        this.rptList.DataSource = dt;
        this.rptList.DataBind();
    }
    #endregion

    #region 게시물 데이터 조회 - GetBoardList()
    /// <summary>
    /// 게시물 데이터 조회
    /// </summary>
    /// <returns></returns>
    private DataTable GetBoardList()
    {
        DataTable dt = null;


        var dd = from o in dt.AsEnumerable()
                 where o.Field<int>("ddd") > 0
                 select new
                 {
                     customID = o.Field<string>("dd")
                 };

        using (DID.Service.Community.Faq obj = new DID.Service.Community.Faq())
        {
            //dt = obj.GetFaqList((int)Common.BoardMasterID.Faq, "");
        }

        return dt;
    }
    #endregion
}
