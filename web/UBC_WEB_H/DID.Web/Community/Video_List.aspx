﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Video_List.aspx.cs" Inherits="Video_List" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoQuery(code) {
        $("#<%=txtSearchWord.ClientID %>").val(code);
        $("#<%= btnSearch.ClientID%>").click();
    }


    function DoReg() {
        location.href = "Video_Write.aspx";
    }
    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Button id="btnSearch" runat="server" OnClick="btnSearch_click" style="display:none"  />

    <table style="width:800;height:100%" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><span><img src="/Asset/Images/body/bullet01.gif" /> 동영상 매뉴얼</span></span>
            </td>
        </tr>
    </table>
        
    <div class='topOptionBtns' style="width:800px">
        <%= Common.SetScriptBtn("REG") %>
    </div>    
    
    <div>
        <img src="/Asset/Images/body/bt_down00.gif" onclick="DoQuery('')" style="cursor:pointer" />
        <img src="/Asset/Images/body/bt_down01.gif" onclick="DoQuery('1')" style="cursor:pointer" />
        <img src="/Asset/Images/body/bt_down02.gif" onclick="DoQuery('2')" style="cursor:pointer" />
        <img src="/Asset/Images/body/bt_down03.gif" onclick="DoQuery('3')" style="cursor:pointer" />
                           
        <asp:DropDownList ID="ddlSearchKind" runat="server" CssClass="selectType" style="display:none">
            <asp:ListItem Text="전체" Value=""></asp:ListItem>
            <asp:ListItem Text="제목" Value="title"></asp:ListItem>
            <asp:ListItem Text="내용" Value="contents"></asp:ListItem>
            <asp:ListItem Text="등록자" Value="userNm"></asp:ListItem>
            <asp:ListItem Text="구분" Value="contentCd" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtSearchWord" runat="server" CssClass="textType" Width="300px" style="display:none"/>
    </div>
    <asp:DataList ID="dList" runat="server" Width="800" style="margin-top:10px" RepeatColumns="5" BorderWidth="0" CellPadding="0" CellSpacing="0" RepeatDirection="Horizontal" RepeatLayout="Table" 
    OnItemDataBound="dList_ItemDataBound">  
    <ItemStyle HorizontalAlign="Center" />
    <ItemTemplate>
    
        <table width="132" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <span style="cursor:pointer" onclick="location.href='Video_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>'">
                    <asp:Image ID="img" Width="132" Height="76" runat="server" ImageUrl="/Asset/Images/body/no_img.gif" onerror="this.src='/Asset/Images/body/no_img.gif'"  />
                </span>
            </td>
        </tr>
        <tr>
            <td class="player_title" align="left">
                <span style="cursor:pointer" onclick="location.href='Video_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>'"><%#Util.DB2HTML(Eval("title").ToString()) %></span>
            </td>
        </tr>
        <tr>
            <td class="player_writer" align="left">- 등록자 : <%#Util.DB2HTML(Eval("userName").ToString())%><br />
            - 등록일 : <%#Eval("createDate", "{0:yyyy-MM-dd}")%></td>
        </tr>
        <tr>
            <td class="player_count" align="left">- 조회수 : <%#Eval("readCount")%></td>
        </tr>
        </table>
        
    </ItemTemplate>
    </asp:DataList> 
    <asp:Panel ID="pnlMsg" runat="server">
        <table style="width:100%;height:200px;margin-top:10px" class="boardInputType">
            <tr>
                <td style="text-align:center">
                조회된 데이터가 없습니다.
                </td>
            </tr>
        </table>
    </asp:Panel>   
    <div id="pagenum2" style="margin-top:15px">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>     
       	
</asp:Content>

