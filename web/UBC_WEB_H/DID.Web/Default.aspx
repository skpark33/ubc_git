﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" Title="HYUNDAI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
<title>:::  HYUNDAI :::</title>
</head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="./Asset/Style/login_layout.css" rel="stylesheet" type="text/css" />
<link href="./Asset/Style/login_menu_style.css" rel="stylesheet" type="text/css" />
<body >
<div class="wrap">
  <!-- fff -->
  <div id="Header">
    <div id="TopMenuBox"> <img src="./Asset/Images/download/hundai_txt.png" style="padding-left:550px; padding-top:20px;"/>
      <div class="logo"><img src="./Asset/Images/logo_hyundai.gif" /></div>      
    </div>
  </div>


<!-- contents -->
<div class="login_body"> 
<p class="login_title"><img src="./Asset/Images/login/title.png" /></p>
<form id="form1" runat="server">
      <!-- 로그인 영역 -->
      <div class="login_input">
        <p class="line_bottom pad_B10"><img src="./Asset/Images/login/txt_login.gif" /></p>
        
        <div  class="pad_T20 pad_B30 align_C">
        
        
        <table width="85%" border="0" cellspacing="0" cellpadding="0">
          <caption>
          </caption>
          <colgroup>
          <col width="62">
          <col width="">
          <col width="100">
          </colgroup>
          <tbody>
            <tr>
              <td height="32"><img src="./Asset/Images/login/id.gif" /></td>
              <td><asp:TextBox ID="txtUserID" runat="server" Width=150    TabIndex="1"  Text=""   CssClass="i_text" /></td>
              <td rowspan="2">     
                <asp:ImageButton runat="server" ID="btnLogin"   ImageUrl="./Asset/Images/login/bt_login.gif" onclick="btnLogin_Click"   >
                </asp:ImageButton>
              </td>
            </tr>
            <tr>
              <td height="32"><img src="./Asset/Images/login/pwd.gif" /></td>
              <td><asp:TextBox  ID="txtPwd" runat="server"  Width=150 TabIndex="2"   TextMode="Password" Text="" CssClass="i_text" />
              </td>
            </tr>
          </tbody>
        </table>
        
        </div>
        <div class="pad_T20 line_top">
            <a href="findPassword.aspx"><span class="float_R" >비밀번호 찾기</span> </a>
        </div>
      </div>
      <!-- //로그인 영역 -->

</div>
<!-- //contents -->
<!--footer-->
<%--<div id="footer">
  <div class="body"> <img src="./Asset/Images/footer_img.gif" /> </div>
</div>--%>
</div>
    </form>
</div>
</body>
</html>
