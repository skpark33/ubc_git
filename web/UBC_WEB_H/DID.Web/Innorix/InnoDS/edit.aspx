<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<head>
<title>Innorix Multi Platform Solution - InnoDS</title>
<script type="text/javascript">
	// 컴포넌트 로드 완료시 자동 호출되는 콜백 함수
	function OnLoadComplete(objName)
	{
		SetInnoDSInit(objName);
	}

	function SetInnoDSInit(objName)
	{
		removeAllFlag = false;
		
		// 파일 삭제정보 전역배열
		delArray = new Array(); 
		
		// 선택된 모든 파일제거
		InnoDS.RemoveAllItems();

		// 수정모드 가상파일 추가
		InnoDS.AddVirtualFile("a.zip", 2345678, "a.zip");
		InnoDS.AddVirtualFile("b.txt", 3456789, "b.txt");
		InnoDS.AddVirtualFile("c.doc", 1567890, "c.doc");
		InnoDS.AddVirtualFile("d.wma", 4567890, "d.wma");
	}
	
	function upload()
	{
		// 삭제된 파일 정보를 폼에 추가
		for (var i in delArray)
		{
			var oForm = document.f_write;
			var oInput = document.createElement('input');
			oInput.type = 'hidden';
			oInput.name = '_innods_deleted_file';
			oInput.value = delArray[i];
			oForm.insertBefore(oInput , oForm.firstChild);
		}

		// 존재하는 파일 정보를 폼에 추가		
		for (var i = 0; i < InnoDS.GetFileCount(); i++)
		{
			if (InnoDS.IsTempFile(i))
			{
				var oForm = document.f_write;	
				var oInput = document.createElement('input');			
				oInput.type = 'hidden';
				oInput.name = '_innods_exist_file';
				oInput.value = InnoDS.GetFileID(i);
				oForm.insertBefore(oInput, oForm.firstChild);
			}
		}				

		var addFileCount = 0;

		// 가상 파일을 제외한 실제 첨부파일 개수
		for (var i = 0; i < InnoDS.GetFileCount(); i++)
		{
			if (!InnoDS.IsTempFile(i))
			{
				addFileCount++;
			}
		}		
		
		// 첨부 파일이 있으면 컴포넌트로 파일전송
		if (addFileCount > 0)
		{
			// 서버에 저장될 서브 디렉토리 지정
			InnoDS.AddPostData("_SUB_DIR", SubDir);
			
			// 업로드 시작
			InnoDS.StartUpload();
		}
		else // 첨부 파일이 없으면 폼값과 삭제된 파일정보 전송
		{
			InnoDSSubmit(document.f_write);
		}
	}

	// 파일이 제거되기 전 각각의 아이템 별로 발생하는 이벤트 입니다.
	function OnBeforeRemoveItem(objName, index)
	{
		// 가상파일 삭제시 파일ID를 삭제정보 배열에 추가
		if (InnoDS.IsTempFile(index))
		{
			delArray.push(InnoDS.GetFileID(index));
		}
	}

	// 사용자에 의해 파일이 제거되기전 발생하는 이벤트 입니다.
	function OnRemoveItems(objName)
	{
		if (removeAllFlag)
		{
			for (var i = 0; i < InnoDS.GetFileCount(); i++)
			{
				if (InnoDS.IsTempFile(i))
				{
					delArray.push(InnoDS.GetFileID(i));
				}
			}
		}
		removeAllFlag = true;
	}	
	
	// OnUploadComplete는 파일 업로드 완료 후 발생하는 이벤트 입니다.
	// 파일 업로드가 완료되면 f_write 폼 데이타와 업로드된 파일 정보를 함께 전송 합니다.
	function OnUploadComplete(objName)
	{
		InnoDSSubmit(document.f_write);
	}
</script>
</head>
<body>

<form action="edit_action.aspx" name="f_write" method="post" enctype="multipart/form-data">
test1 : <input type="text" name="test1" />
</form><br />

<div style="border: 1px solid #C0C0C0; width:500px;">
<script type="text/javascript" src="InnoDS.js"></script>
<script type="text/javascript">
	var Enc = "";

	var InputType = "fixed";
	var UploadURL = "action_ds.aspx";
	var SubDir = "2010";	

	InnoDSInit( -1 , -1 , -1 , 500 , 200 );
</script>
</div>

<br />
<input type="button" value="파일추가" onClick="InnoDS.OpenFileDialog();" />
<input type="button" value="첨부파일 초기화" onClick="SetInnoDSInit();" />
<br /><br />
<input type="button" value="전송하기" onClick="upload();" />

</body>
</html>