<%
Dim ROOT_DIR
Dim filenameArr
Dim folderArr
Dim filesizeArr
Dim i

%>

<html>
<head>
<title>InnoDS</title>
</head>

<body>
<h1>전송완료</h1>
<h5>test1 : <strong><%=Request("test1")%></strong></h5>
<h5>서브폴더 : <strong><%=Request("_SUB_DIR")%></strong></h5>
<hr / >
&nbsp;<strong>전송결과</strong> - <a href="folder.aspx">다른 파일 전송하기</a>
<hr / >

<table width=490 border=0 cellspacing=0 cellpadding=0>
<tr>
  <td bgcolor=#CCCCCC>
    <table width=100% border=0 cellspacing=1 cellpadding=0>
    <tr>
      <td bgcolor=#EBE8D6 align=center height=25>파일명 (저장기준)</td>
      <td width=70 bgcolor=#EBE8D6 align=center>폴더</td>
      <td width=70 bgcolor=#EBE8D6 align=center>용량</td>
    </tr>
<%
filenameArr = Split(Request("_innods_filename"), ",")
folderArr = Split(Request("_innods_folder"), ",")
filesizeArr = Split(Request("_innods_filesize"), ",")

For i = 0 To filenameArr.Length-1
%>
    <tr>
      <td bgcolor=#FFFFFF align=left height=22>&nbsp;<%=filenameArr(i)%></td>
      <td bgcolor=#FFFFFF align=left>&nbsp;<%=folderArr(i)%></td>
      <td bgcolor=#FFFFFF align=left>&nbsp;<%=filesizeArr(i)%></td>
    </tr>
<%
Next
%>
    </table>
  </td>
</tr>
</table>

</body>

</html>