﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="main.aspx.cs" Inherits="main" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> ::: HYUNDAI ::: </title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="/Asset/Style/layout.css" rel="stylesheet" type="text/css" />
    <link href="/Asset/Style/menu_style.css" rel="stylesheet" type="text/css" />
     
    <script src="/Asset/Script/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="/Asset/Script/jquery.cookie.js" type="text/javascript"></script>
    <script src="/Asset/Script/jquery.treeview.js" type="text/javascript"></script>
    <script src="/Asset/Script/json2.js" type="text/javascript"></script>
    <script src="/Asset/Script/common.js" type="text/javascript"></script>
    <script src="/Asset/Script/tabs.js" type="text/javascript"></script>    
    <script src="/Asset/Script/wcfService.js" type="text/javascript"></script>       
</head>    


<body >
<form id="mainFrom" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    <Services>
        <asp:ServiceReference Path="~/WebService/wsLeftMenu.svc" />
    </Services>
</asp:ScriptManager>
<div class="wrap">
<!--  추가 -->
<div id="Header">
    <div id="TopMenuBox" runat="server" >
        <div class="logo"><img   src="/Asset/Images/logo_hyundai.gif" alt="로고" /></div>
        <div class="menu">
            <ul class="topMenuTab" style="width:1000px">
                <asp:Literal ID="lblTopMenu" runat="server" />        
             </ul>
        </div>
        <ul class="topMenu"  >
			<li><img src="./Asset/Images/agronet/ico_mem.gif" alt=""> <asp:Label ID="userName" runat="server" Text=""></asp:Label>&nbsp;| <asp:Label ID="userType" runat="server"  ></asp:Label></li>
		</ul>
		<div class="logout">
			<a href="logout.aspx"><img src="./Asset/Images/agronet/btn_logout.gif" alt="로그아웃"></a>
		</div> 
    </div>
    
 </div>
<!--  추가 끝 -->
 
    <div class="container" >
        <div class="leftmenu">
           
            <ul></ul>         
        </div>
        <div class="menubutton">&nbsp;</div>
        <div class="maincontent" >
           <div class="sub_tabs">
               <div id="TabBox">
                   <ul></ul>
               </div>
               <div class="command_box">
                </div>
           </div> 
           
           
           <div class="frame_box"      >
             
            <iframe id="frame0" class="DivHide" height="30" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src=""></iframe>
            <iframe id="frame1" class="DivHide" height="30" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src=""></iframe>
            <iframe id="frame2" class="DivHide" height="30" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src=""></iframe>
            <iframe id="frame3" class="DivHide" height="30" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src=""></iframe>
            <iframe id="frame4" class="DivHide" height="30" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src=""></iframe>
            <iframe id="frame5" class="DivHide" height="30" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src=""></iframe>
            <iframe id="frame6" class="DivHide" height="30" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto" src=""></iframe>
           </div>       
        </div>
    </div>    
    <div class="footer"></div>
    <input type="hidden" id="hdnFrameCount" value="0" />
    <input type="hidden" id="hdnMenuCode" value="0" />   <!-- 첫로딩시 왼쪽트리에 뿌려줄 대메뉴의 라지코드  -->
    <input type="hidden" id="hdnActiveId" />
</div>
 
<script type="text/javascript">
    
    // 페이지 로딩시
    $(document).ready(function() {

        $('.menu li').each(function() {
            var _class = $(this).attr("title");
            $(this).attr("title", "").data("class", _class);
        });

        
        // 왼쪽 메뉴 가져오기
        Wcf_LeftMenu("<%=this.Profile.COMP_TYPE%>", $('#hdnMenuCode').val(), "<%=this.Profile.LANG_FLAG%>");

        // 상단 메뉴
        $('.menu li').click(function() {

            Wcf_LeftMenu("<%=this.Profile.COMP_TYPE%>", $(this).attr("id"), "<%=this.Profile.LANG_FLAG%>");
            $('.menu li').removeClass('on');
            $(this).addClass('on');

            $('#TopMenuBox').removeClass().addClass($(this).data("class"));

            $.cookie('ShowMenu', 'show');
            Common_LeftMenu_A();
            $('#hdnMenuCode').val($(this).attr("id"));

        });

        // Default 페이지 Sub Tab 생성  COMP_TYPE
        Tabs_Onload('<%=this.Profile.COMP_TYPE%>'); 

        // 탭 삭제
        $('#tab_delete').click(function() { Tabs_Del(); });

        // 탭 새로고침
        $('#refresh').click(function() { Tabs_Refresh(); });
    });
</script>

 
</form>
</body>
</html>