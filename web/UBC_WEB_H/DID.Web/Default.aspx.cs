﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using DID.Common.Framework;

public partial class _Default : System.Web.UI.Page 
{
    public string m_Domain;
    private bool IsSecureConnection; 

    protected void Page_Load(object sender, EventArgs e)
    {
        m_Domain = Request.Url.Host;
        //보안 통신이 아닌 경우 https로 redirection.
        IsSecureConnection = Request.IsSecureConnection;
        if (!Request.IsSecureConnection && m_Domain != "58.87.34.248")
        {
            string uri = "https://" + m_Domain + Request.RawUrl;
            this.Response.Redirect(uri);
            this.Response.End();


        }
         
        // 솔루션에서 들어왔을때에의 처리 : 메뉴를 제외한 단일 프로그램이 떠야 한다.
        // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", Request.QueryString["SITE"].ToString()), true);

        //GET 으로 들어오는 인자의 갯수
        string METHOD_GET_COUNT = Request.QueryString.Keys.Count.ToString();
         
        if (METHOD_GET_COUNT == "0")
        {
            // 인자가 없으면 웹에서 로그인한것
        }
        else
        {
            // 인자가 있으면 솔루션에서 로그인한 것
            // http://localhost/default.aspx?GUBUN=UBC&SITE=HYUNDAI&ID=HYUNDAImanager&PW=HYUNDAImanager&PROGRAM=community/notice_list.aspx


            for (int i = 0; i < Request.QueryString.Keys.Count; i++)
            {
                if (Request.QueryString.Keys[i].ToString() == "GUBUN")
                {
                    if (urlDecode(Request.QueryString["GUBUN"].ToString()) == "UBC")
                    {
                        string program = urlDecode(Request.QueryString["PROGRAM"].ToString());
            string url = "http://" + m_Domain;
            if (program != null && program.Length > 0)
            {
                if (program.ToLower().StartsWith("http://") || program.ToLower().StartsWith("https://"))    //?붿껌 URL??http ?뱀? https媛 ?ы븿??寃쎌슦 蹂寃??놁쓬.
                {
                    url = program;
                }
                else if (program.StartsWith("/"))
                {
                    url += program;
                }
                else
                {
                    url += "/" + program;
                }
            }
            else
            {
                url += "/main.aspx";
            }
/*
			string url = "http://" + m_Domain + "/main.aspx";
                        //일반통신으로 변경
                        //string url = "http://" + m_Domain + "/main.aspx";
			string url = "http://" + m_Domain;
                        if (program.ToLower().StartsWith("http://"))    //요청 URL에 http 혹은 https가 포함된 경우 변경 없음.
                        {
                            url = program;
                        }
                        else if (program.StartsWith("/"))
                        {
                            url += program;
                        }
                        else
                        {
                            url += "/" + program;
                        }
*/
                        userLogin(
                              urlDecode(Request.QueryString["SITE"].ToString())
                            , urlDecode(Request.QueryString["ID"].ToString())
                            , urlDecode(Request.QueryString["PW"].ToString())
                            , url
                            , "S"
                            );

                    }
                }
            } 
        }
         
    


        if (!this.IsPostBack){ txtUserID.Focus(); }
        //Response.Write(User.Identity.IsAuthenticated);
         
    }


    private string urlDecode(string urlEncode)
    {
        //치환대상문자  치환문자
        //    =           .
        //    +           *
        //    /           ?

        string rtStr="";
       
        rtStr = urlEncode.Replace('.', '=');
        rtStr = rtStr.Replace('*', '+');
        rtStr = rtStr.Replace('?', '/');

        // rtStr 복호화
        rtStr = System.Text.Encoding.GetEncoding("euc-kr").GetString(System.Convert.FromBase64String(rtStr));
         
        return rtStr; 
    }


     

    protected void btnLogin_Click(object sender, EventArgs e)
    {
 
        string  idDecode = System.Text.Encoding.GetEncoding("UTF-8").GetString(Convert.FromBase64String(   txtUserID.Text   ));
        string  pwDecode = System.Text.Encoding.GetEncoding("UTF-8").GetString(Convert.FromBase64String(   txtPwd.Text      ));

        //일반 통신으로 변경
        string url = "http://" + m_Domain + "/main.aspx";
        userLogin(System.Configuration.ConfigurationManager.AppSettings["ROOT_SITE_ID"], idDecode, pwDecode, url, "W");
    
    }


    public void userLogin(string usersite, string userid, string userpassword, string program, string entryPoint)
    {

        DID.Service.Utility.FormAuthenticateUser objAuthUser = new DID.Service.Utility.FormAuthenticateUser();
        objAuthUser.UserID = userid;
        objAuthUser.UserPwd = userpassword;
        objAuthUser.CompType = "H";
        objAuthUser.LangFlag = "KR";
        objAuthUser.MgrId = "1";
        objAuthUser.SiteId = usersite;

        DataTable dt = objAuthUser.CheckUserValidation();

        if (dt.Rows.Count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "fail", "loginFailAction();", true);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", "alert('아이디 혹은 비밀번호가 일치하지 않습니다.');", true);
        }
        else
        {
             
            dt = objAuthUser.GetUserInfo();

            //프로파일을 설정하는 로직
            Profile.USER_ID = userid;
            Profile.USER_NAME = dt.ToDataRow()["userName"].ToString();
            Profile.SITE_ID = dt.ToDataRow()["siteId"].ToString();
            //Profile.DEPT_CODE = dt.ToDataRow()[""].ToString()
            //Profile.DEPT_NAME = dt.ToDataRow()[""].ToString()
            Profile.LANG_FLAG = "KR";
            Profile.COMP_TYPE = "H" ; // H, K

            //USER_TYPE : 1-super admin    2-site admin  3-user
            //3  일때에가 일반 유저이다
            Profile.USER_TYPE = dt.ToDataRow()["userType"].ToString();
            
            
            // 웹에 진입하는 부분 웹(W)인지 솔루션(S)인지
            Profile.ENTRY_POINT = entryPoint;

            //if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
            //{
            //    objAuthUser.SetUserInfoContext("LOGIN_USER_ID", urlDecode(Request.QueryString["ID"].ToString()));
            //    objAuthUser.SetUserInfoContext("LOGIN_USER_PWD", urlDecode(Request.QueryString["PW"].ToString()));
            //}

            objAuthUser.CreateAuthenticationTicket();
            this.Response.Redirect(program, false);
        }
    
    }




}
