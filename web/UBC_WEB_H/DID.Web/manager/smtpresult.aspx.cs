﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;
using System.Net.Mail;

public partial class manager_smtpresult : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {

            string returnMsg = "";
            string success_yn = "";
            
            if (sendEmail("127.0.0.1", Request["send_email"], Request["receive_mail"], Request["title"], Request["contents"], false, out returnMsg))
            {                
                success_yn = "Y";
            }
            else
            {
                success_yn = "N";
            }

            Response.Write(returnMsg + "<br />");
            
            using (DID.Service.Manager.Smtp obj = new DID.Service.Manager.Smtp())
            {
                if( success_yn == "Y" )
                {
                    obj.smtpLogInsert(Request["company"]
                                    , Request["send_email"]
                                    , Request["receive_mail"]
                                    , Request["title"]
                                    , Request["contents"]
                                    , success_yn);
                }
                else
                {
                    obj.smtpLogInsert(Request["company"]
                                    , Request["send_email"]
                                    , Request["receive_mail"]
                                    ,"Error"
                                    , returnMsg
                                    , success_yn);
                }
            }
        }
    }

    public bool sendEmail(string server
                            , string from
                            , string to
                            , string Subject
                            , string Body
                            , bool isBodyHTML
                            , out string resultMsg
                         )
    {
        resultMsg = "";

        if (from.IsNullOrEmpty())
        {
            resultMsg = "Sender email address is not exist";
            return false;
        }
        if (to.IsNullOrEmpty())
        {
            resultMsg = "Recipient email address is not exist";
            return false;
        }

        Subject = (Subject.IsNullOrEmpty()) ? "" : Subject;
        Body = (Body.IsNullOrEmpty()) ? "" : Body;


        MailMessage message = new MailMessage();

        // 메일 제목
        message.Subject = Subject;

        // 메세지 BODY 부분처리(HTML or not)
        if (isBodyHTML == true)
        {
            message.IsBodyHtml = true;
            message.Body = Body.Replace("\r\n", "<br />");
        }
        else
        {
            message.IsBodyHtml = false;
            message.Body = Body;
        }

        // 인코딩
        message.SubjectEncoding = System.Text.Encoding.UTF8;
        message.BodyEncoding = System.Text.Encoding.UTF8;

        // 보내는주소
        string[] toAddresses = System.Text.RegularExpressions.Regex.Split(to, "[,;] *");
        foreach (string toAddress in toAddresses)
        {
            message.To.Add(new MailAddress(toAddress));
        }

        // 받는주소
        message.From = new MailAddress(from);

        // 서버 주소 셋팅
        SmtpClient client = new SmtpClient(server.IsNullOrEmpty() ? "127.0.0.1" : server);

        try
        {
            client.Send(message);
            resultMsg = "Email has sent successfully!! ";

            return true;
        }
        catch (Exception ex)
        {
            resultMsg = String.Format("Email sending has failed: {0} - {1}", ex.ToString(), ex.Message);

            return false;
        }

    }
}
