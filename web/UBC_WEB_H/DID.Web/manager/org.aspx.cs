﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using DID.Common.Framework;

public partial class manager_org : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.InitControl();
            this.DataBindControl();

            // 비즈니스타입 콤보셋팅
            initBusinessTypeCombo();
            ddlBusinessType.SelectedIndex = 0;
        }
    }

    #region Utitity

    private void SetTextBoxMode(bool isEditabled, params TextBox[] objs)
    {
        for (int i = 0; i < objs.Length; i++)
        {
            if (isEditabled)
            {
                objs[i].ReadOnly = false;
                objs[i].BorderStyle = BorderStyle.Solid;
                objs[i].BorderColor = System.Drawing.Color.FromArgb(204,204,204);
                objs[i].BorderWidth = 1;
                objs[i].Attributes.Add("onfocus", "this.select();");
            }
            else
            {
                objs[i].ReadOnly = true;
                objs[i].BorderStyle = BorderStyle.None;
                objs[i].BorderWidth = 0;
            }
        }
    }
    
    #endregion

    #region 컨트롤 초기화
   
    private void InitControl()
    {   
        // 조직콤보 - 조회용
        Common.BindDropDownList(ddlType, GetOrgCode(ROOT_SITE_ID), "전체");
        ddlType_SelectedIndexChanged(null, null); 


        // 조직콤보 - 등록용
        Common.BindDropDownList(ddlTypeReg, GetOrgCode(ROOT_SITE_ID), "루트");
        ddlTypeReg_SelectedIndexChanged(null, null);        

    }

    // 비즈니스타입 콤보셋팅
    protected void initBusinessTypeCombo()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("code", typeof(String)));
        dt.Columns.Add(new DataColumn("name", typeof(String)));

        dt.Rows.Add(CreateRow("", "선택", dt));
        dt.Rows.Add(CreateRow("지역본부", "지역본부", dt));
        dt.Rows.Add(CreateRow("서비스센터", "서비스센터", dt));
        dt.Rows.Add(CreateRow("출고센터", "출고센터", dt));
        dt.Rows.Add(CreateRow("공장", "공장", dt)); // 2012.02.22 gwangsoo
        dt.Rows.Add(CreateRow("지점", "지점", dt));
        dt.Rows.Add(CreateRow("대리점", "대리점", dt));
        dt.Rows.Add(CreateRow("기타", "기타", dt));

        ddlBusinessType.Items.Clear();
        ddlBusinessType.DataSource = dt;
        ddlBusinessType.DataBind();
    }

    DataRow CreateRow(String Text, String Value, DataTable dt)
    {
        DataRow dr = dt.NewRow();
        dr[0] = Text;
        dr[1] = Value;

        return dr;
    }


    #endregion

    #region 조회용 조직콤보 이벤트 핸들러
    
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass1.Visible = false;
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;

        ddlClass1.Items.Clear();
        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();

        if (!ddlType.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlType.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass1.Visible = true;
                Common.BindDropDownList(ddlClass1, dt, "전체");
            }
        }

        // 조회에서 사용될 선택될 조직코드 셋팅
        hiddenSelectedSiteId.Value = ddlType.SelectedValue;

        this.DataBindControl();

    }

    protected void ddlClass1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;

        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();

        if (!ddlClass1.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass1.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass2.Visible = true;
                Common.BindDropDownList(ddlClass2, dt, "전체");
            }

            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteId.Value = ddlClass1.SelectedValue;
        }
        else
        {
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteId.Value = ddlType.SelectedValue;
        }

        this.DataBindControl();
    }

    protected void ddlClass2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass3.Visible = false;

        ddlClass3.Items.Clear();

        if (!ddlClass2.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass2.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass3.Visible = true;
                Common.BindDropDownList(ddlClass3, dt, "전체");
            }

            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteId.Value = ddlClass2.SelectedValue;
        }
        else
        {
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteId.Value = ddlClass1.SelectedValue;
        }

        this.DataBindControl();

    }

    protected void ddlClass3_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 조회에서 사용될 선택될 조직코드 셋팅
        if (!ddlClass3.SelectedValue.IsNullOrEmpty())
        {
            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteId.Value = ddlClass3.SelectedValue;
        }
        else
        {
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteId.Value = ddlClass2.SelectedValue;

        }

        this.DataBindControl();
    }
    
    #endregion

    #region 등록용 조직콤보 이벤트 핸들러

    protected void ddlTypeReg_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClassReg1.Visible = false;
        ddlClassReg2.Visible = false;
        ddlClassReg3.Visible = false;

        ddlClassReg1.Items.Clear();
        ddlClassReg2.Items.Clear();
        ddlClassReg3.Items.Clear();

        if (!ddlTypeReg.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlTypeReg.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClassReg1.Visible = true;
                Common.BindDropDownList(ddlClassReg1, dt, "선택");

            }
        }

        // 등록시 사용될 선택될 조직코드 셋팅
        hiddenSelectedSiteIdReg.Value = ddlTypeReg.SelectedValue;

        
    }

    protected void ddlClassReg1_SelectedIndexChanged(object sender, EventArgs e)
    {

        ddlClassReg2.Visible = false;
        ddlClassReg3.Visible = false;

        ddlClassReg2.Items.Clear();
        ddlClassReg3.Items.Clear();

        if (!ddlClassReg1.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClassReg1.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClassReg2.Visible = true;
                Common.BindDropDownList(ddlClassReg2, dt, "선택");

            }

            // 등록시 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteIdReg.Value = ddlClassReg1.SelectedValue;
        }
        else
        {
            // 등록시 사용될 선택될 조직코드 셋팅
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteIdReg.Value = ddlTypeReg.SelectedValue;
            
        }


    }

    protected void ddlClassReg2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClassReg3.Visible = false;

        ddlClassReg3.Items.Clear();

        if (!ddlClassReg2.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClassReg2.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClassReg3.Visible = true;
                Common.BindDropDownList(ddlClassReg3, dt, "선택");
    
            }

            // 등록시 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteIdReg.Value = ddlClassReg2.SelectedValue;

            
        }
        else
        {
            // 등록시 사용될 선택될 조직코드 셋팅
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteIdReg.Value = ddlClassReg1.SelectedValue;
            
        }
               

    }

    protected void ddlClassReg3_SelectedIndexChanged(object sender, EventArgs e)
    {        
        // 등록시 사용될 선택될 조직코드 셋팅
        if (!ddlClassReg3.SelectedValue.IsNullOrEmpty())
        {
            // 등록시 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteIdReg.Value = ddlClassReg3.SelectedValue;
 
        }
        else
        {
            // 등록시 사용될 선택될 조직코드 셋팅
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteIdReg.Value = ddlClassReg2.SelectedValue;
            
            
        }
               

    }

    #endregion

    #region CheckedChanged 이벤트 핸들러
    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkTest = (CheckBox)sender;
        GridViewRow grdRow = (GridViewRow)chkTest.NamingContainer;

        TextBox txtname = grdRow.FindControl("txtSiteId") as TextBox;
        txtname.ReadOnly = true;
        TextBox txtlocation = grdRow.FindControl("txtSiteName") as TextBox;
        TextBox txtPhoneNo = grdRow.FindControl("txtPhoneNo") as TextBox;
        TextBox txtShopOpenTime = grdRow.FindControl("txtShopOpenTime") as TextBox;
        TextBox txtShopCloseTime = grdRow.FindControl("txtShopCloseTime") as TextBox;
        TextBox txtHoliday = grdRow.FindControl("txtHoliday") as TextBox;
        TextBox txtComment1 = grdRow.FindControl("txtComment1") as TextBox;
        TextBox txtComment2 = grdRow.FindControl("txtComment2") as TextBox;

        SetTextBoxMode(chkTest.Checked, txtlocation, txtPhoneNo, txtShopOpenTime, txtShopCloseTime, txtHoliday, txtComment1, txtComment2);
    }
    #endregion

    #region 그리드뷰 gvList_RowDataBound()
    /// <summary>
    /// 그리드뷰 gvList_RowDataBound
    /// </summary>
    protected void gvList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        DataRowView drv = (DataRowView)e.Row.DataItem;


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }
    }
    #endregion

    #region 버튼 클릭 이벤트 핸들러 

    // 신규버튼
    protected void btnInit_Click(object sender, EventArgs e)
    {
        txtSiteId.Text = string.Empty;
        txtSiteName.Text = string.Empty;
        txtPhoneNo.Text = string.Empty;
        txtShopOpenTime.Text = string.Empty;
        txtShopCloseTime.Text = string.Empty;
        txtHoliday.Text = string.Empty;
        txtComment1.Text = string.Empty;
        txtComment2.Text = string.Empty;

        // 비즈니스타입 콤보
        ddlBusinessType.SelectedIndex = 0;

        this.InitControl();
        
    }

    // 등록버튼
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
             
            if (obj.GetOrgInfo(txtSiteId.Text, ROOT_SITE_ID).ToDataRow() != null)
            {

                this.ScriptExecute("이미 등록된 조직코드 입니다.");
                return;
            }

            obj.Insert("1"
                       , txtSiteId.Text
                       , txtSiteName.Text
                       , txtPhoneNo.Text
                       , ddlBusinessType.SelectedValue
                       , txtSiteId.Text
                       , txtShopOpenTime.Text
                       , txtShopCloseTime.Text
                       , txtHoliday.Text
                       , txtComment1.Text
                       , txtComment2.Text
                       , (hiddenSelectedSiteIdReg.Value.IsNullOrEmpty()) ? ROOT_SITE_ID : hiddenSelectedSiteIdReg.Value
                       , ROOT_SITE_ID
                       );
        }

        this.ScriptExecute("등록 되었습니다.");        
        this.InitControl();
        this.DataBindControl();

    }

    // 수정버튼
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            foreach (GridViewRow row in gvList.Rows)
            {
                if (((CheckBox)row.Cells[0].FindControl("chkSelect")).Checked)
                {
                    
                    obj.Update(((HiddenField)row.Cells[1].FindControl("hdnMgrId")).Value
                               , ((TextBox)row.Cells[1].FindControl("txtSiteId")).Text
                               , ((TextBox)row.Cells[1].FindControl("txtSiteName")).Text
                               , ((TextBox)row.Cells[1].FindControl("txtPhoneNo")).Text
                               //, ddlType.SelectedValue
                               , ((TextBox)row.Cells[1].FindControl("txtBusinessType")).Text
                               , ((TextBox)row.Cells[1].FindControl("txtSiteId")).Text
                               , ((TextBox)row.Cells[1].FindControl("txtShopOpenTime")).Text
                               , ((TextBox)row.Cells[1].FindControl("txtShopCloseTime")).Text
                               , ((TextBox)row.Cells[1].FindControl("txtHoliday")).Text
                               , ((TextBox)row.Cells[1].FindControl("txtComment1")).Text
                               , ((TextBox)row.Cells[1].FindControl("txtComment2")).Text
                               , ROOT_SITE_ID
                               );

                }
            }
        }

        this.ScriptExecute("수정 되었습니다.");
        this.DataBindControl();

    }

    // 삭제버튼
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        int totalChecked = 0;
        int totalDeleted = 0;

        ArrayList listUndeleted = new ArrayList();

        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            foreach (GridViewRow row in gvList.Rows)
            {
                if (((CheckBox)row.Cells[0].FindControl("chkSelect")).Checked)
                {                   
                    totalChecked++;

                    string siteId = ((TextBox)row.Cells[1].FindControl("txtSiteId")).Text;

                    // 하위 사이트가 있으면 삭제 안함
                    if (hasChildSite(siteId) == true)
                    {
                        listUndeleted.Add(siteId);
                        continue;
                    }
                    
                    // DB 데이타 삭제
                    obj.Delete(((HiddenField)row.Cells[1].FindControl("hdnMgrId")).Value, ((TextBox)row.Cells[1].FindControl("txtSiteId")).Text, ROOT_SITE_ID);

                    totalDeleted++;
                }
            }
        }
        string resultMessage;

        if (totalChecked != totalDeleted)
        {
            resultMessage = String.Format("선택하신 {0} 건중 총 {1} 건 삭제되었습니다.\\n", totalChecked, totalDeleted);
            resultMessage += "선택한 조직이 하위 조직을 가지고 있으면 삭제되지 않습니다.\\n";
            resultMessage += "하위 조직을 먼저 삭제 하신 후 삭제하셔야 합니다.\\n\\n";
            resultMessage += "[삭제되지 않은 조직코드 목록]\\n";

            foreach (String undeletedSiteID in listUndeleted)
            {
                resultMessage += undeletedSiteID + ",";
            }

            resultMessage = resultMessage.TrimEnd(',');
        }
        else
        {
            resultMessage = String.Format("선택하신 총 {0} 건 삭제 되었습니다.\\n", totalDeleted);
        }

        this.ScriptExecute(resultMessage);
        this.InitControl();
        this.DataBindControl();
    }

    // 하위 조직이 있는지 여부 검사
    protected bool hasChildSite(string siteId)
    {
        
        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {

            DataTable dt = obj.GeChildSiteListByParentID(siteId, ROOT_SITE_ID);

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    #endregion

    #region 컨트롤 데이터 바인딩
    private void DataBindControl()
    {
        string siteId = (hiddenSelectedSiteId.Value.IsNullOrEmpty()) ? ROOT_SITE_ID : hiddenSelectedSiteId.Value;

        DataTable dt = this.GetOrgList(siteId, "");
        this.gvList.DataSource = dt;
        this.gvList.DataBind();
    }
    #endregion

    #region 데이터 조회
    private DataTable GetOrgList(string siteId, string businessType)
    {
        DataTable dt = null;

        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            dt = obj.GetOrgList(siteId, businessType, ROOT_SITE_ID);
        }

        return dt;
    }

    private DataTable GetOrgCode(string parentId)
    {
        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            return obj.GetOrgCode(parentId, ROOT_SITE_ID);
        }
    }


    #endregion
}
