﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="org.aspx.cs" Inherits="manager_org"  %>

<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoInit() {
        
    }
    
    function DoReg() {

        
        
        if (Trim($("#<%= txtSiteName.ClientID%>").val()) == "") {
            alert("명칭을 입력하십시요.");
            $("#<%= txtSiteName.ClientID%>").focus();
            return false;
        }

        if (Trim($("#<%= txtSiteId.ClientID%>").val()) == "") {
            alert("코드를 입력하십시요.");
            $("#<%= txtSiteId.ClientID%>").focus();
            return false;
        }

        if ($("#<%=ddlBusinessType.ClientID %> option:selected").val() == "") {
            alert("조직종류를 입력하십시요.");
            $("#<%= ddlBusinessType.ClientID%>").focus();
            return false;
        } 
       
        return true;
    }

    function DoUpdate()
    {
        if ($("input:checkbox[name*=chkSelect]:checked").length == 0) {
            alert("수정할 항목이 없습니다.");
            return false;
        }

        return true;
    }

    function DoDelete() {

        if ($("input:checkbox[name*=chkSelect]:checked").length == 0) {
            alert("삭제할 항목이 없습니다.");
            return false;
        }
        
        if(!confirm("조직 삭제 시 해당조직과 관련된 데이터들은 전부 삭제 됩니다.\n 정말로 삭제 하시겠습니까?   "))
            return false;

        return true;
    }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnBusinessType" runat="server"  />
    <asp:HiddenField ID="hdnSearchSiteId" runat="server"  />

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><span><img src="/Asset/Images/body/bullet01.gif" /> 조직관리</span></span>
            </td>
        </tr>
    </table>

    <div class='topOptionBtns'>
        <asp:ImageButton ID="btnUpdate" runat="server" ToolTip="수정" OnClick="btnUpdate_Click" OnClientClick='return DoUpdate()' ImageUrl="~/Asset/Images/button/btn_update.jpg"  />
        <asp:ImageButton ID="btnDelete"  runat="server" ToolTip="삭제" OnClick="btnDelete_Click" OnClientClick='return DoDelete()' ImageUrl="~/Asset/Images/button/btn_delete.jpg"  />        
    </div>    

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pad_B10">
    <tr>
    <td width="7"><img src="/Asset/Images/body/box_left.gif" /></td>
    <td style="background:url(/Asset/Images/body/box_bg.gif) repeat-x">
        <div class="pad_L10">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:DropDownList ID="ddlType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass1" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass1_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass2" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass2_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass3" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass3_SelectedIndexChanged" />
                
                <asp:HiddenField ID="hiddenSelectedSiteId" runat="server" />
                    
            </ContentTemplate>
            
            </asp:UpdatePanel>
        </div>
    </td>
    <td width="8"><img src="/Asset/Images/body/box_right.gif" /></td>
    </tr>
    </table>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnUpdate" />
        <asp:AsyncPostBackTrigger ControlID="btnDelete" />
    </Triggers>
    <ContentTemplate>
    
        <table class="boardListType" style="width:930px;border-collapse:collapse;">
        <colgroup>
            <col width="5%" /> 
            <col width="10%" /> 
            <col width="10%" /> 
            <col width="10%" /> 
            <col width="5%" /> 
            <col width="5%" /> 
            <col width="5%" /> 
           <%--
            <col width="4%" /> 
            <col width="4%" />
            --%>
        </colgroup>
        <tr>
            <th>선택</th>
            <th>코드</th>
            <th>명칭</th>
            <th>연락처</th>
            <th>개점시간</th>
            <th>폐점시간</th>
            <th>휴일</th>
           <%--
            <th>MMS<br />할당개수</th>
            <th>MMS<br />남은개수</th>
            --%>
        </tr>
        </table>
    
        <div class="container" style="width:950px;height:300px;overflow:auto;">

        <asp:GridView ID="gvList" runat="server" Width="930px" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false" 
        CssClass="boardListType" ShowHeader="false" style="margin-top:-2px;border-collapse:collapse;">
        <Columns>
            <asp:TemplateField HeaderText="선택">
                <HeaderStyle Width="5%" />
                <ItemStyle CssClass="align_C" Width="5%" />
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="코드">
                <HeaderStyle Width="10%" />
                <ItemStyle CssClass="align_C" Width="10%" />
                <ItemTemplate>
                    <asp:TextBox ID="txtSiteId" runat="server" Text='<%# Bind("siteId") %>' style="font-family:Dotum;"  ReadOnly="true"  Width="130"  BorderStyle="none" BorderWidth="1" />
                    <asp:HiddenField ID="hdnMgrId" runat="server" Value='<%# Bind("mgrId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="명칭">
                <HeaderStyle Width="10%" />
                <ItemStyle CssClass="align_L" Width="10%" />
                <ItemTemplate>
                    <asp:TextBox ID="txtSiteName" runat="server" Text='<%# Bind("siteName") %>' style="font-family:Dotum;" ReadOnly="true" Width="130"  BorderStyle="none" BorderWidth="1" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="연락처">
                <HeaderStyle Width="10%" />
                <ItemStyle CssClass="left" Width="10%" />
                <ItemTemplate>
                    <asp:TextBox ID="txtPhoneNo" runat="server" Text='<%# Bind("phoneNo1") %>' style="font-family:Dotum;" ReadOnly="true"  Width="130" BorderStyle="none" BorderWidth="1" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="개점시간" HeaderStyle-CssClass="center">
                <HeaderStyle Width="5%" />
                <ItemStyle CssClass="left" Width="5%" />
                <ItemTemplate>
                    <asp:TextBox ID="txtShopOpenTime" runat="server" Text='<%# Bind("shopOpenTime") %>' style="font-family:Dotum;" Width="65" ReadOnly="true"  BorderStyle="none" BorderWidth="1" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="폐점시간" HeaderStyle-CssClass="center">
                <HeaderStyle Width="5%" />
                <ItemStyle CssClass="left" Width="5%" />
                <ItemTemplate>
                    <asp:TextBox ID="txtShopCloseTime" runat="server" Text='<%# Bind("shopCloseTime") %>' style="font-family:Dotum;" Width="65" ReadOnly="true"  BorderStyle="none" BorderWidth="1" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="휴일">
                <HeaderStyle Width="5%" />
                <ItemStyle CssClass="left" Width="5%"  />
                <ItemTemplate>
                    <asp:TextBox ID="txtHoliday" runat="server" Text='<%# Bind("holiday") %>' style="font-family:Dotum;" ReadOnly="true" Width="65"  BorderStyle="none" BorderWidth="1" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="MMS할당제한" Visible=false>
                <HeaderStyle Width="4%" />
                <ItemStyle CssClass="left" Width="4%" />
                <ItemTemplate>
                     <asp:TextBox ID="txtComment1" runat="server" Text='<%# Bind("comment1") %>' style="font-family:Dotum;" onkeyup="NumberOnly(this);" ReadOnly="true" Width="40"  BorderStyle="none" BorderWidth="1" />
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="MMS남은제한" Visible=false>
                <HeaderStyle Width="4%" />
                <ItemStyle CssClass="left" Width="4%" />
                <ItemTemplate>
                     <asp:TextBox ID="txtComment2" runat="server" Text='<%# Bind("comment2") %>' style="font-family:Dotum;" onkeyup="NumberOnly(this);" ReadOnly="true" Width="40"  BorderStyle="none" BorderWidth="1" />
                </ItemTemplate>
            </asp:TemplateField>  
            
            <asp:TemplateField HeaderText="조직종류" Visible=false>
                <HeaderStyle Width="4%" />
                <ItemStyle CssClass="left" Width="4%" />
                <ItemTemplate>
                     <asp:TextBox ID="txtBusinessType" runat="server" Text='<%# Bind("businessType") %>' style="font-family:Dotum;"  ReadOnly="true" Width="40"  BorderStyle="none" BorderWidth="1" />
                </ItemTemplate>
            </asp:TemplateField>      
            
            
        </Columns>
        <EmptyDataTemplate>
            <table style="width:100%;height:290px;margin-top:-5px;">
                <tr>
                    <td style="text-align:center">
                    조회된 데이터가 없습니다.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        </asp:GridView>   
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    
    <div style="color:#8b0000;">◆ 체크박스를 선택하면 N건을 수정 또는 삭제 할 수 있습니다.</div> 
    
    <div class='topOptionBtns' style="margin-top:5px">
        <asp:ImageButton ID="btnInit" runat="server" ToolTip="신규" OnClick="btnInit_Click" ImageUrl="~/Asset/Images/button/btn_new.jpg"  />
        <asp:ImageButton ID="btnInsert"  runat="server" ToolTip="등록" OnClick="btnInsert_Click" OnClientClick='return DoReg()' ImageUrl="~/Asset/Images/button/btn_reg.jpg"  />        
        
    </div>    
    
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnInsert" />
        <asp:AsyncPostBackTrigger ControlID="btnInit" />
    </Triggers>
    <ContentTemplate>
      <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
        <colgroup>
            <col width="15%" />
            <col width="auto" /> 
            <col width="15%" />
            <col width="auto" /> 
            <col width="15%" />
            <col width="auto" /> 
        </colgroup>
        <tr>
            <th>상위코드<font color="red"> *</font></th>
            <td colspan="5"> 
                <asp:DropDownList ID="ddlTypeReg" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlTypeReg_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClassReg1" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClassReg1_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClassReg2" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClassReg2_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClassReg3" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClassReg3_SelectedIndexChanged" />
                
                <asp:HiddenField ID="hiddenSelectedSiteIdReg" runat="server" />
            </td>
            
        </tr> 
        <tr>
            <th>명칭<font color="red"> *</font></th>
            <td>
                <asp:TextBox ID="txtSiteName" runat="server" Width="135" CssClass="textType" />
            </td> 
            <th>코드<font color="red"> *</font></th>
            <td> 
                <asp:TextBox ID="txtSiteId" runat="server" Width="135"  CssClass="textType" />
            </td>
            <th>조직종류<font color="red"> *</font></th>
            <td>
              <asp:DropDownList ID="ddlBusinessType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="false" />
            </td>
        </tr>        
        <tr> 
            <th>개점시간</th>
            <td> 
                <asp:TextBox ID="txtShopOpenTime" runat="server" Width="135" CssClass="textType" />
            </td>  
            <th>폐점시간</th>
            <td>
                <asp:TextBox ID="txtShopCloseTime" runat="server" Width="135" CssClass="textType" />
            </td>             
            <th>휴일</th>
            <td>
                <asp:TextBox ID="txtHoliday" runat="server" Width="135"  CssClass="textType" />
            </td> 
        </tr> 
        <tr> 
            <th>연락처</th>
            <td> 
                <asp:TextBox ID="txtPhoneNo" runat="server" Width="135" CssClass="textType" />
            </td>  
            
            <th><%--MMS할당개수(월)--%></th>
            <td>
                  <asp:TextBox ID="txtComment1" runat="server" Width="135"  CssClass="textType" onkeyup="NumberOnly(this);" Visible=false />&nbsp;
            </td> 
            <th><%--MMS남은개수--%></th>
            <td>
                 <asp:TextBox ID="txtComment2" runat="server" Width="135"  CssClass="textType" onkeyup="NumberOnly(this);" Visible=false />&nbsp;
            </td>
            
        </tr> 
        
        
        </table> 
    </ContentTemplate>
    </asp:UpdatePanel>
    <div style="color:#8b0000;margin-top:5px">◆ 신규버튼은 신규 데이터를 입력할 수 있도록 <B>입력화면을 초기화</B> 합니다.</div> 

</asp:Content>

