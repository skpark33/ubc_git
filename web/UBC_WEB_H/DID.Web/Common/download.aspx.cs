using System;
using System.IO;
using System.Text;
using System.Data;

using DID.Common.Framework;

public partial class _Default : BasePage
{
	public void Page_Load(object sender, EventArgs e)
	{
        DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
        if (impersonater.ImpersonateValidUser())
        {
            this.Download();

            impersonater.UndoImpersonation();
        }
	}

    private void Download()
    {
        string attachId = Request["attachId"];
        int boardMasterId = Convert.ToInt32(Request["boardMasterId"]);

        DataTable dt = null;
        String FilePath = "";
        String FileName = "";

        using (DID.Service.Community.BoardBase obj = new DID.Service.Community.BoardBase())
        {
            dt = obj.GetAttach(Convert.ToInt32(attachId));

            FileName = dt.ToDataRow()["fileName"].ToString();
        }

        FilePath = System.IO.Path.Combine(Common.GetFilePath(boardMasterId), FileName);

        String szOffset = Request["_filesize"];
        if (szOffset == null)
        {
            szOffset = "0";
        }

        Int64 file_offset = Int64.Parse(szOffset);


        // Length of the file:
        int length;

        // Total bytes to read:
        long dataToRead;


        System.IO.Stream iStream = null;
        byte[] buffer = new byte[10000];

        try
        {
            iStream = new System.IO.FileStream(FilePath, System.IO.FileMode.Open,
                    System.IO.FileAccess.Read, System.IO.FileShare.Read);

            iStream.Seek(file_offset, SeekOrigin.Begin);

            dataToRead = iStream.Length;
            dataToRead -= file_offset;
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", Convert.ToString(dataToRead));
            Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName);


            while (dataToRead > 0)
            {
                // Verify that the client is connected.
                if (Response.IsClientConnected)
                {
                    // Read the data in buffer.
                    length = iStream.Read(buffer, 0, 10000);

                    // Write the data to the current output stream.
                    Response.OutputStream.Write(buffer, 0, length);

                    // Flush the data to the HTML output.
                    Response.Flush();

                    buffer = new Byte[10000];
                    dataToRead = dataToRead - length;
                }
                else
                {
                    //prevent infinite loop if user disconnects
                    dataToRead = -1;
                }
            }

        }
        catch (Exception ex)
        {
            // Trap the error, if any.
            Response.Write("Error : " + ex.Message);

        }
        finally
        {
            if (iStream != null)
            {
                //Close the file.
                iStream.Close();
            }
            Response.Close();

        }
    }
}