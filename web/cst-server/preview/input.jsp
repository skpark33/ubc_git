<%--
  - 작성자: yngwie@sqisoft.com
  - 일자: 2007.08.23
  - 저작권 표시:
  - @(#)
  - 설명: 미리보기 메인  화면
  - 수정자:
  - 수정일:
  - 수정사유:
  --%>
<%@page language="java"  contentType="text/html;charset=UTF-8" %>
<%@page import="com.sqisoft.utv.core.common.Constants" %>
<%@ taglib uri="/tlds/struts-logic" prefix="logic" %>
<%@ taglib uri="/tlds/struts-bean" prefix="bean" %>
<%@ taglib uri="/tlds/struts-html" prefix="html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" contents="text/html; charset=UTF-8" />
<title></title>

<script language="JavaScript" type="text/JavaScript">
<!--
function setToday(){
	var date = new Date();

	var year = date.getFullYear();

	var month = date.getMonth() + 1;
	if(month < 10) month = '0' + month;

	var day = date.getDate();
	if(day < 10) day = '0' + day;

	var hour = date.getHours();
	if(hour < 10) hour = '0' + hour;

	var min = date.getMinutes();
	if(min < 10) min = '0' + min;

	var yearObj = document.getElementById("year");
	var monthObj = document.getElementById("month");
	var dayObj = document.getElementById("day");

	var hourObj = document.getElementById("hour");
	var minObj = document.getElementById("min");

	setSelectedValue(monthObj, month);
	setSelectedValue(dayObj, day);
	setSelectedValue(hourObj, hour);
	setSelectedValue(minObj, min);
}

function setSelectedValue(obj, txt){

	for(var i = 0; i < obj.options.length; i++){
		var value = obj.options[i].value;

		if(txt == value) {
			obj.options[i].selected = true;
			break;
		}
	}
}

function setDateParam(){
	var date = document.getElementById("year").value + document.getElementById("month").value + document.getElementById("day").value;
	var time = document.getElementById("hour").value + ":" + document.getElementById("min").value + ":00";

	var frm = document.inputForm;
	frm.date.value = date;
	frm.time.value = time;

	return true;

}

//-->
</script>
</head>

<body onload="setToday()" style="margin:0 0 0 0; background-color:#FFFF80">
<html:form method="get" action="login" target="bodyFrame" onsubmit="return setDateParam()">
	<input type="hidden" name="date" >
	<input type="hidden" name="time" >

	<table border="0"  height="30" align="center" bgcolor="#80FF00">
		<tbody>
			<tr>
				<td >Site ID</td>
				<td><input type="text" maxlength="10" size="10" value="sqisoft" name="siteId" /></td>
				<td >Host ID</td>
				<td><input type="text" maxlength="10" size="10" value="UTV-Test" name="hostId" />&nbsp;&nbsp;&nbsp;</td>
				<td>
					<select id="year">
						<option value="2007">2007</option>
						<option value="2008">2008</option>
					</select>
				</td>
				<td >년&nbsp;</td>
				<td>
					<select  id="month">
						<%  for (int i = 1; i <= 12; i++) {
								String str = "" + i;
								if(i < 10) str = "0" + str;
						%>
							<option value="<%=str%>" ><%=str%></option>
						<% } %>
					</select>
				</td>
				<td>월&nbsp;</td>
				<td>
					<select id="day">
						<%  for (int i = 1; i <= 31; i++) {
								String str = "" + i;
								if(i < 10) str = "0" + str;
						%>
							<option value="<%=str%>" ><%=str%></option>
						<% } %>
					</select>
				</td>
				<td>일&nbsp;</td>
				<td>
					<select id="hour">
						<%  for (int i = 1; i <= 24; i++) {
								String str = "" + i;
								if(i < 10) str = "0" + str;
						%>
							<option value="<%=str%>" ><%=str%></option>
						<% } %>
					</select>
				</td>
				<td>시&nbsp;</td>
				<td>
					<select id="min">
						<%  for (int i = 1; i <= 60; i++) {
								String str = "" + i;
								if(i < 10) str = "0" + str;
						%>
							<option value="<%=str%>" ><%=str%></option>
						<% } %>
					</select>
				</td>
				<td>분</td>
				<td>&nbsp;&nbsp;&nbsp;<input type="submit" value="조회"></td>
			</tr>
		</tbody>
	</table>
</html:form>
</body>
</html>
