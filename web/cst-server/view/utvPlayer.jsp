<%--
  - 작성자: yngwie@sqisoft.com
  - 일자: 2007.08.14
  - 저작권 표시:
  - @(#)
  - 설명: main frame 표시 화면
  - 수정자:
  - 수정일:
  - 수정사유:
  --%>
<%@page language="java"  contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<style>
body { 
	margin: 0px; 
	background-color: #000000;
    overflow-y: hidden;
    overflow-x: hidden;
}
</style>
<script src="/js/common.js" ></script>
<script src="/js/FABridge.js" ></script>
<script language="JavaScript" type="text/javascript">
	
	var initCallback = function(){
		var flexApp = FABridge.flash.root();

		var siteId = "<%=request.getAttribute("siteId")%>";
		var hostId = "<%=request.getAttribute("hostId")%>";
		var localIp = "<%=request.getAttribute("localIp")%>";
		var serverUrl = "<%=request.getAttribute("serverUrl")%>";
		
		var fsu = "<%=request.getAttribute("FLV_SERVER_URL")%>";
		var flu = "<%=request.getAttribute("FLV_LOCAL_URL")%>";
		var fsp = "<%=request.getAttribute("FLV_SERVER_PROTOCOL")%>";
		var flp = "<%=request.getAttribute("FLV_LOCAL_PROTOCOL")%>";
		var isu = "<%=request.getAttribute("IMG_SERVER_URL")%>";
		var ilu = "<%=request.getAttribute("IMG_LOCAL_URL")%>";
		
		flexApp.setHostInfo(siteId, hostId, localIp, serverUrl, fsu, flu, fsp, flp, isu, ilu);
		flexApp.initTemplate();
		
	}

	FABridge.addInitializationCallback("flash",initCallback);


	function setDateTime(dateStr, timeStr){
		var flexApp = FABridge.flash.root();
	}

	function refreshTemplate(tplId){
		var flexApp = FABridge.flash.root();
		
		flexApp.getTemplate(tplId);
	}

	function refreshFrame(val){
		var value = new String(val);
		var tplId = value.substring(0, 2);
		var frmId = value.substring(2);
		
		var flexApp = FABridge.flash.root();
		
		flexApp.getFrame(tplId, frmId);
	}	
	
	function refreshPromotionValueList(frmId){
		var flexApp = FABridge.flash.root();
		
		flexApp.getPromotionValueList(frmId);
	}	

</script>
</head>
<body scroll="no" >
	<script>showFlash()</script>
</body>
</html>