
package com.sqisoft.utv.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Site 의 정보를 보관한다.
 *
 * @author yngwie
 *
 */
public final class UtvSite implements Serializable{

	private static final long serialVersionUID = 5657182301574535402L;

	private String siteId;
	private String hostId;

	public String toString(){
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);

	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
}
