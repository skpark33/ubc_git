
package com.sqisoft.utv.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 *
 * @author yngwie
 *
 */
public final class UtvSchedule implements Serializable {

	private static final long serialVersionUID = 1574209501481063513L;

	private String siteId;
	private String hostId;
	private int scheduleId;
	
	private UtvTemplate template;
	
	private String frameId;
	private String requestId;
	private String contentsId;

	private UtvContents contents;

	private UtvFrame frame;

	private Timestamp startDate;
	private Timestamp endDate;
	private Timestamp startTime;
	private Timestamp endTime;

	private Timestamp fromTime;
	private Timestamp toTime;

	private int openningFlag;
	private int priority;

	private int castingState;
	private Timestamp castingStateTime;
	private String castingStateHistory;
	private String result;
	private int operationalState;
	private int adminState;

	public String toString(){
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);

	}

	public int getAdminState() {
		return adminState;
	}

	public void setAdminState(int adminState) {
		this.adminState = adminState;
	}

	public int getCastingState() {
		return castingState;
	}

	public void setCastingState(int castingState) {
		this.castingState = castingState;
	}

	public String getCastingStateHistory() {
		return castingStateHistory;
	}

	public void setCastingStateHistory(String castingStateHistory) {
		this.castingStateHistory = castingStateHistory;
	}

	public Timestamp getCastingStateTime() {
		return castingStateTime;
	}

	public void setCastingStateTime(Timestamp castingStateTime) {
		this.castingStateTime = castingStateTime;
	}

	public String getContentsId() {
		return contentsId;
	}

	public void setContentsId(String contentsId) {
		this.contentsId = contentsId;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public String getFrameId() {
		return frameId;
	}

	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}

	public Timestamp getFromTime() {
		return fromTime;
	}

	public void setFromTime(Timestamp fromTime) {
		this.fromTime = fromTime;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public int getOpenningFlag() {
		return openningFlag;
	}

	public void setOpenningFlag(int openningFlag) {
		this.openningFlag = openningFlag;
	}

	public int getOperationalState() {
		return operationalState;
	}

	public void setOperationalState(int operationalState) {
		this.operationalState = operationalState;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getToTime() {
		return toTime;
	}

	public void setToTime(Timestamp toTime) {
		this.toTime = toTime;
	}

	public UtvContents getContents() {
		return contents;
	}

	public void setContents(UtvContents contents) {
		this.contents = contents;
	}

	public UtvFrame getFrame() {
		return frame;
	}

	public void setFrame(UtvFrame frame) {
		this.frame = frame;
	}

	public UtvTemplate getTemplate() {
		return template;
	}

	public void setTemplate(UtvTemplate template) {
		this.template = template;
	}



}
