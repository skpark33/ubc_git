

package com.sqisoft.utv.dto;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public final class UtvContents implements Serializable {

	private static final long serialVersionUID = -118772779231577306L;

	private String frameId;
	private String contentsId;
	private String promotionId;
	private String parentContentsId;
	private String contentsName;
	private String requestId;
	private String saleContentsId;

	private int contentsState;
	private int contentsType;
	private String description;
	private String location;
	private String fileName;
	private int width;
	private int height;

	private int duration;
	private int runningTime;
	private int encodingTime;
	private String encodingInfo;

	private String comment1;
	private String comment2;
	private String comment3;
	
	private String bgColor;
	private String fgColor;
	private String font;
	private int fontSize;
	private int playSpeed;
	private String playerInfo;
	private int soundVolume;
	private boolean defaultContent;
	private String promotionValueList;
	
	private int playOrder;
	private int maxOrder;
	
	private int grade;
	
	private List<UtvProgressBar> progressBarList;

	public int getGrade() {
		return grade;
	}


	public void setGrade(int grade) {
		this.grade = grade;
	}


	public String toString(){
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);

	}


	public String getContentsId() {
		return contentsId;
	}


	public void setContentsId(String contentsId) {
		this.contentsId = contentsId;
	}


	public String getParentContentsId() {
		return parentContentsId;
	}


	public void setParentContentsId(String parentContentsId) {
		this.parentContentsId = parentContentsId;
	}


	public String getContentsName() {
		return contentsName;
	}


	public void setContentsName(String contentsName) {
		this.contentsName = contentsName;
	}


	public String getRequestId() {
		return requestId;
	}


	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}


	public String getSaleContentsId() {
		return saleContentsId;
	}


	public void setSaleContentsId(String saleContentsId) {
		this.saleContentsId = saleContentsId;
	}


	public int getContentsState() {
		return contentsState;
	}


	public void setContentsState(int contentsState) {
		this.contentsState = contentsState;
	}


	public int getContentsType() {
		return contentsType;
	}


	public void setContentsType(int contentsType) {
		this.contentsType = contentsType;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public int getWidth() {
		return width;
	}


	public void setWidth(int width) {
		this.width = width;
	}


	public int getHeight() {
		return height;
	}


	public void setHeight(int height) {
		this.height = height;
	}


	public int getRunningTime() {
		return runningTime;
	}


	public void setRunningTime(int runningTime) {
		this.runningTime = runningTime;
	}


	public int getEncodingTime() {
		return encodingTime;
	}


	public void setEncodingTime(int encodingTime) {
		this.encodingTime = encodingTime;
	}


	public String getEncodingInfo() {
		return encodingInfo;
	}


	public void setEncodingInfo(String encodingInfo) {
		this.encodingInfo = encodingInfo;
	}


	public String getComment1() {
		return comment1;
	}


	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}


	public String getComment2() {
		return comment2;
	}


	public void setComment2(String comment2) {
		this.comment2 = comment2;
	}


	public String getComment3() {
		return comment3;
	}


	public void setComment3(String comment3) {
		this.comment3 = comment3;
	}


	public String getBgColor() {
		return bgColor;
	}


	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}


	public String getFgColor() {
		return fgColor;
	}


	public void setFgColor(String fgColor) {
		this.fgColor = fgColor;
	}


	public String getFont() {
		return font;
	}


	public void setFont(String font) {
		this.font = font;
	}


	public int getFontSize() {
		return fontSize;
	}


	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}


	public int getPlaySpeed() {
		return playSpeed;
	}


	public void setPlaySpeed(int playSpeed) {
		this.playSpeed = playSpeed;
	}


	public String getPlayerInfo() {
		return playerInfo;
	}


	public void setPlayerInfo(String playerInfo) {
		this.playerInfo = playerInfo;
	}


	public int getSoundVolume() {
		return soundVolume;
	}


	public void setSoundVolume(int soundVolume) {
		this.soundVolume = soundVolume;
	}


	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}


	public String getFrameId() {
		return frameId;
	}


	public void setFrameId(String frameId) {
		this.frameId = frameId;
	}

	public boolean isDefaultContent() {
		return defaultContent;
	}


	public void setDefaultContent(boolean defaultContent) {
		this.defaultContent = defaultContent;
	}


	public int getPlayOrder() {
		return playOrder;
	}


	public void setPlayOrder(int playOrder) {
		this.playOrder = playOrder;
	}


	public int getMaxOrder() {
		return maxOrder;
	}


	public void setMaxOrder(int maxOrder) {
		this.maxOrder = maxOrder;
	}


	public String getPromotionId() {
	    return promotionId;
	}


	public void setPromotionId(String promotionId) {
	    this.promotionId = promotionId;
	}


	public String getPromotionValueList() {
	    return promotionValueList;
	}


	public void setPromotionValueList(String promotionValueList) {
	    this.promotionValueList = promotionValueList;
	}


	public List<UtvProgressBar> getProgressBarList() {
	    return progressBarList;
	}


	public void setProgressBarList(List<UtvProgressBar> progressBarList) {
	    this.progressBarList = progressBarList;
	}




}
