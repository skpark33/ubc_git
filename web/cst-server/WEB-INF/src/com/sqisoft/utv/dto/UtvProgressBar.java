package com.sqisoft.utv.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class UtvProgressBar implements Serializable {
    
    private static final long serialVersionUID = -5763190148772638669L;
    private String contentsId;
    private String progressBarId;
    private String itemName;
    private int x;
    private int y;
    private int width;
    private int height;
    private String fgColor;
    private String bgColor;
    private String borderColor;
    private String borderThickness;
    private int valueVisible;
    private int fontSize;
    private int minValue;
    private int maxValue;
    
    public UtvProgressBar() {
	
    }

    public String toString(){
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
    
    public String getContentsId() {
        return contentsId;
    }
    public void setContentsId(String contentsId) {
        this.contentsId = contentsId;
    }
    public String getProgressBarId() {
        return progressBarId;
    }
    public void setProgressBarId(String progressBarId) {
        this.progressBarId = progressBarId;
    }
    public String getItemName() {
        return itemName;
    }
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public String getFgColor() {
        return fgColor;
    }
    public void setFgColor(String fgColor) {
        this.fgColor = fgColor;
    }
    public String getBgColor() {
        return bgColor;
    }
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }
    public String getBorderColor() {
        return borderColor;
    }
    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }
    public String getBorderThickness() {
        return borderThickness;
    }
    public void setBorderThickness(String borderThickness) {
        this.borderThickness = borderThickness;
    }
    public int getValueVisible() {
        return valueVisible;
    }
    public void setValueVisible(int valueVisible) {
        this.valueVisible = valueVisible;
    }
    public int getFontSize() {
        return fontSize;
    }
    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }
    public int getMinValue() {
        return minValue;
    }
    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }
    public int getMaxValue() {
        return maxValue;
    }
    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }
    
}
