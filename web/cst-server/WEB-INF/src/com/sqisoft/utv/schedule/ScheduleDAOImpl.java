
package com.sqisoft.utv.schedule;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.sqisoft.utv.core.common.Constants;
import com.sqisoft.utv.core.exception.JDBCException;
import com.sqisoft.utv.core.jdbc.ComponentDAO;
import com.sqisoft.utv.core.jdbc.LoggableStatement;
import com.sqisoft.utv.core.sql.SQLFactory;
import com.sqisoft.utv.core.sql.SQLParser;
import com.sqisoft.utv.dto.UtvContents;
import com.sqisoft.utv.dto.UtvFrame;
import com.sqisoft.utv.dto.UtvHost;
import com.sqisoft.utv.dto.UtvProgressBar;
import com.sqisoft.utv.dto.UtvTemplate;

public final class ScheduleDAOImpl extends ComponentDAO {

	
	public Map getResult(Map<String, String> param, int index, String CONTENTS_TYPE_PROMOTION) throws JDBCException {
		Map result = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try{
			SQLParser parser = SQLFactory.getParser("sql_utv");
			
			String query = parser.getQuery("ScheduleAction", "getResult", "1");
			
			String replaceStr1 = "";
			switch(index) {
        			case 1 : //initTemplate
        			    replaceStr1 = "";
        			    break;
        			case 2 : //getTemplate
        			    replaceStr1 = " and templateId = ?\n";
        			    break;
        			case 3 : //getFrame
        			    replaceStr1 = " and templateId = ?\n and frameId = ?\n";
        			    break;
        			default :
        			     break;
			}
			
			query = query.replaceAll("#replaceStr1#", replaceStr1);
			
			conn = getConnection(Constants.DB_ACCESS_KEY_DEFAULT);
			pstmt = new LoggableStatement(conn, query);

			int idx = 0;

			pstmt.setString(++idx, getString(param, "siteId"));
			pstmt.setString(++idx, getString(param, "hostId"));
			
			if(index > 1) pstmt.setString(++idx, getString(param, "templateId"));
			if(index > 2) pstmt.setString(++idx, getString(param, "frameId"));
			
			if(getString(param, "date").length() == 8) {
				pstmt.setString(++idx, getString(param, "date"));
				pstmt.setString(++idx, getString(param, "time"));
			}
			
			pstmt.setString(++idx, getString(param, "siteId"));
			pstmt.setString(++idx, getString(param, "hostId"));
			
			if(index > 1) pstmt.setString(++idx, getString(param, "templateId"));
			if(index > 2) pstmt.setString(++idx, getString(param, "frameId"));
			
			if(getString(param, "date").length() == 8) {
				pstmt.setString(++idx, getString(param, "date"));
				pstmt.setString(++idx, getString(param, "time"));
			}			
			
			if(index > 1) {
			    pstmt.setString(++idx, getString(param, "templateId"));
			}else {
			    pstmt.setString(++idx, "01");
			}
			
			pstmt.setString(++idx, getString(param, "siteId"));
			pstmt.setString(++idx, getString(param, "hostId"));
			
			if(index > 1) pstmt.setString(++idx, getString(param, "templateId"));
			if(index > 2) pstmt.setString(++idx, getString(param, "frameId"));
			
			if(getString(param, "date").length() == 8) {
				pstmt.setString(++idx, getString(param, "date"));
			}
			
			pstmt.setString(++idx, getString(param, "siteId"));
			pstmt.setString(++idx, getString(param, "hostId"));
			
			if(index > 1) pstmt.setString(++idx, getString(param, "templateId"));
			if(index > 2) pstmt.setString(++idx, getString(param, "frameId"));
			
			if(getString(param, "date").length() == 8) {
				pstmt.setString(++idx, getString(param, "date"));
				pstmt.setString(++idx, getString(param, "time"));
			}			

			pstmt.setInt(++idx, getInt(param, "playOrder"));

			log.debug("\n" +  ((LoggableStatement)pstmt).getQueryString() );

			rset = pstmt.executeQuery();
			
			boolean isSet = false;
			
			UtvHost host = new UtvHost();
			host.setLocalIp(getString(param, "localIp"));
			
			UtvTemplate template = new UtvTemplate();
			
			List<UtvFrame> frames = new ArrayList<UtvFrame>();
			
			while(rset.next()) {
				if(!isSet) {
					host.setSiteId(rset.getString("siteId"));
					host.setHostId(rset.getString("hostId"));
					
					template.setTemplateId(rset.getString("templateId"));
					template.setWidth(rset.getInt("templateWidth"));
					template.setHeight(rset.getInt("templateHeight"));
					
					isSet = true;
				}
				
				UtvFrame frame = new UtvFrame();
				
				frame.setTemplateId(rset.getString("templateId"));
				frame.setFrameId(rset.getString("frameId"));
				frame.setWidth(rset.getInt("frameWidth"));
				frame.setHeight(rset.getInt("frameHeight"));
				frame.setX(rset.getInt("x"));
				frame.setY(rset.getInt("y"));
				frame.setGrade(rset.getInt("grade"));
				
				frame.setBorderColor(rset.getString("borderColor"));
				frame.setBorderStyle(rset.getString("borderStyle"));
				frame.setBorderThickness(rset.getInt("borderThickness"));
				frame.setCornerRadius(rset.getInt("cornerRadius"));
				
				UtvContents contents = new UtvContents();
				contents.setFrameId(rset.getString("frameId"));
				contents.setContentsId(rset.getString("contentsId"));
				contents.setContentsName(rset.getString("contentsName"));
				contents.setContentsType(rset.getInt("contentsType"));
				contents.setDuration(rset.getInt("duration"));
				contents.setFgColor(rset.getString("fgColor"));
				contents.setBgColor(rset.getString("bgColor"));
				contents.setFont(rset.getString("font"));
				contents.setFontSize(rset.getInt("fontSize"));
				contents.setFileName(rset.getString("filename"));
				contents.setLocation(rset.getString("location"));
				contents.setRunningTime(rset.getInt("runningTime"));
				contents.setSoundVolume(rset.getInt("soundVolume"));
				contents.setPlaySpeed(rset.getInt("playSpeed"));
				contents.setComment1(rset.getString("comment1"));
				contents.setComment2(rset.getString("comment2"));
				contents.setComment3(rset.getString("comment3"));
				contents.setDescription(rset.getString("description"));
				contents.setPromotionId(rset.getString("promotionId"));
				contents.setPromotionValueList(rset.getString("promotionValueList"));
				contents.setDefaultContent( Boolean.valueOf( rset.getString("defaultContent") ).booleanValue() );
				
				contents.setPlayOrder(rset.getInt("playOrder"));
				contents.setMaxOrder(rset.getInt("maxOrder"));
				
				contents.setGrade(rset.getInt("grade"));
				
				if(String.valueOf( contents.getContentsType() ).equals( CONTENTS_TYPE_PROMOTION ) ) {
				    contents.setProgressBarList( getProgressBarList(conn, contents.getContentsId()) );
				}
				
				frame.setContents(contents);
				
				frames.add(frame);
				
			}
			
			result = new HashMap<String, Object>();
			result.put("host", host);
			result.put("template", template);
			result.put("frames", frames);
			
			log.debug(getListToString(frames));
			
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			throw new JDBCException(e.getMessage());
		}finally{
			close(conn, pstmt, rset);
		}
		
		return result;
	}	
	
	public List<UtvProgressBar> getProgressBarList(Connection conn, String contentsId) throws JDBCException {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		List<UtvProgressBar> barList = null;
		
		try{
			SQLParser parser = SQLFactory.getParser("sql_utv");
			
			String query = parser.getQuery("ScheduleAction", "getProgressBarList", "1");
			
			pstmt = new LoggableStatement(conn, query);

			int idx = 0;

			pstmt.setString(++idx, contentsId);
			
			log.debug("\n" +  ((LoggableStatement)pstmt).getQueryString() );
			
			rset = pstmt.executeQuery();
			
			barList = new ArrayList<UtvProgressBar>();
			
			while(rset.next()) {
			    UtvProgressBar bar = new UtvProgressBar();
			    bar.setBgColor(rset.getString("bgColor"));
			    bar.setBorderColor(rset.getString("borderColor"));
			    bar.setBorderThickness(rset.getString("borderThickness"));
			    bar.setContentsId(rset.getString("contentsId"));
			    bar.setFgColor(rset.getString("fgColor"));
			    bar.setFontSize(rset.getInt("fontSize"));
			    bar.setHeight(rset.getInt("height"));
			    bar.setItemName(rset.getString("itemName"));
			    bar.setMaxValue(rset.getInt("maxValue"));
			    bar.setMinValue(rset.getInt("minValue"));
			    bar.setProgressBarId(rset.getString("progressBarId"));
			    bar.setValueVisible(rset.getInt("valueVisible"));
			    bar.setWidth(rset.getInt("width"));
			    bar.setX(rset.getInt("x"));
			    bar.setY(rset.getInt("y"));
			    
			    barList.add(bar);
			    
			}
			
			
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			throw new JDBCException(e.getMessage());
		}finally{
			close(pstmt, rset);
		}
		
		return barList;
			
	}		
/*	
	public Map<String, List<UtvProgressBar>> getProgressBarList(Map<String, String> param) throws JDBCException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		Map<String, List<UtvProgressBar>> result = null;
		
		try{
			SQLParser parser = SQLFactory.getParser("sql_utv");
			
			String query = parser.getQuery("ScheduleAction", "getProgressBarList", "1");
			
			conn = getConnection(Constants.DB_ACCESS_KEY_DEFAULT);
			pstmt = new LoggableStatement(conn, query);

			int idx = 0;

			pstmt.setString(++idx, getString(param, "contentsId"));
			
			rset = pstmt.executeQuery();
			
			List<UtvProgressBar> barList = new ArrayList<UtvProgressBar>();
			
			while(rset.next()) {
			    UtvProgressBar bar = new UtvProgressBar();
			    bar.setBgColor(rset.getString("bgColor"));
			    bar.setBorderColor(rset.getString("borderColor"));
			    bar.setBorderThickness(rset.getString("borderThickness"));
			    bar.setContentsId(rset.getString("contentsId"));
			    bar.setFgColor(rset.getString("fgColor"));
			    bar.setFontSize(rset.getInt("fontSize"));
			    bar.setHeight(rset.getInt("height"));
			    bar.setItemName(rset.getString("itemName"));
			    bar.setMaxValue(rset.getInt("maxValue"));
			    bar.setMinValue(rset.getInt("minValue"));
			    bar.setProgressBarId(rset.getString("progressBarId"));
			    bar.setValueVisible(rset.getInt("valueVisible"));
			    bar.setWidth(rset.getInt("width"));
			    bar.setX(rset.getInt("x"));
			    bar.setY(rset.getInt("y"));
			    
			    barList.add(bar);
			    
			}
			
			result = new HashMap<String, List<UtvProgressBar>>();
			result.put("barList", barList);			
						
			
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			throw new JDBCException(e.getMessage());
		}finally{
			close(conn, pstmt, rset);
		}
		
		return result;
			
	}	
*/
	
	public Map<String, String> getPromotionValueList(Map<String, String> param) throws JDBCException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try{
			SQLParser parser = SQLFactory.getParser("sql_utv");
			
			String query = parser.getQuery("ScheduleAction", "getPromotionValueList", "1");
			
			conn = getConnection(Constants.DB_ACCESS_KEY_DEFAULT);
			pstmt = new LoggableStatement(conn, query);

			int idx = 0;

			pstmt.setString(++idx, getString(param, "contentsId"));
			
			rset = pstmt.executeQuery();
			
			
			if(rset.next()) {
			    String value = rset.getString("promotionValueList");
			    param.put("promotionValueList", value);
			}
			
						
			
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			throw new JDBCException(e.getMessage());
		}finally{
			close(conn, pstmt, rset);
		}
		
		return param;
			
	}
	
	private String getListToString(List<UtvFrame> result) {
		StringBuffer sb = new StringBuffer();
		
		for(int i = 0; i < result.size(); i++){
			UtvFrame obj = (UtvFrame)result.get(i);
			sb.append(ToStringBuilder.reflectionToString(obj, ToStringStyle.MULTI_LINE_STYLE));
		}
		
		return sb.toString();
	}

}
