
package com.sqisoft.utv.schedule;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sqisoft.utv.core.action.BaseDispatchAction;
import com.sqisoft.utv.core.util.SimpleUtil;
import com.sqisoft.utv.dto.UtvProgressBar;

public final class ScheduleAction extends BaseDispatchAction {

	public ActionForward initTemplate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		beforeHandWork(request);

		String localIp = request.getRemoteAddr();
		
		Map result = null;
		
                String siteId = SimpleUtil.nullCheck(request.getParameter("siteId")); // UTV_Site.siteId
                String hostId = SimpleUtil.nullCheck(request.getParameter("hostId")); // UTV_Host.hostId		
		String date = SimpleUtil.nullCheck(request.getParameter("date"));
		String time = SimpleUtil.nullCheck(request.getParameter("time"));
		String playOrder = SimpleUtil.nullCheck(request.getParameter("playOrder"));
		
		try{
			response.setContentType("text/JavaScript");
			
			// String siteId, String hostId, String date, String time, String localIp
			Map<String, String> param  = new HashMap<String, String>();
			param.put("siteId", siteId);
			param.put("hostId", hostId);
			param.put("date", date);
			param.put("time", time);
			param.put("playOrder", playOrder);
			param.put("localIp", localIp);

			ScheduleDAOImpl dao = (ScheduleDAOImpl)getCustomDAO(ScheduleDAOImpl.class.getName());
			
			result = dao.getResult(param, 1, getContextEnum("ContentsType", "CONTENTS_TYPE_PROMOTION"));

			JSONObject json = JSONObject.fromObject(result);
			
//			log.debug(json.toString());
			
			PrintWriter pOut = response.getWriter();
			pOut.println(json.toString());
			pOut.flush();
			
		
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			request.setAttribute("javax.servlet.jsp.jspException", e);
			return mapping.findForward("error");
		}
		
		return null;
	}
	
	public ActionForward getTemplate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		beforeHandWork(request);
		
		String localIp = request.getRemoteAddr();

		Map<String, Object> result = null;
		
                String siteId = SimpleUtil.nullCheck(request.getParameter("siteId")); // UTV_Site.siteId
                String hostId = SimpleUtil.nullCheck(request.getParameter("hostId")); // UTV_Host.hostId		
                String templateId = SimpleUtil.nullCheck(request.getParameter("templateId")); // UTV_Host.templateId		
		String date = SimpleUtil.nullCheck(request.getParameter("date"));
		String time = SimpleUtil.nullCheck(request.getParameter("time"));
		String playOrder = SimpleUtil.nullCheck(request.getParameter("playOrder"));
		
		try{
			response.setContentType("text/JavaScript");
			
			// String siteId, String hostId, String date, String time, String localIp
			Map<String, String> param  = new HashMap<String, String>();
			param.put("siteId", siteId);
			param.put("hostId", hostId);
			param.put("templateId", templateId);
			param.put("date", date);
			param.put("time", time);
			param.put("playOrder", playOrder);
			param.put("localIp", localIp);

			ScheduleDAOImpl dao = (ScheduleDAOImpl)getCustomDAO(ScheduleDAOImpl.class.getName());

			result = dao.getResult(param, 2, getContextEnum("ContentsType", "CONTENTS_TYPE_PROMOTION"));

			JSONObject json = JSONObject.fromObject(result);
			
			PrintWriter pOut = response.getWriter();
			pOut.println(json.toString());
			pOut.flush();
			
		
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			request.setAttribute("javax.servlet.jsp.jspException", e);
			return mapping.findForward("error");
		}
		
		return null;
	}	
	

	public ActionForward getFrame(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		beforeHandWork(request);
		
		String localIp = request.getRemoteAddr();

		Map<String, Object> result = null;
		
                String siteId = SimpleUtil.nullCheck(request.getParameter("siteId")); // UTV_Site.siteId
                String hostId = SimpleUtil.nullCheck(request.getParameter("hostId")); // UTV_Host.hostId		
                String templateId = SimpleUtil.nullCheck(request.getParameter("templateId")); // UTV_Host.templateId		
                String frameId = SimpleUtil.nullCheck(request.getParameter("frameId")); 
		String date = SimpleUtil.nullCheck(request.getParameter("date"));
		String time = SimpleUtil.nullCheck(request.getParameter("time"));
		String playOrder = SimpleUtil.nullCheck(request.getParameter("playOrder"));
		
		try{
			response.setContentType("text/JavaScript");
			
			// String siteId, String hostId, String date, String time, String localIp
			Map<String, String> param  = new HashMap<String, String>();
			param.put("siteId", siteId);
			param.put("hostId", hostId);
			param.put("templateId", templateId);
			param.put("frameId", frameId);
			param.put("date", date);
			param.put("time", time);
			param.put("playOrder", playOrder);
			param.put("localIp", localIp);

			ScheduleDAOImpl dao = (ScheduleDAOImpl)getCustomDAO(ScheduleDAOImpl.class.getName());

			result = dao.getResult(param, 3, getContextEnum("ContentsType", "CONTENTS_TYPE_PROMOTION"));

			JSONObject json = JSONObject.fromObject(result);
			
			PrintWriter pOut = response.getWriter();
			pOut.println(json.toString());
			pOut.flush();
			
		
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			request.setAttribute("javax.servlet.jsp.jspException", e);
			return mapping.findForward("error");
		}
		
		return null;
	}		

/*	
	public ActionForward getProgressBarList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
	    	beforeHandWork(request);

		List<UtvProgressBar> result = null;
		
		String siteId = SimpleUtil.nullCheck(request.getParameter("siteId")); // UTV_Site.siteId
		String hostId = SimpleUtil.nullCheck(request.getParameter("hostId")); // UTV_Host.hostId		
//            String templateId = SimpleUtil.nullCheck(request.getParameter("templateId")); // UTV_Host.templateId		
//            String frameId = SimpleUtil.nullCheck(request.getParameter("frameId")); 
	    	String contentsId = SimpleUtil.nullCheck(request.getParameter("contentsId")); 
	    	
		try{
			response.setContentType("text/JavaScript");
			
			// String siteId, String hostId, String date, String time, String localIp
			Map<String, String> param  = new HashMap<String, String>();
			param.put("siteId", siteId);
			param.put("hostId", hostId);
//			param.put("templateId", templateId);
//			param.put("frameId", frameId);			
			param.put("contentsId", contentsId);

			ScheduleDAOImpl dao = (ScheduleDAOImpl)getCustomDAO(ScheduleDAOImpl.class.getName());

			result = dao.getProgressBarList(param);

			JSONObject json = JSONObject.fromObject(result);
			
			PrintWriter pOut = response.getWriter();
			pOut.println(json.toString());
			pOut.flush();
			
		
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			request.setAttribute("javax.servlet.jsp.jspException", e);
			return mapping.findForward("error");
		}	    	
		
	    	return null;
	}	
*/	
	public ActionForward getPromotionValueList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
	    	beforeHandWork(request);

		Map<String, String> result = null;
		
                String siteId = SimpleUtil.nullCheck(request.getParameter("siteId")); // UTV_Site.siteId
                String hostId = SimpleUtil.nullCheck(request.getParameter("hostId")); // UTV_Host.hostId		
                String templateId = SimpleUtil.nullCheck(request.getParameter("templateId")); // UTV_Host.templateId		
                String frameId = SimpleUtil.nullCheck(request.getParameter("frameId")); 
	    	String contentsId = SimpleUtil.nullCheck(request.getParameter("contentsId")); 
	    	
		try{
			response.setContentType("text/JavaScript");
			
			// String siteId, String hostId, String date, String time, String localIp
			Map<String, String> param  = new HashMap<String, String>();
			param.put("siteId", siteId);
			param.put("hostId", hostId);
			param.put("templateId", templateId);
			param.put("frameId", frameId);			
			param.put("contentsId", contentsId);

			ScheduleDAOImpl dao = (ScheduleDAOImpl)getCustomDAO(ScheduleDAOImpl.class.getName());

			result = dao.getPromotionValueList(param);

			JSONObject json = JSONObject.fromObject(result);
			
			PrintWriter pOut = response.getWriter();
			pOut.println(json.toString());
			pOut.flush();
			
		
		}catch(Exception e){
			//log.error(e);
			e.printStackTrace();
			request.setAttribute("javax.servlet.jsp.jspException", e);
			return mapping.findForward("error");
		}	    	
		
	    	return null;
	}
	

	/**
	 * client pc 에 content 정보가 있는지 확인 후 호출해야 할  url 을 생성하여 반환한다.
	 * 2007.12.20 사용하지 않음
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
/*	
	private String getLocalContentsInfo(HttpServletRequest request, UtvContents contents) throws Exception {
		
		String clientIp = request.getRemoteAddr();
		String contentsServerIp = this.getContextProperty("CONTENTS_SERVER_IP");
		
		String filePath = "";

		filePath += contents.getLocation();
		
		filePath += contents.getFileName();

		String protocol = "http://" ;
		String result = protocol; // 결과 url		
		
		GetMethod method = null;
		int fileSize= 0;
		
		try{

			HttpClient client = new HttpClient();
			
			log.debug("\n contentsId>>>" + contents.getContentsId());

			method = new GetMethod(protocol + clientIp + filePath);

			method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, true));
			method.getParams().setSoTimeout(300);
			
			int statusCode = client.executeMethod(method);
			
			Header[] headers = method.getResponseHeaders();
			
			log.debug("\n " +  contents.getContentsId() + ">>> statusCode>>>" + statusCode);
			log.debug("\n " +  contents.getContentsId() + ">>> Status Text>>>" + HttpStatus.getStatusText(statusCode));
			log.debug("\n " +  contents.getContentsId() + ">>> Method status: " + method.getStatusLine());
		
			try{
				fileSize = Integer.parseInt( method.getResponseHeader("Content-Length").getValue() );
			}catch(NumberFormatException e){
				//
				fileSize = 0;
			}
			
			if (statusCode == HttpStatus.SC_OK && fileSize > 1000) {
				result += clientIp + filePath;
			}else{
				result += contentsServerIp + filePath;
			}
			
		}catch(Exception e){ 
			// client pc 에 web server 가 기동이 안된 경우 또는 기타 이유로 접근 불가인 경우
			// 무조건 contents server 로 접속한다.
			log.error(e);
			result += contentsServerIp + filePath;
		}finally{
			// releaseConnection 을 호출하면 시간이 오래 걸리므로 생략한다.
			//method.releaseConnection(); 
			
		}
		
		log.debug("\n " +  contents.getContentsId() + ">>>contents.getFileName()>>" + contents.getFileName());
		log.debug("\n " +  contents.getContentsId() + ">>>final !! contentsServerURI>>>" + result);		

		return result;
	}	
*/
}
