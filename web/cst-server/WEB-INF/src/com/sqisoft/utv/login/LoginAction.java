
package com.sqisoft.utv.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sqisoft.utv.core.action.BaseAction;
import com.sqisoft.utv.dto.UtvHost;

/**
 *
 *
 *
 * @author yngwie@sqisoft.com
 *
 */
public final class LoginAction extends BaseAction {

	/**
	 * 1. UTV_BRW 가 처음 기동시 Site & Host 정보를 받아서  인증 처리
	 * 2. 인증 확인 후 스케줄 조회 메소드를 호출한다.
	 *
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String target = "success";

		// UTV_BRW 에서 UTV_CST 를 처음 호출시 넘겨줘야 하는 파라미터
        String siteId = request.getParameter("siteId"); // UTV_Site.siteId
        String hostId = request.getParameter("hostId"); // UTV_Host.hostId

        String localIp = request.getRemoteAddr(); // client pc ip 를 받아온다.
        
        // additional parameters
        String date = request.getParameter("date");
        String time = request.getParameter("time");

        log.debug("\nsiteId=" + siteId);
        log.debug("\nhostId=" + hostId);
        log.debug("\nclient ip=" + localIp);

        log.debug("\ndate=" + date);
        log.debug("\ntime=" + time);

        String reqPath = request.getRequestURI();
        String method = request.getMethod();
        String queryStr = request.getQueryString();
        String referer = request.getHeader("referer");
        
        String serverUrl = request.getServerName() + request.getContextPath();

        String url = request.getRequestURL().toString();

		log.debug("requestURL=" + url);
		log.debug("reqPath=" + reqPath);
		log.debug("queryString=" + queryStr);
		log.debug("method=" + method);
		log.debug("referer=" + referer);
		log.debug("serverUrl=" + serverUrl);

		try{

			LoginDAOImpl dao = (LoginDAOImpl)getCustomDAO(LoginDAOImpl.class.getName());
			int result = dao.loginCheck(siteId, hostId);

			if(result == 1) { // siteId & hostId 존재

				UtvHost host = new UtvHost();
				host.setSiteId(siteId);
				host.setHostId(hostId);
				host.setLocalIp(localIp);

				setSessionObject(request, "host", host, true);
				
				request.setAttribute("siteId", siteId);
				request.setAttribute("hostId", hostId);
				request.setAttribute("localIp", localIp);
				request.setAttribute("serverUrl", serverUrl);
				
				request.setAttribute("FLV_SERVER_URL", getContextProperty("FLV_SERVER_URL"));
				request.setAttribute("FLV_LOCAL_URL", getContextProperty("FLV_LOCAL_URL"));
				request.setAttribute("FLV_SERVER_PROTOCOL", getContextProperty("FLV_SERVER_PROTOCOL"));
				request.setAttribute("FLV_LOCAL_PROTOCOL", getContextProperty("FLV_LOCAL_PROTOCOL"));
				request.setAttribute("IMG_SERVER_URL", getContextProperty("IMG_SERVER_URL"));
				request.setAttribute("IMG_LOCAL_URL", getContextProperty("IMG_LOCAL_URL"));
				
				
				if(date != null && date.length() == 8){
					request.setAttribute("date", date);
					request.setAttribute("time", time);
				}

			}else{ // siteId or hostid 미존재
				// 아직은 별도 처리 하지 않음	
				target = "failure";

			}

//			log.debug("target=" + target);

		}catch(Exception e){
			log.error(e);
			request.setAttribute("javax.servlet.jsp.jspException", e);
			return mapping.findForward("error");
		}

        return mapping.findForward(target);
	}
}

