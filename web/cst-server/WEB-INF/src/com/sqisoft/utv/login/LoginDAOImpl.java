
package com.sqisoft.utv.login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.sqisoft.utv.core.common.Constants;
import com.sqisoft.utv.core.exception.JDBCException;
import com.sqisoft.utv.core.jdbc.ComponentDAO;
import com.sqisoft.utv.core.jdbc.LoggableStatement;
import com.sqisoft.utv.core.sql.SQLFactory;
import com.sqisoft.utv.core.sql.SQLParser;

public final class LoginDAOImpl extends ComponentDAO {

	public int loginCheck(String siteId, String hostId) throws JDBCException {
		int result = -1;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try{
			SQLParser parser = SQLFactory.getParser("sql_utv");
			String query = parser.getQuery("LoginAction", "execute", "1");

			conn = getConnection(Constants.DB_ACCESS_KEY_DEFAULT);
			pstmt = new LoggableStatement(conn, query);
			pstmt.setString(1, siteId);
			pstmt.setString(2, hostId);

			log.debug( ((LoggableStatement)pstmt).getQueryString() );

			rset = pstmt.executeQuery();

			if(rset.next()){
				result = rset.getInt(1);
			}

			log.debug("result=" + result);

		}catch(Exception e){
			log.error(e);
			e.printStackTrace();
			throw new JDBCException(e.getMessage());
		}finally{
			close(conn, pstmt, rset);
		}

		return result;
	}
}
