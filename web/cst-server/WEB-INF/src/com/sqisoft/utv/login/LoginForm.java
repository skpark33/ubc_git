package com.sqisoft.utv.login;

import javax.servlet.http.HttpServletRequest;

//import org.apache.struts.action.ActionError;
//import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public final class LoginForm extends ActionForm {
	
	private static final long serialVersionUID = -2682116604039758876L;
	
	private String siteId;
	private String hostId;

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.siteId = null;
		this.hostId = null;

	}	
/*
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if ((siteId == null) || (siteId.length() < 1))  errors.add("siteId", new ActionError("error.siteId.required"));
		return errors;
	}
*/	
	
}
