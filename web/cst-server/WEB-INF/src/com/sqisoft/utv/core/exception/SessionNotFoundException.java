package com.sqisoft.utv.core.exception;

/**
  *
  *
  * @author yngwie yngwie@sqisoft.com
  * @version 1.0
  * @see
  *
  */
public class SessionNotFoundException extends BaseException {

	public SessionNotFoundException(){
		super();
	}

	public SessionNotFoundException(String msg){
		super(msg);
	}

	public SessionNotFoundException(Throwable cause){
		super(cause);
	}

}
