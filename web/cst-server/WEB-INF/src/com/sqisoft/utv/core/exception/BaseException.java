package com.sqisoft.utv.core.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
  *
  * @version 1.0
  * @author yngwie yngwie@wincc.co.kr
  * @see
  *
  */
public abstract class BaseException extends Exception {

	protected Throwable rootCause = null;
	private List exceptions = new ArrayList();
	private Object[] messageArgs = null;
	private String messageKey = null;
	/**
	 *
	 */
	public BaseException() {
		super();
	}

	/**
	 * @param message
	 */
	public BaseException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
//	public BaseException(String message, Throwable cause) {
//		super(message, cause);
//	}

	public BaseException(Throwable cause) {
		this.rootCause = cause;
	}

	public void printStackTrace(PrintWriter writer) {
		super.printStackTrace(writer);
		if(getRootCause() != null) {
			getRootCause().printStackTrace(writer);
		}
		writer.flush();
	}

	public void printStackTrace(PrintStream outStream) {
		printStackTrace(new PrintWriter(outStream));
	}

	public void printStackTrace() {
		printStackTrace(System.err);
	}

	public Throwable getRootCause() {
		return rootCause;
	}

	public List getExceptions() {
		return exceptions;
	}

	public Object[] getMessageArgs() {
		return messageArgs;
	}

	public BaseException(String msgKey, Object[] args) {
		this.messageKey = msgKey;
		this.messageArgs = args;
	}

	public void setRootCause(Throwable anException) {
		rootCause = anException;
	}

	public void addException(BaseException ex) {
		exceptions.add(ex);
	}

	public void setMessageKey(String key) {
		this.messageKey = key;
	}

	public String getMessageKey() {
		return messageKey;
	}

	public void setMessageArgs(Object[] args) {
		this.messageArgs = args;
	}
}
