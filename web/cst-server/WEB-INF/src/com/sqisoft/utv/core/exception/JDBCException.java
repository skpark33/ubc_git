
package com.sqisoft.utv.core.exception;

/**
  *
  * @version 1.0
  * @author yngwie yngwie@sqisoft.com
  * @see
  *
  */
public class JDBCException extends BaseException {
	public JDBCException(){
		super();
	}

	public JDBCException(String msg){
		super(msg);
	}

	public JDBCException(Throwable cause){
		super(cause);
	}
}