
package com.sqisoft.utv.core.jdbc;

import java.io.Serializable;

/**
 *
 * @version 1.0
 * @author yngwie yngwie@wincc.co.kr
 * @see
 *
 */
public final class Database implements Serializable {

	private String name;
	private String jndiName;

	public Database() {
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setJndiName(String jndiName) {
		this.jndiName = jndiName;
	}

	public String getJndiName() {
		return this.jndiName;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer("Database [name=");
		sb.append(name);
		if (jndiName != null) {
			sb.append(", jndiName=");
			sb.append(jndiName);
		}
		sb.append("]");
		return (sb.toString());
	}

}
