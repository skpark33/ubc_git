package com.sqisoft.utv.core.resources;

import java.io.BufferedInputStream;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.digester.Digester;
import org.apache.log4j.Logger;

import com.sqisoft.utv.core.common.Constants;
import com.sqisoft.utv.core.log.LogUtil;

public class ResourcesServlet extends HttpServlet {

	private String pathName = "/WEB-INF/conf/rerouces.xml";
	private int debug = 0;
	
	private Logger log = LogUtil.getLogger(Constants.LOGGER_TYPE);

	public void init() throws ServletException {

		String value = null;
		value = getServletConfig().getInitParameter("debug");
		try {
			debug = Integer.parseInt(value);
		} catch (Throwable t) {
			debug = 0;
		}
		if (debug >= 1) log.info("Initializing resource servlet");

		value = getServletConfig().getInitParameter("pathName");

		if (value != null)	pathName = value;

		try {
			load();
		} catch (Exception e) {
			log.error("resource load exception", e);
			throw new UnavailableException("Cannot load resource from '" + pathName + "'");
		}

	}

	private synchronized void load() throws Exception {

		// Acquire an input stream to our database file
		if (debug >= 1) log.info("Loading resource from '" + pathName + "'");
		InputStream is = getServletContext().getResourceAsStream(pathName);
		if (is == null) {
			log.info("No such resource available - loading empty resource");
			return;
		}

		BufferedInputStream bis = new BufferedInputStream(is);

		Digester digester = new Digester();
		digester.setValidating(false);
		
		digester.addObjectCreate("resources", "com.sqisoft.utv.core.resources.Resources");
		
		digester.addObjectCreate("resources/property", "com.sqisoft.utv.core.resources.Property");
		digester.addBeanPropertySetter("resources/property/key", "key");
		digester.addBeanPropertySetter("resources/property/value", "value");
		digester.addSetNext("resources/property", "addProperty");

		digester.addObjectCreate("resources/enum", "com.sqisoft.utv.core.resources.Enum");
		digester.addSetProperties("resources/enum", "name", "name");
		
		digester.addObjectCreate("resources/enum/property", "com.sqisoft.utv.core.resources.Property");
		digester.addBeanPropertySetter("resources/enum/property/key", "key");
		digester.addBeanPropertySetter("resources/enum/property/value", "value");		
		digester.addSetNext("resources/enum/property", "addProperty");		
		
		digester.addSetNext("resources/enum", "addEnum");

		Resources resources = (Resources)digester.parse(bis);
		
		log.info(resources);
		
		this.getServletContext().setAttribute("resources", resources);
		
		bis.close();

	}

	public void destroy(){
		this.getServletContext().removeAttribute("resources");
		
		log.info("Finalizing resource servlet");
	}
}