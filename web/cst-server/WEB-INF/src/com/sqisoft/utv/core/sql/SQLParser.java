package com.sqisoft.utv.core.sql;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
//import java.util.StringTokenizer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
  *
  * @version 1.0
  * @author yngwie yngwie@sqisoft.com
  * @see
  *
  */
public class SQLParser {

	private long lastModified;
	private String filePath;
	private String parserName;
	private boolean reload = false;
	private SAXBuilder builder;
	private Document doc;
	private static final String lineSeparator = System.getProperty("line.separator");
	private static final String space = " ";


	public SQLParser(InputStream is, String name){
		setName(name);
		init(is);
	}

	public SQLParser(String path, String name, boolean reload){
		setName(name);
		this.filePath = path + name;
		this.reload = reload;
		init();
	}

	private void setName(String name){
		int idx = name.indexOf(".");
		this.parserName = name.substring(0, idx);
	}

	public String getName(){
		return this.parserName;
	}

	public String getFilePath(){
		return filePath;
	}

	public void setReload(boolean reload){
		this.reload = reload;
	}

	public boolean getReload(){
		return this.reload;
	}

	public long getLastModified(){
		return this.lastModified;
	}

	public boolean isModified(){
		File file = new File(filePath);
		long newModified = file.lastModified();
		if(lastModified != newModified) return true;
		else return false;
	}

	private void init(){
		InputStream is = null;
		builder = new SAXBuilder();
		doc = null;
		try{
			File file = new File(filePath);
			lastModified = file.lastModified();
			is = new BufferedInputStream(new FileInputStream(file));
			doc = builder.build(is);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				is.close();
			}catch(IOException ie){
				ie.printStackTrace();
			}
		}
	}

	private void init(InputStream is){
		builder = new SAXBuilder();
		doc = null;
		try{
			doc = builder.build(is);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private Element getSelectChildElement(Element element, String cl, String mt, String idx){
		Element returnElement = null;
		List list = element.getChildren();
		Iterator i = list.iterator();
		while(i.hasNext()){
			Element ce = (Element)i.next();
			String className = ce.getAttribute("class").getValue();
			String methodName = ce.getAttribute("method").getValue();
			String index = ce.getAttribute("index").getValue();
			if(className.equals(cl) && methodName.equals(mt) && index.equals(idx)){
				returnElement = ce;
				break;
			}
		}
		return returnElement; //null 이 리턴될수 있다
	}

	private Element getSelectChildElement(String cl, String mt, String idx){
		Element root = doc.getRootElement();
		return getSelectChildElement(root, cl, mt, idx);
	}

	public String getQuery(String cl, String mt){
		String query = getQuery(cl, mt, "1");
		return query;
	}


	public String getQuery(String cl, String mt, String idx){
		if(reload){
			if(isModified()) init();
		}
		Element e = getSelectChildElement(cl, mt, idx);
		String query = makeQuery(e);
		return query;
	}


	private String makeQuery(Element e){

		StringBuffer sb = new StringBuffer();

		String sqlCommand = e.getChild("sql-command").getTextTrim();
		String sqlColumn = e.getChild("sql-column").getTextTrim();
		String sqlTable = e.getChild("sql-table").getTextTrim();
		String sqlWhere = e.getChild("sql-where").getTextTrim();
		String sqlOption = e.getChild("sql-option").getTextTrim();

		if(sqlCommand.equalsIgnoreCase("SELECT")){
			sb.append(sqlCommand + space);
			sb.append(sqlColumn + lineSeparator);
			sb.append(" FROM ");
			sb.append(sqlTable + lineSeparator);
			if(sqlWhere.length() > 0) {
				sb.append(" WHERE ");
				sb.append(sqlWhere + lineSeparator);
			}
			if(sqlOption.length() > 0) sb.append(sqlOption + lineSeparator);
		}else if(sqlCommand.equalsIgnoreCase("INSERT")){
			sb.append(sqlCommand + space);
			sb.append(" INTO ");
			sb.append(sqlTable + lineSeparator);
			sb.append("(");
			sb.append(sqlColumn + ")");
			sb.append(lineSeparator);
			sb.append(" VALUES (");
			sb.append(sqlWhere);
			if(sqlOption.length() > 0){
				sb.append(lineSeparator);
				sb.append(sqlOption + lineSeparator);
			}
			sb.append(")");
		}else if(sqlCommand.equalsIgnoreCase("UPDATE")){
			sb.append(sqlCommand + space);
			sb.append(sqlTable + lineSeparator);
			sb.append("SET ");
			sb.append(sqlColumn + space);
			sb.append(" WHERE ");
			sb.append(sqlWhere);
			sb.append(lineSeparator);
			if(sqlOption.length() > 0){
				sb.append(lineSeparator);
				sb.append(sqlOption + lineSeparator);
			}
		}else if(sqlCommand.equalsIgnoreCase("DELETE")){
			sb.append(sqlCommand + space);
			sb.append(" FROM ");
			sb.append(sqlTable + lineSeparator);
			sb.append(" WHERE ");
			sb.append(sqlWhere);
			sb.append(lineSeparator);

		}else{ // 이도 저도 아니면 몽땅 더한다.
			if(sqlCommand.length() > 1) sb.append(sqlCommand + lineSeparator);
			if(sqlColumn.length() > 1) sb.append(sqlColumn + lineSeparator);
			if(sqlTable.length() > 1) sb.append(sqlTable + lineSeparator);
			if(sqlWhere.length() > 1) sb.append(sqlWhere + lineSeparator);
			if(sqlOption.length() > 1) sb.append(sqlOption + lineSeparator);
		}

		return sb.toString();
	}

}