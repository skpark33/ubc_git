
package com.sqisoft.utv.core.jdbc;


/**
  *
  * @version 1.1
  * @author yngwie yngwie@wincc.co.kr
  * @see
  *
  */
public abstract class DAOFactory {

	public static final int ORACLE_DAO = 1;
	public static final int MYSQL_DAO = 2;
	public static final int SYBASE_DAO = 3;
	public static final int DB2_DAO = 4;
	public static final int MSSQL_DAO = 5;

	public abstract ComponentDAO getComponentDAO(String className);


	public static DAOFactory getDAOFactory(int which) {
		switch(which){
			case ORACLE_DAO : return new OracleDAOFactory();
			case MYSQL_DAO : return new MySqlDAOFactory();
			case MSSQL_DAO : return new MsSqlDAOFactory();
			default : return null;
		}

	}

	public abstract int getClassListSize();

	public abstract String[] getClassList();
}
