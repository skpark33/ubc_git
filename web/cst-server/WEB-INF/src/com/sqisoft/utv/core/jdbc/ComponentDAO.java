package com.sqisoft.utv.core.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.sqisoft.utv.core.common.Constants;
import com.sqisoft.utv.core.jdbc.DataSourceFactory;
import com.sqisoft.utv.core.log.LogUtil;

/**
 * 
 * 
 * 2005.06.13 yngwie mode 멤버변수에서 삭제 
 * 
 * @version 1.0
 * @author yngwie yngwie@wincc.co.kr
 * @see
 * 
 */
public abstract class ComponentDAO {
	
	protected Logger log = LogUtil.getLogger(Constants.LOGGER_TYPE);
	//protected boolean mode = true;
	
	/**
	 * Connection 의 commit mode 를 셋팅한다..
	 * @param mode
	 */
/*	
	public void setMode(boolean mode){
		this.mode = mode;
	}
*/	
	/**
	 * Connection 의 commit mode 를 반환한다. 
	 * @return mode Connection 의  현재 설정된 commit mode 
	 */
/*	
	public boolean getMode(){
		return this.mode;
	}
*/	
	/**
	 * 해당 ResultSet 의 fetch size 를 설정한다.
	 * fetch size 는 일종의 cache 로 한번에 database에서 가져올 row 의 건수이다.
	 * @param rset
	 * @param rows
	 * @throws SQLException
	 */
	public void setFetchSize(ResultSet rset, int rows) throws SQLException{
		rset.setFetchSize(rows);
	}
	
	/**
	*	해당 컨테이너로부터 DataSource를 얻어내어 Connection객체를 생성후 반환한다.
	*
	*	@param dbName lookup할 database jndiname
	*	@return java.sql.Connection
	*/
	public Connection getConnection(String dbName) throws SQLException{
		Connection conn = null;
		DataSource ds = DataSourceFactory.getDataSource(dbName);
		if ( ds == null ) throw new SQLException("Can't find the DataSource object!");
		conn = ds.getConnection();
		return conn;
	}
	
	/**
	 * dbName 을 가지고 해당 jndi 를 이용해 Connection 객체를 반환한다.
	 * mode 가  false 일 경우 autoCommit mode 를 false 로 설정한다.
	 * @param dbName
	 * @param mode
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection(String dbName, boolean mode) throws SQLException{
		Connection conn = null;
		DataSource ds = DataSourceFactory.getDataSource(dbName);
		if ( ds == null ) throw new SQLException("Can't find the DataSource object!");
		conn = ds.getConnection();
		//this.mode = mode;
		if(!mode) {
			conn.setAutoCommit(mode);
		} 
		return conn;
	}

	/**
	*	해당 컨테이너로부터 default DataSource를 얻어내어 default로 설정된 Connection객체를 생성후 반환한다.
	*
	*	@return java.sql.Connection
	*/
	public Connection getConnection() throws SQLException	{
		return getConnection(Constants.DB_ACCESS_KEY_DEFAULT);
	}

	/**
	*	java.sql.Connection을 commit한다.
	*	
	*	@param conn Connection
	*/
	public void commit(Connection conn){
		if(conn != null){
			try {
				conn.commit();
			} catch (SQLException e) {
			}
		}
	}
	
	/**
	*	java.sql.Connection을 close한다.
	*  mode 가 false 일 경우 AutoCommit mode 를 true 로 설정한다.
	*	
	*	@param conn Connection
	*/
	public void close(Connection conn){
		if(conn != null){
			try{
			    boolean mode = conn.getAutoCommit();
				if(!mode) {
					mode = true;
					conn.setAutoCommit(mode);
				} 
				conn.close();
			}catch(Exception e){
			}
		}
	}
	
	/**
	*	java.sql.Statement를 close한다.
	*	
	*	@param stmt  Statement
	*/
	public void close(Statement stmt){
		if(stmt != null){
			try{
				stmt.close();
			}catch(Exception e){
			}
		}
	}

	/**
	*	java.sql.ResultSet을 close한다.
	*	
	*	@param rset  ResultSet
	*/
	public void close(ResultSet rset){
		if(rset != null){
			try{
				rset.close();
			}catch(Exception e){
			}
		}
	}
	
	public void close(Statement stmt, ResultSet rset) {
		close(rset);
		close(stmt);
	}	

	public void close(Connection conn, Statement stmt, ResultSet rset) {
		close(rset);
		close(stmt);
		close(conn);
	}
	
	public String getString(Map map, String key) {
		
		return map.get(key).toString();
	}	
	
	public int getInt(Map map, String key) {
		
		return Integer.parseInt(map.get(key).toString());
	}		
}
