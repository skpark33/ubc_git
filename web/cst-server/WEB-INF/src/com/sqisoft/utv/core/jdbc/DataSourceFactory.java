
package com.sqisoft.utv.core.jdbc;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.sqisoft.utv.core.common.Constants;

/**
 *
 * @version 1.0
 * @author yngwie yngwie@sqisoft.com
 * @see
 *
 */
public class DataSourceFactory {
	private static Map dataSource = new HashMap();

	public static void init(List databases) {
		try{
			Context ctx = new InitialContext();
			Iterator iter = databases.iterator();
			while( iter.hasNext() ) {
				 Database db = (Database) iter.next();
				 DataSource ds = (DataSource) ctx.lookup(db.getJndiName());
				 System.out.println("-=-=-=-=-=-=-=-=-= DataSource Object succeed [ " + db.getName() + " ]");
				 dataSource.put(db.getName(), ds);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static DataSource getDataSource() {
		return getDataSource(Constants.DB_ACCESS_KEY_DEFAULT);
	}

	public static DataSource getDataSource(String key) {
		if( dataSource.containsKey(key) ) {
			return (DataSource) dataSource.get(key);
		}
		return null;
	}
}
