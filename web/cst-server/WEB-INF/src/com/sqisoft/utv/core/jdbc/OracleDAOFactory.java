
package com.sqisoft.utv.core.jdbc;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.sqisoft.utv.core.log.LogUtil;



/**
  *
  * @version 1.0
  * @author yngwie yngwie@sqisoft.com
  * @see
  *
  */
public class OracleDAOFactory extends DAOFactory {
	/**
	*	현재 생성되어져 있는 classList객체
	*/
	private static Hashtable classList = new Hashtable();
	private Logger logger = LogUtil.getLogger(this.getClass());

	/**
	*	클래스이름을 입력받아 해당 객체를 먼저 메모리에서 찾은 후 없으면
	*	객체를 생성한다.
	*
	*	@param className 생성한 클래스명 FQCN
	*	@return 생성된 ComponentDAO객체
	*/
	public ComponentDAO getComponentDAO(String className) {
		Class cl = null;
		Object obj = null;

		if( classList == null) classList = new Hashtable();
		if( classList.containsKey( className) ) {
			return (ComponentDAO)classList.get(className);
		}
		try{
			cl = Class.forName(className);
			obj = cl.newInstance();
		}catch(Exception e) {
			System.out.println("Instanciate faild at OracleDAO Factory : " + e);
			logger.error(e);
		}
		classList.put(className, obj);
		return (ComponentDAO) obj;
	}

	public int getClassListSize(){
		return classList.size();
	}

	public String[] getClassList(){
		String[] strs = null;
		if( classList != null && !classList.isEmpty() ){
			Object[] objs = classList.keySet().toArray();
			strs = new String[objs.length];
			for(int i = 0; i < objs.length; i++){
				strs[i] = (String)objs[i];
			}
		}
		return strs;
	}

}
