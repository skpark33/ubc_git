package com.sqisoft.utv.core.log;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

 /**
  *
  * @version 1.0
  * @author yngwie yngwie@sqisoft.com
  * @see
  *
  */

public class LogUtil {

	public static void init(String resource){
		DOMConfigurator.configure(resource);
	}

	public static Logger getLogger(Class c){
		return Logger.getLogger(c);
	}

	/**
	 * 2003. 08.04 Ãß°¡
	 * properties ¿¡ Á¤ÀÇÇÑ category name ¿¡ ÇØ´çÇÏ´Â Logger instace ¸¦ ¹ÝÈ¯ÇÑ´Ù.
	 * @param source
	 * @return
	 */
	public static Logger getLogger(String source){
		return Logger.getLogger(source);
	}
}

