package com.sqisoft.utv.core.sql;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.sqisoft.utv.core.common.Constants;
import com.sqisoft.utv.core.log.LogUtil;

/**
  *
  * @version 1.0
  * @author yngwie yngwie@sqisoft.com
  * @see
  *
  */
public class SQLFactory {
	private static Hashtable sqlParsers = new Hashtable();
	private static Logger log = LogUtil.getLogger(SQLFactory.class);

	public static void init(){
		if(sqlParsers == null) sqlParsers = new Hashtable();
		else sqlParsers.clear();
	}
	public static SQLParser getParser(){
		return (SQLParser)sqlParsers.get(Constants.SQLPARSER_DEFAULT);
	}

	public static SQLParser getParser(String key){
		if(sqlParsers.containsKey(key)){
			return (SQLParser)sqlParsers.get(key);
		}
		return null;
	}

	public static void addSqlParser(String key, SQLParser parser){
		if(sqlParsers.containsKey(key)){
			sqlParsers.remove(key);
			System.out.println("-=-=-=-=-=-=-=-=-= exist SQL file removed [ " + parser.getName() + " ]" );
		}
		sqlParsers.put(key, parser);
	}

}
