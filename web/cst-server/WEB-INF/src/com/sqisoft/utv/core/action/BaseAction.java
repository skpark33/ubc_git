package com.sqisoft.utv.core.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;

import com.sqisoft.utv.core.common.Constants;
import com.sqisoft.utv.core.exception.SessionNotFoundException;
import com.sqisoft.utv.core.jdbc.ComponentDAO;
import com.sqisoft.utv.core.jdbc.DAOFactory;
import com.sqisoft.utv.core.log.LogUtil;
import com.sqisoft.utv.core.resources.Resources;


/**
 * 각 기능별로 구현할 모든 Action 클래스들의 base Class
 * 해당 Action class 를 구현할때는 반드시 이 클래스를 상속받아 사용한다.
 *
 * @author yngwie@wincc.co.kr
 *
 */
public abstract class BaseAction extends Action {

	protected Logger log = LogUtil.getLogger(Constants.LOGGER_TYPE);

	// oracle 을 기본 dbms 로 설정한다.
	protected static int DAO_NAME = DAOFactory.MSSQL_DAO;

	protected ComponentDAO getCustomDAO(String className){
		DAOFactory factory = DAOFactory.getDAOFactory(DAO_NAME);
		return factory.getComponentDAO(className);
	}

	protected void printDAOFactoryInfo(){
		DAOFactory factory = DAOFactory.getDAOFactory(DAO_NAME);
		StringBuffer output = new StringBuffer();
		output.append("\r\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
		output.append("\r\n == DAO pool Size is " + factory.getClassListSize() + " ==");
		output.append("\r\n ===== Registered DAO Class List =====");

		String[] names = factory.getClassList();
		for(int i = 0; i < names.length; i++){
			output.append("\r\n" + names[i]);
		}
		output.append("\r\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
		log.info(output.toString());
	}

	protected Object getSessionObject(HttpServletRequest request, String attrName) {
		Object sessionObj = null;
		HttpSession session = request.getSession(false);
		if ( session != null ){
			sessionObj = session.getAttribute(attrName);
		}
		return sessionObj;
	}

	protected Object getApplicationObject(String attrName) {
		return servlet.getServletContext().getAttribute(attrName);
	}

	/**
	 *
	 *
	 * @param request
	 * @param objName
	 * @param obj
	 * @param ignored session 에 해당 object 가 존재할경우 무시하고 set 할것인지의 여부
	 * @throws SessionNotFoundException
	 */
	protected void setSessionObject(HttpServletRequest request, String objName, Object obj,  boolean ignored) throws SessionNotFoundException{
		HttpSession session = request.getSession(false);

		if(session != null) {
			Object sessionObj = session.getAttribute(objName);
			if(ignored) {
				session.setAttribute(objName, obj);
			}else{
				if(sessionObj != null)
					throw new SessionNotFoundException("request session already exist!");
				else session.setAttribute(objName, obj);
			}
		}else{
			throw new SessionNotFoundException("request session not found!");
		}
	}

	protected void removeSessionObject(HttpServletRequest request, String objName) throws SessionNotFoundException {
		removeSessionObject(request, objName, true);
	}

	/**
	 * session 에 저장된 object 중 objName 에 해당하는 object를 찾아 삭제한다.
	 * ignored 가 true 로 되어 있을 경우 objName 에 해당하는 object 가 없으면 무시한다.
	 * ignored 가 false 로 되어 있을 경우 objName 에 해당하는 object 가 없으면 exception 을 던진다.
	 *
	 * @param request
	 * @param objName
	 * @param ignored
	 * @throws SessionNotFoundException
	 */
	protected void removeSessionObject(HttpServletRequest request, String objName, boolean ignored)  throws SessionNotFoundException {
		HttpSession session = request.getSession(false);

		if(session != null) {
			if(session.getAttribute(objName) != null) session.removeAttribute(objName);
		}else{
			if(!ignored) throw new SessionNotFoundException("request session not found!");
		}
	}

	protected void beforeHandWork(HttpServletRequest request){

	}
	
	private Object getContextAttribute(String name){
		return this.getServlet().getServletContext().getAttribute(name);
	}
	
	protected String getContextProperty(String key) {
		
		Resources r = (Resources)getContextAttribute("resources");
		
		return r.getProperty(key);
	}

	protected String getContextEnum(String name, String key) {
		
		Resources r = (Resources)getContextAttribute("resources");
		
		return r.getEnum(name, key);
	}	

}
