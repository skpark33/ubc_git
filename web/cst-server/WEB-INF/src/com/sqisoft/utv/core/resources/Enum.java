package com.sqisoft.utv.core.resources;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Enum implements Serializable {
	
	private String name;
	
	private Vector properties;
	
	public Enum(){
		properties = new Vector();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addProperty(Property prop){
		properties.addElement(prop);
	}
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}	
	
	public String getProperty(String key) {
		String result = null;
		
		Iterator iter = properties.iterator();
		while(iter.hasNext()) {
			Property p = (Property)iter.next();
			if(key.equals(p.getKey())) {
				result = p.getValue();
				break;
			}
		}
		
		return result;
	}

}
