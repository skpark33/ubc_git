package com.sqisoft.utv.core.common;

/**
  *
  * @version 1.0
  * @author yngwie yngwie@wincc.co.kr
  * @see
  *
  */
public interface Constants {

	String FORWARD_OK = "ok";
	String FORWARD_CANCEL = "cancel";
	String FORWARD_SYNCHRO_ERROR = "synchroError";

	String REQUEST_FORWARD_KEY = "req_forward";
	String REQUEST_FORWARD_ADD = "req_add";
	String REQUEST_FORWARD_REMOVE = "req_remove";
	String REQUEST_FORWARD_MODIFY = "req_modify";

	String LINE_SEPARATOR = System.getProperty("line.separator"); // \n
	String FILE_SEPARATOR = System.getProperty("file.separator");  // \ or /
	String PATH_SEPARATOR = System.getProperty("path.separator"); // ; or :

    String LOGGER_TYPE= "file_console";

    String DB_ACCESS_KEY_DEFAULT = "utv.mssql";
    String SQLPARSER_DEFAULT = "sqllist";
 /*   
    String CONTENTS_FLV_PATH = "/utv_flv/";
    String CONTENTS_IMG_PATH = "/utv_img/";
    
    String CONTENTS_SERVER_IP = "211.232.57.172";
    
    int CONTENTS_TYPE_VIDEO = 0;
    int CONTENTS_TYPE_SMS = 1;
    int CONTENTS_TYPE_IMAGE = 2;
    int CONTENTS_TYPE_CAMPAIGNE = 3;
    int CONTENTS_TYPE_TV = 4;
    int CONTENTS_TYPE_TICKER = 5;
    int CONTENTS_TYPE_ETC =99;
*/
}
