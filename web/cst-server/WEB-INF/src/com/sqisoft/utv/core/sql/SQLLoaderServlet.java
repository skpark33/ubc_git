package com.sqisoft.utv.core.sql;

import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;



/**
  *
  * @version 1.0
  * @author yngwie yngwie@sqisoft.com
  * @see
  *
  */
public class SQLLoaderServlet extends HttpServlet {

	private String filePath = "/WEB-INF/conf/";
	private int debug = 0;

	public void init() throws ServletException{
		String value = null;
		value = getServletConfig().getInitParameter("debug");
		try {
			debug = Integer.parseInt(value);
		} catch (Throwable t) {
			debug = 0;
		}
		if (debug >= 1) log("Initializing SQLLoaderServlet");

		try {
			load();
		} catch (Exception e) {
			log("SQLLoaderServlet load exception", e);
			throw new UnavailableException("Cannot load sqlfile from '" + filePath  + "'");
		}
	}
	/*
	 * 하나 이상의 sql...xml 파일을 로드하여 SQLFactory 에 보관한다.
	 * factory 에서 해당 sqlParser 를 가져올때 사용하는 key 는 경로명과 확장자를 제외한 파일명이다.
	 *
	 */
	private synchronized void load() throws Exception{
		String prefix = getServletContext().getRealPath("/");

		if (debug >= 1) log("ServletContext Root Path [" + prefix + " ]");

		String value = getServletConfig().getInitParameter("fileName");
		boolean reload = Boolean.valueOf(getServletConfig().getInitParameter("reload")).booleanValue();
		StringTokenizer stk = new StringTokenizer(value, ",");
		int count = stk.countTokens();

		SQLFactory.init(); // 초기화

		while(stk.hasMoreTokens()){
			String fileName = stk.nextToken().trim();
			/*
			InputStream is = getServletContext().getResourceAsStream(filePath + fileName);
			if (is == null) {
				log("No such resource available - loading fail SQLLoader");
				return;
			}

			BufferedInputStream bis = new BufferedInputStream(is);

			SQLParser parser = new SQLParser(bis, fileName);
			SQLFactory.addSqlParser(parser.getName(), parser);
			if (debug >= 1) log("-=-=-=-=-=-=-=-=-= SQL file Load succeed [ " + parser.getName() + " ]" );
			bis.close();
			*/
			SQLParser parser = new SQLParser(prefix + filePath , fileName, reload);
			SQLFactory.addSqlParser(parser.getName(), parser);

			//if (debug >= 1) log("-=-=-=-=-=-=-=-=-= SQL file Load succeed [ " + parser.getName() + " ]" );
			System.out.println("-=-=-=-=-=-=-=-=-= SQL file Load succeed [ " + parser.getName() + " ]" );
		}
	}

	public void destroy(){
		log("Finalizing SQLParser servlet");
	}
}
