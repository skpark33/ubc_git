package com.sqisoft.utv.core.resources;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Resources implements Serializable {
	
	private Vector properties;
	private Vector enums;
	
	public Resources(){
		
		properties = new Vector();
		enums = new Vector();
	}
	
	public void addEnum(Enum e){
		enums.addElement(e);
	}
	
	public void addProperty(Property p){
		properties.addElement(p);
	}
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
	public String getProperty(String key) {
		String result = null;
		
		Iterator iter = properties.iterator();
		while(iter.hasNext()) {
			Property p = (Property)iter.next();
			if(key.equals(p.getKey())) {
				result = p.getValue();
				break;
			}
		}
		
		return result;
	}
	
	public String getEnum(String name, String key){
		String result = null;
		
		Iterator iter = enums.iterator();
		while(iter.hasNext()) {
			Enum e = (Enum)iter.next();
			
			if(name.equals(e.getName())) {
				result = e.getProperty(key);
				break;
			}
		}
		
		return result;
	}

}



