
package com.sqisoft.utv.core.jdbc;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.digester.Digester;
import org.apache.log4j.Logger;

import com.sqisoft.utv.core.common.Constants;
import com.sqisoft.utv.core.log.LogUtil;

/**
 *
 * @version 1.0
 * @author yngwie yngwie@sqisoft.com
 * @see
 *
 */
public class DatabaseServlet extends HttpServlet {

	private List database = null;
	private String pathName = "/WEB-INF/conf/database.xml";
	private int debug = 0;
	
	private Logger log = LogUtil.getLogger(Constants.LOGGER_TYPE);

	public void init() throws ServletException {

		// Process our servlet initialization parameters
		String value = null;
		value = getServletConfig().getInitParameter("debug");
		try {
			debug = Integer.parseInt(value);
		} catch (Throwable t) {
			debug = 0;
		}
		if (debug >= 1) log.info("Initializing database servlet");

		value = getServletConfig().getInitParameter("pathName");

		if (value != null)	pathName = value;

		// Load our database from persistent storage
		try {
			load();
			// DataSourceFactory에 해당 데이터베이스를 로드하도록 한다.
			DataSourceFactory.init(database);
		} catch (Exception e) {
			log.error("Database load exception", e);
			throw new UnavailableException("Cannot load database from '" + pathName + "'");
		}

	}

	private synchronized void load() throws Exception {

		// Initialize our database
		database = new ArrayList();

		// Acquire an input stream to our database file
		if (debug >= 1) log.info("Loading database from '" + pathName + "'");
		InputStream is = getServletContext().getResourceAsStream(pathName);
		if (is == null) {
			log.info("No such resource available - loading empty database");
			return;
		}

		BufferedInputStream bis = new BufferedInputStream(is);

		// Construct a digester to use for parsing
		Digester digester = new Digester();
		digester.push(this);
		//digester.setDebug(debug); //deprecate method
		digester.setValidating(false);
		// 데이터베이스를 파싱하여 저장할 객체의 생성
		digester.addObjectCreate("databases/database", "com.sqisoft.utv.core.jdbc.Database");
		// 파싱된 내용을 이용하여 프로퍼티를 세팅한다.
		digester.addSetProperties("databases/database");
		// 현재 클래스의 addDatabase메소드를 호출한다.
		digester.addSetNext("databases/database", "addDatabase");
		// Parse the input stream to initialize our database
		digester.parse(bis);
		
		bis.close();

	}

	public void addDatabase(Database db) {
		database.add(db);
	}

	public void destroy(){
		log.info("Finalizing database servlet");
	}
}
