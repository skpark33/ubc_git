package com.sqisoft.utv.core.log;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

import com.sqisoft.utv.core.common.Constants;

/**
 *
 * @version 1.0
 * @author yngwie yngwie@sqisoft.com
 * @see
 *
 */
public class LogServlet extends HttpServlet {

	private String pathName = "/WEB-INF/conf/daily_fileConsole.xml";

	public void init() throws ServletException {

		String value = null;

		value = getServletContext().getInitParameter("pathName");

		if(value != null) pathName = value;
		String prefix = getServletContext().getRealPath("/");

		try{
			LogUtil.init(prefix + pathName);
			Logger logger = LogUtil.getLogger(Constants.LOGGER_TYPE);
			logger.info("LogServlet initialized");
			logger.info("init file [ " + pathName + " ]");

		}catch(Exception e){
			System.out.println("Log4J load exception======================");
			e.printStackTrace();
			throw new UnavailableException("Cannot load log4j from '" + pathName + "'");
		}
	}
}
