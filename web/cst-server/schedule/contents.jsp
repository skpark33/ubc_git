<%--
  - 작성자: yngwie@sqisoft.com
  - 일자: 2007.08.14
  - 저작권 표시:
  - @(#)
  - 설명: content 표시 화면
  - 수정자:
  - 수정일:
  - 수정사유:
  --%>
<%@page language="java"  contentType="text/html;charset=UTF-8" %>
<%@page import="com.sqisoft.utv.core.resources.Resources" %>
<%@ taglib uri="/tlds/struts-logic" prefix="logic" %>
<%@ taglib uri="/tlds/struts-bean" prefix="bean" %>
<%
	Resources r = (Resources)this.getServletContext().getAttribute("resources");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" contents="text/html; charset=UTF-8" />
<title></title>
<link href="/css/style.css" media="screen" rel="Stylesheet" type="text/css"/>
<style type="text/css">
.tickerText  {
	margin: 0;
	font-size: 38px;
	font-weight: bold;
	color: #FFFF80; 
	height:23;
	filter:shadow(color=#cccccc, strength:2, direction=120);
	padding-left: 5px;
	padding-right: 5px;
	padding-top: 10px;
	vertical-align : middle;
}

div.sms{
    margin: 0;
    margin-bottom: 10px;
    background-color: #CEFFFF;
    border: 5px solid #FF8000;
    padding: 5px 25px 25px 25px;
    font-size : 20px;
    font-weight : bold;
    color : #800080;
}
</style>
<script language="JavaScript" src="/player/swfobject.js"></script>
</head>

<body scroll="no">
<logic:notEmpty name="contents" scope="request">

<script language="JavaScript" type="text/JavaScript">
<!--
	// 소스정보를 info layer 에 추가한다.
	var divObj = parent.document.getElementById("info<bean:write name="frameId"/>Div");
	
	var srcLoc = "<bean:write name="contentsServerURI"/>";
	var html = divObj.innerHTML;	
	html = html + "<br>type:<bean:write name="contents" property="contentsType"/>"
	if(srcLoc.length > 0 ) {
		html = html + "&nbsp;&nbsp;" + srcLoc;
	}
	divObj.innerHTML = html;

//-->
</script>

	<!-- flv -->
	<logic:equal name="contents" property="contentsType" value="<%=r.getEnum("ContentsType", "CONTENTS_TYPE_VIDEO") %>">

		<DIV id="player"></DIV>
		<script type="text/javascript">
			var s1 = new SWFObject("/player/flvplayer.swf","mpl","<bean:write name="contents" property="width"/>","<bean:write name="contents" property="height"/>","7");
			//var s1 = new SWFObject("/player/flvplayer.swf","mpl","300","200","7");
			s1.addParam("allowfullscreen","false");
			s1.addParam("wmode","transparent");
			//s1.addVariable("file","<bean:write name="contentsServerURI"/>");
			s1.addVariable("image","/player/preview.jpg");
			s1.addVariable("backcolor", "0x000000");
			s1.addVariable("autostart", "true");
			s1.addVariable("enablejs", "true");
			s1.addVariable("repeat", "true");
			s1.addVariable("displayheight", "<bean:write name="contents" property="height"/>");
			s1.addVariable("largecontrols ", "false");
			s1.addVariable("showdigits", "false");
			s1.addVariable("showicons", "false");
			s1.addVariable("showvolume", "false");
			s1.addVariable("showeq", "false");
			<logic:notEqual name="contents" property="volume" value="0">	
				s1.addVariable("volume","0");
			</logic:notEqual>
			s1.addVariable("type", "flv");
			//s1.addVariable("streamscript", "http://211.232.57.139/flvprovider.php?file=0101_1_sqisoft_0001.flv&position=100");
			s1.addVariable("file","ftp://sqi:sqicop@211.232.57.193:21/ENC/0201_3_sqisoft_0009.flv");
			s1.write("player");
		</script>
		
	</logic:equal>

	<!-- image -->
	<logic:equal name="contents" property="contentsType" value="<%=r.getEnum("ContentsType", "CONTENTS_TYPE_IMAGE") %>">
		<img src="<bean:write name="contentsServerURI"/>" width="100%" height="100%" border="0">
	</logic:equal>

	<!-- sms -->
	<logic:equal name="contents" property="contentsType" value="<%=r.getEnum("ContentsType", "CONTENTS_TYPE_SMS") %>">
		<div class="sms">
			<bean:write name="contents" property="comment1"/>
		</div>
	</logic:equal>

	<!-- ticker -->
	<logic:equal name="contents" property="contentsType" value="<%=r.getEnum("ContentsType", "CONTENTS_TYPE_TICKER") %>">
		<marquee direction="left" scrollamount="3" scrolldelay="1" class="tickerText">
			<bean:write name="contents" property="comment1"/><bean:write name="contents" property="comment2"/><bean:write name="contents" property="comment3"/>
		</marguee>
	</logic:equal>

</logic:notEmpty>
</body>
</html>
