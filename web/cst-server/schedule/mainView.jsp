<%--
  - 작성자: yngwie@sqisoft.com
  - 일자: 2007.08.14
  - 저작권 표시:
  - @(#)
  - 설명: main frame 표시 화면
  - 수정자:
  - 수정일:
  - 수정사유:
  --%>
<%@page language="java"  contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="/tlds/struts-logic" prefix="logic" %>
<%@ taglib uri="/tlds/struts-bean" prefix="bean" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<link href="/css/style.css" media="screen" rel="Stylesheet" type="text/css"/>
<script language="JavaScript" type="text/JavaScript">
<!--
var infoVisible = false;

/**

*/
function changeHideShow(){
	if(infoVisible ){
		infoVisible = false;
	}else{
		infoVisible = true;
	}
	hideShowDiv();
}

/**
	click event 에 따라 info layer 의 display 속성을 변경한다.
*/
function hideShowDiv(){
	var displayStr = '';
	var borderStr = '';
	
	if(infoVisible) {
		displayStr = '';
		borderStr = '1px solid #FFFF00';
	}else{
		displayStr = 'none';
		borderStr = '0px';
	}
	
	// main frame border line 설정
	var mainObj = document.getElementById("mainDiv");
	mainObj.style.border = borderStr;
	
	for(var i = 1; i <= <bean:write name="listSize"/>; i++){
		var obj = document.getElementById("info0" + i + "Div");
		var bObj = document.getElementById("frame0" + i + "Div");
		
		// info layer border line 설정
		if(obj) {
			obj.style.display = displayStr;
			setStyle(obj, bObj.style.posLeft, bObj.style.posTop, borderStr, displayStr);
		}
		
		// contents layer display 설정
		if(bObj) {
			bObj.style.border = borderStr;
		}
	}
}

/**
	info layer 의 style 을 지정한다.
*/
function setStyle(obj, l, t, borderStr, displayStr){
	obj.style.position = 'absolute';
	obj.style.width = 240;
	obj.style.height = 8;
	obj.style.posLeft = l ;
	obj.style.posTop = t;
	
	obj.style.fontSize = '7pt';
	
	obj.style.border = borderStr;
	obj.style.backgroundColor = '#FFFFFF';
	obj.style.zIndex = '100';
	obj.style.display = displayStr;
}

//-->
</script>
</head>

<body scroll="no">
<!-- background layer -->
<DIV id="mainDiv" class="mainDiv" style="top:0px; left:0px; width:<bean:write name="width"/>px; height:<bean:write name="height"/>px; z-index:-1;">
	<input type="button" style="position:absolute; top: 1px; left: 1px; width:3px; height:3px;" onclick="javascript:changeHideShow()">
	
	<logic:notEmpty name="scheduleList" scope="request">
		<logic:iterate id="frames" name="scheduleList" type="com.sqisoft.utv.dto.UtvSchedule" indexId="index">
		    <%-- display info div --%>
			<DIV id="info<bean:write name="frames" property="frameId"/>Div" style="display:none" >
				frameId:<bean:write name="frames" property="frameId"/> left:<bean:write name="frames" property="frame.x"/>; top:<bean:write name="frames" property="frame.y"/>; width:<bean:write name="frames" property="frame.width"/>; height:<bean:write name="frames" property="frame.height"/>
			</DIV>
			<DIV id="frame<bean:write name="frames" property="frameId"/>Div" style="position:absolute; overflow:hidden; left:<bean:write name="frames" property="frame.x"/>px; top:<bean:write name="frames" property="frame.y"/>px; width:<bean:write name="frames" property="frame.width"/>px; height:<bean:write name="frames" property="frame.height"/>px; border : 0px solid #FFFFFF; z-index:<bean:write name="index"/>;">
				<IFRAME name="<bean:write name="frames" property="frameId"/>" src="/schedule.utv?method=getContents&templateId=<bean:write name="frames" property="templateId"/>&frameId=<bean:write name="frames" property="frameId"/>&date=<bean:write name="date"/>&time=<bean:write name="time"/>&tmpVar=<%=System.currentTimeMillis() %>" frameborder="0" scrolling="no" width="100%" height="100%"></IFRAME>
			</DIV>
		</logic:iterate>
	</logic:notEmpty>

</DIV>
</body>
</html>

