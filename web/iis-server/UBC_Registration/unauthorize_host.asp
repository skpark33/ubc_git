<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' host id
	Dim strHostId
	strHostId = Request("hostId")

	' sql query
	Dim strSQL
	strSQL = "select * from utv_host where siteId = '" + strSiteId + "' and hostId = '" + strHostId + "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then
		' exist record

		' extract Left-3-char --> check 'DEL' keyword
		Dim str_left
		str_left = ""

		if not IsNull(objRS("macAddress")) then
			Dim old_mac_addr
			old_mac_addr = ObjRS("macAddress")

			if Length(old_mac_addr) > 3 then
				str_left = Left(old_mac_addr, 3)
			end if
		end if

		' check 'DEL' keyword
		if str_left <> 'DEL' then
			Dim tm
			tm = now()

			' un-authorize
			ObjRS("macAddress") = "DEL " + FormatDateTime(tm, 2)
			ObjRS.Update

			Response.Write "OK"
		else
			' already un-authorized
			Response.Write "ALREADY"
		end if
	else
		' not exist record
		Response.Write "FAIL"
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
