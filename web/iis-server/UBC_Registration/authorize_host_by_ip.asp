<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	'
	' 기아,현대 만을 위한 버전
	' ip주소로 검사해서 인증
	' mac주소를 다르더라도 무조건 인증
	'

	' Mac Address
	Dim strMacAddr
	strMacAddr = Request("macAddr")

	' IP Address
	Dim strIPAddr
	strIPAddr = Request("ipAddr")

	' reset
	Dim strReset
	strReset = Request("reset")

	' sql query
	Dim strSQL
	strSQL = "select * from utv_host where ipAddress = '" + strIPAddr + "'"

	if strMacAddr = "" or strIPAddr = "" then
		if strMacAddr = "" then
			' mac-addr is null
			Response.Write "5|"
		else
			' ip-addr is null
			Response.Write "7|"
		end if
	else
		' create db object
		Dim ObjRS
		Set ObjRS = Server.CreateObject("ADODB.Recordset")
		ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

		if Not ObjRS.EOF then
			' exist record

			Dim host_id
			host_id = ObjRS("hostId")

			Dim old_mac
			old_mac = ObjRS("macAddress")

			Dim edition
			edition = ObjRS("edition")

			if strReset = "1" then
				' reset authorization
				ObjRS("macAddress") = strMacAddr
				ObjRS.Update
				Response.Write "4|"
			else
				if old_mac = "" or IsNull(old_mac) then
					' write authorization
					ObjRS("macAddress") = strMacAddr
					ObjRS("authDate") = now()
					ObjRS.Update
					Response.Write "1|"
				elseif old_mac = strMacAddr then
					' already authorize
					Response.Write "2|"
				else
					' already authorize another host -> 무조건 인증
					'Response.Write "3|"
					Response.Write "1|"
				end if
			end if

			Response.Write host_id + "|" + edition
		else
			' not exist record
			Response.Write "6|"
		end if

		ObjRS.Close
		Set ObjRS = Nothing
	end if

	objConn.Close
	Set objConn = Nothing
%>
