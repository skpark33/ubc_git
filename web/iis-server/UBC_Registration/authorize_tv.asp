<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' host id
	Dim strHostId
	strHostId = Request("hostId")

	' sql query
	Dim strSQL
	strSQL = "select * from utv_host where siteId = '" + strSiteId + "' and hostId = '" + strHostId + "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then
		' exist record

		' set plungin-type
		Dim plungin_type
		plungin_type = ObjRS("plugInType")

		if plungin_type = "" or IsNull(plungin_type) then
			' null field -> set to "TV"
			plungin_type = "TV"
			ObjRS("plugInType") = plungin_type
			ObjRS.Update
		else
			' exist something -> add "TV"
			if InStr(plungin_type, "TV") > 0 then
			else
				plungin_type = plungin_type + ",TV"
				ObjRS("plugInType") = plungin_type
				ObjRS.Update
			end if
		end if

		' check license-type
		Dim license_type
		license_type = ObjRS("licenseType")

		if license_type = "" or IsNull(license_type) then
			' null field
			Response.Write "denied"
		else
			if InStr(license_type, "TV") > 0 then
				Response.Write "ok"
			else
				Response.Write "denied"
			end if
		end if
	else
		' not exist record
		Response.Write "none"
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
