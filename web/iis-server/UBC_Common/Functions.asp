<%
	Function CreateGUID(tmpLength)
		Randomize Timer
		Dim tmpCounter,tmpGUID
		Const strValid = "0123456789ABCDEF"
		For tmpCounter = 1 To tmpLength
			tmpGUID = tmpGUID & Mid(strValid, Int(Rnd(1) * Len(strValid)) + 1, 1)
		Next
		CreateGUID = tmpGUID
	End Function

	Function CreateWindowsGUID()
		CreateWindowsGUID = "{" & CreateGUID(8) & "-" & CreateGUID(4) & "-" & CreateGUID(4) & "-" & CreateGUID(4) & "-" & CreateGUID(12) & "}"
	End Function

	Function TimeToString(tm)
		if not IsNull(tm) then
			'TimeToString = CStr(DatePart("yyyy", tm)) + "/" + CStr(DatePart("m", tm)) + "/" + CStr(DatePart("d", tm)) + " " + CStr(DatePart("h", tm)) + ":" + + CStr(DatePart("n", tm)) + ":" + + CStr(DatePart("s", tm))
			TimeToString = FormatDateTime(tm, 2) + " " + FormatDateTime(tm, 4) + ":" + Right(FormatDateTime(tm, 0), 2)
		else
			TimeToString = ""
		end if
	End Function

	Function TimeToString2(tm, div)
		if not IsNull(tm) then
			Dim str
			str = FormatDateTime(tm, 2) + " " + FormatDateTime(tm, 4) + ":" + Right(FormatDateTime(tm, 0), 2)
			TimeToString2 = Replace(str, "-", "/")
		else
			TimeToString2 = ""
		end if
	End Function

	Function IntToString(nValue)
		if not IsNull(nValue) then
			IntToString = CStr(nValue)
		else
			IntToString = ""
		end if
	End Function
%>
