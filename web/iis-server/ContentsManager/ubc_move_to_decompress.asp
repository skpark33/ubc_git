<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then
		' exist record

		Dim contentsState
		contentsState = CInt(ObjRS("contentsState"))

		if contentsState = 9 or contentsState = 11 or contentsState = 13 or contentsState = 15 then
			' only can that contents state is compressed

			' file name
			Dim strFilename
			strFilename = objRS("filename")
			if IsNull(strFilename) then
				strFilename = ""
			end if

			' source file path
			Dim strCompressPath
			strCompressPath = CONTENTS_PATH + objRS("location") + strFilename
			if IsNull(strCompressPath) then
				strCompressPath = ""
			end if

			' source file path
			Dim strSrcPath
			Dim strNewFilename
			if strFilename <> "" then
				Dim length
				length = InStrRev(strFilename, ".", -1, 0)
				strNewFilename = Left(strFilename, length-1)
				strSrcPath = CONTENTS_PATH + objRS("location") + strNewFilename
			end if

			' command line
			Dim strCommand
			strCommand = Server.MapPath("gunzip.exe") + " -f " + Chr(34) + strCompressPath + Chr(34)

			if strFilename <> "" then
				' not null filename

				Dim WshShell
				Set WshShell = CreateObject("WScript.Shell")

				Dim oExec
				Set oExec = WshShell.Exec(strCommand)

				Do While oExec.Status = 0
					 'WScript.Sleep 100
				Loop

				Set oExec = Nothing
				Set WshShell = Nothing
			end if

			' create file object
			Dim objFSO
			set objFSO = Server.CreateObject("Scripting.FileSystemObject")

			if strFilename = "" or ( strFilename <> "" and objFSO.FileExists(strSrcPath) ) then

				if strFilename <> "" then
					ObjRS("filename") = strNewFilename
				end if

				' check backup flag
				contentsState = contentsState and 7

				ObjRS("contentsState") = contentsState
				ObjRS.Update

				Response.Write "TRUE"

			else
				Response.Write "FAIL"
			end if

			Set objFSO = Nothing

		else
			' not compressed
			Response.Write "CANT"
		end if
	else
		' not exist record
		Response.Write "FALSE"
	end if


	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
