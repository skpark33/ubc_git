<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON'"

	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")
	if strContentsId <> "" then
		strSQL = strSQL + " and contentsId = '" + strContentsId + "'"
	end if

	' contents type
	Dim strContentsType
	strContentsType = Request("contentsType")
	if strContentsType <> "" then
		strSQL = strSQL + " and contentsType = '" + strContentsType + "'"
	end if

	' keyword
	Dim strKeyword
	strKeyword = Request("keyword")
	if strKeyword <> "" then
		strSQL = strSQL + " and contentsName like '%" + strKeyword + "%'"
	end if

	' contents state
	Dim strContentsState
	strContentsState = Request("contentsState")

'Response.Write "SQL=" + strSQL + "and contentsState=" + strContentsState + vbCrLf

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	Do While Not ObjRS.EOF
		Dim nContentsState
		nContentsState = -1

		' get contents state
		if not IsNull(objRS("contentsState")) then
			Dim contents_state
			contents_state = objRS("contentsState")
			if contents_state <> "" then
				nContentsState = CInt(contents_state)
			end if
		end if

		Dim result
		result = 0

		' check condition(s)
		if strContentsState = "normal" then
			if nContentsState = 0 or nContentsState = 1 or nContentsState = 3 then
				result = 1
			end if
		elseif strContentsState = "working" then
			if nContentsState = 0 then
				result = 1
			end if
		elseif strContentsState = "encoded" then
			if nContentsState = 1 then
				result = 1
			end if
		elseif strContentsState = "verified" then
			if nContentsState = 3 then
				result = 1
			end if
		elseif strContentsState = "backup" then
			if nContentsState = 5 or nContentsState = 7 then
				result = 1
			end if
		elseif strContentsState = "compress" then
			if nContentsState = 9 or nContentsState = 11 or nContentsState = 13 or nContentsState = 15 then
				result = 1
			end if
		else
			result = 1
		end if

		if result = 1 then
			' condition(s) is OK

			Dim tm

			'Response.Write "mgrId=" + objRS("mgrId") + vbCrLf
			'Response.Write "siteId=" + objRS("siteId") + vbCrLf
			'Response.Write "programId=" + objRS("programId") + vbCrLf
			Response.Write "contentsId=" + objRS("contentsId") + vbCrLf
			Response.Write "category=" + objRS("category") + vbCrLf
			Response.Write "promotionId=" + objRS("promotionId") + vbCrLf
			Response.Write "contentsName=" + objRS("contentsName") + vbCrLf
			if not IsNull(objRS("contentsType")) then
				Response.Write "contentsType=" + CStr(objRS("contentsType")) + vbCrLf
			end if
			Response.Write "parentsId=" + objRS("parentsId") + vbCrLf
			Response.Write "registerId=" + objRS("registerId") + vbCrLf
			if not IsNull(objRS("registerTime")) then
				tm = objRS("registerTime")
				Response.Write "registerTime=" + CStr(DatePart("yyyy", tm)) + "/" + CStr(DatePart("m", tm)) + "/" + CStr(DatePart("d", tm)) + " " + CStr(DatePart("h", tm)) + ":" + + CStr(DatePart("n", tm)) + ":" + + CStr(DatePart("s", tm)) + vbCrLf
			end if
			if not IsNull(objRS("isRaw")) then
				Response.Write "isRaw=" + CStr(objRS("isRaw")) + vbCrLf
			end if
			Response.Write "contentsState=" + CStr(objRS("contentsState")) + vbCrLf
			if not IsNull(objRS("contentsStateTime")) then
				tm = objRS("contentsStateTime")
				Response.Write "contentsStateTime=" + CStr(DatePart("yyyy", tm)) + "/" + CStr(DatePart("m", tm)) + "/" + CStr(DatePart("d", tm)) + " " + CStr(DatePart("h", tm)) + ":" + + CStr(DatePart("n", tm)) + ":" + + CStr(DatePart("s", tm)) + vbCrLf
			end if
			Response.Write "contentsStateHistory=" + objRS("contentsStateHistory") + vbCrLf
			Response.Write "location=" + objRS("location") + vbCrLf
			Response.Write "filename=" + objRS("filename") + vbCrLf
			if not IsNull(objRS("width")) then
				Response.Write "width=" + CStr(objRS("width")) + vbCrLf
			end if
			if not IsNull(objRS("height")) then
				Response.Write "height=" + CStr(objRS("height")) + vbCrLf
			end if
			if not IsNull(objRS("volume")) then
				Response.Write "volume=" + CStr(objRS("volume")) + vbCrLf
			end if
			if not IsNull(objRS("runningTime")) then
				Response.Write "runningTime=" + CStr(objRS("runningTime")) + vbCrLf
			end if
			Response.Write "verifier=" + objRS("verifier") + vbCrLf
			if not IsNull(objRS("duration")) then
				Response.Write "duration=" + CStr(objRS("duration")) + vbCrLf
			end if
			Response.Write "videoCodec=" + objRS("videoCodec") + vbCrLf
			Response.Write "audioCodec=" + objRS("audioCodec") + vbCrLf
			Response.Write "encodingInfo=" + objRS("encodingInfo") + vbCrLf
			if not IsNull(objRS("encodingTime")) then
				tm = objRS("encodingTime")
				Response.Write "encodingTime=" + CStr(DatePart("yyyy", tm)) + "/" + CStr(DatePart("m", tm)) + "/" + CStr(DatePart("d", tm)) + " " + CStr(DatePart("h", tm)) + ":" + + CStr(DatePart("n", tm)) + ":" + + CStr(DatePart("s", tm)) + vbCrLf
			end if
			Response.Write "description=" + objRS("description") + vbCrLf
			Response.Write "comment1=" + objRS("comment1") + vbCrLf
			Response.Write "comment2=" + objRS("comment2") + vbCrLf
			Response.Write "comment3=" + objRS("comment3") + vbCrLf
			Response.Write "comment4=" + objRS("comment4") + vbCrLf
			Response.Write "comment5=" + objRS("comment5") + vbCrLf
			Response.Write "comment6=" + objRS("comment6") + vbCrLf
			Response.Write "comment7=" + objRS("comment7") + vbCrLf
			Response.Write "comment8=" + objRS("comment8") + vbCrLf
			Response.Write "comment9=" + objRS("comment9") + vbCrLf
			Response.Write "comment10=" + objRS("comment10") + vbCrLf
			if not IsNull(objRS("currentComment")) then
				Response.Write "currentComment=" + CStr(objRS("currentComment")) + vbCrLf
			end if
			Response.Write "bgColor=" + objRS("bgColor") + vbCrLf
			Response.Write "fgColor=" + objRS("fgColor") + vbCrLf
			Response.Write "font=" + objRS("font") + vbCrLf
			if not IsNull(objRS("fontSize")) then
				Response.Write "fontSize=" + CStr(objRS("fontSize")) + vbCrLf
			end if
			if not IsNull(objRS("playSpeed")) then
				Response.Write "playSpeed=" + CStr(objRS("playSpeed")) + vbCrLf
			end if
			if not IsNull(objRS("soundVolume")) then
				Response.Write "soundVolume=" + CStr(objRS("soundVolume")) + vbCrLf
			end if
			Response.Write "promotionValueList=" + objRS("promotionValueList") + vbCrLf
			Response.Write "snapshotFile=" + objRS("snapshotFile") + vbCrLf
			if not IsNull(objRS("isPublic")) then
				Response.Write "isPublic=" + CStr(objRS("isPublic")) + vbCrLf
			end if
			if not IsNull(objRS("serialNo")) then
				Response.Write "serialNo=" + CStr(objRS("serialNo")) + vbCrLf
			end if
			if not IsNull(objRS("tvInputType")) then
				Response.Write "tvInputType=" + CStr(objRS("tvInputType")) + vbCrLf
			end if
			Response.Write "tvChannel=" + objRS("tvChannel") + vbCrLf
			if not IsNull(objRS("tvPortType")) then
				Response.Write "tvPortType=" + CStr(objRS("tvPortType")) + vbCrLf
			end if
			if not IsNull(objRS("direction")) then
				Response.Write "direction=" + CStr(objRS("direction")) + vbCrLf
			end if
			if not IsNull(objRS("align")) then
				Response.Write "align=" + CStr(objRS("align")) + vbCrLf
			end if
			if not IsNull(objRS("isUsed")) then
				Response.Write "isUsed=" + CStr(objRS("isUsed")) + vbCrLf
			end if
			Response.Write "wizardXML=" + objRS("wizardXML") + vbCrLf
			Response.Write "wizardFiles=" + objRS("wizardFiles") + vbCrLf
		end if

		ObjRS.MoveNext
	Loop


	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
