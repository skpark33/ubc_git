<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	Dim strContentsId
	strContentsId = Request("contentsId")
'Response.Write "contentsId : " + strContentsId + vbCrLf

	Dim strContentsName
	strContentsName = Request("contentsName") 
	if strContentsName = "(null)" then
		strContentsName = ""
	end if
'Response.Write "contentsName : " + strContentsName + vbCrLf

	Dim strContentsType
	strContentsType = Request("contentsType")
'Response.Write "contentsType : " + strContentsType + vbCrLf

	Dim strRegisterTime
	strRegisterTime = Request("registerTime") '현재시간 입력
'Response.Write "registerTime : " + strRegisterTime + vbCrLf

	Dim strIsRaw
	strIsRaw = Request("isRaw") ' 0
'Response.Write "isRaw : " + strIsRaw + vbCrLf

	Dim strContentsState
	strContentsState = Request("contentsState") ' 0
'Response.Write "contentsState : " + strContentsState + vbCrLf

	Dim strContentsStateTime
	strContentsStateTime = Request("contentsStateTime") '현재시간 입력
'Response.Write "contentsStateTime : " + strContentsStateTime + vbCrLf

	Dim strLocation
	strLocation = Request("location") ' "/contents/COMMON/"
	if strLocation = "(null)" then
		strLocation = ""
	end if
'Response.Write "location : " + strLocation + vbCrLf

	Dim strFilename
	strFilename = Request("filename") ' ""
	if strFilename = "(null)" then
		strFilename = ""
	end if
'Response.Write "filename : " + strFilename + vbCrLf

	Dim strWidth
	strWidth = Request("width") ' 0
'Response.Write "width : " + strWidth + vbCrLf

	Dim strHeight
	strHeight = Request("height") ' 0
'Response.Write "height : " + strHeight + vbCrLf

	Dim strVolume
	strVolume = Request("volume") ' 0
'Response.Write "volume : " + strVolume + vbCrLf

	Dim strRunningTime
	strRunningTime = Request("runningTime") ' 0
'Response.Write "runningTime : " + strRunningTime + vbCrLf

	Dim strEncodingTime
	strEncodingTime = Request("encodingTime") '현재시간 입력
'Response.Write "encodingTime : " + strEncodingTime + vbCrLf

	Dim strComment1
	strComment1 = Request("comment1") ' ""
	if strComment1 = "(null)" then
		strComment1 = ""
	end if
'Response.Write "comment1 : " + strComment1 + vbCrLf

	Dim strComment2
	strComment2 = Request("comment2") ' ""
	if strComment2 = "(null)" then
		strComment2 = ""
	end if
'Response.Write "comment2 : " + strComment2 + vbCrLf

	Dim strComment3
	strComment3 = Request("comment3") ' ""
	if strComment3 = "(null)" then
		strComment3 = ""
	end if
'Response.Write "comment3 : " + strComment3 + vbCrLf

	Dim strComment4
	strComment4 = Request("comment4") ' ""
	if strComment4 = "(null)" then
		strComment4 = ""
	end if
'Response.Write "comment4 : " + strComment4 + vbCrLf

	Dim strComment5
	strComment5 = Request("comment5") ' ""
	if strComment5 = "(null)" then
		strComment5 = ""
	end if
'Response.Write "comment5 : " + strComment5 + vbCrLf

	Dim strComment6
	strComment6 = Request("comment6") ' ""
	if strComment6 = "(null)" then
		strComment6 = ""
	end if
'Response.Write "comment6 : " + strComment6 + vbCrLf

	Dim strComment7
	strComment7 = Request("comment7") ' ""
	if strComment7 = "(null)" then
		strComment7 = ""
	end if
'Response.Write "comment7 : " + strComment7 + vbCrLf

	Dim strComment8
	strComment8 = Request("comment8") ' ""
	if strComment8 = "(null)" then
		strComment8 = ""
	end if
'Response.Write "comment8 : " + strComment8 + vbCrLf

	Dim strComment9
	strComment9 = Request("comment9") ' ""
	if strComment9 = "(null)" then
		strComment9 = ""
	end if
'Response.Write "comment9 : " + strComment9 + vbCrLf

	Dim strComment10
	strComment10 = Request("comment10") ' ""
	if strComment10 = "(null)" then
		strComment10 = ""
	end if
'Response.Write "comment10 : " + strComment10 + vbCrLf

	Dim strCurrentComment
	strCurrentComment = Request("currentComment") ' 0
'Response.Write "currentComment : " + strCurrentComment + vbCrLf

	Dim strBgColor
	strBgColor = Request("bgColor") ' ""
	if strBgColor = "(null)" then
		strBgColor = ""
	end if
'Response.Write "bgColor : " + strBgColor + vbCrLf

	Dim strFgColor
	strFgColor = Request("fgColor") ' ""
	if strFgColor = "(null)" then
		strFgColor = ""
	end if
'Response.Write "fgColor : " + strFgColor + vbCrLf

	Dim strFont
	strFont = Request("font") ' ""
	if strFont = "(null)" then
		strFont = ""
	end if
'Response.Write "font : " + strFont + vbCrLf

	Dim strFontSize
	strFontSize = Request("fontSize") ' 0
'Response.Write "fontSize : " + strFontSize + vbCrLf

	Dim strPlaySpeed
	strPlaySpeed = Request("playSpeed") ' 0
'Response.Write "playSpeed : " + strPlaySpeed + vbCrLf

	Dim strSoundVolume
	strSoundVolume = Request("soundVolume") ' 0
'Response.Write "soundVolume : " + strSoundVolume + vbCrLf

	Dim strIsPublic
	strIsPublic = Request("isPublic") ' 1
'Response.Write "isPublic : " + strIsPublic + vbCrLf

	Dim strDirection
	strDirection = Request("direction") ' 0
'Response.Write "direction : " + strDirection + vbCrLf

	Dim strAlign
	strAlign = Request("align") ' 0
'Response.Write "align : " + strAlign + vbCrLf

	Dim strIsUsed
	strIsUsed = Request("isUsed") ' 1
'Response.Write "isUsed : " + strIsUsed + vbCrLf

	Dim strWizardFiles
	strWizardFiles = Request("wizardFiles")
	if strWizardFiles = "(null)" then
		strWizardFiles = ""
	end if
'Response.Write "wizardFiles : " + strWizardFiles + vbCrLf

	Dim strWizardXML
	strWizardXML = Request("wizardXML")
	if strWizardXML = "(null)" then
		strWizardXML = ""
	end if
'Response.Write "wizardXML : " + strWizardXML + vbCrLf

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim objRS_check
	Set objRS_check = Server.CreateObject("ADODB.Recordset")
	objRS_check.Open strSQL, objConn

	if objRS_check.EOF then
		' not exist record

		' create db object
		Dim objRS_add
		Set objRS_add = Server.CreateObject("ADODB.Recordset")
		objRS_add.Open "ubc_contents", objConn, , adLockOptimistic, adCmdTable

		' create new record
		objRS_add.AddNew

		objRS_add("mgrId") = "1"
		objRS_add("siteId") = "COMMON"
		objRS_add("programId") = "COMMON"
		objRS_add("contentsId") = strContentsId 
		'objRS_add("category") = strCategory
		'objRS_add("promotionId") = strPromotionId
		objRS_add("contentsName") = strContentsName
		objRS_add("contentsType") = strContentsType
		'objRS_add("parentsId") = strParentsId
		'objRS_add("registerId") = strRegisterId
		objRS_add("registerTime") = CDate(strRegisterTime) '현재시간 입력
		objRS_add("isRaw") = strIsRaw ' 0
		objRS_add("contentsState") = strContentsState ' 0
		objRS_add("contentsStateTime") = CDate(strContentsStateTime) '현재시간 입력
		'objRS_add("contentsStateHistory") = strContentsStateHistory
		objRS_add("location") = strLocation ' "/contents/COMMON/"
		objRS_add("filename") = strFilename ' ""
		objRS_add("width") = strWidth ' 0
		objRS_add("height") = strHeight ' 0
		objRS_add("volume") = strVolume ' 0
		objRS_add("runningTime") = strRunningTime ' 0
		'objRS_add("verifier") = strVerifier 
		'objRS_add("duration") = strDuration 
		'objRS_add("videoCodec") = strVideoCodec 
		'objRS_add("audioCodec") = strAudioCodec 
		'objRS_add("encodingInfo") = strEncodingInfo 
		objRS_add("encodingTime") = CDate(strEncodingTime) '현재시간 입력
		'objRS_add("description") = strDescription 
		objRS_add("comment1") = strComment1 ' ""
		objRS_add("comment2") = strComment2
		objRS_add("comment3") = strComment3
		objRS_add("comment4") = strComment4
		objRS_add("comment5") = strComment5
		objRS_add("comment6") = strComment6
		objRS_add("comment7") = strComment7 
		objRS_add("comment8") = strComment8
		objRS_add("comment9") = strComment9
		objRS_add("comment10") = strComment10
		objRS_add("currentComment") = strCurrentComment ' 0
		objRS_add("bgColor") = strBgColor ' ""
		objRS_add("fgColor") = strFgColor ' ""
		objRS_add("font") = strFont ' ""
		objRS_add("fontSize") = strFontSize ' 0
		objRS_add("playSpeed") = strPlaySpeed ' 0
		objRS_add("soundVolume") = strSoundVolume ' 0
		'objRS_add("promotionValueList") = strPromotionValueList 
		'objRS_add("snapshotFile") = strSnapshotFile 
		objRS_add("wizardFiles") = strWizardFiles ' 1
		objRS_add("isPublic") = strIsPublic ' 1
		'objRS_add("serialNo") = Request("serialNo") 
		'objRS_add("tvInputType") = strTvInputType 
		'objRS_add("tvChannel") = strTvChannel 
		'objRS_add("tvPortType") = strTvPortType 
		objRS_add("direction") = strDirection ' 0
		objRS_add("align") = strAlign ' 0
		objRS_add("isUsed") = strIsUsed ' 1
		objRS_add("wizardXML") = strWizardXML ' 1

		objRS_add.Update

		objRS_add.Close
		Set objRS_add = Nothing

		Response.Write "TRUE"
	else
		' exist record
		Response.Write "ALREADY"
	end if

	objRS_check.Close
	Set objRS_check = Nothing

	objConn.Close
	Set objConn = Nothing
%>
