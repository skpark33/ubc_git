<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then
		' exist record

		'
		' 다른 schedule에서 사용 유무 체크 루틴 추가 필요
		'
		'if using then
			' not using

			Dim contentsState
			contentsState = CInt(ObjRS("contentsState"))

			if contentsState = 1 or contentsState = 3 then
				' only can that contents state is uploaded, or verified

				' check backup flag
				contentsState = contentsState or 4

				ObjRS("contentsState") = contentsState
				ObjRS.Update
 
				Response.Write "TRUE"
			else
				' not uploaded, and not verified
				Response.Write "CANT"
			end if
		'else
		'	' using another schedule
		'	Response.Write "USING"
		'end if
	else
		' not exist record
		Response.Write "FALSE"
	end if


	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
