<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' program id
	Dim strProgramId
	strProgramId = Request("programId")

	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")
'Response.Write "contentsId : " + strContentsId + vbCrLf

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = '" + strSiteId + "' and programId = '" + strProgramId + "' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim objRS_private
	Set objRS_private = Server.CreateObject("ADODB.Recordset")
	objRS_private.Open strSQL, objConn

	if Not objRS_private.EOF then
		' exist record

		' check contents-state
		Dim contentsState
		contentsState = CInt(objRS_private("contentsState"))

		if contentsState <> 0 then
			' only verified contents can be copied

			' new filename
			Dim strNewFilename
			strNewFilename = objRS_private("filename")
			if strNewFilename <> "" then
				strNewFilename = "COMMON_" + objRS_private("programId") + "_" + objRS_private("filename") 
			end if

			' new contents id
			Dim strNewContentsId
			strNewContentsId = "COMMON_" + objRS_private("programId") + "_" + strContentsId

			' check to exist new contents id
			Dim objRS_common
			Set objRS_common = Server.CreateObject("ADODB.Recordset")
			strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strNewContentsId + "'"
			objRS_common.Open strSQL, objConn

			'
			' contents id 중복시 id 변경 루틴 추가 필요
			'

			' source file path
			Dim strSrcPath
			strSrcPath = CONTENTS_PATH + objRS_private("location") + objRS_private("filename") 

			' new file path
			Dim strNewPath
			strNewPath = CONTENTS_PATH + "\contents\COMMON\" + strNewFilename

			' create file object
			Dim objFSO
			set objFSO = Server.CreateObject("Scripting.FileSystemObject")

			'
			' filename 중복시 name 변경 루틴 추가 필요
			'

			if objRS_common.EOF and (strNewFilename = "" or (Not objFSO.FileExists(strNewPath) and strNewFilename <> "")) then
				' not exist record & file

				objRS_common.Close

				if strNewFilename <> "" then
					' file copy
					objFSO.CopyFile strSrcPath, Replace(strNewPath, "/", "\")
				end if

				' create db object
				Set objRS_common = Server.CreateObject("ADODB.Recordset")
				objRS_common.Open "ubc_contents", objConn, , adLockOptimistic, adCmdTable

				' create new record
				objRS_common.AddNew

				objRS_common("mgrId") = objRS_private("mgrId") 
				objRS_common("siteId") = "COMMON" 
				objRS_common("programId") = "COMMON"
				objRS_common("contentsId") = strNewContentsId
				'objRS_common("category") = objRS_private("category") 
				'objRS_common("promotionId") = objRS_private("promotionId") 
				objRS_common("contentsName") = objRS_private("contentsName") 
				objRS_common("contentsType") = objRS_private("contentsType") 
				'objRS_common("parentsId") = objRS_private("parentsId") 
				'objRS_common("registerId") = objRS_private("registerId") 
				objRS_common("registerTime") = objRS_private("registerTime") 
				'objRS_common("isRaw") = objRS_private("isRaw") 
				objRS_common("contentsState") = objRS_private("contentsState") 
				objRS_common("contentsStateTime") = objRS_private("contentsStateTime") 
				'objRS_common("contentsStateHistory") = objRS_private("contentsStateHistory") 
				objRS_common("location") = "/contents/COMMON/" 'objRS_private("location") 
				objRS_common("filename") = strNewFilename
				objRS_common("width") = objRS_private("width") 
				objRS_common("height") = objRS_private("height") 
				objRS_common("volume") = objRS_private("volume") 
				objRS_common("runningTime") = objRS_private("runningTime") 
				'objRS_common("verifier") = objRS_private("verifier") 
				'objRS_common("duration") = objRS_private("duration") 
				'objRS_common("videoCodec") = objRS_private("videoCodec") 
				'objRS_common("audioCodec") = objRS_private("audioCodec") 
				'objRS_common("encodingInfo") = objRS_private("encodingInfo") 
				objRS_common("encodingTime") = objRS_private("encodingTime") 
				'objRS_common("description") = objRS_private("description") 
				objRS_common("comment1") = objRS_private("comment1") 
				objRS_common("comment2") = objRS_private("comment2") 
				objRS_common("comment3") = objRS_private("comment3") 
				objRS_common("comment4") = objRS_private("comment4") 
				objRS_common("comment5") = objRS_private("comment5") 
				objRS_common("comment6") = objRS_private("comment6") 
				objRS_common("comment7") = objRS_private("comment7") 
				objRS_common("comment8") = objRS_private("comment8") 
				objRS_common("comment9") = objRS_private("comment9") 
				objRS_common("comment10") = objRS_private("comment10") 
				objRS_common("currentComment") = objRS_private("currentComment") 
				objRS_common("bgColor") = objRS_private("bgColor") 
				objRS_common("fgColor") = objRS_private("fgColor") 
				objRS_common("font") = objRS_private("font") 
				objRS_common("fontSize") = objRS_private("fontSize") 
				objRS_common("playSpeed") = objRS_private("playSpeed") 
				objRS_common("soundVolume") = objRS_private("soundVolume") 
				'objRS_common("promotionValueList") = objRS_private("promotionValueList") 
				'objRS_common("snapshotFile") = objRS_private("snapshotFile") 
				objRS_common("wizardFiles") = objRS_private("wizardFiles") 
				objRS_common("isPublic") = objRS_private("isPublic") 
				'objRS_common("serialNo") = objRS_private("serialNo") 
				'objRS_common("tvInputType") = objRS_private("tvInputType") 
				'objRS_common("tvChannel") = objRS_private("tvChannel") 
				'objRS_common("tvPortType") = objRS_private("tvPortType") 
				objRS_common("direction") = objRS_private("direction") 
				objRS_common("align") = objRS_private("align") 
				objRS_common("isUsed") = objRS_private("isUsed") 
				objRS_common("wizardXML") = objRS_private("wizardXML") 

				objRS_common.Update

				Response.Write "TRUE"
			else
				' exist record or file
				Response.Write "ALREADY"
			end if

			objRS_common.Close
			Set objRS_common = Nothing

			Set objFSO = Nothing
		else
			' not verified contents
			Response.Write "CANT"
		end if
	else
		' not exist record
		Response.Write "FALSE"
	end if

	objRS_private.Close
	Set objRS_private = Nothing

	objConn.Close
	Set objConn = Nothing
%>
