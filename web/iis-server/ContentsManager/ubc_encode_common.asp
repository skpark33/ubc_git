<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim objRS
	Set objRS = Server.CreateObject("ADODB.Recordset")
	objRS.Open strSQL, objConn

	if Not objRS.EOF then
		' exist record

		' filename
		Dim strFilename
		strFilename = objRS("filename")

		' contents state
		Dim contentsState
		contentsState = CInt(objRS("contentsState"))
		contentsState = contentsState and 1

		' contents type
		Dim strContentsType
		strContentsType = objRS("contentsType")

		if strContentsType = "0" and contentsState = 1 then
			' only can video type, and verified

			if strFilename <> "" then
				' only can that file exist

				' filename
				Dim strNewFilename
				strNewFilename = "ENC_" + strFilename

				' contents id
				Dim strNewContentsId
				strNewContentsId = "ENC_" + strContentsId

				'check exist
				Dim objRS_enc
				Set objRS_enc = Server.CreateObject("ADODB.Recordset")
				strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strNewContentsId + "'"
				objRS_enc.Open strSQL, objConn

				' source file path
				Dim strSrcPath
				strSrcPath = CONTENTS_PATH + objRS("location") + objRS("filename") 

				' new file path
				Dim strNewPath
				strNewPath = CONTENTS_PATH + "\contents\COMMON\" + strNewFilename

				' encoding script path
				Dim strScriptPath
				strScriptPath = CONTENTS_PATH + "\contents\COMMON\" + strNewFilename + ".bat"

				' create file object
				Dim objFSO
				set objFSO = Server.CreateObject("Scripting.FileSystemObject")

				if objRS_enc.EOF then 'and ( not objFSO.FileExists(strNewPath) ) then '파일 중복은 무시
					' not exist record

					objRS_enc.Close
					set objRS_enc = Nothing

					' create script file
					Dim objOpenFile
					Set objOpenFile = objFSO.CreateTextFile(strScriptPath, true)
					objOpenFile.Write ENCODING_PATH + " " + Chr(34) + strSrcPath + Chr(34) + " -o " + Chr(34) + strNewPath + Chr(34) + " -mc 0 -audio-preload 0.0 -of lavf -lavfopts format=AVI -lavcopts vglobal=1 -ofps 24.000 -ovc xvid -xvidencopts bitrate=8000:max_bframes=0:quant_type=h263:me_quality=6 -srate 44100 -oac mp3lame -lameopts vbr=0  -lameopts br=96 -lameopts mode=0 -lameopts aq=7 -af volnorm -lavcopts threads=6 -xvidencopts threads=6 -lavdopts threads=6 -x264encopts threads=6"
					objOpenFile.Write vbCrLf
					objOpenFile.Write "start http://localhost:" + IIS_PORT + "/ContentsManager/ubc_complete_to_encode.asp?contentsId=" + strNewContentsId
					objOpenFile.Write vbCrLf
					objOpenFile.Write "del %0"
					objOpenFile.Write vbCrLf
					objOpenFile.Close
					Set objOpenFile = Nothing

					' run scripting file
					Dim Shell
					Set Shell = CreateObject("WScript.Shell")
					Shell.Run strScriptPath, 3, false
					Set Shell = Nothing

					' create db object
					Set objRS_enc = Server.CreateObject("ADODB.Recordset")
					objRS_enc.Open "ubc_contents", objConn, , adLockOptimistic, adCmdTable

					' create new record
					objRS_enc.AddNew

					objRS_enc("mgrId") = objRS("mgrId") 
					objRS_enc("siteId") = "COMMON" 
					objRS_enc("programId") = "COMMON"
					objRS_enc("contentsId") = strNewContentsId
					'objRS_enc("category") = objRS("category") 
					'objRS_enc("promotionId") = objRS("promotionId") 
					objRS_enc("contentsName") = objRS("contentsName") 
					objRS_enc("contentsType") = objRS("contentsType") 
					'objRS_enc("parentsId") = objRS("parentsId") 
					'objRS_enc("registerId") = objRS("registerId") 
					objRS_enc("registerTime") = objRS("registerTime") 
					'objRS_enc("isRaw") = objRS("isRaw") 
					objRS_enc("contentsState") = 0 'objRS("contentsState") 
					objRS_enc("contentsStateTime") = objRS("contentsStateTime") 
					'objRS_enc("contentsStateHistory") = objRS("contentsStateHistory") 
					objRS_enc("location") = "/contents/COMMON/" 'objRS("location") 
					objRS_enc("filename") = strNewFilename
					objRS_enc("width") = objRS("width") 
					objRS_enc("height") = objRS("height") 
					objRS_enc("volume") = "0" 'objRS("volume") 
					objRS_enc("runningTime") = objRS("runningTime") 
					'objRS_enc("verifier") = objRS("verifier") 
					'objRS_enc("duration") = objRS("duration") 
					'objRS_enc("videoCodec") = objRS("videoCodec") 
					'objRS_enc("audioCodec") = objRS("audioCodec") 
					'objRS_enc("encodingInfo") = objRS("encodingInfo") 
					objRS_enc("encodingTime") = objRS("encodingTime") 
					'objRS_enc("description") = objRS("description") 
					objRS_enc("comment1") = objRS("comment1") 
					objRS_enc("comment2") = objRS("comment2") 
					objRS_enc("comment3") = objRS("comment3") 
					objRS_enc("comment4") = objRS("comment4") 
					objRS_enc("comment5") = objRS("comment5") 
					objRS_enc("comment6") = objRS("comment6") 
					objRS_enc("comment7") = objRS("comment7") 
					objRS_enc("comment8") = objRS("comment8") 
					objRS_enc("comment9") = objRS("comment9") 
					objRS_enc("comment10") = objRS("comment10") 
					objRS_enc("currentComment") = objRS("currentComment") 
					objRS_enc("bgColor") = objRS("bgColor") 
					objRS_enc("fgColor") = objRS("fgColor") 
					objRS_enc("font") = objRS("font") 
					objRS_enc("fontSize") = objRS("fontSize") 
					objRS_enc("playSpeed") = objRS("playSpeed") 
					objRS_enc("soundVolume") = objRS("soundVolume") 
					'objRS_enc("promotionValueList") = objRS("promotionValueList") 
					'objRS_enc("snapshotFile") = objRS("snapshotFile") 
					objRS_enc("wizardFiles") = objRS("wizardFiles") 
					objRS_enc("isPublic") = objRS("isPublic") 
					'objRS_enc("serialNo") = objRS("serialNo") 
					'objRS_enc("tvInputType") = objRS("tvInputType") 
					'objRS_enc("tvChannel") = objRS("tvChannel") 
					'objRS_enc("tvPortType") = objRS("tvPortType") 
					objRS_enc("direction") = objRS("direction") 
					objRS_enc("align") = objRS("align") 
					objRS_enc("isUsed") = objRS("isUsed") 
					objRS_enc("wizardXML") = objRS("wizardXML") 

					objRS_enc.Update

					Response.Write "TRUE"
				else
					' exist record
					Response.Write "ALREADY"
				end if

				objRS_enc.Close
				Set objRS_enc = Nothing

				Set objFSO = Nothing
			else
				' filename is null
				Response.Write "NOTHING"
			end if
		else
			' not video type, or not verified
			Response.Write "CANT"
		end if

	else
		' not exist record
		Response.Write "FALSE"
	end if

	objRS.Close
	Set objRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
