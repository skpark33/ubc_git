<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then
		' exist record

		'
		' 다른 schedule에서 사용 유무 체크 루틴 추가 필요
		'
		'if using then
			' not using

			Dim contentsState
			contentsState = CInt(ObjRS("contentsState"))

			if contentsState = 1 or contentsState = 3 or contentsState = 5 or contentsState = 7 then
				' only can that contents state is uploaded, or verified, or backup

				' file name
				Dim strFilename
				strFilename = objRS("filename")
				if IsNull(strFilename) then
					strFilename = ""
				end if

				' source file path
				Dim strSrcPath
				strSrcPath = CONTENTS_PATH + objRS("location") + strFilename
				if IsNull(strSrcPath) then
					strSrcPath = ""
				end if

				' command line
				Dim strCommand
				strCommand = Server.MapPath("gzip.exe") + " " + Chr(34) + strSrcPath + Chr(34)

				if strFilename <> "" then
					' not null filename

					Dim WshShell
					Set WshShell = CreateObject("WScript.Shell")

					Dim oExec
					Set oExec = WshShell.Exec(strCommand)

					Do While oExec.Status = 0
						 'WScript.Sleep 100
					Loop

					Set oExec = Nothing
					Set WshShell = Nothing
				end if

				' create file object
				Dim objFSO
				set objFSO = Server.CreateObject("Scripting.FileSystemObject")
				
				if IsNull(strFilename) or IsNull(strSrcPath) or strFilename = "" or ( strFilename <> "" and objFSO.FileExists(strSrcPath + ".gz") ) then

					if strFilename <> "" then
						ObjRS("filename") = strFilename + ".gz"
					end if

					' check compress flag
					contentsState = contentsState or 8

					ObjRS("contentsState") = contentsState
					ObjRS.Update

					Response.Write "TRUE"
				else

					Response.Write "FAIL"
				end if

				Set objFSO = Nothing

			else
				' not uploaded, and not verified, and not backup
				Response.Write "CANT"
			end if
		'else
		'	' using another schedule
		'	Response.Write "USING"
		'end if
	else
		' not exist record
		Response.Write "FALSE"
	end if


	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
