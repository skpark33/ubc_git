<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	Dim strContentsId
	strContentsId = Request("contentsId")
'Response.Write "contentsId : " + strContentsId + vbCrLf

	Dim strContentsName
	strContentsName = Request("contentsName") 
'Response.Write "contentsName : " + strContentsName + vbCrLf

	Dim strContentsType
	strContentsType = Request("contentsType")
'Response.Write "contentsType : " + strContentsType + vbCrLf
	 
	Dim strRegisterTime
	strRegisterTime = Request("registerTime") '현재시간 입력
'Response.Write "registerTime : " + strRegisterTime + vbCrLf

	Dim strIsRaw
	strIsRaw = Request("isRaw") ' 0
'Response.Write "isRaw : " + strIsRaw + vbCrLf

	Dim strContentsState
	strContentsState = Request("contentsState") ' 0
'Response.Write "contentsState : " + strContentsState + vbCrLf

	Dim strContentsStateTime
	strContentsStateTime = Request("contentsStateTime") '현재시간 입력
'Response.Write "contentsStateTime : " + strContentsStateTime + vbCrLf

	Dim strLocation
	strLocation = Request("location") ' "/contents/COMMON/"
'Response.Write "location : " + strLocation + vbCrLf

	Dim strFilename
	strFilename = Request("filename") ' ""
'Response.Write "filename : " + strFilename + vbCrLf

	Dim strWidth
	strWidth = Request("width") ' 0
'Response.Write "width : " + strWidth + vbCrLf

	Dim strHeight
	strHeight = Request("height") ' 0
'Response.Write "height : " + strHeight + vbCrLf

	Dim strVolume
	strVolume = Request("volume") ' 0
'Response.Write "volume : " + strVolume + vbCrLf

	Dim strRunningTime
	strRunningTime = Request("runningTime") ' 0
'Response.Write "runningTime : " + strRunningTime + vbCrLf

	Dim strEncodingTime
	strEncodingTime = Request("encodingTime") '현재시간 입력
'Response.Write "encodingTime : " + strEncodingTime + vbCrLf

	Dim strComment1
	strComment1 = Request("comment1") ' ""
'Response.Write "comment1 : " + strComment1 + vbCrLf

	Dim strComment2
	strComment2 = Request("comment2") ' ""
'Response.Write "comment2 : " + strComment2 + vbCrLf

	Dim strComment3
	strComment3 = Request("comment3") ' ""
'Response.Write "comment3 : " + strComment3 + vbCrLf

	Dim strComment4
	strComment4 = Request("comment4") ' ""
'Response.Write "comment4 : " + strComment4 + vbCrLf

	Dim strComment5
	strComment5 = Request("comment5") ' ""
'Response.Write "comment5 : " + strComment5 + vbCrLf

	Dim strComment6
	strComment6 = Request("comment6") ' ""
'Response.Write "comment6 : " + strComment6 + vbCrLf

	Dim strComment7
	strComment7 = Request("comment7") ' ""
'Response.Write "comment7 : " + strComment7 + vbCrLf

	Dim strComment8
	strComment8 = Request("comment8") ' ""
'Response.Write "comment8 : " + strComment8 + vbCrLf

	Dim strComment9
	strComment9 = Request("comment9") ' ""
'Response.Write "comment9 : " + strComment9 + vbCrLf

	Dim strComment10
	strComment10 = Request("comment10") ' ""
'Response.Write "comment10 : " + strComment10 + vbCrLf

	Dim strCurrentComment
	strCurrentComment = Request("currentComment") ' 0
'Response.Write "currentComment : " + strCurrentComment + vbCrLf

	Dim strBgColor
	strBgColor = Request("bgColor") ' ""
'Response.Write "bgColor : " + strBgColor + vbCrLf

	Dim strFgColor
	strFgColor = Request("fgColor") ' ""
'Response.Write "fgColor : " + strFgColor + vbCrLf

	Dim strFont
	strFont = Request("font") ' ""
'Response.Write "font : " + strFont + vbCrLf

	Dim strFontSize
	strFontSize = Request("fontSize") ' 0
'Response.Write "fontSize : " + strFontSize + vbCrLf

	Dim strPlaySpeed
	strPlaySpeed = Request("playSpeed") ' 0
'Response.Write "playSpeed : " + strPlaySpeed + vbCrLf

	Dim strSoundVolume
	strSoundVolume = Request("soundVolume") ' 0
'Response.Write "soundVolume : " + strSoundVolume + vbCrLf

	Dim strIsPublic
	strIsPublic = Request("isPublic") ' 1
'Response.Write "isPublic : " + strIsPublic + vbCrLf

	Dim strDirection
	strDirection = Request("direction") ' 0
'Response.Write "direction : " + strDirection + vbCrLf

	Dim strAlign
	strAlign = Request("align") ' 0
'Response.Write "align : " + strAlign + vbCrLf

	Dim strIsUsed
	strIsUsed = Request("isUsed") ' 1
'Response.Write "isUsed : " + strIsUsed + vbCrLf

	Dim strWizardFiles
	strWizardFiles = Request("wizardFiles")
'Response.Write "wizardFiles : " + strWizardFiles + vbCrLf

	Dim strWizardXML
	strWizardXML = Request("wizardXML")
'Response.Write "wizardXML : " + strWizardXML + vbCrLf

	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strContentsId + "'"

	Dim objRS
	Set objRS = Server.CreateObject("ADODB.Recordset")
	objRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if not objRS.EOF then

		'if strContentsId<>"" then
		'	objRS("contentsId") = strcontentsId
		'end if

		if strContentsName = "(null)" then
			objRS("contentsName") = ""
		elseif strContentsName<>"" then
			objRS("contentsName") = strContentsName
		end if

		if strContentsType<>"" then
			objRS("contentsType") = CInt(strContentsType)
		end if

		if strRegisterTime<>"" then
			objRS("registerTime") = CDate(strRegisterTime) '현재시간 입력
		end if

		if strIsRaw<>"" then
			objRS("isRaw") = CInt(strIsRaw) ' 0
		end if

		if strContentsState<>"" then
			objRS("contentsState") = CInt(strContentsState) ' 0
		end if

		if strContentsStateTime<>"" then
			objRS("contentsStateTime") = CDate(strContentsStateTime) '현재시간 입력
		end if

		if strLocation = "(null)" then
			objRS("location") = ""
		elseif strLocation<>"" then
			objRS("location") = strLocation ' "/contents/COMMON/"
		end if

		if strFilename = "(null)" then
			objRS("filename") = ""
		elseif strFilename<>"" then
			objRS("filename") = strFilename ' ""
		end if

		if strWidth<>"" then
			objRS("width") = CInt(strWidth) ' 0
		end if

		if strHeight<>"" then
			objRS("height") = CInt(strHeight) ' 0
		end if

		if strVolume<>"" then
			objRS("volume") = CLng(strVolume) ' 0
		end if

		if strRunningTime<>"" then
			objRS("runningTime") = CLng(strRunningTime) ' 0
		end if

		if strEncodingTime<>"" then
			objRS("encodingTime") = CDate(strEncodingTime) '현재시간 입력
		end if

		if strComment1 = "(null)" then
			objRS("comment1") = ""
		elseif strComment1<>"" then
			objRS("comment1") = strComment1 ' ""
		end if

		if strComment2 = "(null)" then
			objRS("comment2") = ""
		elseif strComment2<>"" then
			objRS("comment2") = strComment2 
		end if

		if strComment3 = "(null)" then
			objRS("comment3") = ""
		elseif strComment3<>"" then
			objRS("comment3") = strComment3 
		end if

		if strComment4 = "(null)" then
			objRS("comment4") = ""
		elseif strComment4<>"" then
			objRS("comment4") = strComment4 
		end if

		if strComment5 = "(null)" then
			objRS("comment5") = ""
		elseif strComment5<>"" then
			objRS("comment5") = strComment5 
		end if

		if strComment6 = "(null)" then
			objRS("comment6") = ""
		elseif strComment6<>"" then
			objRS("comment6") = strComment6 
		end if

		if strComment7 = "(null)" then
			objRS("comment7") = ""
		elseif strComment7<>"" then
			objRS("comment7") = strComment7 
		end if

		if strComment8 = "(null)" then
			objRS("comment8") = ""
		elseif strComment8<>"" then
			objRS("comment8") = strComment8 
		end if

		if strComment9 = "(null)" then
			objRS("comment9") = ""
		elseif strComment9<>"" then
			objRS("comment9") = strComment9 
		end if

		if strComment10= "(null)" then
			objRS("comment10") = ""
		elseif strComment10<>"" then
			objRS("comment10") = strComment10 
		end if

		if strCurrentComment<>"" then
			objRS("currentComment") = CInt(strCurrentComment) ' 0
		end if

		if strBgColor = "(null)" then
			objRS("bgColor") = ""
		elseif strBgColor<>"" then
			objRS("bgColor") = strBgColor ' ""
		end if

		if strFgColor = "(null)" then
			objRS("fgColor") = ""
		elseif strFgColor<>"" then
			objRS("fgColor") = strFgColor ' ""
		end if

		if strFont = "(null)" then
			objRS("font") = ""
		elseif strFont<>"" then
			objRS("font") = strFont ' ""
		end if

		if strFontSize<>"" then
			objRS("fontSize") = CInt(strFontSize) ' 0
		end if

		if strPlaySpeed<>"" then
			objRS("playSpeed") = CInt(strPlaySpeed) ' 0
		end if

		if strSoundVolume<>"" then
			objRS("soundVolume") = CInt(strSoundVolume) ' 0
		end if

		if strIsPublic<>"" then
			objRS("isPublic") = CInt(strIsPublic) ' 1
		end if

		if strDirection<>"" then
			objRS("direction") = CInt(strDirection) ' 0
		end if

		if strAlign<>"" then
			objRS("align") = CInt(strAlign) ' 0
		end if

		if strIsUsed<>"" then
			objRS("isUsed") = CInt(strIsUsed) ' 1
		end if

		if strWizardFiles = "(null)" then
			objRS("wizardFiles") = ""
		elseif strWizardFiles<>"" then
			objRS("wizardFiles") = strWizardFiles
		end if

		if strWizardXML = "(null)" then
			objRS("wizardXML") = ""
		elseif strWizardXML<>"" then
			objRS("wizardXML") = strWizardXML
		end if

		objRS.Update

		Response.Write "TRUE"
	else
		Response.Write "FALSE"
	end if

	objRS.Close
	Set objRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
