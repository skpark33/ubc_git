<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->

<html>
<head>
<script>
window.open("blank.html", "_self").close();
</script>
</head>
<body>

<%
	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then
		' exist record

		Dim nFileLength
		nFileLength = 0

		' filename		
		Dim strFilename
		strFilename = objRS("filename")

		if strFilename <> "" then
			' exist filename

			' create file obejct
			Dim objFSO
			set objFSO = Server.CreateObject("Scripting.FileSystemObject")

			' file path
			Dim strPath
			strPath = CONTENTS_PATH + objRS("location") + strFilename

			if objFSO.FileExists(strPath) then
				' exist file
				Dim contents_file
				Set contents_file = objFSO.GetFile(srtPath)
				nFileLength = contents_file.Size
			end if
		end if

		' modify contents-state
		Dim contentsState
		contentsState = CInt(ObjRS("contentsState"))
		contentsState = contentsState or 1

		' update  state & filesize
		ObjRS("contentsState") = contentsState
		ObjRS("volume") = CStr(nFileLength)
		ObjRS.Update

		Set objFSO = Nothing

		Response.Write "TRUE"
	else
		' not exist record
		Response.Write "FALSE"
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>

</body>
</html>
