<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/ContentsManager/adovbs.inc" -->
<!-- #include virtual="/ContentsManager/LocalSettings.asp" -->
<%
	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")

	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' program id
	Dim strProgramId
	strProgramId = Request("programId")

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = 'COMMON' and programId = 'COMMON' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim objRS_common
	Set objRS_common = Server.CreateObject("ADODB.Recordset")
	objRS_common.Open strSQL, objConn

	if Not objRS_common.EOF then
		' exist record

		' check contents-state
		Dim contentsState
		contentsState = CInt(objRS_common("contentsState"))

		if contentsState = 3 then
			' only verified contents can be copied

			Dim objRS_private

			' creaet db object
			Set objRS_private = Server.CreateObject("ADODB.Recordset")
			strSQL = "select * from ubc_contents where siteId = '" + strSiteId + "' and programId = '" + strProgramId + "' and contentsId = '" + strContentsId + "'"
			objRS_private.Open strSQL, objConn

			'
			' contents id 중복시 id 변경 루틴 추가 필요
			'

			if objRS_private.EOF then
				' not exist record

				objRS_private.Close

				' create db object
				Set objRS_private = Server.CreateObject("ADODB.Recordset")
				objRS_private.Open "ubc_contents", objConn, , adLockOptimistic, adCmdTable

				' create new record
				objRS_private.AddNew

				objRS_private("mgrId") = objRS_common("mgrId") 
				objRS_private("siteId") = strSiteId 
				objRS_private("programId") = strProgramId 
				objRS_private("contentsId") = strContentsId
				'objRS_private("category") = objRS_common("category") 
				'objRS_private("promotionId") = objRS_common("promotionId") 
				objRS_private("contentsName") = objRS_common("contentsName") 
				objRS_private("contentsType") = objRS_common("contentsType") 
				'objRS_private("parentsId") = objRS_common("parentsId") 
				'objRS_private("registerId") = objRS_common("registerId") 
				objRS_private("registerTime") = objRS_common("registerTime") 
				'objRS_private("isRaw") = objRS_common("isRaw") 
				objRS_private("contentsState") = objRS_common("contentsState") 
				objRS_private("contentsStateTime") = objRS_common("contentsStateTime") 
				'objRS_private("contentsStateHistory") = objRS_common("contentsStateHistory") 
				objRS_private("location") = objRS_common("location") 
				objRS_private("filename") = objRS_common("filename") 
				objRS_private("width") = objRS_common("width") 
				objRS_private("height") = objRS_common("height") 
				objRS_private("volume") = objRS_common("volume") 
				objRS_private("runningTime") = objRS_common("runningTime") 
				'objRS_private("verifier") = objRS_common("verifier") 
				'objRS_private("duration") = objRS_common("duration") 
				'objRS_private("videoCodec") = objRS_common("videoCodec") 
				'objRS_private("audioCodec") = objRS_common("audioCodec") 
				'objRS_private("encodingInfo") = objRS_common("encodingInfo") 
				objRS_private("encodingTime") = objRS_common("encodingTime") 
				'objRS_private("description") = objRS_common("description") 
				objRS_private("comment1") = objRS_common("comment1") 
				objRS_private("comment2") = objRS_common("comment2") 
				objRS_private("comment3") = objRS_common("comment3") 
				objRS_private("comment4") = objRS_common("comment4") 
				objRS_private("comment5") = objRS_common("comment5") 
				objRS_private("comment6") = objRS_common("comment6") 
				objRS_private("comment7") = objRS_common("comment7") 
				objRS_private("comment8") = objRS_common("comment8") 
				objRS_private("comment9") = objRS_common("comment9") 
				objRS_private("comment10") = objRS_common("comment10") 
				objRS_private("currentComment") = objRS_common("currentComment") 
				objRS_private("bgColor") = objRS_common("bgColor") 
				objRS_private("fgColor") = objRS_common("fgColor") 
				objRS_private("font") = objRS_common("font") 
				objRS_private("fontSize") = objRS_common("fontSize") 
				objRS_private("playSpeed") = objRS_common("playSpeed") 
				objRS_private("soundVolume") = objRS_common("soundVolume") 
				'objRS_private("promotionValueList") = objRS_common("promotionValueList") 
				'objRS_private("snapshotFile") = objRS_common("snapshotFile") 
				objRS_private("wizardFiles") = objRS_common("wizardFiles") 
				objRS_private("isPublic") = objRS_common("isPublic") 
				'objRS_private("serialNo") = objRS_common("serialNo") 
				'objRS_private("tvInputType") = objRS_common("tvInputType") 
				'objRS_private("tvChannel") = objRS_common("tvChannel") 
				'objRS_private("tvPortType") = objRS_common("tvPortType") 
				objRS_private("direction") = objRS_common("direction") 
				objRS_private("align") = objRS_common("align") 
				objRS_private("isUsed") = objRS_common("isUsed") 
				objRS_private("wizardXML") = objRS_common("wizardXML") 

				objRS_private.Update

				Response.Write "TRUE"
			else
				' exist record
				Response.Write "ALREADY"
			end if

			objRS_private.Close
			Set objRS_private = Nothing
		else
			' not verified contents
			Response.Write "CANT"
		end if
	else
		' not exist record
		Response.Write "FALSE"
	end if

	objRS_common.Close
	Set objRS_common = Nothing

	objConn.Close
	Set objConn = Nothing
%>
