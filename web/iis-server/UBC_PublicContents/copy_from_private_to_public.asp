<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<%
	Function CreateMainContentsID()
		Dim bCreate
		Dim strNewContentsID
		Dim strNewPath
		Dim strSQL

		Dim objRS_public
		Set objRS_public = Server.CreateObject("ADODB.Recordset")

		Dim objFSO
		set objFSO = Server.CreateObject("Scripting.FileSystemObject")

		bCreate = 0
		do while bCreate = 0
			strNewContentsID = CreateWindowsGUID()
			strNewPath = CONTENTS_PATH + "\" + strNewContentsID

			strSQL = "select * from ubc_contents where siteId = 'Public' and programId = 'Public' and contentsId = '" + strNewContentsID + "'"

			objRS_public.Open strSQL, objConn

			if objRS_public.EOF and not objFSO.FolderExists(strNewPath) then
				bCreate = 1
			end if

			objRS_public.Close
		loop

		CreateMainContentsID = strNewContentsID

		Set objRS_public = nothing
		Set objFSO = nothing
	End Function

	Function CreateSubContentsID()
		Dim bCreate
		Dim strNewContentsID
		Dim strSQL

		Dim objRS_public
		Set objRS_public = Server.CreateObject("ADODB.Recordset")

		bCreate = 0
		do while bCreate = 0
			strNewContentsID = CreateWindowsGUID()

			strSQL = "select * from ubc_contents where siteId = 'Public' and programId = 'Public' and contentsId = '" + strNewContentsID + "'"

			objRS_public.Open strSQL, objConn

			if objRS_public.EOF then
				bCreate = 1
			end if

			objRS_public.Close
		loop

		CreateSubContentsID = strNewContentsID

		Set objRS_public = nothing
	End Function

	Function CopyContents(source_path, target_path)
		''if exist oldpath, copy folder oldpath -> new(id)path
		'Dim objFSO
		'set objFSO = Server.CreateObject("Scripting.FileSystemObject")
		'if objFSO.FolderExists(source_path) Then
		'	objFso.CopyFolder source_path,target_path
		'end if
		'Set objFSO = Nothing


		'절대패스로 지정해야함
		Dim cmd
		cmd = PUBLIC_CONTENTS_TRIGGER + " " + source_path + " " + target_path

		'객체 생성
		Dim WShell
		Set WShell = Server.CreateObject("wscript.shell")

		'객체 생성 실패시 에러
		if WShell is nothing then
			CopyContents = 0
		else
			'외부 프로그램 실행후 리턴값 받아서
			Dim ret
			ret = WShell.Run(cmd, 0, true)

			'객체 소멸
			Set WShell = nothing

			CopyContents = 1
		end if
	End Function


	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' program id
	Dim strProgramId
	strProgramId = Request("programId")

	' contents id
	Dim strContentsId
	strContentsId = Request("contentsId")

	' contents id
	Dim strNewContentsId
	strNewContentsId = CreateMainContentsID()

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_contents where siteId = '" + strSiteId + "' and programId = '" + strProgramId + "' and contentsId = '" + strContentsId + "'"

	' create db object
	Dim objRS_private
	Set objRS_private = Server.CreateObject("ADODB.Recordset")
	objRS_private.Open strSQL, objConn

	if objRS_private.EOF then
		' not exist record (not exist private contents)
		Response.Write "FALSE"
	else
		' exist record

		' check contents-state
		Dim contentsState
		contentsState = CInt(objRS_private("contentsState"))

		if contentsState = 0 then
			' not verified contents
			Response.Write "NOTVERIFIED"
		else
			' only verified contents can be copied

			'Dim strOldPath
			'strOldPath = CONTENTS_PATH + "\" + strContentsId

			'Dim strNewPath
			'strNewPath = CONTENTS_PATH + "\" + strNewContentsId

			Dim ret
			'ret = CopyContents(source_path, target_path)
			ret = CopyContents(strContentsId, strNewContentsId)

			if ret = 0 then
				Response.Write "FALSE"
			else

				' create db object
				Dim objRS_public
				Set objRS_public = Server.CreateObject("ADODB.Recordset")
				objRS_public.Open "ubc_contents", objConn, , adLockOptimistic, adCmdTable

				' create new main record
				objRS_public.AddNew

				objRS_public("mgrId") = objRS_private("mgrId") 
				objRS_public("siteId") = "Public" 
				objRS_public("programId") = "Public"
				objRS_public("contentsId") = strNewContentsId
				objRS_public("category") = objRS_private("category") 
				'objRS_public("promotionId") = objRS_private("promotionId") 
				objRS_public("contentsName") = objRS_private("contentsName") 
				objRS_public("contentsType") = objRS_private("contentsType") 
				objRS_public("parentsId") = objRS_private("parentsId") 
				objRS_public("registerId") = objRS_private("registerId") 
				'objRS_public("registerTime") = objRS_private("registerTime") 
				objRS_public("registerTime") = now()
				'objRS_public("isRaw") = objRS_private("isRaw") 
				objRS_public("contentsState") = objRS_private("contentsState") 
				objRS_public("contentsStateTime") = objRS_private("contentsStateTime") 
				'objRS_public("contentsStateHistory") = objRS_private("contentsStateHistory") 
				objRS_public("location") = "/contents/" + LCase(strNewContentsId) + "/"
				objRS_public("filename") = objRS_private("filename") 
				objRS_public("width") = objRS_private("width") 
				objRS_public("height") = objRS_private("height") 
				objRS_public("volume") = objRS_private("volume") 
				objRS_public("runningTime") = objRS_private("runningTime") 
				objRS_public("verifier") = objRS_private("verifier") 
				objRS_public("duration") = objRS_private("duration") 
				'objRS_public("videoCodec") = objRS_private("videoCodec") 
				'objRS_public("audioCodec") = objRS_private("audioCodec") 
				'objRS_public("encodingInfo") = objRS_private("encodingInfo") 
				objRS_public("encodingTime") = objRS_private("encodingTime") 
				objRS_public("description") = objRS_private("description") 
				objRS_public("comment1") = objRS_private("comment1") 
				objRS_public("comment2") = objRS_private("comment2") 
				objRS_public("comment3") = objRS_private("comment3") 
				objRS_public("comment4") = objRS_private("comment4") 
				objRS_public("comment5") = objRS_private("comment5") 
				objRS_public("comment6") = objRS_private("comment6") 
				objRS_public("comment7") = objRS_private("comment7") 
				objRS_public("comment8") = objRS_private("comment8") 
				objRS_public("comment9") = objRS_private("comment9") 
				objRS_public("comment10") = objRS_private("comment10") 
				objRS_public("currentComment") = objRS_private("currentComment") 
				objRS_public("bgColor") = objRS_private("bgColor") 
				objRS_public("fgColor") = objRS_private("fgColor") 
				objRS_public("font") = objRS_private("font") 
				objRS_public("fontSize") = objRS_private("fontSize") 
				objRS_public("playSpeed") = objRS_private("playSpeed") 
				objRS_public("soundVolume") = objRS_private("soundVolume") 
				'objRS_public("promotionValueList") = objRS_private("promotionValueList") 
				'objRS_public("snapshotFile") = objRS_private("snapshotFile") 
				objRS_public("wizardFiles") = objRS_private("wizardFiles") 
				objRS_public("isPublic") = 1
				'objRS_public("serialNo") = objRS_private("serialNo") 
				'objRS_public("tvInputType") = objRS_private("tvInputType") 
				'objRS_public("tvChannel") = objRS_private("tvChannel") 
				'objRS_public("tvPortType") = objRS_private("tvPortType") 
				objRS_public("direction") = objRS_private("direction") 
				objRS_public("align") = objRS_private("align") 
				objRS_public("isUsed") = objRS_private("isUsed") 
				objRS_public("wizardXML") = objRS_private("wizardXML") 

				objRS_public("contentsCategory") = objRS_private("contentsCategory") 
				objRS_public("purpose") = objRS_private("purpose") 
				objRS_public("hostType") = objRS_private("hostType") 
				objRS_public("vertical") = objRS_private("vertical") 
				objRS_public("resolution") = objRS_private("resolution") 
				objRS_public("validationDate") = objRS_private("validationDate") 
				objRS_public("requester") = objRS_private("requester") 
				objRS_public("verifyMembers") = objRS_private("verifyMembers") 
				objRS_public("verifyTime") = objRS_private("verifyTime") 
				objRS_public("registerType") = objRS_private("registerType") 

				objRS_public.Update

				objRS_public.Close
				Set objRS_public = Nothing

				Dim strParentsId
				strParentsId = objRS_private("contentsId")

				objRS_private.Close
				Set objRS_private = Nothing

				' sql query
				'strSQL = "select * from ubc_contents where siteId = '" + strSiteId + "' and programId = '" + strProgramId + "' and parentsId = '" + strParentsId + "'"
				strSQL = "select * from ubc_contents where siteId = '" + strSiteId + "' and programId = '" + strProgramId + "' and location like '%" + strParentsId + "%' and contentsId <> '" + strParentsId + "'"

				' create db object
				Set objRS_private = Server.CreateObject("ADODB.Recordset")
				objRS_private.Open strSQL, objConn

				Do While Not objRS_private.EOF
					' create db object
					Set objRS_public = Server.CreateObject("ADODB.Recordset")
					objRS_public.Open "ubc_contents", objConn, , adLockOptimistic, adCmdTable

					Dim strNewSubContentsId
					strNewSubContentsId = CreateSubContentsID()

					Dim strNewLocation
					strNewLocation = Replace(objRS_private("location"), LCase(strParentsId), LCase(strNewContentsId))

					' create new sub record
					objRS_public.AddNew

					objRS_public("mgrId") = objRS_private("mgrId") 
					objRS_public("siteId") = "Public" 
					objRS_public("programId") = "Public"
					objRS_public("contentsId") = strNewSubContentsId
					objRS_public("category") = objRS_private("category") 
					'objRS_public("promotionId") = objRS_private("promotionId") 
					objRS_public("contentsName") = objRS_private("contentsName") 
					objRS_public("contentsType") = objRS_private("contentsType") 
					objRS_public("parentsId") = strNewContentsId
					objRS_public("registerId") = objRS_private("registerId") 
					'objRS_public("registerTime") = objRS_private("registerTime") 
					objRS_public("registerTime") = now()
					'objRS_public("isRaw") = objRS_private("isRaw") 
					objRS_public("contentsState") = objRS_private("contentsState") 
					objRS_public("contentsStateTime") = objRS_private("contentsStateTime") 
					'objRS_public("contentsStateHistory") = objRS_private("contentsStateHistory") 
					objRS_public("location") = strNewLocation
					objRS_public("filename") = objRS_private("filename") 
					objRS_public("width") = objRS_private("width") 
					objRS_public("height") = objRS_private("height") 
					objRS_public("volume") = objRS_private("volume") 
					objRS_public("runningTime") = objRS_private("runningTime") 
					objRS_public("verifier") = objRS_private("verifier") 
					objRS_public("duration") = objRS_private("duration") 
					'objRS_public("videoCodec") = objRS_private("videoCodec") 
					'objRS_public("audioCodec") = objRS_private("audioCodec") 
					'objRS_public("encodingInfo") = objRS_private("encodingInfo") 
					objRS_public("encodingTime") = objRS_private("encodingTime") 
					objRS_public("description") = objRS_private("description") 
					objRS_public("comment1") = objRS_private("comment1") 
					objRS_public("comment2") = objRS_private("comment2") 
					objRS_public("comment3") = objRS_private("comment3") 
					objRS_public("comment4") = objRS_private("comment4") 
					objRS_public("comment5") = objRS_private("comment5") 
					objRS_public("comment6") = objRS_private("comment6") 
					objRS_public("comment7") = objRS_private("comment7") 
					objRS_public("comment8") = objRS_private("comment8") 
					objRS_public("comment9") = objRS_private("comment9") 
					objRS_public("comment10") = objRS_private("comment10") 
					objRS_public("currentComment") = objRS_private("currentComment") 
					objRS_public("bgColor") = objRS_private("bgColor") 
					objRS_public("fgColor") = objRS_private("fgColor") 
					objRS_public("font") = objRS_private("font") 
					objRS_public("fontSize") = objRS_private("fontSize") 
					objRS_public("playSpeed") = objRS_private("playSpeed") 
					objRS_public("soundVolume") = objRS_private("soundVolume") 
					'objRS_public("promotionValueList") = objRS_private("promotionValueList") 
					'objRS_public("snapshotFile") = objRS_private("snapshotFile") 
					objRS_public("wizardFiles") = objRS_private("wizardFiles") 
					objRS_public("isPublic") = 1
					'objRS_public("serialNo") = objRS_private("serialNo") 
					'objRS_public("tvInputType") = objRS_private("tvInputType") 
					'objRS_public("tvChannel") = objRS_private("tvChannel") 
					'objRS_public("tvPortType") = objRS_private("tvPortType") 
					objRS_public("direction") = objRS_private("direction") 
					objRS_public("align") = objRS_private("align") 
					objRS_public("isUsed") = objRS_private("isUsed") 
					objRS_public("wizardXML") = objRS_private("wizardXML") 

					objRS_public("contentsCategory") = objRS_private("contentsCategory") 
					objRS_public("purpose") = objRS_private("purpose") 
					objRS_public("hostType") = objRS_private("hostType") 
					objRS_public("vertical") = objRS_private("vertical") 
					objRS_public("resolution") = objRS_private("resolution") 
					objRS_public("validationDate") = objRS_private("validationDate") 
					objRS_public("requester") = objRS_private("requester") 
					objRS_public("verifyMembers") = objRS_private("verifyMembers") 
					objRS_public("verifyTime") = objRS_private("verifyTime") 
					objRS_public("registerType") = objRS_private("registerType") 

					objRS_public.Update

					objRS_public.Close
					Set objRS_public = Nothing

					objRS_private.MoveNext
				Loop

				Response.Write "TRUE"
			end if
		end if
	end if

	objRS_private.Close
	Set objRS_private = Nothing

	objConn.Close
	Set objConn = Nothing
%>
