<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' host id
	Dim strHostId
	strHostId = Request("hostId")

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_hostview where mgrId <> ''"

	' make where clause
	if strSiteId <> "" then
		strSQL = strSQL + " and siteId = '" + strSiteId + "'"
	end if
	if strHostId <> "" then
		strSQL = strSQL + " and hostId = '" + strHostId + "'"
	end if

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	if Not ObjRS.EOF then
		' exist record

		Response.Write "OK" + vbCrLf

		Response.Write "mgrId=" + objRS("mgrId") + vbCrLf
		Response.Write "siteId=" + objRS("siteId") + vbCrLf
		Response.Write "hostId=" + objRS("hostId") + vbCrLf
		Response.Write "hostName=" + objRS("hostName") + vbCrLf
		Response.Write "domainName=" + objRS("domainName") + vbCrLf
		Response.Write "period=" + IntToString(objRS("period")) + vbCrLf
		Response.Write "description=" + objRS("description") + vbCrLf
		Response.Write "soundVolume=" + IntToString(objRS("soundVolume")) + vbCrLf
		Response.Write "mute=" + IntToString(objRS("mute")) + vbCrLf
		Response.Write "startupTime=" + objRS("startupTime") + vbCrLf
		Response.Write "shutdownTime=" + objRS("shutdownTime") + vbCrLf
		Response.Write "edition=" + objRS("edition") + vbCrLf
		Response.Write "version=" + objRS("version") + vbCrLf
		Response.Write "zipCode=" + objRS("zipCode") + vbCrLf
		Response.Write "addr1=" + objRS("addr1") + vbCrLf
		Response.Write "addr2=" + objRS("addr2") + vbCrLf
		Response.Write "addr3=" + objRS("addr3") + vbCrLf
		Response.Write "addr4=" + objRS("addr4") + vbCrLf
		Response.Write "screenshotPeriod=" + IntToString(objRS("screenshotPeriod")) + vbCrLf

		Dim tm_authDate
		tm_authDate = objRS("authDate")

		Response.Write "authDate=" + TimeToString(tm_authDate) + vbCrLf
		Response.Write "autoSchedule1=" + objRS("autoSchedule1") + vbCrLf
		Response.Write "autoSchedule2=" + objRS("autoSchedule2") + vbCrLf
		Response.Write "currentSchedule1=" + objRS("currentSchedule1") + vbCrLf
		Response.Write "currentSchedule2=" + objRS("currentSchedule2") + vbCrLf
		Response.Write "lastSchedule1=" + objRS("lastSchedule1") + vbCrLf
		Response.Write "lastSchedule2=" + objRS("lastSchedule2") + vbCrLf

		Dim tm_lastScheduleTime1
		tm_lastScheduleTime1 = objRS("lastScheduleTime1")

		Response.Write "lastScheduleTime1=" + TimeToString(tm_lastScheduleTime1) + vbCrLf

		Dim tm_lastScheduleTime2
		tm_lastScheduleTime2 = objRS("lastScheduleTime2")

		Response.Write "lastScheduleTime2=" + TimeToString(tm_lastScheduleTime2) + vbCrLf
		Response.Write "networkUse1=" + IntToString(objRS("networkUse1")) + vbCrLf
		Response.Write "networkUse2=" + IntToString(objRS("networkUse2")) + vbCrLf
		Response.Write "scheduleApplied1=" + IntToString(objRS("scheduleApplied1")) + vbCrLf
		Response.Write "scheduleApplied2=" + IntToString(objRS("scheduleApplied2")) + vbCrLf
		Response.Write "holiday=" + objRS("holiday") + vbCrLf
		Response.Write "soundDisplay=" + IntToString(objRS("soundDisplay")) + vbCrLf
		Response.Write "autoUpdateFlag=" + IntToString(objRS("autoUpdateFlag")) + vbCrLf
		Response.Write "playLogDayLimit=" + IntToString(objRS("playLogDayLimit")) + vbCrLf
		Response.Write "hddThreshold=" + IntToString(objRS("hddThreshold")) + vbCrLf
		Response.Write "playLogDayCriteria=" + objRS("playLogDayCriteria") + vbCrLf
		Response.Write "baseSchedule1=" + objRS("baseSchedule1") + vbCrLf
		Response.Write "baseSchedule2=" + objRS("baseSchedule2") + vbCrLf
		Response.Write "renderMode=" + IntToString(objRS("renderMode")) + vbCrLf
		Response.Write "category=" + objRS("category") + vbCrLf
		Response.Write "contentsDownloadTime=" + objRS("contentsDownloadTime") + vbCrLf
		Response.Write "download1State=" + IntToString(objRS("download1State")) + vbCrLf
		Response.Write "download2State=" + IntToString(objRS("download2State")) + vbCrLf
		Response.Write "progress1=" + objRS("progress1") + vbCrLf
		Response.Write "progress2=" + objRS("progress2") + vbCrLf
		Response.Write "monitorOff=" + IntToString(objRS("monitorOff")) + vbCrLf
		Response.Write "monitorOffList=" + objRS("monitorOffList") + vbCrLf
		Response.Write "hostType=" + IntToString(objRS("hostType")) + vbCrLf
		Response.Write "mmsCount=" + objRS("mmsCount") + vbCrLf
		Response.Write "winPassword=" + objRS("winPassword") + vbCrLf
		Response.Write "siteName=" + objRS("siteName") + vbCrLf
		Response.Write "chainNo=" + objRS.Fields.Item("chainNo") + vbCrLf
		Response.Write "businessCode=" + objRS("businessCode") + vbCrLf
		Response.Write "businessType=" + objRS("businessType") + vbCrLf
		Response.Write "phoneNo1=" + objRS("phoneNo1") + vbCrLf
		'Response.Write "mobileNo=" + objRS("mobileNo") + vbCrLf
		Response.Write "weekShutdownTime=" + objRS("weekShutdownTime") + vbCrLf
	else
		' not exist record
		Response.Write "Fail" + vbCrLf
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
