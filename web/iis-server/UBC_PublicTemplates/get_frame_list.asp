<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' sql query
	Dim strSQL
	strSQL = "select * from ubc_cframe"

	' customer
	Dim strCustomer
	strCustomer = Request("customer")
	if strCustomer <> "" then
		strSQL = strSQL + " where customer like '%" + strCustomer + "%'"
	end if

	' templateIdList
	Dim strTemplateIdList
	strTemplateIdList = Request("templateIdList")
	if strTemplateIdList <> "" then
		strSQL = strSQL + " and templateIdList in (" + strTemplateIdList + ")"
	end if

	' sort
	strSQL = strSQL + " order by mgrId,templateId,frameId"

	'debuging query-out
	'Response.Write strSQL + vbCrLf

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	Do While Not ObjRS.EOF

		Response.Write "templateId=" + objRS("templateId") + vbCrLf
		Response.Write "frameId=" + objRS("frameId") + vbCrLf
		Response.Write "grade=" + CStr(objRS("grade")) + vbCrLf
		Response.Write "x=" + CStr(objRS("x")) + vbCrLf
		Response.Write "y=" + CStr(objRS("y")) + vbCrLf
		Response.Write "width=" + CStr(objRS("width")) + vbCrLf
		Response.Write "height=" + CStr(objRS("height")) + vbCrLf
		Response.Write "isPIP=" + CStr(objRS("isPIP")) + vbCrLf
		Response.Write "borderStyle=" + objRS("borderStyle") + vbCrLf
		Response.Write "borderThickness=" + CStr(objRS("borderThickness")) + vbCrLf
		Response.Write "borderColor=" + objRS("borderColor") + vbCrLf
		Response.Write "cornerRadius=" + CStr(objRS("cornerRadius")) + vbCrLf
		Response.Write "description=" + objRS("description") + vbCrLf
		Response.Write "comment1=" + objRS("comment1") + vbCrLf
		Response.Write "comment2=" + objRS("comment2") + vbCrLf
		Response.Write "comment3=" + objRS("comment3") + vbCrLf
		Response.Write "alpha=" + CStr(objRS("alpha")) + vbCrLf
		Response.Write "isTV=" + CStr(objRS("isTV")) + vbCrLf
		Response.Write "customer=" + objRS("customer") + vbCrLf
		'Response.Write "zindex=" + CStr(objRS("zindex")) + vbCrLf

		ObjRS.MoveNext
	Loop


	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
