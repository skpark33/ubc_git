﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;
using System.Text;

/* MySQL DB */
using MySql.Data;
using MySql.Data.MySqlClient;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = "UBC SMS Login page";

    }
    
    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
        if( MyLoginValidation(Login1.UserName, Login1.Password) )
        {
            e.Authenticated = true;
        }
        else
        {
            e.Authenticated = false;
        }
    }

    protected bool MyLoginValidation(string userName, string passWord)
    {
        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;

        MySqlConnection conn = null;
        MySqlCommand cmd = null;
        
        bool isLoginValidated = false;

        try
        {
            conn = new MySqlConnection(connStr);
            conn.Open();

            // 스토어드 프로시저 사용할 경우
            //string cmdText = "usp_Sms_Login";
            //cmd = new MySqlCommand(cmdText, conn);
            //cmd.CommandType = CommandType.StoredProcedure;

            cmd = new MySqlCommand("SELECT * FROM utv_user WHERE userId = @in_userId AND password = @in_password", conn);
            
            cmd.Parameters.AddWithValue("@in_userId", userName);
            cmd.Parameters["@in_userId"].Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@in_password", passWord);
            cmd.Parameters["@in_password"].Direction = ParameterDirection.Input;

            isLoginValidated = cmd.ExecuteReader().HasRows;
        }
        catch (MySqlException mysqlEx)
        {
            string msg = string.Format("MySqlException Occured {0}:{1}", mysqlEx.Number, mysqlEx.Message);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }
        finally
        {
            if (conn != null) conn.Close();
        }

        return isLoginValidated;
       
    }
}