﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ConfigVariables.aspx.cs" Inherits="ConfigVariables_ConfigVariables" Title="Configuration Variables" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<!-- 그리드뷰 정렬 (참고) http://tablesorter.com/docs/ //-->
<script src="../Scripts/jquery.tablesorter.min.js" type="text/javascript"></script>

<style type="text/css">
   
    th
    {
        cursor:pointer;
        text-align:left; 
        text-decoration: underline;
    }
    th.headerSortUp 
    {     
        background-image: url(../images/asc.gif);
        background-position: right center;
        background-repeat:no-repeat; 
    }
    th.headerSortDown 
    {     
        background-image: url(../images/desc.gif);   
        background-position: right center;
        background-repeat:no-repeat; 
    }     
    td div
    {
        overflow:auto;
    }
    
</style>

<script language="javascript" type="text/javascript">

    function pageLoad() {
        
        // 헤더 클릭시 소팅
        $("#GridView1").tablesorter(
        {
            headers: { 0: { sorter: false} }
        });
        
        // 헤더의 체크박스 클릭시 전체 체크박스 선택
        $("#chkHeader").click(function () {
            var isChecked = $(this).is(':checked');
            $("[id*=chkRow]").attr('checked', isChecked);
        });

        // 팁을 한번만 보여주도록 숨겨놓은 값으로 제어함
        $("div.section_tip").css("display", $("#hdnTipDisplay").val());       
    }

    function DoAction() {
        var checkedCount = $("[id*=chkRow]:checked").length;

        if (checkedCount <= 0) {
            alert("선택된 항목이 없습니다");
            return false;
        }
        else {
            if (!confirm(checkedCount + " 개 항목에 적용합니다. 계속하시겠습니까?"))
                return false;
        }
        return true;
    }

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" >
<ContentTemplate>
      
    <div class="section" >
        <label>Customer</label>
        <asp:DropDownList ID="ddlCustomer" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true" />
        <asp:TextBox ID="txtCustomer" Width="100px" runat="server" AutoPostBack="false" Visible="false" />
        <label>Scope</label>
        <asp:DropDownList ID="ddlScope" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true" />
        <asp:TextBox ID="txtScope" Width="100px" runat="server" AutoPostBack="false" Visible="false" />
        <label>File Name</label>
        <asp:DropDownList ID="ddlFileName" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true" />
        <asp:TextBox ID="txtFileName" Width="150px" runat="server" AutoPostBack="false" Visible="false" />
        <label>App Name</label>
        <asp:DropDownList ID="ddlAppName" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true" />
        <asp:TextBox ID="txtAppName" Width="100px" runat="server" AutoPostBack="false" Visible="false" />
        <label>Key Name</label>
        <asp:DropDownList ID="ddlKeyName" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true" />
        <asp:TextBox ID="txtKeyName" Width="150px" runat="server" AutoPostBack="false" Visible="false" />
        <asp:CheckBox ID="chkIsAll" Text="All" runat="server" AutoPostBack="false" oncheckedchanged="chkIsAll_CheckedChanged" />
        <asp:Button ID="btnRefresh" runat="server" Text="Refresh"  onclick="btnRefresh_Click" />
        <asp:Button ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" />
        <asp:Button ID="btnDel" runat="server" Text="Delete" onclick="btnDel_Click" />
    </div>
    <asp:UpdatePanel ID="InsertPanel" runat="server" Visible="false" >
    <ContentTemplate>
    <div class="section">
        <table class="tableType2" style="width: 100%;">
            <thead>
            </thead>
            <tr>
                <th style="width: 80px">
                    <label>Customer</label>
                </td>
                <td style="width: 320px">
                    <asp:DropDownList ID="DropDownList1" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlEdit_SelectedIndexChanged" AutoPostBack="true" />
                    <asp:TextBox ID="TextBox1" Width="100%" runat="server" AutoPostBack="false" Visible="false" />
                </td>
                <th style="width: 80px">
                    <label>Scope</label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlEdit_SelectedIndexChanged" AutoPostBack="true" />
                    <asp:TextBox ID="TextBox2" Width="100%" runat="server" AutoPostBack="false" Visible="false" />
                </td>
                <th style="width: 80px">
                    <label>File Name</label>
                </td>
                <td style="width: 320px">
                    <asp:DropDownList ID="DropDownList3" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlEdit_SelectedIndexChanged" AutoPostBack="true" />
                    <asp:TextBox ID="TextBox3" Width="100%" runat="server" AutoPostBack="false" Visible="false" />
                </td>
            </tr>
            <tr>
                <th>
                    <label>App Name</label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList4" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlEdit_SelectedIndexChanged" AutoPostBack="true" />
                    <asp:TextBox ID="TextBox4" Width="100%" runat="server" AutoPostBack="false" Visible="false" />
                </td>
                <th>
                    <label>Key Name</label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList5" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlEdit_SelectedIndexChanged" AutoPostBack="true" />
                    <asp:TextBox ID="TextBox5" Width="100%" runat="server" AutoPostBack="false" Visible="false" />
                </td>
                <th>
                    <label>Value</label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox6" Width="99%" runat="server" AutoPostBack="false" />
                </td>
            </tr>
            <tr>
                <th>
                    <label>Start Date</label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox7" Width="100px" runat="server" AutoPostBack="false" /><label>( ex: YYYY-MM-DD )</label>
                </td>
                <th>
                    <label>Remark</label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox8" Width="99%" runat="server" AutoPostBack="false" />
                </td>
            </tr>
        </table>
        <asp:Button ID="btnInsert" runat="server" Text="Save" OnClick="btnInsert_Click" />
        <asp:Button ID="btnInsertCancel" runat="server" Text="Cancel" onclick="btnInsertCancel_Click" />
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    <div class="section_tip">
        <em class="tip">TIP!</em> 다중정렬: Shift + 헤더 클릭<span class="close" onclick="$(this).parent().slideUp(); $('#hdnTipDisplay').val('none');"> X Close</span>
        <asp:HiddenField ID="hdnTipDisplay" runat="server" Value="block" ClientIDMode="Static"/>
    </div>
    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
    <ProgressTemplate>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center" CssClass="section_loading">   
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/loading.gif" />
            <br />작업중 입니다. 잠시만 기다려 주세요.
        </asp:Panel>
    </ProgressTemplate>
    </asp:UpdateProgress>

    <div >   
        <asp:GridView ID="GridView1" runat="server" 
                OnRowDataBound="GridView1_RowDataBound" 
                OnPreRender="GridView1_PreRender" 
                AutoGenerateColumns="false" 
                ClientIDMode="Static"
                CssClass="tableType1" DataKeyNames="customer,scope,filename,appname,keyname,startdate" >  
        
        <Columns>
            
            <asp:TemplateField HeaderText="선택"> 
                <HeaderStyle Width="30px" CssClass="header" />                
                <HeaderTemplate>
                    <input id="chkHeader" type="checkbox" />
                </HeaderTemplate>            
                <ItemStyle Width="30px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:CheckBox ID="chkRow" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>    

             <asp:TemplateField HeaderText="SEQ">   
                <HeaderStyle Width="40px" CssClass="font_85" />
                <ItemStyle Width="40px" HorizontalAlign="Right" />
                <ItemTemplate>     
                    <div><span class="font_85"><%# Container.DataItemIndex+1 %></span></div>            
                </ItemTemplate>
            </asp:TemplateField>            
            
            <asp:TemplateField HeaderText="Customer">
                <HeaderStyle Width="100px" CssClass="font_85" />
                <ItemStyle Width="100px" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("customer")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Scope">
                <HeaderStyle Width="100px" CssClass="font_85" />
                <ItemStyle Width="100px" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("scope")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Filename">
                <HeaderStyle Width="120px" CssClass="font_85" />
                <ItemStyle Width="120px" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("fileName")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="App Name">
                <HeaderStyle Width="100px" CssClass="font_85" />
                <ItemStyle Width="100px" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("appName")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Key Name">
                <HeaderStyle Width="150px" CssClass="font_85" />
                <ItemStyle Width="150px" />
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("keyName")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Value">
                <HeaderStyle Width="80px" CssClass="font_85" />
                <ItemStyle Width="80px" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("value")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Start Date">
                <HeaderStyle Width="120px" CssClass="font_85" />
                <ItemStyle Width="120px" HorizontalAlign="Center" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("startDate") %></span></div>          
                </ItemTemplate>
            </asp:TemplateField> 
            
            <asp:TemplateField HeaderText="End Date">
                <HeaderStyle Width="120px" CssClass="font_85" />                
                <ItemStyle Width="120px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("endDate") %></span></div>          
                </ItemTemplate>
            </asp:TemplateField>                        

            <asp:TemplateField HeaderText="Reg Date">
                <HeaderStyle Width="120px" CssClass="font_85" />                
                <ItemStyle Width="120px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("regDate") %></span></div>          
                </ItemTemplate>
            </asp:TemplateField>                        

            <asp:TemplateField HeaderText="Reg User">
                <HeaderStyle Width="70px" CssClass="font_85" />
                <ItemStyle Width="70px" />                 
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("regUser")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Remark">   
                <HeaderStyle Width="10%" CssClass="font_85" />
                <ItemStyle Width="10%" />
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("remark")%></span></div> 
                </ItemTemplate>
            </asp:TemplateField>

        </Columns> 
        <EmptyDataRowStyle HorizontalAlign="Center" />
        <EmptyDataTemplate>
            No data found
        </EmptyDataTemplate>         
        </asp:GridView>
    </div>

</ContentTemplate>
</asp:UpdatePanel> 
</asp:Content>

