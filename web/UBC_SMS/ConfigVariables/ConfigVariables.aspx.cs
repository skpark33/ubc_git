﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/* MySQL DB */
using MySql.Data;
using MySql.Data.MySqlClient;

public partial class ConfigVariables_ConfigVariables : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initControl();
            BindData();
        }
    }

    protected void initControl()
    {
        this.Title = "List of Configurion Variables";

        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;
        MySqlConnection conn = new MySqlConnection(connStr);
        conn.Open();

        MySqlCommand cmd = new MySqlCommand("SELECT distinct customer as code, customer as name FROM utv_configvariables where endDate is null ORDER BY name", conn);
        MySqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlCustomer, dt, "전체", true);
        dt.Rows.RemoveAt(0);
        BindDropDownList(DropDownList1, dt);
        dr.Close();

        cmd = new MySqlCommand("SELECT distinct scope AS code, scope AS name FROM utv_configvariables where endDate is null ORDER BY name", conn);
        dr = cmd.ExecuteReader();
        dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlScope, dt, "전체", true);
        dt.Rows.RemoveAt(0);
        BindDropDownList(DropDownList2, dt);
        dr.Close();

        cmd = new MySqlCommand("SELECT distinct filename AS code, filename AS name FROM utv_configvariables where endDate is null ORDER BY name", conn);
        dr = cmd.ExecuteReader();
        dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlFileName, dt, "전체", true);
        dt.Rows.RemoveAt(0);
        BindDropDownList(DropDownList3, dt);
        dr.Close();

        cmd = new MySqlCommand("SELECT distinct appname AS code, appname AS name FROM utv_configvariables where endDate is null ORDER BY name", conn);
        dr = cmd.ExecuteReader();
        dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlAppName, dt, "전체", true);
        dt.Rows.RemoveAt(0);
        BindDropDownList(DropDownList4, dt);
        dr.Close();

        cmd = new MySqlCommand("SELECT distinct keyname AS code, keyname AS name FROM utv_configvariables where endDate is null ORDER BY name", conn);
        dr = cmd.ExecuteReader();
        dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlKeyName, dt, "전체", true);
        dt.Rows.RemoveAt(0);
        BindDropDownList(DropDownList5, dt);
        dr.Close();

        TextBox7.Text = System.DateTime.Now.ToString("yyyy-MM-dd");

        conn.Close();
    }

    protected void BindData()
    {
        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;

        MySqlConnection conn = null;
        MySqlCommand cmd = null;

        try
        {
            conn = new MySqlConnection(connStr);
            conn.Open();

            string filter = "";

            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue.ToString()) ||
                (txtCustomer.Visible && !string.IsNullOrEmpty(txtCustomer.Text)))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND customer = @customer ";
                else
                    filter += " WHERE customer = @customer ";
            }

            if (!string.IsNullOrEmpty(ddlScope.SelectedValue.ToString()) || 
                (txtScope.Visible && !string.IsNullOrEmpty(txtScope.Text)))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND scope like @scope ";
                else
                    filter += " WHERE scope like @scope ";
            }

            if (!string.IsNullOrEmpty(ddlFileName.SelectedValue.ToString()) ||
                (txtFileName.Visible && !string.IsNullOrEmpty(txtFileName.Text)))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND fileName like @filename ";
                else
                    filter += " WHERE fileName like @filename ";
            }

            if (!string.IsNullOrEmpty(ddlAppName.SelectedValue.ToString()) ||
                (txtAppName.Visible && !string.IsNullOrEmpty(txtAppName.Text)))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND appName like @appname ";
                else
                    filter += " WHERE appName like @appname ";
            }

            if (!string.IsNullOrEmpty(ddlKeyName.SelectedValue.ToString()) ||
                (txtKeyName.Visible && !string.IsNullOrEmpty(txtKeyName.Text)))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND keyName like @keyname ";
                else
                    filter += " WHERE keyName like @keyname ";
            }

            if (!chkIsAll.Checked)
            {
                //if (!string.IsNullOrEmpty(filter))
                //    filter += " AND DATE_FORMAT( ifnull(endDate, sysdate()), '%Y%m%d' ) >= DATE_FORMAT( sysdate(), '%Y%m%d' ) ";
                //else
                //    filter += " WHERE DATE_FORMAT( ifnull(endDate, sysdate()), '%Y%m%d' ) >= DATE_FORMAT( sysdate(), '%Y%m%d' ) ";
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND ifnull(endDate, sysdate()) >= sysdate() ";
                else
                    filter += " WHERE ifnull(endDate, sysdate()) >= sysdate() ";
            }

            string cmdText = @"SELECT customer
                                    , scope
                                    , fileName
                                    , appName
                                    , keyName
                                    , value
                                    , DATE_FORMAT(startDate, '%Y-%m-%d %H:%i:%s' ) AS startDate
                                    , DATE_FORMAT(endDate, '%Y-%m-%d %H:%i:%s' ) AS endDate
                                    , DATE_FORMAT(regDate, '%Y-%m-%d %H:%i:%s' ) AS regDate
                                    , regUser
                                    , remark
                                FROM utv_configvariables ";

            if (!string.IsNullOrEmpty(filter)) cmdText += filter;
            cmdText += " ORDER BY customer, scope, filename, appname";

            cmd = new MySqlCommand(cmdText, conn);

            //// 스토어드 프로시저 사용할 경우
            //string cmdText = "usp_Sms_ServerProcessList";
            //cmd = new MySqlCommand(cmdText, conn);
            //cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@customer", (txtCustomer.Visible ? txtCustomer.Text : ddlCustomer.SelectedValue.ToString())).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@scope", (txtScope.Visible ? txtScope.Text : ddlScope.SelectedValue.ToString())).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@filename", (txtFileName.Visible ? txtFileName.Text : ddlFileName.SelectedValue.ToString())).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@appname", (txtAppName.Visible ? txtAppName.Text : ddlAppName.SelectedValue.ToString())).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@keyname", (txtKeyName.Visible ? txtKeyName.Text : ddlKeyName.SelectedValue.ToString())).Direction = ParameterDirection.Input;

            MySqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch (MySqlException mysqlEx)
        {
            string msg = string.Format("MySqlException Occured {0}:{1}", mysqlEx.Number, mysqlEx.Message);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }
        finally
        {
            if (conn != null) conn.Close();
        }

        if (GridView1.Rows.Count > 0)
        {
            // jquery.tablesorter 를 이용하기위해서 필요한 설정!!
            // 시작
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            // 끝
            // jquery.tablesorter 를 이용하기위해서 필요한 설정!!

            //btnExcel.Enabled = true;
        }
        else
        {
            //btnExcel.Enabled = false;
        }
    }

    public void BindDropDownList(DropDownList ddlObj, DataTable dt)
    {
        ddlObj.Items.Clear();
        ddlObj.DataSource = dt;
        ddlObj.DataBind();
    }

    public void BindDropDownList(DropDownList ddlObj, DataTable dt, string title, bool directInput)
    {
        if (!string.IsNullOrEmpty(title))
        {
            DataRow dr = dt.NewRow();
            dr[0] = "";
            dr[1] = title;

            dt.Rows.InsertAt(dr, 0);
        }

        if (directInput)
        {
            DataRow dr = dt.NewRow();
            dr[0] = "직접입력";
            dr[1] = "직접입력";

            dt.Rows.Add(dr);
        }

        ddlObj.Items.Clear();
        ddlObj.DataSource = dt;
        ddlObj.DataBind();
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl = null;
        TextBox txt = null;

        if (sender.Equals(ddlCustomer))
        {
            ddl = ddlCustomer;
            txt = txtCustomer;
        }
        else if (sender.Equals(ddlScope))
        {
            ddl = ddlScope;
            txt = txtScope;
        }
        else if (sender.Equals(ddlFileName))
        {
            ddl = ddlFileName;
            txt = txtFileName;
        }
        else if (sender.Equals(ddlAppName))
        {
            ddl = ddlAppName;
            txt = txtAppName;
        }
        else if (sender.Equals(ddlKeyName))
        {
            ddl = ddlKeyName;
            txt = txtKeyName;
        }

        if (ddl != null && txt != null)
        {
            txt.Visible = (ddl.SelectedValue.ToString() == "직접입력");
            txt.Width = ddl.Width;
            if (!txt.Visible) BindData();
            else txt.Focus();
        }
    }

    protected void chkIsAll_CheckedChanged(object sender, EventArgs e)
    {
        BindData();
    }

    protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;

            string endDate = rowView["endDate"].ToString();
            string regDate = rowView["regDate"].ToString();

            if (!string.IsNullOrEmpty(endDate))
            {
                e.Row.CssClass = "deleted";
            }
            else if (!string.IsNullOrEmpty(regDate) && regDate.Substring(0, 10) == System.DateTime.Now.ToString("yyyy-MM-dd"))
            {
                e.Row.CssClass = "new";
            }
        }
    }

    protected void GridView1_PreRender(Object sender, EventArgs e)
    {
        if (GridView1.Rows.Count > 0)
        {
            // 첫번째 컬럼은 체크박스 이므로 다른 Sortable 컬럼의 스타일을 없앤다
            GridView1.HeaderRow.Cells[0].Style.Add("cursor", "default");
            GridView1.HeaderRow.Cells[0].Style.Add("text-align", "center");
        }
    }

    protected void ddlEdit_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl = null;
        TextBox txt = null;

        if (sender.Equals(DropDownList1))
        {
            ddl = DropDownList1;
            txt = TextBox1;
        }
        else if (sender.Equals(DropDownList2))
        {
            ddl = DropDownList2;
            txt = TextBox2;
        }
        else if (sender.Equals(DropDownList3))
        {
            ddl = DropDownList3;
            txt = TextBox3;
        }
        else if (sender.Equals(DropDownList4))
        {
            ddl = DropDownList4;
            txt = TextBox4;
        }
        else if (sender.Equals(DropDownList5))
        {
            ddl = DropDownList5;
            txt = TextBox5;
        }

        if (ddl != null && txt != null)
        {
            txt.Visible = (ddl.SelectedValue.ToString() == "직접입력");
            txt.Width = ddl.Width;
            if (!txt.Visible) BindData();
            else txt.Focus();
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        DropDownList1.SelectedValue = (ddlCustomer.SelectedIndex == 0 ? ddlCustomer.Items[1].Text : ddlCustomer.SelectedValue);
        TextBox1.Text = txtCustomer.Text;
        TextBox1.Visible = txtCustomer.Visible;

        DropDownList2.SelectedValue = (ddlScope.SelectedIndex == 0 ? ddlScope.Items[1].Text : ddlScope.SelectedValue);
        TextBox2.Text = txtScope.Text;
        TextBox2.Visible = txtScope.Visible;

        DropDownList3.SelectedValue = (ddlFileName.SelectedIndex == 0 ? ddlFileName.Items[1].Text : ddlFileName.SelectedValue);
        TextBox3.Text = txtFileName.Text;
        TextBox3.Visible = txtFileName.Visible;

        DropDownList4.SelectedValue = (ddlAppName.SelectedIndex == 0 ? ddlAppName.Items[1].Text : ddlAppName.SelectedValue);
        TextBox4.Text = txtAppName.Text;
        TextBox4.Visible = txtAppName.Visible;

        DropDownList5.SelectedValue = (ddlKeyName.SelectedIndex == 0 ? ddlKeyName.Items[1].Text : ddlKeyName.SelectedValue);
        TextBox5.Text = txtKeyName.Text;
        TextBox5.Visible = txtKeyName.Visible;

        TextBox6.Text = "";
        TextBox7.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
        TextBox8.Text = "";

        btnNew.Visible = false;
        InsertPanel.Visible = true;
    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        string customer = (TextBox1.Visible ? TextBox1.Text : DropDownList1.SelectedValue.ToString());
        string scope = (TextBox2.Visible ? TextBox2.Text : DropDownList2.SelectedValue.ToString());
        string fileName = (TextBox3.Visible ? TextBox3.Text : DropDownList3.SelectedValue.ToString());
        string appName = (TextBox4.Visible ? TextBox4.Text : DropDownList4.SelectedValue.ToString());
        string keyName = (TextBox5.Visible ? TextBox5.Text : DropDownList5.SelectedValue.ToString());
        string value = TextBox6.Text;
        string startDate = TextBox7.Text;
        string regUser = User.Identity.Name;
        string remark = TextBox8.Text;

        string msg = "";

        customer.Trim();
        scope.Trim();
        fileName.Trim();
        appName.Trim();
        keyName.Trim();
        value.Trim();
        startDate.Trim();

        if (string.IsNullOrEmpty(customer)) msg = "Customer 를 입력하세요";
        else if (string.IsNullOrEmpty(scope)) msg = "Scope 를 입력하세요";
        else if (string.IsNullOrEmpty(fileName)) msg = "Filename 을 입력하세요";
        else if (string.IsNullOrEmpty(appName)) msg = "App Name 을 입력하세요";
        else if (string.IsNullOrEmpty(keyName)) msg = "Key Name 을 입력하세요";
        else if (string.IsNullOrEmpty(value)) msg = "Value 를 입력하세요";
        else if (startDate.Length < 10) msg = "Start Date 를 입력하세요";

        if (!string.IsNullOrEmpty(msg))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
            return;
        }

        if (InsertData())
        {
            btnInsertCancel_Click(sender, e);
            BindData();
        }
    }

    protected bool InsertData()
    {
        string customer = (TextBox1.Visible ? TextBox1.Text : DropDownList1.SelectedValue.ToString());
        string scope = (TextBox2.Visible ? TextBox2.Text : DropDownList2.SelectedValue.ToString());
        string fileName = (TextBox3.Visible ? TextBox3.Text : DropDownList3.SelectedValue.ToString());
        string appName = (TextBox4.Visible ? TextBox4.Text : DropDownList4.SelectedValue.ToString());
        string keyName = (TextBox5.Visible ? TextBox5.Text : DropDownList5.SelectedValue.ToString());
        string value = TextBox6.Text;
        string startDate = TextBox7.Text;
        string regUser = User.Identity.Name;
        string remark = TextBox8.Text;

        customer.Trim();
        scope.Trim();
        fileName.Trim();
        appName.Trim();
        keyName.Trim();
        value.Trim();
        startDate.Trim();

        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;

        MySqlConnection conn = null;
        MySqlCommand cmd = null;

        string msg = "";
        bool result = true;

        try
        {
            conn = new MySqlConnection(connStr);
            conn.Open();

            string cmdText = @"insert into utv_configvariables (customer, scope, fileName, appName, keyName, value, startDate, regDate, regUser, remark)
                                values ( @customer, @scope, @filename, @appname, @keyname, @value, @startdate, sysdate(), @reguser, @remark);";

            cmd = new MySqlCommand(cmdText, conn);

            //// 스토어드 프로시저 사용할 경우
            //string cmdText = "usp_Sms_ServerProcessList";
            //cmd = new MySqlCommand(cmdText, conn);
            //cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@customer", customer).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@scope", scope).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@filename", fileName).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@appname", appName).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@keyname", keyName).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@value", value).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@startdate", startDate).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@reguser", User.Identity.Name).Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@remark", TextBox8.Text).Direction = ParameterDirection.Input;

            int cnt = cmd.ExecuteNonQuery();
        }
        catch (MySqlException mysqlEx)
        {
            result = false;
            msg = string.Format("MySqlException Occured {0}:{1}", mysqlEx.Number, mysqlEx.Message);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }
        catch (Exception err)
        {
            result = false;
            msg = string.Format("Exception Occured {0}", err.Message);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }
        finally
        {
            if (conn != null) conn.Close();
        }

        if (result)
        {
            msg = "저장되었습니다.";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }

        return result;
    }

    protected void btnInsertCancel_Click(object sender, EventArgs e)
    {
        btnNew.Visible = true;
        InsertPanel.Visible = false;
    }

    protected void btnDel_Click(object sender, EventArgs e)
    {
        DeleteData();

        BindData();
    }

    protected void DeleteData()
    {
        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;

        MySqlConnection conn = null;
        MySqlCommand cmd = null;

        try
        {
            conn = new MySqlConnection(connStr);
            conn.Open();

            string cmdText = @"UPDATE utv_configvariables SET regUser = @regUser, endDate = now()
                                WHERE customer = @customer
                                  AND scope = @scope
                                  AND fileName = @fileName
                                  AND appName = @appName
                                  AND keyName = @keyName
                                  AND startDate = @startDate ";

            int count = 0;
            int updatedCount = 0;
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (((CheckBox)row.Cells[0].FindControl("chkRow")).Checked)
                {
                    count++;

                    cmd = new MySqlCommand(cmdText, conn);

                    cmd.Parameters.AddWithValue("@customer", GridView1.DataKeys[row.RowIndex]["customer"].ToString()).Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@scope", GridView1.DataKeys[row.RowIndex]["scope"].ToString()).Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@fileName", GridView1.DataKeys[row.RowIndex]["filename"].ToString()).Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@appName", GridView1.DataKeys[row.RowIndex]["appname"].ToString()).Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@keyName", GridView1.DataKeys[row.RowIndex]["keyname"].ToString()).Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@startDate", GridView1.DataKeys[row.RowIndex]["startdate"].ToString()).Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@regUser", "admin").Direction = ParameterDirection.Input;

                    if (cmd.ExecuteNonQuery() > 0) updatedCount++;
                }
            }

            string msg = string.Format("Total {0} of selected {1} Updated!!", updatedCount.ToString(), count.ToString());
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
            BindData();
        }
        catch (MySqlException mysqlEx)
        {
            string msg = string.Format("MySqlException Occured {0}:{1}", mysqlEx.Number, mysqlEx.Message);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }
        finally
        {
            if (conn != null) conn.Close();
        }
    }
}
