﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

<!-- 그리드뷰 정렬 (참고) http://tablesorter.com/docs/ //-->
<script src="./Scripts/jquery.tablesorter.min.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">

    $(document).ready(function () {

        // 헤더 클릭시 소팅
        $("#GridView1").tablesorter();

    });

</script>

<style type="text/css">
    th
    {
        cursor:pointer;
        text-align:left; 
        text-decoration: underline;
    }
    th.headerSortUp 
    {     
        background-image: url(./images/asc.gif);
        background-position: right center;
        background-repeat:no-repeat; 
    }
    th.headerSortDown 
    {     
        background-image: url(./images/desc.gif);   
        background-position: right center;
        background-repeat:no-repeat; 
    } 
    td div
    {
        overflow:auto;
    }
    
</style>

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        서버프로세스 요약
    </h2>
    <div class="section" style="width:50%">
        <asp:GridView ID="GridView1" runat="server"
                    AutoGenerateColumns="false" 
                    ClientIDMode="Static"
                    CssClass="tableType2" >

        <Columns>
            
            <asp:TemplateField HeaderText="Server ID">
                <ItemTemplate>
                        <div><%# Eval("serverId")%></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Server IP">
                <HeaderStyle Width="120px" />
                <ItemTemplate>
                        <div><%# Eval("serverIp")%></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Active Count">
                <HeaderStyle Width="120px" />
                <ItemStyle HorizontalAlign="Right" />
                <ItemTemplate>
                        <div><%# Eval("cnt_active")%></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Inactive Count">
                <HeaderStyle Width="120px" />
                <ItemStyle HorizontalAlign="Right" />
                <ItemTemplate>
                        <div><%# Eval("cnt_inactive")%></div>          
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>

        </asp:GridView>

    </div>

</asp:Content>
