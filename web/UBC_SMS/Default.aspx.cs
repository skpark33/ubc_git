﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;
using System.Text;

/* MySQL DB */
using MySql.Data;
using MySql.Data.MySqlClient;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initControl();
            BindData();
        }  
    }
    protected void initControl()
    {
        this.Title = "UBC서버감시";
    }
    
    protected void BindData()
    {
        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;

        MySqlConnection conn = null;
        MySqlCommand cmd = null;

        try
        {
            conn = new MySqlConnection(connStr);
            conn.Open();

            string cmdText = @"
                      SELECT  MAX(S.serverId)     AS serverId
                            , MAX(S.serverIp)     AS serverIp
                            , SUM(S.active_cnt)   AS cnt_active
                            , SUM(S.inactive_cnt) AS cnt_inactive
                        FROM (
                               SELECT  COUNT(status) AS active_cnt
                                      ,0             AS inactive_cnt
                                      ,MAX(serverId) AS serverId
                                      ,MAX(serverIp) AS serverIp
                                 FROM utv_serverprocess 
                                WHERE status = 'Active'
                                GROUP BY serverId
                                UNION ALL
                               SELECT  0             AS active_cnt
                                      ,COUNT(status) AS inactive_cnt
                                      ,MAX(serverId) AS serverId
                                      ,MAX(serverIp) AS serverIp
                                 FROM utv_serverprocess 
                                WHERE status <> 'Active'
                                GROUP BY serverId 
                              ) S
                        GROUP BY S.serverId";

            cmd = new MySqlCommand(cmdText, conn);
            
            MySqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch (MySqlException mysqlEx)
        {
            string msg = string.Format("MySqlException Occured {0}:{1}", mysqlEx.Number, mysqlEx.Message);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }
        finally
        {
            if (conn != null) conn.Close();
        }

        if (GridView1.Rows.Count > 0)
        {
            // jquery.tablesorter 를 이용하기위해서 필요한 설정!!
            // 시작
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            // 끝
            // jquery.tablesorter 를 이용하기위해서 필요한 설정!!
        }
        
    }

}
