﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ServerProcess.aspx.cs" Inherits="ServerProcess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

<!-- 그리드뷰 정렬 (참고) http://tablesorter.com/docs/ //-->
<script src="../Scripts/jquery.tablesorter.min.js" type="text/javascript"></script>

<style type="text/css">
   
    th
    {
        cursor:pointer;
        text-align:left; 
        text-decoration: underline;
    }
    th.headerSortUp 
    {     
        background-image: url(../images/asc.gif);
        background-position: right center;
        background-repeat:no-repeat; 
    }
    th.headerSortDown 
    {     
        background-image: url(../images/desc.gif);   
        background-position: right center;
        background-repeat:no-repeat; 
    }     
    td div
    {
        overflow:auto;
    }
    
</style>

<script language="javascript" type="text/javascript">

    function pageLoad() {
        
        // 헤더 클릭시 소팅
        $("#GridView1").tablesorter(
        {
            headers: { 0: { sorter: false} }
        });
        
        // 헤더의 체크박스 클릭시 전체 체크박스 선택
        $("#chkHeader").click(function () {
            var isChecked = $(this).is(':checked');
            $("[id*=chkRow]").attr('checked', isChecked);
        });

        // 팁을 한번만 보여주도록 숨겨놓은 값으로 제어함
        $("div.section_tip").css("display", $("#hdnTipDisplay").val());       
    }

    function DoAction() {
        var checkedCount = $("[id*=chkRow]:checked").length;

        if (checkedCount <= 0) {
            alert("선택된 항목이 없습니다");
            return false;
        }
        else {
            if (!confirm(checkedCount + " 개 항목에 적용합니다. 계속하시겠습니까?"))
                return false;
        }
        return true;
    }

</script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
<Triggers>
    <asp:PostBackTrigger ControlID="btnExcel" />
</Triggers>
<ContentTemplate>

    <div class="section" >
        <label>Status</label>
        <asp:DropDownList ID="ddlStatus" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
        <label>Customer</label>
        <asp:DropDownList ID="ddlCustomer" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true" >
        </asp:DropDownList>
        <label>Server ID</label>
        <asp:DropDownList ID="ddlServerId" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
        <label>Server IP</label>
        <asp:DropDownList ID="ddlServerIp" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
        <label>Binary Name</label>
        <asp:DropDownList ID="ddlBinaryName" runat="server" DataTextField="name" DataValueField="code" onselectedindexchanged="ddlFilter_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList> 
        <asp:Button ID="btnRefresh" runat="server" Text="Refresh"  onclick="btnRefresh_Click" />     
        <asp:Button ID="btnExcel" runat="server" Text="Save as Excel" onclick="btnExcel_Click" />        
    </div>    
    
    <div class="section_tip">
        <em class="tip">TIP!</em> 다중정렬: Shift + 헤더 클릭<span class="close" onclick="$(this).parent().slideUp(); $('#hdnTipDisplay').val('none');"> X Close</span>
        <asp:HiddenField ID="hdnTipDisplay" runat="server" Value="block" ClientIDMode="Static"/>
    </div>

    <div class="section_command" >
        Checked Item(s) 
        <asp:DropDownList ID="ddlAction" runat="server" >
            <asp:ListItem Value="Stopping" Text="Stopping" />
            <asp:ListItem Value="Starting" Text="Starting" />
        </asp:DropDownList>
        <asp:Button ID="btnAction" runat="server" Text="Action" OnClientClick="return DoAction();" onclick="btnAction_Click"  />     
    </div>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
    <ProgressTemplate>
        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center" CssClass="section_loading">   
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/loading.gif" />
            <br />작업중 입니다. 잠시만 기다려 주세요.
        </asp:Panel>
    </ProgressTemplate>
    </asp:UpdateProgress>

    <div >   
        <asp:GridView ID="GridView1" runat="server" 
                OnRowDataBound="GridView1_RowDataBound" 
                OnPreRender="GridView1_PreRender" 
                AutoGenerateColumns="false" 
                ClientIDMode="Static"
                CssClass="tableType1" DataKeyNames="customer,serverId,appId" >  
        
        <Columns>
            
            <asp:TemplateField HeaderText="선택"> 
                <HeaderStyle Width="30px" CssClass="header" />                
                <HeaderTemplate>
                    <input id="chkHeader" type="checkbox" />
                </HeaderTemplate>            
                <ItemStyle Width="30px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:CheckBox ID="chkRow" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>    

             <asp:TemplateField HeaderText="SEQ">   
                <HeaderStyle Width="40px" CssClass="font_85" />
                <ItemStyle Width="40px" HorizontalAlign="Center" />
                <ItemTemplate>     
                    <div><span class="font_85"><%# Container.DataItemIndex+1 %></span></div>            
                </ItemTemplate>
            </asp:TemplateField>            
            
            <asp:TemplateField HeaderText="Command">
                <HeaderStyle Width="75px" CssClass="font_85" />
                <ItemStyle Width="75px" HorizontalAlign="Center" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("command")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Command Time">
                <HeaderStyle Width="120px" CssClass="font_85" />
                <ItemStyle Width="120px" HorizontalAlign="Center" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("commandTime")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Excute Time">
                <HeaderStyle Width="120px" CssClass="font_85" />
                <ItemStyle Width="120px" HorizontalAlign="Center" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("excuteTime")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Status">
                <HeaderStyle Width="60px" CssClass="font_85" />
                <ItemStyle Width="90px" HorizontalAlign="Center" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("status")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Customer">
                <HeaderStyle Width="90px" CssClass="font_85" />
                <ItemStyle Width="90px" />
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("customer")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Server ID">
                <HeaderStyle Width="90px" CssClass="font_85" />
                <ItemStyle Width="90px" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("serverId")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Server IP">
                <HeaderStyle Width="90px" CssClass="font_85" />
                <ItemStyle Width="90px" HorizontalAlign="Center" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("serverIp")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Last UpdateTime">
                <HeaderStyle Width="120px" CssClass="font_85" />
                <ItemStyle Width="120px" HorizontalAlign="Center" />
                <ItemTemplate>
                     <div><span class="font_85"><%# Eval("lastUpdateTime") %></span></div>          
                </ItemTemplate>
            </asp:TemplateField> 
            
            <asp:TemplateField HeaderText="StartUp Time">
                <HeaderStyle Width="120px" CssClass="font_85" />                
                <ItemStyle Width="120px" HorizontalAlign="Center" />
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("startUpTime") %></span></div>          
                </ItemTemplate>
            </asp:TemplateField>                        

            <asp:TemplateField HeaderText="App ID">
                <HeaderStyle Width="10%" CssClass="font_85" />
                <ItemStyle Width="10%" />                 
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("appId")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Binary Name">
                <HeaderStyle Width="10%" CssClass="font_85" />
                <ItemStyle Width="10%" /> 
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("binaryName")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Debug">
                <HeaderStyle Width="10%" CssClass="font_85" />
                <ItemStyle Width="10%" /> 
                 <ItemTemplate>
                    <div><span class="font_85"><%# Eval("debug")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Argument"> 
                <HeaderStyle Width="10%" CssClass="font_85" />
                <ItemStyle Width="10%" /> 
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("argument")%></span></div>          
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Properties">   
                <HeaderStyle Width="10%" CssClass="font_85" />
                <ItemStyle Width="10%" />
                <ItemTemplate>
                    <div><span class="font_85"><%# Eval("properties")%></span></div> 
                </ItemTemplate>
            </asp:TemplateField>

        </Columns> 
        <EmptyDataRowStyle HorizontalAlign="Center" />
        <EmptyDataTemplate>
            No data found
        </EmptyDataTemplate>         
        </asp:GridView>
                   
    </div>

</ContentTemplate>
</asp:UpdatePanel> 

</asp:Content>

