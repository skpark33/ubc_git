﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;
using System.Text;

/* MySQL DB */
using MySql.Data;
using MySql.Data.MySqlClient;

public partial class ServerProcess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initControl();
            BindData(); 
        }               
    }

    protected void initControl()
    {
        this.Title = "List of SeverProcess";

        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;
        MySqlConnection conn = new MySqlConnection(connStr);
        conn.Open();

        MySqlCommand cmd = new MySqlCommand("SELECT distinct customer as code, customer as name FROM utv_serverprocess ORDER BY name", conn);
        MySqlDataReader dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlCustomer, dt, "전체");
        dr.Close();

        cmd = new MySqlCommand("SELECT distinct serverId AS code, serverId AS name FROM utv_serverprocess ORDER BY name", conn);
        dr = cmd.ExecuteReader();
        dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlServerId, dt, "전체");
        dr.Close();

        cmd = new MySqlCommand("SELECT distinct serverIp AS code, serverIp AS name FROM utv_serverprocess ORDER BY name", conn);
        dr = cmd.ExecuteReader();
        dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlServerIp, dt, "전체");
        dr.Close();

        cmd = new MySqlCommand("SELECT distinct binaryName AS code, binaryName AS name FROM utv_serverprocess ORDER BY name", conn);
        dr = cmd.ExecuteReader();
        dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlBinaryName, dt, "전체");
        dr.Close();

        cmd = new MySqlCommand("SELECT distinct status AS code, status AS name FROM utv_serverprocess ORDER BY name", conn);
        dr = cmd.ExecuteReader();
        dt = new DataTable();
        dt.Load(dr);
        BindDropDownList(ddlStatus, dt, "전체");
        dr.Close();

        conn.Close();
    }

    
    protected void BindData()
    {   
        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;

        MySqlConnection conn = null;
        MySqlCommand cmd = null;
              
        try
        {
            conn = new MySqlConnection(connStr);
            conn.Open();
            
            string filter = "";

            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue.ToString()))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND customer = @customer ";
                else
                    filter += " WHERE customer = @customer ";
            }

            if (!string.IsNullOrEmpty(ddlServerId.SelectedValue.ToString()))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND serverId = @serverId ";
                else
                    filter += " WHERE serverId = @serverId ";
            }

            if (!string.IsNullOrEmpty(ddlServerIp.SelectedValue.ToString()))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND serverIp = @serverIp ";
                else
                    filter += " WHERE serverIp = @serverIp ";
            }

            if (!string.IsNullOrEmpty(ddlBinaryName.SelectedValue.ToString()))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND binaryName = @binaryName ";
                else
                    filter += " WHERE binaryName = @binaryName ";
            }

            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue.ToString()))
            {
                if (!string.IsNullOrEmpty(filter))
                    filter += " AND status = @status ";
                else
                    filter += " WHERE status = @status ";
            }

            string cmdText = @"SELECT status
                                    , customer
                                    , serverId
                                    , serverIp
                                    , DATE_FORMAT(lastUpdateTime, '%Y-%m-%d %H:%i:%s' ) AS lastUpdateTime
                                    , DATE_FORMAT(startUpTime, '%Y-%m-%d %H:%i:%s' ) AS startUpTime
                                    , appId
                                    , binaryName
                                    , debug
                                    , argument
                                    , properties 
                                    , command
                                    , DATE_FORMAT(commandTime, '%Y-%m-%d %H:%i:%s' ) AS commandTime
                                    , DATE_FORMAT(excuteTime, '%Y-%m-%d %H:%i:%s' ) AS excuteTime
                                FROM utv_serverprocess ";

            if (!string.IsNullOrEmpty(filter)) cmdText += filter;
            cmdText += " ORDER BY customer";

            cmd = new MySqlCommand(cmdText, conn);
            
            //// 스토어드 프로시저 사용할 경우
            //string cmdText = "usp_Sms_ServerProcessList";
            //cmd = new MySqlCommand(cmdText, conn);
            //cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@customer", ddlCustomer.SelectedValue.ToString());
            cmd.Parameters["@customer"].Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@serverId", ddlServerId.SelectedValue.ToString());
            cmd.Parameters["@serverId"].Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@serverIp", ddlServerIp.SelectedValue.ToString());
            cmd.Parameters["@serverIp"].Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@binaryName", ddlBinaryName.SelectedValue.ToString());
            cmd.Parameters["@binaryName"].Direction = ParameterDirection.Input;
            cmd.Parameters.AddWithValue("@status", ddlStatus.SelectedValue.ToString());
            cmd.Parameters["@status"].Direction = ParameterDirection.Input;

            MySqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }        
        catch (MySqlException mysqlEx)
        {            
            string msg = string.Format("MySqlException Occured {0}:{1}", mysqlEx.Number, mysqlEx.Message);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }
        finally
        {
            if (conn!=null) conn.Close();
        }
        
        if (GridView1.Rows.Count > 0)
        {
            // jquery.tablesorter 를 이용하기위해서 필요한 설정!!
            // 시작
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            // 끝
            // jquery.tablesorter 를 이용하기위해서 필요한 설정!!

            btnExcel.Enabled = true;
        }
        else
        {
            btnExcel.Enabled = false;
        }
    }

    public void BindDropDownList(DropDownList ddlObj, DataTable dt, string title)
    {
        if (!string.IsNullOrEmpty(title))
        {
            DataRow dr = dt.NewRow();
            dr[0] = "";
            dr[1] = title;

            dt.Rows.InsertAt(dr, 0);
        }

        ddlObj.Items.Clear();
        ddlObj.DataSource = dt;
        ddlObj.DataBind();
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }
    
    protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;

            string status = rowView["status"].ToString();

            if (!status.ToLower().Equals("active")) e.Row.CssClass = "inactive";

        }
    }

    protected void GridView1_PreRender(Object sender, EventArgs e)
    {
        if (GridView1.Rows.Count > 0)
        {
            // 첫번째 컬럼은 체크박스 이므로 다른 Sortable 컬럼의 스타일을 없앤다
            GridView1.HeaderRow.Cells[0].Style.Add("cursor", "default");
            GridView1.HeaderRow.Cells[0].Style.Add("text-align", "center");
        }

    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        // 엑셀화일명
        string fileName = string.Format("ServerProcessList_{0}.csv", DateTime.Now.ToString("yyyyMMdd"));

        // 엑셀 포맷으로 저장
        ExportGridToExcel(GridView1, fileName);
    }

    // 엑셀 포맷으로 저장
    public void ExportGridToExcel(GridView gridView, string fileName)
    {
        StringBuilder dataToExport = new StringBuilder();

        // 첫번째,두번째 컬럼(선택박스,seq)는 제외하기 위해서 index를 i=2부터 시작
        for( int i=2 ; i<gridView.HeaderRow.Cells.Count ; i++ )
        {
            dataToExport.Append(String.Format("{0}\t", gridView.HeaderRow.Cells[i].Text));
        }
        dataToExport.Append("\n");        

        foreach (GridViewRow GridViewRow in gridView.Rows)
        {
            // 첫번째,두번째 컬럼(선택박스,seq)는 제외하기 위해서 index를 i=2부터 시작
            for (int i = 2; i < GridViewRow.Cells.Count; i++)
            {
                DataBoundLiteralControl dlc = GridViewRow.Cells[i].Controls[0] as DataBoundLiteralControl;

                if (dlc != null)
                {
                    // html tag 를 제외한 텍스트 추출
                    string innerPlainText = HtmlRemoval.StripTagsRegexCompiled(dlc.Text.Trim());

                    dataToExport.Append(String.Format("=\"{0}\"\t", innerPlainText));
                }
            }
            dataToExport.Append("\n");
        }

        if (!string.IsNullOrEmpty(dataToExport.ToString()))
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8)));
            Response.ContentType = "text/csv";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.Write(Encoding.Unicode.GetString(Encoding.Unicode.GetPreamble()));
            Response.Write(dataToExport.ToString());
            Response.Flush();
            Response.End();
        }
    }

    protected void btnAction_Click(object sender, EventArgs e)
    {
        string connStr = ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString;

        MySqlConnection conn = null;
        MySqlCommand cmd = null;

        try
        {
            conn = new MySqlConnection(connStr);
            conn.Open();

            string cmdText = @"UPDATE utv_serverprocess SET command = @commandValue, commandTime = now()
                                WHERE customer = @customer
                                  AND serverId = @serverId
                                  AND appId = @appId ";
            
            int count = 0;
            int updatedCount = 0;
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (((CheckBox)row.Cells[0].FindControl("chkRow")).Checked)
                {
                    count++;

                    cmd = new MySqlCommand(cmdText, conn);

                    cmd.Parameters.AddWithValue("@commandValue", ddlAction.SelectedValue);
                    cmd.Parameters["@commandValue"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@customer", GridView1.DataKeys[row.RowIndex]["customer"].ToString());
                    cmd.Parameters["@customer"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@serverId", GridView1.DataKeys[row.RowIndex]["serverId"].ToString());
                    cmd.Parameters["@serverId"].Direction = ParameterDirection.Input;
                    cmd.Parameters.AddWithValue("@appId", GridView1.DataKeys[row.RowIndex]["appId"].ToString());
                    cmd.Parameters["@appId"].Direction = ParameterDirection.Input;

                    if (cmd.ExecuteNonQuery() > 0) updatedCount++;
                    
                }
            }

            string msg = string.Format("Total {0} of selected {1} Updated!!", updatedCount.ToString(), count.ToString());
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
            BindData();
        }
        catch (MySqlException mysqlEx)
        {            
            string msg = string.Format("MySqlException Occured {0}:{1}", mysqlEx.Number, mysqlEx.Message);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alert", string.Format("alert('{0}');", msg), true);
        }
        finally
        {
            if (conn!=null) conn.Close();
        }

    }
}