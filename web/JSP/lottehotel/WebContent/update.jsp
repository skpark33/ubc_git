<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> <!-- errorPage="error.jsp"% -->
<%@ page import="java.sql.*, javax.sql.*, javax.naming.*" %>
<%@ page import="org.apache.log4j.*" %>
<%! public Logger infolog = Logger.getLogger("infolog"); %>
<%! public Logger errlog = Logger.getLogger("errlog"); %>
<%
	if((String)session.getAttribute("id") == null) {
		response.sendRedirect("index.jsp");
	}
	
	infolog.info("Event Update JSP page Open");

	int eid = Integer.parseInt(request.getParameter("eid"));
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String date = "";
	String fdate = "";
	String sql = "";
	String opsql = "";
	String roomsql = "";
	String lsql = "";
	String stime = "";
	String ftime = "";
	String room = "";
	String floor = "";
	String title = "";
	String line1 = "";
	String line2 = "";
	String line3 = "";
	int size1 = 0;
	int size2 = 0;
	int size3 = 0;
	String color = "";
	String logo = "";
	int cli = 0;
	int climin=0;
	String chkroom = "";
	String sendList = "";
	String []chkcolor = { "BASIC", "YELLOW", "RED", "BLUE", "GREEN" };
	String opHTML = "";
	String chkHTML = "";
	String lobHTML = "";
	String vHTML = "";
	String oproom = "";
	String hostId = "";
	String hostName = "";
	String addr1 = "";
	String visible = "";
	String bg = "";
	
	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");

		sql = "select date, fdate, DATE_FORMAT(stime, '%H%i') as stime, DATE_FORMAT(ftime, '%H%i') as ftime, room, floor, line1, line2, line3, size1, size2, size3, color, logo, clihour, climin, title, chkroom, sendList, visible, bgfile from ubc_web_hotel_event where eid=" + eid;
		opsql = "select hostName from ubc_host where category like '%방형%' order by hostName";
		roomsql = "select hostName, addr1 from ubc_host where category like '%방형%' AND NOT category like '%가상단말%' order by hostName";
		lsql = "select hostId, hostName, addr1 from ubc_host where category like '%층형%' OR category like '%로비%' order by hostName";
		
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		while ( rs.next()) {
			date = rs.getString("date");
			fdate = rs.getString("fdate");
			stime = rs.getString("stime");
			ftime = rs.getString("ftime");
			room = rs.getString("room");
			floor = rs.getString("floor");
			line1 = rs.getString("line1");
			line2 = rs.getString("line2");
			line3 = rs.getString("line3");
			size1 = (int)(rs.getFloat("size1"));
			size2 = (int)(rs.getFloat("size2"));
			size3 = (int)(rs.getFloat("size3"));
			color = rs.getString("color");
			logo = rs.getString("logo");
			cli = rs.getInt("clihour");
			climin = rs.getInt("climin");
			title = rs.getString("title");
			chkroom = rs.getString("chkroom");
			sendList = rs.getString("sendList");
			visible = rs.getString("visible");
			bg = rs.getString("bgfile");
			
			// 룸 글씨 보임 유무 CheckBox 
			if(visible != null) {
				vHTML = vHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"text-hidden-chkbox\" name=\"visible\" value=\"y\" checked/>  글 숨기기</label><br/>";
			}
			else {
				vHTML = vHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"text-hidden-chkbox\" name=\"visible\" value=\"y\"/>  글 숨기기</label><br/>";
			}
		}
		rs = null;
		// SelectBox Data
		rs = stmt.executeQuery(opsql);
		while ( rs.next()) {
			oproom = rs.getString("hostName");
			if( room.equals(oproom)) {
				opHTML = opHTML + "<option value=\"" + room + "\" selected> " + room + "</option>";
			}
			else {
				opHTML = opHTML + "<option value=\"" + oproom + "\"> " + oproom + "</option>";
			}
		}
		rs = null;
		// 존재하는 룸단말 CheckBox Data
		rs = stmt.executeQuery(roomsql);
		while(rs.next()) {
			hostName = rs.getString("hostName");
			addr1 = rs.getString("addr1");
			if(room.substring(0,5).equals(hostName.substring(0,5))) {
				if(chkroom == null) {
					chkHTML = chkHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"roomchk-field\" name=\"chkroom\" value=\"" + hostName + "#" + addr1 + "\" onclick=\"chk_room(this)\"/>  " + hostName + "</label><br/>\n";
				} 
				else if(room.substring(0,5).equals(chkroom.substring(0,5))) {
					chkHTML = chkHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"roomchk-field\" name=\"chkroom\" value=\"" + hostName + "#" + addr1 + "\" onclick=\"chk_room(this)\" checked/>  " + hostName + "</label><br/>\n";
				}
			}
			else {
				chkHTML = chkHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"roomchk-field\" name=\"chkroom\" value=\"" + hostName + "#" + addr1 + "\" onclick=\"chk_room(this)\" disabled />  " + hostName + "</label><br/>\n";
			}
		}
		rs = null;
		// 공용부 단말 CheckBox Data
		rs = stmt.executeQuery(lsql);
		if(sendList != null) {
			String []data = sendList.split("#");
			while (rs.next()) {
				String flag = "true";
				hostId = rs.getString("hostId");
				hostName = rs.getString("hostName");
				addr1 = rs.getString("addr1");
				for(int i=0; i<data.length; i++) {
					if(flag.equals("true") && !(flag.equals("end"))) {
						if( hostName.equals(data[i])) {
							lobHTML = lobHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"lobchk-field\" name=\"chklob\" value=\"" + hostId + "#" + hostName + "#" + addr1 + "&&\" checked/>  " + hostName + "</label><br/>\n";
							flag = "end";
						}
						else if ( i == data.length-1) {
							flag = "false";
						}
					}
					if(flag.equals("false")){
						lobHTML = lobHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"lobchk-field\" name=\"chklob\" value=\"" + hostId + "#" + hostName + "#" + addr1 + "&&\"/>  " + hostName + "</label><br/>\n";
						flag = "end";
					}
				}
			}
		}
		else {
			while (rs.next()) {
				hostId = rs.getString("hostId");
				hostName = rs.getString("hostName");
				lobHTML = lobHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"lobchk-field\" name=\"chklob\" value=\"" + hostId + "#" + hostName + "#" + addr1 + "&&\"/>  " + hostName + "</label><br/>\n";
			}
		}
	} catch(SQLException e) {
		e.printStackTrace();
		errlog.warn(e.getMessage());
		infolog.info(e.getMessage() + "DB error");
	}
	finally {
		if(rs!=null) {rs.close();}
		if(stmt!=null) {stmt.close();}
		if(conn!=null) {conn.close();}
	}
%>
<!DOCTYPE html>
<html>
<head>
<base target="_self">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<link rel='stylesheet' type='text/css' href='./css/addlayout.css'/>
<link rel='stylesheet' href='css/jquery-ui-1.10.3.datepicker.css' />
<link rel="shortcut icon" href="img/favicon2.ico" type="image/x-icon">
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui-1.10.3.datepicker.min.js"></script>
<script src="js/check.js"></script>
<script type="text/javascript">
	/* font 선택 중복 불가처리 */
	function chk_only(only) {
		var obj = document.getElementsByName("font-color");
		for(var i = 0; i<obj.length; i++) {
			if(obj[i] != only) {
				obj[i].checked = false;
			}
		}
	}
	/* 룸 선택 중복불가 처리 */
	function chk_room(only) {
		var obj = document.getElementsByName("chkroom");
		for(var i = 0; i<obj.length; i++) {
			if(obj[i] != only) {
				obj[i].checked = false;
			}
		}
	}
	function fnSel(val) {
		var obj3 = document.getElementsByName("chkroom");
		var objlob = document.getElementsByName("chklob");
		for(var i=0; i<obj3.length; i++) {
			if(val.value.substring(0,5) == obj3[i].value.split("#")[0].substring(0,5)) { 
				obj3[i].checked = true;
				obj3[i].disabled = false;
				for(var j=0; j<objlob.length; j++) {
					if( obj3[i].value.split("#")[1] == objlob[j].value.split("&&")[0].split("#")[2] 
					|| objlob[j].value.split("&&")[0].split("#")[2] == '1F'
					|| objlob[j].value.split("&&")[0].split("#")[2] == 'B2F'
					|| objlob[j].value.split("&&")[0].split("#")[2] == 'B3F'
					|| objlob[j].value.split("&&")[0].split("#")[2] == 'B4F') {
						objlob[j].checked = true;
					}
					else {
						objlob[j].checked = false;
					}
				}
			}
			else if( val.value.substring(0,5) == 'CRYST' || val.value.substring(0,5) == 'LOTTE') {
				obj3[i].disabled = true;
				obj3[i].checked = false;
				for(var j=0; j<objlob.length; j++) {
				if( objlob[j].value.split("&&")[0].split("#")[2] == '3F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == '1F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B2F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B3F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B4F') {
						objlob[j].checked = true;
					}
					else {
						objlob[j].checked = false;
					}
				}
			}
			else { 
				obj3[i].checked = false;
				obj3[i].disabled = true;
				if(val.value.search('ROOM') == -1) {
					for(var j=0; j<objlob.length; j++) {
						if( objlob[j].value.split("&&")[0].split("#")[2] == '1F'
							|| objlob[j].value.split("&&")[0].split("#")[2] == 'B2F'
							|| objlob[j].value.split("&&")[0].split("#")[2] == 'B3F'
							|| objlob[j].value.split("&&")[0].split("#")[2] == 'B4F') {
							objlob[j].checked = true;
						}
						else {
							objlob[j].checked = false;
						}
					}
				}
			}
		} 
	}
	function filereset(form) {
		form.bgupload.select();
		document.selection.clear();
	}
	/* Button type Submit */
	function submitjs(index) {
		if(index == 1) {
			document.listform.action="updateDB.jsp";
		}
		if(index == 2) {
			document.listform.action="insertDB.jsp?new=ok";
		}
		if(check()) {
			document.listform.submit();
		}
	}
	$(function() { 
		    $( "#date" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
			    onClose: function( selectedDate ) {
			    		$('#fdate').datepicker("option", "minDate", selectedDate);
			    }
		    });
		  });
	$(function() { 
		    $( "#fdate" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
			    onClose: function( selectedDate ) {
			    		$('#date').datepicker("option", "maxDate", selectedDate);
			    }
		    });
		  });

</script>
<title>EVENT UPDATE</title>
</head>
<body>
	<form name="listform" class="form-container" method="POST" enctype="multipart/form-data">
<div class="form-title">
	<h2>행사 수정</h2>
</div>
<div class="form-title">행사명
		<span><input class="title-form-field" type="text" name="title" value="<%=title%>"/></span>
	</div>
<div class="form-title"><pre>행사내용                                                                               글씨크기</pre></div>
<div>
	<input class="form-field" type="text" name="first-line" value="<%=line1%>"/>
	<% for(int i = 1; i<=5; i++) {
		if( size1 == i) { 
			out.println("<input type=\"radio\" name=\"first-size\" value=\"" + i + "\" checked=\"checked\">" + i);
		}
		else {
			out.println("<input type=\"radio\" name=\"first-size\" value=\"" + i + "\">" + i);
		}
	}
	%>
	<%-- <input class="size-form-field" type="text" name="first-size" value="<%=size1%>"/><label for="first-size"> % </label> --%>
</div>
<div>
	<input class="form-field" type="text" name="second-line" value="<%=line2%>"/> 
	<% for(int i = 1; i<=5; i++) {
		if( size2 == i) { 
			out.println("<input type=\"radio\" name=\"second-size\" value=\"" + i + "\" checked=\"checked\">" + i);
		}
		else {
			out.println("<input type=\"radio\" name=\"second-size\" value=\"" + i + "\">" + i);
		}
	}
	%>
	<%-- <input class="size-form-field" type="text" name="second-size" value="<%=size2%>"/><label for="second-size"> % </label> --%>
</div>
<div>
	<input class="form-field" type="text" name="third-line" value="<%=line3%>"/>
	<% for(int i = 1; i<=5; i++) {
		if( size3 == i) { 
			out.println("<input type=\"radio\" name=\"third-size\" value=\"" + i + "\" checked=\"checked\">" + i);
		}
		else {
			out.println("<input type=\"radio\" name=\"third-size\" value=\"" + i + "\">" + i);
		}
	}
	%>
	<%-- <input class="size-form-field" type="text" name="third-size" value="<%=size3%>"/><label for="third-size"> % </label> --%>
</div>
<!-- <div class="form-title">글씨색상</div> -->
<fieldset class="form-fieldset">
	<legend>글씨색상</legend>
	<div class="form-checkbox">
		<% for(int i=0; i<5; i++) {
			if(chkcolor[i].equals(color)) {
				out.println("<label class=\"color-label\"><input class=\"font-color\" type=\"checkbox\" name=\"font-color\" value=\"" + chkcolor[i] +"\" checked=\"checked\" onclick=\"chk_only(this)\"/>  " + chkcolor[i] + "</label>");
			}
			else {
				out.println("<label class=\"color-label\"><input class=\"font-color\" type=\"checkbox\" name=\"font-color\" value=\"" + chkcolor[i] +"\" onclick=\"chk_only(this)\"/>  " + chkcolor[i] + "</label>");
			}
		}%>
	</div>
</fieldset>
<div class="form-title"><pre>행사시작날짜        행사종료날짜		행사표출시간</pre></div>
<div>
	<input class="date-form-field" type="text" name="date" id="date" value="<%=date%>" readonly/><label for="fdate"> -</label>
	<input class="date-form-field" type="text" name="fdate" id="fdate" value="<%=fdate%>" readonly/>
	<span class="blankspan"></span>
	<input class="prev-form-field" type="text" name="clihour" id="clihour" maxlength="2" value="<%=cli %>"/><label for="clihour">시간</label>
	<input class="prev-form-field" type="text" name="climin" id="climin" maxlength="2" value="<%=climin %>"/><label for="climin">분 전</label> 
	<span class="time-ex">기본 시작시간 3시간전</span>   
</div>

<div class="form-title"><pre>시작시간             종료시간</pre></div>
<div>
	<input class="time-form-field" type="text" name="stime" value="<%=stime%>" maxlength="4" onkeypress="inNum();"/>&nbsp;&nbsp;<label for="ftime">  ~  </label>&nbsp;&nbsp;
	<input class="time-form-field" type="text" name="ftime" value="<%=ftime%>" maxlength="4" onkeypress="inNum();"/>
	<span class="time-ex">ex) 09:00am > 0900 // 09:00pm > 2100</span>
</div>
<div class="form-title">행사장</div>
<div>
	<select class="sel" name="room" onchange="fnSel(this)">
		<option value="">장소선택</option>
		<%=opHTML %>
	</select>
	<fieldset id="form-location">
		<legend>[행사표출장소]</legend>
		<fieldset class="info-location-room">
			<legend>행사장</legend>
			<%=chkHTML %>
		</fieldset>
		<fieldset class="info-location-lobby">
			<legend>공용부</legend>
			<%=lobHTML %>
		</fieldset>
	</fieldset>
</div>
<div class="form-title">배경이미지 등록 <span><%=vHTML %></span></div>
<div>
	<input class="upload-form-field" type="file" name="bgupload" id="bgupload" title="파일 찾기" value="파일찾기"/>
	<!-- <span><input type="button" name="clear" id="clear" value="삭제" onclick="filereset(this.form)"/></span> -->
	
	<span>
	 	<% if(bg == null || ("null".equals(bg))) { %>
			<label class="chk_label"><input type="checkbox" class="filename-form-field" name="old_bg" disabled="disabled"/>  등록 파일없음</label>
		<% }
		else { %>
			<label class="chk_label"><input type="checkbox" class="filename-form-field" name="old_bg" value="y"/>  <%=bg %> 파일삭제</label>
		<% } %>
		<input type="hidden" name="old_bg_upload" value="<%=bg %>"/>
	</span>
</div>
<div class="form-title">로고이미지 등록</div>
<div>
	<input class="upload-form-field" type="file" name="upload" title="파일 찾기" value="파일찾기"/>
	<%-- <span><%=vHTML %></span> --%>
	<span>
		<% if(logo == null || ("null".equals(logo))) { %>
			<label class="chk_label"><input type="checkbox" class="filename-form-field" name="old_logo" disabled="disabled"/>  등록 파일없음</label>
		<% }
		else { %>
			<label class="chk_label"><input type="checkbox"  class="filename-form-field" name="old_logo" value="y"/>  <%=logo %> 파일삭제</label>
		<% } %>
		<input type="hidden" name="old_upload" value="<%=logo %>"/>
	</span>
</div>
<div class="submit-container">
	<input type="hidden" name="eid" value="<%=eid %>"/>
	<input type="hidden" name="floor" value="<%=floor %>"/>
	<input class="submit-button" type="button" value="새로저장" onclick="submitjs(2)"/>
	<input class="submit-button" type="button" value="수정" onclick="submitjs(1)"/>
	<input class="submit-button" type="button" value="취소" onclick="window.close()"/>
</div>
</form>
</body>
</html>
