<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@ page import="java.sql.*, javax.naming.*, javax.sql.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="org.apache.log4j.*" %>
<%! public Logger infolog = Logger.getLogger("infolog"); %>
<%! public Logger errlog = Logger.getLogger("errlog"); %>
<%
	/* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	
	if(((String)session.getAttribute("id")) == null) {
		response.sendRedirect("index.jsp");
	}
	
	Calendar now = Calendar.getInstance();
	String year = now.get(Calendar.YEAR) + "";
	String month = now.get(Calendar.MONTH)+ 1 + "";
	String day = now.get(Calendar.DATE) + "";
	String hour = now.get(Calendar.HOUR_OF_DAY) + "";
	String minute = now.get(Calendar.MINUTE) + "";
	String ymd = year + "-" + month + "-" + day;
	String clock = hour + ":" +  minute;
	
	String chk = "";
	chk = request.getParameter("chk");
	if(chk == null) { chk = "n"; }
	
	String start = request.getParameter("start");
	String end = request.getParameter("end");
	
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	// DB Connection & Query
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String trHTML = "";
	String sql = null;
	String date = null; 
	String fdate = null;
	String stime = null;
	String ftime = null;
	String room = null;
	String floor = null;
	String event = null;
	String title = null;
	String sendList = null;
	String chkroom = null;
	String []data;
	int eid = 0;
	String location = "없음";
	
	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");

		if( chk.equals("n") ) { sql = "select * from ubc_web_hotel_event where date <= DATE_FORMAT(now(), '%Y%m%d') AND fdate >= DATE_FORMAT(now(), '%Y%m%d') order by stime"; }
		else if( chk.equals("y") ){ sql = "select * from ubc_web_hotel_event where (date <= \'" + start + "\' AND fdate >= \'" + start + "\' AND date <= \'" + end + "\' AND fdate >= \'" + end + "\')" 
							+ " OR (fdate >= \'" + start + "\' AND fdate <= \'" + end + "\') OR (date >= \'" + start + "\' AND date <= \'" + end + "\') order by date, stime "; }
		
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		while(rs.next()) {
			eid = rs.getInt("eid");
			date = rs.getString("date");
			fdate = rs.getString("fdate");
			stime = rs.getString("stime").substring(0,5);
			ftime = rs.getString("ftime").substring(0,5);
			room = rs.getString("room");
			floor = rs.getString("floor");
			event = rs.getString("line1");
			event += "<br/>" + rs.getString("line2");
			event += "<br/>" + rs.getString("line3");
			title = rs.getString("title");
			chkroom = rs.getString("chkroom");
			sendList = rs.getString("sendList");
			
			// Tooltip 데이터
			if(chkroom != null) { 
				location = chkroom; 
				if(sendList != null) {
					data = sendList.split("#");
					for(int i=0; i<data.length; i++) {
						location += "<br/>" + data[i];
					}
				}
			}
			else {
				if(sendList != null) {
					data = sendList.split("#");
					location = data[0];
					for(int i=1; i<data.length; i++) {
						location += "<br/>" + data[i];
					}
				}
			}
			if( (df.parse(date).getTime() <= df.parse(ymd).getTime() && df.parse(fdate).getTime() >= df.parse(ymd).getTime() && sdf.parse(ftime).getTime() > sdf.parse(clock).getTime()) 
					|| (df.parse(date).getTime() <= df.parse(ymd).getTime()-(60*60*24*1000) && df.parse(fdate).getTime() >= df.parse(ymd).getTime()-(60*60*24*1000) && sdf.parse(stime).getTime() > sdf.parse(ftime).getTime() && sdf.parse(ftime).getTime() > sdf.parse(clock).getTime()) 
					|| (df.parse(date).getTime() <= df.parse(ymd).getTime() && df.parse(fdate).getTime() >= df.parse(ymd).getTime() && sdf.parse(stime).getTime() > sdf.parse(ftime).getTime()) ) {
				
				trHTML = trHTML + "\t\t\t\t<tr class=\"row_body\">\n";
				//trHTML += "\t\t\t\t<td>" + "<input type=\"checkbox\" name=\"sel\" value=\"" + eid + "\" onclick=\"chk_only(this)\"/>" + "</td>\n";
				trHTML += "\t\t\t\t<td>" + title + "</td>\n";
				trHTML += "\t\t\t\t<td>" + date + "</td>\n";
				trHTML += "\t\t\t\t<td>" + fdate + "</td>\n";
				trHTML += "\t\t\t\t<td>" + stime + "</td>\n";
				trHTML += "\t\t\t\t<td>" + ftime + "</td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" class=\"tooltip\" title=\"" + event + "\" onclick=\"event_view('" + eid + "', '" + date + "')\">보기</a></td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" onclick=\"room_view('" + eid + "')\" >" + room + "</a></td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" class=\"tooltip\" title=\"" + location + "\">보기</a></td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" onclick=\"modModal('" + eid + "')\" class=\"btn\">수정</a></td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" onclick=\"chk_delete('" + eid + "')\" class=\"btn\">삭제</a></td>\n" + "\t\t\t\t</tr>\n";
			}
			else {
				trHTML = trHTML + "\t\t\t\t<tr class=\"row_body_last\">\n";
				//trHTML += "\t\t\t\t<td>" + "<input type=\"checkbox\" name=\"sel\" value=\"" + eid + "\" onclick=\"chk_only(this)\"/>" + "</td>\n";
				trHTML += "\t\t\t\t<td>" + title + "</td>\n";
				trHTML += "\t\t\t\t<td>" + date + "</td>\n";
				trHTML += "\t\t\t\t<td>" + fdate + "</td>\n";
				trHTML += "\t\t\t\t<td>" + stime + "</td>\n";
				trHTML += "\t\t\t\t<td>" + ftime + "</td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" class=\"tooltip\" title=\"" + event + "\" onclick=\"event_view('" + eid + "', '" + date + "')\">보기</a></td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" onclick=\"room_view('" + eid + "')\" >" + room + "</a></td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" class=\"tooltip\" title=\"" + location + "\">보기</a></td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" onclick=\"modModal('" + eid + "')\" class=\"btn\">수정</a></td>\n";
				trHTML += "\t\t\t\t<td><a href=\"#\" onclick=\"chk_delete('" + eid + "')\" class=\"btn\">삭제</a></td>\n" + "\t\t\t\t</tr>\n";
			}
			location = "없음";
		} 
	} catch(SQLException e) {
		e.printStackTrace();
		errlog.warn(e.getMessage());
	} finally {
		if(rs!=null) {rs.close();}
		if(stmt!=null) {stmt.close();}
		if(conn!=null) {conn.close();}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<link rel='stylesheet' type='text/css' href='css/list.css' />
<link rel='stylesheet' href='css/jquery-ui-1.10.3.datepicker.css'/>
<link rel='stylesheet' href='css/tip-darkgray.css'/>
<link rel="shortcut icon" href="img/favicon2.ico" type="image/x-icon">
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui-1.10.3.datepicker.min.js"></script>
<script src="js/jquery.poshytip.min.js"></script>
<script src="js/button.js"></script>
<script type="text/javascript">
	  $(function() { 
	    $( "#toDate" ).datepicker({
		    inline: true,
		    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
		    prevText: 'prev',
		    nextText: 'next',
		    showButtonPanel: true,	/* 버튼 패널 사용 */
		    changeMonth: true,		/* 월 선택박스 사용 */
		    changeYear: true,		/* 년 선택박스 사용 */
		    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
		    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
		    showOn: "button",
		    buttonImage: "img/calendar03.gif",
		    buttonImageOnly: true,
		    minDate: '-30y',
		    closeText: '닫기',
		    currentText: '오늘',
		    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
		    /* 한글화 */
		    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
		    showAnim: 'slideDown',
		    /* 날짜 유효성 체크 */
		    onClose: function( selectedDate ) {
			    $('#fromDate').datepicker("option","minDate", selectedDate);
		    }
	    });
	  });
	$(function() {
		    $( "#fromDate" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,
			    changeMonth: true,
			    changeYear: true,
			    showOtherMonths: true,
			    selectOtherMonths: true,
			    showOn: "button",
			    buttonImage: "img/calendar03.gif",
			    buttonImageOnly: true,
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,
			    showAnim: 'slideDown',
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    /* 날짜 유효성 체크 */ 
			    onClose: function( selectedDate ) {
				    $('#toDate').datepicker("option", "maxDate", selectedDate);
			    }
		    });
	});
	$(function() {
		$('.tooltip').poshytip({
			className: 'tip-darkgray',
			bgImageFrameSize: 15,
			offsetX: -20
		});
	});
	$(document).ready(function() {
		if(<%=start %> == null && <%=end %> == null) {
			$('#toDate, #fromDate').val($.datepicker.formatDate($.datepicker.ATOM, new Date()));	
		}
		else {
			$('#toDate').val('<%=start %>');
			$('#fromDate').val('<%=end %>');
		}
	});
	/* 체크박스 복수선택 불허용 */  
	function chk_only(only) {
		var obj = document.getElementsByName("sel");
		for(var i = 0; i<obj.length; i++) {
			if(obj[i] != only) {
				obj[i].checked = false;
			}
		}
	}
</script>
<title>LOTTE HOTEL 행사리스트</title>
</head>
<body>  
	<div class="top"> <a href="#" onclick="logout()" class="logout">로그아웃</a>	</div>
	<div class="top-logo"><a href="http://www.lottehotelbusan.com/kr/" target="_blank"><img src="img/logo.gif"></a></div>
	
	<form class="form-layout">
		<div>
			<h1 class="logo">관리 리스트</h1>
		</div>
		<div class="period">
			<input type="text" class="date" name="toDate" id="toDate" readonly />
			<label for="fromDate">-</label>
			<input type="text" class="date" name="fromDate" id="fromDate" readonly />
			<a href="#"><img src="./img/search_button04.png" onclick="datefilter()" alt="기간 검색 아이콘"/></a>
			<a href="#" onclick="lobby_view('LOTTEH-30501')" class="preview">메인로비</a>
			<a href="#" onclick="addModal()" class="add">행사 추가</a> 
			
			<!-- <a href="#" onclick="lobby_view('LOTTEH-30513')" class="preview">B2F로비</a>
			<a href="#" onclick="lobby_view('LOTTEH-30512')" class="preview">B3F로비</a>
			<a href="#" onclick="lobby_view('LOTTEH-30511')" class="preview">B4F로비</a> -->
			<!-- <a href="#" onclick="room_view()" class="preview">룸형</a> -->
		</div>
		<div class="listview">
			<table class="list_table">
			<thead>
				<tr>
					<!-- <th>선택</th> -->
					<th>행사명</th>
					<th>시작날짜</th>
					<th>종료날짜</th>
					<th>시작시간</th>
					<th>종료시간</th>
					<th>행사내용</th>
					<th>행사장</th>
					<th>표출장소</th>
					<th>수정</th>
					<th>삭제</th>
				</tr>
			</thead>
			<tbody>
<%=trHTML %>
			</tbody>
			</table>						
		</div>
		<div class="btn_gr">
			<input type="hidden" id="start" value="<%=start %>">
			<input type="hidden" id="end" value="<%=end %>">
		</div>
	</form>
</body>
</html>
