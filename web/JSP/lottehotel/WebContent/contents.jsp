<%@page import="com.sun.corba.se.spi.orbutil.fsm.State"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="javax.naming.*, javax.sql.*" %>
<%@ page session="false" %>
<%@ page import="org.apache.log4j.*" %>
<%! public Logger infolog = Logger.getLogger("infolog"); %>
<%! public Logger errlog = Logger.getLogger("errlog"); %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Insert title here</title>
</head>
<body>
<%
	String hostId = request.getParameter("HostId");
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String sql = "";
	int cnt = 0;

	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		
		if( conn == null) {
			out.println("DB Connection Fail");
		}
		else {
			sql = "select count(*) as cnt from ubc_web_hotel_event"
			 	+ " where hostList like '%" + hostId + "%' AND (" 
			 	+ " (date<=DATE_FORMAT(now(), '%Y-%m-%d') AND fdate>=DATE_FORMAT(now(), '%Y-%m-%d') AND displayTime <= date_format(now(), '%H:%i') AND ftime > DATE_FORMAT(now(), '%H:%i'))"
			 	+ " OR (date<=date_format(now(), '%Y-%m-%d') and fdate>=date_format(now(), '%y-%m-%d') and displayTime <= date_format(now(), '%H:%i') AND  stime > ftime )"
			 	+ " OR (date<=date_format(date_sub(now(), interval 1 day), '%Y-%m-%d') and fdate>=date_format(date_sub(now(), interval 1 day), '%Y-%m-%d') and displayTime <= date_format(now(), '%H:%i') AND stime > ftime and ftime > date_format(now(), '%H:%i'))"
			 	+ " )";
				
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				cnt = rs.getInt("cnt");
			}
			if( cnt > 6 ) {
				cnt = 6;
			}
		}
	} catch(SQLException e) {
		e.printStackTrace();
		errlog.warn(e.getMessage());
	}
	finally {
		if(rs!=null) { rs.close(); }
		if(stmt!=null) {stmt.close();}
		if(conn!=null) {conn.close();}
	}
	RequestDispatcher dispatcher = request.getRequestDispatcher("content.jsp?data="+cnt+"&HostId="+hostId);
	dispatcher.forward(request, response);
%>
</body>
</html>
