<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
    /* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-store");
	
      String start = request.getParameter("start");	
      String hostId = request.getParameter("hostId");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Lobby preview</title>
<style>
	* {
		margin: 0;
		padding: 0;
	}
</style>
</head>
<body>
	<iframe src="http://localhost:8000/lottehotel/contentsprev.jsp?start=<%=start%>&hostId=<%=hostId %>" width="100%" height="1320px" scrolling="yes" frameborder="0" marginwidth="0" marginheight="0"></iframe>
</body>
</html>
