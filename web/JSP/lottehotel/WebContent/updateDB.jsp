<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, java.util.*, java.io.*" %>
<%@ page import="javax.naming.*, javax.sql.*" %>
<%@ page import="com.oreilly.servlet.MultipartRequest" %>
<%@ page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>
<%@page import="java.sql.Date, java.text.SimpleDateFormat"%>
<%@ page import="org.apache.log4j.*" %>
<%! public Logger infolog = Logger.getLogger("infolog"); %>
<%! public Logger errlog = Logger.getLogger("errlog"); %>
<%
	SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat cdf = new SimpleDateFormat("yyyy/MM/dd");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
		infolog.info("Update data...");
		String sql = "";
		String flsql= "";
		String filesql = "";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String filePath = "C:\\Users\\DELL\\workspace\\lottehotel\\WebContent\\uploadfiles\\";	// file SAVE location.
		//String filePath = "C:/Apache Software Foundation/Tomcat 7.0/webapps/lottehotel/uploadfiles/";	// .Server file SAVE location.
		//String filePath = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/lottehotel/uploadfiles/";	// .219Server file SAVE location.
		int maxSize = 10 * 1024 * 1024;	// 10MB limit.
		

		String hostList = null;
		String sendList = null;
		String data = "";
		
 		 try {
 			Class.forName("com.mysql.jdbc.Driver");
 			Class.forName("org.apache.commons.dbcp.PoolingDriver");
 			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
 			
			MultipartRequest multi = new MultipartRequest(request, filePath, maxSize, "utf-8", new DefaultFileRenamePolicy());
			
			int eid = Integer.parseInt(multi.getParameter("eid"));
			Date date = new java.sql.Date(df.parse(multi.getParameter("date")).getTime());
			Date fdate = new java.sql.Date(df.parse(multi.getParameter("fdate")).getTime());
			Time stime = new java.sql.Time(sdf.parse(multi.getParameter("stime")).getTime());
			Time ftime = new java.sql.Time(sdf.parse(multi.getParameter("ftime")).getTime());
			int clihour = Integer.parseInt(multi.getParameter("clihour"));
			int climin = Integer.parseInt(multi.getParameter("climin"));
			String title = multi.getParameter("title");
			String first_line = multi.getParameter("first-line");
			String second_line = multi.getParameter("second-line");
			String third_line = multi.getParameter("third-line");
			int first_size = Integer.parseInt(multi.getParameter("first-size"));
			int second_size = Integer.parseInt(multi.getParameter("second-size"));
			int third_size = Integer.parseInt(multi.getParameter("third-size"));
			String room = multi.getParameter("room");
			//String floor = multi.getParameter("floor");
			String color = multi.getParameter("font-color");
			String bg = multi.getFilesystemName("bgupload");	// bg 업로드 파일명
			String oldbg = multi.getParameter("old_bg_upload");	// 업로드 된 이전 bg 파일명
			String oldbgchk = multi.getParameter("old_bg");		// 삭제 체크박스 
			String logo = multi.getFilesystemName("upload");	// logo 업로드 파일명
			String oldfile = multi.getParameter("old_upload");	// 업로드 된 이전 logo 파일명
			String oldlogochk = multi.getParameter("old_logo");	// 삭제 체크박스
			String chkroom = multi.getParameter("chkroom");
			String []chklob = multi.getParameterValues("chklob");
			String visible = multi.getParameter("visible");
			
			if(chkroom != null) {
				chkroom = chkroom.split("#")[0];
			}
			// UBC cli 시간처리 데이터
			Time displayTime = new java.sql.Time(sdf.parse(multi.getParameter("stime")).getTime()-(clihour*60*60*1000)-(climin*60*1000));
			Date displayDate = null;
			if(sdf.parse(sdf.format(displayTime)).getTime() > sdf.parse(sdf.format(stime)).getTime()) {
				displayDate = new java.sql.Date(df.parse(multi.getParameter("date")).getTime()-(24*60*60*1000));
			}
			else {
				displayDate = new java.sql.Date(df.parse(multi.getParameter("date")).getTime());
			}
			
			// logo file
			if( logo == null) {
				if("null".equals(oldfile) || oldfile == null) {
					logo = null;
				}
				else {
					logo = oldfile;	// 새로 저장했을때 기존 파일 유지
					if(oldlogochk!=null && oldlogochk.equals("y")) {
						File logofile = new File(filePath + logo);
						if(logofile.exists()) {
							logofile.delete();
						}
						logo = null;
					}
				}
			}
			
			// background file
			if( bg == null) {
				if("null".equals(oldbg) || oldbg == null) {
					bg = null;
				}
				else {
					bg = oldbg;
					if(oldbgchk!=null && oldbgchk.equals("y")) {
						File bgfile = new File(filePath + bg);
						if(bgfile.exists()) {
							bgfile.delete();
						}
						bg = null;
					}
				}
			}
			
			//long diff = fdate.getTime() - date.getTime();
			//long Days = (diff / (24 * 60 * 60 * 1000)) + 1;
			
			String floor = "";
			String hostId = "";
			String siteId = "";
			
			if(conn == null) {
				infolog.info("update.jsp DB Connection Fail");
			}
			else {
				flsql = "select addr1, hostId, siteId from ubc_host where hostName=?";
				pstmt = conn.prepareStatement(flsql);
				if(chkroom != null) {
					pstmt.setString(1, chkroom);
				}
				else {
					pstmt.setString(1, room);
				}
				rs = pstmt.executeQuery();
				while(rs.next()) {
					floor = rs.getString("addr1");
					hostId = rs.getString("hostId");
					siteId = rs.getString("siteId");
				}
				// hostList, sendList 데이터 생성
				if(true) {
					hostList = "[";
					if(chklob != null) {
						for(int i = 0 ; i<chklob.length; i++) {
							data += chklob[i];
						}
						String []arr = data.split("&&");
						String [][]brr = new String[arr.length][2];
						for(int j=0; j<arr.length; j++) {
							for(int k=0; k<2; k++) {
								brr[j][k] = arr[j].split("#")[k];
							}
						}
						sendList = "";
						for(int i = 0; i<chklob.length; i++) {
							hostList += "\"" + brr[i][0] + "\"";
							sendList += brr[i][1];
							if(i < chklob.length-1) {
								hostList += ", ";
								sendList += "#";
							}
						}
						if(chkroom != null) {
							hostList += ", \"" + hostId + "\"";
						}
					}
					else {
						if(chkroom != null) {
							hostList += "\"" + hostId + "\"";
						}
					}
					hostList += "]";
				}
				
				//sql="update ubc_web_hotel_event set line1=?, line2=?, line3=?, size1=?, size2=?, size3=?, date=?, stime=?, ftime=?, room=?, color=?, logo=?  clitime=?, floor=(select addr1 from ubc_host where hostName='"+ room + "') where eid=?";
				sql="update ubc_web_hotel_event set line1=?, line2=?, line3=?, size1=?, size2=?, size3=?, date=?, stime=?, ftime=?, room=?, color=?, logo=?, clihour=?,"
						+ " climin=?, hostId=?, fdate=?, title=?, chkroom=?, hostList=?, sendList=?, visible=?, bgfile=?, floor=(select addr1 from ubc_host where hostName='"+  room +"')," 
						+ " touchtime=sysdate(), displayDate=?, displayTime=? where eid=" + eid;
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, first_line);
				pstmt.setString(2, second_line);
				pstmt.setString(3, third_line);
				pstmt.setFloat(4, first_size);
				pstmt.setFloat(5, second_size);
				pstmt.setFloat(6, third_size);
				pstmt.setDate(7, date);
				pstmt.setTime(8, stime);
				pstmt.setTime(9, ftime);
				pstmt.setString(10, room);
				pstmt.setString(11, color);
				pstmt.setString(12, logo);
				pstmt.setInt(13, clihour);
				pstmt.setInt(14, climin);
				pstmt.setString(15, hostId);
				pstmt.setDate(16, fdate);
				pstmt.setString(17, title);
				pstmt.setString(18, chkroom);
				pstmt.setString(19, hostList);
				pstmt.setString(20, sendList);
				pstmt.setString(21, visible);
				pstmt.setString(22, bg);
				pstmt.setDate(23, displayDate);
				pstmt.setTime(24, displayTime);
				pstmt.executeUpdate();
				
			}
 		 } catch (SQLException ex) {
			errlog.warn(ex.getMessage());
			infolog.info("update Fail : " + ex.getMessage());
		}
 		finally {
 			if(rs!=null) { rs.close(); }
 			if(pstmt!=null) {pstmt.close();}
 			if(conn!=null) {conn.close();}
 		}
 		//response.sendRedirect("list.jsp");
	%>
	<script>
		window.onload=function() {
			re();
		}
		function re() {
			window.returnValue='true';
			window.close();
		}
	</script>
</body>
</html>
