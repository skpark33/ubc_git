<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	/* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-store"); 

	 if(((String)session.getAttribute("id")) == null) {
		 session.invalidate();
		response.sendRedirect("index.jsp");
	} 
	else {
		session.removeAttribute("id");
		session.invalidate();
		response.sendRedirect("index.jsp");
	}
%>
