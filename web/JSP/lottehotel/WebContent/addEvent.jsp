<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@ page import="java.sql.*, javax.naming.*, javax.sql.*" %>
<%@ page import="org.apache.log4j.*" %>
<%! public Logger infolog = Logger.getLogger("infolog"); %>
<%! public Logger errlog = Logger.getLogger("errlog"); %>
<%
	if((String)session.getAttribute("id") == null ) {
		response.sendRedirect("index.jsp");		// 세션없을 시 url 직접접근 불허용
	}

	infolog.info("Event add JSP page Open");
	
	String start = request.getParameter("start");
	
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String sql = "";
	String lsql = "";
	String roomsql = "";
	String eidsql = "";
	String room = "";
	String opHTML = "";
	String chkHTML = "";
	String lobHTML = "";
	String hostId = "";
	String hostName = "";
	String addr1 = "";
	int eid = 0;
	
	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		
		sql = "select hostName from ubc_host where category like '%방형%' order by hostName"; 
		roomsql = "select hostName, addr1 from ubc_host where category like '%방형%' AND NOT category like '%가상단말%' order by hostName";
		lsql = "select hostId, hostName, addr1 from ubc_host where category like '%층형%' OR category like '%로비%' order by hostName";
		//eidsql = "select coalesce(max(eid), 0) + 1 as eid from ubc_web_hotel_event";
		
		// SelectBox Data
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		while(rs.next()) {
			room = rs.getString("hostName");
			opHTML = opHTML + "<option value=\"" + room + "\">" + room + "</option>\n";
		}
		rs = null;
		// 실제 존재하는 룸 단말 CheckBox Data
		rs = stmt.executeQuery(roomsql);
		while(rs.next()) {
			hostName = rs.getString("hostName");
			addr1 = rs.getString("addr1");
			chkHTML = chkHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"roomchk-field\" name=\"chkroom\" value=\"" + hostName + "#" + addr1 + "\" onclick=\"chk_room(this)\"/>  " + hostName + "</label><br/>\n";
		}
		rs = null;
		// 공용부 단말 CheckBox Data		
		rs = stmt.executeQuery(lsql);
		while(rs.next()) {
			hostId = rs.getString("hostId");
			hostName = rs.getString("hostName");
			addr1 = rs.getString("addr1");
			lobHTML = lobHTML + "<label class=\"chk_label\"><input type=\"checkbox\" class=\"lobchk-field\" name=\"chklob\" value=\"" + hostId + "#" + hostName + "#" + addr1 + "&&\"/>  " + hostName + "</label><br/>\n";
		}
		/* rs = null;
		rs = stmt.executeQuery(eidsql);
		while(rs.next()) {
			eid = rs.getInt("eid");
		} */
	} catch(SQLException e) {
		e.printStackTrace();
		errlog.warn(e.getMessage());
		infolog.info(e.getMessage() + "DB error");
	}
	finally {
		if(rs!=null) {rs.close();}
		if(stmt!=null) {stmt.close();}
		if(conn!=null) {conn.close();}
	}
%>
<!DOCTYPE html>
<html>
<head>
<base target="_self">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<base target=""/> 
<link rel='stylesheet' type='text/css' href='./css/addlayout.css'/>
<link rel='stylesheet' href='css/jquery-ui-1.10.3.datepicker.css' />
<link rel="shortcut icon" href="img/favicon2.ico" type="image/x-icon">
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui-1.10.3.datepicker.min.js"></script>
<script src="js/check.js"></script>
<script type="text/javascript">
	/* 체크박스 복수선택 불허용 */
	function chk_only(only) {
		var obj = document.getElementsByName("font-color");
		for(var i = 0; i<obj.length; i++) {
			if(obj[i] != only) {
				obj[i].checked = false;
			}
		}
	}
	/* 룸 체크박스 복수선택 불허용 */
	function chk_room(only) {
		var obj = document.getElementsByName("chkroom");
		for(var i = 0; i<obj.length; i++) {
			if(obj[i] != only) {
				obj[i].checked = false;
			}
		}
	}
	function fnSel(val) {
		var obj3 = document.getElementsByName("chkroom");
		var objlob = document.getElementsByName("chklob");
		for(var i=0; i<obj3.length; i++) {
			if( val.value.substring(0,5) == obj3[i].value.split("#")[0].substring(0,5)) { 
				obj3[i].checked = true;
				obj3[i].disabled = false;
				for(var j=0; j<objlob.length; j++) {
					if( obj3[i].value.split("#")[1] == objlob[j].value.split("&&")[0].split("#")[2] 
						|| objlob[j].value.split("&&")[0].split("#")[2] == '1F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B2F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B3F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B4F') {
						objlob[j].checked = true;
					}
					else {
						objlob[j].checked = false;
					}
				}
			}
			else if( val.value.substring(0,5) == 'CRYST' || val.value.substring(0,5) == 'LOTTE') {
				obj3[i].checked = false;
				obj3[i].disabled = true;
					for(var j=0; j<objlob.length; j++) {
					if( objlob[j].value.split("&&")[0].split("#")[2] == '3F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == '1F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B2F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B3F'
						|| objlob[j].value.split("&&")[0].split("#")[2] == 'B4F') {
						objlob[j].checked = true;
					}
					else {
						objlob[j].checked = false;
					}
				}
			}
			else {	
				obj3[i].checked = false;
				obj3[i].disabled = true;
				if(val.value.search('ROOM') == -1) {
					for(var j=0; j<objlob.length; j++) {
						if( objlob[j].value.split("&&")[0].split("#")[2] == '1F'
							|| objlob[j].value.split("&&")[0].split("#")[2] == 'B2F'
							|| objlob[j].value.split("&&")[0].split("#")[2] == 'B3F'
							|| objlob[j].value.split("&&")[0].split("#")[2] == 'B4F') {
							objlob[j].checked = true;
						}
						else {
							objlob[j].checked = false;
						}
					}
				}
			}
		} 
	}
	/* input type=file value Control ( value Delete )*/
	function filereset(form) {
		form.bgupload.select();
		document.selection.clear();
	}
	$(function() { 
		    $( "#date" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
			    onClose: function( selectedDate ) {
			    		$('#fdate').datepicker("option", "minDate", selectedDate);
			    }
		    });
	});
	$(function() { 
		    $( "#fdate" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
			    onClose: function( selectedDate ) {
			    		$('#date').datepicker("option", "maxDate", selectedDate);
			    }
		    });
	});
	$(document).ready(function() {
		if(<%=start %> == null) {
			$('#date, #fdate').val($.datepicker.formatDate($.datepicker.ATOM, new Date()));
		}
		else {
			$('#date, #fdate').val('<%=start %>');
		}
	});
</script>
<title>EVENT ADD</title>
</head>
<body>
<form name="listform" class="form-container" action="insertDB.jsp" method="POST" onsubmit="return check();" enctype="multipart/form-data">
	<div class="form-title">
		<h2>행사 등록</h2>
	</div>
	<div class="form-title">행사명
		<span><input class="title-form-field" type="text" name="title"/></span>
	</div>
	<div class="form-title"><pre>행사내용                                                                            글씨크기</pre></div>
	<div>
		<input class="form-field" type="text" name="first-line"/> 
		<input type="radio" name="first-size" value="1">1
		<input type="radio" name="first-size" value="2">2
		<input type="radio" name="first-size" value="3" checked="checked">3
		<input type="radio" name="first-size" value="4">4
		<input type="radio" name="first-size" value="5">5
	</div>
	<div>
		<input class="form-field" type="text" name="second-line"/> 
		<input type="radio" name="second-size" value="1">1
		<input type="radio" name="second-size" value="2">2
		<input type="radio" name="second-size" value="3" checked="checked">3
		<input type="radio" name="second-size" value="4">4
		<input type="radio" name="second-size" value="5">5
	</div>
	<div>
		<input class="form-field" type="text" name="third-line"/>
		<input type="radio" name="third-size" value="1">1
		<input type="radio" name="third-size" value="2">2
		<input type="radio" name="third-size" value="3" checked="checked">3
		<input type="radio" name="third-size" value="4">4
		<input type="radio" name="third-size" value="5">5
	</div>
	<!-- <div class="form-title">글씨색상</div> -->
	<fieldset class="form-fieldset">
		<legend>글씨색상</legend>
		<div class="form-checkbox">
			<label class="color-label"><input class="font-color" type="checkbox" name="font-color" value="BASIC" onclick="chk_only(this)"/>  BASIC</label>
			<label class="color-label"><input class="font-color" type="checkbox" name="font-color" value="YELLOW" checked="checked" onclick="chk_only(this)"/>  YELLOW</label>
			<label class="color-label"><input class="font-color" type="checkbox" name="font-color" value="RED" onclick="chk_only(this)"/>  RED</label>
			<label class="color-label"><input class="font-color" type="checkbox" name="font-color" value="BLUE" onclick="chk_only(this)"/>  BLUE</label>
			<label class="color-label"><input class="font-color" type="checkbox" name="font-color" value="GREEN" onclick="chk_only(this)"/>  GREEN</label>
		</div>
	</fieldset>
	<div class="form-title"><pre>행사시작날짜        행사종료날짜		행사표출시간</pre></div> 
	<div>
		<input class="date-form-field" type="text" name="date" id="date" readonly/><label for="fdate"> -</label>
		<input class="date-form-field" type="text" name="fdate" id="fdate" readonly/>
		<span class="blankspan"></span>
		<input class="prev-form-field" type="text" name="clihour" id="clihour" value="3" maxlength="2" /><label for="clihour">시간</label>
	      <input class="prev-form-field" type="text" name="climin" id="climin" value="0" maxlength="2" /><label for="climin">분 전</label>	
		<span class="time-ex">기본 시작시간 3시간전</span>
	</div>

	<div class="form-title"><pre>시작시간      종료시간</pre></div>
	<div>
		<input class="time-form-field" type="text" name="stime" maxlength="4" onkeypress="inNum();"/><label for="ftime"> -</label>
		<input class="time-form-field" type="text" name="ftime" maxlength="4" onkeypress="inNum();"/>
		<span class="time-ex">ex) 09:00am = 0900 // 09:00pm = 2100</span>
	</div>
	<div class="form-title">행사장</div>
	<div>
		<select class="sel" name="room" onchange="fnSel(this)">
			<option value="">장소선택</option>
			<%=opHTML %>
		</select>
		<fieldset id="form-location">
			<legend>[행사표출장소]</legend>
			<fieldset class="info-location-room">
				<legend>행사장</legend>
				<%=chkHTML %>
			</fieldset>
			<fieldset class="info-location-lobby">
				<legend>공용부</legend>
				<%=lobHTML %>
			</fieldset>
		</fieldset>
	</div>
	<div class="form-title">배경이미지 등록 <span><label class="chk_label"><input type="checkbox" class="text-hidden-chkbox" name="visible" value="y"/>글숨기기</label></span></div>
	<div>
		<input class="upload-form-field" type="file" name="bgupload" id="bgupload" title="파일 찾기" value="파일찾기"/>
		<!-- <span><input type="button" name="clear" id="clear" value="취소" onclick="filereset(this.form)"/></span> -->
	</div>
	<div class="form-title">로고이미지 등록</div>
	<div>
		<input class="upload-form-field" type="file" name="upload" id="upload" title="파일 찾기" value="파일찾기"/>
	</div>
	<div class="submit-container">
		<%-- <input type="hidden" name="eid" value="<%=eid %>"/> --%>
		<input class="submit-button" type="submit" value="등록" />
		<input class="submit-button" type="button" value="취소" onclick="window.close()"/> 
		<!-- <input class="submit-button" type="button" value="취소" onclick="location.href='list.jsp'"/> -->
	</div>
</form>
</body>
</html>
