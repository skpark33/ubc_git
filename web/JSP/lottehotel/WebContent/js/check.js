function check() {
	if(document.listform.title.value=="") {
		alert("행사명을 입력하세요");
		document.listform.title.focus();
		return false;
	}
	if(document.listform.stime.value=="") {
		alert("행사 시작시간을 입력하세요");
		document.listform.stime.focus();
		return false;
	}
	if(document.listform.ftime.value=="") {
		alert("행사 종료시간을 입력하세요");
		document.listform.ftime.focus();
		return false;
	}
	if(document.listform.clihour.value=="") {
		alert("행사 표출 시간을 입력하세요");
		document.listform.clihour.focus();
		return false;
	}
	if(document.listform.clihour.value > 23) {
		alert("23시간 이상 입력하실 수 없습니다. 표출시간을 확인하세요");
		document.listform.clihour.value = 23;
		document.listform.clihour.focus();
		return false;
	}
	if(document.listform.climin.value=="") {
		alert("행사 표출 분을 입력하세요");
		document.listform.climin.focus();
		return false;
	}
	if(document.listform.climin.value > 59) {
		alert("59분 이상 입력하실 수 없습니다. 표출시간을 확인하세요");
		document.listform.climin.value=59;
		document.listform.climin.focus();
		return false;
	}
	if(document.listform.room.value=="") {
		alert("행사 장소를 선택하세요");
		return false;
	}
	if(document.listform.stime.value > document.listform.ftime.value) {
		alert("이 행사는 2일에 걸쳐 진행됩니다.");
		return true;
	}
	
	var room = document.getElementsByName("chkroom");
	var lob = document.getElementsByName("chklob");
	var chk1;
	var chk2;
	for(var i =0; i<room.length; i++) {
		if(room[i].checked == false){
			chk1 = false;
		}
		else {
			chk1 = true;
			break;
		}
	}
	for( var j = 0; j<lob.length; j++) {
		if(lob[j].checked == false) {
			chk2 = false;
		}
		else {
			chk2 = true;
			break;
		}
	}
	if(chk1 == false && chk2 == false) {
		alert("표출장소 한 곳은 선택하셔야 합니다.");
		return false;
	}
	else {
		return true;
	}
}
function inNum() {
	if((event.keyCode<48)||(event.keyCode>57))
		event.returnValue=false;
}
