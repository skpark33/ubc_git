	/* 행사 삭제 버튼 스크립트 */
	function chk_delete(eid) {
		var num = eid;
		var hide_S = document.getElementById("start");
		var hide_E = document.getElementById("end");
		if(confirm("선택한 행사를 삭제하시겠습니까?") == true ) {
			location.href='delete.jsp?eid=' + num + '&start=' + hide_S.value + '&end=' + hide_E.value;
		}
		else {
			return;
		}
	}
	/* 날짜검색 필터 */
	function datefilter() {
		var sDate = document.getElementById("toDate");
		var eDate = document.getElementById("fromDate");
		if( sDate.value == "" || eDate.value == "") {
			alert("날짜를 선택하세요");
		}
		else {
			location.href='list.jsp?chk=y&start=' + sDate.value + '&end=' + eDate.value;
		}
	}
	/* 행사 추가 버튼 모달스크립트 */
	function addModal() {
		var date = document.getElementById("toDate");
		var style = "dialogWidth: 600px; dialogHeight:680px; center:yes; scroll:1; resizable:1; status:no;";
		var returnValue = window.showModalDialog("addEvent.jsp?start=" + date.value, window, style);
		if(returnValue== 'true') {
			location.reload();
		}
		else {
			return;
		}
	}
	/* 행사 수정 버튼 모달스크립트 */
	function modModal(eid) {
		var style = "dialogWidth:600px; dialogHeight:680px; center:yes; scroll:1; resizable:no; status:no;";
		var num = eid;
		var returnValue = window.showModalDialog("update.jsp?eid=" + num, window, style);
		if(returnValue == 'true') {
			location.reload();
		}
		else {
			return;
		}
	}
	/* 로비형 미리보기 */
	function lobby_view(val) {
		var hostId = val;
		var start = document.getElementById("toDate");
		var style = "width=1085px, height=1320px,scrollbars=1, status=no";
		window.open("lobbyprev.jsp?start="+ start.value + "&hostId=" + hostId,"",style);
	}
	
	/* 룸형 미리보기 */
	function room_view(eid) {
		var obj = eid;
		var style;
		style = "width=1435px; height=895px; scrollbars=yes; status=no";
		window.open("roomprev.jsp?eid=" + obj, "", style);
	}
	/* 로비에 뿌려질 각 행사 미리보기 */
	function event_view(eid, date) {
		var obj = eid;
		var start = date; 
		var style = "width=1080px; height=410px; scrollbars=no; status=no";
		window.open("evtprev.jsp?eid=" + obj + "&start=" + start, "", style);
	}
	function logout() {
		location.href='logout.jsp';
	}
