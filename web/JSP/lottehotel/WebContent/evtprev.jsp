<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
    /* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-store");
	
	int eid = Integer.parseInt(request.getParameter("eid"));
	String start = request.getParameter("start");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>event preview</title>
<style>
	* {
		margin: 0;
		padding: 0;
		background-color: black;
	}
	html, body { height: 100%; overflow: hidden; }
</style>
</head>
<body>
	<iframe src="eventsprev.jsp?eid=<%=eid %>&start=<%=start %>" width="100%" height="100%" scrolling="yes" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
</html>