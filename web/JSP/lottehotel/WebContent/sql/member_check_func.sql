DELIMITER $$

CREATE DEFINER=`ubc`@`%` FUNCTION `Member_ValidCheck`(id varchar(255), pw varchar(50)) RETURNS int(11)
BEGIN
declare chk int;
select  1 into chk from dbo.ubc_user where userid=id AND password = dbo.Password_Encode(pw);
RETURN chk;
END