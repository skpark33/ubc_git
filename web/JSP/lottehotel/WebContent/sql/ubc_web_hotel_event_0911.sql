SELECT * FROM dbo.ubc_web_hotel_event;delimiter $$

CREATE TABLE `ubc_web_hotel_event` (
  `eid` int(11) NOT NULL,
  `date` date NOT NULL,
  `fdate` date NOT NULL,
  `stime` time NOT NULL,
  `ftime` time NOT NULL,
  `room` varchar(50) NOT NULL,
  `floor` varchar(32) NOT NULL,
  `line1` varchar(255) NOT NULL,
  `line2` varchar(255) NOT NULL,
  `line3` varchar(255) NOT NULL,
  `size1` int(11) NOT NULL,
  `size2` int(11) NOT NULL,
  `size3` int(11) NOT NULL,
  `color` varchar(32) NOT NULL,
  `logo` varchar(128) DEFAULT NULL,
  `clihour` int(11) DEFAULT NULL,
  `climin` int(11) DEFAULT NULL,
  `hostId` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `chkroom` varchar(50) DEFAULT NULL,
  `hostList` varchar(255) DEFAULT NULL,
  `sendList` varchar(255) DEFAULT NULL,
  `visible` varchar(10) DEFAULT NULL,
  `bgfile` varchar(255) DEFAULT NULL,
  `touchtime` datetime NOT NULL,
  `displayDate` date NOT NULL,
  `displayTime` time NOT NULL,
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

