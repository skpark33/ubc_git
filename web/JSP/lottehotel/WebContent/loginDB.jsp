<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, javax.naming.*, javax.sql.*" %>
<%@ page import="org.apache.log4j.*" %>
<%! public Logger infolog = Logger.getLogger("infolog"); %>
<%! public Logger errlog = Logger.getLogger("errlog"); %>
<%
	/* Cache remove */
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Pragma","no-cache"); 
	
	// Session ID, PWD GET
	session.setAttribute("id", request.getParameter("userid"));
	session.setAttribute("pwd", request.getParameter("userpwd"));
	
	Connection conn = null;
	PreparedStatement pstmt= null;
	ResultSet rs = null;
	//CallableStatement cs = null;
	String sql = "";
	int chk= 0;
	
	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		
		sql = "select Member_ValidCheck(?,?) as chk";
		pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, (String)session.getAttribute("id"));
		pstmt.setString(2, (String)session.getAttribute("pwd"));
		rs = pstmt.executeQuery();
		while(rs.next()) {
			chk = rs.getInt("chk");	
		}
		/*Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		conn=DriverManager.getConnection("jdbc:sqlserver://211.232.57.219:1433;DatabaseName=ubc", "ubc", "-507263a");
		
		 sql = "{call usp_Member_ValidCheck(?,?,?)}"; 	// mssql 프로시저 콜
		cs = conn.prepareCall(sql);
		cs.setString(1, (String)session.getAttribute("id"));
		cs.setString(2, (String)session.getAttribute("pwd"));
		cs.registerOutParameter(3,Types.INTEGER );
		cs.execute();
		
		chk = cs.getInt(3);*/
		
	} catch(SQLException e) {
		System.out.println(e.getMessage());
		errlog.warn(e.getMessage());
	} finally { 
		//if(cs!=null) { cs.close();}
		if(pstmt!=null) { pstmt.close(); }
		if(rs!=null) {rs.close(); }
		if(conn!=null) { conn.close(); }
	}
	
	if(chk==1) {
			infolog.info("login Success");
			response.sendRedirect("list.jsp");
	}
	else {
		infolog.info("login Fail");
		session.removeAttribute("id");
		session.invalidate();
		response.sendRedirect("index.jsp");
	}
%>
