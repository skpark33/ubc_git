<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, java.util.Calendar" %>
<%@ page import="javax.naming.*, javax.sql.*" %>
<%@ page session="false" %>
<%
	/* Cache remove */
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Pragma","no-cache"); 

	int data = Integer.parseInt(request.getParameter("data"));		// 하루 행사 수 받아옴. 최대 6개 고정
	String start = request.getParameter("start");
	String hostId = request.getParameter("hostId");
	
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String sql = "";
	String stime = "";
	String room = "";
	String floor = "";
	String line1 = "";
	String line2 = "";
	String line3 = "";
	int size1 = 0;
	int size2 = 0;
	int size3 = 0;
	String color = "";
	String logo = "";
	String trHTML = "";
	int cnt = 1;
	
	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		
		sql = "select * from ubc_web_hotel_event"
			+ " where date<=DATE_FORMAT('" + start + "', '%Y-%m-%d') AND fdate>=DATE_FORMAT('" + start + "', '%Y-%m-%d')"  
			+ " AND hostList like '%" + hostId + "%'"
			+ " ORDER BY stime";
		
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		while(rs.next()) {
			stime = rs.getString("stime").substring(0,5);	// ss REMOVE
			room = rs.getString("room");
			floor = rs.getString("floor");
			line1 = rs.getString("line1");
			line2 = rs.getString("line2");
			line3 = rs.getString("line3");
			size1 = rs.getInt("size1");
			size2 = rs.getInt("size2");
			size3 = rs.getInt("size3");
			color = rs.getString("color");
			logo = rs.getString("logo");
			if( data > 5 ) { 	trHTML += "rollingDiv.addRollingItem(\""; }
			trHTML += "<table id=\'event_" + data +  "\' class=\'" + color + "\' width=\'94%\' align=\'center\' border=\'0\'>";
			if(data < 4) {
				trHTML += "\t\t\t\t<tr><td class=\"topblank_" + data + "\" colspan=\"5\"></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"logo_" + data + "\" rowspan=\"5\"><!--img src=\"uploadfiles/" + logo + "\" width=\"96px\" height=\"64px\" onerror=\"this.src=\'img/blank.png\'\"--></td>\n";
				trHTML += "\t\t\t\t\t<td id=\"line1_" + data + "_" + size1 + "\" colspan=\"3\">" + line1 +  "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"logo_" + data + "\" rowspan=\"5\"><!--img src=\"uploadfiles/" + logo + "\" width=\"96px\" height=\"64px\" onerror=\"this.src=\'img/blank.png\'\"--></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"line2_" + data + "_" + size2 + "\" colspan=\"3\">" + line2 + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"line3_" + data + "_" + size3 + "\" colspan=\"3\">" + line3 + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td class=\"midblank_" + data + "\" colspan=\"3\"></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"floor" + data + "\">" + floor + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"time" + data + "\">" + stime + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"room" + data + "\">" + room + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td class=\"bottomblank_" + data + "\" colspan=\"5\"></td></tr>\n";
			}
			 else {
				trHTML += "<tr><td class=\'topblank_" + data + "\' colspan=\'3\'></td></tr>";
				trHTML += "<tr><td id=\'logo_" + data + "\' rowspan=\'3\'></td>";
				trHTML += "<td id=\'line1_" + data + "_" + size1 + "\' class=\'line" + data + "\'>" + line1 +  "</td>";
				trHTML += "<td id=\'floor" + data + "\'>" + floor + "</td></tr>";
				trHTML += "<tr><td id=\'line2_" + data + "_" + size2 + "\'class=\'line" + data + "\'>" + line2 + "</td>";
				trHTML += "<td id=\'time" + data + "\'>" + stime + "</td></tr>";
				trHTML += "<tr><td id=\'line3_" + data + "_" + size3 + "\'class=\'line" + data + "\'>" + line3 + "</td>";
				trHTML += "<td id=\'room" + data + "\'>" + room + "</td></tr>";
				trHTML += "<tr><td class=\'bottomblank_" + data + "\' colspan=\'3\'></td></tr>";
			}
			trHTML += "</table>";
			if(data < 6 ) {
				if(data > cnt) {
					trHTML += "<div id=\'blank" + data + "\'></div>";
				}
			}
			else {
				trHTML += "<div id=\'blank" + data + "\'></div>";
			}
			if( data > 5 ) { trHTML += "\");\n"; }
			++cnt;
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
	finally{
		if(rs!=null) { rs.close(); }
		if(stmt!=null) { stmt.close(); }
		if(conn!=null) { conn.close(); }
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel='stylesheet' type='text/css' href='css/layout.css'>
<link rel="shortcut icon" href="img/favicon2.ico" type="image/x-icon">
<script type="text/javascript" src="js/jquery-1.2.3.pack.js"></script>
<script type="text/javascript" src="js/jquery.rolling.js"></script>
<title>LOTTE HOTEL</title>
<script>
	$(function() {
		var rollingDiv = $("#rolling");
		rollingDiv.rolling("up", 1080, 210, 5);
		 <%=trHTML %>  
		rollingDiv.startRolling(10, 5000, 150);
	});
</script>
</head>
<body>
		<table border="0" width="100%" height="1320px" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                        <td id="top" colspan="2" height="10px" > </td>
                  </tr>
                  <tr>
                        <td id="banner" align="center" valign="middle">
                              <table  width="904px" cellpadding="0" cellspacing="0"  style="color: #f4f0eb; vertical-align: top;">
                                    <tr>
                                          <td rowspan="2" id="te" align="left"></td>
                                          <td height="50px"></td>
                                    </tr>
                                    <tr>
                                          <td id="ymd" align="left"><%=start %></td>
                                    </tr>
                              </table>
                        </td>
                  <tr>
                        <td colspan="2" height="15em" > </td> 
                  </tr>
                  <tr>
                  	<td id="evtd"> 
            	<% if(data > 5 ) { %>
 	                 <div id="rolling"></div> 
            	<% } 
            	else { %>
				 <%=trHTML %>  
            	<% } %>
                  </td>
                  </tr>
            </table>
</body>
</html>
