<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page session="false" %>
<%@ page import="java.sql.*, javax.sql.*, javax.naming.*" %>
<%
	request.setCharacterEncoding("UTF-8");
	int eid = Integer.parseInt(request.getParameter("Eid"));
	
	/* Cache remove */
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Pragma","no-cache"); 

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String sql = "";
	String tsql = "";
	String stime = "";
	String ftime = "";
	String line1 = "";
	String line2 = "";
	String line3 = "";
	String size1 = "";
	String size2 = "";
	String size3 = "";
	String color = "";
	String logo = "";
	String visible = "";
	String bg = "";
	
	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		stmt = conn.createStatement();

		tsql = "update ubc_web_hotel_event set touchtime=sysdate() where eid=" + eid;
		sql = "select * from  ubc_web_hotel_event where eid=" + eid;
		
		stmt.executeUpdate(tsql);

		rs = stmt.executeQuery(sql);
		while(rs.next()) {
			stime = rs.getString("stime").substring(0,5);
			ftime = rs.getString("ftime");
			line1 = rs.getString("line1");
			line2 = rs.getString("line2");
			line3 = rs.getString("line3");
			size1 = rs.getString("size1");
			size2 = rs.getString("size2");
			size3 = rs.getString("size3");
			color = rs.getString("color");
			logo = rs.getString("logo");
			bg = rs.getString("bgfile");
			visible = rs.getString("visible");
		}
		if(visible == null) {
			visible = "n";
		}
		
	} catch(SQLException e) {
		e.printStackTrace();
	}
	finally {
		if(rs!=null) { rs.close(); }
		if(stmt!=null) { stmt.close(); }
		if(conn!=null) { conn.close(); }
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<link rel='stylesheet' type='text/css' href='css/roomlayout.css'>
<link rel="shortcut icon" href="img/favicon2.ico" type="image/x-icon">
<script src="js/jquery-1.9.1.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#tb_y').find('tr').hide();
	});
</script>
<title>ROOM</title>
</head>
<body>
	<table class="<%=color %>" id="tb_<%=visible %>" border="0" cellpadding="0" cellspacing="0" style="background: url(uploadfiles/<%=bg %>) no-repeat center;  background-size: cover; ">
		<tr><td height="20px">
		</td></tr>
		<tr><td id="logo">
			<img src="uploadfiles/<%=logo %>" onerror="this.src='img/blank.png'">
		</td></tr>
		<tr><td id="line1_<%=size1 %>">
			<%=line1 %>	
		</td></tr>
		<tr><td id="line2_<%=size2 %>">
			<%=line2 %>
		</td></tr>
		<tr><td id="line3_<%=size3 %>">
			<%=line3 %>
		</td></tr>
		<tr><td height="100px">
		</td></tr>
		<tr><td id="time" >
			<%=stime %>
		</td></tr>
		<tr><td height="30px">
		</td></tr>
	</table>
</body>
</html>
