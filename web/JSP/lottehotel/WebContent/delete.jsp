<%@page import="javax.annotation.processing.FilerException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, javax.naming.*, javax.sql.*" %>
<%@ page import="java.io.*" %>
<%@ page import="org.apache.log4j.*" %>
<%! public Logger infolog = Logger.getLogger("infolog"); %>
<%! public Logger errlog = Logger.getLogger("errlog"); %>
<%
	int eid = Integer.parseInt(request.getParameter("eid"));
	String start = request.getParameter("start");
	String end = request.getParameter("end");
	
	infolog.info("delete data....");
	
	String sql = "";
	String sitesql = "";
	String filesql = "";
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	//String filePath = "C:\\Users\\DELL\\workspace\\lottehotel\\WebContent\\uploadfiles\\";	// file SAVE location.
	String filePath = "C:/Apache Software Foundation/Tomcat 7.0/webapps/lottehotel/uploadfiles/";	// .Server file SAVE location.
	//String filePath = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/lottehotel/uploadfiles/";	// .Server file SAVE location.
	
	 try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		
		String siteId = "";
		String logo = "";
		String bg = "";
		
		if(conn == null) {
			infolog.info("delete.jsp DB Connection Fail");
		}
		else {
			sitesql = "select siteId FROM ubc_host where category like '%Hotel%' limit 1";
			pstmt = conn.prepareStatement(sitesql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				siteId = rs.getString("siteId");
			}
			rs = null;
			filesql = "select logo, bgfile FROM ubc_web_hotel_event where eid = " + eid;
			pstmt = conn.prepareStatement(filesql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				logo = rs.getString("logo");
				bg = rs.getString("bgfile");
			}
			rs = null;
			sql = "delete from ubc_web_hotel_event where eid=?";		
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, eid);
			pstmt.executeUpdate();	
			
			try { 
				// 첨부파일 제거
				if(logo!=null || !("null".equals(logo))) {
					File logofile = new File(filePath + logo);
					if(logofile.exists()) {
						logofile.delete();
					}
				}
				if(bg!=null || !("null".equals(bg))) {
					File bgfile = new File(filePath + bg);
					if(bgfile.exists()) {
						bgfile.delete();
					}
				}
			} catch(Exception f) {
				f.printStackTrace();
			}
	 	}
	 } catch(SQLException e) {
		 e.printStackTrace();
		errlog.warn(e.getMessage());
		infolog.info("delete Fail : " + e.getMessage());
	 }
	 finally {
		 if(pstmt!=null) {pstmt.close();}
		 if(conn!=null) {conn.close();}
	 }
	if( "null".equals(start) || "null".equals(end) ) {
		response.sendRedirect("list.jsp");
	}
	else if( start != null && end != null) {
		response.sendRedirect("list.jsp?chk=y&start=" + start + "&end=" + end);
	}
%>
