<%@page import="org.apache.catalina.startup.Catalina"%>
<%@page import="java.nio.channels.FileChannel"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.sql.*, java.util.*, java.io.*, java.nio.*, java.util.Calendar " %>
<%@ page import="javax.naming.*, javax.sql.*" %>
<%@ page import="com.oreilly.servlet.MultipartRequest" %>
<%@ page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>
<%@ page import="org.apache.log4j.*" %>
<%! public Logger infolog = Logger.getLogger("infolog"); %>
<%! public Logger errlog = Logger.getLogger("errlog"); %>
<%
	String ok = request.getParameter("new");

	SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat cdf = new SimpleDateFormat("yyyy/MM/dd");

	infolog.info("Insert Data...");
	
	Calendar now = Calendar.getInstance();
	String minute = now.get(Calendar.MINUTE) + "";
	String sec = now.get(Calendar.SECOND) + "";
	String copy = "_" + minute + sec; 
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>EVENT INSERT in DB</title>
</head>
<body>
	<%	
		FileChannel fin;
		FileChannel fout;
		String sql = "";
		String flsql = "";
		String neweidsql = "";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String filePath = "C:\\Users\\DELL\\workspace\\lottehotel\\WebContent\\uploadfiles\\";	// file SAVE location.
		//String filePath = "C:/Apache Software Foundation/Tomcat 7.0/webapps/lottehotel/uploadfiles/";	// .Server file SAVE location.
		//String filePath = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/lottehotel/uploadfiles/";	// .219Server file SAVE location.
		int maxSize = 10 * 1024 * 1024;	// 10MB limit.
		
		String data = "";
		String hostList = null;
		String sendList = null;
		String floor = "";
		String hostId = "";
		String siteId = "";
		int eid=0;
		
 		 try {
 			Class.forName("com.mysql.jdbc.Driver");
 			Class.forName("org.apache.commons.dbcp.PoolingDriver");
 			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");

			MultipartRequest multi = new MultipartRequest(request, filePath, maxSize, "utf-8", new DefaultFileRenamePolicy());
			
			//int eid = Integer.parseInt(multi.getParameter("eid"));
			Date date = new java.sql.Date(df.parse(multi.getParameter("date")).getTime());
			Date fdate = new java.sql.Date(df.parse(multi.getParameter("fdate")).getTime());
			Time stime = new java.sql.Time(sdf.parse(multi.getParameter("stime")).getTime());
			Time ftime = new java.sql.Time(sdf.parse(multi.getParameter("ftime")).getTime());
			int clihour = Integer.parseInt(multi.getParameter("clihour"));
			int climin = Integer.parseInt(multi.getParameter("climin"));
			String title = multi.getParameter("title");
			String first_line = multi.getParameter("first-line");
			String second_line = multi.getParameter("second-line");
			String third_line = multi.getParameter("third-line");
			int first_size = Integer.parseInt(multi.getParameter("first-size"));
			int second_size = Integer.parseInt(multi.getParameter("second-size"));
			int third_size = Integer.parseInt(multi.getParameter("third-size"));
			String color = multi.getParameter("font-color");
			String room = multi.getParameter("room");
			String bg = multi.getFilesystemName("bgupload");
			String logo = multi.getFilesystemName("upload");
			String oldfile = multi.getParameter("old_upload");	// 업로드 된 이전 logo 파일명
			String oldlogochk = multi.getParameter("old_logo");	// 삭제 체크박스
			String oldbg = multi.getParameter("old_bg_upload");	// 업로드 된 이전 bg 파일명
			String oldbgchk = multi.getParameter("old_bg");		// 삭제 체크박스 
			String chkroom = multi.getParameter("chkroom");
			String []chklob = multi.getParameterValues("chklob");
			String visible = multi.getParameter("visible");
						
			if(chkroom != null) {
				chkroom = chkroom.split("#")[0];
			}
			
			// UBC cli 시간처리 데이터
			Time displayTime = new java.sql.Time(sdf.parse(multi.getParameter("stime")).getTime()-(clihour*60*60*1000)-(climin*60*1000));
			Date displayDate = null;
			if(sdf.parse(sdf.format(displayTime)).getTime() > sdf.parse(sdf.format(stime)).getTime()) {
				displayDate = new java.sql.Date(df.parse(multi.getParameter("date")).getTime()-(24*60*60*1000));
			}
			else {
				displayDate = new java.sql.Date(df.parse(multi.getParameter("date")).getTime());
			}
			
			// logo file 
			if( logo == null) {
				if("null".equals(oldfile) || oldfile == null) {
					logo = null;
				}
				else {
					try {
						if(oldlogochk!=null && oldlogochk.equals("y")) {
							logo = null;
						}
						else {
							// 새로저장(다른이름저장)시 첨부파일 복사
							String _fileName=oldfile.substring(0, oldfile.lastIndexOf('.'));
							String _ext = oldfile.substring(oldfile.lastIndexOf('.'));
							File oriFile = new File(filePath + oldfile);
							File newFile = new File(filePath + _fileName + copy + _ext);
							fin = new FileInputStream(oriFile).getChannel();
							fout = new FileOutputStream(newFile).getChannel();
							
							logo = _fileName+copy+_ext;					// 새로 저장했을때 기존 파일 유지
							fin.transferTo(0, fin.size(), fout);
							
							fout.close();
							fin.close();
						}
					} catch(Exception e) {
						e.printStackTrace();
						errlog.warn(e.getMessage());
					}
				}
			}
			// background file
			if( bg == null) {
				if("null".equals(oldbg) || oldbg == null) {
					bg = null;
				}
				else {
					try {
						if(oldbgchk!=null && oldbgchk.equals("y")) {
							bg = null;
						}
						else {
							// 새로저장(다른이름저장)시 첨부파일 복사
							String _fileName=oldbg.substring(0, oldbg.lastIndexOf('.'));
							String _ext = oldbg.substring(oldbg.lastIndexOf('.'));
							File oriFile = new File(filePath + oldbg);
							File newFile = new File(filePath + _fileName + copy + _ext);
							fin = new FileInputStream(oriFile).getChannel();
							fout = new FileOutputStream(newFile).getChannel();
							
							bg = _fileName+copy+_ext;
							fin.transferTo(0, fin.size(), fout);
							
							fout.close();
							fin.close();
						}
					} catch(Exception e) {
						e.printStackTrace();	
						errlog.warn(e.getMessage());
					}
				}
			}
			
			if(conn == null) {
				infolog.info("insertDB Connection Fail");
			}
			else {
				// 새로저장(다른이름저장)시 eid 새로 부여
				//if( ok != null) {
					neweidsql = "select coalesce(max(eid), 0) + 1 as eid from ubc_web_hotel_event";
					pstmt = conn.prepareStatement(neweidsql);
					rs = pstmt.executeQuery();
					while(rs.next()) {
						eid = rs.getInt("eid");
					}
				//}
				
				flsql = "select addr1, hostId, siteId from ubc_host where hostName=?";
				pstmt = conn.prepareStatement(flsql);
				if(chkroom != null) {
					pstmt.setString(1, chkroom);
				}
				else {
					pstmt.setString(1, room);
				}
				rs = pstmt.executeQuery();
				while(rs.next()) {
					floor = rs.getString("addr1");
					hostId = rs.getString("hostId");
					siteId = rs.getString("siteId");
				}
				// hostList, sendList 데이터 생성
				if(true) {
					hostList = "[";
					if(chklob != null) {
						for(int i = 0 ; i<chklob.length; i++) {
							data += chklob[i];
						}
						String []arr = data.split("&&");
						String [][]brr = new String[arr.length][2];
						for(int j=0; j<arr.length; j++) {
							for(int k=0; k<2; k++) {
								brr[j][k] = arr[j].split("#")[k];
							}
						}
						sendList = "";
						for(int i = 0; i<chklob.length; i++) {
							hostList += "\"" + brr[i][0] + "\"";
							sendList += brr[i][1];
							if(i < chklob.length-1) {
								hostList += ", ";
								sendList += "#";
							}
						}
						if(chkroom != null) {
							hostList += ", \"" + hostId + "\"";
						}
					}
					else {
						if(chkroom != null) {
							hostList += "\"" + hostId + "\"";
						}
					}
					hostList += "]";
				}
				
				sql="insert into ubc_web_hotel_event values(?,?,?,?,?,?,(select addr1 from ubc_host where hostName='"+  room +"'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate(),?,?)";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, eid);
				pstmt.setDate(2, date);
				pstmt.setDate(3, fdate);
				pstmt.setTime(4, stime);
				pstmt.setTime(5, ftime);
				pstmt.setString(6, room);
				pstmt.setString(7, first_line);
				pstmt.setString(8, second_line);
				pstmt.setString(9, third_line);
				pstmt.setInt(10, first_size);
				pstmt.setInt(11, second_size);
				pstmt.setInt(12, third_size);
				pstmt.setString(13, color);
				pstmt.setString(14, logo);
				pstmt.setInt(15, clihour);
				pstmt.setInt(16, climin);
				pstmt.setString(17, hostId);
				pstmt.setString(18, title);
				pstmt.setString(19, chkroom);
				pstmt.setString(20, hostList);
				pstmt.setString(21, sendList);
				pstmt.setString(22, visible);
				pstmt.setString(23, bg);
				pstmt.setDate(24, displayDate);
				pstmt.setTime(25, displayTime);
				pstmt.executeUpdate();
				
			}
 		 } catch (Exception ex) {
			ex.printStackTrace();
			errlog.warn(ex.getMessage());
			infolog.info("insert Fail : " + ex.getMessage());
 		 }
 		finally {
 			if(rs!=null) { rs.close(); }
 			if(pstmt!=null) {pstmt.close();}
 			if(conn!=null) {conn.close();}
 		}
 		//response.sendRedirect("addEvent.jsp");
	%>
	<script>
		window.onload=function() {
			re();
		}
		function re() {
			window.returnValue='true';
			window.close();
		}
	</script>
</body>
</html>
