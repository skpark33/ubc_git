<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, java.util.Calendar" %>
<%@ page import="javax.naming.*, javax.sql.*" %>
<%@ page session="false" %>
<%
	/* Cache remove */
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Pragma","no-cache"); 

	int data = Integer.parseInt(request.getParameter("data"));		// 하루 행사 수 받아옴. 최대 6개 고정
	int eid = Integer.parseInt(request.getParameter("eid"));
	
	Calendar now = Calendar.getInstance();
	String hour = now.get(Calendar.HOUR_OF_DAY) + "";
	String minute = now.get(Calendar.MINUTE) + "";
	String clock = hour + ":" +  minute;
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel='stylesheet' type='text/css' href='css/preview.css'>
<link rel="shortcut icon" href="img/favicon2.ico" type="image/x-icon">
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<title>LOTTE HOTEL</title>
</head>
<body>
<%
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String sql = "";
	String stime = "";
	String room = "";
	String floor = "";
	String line1 = "";
	String line2 = "";
	String line3 = "";
	int size1 = 0;
	int size2 = 0;
	int size3 = 0;
	String color = "";
	String logo = "";
	String trHTML = "";
	int cnt = 1;
	
	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		
		sql = "select * from ubc_web_hotel_event where eid=" + eid;
		
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		while(rs.next()) {
			stime = rs.getString("stime").substring(0,5);	// ss REMOVE
			room = rs.getString("room");
			floor = rs.getString("floor");
			line1 = rs.getString("line1");
			line2 = rs.getString("line2");
			line3 = rs.getString("line3");
			size1 = rs.getInt("size1");
			size2 = rs.getInt("size2");
			size3 = rs.getInt("size3");
			color = rs.getString("color");
			logo = rs.getString("logo");
			trHTML = trHTML + "\t\t\t\t<table id=\"event_" + data +  "\" class=\"" + color + "\" width=\"94%\" align=\"center\" border=\"0\">\n";
			if(data < 4) {
				trHTML += "\t\t\t\t<tr><td class=\"topblank_" + data + "\" colspan=\"5\"></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"logo_" + data + "\" rowspan=\"5\"><!--img src=\"uploadfiles/" + logo + "\" width=\"96px\" height=\"64px\" onerror=\"this.src=\'img/blank.png\'\"--></td>\n";
				trHTML += "\t\t\t\t\t<td id=\"line1_" + data + "_" + size1 + "\" colspan=\"3\">" + line1 +  "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"logo_" + data + "\" rowspan=\"5\"><!--img src=\"uploadfiles/" + logo + "\" width=\"96px\" height=\"64px\" onerror=\"this.src=\'img/blank.png\'\"--></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"line2_" + data + "_" + size2 + "\" colspan=\"3\">" + line2 + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"line3_" + data + "_" + size3 + "\" colspan=\"3\">" + line3 + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td class=\"midblank_" + data + "\" colspan=\"3\"></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"floor" + data + "\">" + floor + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"time" + data + "\">" + stime + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"room" + data + "\">" + room + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td class=\"bottomblank_" + data + "\" colspan=\"5\"></td></tr>\n";
			}
			else {
				trHTML += "\t\t\t\t<tr><td class=\"topblank_" + data + "\" colspan=\"3\"></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"logo_" + data + "\" rowspan=\"3\"><!--img src=\"uploadfiles/" + logo + "\" width=\"96px\" height=\"64px\" onerror=\"this.src=\'img/blank.png\'\"--></td>\n";
				trHTML += "\t\t\t\t\t<td id=\"line1_" + data + "_" + size1 + "\" class=\"line" + data + "\">" + line1 +  "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"floor" + data + "\">" + floor + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"line2_" + data + "_" + size2 + "\"class=\"line" + data + "\">" + line2 + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"time" + data + "\">" + stime + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"line3_" + data + "_" + size3 + "\"class=\"line" + data + "\">" + line3 + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"room" + data + "\">" + room + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td class=\"bottomblank_" + data + "\" colspan=\"3\"></td></tr>\n";
			}
			trHTML += "\t\t\t\t</table>\n";
			if(data > cnt) {
				trHTML += "\t\t\t\t<div id=\"blank" + data + "\"></div>\n";
			}
			++cnt;
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
	finally{
		if(rs!=null) { rs.close(); }
		if(stmt!=null) { stmt.close(); }
		if(conn!=null) { conn.close(); }
	}
%>

<%=trHTML %>

</body>
</html>
