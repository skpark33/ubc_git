<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.*, javax.xml.parsers.*" %>
<%@ page import="org.w3c.dom.*, org.xml.sax.SAXException" %>
<%@ page import="java.sql.*, java.util.Calendar" %>
<%@ page import="javax.naming.*, javax.sql.*" %>
<%@ page session="false" %>
<%
	/* Cache remove */
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Pragma","no-cache"); 

	int data = Integer.parseInt(request.getParameter("data"));		// 층 행사 수 받아옴
	String _hostId = request.getParameter("HostId"); 
	
	Calendar now = Calendar.getInstance();
	String hour = now.get(Calendar.HOUR_OF_DAY) + "";
	String minute = now.get(Calendar.MINUTE) + "";
	String clock = hour + ":" +  minute;

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String sql = "";
	String stime = "";
	String room = "";
	String hostId = "";
	String floor = "";
	String line1 = "";
	String line2 = "";
	String line3 = "";
	int size1 = 0;
	int size2 = 0;
	int size3 = 0;
	String color = "";
	String logo = "";
	String trHTML = "";
	int cnt = 1;
	String tag = "";
	
	try { 
		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		//Document doc = docBuilder.parse(new File("C:/Users/DELL/workspace/lottehotel/WebContent/arrow/arrow.xml"));
		//Document doc = docBuilder.parse(new File("C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/lottehotel/arrow/arrow.xml"));
		Document doc = docBuilder.parse(new File("C:/Apache Software Foundation/Tomcat 7.0/webapps/lottehotel/arrow/arrow.xml"));
		Element root = doc.getDocumentElement();
		NodeList tag_name = doc.getElementsByTagName("name");
		NodeList tag_img = doc.getElementsByTagName("img");
		
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		
		sql = "select * from ubc_web_hotel_event"
			+ " where hostList like '%" + _hostId + "%' AND (" 
		 	+ " (date<=DATE_FORMAT(now(), '%Y-%m-%d') AND fdate>=DATE_FORMAT(now(), '%Y-%m-%d') AND displayTime <= date_format(now(), '%H:%i') AND ftime > DATE_FORMAT(now(), '%H:%i'))"
		 	+ " OR (date<=date_format(now(), '%Y-%m-%d') and fdate>=date_format(now(), '%y-%m-%d')  and displayTime <= date_format(now(), '%H:%i') AND stime > ftime )"
		 	+ " OR (date<=date_format(date_sub(now(), interval 1 day), '%Y-%m-%d') and fdate>=date_format(date_sub(now(), interval 1 day), '%Y-%m-%d') and displayTime <= date_format(now(), '%H:%i') AND stime > ftime and ftime > date_format(now(), '%H:%i'))"
		 	+ " )" 
			+ " ORDER BY stime limit 6";
		
		stmt = conn.createStatement();
		rs = stmt.executeQuery(sql);
		while(rs.next()) {
			stime = rs.getString("stime").substring(0,5);	// ss REMOVE
			room = rs.getString("room");
			hostId = rs.getString("hostId");
			tag = hostId;
			floor = rs.getString("floor");
			line1 = rs.getString("line1");
			line2 = rs.getString("line2");
			line3 = rs.getString("line3");
			size1 = rs.getInt("size1");
			size2 = rs.getInt("size2");
			size3 = rs.getInt("size3");
			color = rs.getString("color");
			logo = rs.getString("logo");
			trHTML = trHTML + "\t\t\t\t<table id=\"event_" + data + "\" class=\"" + color + "\" width=\"93%\" align=\"center\" border=\"0\">\n";
			if(data < 4) {
				trHTML += "\t\t\t\t<tr><td class=\"topblank_" + data + "\" colspan=\"5\"></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"logo_" + data + "\" rowspan=\"5\"></td>\n";
				trHTML += "\t\t\t\t\t<td id=\"line1_" + data + "_" + size1 + "\" colspan=\"3\">" + line1 +  "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"logo_" + data + "\" rowspan=\"5\"></td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"line2_" + data + "_" + size2 + "\" colspan=\"3\">" + line2 + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td id=\"line3_" + data + "_" + size3 + "\" colspan=\"3\">" + line3 + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td class=\"midblank_" + data + "\" colspan=\"3\"></td></tr>\n";
				for(int i=0; i<tag_name.getLength(); i++) {
					if( tag.equals(tag_name.item(i).getFirstChild().getNodeValue())){
						trHTML += "\t\t\t\t<tr><td id=\"floor" + data + "\"><img src=\"arrow/" + tag_img.item(i).getFirstChild().getNodeValue() + "\"></td>\n";
					}
				}
				trHTML += "\t\t\t\t\t<td id=\"time" + data + "\">" + stime + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"room" + data + "\">" + room + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td class=\"bottomblank_" + data + "\" colspan=\"5\"></td></tr>\n";
			}
			else {
				trHTML += "\t\t\t\t<tr><td class=\"topblank_" + data + "\" colspan=\"3\"></td></tr>\n";
				trHTML += "\t\t\t\t<tr>" + "<td id=\"logo_" + data + "\" rowspan=\"3\"></td>\n";
				trHTML += "\t\t\t\t\t<td id=\"line1_" + data + "_" + size1 + "\" class=\"line" + data + "\">" + line1 +  "</td>\n";
				for(int i=0; i<tag_name.getLength(); i++) {
					if( tag.equals(tag_name.item(i).getFirstChild().getNodeValue())){
						trHTML += "\t\t\t\t\t<td id=\"floor" + data + "\"><img src=\"arrow/" + tag_img.item(i).getFirstChild().getNodeValue() + "\"></td></tr>\n";
					}
				}
				trHTML += "\t\t\t\t<tr>" + "<td id=\"line2_" + data + "_" + size2 + "\"class=\"line" + data + "\">" + line2 + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"time" + data + "\">" + stime + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr>" + "<td id=\"line3_" + data + "_" + size3 + "\"class=\"line" + data + "\">" + line3 + "</td>\n";
				trHTML += "\t\t\t\t\t<td id=\"room" + data + "\">" + room + "</td></tr>\n";
				trHTML += "\t\t\t\t<tr><td class=\"bottomblank_" + data + "\" colspan=\"3\"></td></tr>\n";
			}
			trHTML += "\t\t\t\t</table>\n";
			if(data > cnt ) {
				trHTML += "\t\t\t\t<div id=\"blank" + data + "\"></div>";
			}
			++cnt;
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
	finally{
		if(rs!=null) { rs.close(); }
		if(stmt!=null) { stmt.close(); }
		if(conn!=null) { conn.close(); }
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width">
<link rel='stylesheet' type='text/css' href='css/floorlayout_sd.css'>
<link rel="shortcut icon" href="img/favicon2.ico" type="image/x-icon">
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<title>LOTTE HOTEL</title>
	<script>
		window.onload=function() {
			clock();
		}
		function clock() {
			window.setTimeout("clock()", 3600000);
                        
			var now = new Date();
			var yy = now.getFullYear();
			var mm = now.getMonth() + 1 ;
			var dd = now.getDate();
			var day = now.getDay();
                        
			var arr = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
			ymd.innerText = "" + yy + "." + mm + "." + dd + " " + arr[day];
		}
	</script>
</head>
<body>
		<table border="0" width="100%" height="916px" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                        <td id="top" colspan="2" height="5px" > </td>
                  </tr>
                  <tr>
                        <td id="banner" align="center" valign="middle">
                              <table width="592px" border="0" cellpadding="0" cellspacing="0"  style="color: #f4f0eb; vertical-align: top;">
                                    <tr>
                                          <td rowspan="2" id="te" align="left" ></td>
                                          <td height="30px"></td>
                                    </tr>
                                    <tr>
                                          <td id="ymd" align="left"></td>
                                    </tr>
                              </table>
                        </td>
                  <tr>
                        <td colspan="2" height="15em" > </td> 
                  </tr>
                  <tr>
                  	<td id="evtd"> 
<%=trHTML %>
                  	 </td>
                  </tr>
            </table>
</body>
</html>
