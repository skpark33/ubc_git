<%@page import="com.sun.corba.se.spi.orbutil.fsm.State"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*, java.util.Calendar" %>
<%@ page import="javax.naming.*, javax.sql.*" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Insert title here</title>
</head>
<body>
<%
	/* Cache remove */
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Pragma","no-cache"); 

	int eid = Integer.parseInt(request.getParameter("eid"));
	String start = request.getParameter("start");

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String sql = "";
	int cnt = 0;
	
	try {
		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("org.apache.commons.dbcp.PoolingDriver");
		conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:/pool");
		
		
		if( conn == null) {
			out.println("DB Connection Fail");
		}
		else {
			sql = "select count(*) as cnt from ubc_web_hotel_event"
				+ " where (date<=DATE_FORMAT('" + start + "', '%Y-%m-%d') AND fdate>=DATE_FORMAT('" + start + "', '%Y-%m-%d'))";
			
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				cnt = rs.getInt("cnt");
			}
			if( cnt > 6 ) {
				cnt = 6;
			}
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
	finally {
		if(rs!=null) { rs.close(); }
		if(stmt!=null) {stmt.close();}
		if(conn!=null) {conn.close();}
	}
	RequestDispatcher dispatcher = request.getRequestDispatcher("eventprev.jsp?data="+ cnt + "&eid=" + eid);
	dispatcher.forward(request, response);
%>

</body>
</html>
