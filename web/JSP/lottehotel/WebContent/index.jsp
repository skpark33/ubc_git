<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" errorPage="error.jsp"%>
<%
    /* Cache remove */
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	response.setHeader("Pragma","no-cache"); 
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width-device-width, initial-scale=1.0">
<link rel='stylesheet' type='text/css' href='css/login.css'>
<link rel="shortcut icon" href="img/favicon2.ico" type="image/x-icon">
<script src="js/modernizr.custom.63321.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<title>LOGIN</title>
</head>
<body>
	<div class="wrap">
		<header>
			<h1>Lotte Hotel Event Management</h1>
		</header>
		<section class="main">
		<form class="login-form" action="loginDB.jsp" method="post">
			<h1>log in</h1>
			<p>
				<label for="login">id</label>
				<input type="text" name="userid" placeholder="User Id"/>
			</p>
			<p>
				<label for="password">Password</label>
				<input type="password" name="userpwd" placeholder="User Password"/>
			</p>
			<p>
				<input type="submit" name="submit" value="Login"/>
			</p>
		</form>
		</section>
	</div>
</body>
</html>