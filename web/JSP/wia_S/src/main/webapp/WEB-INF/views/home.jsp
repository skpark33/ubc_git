<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%
	request.setCharacterEncoding("utf-8");
	if(session.getAttribute("userinfo") == null) {
		response.sendRedirect("index.do");
	}
	
	/* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
	
	//int cnt = 1;
	String sday = request.getParameter("sdate_check");
	String fday = request.getParameter("fdate_check");
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=0.5">
	<jsp:include page="/WEB-INF/views/include/declare.jsp"></jsp:include>
	<script type="text/javascript">
		google.load("visualization", "1", {packages:['corechart']});
		
		$(document).ready(function () {
			jQuery.ajaxSettings.traditional = true;
			$.ajax({
				type: "post",
				url: "autoSubmit.do",
				success: function(data) {
					drawPieChart(eval(data));
					google.setOnLoadCallback(drawPieChart);
				}
			});
		});
		function drawPieChart(formdata) {
			var data = google.visualization.arrayToDataTable(formdata);
			var option = {
					title: '최근 일주일간 컨텐츠 Top 10',
					is3D: true,
					legend: 'labeled',
					pieSliceText: 'value',
					slices: { 0: {offset:0.15},
						1: {offset:0.1},
						2: {offset:0.05},
					},
			};
			new google.visualization.PieChart(document.getElementById('recentview')).draw(data, option);
		}
	</script>
</head>
<body>
<div id="wrap">
	<div id="topline"></div>
	<jsp:include page="/WEB-INF/views/include/top.jsp"></jsp:include>
	<div id="main">
		<div id="tabs">
			<jsp:include page="/WEB-INF/views/include/logo.jsp"></jsp:include>
			<div id="title">
				<img src="./resources/img/grid.png" width="20" height="20"/>
				<p>홈</p>
			</div>
			<div id="mLayer">
				<div id="recentview"></div>
				<!-- <div id="rcontents">
					<ul>
						<li></li>
					</ul>
				</div> -->
			</div>
		</div> 
	</div>
	<jsp:include page="/WEB-INF/views/include/footer.jsp"></jsp:include>
</div>
</body>
</html>
