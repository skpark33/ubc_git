<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- <%@ page session="false" %>
<%@ page import="java.util.*" %>
<%
	int cnt = 1;
	String sday = request.getParameter("sdate");
	String fday = request.getParameter("fdate");
%> --%>
<%-- <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<link rel="shortcut icon" href="./resources/img/favicon_W.ico" type="image/x-icon">
<link rel="stylesheet" href="./resources/css/welcome.css">
<link rel="stylesheet" href="./resources/css/jquery-ui-1.10.3.datepicker.css">
<script src="./resources/js/button.js"></script>
<script src="./resources/js/jquery-1.9.1.js"></script>
<script src="./resources/js/jquery-ui-1.10.3.datepicker.js"></script>
<script type="text/javascript">
	$(function() { 
		    $( "#sdate" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    showOn: 'button',
			    buttonImage: 'resources/img/calendar_20.png',
			    buttonImageOnly: true,
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
			    onClose: function( selectedDate ) {
			    		$('#fdate').datepicker("option", "minDate", selectedDate);
			    }
		    });
	});
	$(function() { 
		    $( "#fdate" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    showOn: 'button',
			    buttonImage: 'resources/img/calendar_20.png',
			    buttonImageOnly: true,
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
			    onClose: function( selectedDate ) {
			    		$('#sdate').datepicker("option", "maxDate", selectedDate);
			    }
		    });
	});
	$(function() { 
		$( "#date" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
		    });
	});
	$(document).ready(function () {
		if( <%=sday %> == null && <%=fday %> == null ) {
			$('#sdate, #fdate').val($.datepicker.formatDate($.datepicker.ATOM, new Date()));
		}
		else {
			$('#sdate').val('<%=sday%>');
			$('#fdate').val('<%=fday%>');
		}
	});
</script> --%>
<title>현대위아 - Welcome Board</title>
</head>
<body>
<%-- 	<div id="wrap">
		<div id="title">
			<img alt="logo" src="./resources/img/en_wc_color.png" width="280px">
		</div>
		<form name="msgform" method="POST">
		<div id="date-search">
			<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly/> -
			<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly/>
			<button type="button" class="btn btn-1 btn-1a" onclick="dateCheck()">조회</button>
			<!-- <a href="#"><img src="./resources/img/search_3.png" onclick="dateCheck()"/></a> -->
		</div>
		<div id="msg-table">
			<table id="mtable">
				<thead>
					<tr>
						<th>날짜</th>
						<th>시작시간</th>
						<th>종료시간</th>
						<th>웰컴보드 메세지</th>
						<th colspan="2">버튼</th>
					</tr>
				</thead>
				<tfoot>
					<tr id=foot>
						<td><input type="text" name="date" id="date" size="13" readonly/></td>
						<td><input type="text" name="stime" id="stime" class="input-time" size="8" maxlength="4" onkeypress="numCheck()"/></td>
						<td><input type="text" name="ftime" id="ftime" class="input-time" size="8" maxlength="4" onkeypress="numCheck()"/></td>
						<td><input type="text" name="msg" id="msg" size="50"/></td>
						<td colspan="2"><button type="button" id="btn_submit" class="btn btn-1 btn-1a" onclick="submit_js(1)">등록</button></td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="msglist" items="${ list }">
					<tr>
						<td>${ msglist.date }</td>
						<td>${ msglist.stime }</td>
						<td>${ msglist.ftime }</td>
						<td>${ msglist.msg }</td>
						<td><button type="button" class="btn btn-1 btn-1a" onclick="modi_js(<%=cnt%>, ${ msglist.mid })">수정</button></td>
						<td><button type="button" class="btn btn-2 btn-2a" onclick="del_js(${ msglist.mid })">삭제</button></td>
					</tr>
					<% cnt++; %>
					</c:forEach>
				</tbody>
			</table>
		</div>
		</form>
	</div>
</body>
</html>
--%>

<%-- <!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=0.5">
	<jsp:include page="/WEB-INF/views/include/declare.jsp"></jsp:include>
	<script type="text/javascript">
		$(function() { 
			    $( "#sdate_check" ).datepicker({
				    inline: true,
				    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
				    prevText: 'prev',
				    nextText: 'next',
				    showButtonPanel: true,	/* 버튼 패널 사용 */
				    changeMonth: true,		/* 월 선택박스 사용 */
				    changeYear: true,		/* 년 선택박스 사용 */
				    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
				    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
				    minDate: '-30y',
				    closeText: '닫기',
				    currentText: '오늘',
				    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
				    showOn: 'button',
				    buttonImage: 'resources/img/calendar_20.png',
				    buttonImageOnly: true,
				    /* 한글화 */
				    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
				    showAnim: 'fadeIn',
				    onClose: function( selectedDate ) {
				    		$('#fdate_check').datepicker("option", "minDate", selectedDate);
				    }
			    });
		});
		$(function() { 
			    $( "#fdate_check" ).datepicker({
				    inline: true,
				    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
				    prevText: 'prev',
				    nextText: 'next',
				    showButtonPanel: true,	/* 버튼 패널 사용 */
				    changeMonth: true,		/* 월 선택박스 사용 */
				    changeYear: true,		/* 년 선택박스 사용 */
				    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
				    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
				    minDate: '-30y',
				    closeText: '닫기',
				    currentText: '오늘',
				    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
				    showOn: 'button',
				    buttonImage: 'resources/img/calendar_20.png',
				    buttonImageOnly: true,
				    /* 한글화 */
				    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
				    showAnim: 'fadeIn',
				    onClose: function( selectedDate ) {
				    		$('#sdate_check').datepicker("option", "maxDate", selectedDate);
				    }
			    });
		});
		$(function() { 
			$( "#sdate" ).datepicker({
				    inline: true,
				    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
				    prevText: 'prev',
				    nextText: 'next',
				    showButtonPanel: true,	/* 버튼 패널 사용 */
				    changeMonth: true,		/* 월 선택박스 사용 */
				    changeYear: true,		/* 년 선택박스 사용 */
				    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
				    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
				    minDate: '-30y',
				    closeText: '닫기',
				    currentText: '오늘',
				    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
				    /* 한글화 */
				    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
				    showAnim: 'fadeIn',
				    onClose: function( selectedDate ) {
				    		$('#fdate').datepicker("option", "minDate", selectedDate);
				    }
			    });
		});
		$(function() { 
			$( "#fdate" ).datepicker({
				    inline: true,
				    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
				    prevText: 'prev',
				    nextText: 'next',
				    showButtonPanel: true,	/* 버튼 패널 사용 */
				    changeMonth: true,		/* 월 선택박스 사용 */
				    changeYear: true,		/* 년 선택박스 사용 */
				    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
				    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
				    minDate: '-30y',
				    closeText: '닫기',
				    currentText: '오늘',
				    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
				    /* 한글화 */
				    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
				    showAnim: 'fadeIn',
				    onClose: function( selectedDate ) {
				    		$('#sdate').datepicker("option", "maxDate", selectedDate);
				    }
			    });
		});
		$(function() {
			$('.input-time').poshytip({
				className: 'tip-twitter',
				showOn: 'focus',
				alignTo: 'target',
				alignX: 'center',
				alignY: 'bottom',
				offsetX: 0,
				offsetY: 10,				
				showTimeout: 1,
				//bgImageFrameSize: 15,
				allowTipHover: false,
				fade: true,
				slide: false
			});
		});
		
		function modi_js(val, mid) {
			var id = mid;
			var num = val;
			$(".btn-del").hide();
			$("#add").attr("disabled", true);
			var sdate_check = document.getElementById("sdate_check").value;
			var fdate_check = document.getElementById("fdate_check").value;
			// val 로 해당 라인 색깔 지정해주기
			$.ajax({
				type: "POST",
				url: "wcbModify.do",
				data: { "mid": id, "sdate_check": sdate_check, "fdate_check": fdate_check, "rowNum": num },
				success: function(html) {
					$("#utable").show();
					$("#utable").html(html);
				}
			});
		}
		
		$(document).ready(function () {
			$('#itable, #utable').hide();
			var today = $.datepicker.formatDate($.datepicker.ATOM, new Date());
			var week = new Date(today);
			var tmp = week.getDate();
			
			if( <%=sday %> == null && <%=fday %> == null ) {
				$('#fdate_check').val($.datepicker.formatDate($.datepicker.ATOM, new Date()));
				$('#sdate_check').val($.datepicker.formatDate($.datepicker.ATOM, new Date(week.setDate(tmp-6))));
			}
			else {
				$('#sdate_check').val('<%=sday%>');
				$('#fdate_check').val('<%=fday%>');
			}
			
			function effect_i() {
				$('#itable').toggle( 'blind', {}, 500 );
				$('#add, #btn-modi').attr("disabled", true);
			}
			$('#add').click(function() {	
				effect_i();
			});
			
			$('#now-chk').click(function() {
				var nowtime = new Date();
				$('#sdate').val($.datepicker.formatDate($.datepicker.ATOM, new Date()));
				if( nowtime.getMinutes().toString().length > 1) {
					$('#stime').val(nowtime.getHours()+ "" +nowtime.getMinutes()-1);
				}
				else {
					$('#stime').val(nowtime.getHours()+ "0" +nowtime.getMinutes()-1);
				}
			});
		});
	</script>
</head>
<body>
<div id="wrap">
	<div id="topline"></div>
	<jsp:include page="/WEB-INF/views/include/top.jsp"></jsp:include>
	<div id="main">
		<div id="tabs">
			<jsp:include page="/WEB-INF/views/include/logo.jsp"></jsp:include>
			<div id="title">
				<img src="./resources/img/grid.png" width="20" height="20"/>
				<p>홈</p>
			</div>
			<form name="msgform" method="POST" enctype="multipart/form-data">
			<div id="date-search">
				<input type="text" name="sdate_check" id="sdate_check" class="form-date" size="13" readonly/> -
				<input type="text" name="fdate_check" id="fdate_check" class="form-date" size="13" readonly/>
				<button type="button" class="btn btn-1 btn-1a" onclick="dateCheck()">조회</button>
			</div>
			<div id="msg-table">
				<div id="input-menu">
					<button type="button" class="btn btn-1 btn-1a" id="add">추가</button>
				</div>
				<table id="mtable">
					<thead>
						<tr>
							<th>시작날짜</th>
							<th>시작시간</th>
							<th>종료날짜</th>
							<th>종료시간</th>
							<th>파일명</th>
							<th colspan="2">버튼</th>
						</tr>
					</thead>
					<tfoot>
					</tfoot>
					<tbody>
					<!-- 라인색상 처리해야함  -->
						<c:forEach var="msglist" items="${ list }">
							<c:if test="${msglist.mid == nowMsg }">
								<tr class="nowmsg">
							</c:if>
							<c:if test="${msglist.mid != nowMsg }">
								<tr>									
							</c:if>
							<td>${ msglist.sdate }</td>
							<td>${ msglist.stime }</td>
							<td>${ msglist.fdate }</td>
							<td>${ msglist.ftime }</td>
							<td>${ msglist.filename }</td>
							<td><button type="button" id="btn-modi" class="btn btn-1 btn-1a" onclick="modi_js(<%=cnt%>, '${ msglist.mid }')">수정</button></td>
							<td><button type="button" id="btn-del" class="btn btn-2 btn-2a btn-del" onclick="del_js('${ msglist.mid }')">삭제</button></td>
						</tr>
						<% cnt++; %>
						</c:forEach>
					</tbody>
				</table>
				
				<table id="itable">
					<thead>
						<tr>
							<th colspan='8'>웰컴메세지 추가</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan='2'>시작날짜</td>
							<td>시작시간</td>
							<td>종료날짜</td>
							<td>종료시간</td>
							<td>파일업로드</td>
							<td colspan='2'>버튼</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td><input type="checkbox" name="now" id="now-chk" onclick="nowCheck()" /></td>
							<td><input type="text" name="sdate" id="sdate" size="13" readonly/></td>
							<td><input type="text" name="stime" id="stime" class="input-time" size="8" maxlength="4" title="입력양식 <br />오전 9시 = '0900' <br />오후 9시 = '2100'" onkeypress="numCheck()" style="ime-mode:Disabled;"/></td>
							<td><input type="text" name="fdate" id="fdate" size="13" readonly/></td>
							<td><input type="text" name="ftime" id="ftime" class="input-time" size="8" maxlength="4" title="입력양식 <br />오전 9시 = '0900' <br />오후 9시 = '2100'" onkeypress="numCheck()" style="ime-mode:Disabled;"/></td>
							<td><input type="file" name="file" id="file" accept=".pptx,.ppt"></td>
							<td><button type="button" id="btn_submit" class="btn btn-1 btn-1a" onclick="submit_js()">등록</button></td>
							<td><button type="button" id="btn_submit" class="btn btn-1 btn-1a" onclick="history.go(0)">취소</button></td>
						</tr>
					</tfoot>
				</table>
				
			</div>
			</form>
			<form name="updateForm" method="POST" enctype="multipart/form-data">
				<table id="utable">
				</table>
			</form>
		</div> 
	</div>
	<jsp:include page="/WEB-INF/views/include/footer.jsp"></jsp:include>
</div>
</body>
</html> --%>