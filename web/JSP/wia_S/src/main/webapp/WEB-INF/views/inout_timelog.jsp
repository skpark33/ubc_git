<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%
	request.setCharacterEncoding("utf-8");
	if(session.getAttribute("userinfo") == null) {
		response.sendRedirect("index.do");
	}
	
	/* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<jsp:include page="/WEB-INF/views/include/declare.jsp"></jsp:include>
	<script type="text/javascript">
		google.load('visualization', '1', {packages:['table']});
		function formSubmit() {
			var formData = $('#form-if').serialize();
			
			jQuery.ajaxSettings.traditional = true;
			$.ajax({
				type: 'POST',
				url: 'inoutSubmit.do',
				data: formData,
				success: function(data) {
					//alert(data);
					drawchart(eval(data));
					google.setOnLoadCallback(drawchart);
				}
			});
		}
		function drawchart(formdata) {
			var data = google.visualization.arrayToDataTable(formdata);
			var cssClassNames = { tableCell: 'tableCellClass'};
			var option = {
					width: '1050px',
					cssClassNames: cssClassNames,
			};
			new google.visualization.Table(document.getElementById('tableview')).draw(data, option);
		}
		
	</script>
</head>
<body>
<div id="wrap">
	<div id="topline"></div>
	<jsp:include page="/WEB-INF/views/include/top.jsp"></jsp:include>
	<%-- <jsp:include page="/WEB-INF/views/sidemenu.jsp"></jsp:include> --%>
	<div id="main">
		<div id="tabs">
			<jsp:include page="/WEB-INF/views/include/logo.jsp"></jsp:include>
			<div id="title">
					<img src="./resources/img/grid.png" width="20" height="20"/>
					<p>고객 입장, 퇴장시간 로그</p>
					<button type="button" class="btn btn-1 btn-1a" onclick="formSubmit()">전송</button>
			</div>
			<form id="form-if" action="test.do" method="post">
				<div id="search">
					<table id="form-search">
						<tr>
							<td class="subj">기 간</td>
							<td class="detail">
								<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly> -
								<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly>
							</td>
							<td class="subj">고객명</td>
							<td class="detail">
								<label for="customer">고객명 : <input type="text" name="customName" id="customName" size="12" value=""></label>
							</td>
						</tr>
					</table>
				</div>
			</form>
			<div id="tableview"></div>			
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/include/footer.jsp"></jsp:include>	
</div>

</body>
</html>

<!-- <fieldset id="form-fieldset">
						<legend>검색 조건</legend>
						<div id="fieldset-if">
							<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly> -
							<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly>
							<label for="customer">고객명 : <input type="text" name="customName" id="customName" size="12" value=""></label>
						</div>
					</fieldset> -->