	<%@ page language="java" contentType="text/html; charset=UTF-8"
	    pageEncoding="UTF-8"%>
	<title>현대위아 - HYUNDAI WIA 통계/로그</title>
	<link rel="shortcut icon" href="./resources/img/favicon_H.ico" type="image/x-icon">
	<link rel="stylesheet" href="./resources/css/main.css">
	<link rel="stylesheet" href="./resources/css/childmain.css">
	<link rel="stylesheet" href="./resources/css/googlechart.css">
	<link rel="stylesheet" href="./resources/css/welcome.css">
	<link rel="stylesheet" href="./resources/css/tip-twitter.css">
	<link rel="stylesheet" href="./resources/css/jquery-ui-1.10.3.datepicker.css">
	<link rel="stylesheet" href="./resources/css/jquery.multiselect.css">
	<script src="./resources/js/jquery-1.9.1.js"></script>
	<script src="./resources/js/jquery-ui-1.10.3.datepicker.js"></script>
	<script src="./resources/js/jquery.multiselect.js"></script>
	<script src="./resources/js/jquery.poshytip.min.js"></script>
	<script src="./resources/js/modernizr.custom.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script src="./resources/js/button.js"></script>
	
	 <script type="text/javascript">
		$(function() { 
			    $( "#sdate-search" ).datepicker({
				    inline: true,
				    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
				    prevText: 'prev',
				    nextText: 'next',
				    showButtonPanel: true,	/* 버튼 패널 사용 */
				    changeMonth: true,		/* 월 선택박스 사용 */
				    changeYear: true,		/* 년 선택박스 사용 */
				    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
				    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
				    minDate: '-30y',
				    closeText: '닫기',
				    currentText: '오늘',
				    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
				    showOn: 'button',
				    buttonImage: 'resources/img/calendar_20.png',
				    buttonImageOnly: true,
				    /* 한글화 */
				    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
				    showAnim: 'fadeIn',
				    onClose: function( selectedDate ) {
				    		$('#fdate-search').datepicker("option", "minDate", selectedDate);
				    }
			    });
		});
		$(function() { 
			    $( "#fdate-search" ).datepicker({
				    inline: true,
				    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
				    prevText: 'prev',
				    nextText: 'next',
				    showButtonPanel: true,	/* 버튼 패널 사용 */
				    changeMonth: true,		/* 월 선택박스 사용 */
				    changeYear: true,		/* 년 선택박스 사용 */
				    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
				    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
				    minDate: '-30y',
				    closeText: '닫기',
				    currentText: '오늘',
				    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
				    showOn: 'button',
				    buttonImage: 'resources/img/calendar_20.png',
				    buttonImageOnly: true,
				    /* 한글화 */
				    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
				    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
				    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
				    showAnim: 'fadeIn',
				    onClose: function( selectedDate ) {
				    		$('#sdate-search').datepicker("option", "maxDate", selectedDate);
				    }
			    });
		});
		$(document).ready(function() {
			var today = $.datepicker.formatDate($.datepicker.ATOM, new Date());
			var week = new Date(today);
			var tmp = week.getDate();
			
			$('#fdate-search').val(today);
			$('#sdate-search').val($.datepicker.formatDate($.datepicker.ATOM, new Date(week.setDate(tmp-7))));
		});
	</script>
