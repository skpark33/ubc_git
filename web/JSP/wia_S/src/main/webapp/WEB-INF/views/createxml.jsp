<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "org.jdom2.*, org.jdom2.output.*, java.io.*, java.util.List;" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String sdate = request.getParameter("sdate");
String fdate = request.getParameter("fdate");

if( (sdate == null || fdate == null) ) {
	response.sendRedirect("welcome.do");
}
else if( sdate != null && fdate != null) {
	if( fdate.equals(sdate) ) {
		response.sendRedirect("welcome.do");
	}
	else {
		response.sendRedirect("wcbSearch.do?sdate=" + sdate + "&fdate=" + fdate);
	}
}
%>