<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="top">
		<div id="cssmenu">
			<ul>
			   <li class='active'><a href='main.do'><span>HOME</span></a></li>
			   <li class='has-sub'><a href='#'><span>Statistics</span></a>
			      <ul>
			         <!-- <li><a href='custom.do'><span>고객별 체험 통계</span></a></li> -->
			         <li><a href='period.do'><span>기간별 체험 통계</span></a></li>
			         <li><a href='topten.do'><span>Top 10 통계</span></a></li>
			         <!-- <li class='last'><a href='progress.do'><span>컨텐츠 체험 추이</span></a></li> -->
			      </ul>
			   </li>
			   <!-- <li class='has-sub last'><a href='#'><span>Log</span></a>
			      <ul>
			         <li><a href='inout.do'><span>고객 입, 퇴장시간 로그</span></a></li>
			         <li class='last'><a href='explog.do'><span>컨텐츠 체험 로그</span></a></li>
			      </ul>
			   </li> -->
			   <li class='logout'><a href='logout.do'><span>Logout</span></a></li>
			</ul>
		</div>
	</div>
	