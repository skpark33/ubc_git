<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- <%@ page import="java.util.*, java.io.*" %> --%>

<c:out value="[  "/>
<c:out value="${ col }," escapeXml="false"/>
<c:forEach var="line" items="${ data }">
	<c:out value="[ '${ line.Month }월'," escapeXml="false"/>
	<c:forEach var="i" begin="1" end="${ colcnt }" step="1">
		<c:set var="tmp">col${ i }</c:set>
		<c:out value="${ line[tmp] },"/>
	</c:forEach>
	<c:out value=" ], "/>
</c:forEach>
<c:out value=" ]"/>





<%-- 
	<c:out value="${ line.simple },"/>
	<c:out value="${ line.quote },"/>
	<c:out value="${ line.mycar },"/>
	<c:out value="${ line.introcar },"/>
	<c:out value="${ line.brand },"/>
	<c:out value="${ line.exp },"/>
	<c:out value="${ line.fun },"/>
	<c:out value="${ line.introduce },"/>
	<c:out value="${ line.tech },"/>
	<c:out value="${ line.price } ],"/> 
--%>




<%-- <%
	StringBuilder sb = new StringBuilder();
	String header = "";
	String []arg = new String[11];

	sb.append("[ [ ");
%>
	<c:forEach var="col" items="${ col }">
	
	<%
		header = pageContext.getAttribute("col",PageContext.PAGE_SCOPE).toString();
	
		sb.append("'" + header + "', ");
	%>
	
 </c:forEach> 
	
<%
	sb.append("], ");

	ArrayList list = (ArrayList)request.getAttribute("data");
%>
 
	<c:forEach var="line" items="${ data }">
		<c:set var="mon" value="${ line.Month }" />
		<c:set var="simple" value="${ line.simple }" />
		<c:set var="quote" value="${ line.quote }" />
		<c:set var="mycar" value="${ line.mycar }" />
		<c:set var="introcar" value="${ line.introcar }" />
		<c:set var="brand" value="${ line.brand }" />
		<c:set var="exp" value="${ line.exp }" />
		<c:set var="fun" value="${ line.fun }" />
		<c:set var="intro" value="${ line.introduce }"/>
		<c:set var="tech" value="${ line.tech }" />
		<c:set var="price" value="${ line.price }" />
 	
		<%
			arg[0] = pageContext.getAttribute("mon",PageContext.PAGE_SCOPE).toString();
			arg[1] = pageContext.getAttribute("simple",PageContext.PAGE_SCOPE).toString();
			arg[2] = pageContext.getAttribute("quote",PageContext.PAGE_SCOPE).toString();
			arg[3] = pageContext.getAttribute("mycar",PageContext.PAGE_SCOPE).toString();
			arg[4] = pageContext.getAttribute("introcar",PageContext.PAGE_SCOPE).toString();
			arg[5] = pageContext.getAttribute("brand",PageContext.PAGE_SCOPE).toString();
			arg[6] = pageContext.getAttribute("exp",PageContext.PAGE_SCOPE).toString();
			arg[7] = pageContext.getAttribute("fun",PageContext.PAGE_SCOPE).toString();
			arg[8] = pageContext.getAttribute("intro",PageContext.PAGE_SCOPE).toString();
			arg[9] = pageContext.getAttribute("tech",PageContext.PAGE_SCOPE).toString();
			arg[10] = pageContext.getAttribute("price",PageContext.PAGE_SCOPE).toString();
			
			sb.append("[ '" + arg[0] + "월', " + arg[1] +  ", " + arg[2] + ", " + arg[3] +  ", " + arg[4] +  ", " + arg[5] +  ", " + arg[6] +  ", " + arg[7] +  ", " + arg[8] + 
					 ", " + arg[9] +  ", " + arg[10] + " ],");
		%> 
	 </c:forEach> 
<%
	sb.append(" ]");
%>
<%=sb.toString()%> --%>