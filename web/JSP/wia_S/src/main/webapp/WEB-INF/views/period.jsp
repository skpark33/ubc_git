<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%
	request.setCharacterEncoding("utf-8");
	if(session.getAttribute("userinfo") == null) {
		response.sendRedirect("index.do");
	}
	
	/* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<jsp:include page="/WEB-INF/views/include/declare.jsp"></jsp:include>
	<script type="text/javascript">
		google.load("visualization", "1", {packages:['corechart', 'table']});
		$(document).ready(function() {
			$("#menu2, #menu3, #btnExcel, #hiddenview").hide();
		});
		function key_sel_menu2(val) {
			document.forms[0].menu2.selectedIndex=0;
			//document.forms[0].menu3.selectedIndex=0;
			var menu = val.value;
			$.ajax({
				type: "POST",
				url: "keySelect.do",
				data: "key=" + menu + "&key2=",
				success: function(html) {
					if(html == 0 ) {
						$("#menu2").hide();
						//$("#menu3").hide();
					}
					else {
						$("#menu2").show();
						$("#menu2").html(html);
						//$("#menu3").hide();
					}
				}
			});
		}
		function key_sel_menu3(val) {
			//document.forms[0].menu3.selectedIndex=0;
			var menu1 = document.getElementById("menu1").value;
			var menu2 = val.value;
			$.ajax({
				type: "POST",
				url: "keySelect.do",
				data: "key=" + menu1 + "&key2=" + menu2 + "&key3=",
				success: function(html) {
					if(html == 0) {
						//$("#menu3").hide();
					}
					else {
						//$("#menu3").show();
						//$("#menu3").html(html);
					}
				}
			});
		}
		function formSubmit() {
			var formData = $('#form-if').serialize();
			$('#labeling').text("조회기간 : " + $('#sdate-search').val() + " ~ " + $('#fdate-search').val());
			$('#tmp_sdate').attr('value', $('#sdate-search').val());
			$('#tmp_fdate').attr("value", $('#fdate-search').val());
			$('#tmp_menu1').attr('value', $('#menu1').val());
			$('#tmp_menu2').attr('value', $('#menu2').val());
			$('#tmp_period').attr('value', $('input:radio[name=period-kind]:checked').val());
			jQuery.ajaxSettings.traditional = true;
			$.ajax({
				type: 'POST',
				url: 'periodSubmit.do',
				data: formData,
				success: function(data) {
					drawChart(eval(data));
					google.setOnLoadCallback(drawChart);
					$("#hiddenview").show();
					$("#btnExcel").show();
				}
			});
		}
		
		function btnExcel() {
			var form = "<form action='/periodExcel.do' method='post'>";
			form += "<input type='hidden' name='sdate-search' value='" + $('#tmp_sdate').val() + "'/>";
			form += "<input type='hidden' name='fdate-search' value='" + $('#tmp_fdate').val() + "'/>";
			form += "<input type='hidden' name='menu1' value='" + $('#tmp_menu1').val() + "'/>";
			form += "<input type='hidden' name='menu2' value='" + $('#tmp_menu2').val() + "'/>";
			form += "<input type='hidden' name='period-kind' value='" + $('#tmp_period').val() + "'/>";
			form += "</form>"; 
			jQuery(form).appendTo("body").submit().remove();
		}
		
		function drawChart(formdata){
			var cdata = google.visualization.arrayToDataTable(formdata);
			
			var coption = {
					title: '기간별 체험 통계',
			};
			var toption = {
					width: '1050px',
					cssClassNames: { tableCell: 'tableCellClass'},
			};
			var col = new google.visualization.ColumnChart(document.getElementById('chartview'));
			col.draw(cdata, coption);
			
			var table = new google.visualization.Table(document.getElementById('tableview'));
			table.draw(cdata, toption);
			
			google.visualization.events.addListener(col, 'select', function() {
				table.setSelection(col.getSelection());
			});
			google.visualization.events.addListener(table, 'select', function() {
				col.setSelection(table.getSelection());
			});
		}
	</script>
</head>
<body>
<div id="wrap">
	<div id="topline"></div>
	<jsp:include page="/WEB-INF/views/include/top.jsp"></jsp:include>
	<%-- <jsp:include page="/WEB-INF/views/sidemenu.jsp"></jsp:include> --%>
	<div id="main">
		<div id="tabs">
			<jsp:include page="/WEB-INF/views/include/logo.jsp"></jsp:include>
			<div id="title">
				<img src="./resources/img/grid.png" width="20" height="20"/>
				<p>기간별 체험 통계</p>
			</div>
			<form id="form-if" action="test.do" method="post">
				<div id="search">
					<table id="form-search">
						<tr>
							<td class="subj">조회기간</td>
							<td class="detail">
								<input type="text" name="sdate-search" id="sdate-search" class="form-date" size="13" readonly> -
								<input type="text" name="fdate-search" id="fdate-search" class="form-date" size="13" readonly>
							</td>
							<td class="subj" id="org-ex">컨텐츠</td>
							<td>
								<select name="menu1" id="menu1" class="menu1" onchange="key_sel_menu2(this)">
									<option>전체</option>
								<c:forEach var="menu" items="${ keyword }">
									<option value="${ menu.name }">${ menu.name }</option>
								</c:forEach>
								</select>
								<select name="menu2" id="menu2" class="menu3" onchange="key_sel_menu3(this)">
									<option>전체</option>
								</select>
								<!-- <select name="menu3" id="menu3" class="menu3">
									<option>전체</option>
								</select> -->
							</td>
						</tr>
						<tr class="second-row">
							<td class="subj">기간선택</td>
							<td class="detail" colspan="3">
								<input type="radio" name="period-kind" id="year" value="year"><label for="year" class="switch switch-y">년도 별</label>
								<input type="radio" name="period-kind" id="month" value="month" checked><label for="month" class="switch switch-m">월 별</label>
								<input type="radio" name="period-kind" id="quarter" value="quarter"><label for="quarter" class="switch switch-q">분기 별</label>
								<input type="radio" name="period-kind" id="days" value="days"><label for="days" class="switch switch-d">요일 별</label>
							</td>
						</tr>
					</table>
				</div>
			</form>
			<div id="btnForm">
				<button type="button" id="btnExcel" class="btn btn-1 btn-1a" onclick="btnExcel()">엑셀파일</button>
				<button type="button" class="btn btn-1 btn-1a" onclick="formSubmit()">조회</button>
			</div>
			<div id="hiddenview">
				<span id="labeling"></span>
				<input type="hidden" id="tmp_sdate" value=""/>
				<input type="hidden" id="tmp_fdate" value=""/>
				<input type="hidden" id="tmp_menu1" value=""/>
				<input type="hidden" id="tmp_menu2" value=""/>
				<input type="hidden" id="tmp_period" value=""/>
			</div>
			<div id="chartview"></div>
			<div id="tableview"></div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/include/footer.jsp"></jsp:include>	
</div>

</body>
</html>
					<%-- <fieldset id="form-fieldset">
						<legend>검색 조건</legend>
						<div id="fieldset-if">
							<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly> -
							<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly>
							<select id="menu1" class="menu1" onchange="org_sel_menu2(this)">
								<option>전체</option>
							<c:forEach var="org" items="${ orglist }">
								<option value="${ org.name }">${ org.name }</option>
							</c:forEach>
							</select>
							<select id="menu2" class="menu3" onchange="org_sel_menu3(this)">
								<option>전체</option>
							</select>
							<select id="menu3" class="menu3">
								<option>전체</option>
							</select>
							<!-- <input type="button" id="submitbtn" name="submitbtn" value="전송" onclick="formSubmit()"/> -->
						</div>
					</fieldset>
				</div>
				<div id="search-p">
					<fieldset id="form-fieldset">
						<legend>기간별 검색</legend>
						<div id="fieldset-if" class="radio">
							<input type="radio" name="period-kind" id="year" value="year"><label for="year" class="switch switch-y">년도 별</label>
							<input type="radio" name="period-kind" id="month" value="month" checked><label for="month" class="switch switch-m">월 별</label>
							<input type="radio" name="period-kind" id="quarter" value="quarter"><label for="quarter" class="switch switch-q">분기 별</label>
							<input type="radio" name="period-kind" id="days" value="days"><label for="days" class="switch switch-d">요일 별</label>
						</div>
					</fieldset> --%>