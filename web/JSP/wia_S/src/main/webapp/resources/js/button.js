function submit_js() {
	if( checkForm() ) {
		document.msgform.action="wcbInsert.do";
		document.msgform.submit();
	}
}

function update_js(val) {
	if( ucheckForm(val) ) {
		document.updateForm.action="wcbUpdate.do";
		document.updateForm.submit();
	}
}

function del_js(mid) {
	var id = mid;
	var sdate_check = document.getElementById("sdate_check");
	var fdate_check = document.getElementById("fdate_check");
	if(confirm("메세지를 삭제하시겠습니까?") == true) {
		if(sdate_check.value != fdate_check.value) {
			location.href='wcbDelete.do?mid=' + id + '&sdate_check=' + sdate_check.value + '&fdate_check=' + fdate_check.value;
		}
		else {
			location.href='wcbDelete.do?mid=' + id;
		}
	}
	else {
		return;
	}
}
	
function dateCheck() {
	var sdate_check = document.getElementById("sdate_check");
	var fdate_check = document.getElementById("fdate_check");
	if( sdate_check.value == "" || fdate_check.value == "") {
		alert("시간을 선택하세요");
	}
	else {
		location.href='wcbSearch.do?sdate_check=' + sdate_check.value + '&fdate_check=' + fdate_check.value;
	}
}

function numCheck() {
	if((event.keyCode<48) || (event.keyCode>57)) {
		event.returnValue=false;
	}
}

function checkForm() {
	var mtable = document.getElementById("mtable");
	var rowNum = mtable.rows.length;
	
	if(document.msgform.sdate.value=="") {
		alert("시작날짜를 지정하세요!");
		document.msgform.sdata.focus();
		return false;
	}
	if(document.msgform.fdate.value=="") {
		alert("종료날짜를 지정하세요!");
		document.msgform.fdata.focus();
		return false;
	}
	if(document.msgform.sdate.value.length < 10) {
		alert("시작날짜입력란을 확인하세요.");
		document.msgform.sdata.focus();
		return false;
	}
	if(document.msgform.fdate.value.length < 10) {
		alert("종료날짜입력란을 확인하세요.");
		document.msgform.fdata.focus();
		return false;
	}
	if(document.msgform.stime.value=="") {
		alert("시작시간을 입력하세요!");
		document.msgform.stime.focus();
		return false;
	}
	if(document.msgform.stime.value.length < 4) {
		alert("시작시간을 확인하세요");
		document.msgform.stime.focus();
		return false;
	}
	
	if(document.msgform.ftime.value=="") {
		alert("종료시간을 입력하세요!");
		document.msgform.ftime.focus();
		return false;
	}
	if(document.msgform.ftime.value.length < 4) {
		alert("종료시간을 확인하세요!");
		document.msgform.ftime.focus();
		return false;
	}
	if(document.msgform.file.value=="") {
		alert("업로드 할 파일을 등록하세요!");
		document.msgform.file.focus();
		return false;
	}
	
	var ext = document.getElementById('file').value;
	ext = ext.slice(ext.indexOf(".") + 1).toLowerCase();
	if(!(ext==='pptx' || ext==='ppt')) {
		alert("MS PowerPoint 파일(pptx, ppt)만 업로드 가능합니다.");
		return false;
	}
	
	var sdate = document.msgform.sdate.value;
	var fdate = document.msgform.fdate.value;
	var stime = document.msgform.stime.value;
	var ftime = document.msgform.ftime.value;
	var stime_tmp = new Array(2);
	var ftime_tmp = new Array(2);
	
	var sdate_tmp = sdate.split("-");
	var fdate_tmp = fdate.split("-");
	stime_tmp[0] = stime.substring(0,2);
	stime_tmp[1] = stime.substring(2,4);
	ftime_tmp[0] = ftime.substring(0,2);
	ftime_tmp[1] = ftime.substring(2,4);
	
	var inputSdate = new Date( sdate_tmp[0], sdate_tmp[1]-1, sdate_tmp[2], stime_tmp[0], stime_tmp[1], 0);
	var inputFdate = new Date( fdate_tmp[0], fdate_tmp[1]-1, fdate_tmp[2], ftime_tmp[0], ftime_tmp[1], 0);
	
	// 같은날 종료시간이 시작시간보다 앞설때
	if(inputSdate.getTime() >= inputFdate.getTime()) {
		alert("종료시간이 올바르지 않습니다.");
		document.msgform.ftime.focus();
		return false;
	}

	// 메세지간 비교 처리
	for(var i=1; i<=rowNum-1; i++) {
		var sdate_list = mtable.rows[i].cells[0].outerText;
		var stime_list = mtable.rows[i].cells[1].outerText;
		var fdate_list = mtable.rows[i].cells[2].outerText;
		var ftime_list = mtable.rows[i].cells[3].outerText;
		var sdatelist_tmp = sdate_list.split("-");
		var stimelist_tmp = stime_list.split(":");
		var fdatelist_tmp = fdate_list.split("-");
		var ftimelist_tmp = ftime_list.split(":");
			
		var getSdate = new Date( sdatelist_tmp[0], sdatelist_tmp[1]-1, sdatelist_tmp[2], stimelist_tmp[0], stimelist_tmp[1], 0);
		var getFdate = new Date( fdatelist_tmp[0], fdatelist_tmp[1]-1, fdatelist_tmp[2], ftimelist_tmp[0], ftimelist_tmp[1], 0);
			
		if ( (getSdate.getTime() > inputSdate.getTime() && getSdate.getTime() > inputFdate.getTime()) ) {
		} 
		else if ( (getFdate.getTime() < inputSdate.getTime() && getFdate.getTime() < inputFdate.getTime()) ) {
		}
		else {
			alert("메세지 시간범위가 중복됩니다. 시간을 확인하세요!");
			document.msgform.stime.focus();
			return false;
		}
			/*if( (getSdate.getTime() >= inputSdate.getTime() && getSdate.getTime() <= inputFdate.getTime() && 
					getFdate.getTime() >= inputSdate.getTime() && getFdate.getTime() >= inputSdate.getTime()) ) {
				alert("메세지 시간범위가 중복됩니다. 시간을 확인하세요! - 1");
				document.msgform.ftime.focus();
				return false;
			}
			else if( (getFdate.getTime() >= inputSdate.getTime() && getFdate.getTime() <= inputFdate.getTime()) && getSdate.getTime() <= inputSdate.getTime() && getSdate.getTime() <= inputSdate.getTime() ) {
				alert("메세지 시간범위가 중복됩니다. 시간을 확인하세요! - 2");
				document.msgform.stime.focus();
				return false;
			}
			else if( (getSdate.getTime() <= inputSdate.getTime() && getSdate.getTime() <= inputFdate.getTime() && getFdate.getTime() >= inputSdate.getTime() && getFdate.getTime() >= inputFdate.getTime()) ){
				alert("메세지 시간범위가 중복됩니다. 시간을 확인하세요! - 3");
				document.msgform.stime.focus();
				return false;
			}
			else if( (getSdate.getTime() >= inputSdate.getTime() && getFdate.getTime() <= inputFdate.getTime()) ) {
				alert("메세지 시간범위가 중복됩니다. 시간을 확인하세요! - 4");
				document.msgform.stime.focus();
				return false;
			}*/
	}
	return true;
}

function ucheckForm(val) {
	var num = val;
	var mtable = document.getElementById("mtable");
	var rowNum = mtable.rows.length;
	
	if(document.updateForm.usdate.value=="") {
		alert("시작날짜를 지정하세요!");
		document.updateForm.usdata.focus();
		return false;
	}
	if(document.updateForm.ufdate.value=="") {
		alert("종료날짜를 지정하세요!");
		document.updateForm.ufdata.focus();
		return false;
	}
	if(document.updateForm.usdate.value.length < 10) {
		alert("시작날짜입력란을 확인하세요.");
		document.updateForm.sdata.focus();
		return false;
	}
	if(document.updateForm.ufdate.value.length < 10) {
		alert("종료날짜입력란을 확인하세요.");
		document.updateForm.ufdata.focus();
		return false;
	}
	if(document.updateForm.ustime.value=="") {
		alert("시작시간을 입력하세요!");
		document.updateForm.ustime.focus();
		return false;
	}
	if(document.updateForm.ustime.value.length < 4) {
		alert("시작시간을 확인하세요");
		document.updateForm.ustime.focus();
		return false;
	}
	
	if(document.updateForm.uftime.value=="") {
		alert("종료시간을 입력하세요!");
		document.updateForm.uftime.focus();
		return false;
	}
	if(document.updateForm.uftime.value.length < 4) {
		alert("종료시간을 확인하세요!");
		document.updateForm.uftime.focus();
		return false;
	}
	
	var usdate = document.updateForm.usdate.value;
	var ufdate = document.updateForm.ufdate.value;
	var ustime = document.updateForm.ustime.value;
	var uftime = document.updateForm.uftime.value;
	var stime_tmp = new Array(2);
	var ftime_tmp = new Array(2);
	
	var sdate_tmp = usdate.split("-");
	var fdate_tmp = ufdate.split("-");
	stime_tmp[0] = ustime.substring(0,2);
	stime_tmp[1] = ustime.substring(2,4);
	ftime_tmp[0] = uftime.substring(0,2);
	ftime_tmp[1] = uftime.substring(2,4);
	
	var inputSdate = new Date( sdate_tmp[0], sdate_tmp[1]-1, sdate_tmp[2], stime_tmp[0], stime_tmp[1], 0);
	var inputFdate = new Date( fdate_tmp[0], fdate_tmp[1]-1, fdate_tmp[2], ftime_tmp[0], ftime_tmp[1], 0);
	
	// 같은날 종료시간이 시작시간보다 앞설때
	if(inputSdate.getTime() >= inputFdate.getTime()) {
		alert("종료시간이 올바르지 않습니다.");
		document.updateForm.uftime.focus();
		return false;
	}

	// 메세지간 비교 처리
	for(var i=1; i<=rowNum-1; i++) {
		var usdate_list = mtable.rows[i].cells[0].outerText;
		var ustime_list = mtable.rows[i].cells[1].outerText;
		var ufdate_list = mtable.rows[i].cells[2].outerText;
		var uftime_list = mtable.rows[i].cells[3].outerText;
		var sdatelist_tmp = usdate_list.split("-");
		var stimelist_tmp = ustime_list.split(":");
		var fdatelist_tmp = ufdate_list.split("-");
		var ftimelist_tmp = uftime_list.split(":");
			
		var getSdate = new Date( sdatelist_tmp[0], sdatelist_tmp[1]-1, sdatelist_tmp[2], stimelist_tmp[0], stimelist_tmp[1], 0);
		var getFdate = new Date( fdatelist_tmp[0], fdatelist_tmp[1]-1, fdatelist_tmp[2], ftimelist_tmp[0], ftimelist_tmp[1], 0);
		
		if( num != i) {
			if ( (getSdate.getTime() > inputSdate.getTime() && getSdate.getTime() > inputFdate.getTime()) ) {
			} 
			else if ( (getFdate.getTime() < inputSdate.getTime() && getFdate.getTime() < inputFdate.getTime()) ) {
			}
			else {
				alert("메세지 시간범위가 중복됩니다. 시간을 확인하세요!");
				document.updateForm.ustime.focus();
				return false;
			}
		}
	}
	return true;
}