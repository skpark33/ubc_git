package com.exam.dao;

import java.util.HashMap;
import java.util.List;


public interface LogDAO {
	public List<String> inoutLog(HashMap map);
	public List<String> contentslog(HashMap map);
}
