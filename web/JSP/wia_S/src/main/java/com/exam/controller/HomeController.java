package com.exam.controller;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.exam.mapper.Mybatis;
import com.exam.model.UserVO;
import com.exam.service.CustomerService;
import com.exam.service.LogService;
import com.exam.service.StatisticsService;
import com.exam.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private SqlSessionFactory sqlMapper = Mybatis.getInstance();
	
	@Resource
	private StatisticsService statisticsService;
	@Resource
	private LogService logService;
	@Resource
	private CustomerService customerService;
	@Resource
	private UserService userService;
	
	
/**** Side Accordion Menu ****/
	// 로그인 페이지
	@RequestMapping("/index.do") 
	public String index() {
		return "login";
	}
	
	// 로그인 처리
	@RequestMapping("/login.do")
	public String login(@ModelAttribute UserVO uservo, HttpSession session, HttpServletRequest request) {
		List<UserVO> user = userService.login(uservo);
		if(user.size() != 0) {
			session.setAttribute("userinfo", user);
			return "redirect:/main.do";
		}
		else {
			session.invalidate();
			return "redirect:/index.do";
		}
	}
	
	// 로그아웃 처리 
	@RequestMapping("/logout.do") 
	public String logout(HttpSession session, HttpServletRequest request) {
		session = request.getSession();
		//session.removeAttribute("userinfo");
		session.invalidate();
		return "redirect:/index.do";
	}
	
	// UBC 다이렉트 로그인
	@RequestMapping("ubc.do")
	public String ubc(HttpServletRequest request, HttpSession session, @ModelAttribute UserVO uservo) {
		String ID = request.getParameter("ID");
		String PW = request.getParameter("PW");
		String PROGRAM = request.getParameter("PROGRAM");
		
		uservo.setUserId(ID);
		uservo.setPassword(PW);
		List<UserVO> user = userService.login(uservo);
		session.setAttribute("userinfo", user);
		
		return "redirect:/"+PROGRAM+".do";		
	}
	
	// HOME 메뉴 이동
	@RequestMapping("/main.do")
	public String main(HttpSession session, Model model) {
		
		return "home";
	}
	
	// Top 10 통계 메뉴 이동
	@RequestMapping("/topten.do")
	public String top(Model model, HttpSession session, HttpServletRequest request) {
		String menu1 = "";
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("menu1", menu1);
			
		List keyword = statisticsService.menuCode(map);
		model.addAttribute("keyword", keyword);
			
		return "topten";
	}
	
	// 기간별 체험 통계 메뉴 이동
	@RequestMapping("/period.do")
	public String period(Model model) {
		String menu1 = "";
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("menu1", menu1);
		
		List keyword = statisticsService.menuCode(map);
		model.addAttribute("keyword", keyword);
		
		return "period";
	}
	
/**** ajax Org Return Controller ****/
	/* 메뉴코드 */
	@RequestMapping("keySelect.do")
	public String keySelect(HttpServletRequest request, Model model) {
		String menu1 = request.getParameter("key");
		String menu2 = request.getParameter("key2");
		//String menu3 = request.getParameter("key3");
		String opHTML = "<option>전체</option>\n";
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("menu1", menu1);
		map.put("menu2", menu2);
		//map.put("menu3", menu3);
		
		List keyword = statisticsService.menuCode(map);
		if(keyword.size() != 0) {
			model.addAttribute("opHTML", opHTML);
			model.addAttribute("keyword", keyword);
		}
		return "ajax/orgajax";
	}
	
	/* 조직코드 */
	/* 현대위아 웹페이지에서 조직코드를 사용하지 않음 */
	/*
	@RequestMapping("/orgSelect.do")
	public String orgSelect(HttpServletRequest request, Model model) {
		String view = request.getParameter("view");
		String menu = request.getParameter("org");
		String franchizeType = "HYUNDAI";
		String opHTML = "<option>전체</option>\n";
		
		HashMap map = new HashMap();
		map.put("parentId", menu);
		map.put("franchizeType", franchizeType);
		
		List orglist = statisticsService.orgCode(map);
		if(orglist.size() != 0) {
			model.addAttribute("opHTML", opHTML);
			model.addAttribute("orglist2", orglist);
		}
		return "ajax/orgajax";
	}
	*/
	
/**** ajax Controller ****/
	/* 통계 데이터처리 컨트롤러 */
	/* 통계차트 데이터 컨트롤러 */
	
	// 기간별 컨텐츠 체험 통계
	@RequestMapping("periodSubmit.do")
	public String periodSubmit(HttpServletRequest request, Model model) {
		ArrayList<String> colName = new ArrayList<String>();
		int colCnt=0;
		String tmp = null;
		String sdate = request.getParameter("sdate-search");
		String fdate = request.getParameter("fdate-search");
		String period = request.getParameter("period-kind");
		String menu1 = request.getParameter("menu1");
		String menu2 = request.getParameter("menu2");
		//String menu3 = request.getParameter("menu3");
		
		try{
			SqlSession session = sqlMapper.openSession();
			CallableStatement cs = session.getConnection().prepareCall("{call usp_Statistics_Wia_period(?,?,?,?,?,?)}");
			cs.setString(1, sdate);
			cs.setString(2, fdate);
			cs.setString(3, period);
			cs.setString(4, menu1);
			cs.setString(5, menu2);
			//cs.setString(6, menu3);
			cs.setString(6, "y");
			ResultSet rs = cs.executeQuery();
			
			ResultSetMetaData metadata = rs.getMetaData();
			colCnt = metadata.getColumnCount();
			for(int i=1; i<=colCnt; i++) {
				tmp = metadata.getColumnName(i);
				colName.add("'" + tmp + "'");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("sdate", sdate);
		map.put("fdate", fdate);
		map.put("period", period);
		map.put("menu1", menu1);
		map.put("menu2", menu2);
		//map.put("menu3", menu3);
		
		List data = statisticsService.periodStst(map);
		model.addAttribute("data", data);
		model.addAttribute("col", colName);
		model.addAttribute("colcnt", colCnt-1);
		model.addAttribute("period", period);
		
		return "ajax/periodajax";
	}
	
	// TOP 10 컨텐츠 통계
	@RequestMapping("toptenSubmit.do")
	public String toptenSubmit(HttpServletRequest request, Model model) {
		String sdate = request.getParameter("sdate-search");
		String fdate = request.getParameter("fdate-search");
		String menu1 = request.getParameter("menu1");
		String menu2 = request.getParameter("menu2");
		String menu3 = request.getParameter("menu3");
		//String customer="";

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("sdate", sdate);
		map.put("fdate", fdate);
		map.put("menu1", menu1);
		map.put("menu2", menu2);
		map.put("menu3", menu3);
		//map.put("custom", customer);
		
		List data = statisticsService.toptenStst(map);
		model.addAttribute("data", data);
		return "ajax/toptenajax";
	}
	
	@RequestMapping("/autoSubmit.do")
	public String autoSubmit(Model model) throws ParseException {
		Calendar today = Calendar.getInstance();
		String fdate = today.get(today.YEAR) + "-" + (today.get(today.MONTH) + 1) + "-" + today.get(today.DAY_OF_MONTH);
		today.add(today.DAY_OF_MONTH, -7);
		String sdate = today.get(today.YEAR) + "-" + (today.get(today.MONTH) + 1) + "-" + today.get(today.DAY_OF_MONTH);
		
		Date s = new java.sql.Date(df.parse(sdate).getTime());
		Date f = new java.sql.Date(df.parse(fdate).getTime());
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("sdate", df.format(s));
		map.put("fdate", df.format(f));
		
		List data = statisticsService.recentlist(map);
		model.addAttribute("data", data);
		
		return "ajax/recentajax";
	}
	
	// TOP 10 엑셀파일 
	@RequestMapping("/toptenExcel.do")
	public String toptenExcel(Model model, HttpServletRequest request) {
		String sdate = request.getParameter("sdate-search");
		String fdate = request.getParameter("fdate-search");
		String menu1 = request.getParameter("menu1");
		String menu2 = request.getParameter("menu2");
		//String menu3 = request.getParameter("menu3");
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("sdate", sdate);
		map.put("fdate", fdate);
		map.put("menu1", menu1);
		map.put("menu2", menu2);
		//map.put("menu3", menu3);
		
		List data = statisticsService.toptenStst(map);
		model.addAttribute("data", data);
		model.addAttribute("sdate", sdate);
		model.addAttribute("fdate", fdate);
		model.addAttribute("type", "top");
		
		return "download";
	}
	
	// 기간별통계 엑셀파일
	@RequestMapping("/periodExcel.do")
	public String periodExcel(Model model, HttpServletRequest request) {
		String sdate = request.getParameter("sdate-search");
		String fdate = request.getParameter("fdate-search");
		String menu1 = request.getParameter("menu1");
		String menu2 = request.getParameter("menu2");
		//String menu3 = request.getParameter("menu3");
		String period = request.getParameter("period-kind");
		ArrayList<String> colName = new ArrayList<String>();
		int colCnt=0;
		String tmp = null;
		
		try{
			SqlSession session = sqlMapper.openSession();
			CallableStatement cs = session.getConnection().prepareCall("{call usp_Statistics_Wia_period(?,?,?,?,?,?)}");
			cs.setString(1, sdate);
			cs.setString(2, fdate);
			cs.setString(3, period);
			cs.setString(4, menu1);
			cs.setString(5, menu2);
			//cs.setString(6, menu3);
			cs.setString(6, "y");
			ResultSet rs = cs.executeQuery();
			
			ResultSetMetaData metadata = rs.getMetaData();
			colCnt = metadata.getColumnCount();
			for(int i=1; i<=colCnt; i++) {
				tmp = metadata.getColumnName(i);
				colName.add(tmp);
			}
		}catch(Exception e) {
			logger.warn(e.toString());
		}
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("sdate", sdate);
		map.put("fdate", fdate);
		map.put("menu1", menu1);
		map.put("menu2", menu2);
		//map.put("menu3", menu3);
		map.put("period", period);
		
		List data = statisticsService.periodStst(map);
		model.addAttribute("data", data);
		model.addAttribute("sdate", sdate);
		model.addAttribute("fdate", fdate);
		model.addAttribute("colName", colName);
		model.addAttribute("period_input", period);
		model.addAttribute("type", "period");
		
		return "download";
	}
	
}