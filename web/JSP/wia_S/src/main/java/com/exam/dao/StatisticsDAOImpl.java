package com.exam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.exam.model.statisticsVO;

@Repository
public class StatisticsDAOImpl implements StatisticsDAO {

	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public List<statisticsVO> orgCode(HashMap map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.orgCode", map);
	}
	
	@Override
	public List<statisticsVO> customerlist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.customerlist");
	}
	
	@Override
	public List<statisticsVO> menuCode(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.menuCode", map);
	}
	
	@Override
	public List<statisticsVO> toptenStst(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.toptenStst", map);
	}
	
	@Override
	public List<statisticsVO> progressStst(HashMap map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.progressStst", map);
	}

	@Override
	public List<HashMap<String, Integer>> periodStst(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.periodStst", map);
	}

	@Override
	public List<statisticsVO> customStst(HashMap map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.customStst", map);
	}

	@Override
	public List<statisticsVO> recentList(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.recentlist", map);
	}
	
	/*@Override
	public List<statisticsVO> keywordlist(HashMap map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.statistics.keywordlist", map);
	}*/


}
