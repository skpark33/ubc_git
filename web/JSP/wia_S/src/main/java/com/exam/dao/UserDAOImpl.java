package com.exam.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.exam.model.UserVO;

@Component
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public List<UserVO> login(UserVO uservo) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.user.login", uservo);
	}

}
