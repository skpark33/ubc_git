package com.exam.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.w3c.dom.*;

import com.exam.model.BoardVO;
import com.exam.model.UserVO;
import com.exam.service.BoardService;

@Controller
public class BoardController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private String uploadRepository;
	SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	public void setUploadRepository(String uploadRepository) {
		this.uploadRepository = uploadRepository;
	}
	
	@Resource
	private BoardService boardService;
	
	/**** Welcome Board Controller *****/
	/* 웰컴보드 웹페이지 컨트롤러 */
	
	// 웰컴보드 메인
	@RequestMapping("/welcome.do")
	public String welcome(Model model) {
		List msg_list = boardService.listMsg();
		String nowMsg = boardService.nowMsg();
		model.addAttribute("nowMsg", nowMsg);
		model.addAttribute("list", msg_list);
		return "home";
	}
	
	@RequestMapping("/createxml.do")
	public String createxml(Model model) {
		return "createxml";
	}
	
	// 현재시간 이후 리스트 xml 뷰 페이지
	@RequestMapping("/xmlview.do")
	public String xmlview(Model model) {
		List xmldata = boardService.loadMsg();
		model.addAttribute("msg", xmldata);
		return "xmlview";
	}
	
	// 리스트 입력
	@RequestMapping("/wcbInsert.do")
	public String wcbInsert(@ModelAttribute BoardVO boardVO, HttpServletRequest request, RedirectAttributes redirectAttribute) throws ParseException {
		HttpSession session = request.getSession();
		List<UserVO> userinfo = (List) session.getAttribute("userinfo");
		redirectAttribute.addAttribute("sdate_check", request.getParameter("sdate_check"));
		redirectAttribute.addAttribute("fdate_check", request.getParameter("fdate_check"));
		Time stime = new java.sql.Time(sdf.parse(request.getParameter("stime")).getTime());
		Time ftime = new java.sql.Time(sdf.parse(request.getParameter("ftime")).getTime());
		MultipartFile file = boardVO.getFile();
		String filename = file.getOriginalFilename();
		long filesize = file.getSize();
		String md5 = null;
		
		String mid = UUID.randomUUID().toString().replace("-","");
		
		String savePath = uploadRepository + File.separator + mid + File.separator;
		try {
			createDir(savePath);
			file.transferTo(new File(savePath+filename));
			md5 = filemd5(savePath+filename); 
		} catch(Exception e) {
			logger.warn(e.toString());
		}
		
		boardVO.setMid(mid);
		boardVO.setStime(stime+"");
		boardVO.setFtime(ftime+"");
		boardVO.setFilesize(filesize);
		boardVO.setFilename(filename);
		boardVO.setFileMD5(md5);
		boardVO.setUser(userinfo.get(0).getUserId());
		boardService.insertMsg(boardVO);		
		return "redirect:/createxml.do";
	}
	
	// 리스트 삭제
	@RequestMapping("/wcbDelete.do")
	public String wcbDelete(HttpServletRequest request, RedirectAttributes redirectAttribute) {
		String mid = request.getParameter("mid");
		redirectAttribute.addAttribute("sdate_check", request.getParameter("sdate_check"));
		redirectAttribute.addAttribute("fdate_check", request.getParameter("fdate_check"));
		
		String filename = boardService.setfilename(mid);
		String filePath = uploadRepository + File.separator + mid;
		
		try {
			File file = new File(filePath);
			deleteDir(file);
		} catch(Exception e) {
			logger.warn(e.toString());
		}
		
		boardService.delMsg(mid);
		
		return "redirect:/createxml.do";
	}
	
	// 리스트 수정
	@RequestMapping("/wcbUpdate.do")
	public String wcbUpdate(@ModelAttribute BoardVO boardVO, HttpServletRequest request, RedirectAttributes redirectAttribute) throws ParseException {
		HttpSession session = request.getSession();
		List<UserVO> userinfo = (List) session.getAttribute("userinfo");
		String mid = request.getParameter("mid");
		Date sdate = new java.sql.Date(df.parse(request.getParameter("usdate")).getTime());
		Date fdate = new java.sql.Date(df.parse(request.getParameter("ufdate")).getTime());
		Time stime = new java.sql.Time(sdf.parse(request.getParameter("ustime")).getTime());
		Time ftime = new java.sql.Time(sdf.parse(request.getParameter("uftime")).getTime());
		redirectAttribute.addAttribute("sdate_check", request.getParameter("sdate_check"));
		redirectAttribute.addAttribute("fdate_check", request.getParameter("fdate_check"));
		String oldfile = request.getParameter("oldfile");
		long oldsize = Integer.parseInt(request.getParameter("oldsize"));
		MultipartFile file = boardVO.getFile();
		String filename = file.getOriginalFilename();
		long filesize = file.getSize();
		String md5 = null;
		
		String oldPath = uploadRepository + File.separator + mid + File.separator + oldfile;
		String savePath = uploadRepository + File.separator + mid + File.separator + filename;
		try {
			if(file.getSize() > 0) { 	// 파일 업로드 했을 때, 이전 파일 무조건 삭제
				logger.info("Old file path = {}", oldPath);
				logger.info("Update file path = {}", savePath);
				File old = new File(oldPath);
				old.delete();
				file.transferTo(new File(savePath));
				md5 = filemd5(savePath);
			}
			else {	// 파일 업로드 안했을 때 이전 파일 유지
				filename = oldfile;
				filesize = oldsize;
				md5 = filemd5(oldPath);
			}
		} catch(Exception e) {
			logger.warn(e.toString());
		}
		
		boardVO.setMid(mid);
		boardVO.setSdate(sdate);
		boardVO.setFdate(fdate);
		boardVO.setStime(stime+"");
		boardVO.setFtime(ftime+"");
		boardVO.setFilesize(filesize);
		boardVO.setFilename(filename);
		boardVO.setFileMD5(md5);
		boardVO.setUser(userinfo.get(0).getUserId());
		boardService.modiMsg(boardVO);
		return "redirect:/createxml.do";
	}
	
	// 수정 Ajax
	@RequestMapping("/wcbModify.do") 
	public String wcbModify(Model model, HttpServletRequest request, RedirectAttributes redirectAttribute) {
		String mid = request.getParameter("mid");
		int rowNum = Integer.parseInt(request.getParameter("rowNum"));
		//redirectAttribute.addAttribute("sdate_check", request.getParameter("sdate_check"));
		//redirectAttribute.addAttribute("fdate_check", request.getParameter("fdate_check"));
		
		List sellist = boardService.midMsg(mid);
		model.addAttribute("midData", sellist);
		model.addAttribute("rowNum", rowNum);
		model.addAttribute("sdate_check", request.getParameter("sdate_check"));
		model.addAttribute("fdate_check", request.getParameter("fdate_check"));
		
		return "ajax/modiajax";
	}
	
	// 기간 리스트 검색
	@RequestMapping("/wcbSearch.do")
	public String wcbSearch(Model model, HttpServletRequest request) {
		String sdate_check = request.getParameter("sdate_check");
		String fdate_check = request.getParameter("fdate_check");
		HashMap map = new HashMap();
		map.put("sdate_check", sdate_check);
		map.put("fdate_check", fdate_check);
		
		String nowMsg = boardService.nowMsg();
		List msg_list = boardService.searchMsg(map);
		
		request.setAttribute("sdate_check",	sdate_check);
		request.setAttribute("fdate_check", fdate_check);
		model.addAttribute("list", msg_list);
		model.addAttribute("nowMsg", nowMsg);
		return "home";
	}
	
	// Make Directory 
	public void createDir(String path) {
		File dir = new File(path);
		
		if(!dir.exists()) {
			dir.mkdirs();
		}
	}
		
	// 디렉토리 및 하위파일 모두 삭제
	public void deleteDir(File path) {
		if(!path.exists()) {
			return;
		}
		File []files = path.listFiles();
		for(File file : files) {
			if( file.isDirectory()) {
				deleteDir(file);
			}
			else if( file.isFile()) {
				file.delete();
			}
		}
		path.delete();
	}
		
	// file MD5
	public String filemd5(String fileName) throws Exception {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		
		FileInputStream fis = new FileInputStream(new File(fileName));
		BufferedInputStream bis = new BufferedInputStream(fis);
		DigestInputStream dis = new DigestInputStream(bis, md5);
		
		while(dis.read() != -1) ;
			
		byte[] hash = md5.digest();
		
		return byteArray2Hex(hash);
	}
		
	private String byteArray2Hex(byte[] hash) {
		Formatter formatter = new Formatter();
		for(byte b : hash) {
			formatter.format("%02X", b);
		}
		return formatter.toString();
	}
}