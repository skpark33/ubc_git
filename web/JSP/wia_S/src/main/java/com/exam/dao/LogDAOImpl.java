package com.exam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class LogDAOImpl implements LogDAO {
	
	@Autowired
	private SqlSession sqlSession;
	@Override
	public List<String> inoutLog(HashMap map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.log.inoutLog", map);
	}
	@Override
	public List<String> contentslog(HashMap map) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.log.contentslog", map);
	}

}
