package com.exam.service;

import com.exam.model.CustomerVO;

public interface CustomerService {
	public void income(CustomerVO customerVO);
	public void outgo(CustomerVO customerVO);
}
