package com.exam.service;

import java.util.HashMap;
import java.util.List;


public interface LogService {
	public List<String> inoutLog(HashMap map);
	public List<String> contentslog(HashMap map);
}
