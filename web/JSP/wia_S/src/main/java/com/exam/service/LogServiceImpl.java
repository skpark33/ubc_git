package com.exam.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.dao.LogDAO;

@Service
public class LogServiceImpl implements LogService {
	
	@Autowired
	private LogDAO logdao;
	@Override
	public List<String> inoutLog(HashMap map) {
		// TODO Auto-generated method stub
		return logdao.inoutLog(map);
	}
	@Override
	public List<String> contentslog(HashMap map) {
		// TODO Auto-generated method stub
		return logdao.contentslog(map);
	}

}
