package com.exam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.dao.CustomerDAO;
import com.exam.model.CustomerVO;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerDAO;
	
	@Override
	public void income(CustomerVO customerVO) {
		// TODO Auto-generated method stub
		customerDAO.income(customerVO);
	}

	@Override
	public void outgo(CustomerVO customerVO) {
		// TODO Auto-generated method stub
		customerDAO.outgo(customerVO);
	}

}
