package com.exam.dao;

import com.exam.model.CustomerVO;

public interface CustomerDAO {
	public void income(CustomerVO customerVO);
	public void outgo(CustomerVO customerVO);
}
