package com.exam.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.exam.dao.StatisticsDAO;
import com.exam.model.statisticsVO;

@Service
public class StatisticsServiceImpl implements StatisticsService {

	@Autowired
	private StatisticsDAO statisticsDAO;
	
	@Override
	public List<statisticsVO> orgCode(HashMap map) {
		// TODO Auto-generated method stub
		return statisticsDAO.orgCode(map);
	}

	@Override
	public List<statisticsVO> customerlist() {
		// TODO Auto-generated method stub
		return statisticsDAO.customerlist();
	}

	@Override
	public List<statisticsVO> menuCode(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return statisticsDAO.menuCode(map);
	}

	@Override
	public List<statisticsVO> toptenStst(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return statisticsDAO.toptenStst(map);
	}

	@Override
	public List<statisticsVO> progressStst(HashMap map) {
		// TODO Auto-generated method stub
		return statisticsDAO.progressStst(map);
	}

	@Override
	public List<HashMap<String, Integer>> periodStst(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return statisticsDAO.periodStst(map);
	}

	@Override
	public List<statisticsVO> customStst(HashMap map) {
		// TODO Auto-generated method stub
		return statisticsDAO.customStst(map);
	}

	@Override
	public List<statisticsVO> recentlist(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return statisticsDAO.recentList(map);
	}

	/*@Override
	public List<statisticsVO> keywordlist(HashMap map) {
		// TODO Auto-generated method stub
		return statisticsDAO.keywordlist(map);
	}*/

}
