package com.exam.service;

import java.util.HashMap;
import java.util.List;

import com.exam.model.statisticsVO;

public interface StatisticsService {
	public List<statisticsVO> orgCode(HashMap map);
	public List<statisticsVO> customerlist();
	public List<statisticsVO> menuCode(HashMap<String, String> map);
	public List<statisticsVO> toptenStst(HashMap<String, String> map);
	public List<statisticsVO> progressStst(HashMap map);
	public List<HashMap<String, Integer>> periodStst(HashMap<String, String> map);
	public List<statisticsVO> customStst(HashMap map);
	public List<statisticsVO> recentlist(HashMap<String, String> map);
	//public List<statisticsVO> keywordlist(HashMap map);
}
