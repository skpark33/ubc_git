package com.exam.view;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.exam.model.statisticsVO;

public class ExcelView extends AbstractExcelView {
	private static final Logger logger = LoggerFactory.getLogger(ExcelView.class);
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			HSSFWorkbook workbook, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		// 통계 조건
		String _type = (String) model.get("type");
		String _sdate = (String) model.get("sdate");
		String _fdate = (String) model.get("fdate");
		String userAgent = request.getHeader("User-Agent");
		String _fileDate = _sdate + "_" + _fdate;
		String fileName = _fileDate;
		
		Map<String, CellStyle> styles = createStyle(workbook);
		
		if( _type.equals("top")) {
			List<statisticsVO> list = (List<statisticsVO>) model.get("data");
			
			fileName += "_top10통계";
			
			HSSFSheet sheet = workbook.createSheet("Top10통계");
			
			HSSFRow titleRow = sheet.createRow(0);
			titleRow.setHeightInPoints(35);
			HSSFCell titleCell = titleRow.createCell(0);
			titleCell.setCellValue("TOP 10 통계 (" + _sdate + "~" + _fdate + ")");
			sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$c$1"));
			titleCell.setCellStyle(styles.get("title"));
			
			HSSFRow headRow = sheet.createRow(2);
			headRow.createCell(0).setCellValue("순위");
			headRow.createCell(1).setCellValue("컨텐츠");
			headRow.createCell(2).setCellValue("터치횟수");
			
			// 데이터
			HSSFRow dataRow = null;
			//HSSFCell dataCell = null;
			int rowNum = 3;
			for(int i=0; i<list.size(); i++) {
				dataRow = sheet.createRow(rowNum++);
				dataRow.createCell(0).setCellValue(list.get(i).getRank());
				dataRow.createCell(1).setCellValue(list.get(i).getContents());
				dataRow.createCell(2).setCellValue(list.get(i).getPoint());
				sheet.setColumnWidth(0, 9*256);
				sheet.setColumnWidth(1, 23*256);
				sheet.setColumnWidth(2, 12*256);
			}
		}
		
		if( _type.equals("period")) {
			List<HashMap<String, Integer>> list = (List<HashMap<String, Integer>>) model.get("data");
			ArrayList<String> colList = (ArrayList<String>) model.get("colName");
			String _period = (String) model.get("period_input");
			
			fileName += "_기간별통계";
			
			HSSFSheet sheet = workbook.createSheet("기간별통계");
			
			HSSFRow titleRow = sheet.createRow(0);
			titleRow.setHeightInPoints(35);
			HSSFCell titleCell = titleRow.createCell(0);
			titleCell.setCellValue("기간별 통계 (" + _sdate + "~" + _fdate + ")");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, colList.size()-1));
			titleCell.setCellStyle(styles.get("title"));
			
			HSSFRow headRow = sheet.createRow(2);
			for(int i=0; i<colList.size(); i++) {
				headRow.createCell(i).setCellValue(colList.get(i));
			}
			
			HSSFRow dataRow = null;
			//HSSFCell dataCell = null;
			int rowNum = 3;
			for( int j=0; j<list.size(); j++) {
				dataRow = sheet.createRow(rowNum++);
				if( _period.equals("year")) {
					dataRow.createCell(0).setCellValue(list.get(j).get("Period") + "년");
				}
				else if( _period.equals("month")) {
					dataRow.createCell(0).setCellValue(list.get(j).get("Period") + "월");
				}
				else if( _period.equals("quarter")) {
					dataRow.createCell(0).setCellValue(list.get(j).get("Period") + "분기");
				}
				else if( _period.equals("days")) {
					dataRow.createCell(0).setCellValue(list.get(j).get("Period") + "일");
				}
				for(int k=1; k<colList.size(); k++) {
					dataRow.createCell(k).setCellValue(list.get(j).get("col"+k));
					sheet.setColumnWidth(k, 11*256);
				}
			}
			
		}
		
		if(userAgent.indexOf("MSIE") > -1) {
			fileName = URLEncoder.encode(fileName, "utf-8");
		}
		else {
			fileName = new String(fileName.getBytes("utf-8"), "iso-8859-1");
		}
		
		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "Attachment; Filename=\"" + fileName + ".xls\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
	}

	private Map<String, CellStyle> createStyle(HSSFWorkbook workbook) {
		// TODO Auto-generated method stub
		Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
		CellStyle style;
		
		Font titleFont = workbook.createFont();
		titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		titleFont.setFontHeightInPoints((short)12);
		style = workbook.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFont(titleFont);
		styles.put("title", style);
		
		style = workbook.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		styles.put("head", style);
		
		return styles;
	}
	
}
