package com.exam.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.exam.model.BoardVO;

@Repository
public class BoardDAOImpl implements BoardDAO {

	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public void insertMsg(BoardVO boardVO) {
		sqlSession.insert("com.exam.mapper.welcome.insertMsg", boardVO);
	}
	@Override
	public List<BoardVO> loadMsg() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.welcome.loadMsg");
	}
	@Override
	public void delMsg(String mid) {
		// TODO Auto-generated method stub
		sqlSession.delete("com.exam.mapper.welcome.delMsg", mid);
	}
	@Override
	public void modiMsg(BoardVO boardVO) {
		// TODO Auto-generated method stub
		sqlSession.update("com.exam.mapper.welcome.modiMsg", boardVO);
	}
	@Override
	public List<BoardVO> searchMsg(HashMap map){
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.welcome.searchMsg", map);
	}
	@Override
	public List<BoardVO> listMsg() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.welcome.listMsg");
	}
	@Override
	public List<BoardVO> midMsg(String mid) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.exam.mapper.welcome.midMsg", mid);
	}
	@Override
	public String setfilename(String mid) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("com.exam.mapper.welcome.setfilename", mid);
	}
	@Override
	public String nowMsg() {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("com.exam.mapper.welcome.nowMsg");
	}

}
