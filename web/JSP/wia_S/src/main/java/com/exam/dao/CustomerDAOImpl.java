package com.exam.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.exam.model.CustomerVO;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public void income(CustomerVO customerVO) {
		// TODO Auto-generated method stub
		sqlSession.insert("com.exam.mapper.customer.income", customerVO);
	}

	@Override
	public void outgo(CustomerVO customerVO) {
		// TODO Auto-generated method stub
		sqlSession.update("com.exam.mapper.customer.outgo", customerVO);
	}

}
