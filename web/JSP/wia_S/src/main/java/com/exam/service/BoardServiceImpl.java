package com.exam.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.exam.dao.BoardDAO;
import com.exam.model.BoardVO;

@Service
public class BoardServiceImpl implements BoardService {

	@Autowired
	private BoardDAO boardDAO;
	
	@Override
	public void insertMsg(BoardVO boardVO) {
		// TODO Auto-generated method stub
		boardDAO.insertMsg(boardVO);
	}
	@Override
	public List<BoardVO> loadMsg() {
		// TODO Auto-generated method stub
		return (List)boardDAO.loadMsg();
	}
	@Override
	public void delMsg(String mid) {
		// TODO Auto-generated method stub
		boardDAO.delMsg(mid);
	}
	@Override
	public void modiMsg(BoardVO boardVO) {
		// TODO Auto-generated method stub
		boardDAO.modiMsg(boardVO);
	}
	@Override
	public List<BoardVO> searchMsg(HashMap map) {
		// TODO Auto-generated method stub
		return boardDAO.searchMsg(map);
	}
	@Override
	public List<BoardVO> listMsg() {
		// TODO Auto-generated method stub
		return (List)boardDAO.listMsg();
	}
	@Override
	public List<BoardVO> midMsg(String mid) {
		// TODO Auto-generated method stub
		return (List)boardDAO.midMsg(mid);
	}
	@Override
	public String setfilename(String mid) {
		// TODO Auto-generated method stub
		return boardDAO.setfilename(mid);
	}
	@Override
	public String nowMsg() {
		// TODO Auto-generated method stub
		return boardDAO.nowMsg();
	}

}
