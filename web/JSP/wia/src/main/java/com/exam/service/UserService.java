package com.exam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.dao.UserDAO;
import com.exam.model.UserVO;

public interface UserService {
	public List<UserVO> login(UserVO uservo);
	/*public List<UserVO> getUser();
	public void insertUser(UserVO uservo);*/
}
