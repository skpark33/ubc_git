package com.exam.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.exam.model.UserVO;

public interface UserDAO {
	
	public List<UserVO> login(UserVO uservo);
	/*public List<UserVO> getUser();
	public void insertUser(UserVO uservo);*/
	
}
