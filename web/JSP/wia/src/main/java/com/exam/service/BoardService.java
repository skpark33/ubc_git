package com.exam.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.exam.model.BoardVO;

public interface BoardService {
	public void insertMsg(BoardVO boardVO);
	public List<BoardVO> listMsg();
	public List<BoardVO> loadMsg();
	public List<BoardVO> midMsg(String mid);
	public void delMsg(String mid);
	public void modiMsg(BoardVO boardVO);
	public List<BoardVO> searchMsg(HashMap map);
	public String setfilename(String mid);
	public String nowMsg();
}
