package com.exam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.dao.UserDAO;
import com.exam.model.UserVO;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userdao;
	
	/*@Override
	public List<UserVO> getUser() {
		// TODO Auto-generated method stub
		return userdao.getUser();
	}
	@Override
	public void insertUser(UserVO uservo) {
		// TODO Auto-generated method stub
		userdao.insertUser(uservo);
	}*/
	
	@Override
	public List<UserVO> login(UserVO uservo) {
		// TODO Auto-generated method stub
		return userdao.login(uservo);
	}

}
