package com.exam.controller;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.exam.mapper.Mybatis;
import com.exam.model.UserVO;
import com.exam.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private SqlSessionFactory sqlMapper = Mybatis.getInstance();
	
	@Resource
	private UserService userService;
	
	
/**** Side Accordion Menu ****/
	// 로그인 페이지
	@RequestMapping("/index.do") 
	public String index() {

		return "login";
		//int active = 0;
		//model.addAttribute("active", active);
	}
	
	// 로그인 처리
	@RequestMapping("/login.do")
	public String login(@ModelAttribute UserVO uservo, HttpSession session, HttpServletRequest request) {
		List user = userService.login(uservo);
		if(user.size() != 0) {
			session.setAttribute("userinfo", user);
			return "redirect:/welcome.do";
		}
		else {
			session.invalidate();
			return "redirect:/index.do";
		}
	}
	
	// 로그아웃 처리 
	@RequestMapping("/logout.do") 
	public String logout(HttpSession session, HttpServletRequest request) {
		session = request.getSession();
		//session.removeAttribute("userinfo");
		session.invalidate();
		return "redirect:/index.do";
	}
	
	// HOME 메뉴 이동
	@RequestMapping("/main.do")
	public String main(HttpSession session, Model model) {
		
		return "home";
	}

}

/**
 * Simply selects the home view to render by returning its name.
 */
/*@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	//List list = userservice.getUser();
	//UserVO vo = (UserVO) userservice.getUser();
	//model.addAttribute("list", userservice.getUser());
	//model.addAttribute("list", list);
	}*/

/*
	@RequestMapping("/test.do")
	public String test(@ModelAttribute UserVO uservo, Model model, HttpServletRequest request) {
		int active = 1;
		String url = "custom.jsp";
		userservice.insertUser(uservo);
		//String text = request.getParameter("test");
		//model.addAttribute("text", text);
		model.addAttribute("active", active);
		model.addAttribute("url", url);
		return "home";
	}
 */