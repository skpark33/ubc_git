<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:out value="[ ['고객명', '날짜', '입장시간', '퇴장시간', '비고'], " escapeXml="false"/>
<c:forEach var="data" items="${ data }">
	<c:out value="[ '${data.customerName }'," escapeXml="false"/>
	<c:out value=" '${data.time }'," escapeXml="false"/>
	<c:out value=" '${data.intime }'," escapeXml="false"/>
	<c:out value=" '${data.outtime }'," escapeXml="false"/>
	<c:out value=" '${data.comment }' ], " escapeXml="false"/>
</c:forEach>
<c:out value=" ]"/>