<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="header">
		<div id="logo">
			<img alt="logo" src="./resources/img/HYUNDAI-WIALOGO.jpg" width="150px">
		</div>
		<div id="sidemenu">
			<h3>홈</h3>
			<div>
				<ul>
					<li><a href="index.do" class="submenu">홈</a></li>
				</ul>
			</div>
			<h3>통계</h3>
			<div>
				<ul>
					<li><a href="custom.do" class="submenu">고객별 체험 통계</a></li>
					<li><a href="period.do" class="submenu">기간별 체험 통계</a></li>
					<li><a href="top.do" class="submenu">Top 10 통계</a></li>
					<li><a href="progress.do" class="submenu">컨텐츠 체험 추이</a></li>
				</ul>
			</div>
			<h3>로그조회</h3>
			<div>
				<ul>
					<li><a href="inout.do" class="submenu">고객 입, 퇴장시간 로그</a></li>
					<li><a href="explog.do" class="submenu">컨텐츠 체험 로그</a></li>
				</ul>
			</div>
		</div>
	</div>
	