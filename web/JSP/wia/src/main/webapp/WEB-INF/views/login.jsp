<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	/* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=0.9">
<link rel="shortcut icon" href="./resources/img/favicon_H.ico" type="image/x-icon">
<link rel="stylesheet" href="./resources/css/login.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="./resources/js/jquery.placeholder.js"></script>
<title>현대위아 - 웰컴보드 LOGIN</title>
</head>
<body>
	<div class="login">
		<h1><img src="./resources/img/ko_wc_color_c.png" alt="logo" width="150px" id="logo"><br>
		<img src="./resources/img/Lock.png" width="20px" alt="lock">  Login</h1>
		<form method="POST" action="login.do">
			<p><input type="text" name="userId" placeholder="User ID"></p>
			<p><input type="password" name="password" value="" placeholder="Password"></p>
			<p class="submit"><input type="submit" name="commit" value="Login"></p>
	    </form>
	</div>
	<script>
		$(function() {
			$('input').placeholder();
		});
	</script>
</body>
</html>