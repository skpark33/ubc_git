<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%
	StringBuilder sb = new StringBuilder();
	String rk = null;
	String con = null;
	String pnt = null;
	
	sb.append("[ ['Contents', 'Click Counter'], ");
%>

	<c:forEach var="pie" items="${ data }">
		<c:set var="content" value="${ pie.contents }"/>
		<c:set var="point" value="${pie.point }"/>
	
	<%
		con = pageContext.getAttribute("content", PageContext.PAGE_SCOPE).toString();
		pnt = pageContext.getAttribute("point", PageContext.PAGE_SCOPE).toString();		
		
		sb.append("['" + con + "', " + pnt + "], ");	
	%>
	</c:forEach>
	
<%
	sb.append(" ]^*[ ['Rank', 'Contents', 'Click Counter'],");
	con = null;
	pnt = null;
%>

	<c:forEach var="table" items="${ data }">
		<c:set var="rank" value="${ table.rank }"/>
		<c:set var="content" value="${ table.contents }" />
		<c:set var="point" value="${ table.point }"/>
		
	<%
		rk = pageContext.getAttribute("rank", PageContext.PAGE_SCOPE).toString();
		con = pageContext.getAttribute("content", PageContext.PAGE_SCOPE).toString();
		pnt = pageContext.getAttribute("point", PageContext.PAGE_SCOPE).toString();
		
		sb.append("[" + rk + ", '" + con + "', " + pnt + "],");
	%>
	</c:forEach>
	
<%
	sb.append(" ]");
%>
<%=sb.toString() %>
	