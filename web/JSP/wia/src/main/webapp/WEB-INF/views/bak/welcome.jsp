<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- <%@ page session="false" %>
<%@ page import="java.util.*" %>
<%
	int cnt = 1;
	String sday = request.getParameter("sdate");
	String fday = request.getParameter("fdate");
%> --%>
<%-- <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<link rel="shortcut icon" href="./resources/img/favicon_W.ico" type="image/x-icon">
<link rel="stylesheet" href="./resources/css/welcome.css">
<link rel="stylesheet" href="./resources/css/jquery-ui-1.10.3.datepicker.css">
<script src="./resources/js/button.js"></script>
<script src="./resources/js/jquery-1.9.1.js"></script>
<script src="./resources/js/jquery-ui-1.10.3.datepicker.js"></script>
<script type="text/javascript">
	$(function() { 
		    $( "#sdate" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    showOn: 'button',
			    buttonImage: 'resources/img/calendar_20.png',
			    buttonImageOnly: true,
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
			    onClose: function( selectedDate ) {
			    		$('#fdate').datepicker("option", "minDate", selectedDate);
			    }
		    });
	});
	$(function() { 
		    $( "#fdate" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    showOn: 'button',
			    buttonImage: 'resources/img/calendar_20.png',
			    buttonImageOnly: true,
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
			    onClose: function( selectedDate ) {
			    		$('#sdate').datepicker("option", "maxDate", selectedDate);
			    }
		    });
	});
	$(function() { 
		$( "#date" ).datepicker({
			    inline: true,
			    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
			    prevText: 'prev',
			    nextText: 'next',
			    showButtonPanel: true,	/* 버튼 패널 사용 */
			    changeMonth: true,		/* 월 선택박스 사용 */
			    changeYear: true,		/* 년 선택박스 사용 */
			    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
			    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
			    minDate: '-30y',
			    closeText: '닫기',
			    currentText: '오늘',
			    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
			    /* 한글화 */
			    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
			    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
			    showAnim: 'fadeIn',
		    });
	});
	$(document).ready(function () {
		if( <%=sday %> == null && <%=fday %> == null ) {
			$('#sdate, #fdate').val($.datepicker.formatDate($.datepicker.ATOM, new Date()));
		}
		else {
			$('#sdate').val('<%=sday%>');
			$('#fdate').val('<%=fday%>');
		}
	});
</script> --%>
<title>현대위아 - Welcome Board</title>
</head>
<body>
	<div id="wrap">
		<div id="title">
			<img alt="logo" src="./resources/img/en_wc_color.png" width="280px">
		</div>
		<%-- <form name="msgform" method="POST">
		<div id="date-search">
			<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly/> -
			<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly/>
			<button type="button" class="btn btn-1 btn-1a" onclick="dateCheck()">조회</button>
			<!-- <a href="#"><img src="./resources/img/search_3.png" onclick="dateCheck()"/></a> -->
		</div>
		<div id="msg-table">
			<table id="mtable">
				<thead>
					<tr>
						<th>날짜</th>
						<th>시작시간</th>
						<th>종료시간</th>
						<th>웰컴보드 메세지</th>
						<th colspan="2">버튼</th>
					</tr>
				</thead>
				<tfoot>
					<tr id=foot>
						<td><input type="text" name="date" id="date" size="13" readonly/></td>
						<td><input type="text" name="stime" id="stime" class="input-time" size="8" maxlength="4" onkeypress="numCheck()"/></td>
						<td><input type="text" name="ftime" id="ftime" class="input-time" size="8" maxlength="4" onkeypress="numCheck()"/></td>
						<td><input type="text" name="msg" id="msg" size="50"/></td>
						<td colspan="2"><button type="button" id="btn_submit" class="btn btn-1 btn-1a" onclick="submit_js(1)">등록</button></td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach var="msglist" items="${ list }">
					<tr>
						<td>${ msglist.date }</td>
						<td>${ msglist.stime }</td>
						<td>${ msglist.ftime }</td>
						<td>${ msglist.msg }</td>
						<td><button type="button" class="btn btn-1 btn-1a" onclick="modi_js(<%=cnt%>, ${ msglist.mid })">수정</button></td>
						<td><button type="button" class="btn btn-2 btn-2a" onclick="del_js(${ msglist.mid })">삭제</button></td>
					</tr>
					<% cnt++; %>
					</c:forEach>
				</tbody>
			</table>
		</div>
		</form> --%>
	</div>
</body>
</html>