<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%
	request.setCharacterEncoding("utf-8");
	if(session.getAttribute("userinfo") == null) {
		response.sendRedirect("index.do");
	}
	
	/* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<jsp:include page="/WEB-INF/views/include/declare.jsp"></jsp:include>
	<script type="text/javascript">
		google.load('visualization', '1', {packages:['table']});
		
		$(document).ready(function() {
			$("#org-ex, #menu1, #menu2, #menu3").hide();
			$("#customer").multiselect({
				checkAllText: "전체선택",
				uncheckAllText: "선택해제",
				noneSelectedText: "고객명 선택",
				minWidth: 195
			});
		});
		function key_sel_menu2(val) {
			document.forms[0].menu2.selectedIndex=0;
			document.forms[0].menu3.selectedIndex=0;
			var menu = val.value;
			$.ajax({
				type: "POST",
				url: "keySelect.do",
				data: "key=" + menu + "&key2=",
				success: function(html) {
					if(html == 0) {
						$("#menu2").hide();
						$("#menu3").hide();
					}
					else {
						$("#menu3").hide();
						$("#menu2").show();
						$("#menu2").html(html);
					}
				}
			});
		}
		function key_sel_menu3(val) {
			document.forms[0].menu3.selectedIndex=0;
			var menu1 = document.getElementById("menu1").value;
			var menu2 = val.value;
			$.ajax({
				type: "POST",
				url: "keySelect.do",
				data: "key=" + menu1 + "&key2=" + menu2 + "&key3=",
				success: function(html) {
					if(html == 0) {
						$("#menu3").hide();
					}
					else {
						$("#menu3").show();
						$("#menu3").html(html);
					}
				}
			});
		}
		function formSubmit() {
			var formData = $('#form-if').serialize();
			
			jQuery.ajaxSettings.traditional = true;
			$.ajax({
				type: 'POST',
				url: 'customSubmit.do',
				data: formData,
				success: function(data) {
					drawChart(eval(data));
					google.setOnLoadCallback(drawChart);
				},
				error: function(request, status, error) {
				}
			});
		}
		function drawChart(formdata) {
			var data = google.visualization.arrayToDataTable(formdata);
			var cssClassNames = { tableCell: 'tableCellClass' };
			var option = {
					width: '1050px',
					cssClassNames: cssClassNames,
			};
			new google.visualization.Table(document.getElementById('tableview')).draw(data, option);
		}
	</script>
</head>
<body>
<div id="wrap">
	<div id="topline"></div>
	<jsp:include page="/WEB-INF/views/include/top.jsp"></jsp:include>
	<%-- <jsp:include page="/WEB-INF/views/sidemenu.jsp"></jsp:include> --%>
	<div id="main">
		<div id="tabs">
			<jsp:include page="/WEB-INF/views/include/logo.jsp"></jsp:include>
			<div id="title">
				<img src="./resources/img/grid.png" width="20" height="20"/>
				<p>고객별 체험 통계</p>
				<button type="button" class="btn btn-1 btn-1a" onclick="formSubmit()">전송</button>
			</div>
			<form id="form-if" action="test.do" method="post">
				<div id="search">
					<table id="form-search">
						<tr>
							<td class="subj">기 간</td>
							<td class="detail">
								<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly> -
								<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly>
							</td>
							<td class="subj" id="org-ex">컨텐츠</td>
							<td class="detail">
								<select id="menu1" class="menu1" onchange="key_sel_menu2(this)">
									<option>전체</option>
								<c:forEach var="menu" items="${ keyword }">
									<option value="${ menu.name}">${ menu.name }</option>
								</c:forEach>
								</select>
								<select id="menu2" class="menu2" onchange="key_sel_menu3(this)">
									<option>전체</option>
								</select>
								<select id="menu3" class="menu3">
									<option>전체</option>
								</select>
							</td>
						</tr>	
						<tr class="second-row">
							<td class="subj">고객 선택</td>
							<td class="detail" colspan="3">
								<select name="customer" id="customer" multiple="multiple">
								<c:forEach var="list" items="${ clist }">
									<option value="${ list }">${ list}</option>
								</c:forEach>
								</select>
							</td>
						</tr>
					</table>
				</div>
			</form>
			<div id="tableview"></div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/include/footer.jsp"></jsp:include>
</div>

</body>
</html>

<%-- <fieldset id="form-fieldset">
						<legend>검색 조건</legend>
						<div id="fieldset-if">
							<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly> -
							<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly>
							<select id="menu1" class="menu1" onchange="key_sel_menu2(this)">
								<option>전체</option>
							<c:forEach var="menu" items="${ keyword }">
								<option value="${ menu.name}">${ menu.name }</option>
							</c:forEach>
							</select>
							<select id="menu2" class="menu2" onchange="key_sel_menu3(this)">
								<option>전체</option>
							</select>
							<select id="menu3" class="menu3">
								<option>전체</option>
							</select>
						</div>
					</fieldset>
				</div>
				<div id="search-c">
					<fieldset id="form-fieldset">
						<legend>고객별 검색</legend>
						<div id="fieldset-if">
							<select name="customer" id="customer" multiple="multiple">
							<c:forEach var="list" items="${ clist }">
								<option value="${ list }">${ list}</option>
							</c:forEach>
							</select>
						</div>
					</fieldset> --%>