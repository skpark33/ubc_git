<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<thead>
	<tr>
		<th colspan='8'>웰컴메세지 수정</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>시작날짜</td>
		<td>시작시간</td>
		<td>종료날짜</td>
		<td>종료시간</td>
		<td>파일업로드</td>
		<td>업로드 된 파일명</td>
		<td colspan='2'>버튼</td>
	</tr>
</tbody>
<tfoot>
	<tr>
	<c:forEach var="data" items="${midData }" >
		<input type="hidden" name="sdate_check" id="sdate_check" value="${sdate_check }" />
		<input type="hidden" name="fdate_check" id="fdate_check" value="${fdate_check }" />
		<input type="hidden" name="mid" id="mid" value="${data.mid }" />
		<input type="hidden" name="oldfile" id="oldfile" value="${data.filename }"/>
		<input type="hidden" name="oldsize" id="oldsize" value="${data.filesize }"/>
		<td><input type="text" name="usdate" id="usdate" size="13" value="${data.sdate }" readonly/></td>
		<td><input type="text" name="ustime" id="ustime" class="input-time" size="8" maxlength="4" value="${fn:split(data.stime,':')[0]}${fn:split(data.stime,':')[1]}" title="입력양식 <br />오전 9시 = '0900' <br />오후 9시 = '2100'" onkeypress="numCheck()" style="ime-mode:Disabled;"/></td>
		<td><input type="text" name="ufdate" id="ufdate" size="13" value="${data.fdate }" readonly/></td>
		<td><input type="text" name="uftime" id="uftime" class="input-time" size="8" maxlength="4" value="${fn:split(data.ftime,':')[0]}${fn:split(data.ftime,':')[1]}" title="입력양식 <br />오전 9시 = '0900' <br />오후 9시 = '2100'" onkeypress="numCheck()" style="ime-mode:Disabled;"/></td>
		<td><input type="file" name="file" id="file" accept=".pptx,.ppt"></td>
		<td>${data.filename }</td>
		<td><button type="button" id="btn_submit" class="btn btn-3 btn-3a" onclick="update_js(${rowNum})">등록</button></td>
		<td><button type="button" id="btn_submit" class="btn btn-3 btn-3a" onclick="history.go(0)">취소</button></td>
	</c:forEach>
	</tr>
</tfoot>

<script type="text/javascript">
$(function() { 
	$( "#usdate" ).datepicker({
		    inline: true,
		    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
		    prevText: 'prev',
		    nextText: 'next',
		    showButtonPanel: true,	/* 버튼 패널 사용 */
		    changeMonth: true,		/* 월 선택박스 사용 */
		    changeYear: true,		/* 년 선택박스 사용 */
		    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
		    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
		    minDate: '-30y',
		    closeText: '닫기',
		    currentText: '오늘',
		    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
		    /* 한글화 */
		    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
		    showAnim: 'fadeIn',
		    onClose: function( selectedDate ) {
		    		$('#ufdate').datepicker("option", "minDate", selectedDate);
		    }
	    });
});
$(function() { 
	$( "#ufdate" ).datepicker({
		    inline: true,
		    dateFormat: "yy-mm-dd",	/* 날짜 포맷 */
		    prevText: 'prev',
		    nextText: 'next',
		    showButtonPanel: true,	/* 버튼 패널 사용 */
		    changeMonth: true,		/* 월 선택박스 사용 */
		    changeYear: true,		/* 년 선택박스 사용 */
		    showOtherMonths: true,	/* 이전/다음 달 일수 보이기 */
		    selectOtherMonths: true,	/* 이전/다음 달 일 선택하기 */
		    minDate: '-30y',
		    closeText: '닫기',
		    currentText: '오늘',
		    showMonthAfterYear: true,		/* 년과 달의 위치 바꾸기 */
		    /* 한글화 */
		    monthNames : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    monthNamesShort : ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    dayNames : ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesShort : ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesMin : ['일', '월', '화', '수', '목', '금', '토'],
		    showAnim: 'fadeIn',
		    onClose: function( selectedDate ) {
		    		$('#usdate').datepicker("option", "maxDate", selectedDate);
		    }
	    });
});
$(function() {
	$('.input-time').poshytip({
		className: 'tip-twitter',
		showOn: 'focus',
		alignTo: 'target',
		alignX: 'center',
		alignY: 'bottom',
		offsetX: 0,
		offsetY: 10,				
		showTimeout: 1,
		//bgImageFrameSize: 15,
		allowTipHover: false,
		fade: true,
		slide: false
	});
});
</script>
