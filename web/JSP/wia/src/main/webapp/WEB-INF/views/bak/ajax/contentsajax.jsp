<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:out value="[ ['날짜', '체험시간', '컨텐츠명', '고객명'], " escapeXml="false"/>
<c:forEach var="data" items="${ data }">
	<c:out value="[ '${data.playDate }'," escapeXml="false"/>
	<c:out value=" '${data.time }'," escapeXml="false"/>
	<c:out value=" '${data.keyword }'," escapeXml="false"/>
	<c:out value=" '${data.customerName }' ], " escapeXml="false"/>
</c:forEach>
<c:out value=" ]"/>


<%-- 
<table>
	<tr>
		<td>날짜</td>
		<td>체험시각</td>
		<td>컨텐츠명</td>
		<td>고객명</td>
	</tr>
	<c:forEach var="list" items="${ data }">
	<tr>
		<td>${ list.playDate }</td>
		<td>${ list.time }</td>
		<td>${ list.keyword }</td>
		<td>${ list.customerName }</td>
	</tr>
	</c:forEach>
</table> --%>