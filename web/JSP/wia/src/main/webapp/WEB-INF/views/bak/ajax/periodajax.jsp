<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:out value="[ "/>
<c:out value="${ col }," escapeXml="false"/>
<c:forEach var="list" items="${ data }">
	<c:if test="${ period == 'year' }">
		<c:out value="[ '${ list.Period }년'," escapeXml="false"/>
	</c:if>
	<c:if test="${ period == 'month' }">
		<c:out value="[ '${ list.Period }월'," escapeXml="false"/>
	</c:if>
	<c:if test="${ period == 'quarter' }">
		<c:out value="[ '${ list.Period }분기'," escapeXml="false"/>
	</c:if>
	<c:if test="${ period == 'days' }">
		<c:out value="[ '${ list.Period }일'," escapeXml="false"/>
	</c:if>
	<c:forEach var="i" begin="1" end="${ colcnt }" step="1">
		<c:set var="tmp">col${ i }</c:set>
		<c:out value="${ list[tmp] },"/>
	</c:forEach>
	<c:out value=" ], "/>	
</c:forEach>
<c:out value=" ]"/>