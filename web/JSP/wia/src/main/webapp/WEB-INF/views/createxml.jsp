<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "org.jdom2.*, org.jdom2.output.*, java.io.*, java.util.List;" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String sdate_check = request.getParameter("sdate_check");
String fdate_check = request.getParameter("fdate_check");

if( (sdate_check == null || fdate_check == null) ) {
	response.sendRedirect("welcome.do");
}
else if( sdate_check != null && fdate_check != null) {
	if( fdate_check.equals(sdate_check) ) {
		response.sendRedirect("welcome.do");
	}
	else {
		response.sendRedirect("wcbSearch.do?sdate_check=" + sdate_check + "&fdate_check=" + fdate_check);
	}
}
%>