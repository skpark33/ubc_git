<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%
	request.setCharacterEncoding("utf-8");
	if(session.getAttribute("userinfo") == null) {
		response.sendRedirect("index.do");
	}
	
	/* Cache remove */
	response.setHeader("Pragma","no-cache"); 
	response.setHeader("Cache-Control","no-store"); 
	response.setDateHeader("Expires",0);
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<jsp:include page="/WEB-INF/views/include/declare.jsp"></jsp:include>
	<script type="text/javascript">
		google.load("visualization", "1", {packages:['corechart', 'table']});
		$(document).ready(function() {
			$("#org-ex, #menu1, #menu2, #menu3").hide();
		});
		function org_sel_menu2(val) {
			document.forms[0].menu2.selectedIndex=0;
			document.forms[0].menu3.selectedIndex=0;
			var menu = val.value;
			$.ajax({
				type: "POST",
				url: "orgSelect.do",
				data: "view=period&org=" + menu,
				success: function(html) {
					if(html == 0 ) {
						$("#menu2").hide();
						$("#menu3").hide();
					}
					else {
						$("#menu3").hide();
						$("#menu2").show();
						$("#menu2").html(html);
					}
				}
			});
		}
		function org_sel_menu3(val) {
			document.forms[0].menu3.selectedIndex=0;
			var menu = val.value;
			$.ajax({
				type: "POST",
				url: "orgSelect.do",
				data: "view=period&org=" + menu,
				success: function(html) {
					if(html == 0) {
						$("#menu3").hide();
					}
					else {
						$("#menu3").show();
						$("#menu3").html(html);
					}
				}
			});
		}
		function formSubmit() {
			var formData = $('#form-if').serialize();
			
			jQuery.ajaxSettings.traditional = true;
			$.ajax({
				type: 'POST',
				url: 'periodSubmit.do',
				data: formData,
				success: function(data) {
					drawChart(eval(data));
					google.setOnLoadCallback(drawChart);
				}
			});
		}
		function drawChart(formdata){
			var cdata = google.visualization.arrayToDataTable(formdata);
			
			var coption = {
					title: '기간별 체험 통계',
			};
			var toption = {
					width: '1050px',
					cssClassNames: { tableCell: 'tableCellClass'},
			};
			var col = new google.visualization.ColumnChart(document.getElementById('chartview'));
			col.draw(cdata, coption);
			
			var table = new google.visualization.Table(document.getElementById('tableview'));
			table.draw(cdata, toption);
			
			google.visualization.events.addListener(col, 'select', function() {
				table.setSelection(col.getSelection());
			});
			google.visualization.events.addListener(table, 'select', function() {
				col.setSelection(table.getSelection());
			});
		}
	</script>
</head>
<body>
<div id="wrap">
	<div id="topline"></div>
	<jsp:include page="/WEB-INF/views/include/top.jsp"></jsp:include>
	<%-- <jsp:include page="/WEB-INF/views/sidemenu.jsp"></jsp:include> --%>
	<div id="main">
		<div id="tabs">
			<jsp:include page="/WEB-INF/views/include/logo.jsp"></jsp:include>
			<div id="title">
				<img src="./resources/img/grid.png" width="20" height="20"/>
				<p>기간별 체험 통계</p>
				<button type="button" class="btn btn-1 btn-1a" onclick="formSubmit()">전송</button>
			</div>
			<form id="form-if" action="test.do" method="post">
				<div id="search">
					<table id="form-search">
						<tr>
							<td class="subj">기 간</td>
							<td class="detail">
								<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly> -
								<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly>
							</td>
							<td class="subj" id="org-ex">컨텐츠</td>
							<td class="detail">
								<select id="menu1" class="menu1" onchange="org_sel_menu2(this)">
									<option>전체</option>
								<c:forEach var="org" items="${ orglist }">
									<option value="${ org.name }">${ org.name }</option>
								</c:forEach>
								</select>
								<select id="menu2" class="menu3" onchange="org_sel_menu3(this)">
									<option>전체</option>
								</select>
								<select id="menu3" class="menu3">
									<option>전체</option>
								</select>
							</td>
						</tr>
						<tr class="second-row">
							<td class="subj">기간별 선택</td>
							<td class="detail" colspan="3">
								<input type="radio" name="period-kind" id="year" value="year"><label for="year" class="switch switch-y">년도 별</label>
								<input type="radio" name="period-kind" id="month" value="month" checked><label for="month" class="switch switch-m">월 별</label>
								<input type="radio" name="period-kind" id="quarter" value="quarter"><label for="quarter" class="switch switch-q">분기 별</label>
								<input type="radio" name="period-kind" id="days" value="days"><label for="days" class="switch switch-d">요일 별</label>
							</td>
						</tr>
					</table>
				</div>
			</form>
			<div id="chartview"></div>
			<div id="tableview"></div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/views/include/footer.jsp"></jsp:include>	
</div>

</body>
</html>


					<%-- <fieldset id="form-fieldset">
						<legend>검색 조건</legend>
						<div id="fieldset-if">
							<input type="text" name="sdate" id="sdate" class="form-date" size="13" readonly> -
							<input type="text" name="fdate" id="fdate" class="form-date" size="13" readonly>
							<select id="menu1" class="menu1" onchange="org_sel_menu2(this)">
								<option>전체</option>
							<c:forEach var="org" items="${ orglist }">
								<option value="${ org.name }">${ org.name }</option>
							</c:forEach>
							</select>
							<select id="menu2" class="menu3" onchange="org_sel_menu3(this)">
								<option>전체</option>
							</select>
							<select id="menu3" class="menu3">
								<option>전체</option>
							</select>
							<!-- <input type="button" id="submitbtn" name="submitbtn" value="전송" onclick="formSubmit()"/> -->
						</div>
					</fieldset>
				</div>
				<div id="search-p">
					<fieldset id="form-fieldset">
						<legend>기간별 검색</legend>
						<div id="fieldset-if" class="radio">
							<input type="radio" name="period-kind" id="year" value="year"><label for="year" class="switch switch-y">년도 별</label>
							<input type="radio" name="period-kind" id="month" value="month" checked><label for="month" class="switch switch-m">월 별</label>
							<input type="radio" name="period-kind" id="quarter" value="quarter"><label for="quarter" class="switch switch-q">분기 별</label>
							<input type="radio" name="period-kind" id="days" value="days"><label for="days" class="switch switch-d">요일 별</label>
						</div>
					</fieldset> --%>