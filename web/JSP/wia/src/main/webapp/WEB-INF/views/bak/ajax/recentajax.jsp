<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:out value="[ ['contents', 'view'], " escapeXml="false"/>
<c:forEach var="list" items="${ data }">
	<c:out value="[ '${ list.contents }', " escapeXml="false"/>
	<c:out value="${ list.point } ], "/>
</c:forEach>
<c:out value=" ]" />