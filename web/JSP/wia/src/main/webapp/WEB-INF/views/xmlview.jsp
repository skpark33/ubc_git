<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "org.jdom2.*, org.jdom2.output.*, java.io.*, java.util.List;" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
try {
	Document document = new Document();
	
	Element xml = new Element("xml");
%>
	<c:forEach var="list" items="${ msg }">
		<c:set var="cdate" value="${ list.date }"/>
		<c:set var="cstime" value="${ list.stime }"/>
		<c:set var="cftime" value="${ list.ftime }"/>
		<c:set var="cmsg" value="${ list.msg }"/>
		<c:set var="cfontname" value="${ list.fontname }"/>
		<c:set var="cfontsize" value="${ list.fontsize }"/>
		<c:set var="cfontcolor" value="${ list.fontcolor }"/>
			
		<%
			Element board = new Element("board");
			Element date = new Element("date");
			Element stime = new Element("stime");
			Element ftime = new Element("ftime");
			Element msg = new Element("msg");
			Element font = new Element("font");
			Element fontsize = new Element("fontsize");
			Element fontcolor = new Element("fontcolor");
		
			date.setText(pageContext.getAttribute("cdate", PageContext.PAGE_SCOPE).toString());
			stime.setText(pageContext.getAttribute("cstime", PageContext.PAGE_SCOPE).toString());
			ftime.setText(pageContext.getAttribute("cftime", PageContext.PAGE_SCOPE).toString());
			msg.setText(pageContext.getAttribute("cmsg", PageContext.PAGE_SCOPE).toString()); 
			font.setText(pageContext.getAttribute("cfontname", PageContext.PAGE_SCOPE).toString());
			fontsize.setText(pageContext.getAttribute("cfontsize", PageContext.PAGE_SCOPE).toString());
			fontcolor.setText(pageContext.getAttribute("cfontcolor", PageContext.PAGE_SCOPE).toString());
			
			board.addContent(date);
			board.addContent(stime);
			board.addContent(ftime);
			board.addContent(msg);
			board.addContent(font);
			board.addContent(fontsize);
			board.addContent(fontcolor);
			
			xml.addContent(board);
		%>		
	</c:forEach>
<%	
	document.setContent(xml);
	XMLOutputter output = new XMLOutputter();
	output.setFormat(Format.getPrettyFormat());

	out.println("OK");
	out.println(output.outputString(document));
	request.setAttribute("xml", output.outputString(document));
} catch(Exception e) {
	e.printStackTrace();
	e.getMessage();
}
%>