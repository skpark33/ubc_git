	<%@ page language="java" contentType="text/html; charset=UTF-8"
	    pageEncoding="UTF-8"%>
	<title>현대위아 - HYUNDAI WIA WelcomeBoard</title>
	<link rel="shortcut icon" href="./resources/img/favicon_H.ico" type="image/x-icon">
	<link rel="stylesheet" href="./resources/css/main.css">
	<link rel="stylesheet" href="./resources/css/jquery-ui-1.10.3.tabs.css">
	<link rel="stylesheet" href="./resources/css/childmain.css">
	<link rel="stylesheet" href="./resources/css/googlechart.css">
	<link rel="stylesheet" href="./resources/css/welcome.css">
	<link rel="stylesheet" href="./resources/css/tip-twitter.css">
	<link rel="stylesheet" href="./resources/css/jquery-ui-1.10.3.datepicker.css">
	<script src="./resources/js/jquery-1.9.1.js"></script>
	<script src="./resources/js/jquery-ui-1.10.3.datepicker.js"></script>
	<script src="./resources/js/jquery-ui-1.10.3.tabs.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="./resources/js/jquery.poshytip.min.js"></script>
	<script src="./resources/js/modernizr.custom.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script src="./resources/js/button.js"></script>
	