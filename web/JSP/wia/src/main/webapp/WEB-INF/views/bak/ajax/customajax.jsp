<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:out value="[  "/>
<c:out value="${ col }," escapeXml="false"/>
<c:forEach var="list" items="${ data }">
	<c:out value="[ '${ list.Customer }'," escapeXml="false"/>
	<c:forEach var="i" begin="1" end="${ colcnt }" step="1">
		<c:set var="tmp">col${ i }</c:set>
		<c:out value="${ list[tmp] },"/>
	</c:forEach>
	<c:out value=" ],"/>
</c:forEach>
<c:out value=" ]"/>