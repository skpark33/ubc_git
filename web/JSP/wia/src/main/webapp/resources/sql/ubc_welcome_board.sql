USE [ubc]
GO

/****** Object:  Table [dbo].[ubc_welcome_board]    Script Date: 04/21/2014 11:08:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ubc_welcome_board]') AND type in (N'U'))
DROP TABLE [dbo].[ubc_welcome_board]
GO

USE [ubc]
GO

/****** Object:  Table [dbo].[ubc_welcome_board]    Script Date: 04/21/2014 11:08:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ubc_welcome_board](
	[mid] [varchar](50) NOT NULL,
	[sdate] [date] NOT NULL,
	[stime] [time](0) NOT NULL,
	[fdate] [date] NOT NULL,
	[ftime] [time](0) NOT NULL,
	[filename] [varchar](255) NOT NULL,
	[filesize] [bigint] NULL,
	[fileMD5] [varchar](255) NULL,
	[registerTime] [datetime] NOT NULL,
	[userId] [varchar](50) NULL,
 CONSTRAINT [PK__welcom_b__DF5032EC14270015] PRIMARY KEY CLUSTERED 
(
	[mid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


