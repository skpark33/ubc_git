package com.sqisoft.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilterQuery {
	private static SqlSessionFactory sqlMapper = Mybatis.getInstance();
	private static String listsql = " (addr4 is not null and addr4 <> '')";	// 표시단말
	private static String nonlistsql = " ( addr4 is null or addr4 = '')";	// 미표시단말
	private static final Logger logger = LoggerFactory.getLogger(FilterQuery.class);
	
	/*** where절 쿼리 ***/
	// 마커위치 쿼리
	public static List<Map<String, Object>> markerFilter(String sql, String query) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map;
		if(query != null) {
			try {
				SqlSession session = sqlMapper.openSession();
				PreparedStatement psmt = null;
				if("null".equals(query) || query == "") {
					psmt = session.getConnection().prepareStatement(sql + listsql);
				}

				else if(query != null) {
					psmt = session.getConnection().prepareStatement(sql +  query);
				}
				ResultSet rs = psmt.executeQuery();
				
				ResultSetMetaData metaData = rs.getMetaData();
				int colCnt = metaData.getColumnCount();
				String colName;
				while(rs.next()) {
					map = new HashMap<String, Object>();
					for(int i=0; i<colCnt; i++) {
						colName = metaData.getColumnName(i+1);
						map.put(colName, rs.getString(colName));
					}
					list.add(map);
				}
				
			} catch(Exception e) {
				logger.warn("FilterQuery_MarkerFilter Exception= {}", e.toString());
				logger.warn("FilterQuery_MarkerFilter Exception= {}", e.getMessage());
				e.printStackTrace();
			}
		}
		return list;
	}
	
	// 표시단말 쿼리
	public static List<Map<String, Object>> listFilter(String sql, String query) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map;
		if(query != null) {
			try {
				SqlSession session = sqlMapper.openSession();
				PreparedStatement psmt = null;
				
				if("null".equals(query) || query == "") {
					psmt = session.getConnection().prepareStatement(sql + listsql);
				}
				else if( query != null) {
					psmt = session.getConnection().prepareStatement(sql + query + " and " + listsql);
				}
				ResultSet rs = psmt.executeQuery();
				
				ResultSetMetaData metaData = rs.getMetaData();
				int colCnt = metaData.getColumnCount();
				String colName;
				while(rs.next()) {
					map = new HashMap<String, Object>();
					for(int i=0; i<colCnt; i++) {
						colName = metaData.getColumnName(i+1);
						map.put(colName, rs.getString(colName));
					}
					list.add(map);
				}
				
			} catch(Exception e) {
				logger.warn("FilterQuery_listFilter Exception= {}", e.toString());
				logger.warn("FilterQuery_listFilter Exception= {}", e.getMessage());
				e.printStackTrace();
			}
		}
		return list;
	}
	
	// 미표시단말 쿼리
	public static List<Map<String, Object>> nonlistFilter(String sql, String query) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map;
		if(query != null) {
			try {
				SqlSession session = sqlMapper.openSession();
				PreparedStatement psmt = null;
				
				if( "null".equals(query) || query == "") {
					psmt = session.getConnection().prepareStatement(sql +  nonlistsql);
				}
				else if(query != null) {
					psmt = session.getConnection().prepareStatement(sql + query + " and " + nonlistsql);
				}
				ResultSet rs = psmt.executeQuery();
				
				ResultSetMetaData metaData = rs.getMetaData();
				int colCnt = metaData.getColumnCount();
				String colName;
				while(rs.next()) {
					map = new HashMap<String, Object>();
					for(int i=0; i<colCnt; i++) {
						colName = metaData.getColumnName(i+1);
						map.put(colName, rs.getString(colName));
					}
					list.add(map);
				}
				
			} catch(Exception e) {
				logger.warn("FilterQuery_nonlist Exception= {}", e.toString());
				logger.warn("FilterQuery_nonlist Exception= {}", e.getMessage());
				e.printStackTrace();
			}
		}
		return list;
	}
}
