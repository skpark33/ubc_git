package com.sqisoft.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class mapDAOImpl implements mapDAO {

	@Autowired
	private SqlSession sqlSession;
		
	/*@Override
	public List<String> hostinfo() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.sqisoft.mapper.host.hostinfo");
	}
	
	@Override
	public List<String> hostlist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.sqisoft.mapper.host.hostlist");
	}

	@Override
	public List<String> mhostlist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("com.sqisoft.mapper.host.mhostlist");
	}*/

	@Override
	public void saveall(HashMap map) {
		// TODO Auto-generated method stub
		sqlSession.update("com.sqisoft.mapper.host.saveall", map);
	}

	@Override
	public void updateaddr(HashMap map) {
		// TODO Auto-generated method stub
		sqlSession.update("com.sqisoft.mapper.host.updateaddr", map);
	}

	@Override
	public void delete(String hostId) {
		// TODO Auto-generated method stub
		sqlSession.update("com.sqisoft.mapper.host.delete", hostId);
	}


	/*@Override
	public void addSubmit(HashMap map) {
		// TODO Auto-generated method stub
		sqlSession.update("com.sqisoft.mapper.host.addSubmit", map);
	}*/
}
