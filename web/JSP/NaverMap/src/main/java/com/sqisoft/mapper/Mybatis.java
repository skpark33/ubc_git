package com.sqisoft.mapper;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class Mybatis {
	public static SqlSessionFactory sqlMapper = null;
	
	public static SqlSessionFactory getInstance() {
		if(sqlMapper == null ) {
			try {
				String resource = "mybatis-config.xml";
				InputStream is = Resources.getResourceAsStream(resource);
				sqlMapper = new SqlSessionFactoryBuilder().build(is);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return sqlMapper;
	}
}
