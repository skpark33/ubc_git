package com.sqisoft.controller;

import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import com.sqisoft.dao.mapDAO;
import com.sqisoft.mapper.FilterQuery;
/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	public static String list_sql = "select hostId, hostName from ubc_host where ";
	public static String marker_sql = "select hostId, hostName, hostType, operationalState, addr3, addr4 from ubc_host where ";
	
	@Autowired
	private mapDAO mapdao;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	// 초기 mode에 따른 페이지 연결
	@RequestMapping("/index.do")
	public String index(Model model, HttpServletRequest req) {
		String mode = req.getParameter("mode");
		String query = req.getParameter("query");
		
		logger.info("query= {}", query);
		
		if("null".equals(mode) || mode == "") {
			logger.info("nullmode= {}", mode);
			model.addAttribute("query", query);
			return "navermap";
		}
		else if("edit".equals(mode)) {
			// 편집모드 call
			logger.info("mode= {}", mode);
			logger.info("Naver Map Edit Mode");
			List<Map<String, Object>> list = FilterQuery.nonlistFilter(list_sql, query);
			List<Map<String, Object>> mlist = FilterQuery.listFilter(list_sql, query);
			
			model.addAttribute("mlist", mlist);
			model.addAttribute("list", list);
			model.addAttribute("query", query);
			return "edit";
		}
		else if("check".equals(mode)) {
			// 조회모드 Call
			logger.info("mode= {}", mode);
			logger.info("Naver Map Check Mode");
			
			model.addAttribute("query", query);
			return "navermap";
		}
		else {
			// 초기 index Call  * UBC에서 호출될때는 이 조건을 타지 않음.
			logger.info("indexmode= {}", mode);
			model.addAttribute("query", query);
			return "navermap";
		}
	}
	
	// 조회모드 맵 Ajax
	@RequestMapping("/defaultMap.do")
	public String defaultMap(HttpServletRequest req, Model model) {
		String lat = req.getParameter("lat");
		String lon = req.getParameter("lon");
		String query = req.getParameter("query");
		
		List<Map<String, Object>> listinfo = FilterQuery.markerFilter(marker_sql, query);
		
		model.addAttribute("list", listinfo);
		model.addAttribute("lat", lat);
		model.addAttribute("lon", lon);
		return "ajax/defaultmap";                                                                                                                                  
	}
	
	// 편집모드 맵 Ajax
	@RequestMapping("/editMap.do")
	public String editMap(HttpServletRequest req, Model model) {
		String lat = req.getParameter("lat");
		String lon = req.getParameter("lon");
		String query = req.getParameter("query");
		
		List<Map<String, Object>> listinfo = FilterQuery.markerFilter(marker_sql, query);
		
		model.addAttribute("list", listinfo);
		model.addAttribute("lat", lat);
		model.addAttribute("lon", lon);
		model.addAttribute("query", query);
		return "ajax/editmap";
	}
	
	// 맵 이동
	@RequestMapping("/posxy.do")
	public String posxy(HttpServletRequest req, Model model) {
		String lat = "";
		String lon = "";
		String address = "";
		String addr = req.getParameter("addr");
		logger.info("Insert addr Data= {}", addr);
		addr = addr.replace(" ", "");
		InputStreamReader isr = null;
		
		try {
			String server = "http://openapi.map.naver.com/api/geocode.php?key=fe9884e879146a76110cb3266df01272&encoding=utf-8&coord=latlng&query=" + addr;
			logger.info("receive Server Url= {}", server);
			URL url = new URL(server);
			isr = new InputStreamReader(url.openStream());
			logger.info("Open Stream--");

			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = db.parse(new InputSource(isr));
			doc.getDocumentElement().normalize();

			logger.info("Document parse--");
			
			NodeList x = doc.getElementsByTagName("x");
			NodeList y = doc.getElementsByTagName("y");
			NodeList addr_tmp = doc.getElementsByTagName("address");
			
			lat = y.item(0).getFirstChild().getNodeValue();
			lon = x.item(0).getFirstChild().getNodeValue();
			address = addr_tmp.item(0).getFirstChild().getNodeValue();
			
			isr.close();
			
		} catch(Exception e) {
			logger.warn("Naver Server URL Exception= {}", e.toString());
			logger.warn("Naver Server URL Exception= {}", e.getMessage());
			e.printStackTrace();
		}
		
		logger.info("return Latitude= {} ", lat);
		logger.info("return Longitude= {} ", lon);
		logger.info("return Address= {} ", address);
		model.addAttribute("lat", lat);
		model.addAttribute("lon", lon);
		model.addAttribute("address", address);
		return "ajax/LatLng";
	}
	
	// 신규 마커 맵에 추가	*마우스 추가로 인해 사용안함
	@RequestMapping("/regi.do")
	public String regi(HttpServletRequest req, Model model) {
		String []arg = req.getParameterValues("chk-list");
		String center = req.getParameter("center");
		
		model.addAttribute("lat", center.split(",")[1]);
		model.addAttribute("lon", center.split(",")[0]);
		model.addAttribute("hostlist", arg);
		
		return "ajax/Addmarker";
	}
	
	//신규 마커 및 기존 마커 전체 좌표 DB 저장
	@RequestMapping("/saveall.do") 
	public String saveall(HttpServletRequest req, Model model) {
		HashMap map = new HashMap();
		String query = req.getParameter("query");
		String pos = req.getParameter("pos");
		String datalist = req.getParameter("data");
		String []add = datalist.split(",");
		String div[][] = new String[add.length][2];	// 분할된 데이터
		for(int i=0; i<add.length; i++) {
			for(int j=0; j<2; j++) {
				div[i][j] = add[i].split("//")[j].replace("^", ",");		// 좌표값 나누기
			}
		}
		for(int i=0; i<add.length; i++) {
			for(int j=0; j<2; j++) {
				map.put("hostId", div[i][0]);
				map.put("pos", div[i][1]);
				mapdao.saveall(map);
				logger.info("{} Marker Add & Location Update");
			}
		}
		logger.info("Marker Save All");
		
		List<Map<String, Object>> list = FilterQuery.nonlistFilter(list_sql, query);
		List<Map<String, Object>> mlist = FilterQuery.listFilter(list_sql, query);
		
		model.addAttribute("mlist", mlist);
		model.addAttribute("list", list);
		model.addAttribute("lat", pos.split(",")[0]);
		model.addAttribute("lon", pos.split(",")[1]);
		model.addAttribute("query", query);
		return "edit";
	}
	
	// 마우스 우클릭으로 단말 등록
	@RequestMapping("/clickAdd.do")
	public String clickAdd(HttpServletRequest req, Model model) {
		String pos = req.getParameter("pos");
		String query = req.getParameter("query");
		String []arg = req.getParameterValues("chk-list");
		logger.info("query= {}", query);
		HashMap map = new HashMap();
		map.put("pos", pos);
		
		for(int i=0; i<arg.length; i++) {
			map.put("hostId", arg[i]);
			mapdao.saveall(map);
			logger.info("{} Marker Add", arg[i]);
		}
		logger.info("Marker Save");
		
		List<Map<String, Object>> list = FilterQuery.nonlistFilter(list_sql, query);
		List<Map<String, Object>> mlist = FilterQuery.listFilter(list_sql, query);
		
		model.addAttribute("mlist", mlist);
		model.addAttribute("list", list);
		model.addAttribute("lat", pos.split(",")[0]);
		model.addAttribute("lon", pos.split(",")[1]);
		model.addAttribute("query", query);
		
		return "edit";
	}
	
	// 단일 단말 위치좌표 수정
	@RequestMapping("/updatehost.do")
	public String updatehost(HttpServletRequest req, Model model) {
		String hostId = req.getParameter("hostId");
		String pos = req.getParameter("pos");
		String query = req.getParameter("query");
		//String []xy = pos.split(",");
		
		HashMap map = new HashMap();
		map.put("hostId", hostId);
		map.put("pos", pos);
		
		mapdao.saveall(map);
		logger.info("{} Marker Save", hostId);
		
		List<Map<String, Object>> list = FilterQuery.nonlistFilter(list_sql, query);
		List<Map<String, Object>> mlist = FilterQuery.listFilter(list_sql, query);
		
		model.addAttribute("mlist", mlist);
		model.addAttribute("list", list);
		model.addAttribute("lat", pos.split(",")[0]);
		model.addAttribute("lon", pos.split(",")[1]);
		model.addAttribute("query", query);
		
		return "edit";
	}
	
	// 좌표 NULL 처리 즉, 마커제거
	@RequestMapping("/deletehost.do")
	public String deletehost(HttpServletRequest req, Model model) {
		String hostId = req.getParameter("hostId");
		String pos = req.getParameter("pos");
		String query = req.getParameter("query");
		
		mapdao.delete(hostId);
		logger.info("{} Marker delete", hostId);
		
		List<Map<String, Object>> list = FilterQuery.nonlistFilter(list_sql, query);
		List<Map<String, Object>> mlist = FilterQuery.listFilter(list_sql, query);
		
		model.addAttribute("mlist", mlist);
		model.addAttribute("list", list);
		model.addAttribute("lat", pos.split(",")[0]);
		model.addAttribute("lon", pos.split(",")[1]);
		model.addAttribute("query", query);
		
		return "edit";
	}
	
	// 선택마커 제거
	@RequestMapping("/deleteall.do")
	public String deleteall(HttpServletRequest req, Model model) {
		String []arg = req.getParameterValues("mchk-list");
		String query = req.getParameter("query");
		
		for(int i=0; i<arg.length; i++) {
			mapdao.delete(arg[i]);
			logger.info("{} Marker delete", arg[i]);
		}
		
		List<Map<String, Object>> list = FilterQuery.nonlistFilter(list_sql, query);
		List<Map<String, Object>> mlist = FilterQuery.listFilter(list_sql, query);
		
		model.addAttribute("mlist", mlist);
		model.addAttribute("list", list);
		model.addAttribute("query", query);
		return "edit";
	}
	
	// 위치 초기화
	@RequestMapping("/reset.do")
	public String reset(HttpServletRequest req, Model model) {
		String pos = req.getParameter("pos");
		String query = req.getParameter("query");
		
		List<Map<String, Object>> list = FilterQuery.nonlistFilter(list_sql, query);
		List<Map<String, Object>> mlist = FilterQuery.listFilter(list_sql, query);
		
		model.addAttribute("mlist", mlist);
		model.addAttribute("list", list);
		model.addAttribute("lat", pos.split(",")[0]);
		model.addAttribute("lon", pos.split(",")[1]);
		model.addAttribute("query", query);
		
		logger.info("Marker Position Reset");
		return "edit";
	}
	// 주소정보 수정
	@RequestMapping("/updateAddr.do")
	public String updateAddr(HttpServletRequest req, Model model) {
		String hostId = req.getParameter("hostId");
		String addr3 = req.getParameter("addr3");
		String query = req.getParameter("query");
		
		HashMap map = new HashMap();
		map.put("hostId", hostId);
		map.put("addr", addr3);
		
		mapdao.updateaddr(map);
		logger.info("{} Marker Update at Address", hostId);
		
		List<Map<String, Object>> list = FilterQuery.nonlistFilter(list_sql, query);
		List<Map<String, Object>> mlist = FilterQuery.listFilter(list_sql, query);
		
		model.addAttribute("mlist", mlist);
		model.addAttribute("list", list);
		model.addAttribute("query", query);
		
		return "edit";
	}
	
}

/*@RequestMapping("/addSubmit.do") 
	public String addSubmit(HttpServletRequest req, Model model) {
		String hostId = req.getParameter("hostId");
		String addr = req.getParameter("addr");
		String lat = req.getParameter("lat");
		String lon = req.getParameter("lon");
		
		HashMap map = new HashMap();
		map.put("hostId", hostId);
		map.put("addr", addr);
		map.put("pos", lat+","+lon);
		
		mapdao.addSubmit(map);
		
		return "redirect:/index.do";
	}*/


/*@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}*/

/*SAXParser sax = SAXParserFactory.newInstance().newSAXParser();
DefaultHandler dh = new DefaultHandler() {
	private boolean bool_x = false;
	private boolean bool_y = false;
	private boolean bool_addr = false;
	private StringBuilder sb;
	
	@Override
	public void startElement(String uri, String localName,
			String qName, Attributes attributes)
					throws SAXException {
		// TODO Auto-generated method stub
		if(qName.equals("x")) {  bool_x = true; }
		else if(qName.equals("y")) { 	bool_y = true; }
		else if(qName.equals("address")) { 	bool_addr = true; }
		
		if(bool_x && bool_y && bool_addr) {
			sb = new StringBuilder();
			logger.info("sb data= {}", sb.toString());
		}
		
		super.startElement(uri, localName, qName, attributes);
	}
	
	@Override
	public void endElement(String uri, String localName,
			String qName) throws SAXException {
		// TODO Auto-generated method stub
		if(sb != null) {
			if(bool_x) { logger.info("sax xml parser X= {}", sb.toString()); }
			else if(bool_y) { logger.info("sax xml parser y= {}", sb.toString()); }
			else if(bool_addr) { logger.info("sax xml parser addr= {}", sb.toString()); }
			sb = null;
		}
		
		if (qName.equals("x")) { bool_x = false; }
		if (qName.equals("y")) { bool_y = false; }
		if (qName.equals("address")) { bool_addr = false; }
		
		super.endElement(uri, localName, qName);
	}
	
	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		super.endDocument();
	}
	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		super.startDocument();
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		if(sb!=null) {
			sb.append(handleCharacters(ch, start, length));
		}
		super.characters(ch, start, length);
	}

	private String handleCharacters(char[] ch, int start,
			int length) {
		// TODO Auto-generated method stub
		StringBuilder sb2 = new StringBuilder();
		for(int i=start; i<start+length; i++) {
			sb2.append(ch[i]);
		}
		return sb2.toString();
	}
	
};
sax.parse(new InputSource(isr), dh);*/