<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.io.*, java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<jsp:include page="/WEB-INF/views/include/include.jsp"></jsp:include>
<script>
	$(document).ready(function() {
		/* if(navigator.geolocation) {
			//navigator.geolocation.getCurrentPosition(nowLocation);
			navigator.geolocation.getCurrentPosition(
					function nowLocation(position) {
						var lat = position.coords.latitude;
						var lon = position.coords.longitude;
						
						$.ajax({
							type: "POST",
							url: "defaultMap.do",
							data: "lat=" + lat + "&lon=" + lon,
							success: function(data) {
								$('#mapview').html(data);
							}
						});
					},
					function(error) {
						alert("브라우저의 위치추적을 허용하지 않으셨습니다. 기본좌표로 이동합니다.");
						var lat = 37.5327619;
						var lon = 127.0139427;
						
						$.ajax({
							type: "POST",
							url: "defaultMap.do",
							data: "lat=" + lat + "&lon=" + lon,
							success: function(data) {
								$('#mapview').html(data);
							}
						});		
					}
			);
		}	 */
		//else {
			//alert("Your Browser don't support for Geolocation");
			var lat = 37.5327619;
			var lon = 127.0139427;
			
			$.ajax({
				type: "POST",
				url: "defaultMap.do",
				data: { "lat": lat, "lon": lon, "query": "${query}" },
				success: function(data) {
					$('#mapview').html(data);
				}
			});		
		//}
	});
	
</script>
</head>
<body>
<div id="wrap">
	<div id="def-controller">
		<div id="btn-top" class="def-top">
			<input type="hidden" name="query" id="query" value="${query }" />
			<button type="button" class="toggle-btn btn btn-1 btn-1a" id="def-btn" onclick="defMode()">조회모드</button>
			<button type="button" class="toggle-btn btn btn-1 btn-1a" id="edit-btn" onclick="editMode()">편집모드</button>
			<img src="./resources/img/NAVER.png" alt="powered by NAVER" width="100px"/>
		</div>
	</div>
	<div id="mapview" class="def-mapview"></div>
</div>
	
</body>
</html>