<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <div id="map"></div>
	<script type="text/javascript">
		nhn.api.map.setEasyConvention('ubc');
		var data = {};	// 마커객체정보를 저장하기 위한 객체 리터럴.
		
		$(window).bind('resize', function(e) {
			window.resizeEvt;
			$(window).resize(function() {
				clearTimeout(window.resizeEvt);
				window.resizeEvt = setTimeout(function() {
					oMap.setSize(new ubcSize(document.documentElement.clientWidth, document.documentElement.clientHeight));				
				}, 250);
			});
		}).resize(); 
	
		var oPoint = new ubcLatLng( '${lat}', '${lon}');
		ubcsetDefaultPoint('LatLng');
		var oMap = new ubcMap(document.getElementById('map'), {
			point: oPoint,
			zoom: 9,
			mapMode : 0,
			size: new ubcSize(document.documentElement.clientWidth, document.documentElement.clientHeight)
		});
		
		var oZoom = new ubcZoomControl();
		oMap.addControl(oZoom);
		oZoom.setPosition({
			top: 90,
			left: 15
		});
		
		var oMapType = new ubcMapTypeBtn();
		oMap.addControl(oMapType);
		oMapType.setPosition({
			top: 15,
			left: 15
		});
		
		var oTraffic = new ubcTrafficMapBtn();
		oMap.addControl(oTraffic);
		oTraffic.setPosition({
			top: 15,
			left: 90
		});
		
		var oInfo = new ubcInfoWindow();
		oInfo.setVisible(false);
		oMap.addOverlay(oInfo);
		
		var oLabel = new ubcMarkerLabel(); // - 마커 라벨 선언
	      oMap.addOverlay(oLabel); // - 마커 라벨을 지도에 추가한다. 기본은 라벨이 보이지 않는 상태로 추가됨.
		
	      oMap.attach("mouseenter", function(oEvent){
		      var oTarget = oEvent.target;
		      if(oTarget instanceof ubcDraggableMarker){
		              oLabel.setVisible(true, oTarget); // - 특정 마커를 지정하여 해당 마커의 title을 보여준다.
		      }
		});

		oMap.attach("mouseleave", function(oEvent) {
		      var oTarget = oEvent.target;
		      if(oTarget instanceof ubcDraggableMarker){
		              oLabel.setVisible(false);
		      }
		});
		
		 
		var defaultSpriteIcon = {
	      		url:'./resources/img/on_off.png',
	                  size:{width:40, height:32},
	                  spriteSize:{width:80, height:40},
	                  imgOrder:0, 
	                  offset : {width: 20, height: 32}
		};
		var i = 0;
		<c:forEach var="list" items="${hostlist}">
			var SpriteIcon = new ubcSpriteIcon(
				defaultSpriteIcon.url,	// hostType  
			      defaultSpriteIcon.size, 
				defaultSpriteIcon.spriteSize,
				0, 	// Sprite image index == operationalState value
			      defaultSpriteIcon.offset);
			
			var oMarker = new ubcDraggableMarker(SpriteIcon , {
				title : '${list}',
				point : new ubcLatLng('${lat}', '${lon}'),
				zIndex : 1,
				smallSrc: SpriteIcon});
			
			oMap.addOverlay(oMarker);
			data[i] = {title: '${list}', lat: '${lat}', lon: '${lon}'};
			i++;
		</c:forEach>
		
		// 마커 드래그시 이벤트
		oMap.attach("changePosition", function(moveEvt) {
			oTarget = moveEvt.target;
			for(j in data) {
				if(data[j].title === oTarget.getTitle()) {
					data[j].lat = moveEvt.newPoint.getLat();
					data[j].lon = moveEvt.newPoint.getLng();
					//console.log(data[j].lat + ", " + data[j].lon);
				}
			}
			//console.log(oTarget.getTitle() +"/ new : " + moveEvt.newPoint + ", old : " + moveEvt.oldPoint);
		});
		
		function resetMarker() {
			oMap.clearOverlay();
		}
		
		/* 우클릭 이벤트 */
		//function rClickEvt() {
		oMap.attach('contextmenu', function(oEvt) {
			var lat = oEvt.point.getLat();
			var lon = oEvt.point.getLng();
			var oTarget = oEvt.target;
			if(oTarget instanceof ubcMap) {
				oInfo.setContent('<div id="rClick"><div id="rcLayout">' +
		            		'<table id="rc-table"><tr id="rc-list"><td class="td-btn"><button type="button" id="save-btn" class="btn btn-3 btn-3a" onclick="saveall()">단말위치 모두 저장</button></td>' +
		            		'<tr id="rc-list"><td class="td-btn"><button type="button" id="reset-btn" class="btn btn-3 btn-3a" onclick="resetMarker()">위치 원래대로</button></td>' +
		            		'</div></div>');
				oInfo.setPoint(new ubcLatLng(lat, lon));
				oInfo.setVisible(true);
				oInfo.autoPosition();
			}
		});
			
		oMap.attach('click', function(oEvt) {
			oInfo.setVisible(false);
		});	
		//}
		//rClickEvt();
		
		function saveall() {
			var arr = new Array();
			for(k in data) {
				arr[k] = data[k].title+"//"+data[k].lat+"^"+data[k].lon;
			}
			location.href="saveall.do?data=" + arr;
			
		}
		
		/* var oSize = new ubcSize(28, 37);
            var oOffset = new ubcSize(14, 37);
            var oIcon = new ubcIcon('http://static.naver.com/maps2/icons/pin_spot2.png', oSize, oOffset);
            
            var oMarker = new ubcMarker(oIcon, { title : '마커 : ' + oPoint.toString() });
            oMarker.setPoint(oPoint);
            oMap.addOverlay(oMarker);
            
            oMap.attach('click', function(oClickEvent) {
            	var oTarget = oClickEvent.target;
            	oInfo.setVisible(false);
            	
            	if(oTarget instanceof ubcMarker) {
            		oInfo.setContent('<div id="frame"><div id="innerLayout">' +
            		'<table id="info-table"><tr id="header-1"><td class="header">단말선택</td><td colspan="2" class="content"><select name="host" id="host">' +
            		'<c:forEach var="data" items="${host}">'+
            			'<option value="${data.hostId}">${data.hostName} - (${data.hostId})</option>'+
            		'</c:forEach>'+
            		'</select></td></tr>' +
            		'<tr><td colspan="3" class="td-line"></td>' + 
            		'<tr><td colspan="3" class="td-blank"></td>' + 
            		'<tr id="header-2"><td class="header">주소</td><td colspan="2" class="content">${address}</td></tr>' +
            		'<tr id="header-3"><td class="header">좌표</td><td colspan="2" class="content">${lat} , ${lon}</td></tr>' + 
            		'<tr><td colspan="3" class="td-line-2"></td>' + 
            		'<tr><td colspan="3" class="td-blank"></td>' + 
            		'<input type="hidden" id="addr" value="${address}"/><input type="hidden" id="lat" value="${lat}"/><input type="hidden" id="lon" value="${lon}"/>' +
            		'<tr id="header-btn"><td></td><td><button type="button" id="ok-btn" class="btn btn-2 btn-2a" onclick="addSubmit()">등록</button></td><td><button type="button" id="cancel" class="btn btn-2 btn-2a" onclick="cancel()">취소</button></td></tr></table>' +
            		'</div></div>');
            		oInfo.setPoint(oTarget.getPoint());
            		oInfo.setPosition({right: -200, top: 63});
            		oInfo.setVisible(true);
            		oInfo.autoPosition();
            		return;
            	}
            }); */
	</script>
	