<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="def-map"></div>
	<script type="text/javascript">
		nhn.api.map.setEasyConvention('ubc');
		document.oncontextmenu = new Function('return false');	// 우클릭 방지
		var type = ['설정안함', 'DID', 'Kiosk', 'Beam Projector', 'Multi-Screen', 'PDP', 'Etc'];
		// 브라우저 크기에 맞게 맵 자동조정
		$(window).bind('resize', function(e) {
			window.resizeEvt;
			$(window).resize(function() {
				clearTimeout(window.resizeEvt);
				window.resizeEvt = setTimeout(function() {
					oMap.setSize(new ubcSize(document.documentElement.clientWidth, document.documentElement.clientHeight));				
				}, 250);
			});
		}).resize(); 
		
		// Naver map Draw
		var oPoint = new ubcLatLng( '${lat}', '${lon}');
		ubcsetDefaultPoint('LatLng');
		var oMap = new ubcMap(document.getElementById('def-map'), {
			point: oPoint,
			zoom: 8,
			enableWheelZoom: true,
			enableDragPan: true,
			mapMode : 0,
			size: new ubcSize(document.documentElement.clientWidth, document.documentElement.clientHeight)
		});
		
		var oZoom = new ubcZoomControl();
		oMap.addControl(oZoom);
		oZoom.setPosition({
			top: 90,
			left: 15
		});
		
		var oMapType = new ubcMapTypeBtn();
		oMap.addControl(oMapType);
		oMapType.setPosition({
			top: 15,
			left: 15
		});
		
		var oTraffic = new ubcTrafficMapBtn();
		oMap.addControl(oTraffic);
		oTraffic.setPosition({
			top: 15,
			left: 90
		});
		
		var oLabel = new ubcMarkerLabel();
		oMap.addOverlay(oLabel);
		
		oMap.attach("mouseenter", function(oEvent){
			      var oTarget = oEvent.target;
			      if(oTarget instanceof ubcSpriteMarker){
			              oLabel.setVisible(true, oTarget); // - 특정 마커를 지정하여 해당 마커의 title을 보여준다.
			      }
			});

		oMap.attach("mouseleave", function(oEvent) {
		      var oTarget = oEvent.target;
		      if(oTarget instanceof ubcSpriteMarker){
		              oLabel.setVisible(false);
		      }
		});
		
		// Sprite Icon 정보설정
		var defaultSpriteIcon = {
	      		url:'./resources/img/on_off.png',
	                  size:{width:40, height:32},
	                  spriteSize:{width:80, height:40},
	                  imgOrder:0, 
	                  offset : {width: 20, height: 32}
		};

		<c:forEach var="list" items="${list}">
			var SpriteIcon = new ubcSpriteIcon(
					defaultSpriteIcon.url,	// hostType  
			            defaultSpriteIcon.size, 
			            defaultSpriteIcon.spriteSize,
			            Number('${list.operationalState}'), 	// Sprite image index == operationalState value
			            defaultSpriteIcon.offset);
			
			var oMarker = new ubcSpriteMarker(SpriteIcon , {
				title : '${list.hostId} (${list.hostName})',
				point : new ubcLatLng('${fn:split(list.addr4,",")[0]}', '${fn:split(list.addr4,",")[1]}'),
				zIndex : 1,
				smallSrc: SpriteIcon});
			
			oMap.addOverlay(oMarker);
		</c:forEach>
		
		var oInfo = new ubcInfoWindow();
		oInfo.setVisible(false);
		oMap.addOverlay(oInfo);
		
		oMap.attach('click', function(ClickEvt) {
	            var oTarget = ClickEvt.target;
	            oInfo.setVisible(false);
	            
	            if(oTarget instanceof ubcSpriteMarker) {
	            	<c:forEach var="list" items="${list}">
	            		if( oTarget.getTitle().split(" ")[0] === '${list.hostId}') {
	            			var address;
	            			var typeNum = Number('${list.hostType}');
	            			if('${list.addr3}' === null || '${list.addr3}' === '') { address = '주소가 입력되지 않았습니다.';}
	            			else { address = '${list.addr3}';}
	            			
			            	oInfo.setContent('<div id="frame"><div id="innerLayout">' +
			            	'<table id="info-table"><tr id="header-h"><td colspan="2" class="header">${list.hostName} / ${list.hostId}</td></tr>' +
			            	'<tr><td colspan="2" class="td-line"></td>' + 
			            	'<tr><td colspan="2" class="td-blank"></td>' + 
			            	'<tr id="header-f"><td class="header head-sub">주소</td><td class="content content-def">' + address + '</td></tr>' +
			            	'<tr id="header-m"><td class="header head-sub">타입</td><td class="content content-def">' + type[typeNum] + '</td></tr>' + 
			            	'<tr id="header-l"><td class="header head-sub">좌표</td><td class="content content-def">${lat} , ${lon}</td></tr>' + 
			            	'<tr><td colspan="2" class="td-line-2"></td>' + 
			            	'<tr><td colspan="2" class="td-blank"></td>' + 
			            	'<input type="hidden" id="addr" value="${address}"/><input type="hidden" id="lat" value="${lat}"/><input type="hidden" id="lon" value="${lon}"/>' +
			            	'<tr id="header-btn"><td colspan="2" class="content"><button type="button" id="cancel" class="btn btn-2 btn-2a" onclick="cancel()">닫기</button></td></tr></table>' +
			            	'</div></div>');
			            	oInfo.setPoint(oTarget.getPoint());
			            	oInfo.setPosition({right: -200, top: 50});
			            	oInfo.setOpacity(0.9);
			            	oInfo.setVisible(true);
			            	oInfo.autoPosition();
			            	return;
	            		}
	            	</c:forEach>
	            }
	      }); 
	</script>
	