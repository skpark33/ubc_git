<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="map"></div>
	<script type="text/javascript">
		nhn.api.map.setEasyConvention('ubc');
		document.oncontextmenu = new Function('return false');	// 우클릭 방지
		var type = ['설정안함', 'DID', 'Kiosk', 'Beam Projector', 'Multi-Screen', 'PDP', 'Etc'];
		var id;
		var data = {};	// 전체 마커에 대한 리터럴 객체
		var mdata = {};	// 움직인 마커에 대한 리터럴 객체
		
		//  Browser-size Auto fit
		$(window).bind('resize', function(e) {
			window.resizeEvt;
			$(window).resize(function() {
				clearTimeout(window.resizeEvt);
				window.resizeEvt = setTimeout(function() {
					oMap.setSize(new ubcSize(document.documentElement.clientWidth, document.documentElement.clientHeight));				
				}, 250);
			});
		}).resize(); 
		
		// Navermap draw
		var oPoint = new ubcLatLng( '${lat}', '${lon}');
		ubcsetDefaultPoint('LatLng');
		var oMap = new ubcMap(document.getElementById('map'), {
			point: oPoint,
			zoom: 8,
			mapMode : 0,
			size: new ubcSize(document.documentElement.clientWidth, document.documentElement.clientHeight)
		});

/*** Naver Map option ***/

		// ZOOM
		var oZoom = new ubcZoomControl();
		oMap.addControl(oZoom);
		oZoom.setPosition({
			top: 90,
			left: 15
		});
		
		// 일반 / 위성지도 
		var oMapType = new ubcMapTypeBtn();
		oMap.addControl(oMapType);
		oMapType.setPosition({
			top: 15,
			left: 15
		});
		
		// 실시간교통정보
		var oTraffic = new ubcTrafficMapBtn();
		oMap.addControl(oTraffic);
		oTraffic.setPosition({
			top: 15,
			left: 90
		});
		
		var oLabel = new ubcMarkerLabel(); // - 마커 라벨 선언
	      oMap.addOverlay(oLabel); // - 마커 라벨을 지도에 추가한다. 기본은 라벨이 보이지 않는 상태로 추가됨.
	      
		var defaultSpriteIcon = {
	      		url:'./resources/img/on_off.png',
	                  size:{width:40, height:32},
	                  spriteSize:{width:80, height:40},
	                  imgOrder:0, 
	                  offset : {width: 20, height: 32}
		};
		
		var i = 0;
	<c:forEach var="list" items="${list}">
		var SpriteIcon = new ubcSpriteIcon(
				defaultSpriteIcon.url,	// hostType  
		            defaultSpriteIcon.size, 
		            defaultSpriteIcon.spriteSize,
		            Number('${list.operationalState}'), 	// Sprite image index == operationalState value
		            defaultSpriteIcon.offset);
		
		var oMarker = new ubcDraggableMarker(SpriteIcon , {
			title : '${list.hostId} (${list.hostName})',
			point : new ubcLatLng('${fn:split(list.addr4,",")[0]}', '${fn:split(list.addr4,",")[1]}'),
			zIndex : 1,
			smallSrc: SpriteIcon});
		
		oMap.addOverlay(oMarker);
		data[i] = {title: '${list.hostId}', lat: '${fn:split(list.addr4,",")[0]}', lon: '${fn:split(list.addr4,",")[1]}'};
		i++;
	</c:forEach>
		
		/* 마커등록 한 것과 안한것 리스트 표기 */
		/* var chk = document.getElementsByName("chk-list");
		var mchk = document.getElementsByName("mchk-list");
		for(a in data) {
			for(var i=0; i<chk.length; i++) {
				if(data[a].title === chk[i].value) {
					chk[i].disabled=true;
				}
			}
		}
		for(var j=0; j<mchk.length; j++) {
			for(b in data) {
				if(mchk[j].value === data[b].title) {
					mchk[j].disabled=false;
				}
			}
		} */
		
		var v = 0;
		oMap.attach("changePosition", function (moveEvt) {
			var oTarget = moveEvt.target;
			for( j in data) {
				if(data[j].title === oTarget.getTitle().split(" ")[0]) {
					mdata[v] = {title: data[j].title, lat: moveEvt.newPoint.getLat(), lon: moveEvt.newPoint.getLng()};
					++v;
				}
			}
		});
	      
	      oMap.attach("mouseenter", function(oEvent){
		      var oTarget = oEvent.target;
		      if(oTarget instanceof ubcDraggableMarker){
		              oLabel.setVisible(true, oTarget); // - 특정 마커를 지정하여 해당 마커의 title을 보여준다.
		      }
		});

		oMap.attach("mouseleave", function(oEvent) {
		      var oTarget = oEvent.target;
		      if(oTarget instanceof ubcDraggableMarker){
		              oLabel.setVisible(false);
		      }
		});
		
		var oInfo_r = new ubcInfoWindow();
		oInfo_r.setVisible(false);
		var oInfo_l = new ubcInfoWindow();
		oInfo_l.setVisible(false);
		oMap.addOverlay(oInfo_r);
		oMap.addOverlay(oInfo_l);
		
		/* 우클릭 이벤트 */
		//function rClickEvt() {
		oMap.attach('contextmenu', function(oEvt) {
			var lat = oEvt.point.getLat();
			var lon = oEvt.point.getLng();
			var omapTarget = oEvt.target;
			
			//console.log("map event pos : " + lon + "," + lat);
			if(omapTarget instanceof ubcMap) {
				oInfo_r.setContent('<div id="rClick"><div id="rcLayout">' +
		            		'<table id="rc-table">' + 
		            		'<input type="hidden" id="pos" value="' + lat + "," + lon + '">' +
		            		'<tr id="rc-list"><td class="td-btn"><button type="button" id="save-btn" class="btn btn-3 btn-3a" onclick="updateAll()">단말위치 변경 저장</button></td>' +
		            		'<tr id="rc-list"><td class="td-btn"><button type="button" id="reset-btn" class="btn btn-3 btn-3a" onclick="resetCall()">단말위치 변경 취소</button></td>' +
		            		'<tr id="rc-list"><td class="td-btn"><button type="button" id="add-btn" class="btn btn-3 btn-3a" onclick="clickAdd()">현재위치 선택된 단말추가</button></td>' +
		            		'</div></div>');
				oInfo_r.setPoint(new ubcLatLng(lat, lon));
				oInfo_r.setVisible(true);
				oInfo_r.autoPosition();
			}
			if(omapTarget instanceof ubcDraggableMarker) {
				//console.log("mousepos " + omapTarget.getTitle() + " - " + lat + ", " + lon);
				//console.log("markerpos " + omapTarget.getTitle() + " - " + omapTarget.getPoint().getLat() + ", " + omapTarget.getPoint().getLng());
				oInfo_r.setContent('<div id="rClick"><div id="rcLayout">' +
		            		'<table id="rc-table"><tr id="rc-list"><td class="td-btn">' +
		            		'<input type="hidden" id="host" value="'+ omapTarget.getTitle().split(" ")[0] + '">' +
		            		'<input type="hidden" id="pos" value="' + omapTarget.getPoint().getLat() + "," + omapTarget.getPoint().getLng() + '">' +
		            		'<button type="button" id="save-btn" class="btn btn-3 btn-3a" onclick="updatehost()">단말위치 변경 저장</button></td>' +
		            		'<tr id="rc-list"><td class="td-btn"><button type="button" id="del-btn" class="btn btn-3 btn-3a" onclick="deletehost()">단말위치 삭제</button></td>' +
		            		'</div></div>');
		            		//'<tr id="rc-list"><td class="td-btn"><button type="button" id="reset-btn" class="btn btn-3 btn-3a">위치초기화</button></td>' +
				oInfo_r.setPoint(new ubcLatLng(lat, lon));
				oInfo_r.setVisible(true);
				oInfo_r.autoPosition();
			}
		});
			
		oMap.attach('click', function(oclickEvt) {
			var oTarget = oclickEvt.target;
			oInfo_r.setVisible(false);
			oInfo_l.setVisible(false);
			
			if(oTarget instanceof ubcDraggableMarker) {
				<c:forEach var="list" items="${list}">
					if(oTarget.getTitle().split(" ")[0] === '${list.hostId}') {
						id = '${list.hostId}';
						var address;
	            			var typeNum = Number('${list.hostType}');
	            			if('${list.addr3}' === null || '${list.addr3}' === '') { address = '주소가 입력되지 않았습니다.';}
	            			else { address = '${list.addr3}';}
	            			
		            		oInfo_l.setContent('<div id="frame"><div id="innerLayout">' +
			            		'<table id="info-table"><tr id="header-h"><td colspan="3" class="header">${list.hostName} / ${list.hostId}</td></tr>' +
			            		'<tr><td colspan="3" class="td-line"></td>' + 
			            		'<tr><td colspan="3" class="td-blank"></td>' + 
			            		'<tr id="header-f"><td class="header head-sub">주소</td><td id="addr" class="content content-edit"><input type="text" name="addrtxt" id="addrtxt" value="' + address + '" size="30"/></td>' +
			            		'<td><button type="button" id="modi" class="btn btn-2 btn-2a" onclick="modibtn()">수정</button></td></tr>' +
			            		'<tr id="header-m"><td class="header head-sub">타입</td><td colspan="2" id="hosttype" class="content content-edit">' + type[typeNum] + '</td></tr>' +
			            		'<tr id="header-l"><td class="header head-sub">좌표</td><td colspan="2" id="pos" class="content content-edit">${lat} , ${lon}</td></tr>' + 
			            		'<tr><td colspan="3" class="td-line-2"></td>' + 
			            		'<tr><td colspan="3" class="td-blank"></td>' + 
			            		'<input type="hidden" id="addr" value="${address}"/><input type="hidden" id="lat" value="${lat}"/><input type="hidden" id="lon" value="${lon}"/>' +
			            		'<tr id="header-btn"><td colspan="3" class="content"><button type="button" id="cancel" class="btn btn-2 btn-2a" onclick="cancel()">닫기</button></td></tr></table>' +
			            		'</div></div>');
		            		oInfo_l.setPoint(oTarget.getPoint());
		            		oInfo_l.setPosition({right: -200, top: 50});
		            		oInfo_l.setOpacity(0.9);
		            		oInfo_l.setVisible(true);
		            		oInfo_l.autoPosition();
		            		return;
					}
            		</c:forEach>
            	}
		});	
		//}
		//rClickEvt();
		
		$(function() {
			$(".div-fs fieldset label").mouseenter(function() {
				var idx = $("label").index(this)-2;	
				console.log( $('input[name=chk-list]').eq(idx).attr('value') );
				
				
				/* for(i in data) {
					if( $('input[name=chk-list]').eq(idx).attr('value') == data[i].title ) {
						console.log("true");
					}
				} */
			});
		});
		
		/* 검색 주소로 이동 Ajax */
		function userquery() {
			var uq = document.getElementById("address").value;
			var addr = uq;
			addr = addr.replace(/(^\s*)|(\s*$)/g, "");
			addr = encodeURIComponent(addr);
			$.ajax({
				type: "POST",
				url: "posxy.do",
				data: { "addr": addr },
				success: function(data) {
					console.log(data);
					oMap.setCenter(new ubcLatLng(data.split(",")[0], data.split(",")[1]));
				}
			});
		}
		
		// 사이드 메뉴에서 마커 등록시 중심좌표 데이터 전송 
		function centerXY() {
			var center = oMap.getCenter();
			document.getElementById('center').value = center;
		}
		
		// 단말정보에서 주소값 수정 버튼
		function modibtn() {
			var addr_data = document.getElementById("addrtxt").value;
			location.href="updateAddr.do?hostId=" + id + "&addr3=" + addr_data + "&query=" + encodeURIComponent("${query}");
		}
		
		// 단말 전체 위치 저장
		function updateAll() {
			var pos = document.getElementById('pos').value;
			var arr = new Array();
			for(k in mdata) {
				arr[k] = mdata[k].title+"//"+mdata[k].lat+"^"+mdata[k].lon;
				console.log(mdata[k].title+"//"+ mdata[k].lat+"^"+ mdata[k].lon);
			}
			location.href="saveall.do?data=" + arr + "&pos=" + pos + "&query=" + encodeURIComponent("${query}");
		}
		
		// 마우스 우클릭으로 마커 저장
		function clickAdd(lat, lon) {
			var chk = document.getElementsByName('chk-list');
			var flag = false
			for(var i=0; i<chk.length; i++) {
				if( (chk[i].checked) ) {
					flag = true;
				}
			}
			if(flag) {
				var formData = $('#listform').serialize();
				var pos = document.getElementById('pos').value;
				location.href="clickAdd.do?pos=" + pos + "&query=" + encodeURIComponent("${query}") + "&formData=" + formData;
			}
			else { 
				alert("단말이 선택되지 않았습니다.");
			}
		}
		
		// 단일 단말 위치 수정
		function updatehost() {
			var hostId = document.getElementById("host").value;
			var pos = document.getElementById("pos").value;
			
			location.href="updatehost.do?hostId=" + hostId + "&pos=" + pos + "&query=" + encodeURIComponent("${query}");
		}
		
		// 단일 단말 삭제
		function deletehost() {
			var hostId = document.getElementById("host").value;
			var pos = document.getElementById("pos").value;
			
			location.href="deletehost.do?hostId=" + hostId + "&pos=" + pos + "&query=" + encodeURIComponent("${query}");
		}
		
		function resetCall() {
			var pos = document.getElementById("pos").value;
			location.href="reset.do?pos=" + pos + "&query=" + encodeURIComponent("${query}");
		}
 	</script>