<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<jsp:include page="/WEB-INF/views/include/include.jsp"></jsp:include>
<script>
	$(document).ready(function() {
		if('${lat}' === '' && '${lon}' === '') {
			/* if(navigator.geolocation) {
				//navigator.geolocation.getCurrentPosition(nowLocation);
				navigator.geolocation.getCurrentPosition(
						function nowLocation(position) {
							var lat = position.coords.latitude;
							var lon = position.coords.longitude;
							
							$.ajax({
								type: "POST",
								url: "editMap.do",
								data: "lat=" + lat + "&lon=" + lon,
								success: function(data) {
									$('#mapview').html(data);
								}
							});
						},
						function(error) {
							alert("브라우저의 위치추적을 허용하지 않으셨습니다. 기본좌표로 이동합니다.");
							var lat = 37.5327619;
							var lon = 127.0139427;
							
							$.ajax({
								type: "POST",
								url: "editMap.do",
								data: "lat=" + lat + "&lon=" + lon,
								success: function(data) {
									$('#mapview').html(data);
								}
							});		
						}
				);
			} */
			//else {
				//alert("Your Browser don't support for Geolocation");
				var lat = 37.5327619;
				var lon = 127.0139427;
				
				$.ajax({
					type: "POST",
					url: "editMap.do",
					data: { "lat": lat, "lon": lon, "query": "${query}" },
					success: function(data) {
						$('#mapview').html(data);
					}
				});
			//}
		}
		else if('${lat}' !== '' && '${lon}' !== '') {
			$.ajax({
				type: "POST",
				url: "editMap.do",
				data: { "lat": '${lat}', "lon": '${lon}', "query": "${query}" },
				success: function(data) {
					$('#mapview').html(data);
				}
			});
		}
	});
	
	/* function Add() {
		centerXY();
		var formData = $('#listform').serialize();
		jQuery.ajaxSettings.traditional = true;
		 $.ajax({
			type: 'POST',
			url: 'regi.do',
			data: formData,
			success: function(data) {
				$('#mapview').empty();
				$('#mapview').html(data);
			}
		});
	} */
	
	$(function() {
		$("#side-list").tabs();
		
		var flag = 1;
		var chk = document.getElementsByName("chk-list");
		var mchk = document.getElementsByName("mchk-list");
		
		function effect() {
			$('#sidebar').toggle( 'drop', {}, 500); 
		}
		$('#menu-btn').click(function() {
			effect();
			if(flag === 1) {
				setTimeout(function() {$('.edit-mapview').css("width", "100%").css("margin-left", "0");
								$('#map').css("margin-left","0"); 
								$('#controller').css("margin-left", "0");
								$('#btn-top').css("margin-left", "0");
						}, 500);
				flag = 0;
			}
			else if(flag === 0) {
				$('.edit-mapview').css("width", "100%").css("margin-left", "-340px");
				$('#map').css("margin-left","320px"); 
				$('#controller').css("margin-left", "-340px");
				$('#btn-top').css("margin-left", "330px");
				flag = 1;
			}
			return false;
		});
		$('#allchk').click(function() {
			for(var i=0; i<chk.length; i++) {
				if( !(chk[i].disabled) ) {
					chk[i].checked = true;
				}
			}
		});
		$('#nonechk').click(function() {
			for(var i=0; i<chk.length; i++) {
				chk[i].checked = false;
			}
		});
		$('#mallchk').click(function() {
			for(var i=0; i<mchk.length; i++) {
				if( !(mchk[i].disabled) ) {
					mchk[i].checked = true;
				}
			}
		});
		$('#mnonechk').click(function() {
			for(var i=0; i<mchk.length; i++) {
				mchk[i].checked = false;
			}
		});
	});
</script>
</head>
<body>
<div id="wrap">
	<form id="listform" method="POST">
	<div id="sidebar">
		<div id="side-top">
			<input type="text" name="address" id="address" class="addr-text txtbox" disabled="disabled"/>
			<button type="button" id="send-map" class="btn btn-1 btn-1a" onclick="userquery()" disabled="disabled">이동</button> 
		</div>
		<div id="side-list">
			<ul>
				<li><a href="#dellist">표시단말</a></li>	<!-- 등록 된 단말 리스트  -->
				<li><a href="#addlist">미표시단말</a></li>	<!-- 등록 안 된 단말 리스트  -->
			</ul>
			<div id="addlist" class="list-form">
				<div class="sel-div">
					<input type="radio" name="radiobox" id="allchk" value="all"><label for="allchk" class="switch">전체선택</label>
					<input type="radio" name="radiobox" id="nonechk" value="none" checked="checked"><label for="nonechk" class="switch">선택해제</label>
				</div>
				<div class="div-fs">
					<fieldset>
						<c:forEach var="host" items="${list }">
							<label id="${host.hostId }"><input type="checkbox" name="chk-list" id="${host.hostId }" class="chkbox" value="${host.hostId }" />&nbsp; ${host.hostName } (${host.hostId })</label><br>
						</c:forEach>
					</fieldset>
				</div>
			</div>
			<div id="dellist" class="list-form">
				<div class="sel-div">
					<input type="radio" name="mradiobox" id="mallchk" value="all"><label for="mallchk" class="switch">전체선택</label>
					<input type="radio" name="mradiobox" id="mnonechk" value="none" checked="checked"><label for="mnonechk" class="switch">선택해제</label>
				</div>
				<div class="div-fs">
					<fieldset>
						<c:forEach var="host" items="${mlist }">
							<label id="${host.hostId }"><input type="checkbox" name="mchk-list" id="${host.hostId }" class="chkbox" value="${host.hostId }"/>&nbsp; ${host.hostName } (${host.hostId })</label><br>
						</c:forEach>
					</fieldset>
				</div>
				<div>
					<button type="button" id="del-btn" class="btn btn-1 btn-1a" onclick="deleteall()">선택단말 삭제</button>
				</div>
			</div>
		</div>
	</div>
	</form>
	<div id="controller">
		<div id="btn-top" class="edit-top">
			<input type="hidden" name="query" id="query" value="${query }" />
			<button type="button" class="toggle-btn btn btn-1 btn-1a" id="menu-btn" >메뉴</button>
			<button type="button" class="toggle-btn btn btn-1 btn-1a" id="def-btn" onclick="defMode()">조회모드</button>
			<button type="button" class="toggle-btn btn btn-1 btn-1a" id="edit-btn" onclick="editMode()">편집모드</button>
			<img src="./resources/img/NAVER.png" alt="powered by NAVER" width="100px"/>
		</div>
	</div>
	<div id="mapview" class="edit-mapview"></div>
</div>

</body>
</html>