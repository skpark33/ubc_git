<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<head>
<title>Innorix Multi Platform Solution - InnoDS</title>
<script type="text/javascript">
	function Upload()
	{			
		var InnoDSObj_Main = GetDSObject('InnoDS_Main')
		var InnoDSObj_01 = GetDSObject('InnoDS_01')
		var InnoDSObj_02 = GetDSObject('InnoDS_02')
		var InnoDSObj_03 = GetDSObject('InnoDS_03')

		// POST 헤더정보 초기화
		InnoDSObj_Main.ClearPostData();
		
		// 선택된 모든 파일제거
		InnoDSObj_Main.RemoveAllItems();

		// InnoDSObj_01에 선택된 파일을 InnoDSObj_Main에  추가
		for (var i=0; i<InnoDSObj_01.GetFileCount(); i++)
		{	
			InnoDSObj_Main.AddLocalFile(InnoDSObj_01.GetFileName(i));
		}

		// InnoDSObj_02에 선택된 파일을 InnoDSObj_Main에  추가
		for (var i=0; i<InnoDSObj_02.GetFileCount(); i++)
		{	
			InnoDSObj_Main.AddLocalFile(InnoDSObj_02.GetFileName(i));
		}

		// InnoDSObj_03에 선택된 파일을 InnoDSObj_Main에  추가
		for (var i=0; i<InnoDSObj_03.GetFileCount(); i++)
		{	
			InnoDSObj_Main.AddLocalFile(InnoDSObj_03.GetFileName(i));
		}		

		// 첨부 파일이 있으면 컴포넌트로 파일전송
		if (InnoDSObj_Main.GetFileCount() > 0)
		{
			// 서버에 저장될 서브 디렉토리 지정
			InnoDSObj_Main.AddPostData("_SUB_DIR", SubDir);
			
			// 업로드 시작
			InnoDSObj_Main.StartUpload();
		}
		else // 첨부 파일이 없으면 폼만 전송
		{
			InnoDSSubmitMulti(document.f_write, InnoDSObj_Main);		
		}
	}
	
	// 컴포넌트 로드 완료시 자동 호출되는 콜백 함수
	function OnLoadComplete(objName)
	{
		var DSObject = GetDSObject(objName);
		
		if (objName == "InnoDS_01")
		{
			DSObject.AppendFilter( "문서파일", "*.doc; *.xls; *.ppt; *.pdf; *.txt");
		}
		
		if (objName == "InnoDS_02")
		{
			DSObject.AppendFilter( "그림파일", "*.jpg; *.gif; *.png; *.bmp");
		}
	}
	
	// 파일이 추가된 후 발생하는 이벤트 입니다.
	function OnAddFile(objName, FileName, FileSize)
	{
		var DSObject = GetDSObject(objName);
		
		if (objName == "InnoDS_01")
		{
			DSObject.RemoveFromIndex(0);
			var strExt = FileName.split("\\");
			var short_name = strExt[strExt.length-1];
			document.getElementById("input_file_01").value = short_name + " (" + FileSize + "bytes)";
		}

		if (objName == "InnoDS_02")
		{
			DSObject.RemoveFromIndex(0);
			var strExt = FileName.split("\\");
			var short_name = strExt[strExt.length-1];
			document.getElementById("input_file_02").value = short_name + " (" + FileSize + "bytes)";
		}
	}	
	
	// OnUploadComplete는 파일 업로드 완료 후 발생하는 이벤트 입니다.
	// 파일 업로드가 완료되면 f_write 폼 데이타와 업로드된 파일 정보를 함께 전송 합니다.
	function OnUploadComplete(objName)
	{
		var DSObject = GetDSObject(objName);
		InnoDSSubmitMulti(document.f_write , DSObject);	
	}

	// OnUploadCancel()는 업로드 중 사용자가 [ESC]키나 [x]를 클릭하여
	// 전송을 중지했을 때 발생되는 이벤트 입니다. 
	function OnUploadCancel(objName)
	{
		alert("UploadCancel");
	}
</script>
</head>

<body>
<form action="multi_action.aspx" name="f_write" method="post" enctype="multipart/form-data">
test1 : <input type="text" name="test1" />
</form><br />

<script type="text/javascript" src="InnoDS.js"></script>
<script type="text/javascript">
	var Enc = "";
	
	var InputType = "fixed";
	var UploadURL = "action_ds.aspx";
	var SubDir = "2010";
	var ExistFileAdd = "true";
	
	InnoDSInitMulti("1000MB", "1000MB", 7, 1, 1, "InnoDS_Main");
</script>

문서파일(doc, xls, ppt, pdf, txt)<br />
<input type="text" id="input_file_01" style="width:420px" />
<input type="button" value="파일찾기" onClick="GetDSObject('InnoDS_01').OpenFileDialog();" /><br /><br />

<script type="text/javascript">
	var LimitExt = "doc;xls;ppt;pdf;txt";
	var ExtPermission = "true";
	InnoDSInitMulti("500MB", "500MB", 2, 1, 1, "InnoDS_01");
</script>

그림파일(jpg, gif, png, bmp)<br />
<input type="text" id="input_file_02" style="width:420px" />
<input type="button" value="파일찾기" onClick="GetDSObject('InnoDS_02').OpenFileDialog();" /><br /><br />

<script type="text/javascript">
	var LimitExt = "jpg;gif;png;bmp";
	var ExtPermission = "true";
	InnoDSInitMulti("500MB", "500MB", 2, 1, 1, "InnoDS_02");
</script>

<div style="border: 1px solid #c0c0c0; width:500px">
<script type="text/javascript">
	var ExtPermission = "false";
	InnoDSInitMulti("500MB", "500MB", 5, 500, 200, "InnoDS_03");
</script>
</div><br />

<input type="button" value="파일추가" onClick="GetDSObject('InnoDS_03').OpenFileDialog();" /><br /><br />
<input type="button" value="전송하기" onClick="Upload();" />
	
</body>
</html>