﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;
 
public partial class manager_code : BasePage
{ 
    protected void Page_Load(object sender, EventArgs e)
    {       
        if (!this.IsPostBack)
        {
            // 등록 : 순서 콤보 초기화
            ddlDorderReg.SelectedIndex = ddlDorderReg.Items.Count - 1;            
        }    
    }

    // 카테고리  불러오기
    protected void CodeList()
    {
        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            DataTable dt = obj.GetCodeList(ROOT_SITE_ID);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }

    /////////////////////////// 카테고리 관리 ////////////////////////        


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    { 

        //삭제버튼 클릭시 컨펌하기
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                foreach (Control control in cell.Controls)
                {
                    ImageButton button = control as ImageButton;

                    if (button != null)
                    {
                        if (button.CommandName == "Delete")
                            button.OnClientClick = "if (!confirm('카테고리 삭제 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다\\n카테고리에 속해있는 모든 코드값도 삭제됩니다\\n계속 하시겠습니까?  ')) return false;";
                        else if (button.CommandName == "Update")
                            button.OnClientClick = "if (!confirm('카테고리 변경 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다\\n계속 하시겠습니까?  ')) return false;";
                    }


                }
            }
        } 
    }

    protected void GridView1_PreRender(object sender, EventArgs e)
    {
        this.CodeList();    
    }

    // 카테고리 수정 버튼 클릭시 쓰기모드 활성화.
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        GridView1.SelectedIndex = -1;
        GridView1.EditIndex = e.NewEditIndex;
    }

    // 카테고리 취소 버튼 클릭시
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        GridView1.EditIndex = -1;
    }

    // 카테고리 신규등록
    protected void btnNewCodeName_Click(object sender, EventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string codeNameNew = NewCodeName.Text.Trim();

            if (Common.isTextInGridview(GridView1, 0, codeNameNew) == true)
            {
                ScriptExecute("같은 이름의 카테고리명이 존재합니다.\\n다른이름으로 등록하세요");
                return;
            }

            obj.GetCodeInsert(ROOT_SITE_ID, codeNameNew);
        }
    }

    //코드 업데이트 버튼 클릭시
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string codeId = GridView1.DataKeys[e.RowIndex].Value.ToString();
            string categoryName = ((TextBox)GridView1.Rows[e.RowIndex].Cells[0].Controls[0]).Text;

            if (Common.isTextInGridview(GridView1, 0, categoryName) == true)
            {
                ScriptExecute("같은 이름의 카테고리명이 존재합니다.\\n다른이름으로 등록하세요");
                return;
            }

            obj.GetCodeUpdate(ROOT_SITE_ID, codeId, categoryName);
        }

        GridView1.EditIndex  = -1;
    }

    //코드 삭제 클릭시
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            
            string codeId = GridView1.DataKeys[e.RowIndex].Value.ToString();
            
            obj.GetCodeDelete(ROOT_SITE_ID, codeId);
        }

        // 현재 선택되어 있는 카테고리가 삭제되었을 경우 카테고리 선택값 초기화
        if( GridView1.Rows[e.RowIndex].Cells[0].Text == hiddenCategoryName.Text )
        {
            hiddenCategoryName.Text = "";
            lblcategoryName.Text = "";
        }
    }
    
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        hiddenCategoryName.Text = GridView1.Rows[e.NewSelectedIndex].Cells[0].Text;
        lblcategoryName.Text = "[" + GridView1.Rows[e.NewSelectedIndex].Cells[0].Text + "]"  ;
    }     
 
    /////////////////////////// 코드 관리 ////////////////////////        

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
        if (e.Row.RowType == DataControlRowType.DataRow)
        {   
            //삭제버튼 클릭시 컨펌하기
            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                
                foreach (Control control in cell.Controls)
                {
                    ImageButton button = control as ImageButton;
                    if (button != null)
                    {
                        if (button.CommandName == "Delete")
                        {
                            button.OnClientClick = "if (!confirm('코드 삭제 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다.\\n계속 하시겠습니까?  ')) return false;";
                        }
                        else if (button.CommandName == "Update")
                        {
                            button.OnClientClick = "if (!confirm('코드 변경 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다.\\n계속 하시겠습니까?  ')) return false;";
                        }
                    }

                   
                }

            }

            // 변경시 순서와 사용여부 드롭다운리스트 활성화
            if ((e.Row.RowState & DataControlRowState.Edit) > 0 )
            {
                (e.Row.FindControl("ddlDorder") as DropDownList).Enabled = true;
                (e.Row.FindControl("ddlVisible") as DropDownList).Enabled = true;
                
            }

                      
        }


    }


    protected void GridView2_PreRender(object sender, EventArgs e)
    {
        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            DataTable dt = obj.GetDetailCodeList( ROOT_SITE_ID, hiddenCategoryName.Text );
            GridView2.DataSource = dt;             
            GridView2.DataBind();
             
        }

        for (int i = 0; i < GridView2.Rows.Count; i++)
        {
            DropDownList ddl = (DropDownList)GridView2.Rows[i].Cells[3].FindControl("ddlDorder");
            ddl.SelectedValue = GridView2.Rows[i].Cells[5].Text;

            ddl = (DropDownList)GridView2.Rows[i].Cells[4].FindControl("ddlVisible");
            ddl.SelectedValue = GridView2.Rows[i].Cells[6].Text;

        }       
         
    }

  
    // 코드 신규등록
    protected void newCodeDetailAdd_Click(object sender, EventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string enumString = txtEnumString.Text.Trim();
            string dorder = ddlDorderReg.SelectedValue;
            string visible = ddlVisibleReg.SelectedValue;

            if (Common.isTextInGridview(GridView2, 1, enumString) == true)
            {
                ScriptExecute("같은 이름의 코드명이 존재합니다.\\n다른이름으로 등록하세요");
                return;
            }

            obj.GetCodeDetailInsert(  ROOT_SITE_ID
                                    , hiddenCategoryName.Text
                                    , enumString
                                    , dorder.ToInt()
                                    , visible.ToInt());

        }
    }

    // 코드 업데이트
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string codeId = GridView2.DataKeys[e.RowIndex].Value.ToString();

            string enumString = ((TextBox)GridView2.Rows[e.RowIndex].Cells[1].Controls[0]).Text.Trim();
                        
            DropDownList ddlDorder = (DropDownList)GridView2.Rows[e.RowIndex].Cells[3].FindControl("ddlDorder");
            string dorder = ddlDorder.SelectedValue;

            DropDownList ddlVisible = (DropDownList)GridView2.Rows[e.RowIndex].Cells[4].FindControl("ddlVisible");
            string visible = ddlVisible.SelectedValue;

            if (enumString.IsNullOrEmpty())
            {
                //((TextBox)GridView2.Rows[e.RowIndex].Cells[1].Controls[0]).Focus();
                ScriptExecute("코드명을 입력하세요.");
                return;
            }

            if (Common.isTextInGridview(GridView2, 1, enumString) == true)
            {
                ScriptExecute("같은 이름의 코드명이 존재합니다.\\n다른이름으로 등록하세요");
                return;
            }
            
            obj.GetCodeDetailUpdate(  ROOT_SITE_ID
                                    , codeId
                                    , enumString
                                    , dorder.ToInt()
                                    , visible.ToInt());

        }

        GridView2.EditIndex = -1;         

    }

    // 코드 수정 버튼 클릭시 쓰기모드 활성화.
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        GridView2.SelectedIndex = -1;
        GridView2.EditIndex = e.NewEditIndex;

        
    }

    // 코드 삭제 버튼 클릭시
    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string codeId = GridView2.DataKeys[e.RowIndex].Value.ToString();
            obj.GetCodeDetailDelete(ROOT_SITE_ID, codeId);
        }
    }

    // 코드 취소 버튼 클릭시
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute("권한이 없습니다."); return; }

        GridView2.EditIndex = -1;
    }    
    
}
