﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="generalSettings.aspx.cs" Inherits="manager_generalSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript">

    function addURL()
    {
        var urlId = $("#<%=txtURLId.ClientID %>").val();

        if( urlId == "" )
        {
            alert("URL ID를 입력하세요.");
            $("#<%=txtURLId.ClientID %>").focus();
            return false ;
        }

        // 올바른 URL ID 인지 검사 하기 위한 정규식
        // 다음 문자로 시작되면 안됨! ==> [,=,:,;,#,<,공백
        // 공백을 포함하면 안됨!
        var regExp = /^[^\[=:;#\<\s]+\S*$/;

        if (!regExp.test(urlId))
        {
            alert("URL ID는 [,=,:,;,# 문자로 시작 될수 없으며 공백문자를 포함할 수 없습니다.");
            $("#<%=txtURLId.ClientID %>").focus();
            return false;
        }

        var urlValue = $("#<%=txtURLValue.ClientID %>").val();        

        if ($.trim(urlValue) == "") {
            alert("URL을 입력하세요.");
            $("#<%=txtURLValue.ClientID %>").focus();
            return false;
        }
        
        return true;
    }       
    
    
</script>

<style type="text/css">

#url_section 
{
	padding: 10px 10px;        
    margin: 0;     
    border: solid 1px #ddd;
    
    /*CSS3 properties*/
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    
    box-shadow: rgb(0,0,0) 4px 2px 6px;    
    -webkit-box-shadow: rgb(0,0,0) 4px 2px 6px;
    -moz-box-shadow: rgb(0,0,0) 4px 2px 6px;
}


.hiddencol
{
    display : none;
}
    

</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div>

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
    <colgroup>
        <col width="auto" />
        <col width="50%" />
    </colgroup>
    <tr>
    <td>
        <span class="subTitle"><span><img src="/Asset/Images/body/bullet01.gif" /> 사이트 설정</span></span>
    </td>
    </tr>
    </table>

</div>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div id="url_section">
    
    <h3><img src="/Asset/Images/agronet/ico_chk01.gif" /> URL 설정</h3>    
    
    <div class="edit_area">                    
        <img src="/Asset/Images/agronet/bul_arr01.gif" /> 
        <asp:Label runat="server" Text="URL ID" CssClass="pad_R10" /><asp:TextBox ID="txtURLId" CssClass="textType"  runat="server" />
        <asp:Label runat="server" Text="URL" CssClass="pad_R10" /><asp:TextBox ID="txtURLValue" CssClass="textType"  runat="server" />
        <asp:button ID="btnAddURL" runat="server" OnClientClick="return addURL();" OnClick="btnAddURL_Click" Text="Add"/>
    </div>
    
    
    <div >        
        
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="urlId" CssClass="boardListType2" Height="200px" 
                onprerender="GridView1_PreRender" 
                onrowediting="GridView1_RowEditing"
                onrowdeleting="GridView1_RowDeleting"
                onrowdatabound="GridView1_RowDataBound" 
                onrowupdating="GridView1_RowUpdating" 
                onrowcancelingedit="GridView1_RowCancelingEdit">
        <Columns>
            <asp:BoundField DataField="seq" HeaderText="No." ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ReadOnly="true" />
            <asp:BoundField DataField="urlId" HeaderText="URL ID" HeaderStyle-Width="30%" ReadOnly="true" ConvertEmptyStringToNull=true />
            <asp:BoundField DataField="url" HeaderText="URL" ControlStyle-Width="90%" HtmlEncode="false" ConvertEmptyStringToNull=true />
            <asp:BoundField DataField="url_hidden" HtmlEncode="false" ReadOnly="true" ConvertEmptyStringToNull=true ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass=hiddencol />
       
            <asp:CommandField 
                ButtonType="Button" 
                HeaderStyle-Width="20%" 
                ItemStyle-HorizontalAlign="Center"                 
                HeaderText="Action" 
                ShowEditButton="true" ShowDeleteButton="true"/>

        </Columns>
        
        <EmptyDataTemplate>
            <table style="width:100%;height:290px;">
                <tr>
                    <td style="text-align:center">
                    조회된 데이터가 없습니다.
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>

        </asp:GridView>    
           
    </div>
    
    
    
    

</div>

</ContentTemplate>    
</asp:UpdatePanel>

</asp:Content>

