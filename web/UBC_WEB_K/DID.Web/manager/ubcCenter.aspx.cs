﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using DID.Common.Framework;

using System.Diagnostics;
using System.Web.UI.HtmlControls;

using System.Management;

public partial class ubcCenter : BasePage
{
    protected const string UPDATE_DIRECTORY = "d:/software_update";
    protected const string UPDATE_LOG_DIRECTORY = UPDATE_DIRECTORY + "/" + "log";
    protected const string UPDATE_LOG_EXTENSION = "ubc.update.log";

    protected const string UPDATE_SERVER = "ubcupdate.sqisoft.com:8880";
    protected const string SERVER_MODULE_DIRECTORY = "SERVER";
    protected const string CLIENT_MODULE_DIRECTORY = "CLIENT";

    protected const int MAX_LOG_FILE_COUNT = 30;

    protected const string EXCUTABLE_SERVER = "58.87.34.251";   // 기아 web서버02
    //protected const string EXCUTABLE_SERVER = "58.87.34.248"; // 현대 web서버02

    protected bool isExcutable = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.InitControl();
        }
                
        if (!System.IO.Directory.Exists(UPDATE_DIRECTORY)) System.IO.Directory.CreateDirectory(UPDATE_DIRECTORY);
        if (!System.IO.Directory.Exists(UPDATE_LOG_DIRECTORY)) System.IO.Directory.CreateDirectory(UPDATE_LOG_DIRECTORY);

        System.IO.Directory.SetCurrentDirectory(UPDATE_DIRECTORY);

        if (checkWgetProcess() == true)
        {
            btnClient.Enabled = false;
            btnServer.Enabled = false;
            btnCancel.Visible = true;
            imgUpdate.Visible = true;

            Timer1.Enabled = true;
        }
        else
        {
            btnClient.Enabled = true;
            btnServer.Enabled = true;
            btnCancel.Visible = false;
            imgUpdate.Visible = false;
        }

    }

    private void InitControl()
    {
        // 권한 체크
        if (Profile.USER_TYPE != "-1")
        {
            divCmd.Visible = false;
            lblMsg.Text = " (작업 권한이 없습니다.)";
            txtWorkLog.Enabled = false;
            txtTransLog.Enabled = false;
            txtErrorLog.Enabled = false;
            return;
        }

        // 웹서버2 인지 체크
        if (System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList[0].ToString() != EXCUTABLE_SERVER)
        {
            isExcutable = false;
            return;
        }

        // max 값 이상일경우 로그화일 삭제
        deleteLogFiles();

        if (checkWgetProcess() == true) 
            setWorkLog("다른 사용자에 의해 업데이트가 진행중입니다...", true);
    }

    // 로그화일 삭제
    protected void deleteLogFiles()
    {
        try
        {
            // 로그 디렉토리의 로그 화일목록을 가져온다
            string[] logFiles = Directory.GetFiles(UPDATE_LOG_DIRECTORY, "*." + UPDATE_LOG_EXTENSION);

            // 화일명을 기준으로 오름차순으로 정렬
            Array.Sort(logFiles);

            int overCount = logFiles.Length - MAX_LOG_FILE_COUNT;

            for( int i=0 ; i<overCount ; i++ )
            {
                File.Delete(logFiles[i]); // MSDN: 지정된 파일이 없어도 예외가 throw되지 않습니다.                
            }
        }
        catch (System.IO.DirectoryNotFoundException eDNF)
        {
            // 디렉토리 존재하지 않을경우는 로그 화일 자체가 없다는 뜻이므로 그냥 패스~
        }
        catch (Exception e)
        {
            setWorkLog(string.Format("Exception [deleteLogFiles] : {0}", e.ToString()));
        }
    }
    
    // 작업로그
    protected void setWorkLog(string log)
    {       
        System.DateTime now = System.DateTime.Now;
        txtWorkLog.Text += string.Format("[{0} {1}] {2}\n", now.ToShortDateString(), now.ToShortTimeString(), log);
    }
    // 작업로그
    protected void setWorkLog(string log, bool clear)
    {
        if (clear) txtWorkLog.Text = "";
        setWorkLog(log);
    }
    
    // 전송에러 로그
    protected void setErrorLog(string error)
    {
        System.DateTime now = System.DateTime.Now;
        txtErrorLog.Text += string.Format("[{0} {1}] {2}\n", now.ToShortDateString(), now.ToShortTimeString(), error);
    }
    // 전송에러 로그    
    protected void setErrorLog(string error, bool clear)
    {
        if (clear) txtErrorLog.Text = "";
        setErrorLog(error);
    }
    
    // 업데이트 진행 여부 검사
    protected bool checkWgetProcess()
    {
        SelectQuery selectQuery = new SelectQuery("Win32_Process", "Name='wget.exe'");
        ManagementObjectSearcher searcher = new ManagementObjectSearcher(selectQuery);

        foreach (ManagementObject proc in searcher.Get())
        {
            if (proc["CommandLine"].ToString().ToLower().Contains(UPDATE_LOG_EXTENSION.ToLower()))
                return true;
        }

        return false;
    }    

    // release.txt 를 읽어서 버전정보를 가져옴
    protected string GetUpdateVersion(string releasefileName)
    {
        string version = "";

        try
        {
            // 다운받아서 저장할 화일명
            string outputReleasefileName = "ubc_release.txt";

            string commnad = string.Format("-O {0} -t 2 HTTP://{1}/{2}"
                                            , outputReleasefileName
                                            , UPDATE_SERVER
                                            , releasefileName
                                          );

            // 프로세스 시작
            Process process = Process.Start("wget.exe", commnad);
            process.WaitForExit();
            int ExitCode = process.ExitCode;
            process.Close();

            if (ExitCode == 0)
            {
                if (File.Exists(outputReleasefileName))
                {
                    StreamReader sr = File.OpenText(outputReleasefileName);

                    string s = sr.ReadLine();

                    if (!s.IsNullOrEmpty())
                    {
                        version = s.TrimStart('#');
                    }

                    sr.Close();
                    sr.Dispose();

                    // ubc_release.txt 삭제
                    File.Delete(outputReleasefileName);
                }
            }

        }
        catch (Exception e)
        {
            setWorkLog(string.Format("Exception [GetUpdateVersion] : {0}", e.ToString()));
        }

        return version;
    }

    // 클라이언트 업데이트 버튼 핸들러
    protected void btnClient_Click(object sender, EventArgs e)
    {
        setWorkLog("+++++++++++++++++++++++ Update Start +++++++++++++++++++++++++", true);
        setWorkLog("Client Update");
        updateStart(UPDATE_SERVER, CLIENT_MODULE_DIRECTORY);
    }

    // 서버 업데이트 버튼 핸들러
    protected void btnServer_Click(object sender, EventArgs e)
    {
        setWorkLog("+++++++++++++++++++++++ Update Start +++++++++++++++++++++++++", true);
        setWorkLog("Server Update");
        updateStart(UPDATE_SERVER, SERVER_MODULE_DIRECTORY);
    }

    // 업데이트 실행
    protected void updateStart(string serverURL, string moduleDirectory)
    {        
        // 작업중인지 확인
        if (checkWgetProcess() == true)
        {
            setWorkLog("다른 사용자에 의해 업데이트가 진행중입니다...", true);
            return;
        }        

        // 버전전보 가져오기
        string version = GetUpdateVersion(string.Format("{0}/ReleaseInfo/release.txt", moduleDirectory));

        if (version.IsNullOrEmpty())
        {
            setWorkLog("버젼 가져오기에 실패했습니다. 다시 시도해 보시고 계속 에러 발생시 담당자에게 문의하세요");

            btnClient.Enabled = true;
            btnServer.Enabled = true;
            
        }
        else
        {
            // 로그화일명 만들고
            string logFileName = string.Format("{0}.{1}.{2}"                                                
                                                , System.DateTime.Now.ToString("yyyyMMddHHmmss")
                                                , version
                                                , UPDATE_LOG_EXTENSION);

            // 인자 문자열 만들고
            string commnad = string.Format("-o {0} -P {1} -nH -m -x -nv -R index.html HTTP://{2}/{3}/"
                                            , UPDATE_LOG_DIRECTORY + "/" + logFileName
                                            , version
                                            , serverURL
                                            , moduleDirectory
                                          );

            // 프로세스 시작
            Process.Start("wget.exe", commnad);

            Timer1.Enabled = true;

        }  
    }

    // 작업취소 버튼 핸들러
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        setWorkLog("+++++++++++++++++++++++ Update Canceled +++++++++++++++++++++++");        

        SelectQuery selectQuery = new SelectQuery("Win32_Process", "Name='wget.exe'");
        ManagementObjectSearcher searcher = new ManagementObjectSearcher(selectQuery);

        foreach (ManagementObject proc in searcher.Get())
        {
            if (proc["CommandLine"].ToString().ToLower().Contains(UPDATE_LOG_EXTENSION.ToLower()))
            {
                 proc.InvokeMethod("Terminate", null);
                break;
            }
        }

        Timer1.Enabled = false;
        btnClient.Enabled = true;
        btnServer.Enabled = true;
        
        btnCancel.Visible = false;
        imgUpdate.Visible = false;

    }

    // 타이머 핸들러
    protected void Timer1_Tick(object sender, EventArgs e)
    {        
        // 로그 디렉토리의 로그 화일목록을 가져온다
        string[] logFiles = Directory.GetFiles(UPDATE_LOG_DIRECTORY, "*." + UPDATE_LOG_EXTENSION);
        if (logFiles.Length <= 0) return;

        // 화일명을 기준으로 오름차순으로 정렬
        Array.Sort(logFiles);  
      
        // 최신로그화일
        string logFile = logFiles[logFiles.Length - 1];

        StreamReader sr = null;

        try
        {
            // wget 프로세스가 끝났는지 확인 하기위해 로그 화일을 열어봄.
            // wget 프로세스가 로그 화일을 쓰고있으면 System.IO.IOException 이 발생함으로 아직 wget 이 종료 되지 않은것으로 판단함
            sr = new StreamReader(logFile);
            sr.Close();

            // wget 이 종료된 후에 화면에 아직 출력되지 않은 로그가 있으므로 원본 로그화일을 열어 남은 부분을 모두 출력함
            showTransLog(logFile);

            // 로그화일명에 들어있는 버젼정보 가져오기 (로그화일명포맷 : 날짜.버젼.ubc.update.log)
            string version;
            version = Path.GetFileName(logFile); // 전체 PATH 에서 화일명과 확장자만 가져옴
            version = version.TrimEnd(UPDATE_LOG_EXTENSION.ToCharArray()); // 로그 확장자 삭제 (ubc.update.log)
            version = version.Substring(version.IndexOf(".") + 1); // 버젼부분 뽑아냄
            
            // 업데이트가 잘됏는지 확인 하기 위해서 다운로드된 폴더의 사이즈를 화면에 보여줌
            long server = getDirectorySize(Path.Combine(Path.Combine(UPDATE_DIRECTORY, version), SERVER_MODULE_DIRECTORY));
            long client = getDirectorySize(Path.Combine(Path.Combine(UPDATE_DIRECTORY, version), CLIENT_MODULE_DIRECTORY));
            setWorkLog(string.Format("Download Size: Server {0:#,0} MB({1:#,0} byte) Client {2:#,0} MB({3:0,0} byte)"
                                        , server / 1024
                                        , server
                                        , client / 1024
                                        , client
                                    ));

            setWorkLog("+++++++++++++++++++++++ Update Finished +++++++++++++++++++++++");

            Timer1.Enabled = false;
            btnClient.Enabled = true;
            btnServer.Enabled = true;
            btnCancel.Visible = false;
            imgUpdate.Visible = false;
        }
        catch (System.IO.IOException ioe)
        {
            // 로그화일의 사본을 만들어 화면에 보여준다
            showTransLog(logFile);
        }
        catch (Exception ex)
        {
            setWorkLog(string.Format("Exception [Timer1_Tick] : {0}", ex.ToString()));
        }
        
    }

    // 로그 화일의 복사본을 만들고 그내용을 읽어서 화면에 전송 상태를 보여준다
    protected void showTransLog(string logFileName)
    {
        try
        {
            if (File.Exists(logFileName))
            {               
                string tempLogFileName = UPDATE_LOG_DIRECTORY + "/" + System.Guid.NewGuid().ToString();

                // 새로운 로그 화일일 경우 초기화
                if (HiddenLogFileName.Value != tempLogFileName)
                {
                    HiddenLogFileName.Value = tempLogFileName;
                    HiddenLogFileRowCount.Value = "0";
                    txtTransLog.Text = "";
                    txtErrorLog.Text = "";
                }

                // 똑같은 로그화일이 있다면 삭제
                File.Delete(tempLogFileName); // MSDN: 지정된 파일이 없어도 예외가 throw되지 않습니다.

                // 화면에 보여주기 위한 temp 로그화일 생성
                File.Copy(logFileName, tempLogFileName);

                StreamReader sr = File.OpenText(tempLogFileName);

                string s = "";
                string s_previous = "";
                int logfileRowCount = 0;

                while ((s = sr.ReadLine()) != null)
                {
                    logfileRowCount++;

                    if (HiddenLogFileRowCount.Value.ToInt() >= logfileRowCount)
                        continue;

                    txtTransLog.Text = s + "\n" + txtTransLog.Text;

                    // 에러 발생시 에러 표시  
                    if ((s.Contains("->") && !s.Contains("[1]")) || s.ToLower().Contains("error"))
                    {
                        setErrorLog(s + "[" + s_previous + "]");
                    }

                    s_previous = s.TrimEnd(':',' ');
                }

                sr.Close();

                
                // 화면에 보여주기 위한 temp 로그화일 삭제
                File.Delete(tempLogFileName); // MSDN: 지정된 파일이 없어도 예외가 throw되지 않습니다.

                HiddenLogFileRowCount.Value = logfileRowCount.ToString(); 
            }

        }
        catch (Exception e)
        {
            setWorkLog(string.Format("Exception [writeResultLog] : {0}", e.ToString()));
        }
    }

    // 디렉토리 사이즈 알아내는 함수
    protected long getDirectorySize(string path)
    {
        if (!System.IO.Directory.Exists(path)) return 0;

        long fileSieze = 0;        

        string[] fileNames = Directory.GetFiles(path, "*.*");
        
        foreach (string fileName in fileNames)
        {
            FileInfo info = new FileInfo(fileName);
            fileSieze += info.Length;
        }

        string[] directories = Directory.GetDirectories(path);

        foreach (string directory in directories)
        {
            fileSieze += getDirectorySize(directory);
        }

        return fileSieze;
    }

    
}

