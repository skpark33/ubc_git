/**
 * Husky Editor Extension
 * @author soulketchup@nate.com
 * @version 0.0.2
 */

oMessageMap["SE_EmbedMedia.notSupportedFileType"] = "지원되지 않는 파일형식입니다";
oMessageMap["SE_EmbedMedia.invalidEmbedSource"] = "잘못된 HTML 소스형식입니다";
oMessageMap["SE_PreviewInNewWin.PopupBlocked"] = "팝업창이 차단되었습니다";

var ext = {};
ext.maggot = {};

ext.maggot.SE_PreviewInNewWin = $Class({
  name : "SE_PreviewInNewWin",
  $init : function(elAppContainer){
  },
  $ON_MSG_APP_READY : function(){
    $Fn($Fn(this.oApp.exec, this.oApp).bind("PREVIEW_IN_NEWWIN", []), this).attach(cssquery.getSingle("BUTTON.preview_in_newwin"), "mousedown");
  },
  $ON_PREVIEW_IN_NEWWIN : function(){
    this.oApp.exec("HIDE_ACTIVE_LAYER", []);
    var wnd_se_preview = window.open('se_blank.html','wnd_se_preview','width=640px,height=500px,scrollbars,resizable');
    if(wnd_se_preview) {
      with(wnd_se_preview.document) {
        open();
        write("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><title>미리보기</title><link href=\"css/style.css\" rel=\"stylesheet\" type=\"text/css\" /></head><body id=\"smartInput\" class=\"smartOutput\">");
        write(this.oApp.getIR());
        write("</body></html>");
        close();
      }
      wnd_se_preview.focus();
    } else {
      alert(this.oApp.$MSG("SE_PreviewInNewWin.PopupBlocked"));
    }
  }
});

ext.maggot.SE_EmbedMedia = $Class({
  name : "SE_EmbedMedia",
  reguiredPlugin : ["SE_EditingArea_WYSIWYG"],
  bLocationMode : true,
  $init : function(elAppContainer){
    this._assignHTMLObjects(elAppContainer);
  },
  _convertPseudoToReal : function(htmlSrc, sMode) {
    htmlSrc = htmlSrc.replace(/<img\s+[^>]+?class\s*=\s*(?:["'])?ext_embed_([a-z0-9]+?)(["']|\s)[\s\S]+?>/ig, function($0, $1){
      var convert = "", embedName = "", embedType = $1, dimension = [100, 100], embedSrc = "";
      if(/\s+name\s*=\s*(?:["'])?([^"'> ]+)/i.test($0)) embedName = RegExp.$1;
      if(/width\s*\:\s*(\d+)px/i.test($0)) dimension[0] = RegExp.$1;
      if(/height\s*\:\s*(\d+)px/i.test($0)) dimension[1] = RegExp.$1;
      if(/longdesc\s*\=\s*(?:["])?([a-z0-9%_\-.']+)/i.test($0)) embedSrc = decodeURIComponent(RegExp.$1);
      switch(embedType) {
        case "swf":
          convert = "<object " + (embedName ? " name=\"" + embedName + "\"" : "") + " class=\"embed_{3}\" width=\"{1}\" height=\"{2}\" type=\"application/x-shockwave-flash\" data=\"{0}\"><param name=\"movie\" value=\"{0}\"><param name=\"quality\" value=\"high\"><p>이 콘텐츠를 보기위해 Flash player가 필요합니다</p></object>";
          break;
        case "asf":
        case "wmv":
          convert = "<object" + (embedName ? " name=\"" + embedName + "\"" : "") + " class=\"embed_{3}\" width=\"{1}\" height=\"{2}\" classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\"><param name=\"autostart\" value=\"False\" /><param name=\"showstatusbar\" value=\"true\" /><param name=\"filename\" value=\"{0}\" /><embed" + (embedName ? " name=\"" + embedName + "\"" : "") + " width=\"{1}\" height=\"{2}\" type=\"application/x-ms-{3}\" autostart=\"false\" src=\"{0}\"></embed></object>";
          break;
        case "mp3":
          convert = "<object" + (embedName ? " name=\"" + embedName + "\"" : "") + " class=\"embed_{3}\" width=\"{1}\" height=\"{2}\" classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\"><param name=\"autostart\" value=\"False\" /><param name=\"showstatusbar\" value=\"true\" /><param name=\"filename\" value=\"{0}\" /><embed" + (embedName ? " name=\"" + embedName + "\"" : "") + " width=\"{1}\" height=\"{2}\" type=\"application/x-mplayer2\" autostart=\"false\" src=\"{0}\"></embed></object>";
          break;
        case "wma":
          convert = "<object" + (embedName ? " name=\"" + embedName + "\"" : "") + " class=\"embed_{3}\" width=\"{1}\" height=\"{2}\" classid=\"clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95\"><param name=\"autostart\" value=\"False\" /><param name=\"showstatusbar\" value=\"true\" /><param name=\"filename\" value=\"{0}\" /><embed" + (embedName ? " name=\"" + embedName + "\"" : "") + " width=\"{1}\" height=\"{2}\" type=\"application/x-ms-wma\" autostart=\"false\" src=\"{0}\"></embed></object>";
          break;
        case "src":
          var replaced = [false, false];
          convert = embedSrc.replace(/(width|height)(\s*[:=]\s*)(["'])?(\d+)/ig, function($0,$1,$2,$3,$4){
            switch($1.toLowerCase()) {
              case "width" : replaced[0] = true; return $1 + $2 + ($3 || "") + dimension[0];
              case "height" : replaced[1] = true; return $1 + $2 + ($3 || "") + dimension[1];
            }
            return $0;
          });
          if(replaced[1] == false)  //set height property if height is not set
            convert = convert.replace(/^<(object|embed)(\s+)/i, "<$1 height=\"{2}\"$2");
          if(replaced[0] == false)  //set width property if width is not set
            convert = convert.replace(/^<(object|embed)(\s+)/i, "<$1 width=\"{1}\"$2");
          break;
      }
      if(convert)
        return convert.replace(/\{([0-3])\}/g, function(a,b){
          switch(b) {
            case "0": return embedSrc;
            case "1": return dimension[0];
            case "2": return dimension[1];
            case "3": return embedType;
            default : return a;
          }
        });
      else
        return $0;
    });
    return htmlSrc;
  },
  _convertEmbed : function(objectSrc) {
    var convert = "", embedName = "", embedType = "src", dimension = [100, 100], embedSrc = "";
    if(/\s+name\s*=\s*(?:["'])?([^"'> ]+)/i.test(objectSrc)) embedName = RegExp.$1;
    if(/class\s*=\s*(?:["'])?embed_([a-z0-9]+)/i.test(objectSrc)) embedType = RegExp.$1;
    if(/width\s*=\s*(?:["'])?(\d+)/i.test(objectSrc)) dimension[0] = RegExp.$1;
    if(/height\s*=\s*(?:["'])?(\d+)/i.test(objectSrc)) dimension[1] = RegExp.$1;
    if(/(?:src|data)\s*=\s*(?:["'])?([ %a-z0-9.\-_:;\/\?=\&]+)/i.test(objectSrc)) embedSrc = RegExp.$1;
    switch(embedType) {
      case "swf":
      case "asf":
      case "wmv":
      case "wma":
      case "mp3":
        return "<img src=\"img/blank.gif\"" + (embedName ? " name=\"" + embedName + "\"" : "") + " class=\"ext_embed_" + embedType + "\" style=\"width:" + dimension[0] + "px;height:" + dimension[1] + "px\" longdesc=\"" + encodeURIComponent(embedSrc) + "\">";
      default:
        return "<img src=\"img/blank.gif\"" + (embedName ? " name=\"" + embedName + "\"" : "") + " class=\"ext_embed_src\" style=\"width:" + dimension[0] + "px;height:" + dimension[1] + "px;\" longdesc=\"" + encodeURIComponent(objectSrc) + "\">";
    }
  },
  _convertRealToPseudo : function(htmlSrc, sMode) {
    if(sMode != "HTMLSrc_TO_IR"){
      htmlSrc = htmlSrc.replace(/<(object)\s+[\s\S]+?<\/\1>/ig, this._convertEmbed).replace(/<(embed)\s+[\s\S]+?(<\/\1>|>)/ig, this._convertEmbed);
    }
    return htmlSrc;
  },
  _embedConverter : function(f, sMode) {
    var owner = this;
    return (function(){
      return f.apply(owner, [arguments[0], sMode]);
    });
  },
  _assignHTMLObjects : function(elAppContainer){
    this.elDropdownLayer = cssquery.getSingle("DIV.husky_seditor_embed_layer", elAppContainer);
    var oTmp = cssquery("LI", this.elDropdownLayer);
    this.oLocTab = oTmp[0];
    this.oSrcTab = oTmp[1];
    this.oAcceptButton = cssquery.getSingle("BUTTON.accept", this.elDropdownLayer);
    this.oCancelButton = cssquery.getSingle("BUTTON.cancel", this.elDropdownLayer);
    this.inputLoc = cssquery.getSingle("#embed_loc");
    this.inputSrc = cssquery.getSingle("#embed_src");
  },
  _initializeEmbed : function(){
    this.inputLoc.value = "";
    this.inputSrc.value = "";
    if(this.oNode && this.oNode.getAttribute) {
      var cls = this.oNode.className;
      //var width = this._parseInt(this.oNode.style.width) || this._parseInt(this.oNode.getAttribute("width"), 0) || 320;
      //var height = this._parseInt(this.oNode.style.height) || this._parseInt(this.oNode.getAttribute("height"), 0) || 320;
      var src = this.oNode.getAttribute("LONGDESC") || "";
      switch(cls) {
        case "ext_embed_src":
          this.oApp.exec("SHOW_SRC", []);
          this.inputSrc.value = decodeURIComponent(src);
          break;
        default:
          this.inputLoc.value = decodeURIComponent(src);
          this.oApp.exec("SHOW_LOC", []);
          break;
      }
    }
  },
  _parseInt : function(value, defaultValue){
    value = parseInt(value, 10);
    return (isNaN(value) ? (defaultValue || 0) : value);
  },
  $ON_MSG_APP_READY : function(){
    $Fn($Fn(this.oApp.exec, this.oApp).bind("SHOW_LOC", []), this).attach(this.oLocTab, "mousedown");
    $Fn($Fn(this.oApp.exec, this.oApp).bind("SHOW_SRC", []), this).attach(this.oSrcTab, "mousedown");

    $Fn($Fn(this.oApp.exec, this.oApp).bind("ACCEPT_EMBED", []), this).attach(this.oAcceptButton, "click");
    $Fn($Fn(this.oApp.exec, this.oApp).bind("TOGGLE_TOOLBAR_ACTIVE_LAYER", [this.elDropdownLayer]), this).attach(this.oCancelButton, "click");

    this.oApp.exec("REGISTER_UI_EVENT", ["embed", "click", "TOGGLE_EMBED_LAYER"]);

    this.oApp.addConverter("HTMLSrc_TO_IR", this._embedConverter(this._convertRealToPseudo, "HTMLSrc_TO_IR"));  //textarea -> wysiwyg1 - html편집상태에서 본문HTML보기시 호출됩니다
    this.oApp.addConverter("IR_TO_WYSIWYG", this._embedConverter(this._convertRealToPseudo, "IR_TO_WYSIWYG"));  //textarea -> wysiwyg2
    this.oApp.addConverter("WYSIWYG_TO_IR", this._embedConverter(this._convertPseudoToReal, "WYSIWYG_TO_IR"));  //wysiwyg -> source
    this.oApp.addConverter("IR_TO_HTMLSrc", this._embedConverter(this._convertPseudoToReal, "IR_TO_HTMLSrc"));  //wysiwyg -> textarea
    this.oApp.setIR(this.oApp.applyConverter("IR_TO_WYSIWYG", this.oApp.getIR()));  //convert default value to pseudo code
  },
  $ON_TOGGLE_EMBED_LAYER : function(){
    this.oSelection = this.oApp.getSelection();
    this.oNode = this.oSelection.getStartNode();
    if(this.oNode && this.oSelection.getEndNode() == this.oNode) {
      //do nothing
    } else {
      this.oNode = null;
    }
    this._initializeEmbed();
    this.oApp.exec("TOGGLE_TOOLBAR_ACTIVE_LAYER", [this.elDropdownLayer]);
  },
  $ON_SHOW_LOC : function(){
    this.bLocationMode = true;
    $Element(this.oLocTab).addClass("on");
    $Element(this.oSrcTab).removeClass("on");

    $Element(this.elDropdownLayer).removeClass("src").addClass("loc");
  },
  $ON_SHOW_SRC : function(){
    this.bLocationMode = false;
    $Element(this.oSrcTab).addClass("on");
    $Element(this.oLocTab).removeClass("on");

    $Element(this.elDropdownLayer).removeClass("loc").addClass("src");
  },
  _acceptEmbed : function(source, name, isLoc) {
    var pseudoHtml = "", dimensions;
    var embedSrc = source.replace(/^\s+|\s+$/g, "");
    if(false !== isLoc) {
      if(false == /\.(wmv|asf|swf|mp3|wma)(\?|$)/i.test(embedSrc)) {
        alert(this.oApp.$MSG("SE_EmbedMedia.notSupportedFileType"));
        return;
      }
      var extension = RegExp.$1.toLowerCase();
      pseudoHtml = "<img src=\"img/blank.gif\"" + (name ? " name=\"" + name + "\"" : "") + " class=\"ext_embed_{ext}\" style=\"width:{width}px;height:{height}px;\" longdesc=\"{src}\">";
      switch(extension) {
        case "swf":
          dimension = [320, 320];
          break;
        case "asf":
          dimension = [320, 270];
          break;
        case "wmv":
          dimension = [320, 280];
          break;
        case "wma":
        case "mp3":
          dimension = [320, 70];
          break;
        default:
          dimension = [100, 100];
          break;
      }
    } else {
      if(false == /^(<object\s+[\s\S]+?<\/object>|<embed\s+[\s\S]+?(<\/embed>|>))$/i.test(embedSrc)) {
        alert(this.oApp.$MSG("SE_EmbedMedia.invalidEmbedSource"));
        return;
      }
      var dimension = [100,100];
      if(/width\s*=\s*(?:["'])?(\d+)/i.test(embedSrc)) dimension[0] = RegExp.$1;
      if(/height\s*=\s*(?:["'])?(\d+)/i.test(embedSrc)) dimension[1] = RegExp.$1;
      pseudoHtml = "<img src=\"img/blank.gif\" class=\"ext_embed_src\" style=\"width:{width}px;height:{height}px;\" longdesc=\"{src}\">";
    }
    pseudoHtml = pseudoHtml.replace(/\{([a-z]+)\}/ig, function($0,$1){
      switch($1) {
        case "ext" : return extension;
        case "src" : return encodeURIComponent(embedSrc);
        case "width" : return dimension[0];
        case "height" : return dimension[1];
      }
      return $0;
    });
    this.oApp.exec("PASTE_HTML", [pseudoHtml, this.oSelection || null]);
  },
  $ON_ACCEPT_EMBED : function(){
    if(arguments.length == 1) {
      this._acceptEmbed.apply(this, [(this.bLocationMode ? this.inputLoc.value : this.inputSrc.value), "", this.bLocationMode]);
      this.oApp.exec("TOGGLE_TOOLBAR_ACTIVE_LAYER", [this.elDropdownLayer]);
    } else {
      this._acceptEmbed.apply(this, arguments);
    }
  }
});