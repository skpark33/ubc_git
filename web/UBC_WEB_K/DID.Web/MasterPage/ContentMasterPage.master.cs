﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DID.Common.Framework;

public partial class MasterPage_ContentMasterPage : BaseMasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            foreach (Control ctl in this.ContentPlaceHolder1.Controls)
            {
                if (ctl.ID == "litMenuName")
                {
                    Literal litMenuName = ctl as Literal;
                    string prog_id = Request.Url.LocalPath.Substring(1, Request.Url.LocalPath.Length - 6).Replace("/", "\\");
                    litMenuName.Text = this.GetMenuName(prog_id,this.LANG_FLAG);
                }
            }
        }

    }

    public string GetMenuName(string prog_id, string lang_code)
    {
        using (DID.Service.Utility.Common obj = new DID.Service.Utility.Common())
        {

            return obj.GetMenuName(prog_id, lang_code);
        }

    }

}
