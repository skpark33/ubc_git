﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;

public partial class interActiveBySite : BasePage
{
    #region 그래프 관련 변수들

    protected string allCount = "0";
    protected string arrayTitle = "";
    protected string arrayString = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.InitControl();
        }
    }

    //초기화
    private void InitControl()
    {
        // fromdate 를 -7일 로 세팅한다.
        fromDate.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");

        // todate 를 오늘날짜로 세팅한다.
        toDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        // 조직콤보
        Common.BindDropDownList(ddlType, GetOrgCode(ROOT_SITE_ID), "전체");

    }


    //엑셀
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        

        string fileName = HttpUtility.UrlEncode("상호작용통계", new UTF8Encoding()) + ".xls";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-Disposition", "attachment;filename=" + fileName);
        Response.Charset = "";
        gvList.EnableViewState = false;
        System.IO.StringWriter tw = new System.IO.StringWriter();
        tw.WriteLine("");
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        gvList.RenderControl(hw);
        Response.Write(tw.ToString());
        Response.End();        




    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time. 
    }


    // 조회
    protected void btnSelect_Click(object sender, EventArgs e)
    {
        using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
        {
            // 기간
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            // 조직콤보에서 선택된 조직의 Path 문자열
            string siteIdPath = getSiteIdPath();

            DataTable dt = obj.GetInterActiveList(ROOT_SITE_ID, fromDateVar, toDateVar, siteIdPath, "", "1");
            this.gvList.DataSource = dt;
            this.gvList.DataBind();

            // 데이타가 있으면
            if (dt.Rows.Count > 0)
            {
                // 엑셀저장 버튼 활성
                btnExcel.Enabled = true;
                btnExcel.Visible = true;

                //  그래프 그려줌
                dt = obj.GetInterActiveGraph(ROOT_SITE_ID, fromDateVar, toDateVar, siteIdPath, "");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string category = "[" + dt.Rows[i]["category1"].ToString() + "]";

                    if (!dt.Rows[i]["category2"].ToString().IsNullOrEmpty())
                        category += "-[" + dt.Rows[i]["category2"].ToString() + "]";

                    if (!dt.Rows[i]["category3"].ToString().IsNullOrEmpty())
                        category += "-[" + dt.Rows[i]["category3"].ToString() + "]";

                    arrayString = arrayString +
                     "data.setValue(" + i + " , 0, '" + category + "'); " +
                     "data.setValue(" + i + " , 1, " + dt.Rows[i]["click_count"] + "); ";
                }

                allCount = dt.Rows.Count.ToString();
                arrayTitle = "카테고리별 이용횟수(TOP10)";

                // 그래프 데이타가 있으면
                if (dt.Rows.Count > 0)
                    PanelGraph.Visible = true;
                else
                    PanelGraph.Visible = false;
            }
            else
            {
                // 그래프 안보이게
                PanelGraph.Visible = false;

                // 엑셀저장 버튼 비활성
                btnExcel.Enabled = false;
                btnExcel.Visible = false;
            }

        }

    }

    // 조직 콤보의 값을 조합하여 siteIdPath를 만듬
    protected string getSiteIdPath()
    {
        const string SITE_DELIM = "/";

        StringBuilder selectedSiteId = new StringBuilder(SITE_DELIM + ROOT_SITE_ID);

        if (!ddlType.SelectedValue.IsNullOrEmpty())
        {
            selectedSiteId.Append(SITE_DELIM + ddlType.SelectedValue);

            if (!ddlClass1.SelectedValue.IsNullOrEmpty())
            {
                selectedSiteId.Append(SITE_DELIM + ddlClass1.SelectedValue);

                if (!ddlClass2.SelectedValue.IsNullOrEmpty())
                {
                    selectedSiteId.Append(SITE_DELIM + ddlClass2.SelectedValue);

                    if (!ddlClass3.SelectedValue.IsNullOrEmpty())
                    {
                        selectedSiteId.Append(SITE_DELIM + ddlClass3.SelectedValue);

                        if (!ddlClass4.SelectedValue.IsNullOrEmpty())
                        {
                            selectedSiteId.Append(SITE_DELIM + ddlClass4.SelectedValue);
                        }
                    }
                }
            }
        }

        return selectedSiteId.ToString();

    }

    // 그리드뷰 gvList_RowDataBound
    protected void gvList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            // 기간
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            // SITE ID            
            string parentSiteId = drv["siteId"].ToString();

            using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
            {
                DataTable dt = new DataTable();

                dt = obj.GetInterActiveList(ROOT_SITE_ID, fromDateVar, toDateVar, getSiteIdPath(), parentSiteId, "2");

                GridView gv = e.Row.FindControl("gvList2") as GridView;
                gv.DataSource = dt;
                gv.DataBind();
            }
        }
    }


    // 그리드뷰 gvList2_RowDataBound
    protected void gvList2_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            string siteId = drv["siteId"].ToString();

            // 토탈 ROW 일 경우
            if (siteId == "total")
            {
                // 글씨체 bold, 셀 배경색 변경
                e.Row.BackColor = System.Drawing.Color.LightYellow;
                e.Row.Font.Bold = true;

                // 상세내역보기 링크 안보이게 설정
                foreach (Control ctl in e.Row.Cells[3].Controls)
                {
                    ctl.Visible = false;
                }
            }
            else
            {
                DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics();
                DataTable dt = obj.GetInterActiveList(ROOT_SITE_ID, drv["fromDate"].ToString(), drv["toDate"].ToString(), drv["siteId"].ToString(), "", "3");

                GridView gv = e.Row.FindControl("gvList3") as GridView;
                gv.DataSource = dt;
                gv.DataBind();
            }

        }
    }

    #region SelectedIndexChanged 이벤트 핸들러
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass1.Visible = false;
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;
        ddlClass4.Visible = false;

        ddlClass1.Items.Clear();
        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();
        ddlClass4.Items.Clear();

        if (!ddlType.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlType.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass1.Visible = true;
                Common.BindDropDownList(ddlClass1, dt, "전체");
            }
        }

    }
    protected void ddlClass1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;
        ddlClass4.Visible = false;

        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();
        ddlClass4.Items.Clear();

        if (!ddlClass1.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass1.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass2.Visible = true;
                Common.BindDropDownList(ddlClass2, dt, "전체");
            }

        }

    }
    protected void ddlClass2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass3.Visible = false;
        ddlClass4.Visible = false;

        ddlClass3.Items.Clear();
        ddlClass4.Items.Clear();

        if (!ddlClass2.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass2.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass3.Visible = true;
                Common.BindDropDownList(ddlClass3, dt, "전체");
            }
        }
    }
    protected void ddlClass3_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass4.Visible = false;

        ddlClass4.Items.Clear();

        if (!ddlClass3.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass3.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass4.Visible = true;
                Common.BindDropDownList(ddlClass4, dt, "전체");
            }
        }
    }



    #endregion

    private DataTable GetOrgCode(string parentId)
    {
        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            return obj.GetOrgCode(parentId, this.ROOT_SITE_ID);
        }
    }

    protected void gvList_PreRender(object sender, EventArgs e)
    {

        Common.GroupColumn(gvList, 0); //그리드뷰의 ID, 병합할 컬럼

    }


}

