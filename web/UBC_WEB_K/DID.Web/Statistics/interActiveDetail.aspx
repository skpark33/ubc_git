﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/PopupMasterPage.master" AutoEventWireup="true" CodeFile="interActiveDetail.aspx.cs" Inherits="interActiveDetail" Title="메뉴별 상세내역" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<table class="boardListType2" cellpadding="0" cellspacing="0" border="0" >
<tr>
    <td><%= title %></td>
</tr>

<tr>
    <td align=center>
        <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" ShowHeader="TRUE" CssClass="boardListType3">
        <Columns>
            <asp:BoundField DataField="category1" HeaderText="1차메뉴" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category1" />
            <asp:BoundField DataField="category2" HeaderText="2차메뉴" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category2" />
            <asp:BoundField DataField="category3" HeaderText="3차메뉴" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category3" />
            <asp:BoundField DataField="category4" HeaderText="4차메뉴" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category4" />
            <asp:BoundField DataField="category5" HeaderText="5차메뉴" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category5" />
            <asp:BoundField DataField="touch_cnt" HeaderText="조회수" HeaderStyle-HorizontalAlign=Center HeaderStyle-Width="60px" ItemStyle-HorizontalAlign=Right SortExpression="touch_cnt" />
        </Columns>
        </asp:GridView>
     </td>
 </tr>
 
<tr>
<td align=center >
    <br /><asp:Button ID="btnClose" runat="server" Text="닫기" OnClientClick="self.close();" /><br />
</td> 
</tr>

</table>
     
    
</asp:Content>

