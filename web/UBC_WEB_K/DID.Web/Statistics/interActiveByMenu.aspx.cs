﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;

using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

public partial class interActiveByMenu : BasePage
{
    protected const string TOTAL_VISITOR_STRING = "총방문자";

    #region 그래프 관련 변수들

    protected string allCount = "0";
    protected string arrayTitle = "";
    protected string arrayString = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.InitControl();
        }
    }

    //초기화
    private void InitControl()
    {
        // fromdate 를 -7일 로 세팅한다.
        fromDate.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");

        // todate 를 오늘날짜로 세팅한다.
        toDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        // 조직콤보
        Common.BindDropDownList(ddlType, GetOrgCode(ROOT_SITE_ID), "전체");

        // Keyword 타입 콤보 초기화
        ddlKeywordType.Items.Add(new ListItem("1차메뉴별"));
        ddlKeywordType.Items.Add(new ListItem("2차메뉴별"));
        ddlKeywordType.Items.Add(new ListItem("3차메뉴별")); // gwangsoo
        ddlKeywordType.Items.Add(new ListItem("4차메뉴별")); // gwangsoo 2012.04.17
        ddlKeywordType.Items.Add(new ListItem("5차메뉴별")); // gwangsoo 2012.04.17

        DataTable dt = GetKeywordCode("", "", "", "");

        // 1차 Keyword  콤보 초기화
        ddlKeyword1.DataSource = dt;
        ddlKeyword1.DataBind();

        // 2차 Keyword  콤보 초기화
        ddlKeyword2.DataSource = dt;
        ddlKeyword2.DataBind();

        // 3차 Keyword  콤보 초기화 gwangsoo 2012.04.17
        ddlKeyword3.DataSource = dt;
        ddlKeyword3.DataBind();

        // 4차 Keyword  콤보 초기화 gwangsoo 2012.04.17
        ddlKeyword4.DataSource = dt;
        ddlKeyword4.DataBind();

        // 체크박스 리스트 초기화
        cblKeyword.DataSource = dt;
        cblKeyword.DataBind();
        foreach (ListItem item in cblKeyword.Items) item.Selected = true;
    }


    //엑셀
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        string fileName = this.Title + "_" + this.selectDate.Text.Replace(" ", "") + ".xls";
        fileName = fileName.Replace(' ', '_');

        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
        Response.Charset = "";
        this.EnableViewState = false;

        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n");

        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
        gvList.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time. 
    }


    // 조회
    protected void btnSelect_Click(object sender, EventArgs e)
    {
        using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
        {
            // 기간
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            // 조직콤보에서 선택된 조직의 Path 문자열
            string searchSiteId = getSiteIdPath();

            DataTable dt = getStatistics(fromDate.Text, toDate.Text, searchSiteId);

            this.gvList.DataSource = dt;
            this.gvList.DataBind();

            // 데이타가 있으면
            if (dt != null && dt.Rows.Count > 0)
            {
                // 엑셀저장 버튼 활성
                btnExcel.Enabled = true;
                btnExcel.Visible = true;
            }
            else
            {
                // 엑셀저장 버튼 비활성
                btnExcel.Enabled = false;
                btnExcel.Visible = false;
            }

        }

    }

    // 조직 콤보의 값을 조합하여 siteIdPath를 만듬
    protected string getSiteIdPath()
    {
        if (ddlClass4.Visible && !ddlClass4.SelectedValue.IsNullOrEmpty()) return ddlClass4.SelectedValue;
        if (ddlClass3.Visible && !ddlClass3.SelectedValue.IsNullOrEmpty()) return ddlClass3.SelectedValue;
        if (ddlClass2.Visible && !ddlClass2.SelectedValue.IsNullOrEmpty()) return ddlClass2.SelectedValue;
        if (ddlClass1.Visible && !ddlClass1.SelectedValue.IsNullOrEmpty()) return ddlClass1.SelectedValue;
        if (ddlType.Visible && !ddlType.SelectedValue.IsNullOrEmpty()) return ddlType.SelectedValue;

        return ROOT_SITE_ID;        
    }

    // 그리드뷰 gvList_RowDataBound
    protected void gvList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Width = 140;
            e.Row.Cells[1].Width = 65;
            e.Row.Cells[2].Width = 100;

            //for (int i = 2; i < e.Row.Cells.Count; i++)
            //{
            //    e.Row.Cells[i].Width = 65;
            //}
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            // SITE
            string siteName = drv["조직"].ToString();
            int nLevel = siteName.Length - siteName.TrimStart(' ').Length;

            siteName = siteName.TrimStart(' ');
            for (int i = 0; i < nLevel; i++)
            {
                siteName = "&nbsp;" + "&nbsp;" + siteName;
            }

            e.Row.Cells[0].Text = siteName;

            System.Drawing.Color backColor = System.Drawing.Color.FromArgb(0xFFFFFF);

            if (nLevel == 0) backColor = System.Drawing.Color.FromArgb(0xFF, 0xCC, 0x00);//0xFF, 0x99, 0x00);
            else if (nLevel == 1) backColor = System.Drawing.Color.FromArgb(0xFF, 0xFF, 0x66);
            else if (nLevel == 2) backColor = System.Drawing.Color.FromArgb(0xFF, 0xFF, 0xCC);

            e.Row.BackColor = backColor;

            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if (i == 0)
                {
                    e.Row.Cells[i].Font.Bold = true;
                }
                else if (i > 1)
                {
                    e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                }
            }
        }
    }

    private DataTable getStatistics(string fromDateVar, string toDateVar, string searchSiteId)
    {
        /////////////////////////////////////////////////////////////////////////////////////////////
        // 조회대상 메뉴
        /////////////////////////////////////////////////////////////////////////////////////////////

        // 체크박스 선택된 메뉴만 리스트로 만든다        
        ArrayList keywordList = new ArrayList();
        //foreach (ListItem item in cblKeyword.Items) if (item.Selected) keywordList.Add(item.Value.Replace(" ", ""));
        foreach (ListItem item in cblKeyword.Items) if (item.Selected) keywordList.Add(item.Value);

        /////////////////////////////////////////////////////////////////////////////////////////////
        // 메인 쿼리 만들기 시작
        /////////////////////////////////////////////////////////////////////////////////////////////
        
        StringBuilder sbSql = new StringBuilder();
        sbSql.AppendLine(string.Format(@"select SPACE(MAX(S.lvl-1)) + MAX(S.siteName) '{0}', MAX(S.siteId) ", "조직"));
//--, MAX(S.lvl)            lvl
//--, COUNT(L.hostId)         hostCnt
        sbSql.AppendLine(string.Format(@", ISNULL(dbo.GetHostName(MAX(S.hostId)), MAX(S.hostId)) '{0}' ", "단말"));
        // 1차메뉴별
        if (ddlKeywordType.SelectedIndex == 0)
            sbSql.AppendLine(string.Format(@", SUM(L.[{0}]) '{0}' ", TOTAL_VISITOR_STRING));
        sbSql.AppendLine(string.Format(@", SUM(L.[{0}]) '{0}' ", "페이지뷰"));
        foreach (string keyword in keywordList)
        {
            sbSql.AppendLine(string.Format(@", SUM(L.[{0}]) '{1}' ", keyword.Replace(" ", ""), keyword));
        }
        sbSql.AppendLine(@" from ( select distinct S.id_path, S.siteId, S.siteName, S.lvl, S.id_path + '/' + ISNULL(H.hostId, '') hostPath, hostId ");
        sbSql.AppendLine(@"       from GetChildSite(@franchizeType,@searchSiteId) S LEFT OUTER JOIN ubc_host H on S.siteId = H.siteId ) S ");
        sbSql.AppendLine(@" ,(	  select PV.id_path, PV.hostPath, PV.hostId ");
        // 1차메뉴별
        if (ddlKeywordType.SelectedIndex == 0)
            sbSql.AppendLine(string.Format(@", ISNULL([{0}], 0) '{0}' ", TOTAL_VISITOR_STRING));

        string pageView = "";
        foreach (string keyword in keywordList)
        {
            sbSql.AppendLine(string.Format(@", ISNULL([{0}], 0) '{0}'", keyword.Replace(" ", "")));
            pageView += (pageView.Length > 0 ? "+" : "") + string.Format(@"ISNULL([{0}], 0)", keyword.Replace(" ", ""));
        }
        sbSql.AppendLine(string.Format(@" ,({0}) '{1}' ", pageView, "페이지뷰"));
        sbSql.AppendLine(@" from ( select max(s.id_path) + '/' id_path, max(s.id_path) + '/' + l.hostId hostPath, l.hostId, ");
        sbSql.AppendLine(string.Format(@" replace(l.keyword{0}, ' ', '' ) as keyword, sum(l.counter) as cnt ", ddlKeywordType.SelectedIndex + 1));
        sbSql.AppendLine(@" from ubc_interactivelog l, dbo.GetChildSite(@franchizeType, @searchSiteId) s ");
        sbSql.AppendLine(@" where l.playDate between @fromDate AND @toDate and l.siteId = s.siteId ");

        // 1차메뉴별
        if (ddlKeywordType.SelectedIndex == 0)
            ;//sbSql.AppendLine(@"       AND keyword1 IN (" + searchKeywordList + ")");
        // 2차메뉴별
        else if (ddlKeywordType.SelectedIndex == 1)
        {
            sbSql.AppendLine(@"       AND keyword1 =  '" + ddlKeyword1.SelectedValue + "' ");
            //sbSql.AppendLine(@"       AND keyword2 IN (" + searchKeywordList + ")");
        }
        // 3차메뉴별
        else if (ddlKeywordType.SelectedIndex == 2)
        {
            sbSql.AppendLine(@"       AND keyword1 =  '" + ddlKeyword1.SelectedValue + "' ");
            sbSql.AppendLine(@"       AND keyword2 =  '" + ddlKeyword2.SelectedValue + "' ");
            //sbSql.AppendLine(@"       AND keyword3 IN (" + searchKeywordList + ")");
        }
        // 4차메뉴별 gwangsoo 2012.04.17
        else if (ddlKeywordType.SelectedIndex == 3)
        {
            sbSql.AppendLine(@"       AND keyword1 =  '" + ddlKeyword1.SelectedValue + "' ");
            sbSql.AppendLine(@"       AND keyword2 =  '" + ddlKeyword2.SelectedValue + "' ");
            sbSql.AppendLine(@"       AND keyword3 =  '" + ddlKeyword3.SelectedValue + "' ");
            //sbSql.AppendLine(@"       AND keyword4 IN (" + searchKeywordList + ")");
        }
        // 5차메뉴별 gwangsoo 2012.04.17
        else
        {
            sbSql.AppendLine(@"       AND keyword1 =  '" + ddlKeyword1.SelectedValue + "' ");
            sbSql.AppendLine(@"       AND keyword2 =  '" + ddlKeyword2.SelectedValue + "' ");
            sbSql.AppendLine(@"       AND keyword3 =  '" + ddlKeyword3.SelectedValue + "' ");
            sbSql.AppendLine(@"       AND keyword4 =  '" + ddlKeyword4.SelectedValue + "' ");
            //sbSql.AppendLine(@"       AND keyword5 IN (" + searchKeywordList + ")");
        }

        sbSql.AppendLine(string.Format(@" group by l.hostId, l.keyword{0} ) data ", ddlKeywordType.SelectedIndex + 1));

        pageView = "";

        // 1차메뉴별
        if (ddlKeywordType.SelectedIndex == 0)
            pageView = TOTAL_VISITOR_STRING;
        foreach (string keyword in keywordList)
        {
            pageView += (pageView.Length > 0 ? "," : "") + "[" + keyword.Replace(" ", "") + "]";
        }
	    sbSql.AppendLine(string.Format(@" PIVOT (SUM(cnt) FOR keyword IN ({0})) PV ) L where L.hostPath like '%' + S.hostPath + '%' GROUP BY S.hostPath ", pageView));

        Database db = DatabaseFactory.CreateDatabase();
        DbCommand Cmd = db.GetSqlStringCommand(sbSql.ToString());
        db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDateVar);
        db.AddInParameter(Cmd, "@toDate", DbType.String, toDateVar);
        db.AddInParameter(Cmd, "@searchSiteId", DbType.String, searchSiteId);
        db.AddInParameter(Cmd, "@franchizeType", DbType.String, ROOT_SITE_ID);

        selectDate.Text = fromDateVar + " ~ " + toDateVar;

        debug.Visible = false;
        
        DataTable result = null;
        try
        {
            Cmd.CommandTimeout = 300;
            result = db.ExecuteDataSet(Cmd).Tables[0];
        }
        catch (Exception e)
        {
            //debug.Visible = true;
            //debug.Text = fromDateVar + " : " + toDateVar + " : " + searchSiteId + " : " + ROOT_SITE_ID + " " + sbSql.ToString();
        }

        return result;
    }

    #region 조직 SelectedIndexChanged 이벤트 핸들러
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass1.Visible = false;
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;
        ddlClass4.Visible = false;

        ddlClass1.Items.Clear();
        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();
        ddlClass4.Items.Clear();

        if (!ddlType.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlType.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass1.Visible = true;
                Common.BindDropDownList(ddlClass1, dt, "전체");
            }
        }
        
    }
    protected void ddlClass1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;
        ddlClass4.Visible = false;

        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();
        ddlClass4.Items.Clear();

        if (!ddlClass1.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass1.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass2.Visible = true;
                Common.BindDropDownList(ddlClass2, dt, "전체");
            }

        }

    }
    protected void ddlClass2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass3.Visible = false;
        ddlClass4.Visible = false;

        ddlClass3.Items.Clear();
        ddlClass4.Items.Clear();

        if (!ddlClass2.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass2.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass3.Visible = true;
                Common.BindDropDownList(ddlClass3, dt, "전체");
            }
        }
    }
    protected void ddlClass3_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass4.Visible = false;

        ddlClass4.Items.Clear();

        if (!ddlClass3.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass3.SelectedValue);           

            if (dt.Rows.Count > 0)
            {
                ddlClass4.Visible = true;
                Common.BindDropDownList(ddlClass4, dt, "전체");
            }
        }
    }



    #endregion

    #region 메뉴 SelectedIndexChanged 이벤트 핸들러

    protected void ddlKeywordType_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 1차메뉴별
        if (ddlKeywordType.SelectedIndex == 0)
        {
            ddlKeyword1.Visible = false;
            ddlKeyword2.Visible = false;
            ddlKeyword3.Visible = false;
            ddlKeyword4.Visible = false;

            // 체크박스 리스트 1차 keyword 로 초기화 
            DataTable dt = GetKeywordCode("", "", "", "");            
            cblKeyword.DataSource = dt;
            cblKeyword.DataBind();
            foreach (ListItem item in cblKeyword.Items) item.Selected = true;
        }
        //// 2차메뉴별
        //else if (ddlKeywordType.SelectedIndex == 1)
        //{
        //    ddlKeyword1.Visible = true;
        //    ddlKeyword1_SelectedIndexChanged(null, null);
        //}
        //// 3차메뉴별
        //else if (ddlKeywordType.SelectedIndex == 2)
        //{
        //    ddlKeyword1.Visible = true;
        //    ddlKeyword2.Visible = true;
        //    ddlKeyword1_SelectedIndexChanged(null, null);
        //}
        //// 4차메뉴별
        //else if (ddlKeywordType.SelectedIndex == 3)
        //{
        //    ddlKeyword1.Visible = true;
        //    ddlKeyword2.Visible = true;
        //    ddlKeyword3.Visible = true;
        //    ddlKeyword1_SelectedIndexChanged(null, null);
        //}
        else
        {
            ddlKeyword1.Visible = (ddlKeywordType.SelectedIndex > 0);
            ddlKeyword2.Visible = (ddlKeywordType.SelectedIndex > 1);
            ddlKeyword3.Visible = (ddlKeywordType.SelectedIndex > 2);
            ddlKeyword4.Visible = (ddlKeywordType.SelectedIndex > 3);
            ddlKeyword1_SelectedIndexChanged(null, null);
        }
    }

    protected void ddlKeyword1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlKeywordType.SelectedIndex == 0 || 
            ddlKeywordType.SelectedIndex == 1 )
        {
            // 체크박스 리스트 1차 keyword 로 초기화 
            DataTable dt = GetKeywordCode(ddlKeyword1.SelectedValue, "", "", "");
            cblKeyword.DataSource = dt;
            cblKeyword.DataBind();
            foreach (ListItem item in cblKeyword.Items) item.Selected = true;
        }
        else
        {
            // 1차 Keyword  콤보 초기화
            DataTable dt = GetKeywordCode(ddlKeyword1.SelectedValue, "", "", "");
            ddlKeyword2.DataSource = dt;
            ddlKeyword2.DataBind();
            ddlKeyword2_SelectedIndexChanged(null, null);
        }
    }

    protected void ddlKeyword2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlKeywordType.SelectedIndex == 0 ||
            ddlKeywordType.SelectedIndex == 1 ||
            ddlKeywordType.SelectedIndex == 2)
        {
            // 체크박스 리스트 2차 keyword 로 초기화
            DataTable dt = GetKeywordCode(ddlKeyword1.SelectedValue, ddlKeyword2.SelectedValue, "", "");
            cblKeyword.DataSource = dt;
            cblKeyword.DataBind();
            foreach (ListItem item in cblKeyword.Items) item.Selected = true;
        }
        else
        {
            // 2차 Keyword  콤보 초기화
            DataTable dt = GetKeywordCode(ddlKeyword1.SelectedValue, ddlKeyword2.SelectedValue, "", "");
            ddlKeyword3.DataSource = dt;
            ddlKeyword3.DataBind();
            ddlKeyword3_SelectedIndexChanged(null, null);
        }
    }

    // gwangsoo 2012.04.17
    protected void ddlKeyword3_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlKeywordType.SelectedIndex == 0 ||
            ddlKeywordType.SelectedIndex == 1 ||
            ddlKeywordType.SelectedIndex == 2 ||
            ddlKeywordType.SelectedIndex == 3)
        {
            // 체크박스 리스트 3차 keyword 로 초기화
            DataTable dt = GetKeywordCode(ddlKeyword1.SelectedValue, ddlKeyword2.SelectedValue, ddlKeyword3.SelectedValue, "");
            cblKeyword.DataSource = dt;
            cblKeyword.DataBind();
            foreach (ListItem item in cblKeyword.Items) item.Selected = true;
        }
        else
        {
            // 3차 Keyword  콤보 초기화
            DataTable dt = GetKeywordCode(ddlKeyword1.SelectedValue, ddlKeyword2.SelectedValue, ddlKeyword3.SelectedValue, "");
            ddlKeyword4.DataSource = dt;
            ddlKeyword4.DataBind();
            ddlKeyword4_SelectedIndexChanged(null, null);
        }
    }

    // gwangsoo 2012.04.17
    protected void ddlKeyword4_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 체크박스 리스트 4차 keyword 로 초기화
        DataTable dt = GetKeywordCode(ddlKeyword1.SelectedValue, ddlKeyword2.SelectedValue, ddlKeyword3.SelectedValue, ddlKeyword4.SelectedValue);
        cblKeyword.DataSource = dt;
        cblKeyword.DataBind();
        foreach (ListItem item in cblKeyword.Items) item.Selected = true;
    }

    #endregion
    
    private DataTable GetOrgCode(string parentId)
    {
        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            return obj.GetOrgCode(parentId, this.ROOT_SITE_ID);
        }
    }

    private DataTable GetKeywordCode(string keyword1, string keyword2, string keyword3, string keyword4)
    {
        using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
        {
            return obj.GetInterActiveKeyword(ROOT_SITE_ID, TOTAL_VISITOR_STRING, keyword1, keyword2, keyword3, keyword4);
        }
    }

    protected void gvList_PreRender(object sender, EventArgs e)
    {

        Common.GroupColumn(gvList, 0); //그리드뷰의 ID, 병합할 컬럼

    }    
}

