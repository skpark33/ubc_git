﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="interActiveBySite.aspx.cs" Inherits="interActiveBySite" Title="제목 없음"  EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<!-- 구글 파이챠트 시작-->

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);      
    
    function drawChart() 
    {
        // 차트 데이타 형식
        // data.setValue(0, 0, '항목1');
        // data.setValue(0, 1, 갯수); 
        // data.setValue(1, 0, 'host_process_down');
        // data.setValue(1, 1, 2); 
        // data.setValue(2, 0, 'host_hdd_overload');
        // data.setValue(2, 1, 2);
        // 참고 : http://code.google.com/apis/ajax/playground/     
        
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', '항목');
        data.addColumn('number', '갯수');
        data.addRows(<%=this.allCount %>);              
              
        <%= this.arrayString %>                                 
                         

        // Create and draw the visualization.
        //new google.visualization.PieChart(document.getElementById('visualization')).draw(data, {title:"<%=this.arrayTitle %>"});
        
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, {width: 820, height: 400, title: '<%=this.arrayTitle %>'});
        
    }   
    
</script>

<!-- 구글 파이챠트 끝-->

<script language="javascript" type="text/javascript">

    function DoShowDetail(siteId, siteName, fromDate, toDate) {

        window.open("./interActiveDetail.aspx?siteId=" + siteId.toString() + "&siteName=" + siteName.toString() + "&fromDate=" + fromDate.toString() + "&toDate=" + toDate.toString(), "interActiveDetail", "toolbar=0,scrollbars=yes,location=0,directories=0,status=0,menubar=0,resizable=no,width=800,height=400");
    }
    
    function DoSelect() {

        if (!confirm('조회 조건에 따라 조회 시간이 오래 걸릴 수 있습니다.\n빠른 조회를 원하시면 조회 조건을 구체화 해주세요.\n계속 하시겠습니까?  ')) return false
        
        // 날짜 유효성 검사 시작

        var strStart_date = $("#<%= fromDate.ClientID%>").val();
        var strEnd_Date = $("#<%= toDate.ClientID%>").val();

        if (!checkDateFromTo(strStart_date, strEnd_Date)) return;

        document.getElementById('Panel1').style.display = 'block';

        document.getElementById('dataPanel').style.display = 'none';  
    }
    
    
</script>

	
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
<table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
<colgroup>
<col width="auto" />
<col width="50%" />
</colgroup>
<tr>
    <td><span class="subTitle"><span id=""><img src="/Asset/Images/body/bullet01.gif" /> 상호 작용 통계</span></span> </td>
</tr>
</table>

<div class='topOptionBtns'>
    <asp:ImageButton ID="btnSelect" runat="server" ToolTip="조회" OnClick="btnSelect_Click" OnClientClick='return DoSelect()' ImageUrl="~/Asset/Images/button/btn_search.jpg"  />
    <asp:ImageButton ID="btnExcel"  runat="server" ToolTip="엑셀저장" OnClick="btnExcel_Click"  ImageUrl="~/Asset/Images/button/btn_excel.jpg" Visible=false />
</div>    

<table class="boardInputType mb10" cellpadding="0" cellspacing="0" border="0">
	<colgroup>		
	<col width="80px" />
	<col width="250px" />
	<col width="80px" />
	<col width="auto" />	
</colgroup>
<tr>
   <th><span>기간</span></th>
    <td>        
      <asp:TextBox ID="fromDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />
        ~
      <asp:TextBox ID="toDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />                   
    </td>   
    <th><span>조직</span></th>  
    <td>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode=Conditional ChildrenAsTriggers=true>
        <ContentTemplate>            
            <asp:DropDownList ID="ddlType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlClass1" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass1_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlClass2" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass2_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlClass3" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass3_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlClass4" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" />
        </ContentTemplate>
        </asp:UpdatePanel>
    </td>                     
</tr>
</table>
    
<div ID="Panel1" style="display: none;">
   
    <table style="width:100%;height:290px;">
    <tr>
        <td style="text-align:center">
        <asp:Image ID="Image2" runat="server"  ImageUrl="~/Asset/Images/loading.gif" />
        <br />
        Processing.... please wait...
        
        </td>
    </tr>
    </table>

</div>  

<div id="dataPanel" class="container" style="display: block;">
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode=Conditional>
    <ContentTemplate>  
            
    <asp:GridView ID="gvList" runat="server"  OnPreRender="gvList_PreRender" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false" 
          ShowHeader="TRUE" CssClass="boardListType1">
    <Columns>
      
        <asp:BoundField DataField="s_date" HeaderText="조회기간" HeaderStyle-Width="170px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="s_date" />
        <asp:BoundField DataField="siteName" HeaderText="조직구분" HeaderStyle-Width="110px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="siteName" />            
        <asp:BoundField DataField="cnt_visit" HeaderText="총 방문자수" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="cnt_visit" />
        <asp:BoundField DataField="cnt_view" HeaderText="총 페이지뷰" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="cnt_view"   />
        
        <asp:TemplateField HeaderText="상세내역">
        <ItemTemplate>

            <asp:GridView runat="server" ID="gvList2" OnRowDataBound="gvList2_RowDataBound"  AutoGenerateColumns="false" ShowHeader="true" CssClass="boardListType2">
            <Columns>
            
                <asp:BoundField DataField="siteName" HeaderText="소속" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign=Left ItemStyle-VerticalAlign=Middle SortExpression="siteName" />
                <asp:BoundField DataField="cnt_visit" HeaderText="총 방문자수" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Right ItemStyle-VerticalAlign=Middle SortExpression="cnt_visit" />
                <asp:BoundField DataField="cnt_view" HeaderText="총 페이지뷰" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Right ItemStyle-VerticalAlign=Middle SortExpression="cnt_view"   />
                
                <asp:TemplateField HeaderText="메뉴별 상세내역" ItemStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <span style="cursor: pointer; border-bottom:solid 1px;" onclick="$(this).parents(2).next().toggle(); return false;">상세내역보기</span>
                    </td></tr><tr style="display: none;"><td colspan=4>
                        
                        <asp:GridView ID="gvList3" runat="server" AutoGenerateColumns="false" ShowHeader="TRUE" CssClass="boardListType3">
                        <Columns>
                            <asp:BoundField DataField="category1" HeaderText="1차메뉴" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category1" />
                            <asp:BoundField DataField="category2" HeaderText="2차메뉴" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category2" />
                            <asp:BoundField DataField="category3" HeaderText="3차메뉴" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category3" />
                            <asp:BoundField DataField="touch_cnt" HeaderText="조회수" HeaderStyle-HorizontalAlign=Center HeaderStyle-Width="60px" ItemStyle-HorizontalAlign=Right SortExpression="touch_cnt" />
                        </Columns>
                        <EmptyDataTemplate>
                            <table style="width:100%;">
                                <tr>
                                    <td style="text-align:center">
                                    조회된 데이터가 없습니다.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        </asp:GridView>
                        
                 </ItemTemplate>
                 </asp:TemplateField>
                 
            </Columns> 
            </asp:GridView>

        </ItemTemplate>            
        </asp:TemplateField>
         
    </Columns> 
    <EmptyDataTemplate>
        <table style="width:100%;height:290px;">
            <tr>
                <td style="text-align:center">
                조회된 데이터가 없습니다.
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    
    </asp:GridView>
    
   </ContentTemplate>
   </asp:UpdatePanel>
        
    <!-- 그래프  -->
    <asp:Panel ID="PanelGraph" runat="server" Visible=false>
        <div id="chart_div" style="width: 820px; height: 400px;"></div>
    </asp:Panel>
    
</div>

</asp:Content>