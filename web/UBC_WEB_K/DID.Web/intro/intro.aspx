﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true"
    CodeFile="intro.aspx.cs" Inherits="intro_intro"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
   <!-- contents -->
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
      
      <!-- 뉴스 -->
        <div class="board_area">
        <div class="pad01"> <span class="float_L"><img src="/Asset/Images/download/board_txt.png" /></span><span class="float_R"><a href="#" onclick="javascript:parent.Tabs_On('Community/Notice_List', '공지사항', 'Community/Notice_list.aspx',true);" >more</a></span> </div>
        <table width="92%" border="0" cellspacing="0" cellpadding="0">
          <caption>
          </caption>
          <colgroup>
          <col width="80%">
          <col width="20%">
          </colgroup>
          <tbody>
        <asp:Repeater ID="rptNotice" runat="server">
        <ItemTemplate>
          <tr>
            <td class="pad_T3B3" style="float:left">
                <span style="cursor:pointer" style="float:left" onclick="javascript:parent.Tabs_On('Community/Notice_List', '공지사항 보기', 'Community/Notice_View.aspx?boardId=<%#Eval("boardId") %>&pgn=1',true);" >
                    <img src="/Asset/Images/agronet/bullet_list.gif" align="absmiddle"/> 
                    <%#Util.DB2HTML(Eval("title").ToString()) + Common.GetNewIcon(Eval("createDate"))%>
                </span>
            </td>
            <td><%#Eval("createDate", "{0:yyyy-MM-dd}")%></td> 
          </tr>
          <tr>
            <td height="1" colspan="2" background="Asset/Images/agronet/dot_hor.gif"></td>
          </tr>
        </ItemTemplate>
        </asp:Repeater>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td align="center">
      
      <!-- 다운 -->
        <div class="down_area">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <caption>
            </caption>
            <colgroup>
            <col width="33%">
            <col width="33%">
            <col width="33%">
            </colgroup>
            <tr>
              <td align="left" valign="top" height="330">
              
              <!-- 플레이어 다운로드 -->
                <div class="mnal_area01">
                  <ul>
                    <li> <img src="/Asset/Images/download/mual_txt02.png" /> </li>
                    <li class="pad_T30 align_C"> 
                    <% if (this.UBC_PLAYER != null && this.UBC_PLAYER.Length > 0) {%>
                        <span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%= this.UBC_PLAYER %>&boardMasterId=<%=(int)Common.BoardMasterID.Program %>'">
                    <% } else {%>
                        <span style="cursor:pointer" onclick="javascript:alert('등록된 파일이 없습니다.'); return false;">
                    <% }%>
                            <img src="/Asset/Images/download/mual_bt01.gif" />
                        </span>   
                    </li>
                    <li style="background-image:url(Asset/Images/agronet/dot_hor.gif); height:1px;"> </li>
                    <li> UBC에 탑재되어 방송 시나리오에 <br />
                      따라 동영상, 이미지, 텍스트, 플래쉬, 웹 등의 멀티미디어 컨텐츠를 방송합니다. </li>
                  </ul>
                </div></td>
              <td align="left" valign="top">
              
              <!-- 스튜디오 다운로드 -->
                <div class="mnal_area03">
                  <ul>
                    <li><img src="/Asset/Images/download/mual_txt03.png" /></li>
                    <li class="pad_T30 align_C"> 
                    <% if (this.UBC_STUDIO != null && this.UBC_STUDIO.Length > 0) {%>
                        <span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%= this.UBC_STUDIO %>&boardMasterId=<%=(int)Common.BoardMasterID.Program %>'">
                    <% } else {%>
                        <span style="cursor:pointer" onclick="javascript:alert('등록된 파일이 없습니다.'); return false;">
                    <% }%>
                        <img src="/Asset/Images/download/mual_bt03.gif" />
                      </sapn> 
                     </li>
                    <li style="background-image:url(Asset/Images/agronet/dot_hor.gif); height:1px;"> </li>
                    <li> UBC  스튜디오는 UBC에 방송될 광고 
                      컨텐츠의 방송 시나리오를 기획하는 툴
                      입니다. </li>
                  </ul>
                </div></td>
              <td align="left" valign="top">
              
              <!-- 매니저 다운로드 -->
                <div class="mnal_area02">
                  <ul>
                    <li><img src="/Asset/Images/download/mual_txt04.png" /></li>
                    <li class="pad_T30 align_C"> 
                    <% if (this.UBC_MANAGER != null && this.UBC_MANAGER.Length > 0) {%>
                        <span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%= this.UBC_MANAGER %>&boardMasterId=<%=(int)Common.BoardMasterID.Program %>'">
                    <% } else {%>
                        <span style="cursor:pointer" onclick="javascript:alert('등록된 파일이 없습니다.'); return false;">
                    <% }%>
                          <img src="/Asset/Images/download/mual_bt02.gif" />
                          </span>
                    
                     </li>
                    <li style="background-image:url(Asset/Images/agronet/dot_hor.gif); height:1px;"> </li>
                    <li> UBC 매니저는 방송상태 및 시스템 <br />
                      상태를 감시하고, 제어하는 툴입니다. </li>
                  </ul>
                </div></td>
            </tr>
            <tr>
              <td colspan="3" align="left">
              
              <!-- 매뉴얼 다운로드 -->
              <div class="mnal_area04"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/Asset/Images/download/mual_txt01.png" />
                  <div class=" pad_T20">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <caption>
                      </caption>
                      <colgroup>
                      <col width="217">
                      <col width="1">
                      <col width="">
                      </colgroup>
                      <tbody>
                        <tr>
                          <td class="pad_R10">
                    <% if (this.UBC_PLAYER_MANUAL != null && this.UBC_PLAYER_MANUAL.Length > 0) {%>
                            <span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%= this.UBC_PLAYER_MANUAL %>&boardMasterId=<%=(int)Common.BoardMasterID.Manual %>'">
                    <% } else {%>
                        <span style="cursor:pointer" onclick="javascript:alert('등록된 파일이 없습니다.'); return false;">
                    <% }%>
                            <img src="/Asset/Images/download/mual_bt01.gif" /></span> </td>
                          <td width="1" background="/Asset/Images/agronet/dot_ver.gif"></td>
                          <td class="pad_L10">UBC 플레이어를 현장에서 조작하는데 필요한 정보를 제공합니다.</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="pad_T10">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <caption>
                      </caption>
                      <colgroup>
                      <col width="217">
                      <col width="1">
                      <col width="">
                      </colgroup>
                      <tbody>
                        <tr>
                          <td class="pad_R10">
                    <% if (this.UBC_STUDIO_MANUAL != null && this.UBC_STUDIO_MANUAL.Length > 0) {%>
                            <span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%= this.UBC_STUDIO_MANUAL %>&boardMasterId=<%=(int)Common.BoardMasterID.Manual %>'">
                    <% } else {%>
                        <span style="cursor:pointer" onclick="javascript:alert('등록된 파일이 없습니다.'); return false;">
                    <% }%>
                            <img src="/Asset/Images/download/mual_bt03.gif" /></span> </td>
                          <td width="1" background="/Asset/Images/agronet/dot_ver.gif"></td>
                          <td class="pad_L10">UBC 스튜디오의 소개 및 기능에 대한 정보를 제공합니다.</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="pad_T10">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <caption>
                      </caption>
                      <colgroup>
                      <col width="217">
                      <col width="1">
                      <col width="">
                      </colgroup>
                      <tbody>
                        <tr>
                          <td class="pad_R10">
                    <% if (this.UBC_MANAGER_MANUAL != null && this.UBC_MANAGER_MANUAL.Length > 0) {%>
                            <span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%= this.UBC_MANAGER_MANUAL %>&boardMasterId=<%=(int)Common.BoardMasterID.Manual %>'">
                    <% } else {%>
                        <span style="cursor:pointer" onclick="javascript:alert('등록된 파일이 없습니다.'); return false;">
                    <% }%>
                            <img src="/Asset/Images/download/mual_bt02.gif" /></span> </td>
                          <td width="1" background="/Asset/Images/agronet/dot_ver.gif"></td>
                          <td class="pad_L10">UBC 매니저의 소개 및 기능에 대한 정보를 제공합니다.</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div></td>
            </tr>
          </table>
        </div></td>
    </tr>
  </table>
  <!-- /뉴스 -->
  <!-- //contents -->


</asp:Content>
