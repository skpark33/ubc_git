﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.ComponentModel;
using System.Security.Permissions;

namespace DID.Web
{
    [
    AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Medium),
    AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Medium),
    DefaultEvent("Paging"),
    ToolboxData("<{0}:Pager runat=\"server\"> </{0}:Pager>"),
    ]
    public class PagerControl : CompositeControl
    {
        public PagerControl()
        {
            //
            // TODO: 여기에 생성자 논리를 추가합니다.
            //
        }
        private const int nPageButtonViewCnt = 10; // 페이징버튼을 보여줄 최대갯수이다.

        private HtmlAnchor hancFirst;
        private HtmlAnchor hancPrev;
        private HtmlAnchor hancNext;
        private HtmlAnchor hancLast;
        private HtmlAnchor[] hancNumbers = new HtmlAnchor[nPageButtonViewCnt];

        private static readonly object EventPagingKey = new object();

        [
        Bindable(true),
        Category("Appearance"),
        DefaultValue(10),
        Description("페이징 단위.")
        ]
        public int PageSize
        {
            get
            {
                object obj = ViewState["PageSize"];
                return (obj == null) ? 15 : (int)obj;
            }
            set
            {
                if (value < 1)
                    ViewState["PageSize"] = 1;
                else
                    ViewState["PageSize"] = value;
            }
        }

        [
        Bindable(true),
        Category("Appearance"),
        DefaultValue(0),
        Description("총 컨텐츠 갯수.")
        ]
        public int TotalRowCount
        {
            get
            {
                object obj = ViewState["TotalRowCount"];
                return (obj == null) ? 0 : (int)obj;
            }
            set
            {
                if (value < 0)
                    ViewState["TotalRowCount"] = 0;
                else
                    ViewState["TotalRowCount"] = value;
            }
        }

        [
        Bindable(true),
        Category("Appearance"),
        DefaultValue(1),
        Description("현재 페이지 인덱스.")
        ]
        public int CurrentPageIndex
        {
            get
            {
                object obj = ViewState["CurrentPageIndex"];
                return (obj == null) ? 1 : (int)obj;
            }
            set
            {
                if (value < 1)
                    ViewState["CurrentPageIndex"] = 1;
                else
                    ViewState["CurrentPageIndex"] = value;
            }
        }

        public int TotalPageCount
        {
            get
            {
                int nTotalPageCnt = 1;
                nTotalPageCnt = this.TotalRowCount / this.PageSize;
                nTotalPageCnt += (this.TotalRowCount % this.PageSize) == 0 ? 0 : 1;
                nTotalPageCnt = nTotalPageCnt > 0 ? nTotalPageCnt : 1;

                return nTotalPageCnt;
            }
        }



        // The Paging event.
        [
        Category("Action"),
        Description("Pager 이미지버튼 및 페이지버튼 클릭 이벤트.")
        ]
        public event EventHandler Paging
        {
            add
            {
                Events.AddHandler(EventPagingKey, value);
            }
            remove
            {
                Events.RemoveHandler(EventPagingKey, value);
            }
        }

        protected virtual void OnPaging(EventArgs e)
        {
            EventHandler PagingHandler = (EventHandler)Events[EventPagingKey];
            if (PagingHandler != null)
            {
                PagingHandler(this, e);
            }
        }


        private void _hancFPNL_ServerClick(object source, EventArgs e)
        {
            HtmlAnchor hanc = (HtmlAnchor)source;
            if (hanc.ID == this.hancFirst.ID)
            {
                this.CurrentPageIndex = 1;
            }
            else if (hanc.ID == this.hancPrev.ID)
            {
                this.CurrentPageIndex -= 1;
            }
            else if (hanc.ID == this.hancNext.ID)
            {
                this.CurrentPageIndex += 1;

                if (this.CurrentPageIndex > this.TotalPageCount)
                    this.CurrentPageIndex = this.TotalPageCount;
            }
            else if (hanc.ID == this.hancLast.ID)
            {
                this.CurrentPageIndex = this.TotalPageCount;
            }

            OnPaging(EventArgs.Empty);
        }

        private void _hanc_ServerClick(object source, EventArgs e)
        {
            HtmlAnchor hanc = (HtmlAnchor)source;
            HtmlGenericControl ctrlSpan = (HtmlGenericControl)hanc.Controls[0];
            this.CurrentPageIndex = Convert.ToInt32(ctrlSpan.InnerHtml);

            OnPaging(EventArgs.Empty);
        }

        protected override void RecreateChildControls()
        {
            EnsureChildControls();
        }

        protected override void CreateChildControls()
        {
            Controls.Clear();

            hancFirst = new HtmlAnchor();
            hancFirst.ID = "hancFirst";

            HtmlImage imgFirst = new HtmlImage();
            imgFirst.Align = "absmiddle";
            imgFirst.Border = 0;
            imgFirst.Src = "/Asset/Images/btn_num_pre2.gif";

            hancFirst.Controls.Add(imgFirst);

            hancFirst.ServerClick += new EventHandler(_hancFPNL_ServerClick);
            this.Controls.Add(hancFirst);

            hancPrev = new HtmlAnchor();
            hancPrev.ID = "hancPrev";

            HtmlImage img1 = new HtmlImage();
            img1.Align = "absmiddle";
            img1.Border = 0;
            img1.Src = "/Asset/Images/btn_num_pre.gif";

            hancPrev.Controls.Add(img1);

            hancPrev.ServerClick += new EventHandler(_hancFPNL_ServerClick);
            this.Controls.Add(hancPrev);

            hancNext = new HtmlAnchor();
            hancNext.ID = "hancNext";

            HtmlImage img2 = new HtmlImage();
            img2.Align = "absmiddle";
            img2.Border = 0;
            img2.Src = "/Asset/Images/btn_num_next.gif";

            hancNext.Controls.Add(img2);

            hancNext.ServerClick += new EventHandler(_hancFPNL_ServerClick);
            this.Controls.Add(hancNext);

            hancLast = new HtmlAnchor();
            hancLast.ID = "hancLast";
            hancLast.ServerClick += new EventHandler(_hancFPNL_ServerClick);


            HtmlImage imgLast = new HtmlImage();
            imgLast.Align = "absmiddle";
            imgLast.Border = 0;
            imgLast.Src = "/Asset/Images/btn_num_next2.gif";

            hancLast.Controls.Add(imgLast);

            this.Controls.Add(hancLast);


            for (int cnt = 0; cnt < this.hancNumbers.Length; cnt++)
            {
                HtmlGenericControl ctrlSpan = new HtmlGenericControl("span");

                hancNumbers[cnt] = new HtmlAnchor();
                hancNumbers[cnt].Controls.Add(ctrlSpan);
                hancNumbers[cnt].ServerClick += new EventHandler(_hanc_ServerClick);
                this.Controls.Add(hancNumbers[cnt]);
            }
        }

        public override void DataBind()
        {
            base.DataBind();

            // 현재페이지 인덱스 기준으로 시작될 페이지 인덱스(0보다 커야 한다)
            int nStartPageIndex = 1;
            nStartPageIndex = (this.CurrentPageIndex / PagerControl.nPageButtonViewCnt) * PagerControl.nPageButtonViewCnt;
            nStartPageIndex -= (this.CurrentPageIndex % PagerControl.nPageButtonViewCnt) == 0 ? PagerControl.nPageButtonViewCnt - 1 : -1;
            nStartPageIndex = nStartPageIndex > 0 ? nStartPageIndex : 1;

            // 현재페이지 인덱스 기준으로 종료될 페이지 인덱스(총 페이지 갯수보다 적어야 한다)
            int nEndPageIndex = 1;
            nEndPageIndex = nStartPageIndex + PagerControl.nPageButtonViewCnt - 1;
            nEndPageIndex = nEndPageIndex <= this.TotalPageCount ? nEndPageIndex : this.TotalPageCount;

            // 현재 선택된 페이지인덱스가 범위안에 있는지 점검한다.
            if (this.CurrentPageIndex < 1) this.CurrentPageIndex = 1;
            if (this.CurrentPageIndex > this.TotalPageCount) this.CurrentPageIndex = this.TotalPageCount;

            //hancFirst.InnerText = "1page";
            //hancLast.InnerText = this.TotalPageCount + "page";

            for (int cnt = 0; cnt < this.hancNumbers.Length; cnt++)
            {
                if ((cnt + nStartPageIndex) <= nEndPageIndex)
                {
                    HtmlGenericControl ctrlSpan = (HtmlGenericControl)hancNumbers[cnt].Controls[0];
                    if (this.CurrentPageIndex == cnt + nStartPageIndex) // 현재 선택된 인덱스라면 선택스타일을 적용한다.
                    {
                        //ctrlSpan.Attributes.Add("class", "num01");
                    }
                    else
                    {
                        //ctrlSpan.Attributes.Remove("class");
                    }
                    ctrlSpan.InnerHtml = (cnt + nStartPageIndex).ToString();

                    hancNumbers[cnt].Visible = true;
                }
                else
                {
                    hancNumbers[cnt].Visible = false;
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            AddAttributesToRender(writer);

            //table 시작
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0", true);
            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center", true);
            writer.AddAttribute(HtmlTextWriterAttribute.Height, "10", true);
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            //tr 시작
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            // 앞부분 랜더링
            {

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "nob", true);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                // 처음 버튼 랜더링
                hancFirst.RenderControl(writer);
                //Strong 끝
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "nob", true);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                //이전 버튼 랜더링
                hancPrev.RenderControl(writer);
                writer.RenderEndTag();
            }

            // 페이지 버튼 랜더링
            {
                int select = (this.CurrentPageIndex - 1) % 10;
                // 페이지 링크 버튼 생성
                for (int cnt = 0; cnt < hancNumbers.Length; cnt++)
                {
                    if (hancNumbers[cnt].Visible)
                    {
                        if (cnt == this.TotalPageCount - 1)
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "num02", true);
                        else
                            writer.AddAttribute(HtmlTextWriterAttribute.Class, "num01", true);

                        if (select == cnt)
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Td);
                            writer.Write(this.CurrentPageIndex);
                            writer.RenderEndTag();
                        }
                        else
                        {

                            writer.RenderBeginTag(HtmlTextWriterTag.Td);
                            hancNumbers[cnt].RenderControl(writer);
                            writer.RenderEndTag();
                        }
                    }
                }
            }

            // 뒤부분 랜더링
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "nob", true);
                //다음 버튼 랜더링
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                hancNext.RenderControl(writer);
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "nob", true);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                // 마지막 버튼 랜더링
                hancLast.RenderControl(writer);
                //Strong 끝
                writer.RenderEndTag();
            }
            // tr 끝
            writer.RenderEndTag();
            // table 끝
            writer.RenderEndTag();
        }
    }
}