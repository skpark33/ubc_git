/////////////////////////////////////////////////////////
//
//  Author: Erik Drent
//  Class:  NewRowGridView
//  Descr:  Allows for adding new rows to the GridView
//          with built in support for ObjectDataSource
//          insert method
//
//  Changelog:
//  2006-09-28  $Erik Drent     Start project
//  2006-10-08  $Erik Drent     Added changed rows
//
/////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace DID.Web
{
    [ToolboxData("<{0}:NewRowGridView runat=server></{0}:NewRowGridView>")]
    public class NewRowGridView : GridView
    {
        private GridViewRowCollection _newRows;
        private List<int> _changedRows;

        #region Properties

        [Browsable(false)]
        public GridViewRowCollection NewRows
        {
            get
            {
                return (this._newRows == null) ? new GridViewRowCollection(new ArrayList()) : this._newRows;
            }
        }

        public int NewRowCount
        {
            get
            {
                object viewState = this.ViewState["NewRowCount"];
                return (viewState == null) ? 0 : (int)viewState;
            }
            set { this.ViewState["NewRowCount"] = value; }
        }

        [Browsable(false)]
        public GridViewRowCollection NewRowsChanged
        {
            get
            {
                if (this._changedRows != null)
                {
                    ArrayList changedRows = new ArrayList();
                    
                    foreach (int rowIndex in this._changedRows)
                    {
                        changedRows.Add(this._newRows[rowIndex]);
                    }

                    return new GridViewRowCollection(changedRows);
                }

                return new GridViewRowCollection(new ArrayList());
            }
        }

        #endregion

        protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
        {
            int rowCount = base.CreateChildControls(dataSource, dataBinding);

            this.CreateNewRows();

            return rowCount;
        }

        private void CreateNewRows()
        {
            if (this.NewRowCount > 0)
            {
                ArrayList list = new ArrayList();
                DataControlField[] fields = this.GetDataControlFields();
                for (int i = 0; i < this.NewRowCount; i++)
                {
                    GridViewRow newRow = this.CreateNewRow(i, fields);
                    list.Add(newRow);
                }
                this._newRows = new GridViewRowCollection(list);

                Table grid;
                if (this.Rows.Count == 0)
                {
                    grid = this.CreateChildTable();
                    this.Controls.Add(grid);
                    if (this.ShowHeader)
                    {
                        GridViewRow headerRow = this.CreateHeaderRow(fields);
                        grid.Rows.Add(headerRow);
                    }
                }
                else
                {
                    grid = (Table)this.Rows[0].Parent;
                }

                int rowIndex = this.Rows.Count + 1;
                foreach (GridViewRow newRow in this._newRows)
                {
                    grid.Rows.AddAt(rowIndex, newRow);
                    rowIndex++;
                }
            }
        }

        private DataControlField[] GetDataControlFields()
        {
            DataControlField[] fields = new DataControlField[this.Columns.Count];
            base.Columns.CopyTo(fields, 0);
            return fields;
        }

        private GridViewRow CreateNewRow(int rowIndex, DataControlField[] fields)
        {
            GridViewRow newRow = base.CreateRow(rowIndex, -1, DataControlRowType.DataRow, DataControlRowState.Insert);
            base.InitializeRow(newRow, fields);

            this.AddRowChanged(newRow);

            return newRow;
        }

        private GridViewRow CreateHeaderRow(DataControlField[] fields)
        {
            GridViewRow headerRow = base.CreateRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
            base.InitializeRow(headerRow, fields);

            return headerRow;
        }

        private void AddRowChanged(Control control)
        {
            foreach (Control ctr in control.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).TextChanged += new EventHandler(this.RowChanged);
                }
                else if (ctr is ListControl)
                {
                    ((ListControl)ctr).SelectedIndexChanged += new EventHandler(this.RowChanged);
                }
                else if (ctr is CheckBox)
                {
                    ((CheckBox)ctr).CheckedChanged += new EventHandler(this.RowChanged);
                }

                if (ctr.HasControls())
                {
                    this.AddRowChanged(ctr);
                }
            }
        }

        private void RowChanged(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((Control)sender).NamingContainer;

            if (this._changedRows == null)
            {
                this._changedRows = new List<int>();
            }

            if (!this._changedRows.Contains(row.RowIndex))
            {
                this._changedRows.Add(row.RowIndex);
            }
        }
    }
}
