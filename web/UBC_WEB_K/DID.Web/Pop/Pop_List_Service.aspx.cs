﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DID.Common.Framework;
using System.IO;

public partial class Pop_List_Service : BasePage //System.Web.UI.Page
{
    public readonly string NEWS     = "news";
    public readonly string DISPLAY  = "display";
 
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.ContentType = "text/xml";    
        Response.Write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        Response.Write("<POP>\n");
        
        // 입력인자가 맞을 경우에만 실행
        if( !Request["siteid"].IsNullOrEmpty() && !Request["type"].IsNullOrEmpty() )
        {
            // 게시판 아이디 가져오기
            int boardMasterId = getBoardMasterId();

            // 적절한 게시판 아이디가 있을경우에만 데이타 검색
            if (boardMasterId != -1)
            {
                DataTable dt = this.GetBoardList(boardMasterId);

                if (!dt.IsNullOrEmpty())
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Response.Write(string.Format("<item>{0}</item>\n"
                            , Path.Combine(this.FILE_PATH_POP, dr[0].ToString()).Replace('\\', '/')));
                    }
                }
            }

        }        

        Response.Write("</POP>\n");
        
    }
    
    #region 게시판ID 설정 - getBoardMasterId()
    /// <summary>
    /// 입력된 param 에 따른 게세판ID 선택
    /// </summary> 
    private int getBoardMasterId()
    {
        string boardType = Request["type"].ToString().ToLower();

        if( boardType == NEWS )
            return (int)Common.BoardMasterID.PopNews;
        else if( boardType == DISPLAY )
            return (int)Common.BoardMasterID.PopDisplay;
        else 
            return -1;
    }
    #endregion


    #region 게시물 데이터 조회 - GetBoardList()
    /// <summary>
    /// 게시물 데이터 조회
    /// </summary> 
    private DataTable GetBoardList(int boardMasterId)
    {
        DataTable dt = null;
    
        using (DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard())
        {
            dt = obj.GetPopListService(boardMasterId, Request["siteid"].ToString());
        }

        return dt;
    }
    #endregion
}
