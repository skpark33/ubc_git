﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Pop_News_View.aspx.cs" Inherits="Pop_News_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Innorix/InnoFD/InnoFD.js"></script>

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/Common/jwplayer.js"></script>
<script type="text/javascript" src="/Common/FLVPDF.js"></script>

<script language="javascript" type="text/javascript">
    function DoList() {
        location.href = "Pop_News_List.aspx?pgn=<%=this.Pgn%>";
    }
    function DoUpdate() {
        location.href = "Pop_News_Write.aspx?boardId=<%=this.BoardID%>&pgn=<%=this.Pgn%>";
    }
    function DoDelete() {
        if(confirm('삭제하시겠습니까?')){
            $("#<%=btnDelete.ClientID%>").click();
        }
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:HiddenField ID="hdnBoardId" runat="server" />
    <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" style="display:none" />
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><span><img src="/Asset/Images/body/bullet01.gif" /> NEWS POP 등록정보</span></span>
                
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn_nonAuth("LIST#UPD#DEL")%>
        <%--<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoList();return false;'>목록</a></span></span>
        <span class='btnType btnModify'><span><a href='javascript:#' onclick='javascript:DoUpdate();return false;'>수정</a></span></span>
        <span class='btnType btnDelete'><span><a href='javascript:#' onclick='javascript:DoDelete();return false;'>삭제</a></span></span>--%>
    </div>    

    <asp:FormView ID="fvBoard" runat="server" style="width:100%">
    <ItemTemplate>
        <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
        <colgroup>
	        <col width="15%" />
	        <col width="auto" />
	        <col width="15%" />
	        <col width="auto" />  
	        <col width="15%" />
	        <col width="auto" />  
        </colgroup>
        <tr>
        <th>제목</th>
            <td colspan="5">
                <%# Eval("title") %>
            </td> 
        </tr> 
        <tr>
            <th>등록자</th>
            <td>
                <%# Eval("userName")%>
            </td>
            <th>등록일</th>
            <td>
                <%# Eval("createDate" ,"{0:yyyy-MM-dd}") %>
            </td>
            <th>조회수</th>
            <td>
                <%# Eval("readCount" ,"{0:n0}") %>
            </td>
        </tr>
        <tr>
            <th>첨부 파일</th>
            <td colspan="5" align="left">
                <asp:Repeater ID="rptFile" runat="server" OnItemDataBound="RptFile_itemDataBound">
                   
                    <ItemTemplate>
                        <div>
                            <p>
                                화일명 : <span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%# Eval("attachId") %>&boardMasterId=<%=(int)Common.BoardMasterID.PopNews %>'"><%# Eval("fileName")%></span>
                                <br />
                                <asp:Label ID="Label_Image_size" runat="server" Text="Label"></asp:Label>
                            </p>
                            <p style="background-color:black; vertical-align:top; width:<%= IMAGE_FRAME_WIDTH %>px; height:<%= IMAGE_FRAME_HEIGHT %>px;">
                                <asp:Image ID="Image_pop" runat="server" BorderWidth="0" />
                            </p>                            
                        </div>        			          
                    </ItemTemplate>
                    <SeparatorTemplate>
                    <hr />
                    </SeparatorTemplate>
                </asp:Repeater>
                &nbsp;
            </td>
        </tr> 
        </table>
    </ItemTemplate> 
    </asp:FormView>
        
</asp:Content>

