﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;
using System.Drawing;

/// <summary>
/// 조회 페이지
/// </summary>
public partial class Pop_Display_View : BasePage
{
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string BoardID
    {
        get { return Request["boardId"] ?? ""; }
    }
    /// <summary>
    /// 게시물번호
    /// </summary>
    public string Thread
    {
        get;set;
    }
    /// <summary>
    /// 게시물 Depth
    /// </summary>
    public string Depth
    {
        get;set;
    }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }
    /// <summary>
    /// 모드 : write,reply,update
    /// </summary>
    public Common.BoardMode Mode
    {
        get
        {
            if (!string.IsNullOrEmpty(this.BoardID) && string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Update;
            else
                return Common.BoardMode.Write;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (this.Mode == Common.BoardMode.Update)
            {
                this.BindControl();
            }
        }
    }

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        DataTable dtList = null;
        DataTable dtFile = null;
        long boardId = Convert.ToInt64(this.BoardID);

        using (DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard())
        {
            dtList = obj.GetDetail(boardId, this.Profile.SITE_ID);
            obj.UpdateReadCount(boardId);
            dtFile = obj.GetAttachList(boardId);
        }

        fvBoard.DataSource = dtList;
        fvBoard.DataBind();

        if (!fvBoard.IsNullOrEmpty())
        {

            Repeater rpt = (Repeater)(fvBoard.FindControl("rptFile"));

            if (!rpt.IsNullOrEmpty())
            {
                rpt.DataSource = dtFile;
                rpt.DataBind();
            }
        }


        if (dtList.Rows.Count > 0)
        {
            this.Thread = dtList.ToDataRow()["thread"].ToString();
            this.Depth = dtList.ToDataRow()["depth"].ToString();
            this.hdnBoardId.Value = dtList.ToDataRow()["boardId"].ToString();
        }

    }
    #endregion

    #region 삭제버튼 클릭 - btnDelete_Click(object sender, EventArgs e)
    /// <summary>
    /// 삭제버튼 클릭
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        using (DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard())
        {
            long boardId = Convert.ToInt64(this.hdnBoardId.Value);
            DataTable dt= obj.GetAttachList(boardId);
            Common.DeleteFile(dt, (int)Common.BoardMasterID.PopDisplay);
            obj.DeleteAttachByBoardId(boardId);
            obj.Delete(boardId);
        }

        this.ScriptExecuteUrl("삭제되었습니다.", "Pop_Display_List.aspx");
    }
    #endregion


    #region 이미지 미리보기에 쓰이는 상수

    public const int KIOSK_FRAME_WIDTH = 770;
    public const int KIOSK_FRAME_HEIGHT = 1370;

    public const float IMAGE_REDUCE_PERCENTAGE = 50;
    public const int IMAGE_FRAME_WIDTH = (int)(KIOSK_FRAME_WIDTH * IMAGE_REDUCE_PERCENTAGE / 100);
    public const int IMAGE_FRAME_HEIGHT = (int)(KIOSK_FRAME_HEIGHT * IMAGE_REDUCE_PERCENTAGE / 100);

    #endregion

    #region 이미지 사이즈 알아오기

    private Size getImageSize(string imageURL)
    {   
        Size returnImageSize = new Size(0, 0);

        DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
        if (impersonater.ImpersonateValidUser())
        {

            //서버 맵 패스로 이미지 경로 설정
            string path = Server.MapPath(imageURL);

            //이미지 존재여부 확인, 이미지가 없을경우 return false
            if (System.IO.File.Exists(path) == false)
            {
                return returnImageSize;
            }
        
             
            //해당 경로에서 이미지 파일을 비트맵으로 로딩
            Bitmap Imagesize = new Bitmap(path, false);

            //불러 들인 이미지의 가로, 세로 크기 저장
            returnImageSize.Width  = Imagesize.Width;
            returnImageSize.Height = Imagesize.Height;

            Imagesize.Dispose();

        impersonater.UndoImpersonation();
        }
        
        return returnImageSize;
    }

    #endregion

    #region 리피터 바운드 이벤트

    protected void RptFile_itemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // 이미지 컨트롤 셋팅
            System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("Image_pop");

            DataRowView dr = (DataRowView)e.Item.DataItem;
            string imgUrl //= "/File/Pop/" + dr["filename"].ToString();
             = Path.Combine(this.FILE_PATH_POP, dr["filename"].ToString()).Replace('\\', '/');
            
            // 이미지 사이즈 가져오기
            Size imageSize = new Size();
            imageSize = getImageSize(imgUrl);

            // 화면에 뿌릴 메세지 작성
            string msg = "현재 이미지 사이즈는 " + imageSize.Width.ToString() + "*" + imageSize.Height.ToString() + " 입니다.";

            msg += " (목록에 보이는 이미지는 실제 사이즈의 " + IMAGE_REDUCE_PERCENTAGE + "% 입니다.)<br />";

            if (imageSize.Width != KIOSK_FRAME_WIDTH || imageSize.Height != KIOSK_FRAME_HEIGHT)
            {
                msg += "디지털 갤러리에 들어가는 POP 권장사이즈는 " + KIOSK_FRAME_WIDTH.ToString() + "*" + KIOSK_FRAME_HEIGHT.ToString() + " 입니다.<br />";

                msg += "POP 사이즈를 확인해 주세요.<br />";
            }


            img.ImageUrl = imgUrl;
            img.Height = (int)(imageSize.Height * IMAGE_REDUCE_PERCENTAGE / 100);
            img.Width = (int)(imageSize.Width * IMAGE_REDUCE_PERCENTAGE / 100);

            // 메세지를 레이블로 보여주기
            Label lb_img_size = (Label)e.Item.FindControl("Label_Image_size");
            lb_img_size.Text = msg;
            lb_img_size.ForeColor = System.Drawing.Color.Salmon;
        }
    }

    #endregion
}
