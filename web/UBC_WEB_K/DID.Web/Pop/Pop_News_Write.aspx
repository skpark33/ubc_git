﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" EnableSessionState="False" ValidateRequest="false" CodeFile="Pop_News_Write.aspx.cs" Inherits="Pop_News_Write" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/default.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">


    function ValidateForm() {
        if (Trim($("#<%= txtTitle.ClientID%>").val()) == "") {
            alert("제목을 입력하십시요.");
            $("#<%= txtTitle.ClientID%>").focus();
            return false;
        }
        
        return true;
    }

    function DoSave() {
        if (ValidateForm()) {
             $("#<%= btnSave.ClientID%>").click();
        }
    }

    function DoList() {
        location.href = "Pop_News_List.aspx";
    }

</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnContents" runat="server"  />
    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="display:none" />
    <input type="hidden" id="hndImagFileName" name="hndImagFileName" />
     
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <asp:Literal ID="lblSubTitle" runat="server" Text=""></asp:Literal></span>
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn_nonAuth("SAVE#LIST")%>
    </div>    
    
    <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
    <colgroup>
	    <col width="13%" />
	    <col width="auto" /> 
	    <col width="13%" />
	    <col width="auto" /> 
	    <col width="13%" />
	    <col width="auto" /> 
    </colgroup>
    <tr>
        <th>제목<font color="red"> *</font></th>
        <td colspan="5"> 
            <asp:TextBox ID="txtTitle" runat="server"  CssClass="textType" Width="660px" />
        </td>
    </tr> 
    <!--tr>
        <th>로그인 전후</th>
        <td>
            <asp:RadioButtonList ID="rdoLoginYn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                <asp:ListItem  Value="Y" Text="전" />
                <asp:ListItem  Value="N" Text="후" Selected="True" />
            </asp:RadioButtonList>
        </td>
        <th>팝업크기</th>
        <td>
            W : <asp:TextBox ID="txtWidth" runat="server" Width="30px" CssClass="textType" />
            H : <asp:TextBox ID="txtHeight" runat="server" Width="30px" CssClass="textType"/>
        </td>
        <th>팝업위치</th>
        <td>
            X : <asp:TextBox ID="txtPosX" runat="server" Width="30px" CssClass="textType"/>
            Y : <asp:TextBox ID="txtPosY" runat="server" Width="30px" CssClass="textType"/>
        </td>
    </tr> 
    <tr>
        <th>팝업사용 여부</th>
        <td>
            <asp:CheckBox ID="chkPopYn1" runat="server" />
        </td>
        <th>탑공지 여부</th>
        <td colspan="3">
            <asp:CheckBox ID="chkTopYn11" runat="server" />
        </td>
    </tr--> 
    <%--<tr>
        <th>목록 상단 위치</th>
        <td>
            <asp:RadioButtonList ID="rdoTopYn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radioType"  >
                <asp:ListItem Text="예&nbsp;" Value="Y"  />
                <asp:ListItem Text="아니오" Value="N" Selected="True" />
            </asp:RadioButtonList>
        </td>
        <th>팝업사용 여부</th>
        <td colspan="3">
            <asp:RadioButtonList ID="rdoPopYn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radioType"  >
                <asp:ListItem Text="예&nbsp;" Value="Y"  />
                <asp:ListItem Text="아니오" Value="N" Selected="True" />
            </asp:RadioButtonList>
        </td>
    </tr> 
    <tr>
        <th>내용<font color="red"> *</font></th>
        <td align="right" colspan="5">
            <span onclick="PopUploader('image','hndImagFileName','hdnImageSave');" style="cursor:pointer">
            <img src="/Asset/Images/body/img.gif" />
            이미지 업로드</span>
            <textarea name="ir1" id="ir1" runat="server" style="width:100%;height:300px" ></textarea>
        </td>
    </tr> --%>
    <tr id="trFileView" runat="server" visible="false">
        <th>첨부된 파일</th>
        <td style="text-align:left" colspan="5">
            <asp:Repeater ID="rptFile" runat="server">
            <ItemTemplate>
                <input type="checkbox" value="<%# Eval("attachId") %>|<%# Eval("fileName") %>" name="_delete_file" /><%# Eval("fileName")%><br />        
            </ItemTemplate>
            </asp:Repeater><p></p>
            <span style="color:#8b0000">◆ 삭제하려면 해당 파일을 체크하고 저장하면 됩니다.</span> 
        </td>
    </tr>
    <tr>
        <th>파일첨부</th>
        <td align="left" colspan="5">
        
			File(s) to upload: <span style="color:#008b00">◆ 등록하신 화일 중 상위 3개만이 디지털갤러리에 나타납니다.</span> <span style="color:#008b00; font-weight:bold; text-decoration: underline;" >(권장사이즈: 770x1370)</span>
			<hr />
	        <Upload:MultiFile id="multiFile" runat="server" useFlashIfAvailable="false">
		        <asp:Button id="multiFileButton" Text="찾아보기" Enabled="<%# multiFile.Enabled %>" runat="server"/>
		        
	        </Upload:MultiFile>

			<div style="display:none;">
			    <Upload:ProgressBar id="inlineProgressBar" runat="server" Inline="true" height="50px" Width="630px" Triggers="btnSave">
			    <asp:Label id="label" runat="server" Text="Check Progress"/>
			    </Upload:ProgressBar>
			</div>

        </td>
    </tr> 
    </table> 
    

</asp:Content>

