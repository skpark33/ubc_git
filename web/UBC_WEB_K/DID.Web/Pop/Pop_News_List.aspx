﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Pop_News_List.aspx.cs" Inherits="Pop_News_List" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoSearch() {
        $("#<%= btnSearch.ClientID%>").click();
    }

    function DoReg() {
        location.href = "Pop_News_Write.aspx";
    }
    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Button id="btnSearch" runat="server" OnClick="btnSearch_click" style="display:none"  />

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><span><img src="/Asset/Images/body/bullet01.gif" /> NEWS POP 등록현황</span></span> <!--게시판-->
            </td>
        </tr>
    </table>
    
   <div id='regBtn' class='topOptionBtns'>
        <%= (TOTAL_ROW_COUNT <= 0) ? Common.SetScriptBtn_nonAuth("REG") : "&nbsp;"%>       
    </div>
    
    <%--<table width="100%" border="0" cellspacing="0" cellpadding="0" class="pad_B10">
    <tr>
    <td width="7"><img src="/Asset/Images/body/box_left.gif" /></td>
    <td style="background:url(/Asset/Images/body/box_bg.gif) repeat-x">
        <div  class="pad_L10">
            <asp:DropDownList ID="ddlSearchKind" runat="server" CssClass="selectType">
                <asp:ListItem Text="전체" Value=""></asp:ListItem>
                <asp:ListItem Text="제목" Value="title"></asp:ListItem>
                <asp:ListItem Text="내용" Value="contents"></asp:ListItem>
                <asp:ListItem Text="등록자" Value="userNm"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearchWord" runat="server" CssClass="textType" Width="300px" />
        </div>
    </td>
    <td width="8"><img src="/Asset/Images/body/box_right.gif" /></td>
    </tr>
    </table>--%>
    
    <asp:GridView ID="gvList" runat="server" rules="all" Width="100%" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false"  
    CssClass="boardListType">
    <Columns>
        <asp:TemplateField HeaderText="번호">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#(this.TOTAL_ROW_COUNT - ((this.CURRENT_PAGE - 1) * this.PAGE_SIZE + Container.DataItemIndex))%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="제목" HeaderStyle-CssClass="center">
            <HeaderStyle Width="50%" />
            <ItemTemplate>
                <a href="Pop_News_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>"><%#Eval("title") %></a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="파일" HeaderStyle-CssClass="center">
            <HeaderStyle Width="5%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField  DataField="userName" HeaderText="등록자" HeaderStyle-Width="13%" ItemStyle-CssClass="center" />
        <asp:TemplateField HeaderText="등록일">
            <HeaderStyle Width="10%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("createDate", "{0:yyyy-MM-dd}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <%--<asp:TemplateField HeaderText="조회수">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("readCount", "{0:n0}")%>
            </ItemTemplate>
        </asp:TemplateField>--%>
    </Columns>
    <EmptyDataTemplate>
        <table style="width:100%;height:200px">
            <tr>
                <td style="text-align:center">
                조회된 데이터가 없습니다.
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    </asp:GridView>   
                
   
     <div id="pagenum2" style="margin-top:15px">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>  
       	
</asp:Content>

