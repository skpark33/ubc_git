﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;
using System.Web.UI.HtmlControls;


/// <summary>
/// 등록,수정,답변 페이지
/// </summary>
public partial class Pop_Display_Write : BasePage
{
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string BoardID
    {
        get { return Request["boardId"] ?? ""; }
    }
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string Thread
    {
        get { return Request["Thread"] ?? ""; }
    }
    /// <summary>
    /// 모드 : write,reply,update
    /// </summary>
    public Common.BoardMode Mode
    {
        get
        {
            if (!string.IsNullOrEmpty(this.BoardID) && string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Update;
            else
                return Common.BoardMode.Write;
        }
    }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }
    /// <summary>
    /// 게시글 Depth
    /// </summary>
    public string Depth
    {
        get { return Request["Depth"] ?? ""; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ((HtmlForm)this.Master.FindControl("form1")).Enctype = "multipart/form-data";

        if (!this.IsPostBack)
        {
            this.InitControl();

            switch (this.Mode)
            {
                case Common.BoardMode.Write:
                    lblSubTitle.Text = "Display POP 등록";
                    break;
                case Common.BoardMode.Update:
                    lblSubTitle.Text = "Display POP 수정";
                    break;
            }

            if (this.Mode == Common.BoardMode.Update)
            {
                this.BindControl();
            }
        }
    }

    private void InitControl()
    {
    }

    #region 저장버튼 클릭 - btnSave_Click(object sender, EventArgs e)
    /// <summary>
    /// 저장버튼 클릭
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        long boardId = 0;

        using (DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard())
        {
            switch (this.Mode)
            {
                case Common.BoardMode.Write:
                    boardId = obj.Insert((int)Common.BoardMasterID.PopDisplay
                                        , Profile.USER_ID
                                        , txtTitle.Text
                                        , Profile.SITE_ID  // contents 필드에 siteID를 저장한다
                                        , (Profile.USER_TYPE == "1" || Profile.USER_TYPE == "-1") ? false : true // 관리자(1),개발자(-1): 관리자가 등록한 POP 를 아래로 나오게 하기위해서 topYn 값을 false로 등록
                                        , Request.UserHostAddress
                                        );
                    break;

                case Common.BoardMode.Update:
                    boardId = Convert.ToInt64(this.BoardID);
                    obj.Update(boardId
                                , Profile.USER_ID
                                , txtTitle.Text
                                , Profile.SITE_ID       // contents 필드에 siteID를 저장한다
                                , (Profile.USER_TYPE == "1" || Profile.USER_TYPE == "-1") ? false : true // 관리자(1),개발자(-1): 관리자가 등록한 POP 를 아래로 나오게 하기위해서 topYn 값을 false로 등록
                                , Request.UserHostAddress
                                );
                    break;

            }

            Common.AttachFile(boardId, (int)Common.BoardMasterID.PopDisplay);
        }

        this.ScriptExecuteUrl("저장되었습니다.", string.Format("Pop_Display_List.aspx?Pgn={0}", this.Pgn));
    }
    #endregion

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        DataTable dt = null;
        DataTable dtFile = null;

        long boardId = Convert.ToInt64(this.BoardID);
        using (DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard())
        {
            dt = obj.GetDetail(boardId, this.Profile.SITE_ID);
            dtFile = obj.GetAttachList(boardId);
        }

        rptFile.DataSource = dtFile;
        rptFile.DataBind();

        if (dtFile.Rows.Count > 0)
        {
            trFileView.Visible = true;
        }

        if (dt.Rows.Count > 0)
        {
            DataRow dr = dt.ToDataRow();

            txtTitle.Text = dr["Title"].ToString();
            hdnContents.Value = dr["Contents"].ToString();
        }
    }
    #endregion
}
