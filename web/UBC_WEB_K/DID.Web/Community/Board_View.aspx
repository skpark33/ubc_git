﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Board_View.aspx.cs" Inherits="Board_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Innorix/InnoFD/InnoFD.js"></script>

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/Common/jwplayer.js"></script>
<script type="text/javascript" src="/Common/FLVPDF.js"></script>

<script language="javascript" type="text/javascript">
    function DoList() {
        location.href = "Board_List.aspx?pgn=<%=this.Pgn%>";
    }
    function DoUpdate() {
        location.href = "Board_Write.aspx?boardId=<%=this.BoardID%>&pgn=<%=this.Pgn%>";
    }
    function DoDelete() {
        if(confirm('삭제하시겠습니까?')){
            $("#<%=btnDelete.ClientID%>").click();
        }
    }
    function DoReply() {
       location.href = "Board_Write.aspx?thread=<%=this.Thread%>&depth=<%=this.Depth%>&boardId=<%=this.BoardID%>&pgn=<%=this.Pgn%>";
    }

    function OnLoadComplete(objName) {
    
        // 다운로드 파일 리스트 추가
        // InnoFD.AddDownloadFile("http://받을파일 URL", "[옵션]출력파일명", [옵션]파일용량);
        <% if(this.AttachFileData!=null){
            for(int i=0 ; i< this.AttachFileData.Rows.Count ; i++){
                System.Data.DataRow dr = this.AttachFileData.Rows[i]; 
        %>
            InnoFD.AddDownloadFile('/Community/FileDownload.aspx?seq=<%=dr["attachId"]%>','<%= Common.GetFileAndPath(dr["filePath"].ToString(),dr["fileName"].ToString())%>');
        <% }} %>   

    }
    
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:HiddenField ID="hdnBoardId" runat="server" />
    <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" style="display:none" />
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><span>게시판 조회</span></span>
                
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <span class='btnType btnSearch'><span><a href='javascript:void(0)' onclick='javascript:DoList();'>목록</a></span></span>        
        <span class='btnType btnRegister'><span><a href='javascript:void(0)' onclick='javascript:DoUpdate();'>수정</a></span></span>        
        <span class='btnType btnRegister'><span><a href='javascript:void(0)' onclick='javascript:DoDelete();'>삭제</a></span></span>        
        <span class='btnType btnRegister'><span><a href='javascript:void(0)' onclick='javascript:DoReply();'>답변</a></span></span>        
    </div>    

    <asp:FormView ID="fvBoard" runat="server" style="width:100%">
    <ItemTemplate>
        <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
        <colgroup>
	        <col width="15%" />
	        <col width="auto" />
	        <col width="15%" />
	        <col width="auto" />  
	        <col width="15%" />
	        <col width="auto" />  
        </colgroup>
        <tr>
        <th>제목</th>
            <td colspan="5">
                <%# Util.DB2HTML(Eval("title").ToString()) %>
            </td> 
        </tr> 
        <tr>
            <th>등록자</th>
            <td>
                <%# Util.DB2HTML(Eval("userId").ToString()) %>
            </td>
            <th>등록일</th>
            <td>
                <%# Eval("createDate" ,"{0:yyyy-MM-dd}") %>
            </td>
            <th>조회수</th>
            <td>
                <%# Eval("readCount" ,"{0:n0}") %>
            </td>
        </tr>
        <tr>
            <th>내용</th>
            <td colspan="5" >
                <div class="smartOutput">
                <%# Eval("contents")%>
                </div>
            </td>
        </tr> 
        <tr>
            <th>첨부 화일</th>
            <td colspan="5" align="right">
            <input type="button" id="ff" value="폴더별" onclick="ViewFDFolder();" />
            <input type="button" id="Button1" value="파일별" onclick="ViewFDFile();" />
                <script type="text/javascript">
                    var Enc = "Ouu4c/GrZ2WelN5djCy1oXQxaxFd5m9ItDD1x5jnpO/6JAMaXfvUwx6wdNoS8VaNGT7cgGOtPbNDp4R1lFes3yI+wRnpvGnWNy55mvmgcReBzCpVIzBP0mEFTfs0TL0a8Lyw8+ggMeY1bNxr";

                    var CharSet = "EUC-KR";
                    var ListStyle = "report";
                    var ViewType = 2;

                    InnoFDInit(650, 150);
                </script>
            </td>
        </tr> 
        </table>
    </ItemTemplate> 
    </asp:FormView>
        
</asp:Content>

