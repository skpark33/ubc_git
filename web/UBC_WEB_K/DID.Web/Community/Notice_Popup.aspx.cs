﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

/// <summary>
/// 조회 페이지
/// </summary>
public partial class Notice_Popup : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.BindControl();
        }
    }

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        DataTable dt = null;
        using (DID.Service.Community.Notice obj = new DID.Service.Community.Notice())
        {
            dt = obj.GetDetailPop(this.Profile.COMP_TYPE );
        }

        fvBoard.DataSource = dt;
        fvBoard.DataBind();
    }
    #endregion

}
