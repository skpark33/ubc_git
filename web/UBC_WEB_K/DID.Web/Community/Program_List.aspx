﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Program_List.aspx.cs" Inherits="Program_List" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoQuery(code) {
        $("#<%=txtSearchWord.ClientID %>").val(code);
        $("#<%= btnSearch.ClientID%>").click();
    }

    function DoReg() {
        location.href = "Program_Write.aspx";
    }
    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Button id="btnSearch" runat="server" OnClick="btnSearch_click" style="display:none"  />

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><span><img src="/Asset/Images/body/bullet01.gif" /> 프로그램 다운로드</span></span>
            </td>
        </tr>
    </table>
        
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn("REG") %>
    </div>    
    
    <div>
        <img src="/Asset/Images/body/bt_down00.gif" onclick="DoQuery('')" style="cursor:pointer" />
        <img src="/Asset/Images/body/bt_down01.gif" onclick="DoQuery('1')" style="cursor:pointer" />
        <img src="/Asset/Images/body/bt_down02.gif" onclick="DoQuery('2')" style="cursor:pointer" />
        <img src="/Asset/Images/body/bt_down03.gif" onclick="DoQuery('3')" style="cursor:pointer" />
                           
        <asp:DropDownList ID="ddlSearchKind" runat="server" CssClass="selectType" style="display:none">
            <asp:ListItem Text="전체" Value=""></asp:ListItem>
            <asp:ListItem Text="제목" Value="title"></asp:ListItem>
            <asp:ListItem Text="내용" Value="contents"></asp:ListItem>
            <asp:ListItem Text="등록자" Value="userNm"></asp:ListItem>
            <asp:ListItem Text="구분" Value="contentCd" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtSearchWord" runat="server" CssClass="textType" Width="300px" style="display:none"/>
    </div>
    
    <asp:GridView ID="gvList" runat="server" Width="100%" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false"  
    CssClass="boardListType">
    <Columns>
        <asp:TemplateField HeaderText="번호">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#(this.TOTAL_ROW_COUNT - ((this.CURRENT_PAGE - 1) * this.PAGE_SIZE + Container.DataItemIndex))%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="구분">
            <HeaderStyle Width="10%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("contentNm")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="제목" HeaderStyle-CssClass="center">
            <HeaderStyle Width="50%" />
            <ItemTemplate>
                <a href="Program_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>"><%#Util.DB2HTML(Eval("title").ToString()) %></a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="파일" HeaderStyle-CssClass="center">
            <HeaderStyle Width="5%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField  DataField="userName" HeaderText="등록자" HeaderStyle-Width="10%" ItemStyle-CssClass="center" />
        <asp:TemplateField HeaderText="등록일">
            <HeaderStyle Width="11%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("createDate", "{0:yyyy-MM-dd}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="조회수">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("readCount", "{0:n0}")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        <table style="width:100%;height:200px">
            <tr>
                <td style="text-align:center">
                조회된 데이터가 없습니다.
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    </asp:GridView>   
                
    <div id="pagenum2" style="margin-top:15px">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>     
       	
</asp:Content>

