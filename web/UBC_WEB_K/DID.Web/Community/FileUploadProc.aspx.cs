using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

using DID.Common.Framework;

public partial class FileUploadProc : BasePage
{
    public string getFileInfo;
    public string Save_Dir { get { return "this.FILE_PATH_COMMON"; } }		// 저장 위치(필요시 변경)

	// 함수 부분

	//****** 파일명 변경 필요시 호출 함수 ******//
	public string make_filename(string _filename)
	{	
		// 파일명변경시켜 업로드 하실 경우 아래와 같은 방식으로 변경 시켜 주시면 됩니다. 
		// 인자 _filename 은 변경전 파일 명입니다.										  
		// 예제) 파일명에 현재날짜를 추가하여 변경 하실 경우	
		//	test.txt -> test(2007-01-06).txt
		//	int pos = _filename.LastIndexOf(".");										  
		//	string name = _filename.Substring(0, pos);									
		//	string ext = _filename.Substring(pos);
		//	string date = DateTime.Now.ToString("yyyy-MM-dd");
		//	_filename = name + "(" + date + ")" + ext;
		
		return _filename;
	}

	// 중복 파일명 변경 함수
	public string file_rename(string _folder_path, string _filename)
	{
		string new_name;
		
		int pos = _filename.LastIndexOf(".");
		string name = _filename.Substring(0, pos);
		string ext = _filename.Substring(pos);

		int i=1;
		// test.txt -> test(1).txt -> test(2).txt -> text(3).txt -> ......
		while(true)
		{
			new_name = name + "(" + i + ")" + ext;
			if( File.Exists(_folder_path+new_name) == false)
			{
				break;
			}
			i++;
		}
		return new_name;
	}

	// Sub 폴더 파라미터 중복시 파싱
	public string param_check(string _sub_dir)
	{
		// MyFolder, MyFolder 와 같이 파라미터 중복시 "," 기준으로 파싱
		int pos = _sub_dir.IndexOf(",");
		if(pos > 0)
		{
			_sub_dir = _sub_dir.Substring(0,pos);
		}
		return _sub_dir;
	}

	//////////////////////////////////////////////////////////////////////////////////

	// 실행 부분

    public void Page_Load(object sender, EventArgs e)
    {
		string root_dir = Server.MapPath(Save_Dir);	
		string sub_dir;
		string file_name;
		string full_path;
		string new_filename;
		string folder;
		long file_size;
		
		// 저장 디렉토리가 없을경우 생성
		if (!Directory.Exists(root_dir))
		{
			Directory.CreateDirectory(root_dir);
		}
		
		// Sub 폴더 지정시 생성 및 처리
		sub_dir = Request["_SUB_DIR"];
		if( sub_dir != null)
		{
			sub_dir = param_check(sub_dir);	// _SUB_DIR 파라미터 중복 체크
			root_dir = root_dir + "\\" + sub_dir + "\\";
			if (!Directory.Exists(root_dir))
			{
				Directory.CreateDirectory(root_dir);
			}
		}

		// 폴더 업로드시 생성 및 처리
		folder = Request["_folder"];
		if( folder != null )
		{
			root_dir =  root_dir + "\\" + folder + "\\";
			if (!Directory.Exists(root_dir))
			{
				Directory.CreateDirectory(root_dir);
			}
		}

	
		file_name = Request["_filename"];
		if (Request["_action"] == "getFileInfo")	// 파일 이어쓰기
        {
			
			full_path = "";
			new_filename = Request["_newname"];	
			if( new_filename != null )
			{
				file_size = 0;
				file_name = new_filename;
				full_path = root_dir + new_filename;
				System.IO.FileInfo fileInfo = new System.IO.FileInfo(full_path);
				file_size = fileInfo.Length;
			}
			else		// 새로운 파일 업로드
			{
				file_size = 0;
				new_filename = make_filename(file_name);
				full_path = root_dir + new_filename;
				if( File.Exists(full_path) == true )
				{
					// 중복 파일명 처리
					new_filename = file_rename(root_dir, file_name);
					full_path = root_dir + new_filename;
				}

			}			

	        getFileInfo = "<file_offset>" + file_size + "</file_offset>";
            getFileInfo += "<file_name>" + new_filename + "</file_name>";
            getFileInfo += "<file_path>" + full_path + "</file_path>";
        }
        else if (Request["_action"] == "attachFile")
        {
            System.Web.HttpFileCollection files = Request.Files;
	        System.Web.HttpPostedFile postedFile = files[0];
			
			file_name = Request["_filename"];
			long current_size = 0;
			long exists_size = 0;
			long saved_size = 0;
					
			full_path = root_dir + file_name;
			
			if( File.Exists(full_path) == true )
			{
				System.IO.FileInfo fileInfo = new System.IO.FileInfo(full_path);
				exists_size = fileInfo.Length;
			}
			current_size = postedFile.ContentLength;
			saved_size = current_size + exists_size;
			
			// 파일 업로드 처리
			Stream fi = postedFile.InputStream;
			FileStream fo = new FileStream( full_path, FileMode.OpenOrCreate);
			fo.Seek(0, SeekOrigin.End);
			int j;
			byte[] buff = new byte[51200];
			do
			{
				j = fi.Read(buff, 0, 51200);
				if (j > 0)
				{
					fo.Write(buff, 0, j);
				}
			}
			while (j > 0);
			fi.Close();
			fo.Close();

			getFileInfo = "<code>0000</code>";

            //return;
        }
    }
}