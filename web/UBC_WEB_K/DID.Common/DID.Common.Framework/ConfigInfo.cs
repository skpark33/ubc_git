﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace DID.Common.Framework
{
    public class ConfigInfo
    {
        public static double LOGIN_TIMEOUT
        {
            get
            {
                string configValue = ConfigurationManager.AppSettings["LOGIN_TIMEOUT"];
                double loginTimeout = 0;

                if (string.IsNullOrEmpty(configValue))
                    loginTimeout = 0;
                else
                    double.TryParse(configValue, out loginTimeout);

                return loginTimeout;
            }
        }

    }
}
