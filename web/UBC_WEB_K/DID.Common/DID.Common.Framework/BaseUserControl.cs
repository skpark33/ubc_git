﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DID.Common.Framework
{
    public class BaseUserControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// 사용자정보
        /// </summary>
        protected UserInfoContext UserInfo = null;

        /// <summary>
        /// 초기화
        /// </summary>
        protected override void OnInit(EventArgs e)
        {
            UserInfo = new UserInfoContext();

            base.OnInit(e);
        }

        /// <summary>
        /// 페이지 접근 권한
        /// </summary>
        protected void IsAccessPageAuth()
        {

        }
    }
}
