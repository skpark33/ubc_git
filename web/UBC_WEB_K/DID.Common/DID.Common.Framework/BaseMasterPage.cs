﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DID.Common.Framework
{
    public class BaseMasterPage : System.Web.UI.MasterPage
    {
        /// <summary>
        /// 사용자정보
        /// </summary>
        protected UserInfoContext UserInfo = null;

        /// <summary>
        /// 초기화
        /// </summary>
        protected override void OnInit(EventArgs e)
        {
            UserInfo = new UserInfoContext();
            base.OnInit(e);
        }

        protected string LANG_FLAG
        {
            get { return UserInfo.LANG_FLAG; }
        }

    }
}
