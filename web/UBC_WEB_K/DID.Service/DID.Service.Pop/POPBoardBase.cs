﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DID.Service.Pop
{
    /// <summary>
    /// 커뮤니티 베이스 클래스
    /// </summary>
    public class POPBoardBase : IDisposable
    {
        private Database db = null;

        protected const string SP_INSERT = "dbo.usp_Community_Board_Reg";
        protected const string SP_UPDATE = "dbo.usp_Community_Board_Upd";
        protected const string SP_REPLY = "dbo.usp_Community_Board_Reply";
        protected const string SP_DELETE = "dbo.usp_Community_Board_Del";
        protected const string SP_READCOUNT_UPDATE = "dbo.usp_Community_Board_ReadCount_Upd";

        protected const string SP_LIST = "dbo.usp_Community_Board_List";
        protected const string SP_FAQ_LIST = "dbo.usp_Community_Faq_List";
        protected const string SP_DETAIL = "dbo.usp_Community_Board_Detail";
        protected const string SP_DETAIL_POPUP = "dbo.usp_Community_Board_Pop";

        protected const string SP_ATTACH_SEL = "dbo.usp_Community_Board_Attach_Sel";
        protected const string SP_ATTACH_DEL = "dbo.usp_Community_Board_Attach_Del";
        protected const string SP_ATTACH_LIST = "dbo.usp_Community_Board_Attach_List";
        protected const string SP_ATTACH_INSERT = "dbo.usp_Community_Board_Attach_Reg";
        protected const string SP_ATTACH_INTRO_LIST = "dbo.usp_Community_Board_Intro_Attach_List";


        public POPBoardBase()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        public void Dispose() { }

        #region 게시물 목록 조회 - GetList
        /// <summary>
        /// 게시물 목록 조회
        /// </summary>
        public DataTable GetList(int boardMasterId
                                , string siteId
                                , string userId
                                , string searchColumn
                                , string searchValue
                                , int page
                                , int pageSize
                                , out int totalRows)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_LIST);

            db.AddInParameter(Cmd, "@BoardMasterID", DbType.Int32, boardMasterId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@SearchColumn", DbType.String, searchColumn);
            db.AddInParameter(Cmd, "@SearchValue", DbType.String,searchValue);
            db.AddInParameter(Cmd, "@Page", DbType.String,page);
            db.AddInParameter(Cmd, "@PageSize", DbType.String,pageSize);

            db.AddOutParameter(Cmd, "@TotalRows",DbType.Int32,4 );

            DataTable dt = db.ExecuteDataSet(Cmd).Tables[0];

            totalRows = Convert.ToInt32(db.GetParameterValue(Cmd, "@TotalRows"));

            return dt;
        }
        #endregion

        #region 게시물 상세조회 - GetDetail
        /// <summary>
        /// 게시물 상세조회
        /// </summary>
        public DataTable GetDetail(long boardId, string siteId)
        {

            DbCommand Cmd = db.GetStoredProcCommand(SP_DETAIL);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);

            return db.ExecuteDataSet(Cmd).Tables[0];

        }
        #endregion

        #region 게시물 첨부파일목록 조회 - GetAttachList
        /// <summary>
        /// 게시물 첨부파일목록 조회
        /// </summary>
        public DataTable GetAttachList(long boardId)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_ATTACH_LIST);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);

            return  db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion

        #region 메인페이지 게시물 첨부파일목록 조회 - GetIntroAttachList
        /// <summary>
        /// 메인페이지 게시물 첨부파일목록 조회
        /// </summary>
        public DataTable GetIntroAttachList()
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_ATTACH_INTRO_LIST);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion


        #region 게시물 첨부파일 조회 - GetAttach
        /// <summary>
        /// 게시물 첨부파일 조회
        /// </summary>
        public DataTable GetAttach(int attachId)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_ATTACH_SEL);

            db.AddInParameter(Cmd, "@attachId", DbType.Int32, attachId);

            return  db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion

        #region 게시물 삭제 - Delete
        /// <summary>
        /// 게시물 삭제
        /// </summary>
        public int Delete(long boardId)
        {

            DbCommand Cmd = db.GetStoredProcCommand(SP_DELETE);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);

            return db.ExecuteNonQuery(Cmd);
        }
        #endregion

        #region 게시물 조회수 증가 - UpdateReadCount
        /// <summary>
        /// 게시물 조회수 증가
        /// </summary>
        public int UpdateReadCount(long boardId)
        {

            DbCommand Cmd = db.GetStoredProcCommand(SP_READCOUNT_UPDATE);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);

            return db.ExecuteNonQuery(Cmd);
        }
        #endregion

        #region 게시물 첨부파일 데이터 저장 - InsertAttach
        /// <summary>
        /// 게시물 첨부파일 데이터 저장
        /// </summary>
        public int InsertAttach(long boardId
                                     , string filePath
                                     , string fileName
                                     , string size)
        {

            DbCommand Cmd = db.GetStoredProcCommand(SP_ATTACH_INSERT);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);
            db.AddInParameter(Cmd, "@filePath", DbType.String, filePath);
            db.AddInParameter(Cmd, "@fileName", DbType.String, fileName);
            db.AddInParameter(Cmd, "@size", DbType.String, size);

            return db.ExecuteNonQuery(Cmd);
        }
        #endregion

        #region 게시물 첨부파일 데이터 삭제 - DeleteAttach
        /// <summary>
        /// 게시물 첨부파일 데이터 삭제
        /// </summary>
        public int DeleteAttach(int attachId)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_ATTACH_DEL);

            db.AddInParameter(Cmd, "@attachId", DbType.Int32, attachId);

            return db.ExecuteNonQuery(Cmd);
        }
        #endregion

        #region 게시물 첨부파일 데이터 삭제 - DeleteAttach
        /// <summary>
        /// 게시물 첨부파일 데이터 삭제
        /// </summary>
        public int DeleteAttachByBoardId(long boardId)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_ATTACH_DEL);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);

            return db.ExecuteNonQuery(Cmd);
        }
        #endregion
    }
}
