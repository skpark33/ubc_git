﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DID.Service.Pop
{
    /// <summary>
    /// 게시판
    /// </summary>
    public class POPBoard : POPBoardBase
    {
        private Database db = null;

        public POPBoard()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        #region 게시물 목록 조회 - GetPopList
        /// <summary>
        /// 게시물 목록 조회
        /// </summary>
        public DataTable GetPopList(int boardMasterId
                                , string siteId
                                , string userId
                                , int page
                                , int pageSize
                                , out int totalRows)
        {
            DbCommand Cmd = db.GetStoredProcCommand("usp_Pop_Board_List");

            db.AddInParameter(Cmd, "@BoardMasterID", DbType.Int32, boardMasterId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@Page", DbType.String,page);
            db.AddInParameter(Cmd, "@PageSize", DbType.String,pageSize);

            db.AddOutParameter(Cmd, "@TotalRows",DbType.Int32,4 );

            DataTable dt = db.ExecuteDataSet(Cmd).Tables[0];

            totalRows = Convert.ToInt32(db.GetParameterValue(Cmd, "@TotalRows"));

            return dt;
        }
        #endregion

        #region 게시물 목록 조회 - GetPopListService
        /// <summary>
        /// 게시물 목록 조회
        /// </summary>
        public DataTable GetPopListService(int boardMasterId, string siteId)
        {
            DbCommand Cmd = db.GetStoredProcCommand("usp_Pop_Board_List_Service");

            db.AddInParameter(Cmd, "@BoardMasterID", DbType.Int32, boardMasterId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);

            DataTable dt = db.ExecuteDataSet(Cmd).Tables[0];

            return dt;
        }
        #endregion

        #region 게시물 등록 - Insert
        /// <summary>
        /// 게시물 등록
        /// </summary>
        public long Insert(int boardMasterId
                            , string userId
                            , string title
                            , string siteId
                            , bool topYn
                            , string ip)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_INSERT);

            db.AddInParameter(Cmd,"@BoardMasterID" , DbType.Int32, boardMasterId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@title", DbType.String, title);
            db.AddInParameter(Cmd, "@contents", DbType.String, siteId);
            db.AddInParameter(Cmd, "@topYn", DbType.Boolean, topYn);
            db.AddInParameter(Cmd, "@ip", DbType.String, ip);

            db.AddOutParameter(Cmd, "@boardId", DbType.Int32, 4);

            db.ExecuteNonQuery(Cmd);

            return Convert.ToInt64(db.GetParameterValue(Cmd, "@boardId"));
        }
        #endregion        

        #region 게시물 수정 - Update
        /// <summary>
        /// 게시물 수정
        /// </summary>
        public int Update(long boardId
                        , string userId
                        , string title
                        , string siteId
                        , bool topYn
                        , string ip)
        {

            DbCommand Cmd = db.GetStoredProcCommand(SP_UPDATE);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@Title", DbType.String, title);
            db.AddInParameter(Cmd, "@Contents", DbType.String, siteId);
            db.AddInParameter(Cmd, "@topYn", DbType.Boolean, topYn);
            db.AddInParameter(Cmd, "@ip", DbType.String, ip);

            return db.ExecuteNonQuery(Cmd);
        }
        #endregion

    }
}
