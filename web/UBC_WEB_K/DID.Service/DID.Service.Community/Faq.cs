﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DID.Service.Community
{
    /// <summary>
    /// FAQ
    /// </summary>
    public class Faq : BoardBase
    {
        private Database db = null;

        public Faq()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        #region 게시물 목록 조회 - GetFaqList
        /// <summary>
        /// 게시물 목록 조회
        /// </summary>
        public DataTable GetFaqList(int boardMasterId, string userId)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_FAQ_LIST);

            db.AddInParameter(Cmd, "@BoardMasterID", DbType.Int32, boardMasterId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            
            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion

        #region 게시물 등록 - Insert
        /// <summary>
        /// 게시물 등록
        /// </summary>
        public long Insert(int boardMasterId
                            , string userId
                            , string contents
                            , string contents_reply
                            , string faqCd
                            , string ip)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_INSERT);

            db.AddInParameter(Cmd,"@BoardMasterID" , DbType.Int32, boardMasterId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@contents", DbType.String, contents);
            db.AddInParameter(Cmd, "@contents_reply", DbType.String, contents_reply);
            db.AddInParameter(Cmd, "@faqCd", DbType.String, faqCd);
            db.AddInParameter(Cmd, "@ip", DbType.String, ip);

            db.AddOutParameter(Cmd, "@boardId", DbType.Int32, 4);

            db.ExecuteNonQuery(Cmd);

            return Convert.ToInt64(db.GetParameterValue(Cmd, "@boardId"));
        }
        #endregion

        #region 게시물 수정 - Update
        /// <summary>
        /// 게시물 수정
        /// </summary>
        public int Update(long boardId
                        , string userId
                        , string contents
                        , string contents_reply
                        , string faqCd
                        , string ip)
        {

            DbCommand Cmd = db.GetStoredProcCommand(SP_UPDATE);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@Contents", DbType.String, contents);
            db.AddInParameter(Cmd, "@contents_reply", DbType.String, contents_reply);
            db.AddInParameter(Cmd, "@faqCd", DbType.String, faqCd);
            db.AddInParameter(Cmd, "@ip", DbType.String, ip);

            return db.ExecuteNonQuery(Cmd);
        }
        #endregion

    }
}
