﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Utility
{
    public class Common : IDisposable
    {
        private Database db = null;

        public Common()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        public void Dispose(){ }

        #region Work Year - GetWorkYear()
        /// <summary>
        /// Work Year
        /// </summary>
        /// <returns></returns>
        public DataTable GetWorkYear()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("WORK_YYYY");
            dt.Columns.Add("WORK_NAME");

            int baseYear = 2000;

            for (int i = 20; i >= 2; i--)
            {
                DataRow dr = dt.NewRow();

                dr["WORK_YYYY"] = baseYear + i;
                dr["WORK_NAME"] = baseYear + i;

                dt.Rows.Add(dr);
            }

            return dt;
        }
        #endregion

        #region Work Month - GetWorkMonth()
        /// <summary>
        /// Work Month
        /// </summary>
        /// <returns></returns>
        public DataTable GetWorkMonth()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("WORK_MONTH");
            dt.Columns.Add("WORK_NAME");

            for (int i = 1; i <= 12; i++)
            {
                DataRow dr = dt.NewRow();

                dr["WORK_MONTH"] = i.ToString().Length == 1 ? "0" + i.ToString() : i.ToString();
                dr["WORK_NAME"] = i;
                dt.Rows.Add(dr);
            }

            return dt;
        }
        #endregion

        #region 페이지 메뉴명 조회- GetMenuName(string PROG_ID)
        /// <summary>
        /// 페이지 메뉴명 조회
        /// </summary>
        public string GetMenuName(string PROG_ID, string LANG_FLAG)
        {
            DbCommand Cmd = db.GetSqlStringCommand(@"
                            SELECT MENU_NAME FROM dbo.TB_POT_MENU_MIDDLE WHERE USE_FLAG='Y' AND PROG_ID = @PROG_ID AND LANG_FLAG=@LANG_FLAG
                            UNION ALL
                            SELECT MENU_NAME FROM dbo.TB_POT_MENU_SMALL WHERE USE_FLAG='Y' AND PROG_ID = @PROG_ID AND LANG_FLAG=@LANG_FLAG
                            ");

            db.AddInParameter(Cmd, "@PROG_ID", DbType.String, PROG_ID);
            db.AddInParameter(Cmd, "@LANG_FLAG", DbType.String, LANG_FLAG);

            DataTable dt = db.ExecuteDataSet(Cmd).Tables[0];

            return dt.Rows.Count > 0 ?  dt.Rows[0][0].ToString() : string.Empty;
        }
        #endregion

        #region 시 - GetSidoCode()
        /// <summary>
        /// 시
        /// </summary>
        /// <returns></returns>
        public DataTable GetSidoCode()
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_zipcode");

            db.AddInParameter(Cmd, "@MODE", DbType.String, "A");

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion

        #region 구 - GetGuCode(string SIDO)
        /// <summary>
        /// 구
        /// </summary>
        /// <returns></returns>
        public DataTable GetGuCode(string SIDO)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_zipcode");

            db.AddInParameter(Cmd, "@MODE", DbType.String, "B");
            db.AddInParameter(Cmd, "@SIDO", DbType.String, SIDO);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion

        #region 동 - GetDongCode(string SIDO, string GU)
        /// <summary>
        /// 동
        /// </summary>
        /// <returns></returns>
        public DataTable GetDongCode(string SIDO, string GU)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_zipcode");

            db.AddInParameter(Cmd, "@MODE", DbType.String, "C");
            db.AddInParameter(Cmd, "@SIDO", DbType.String, SIDO);
            db.AddInParameter(Cmd, "@GU", DbType.String, GU);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion

        #region 공통코드 조회 - GetCommonCode(string ITEM_CODE)
        /// <summary>
        /// 공통코드 조회
        /// </summary>
        /// <returns></returns>
        public DataTable GetCommonCode(string mgrId, string categoryName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_CommonCode_Sel");

            db.AddInParameter(Cmd, "@mgrId", DbType.String, mgrId);
            db.AddInParameter(Cmd, "@categoryName", DbType.String, categoryName);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion

        #region 공통 - 우편번호 검색  -GetAddressList(string SEARCH_WORD)
        /// <summary>
        /// 공통 - 우편번호 검색  
        ///  
        ///  
        /// </summary>
        public DataTable GetAddressList(string SEARCH_WORD)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_zipcode");

            db.AddInParameter(Cmd, "@MODE", DbType.String, "G");
            db.AddInParameter(Cmd, "@SEARCH_WORD", DbType.String, SEARCH_WORD);
            return db.ExecuteDataSet(Cmd).Tables[0];

        }
        #endregion

    }
}
