﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Utility
{
    public class Member : IDisposable
    {
        private Database db = null;

        public Member()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        public void Dispose(){ }

        #region 로그인 관련

        public DataTable ValidateUser(string mgrId, string siteId, string userId, string password)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Member_ValidateUser");
            db.AddInParameter(Cmd, "@mgrId", DbType.String, mgrId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@password", DbType.String, password);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public DataTable GetUserInfo(string mgrId, string siteId, string userId)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Member_GetUserInfo");

            db.AddInParameter(Cmd, "@mgrId", DbType.String, mgrId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        // 2011-08-08 임유석 추가
        // 비밀번호 분실시 아이디와 이메일로 인증 후 새로운 비밀번호 생성
        public DataTable GetNewPassword(string userId, string email)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Member_GetNewPassword");
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@email", DbType.String, email);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        // 2011-08-08 임유석 추가
        // 비밀번호 분실 후 새로운 비밀번호 부여 후 다시 업데이트
        public DataTable UpdatePassword(string userId, string email, string oldPassword, string newPassword)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Member_UpdatePassword");
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@email", DbType.String, email); 
            db.AddInParameter(Cmd, "@oldPassword", DbType.String, oldPassword);
            db.AddInParameter(Cmd, "@newPassword", DbType.String, newPassword);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        #endregion

        #region 메뉴 관련


        public DataTable GetMenuLarge(string COMP_CODE, string LANG_FLAG , string USER_TYPE)
        {
            string userType = "";
            // 유저저타잎이 -1 (개발자) 일때의 처리 : 이때만 전부 보여줌
            // 유저저타잎이  1 (총괄관리자) 일때의 처리 : 이때도 전부 보여줌
            if (USER_TYPE == "-1" || USER_TYPE == "1")
            {
                userType = @" ";
            }
            else {
                userType = @" AND USER_TYPE = '3' ";
                
            }

            string cmdQry = @"
            SELECT 
                LARGE_CODE
                , MENU_NAME 
            FROM 
                dbo.ubc_web_menu_large 
            WHERE 
                COMP_CODE=@COMP_CODE AND USE_FLAG='Y' AND VISIBLE_FLAG='Y' AND LANG_FLAG=@LANG_FLAG
            "+ userType +
            @"ORDER BY 
                SORT_NUM
            ";

            DbCommand Cmd = db.GetSqlStringCommand(cmdQry);

            db.AddInParameter(Cmd, "@COMP_CODE", DbType.String, COMP_CODE);
            db.AddInParameter(Cmd, "@LANG_FLAG", DbType.String, LANG_FLAG);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }



        public DataTable GetMenuMiddle(string COMP_CODE, string LARGE_CODE, string LANG_FLAG)
        {
            string cmdQry = @"
            SELECT 
                MIDDLE_CODE
	            , MENU_NAME
	            , PROG_ID
            FROM 
	            dbo.ubc_web_menu_middle 
            WHERE 
	            COMP_CODE=@COMP_CODE AND LARGE_CODE=@LARGE_CODE AND USE_FLAG='Y' AND VISIBLE_FLAG='Y' AND LANG_FLAG=@LANG_FLAG
            ORDER BY
	            SORT_NUM
            ";

            DbCommand Cmd = db.GetSqlStringCommand(cmdQry);

            db.AddInParameter(Cmd, "@COMP_CODE", DbType.String, COMP_CODE);
            db.AddInParameter(Cmd, "@LARGE_CODE", DbType.String, LARGE_CODE);
            db.AddInParameter(Cmd, "@LANG_FLAG", DbType.String, LANG_FLAG);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

     

        public DataTable GetMenuLargeName(string COMP_CODE, string LARGE_CODE, string LANG_FLAG)
        {
            string cmdQry = @"
            SELECT 
                  MENU_NAME
	            , PROG_ID
            FROM 
	            dbo.ubc_web_menu_large
            WHERE 
	            COMP_CODE=@COMP_CODE AND LARGE_CODE=@LARGE_CODE AND USE_FLAG='Y' AND VISIBLE_FLAG='Y' AND LANG_FLAG=@LANG_FLAG
            ORDER BY
	            SORT_NUM
            ";

            DbCommand Cmd = db.GetSqlStringCommand(cmdQry);

            db.AddInParameter(Cmd, "@COMP_CODE", DbType.String, COMP_CODE);
            db.AddInParameter(Cmd, "@LARGE_CODE", DbType.String, LARGE_CODE);
            db.AddInParameter(Cmd, "@LANG_FLAG", DbType.String, LANG_FLAG);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        
        #endregion

    }
}
