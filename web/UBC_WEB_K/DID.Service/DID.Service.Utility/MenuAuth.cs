﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Utility
{
    public class MenuAuth : IDisposable
    {
        private Database db = null;

        public MenuAuth()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        public void Dispose(){ }

        /// <summary>
        /// 메뉴권한 체크
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <param name="PROG_ID"></param>
        /// <returns></returns>
        public DataTable GetMenuAuthUser(string USER_ID, string PROG_ID)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_menu_auth_user");

            db.AddInParameter(Cmd, "@USER_ID", DbType.String, USER_ID);
            db.AddInParameter(Cmd, "@PROG_ID", DbType.String, PROG_ID);
            
            return db.ExecuteDataSet(Cmd).Tables[0];
        }

    }
}
