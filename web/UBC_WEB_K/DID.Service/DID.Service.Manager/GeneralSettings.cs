﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Manager
{ 
    public class GeneralSettings : IDisposable 
    {
        private Database db = null;

        public GeneralSettings()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() { }
                
        public DataTable GetCustomerURL(string customer)   
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Customer_Url_Sel");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customer);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public int SetCustomerURL(string customer, string url)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Customer_Url_Upd");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customer);
            db.AddInParameter(Cmd, "@url", DbType.String, url);

            return db.ExecuteNonQuery(Cmd);
        }

        public int SetCustomerCopyURL(string customerId, string copyUrl)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_generalSettings_CopyUrl_Upd");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customerId);
            db.AddInParameter(Cmd, "@copyUrl", DbType.String, copyUrl);

            return db.ExecuteNonQuery(Cmd);
        }
    }
}
