﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Manager
{ 
    public class Smtp : IDisposable 
    {
        private Database db = null;

        public Smtp()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() { }


        /// <summary>
        /// SMTP 
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <param name="PROG_ID"></param>
        /// <returns></returns>


        public int smtpLogInsert(string company, string send_email, string receive_email, string title, string contents, string success_yn)   // 로그
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Smtp_Insert");
            db.AddInParameter(Cmd, "@company",        DbType.String, company);
            db.AddInParameter(Cmd, "@send_email",     DbType.String, send_email);
            db.AddInParameter(Cmd, "@receive_email", DbType.String, receive_email);
            db.AddInParameter(Cmd, "@title",          DbType.String, title);
            db.AddInParameter(Cmd, "@contents",       DbType.String, contents);
            db.AddInParameter(Cmd, "@success_yn",     DbType.String, success_yn);
            return db.ExecuteNonQuery(Cmd);
        }
         

    }
}
