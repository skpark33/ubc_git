<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' sql query
	Dim strSQL
	'strSQL = "select * from ubc_contents where isPublic = '1' and contentsType <> '19' and ( parentsId = '' or parentsId is null )"
	strSQL = "select * from ubc_contents where siteId = 'Public' and programId = 'Public' and isPublic = '1'"

	' contentsName
	Dim strContentsName
	strContentsName = Request("contentsName")
	if strContentsName <> "" then
		strSQL = strSQL + " and contentsName like '%%" + strContentsName + "%%'"
	end if

	' category
	Dim strCategory
	strCategory = Request("category")
	if strCategory <> "" then
		strSQL = strSQL + " and category like '%" + strCategory + "%'"
	end if

	' register
	Dim strRegister
	strRegister = Request("register")
	if strRegister <> "" then
		strSQL = strSQL + " and registerId like '%" + strRegister + "%'"
	end if

	' requester
	Dim strRequester
	strRequester = Request("requester")
	if strRequester <> "" then
		strSQL = strSQL + " and requester like '%" + strRequester + "%'"
	end if

	' filename
	Dim strFilename
	strFilename = Request("filename")
	if strFilename <> "" then
		strSQL = strSQL + " and filename like '%" + strFilename + "%'"
	end if

	' startTime
	Dim strStartTime
	strStartTime = Request("startTime")
	if strStartTime <> "" then
		strSQL = strSQL + " and registerTime > CAST('" + strStartTime + " 00:00:00' as datetime)"
	end if

	' endTime
	Dim strEndTime
	strEndTime = Request("endTime")
	if strEndTime <> "" then
		strSQL = strSQL + " and registerTime < CAST('" + strEndTime + " 00:00:00' as datetime)"
	end if

	' contentsType
	Dim strContentsType
	strContentsType = Request("contentsType")
	if strContentsType <> "" then
		strSQL = strSQL + " and contentsType in (" + strContentsType + ")"
	end if

	' contentsState
	Dim strContentsState
	strContentsState = Request("contentsState")
	if strContentsState <> "" then
		strSQL = strSQL + " and contentsState in (" + strContentsState + ")"
	end if

	' contentsCategory
	Dim strContentsCategory
	strContentsCategory = Request("contentsCategory")
	if strContentsCategory <> "" then
		strSQL = strSQL + " and contentsCategory in (" + strContentsCategory + ")"
	end if

	' purpose
	Dim strPurpose
	strPurpose = Request("purpose")
	if strPurpose <> "" then
		strSQL = strSQL + " and purpose in (" + strPurpose + ")"
	end if

	' hostType
	Dim strHostType
	strHostType = Request("hostType")
	if strHostType <> "" then
		strSQL = strSQL + " and hostType in (" + strHostType + ")"
	end if

	' vertical
	Dim strVertical
	strVertical = Request("vertical")
	if strVertical <> "" then
		strSQL = strSQL + " and vertical in (" + strVertical + ")"
	end if

	' resolution
	Dim strResolution
	strResolution = Request("resolution")
	if strResolution <> "" then
		strSQL = strSQL + " and resolution in (" + strResolution + ")"
	end if

	strSQL = strSQL + " order by contentsName"

	'debuging query-out
	'Response.Write strSQL + vbCrLf

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	Do While Not ObjRS.EOF
		Dim tm

		Response.Write "contentsId=" + objRS("contentsId") + vbCrLf
		Response.Write "mgrId=" + objRS("mgrId") + vbCrLf
		Response.Write "siteId=" + objRS("siteId") + vbCrLf
		Response.Write "programId=" + objRS("programId") + vbCrLf
		Response.Write "category=" + objRS("category") + vbCrLf
		Response.Write "contentsName=" + objRS("contentsName") + vbCrLf
		if not IsNull(objRS("contentsType")) then
			Response.Write "contentsType=" + CStr(objRS("contentsType")) + vbCrLf
		end if
		Response.Write "parentsId=" + objRS("parentsId") + vbCrLf
		Response.Write "registerId=" + objRS("registerId") + vbCrLf
		if not IsNull(objRS("registerTime")) then
			tm = objRS("registerTime")
			Response.Write "registerTime=" + TimeToString(tm) + vbCrLf
		end if
		Response.Write "location=" + objRS("location") + vbCrLf
		Response.Write "filename=" + objRS("filename") + vbCrLf
		if not IsNull(objRS("runningTime")) then
			Response.Write "runningTime=" + CStr(objRS("runningTime")) + vbCrLf
		end if
		Response.Write "verifier=" + objRS("verifier") + vbCrLf
		Response.Write "description=" + objRS("description") + vbCrLf
		Response.Write "comment1=" + objRS("comment1") + vbCrLf
		Response.Write "comment2=" + objRS("comment2") + vbCrLf
		Response.Write "comment3=" + objRS("comment3") + vbCrLf
		Response.Write "comment4=" + objRS("comment4") + vbCrLf
		Response.Write "comment5=" + objRS("comment5") + vbCrLf
		Response.Write "comment6=" + objRS("comment6") + vbCrLf
		Response.Write "comment7=" + objRS("comment7") + vbCrLf
		Response.Write "comment8=" + objRS("comment8") + vbCrLf
		Response.Write "comment9=" + objRS("comment9") + vbCrLf
		Response.Write "comment10=" + objRS("comment10") + vbCrLf
		Response.Write "bgColor=" + objRS("bgColor") + vbCrLf
		Response.Write "fgColor=" + objRS("fgColor") + vbCrLf
		Response.Write "font=" + objRS("font") + vbCrLf
		if not IsNull(objRS("fontSize")) then
			Response.Write "fontSize=" + CStr(objRS("fontSize")) + vbCrLf
		end if
		if not IsNull(objRS("playSpeed")) then
			Response.Write "playSpeed=" + CStr(objRS("playSpeed")) + vbCrLf
		end if
		if not IsNull(objRS("soundVolume")) then
			Response.Write "soundVolume=" + CStr(objRS("soundVolume")) + vbCrLf
		end if
		if not IsNull(objRS("isPublic")) then
			Response.Write "isPublic=" + CStr(objRS("isPublic")) + vbCrLf
		end if
		if not IsNull(objRS("align")) then
			Response.Write "align=" + CStr(objRS("align")) + vbCrLf
		end if
		if not IsNull(objRS("width")) then
			Response.Write "width=" + CStr(objRS("width")) + vbCrLf
		end if
		if not IsNull(objRS("height")) then
			Response.Write "height=" + CStr(objRS("height")) + vbCrLf
		end if
		if not IsNull(objRS("currentComment")) then
			Response.Write "currentComment=" + CStr(objRS("currentComment")) + vbCrLf
		end if
		if not IsNull(objRS("volume")) then
			Response.Write "volume=" + CStr(objRS("volume")) + vbCrLf
		end if
		if not IsNull(objRS("contentsState")) then
			Response.Write "contentsState=" + CStr(objRS("contentsState")) + vbCrLf
		end if
		if not IsNull(objRS("contentsCategory")) then
			Response.Write "contentsCategory=" + CStr(objRS("contentsCategory")) + vbCrLf
		end if
		if not IsNull(objRS("purpose")) then
			Response.Write "purpose=" + CStr(objRS("purpose")) + vbCrLf
		end if
		if not IsNull(objRS("hostType")) then
			Response.Write "hostType=" + CStr(objRS("hostType")) + vbCrLf
		end if
		if not IsNull(objRS("vertical")) then
			Response.Write "vertical=" + CStr(objRS("vertical")) + vbCrLf
		end if
		if not IsNull(objRS("resolution")) then
			Response.Write "resolution=" + CStr(objRS("resolution")) + vbCrLf
		end if
		if not IsNull(objRS("validationDate")) then
			tm = objRS("validationDate")
			Response.Write "validationDate=" + CStr(DatePart("yyyy", tm)) + "/" + CStr(DatePart("m", tm)) + "/" + CStr(DatePart("d", tm)) + " " + CStr(DatePart("h", tm)) + ":" + + CStr(DatePart("n", tm)) + ":" + + CStr(DatePart("s", tm)) + vbCrLf
		end if
		Response.Write "requester=" + objRS("requester") + vbCrLf
		Response.Write "verifyMembers=" + objRS("verifyMembers") + vbCrLf
		if not IsNull(objRS("verifyTime")) then
			tm = objRS("verifyTime")
			Response.Write "verifyTime=" + CStr(DatePart("yyyy", tm)) + "/" + CStr(DatePart("m", tm)) + "/" + CStr(DatePart("d", tm)) + " " + CStr(DatePart("h", tm)) + ":" + + CStr(DatePart("n", tm)) + ":" + + CStr(DatePart("s", tm)) + vbCrLf
		end if
		Response.Write "registerType=" + objRS("registerType") + vbCrLf
		Response.Write "wizardXML=" + objRS("wizardXML") + vbCrLf
		Response.Write "wizardFiles=" + objRS("wizardFiles") + vbCrLf

		ObjRS.MoveNext
	Loop


	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
