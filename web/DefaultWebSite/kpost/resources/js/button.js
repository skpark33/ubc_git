function delete_data(boardId, compType) {
	var id = boardId;
	var type = compType;
	if(confirm("삭제하시겠습니까?") == true) {
		location.href = 'deleteData.do?boardId=' + id + '&compType=' + type;
	}
	else {
		return;
	}
}

function update_data(boardId, compType) {
	var id = boardId;
	var type = compType;
	$('#loading').show();
	$.ajax({
		type: "POST",
		url: "updateSet.do",
		data: { "boardId": id, "compType": type },
		success: function(html) {
			$('#loading').hide();
			$("#utable").html(html);
			$('#mhost').multiselect({
					header: "적용할 우체국 선택",
					checkAllText: "전체선택",
					uncheckAllText: "선택해제",
					noneSelectedText: "우체국 선택",
					height: 250,
				});
			$('#updatepopup').bPopup({});
			$('#updatePopClose').bind('click', function(e) {
				var pop = $('#updatepopup').bPopup();
		    	pop.close();
		    });
		}
	});
}

function notice_check() {
	if($.trim($('#title').val()) == "") {
		alert("타이틀을 입력하세요");
		$('#title').focus();
		return false;
	}
}

function unotice_check() {
	if($.trim($('#mtitle').val()) == "") {
		alert("타이틀을 입력하세요");
		$('#mtitle').focus();
		return false;
	}
}

function sijang_check() {
	if($.trim($('#title').val()) == "") {
		alert("타이틀을 입력하세요");
		$('#title').focus();
		return false;
	}
	
	if($.trim($('#contents').val()) == "") {
		alert("url을 입력하세요");
		$('#contents').focus();
		return false;
	}
}

function mtc_check() {
	if($.trim($('#mtitle').val()) == "") {
		alert("타이틀을 입력하세요");
		$('#title').focus();
		return false;
	}
	
	if($.trim($('#mcontents').val()) == "") {
		alert("url을 입력하세요");
		$('#contents').focus();
		return false;
	}
}

function promote_check() {
	var limitchar = /[^<>,!%`"\\/*|:~?\[\]\\\\]/;
	
	if($.trim($('#title').val()) == "") {
		alert("타이틀을 입력하세요");
		$('#title').focus();
		return false;
	}
	if($.trim($(':file').val()) == "") {
		alert("이미지파일을 업로드 하세요");
		return false;
	}
	if($.trim($(':file').val()) != "") {
		var file = $(':file').val();
		var fname = file.slice(file.lastIndexOf("\\")+1, file.lastIndexOf(".")).toLowerCase();
		var ext = file.slice(file.lastIndexOf(".") + 1).toLowerCase();
		if(!(ext==='jpg' || ext==='png' || ext==='bmp')) {
			alert("이미지파일(jpg, png, bmp)만 업로드 가능합니다.");
			return false;
		}
		for(var i =0; i<fname.length; i++) {
			if(limitchar.test(fname.charAt(i)) == false) {
				alert("파일명에 다음과 같은 문자열은 사용하실 수 없습니다. \n  [ ] < > , ! % ` \" \\ / \\\\ * | : ~ ? ");
				return false;
			}
		}
	}
}

function mtf_check() {
	var limitchar = /[^<>,!%`"\\/*|:~?\[\]\\\\]/;
	
	if($.trim($('#mtitle').val()) == "") {
		alert("타이틀을 입력하세요");
		$('#title').focus();
		return false;
	}
	if($.trim($('#ufile').val()) != "") {
		var file = $('#ufile').val();
		var fname = file.slice(file.lastIndexOf("\\")+1, file.lastIndexOf(".")).toLowerCase();
		var ext = file.slice(file.lastIndexOf(".") + 1).toLowerCase();
		if(!(ext==='jpg' || ext==='png' || ext==='bmp')) {
			alert("이미지파일(jpg, png, bmp)만 업로드 가능합니다.");
			return false;
		}
		for(var i =0; i<fname.length; i++) {
			if(limitchar.test(fname.charAt(i)) == false) {
				alert("파일명에 다음과 같은 문자열은 사용하실 수 없습니다. \n  [ ] < > , ! % ` \" \\ / \\\\ * | : ~ ? ");
				return false;
			}
		}
	}
}