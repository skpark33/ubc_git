DELIMITER $$
DROP PROCEDURE IF EXISTS dbo.Statistics_Kpost_period_jq $$
CREATE PROCEDURE dbo.`Statistics_Kpost_period_jq`(in sdate varchar(16), in fdate varchar(16), in period varchar(16), in menu1 varchar(64), in menu2 varchar(64), in colchk varchar(2) )
BEGIN
  drop temporary table if exists yeartable;
  drop temporary table if exists monthtable;
  drop temporary table if exists quartertable;
  drop temporary table if exists daytable;
  
  #yeartable temporary table
	create TEMPORARY table  yeartable (
    nid int not null AUTO_INCREMENT primary key,
    value varchar(8) not null
  );
  insert into yeartable(value) 
  select distinct year(playDate) from dbo.ubc_interactivelog where playDate between sdate and fdate order by playDate;
  #=========================================================================
  #monthtable temporary table
  create TEMPORARY table  monthtable (
    nid int not null AUTO_INCREMENT primary key,
    value varchar(8) not null
  );
  insert into monthtable(value) 
  select distinct month(playDate) from dbo.ubc_interactivelog where playDate between sdate and fdate order by playDate;
  #=========================================================================
  #quartertable temporary table
  create TEMPORARY table  quartertable (
    nid int not null AUTO_INCREMENT primary key,
    value varchar(8) not null
  );
  insert into quartertable(value) 
  select distinct quarter(playDate) from dbo.ubc_interactivelog where playDate between sdate and fdate order by playDate;
  #=========================================================================
  #daytable temporary table
  create TEMPORARY table  daytable (
    nid int not null AUTO_INCREMENT primary key,
    value varchar(8) not null
  );
  insert into daytable(value) 
  select distinct day(playDate) from dbo.ubc_interactivelog where playDate between sdate and fdate order by playDate;
  #=========================================================================
  
  SET @strSql = '';
  SET @colname = '';  
  SET @i1 = 1;
  #SET @i2 = 1;
  #SET @i3 = 1;
  
  select max(nid) into @max1 from yeartable;
  select max(nid) into @max2 from monthtable;
  select max(nid) into @max3 from quartertable;
  select max(nid) into @max4 from daytable;

  
  #year Search 
  IF period = 'year' THEN
  
    IF menu1 = '' or menu1 = '전체' THEN
    
      SET @strSql := CONCAT('select c.keyword1 as Keyword, ');
      WHILE @i1 <= @max1 DO 
      
        select value into @colname from yeartable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@colname, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@colname, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@colname, ''' then c.cnt else 0 end) as ''', @colname, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@colname, ''' then c.cnt else 0 end) as ''', @colname, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword1, year(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 is not null group by keyword1, year(playDate) ) c group by c.keyword1 order by c.keyword1;');
      
    ELSEIF menu2 = '' or menu2 = '전체' THEN
    
      SET @strSql := CONCAT('select c.keyword2 as Keyword, ');
      WHILE @i1 = @max1 DO 
      
        select value into @period from yeartable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword2, year(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 <> '''' and keyword2 is not null group by keyword2, year(playDate) ) c group by c.keyword2 order by c.keyword2;');
      
    ELSE
    
      SET @strSql := CONCAT('select c.keyword3 as Keyword, ');
      WHILE @i1 <= @max1 DO 
      
        select value into @period from yeartable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword3, year(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 = ''', menu2, ''' and keyword3 <> '''' and keyword3 is not null group by keyword2, year(playDate) ) c group by c.keyword3 order by c.keyword3;');
      
    END IF;
  
  #month Search 
  ELSEIF period = 'month' THEN
  
    IF menu1 = '' or menu1 = '전체' THEN
    
      SET @strSql := CONCAT('select c.keyword1 as Keyword, ');
      WHILE @i1 <= @max2 DO 
      
        select value into @period from monthtable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword1, month(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 is not null group by keyword1, month(playDate) ) c group by c.keyword1 order by c.keyword1;');
      
    ELSEIF menu2 = '' or menu2 = '전체' THEN
      
      SET @strSql := CONCAT('select c.keyword2 as Keyword, ');
      WHILE @i1 <= @max2 DO 
      
        select value into @period from monthtable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword2, month(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 <> '''' and keyword2 is not null group by keyword2, month(playDate) ) c group by c.keyword2 order by c.keyword2;');
      
    ELSE
      
      SET @strSql := CONCAT('select c.keyword3 as Keyword, ');
      WHILE @i1 <= @max2 DO 
      
        select value into @period from monthtable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword3, month(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 = ''', menu2, ''' and keyword3 <> '''' and keyword3 is not null group by keyword2, month(playDate) ) c group by c.keyword3 order by c.keyword3;');
      
    END IF;
    
  
  #quarter Search  
  ELSEIF period = 'quarter' THEN
  
    IF menu1 = '' or menu1 = '전체' THEN
      
       SET @strSql := CONCAT('select c.keyword1 as Keyword, ');
      WHILE @i1 <= @max3 DO 
      
        select value into @period from quartertable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword1, quarter(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 is not null group by keyword1, quarter(playDate) ) c group by c.keyword1 order by c.keyword1;');
      
    ELSEIF menu2 = '' or menu2 = '전체' THEN
      
       SET @strSql := CONCAT('select c.keyword2 as Keyword, ');
      WHILE @i1 <= @max3 DO 
      
        select value into @period from quartertable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword2, quarter(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 <> '''' and keyword2 is not null group by keyword2, quarter(playDate) ) c group by c.keyword2 order by c.keyword2;');
      
    ELSE
      
       SET @strSql := CONCAT('select c.keyword3 as Keyword, ');
      WHILE @i1 <= @max3 DO 
      
        select value into @period from quartertable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword3, quarter(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 = ''', menu2, ''' and keyword3 <> '''' and keyword3 is not null group by keyword3, quarter(playDate) ) c group by c.keyword3 order by c.keyword3;');
      
    END IF;
  
  
  #days Search  
  ELSEIF period = 'days' THEN
  
    IF menu1 = '' or menu1 = '전체' THEN
      
      SET @strSql := CONCAT('select c.keyword1 as Keyword, ');
      WHILE @i1 <= @max4 DO 
      
        select value into @period from daytable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max4) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max4) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword1, day(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 is not null group by keyword1, day(playDate) ) c group by c.keyword1 order by c.keyword1;');
      
    ELSEIF menu2 = '' or menu2 = '전체' THEN
      
      SET @strSql := CONCAT('select c.keyword2 as Keyword, ');
      WHILE @i1 <= @max4 DO 
      
        select value into @period from daytable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max4) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max4) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword2, day(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 <> '''' and keyword2 is not null group by keyword2, day(playDate) ) c group by c.keyword2 order by c.keyword2;');
      
    ELSE
      
      SET @strSql := CONCAT('select c.keyword3 as Keyword, ');
      WHILE @i1 <= @max4 DO 
      
        select value into @period from daytable where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max4) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max4) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.Period = ''',@period, ''' then c.cnt else 0 end) as ''', @period, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword3, day(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 = ''', menu2, ''' and keyword3 <> '''' and keyword3 is not null group by keyword3, day(playDate) ) c group by c.keyword3 order by c.keyword3;');
      
    END IF;
   
  END IF;
  
  prepare stmt from @strSql;
  execute stmt;
  deallocate prepare stmt;
  
END $$
DELIMITER ;