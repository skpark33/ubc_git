DELIMITER $$
DROP PROCEDURE IF EXISTS dbo.Statistics_Kpost_org $$
CREATE PROCEDURE dbo.`Statistics_Kpost_org`(in siteId varchar(64), in inFranchizeType varchar(64))
BEGIN
	SELECT  siteId AS code, siteName AS name
  FROM ubc_site
	WHERE parentId = siteId
		   AND franchizeType = inFranchizeType
	ORDER BY siteName;
END $$
DELIMITER ;