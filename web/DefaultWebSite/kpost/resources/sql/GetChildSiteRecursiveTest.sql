DROP PROCEDURE IF EXISTS dbo.GetChildSiteRecursiveTest;
CREATE PROCEDURE dbo.`GetChildSiteRecursiveTest`(level INT, inFranchizeType varchar(255), inParentId varchar(255), inNamePath varchar(8000), inIdPath varchar(8000))
BEGIN

  
  DECLARE done INT DEFAULT 0;
  DECLARE _siteName varchar(255);
  DECLARE _siteId varchar(255);
  DECLARE _level INT DEFAULT 0;
  DECLARE _id_path varchar(255);
  DECLARE _name_path varchar(255);

  
  DECLARE cur1 CURSOR FOR SELECT siteName, siteId, lvl, name_path, id_path from dbo.TempChildSite where franchizeType = inFranchizeType and parentId = inParentId;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

  
  INSERT INTO dbo.TempChildSite
  SELECT mgrId, siteName, siteId, parentId, level, CONCAT(inNamePath, CONCAT('/', siteName)), CONCAT(inIdPath, CONCAT('/', siteId)), inFranchizeType 
  FROM dbo.ubc_site 
  WHERE franchizeType = inFranchizeType 
  AND parentId = inParentId;
 
  
  SET done = 0;
  
  
  OPEN cur1;

  
  REPEAT
    FETCH cur1 INTO _siteName, _siteId, _level, _name_path, _id_path;
    
    IF NOT done THEN
      CALL dbo.GetChildSiteRecursive(_level + 1, inFranchizeType, _siteId, _name_path, _id_path);
    END IF;
    
  UNTIL done END REPEAT;
  
    
  CLOSE cur1;
  
  select h.hostId, h.hostName
  from ubc_host as h,
    (select siteId from dbo.TempChildSite) as temp
  where h.siteId = temp.siteId;
  
END;
