DELIMITER $$
DROP PROCEDURE IF EXISTS dbo.Statistics_Kpost_topten $$
CREATE PROCEDURE dbo.`Statistics_Kpost_topten`(in sdate varchar(16), in fdate varchar(16), in menu1 varchar(64), in menu2 varchar(64))
BEGIN
  IF menu1 = '' or menu1 = '전체' then
    select
        @rank:=@rank+1 rank, c.*
        from (select @rank :=0) rank ,
        (SELECT
            keyword1 as contents,
  	        sum(counter) as point
        FROM ubc_interactivelog 
        WHERE playDate BETWEEN sdate AND fdate
  			    AND keyword1 is not null
  				
  			group by keyword1
  			order by point desc
        ) c
    limit 10;
  
  ELSEIF menu2 = '' or menu2 = '전체' then
      select
          @rank:=@rank+1 rank, c.*
      from (select @rank :=0) rank ,
          (SELECT
              keyword2 as contents,
  	          sum(counter) as point
          FROM ubc_interactivelog 
          WHERE playDate BETWEEN sdate AND fdate
  			  AND keyword1 = menu1
          AND keyword2 is not null 
          AND keyword2 <> ''
  				
  			  group by keyword2
  			  order by point desc
          ) c
      limit 10;
  
  ELSE
      select
          @rank:=@rank+1 rank, c.*
      from (select @rank :=0) rank ,
          (SELECT
              keyword3 as contents,
  	          sum(counter) as point
          FROM ubc_interactivelog 
          WHERE playDate BETWEEN sdate AND fdate
              AND keyword1 = menu1
      			  AND keyword2 = menu2
              AND keyword3 is not null 
              AND keyword3 <> ''
  				
  			  group by keyword3
  			  order by point desc
          ) c
      limit 10;
  END IF;
END $$
DELIMITER ;

