DELIMITER $$
DROP PROCEDURE IF EXISTS dbo.Statistics_Kpost_keyword $$
CREATE PROCEDURE dbo.`Statistics_Kpost_keyword`(in menu1 varchar(64), in menu2 varchar(64))
BEGIN
	if menu1 = '' THEN
    select keyword1 as name
    from ubc_interactivelog
    group by keyword1;
  elseif menu2 = '' THEN
    SELECT keyword2 AS name
		FROM ubc_interactivelog
		WHERE keyword1 = menu1
			and keyword2 is not null
      and keyword2 <> ''
		GROUP BY keyword1, keyword2;
  end if;
END $$
DELIMITER ;