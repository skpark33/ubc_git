DELIMITER $$
DROP PROCEDURE IF EXISTS dbo.Statistics_Kpost_period $$
CREATE PROCEDURE dbo.`Statistics_Kpost_period`(in sdate varchar(16), in fdate varchar(16), in period varchar(16), in menu1 varchar(64), in menu2 varchar(64), in colchk varchar(2) )
BEGIN
  drop temporary table if exists keywordtable1;
  drop temporary table if exists keywordtable2;
  drop temporary table if exists keywordtable3;
  
  #keyword1 temporary table
	create TEMPORARY table  keywordtable1 (
    nid int not null AUTO_INCREMENT primary key,
    _keyword1 varchar(64) not null
  );
  insert into keywordtable1(_keyword1) 
  select distinct keyword1 from dbo.ubc_interactivelog order by keyword1;
  #=========================================================================
  #keyword2 temporary table
  create TEMPORARY table  keywordtable2 (
    nid int not null AUTO_INCREMENT primary key,
    _keyword2 varchar(64) not null
  );
  insert into keywordtable2(_keyword2) 
  select distinct keyword2 from dbo.ubc_interactivelog where keyword1 = menu1 and keyword2 is not null and keyword2 <> ''  order by keyword2;
  #=========================================================================
  #keyword3 temporary table
  create TEMPORARY table  keywordtable3 (
    nid int not null AUTO_INCREMENT primary key,
    _keyword3 varchar(64) not null
  );
  insert into keywordtable3(_keyword3) 
  select distinct keyword3 from dbo.ubc_interactivelog where keyword1 = menu1 and keyword2 = menu2 and keyword3 is not null and keyword3 <> ''  order by keyword3;
  #=========================================================================
  
  SET @strSql = '';
  SET @keyword = '';  
  SET @i1 = 1;
  SET @i2 = 1;
  SET @i3 = 1;
  
  select max(nid) into @max1 from keywordtable1;
  select max(nid) into @max2 from keywordtable2;
  select max(nid) into @max3 from keywordtable3;
  
  #year Search 
  IF period = 'year' THEN
  
    IF menu1 = '' or menu1 = '전체' THEN
    
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i1 <= @max1 DO 
      
        select _keyword1 into @keyword from keywordtable1 where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword1, year(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 is not null group by keyword1, year(playDate) ) c group by c.Period order by c.Period;');
      
    ELSEIF menu2 = '' or menu2 = '전체' THEN
    
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i2 <= @max2 DO 
      
        select _keyword2 into @keyword from keywordtable2 where nid = @i2;
        IF colchk = '' THEN
        
          IF (@i2 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i2);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i2, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i2 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i2 := @i2 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword2, year(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 <> '''' and keyword2 is not null group by keyword2, year(playDate) ) c group by c.Period order by c.Period;');
      
    ELSE
    
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i3 <= @max3 DO 
      
        select _keyword3 into @keyword from keywordtable3 where nid = @i3;
        IF colchk = '' THEN
        
          IF (@i3 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i3);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i3, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i3 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i3 := @i3 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword3, year(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 = ''', menu2, ''' and keyword3 <> '''' and keyword3 is not null group by keyword2, year(playDate) ) c group by c.Period order by c.Period;');
      
    END IF;
  
  #month Search 
  ELSEIF period = 'month' THEN
  
    IF menu1 = '' or menu1 = '전체' THEN
    
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i1 <= @max1 DO 
      
        select _keyword1 into @keyword from keywordtable1 where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword1, month(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 is not null group by keyword1, month(playDate) ) c group by c.Period order by c.Period;');
      
    ELSEIF menu2 = '' or menu2 = '전체' THEN
      
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i2 <= @max2 DO 
      
        select _keyword2 into @keyword from keywordtable2 where nid = @i2;
        IF colchk = '' THEN
        
          IF (@i2 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i2);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i2, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i2 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i2 := @i2 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword2, month(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 <> '''' and keyword2 is not null group by keyword2, month(playDate) ) c group by c.Period order by c.Period;');
      
    ELSE
      
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i3 <= @max3 DO 
      
        select _keyword3 into @keyword from keywordtable3 where nid = @i3;
        IF colchk = '' THEN
        
          IF (@i3 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i3);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i3, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i3 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i3 := @i3 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword3, month(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 = ''', menu2, ''' and keyword3 <> '''' and keyword3 is not null group by keyword2, month(playDate) ) c group by c.Period order by c.Period;');
      
    END IF;
    
  
  #quarter Search  
  ELSEIF period = 'quarter' THEN
  
    IF menu1 = '' or menu1 = '전체' THEN
      
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i1 <= @max1 DO 
      
        select _keyword1 into @keyword from keywordtable1 where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword1, quarter(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 is not null group by keyword1, quarter(playDate) ) c group by c.Period order by c.Period;');
      
    ELSEIF menu2 = '' or menu2 = '전체' THEN
      
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i2 <= @max2 DO 
      
        select _keyword2 into @keyword from keywordtable2 where nid = @i2;
        IF colchk = '' THEN
        
          IF (@i2 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i2);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i2, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i2 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i2 := @i2 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword2, quarter(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 <> '''' and keyword2 is not null group by keyword2, quarter(playDate) ) c group by c.Period order by c.Period;');
      
    ELSE
      
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i3 <= @max3 DO 
      
        select _keyword3 into @keyword from keywordtable3 where nid = @i3;
        IF colchk = '' THEN
        
          IF (@i3 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i3);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i3, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i3 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i3 := @i3 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword3, quarter(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 = ''', menu2, ''' and keyword3 <> '''' and keyword3 is not null group by keyword2, quarter(playDate) ) c group by c.Period order by c.Period;');
      
    END IF;
  
  
  #days Search  
  ELSEIF period = 'days' THEN
  
    IF menu1 = '' or menu1 = '전체' THEN
      
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i1 <= @max1 DO 
      
        select _keyword1 into @keyword from keywordtable1 where nid = @i1;
        IF colchk = '' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i1);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i1, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i1 = @max1) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword1 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i1 := @i1 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword1, day(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 is not null group by keyword1, day(playDate) ) c group by c.Period order by c.Period;');
      
    ELSEIF menu2 = '' or menu2 = '전체' THEN
      
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i2 <= @max2 DO 
      
        select _keyword2 into @keyword from keywordtable2 where nid = @i2;
        IF colchk = '' THEN
        
          IF (@i2 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i2);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i2, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i2 = @max2) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword2 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i2 := @i2 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword2, day(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 <> '''' and keyword2 is not null group by keyword2, day(playDate) ) c group by c.Period order by c.Period;');
      
    ELSE
      
      SET @strSql := CONCAT('select c.Period, ');
      WHILE @i3 <= @max3 DO 
      
        select _keyword3 into @keyword from keywordtable2 where nid = @i3;
        IF colchk = '' THEN
        
          IF (@i3 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i3);
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''',@keyword, ''' then c.cnt else 0 end) as col', @i3, ', ');
          END IF;
          
        ELSEIF colchk = 'y' THEN
        
          IF (@i3 = @max3) THEN
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, '''');
          ELSE
            SET @strSql := CONCAT(@strSql, 'max(case when c.keyword3 = ''', @keyword, ''' then c.cnt else 0 end) as ''', @keyword, ''', ');
          END IF;
          
        END IF;
        
        SET @i3 := @i3 + 1;
      end WHILE;
      
      SET @strSql := CONCAT(@strSql, ' from ( select keyword3, day(playDate) as Period, SUM(counter) as cnt from ubc_interactivelog where playDate between ''', sdate, ''' and ''', fdate, ''' and keyword1 = ''', menu1, ''' and keyword2 = ''', menu2, ''' and keyword3 <> '''' and keyword3 is not null group by keyword2, day(playDate) ) c group by c.Period order by c.Period;');
      
    END IF;
   
  END IF;
  
  prepare stmt from @strSql;
  execute stmt;
  deallocate prepare stmt;
  
END $$
DELIMITER ;
