DROP PROCEDURE IF EXISTS dbo.GetChildSiteTest;
CREATE PROCEDURE dbo.`GetChildSiteTest`(inFranchizeType varchar(255), inSiteId varchar(255))
BEGIN

  
  prepare stmt FROM "CREATE TEMPORARY TABLE IF NOT EXISTS dbo.TempChildSite(mgrId varchar(255), siteName varchar(255), siteId varchar(255), parentId varchar(255), lvl 

INT, name_path varchar(255), id_path varchar(255), franchizeType varchar(255))";
  execute stmt;
  deallocate prepare stmt;

  
  TRUNCATE dbo.TempChildSite;
  
  
  INSERT INTO dbo.TempChildSite
  SELECT mgrId, siteName, siteId, parentId, 1, CONCAT('/', siteName), CONCAT('/', siteId), inFranchizeType 
  FROM dbo.ubc_site 
  WHERE franchizeType = inFranchizeType 
  AND siteId = inSiteId;

  
  CALL dbo.GetChildSiteRecursiveTest(2, inFranchizeType, inSiteId, CONCAT('/', dbo.GetSiteName(inSiteId)), CONCAT('/', inSiteId));

END;
