DELIMITER $$
DROP PROCEDURE IF EXISTS dbo.Statistics_Kpost_host $$
CREATE PROCEDURE dbo.`Statistics_Kpost_host`(in sdate varchar(16), in fdate varchar(16), in flag varchar(4), in insiteId varchar(128))
BEGIN
	IF flag = 'top' THEN
    select @rank:=@rank+1 rank, c.*
    from (select @rank :=0) rank ,
         (select H.hostName as code, sum(counter) as Point
          from ubc_interactivelog L, ubc_host H
          where L.playDate between sdate and fdate
                and L.hostId = H.hostId 
             
          group by L.hostId
          order by Point desc
        ) c
    limit 10;
  ELSE
    IF insiteId = '' OR insiteId = '전체' THEN
      select @rank:=@rank+1 rank, c.*
        from (select @rank :=0) rank ,
             (select H.hostName as code, sum(counter) as Point
              from ubc_interactivelog L, ubc_host H
              where L.playDate between sdate and fdate
                    and L.hostId = H.hostId 
             
              group by L.hostId
              order by Point desc
              ) c;
    ELSE
      select @rank:=@rank+1 rank, c.*
        from (select @rank :=0) rank ,
             (select H.hostName as code, sum(counter) as Point
              from ubc_interactivelog L, ubc_host H, (select siteId from ubc_site where parentId = insiteId) S
              where L.playDate between sdate and fdate
                    and S.siteId = H.siteId and 
                    L.hostId = H.hostId 
             
              group by L.hostId
              order by Point desc
              ) c;
    END IF;
  END IF;
END $$
DELIMITER ;
