<?php
	include "../UBC_Common/LocalSettings.php";

	$enc = $_REQUEST["enc"];
	if( $enc <> "1" )
	{
		$siteId = $_REQUEST["siteId"];
		$hostId = $_REQUEST["hostId"];
	}
	else
	{
		$uid    = base64_decode($_REQUEST["uid"]);
		$upd    = base64_decode($_REQUEST["upd"]);
		$siteId = base64_decode($_REQUEST["siteId"]);
		$hostId = base64_decode($_REQUEST["hostId"]);

		if( $uid <> $check_id || $upd <> $check_pwd )
		{
			exit;
		}
	}

	$query = "select * from ubc_host where adminState=1 and m.mgrId <> ''";
	if( siteId <> '' )
	{
		$query = $query . " and vendor = 'PM=*/Site=" . $siteId . "/Host=" + $hostId . "'";
	}

	//echo "dbip : " . $db_ip . "<br>";
	//echo "user : " . $db_user . "<br>";
	//echo "pswd : " . $db_pwd . "<br>";
	//echo "inst : " . $db_instance . "<br>";
	//echo "query : " . $query . "<br>";

	$conn = mysqli_connect($db_ip, $db_user, $db_pwd, $db_instance);
	if(mysqli_connect_errno())
	{	
		echo "DB ERROR !!! " . mysqli_connect_error();
		exit;
	}

	$result	= mysqli_query($conn, $query);
	if( $row = mysqli_fetch_array($result) )
	{
		echo "OK\n";

		do {
			echo "mgrId=" .        $row['mgrId'] .        "\n";
			echo "siteId=" .       $row['siteId'] .       "\n";
			echo "hostId=" .       $row['hostId'] .       "\n";
			echo "macAddress=" .   $row['macAddress'] .   "\n";
			echo "powerControl=" . $row['powerControl'] . "\n";
			echo "wolPort=" .      $row['wolPort'] .      "\n";
			echo "startupTime=" .  $row['startupTime'] .  "\n";
			echo "shutdownTime=" . $row['shutdownTime'] . "\n";
			echo "holiday=" .      $row['holiday'] .      "\n";
			echo "vendor=" .       $row['vendor'] .       "\n";
		} while( $row = mysqli_fetch_array($result) );
	}
	else
	{
		echo "Fail\n";
	}
	mysqli_close($conn);
?>
