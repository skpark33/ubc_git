<?php
	include "../UBC_Common/LocalSettings.php";

	$enc = $_REQUEST["enc"];
	if( $enc <> "1" )
	{
		$mgrId =  $_REQUEST["mgrId"];
		$siteId = $_REQUEST["siteId"];
	}
	else
	{
		$uid    = base64_decode($_REQUEST["uid"]);
		$upd    = base64_decode($_REQUEST["upd"]);
		$mgrId =  base64_decode($_REQUEST["mgrId"]);
		$siteId = base64_decode($_REQUEST["siteId"]);

		if( $uid <> $check_id || $upd <> $check_pwd )
		{
			exit;
		}
	}

	$query = "select * from ubc_site where mgrId <> ''";
	if( $mgrId <> '' )
	{
		$query = $query . " and mgrId = '" . $mgrId . "'";
	}
	if( $siteId <> '' )
	{
		$query = $query . " and siteId = '" . $siteId . "'";
	}

	//echo "dbip : " . $db_ip . "<br>";
	//echo "user : " . $db_user . "<br>";
	//echo "pswd : " . $db_pwd . "<br>";
	//echo "inst : " . $db_instance . "<br>";
	//echo "query : " . $query . "<br>";

	$conn = mysqli_connect($db_ip, $db_user, $db_pwd, $db_instance);
	if(mysqli_connect_errno())
	{	
		echo "DB ERROR !!! " . mysqli_connect_error();
		exit;
	}

	$result	= mysqli_query($conn, $query);
	if( $row = mysqli_fetch_array($result) )
	{
		echo "OK\n";

		do {
			echo "mgrId=" .         $row['mgrId'] .         "\n";
			echo "siteId=" .        $row['siteId'] .        "\n";
			echo "siteName=" .      $row['siteName'] .      "\n";
			echo "phoneNo1=" .      $row['phoneNo1'] .      "\n";
			echo "phoneNo2=" .      $row['phoneNo2'] .      "\n";
			echo "mobileNo=" .      $row['mobileNo'] .      "\n";
			echo "faxNo=" .         $row['faxNo'] .         "\n";
			echo "franchizeType=" . $row['franchizeType'] . "\n";
			echo "chainNo=" .       $row['chainNo'] .       "\n";
			echo "businessType=" .  $row['businessType'] .  "\n";
			echo "businessCode=" .  $row['businessCode'] .  "\n";
			echo "ceoName=" .       $row['ceoName'] .       "\n";
			echo "siteLevel=" .     $row['siteLevel'] .     "\n";
			echo "shopOpenTime=" .  $row['shopOpenTime'] .  "\n";
			echo "shopCloseTime=" . $row['shopCloseTime'] . "\n";
			echo "holiday=" .       $row['holiday'] .       "\n";
			echo "comment1=" .      $row['comment1'] .      "\n";
			echo "comment2=" .      $row['comment2'] .      "\n";
			echo "comment3=" .      $row['comment3'] .      "\n";
			echo "zipCode=" .       $row['zipCode'] .       "\n";
			echo "addr1=" .         $row['addr1'] .         "\n";
			echo "addr2=" .         $row['addr2'] .         "\n";
			echo "addr3=" .         $row['addr3'] .         "\n";
			echo "addr4=" .         $row['addr4'] .         "\n";
			echo "parentId=" .      $row['parentId'] .      "\n";
		} while( $row = mysqli_fetch_array($result) );
	}
	else
	{
		echo "Fail\n";
	}
	mysqli_close($conn);
?>
