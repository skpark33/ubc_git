<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' host id
	Dim strHostId
	strHostId = Request("hostId")


	' sql query
	Dim strSQL
	strSQL = "select * from ubc_host where adminState=1 and mgrId <> ''"

	' make where clause
	if strSiteId <> "" then
		strSQL = strSQL + " and vendor = 'PM=*/Site=" + strSiteId + "/Host=" + strHostId + "'" 
	end if

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	if Not ObjRS.EOF then
		' exist record

		Response.Write "OK" + vbCrLf

		do while Not ObjRS.EOF

			Response.Write "mgrId=" + objRS("mgrId") + vbCrLf
			Response.Write "siteId=" + objRS("siteId") + vbCrLf
			Response.Write "hostId=" + objRS("hostId") + vbCrLf
			Response.Write "macAddress=" + objRS("macAddress") + vbCrLf
			Response.Write "powerControl=" + IntToString(objRS("powerControl")) + vbCrLf
			Response.Write "wolPort=" + IntToString(objRS("wolPort")) + vbCrLf
			Response.Write "startupTime=" + objRS("startupTime") + vbCrLf
			Response.Write "shutdownTime=" + objRS("shutdownTime") + vbCrLf
			Response.Write "holiday=" + objRS("holiday") + vbCrLf
			Response.Write "vendor=" + objRS("vendor") + vbCrLf
			ObjRS.MoveNext
		Loop

	else
		' not exist record
		Response.Write "Fail" + vbCrLf
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
