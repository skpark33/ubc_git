<?php
	include "../UBC_Common/LocalSettings.php";

	$enc = $_REQUEST["enc"];
	if( $enc <> "1" )
	{
		$hostId = $_REQUEST["hostId"];
	}
	else
	{
		$uid    = base64_decode($_REQUEST["uid"]);
		$upd    = base64_decode($_REQUEST["upd"]);
		$hostId = base64_decode($_REQUEST["hostId"]);

		if( $uid <> $check_id || $upd <> $check_pwd )
		{
			exit;
		}
	}

	$query = "select * from ubc_announce where startTime < GETDATE() and GETDATE() < endTime and hostIdList like '%" . hostId . "%' order by createTime desc";
	if( $hostId == '' )
	{
		echo "Nothing\n";
		exit;
	}

	//echo "dbip : " . $db_ip . "<br>";
	//echo "user : " . $db_user . "<br>";
	//echo "pswd : " . $db_pwd . "<br>";
	//echo "inst : " . $db_instance . "<br>";
	//echo "query : " . $query . "<br>";

	$conn = mysqli_connect($db_ip, $db_user, $db_pwd, $db_instance);
	if(mysqli_connect_errno())
	{	
		echo "DB ERROR !!! " . mysqli_connect_error();
		exit;
	}

	$result	= mysqli_query($conn, $query);
	if( $row = mysqli_fetch_array($result) )
	{
		echo "OK\n";

		do {
			echo "mgrId=" .      $row['mgrId'] .      "\n";
			echo "siteId=" .     $row['pmId'] .       "\n";
			echo "announceId=" . $row['announceId'] . "\n";
			echo "serialNo=" .   $row['serialNo'] .   "\n";
			echo "title=" .      $row['title'] .      "\n";
			echo "creator=" .    $row['creator'] .    "\n";
			echo "createTime=" . $row['createTime'] . "\n";
			echo "hostIdList=" . $row['hostIdList'] . "\n";
			echo "startTime=" .  $row['startTime'] .  "\n";
			echo "endTime=" .    $row['endTime'] .    "\n";
			echo "position=" .   $row['position'] .   "\n";
			echo "height=" .     $row['height'] .     "\n";
			echo "comment1=" .   $row['comment1'] .   "\n";
			echo "comment2=" .   $row['comment2'] .   "\n";
			echo "comment3=" .   $row['comment3'] .   "\n";
			echo "comment4=" .   $row['comment4'] .   "\n";
			echo "comment5=" .   $row['comment5'] .   "\n";
			echo "comment6=" .   $row['comment6'] .   "\n";
			echo "comment7=" .   $row['comment7'] .   "\n";
			echo "comment8=" .   $row['comment8'] .   "\n";
			echo "comment9=" .   $row['comment9'] .   "\n";
			echo "comment10=" .  $row['comment10'] .  "\n";
			echo "font=" .       $row['font'] .       "\n";
			echo "fontSize=" .   $row['fontSize'] .   "\n";
			echo "bgColor=" .    $row['bgColor'] .    "\n";
			echo "fgColor=" .    $row['fgColor'] .    "\n";
			echo "bgLocation=" . $row['bgLocation'] . "\n";
			echo "bgFile=" .     $row['bgFile'] .     "\n";
			echo "playSpeed=" .  $row['playSpeed'] .  "\n";
			echo "alpha=" .      $row['alpha'] .      "\n";
		} while( $row = mysqli_fetch_array($result) );
	}
	else
	{
		echo "Nothing\n";
	}
	mysqli_close($conn);
?>
