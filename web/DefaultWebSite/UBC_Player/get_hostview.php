<?php
	include "../UBC_Common/LocalSettings.php";

	$enc = $_REQUEST["enc"];
	if( $enc <> "1" )
	{
		$siteId = $_REQUEST["siteId"];
		$hostId = $_REQUEST["hostId"];
	}
	else
	{
		$uid    = base64_decode($_REQUEST["uid"]);
		$upd    = base64_decode($_REQUEST["upd"]);
		$siteId = base64_decode($_REQUEST["siteId"]);
		$hostId = base64_decode($_REQUEST["hostId"]);

		if( $uid <> $check_id || $upd <> $check_pwd )
		{
			exit;
		}
	}

	$query = "select * from ubc_hostview where mgrId <> ''";
	if( $siteId <> '' )
	{
		$query = $query . " and siteId = '" . $siteId . "'";
	}
	if( $hostId <> '' )
	{
		$query = $query . " and hostId = '" . $hostId . "'";
	}

	//echo "dbip : " . $db_ip . "<br>";
	//echo "user : " . $db_user . "<br>";
	//echo "pswd : " . $db_pwd . "<br>";
	//echo "inst : " . $db_instance . "<br>";
	//echo "query : " . $query . "<br>";

	$conn = mysqli_connect($db_ip, $db_user, $db_pwd, $db_instance);
	if(mysqli_connect_errno())
	{	
		echo "DB ERROR !!! " . mysqli_connect_error();
		exit;
	}

	$result	= mysqli_query($conn, $query);
	if( $row = mysqli_fetch_array($result) )
	{
		echo "OK\n";

		do {
			echo "mgrId=" .                $row['mgrId'] .                "\n";
			echo "siteId=" .               $row['siteId'] .               "\n";
			echo "hostId=" .               $row['hostId'] .               "\n";
			echo "hostName=" .             $row['hostName'] .             "\n";
			echo "domainName=" .           $row['domainName'] .           "\n";
			echo "period=" .               $row['period'] .               "\n";
			echo "description=" .          $row['description'] .          "\n";
			echo "soundVolume=" .          $row['soundVolume'] .          "\n";
			echo "mute=" .                 $row['mute'] .                 "\n";
			echo "startupTime=" .          $row['startupTime'] .          "\n";
			echo "shutdownTime=" .         $row['shutdownTime'] .         "\n";
			echo "edition=" .              $row['edition'] .              "\n";
			echo "version=" .              $row['version'] .              "\n";
			echo "zipCode=" .              $row['zipCode'] .              "\n";
			echo "addr1=" .                $row['addr1'] .                "\n";
			echo "addr2=" .                $row['addr2'] .                "\n";
			echo "addr3=" .                $row['addr3'] .                "\n";
			echo "addr4=" .                $row['addr4'] .                "\n";
			echo "screenshotPeriod=" .     $row['screenshotPeriod'] .     "\n";
			echo "authDate=" .             $row['authDate'] .             "\n";
			echo "autoSchedule1=" .        $row['autoSchedule1'] .        "\n";
			echo "autoSchedule2=" .        $row['autoSchedule2'] .        "\n";
			echo "currentSchedule1=" .     $row['currentSchedule1'] .     "\n";
			echo "currentSchedule2=" .     $row['currentSchedule2'] .     "\n";
			echo "lastSchedule1=" .        $row['lastSchedule1'] .        "\n";
			echo "lastSchedule2=" .        $row['lastSchedule2'] .        "\n";
			echo "lastScheduleTime1=" .    $row['lastScheduleTime1'] .    "\n";
			echo "lastScheduleTime2=" .    $row['lastScheduleTime2'] .    "\n";
			echo "networkUse1=" .          $row['networkUse1'] .          "\n";
			echo "networkUse2=" .          $row['networkUse2'] .          "\n";
			echo "scheduleApplied1=" .     $row['scheduleApplied1'] .     "\n";
			echo "scheduleApplied2=" .     $row['scheduleApplied2'] .     "\n";
			echo "holiday=" .              $row['holiday'] .              "\n";
			echo "soundDisplay=" .         $row['soundDisplay'] .         "\n";
			echo "autoUpdateFlag=" .       $row['autoUpdateFlag'] .       "\n";
			echo "playLogDayLimit=" .      $row['playLogDayLimit'] .      "\n";
			echo "hddThreshold=" .         $row['hddThreshold'] .         "\n";
			echo "playLogDayCriteria=" .   $row['playLogDayCriteria'] .   "\n";
			echo "baseSchedule1=" .        $row['baseSchedule1'] .        "\n";
			echo "baseSchedule2=" .        $row['baseSchedule2'] .        "\n";
			echo "renderMode=" .           $row['renderMode'] .           "\n";
			echo "category=" .             $row['category'] .             "\n";
			echo "contentsDownloadTime=" . $row['contentsDownloadTime'] . "\n";
			echo "download1State=" .       $row['download1State'] .       "\n";
			echo "download2State=" .       $row['download2State'] .       "\n";
			echo "progress1=" .            $row['progress1'] .            "\n";
			echo "progress2=" .            $row['progress2'] .            "\n";
			echo "monitorOff=" .           $row['monitorOff'] .           "\n";
			echo "monitorOffList=" .       $row['monitorOffList'] .       "\n";
			echo "hostType="  .            $row['hostType'] .             "\n";
			echo "mmsCount=" .             $row['mmsCount'] .             "\n";
			echo "winPassword=" .          $row['winPassword'] .          "\n";
			echo "weekShutdownTime=" .     $row['weekShutdownTime'] .     "\n";
			echo "monitorType=" .          $row['monitorType'] .          "\n";
			echo "siteName=" .             $row['siteName'] .             "\n";
			echo "chainNo=" .              $row['chainNo'] .              "\n";
			echo "businessCode=" .         $row['businessCode'] .         "\n";
			echo "businessType=" .         $row['businessType'] .         "\n";
			echo "phoneNo1=" .             $row['phoneNo1'] .             "\n";
			echo "mobileNo=" .             $row['mobileNo'] .             "\n";
		} while( $row = mysqli_fetch_array($result) );
	}
	else
	{
		echo "Fail\n";
	}
	mysqli_close($conn);
?>
