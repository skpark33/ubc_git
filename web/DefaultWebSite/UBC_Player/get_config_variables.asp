<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' customer
	Dim strCustomer
	strCustomer = Request("customer")

	' scope -> Server,Manager,Studio,Terminal or HostID
	Dim strScope
	strScope = Request("scope")

	' sql query
	Dim strSQL
	strSQL = "select * from utv_configvariables where customer = '" + strCustomer + "' and startDate < now() and (endDate is null or endDate > now() )"

	if strScope = "Server" or strScope = "Manager" or strScope = "Studio" then
		strSQL = strSQL + " and scope = '" + strScope + "'"
	else
		strSQL = strSQL + " and ( scope = 'Terminal' or scope = '" + strScope + "' )"
	end if

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then
		' exist record
		Response.Write "Exist" + vbCrLf

		Do While Not ObjRS.EOF

			Response.Write "scope=" + objRS("scope") + vbCrLf
			Response.Write "fileName=" + objRS("fileName") + vbCrLf
			Response.Write "appName=" + objRS("appName") + vbCrLf
			Response.Write "keyName=" + objRS("keyName") + vbCrLf
			Response.Write "value=" + objRS("value") + vbCrLf

			ObjRS.MoveNext
		Loop
	else
		' not exist
		Response.Write "Nothing" + vbCrLf
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
