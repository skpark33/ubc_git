alter table ubc_web_board modify column contentCd text;

# sample data notification board compType='1'
insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(3,'Post Smart Vision 이 시범운용중입니다3','/Contents/board/3.jpg',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', 'KPOST-75001', '1');
insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(4,'Post Smart Vision 이 시범운용중입니다4','/Contents/board/4.jpg',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', '', '1');
insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(5,'Post Smart Vision 이 시범운용중입니다5','/Contents/board/5.jpg',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', 'KPOST-75000,KPOST-75007', '1');
insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(6,'Post Smart Vision 이 시범운용중입니다6','/Contents/board/6.jpg',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', '', '1');
insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(7,'Post Smart Vision 이 시범운용중입니다7','/Contents/board/7.jpg',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', 'KPOST-75007', '1');
insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(8,'Post Smart Vision 이 시범운용중입니다8','/Contents/board/8.jpg',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', '', '1');

# sample data traditional market board compType='2'

insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(9,'속초 전통시장','/Contents/board/141106_12.mp4',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', '', '2');
insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(10,'세종 전통시장','/Contents/board/141218_14.mp4',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', '', '2');


# sample data shopping mall board compType='3'

insert into ubc_web_board 
(boardMasterId,title,contents,UseYn,createDate,updateDate,contentCd,compType) values 
(9,'Naver','http://www.naver.com',1,'2015-01-26 12:00:00','2015-01-26 12:00:00', '', '3');


