<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' hostid
	Dim strHostId
	strHostId = Request("hostId")

if IsNull(strHostId) or strHostId = "" then
	' no host-id
	Response.Write "Nothing" + vbCrLf

else

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_announce where startTime < GETDATE() and GETDATE() < endTime and hostIdList like '%" + strHostId + "%' order by createTime desc"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	if Not ObjRS.EOF then

		' exist record
		Response.Write "OK" + vbCrLf

		Response.Write "mgrId=" + objRS("mgrId") + vbCrLf
		Response.Write "siteId=" + objRS("siteId") + vbCrLf
		Response.Write "announceId=" + objRS("announceId") + vbCrLf
		Response.Write "serialNo=" + IntToString(objRS("serialNo")) + vbCrLf
		Response.Write "title=" + objRS("title") + vbCrLf
		Response.Write "creator=" + objRS("creator") + vbCrLf
		Response.Write "createTime=" + TimeToString(objRS("createTime")) + vbCrLf
		Response.Write "hostIdList=" + objRS("hostIdList") + vbCrLf
		Response.Write "startTime=" + TimeToString(objRS("startTime")) + vbCrLf
		Response.Write "endTime=" + TimeToString(objRS("endTime")) + vbCrLf
		Response.Write "position=" + IntToString(objRS("position")) + vbCrLf
		Response.Write "height=" + IntToString(objRS("height")) + vbCrLf
		Response.Write "comment1=" + objRS("comment1") + vbCrLf
		Response.Write "comment2=" + objRS("comment2") + vbCrLf
		Response.Write "comment3=" + objRS("comment3") + vbCrLf
		Response.Write "comment4=" + objRS("comment4") + vbCrLf
		Response.Write "comment5=" + objRS("comment5") + vbCrLf
		Response.Write "comment6=" + objRS("comment6") + vbCrLf
		Response.Write "comment7=" + objRS("comment7") + vbCrLf
		Response.Write "comment8=" + objRS("comment8") + vbCrLf
		Response.Write "comment9=" + objRS("comment9") + vbCrLf
		Response.Write "comment10=" + objRS("comment10") + vbCrLf
		Response.Write "font=" + objRS("font") + vbCrLf
		Response.Write "fontSize=" + IntToString(objRS("fontSize")) + vbCrLf
		Response.Write "bgColor=" + objRS("bgColor") + vbCrLf
		Response.Write "fgColor=" + objRS("fgColor") + vbCrLf
		Response.Write "bgLocation=" + objRS("bgLocation") + vbCrLf
		Response.Write "bgFile=" + objRS("bgFile") + vbCrLf
		Response.Write "playSpeed=" + IntToString(objRS("playSpeed")) + vbCrLf
		Response.Write "alpha=" + IntToString(objRS("alpha")) + vbCrLf

	else
		' not exist record
		Response.Write "Nothing" + vbCrLf
	end if

	ObjRS.Close
	Set ObjRS = Nothing

end if

	objConn.Close
	Set objConn = Nothing
%>
