<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' host id
	Dim strHostId
	strHostId = Request("hostId")

	' sql query
	Dim strSQL
	strSQL = "select m.mgrId, m.siteId, m.hostId,m.monitorId,h.monitorType, m.monitorName, m.description, m.startUpTime, m.shutdownTime, m.weekShutdownTime, m.holiday" 
	strSQL = strSQL + " from ubc_monitor m, ubc_host h where m.adminState=1 and m.mgrId <> ''"

	' make where clause
	if strSiteId <> "" then
		strSQL = strSQL + " and m.siteId = '" + strSiteId + "'"
	end if
	if strHostId <> "" then
		strSQL = strSQL + " and m. hostId = '" + strHostId + "'" 
		strSQL = strSQL + " and h. hostId = '" + strHostId + "'" 
	end if

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	if Not ObjRS.EOF then
		' exist record

		Response.Write "OK" + vbCrLf

		do while Not ObjRS.EOF

			Response.Write "mgrId=" + objRS("mgrId") + vbCrLf
			Response.Write "siteId=" + objRS("siteId") + vbCrLf
			Response.Write "hostId=" + objRS("hostId") + vbCrLf
			Response.Write "monitorId=" + objRS("monitorId") + vbCrLf
			Response.Write "monitorType=" + IntToString(objRS("monitorType")) + vbCrLf
			Response.Write "monitorName=" + objRS("monitorName") + vbCrLf
			Response.Write "description=" + objRS("description") + vbCrLf
			Response.Write "startupTime=" + objRS("startupTime") + vbCrLf
			Response.Write "shutdownTime=" + objRS("shutdownTime") + vbCrLf
			Response.Write "weekShutdownTime=" + objRS("weekShutdownTime") + vbCrLf
			Response.Write "holiday=" + objRS("holiday") + vbCrLf
			ObjRS.MoveNext
		Loop

	else
		' not exist record
		Response.Write "Fail" + vbCrLf
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
