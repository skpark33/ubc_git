<?php
	include "../UBC_Common/LocalSettings.php";

	$enc = $_REQUEST["enc"];
	if( $enc <> "1" )
	{
	}
	else
	{
		$uid     = base64_decode($_REQUEST["uid"]);
		$upd     = base64_decode($_REQUEST["upd"]);

		if( $uid <> $check_id || $upd <> $check_pwd )
		{
			exit;
		}
	}

	$query = "select * from utv_host order by hostId";

	//echo $query . "\r\n";

	$conn = mysqli_connect($db_ip, $db_user, $db_pwd, $db_instance);
	if(mysqli_connect_errno())
	{	
		echo "DB ERROR !!! " . mysqli_connect_error();
		exit;
	}

   $result = mysqli_query($conn, $query);
    if( $row = mysqli_fetch_array($result) )
    {
        $info = "OK\r\n[Root]\r\nHostIdList=";
        $info_body = "";
        do {
            $info = $info . $row['hostId'] . ",";
            $info_body = $info_body . "[" . $row['hostId'] . "]\r\n";
            $info_body = $info_body . "mgrId=" . $row['mgrId'] . "\r\n";
            $info_body = $info_body . "siteId=" . $row['siteId'] . "\r\n";
            $info_body = $info_body . "hostId=" . $row['hostId'] . "\r\n";
            $info_body = $info_body . "hostName=" . $row['hostName'] . "\r\n";
            $info_body = $info_body . "ipAddress=" . $row['ipAddress'] . "\r\n";
            $info_body = $info_body . "macAddress=" . $row['macAddress'] . "\r\n";
            $info_body = $info_body . "edition=" . $row['edition'] . "\r\n";
            $info_body = $info_body . "authDate=" . $row['authDate'] . "\r\n";
            $info_body = $info_body . "enterpriseKey=" . $row['enterpriseKey'] . "\r\n";
        } while( $row = mysqli_fetch_array($result) );

        $info = $info . "\r\n" . $info_body;

        $info_enc = base64_encode($info);

        header("Content-Length: " . strlen($info_enc));
        echo $info_enc;
    }
    else
    {
        echo "Fail\r\n";
    }

	mysqli_close($conn);
?>
