<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' host id
	Dim strHostId
	strHostId = Request("hostId")

	' server ip
	Dim strServerIp
	strServerIp = Request("serverIp")

	' sql query
	Dim strSQL
	strSQL = "select b.ipAddress from utv_host a, utv_enterprise b where a.hostId = '" + strHostId + "' and a.enterpriseKey = b.eId and a.changeServerIp = 1 and a.serverIpChanged = 0 and b.ipAddress <> '" + strServerIp + "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	if Not ObjRS.EOF then
		' server ip changed

		Dim host_ip
		host_ip = ObjRS("ipAddress")

		Response.Write host_ip
	else
		ObjRS.Close
		Set ObjRS = Nothing

		strSQL = "select a.ipAddress from utv_host a, utv_enterprise b where a.hostId = '" + strHostId + "' and a.enterpriseKey = b.eId and a.serverIpChanged=0 and b.ipAddress = '" + strServerIp + "'"

		Set ObjRS = Server.CreateObject("ADODB.Recordset")
		ObjRS.Open strSQL, objConn

		if Not ObjRS.EOF then
			' already server ip changed -> set to serverIpChanged

			ObjRS.Close
			Set ObjRS = Nothing

			strSQL = "select * from utv_host where hostId = '" + strHostId + "'"

			Set ObjRS = Server.CreateObject("ADODB.Recordset")
			ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

			if Not ObjRS.EOF then
				ObjRS("serverIpChanged") = "1"
				ObjRS.Update

				Response.Write "serverIpChanged"
			else
				Response.Write "internal error"
			end if
		else
			Response.Write "none"
		end if
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
