<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	'
	' 기아,현대 만을 위한 버전
	' mac주소로 검사해서 인증
	' ip주소를 기록한 후 ip주소가 다를 경우 인증실패
	'

	' Mac Address
	Dim strMacAddr
	strMacAddr = Request("macAddr")

	' IP Address
	Dim strIPAddr
	strIPAddr = Request("ipAddr")

	' reset
	Dim strReset
	strReset = Request("reset")

	' sql query
	Dim strSQL
	strSQL = "select * from utv_host where macAddress = '" + strMacAddr + "'"

	if strMacAddr = "" or strIPAddr = "" then
		if strMacAddr = "" then
			' mac-addr is null
			Response.Write "5|"
		else
			' ip-addr is null
			Response.Write "7|"
		end if
	else
		' create db object
		Dim ObjRS
		Set ObjRS = Server.CreateObject("ADODB.Recordset")
		ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

		if Not ObjRS.EOF then
			' exist record

			Dim old_ip
			old_ip = ObjRS("ipAddress")

			Dim edition
			edition = ObjRS("edition")

			if strReset = "1" then
				' reset authorization
				ObjRS("ipAddress") = strIPAddr
				ObjRS.Update
				Response.Write "4|" + ObjRS("hostId") + "|"
			else
				if old_ip = "" or IsNull(old_ip) then
					' write authorization
					ObjRS("ipAddress") = strIPAddr
					ObjRS("authDate") = now()
					ObjRS.Update
					Response.Write "1|" + ObjRS("hostId") + "|"
				elseif old_ip = strIPAddr then
					' already authorize
					Response.Write "2|" + ObjRS("hostId") + "|"
				else
					' already authorize another host
					Response.Write "3|" + ObjRS("hostId") + "|"
				end if
			end if

			Response.Write edition
		else
			' not exist record
			Response.Write "6|"
		end if

		ObjRS.Close
		Set ObjRS = Nothing
	end if

	objConn.Close
	Set objConn = Nothing
%>
