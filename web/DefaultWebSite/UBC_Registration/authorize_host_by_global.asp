<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' host name
	Dim strHostName
	strHostName = Request("hostName")

	' host type
	Dim strHostType
	strHostType = Request("hostType")

	' macaddress
	Dim strMacAddr
	strMacAddr = Request("macAddr")

	' reset
	Dim strReset
	strReset = Request("reset")

	' sql query
	Dim strSQL
	strSQL = "select * from utv_host where hostName = '" + strHostName + "' and hostType = '" + strHostType + "'"
	'Response.Write strSQL

	if strMacAddr = "" then
		' macaddress is null
		Response.Write "5|"
	else
		' create db object
		Dim ObjRS
		Set ObjRS = Server.CreateObject("ADODB.Recordset")
		ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

		if Not ObjRS.EOF then
			' exist record

			Dim host_id
			host_id = ObjRS("hostId")

			Dim old_mac
			old_mac = ObjRS("macAddress")

			Dim edition
			edition = ObjRS("edition")

			if strReset = "1" then
				' reset authorization
				ObjRS("macAddress") = strMacAddr
				ObjRS.Update
				Response.Write "4|"
			else
				if old_mac = "" or IsNull(old_mac) then
					' write authorization
					ObjRS("macAddress") = strMacAddr
					ObjRS("authDate") = now()
					ObjRS.Update
					Response.Write "1|"
				elseif old_mac = strMacAddr then
					' already authorize
					Response.Write "2|"
				else ' mac is not null & mac is not equal
					' already authorize another host
					Response.Write "3|"
				end if
			end if

			Response.Write host_id + "|" + edition
		else
			' not exist record
			Response.Write "6|"
		end if

		ObjRS.Close
		Set ObjRS = Nothing
	end if

	objConn.Close
	Set objConn = Nothing
%>
