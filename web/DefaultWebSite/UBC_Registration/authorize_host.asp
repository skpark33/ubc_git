<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' host id
	Dim strHostId
	strHostId = Request("hostId")

	' Mac Address
	Dim strMacAddr
	strMacAddr = Request("macAddr")

	' reset
	Dim strReset
	strReset = Request("reset")

	' sql query
	Dim strSQL
	'strSQL = "select * from utv_host where siteId = '" + strSiteId + "' and hostId = '" + strHostId + "'"
	strSQL = "select * from utv_host where hostId = '" + strHostId + "'"

	if strMacAddr = "" then
		' mac-addr is null
		Response.Write "5|"
	else
		' create db object
		Dim ObjRS
		Set ObjRS = Server.CreateObject("ADODB.Recordset")
		ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

		if Not ObjRS.EOF then
			' exist record

			Dim old_mac
			old_mac = ObjRS("macAddress")

			Dim edition
			edition = ObjRS("edition")

			if strReset = "0" then
				if old_mac = "" or IsNull(old_mac) then
					' write authorization
					ObjRS("macAddress") = strMacAddr
					ObjRS("authDate") = now()
					ObjRS.Update
					Response.Write "1|"
				elseif old_mac = strMacAddr then
					' already authorize
					Response.Write "2|"
				else
					' already authorize another host
					Response.Write "3|"
				end if

			else
				' reset authorization
				ObjRS("macAddress") = strMacAddr
				ObjRS.Update
				Response.Write "4|"
			end if

			Response.Write edition
		else
			' not exist record
			Response.Write "6|"
		end if

		ObjRS.Close
		Set ObjRS = Nothing
	end if

	objConn.Close
	Set objConn = Nothing
%>
