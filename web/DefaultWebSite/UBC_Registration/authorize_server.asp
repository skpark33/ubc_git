<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' site id
	Dim strEntKey
	strEntKey = Request("entKey")

	' host id
	Dim strPWD
	strPWD = Request("pwd")

	' Mac Address
	Dim strIpAddr
	strIpAddr = Request("ipAddr")

	' sql query
	Dim strSQL
	strSQL = "select * from utv_enterprise where eId = '" + strEntKey + "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if IsNull(strIpAddr) or strIpAddr = "" then
		' ipaddress
		Response.Write "IpIsNull"
	else
		if Not ObjRS.EOF then
			Dim pwd
			pwd = ObjRS("password")

			if not IsNull(pwd) and strPWD = pwd then
				' password match -> set ipAddress
				Response.Write "Authenticate" + vbCrLf
				Response.Write ObjRS("comment1")

				ObjRS("ipAddress") = strIpAddr
				ObjRS.Update
			else
				' password not match
				Response.Write "InvalidPassword"
			end if
		else
			' exist record
			Response.Write "NotExistKey"
		end if
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
