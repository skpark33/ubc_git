<?php
	include "../UBC_Common/LocalSettings.php";

function getNullOrQuote($str)
{
	if( $str == "" ) return "null";
	return "'" . $str . "'";
}

	$proc       = $_REQUEST["proc"];
	$mgrId      = $_REQUEST["mgrId"];
	$siteId     = $_REQUEST["siteId"];
	$hostId     = $_REQUEST["hostId"];
	$hostName   = $_REQUEST["hostName"];
	$ipAddress  = $_REQUEST["ipAddress"];
	$macAddress = $_REQUEST["macAddress"];
	$edition    = $_REQUEST["edition"];
	$authDate   = $_REQUEST["authDate"];
	$entKey     = $_REQUEST["entKey"];

	$enc = $_REQUEST["enc"];
	if( $enc <> "1" )
	{
	}
	else
	{
		$uid     = base64_decode($_REQUEST["uid"]);
		$upd     = base64_decode($_REQUEST["upd"]);
		$proc       = base64_decode($_REQUEST["proc"]);
		$mgrId      = base64_decode($_REQUEST["mgrId"]);
		$siteId     = base64_decode($_REQUEST["siteId"]);
		$hostId     = base64_decode($_REQUEST["hostId"]);
		$hostName   = base64_decode($_REQUEST["hostName"]);
		$ipAddress  = base64_decode($_REQUEST["ipAddress"]);
		$macAddress = base64_decode($_REQUEST["macAddress"]);
		$edition    = base64_decode($_REQUEST["edition"]);
		$authDate   = base64_decode($_REQUEST["authDate"]);
		$entKey     = base64_decode($_REQUEST["entKey"]);

		if( $uid <> $check_id || $upd <> $check_pwd )
		{
			exit;
		}
	}

	if( $hostId == "" )
	{
		echo "NULL";
		exit;
	}

	$mgrId = getNullOrQuote($mgrId);
	$siteId = getNullOrQuote($siteId);
	$hostId = getNullOrQuote($hostId);
	$hostName = getNullOrQuote($hostName);
	$ipAddress = getNullOrQuote($ipAddress);
	$macAddress = getNullOrQuote($macAddress);
	$edition = getNullOrQuote($edition);
	$authDate = getNullOrQuote($authDate);
	$entKey = getNullOrQuote($entKey);

	$query = "";
	if( $proc == "crt" )
	{
		//$query = "insert into `utv_host`(`mgrId`,`siteId`,`hostId`,`hostName`,`ipAddress`,`macAddress`,`edition`,`authDate`,`enterpriseKey`) values (" . $mgrId . "," . $siteId . "," . $hostId . "," . $hostName . "," . $ipAddress . "," . $macAddress . "," . $edition . "," . $authDate . "," . $entKey . ");";
		$query = "insert into `utv_host`(`mgrId`,`siteId`,`hostId`,`hostName`,`ipAddress`,`macAddress`,`edition`,`enterpriseKey`) values (" . $mgrId . "," . $siteId . "," . $hostId . "," . $hostName . "," . $ipAddress . "," . $macAddress . "," . $edition . "," . $entKey . ");";
	}
	else if( $proc == "upd" )
	{
		//$query = "update utv_host set mgrId = " . $mgrId . " , siteId = " . $siteId . " , hostName = " . $hostName . " , ipAddress = " . $ipAddress . " , macAddress = " . $macAddress . " , authDate = " . $authDate . " , edition = " . $edition . " , enterpriseKey = " . $entKey . " where hostId = " . $hostId . ";"; 
		$query = "update utv_host set mgrId = " . $mgrId . " , siteId = " . $siteId . " , hostName = " . $hostName . " , ipAddress = " . $ipAddress . " , macAddress = " . $macAddress . " , edition = " . $edition . " , enterpriseKey = " . $entKey . " where hostId = " . $hostId . ";"; 
	}
	else if( $proc == "rmv" )
	{
		$query = "delete from utv_host where hostId = " . $hostId . ";";
	}
	else
	{
		echo "Invalid";
		exit;
	}

	//echo $query . "\r\n";

	$conn = mysqli_connect($db_ip, $db_user, $db_pwd, $db_instance);
	if(mysqli_connect_errno())
	{	
		echo "DB ERROR !!! " . mysqli_connect_error();
		exit;
	}

	$result = mysqli_query($conn, $query);
	if( $result === false )
	{
		echo "Fail\r\n";
		mysqli_close($conn);
		exit;
	}

	$row_count = mysqli_affected_rows($conn);
	if( $row_count > 0 )
	{
		echo "OK\r\n";
	}
	else
	{
		echo "Nothing\r\n";
	}

	mysqli_close($conn);
?>
