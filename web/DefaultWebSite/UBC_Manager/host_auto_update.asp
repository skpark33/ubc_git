<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' ent key
	Dim strEnterpriseKey
	strEnterpriseKey = Request("enterpriseKey")

	' password
	Dim strPassword
	strPassword = Request("password")

	' site id
	Dim strSiteId
	strSiteId = Request("siteId")

	' host id
	Dim strHostId
	strHostId = Request("hostId")

	' sql query - check password
	Dim strSQL
	strSQL = "select * from utv_enterprise where eId = '" + strEnterpriseKey + "' and password = '" + strPassword+ "'"

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	if Not ObjRS.EOF then
		' password ok

		ObjRS.Close
		Set ObjRS = Nothing

		strSQL = "select * from utv_host where enterpriseKey = '" + strEnterpriseKey + "' and edition = 'ENT'"

		if strHostId <> "*" and strHostId <> "" then
			strHostId = Replace(strHostId, "*", "%")
			strSQL = strSQL + " and hostId like '" + strHostId + "'"
		end if

		Set ObjRS = Server.CreateObject("ADODB.Recordset")
		ObjRS.Open strSQL, objConn

		if Not ObjRS.EOF then
			Response.Write "ok"

			' exist item
			Dim strHostIdList
			strHostIdList = ""

			Dim tm

			Do While Not ObjRS.EOF
				' hostIdList
				if len(strHostIdList) > 0 then
					strHostIdList = strHostIdList + ","
				end if
				strHostIdList = strHostIdList + objRS("hostId")

				' [hostId]
				Response.Write "[" + objRS("hostId") + "]" + vbCrLf

				' mgrId
				if not IsNull(objRS("mgrId")) then
					Response.Write "mgrId=" + objRS("mgrId") + vbCrLf
				else
					Response.Write "mgrId=" + vbCrLf
				end if

				' siteId
				if not IsNull(objRS("siteId")) then
					Response.Write "siteId=" + objRS("siteId") + vbCrLf
				else
					Response.Write "siteId=" + vbCrLf
				end if

				' hostId
				if not IsNull(objRS("hostId")) then
					Response.Write "hostId=" + objRS("hostId") + vbCrLf
				else
					Response.Write "hostId=" + vbCrLf
				end if

				' hostName
				if not IsNull(objRS("hostName")) then
					Response.Write "hostName=" + objRS("hostName") + vbCrLf
				else
					Response.Write "hostName=" + vbCrLf
				end if

				' macAddress
				if not IsNull(objRS("macAddress")) then
					Response.Write "macAddress=" + objRS("macAddress") + vbCrLf
				else
					Response.Write "macAddress=" + vbCrLf
				end if

				' edition
				if not IsNull(objRS("edition")) then
					Response.Write "edition=" + objRS("edition") + vbCrLf
				else
					Response.Write "edition=" + vbCrLf
				end if

				' authDate
				if not IsNull(objRS("authDate")) then
					tm = objRS("authDate")

'					Response.Write "authDate=" + CStr(DatePart("yyyy", tm)) + "-"
'
'					if DatePart("m", tm) < 10 then
'						Response.Write "0"
'					end if
'					Response.Write CStr(DatePart("m", tm)) + "-"
'
'					if DatePart("d", tm) < 10 then
'						Response.Write "0"
'					end if
'					Response.Write CStr(DatePart("d", tm)) + " "
'
'					if DatePart("h", tm) < 10 then
'						Response.Write "0"
'					end if
'					Response.Write CStr(DatePart("h", tm)) + ":"
'
'					if DatePart("n", tm) < 10 then
'						Response.Write "0"
'					end if
'					Response.Write CStr(DatePart("n", tm)) + ":"
'
'					if DatePart("s", tm) < 10 then
'						Response.Write "0"
'					end if
'					Response.Write CStr(DatePart("s", tm)) + vbCrLf
					Response.Write "authDate=" + TimeToString(tm) + vbCrLf
				else
					Response.Write "authDate=1970-01-01 09:00:00" + vbCrLf
				end if

				' enterpriseKey
				if not IsNull(objRS("enterpriseKey")) then
					Response.Write "enterpriseKey=" + objRS("enterpriseKey") + vbCrLf
				else
					Response.Write "enterpriseKey=" + vbCrLf
				end if

				ObjRS.MoveNext
			Loop

			Response.Write "[ROOT]" + vbCrLf + "HostIdList=" + strHostIdList
		else
			' password ok but not exist item
			Response.Write "none"
		end if
	else
		' wrong password
		Response.Write "denied"
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
