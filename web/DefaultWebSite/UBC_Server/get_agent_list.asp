<%@ language="VBScript" %>
<% Option Explicit %>

<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->

<%
	' sql query
	Dim strSQL
	strSQL = "select * from utv_serverprocess where appId='UBCServerAgent'"

	' debug
	'Response.Write strSQL

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then

		' exist record
		Response.Write "OK" + vbCrLf

		do while Not ObjRS.EOF

			Response.Write "customer=" + objRS("customer") + vbCrLf
			Response.Write "serverId=" + objRS("serverId") + vbCrLf
			Response.Write "serverIp=" + objRS("serverIp") + vbCrLf
			Response.Write "lastUpdateTime=" + TimeToString(objRS("lastUpdateTime")) + vbCrLf
			Response.Write "appId=" + objRS("appId") + vbCrLf
			Response.Write "binaryName=" + objRS("binaryName") + vbCrLf
			Response.Write "argument=" + objRS("argument") + vbCrLf
			Response.Write "properties=" + objRS("properties") + vbCrLf
			Response.Write "debug=" + objRS("debug") + vbCrLf
			Response.Write "status=" + objRS("status") + vbCrLf
			Response.Write "startUpTime=" + TimeToString(objRS("startUpTime")) + vbCrLf
			Response.Write "command=" + objRS("command") + vbCrLf
			Response.Write "commandTime=" + TimeToString(objRS("commandTime")) + vbCrLf
			Response.Write "excuteTime=" + TimeToString(objRS("excuteTime")) + vbCrLf

			ObjRS.MoveNext
		Loop
	else
		' not exist record
		Response.Write "Nothing"
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
