<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' PK

	' customer
	Dim strCustomer
	strCustomer = Request("customer")

	' serverId
	Dim strServerId
	strServerId = Request("serverId")

	' appId
	Dim strAppId
	strAppId = Request("appId")


	' status

	' serverIp
	Dim strServerIp
	strServerIp = Request("serverIp")

	' binaryName
	Dim strBinaryName
	strBinaryName = Request("binaryName")

	' argument
	Dim strArgument
	strArgument = Request("argument")

	' properties
	Dim strProperties
	strProperties = Request("properties")

	' debug
	Dim strDebug
	strDebug = Request("debug")

	' status
	Dim strStatus
	strStatus = Request("status")

	' startUpTime
	Dim strStartUpTime
	strStartUpTime = Request("startUpTime")


	' sql query
	Dim strSQL
	strSQL = "select * from utv_serverprocess where customer = '" + strCustomer + "' and serverId = '" + strServerId + "' and appId = '" + strAppId + "'"

	' debug
	'Response.Write strSQL


	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if ObjRS.EOF then
		' not exist record
		ObjRS.Close

		' create new main record
		objRS.Open "utv_serverprocess", objConn, , adLockOptimistic, adCmdTable
		objRS.AddNew

		objRS("customer") = strCustomer
		objRS("serverId") = strServerId
		objRS("appId") = strAppId
	else
		' exist record
	end if

	ObjRs("lastUpdateTime")	= now()
	ObjRs("serverIp")	= strServerIp
	ObjRs("binaryName")	= strBinaryName
	ObjRs("argument")	= strArgument

	if strAppId <> "UBCServerAgent" then
		ObjRs("properties")	= strProperties
	end if

	ObjRs("debug")		= strDebug
	ObjRs("status")		= strStatus

	if not IsNull(strStartUpTime) and strStartUpTime <> "" then
		ObjRs("startUpTime") = strStartUpTime
	end if

	ObjRS.Update

	Response.Write "OK"

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
