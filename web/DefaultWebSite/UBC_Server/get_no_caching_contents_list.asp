<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
if IsMySQL() = 0 then

	' limit date
	Dim strLimitDate
	strLimitDate = "90"

	' sql query
	Dim strSQL
	strSQL = strSQL + "select distinct contentsId from ubc_contents "
	strSQL = strSQL + "where contentsId not in "
	strSQL = strSQL + "	( select contentsId from ubc_contents where programId in "
	strSQL = strSQL + "		( select aTable.programId from ( "

	if IsMySQL() = 0 then
		strSQL = strSQL + "		select programId from ubc_program where lastUpdateTime > getdate()-" + strLimitDate + " "
		strSQL = strSQL + "		union select programId from ubc_applyLog where applyTime > getdate()-" + strLimitDate + " "
	else
		strSQL = strSQL + "		select programId from ubc_program where lastUpdateTime > Date_Sub(now(), Interval " + strLimitDate + " day) "
		strSQL = strSQL + "		union select programId from ubc_applyLog where applyTime > Date_Sub(now(), Interval " + strLimitDate + " day) "
	end if

	strSQL = strSQL + "				union select programId from ubc_tp "
	strSQL = strSQL + "				union select lastSchedule1 from ubc_host where lastSchedule1 is not null "
	strSQL = strSQL + "				union select lastSchedule2 from ubc_host where lastSchedule2 is not null "
	strSQL = strSQL + "				union select currentSchedule1 from ubc_host where currentSchedule1 is not null "
	strSQL = strSQL + "				union select currentSchedule2 from ubc_host where currentSchedule2 is not null "
	strSQL = strSQL + "			) aTable "
	strSQL = strSQL + "			group by aTable.programId "
	strSQL = strSQL + "		) "
	strSQL = strSQL + "		and contentsType <> '19' "
	strSQL = strSQL + "	) "
	strSQL = strSQL + "and contentsType <> '19' "
	strSQL = strSQL + "order by contentsId "
	if IsMySQL() = 0 then
		strSQL = strSQL + "OPTION (HASH JOIN) "
	end if

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	if Not ObjRS.EOF then
		' exist
		Response.Write "Exist" + vbCrLf

		Do While Not ObjRS.EOF
			' exist record

			Response.Write "contentsId=" + objRS("contentsId") + vbCrLf

			ObjRS.MoveNext
		Loop
	else
		' nothing
		Response.Write "Nothing" + vbCrLf
	end if

	ObjRS.Close
	Set ObjRS = Nothing

else

	Response.Write "Nothing" + vbCrLf

end if

	objConn.Close
	Set objConn = Nothing
%>
