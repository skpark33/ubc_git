<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' PK

	' customer
	Dim strCustomer
	strCustomer = Request("customer")

	' serverId
	Dim strServerId
	strServerId = Request("serverId")

	' appId
	Dim strAppId
	strAppId = Request("appId")

	' onlyGet
	Dim nOnlyGet
	nOnlyGet = CInt(Request("onlyGet"))

	' sql query
	Dim strSQL
	strSQL = "select * from utv_serverprocess where (command = 'Starting' or command = 'Stopping') and (commandTime > excuteTime or excuteTime is null) and customer = '" + strCustomer + "' and serverId = '" + strServerId + "'"
	if strAppId <> "" then
		strSQL = strSQL + " and appId = '" + strAppId + "'"
	end if

	' debug
	'Response.Write strSQL


	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn, adOpenStatic, adLockOptimistic

	if Not ObjRS.EOF then

		' exist record
		Response.Write "OK" + vbCrLf

		do while Not ObjRS.EOF

			Dim command
			command = objRS("command")

			' send info
			Response.Write "appId=" + objRS("appId") + vbCrLf
			Response.Write "command=" + command + vbCrLf

			' update info
			objRS("excuteTime") = now()

			if command = "Starting" and nOnlyGet <> 1 then
				objRS("command") = "Started"
			elseif command = "Stopping" and nOnlyGet <> 1 then
				objRS("command") = "Stopped"
			end if

			ObjRS.Update
			ObjRS.MoveNext
		Loop
	else
		' not exist record
		Response.Write "Fail"
	end if

	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
