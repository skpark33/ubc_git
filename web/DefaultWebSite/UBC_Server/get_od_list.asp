<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' id
	Dim strId
	strId = Request("id")

	' pwd
	Dim strPWD
	strPWD = Request("pwd")

	' sql query
	Dim strSQL
	strSQL = "select * from ubc_pmo"

	if strId <> "ubc" or strPWD <> "sqicop" then
		' auth fail
		Response.Write "Fail"
	else
		' auth pass
		Response.Write "OK" + vbCrLf

		' create db object
		Dim ObjRS
		Set ObjRS = Server.CreateObject("ADODB.Recordset")
		ObjRS.Open strSQL, objConn

		Do While Not ObjRS.EOF
			' exist record

			Response.Write "pmId=" + objRS("pmId") + vbCrLf
			Response.Write "ipAddress=" + objRS("ipAddress") + vbCrLf
			Response.Write "ftpId=" + objRS("ftpId") + vbCrLf
			Response.Write "ftpPasswd=" + objRS("ftpPasswd") + vbCrLf
			Response.Write "ftpPort=" + CStr(objRS("ftpPort")) + vbCrLf
			Response.Write "currentConnectionCount=" + CStr(objRS("currentConnectionCount")) + vbCrLf

			ObjRS.MoveNext
		Loop

		ObjRS.Close
		Set ObjRS = Nothing
	end if

	objConn.Close
	Set objConn = Nothing
%>
