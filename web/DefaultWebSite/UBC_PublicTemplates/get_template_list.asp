<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	' sql query
	Dim strSQL
	strSQL = "select * from ubc_ctemplate where mgrId is not null"

	' description
	Dim strDescription
	strDescription = Request("description")
	if strDescription <> "" then
		strSQL = strSQL + " and description like '%%" + strDescription + "%%'"
	end if

	' startTime
	Dim strStartTime
	strStartTime = Request("startTime")
	if strStartTime <> "" then
		strSQL = strSQL + " and registerTime > CAST('" + strStartTime + " 00:00:00' as datetime)"
	end if

	' endTime
	Dim strEndTime
	strEndTime = Request("endTime")
	if strEndTime <> "" then
		strSQL = strSQL + " and registerTime < CAST('" + strEndTime + " 00:00:00' as datetime)"
	end if

	' registerId
	Dim strRegisterId
	strRegisterId = Request("registerId")
	if strRegisterId <> "" then
		strSQL = strSQL + " and registerId like '%" + strRegisterId + "%'"
	end if

	' customer
	Dim strCustomer
	strCustomer = Request("customer")
	if strCustomer <> "" then
		strSQL = strSQL + " and customer like '%" + strCustomer + "%'"
	end if

	' sort
	strSQL = strSQL + " order by mgrId,templateId"

	'debuging query-out
	'Response.Write strSQL + vbCrLf

	' create db object
	Dim ObjRS
	Set ObjRS = Server.CreateObject("ADODB.Recordset")
	ObjRS.Open strSQL, objConn

	Do While Not ObjRS.EOF
		Dim tm
		Dim tm2

		Response.Write "templateId=" + objRS("templateId") + vbCrLf
		Response.Write "width=" + CStr(objRS("width")) + vbCrLf
		Response.Write "height=" + CStr(objRS("height")) + vbCrLf
		Response.Write "bgColor=" + objRS("bgColor") + vbCrLf
		Response.Write "description=" + objRS("description") + vbCrLf
		if not IsNull(objRS("registerTime")) then
			tm = objRS("registerTime")
			'아래줄 삭제 mssql에서 timestamp 사용법을 모르겠음
			'Response.Write "registerTime=" + CStr(DatePart("yyyy", tm)) + "/" + CStr(DatePart("m", tm)) + "/" + CStr(DatePart("d", tm)) + " " + CStr(DatePart("h", tm)) + ":" + + CStr(DatePart("n", tm)) + ":" + + CStr(DatePart("s", tm)) + vbCrLf
		end if
		Response.Write "registerId=" + objRS("registerId") + vbCrLf
		Response.Write "customer=" + objRS("customer") + vbCrLf

		ObjRS.MoveNext
	Loop


	ObjRS.Close
	Set ObjRS = Nothing

	objConn.Close
	Set objConn = Nothing
%>
