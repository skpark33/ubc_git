<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	Function CreateTemplateID()
		Dim bCreate
		Dim strNewTemplateID
		Dim strSQL

		Dim objRS_template
		Set objRS_template = Server.CreateObject("ADODB.Recordset")

		bCreate = 0
		do while bCreate = 0
			strNewTemplateID = CreateWindowsGUID()

			strSQL = "select * from ubc_ctemplate where templateId = '" + strNewTemplateID + "'"

			objRS_template.Open strSQL, objConn

			if objRS_template.EOF then
				bCreate = 1
			end if

			objRS_template.Close
		loop

		CreateTemplateID = strNewTemplateID

		Set objRS_template = nothing
	End Function



	' templateId
	Dim strTemplateId
	strTemplateId = CreateTemplateID()

	' width
	Dim strWidth
	strWidth = Request("width")

	' height
	Dim strHeight
	strHeight = Request("height")

	' bgColor
	Dim strBgColor
	strBgColor = Request("bgColor")

	' description
	Dim strDescription
	strDescription = Request("description")

	' registerId
	Dim strRegisterId
	strRegisterId = Request("registerId")

	' customer
	Dim strCustomer
	strCustomer = Request("customer")


	' create db object
	Dim objRS_create
	Set objRS_create = Server.CreateObject("ADODB.Recordset")
	objRS_create.Open "ubc_ctemplate", objConn, , adLockOptimistic, adCmdTable

	' create new record
	objRS_create.AddNew

	objRS_create("mgrId") = "1"
	objRS_create("templateId") = strTemplateId
	objRS_create("width") = CInt(strWidth)
	objRS_create("height") = CInt(strHeight)
	objRS_create("bgColor") = strBgColor
	objRS_create("description") = strDescription
	objRS_create("registerId") = strRegisterId
	objRS_create("customer") = strCustomer

	objRS_create.Update

	objRS_create.Close
	Set objRS_create = Nothing

	objConn.Close
	Set objConn = Nothing

	Response.Write strTemplateId
%>
