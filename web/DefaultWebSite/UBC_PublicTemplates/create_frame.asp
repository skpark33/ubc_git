<%@ language="VBScript" %>
<% Option Explicit %>
<!-- #include virtual="/UBC_Common/adovbs.inc" -->
<!-- #include virtual="/UBC_Common/Functions.asp" -->
<!-- #include virtual="/UBC_Common/LocalSettings.asp" -->
<%
	Function CreateFrameID()
		Dim bCreate
		Dim strNewFrameID
		Dim strSQL

		Dim objRS_frame
		Set objRS_frame = Server.CreateObject("ADODB.Recordset")

		bCreate = 0
		do while bCreate = 0
			strNewFrameID = CreateWindowsGUID()

			strSQL = "select * from ubc_cframe where frameId = '" + strNewFrameID + "'"

			objRS_frame.Open strSQL, objConn

			if objRS_frame.EOF then
				bCreate = 1
			end if

			objRS_frame.Close
		loop

		CreateFrameID = strNewFrameID

		Set objRS_frame = nothing
	End Function


	' templateId
	Dim strTemplateId
	strTemplateId = Request("templateId")

	' frameId
	Dim strFrameId
	strFrameId = CreateFrameID()

	' grade
	Dim strGrade
	strGrade = Request("grade")

	' width
	Dim strWidth
	strWidth = Request("width")

	' height
	Dim strHeight
	strHeight = Request("height")

	' x
	Dim strX
	strX = Request("x")

	' y
	Dim strY
	strY = Request("y")

	' isPIP
	Dim strIsPIP
	strIsPIP = Request("isPIP")

	' borderStyle
	Dim strBorderStyle
	strBorderStyle = Request("borderStyle")

	' borderThickness
	Dim strBorderThickness
	strBorderThickness = Request("borderThickness")

	' borderColor
	Dim strBorderColor
	strBorderColor = Request("borderColor")

	' cornerRadius
	Dim strCornerRadius
	strCornerRadius = Request("cornerRadius")

	' description
	Dim strDescription
	strDescription = Request("description")

	' comment1
	Dim strComment1
	strComment1 = Request("comment1")

	' comment2
	Dim strComment2
	strComment2 = Request("comment2")

	' comment3
	Dim strComment3
	strComment3 = Request("comment3")

	' alpha
	Dim strAlpha
	strAlpha = Request("alpha")

	' isTV
	Dim strIsTV
	strIsTV = Request("isTV")

	' customer
	Dim strCustomer
	strCustomer = Request("customer")


	' create db object
	Dim objRS_create
	Set objRS_create = Server.CreateObject("ADODB.Recordset")
	objRS_create.Open "ubc_cframe", objConn, , adLockOptimistic, adCmdTable

	' create new record
	objRS_create.AddNew

	objRS_create("mgrId") = "1"
	objRS_create("templateId") = strTemplateId
	objRS_create("frameId") = strFrameId
	objRS_create("grade") = CInt(strGrade)
	objRS_create("width") = CInt(strWidth)
	objRS_create("height") = CInt(strHeight)
	objRS_create("x") = CInt(strX)
	objRS_create("y") = CInt(strY)
	objRS_create("isPIP") = CInt(strIsPIP)
	objRS_create("borderStyle") = strBorderStyle
	objRS_create("borderThickness") = CInt(strBorderThickness)
	objRS_create("borderColor") = strBorderColor
	objRS_create("cornerRadius") = strCornerRadius
	objRS_create("description") = strDescription
	objRS_create("comment1") = strComment1
	objRS_create("comment2") = strComment2
	objRS_create("comment3") = strComment3
	objRS_create("alpha") = CInt(strAlpha)
	objRS_create("isTV") = CInt(strIsTV)
	objRS_create("customer") = strCustomer

	objRS_create.Update

	objRS_create.Close
	Set objRS_create = Nothing

	objConn.Close
	Set objConn = Nothing

	Response.Write strFrameId
%>
