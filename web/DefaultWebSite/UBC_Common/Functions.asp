<%
	Function CreateGUID(tmpLength)
		Randomize Timer
		Dim tmpCounter,tmpGUID
		Const strValid = "0123456789ABCDEF"
		For tmpCounter = 1 To tmpLength
			tmpGUID = tmpGUID & Mid(strValid, Int(Rnd(1) * Len(strValid)) + 1, 1)
		Next
		CreateGUID = tmpGUID
	End Function

	Function CreateWindowsGUID()
		CreateWindowsGUID = "{" & CreateGUID(8) & "-" & CreateGUID(4) & "-" & CreateGUID(4) & "-" & CreateGUID(4) & "-" & CreateGUID(12) & "}"
	End Function

	Function TimeToString(tm)
		if not IsNull(tm) then
			Dim num_datetime(4)
			Dim str_datetime(4)

			num_datetime(0) = DatePart("m", tm)
			num_datetime(1) = DatePart("d", tm)

			num_datetime(2) = DatePart("h", tm)
			num_datetime(3) = DatePart("n", tm)
			num_datetime(4) = DatePart("s", tm)

			Dim i
			For i = 0 To 4
				if num_datetime(i) < 10 then
					str_datetime(i) = "0" + CStr(num_datetime(i))
				else
					str_datetime(i) = CStr(num_datetime(i))
				end if
			Next

			TimeToString = CStr(DatePart("yyyy", tm)) + "/" + str_datetime(0) + "/" + str_datetime(1) + " " + str_datetime(2) + ":" + str_datetime(3) + ":" + str_datetime(4)
		else
			TimeToString = ""
		end if
	End Function

	Function IntToString(nValue)
		if not IsNull(nValue) then
			IntToString = CStr(nValue)
		else
			IntToString = ""
		end if
	End Function
%>
