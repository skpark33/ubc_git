package com.sqisoft.utv.server;

import org.red5.server.api.IScope;
import org.red5.server.api.stream.IStreamFilenameGenerator;
import org.red5.server.api.stream.IStreamFilenameGenerator.GenerationType;

public class CustomFilenameGenerator implements IStreamFilenameGenerator {

	public String recordPath = "recordedStreams/";
    public String playbackPath = "videoStreams/";
    
    public String generateFilename(IScope scope, String name, GenerationType type) {
        return generateFilename(scope, name, null, type);
    }

    public String generateFilename(IScope scope, String name, String extension, GenerationType type) {
        
    	String filename = null;
        
        if (type == GenerationType.RECORD) {
            filename = recordPath + name;
        } else {
            filename = playbackPath + name;
        }
        
        if (extension != null) {
            filename += extension;
        }
        
        return filename;
    }

	public boolean resolvesToAbsolutePath() {
		return true;
	}
	
	public void setRecordPath(String path) {
		recordPath = path;
	}

	public void setPlaybackPath(String path) {
	    playbackPath = path;
	}	

}