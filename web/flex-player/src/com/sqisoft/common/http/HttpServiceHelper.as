package com.sqisoft.common.http {
	
import mx.rpc.AsyncToken;
import mx.rpc.http.HTTPService;	
import mx.rpc.events.ResultEvent;
import mx.rpc.events.FaultEvent;
import mx.utils.StringUtil;
import mx.rpc.events.AbstractEvent;
				
	public class HttpServiceHelper 	{
		
		public static function sendRequest(props : HttpServiceProps ) : AsyncToken {
			var feedRequest : HTTPService = new HTTPService();
			
			feedRequest.useProxy = props.useProxy;
			feedRequest.method = props.method;		
			
			feedRequest.addEventListener(ResultEvent.RESULT, props.resultEvent);
			feedRequest.addEventListener(FaultEvent.FAULT, props.faultEvent);
			
			var url : String = StringUtil.substitute("{0}/{1}", props.url, props.controller);
			feedRequest.url = url;
			
			var token : AsyncToken =  feedRequest.send(props.param);
			
			token.action = props.tokenAction;
			
			feedRequest = null;
			
			return token;
		}			
		
	}
}