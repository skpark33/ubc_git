package com.sqisoft.common.http {
	
	import mx.rpc.http.HTTPService;	

	public class HttpServiceProps {
		
		private var _url : String;
		private var _useProxy : Boolean = false;
		private var _method : String = "POST";
		private var _resultFormat : String = HTTPService.RESULT_FORMAT_TEXT;
		private var _controller : String;
		private var _action : String;
		private var _param : Object;
		private var _resultEvent : Function;
		private var _faultEvent : Function;
		private var _tokenAction : *;
		
		public function HttpServiceProps () {
			
		}
		
		public function reset() : void {
			_param = null;
			_controller = "";
			_action = "";
			_tokenAction = null;
		}
		
		public function setParameter(key : *, value : * ) : void {
			if( ! _param ) {
				_param = new Object();
			}
			_param[key] = value;
		}
		
		public function get url() : String {
			return _url;			
		}

		public function set url(setValue : String) : void {
			_url = setValue;
		}
		
		public function get useProxy() : Boolean {
			return _useProxy;			
		}

		public function set useProxy(setValue : Boolean) : void {
			_useProxy = setValue;
		}		
		
		public function get method() : String {
			return _method;			
		}

		public function set method(setValue : String) : void {
			_method = setValue;
		}
		
		public function get resultFormat() : String {
			return _resultFormat;			
		}

		public function set resultFormat(setValue : String) : void {
			_resultFormat = setValue;
		}		
		
		public function get controller() : String {
			return _controller;			
		}

		public function set controller(setValue : String) : void {
			_controller = setValue;
		}
		
		public function get action() : String {
			return _action;			
		}

		public function set action(setValue : String) : void {
			_action = setValue;
		}
		
		public function get param() : Object {
			return _param;			
		}
/*
		public function set param(setValue : Object) : void {
			_param = setValue;
		}
*/		
		public function get resultEvent() : Function {
			return _resultEvent;			
		}

		public function set resultEvent(setValue : Function) : void {
			_resultEvent = setValue;
		}
		
		public function get faultEvent() : Function {
			return _faultEvent;			
		}

		public function set faultEvent(setValue : Function) : void {
			_faultEvent = setValue;
		}												
		
		public function get tokenAction() : * {
			return _tokenAction;			
		}

		public function set tokenAction(setValue : *) : void {
			_tokenAction = setValue;
		}			
	}
}