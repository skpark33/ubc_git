package com.sqisoft.common.util {
	import com.adobe.serialization.json.JSON; 
	import flash.geom.Point;
	import flash.display.DisplayObject;
		
	public class SimpleUtil {
		
		public static function getStringToDate(dateStr : String ) : Date {

			var date : Date = new Date();
			
			date.setFullYear(new Number(dateStr.substr(0, 4) ), new Number(dateStr.substr(5, 2) ), new Number(dateStr.substr(8, 2) ) );
			date.setHours(new Number(dateStr.substr(11, 2) ), new Number(dateStr.substr(114, 2) ), new Number(dateStr.substr(17, 2) ) );
			
			return date;
			
		}
		
		public static function encodeJSON(obj : Object ) : String {
			var str : String = JSON.encode(obj);
			
			return str;			
		}
		
		
		public static function decodeJSON(str : String ) : Object {
			
			var obj : Object = JSON.decode(str);
			
			return obj;			
		}		
		
		public static function nullToEmptyStr(obj : String) : String {
			if(obj == null) return "";
			else return obj;
		}
		
		public static function changeLtAndGt(src : String) : String {
			var ptn1 : RegExp = /\&lt/g;
			var ptn2 : RegExp = /\&gt/g;
			
			src = src.replace(ptn1, '<');
			src = src.replace(ptn2, '>');	
			
			return src;
		}

		public static function localToGlobal( obj : DisplayObject ) : Point {
			var pos : Point = new Point(obj.x, obj.y);
			return obj.localToGlobal(pos);			
		}

	}
}