package com.sqisoft.common.util {

	import com.sqisoft.common.component.IFrame;
	import com.sqisoft.common.component.ImageViewer;
	import com.sqisoft.common.component.PromotionViewer;
	import com.sqisoft.common.component.SimpleMarquee;
	import com.sqisoft.common.component.SmsViewer;
	import com.sqisoft.common.component.UTVFlvPlayer;
	import com.sqisoft.utv.vo.Contents;
	import com.sqisoft.utv.vo.Frame;
	
	import mx.core.UIComponent;	
	
	public final class DisplayObjectHelper {
		
		public static function createFlvPlayer(frm : Frame, pls : UTVPlayer) : void {
			
			var obj : Contents = frm.getContents();
			
			// 처음에는 local 에서 컨텐츠 정보를 가져오도록 설정한다.
			obj.srcUrl = pls.getResource('FLV_LOCAL_URL');
			obj.protocol = pls.getResource('FLV_LOCAL_PROTOCOL');
			
			obj.location = ""; //
			
			var flvPlayer : UTVFlvPlayer = new UTVFlvPlayer();
			flvPlayer.x = frm.x;
			flvPlayer.y = frm.y;
			flvPlayer.width = frm.width;
			flvPlayer.height = frm.height;			
			flvPlayer.setMetaData(obj, pls);

			setFrameStyle(frm, flvPlayer);
			setContentsStyle(obj, flvPlayer);
									
			frm.setDispObj(flvPlayer);
			frm.setDispObjLayout();


		}
		
		public static function createImgViewer(frm : Frame, pls : UTVPlayer) : void {
			
			var obj : Contents = frm.getContents();
			//obj.srcUrl = "http://" + pls.getResource('IMG_SERVER_URL') + obj.location + obj.fileName;
			
			var img : ImageViewer = new ImageViewer();
			img.addComponent(frm);			
			
			setFrameStyle(frm, img);
			setContentsStyle(obj, img);

			frm.setDispObj(img);
			frm.setDispObjLayout();

			if(obj.runningTime > 0 ) {
				frm.startTimer();
			}				
		}
		
		public static function createSmsViewer(frm : Frame, pls : UTVPlayer) : void {
			var obj : Contents = frm.getContents();
			
			var sms : SmsViewer = new SmsViewer();
			var tText : String = obj.comment1 + obj.comment2 + obj.comment3;
			
			sms.txt = SimpleUtil.changeLtAndGt(tText);
			sms.addComponent(frm);

			setFrameStyle(frm, sms);
			setContentsStyle(obj, sms);
			
			frm.setDispObj(sms);
			frm.setDispObjLayout();
			
			if(obj.runningTime > 0 ) {
				frm.startTimer();				
			}
		}
		
		public static function createTxtTicker(frm : Frame, pls : UTVPlayer) : void {
			
			var obj : Contents = frm.getContents();
			
			var ticker : SimpleMarquee = new SimpleMarquee();
			
			var tText : String = obj.comment1 + obj.comment2 + obj.comment3;

			ticker.tickerTxt = SimpleUtil.changeLtAndGt(tText);
			ticker.moveDuration = obj.playSpeed;
			ticker.direction = "left";
			ticker.playTime = obj.runningTime;	

			ticker.backImg = obj.srcUrl;
			
			setFrameStyle(frm, ticker);
			setContentsStyle(obj, ticker);
			
			frm.setDispObj(ticker);
			frm.setDispObjLayout();		
			
			if(obj.runningTime > 0 ) {
				frm.startTimer();				
			}
		}
		
		public static function createIFrame(frm : Frame, pls : UTVPlayer) : void {
			var obj : Contents = frm.getContents();
			
			var iframe : IFrame = new IFrame();
			iframe.source = "http://" + obj.fileName;
			
			if(frm.borderStyle != "none") {
				iframe.borderThickness = frm.borderThickness;
			}

			setFrameStyle(frm, iframe);
			setContentsStyle(obj, iframe);
			
			frm.setDispObj(iframe);
			frm.setDispObjLayout();

			if(obj.runningTime > 0 ) {
				frm.startTimer();				
			}
			
			iframe.visible = true;
		}
		
		public static function createPromotionViewer(frm : Frame, pls : UTVPlayer) : void {
			var obj : Contents = frm.getContents();
			var promo : PromotionViewer = new PromotionViewer();
			promo.contents = obj;
			promo.backImg = obj.srcUrl;

			setFrameStyle(frm, promo);
			setContentsStyle(obj, promo);
			
			frm.setDispObj(promo);
			frm.setDispObjLayout();		
			
			if(obj.runningTime > 0 ) {
				frm.startTimer();				
			}			
						
		}
		
		public static function setFrameStyle(frm: Frame, disp : UIComponent) : void {
			if(frm.borderStyle.length > 0) disp.setStyle("borderStyle", frm.borderStyle); // none,inset,outset,solid 
			if(frm.borderColor.length > 0) disp.setStyle("borderColor", frm.borderColor);		
			disp.setStyle("borderThickness", frm.borderThickness);
			disp.setStyle("cornerRadius", frm.cornerRadius);				
		}
		
		public static function setContentsLayout(frm : Frame, disp : UIComponent) : void {
			disp.x = 0;
			disp.y = 0;
			disp.width = frm.width;
			disp.height = frm.height;
		}
				
		public static function setContentsStyle(obj : Contents, disp : UIComponent) : void {
			
			if(obj.bgColor.length > 0) disp.setStyle("backgroundColor", obj.bgColor);
			if(obj.fgColor.length > 0) disp.setStyle("color", obj.fgColor);
			if(obj.font.length > 0) disp.setStyle("fontFamily", obj.font);
			if(obj.fontSize > 0) disp.setStyle("fontSize", obj.fontSize);

		}			
		
		public static function calPlayOrder(pOrd : int, maxOrd : int) : int {
			if(pOrd < maxOrd) return pOrd + 1;
			else return 1;
		}
				
	}
}