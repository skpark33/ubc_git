package com.sqisoft.common.util {
	import flash.xml.XMLDocument;
	
	public class ResourceManager {
		
		private static var _instance : ResourceManager = null;
		private var infoXml : XMLDocument;
		
		public static function getInstance(): ResourceManager {
			if(_instance == null)
			{
				_instance = new ResourceManager();
				_instance.init();
			}
			
			return _instance;
		}
		
		private function init() : void {
			loadConfigXml();
		}
		
		private function loadConfigXml() : void {
			var loader : URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, loadCompleteHandler);
			var request : URLRequest = new URLRequest("hostInfo.xml");
			
			loader.load(request);				
		}
		
		private function loadCompleteHandler(event: Event) : void {
			
			var loader : URLLoader = URLLoader(event.target);
			var xml : XML = XML(loader.data); 
			
			infoXml = new XMLDocument();
			infoXml.ignoreWhite = true;
			infoXml.parseXML(xml.toString());

			loader.close();				
			
			log.debug("hostInfo load complete", infoXml);
			
		}		
		
		public function getResource(key : String) : String {
			var node : XMLNode = infoXml.firstChild; // resources
			var props : Array = node.childNodes; // property
			
			var returnValue : String = null;
			
			for each(var item : XMLNode in props){
				var name : String = item.firstChild.childNodes[0];
				var value : String = item.lastChild.childNodes[0];	
				
				if(name == key) {
					returnValue = value;
					break;
				}		
			}
				
			return returnValue;		
		}		
	}
}