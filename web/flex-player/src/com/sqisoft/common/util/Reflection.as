package com.sqisoft.common.util {

	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import mx.binding.utils.BindingUtils;
	import mx.utils.ObjectUtil;
	
	public final class Reflection 	{
		
		public static function getNewInstance( fqcn : String ) : Object {
			
			var classReference : Class = getDefinitionByName( fqcn ) as Class;
			var instance:Object = new classReference();
			
			return instance;				
		}		
		
		public static function getObjectAccessorList(obj : Object ) : Array {
			
			var x : XMLList = describeType(obj)..accessor;
			
			var result : Array = null;
			
			if(x.length() > 0 ) {
				
				result = new Array();
				
				for(var i : int = 0; i < x.length(); i++) {
					 var name : String = x[i].@name;
					 var type : String = x[i].@type;

					var o : Object = new Object();
					o.name = name;
					o.type = type;
					
					result.push(o);
				}	
			}

			return result;
		}		

		public static function bindDataToObject(item : Object, obj : Object ) : Object {
			var pArray : Array = Reflection.getObjectAccessorList(obj); // get instance accessor
			
			for(var i : int = 0; i < pArray.length; i++ ) {

				var name :String = pArray[i].name;
				var type : Object = pArray[i].type; // instance accessor

				if(item[name] == null) {
					continue;
				}
				
				if(type == 'Date') {
					item[name] = SimpleUtil.getStringToDate(item[name]);
				}else if (type == 'int') {
					item[name] = parseInt(item[name]);
				}else if (type == 'Number' || type == 'uint') {
					item[name] = parseFloat(item[name]);
				}else if (type == 'Boolean'){
					item[name] = new Boolean(item[name]);
				}
				
				if(type == 'Date' || type == 'int' || type == 'uint'|| type == 'Number' || type == 'String' || type == 'Boolean') {
					BindingUtils.bindProperty(obj, name, item, name);
				}

			}
			
			return obj;			
		}
		
		public static function populate(result : Object , target : Object ) : Object {
			
			var ac : Object = null;
			
			var resultType : String = getQualifiedClassName(result);
			
			if(resultType is Array) { // Array
				ac = new Array();

				if(result.length > 0 ) {
					
					for each (var item : Object in result) {
						var obj : Object = bindDataToObject(item, target);
						ac.push( obj );
					}
				}										

			}else if(resultType is Object) { // Object
				ac = bindDataToObject(result, target);

			}else{
				var str : String = "";
				throw new Error(str);
			}

			return ac;
		}
		
        public static function getObjectClone(src : Object) : Object {
            var className:String = getQualifiedClassName(src);
            var cls:Class = getDefinitionByName(className) as Class;
            var ret:Object = new cls();
 
            var q:QName = null;
            var options:Object = { includeReadOnly: false, uris: null, includeTransient: true };
            var property:Object = null;
            
            var classInfo:Object = ObjectUtil.getClassInfo(src, null, options);
            
            for(var i:int = 0; i < classInfo.properties.length;i++){
                q = classInfo.properties[i];
                property = src[q.localName];
                if(!property){
                    ret[q.localName] = property;
                }else{
                    if(ObjectUtil.isSimple(property)){
                        ret[q.localName] = property;
                    }else{
                        ret[q.localName] = getObjectClone(property);
                    }
                }
            }

            return ret;
        }				

		public static function bindDataForGrid(data : Object, target : Object ) : Object {
			
			var pArray : Array = Reflection.getObjectAccessorList(data); // get instance accessor
			
			for(var i : int = 0; i < pArray.length; i++ ) {

				var name :String = pArray[i].name;
				var type : Object = pArray[i].type; // instance accessor
				
				if(type == 'Date') {
					target[name] = SimpleUtil.getStringToDate(data[name]);
				}else if (type == 'int') {
					target[name] = parseInt(data[name]);
				}else if (type == 'Number' || type == 'uint') {
					target[name] = parseFloat(data[name]);
				}else if (type == 'String') {
					target[name] = data[name];
				}else if (type == 'Boolean'){
					target[name] = new Boolean(data[name]);
				}
			}
			
			return target;			
		}	

	}
	
}