package com.sqisoft.common.component {
	
	import com.sqisoft.utv.vo.Contents;
	import com.sqisoft.utv.vo.Frame;
	import com.sqisoft.common.util.Reflection;
	import com.sqisoft.utv.Constants;
	
	import flash.events.AsyncErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.filters.BlurFilter;
	import flash.filters.DropShadowFilter;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.ObjectEncoding;
	import flash.net.Responder;
	import flash.utils.Timer;
	
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	import mx.utils.ObjectUtil;
	
	public final class UTVFlvPlayer extends Canvas {

		private var metaData : Contents;
		
		private var scrubbing : Boolean = false;
		
		private var connection : NetConnection;
		private var stream : NetStream;			
		
		private var flvVideo : Video;
		private var videoHolder : UIComponent;
		
		private var ds : DropShadowFilter;
		private var blur : BlurFilter;
		
		private var _top : UTVPlayer;
		
		private var fileExist : Boolean;
		
		public function UTVFlvPlayer() {
			horizontalScrollPolicy = "off";
			verticalScrollPolicy = "off";
			
			fileExist = false;
		}
		
		public function setMetaData(meta : Object, obj : UTVPlayer) : void {
			_top = obj;
			
			if(metaData == null) metaData = new Contents();
			Reflection.populate(meta, metaData);
			
			initVideoDisplay();
			initConnection();	
		}

		public function initVideoDisplay() : void {
			invalidateDisplayList();	
			
			//ds = new DropShadowFilter(4, 45, 0, .75, 4, 4 , 1, 3, false, false, false);
			//blur = new BlurFilter(1, 1, 3);
			
			videoHolder = new UIComponent();				
			//videoHolder.filters = [ds];
			
			videoHolder.width = this.width;
			videoHolder.height = this.height;

			flvVideo = new Video();
			
			flvVideo.width = this.width;
			flvVideo.height = this.height;

			videoHolder.addChild(flvVideo);
			addChild(videoHolder);
		
		}
			
		public function initConnection() : void {
			if(Constants.TRACE_ON) trace("================ init Connection ================");
			if(connection != null) {
				reset();
			}
			
			connection = new NetConnection();
			connection.client = this;
			connection.objectEncoding = ObjectEncoding.AMF0;
			connection.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			connection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			
			if(metaData.protocol == 'rtmp://' ) { // rtmp only
				connection.connect(metaData.protocol + metaData.srcUrl); // rtmp
			} else { // http or file
				connection.connect(null);
			}				
			
			if(Constants.TRACE_ON) trace("metaData", ObjectUtil.toString(metaData));

		}
		
		private function checkFlvFile() : void {
			var nc_responder : Responder = new Responder(getFlvList, null);
			connection.call("simpleService.getListOfAvailableFLVs", nc_responder);		
			
		}
		
        private function getFlvList(list : Object) : void {
        	fileExist = false; // initialize...

        	if(Constants.TRACE_ON) trace("I WANT THIS [" + metaData.fileName + "]" );
			
			for(var items : String in list) {
				if(Constants.TRACE_ON) trace("GET LIST [" + list[items].name + "]");
				if(metaData.fileName == list[items].name) {
					if(Constants.TRACE_ON) trace("I GOT IT !! [" + list[items].name + "]");
					
					fileExist = true;
					break;
				}
			}
			
			playFlv();
		}		
		
		private function playFlv() : void {
			if(Constants.TRACE_ON) trace("fileName=" + metaData.fileName + " , isLocal= " + metaData.isLocal + " , file Exist=" + fileExist);
			
			if(metaData.isLocal) {
				if(fileExist) { 
					updateVolume();
					doPlay();		
				}else{
	                metaData.isLocal = false;
	                metaData.srcUrl = _top.getResource('FLV_SERVER_URL');
	                metaData.protocol = _top.getResource('FLV_SERVER_PROTOCOL');
	                initConnection();				
				}				
				
			}else{
				if(fileExist) { 
					updateVolume();
					doPlay();		
				}else {
					if(Constants.TRACE_ON) trace("FILE NOT FOUND!! I CAN'T PLAY THIS!");
				}
				
			}
		}
				
        private function netStatusHandler(event : NetStatusEvent) : void {
        	//log.info(metaData.source + " NetStatus=" + event.info.code);
            switch (event.info.code) {
                case "NetConnection.Connect.Success":
                    registerConnection();
                    break;
                case "NetConnection.Connect.Failed": // local 에 컨텐츠가 없으면 서버로 접속 시도
                    metaData.isLocal = false;
                    metaData.srcUrl = _top.getResource('FLV_SERVER_URL');
                    metaData.protocol = _top.getResource('FLV_SERVER_PROTOCOL');
                    initConnection();
                    break;
                case "NetStream.Play.StreamNotFound": // local 에 컨텐츠가 없으면 서버로 접속 시도
                    if(Constants.TRACE_ON) trace("netStatusHandler Stream not found: try reconnect ");

                    break;
                case "NetStream.InvalidArg":
                    break;
                case "NetStream.Buffer.Flush":
                	break; 
                case "NetStream.Buffer.Full":
                    break;
                case "NetStream.Buffer.Empty":
                	break;
                case "NetStream.Seek.Notify":
                	break;    
                case "NetStream.Seek.Stop":
                	break;    
                case "NetStream.Play.Reset":
                	break;
                case "NetStream.Play.Start":
                	break; 
                default :
                	break;
            }
        }
        
        private function securityErrorHandler(event:SecurityErrorEvent) : void {
            //log.info("securityErrorHandler: " + event);
        }			
		
        private function asyncErrorHandler(event:AsyncErrorEvent) : void {
            // ignore AsyncErrorEvent events.
            //log.info("asyncErrorHandler: " + event);
        }			

		public function onBWDone() : void {
			//log.info("onBWDone=");
		}			
		
		public function onLastSecond(...rest) : void {
			//log.info("onLastSecond" );
			
		}
		
		public function registerConnection() : void {
			stream = new NetStream(connection);
			stream.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			stream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			stream.client = this;
			
			checkFlvFile();
		}
	
		public function onPlayStatus(info : Object) : void {
			//log.info(metaData.source + " onPlayStatus=" + ObjectUtil.toString(info));
			switch(info.code) {
				case "NetStream.Play.Complete" : // play 가 완료되었을 경우

					if( metaData.defaultContent ) {
						if(Constants.TRACE_ON) trace("frame [" + metaData.frameId + "] MOVIE next COMMING SOON! playOrder [" + metaData.playOrder + "], maxOrder [" + metaData.maxOrder + "]");						
						_top.getFrameById(metaData.frameId, metaData.playOrder, metaData.maxOrder);
						
					}else{
						if(Constants.TRACE_ON) trace("frame [" + metaData.frameId + "] MOVIE replay!");
						var timer : Timer = new Timer(100, 1);
						timer.addEventListener(TimerEvent.TIMER, replayHandler);
						timer.start();
					}	  
					break;
				case "NetStream.Play.Switch" :
					
					break;
                case "NetStream.Play.Stop":
                
                	break; 
				default :
					break;
			}				
		}
		
		private function replayHandler(Event : TimerEvent) : void {
			stream.play(metaData.location + metaData.fileName);
		}
		
	    public function onMetaData(info : Object) : void {
	        //log.info("metadata: duration=" + info.duration + " width=" + info.width + " height=" + info.height + " framerate=" + info.framerate);
	    }
	    
	    public function onCuePoint(info : Object) : void {
	        //log.info("cuepoint: time=" + info.time + " name=" + info.name + " type=" + info.type);
	    }			
	    
		public function updateVolume() : void {
			var st : SoundTransform = stream.soundTransform;
			
			var adVol : Number = 0;
			if(metaData.grade == 1) adVol = .01;
			st.volume = metaData.soundVolume * adVol;
			stream.soundTransform = st;
		}			  
		
		/**
			1. contents 서버는 rtmp or http 서버로 구성		
		
		*/
		public function doPlay() : void {
			flvVideo.attachNetStream(stream);
			
			var urlStr : String = "";
			//stream.close();
			if(metaData.protocol == 'rtmp://') {
				urlStr = metaData.location + metaData.fileName;
			}else{ // http or file
				urlStr = metaData.protocol + metaData.srcUrl + metaData.location + metaData.fileName;
			}

			stream.play(urlStr);
			
			if(metaData.startPos > 0) {
				var timer : Timer = new Timer(100, 1);
				timer.addEventListener(TimerEvent.TIMER, replayHandler);
				timer.start();				
			}
		}
		
		public function doPause() : void {
			if(stream != null) stream.togglePause();
		}
		
		public function doStop() : void {
			if(stream != null) stream.close();
		}						
		
		/**
			2007.10.04 seek 가 제대로 동작하지 않음
			임시로 play 로 처리함
		*/ 
		public function doStart() : void {
//			stream.seek(metaData.startPos);
			if(stream != null) stream.play(metaData.location + metaData.fileName);
		}
		
		public function reset() : void {
			if(Constants.TRACE_ON) trace("================ reset ================");
			if(stream != null) {
				doStop();
				stream.removeEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
				stream.removeEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);	

				stream = null;
			}
			
			if(connection != null) {
				connection.close();
				connection.removeEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
				connection.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
				
				connection = null;
			}
		}				
		
		public function changeContents(frm : Frame) : void {
			if(Constants.TRACE_ON) trace("================ change Contents ================");
			doStop();
			
			var obj : Contents = frm.getContents();
			
			obj.srcUrl = _top.getResource('FLV_LOCAL_URL');
			obj.protocol = _top.getResource('FLV_LOCAL_PROTOCOL');
			
			obj.location = ""; //
			
			Reflection.populate(obj, metaData);	
			
			checkFlvFile();
		}		
	}
}