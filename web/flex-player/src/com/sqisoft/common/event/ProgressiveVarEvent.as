package com.sqisoft.common.event {
	import flash.events.Event;

	public class ProgressiveVarEvent extends Event {
		public var promotionValueList : String;
		
		public function ProgressiveVarEvent(type:String, vl : String) {
			super(type, true, true);
			promotionValueList = vl;
		}
		
	}
}