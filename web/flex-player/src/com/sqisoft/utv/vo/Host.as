package com.sqisoft.utv.vo {
	
	public final class Host {
		private var _siteId : String;
		private var _hostId : String;
		private var _localIp : String;
		
		public function Host() {
			
		}
		
		public function set siteId(value : String) : void {
			_siteId = value;
		}
		
		public function get siteId() : String {
			return _siteId;
		}
		
		public function set hostId(value : String) : void {
			_hostId = value;
		}
		
		public function get hostId() : String {
			return _hostId;
		}
		
		public function set localIp(value : String) : void {
			_localIp = value;
		}
		
		public function get localIp() : String {
			return _localIp;
		}				
		
	}
}