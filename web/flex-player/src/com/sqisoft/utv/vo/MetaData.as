package com.sqisoft.utv.vo {
	
	
	
	public final class MetaData {
		
		private var _width : Number = 0;
		private var _height : Number = 0;
		private var _duration : Number = 0;
		private var _source : String = "";
		private var _srcUrl : String = null; // rtmp connect url
		private var _framerate : Number = 0;
		private var _volume : Number = 0;
		private var _startPos : Number = 0;
		private var _loop : Boolean = true;
		private var _isLocal : Boolean = true;
		private var _protocol : String = "http://"; // http or rtmp
		private var _runningTime : int = 0;
		private var _defaultContent : Boolean = false;
		private var _frameId : String = "";
		private var _playOrder : int = 0;
		private var _maxOrder : int = 0;
		private var _grade : int = 0;
		
		public function MetaData() {
			
		}
		
		public function get grade() : int {
			return _grade;
		}			
		
		public function set grade(value : int) : void {
			_grade = value;
		}
				
		public function set maxOrder(value : int) : void {
			_maxOrder = value;
		}

		public function get maxOrder() : int {
			return _maxOrder;
		}
				
		public function set playOrder(value : int) : void {
			_playOrder = value;
		}

		public function get playOrder() : int {
			return _playOrder;
		}		
		
		public function set frameId(value : String) : void {
			_frameId = value;
		}

		public function get frameId() : String {
			return _frameId;
		}		
		
		public function set defaultContent(value : Boolean) : void {
			_defaultContent = value;
		}

		public function get defaultContent() : Boolean {
			return _defaultContent;
		}		
		
		public function set runningTime(value : int) : void {
			_runningTime = value;
		}

		public function get runningTime() : int {
			return _runningTime;
		}		
	
		public function set protocol(value : String) : void {
			_protocol = value;
		}

		public function get protocol() : String {
			return _protocol;
		}		
		
		public function set isLocal(value : Boolean) : void {
			_isLocal = value;
		}

		public function get isLocal() : Boolean {
			return _isLocal;
		}		
		
		public function set width(value : Number) : void {
			_width = value;
		}

		public function set height(value : Number) : void {
			_height = value;
		}
		
		public function set duration(value : Number) : void {
			_duration = value;
		}
		
		public function set source(value : String) : void {
			_source = value;
		}
		
		public function set srcUrl(value : String) : void {
			_srcUrl = value;
		}
		
		public function set framerate(value : Number) : void {
			_framerate = value;
		}						
		
		public function set volume(value : Number) : void {
			_volume = value;
		}
		
		public function set startPos(value : Number) : void {
			_startPos = value;
		}		
		
		public function set loop(value : Boolean) : void {
			_loop = value;
		}		

		public function get width() : Number {
			return _width;
		}

		public function get height() : Number {
			return _height;
		}
		
		public function get duration() : Number {
			return _duration;
		}
		
		public function get source() : String {
			return _source;
		}
		
		public function get srcUrl() : String {
			return _srcUrl;
		}	
		
		public function get framerate() : Number {
			return _framerate;
		}		
		
		public function get volume() : Number {
			return _volume;
		}
		
		public function get startPos() : Number {
			return _startPos;
		}	
		
		public function get loop() : Boolean {
			return _loop;
		}														
	}
}