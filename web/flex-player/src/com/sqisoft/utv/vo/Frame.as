package com.sqisoft.utv.vo {
	
	import com.sqisoft.common.component.IFrame;
	import com.sqisoft.common.component.ImageViewer;
	import com.sqisoft.common.component.PromotionViewer;
	import com.sqisoft.common.component.SimpleMarquee;
	import com.sqisoft.common.component.SmsViewer;
	import com.sqisoft.common.component.UTVFlvPlayer;
	import com.sqisoft.common.util.DisplayObjectHelper;
	import com.sqisoft.common.util.Reflection;
	import com.sqisoft.utv.Constants;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.core.UIComponent;
	
	public final class Frame {
		private var _frameId : String;
		private var _width : int;
		private var _height : int;
		
		private var _x : int;
		private var _y : int;
		
		private var contents : Contents;
		
		private var dispObj : UIComponent;
		
		private var _grade : int;
		
		private var timer : Timer;
		
		private var topComp : UTVPlayer;
		
		private var _borderStyle : String = "none";
		private var _borderColor : String = "";
		private var _borderThickness : int = 0;
		private var _cornerRadius : int = 0;

		public function Frame(obj : UTVPlayer) {
			contents = new Contents();
			_grade = 0;
			
			topComp = obj;
		}
		
		public function changeContents(newObj : Contents) : void {
			if(Constants.TRACE_ON) trace("frameId [" + this._frameId + "], change Contents! type [" + newObj.contentsType + "], old [" + contents.contentsName + "] -> new [" + newObj.contentsName + "] ================");
			
			Reflection.populate(newObj, this.contents);
			
			this.stopTimer();
			
			switch(contents.contentsType) {
				case Constants.CONTENTS_TYPE_VIDEO : 
					(dispObj as UTVFlvPlayer).changeContents(this);
					break;					
					
				case Constants.CONTENTS_TYPE_SMS : 
					(dispObj as SmsViewer).changeContents(contents);
					break;					
					
				case Constants.CONTENTS_TYPE_IMAGE : 
					(dispObj as ImageViewer).changeContents(contents);
					break;					
					
				case Constants.CONTENTS_TYPE_PROMOTION : 
					(dispObj as PromotionViewer).changeContents(contents);
					break;					
					
				case Constants.CONTENTS_TYPE_TV : 

					break;					
					
				case Constants.CONTENTS_TYPE_TICKER : 
					(dispObj as SimpleMarquee).changeContents(contents);
					break;					

				case Constants.CONTENTS_TYPE_URL : 
					(dispObj as IFrame).source = "http://" + contents.fileName;
					(dispObj as IFrame).visible = true;
					break;	
										
				default : 
				
					break;
			}

			if(contents.contentsType != Constants.CONTENTS_TYPE_VIDEO 
				&& contents.runningTime > 0 ) {
				startTimer();			
			}			
			
		}
		
		public function reset() : void {
			if(dispObj != null ) {
				
				if(dispObj is UTVFlvPlayer) {
					(dispObj as UTVFlvPlayer).reset();
					(dispObj as UTVFlvPlayer).removeAllChildren();
				}
				
				if(dispObj is IFrame) {
					(dispObj as IFrame).visible = false;
					(dispObj as IFrame).removeAllChildren();
				}
				
				if(dispObj is ImageViewer) {
					(dispObj as ImageViewer).removeAllChildren();
				}	
				
				if(dispObj is SimpleMarquee) {
					(dispObj as SimpleMarquee).removeAllChildren();
				}	
				
				if(dispObj is SmsViewer) {
					(dispObj as SmsViewer).removeAllChildren();
				}												
				
				if(dispObj.parent != null) {
					dispObj.parent.removeChild(dispObj);
				}
				
				dispObj = null;
				if(Constants.TRACE_ON) trace("REMOVE DispObject! frameId [" + _frameId + "]");
			}			
			
			if(timer != null) {
				if(timer.running) stopTimer();
				timer = null;
			}
			
			_frameId = null;
			_x = 0;
			_y = 0;
			_width = 0;
			_height = 0;
			contents = new Contents();
			_grade = 0;
			_borderStyle = "none";
			_borderColor = "#000000";
			_borderThickness = 0;
			_cornerRadius = 0;
			
		}
		
		public function addDispObject() : void {
			if(Constants.TRACE_ON) trace("ADD DispObject! frameId [" + _frameId + "]");
			
			switch(this.contents.contentsType) {
				case Constants.CONTENTS_TYPE_VIDEO : 
					DisplayObjectHelper.createFlvPlayer(this, topComp);
					break;					
				case Constants.CONTENTS_TYPE_SMS : 
					DisplayObjectHelper.createSmsViewer(this, topComp);
					break;					
				case Constants.CONTENTS_TYPE_IMAGE : 
					DisplayObjectHelper.createImgViewer(this, topComp);				
					break;					
				case Constants.CONTENTS_TYPE_PROMOTION : 
					DisplayObjectHelper.createPromotionViewer(this, topComp);
					break;					
				case Constants.CONTENTS_TYPE_TV : 

					break;					
				case Constants.CONTENTS_TYPE_TICKER : 
					DisplayObjectHelper.createTxtTicker(this, topComp);
					break;					
					
				case Constants.CONTENTS_TYPE_URL : 
					DisplayObjectHelper.createIFrame(this, topComp);
					break;	
							
				default : 
				
					break;
			}
			
			topComp.mainStage.addChild(dispObj);
			
		}		
		
		public function setDispObjLayout() : void {
			dispObj.x = _x;
			dispObj.y = _y;
			dispObj.width = _width;
			dispObj.height = _height;			
		}
		
		public function setDispObj(value : UIComponent) : void {
			dispObj = value;
		}
		
		public function getDispObj() : UIComponent {
			return dispObj;
		}		
		
		public function setContents(value : Contents) : void {
			contents = value;
		}
		
		public function getContents() : Contents {
			return contents;
		}	
		
		private function timerHandler(event : TimerEvent) : void {
			topComp.getFrameById( _frameId, contents.playOrder, contents.maxOrder );
		}
		
		public function startTimer() : void {
			if(timer == null) {
				timer = new Timer(contents.runningTime * 1000, 1);
			}
			timer.addEventListener(TimerEvent.TIMER, timerHandler);
			timer.start();
			if(Constants.TRACE_ON) trace("frameId [" + this._frameId + "] TIMER start! contents [" + this.getContents().contentsId + ", " + this.getContents().contentsName + "], sec [" + this.getContents().runningTime + "]");
		}
		
		public function stopTimer() : void {
			if(timer != null && timer.running) {
				timer.stop();
				if(Constants.TRACE_ON) trace("frameId [" + this._frameId + "] TIMER stop! contents [" + this.getContents().contentsId + ", " + this.getContents().contentsName + "], sec [" + this.getContents().runningTime + "]");
			}
		}
		
		public function set frameId(value : String) : void {
			_frameId = value;
		}
		
		public function get frameId() : String {
			return _frameId;
		}
		
		public function set width(value : int) : void {
			_width = value;
		}
		
		public function get width() : int {
			return _width;
		}		
		
		public function set height(value : int) : void {
			_height = value;
		}
		
		public function get height() : int {
			return _height;
		}
		
		public function set x(value : int) : void {
			_x = value;
		}
		
		public function get x() : int {
			return _x;
		}
		
		public function set y(value : int) : void {
			_y = value;
		}
		
		public function get y() : int {
			return _y;
		}
		
		public function get grade() : int {
			return _grade;
		}			
		
		public function set grade(value : int) : void {
			_grade = value;
		}		
		
		public function set cornerRadius(value : int) : void {
			_cornerRadius = value;
		}
		
		public function get cornerRadius() : int {
			return _cornerRadius;
		}
				
		public function set borderThickness(value : int) : void {
			_borderThickness = value;
		}
		
		public function get borderThickness() : int {
			return _borderThickness;
		}
		
		public function set borderColor(value : String) : void {
			_borderColor = value;
		}
		
		public function get borderColor() : String {
			return _borderColor;
		}
				
		public function set borderStyle(value : String) : void {
			_borderStyle = value;
		}
		
		public function get borderStyle() : String {
			return _borderStyle;
		}
				
	}
}