package com.sqisoft.utv.vo {
	public final class ProgressVar {
	    private var _contentsId : String;
	    private var _progressBarId : String;
	    private var _itemName : String;
	    private var _x : int;
	    private var _y : int;
	    private var _width : int;
	    private var _height : int;
	    private var _fgColor : String;
	    private var _bgColor : String;
	    private var _borderColor : String;
	    private var _borderThickness : int;
	    private var _valueVisible : int;
	    private var _fontSize : int;
	    private var _minValue : int;
	    private var _maxValue : int;
	    
		public function ProgressVar() {
		}

		public function get contentsId() : String {
			return _contentsId;
		}
		
		public function set contentsId(v : String) : void {
			_contentsId = v;
		}
		
		public function get progressBarId() : String {
			return _progressBarId;
		}
		
		public function set progressBarId(v : String) : void {
			_progressBarId = v;
		}
		
		public function get itemName() : String {
			return _itemName;
		}
		
		public function set itemName(v : String) : void {
			_itemName = v;
		}
		
		public function get x() : int {
			return _x;
		}
		
		public function set x(v : int) : void {
			_x = v;
		}
		
		public function get y() : int {
			return _y;
		}
		
		public function set y(v : int) : void {
			_y = v;
		}
		
		public function get width() : int {
			return _width;
		}
		
		public function set width(v : int) : void {
			_width = v;
		}
		
		public function get height() : int {
			return _height;
		}
		
		public function set height(v : int) : void {
			_height = v;
		}
		
		public function get fgColor() : String {
			return _fgColor;
		}
		
		public function set fgColor(v : String) : void {
			_fgColor = v;
		}
		
		public function get bgColor() : String {
			return _bgColor;
		}
		
		public function set bgColor(v : String) : void {
			_bgColor = v;
		}
				
		public function get borderColor() : String {
			return _borderColor;
		}
		
		public function set borderColor(v : String) : void {
			_borderColor = v;
		}		

		public function get borderThickness() : int {
			return _borderThickness;
		}
		
		public function set borderThickness(v : int) : void {
			_borderThickness = v;
		}
		
		public function get valueVisible() : int {
			return _valueVisible;
		}
		
		public function set valueVisible(v : int) : void {
			_valueVisible = v;
		}
		
		public function get fontSize() : int {
			return _fontSize;
		}
		
		public function set fontSize(v : int) : void {
			_fontSize = v;
		}
		
		public function get minValue() : int {
			return _minValue;
		}
		
		public function set minValue(v : int) : void {
			_minValue = v;
		}
		
		public function get maxValue() : int {
			return _maxValue;
		}
		
		public function set maxValue(v : int) : void {
			_maxValue = v;
		}
		
	}
}