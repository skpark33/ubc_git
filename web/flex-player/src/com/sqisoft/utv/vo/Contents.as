package com.sqisoft.utv.vo {
	
	public final class Contents {
		
		private var _contentsId : String;
		private var _promotionId : String;
		private var _contentsName : String;
		private var _contentsType : int = 0;
		private var _duration : int = 0;
		
		private var _fileName : String;
		private var _location : String = "";
		
		private var _bgColor : String;
		private var _fgColor : String;
		private var _font : String;
		private var _fontSize : int = 0;
		
		private var _playSpeed : int = 0;
		private var _runningTime : Number = 0;
		private var _soundVolume : Number = 0;
		
		private var _comment1 : String;
		private var _comment2 : String;
		private var _comment3 : String;
		
		private var _description : String;
		private var _promotionValueList : String;
		
		private var _srcUrl : String;
		private var _frameId : String;
		private var _defaultContent : Boolean;
		
		private var _playOrder : int = 0;
		private var _maxOrder : int = 0;
		private var _loop : Boolean = false;
		private var _isLocal : Boolean = true;
		
		private var _protocol : String = "http://"; // http or rtmp
		private var _grade : int = 0;
		private var _startPos : Number = 0;
		
		private var _progressBarList : Array = new Array();
		
		public function Contents() {
			
		}
		
		public function set progressBarList(v : Array) : void {
			_progressBarList = v;
		}
		
		public function get progressBarList() : Array {
			return _progressBarList;
		}
		
		public function set promotionId(value : String) : void {
			_promotionId = value;
		}

		public function get promotionId() : String {
			return _promotionId;
		}	
		
		public function set promotionValueList(value : String) : void {
			_promotionValueList = value;
		}

		public function get promotionValueList() : String {
			return _promotionValueList;
		}					
		
		public function set startPos(value : Number) : void {
			_startPos = value;
		}
		
		public function get startPos() : Number {
			return _startPos;
		}				
		
		public function set isLocal(value : Boolean) : void {
			_isLocal = value;
		}
		
		public function get isLocal() : Boolean {
			return _isLocal;
		}	
		
		public function set protocol(value : String) : void {
			_protocol = value;
		}

		public function get protocol() : String {
			return _protocol;
		}	
		
		public function get grade() : int {
			return _grade;
		}			
		
		public function set grade(value : int) : void {
			_grade = value;
		}								
		
		public function compareContentsId(val : String) : Boolean {
			if(_contentsId == val) return true;
			else return false;
		}
		
		public function compareContentsType(val : int) : Boolean {
			if(_contentsType == val) return true;
			else return false;
		}	
				
		public function set loop(value : Boolean) : void {
			_loop = value;
		}
		
		public function get loop() : Boolean {
			return _loop;
		}		
				
		public function get maxOrder() : int {
			return _maxOrder;
		}			
		
		public function set maxOrder(value : int) : void {
			_maxOrder = value;
		}			
		
		public function get playOrder() : int {
			return _playOrder;
		}			
		
		public function set playOrder(value : int) : void {
			_playOrder = value;
		}		
		
		public function set defaultContent(value : Boolean) : void {
			_defaultContent = value;
		}
		
		public function get defaultContent() : Boolean {
			return _defaultContent;
		}		
		
		public function set frameId(value : String) : void {
			_frameId = value;
		}
		
		public function get frameId() : String {
			return _frameId;
		}
				
		public function set srcUrl(value : String) : void {
			_srcUrl = value;
		}
		
		public function get srcUrl() : String {
			return _srcUrl;
		}		
		
		public function set contentsId(value : String) : void {
			_contentsId = value;
		}
		
		public function get contentsId() : String {
			return _contentsId;
		}
		
		public function set contentsName(value : String) : void {
			_contentsName = value;
		}
		
		public function get contentsName() : String {
			return _contentsName;
		}	 

		public function set contentsType(value : int) : void {
			_contentsType = value;
		}
		
		public function get contentsType() : int {
			return _contentsType;
		}			
		
		public function set duration(value : int) : void {
			_duration = value;
		}
		
		public function get duration() : int {
			return _duration;
		}		
		
		public function set fileName(value : String) : void {
			_fileName = value;
		}
		
		public function get fileName() : String {
			return _fileName;
		}		
		
		public function set location(value : String) : void {
			_location = value;
		}
		
		public function get location() : String {
			return _location;
		}		
		
		public function set bgColor(value : String) : void {
			_bgColor = value;
		}
		
		public function get bgColor() : String {
			return _bgColor;
		}		
		
		public function set fgColor(value : String) : void {
			_fgColor = value;
		}
		
		public function get fgColor() : String {
			return _fgColor;
		}	
		
		public function set font(value : String) : void {
			_font = value;
		}
		
		public function get font() : String {
			return _font;
		}			
		
		public function set fontSize(value : int) : void {
			_fontSize = value;
		}
		
		public function get fontSize() : int {
			return _fontSize;
		}		
		
		public function set playSpeed(value : int) : void {
			_playSpeed = value;
		}
		
		public function get playSpeed() : int {
			return _playSpeed;
		}				
		
		public function set runningTime(value : Number) : void {
			_runningTime = value;
		}
		
		public function get runningTime() : Number {
			return _runningTime;
		}
		
		public function set soundVolume(value : Number) : void {
			_soundVolume = value;
		}
		
		public function get soundVolume() : Number {
			return _soundVolume;
		}		
				
		public function set comment1(value : String) : void {
			_comment1 = value;
		}
		
		public function get comment1() : String {
			return _comment1;
		}	
		
		public function set comment2(value : String) : void {
			_comment2 = value;
		}
		
		public function get comment2() : String {
			return _comment2;
		}	
		
		public function set comment3(value : String) : void {
			_comment3 = value;
		}
		
		public function get comment3() : String {
			return _comment3;
		}					
		
		public function set description(value : String) : void {
			_description = value;
		}
		
		public function get description() : String {
			return _description;
		}		
							
	}
}