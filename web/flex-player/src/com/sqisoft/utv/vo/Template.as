package com.sqisoft.utv.vo {
	
	import com.blitzagency.xray.logger.XrayLog;
	import com.ericfeminella.utils.HashMap;
	
	public final class Template {
		private var _templateId : String;
		private var _width : int;
		private var _height : int;
		private var _frames : HashMap;
		
		private var log : XrayLog;
		
		public function Template() {
			log = new XrayLog();
			_frames = new HashMap();
		}
		
		public function reset() : void {
			_templateId = null;
			_width = 0;
			_height = 0;
			
			// frame 초기화
			if(_frames != null && _frames.size() > 0) {
				var keys : Array = _frames.getKeys();
				
				for(var i : int = 0; i < keys.length; i++) {
					var frmObj : Frame = _frames.getValue(keys[i]);
					if(frmObj != null) frmObj.reset();
				}				
			}
			
			_frames.clear();
		}

		public function set templateId(value : String) : void {
			_templateId = value;
		}
		
		public function get templateId() : String {
			return _templateId;
		}	
				
		public function set width(value : int) : void {
			_width = value;
		}
		
		public function get width() : int {
			return _width;
		}		
		
		public function set height(value : int) : void {
			_height = value;
		}
		
		public function get height() : int {
			return _height;
		}	
		
		public function set frames(value : HashMap) : void {
			_frames = value;
		}
		
		public function get frames() : HashMap {
			return _frames;
		}
		
		public function addFrame(obj : Frame) : void {
			if(obj == null) {
				//log.debug("================ Frame object is null! return ================");
				return;
			}
			
			if( _frames.containsKey(obj.frameId) ) { // 프레임이 있으면
				
				var orgFrm : Frame = _frames.getValue(obj.frameId);
				var cont : Contents = orgFrm.getContents();
				
				// contentsId 를 비교하여 다를 경우에만 replace 한다.
				if( cont.compareContentsId(obj.getContents().contentsId) ) {
					//log.debug("================ contents ID is equal! [" + obj.getContents().contentsId + "] return ================");
					return;
				}
				
				if( cont.compareContentsType(obj.getContents().contentsType) ) {
					// contents 만 바꾼다.
					orgFrm.changeContents(obj.getContents());
				}else{ // contentType 이 다르면 프레임을 제거하고 새로 추가한다.
					removeFrame(obj.frameId);
					_frames.put(obj.frameId, obj);
					
					// add XXX to parent display
					obj.addDispObject();					
				}
				
			} else {
				_frames.put(obj.frameId, obj);
				
				// add XXX to parent display
				obj.addDispObject();
			}
			
		}						

		public function removeFrame(id : String) : void {
			if(_frames.containsKey(id)) {
				var frm : Frame = _frames.getValue(id);
				
				frm.reset();
				
				_frames.remove(id);
			}			
		}
		
		public function getFrame(id : String) : Frame {
			return _frames.getValue(id);
		}		
		
		public function removeAllFrame() : void {
			_frames.clear();
		}
	}
}