package com.sqisoft.utv {
	public class Command {
		public static const INIT_TEMPLATE : int = 0;
		public static const GET_TEMPLATE : int = 1;
		public static const GET_FRAME : int = 2;
		public static const GET_FRAME_BY_ID : int = 3;
		public static const GET_PROMOTION_VALUE_LIST : int = 4;
		public static const GET_PROGRESSBAR_LIST : int = 5;
	}
}