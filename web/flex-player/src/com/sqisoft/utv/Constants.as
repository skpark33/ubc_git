package com.sqisoft.utv {
	
	public final class Constants {
	
		public static const CONTENTS_TYPE_VIDEO : int = 0;
		public static const CONTENTS_TYPE_SMS : int = 1;
		public static const CONTENTS_TYPE_IMAGE : int = 2;
		public static const CONTENTS_TYPE_PROMOTION : int = 3;
		public static const CONTENTS_TYPE_TV : int = 4;
		public static const CONTENTS_TYPE_TICKER : int = 5;
		public static const CONTENTS_TYPE_PHONE : int = 6;
		public static const CONTENTS_TYPE_URL : int = 7;
		public static const CONTENTS_TYPE_ETC : int = 99;
		
		public static const TRACE_ON : Boolean = false;
	
	}
}