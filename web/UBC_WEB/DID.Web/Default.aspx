﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <title>::: UBC :::</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="./Asset/Style/login_layout.css" rel="stylesheet" type="text/css" />
    <link href="./Asset/Style/login_menu_style.css" rel="stylesheet" type="text/css" />

</head>

<body >

<div class="wrap">
  
    <div id="Header">
        <div id="TopMenuBox">
            <div class="logo"><img src="./Asset/Images/logo.gif" /></div>
        </div>
    </div>
   
    <!-- contents -->
    <div class="login_body"> 
    
    <%--<p class="login_title">
        <img src="./Asset/Images/login/title.png" />
    </p>--%>
    
    <form id="form1" runat="server">
        
        <!-- 로그인 영역 -->
        <div class="login_input">
            
            <div class="line_bottom pad_B10" style="background:url(./asset/images/login/login.jpg) no-repeat left top ; height:40px; margin: 0; padding:30px 0 0 80px">
                <span style="font-weight:bold; font-size:30px; "><%= Resources.Resource.txt_0107 %><!-- 사용자 로그인 //--></span>
            </div>

            <div  class="pad_T20 pad_B30 align_C">

                <table width="85%" border="0" cellspacing="0" cellpadding="0">
                <colgroup>
                    <col width="62px" />
                    <col width="" />
                    <col width="100px" />
                </colgroup>
                <tbody>
                    <tr>
                        <td height="32px" style="text-align:right">
                            <%= Resources.Resource.txt_0004 %><!-- 사용자ID //-->
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserID" runat="server" Width="150px" TabIndex="1"  Text=""   CssClass="i_text" />
                        </td>
                        <td rowspan="2">     
                            <asp:ImageButton runat="server" ID="btnLogin"   ImageUrl="./Asset/Images/login/bt_login.gif" onclick="btnLogin_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td height="32" style="text-align:right">
                            <%= Resources.Resource.txt_0010 %><!-- 비밀번호 //-->
                        </td>
                        <td>
                            <asp:TextBox  ID="txtPwd" runat="server"  Width="150" TabIndex="2" TextMode="Password" Text="" CssClass="i_text" />
                        </td>
                    </tr>
                </tbody>
                </table>

            </div>
            
            <div class="pad_T20 line_top">
                <a href="findPassword.aspx"><span class="float_R" ><%= Resources.Resource.txt_0090 %><!-- 비밀번호 찾기 --></span> </a>
            </div>
        </div>
        <!-- //로그인 영역 -->
    
    </form>
    
    </div>
    <!-- //contents -->    
    
</div>
    
</body>


</html>
