﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="contentsAccess.aspx.cs" Inherits="contentsAccess"   EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<!-- 구글 파이챠트 시작-->

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load('visualization', '1', { packages: ['corechart'] });
</script>
<script type="text/javascript">      
    
    function drawVisualization() 
    {
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', '항목');
        data.addColumn('number', '갯수');
        data.addRows(<%=this.allCount %>);              
              
        <%= this.arrayString %>             
                     
        // 차트 데이타 형식
        // data.setValue(0, 0, '항목1');
        // data.setValue(0, 1, 갯수); 
        // data.setValue(1, 0, 'host_process_down');
        // data.setValue(1, 1, 2); 
        // data.setValue(2, 0, 'host_hdd_overload');
        // data.setValue(2, 1, 2);
        // 참고 : http://code.google.com/apis/ajax/playground/                      

        // Create and draw the visualization.
        new google.visualization.PieChart(document.getElementById('visualization')).draw(data, {title:"<%=this.arrayTitle %>"});
    }   

    google.setOnLoadCallback(drawVisualization);
    
</script>

<!-- 구글 파이챠트 끝-->


<script language="javascript" type="text/javascript">


    function DoShowDetail(siteId, siteName, fromDate, toDate) {

        window.open("./interActiveDetail.aspx?siteId=" + siteId.toString() + "&siteName=" + siteName.toString() + "&fromDate=" + fromDate.toString() + "&toDate=" + toDate.toString(), "interActiveDetail", "toolbar=0,scrollbars=0,location=0,directories=0,status=0,menubar=0,resizable=no,width=800,height=400");
    }
    
    function DoSelect() {

        if (!confirm("<%= Resources.Resource.msg_0041 %>"))  // 검색 조건에 따라서 조회 시간이 오래 걸릴 수 있습니다.\n빠른 조회를 원하시면 검색 조건을 구체화 해주세요.\n계속 하시겠습니까?
            return false;
            
        //////////////////////
        // 날짜 유효성 검사 시작

        var startDate = $("#<%= fromDate.ClientID%>").val();
        var endDate = $("#<%= toDate.ClientID%>").val();

        // 시작날짜 문자열에서 년,월,일 을 뽑아 숫자로 변환
        var yearStart = parseInt(startDate.substr(0, 4), 10);
        var monthStart = parseInt(startDate.substr(5, 2), 10);
        var dayStart = parseInt(startDate.substr(8), 10);

        // 종료날짜 문자열에서 년,월,일 을 뽑아 숫자로 변환
        var yearEnd = parseInt(endDate.substr(0, 4), 10);
        var monthEnd = parseInt(endDate.substr(5, 2), 10);
        var dayEnd = parseInt(endDate.substr(8), 10);

        // 날짜 비교
        var dStart = new Date(yearStart, monthStart, dayStart);
        var dEnd = new Date(yearEnd, monthEnd, dayEnd);

        if (dStart > dEnd) {
            alert("<%= Resources.Resource.msg_0040 %>"); // 종료일자가 시작일자보다 앞설 수 없습니다
            return false;
        }

        // 날짜 유효성 검사 끝
        //////////////////////


        document.getElementById('Panel1').style.display = 'block';

        document.getElementById('dataPanel').style.display = 'none';
        
        return true;

    }

       
</script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
  <colgroup>
  <col width="auto" />
  <col width="50%" />
  </colgroup>
  <tr>
    <td><span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span></td>
  </tr>
</table>
 
    <div class='topOptionBtns'>
       <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources: Resource, btn_0007 %>" OnClick="btnSelect_Click" OnClientClick='return DoSelect()' /> <!-- 조회 -->
        <asp:Button ID="btnExcel" runat="server" Text="<%$ Resources: Resource, btn_0006 %>" OnClick="btnExcel_Click" Visible=false  /> <!-- 엑셀저장 -->
    </div>    

    <table class="boardInputType mb10" cellpadding="0" cellspacing="0" border="0">
		<colgroup>		
		<col width="150px" />
		<col width="250px" />
		<col width="150px" />
		<col width="auto" />	
	</colgroup>
    <tr>
       <th><span><%= Resources.Resource.txt_0049 %><!-- 조회기간 --></span></th>
       <td>        
          <asp:TextBox ID="fromDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);" onkeydown="return false;" />
            ~
          <asp:TextBox ID="toDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  onkeydown="return false;"  />                    
        </td>  
        <th><span><%= Resources.Resource.txt_0003 %><!-- 조직명 --></span></th>    
        <td>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:DropDownList ID="ddlType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
                <asp:DropDownList ID="ddlClass1" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass1_SelectedIndexChanged" />
                <asp:DropDownList ID="ddlClass2" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass2_SelectedIndexChanged" />
                <asp:DropDownList ID="ddlClass3" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass3_SelectedIndexChanged" />
                <asp:HiddenField ID="hiddenSiteIdForSearch" runat="server" />                
                <asp:HiddenField ID="hiddenSelectedSiteId" runat="server" />
                <asp:HiddenField ID="hiddenHasChildSite" runat="server" />                    
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>                     
    </tr>
    </table>
    
    
    <div ID="Panel1" style="display: none;">
       
        <table style="width:100%;height:290px;">
        <tr>
            <td style="text-align:center">
            <asp:Image ID="Image2" runat="server"  ImageUrl="~/Asset/Images/loading.gif" />
            <br />
            Processing.... please wait...
            
            </td>
        </tr>
        </table>
    
    </div>  

    <div id="dataPanel" class="container" style="display: block;">
    
    
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate> 
        
        <div class="container" >
        
        <asp:GridView ID="gvList" runat="server"  OnPreRender="gvList_PreRender" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false" 
              ShowHeader="TRUE" CssClass="boardListType1">
              
        <%-- GridView 컬럼  
            조회기간
            조직명
            총 방문자수
            총 페이지뷰
        --%>   
                    
        <Columns>
          
            <asp:BoundField DataField="s_date" HeaderText="<%$ Resources: Resource, txt_0049 %>" HeaderStyle-Width="170px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="s_date" />                        
            <asp:BoundField DataField="siteName" HeaderText="<%$ Resources: Resource, txt_0003 %>" HeaderStyle-Width="110px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="siteName" />
            <asp:BoundField DataField="cnt_visit" HeaderText="<%$ Resources: Resource, txt_0076 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="cnt_visit" />
            <asp:BoundField DataField="cnt_view" HeaderText="<%$ Resources: Resource, txt_0077 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="cnt_view"   />
            
            <asp:TemplateField HeaderText="<%$ Resources: Resource, txt_0078 %>">

            <ItemTemplate>
                <asp:Panel ID="Panel2" runat="server"  >
                    <asp:GridView runat="server" ID="gvList2" OnRowDataBound="gvList2_RowDataBound"  AutoGenerateColumns="false" ShowHeader="true" CssClass="boardListType2">
                    
                     <%-- GridView 컬럼  
                        조직명
                        총 방문자수
                        총 페이지뷰
                        메뉴별 상세내역
                    --%>       
                    
                    <Columns>
                        <asp:BoundField DataField="siteName" HeaderText="<%$ Resources: Resource, txt_0003 %>" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign=Left ItemStyle-VerticalAlign=Middle SortExpression="siteName" />
                        <asp:BoundField DataField="cnt_visit" HeaderText="<%$ Resources: Resource, txt_0076 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Right ItemStyle-VerticalAlign=Middle SortExpression="cnt_visit" />
                        <asp:BoundField DataField="cnt_view" HeaderText="<%$ Resources: Resource, txt_0077 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Right ItemStyle-VerticalAlign=Middle SortExpression="cnt_view"   />
                        <asp:ButtonField Text="<%$ Resources: Resource, txt_0079 %>" HeaderText="<%$ Resources: Resource, txt_0079 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Center />
                    </Columns> 
                    
                    </asp:GridView>
               </asp:Panel>
            </ItemTemplate>
            
            </asp:TemplateField>
             
        </Columns> 
        <EmptyDataTemplate>
            <table style="width:100%;height:290px;">
                <tr>
                    <td style="text-align:center">
                    <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        
        </asp:GridView>   
        
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
     
        <!-- 그래프  -->
        <asp:Panel ID="PanelGraph" runat="server" Visible=false>
            <div id="visualization"  style="width: 820px; height: 400px;"></div>
        </asp:Panel>
        
    </div>
     
</asp:Content>


