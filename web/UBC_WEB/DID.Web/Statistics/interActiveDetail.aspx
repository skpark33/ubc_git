﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/PopupMasterPage.master" AutoEventWireup="true" CodeFile="interActiveDetail.aspx.cs" Inherits="interActiveDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<table class="boardListType2" cellpadding="0" cellspacing="0" border="0" >
<tr>
    <td><%= subTitle %></td>
</tr>

<tr>
    <td align="center">
        <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" ShowHeader="TRUE" CssClass="boardListType3">
        <Columns>
            <asp:BoundField DataField="category1" HeaderText="<%$ Resources: Resource, txt_0082 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category1" />
            <asp:BoundField DataField="category2" HeaderText="<%$ Resources: Resource, txt_0083 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category2" />
            <asp:BoundField DataField="category3" HeaderText="<%$ Resources: Resource, txt_0084 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category3" />
            <asp:BoundField DataField="category4" HeaderText="<%$ Resources: Resource, txt_0159 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category4" />
            <asp:BoundField DataField="category5" HeaderText="<%$ Resources: Resource, txt_0160 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category5" />
            <asp:BoundField DataField="touch_cnt" HeaderText="<%$ Resources: Resource, txt_0085 %>" HeaderStyle-HorizontalAlign=Center HeaderStyle-Width="60px" ItemStyle-HorizontalAlign=Right SortExpression="touch_cnt" />
        </Columns>
        </asp:GridView>
     </td>
 </tr>
 
<tr>
<td align="center" >
    <br /><asp:Button ID="btnClose" runat="server" Text="<%$ Resources: Resource, txt_0086 %>" OnClientClick="self.close();" /><br />
</td> 
</tr>

</table>
     
    
</asp:Content>

