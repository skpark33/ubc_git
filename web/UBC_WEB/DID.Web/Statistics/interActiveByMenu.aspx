﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="interActiveByMenu.aspx.cs" Inherits="interActiveByMenu" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoSelect() {

        if (!confirm("<%= Resources.Resource.msg_0041 %>"))  // 검색 조건에 따라서 조회 시간이 오래 걸릴 수 있습니다.\n빠른 조회를 원하시면 검색 조건을 구체화 해주세요.\n계속 하시겠습니까?
            return false;
        
        // 날짜 유효성 검사 시작

        var strStart_date = $("#<%= fromDate.ClientID%>").val();
        var strEnd_Date = $("#<%= toDate.ClientID%>").val();

        if (!checkDateFromTo(strStart_date, strEnd_Date)) return;

        document.getElementById('Panel1').style.display = 'block';

        document.getElementById('selectDateArea').style.display = 'none';
        document.getElementById('dataPanel').style.display = 'none';
    }


    function toggleChecked(status) {
        $("#<%=cblKeyword.ClientID%> input").each(function() {
            $(this).attr("checked", status);
        })
    }
        
</script>

<style type="text/css">
	
table.keywordListTable
{
    width: 100%;
    table-layout: fixed;
    border-style: none;
    border-collapse: collapse;
    margin: 5px 0 0 0;
}

table.keywordListTable td
{
    margin: 0px;
    padding: 4px;
    color: #555555;
    border: 1px solid #c6c6c6;
    overflow:auto;
}

</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
<colgroup>
<col width="auto" />
<col width="50%" />
</colgroup>
<tr>
    <td><span class="subTitle"><span id=""><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span></span> </td>
</tr>
</table>

<div class='topOptionBtns'>
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources: Resource, btn_0007 %>" OnClick="btnSelect_Click" OnClientClick="return DoSelect()" /> <!-- 조회 -->
    <asp:Button ID="btnExcel" runat="server" Text="<%$ Resources: Resource, btn_0006 %>" OnClick="btnExcel_Click" Visible="false"  /> <!-- 엑셀저장 -->
</div>    

<table class="boardInputType mb10" cellpadding="0" cellspacing="0" border="0">
	<colgroup>		
	<col width="150px" />
	<col width="250px" />
	<col width="150px" />
	<col width="auto" />	
</colgroup>
<tr>
   <th><span>기간</span></th>
    <td>        
      <asp:TextBox ID="fromDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />
        ~
      <asp:TextBox ID="toDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />                   
    </td>   
    <th><span>조직</span></th>  
    <td> 
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>          
        <asp:DropDownList ID="ddlType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
        <asp:DropDownList ID="ddlClass1" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass1_SelectedIndexChanged" />
        <asp:DropDownList ID="ddlClass2" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass2_SelectedIndexChanged" />
        <asp:DropDownList ID="ddlClass3" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass3_SelectedIndexChanged" />
        <asp:DropDownList ID="ddlClass4" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" />
    </ContentTemplate>
    </asp:UpdatePanel>            
    </td>                     
</tr>
<tr>
   <th><span>메뉴</span></th>
    <td colspan=3>     
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode=Conditional>       
        <ContentTemplate>                      
            <asp:DropDownList ID="ddlKeywordType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlKeywordType_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlKeyword1" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlKeyword1_SelectedIndexChanged" Visible=false  />            
            <asp:DropDownList ID="ddlKeyword2" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlKeyword2_SelectedIndexChanged" Visible=false  />            
            <asp:DropDownList ID="ddlKeyword3" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlKeyword3_SelectedIndexChanged" Visible=false  />            
            <asp:DropDownList ID="ddlKeyword4" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlKeyword4_SelectedIndexChanged" Visible=false  />            
            <input type="checkbox" id="chkAll" onclick="toggleChecked(this.checked);" checked="checked"  /><label for="chkAll" ><%= Resources.Resource.txt_0146 %><!-- 전체 선택/해제 --></lable>            
            <asp:CheckBoxList ID="cblKeyword" runat="server" DataTextField="name" DataValueField="code" RepeatDirection="Horizontal" RepeatColumns=5 CssClass="keywordListTable"></asp:CheckBoxList>                        
       </ContentTemplate>
        </asp:UpdatePanel> 
    </td>                           
</tr>
</table>

<div id="Panel1" style="display: none;">
   
    <table style="width:100%;height:290px;">
    <tr>
        <td style="text-align:center">
        <asp:Image ID="Image2" runat="server"  ImageUrl="~/Asset/Images/loading.gif" />
        <br />
        Processing.... please wait...
        
        </td>
    </tr>
    </table>

</div>

<div id="selectDateArea" >
    <span  style="font-weight: bold"><%= Resources.Resource.txt_0049%> : </span>
    <asp:Label ID="selectDate" runat="server" Text=""></asp:Label>
</div>

<div id="dataPanel" class="container" style="display: block;">    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode=Conditional>
    <ContentTemplate>
    <asp:GridView ID="gvList" runat="server"  OnPreRender="gvList_PreRender" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="true" ShowHeader="TRUE" CssClass="boardListType1">
    <EmptyDataTemplate>
        <table style="width:100%;height:290px;">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    
    </asp:GridView>    
          
    </ContentTemplate>
    </asp:UpdatePanel>
 
</div>

<div><asp:Label ID="debug" runat="server" Text=""></asp:Label></div>
 
</asp:Content>