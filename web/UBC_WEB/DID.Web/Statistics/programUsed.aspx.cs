﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;

public partial class programUsed : BasePage
{
    


    #region 그래프 관련 변수들

    protected string allCount = "0";
    protected string arrayTitle = "";
    protected string arrayString = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0013; // 패키지 사용 통계

        if (!this.IsPostBack)
        {
            this.InitControl();
        }
    }

    //초기화
    private void InitControl()
    {
        // fromdate 를 -7일 로 세팅한다.
        fromDate.Text = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");

        // todate 를 오늘날짜로 세팅한다.
        toDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        // 조직콤보
        Common.BindDropDownList(ddlType, GetOrgCode(Profile.COMP_TYPE), Resources.Resource.txt_0030/*전체*/);
    }

    //엑셀
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        string fileName = this.Title + ".xls";
        fileName = fileName.Replace(' ', '_');

        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
        Response.Charset = "";
        this.EnableViewState = false;

        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n");

        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
        gvList.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    // 엑셀 저장을 위해 재정의해야함 (지우면 에러남)
    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time. 
    }


    // 조회
    protected void btnSelect_Click(object sender, EventArgs e)
    {
         
        using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
        {
            // 기간
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            // 선택된 SITE ID
            string siteId = (hiddenSelectedSiteId.Value.IsNullOrEmpty()) ? Profile.COMP_TYPE : hiddenSelectedSiteId.Value;

            DataTable dt = obj.GetProgramUsedList(Profile.COMP_TYPE, fromDateVar, toDateVar, siteId, programId.Text);
            this.gvList.DataSource = dt;
            this.gvList.DataBind();


             // 데이타가 있으면 그래프 그려줌
            if ( dt.Rows.Count > 0 )
            {
                 // 엑셀저장 버튼 활성
                btnExcel.Enabled = true;
                btnExcel.Visible = true;

                //  그래프 그려줌
                dt = obj.GetProgramUsedGraph(Profile.COMP_TYPE, fromDateVar, toDateVar, siteId, programId.Text);
                
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    arrayString = arrayString +
                     "data.setValue(" + i + " , 0, '" + dt.Rows[i]["programName"] + /*" (" + dt.Rows[i]["programId_count"] + ")" +*/ "'); " +
                     "data.setValue(" + i + " , 1, " + dt.Rows[i]["programId_count"] + "); ";                    
                }

                allCount = dt.Rows.Count.ToString();
                arrayTitle = Resources.Resource.txt_0137 + "(TOP10)"; // 패키지별 사용건수(TOP10)
            }
            else
            {
                // 엑셀저장 버튼 비활성
                btnExcel.Enabled = false;
                btnExcel.Visible = false;
            }

            // 그래프 데이타가 있으면
            if (dt.Rows.Count > 0)
                PanelGraph.Visible = true;
            else
                PanelGraph.Visible = false;
        }
    }


    #region 그리드뷰 gvList_RowDataBound()
    /// <summary>
    /// 그리드뷰 gvList_RowDataBound
    /// </summary>
    protected void gvList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        DataRowView drv = (DataRowView)e.Row.DataItem;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }
    }
    #endregion


    #region SelectedIndexChanged 이벤트 핸들러
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass1.Visible = false;
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;

        ddlClass1.Items.Clear();
        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();

        if (!ddlType.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlType.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass1.Visible = true;
                Common.BindDropDownList(ddlClass1, dt, Resources.Resource.txt_0030/*전체*/);
            }
        }

        // 조회에서 사용될 선택될 조직코드 셋팅
        hiddenSelectedSiteId.Value = ddlType.SelectedValue;

    }
    protected void ddlClass1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass2.Visible = false;
        ddlClass3.Visible = false;

        ddlClass2.Items.Clear();
        ddlClass3.Items.Clear();

        if (!ddlClass1.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass1.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass2.Visible = true;
                Common.BindDropDownList(ddlClass2, dt, Resources.Resource.txt_0030/*전체*/);
            }

            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteId.Value = ddlClass1.SelectedValue;
        }
        else
        {
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteId.Value = ddlType.SelectedValue;
        }

    }
    protected void ddlClass2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlClass3.Visible = false;

        ddlClass3.Items.Clear();

        if (!ddlClass2.SelectedValue.IsNullOrEmpty())
        {
            DataTable dt = GetOrgCode(ddlClass2.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                ddlClass3.Visible = true;
                Common.BindDropDownList(ddlClass3, dt, Resources.Resource.txt_0030/*전체*/);
            }

            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteId.Value = ddlClass2.SelectedValue;
        }
        else
        {
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteId.Value = ddlClass1.SelectedValue;
        }

    }

    protected void ddlClass3_SelectedIndexChanged(object sender, EventArgs e)
    {
        // 조회에서 사용될 선택될 조직코드 셋팅
        if (!ddlClass3.SelectedValue.IsNullOrEmpty())
        {
            // 조회에서 사용될 선택될 조직코드 셋팅
            hiddenSelectedSiteId.Value = ddlClass3.SelectedValue;
        }
        else
        {
            // 전체 선택시 상위 콤보 선택값 셋팅
            hiddenSelectedSiteId.Value = ddlClass2.SelectedValue;

        }
    }
    #endregion


    #region 데이터 조회


    private DataTable GetOrgCode(string parentId)
    {

        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {
            return obj.GetOrgCode(parentId, Profile.COMP_TYPE);
        }
    }
    #endregion


    // 그리드뷰 셀병합
    protected void gvList_PreRender(object sender, EventArgs e)
    {

        Common.GroupColumn(gvList, 0); //그리드뷰의 ID, 병합할 컬럼

        Common.GroupColumn(gvList, 1); //그리드뷰의 ID, 병합할 컬럼

    }


}

