﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using DID.Common.Framework;

public partial class interActiveDetail : BasePage
{
    protected string subTitle;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.txt_0079; // 메뉴별 상세내역

        subTitle = string.Format("[{0}] {1}", Request["siteName"], this.Title);

        this.InitControl();
    }

    private void InitControl()
    {
        using (DID.Service.Statistics.Statistics obj = new DID.Service.Statistics.Statistics())
        {
            DataTable dt = obj.GetInterActiveList(Profile.COMP_TYPE, Request["fromDate"], Request["toDate"], Request["siteId"], "", "3");
            this.gvList.DataSource = dt;
            this.gvList.DataBind();
        }
    }


}



