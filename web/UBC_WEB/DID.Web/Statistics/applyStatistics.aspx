﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="applyStatistics.aspx.cs" Inherits="applyStatistics" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


<!-- 구글 파이챠트 시작-->

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load('visualization', '1', { packages: ['corechart'] });
</script>
<script type="text/javascript">      
    
    function drawVisualization() 
    {
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', '항목');
        data.addColumn('number', '갯수');
        data.addRows(<%=this.allCount %>);              
              
        <%= this.arrayString %>             
                     
        // 차트 데이타 형식
        // data.setValue(0, 0, '항목1');
        // data.setValue(0, 1, 갯수); 
        // data.setValue(1, 0, 'host_process_down');
        // data.setValue(1, 1, 2); 
        // data.setValue(2, 0, 'host_hdd_overload');
        // data.setValue(2, 1, 2);
        // 참고 : http://code.google.com/apis/ajax/playground/                      

        // Create and draw the visualization.
        new google.visualization.PieChart(document.getElementById('visualization')).draw(data, {title:"<%=this.arrayTitle %>"});
    }   

    google.setOnLoadCallback(drawVisualization);
    
</script>

<!-- 구글 파이챠트 끝-->


<script language="javascript" type="text/javascript">

    function DoSelect() {

        //////////////////////
        // 날짜 유효성 검사 시작

        var startDate = $("#<%= fromDate.ClientID%>").val();
        var endDate = $("#<%= toDate.ClientID%>").val();

        // 시작날짜 문자열에서 년,월,일 을 뽑아 숫자로 변환
        var yearStart = parseInt(startDate.substr(0, 4), 10);
        var monthStart = parseInt(startDate.substr(5, 2), 10);
        var dayStart = parseInt(startDate.substr(8), 10);

        // 종료날짜 문자열에서 년,월,일 을 뽑아 숫자로 변환
        var yearEnd = parseInt(endDate.substr(0, 4), 10);
        var monthEnd = parseInt(endDate.substr(5, 2), 10);
        var dayEnd = parseInt(endDate.substr(8), 10);

        // 날짜 비교
        var dStart = new Date(yearStart, monthStart, dayStart);
        var dEnd = new Date(yearEnd, monthEnd, dayEnd);

        if (dStart > dEnd) {
            alert("<%= Resources.Resource.msg_0040 %>"); // 종료일자가 시작일자보다 앞설 수 없습니다
            return false;
        }

        // 날짜 유효성 검사 끝
        //////////////////////

        return true;
       
    }

      
</script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 

 <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
  <colgroup>
  <col width="auto" />
  <col width="50%" />
  </colgroup>
  <tr>
    <td><span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span> </td>
  </tr>
</table>
 
    <div class='topOptionBtns'>
        <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources: Resource, btn_0007 %>" OnClick="btnSelect_Click" OnClientClick='return DoSelect()' /> <!-- 조회 -->
        <asp:Button ID="btnExcel" runat="server" Text="<%$ Resources: Resource, btn_0006 %>" OnClick="btnExcel_Click" Visible=false  /> <!-- 엑셀저장 -->
    </div>    

    <table class="boardInputType mb10" cellpadding="0" cellspacing="0" border="0">
		<colgroup>		
		<col width="150px" />
		<col width="250px" />
		<col width="150px" />
		<col width="auto" />	
	</colgroup>
    <tr>
       <th><span><%= Resources.Resource.txt_0049 %><!-- 조회기간 --></span></th>
       <td>        
          <asp:TextBox ID="fromDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);" onkeydown="return false;" />
            ~
          <asp:TextBox ID="toDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  onkeydown="return false;"  />                    
        </td>  
        <th><span><%= Resources.Resource.txt_0003 %><!-- 조직명 --></span></th>  
        <td>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:DropDownList ID="ddlType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass1" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass1_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass2" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass2_SelectedIndexChanged" />

                <asp:DropDownList ID="ddlClass3" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass3_SelectedIndexChanged" />
                
                <asp:HiddenField ID="hiddenSelectedSiteId" runat="server" />
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>                     
    </tr>    
    <tr>
        <th><span><%= Resources.Resource.txt_0045 %><!-- 패키지명 --></span></th>
        <td colspan='3'>    
            <asp:TextBox ID="programId" runat="server"  CssClass="textType" ></asp:TextBox>
        </td>
    </tr>
    </table>
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
    
        <div class="container" >
       
        <asp:GridView ID="gvList" runat="server" Width="100%" OnPreRender="gvList_PreRender" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false" 
              ShowHeader="true" CssClass="boardListType3">
        
        <%-- GridView 컬럼  
            조회기간
            조직명
            패키지명
            등록방법
            사용건수
        --%>
        <Columns>          
            <asp:BoundField DataField="s_date" HeaderText="<%$ Resources: Resource, txt_0049 %>"  HeaderStyle-Width="170px" ItemStyle-VerticalAlign=top ItemStyle-HorizontalAlign=Center SortExpression="s_date" />            
            <asp:BoundField DataField="siteName" HeaderText="<%$ Resources: Resource, txt_0003 %>" HeaderStyle-Width="130px" ItemStyle-VerticalAlign=top ItemStyle-HorizontalAlign=Center SortExpression="siteName" />
            <asp:BoundField DataField="programId" HeaderText="<%$ Resources: Resource, txt_0045 %>" ItemStyle-HorizontalAlign=Left SortExpression="programId" />
            <asp:BoundField DataField="how" HeaderText="<%$ Resources: Resource, txt_0047 %>" HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign=Center SortExpression="how" />
            <asp:BoundField DataField="programId_count" HeaderText="<%$ Resources: Resource, txt_0073 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Right SortExpression="programId_count" />
        </Columns>
        <EmptyDataTemplate>
            <table style="width:100%;height:290px;">
                <tr>
                    <td style="text-align:center">
                    <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        
        </asp:GridView>   
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
     
    <!-- 그래프  -->
    <asp:Panel ID="PanelGraph" runat="server" Visible=false>
        <div id="visualization"  style="width: 820px; height: 400px;"></div>
    </asp:Panel>
     
</asp:Content>


