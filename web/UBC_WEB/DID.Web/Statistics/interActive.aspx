﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="interActive.aspx.cs" Inherits="interActive" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoShowDetail(siteId, siteName, fromDate, toDate) {

        window.open("./interActiveDetail.aspx?siteId=" + siteId.toString() + "&siteName=" + siteName.toString() + "&fromDate=" + fromDate.toString() + "&toDate=" + toDate.toString(), "interActiveDetail", "toolbar=0,scrollbars=yes,location=0,directories=0,status=0,menubar=0,resizable=no,width=800,height=400");
    }
    
    function DoSelect() {

        if (!confirm("<%= Resources.Resource.msg_0041 %>"))  // 검색 조건에 따라서 조회 시간이 오래 걸릴 수 있습니다.\n빠른 조회를 원하시면 검색 조건을 구체화 해주세요.\n계속 하시겠습니까?
            return false        
            
        // 날짜 유효성 검사 시작

        var strStart_date = $("#<%= fromDate.ClientID%>").val();
        var strEnd_Date = $("#<%= toDate.ClientID%>").val();

        if (!checkDateFromTo(strStart_date, strEnd_Date)) return;

        document.getElementById('Panel1').style.display = 'block';

        document.getElementById('dataPanel').style.display = 'none';  
    }

    $(document).ready(function() {
        $('.test').hide();
    });
    
    
</script>

	
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
<table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
<colgroup>
<col width="auto" />
<col width="50%" />
</colgroup>
<tr>
    <td><span class="subTitle"><span id=""><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span></span> </td>
</tr>
</table>

<div class='topOptionBtns'>
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources: Resource, btn_0007 %>" OnClick="btnSelect_Click" OnClientClick="return DoSelect()" /> <!-- 조회 -->
    <asp:Button ID="btnExcel" runat="server" Text="<%$ Resources: Resource, btn_0006 %>" OnClick="btnExcel_Click" Visible="false"  /> <!-- 엑셀저장 -->
</div>    

<table class="boardInputType mb10" cellpadding="0" cellspacing="0" border="0">
	<colgroup>		
	<col width="150px" />
	<col width="250px" />
	<col width="150px" />
	<col width="auto" />	
</colgroup>
<tr>
   <th><span><%= Resources.Resource.txt_0049 %><!-- 조회기간 --></span></th>
    <td>        
      <asp:TextBox ID="fromDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />
        ~
      <asp:TextBox ID="toDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  />                   
    </td>   
    <th><span><%= Resources.Resource.txt_0003 %><!-- 조직명 --></span></th>  
    <td>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode=Conditional>
        <ContentTemplate>            
            <asp:DropDownList ID="ddlType" runat="server" CssClass="selectType" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlClass1" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass1_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlClass2" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass2_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlClass3" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlClass3_SelectedIndexChanged" />
            <asp:DropDownList ID="ddlClass4" runat="server" CssClass="selectType" Visible="false" DataTextField="Name" DataValueField="Code" AutoPostBack="true" />
        </ContentTemplate>
        </asp:UpdatePanel>
    </td>                     
</tr>
</table>
    
<div ID="Panel1" style="display: none;">
   
    <table style="width:100%;height:290px;">
    <tr>
        <td style="text-align:center">
        <asp:Image ID="Image2" runat="server"  ImageUrl="~/Asset/Images/loading.gif" />
        <br />
        Processing.... please wait...
        
        </td>
    </tr>
    </table>

</div>  

<div id="dataPanel" class="container" style="display: block;">
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode=Conditional>
    <ContentTemplate>  
            
    <asp:GridView ID="gvList" runat="server"  OnPreRender="gvList_PreRender" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false" 
          ShowHeader="TRUE" CssClass="boardListType1">
          
      <%-- GridView 컬럼  
        조회기간
        조직명
        총 방문자수
        총 페이지뷰
    --%>  
    
    <Columns>
      
        <asp:BoundField DataField="s_date" HeaderText="<%$ Resources: Resource, txt_0049 %>" HeaderStyle-Width="170px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="s_date" />
        <asp:BoundField DataField="siteName" HeaderText="<%$ Resources: Resource, txt_0003 %>" HeaderStyle-Width="110px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="siteName" />            
        <asp:BoundField DataField="cnt_visit" HeaderText="<%$ Resources: Resource, txt_0076 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="cnt_visit" />
        <asp:BoundField DataField="cnt_view" HeaderText="<%$ Resources: Resource, txt_0077 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Center ItemStyle-VerticalAlign=Top SortExpression="cnt_view"   />            
        
        <asp:TemplateField HeaderText="<%$ Resources: Resource, txt_0078 %>">
        <ItemTemplate>

            <asp:GridView runat="server" ID="gvList2" OnRowDataBound="gvList2_RowDataBound"  AutoGenerateColumns="false" ShowHeader="true" CssClass="boardListType2">
            <Columns>
            
                <%-- GridView 컬럼  
                    조직명
                    총 방문자수
                    총 페이지뷰
                    메뉴별 상세내역
                --%>
                    
                <asp:BoundField DataField="siteName" HeaderText="<%$ Resources: Resource, txt_0003 %>" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign=Left ItemStyle-VerticalAlign=Middle SortExpression="siteName" />
                <asp:BoundField DataField="cnt_visit" HeaderText="<%$ Resources: Resource, txt_0076 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Right ItemStyle-VerticalAlign=Middle SortExpression="cnt_visit" />
                <asp:BoundField DataField="cnt_view" HeaderText="<%$ Resources: Resource, txt_0077 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Right ItemStyle-VerticalAlign=Middle SortExpression="cnt_view"   />
                                
                <asp:TemplateField HeaderText="<%$ Resources: Resource, txt_0079 %>" ItemStyle-HorizontalAlign="Center" >                
                <ItemTemplate>
                    <span style="cursor: pointer; border-bottom:solid 1px;" ><%= Resources.Resource.txt_0079 %></span>
                    <div class="test">                        
                        <asp:GridView ID="gvList3" runat="server" AutoGenerateColumns="false" ShowHeader="TRUE" CssClass="boardListType3">
                        <Columns>
                            <asp:BoundField DataField="category1" HeaderText="<%$ Resources: Resource, txt_0082 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category1" />
                            <asp:BoundField DataField="category2" HeaderText="<%$ Resources: Resource, txt_0083 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category2" />
                            <asp:BoundField DataField="category3" HeaderText="<%$ Resources: Resource, txt_0084 %>" HeaderStyle-HorizontalAlign=Center ItemStyle-HorizontalAlign=Left SortExpression="category3" />
                            <asp:BoundField DataField="touch_cnt" HeaderText="<%$ Resources: Resource, txt_0085 %>" HeaderStyle-HorizontalAlign=Center HeaderStyle-Width="60px" ItemStyle-HorizontalAlign=Right SortExpression="touch_cnt" />
                        </Columns>
                        <EmptyDataTemplate>
                            <table style="width:100%;">
                                <tr>
                                    <td style="text-align:center">
                                    <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        </asp:GridView>
                    </div>       
                        
                 </ItemTemplate>
                 </asp:TemplateField>
                
                 
            </Columns> 
            </asp:GridView>

        </ItemTemplate>            
        </asp:TemplateField>
         
    </Columns> 
    <EmptyDataTemplate>
        <table style="width:100%;height:290px;">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    
    </asp:GridView>
    
   </ContentTemplate>
   </asp:UpdatePanel>
      
</div>

</asp:Content>