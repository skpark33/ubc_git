﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Services;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Data;


using System.Globalization;
using System.Threading;

using DID.Common.Framework;

namespace WebService
{
    public class LeftMenu
    {
        public string menuName { get; set; }
        public string menuUrl { get; set; }
        public string menuDepth { get; set; }
        public string menuCode { get; set; }
        public string treeTopName { get; set; }
        
    }

    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any
        , IncludeExceptionDetailInFaults = true
        , ConcurrencyMode = ConcurrencyMode.Multiple
        , InstanceContextMode = InstanceContextMode.PerSession)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WebService : IWebService
    {

        public string GetLeftMenu(string compType, string largeCode , string langFlag)
        {

            if (langFlag != null)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(langFlag);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(langFlag);
            }           

            string results = "";
            string[] depth = new string[30];
            string[] menuNm = new string[30];
            string[] menuUrl = new string[30];
            string[] menuCd = new string[30];
            string   getTreeTopName = "";
            List<LeftMenu> list = new List<LeftMenu>();

            DataTable dtLargeName = null;
            DataTable dtMiddle = null;

            using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
            {                 
                dtLargeName = obj.GetMenuLargeName(compType, largeCode, langFlag);
                dtMiddle = obj.GetMenuMiddle(compType, largeCode, langFlag);

                //getTreeTopName = dtLargeName.Rows[0]["MENU_NAME"].ToString();   
                getTreeTopName = Resources.Resource.ResourceManager.GetString( dtLargeName.Rows[0]["MENU_NAME"].ToString() );

                int idx = 0;

                foreach (DataRow drMiddle in dtMiddle.Rows)
                {
                    if (dtMiddle.Rows.Count > 0)
                    {
                        ++idx;
                        depth[idx] = "2";
                        //menuNm[idx] = drMiddle["MENU_NAME"].ToString();
                        menuNm[idx] = Resources.Resource.ResourceManager.GetString( drMiddle["MENU_NAME"].ToString() );

                        menuUrl[idx] = drMiddle["PROG_ID"].IsNullOrEmpty() ? string.Empty : drMiddle["PROG_ID"].ToString().Replace("\\", "/") + ".aspx";
                        menuCd[idx] = drMiddle["PROG_ID"].ToString().Replace("\\", "/");

                    }
                    idx++;
                }
            }

            for (int i = 0; i < menuNm.Length; i++)
            {
                if (menuNm[i] != null)
                {
                    var resultValue = new LeftMenu
                    {
                        menuName = menuNm[i]
                        , menuUrl = menuUrl[i]
                        , menuDepth = depth[i]
                        , menuCode = menuCd[i]
                        , treeTopName = getTreeTopName
                    };
                    list.Add(resultValue);
                }
            }

            MemoryStream ms = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(list.GetType());

            serializer.WriteObject(ms, list);
            results = Encoding.UTF8.GetString(ms.ToArray());

            return results;
        }

    }

}
