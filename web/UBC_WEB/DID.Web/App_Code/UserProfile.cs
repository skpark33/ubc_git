﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;


/// <summary>
/// 사용자 정보
/// </summary>
public class UserProile
{
    private ProfileCommon profileCommon = null;

    public string USER_ID
    {
        get { return profileCommon.USER_ID; }
    }

    public string USER_NAME
    {
        get { return profileCommon.USER_NAME; }
    }

    public string DEPT_CODE
    {
        get { return profileCommon.DEPT_CODE; }
    }

    public string DEPT_NAME
    {
        get { return profileCommon.DEPT_NAME; }
    }

    public string USER_TYPE
    {
        get { return profileCommon.USER_TYPE; }
    }

    public string LANG_FLAG
    {
        get { return profileCommon.LANG_FLAG; }
    }

    public UserProile()
    {
        profileCommon = new ProfileCommon();
    }

    private object GetUserInfo(string key)
    {
        return (string.IsNullOrEmpty(key)) ? string.Empty : HttpContext.Current.Request.Cookies["UserInfo"] != null ? HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request.Cookies["UserInfo"][key.ToUpper()]) : string.Empty;
    }

    public void AbandonUserInfo()
    {
        FormsAuthentication.SignOut();

        HttpContext.Current.Response.Cookies["UserInfo"].Value = null;
        HttpContext.Current.Response.Cookies["UserInfo"].Expires = new System.DateTime(1999, 1, 1);
    }

}
