﻿using System;
using System.Data;


public class PagedTable
{
    /// <summary>
    /// 현재 페이지 데이터테이블
    /// </summary>
    public DataTable Data;

    /// <summary>
    /// 페이지 당 데이터 건수
    /// </summary>
    public int PageSize;

    /// <summary>
    /// 전체 데이터 건수
    /// </summary>
    public int TotalCount;

    /// <summary>
    /// 현재 페이지 번호
    /// </summary>
    private int _currentPage;

    /// <summary>
    /// 클래스 기본 생성자
    /// </summary>
    public PagedTable()
    {
    }

    /// <summary>
    /// 전체 페이지수
    /// </summary>
    public int PageCount
    {
        get
        {
            return (int)((TotalCount - 1) / PageSize) + 1;
        }
    }

    /// <summary>
    /// 현재 페이지 번호
    /// </summary>
    public int CurrentPage
    {
        get
        {
            return (_currentPage < 1) ? 1 : ((_currentPage > PageCount) ? PageCount : _currentPage);
        }
        set
        {
            _currentPage = value;
        }
    }
}
