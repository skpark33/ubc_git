﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;

/// <summary>
/// Util의 요약 설명입니다.
/// </summary>
public static class Util
{
    public static String URL_ERROR_ROLE = "/";
    public static String ReplaceSpecialChar(String str)
    {
        if (str == null || str.Length <= 0)
            return str;

        String strRet = str.Replace("＂", "\"");
        strRet = strRet.Replace("＇", "'");
        strRet = strRet.Replace("？", "?");
        strRet = strRet.Replace("“", "\"");
        strRet = strRet.Replace("”", "\"");
        strRet = strRet.Replace("‘", "'");
        strRet = strRet.Replace("’", "'");

        return strRet;
    }


    public static String DB2HTML(String str)
    {
        if (str == null || str.Length <= 0)
            return str;

        String strRet = str.Replace("&" , "&amp;");
        strRet = strRet.Replace("<", "&lt;");
        strRet = strRet.Replace(">", "&gt;");
        strRet = strRet.Replace("'", "&#39;");
        strRet = strRet.Replace("\"", "&#34;");

        return strRet;
    }

    public static String HTML2DB(String str)
    {
        if (str == null || str.Length <= 0)
            return str;

        String strRet = str.Replace("'", "''");
        strRet = strRet.Replace("&lt;", "<");
        strRet = strRet.Replace("&gt;", ">");

        return strRet;
    }

    public static Boolean CheckXSS(string str_Regex)
    {
        string return_Val  = str_Regex.ToLower().Trim();
        Regex regex = new Regex(@"</(?!a|br|blockquote|span|img|p|table|tbody|tr|td|ol|li|div|ul)[^>]*>");

        return regex.IsMatch(return_Val);
    }
}
