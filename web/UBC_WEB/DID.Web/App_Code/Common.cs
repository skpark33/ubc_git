﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;

using Brettle.Web.NeatUpload;

/// <summary>
/// Common의 요약 설명입니다.
/// </summary>
public static class Common
{
    /// <summary>
    /// 게시판 종류
    /// </summary>
    public enum BoardMasterID
    {
        Notice = 1
        , Program = 2
        , Manual = 3
        , Video = 4
        , Contents = 5
        // 2011.06.15 임유석 시작 - Pop 게시판 ID
        , PopNews = 6
        , PopDisplay = 7
        // 2011.06.15 임유석 끝
    }
    /// <summary>
    /// 게시판 쓰기모드
    /// </summary>
    public enum BoardMode
    {
        Write = 1
        , Update = 2
        , Reply =3
    }

    /// <summary>
    /// 긴 문자열 처리
    /// </summary>
    /// <param name="width">문자열 폭</param>
    /// <param name="htmlText">원본 문자열</param>
    /// <returns>결과 문자열</returns>
    public static string EllipsisText(int width, object htmlText)
    {

        return string.Format("<div style=\"width:{0}px;overflow:hidden;text-overflow:ellipsis;\" title=\"{2}\" ><nobr>{1}</nobr></div>"
                            , width
                            , htmlText
                            , htmlText);
    }

    /// <summary>
    /// Base64 인코딩
    /// </summary>
    public static string Base64Encode(string src)
    {
        byte[] arr = new System.Text.UTF8Encoding().GetBytes(src);
        return Convert.ToBase64String(arr);
    }
    /// <summary>
    /// Base64 디코딩
    /// </summary>
    public static string Base64Decode(string strData)
    {
        byte[] bteData = Convert.FromBase64String(strData);
        return System.Text.Encoding.UTF8.GetString(bteData);
    }

    /// <summary>
    /// 데이터테이블 페이징 처리
    /// </summary>
    static public PagedTable GetPagedData(int pageSize, int pageNo, DataView dvData)
    {
        if (pageNo < 1)
            pageNo = 1;

        int TotalCount = 0;                            // 전체 데이터 건수
        int FirstIndex = pageSize * (pageNo - 1);     // 데이터 조회범위 시작 인덱스
        int LastIndex = FirstIndex + pageSize;      // 데이터 조회범위 종료 인덱스

        DataTable dtPaged = dvData.Table.Clone();

        for (int i = 0; i < dvData.Count; i++)
        {
            if (TotalCount >= FirstIndex && TotalCount < LastIndex)
            {
                dtPaged.Rows.Add(dvData[i].Row.ItemArray);
            }

            TotalCount++;
        }

        PagedTable pagedTable = new PagedTable();
        pagedTable.PageSize = pageSize;
        pagedTable.TotalCount = TotalCount;
        pagedTable.CurrentPage = pageNo;
        pagedTable.Data = dtPaged;

        return pagedTable;
    }

    /// <summary>
    /// 들여쓰기
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public static string SetReplyPosition(int depth)
    {
        int pos = depth;				// 들여쓰기 값
        string returnValue = "&nbsp;";														// 기본 들여쓰기 한칸 

        if (pos > 0)
        {
            while (pos > 0)
            {
                returnValue += "&nbsp;&nbsp;&nbsp;";
                pos--;
            }
            returnValue = string.Format("{0}<IMG SRC='/images/community/reply_list.gif' align='AbsMiddle' /> ", returnValue);
        }

        return returnValue;
    }

    public static string GetNewIcon(object createDate)
    {
        // 24시간 이내에 올라온 글은 글 제목 뒤쪽에 이미지를 달아준다.
        DateTime orginDate = (DateTime)createDate;

        TimeSpan gap = DateTime.Now - orginDate;

        string img = string.Empty;

        if (gap.TotalMinutes < 1440)
        {
            img = " <img src=/Asset/Images/board/lastest.gif />";
        }

        return img;
    }


    /// <summary>
    /// 첨부파일 업로드
    /// </summary>
    /// <param name="objFile"></param>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static string UploadFile(FileUpload objFile, string filePath)
    {
        string FileName = string.Empty;

        if (objFile.PostedFile != null && objFile.PostedFile.ContentLength > 0)
        {
            DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
            if (impersonater.ImpersonateValidUser())
            {

                string strSavePathRealName = string.Empty;
                string strFileName2 = string.Empty;
                string strSavePath = string.Empty;
                FileName = Path.GetFileName(objFile.PostedFile.FileName);
                strSavePath = HttpContext.Current.Server.MapPath(filePath) + FileName;

                if (!Directory.Exists(HttpContext.Current.Server.MapPath(filePath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(filePath));
                }

                int i = 0;
                while (File.Exists(strSavePath))
                {
                    i++;
                    strFileName2 = Path.GetFileNameWithoutExtension(FileName)
                        + "(" + i.ToString() + ")"
                        + Path.GetExtension(FileName).ToLower();
                    strSavePath = HttpContext.Current.Server.MapPath(filePath) + strFileName2;
                }

                objFile.PostedFile.SaveAs(strSavePath);
                if (strFileName2 != "")
                    FileName = strFileName2;

                impersonater.UndoImpersonation();

            }

        }

        return FileName;
    }

    /// <summary>
    /// 파일삭제
    /// </summary>
    /// <param name="virtualPath"></param>
    /// <param name="fileName"></param>
    public static void DeleteFile(string virtualPath, string fileName)
    {
        DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
        if (impersonater.ImpersonateValidUser())
        {
            fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(virtualPath), fileName);

            if (System.IO.File.Exists(fileName))
            {
                System.IO.File.Delete(fileName);
            }

            impersonater.UndoImpersonation();
        }
    }

    public static bool CheckAuth()
    {
        ProfileCommon ProfileCommon = HttpContext.Current.Profile as ProfileCommon;

        //개발자전용
        if(ProfileCommon.USER_TYPE.Equals("-1"))
            return true;
        else
        {
            if (ProfileCommon.ENTRY_POINT.Equals("W"))
                return false;
            else
            {
                if (ProfileCommon.USER_TYPE.Equals("1") || ProfileCommon.USER_TYPE.Equals("2"))
                    return true;
                else
                    return false;
            }
        }
    }


    public static string SetScriptBtn(String btnString)
    {
        string msg = "";
        string[] arrString = btnString.Split(new char[] { '#' });

        for (int i = 0; i < arrString.Length; i++)
        {
            switch (arrString[i])
            {
                case "READ":
                    msg = msg + "<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoQuery();return false;'>"
                        + Resources.Resource.btn_0015 + "</a></span></span> "; //보기
                    break;
                case "LIST":
                    msg = msg + "<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoList();return false;'>"
                        + Resources.Resource.btn_0013 + "</a></span></span> "; //목록
                    break;
                case "SAVE":
                    if (CheckAuth())
                        msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoSave();return false;'>"
                            + Resources.Resource.btn_0004 + "</a></span></span> ";//저장
                    break;
                case "SEARCH":
                        msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoSearch();return false;'>"
                            + Resources.Resource.btn_0014 + "</a></span></span> ";//검색
                    break;
                case "REG":
                    if (CheckAuth())
                        msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoReg();return false;'>"
                            + Resources.Resource.btn_0003 + "</a></span></span> ";//등록
                    break;
                case "DEL":
                    if (CheckAuth())
                        msg = msg + "<span class='btnType btnDelete'><span><a href='javascript:#' onclick='javascript:DoDelete();return false;'>"
                            + Resources.Resource.btn_0001 + "</a></span></span> ";//삭제
                    break;
                case "ADD":
                    if (CheckAuth())
                        msg = msg + "<span class='btnType btnPlusLine'><span><a href='javascript:#' onclick='javascript:lineInsert();return false;'>"
                        + "행추가" + "</a></span></span>  ";//행추가
                    break;
                case "UPD":
                    if (CheckAuth())
                        msg = msg + "<span class='btnType btnModify'><span><a href='javascript:#' onclick='javascript:DoUpdate();return false;'>"
                            + Resources.Resource.btn_0000 + "</a></span></span> ";//수정
                    break;
                case "INIT":
                    if (CheckAuth())
                        msg = msg + "<span class='btnType btnNew'><span><a href='javascript:#' onclick='javascript:DoInit();return false;'>"
                            + Resources.Resource.btn_0016 + "</a></span></span> ";//신규
                    break;
                case "CANCEL":
                        msg = msg + "<span class='btnType btnCancel'><span><a href='javascript:#' onclick='javascript:DoSaveCancel();return false;'>"
                            + Resources.Resource.btn_0005 + "</a></span></span> ";//취소
                    break;
            }
        }

        return msg;
    }

    #region 권한체크없는 스크립트 버튼 만들기
    // 2011-07-16 임유석 추가
    public static string SetScriptBtn_nonAuth(String btnString)
    {
        string msg = "";
        string[] arrString = btnString.Split(new char[] { '#' });

        for (int i = 0; i < arrString.Length; i++)
        {
            switch (arrString[i])
            {
                case "READ":
                    msg = msg + "<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoQuery();return false;'>"
                        + Resources.Resource.btn_0015 + "</a></span></span> "; //보기
                    break;
                case "LIST":
                    msg = msg + "<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoList();return false;'>"
                        + Resources.Resource.btn_0013 + "</a></span></span> "; //목록
                    break;
                case "SAVE":

                    msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoSave();return false;'>"
                        + Resources.Resource.btn_0004 + "</a></span></span> ";//저장
                    break;
                case "SEARCH":
                    msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoSearch();return false;'>"
                        + Resources.Resource.btn_0014 + "</a></span></span> ";//검색
                    break;
                case "REG":
                    msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoReg();return false;'>"
                        + Resources.Resource.btn_0003 + "</a></span></span> ";//등록
                    break;
                case "DEL":
                    msg = msg + "<span class='btnType btnDelete'><span><a href='javascript:#' onclick='javascript:DoDelete();return false;'>"
                        + Resources.Resource.btn_0001 + "</a></span></span> ";//삭제
                    break;
                case "ADD":
                    msg = msg + "<span class='btnType btnPlusLine'><span><a href='javascript:#' onclick='javascript:lineInsert();return false;'>"
                    + "행추가" + "</a></span></span>  ";//행추가
                    break;
                case "UPD":
                    msg = msg + "<span class='btnType btnModify'><span><a href='javascript:#' onclick='javascript:DoUpdate();return false;'>"
                        + Resources.Resource.btn_0000 + "</a></span></span> ";//수정
                    break;
                case "INIT":
                    msg = msg + "<span class='btnType btnNew'><span><a href='javascript:#' onclick='javascript:DoInit();return false;'>"
                        + Resources.Resource.btn_0016 + "</a></span></span> ";//신규
                    break;
                case "CANCEL":
                    msg = msg + "<span class='btnType btnCancel'><span><a href='javascript:#' onclick='javascript:DoSaveCancel();return false;'>"
                        + Resources.Resource.btn_0005 + "</a></span></span> ";//취소
                    break;
            }
        }

        return msg;
    }

    #endregion


    // 버튼 세팅 
    // 사용법 : <%=SetScriptBtn("LIST#SAVE#DEL#ADD#ExcelDown#PRINT#RELOAD","그리드 아이디")%> 
    public static string SetScriptBtn(String btnString, String wiseGridId)
    {
        
        string userID = new DID.Common.Framework.UserInfoContext().USER_ID;
        string pageName = HttpContext.Current.Request.Url.AbsolutePath;
        pageName = pageName.Substring(1, pageName.LastIndexOf(".")-1).Replace("/","\\");

        string msg = "";
        string[] arrString = btnString.Split(new char[] { '#' });

        using (DID.Service.Utility.MenuAuth obj = new DID.Service.Utility.MenuAuth())
        {
            DataTable dt = obj.GetMenuAuthUser(userID, pageName);

            if (dt.Rows.Count == 0)
                return "<span>&nbsp;</span>";

            DataRow dr = dt.Rows[0];
             
            for (int i = 0; i < arrString.Length; i++)
            {
                switch (arrString[i])
                {
                    case "LIST":
                        if(dr["GRANT_USE"].Equals("Y"))
                            msg = msg + "<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoQuery(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00410") + "</a></span></span> "; //조회 
                        
                        break;
                    case "SAVE":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoSave(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00411") + "</a></span></span> ";//저장
                        break;
                    case "SEARCH":
                        if (dr["GRANT_USE"].Equals("Y"))
                            msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoSearch(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00412") + "</a></span></span> ";//검색
                        break;
                    case "REG":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoReg(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00413") + "</a></span></span> ";//등록
                        break;
                    case "DEL":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnDelete'><span><a href='javascript:#' onclick='javascript:DoDelete(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00414") + "</a></span></span> ";//삭제
                        break;
                    case "ADD":
                        msg = msg + "<span class='btnType btnPlusLine'><span><a href='javascript:#' onclick='javascript:lineInsert(" + wiseGridId + ");return false;'>"
                            + GetGResource("LBL_00415") + "</a></span></span>  ";//행추가
                        break;
                    case "DTL":
                        if (dr["GRANT_USE"].Equals("Y"))
                            msg = msg + "<span class='btnType btnDetailInfo' ><span><a href='javascript:#' onclick='javascript:DoDetailQuery(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00416") + "</a></span></span> ";//상세정보
                        break;
                    case "UPD":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnModify'><span><a href='javascript:#' onclick='javascript:DoUpdate(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00417") + "</a></span></span> ";//수정
                        break;
                    case "PROC":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoProc(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00418") + "</a></span></span> ";//처리
                        break;
                    case "INIT":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnNew'><span><a href='javascript:#' onclick='javascript:DoInit(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00419") + "</a></span></span> ";//신규
                        break;
                    case "ExcelDown":
                        if (dr["GRANT_OUT"].Equals("Y"))
                            msg = msg + "<span class='btnType btnDownload'><span><a href='javascript:#' onclick='javascript:excelDown(" + wiseGridId + ");return false;'>Excel</a></span></span> ";
                        break;
                    case "PRINT":
                        if (dr["GRANT_OUT"].Equals("Y"))
                            msg = msg + "<span class='btnType btnPrint'><span><a href='javascript:#' onclick='javascript:DoPrint(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00420") + "</a></span></span> ";//인쇄
                        break;
                    case "CANCEL":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnCancel'><span><a href='javascript:#' onclick='javascript:DoSaveCancel(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00421") + "</a></span></span> ";//취소
                        break;
                    case "RELOAD":
                        msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:document.location.reload();return false;'>"
                            + GetGResource("LBL_00422") + "</a></span></span> ";//새로고침
                        break;
                    case "MOVECONFIRM":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:goConfirm();return false;'>"
                                + GetGResource("LBL_00423") + "</a></span></span> ";//이동확인
                        break;
                    case "MOVECANCEL":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnDelete'><span><a href='javascript:#' onclick='javascript:goCancel();return false;'>"
                                + GetGResource("LBL_00424") + "</a></span></span> ";//확인취소
                        break;
                    case "WRITE":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnPlusLine'><span><a href='javascript:#' onclick='javascript:lineInsert(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00425") + "</a></span></span>  ";//글쓰기
                        break;
                    case "REPORTLIST":
                        msg = msg + "<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoList(" + wiseGridId + ");return false;'>"
                            + GetGResource("LBL_00426") + "</a></span></span> ";//목록
                        break;
                    case "REPLY":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnPlusLine'><span><a href='javascript:#' onclick='javascript:replyInsert(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00427") + "</a></span></span>  ";//답변
                        break;
                    case "PROCESS":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:DoProcess(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00428") + "</a></span></span> ";//처리
                        break;
                    case "MONTHCONFIRM":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnRegister'><span><a href='javascript:#' onclick='javascript:goConfirm();return false;'>"
                                + GetGResource("LBL_00429") + "</a></span></span> ";//확정
                        break;
                    case "MONTHCANCEL":
                        if (dr["GRANT_CRUD"].Equals("Y"))
                            msg = msg + "<span class='btnType btnDelete'><span><a href='javascript:#' onclick='javascript:goCancel();return false;'>"
                                + GetGResource("LBL_00430") + "</a></span></span> ";//확정취소
                        break;
                    case "STOCKSEARCH":
                        if (dr["GRANT_USE"].Equals("Y"))
                            msg = msg + "<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoStockQuery(" + wiseGridId + ");return false;'>"
                            + GetGResource("LBL_00431") + "</a></span></span> ";//재고검색
                        break;
                    case "FARMSEARCH":
                        if (dr["GRANT_USE"].Equals("Y"))
                            msg = msg + "<span class='btnType btnSearch'><span><a href='javascript:#' onclick='javascript:DoStockQuery(" + wiseGridId + ");return false;'>"
                                + GetGResource("LBL_00432") + "</a></span></span> ";//계약자검색
                        break;
                }
            }
        }

        return msg.Equals("") ? "<span>&nbsp;</span>" : msg;
    }

    /// <summary>
    /// Global Resource 조회
    /// </summary>
    public static string GetGResource(string key)
    {
        return Resources.didResource.ResourceManager.GetString(key);
    }

    /// <summary>
    /// 공통코드데이터를 DropDownList 컨트롤에 바인딩
    /// </summary>
    public static void BindDropDownList(DropDownList ddlObj, DataTable dt, string title)
    {
        if (!string.IsNullOrEmpty(title))
        {
            DataRow dr = dt.NewRow();
            dr[0] = "";
            dr[1] = title;

            dt.Rows.InsertAt(dr, 0);
        }

        ddlObj.Items.Clear();
        ddlObj.DataSource = dt;
        ddlObj.DataBind();
    }

        /// <summary>
    /// WiseGrid의 콤보컨트롤 바인딩 스크립트 문자열 리턴
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="columnName"></param>
    /// <returns></returns>
    public static string AddComboListValue(string gridName, DataTable dt, string columnName, string title)
    {
        string comboList = string.Empty;

        if (!string.IsNullOrEmpty(title))
        {
            comboList += string.Format("{0}.AddComboListValue('{1}', '{2}', '{3}');\r\n"
                                    , gridName
                                    , columnName
                                    , title
                                    , string.Empty);
        }

        foreach (DataRow dr in dt.Rows)
        {
            comboList += string.Format("{0}.AddComboListValue('{1}', '{2}', '{3}');\r\n"
                                    , gridName
                                    , columnName
                                    , dr[1]
                                    , dr[0]);
        }

        return comboList;
    }

    /// <summary>
    /// WiseGrid의 콤보컨트롤 바인딩 스크립트 문자열 리턴
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="columnName"></param>
    /// <returns></returns>
    public static string AddComboListValue(DataTable dt, string columnName , string title)
    {
        string comboList = string.Empty;

        if (!string.IsNullOrEmpty(title))
        {
            comboList += string.Format("GridObj.AddComboListValue('{0}', '{1}', '{2}');\r\n"
                                    , columnName
                                    , title
                                    , string.Empty);
        }

        foreach (DataRow dr in dt.Rows)
        {
            comboList += string.Format("GridObj.AddComboListValue('{0}', '{1}', '{2}');\r\n"
                                    , columnName
                                    , dr[1]
                                    , dr[0]);
        }

        return comboList;
    }

    public static string ToDateString(object date)
    {

        if (date.ToString().Equals(string.Empty) || date.ToString().Length != 8)
            return string.Empty;

        return string.Format("{0}-{1}-{2}", date.ToString().Substring(0, 4), date.ToString().Substring(4, 2), date.ToString().Substring(6));
    }

    public static string GetFileFullPath(string rootPath, string subPath, string fileName)
    {
        if(!string.IsNullOrEmpty(subPath))
            return string.Format("{0}/{1}/{2}", rootPath, subPath.ToString().Replace("\\", "/"), fileName);
        else
            return string.Format("{0}/{1}", rootPath, fileName);

    }

    public static string GetFileAndPath(string subPath, string fileName)
    {
        if (!string.IsNullOrEmpty(subPath))
            return string.Format("{0}/{1}", subPath.ToString().Replace("\\", "/"), fileName);
        else
            return string.Format("{0}", fileName);

    }
    /// <summary>
    /// 물리적 파일 삭제
    /// </summary>
    public static void DeleteFile(DataTable dt, int boardMasterId)
    {
        string path = Common.GetFilePath(boardMasterId);
        DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
        if (impersonater.ImpersonateValidUser())
        {

            for(int i=0 ; i< dt.Rows.Count ;i++)
            {
                string fileName = System.IO.Path.Combine(path, dt.Rows[i]["fileName"].ToString());

                if (System.IO.File.Exists(fileName))
                    System.IO.File.Delete(fileName);
            }
            impersonater.UndoImpersonation();
        }

    }
    /// <summary>
    /// 첨부파일 처리
    /// </summary>
    /// <param name="boardId"></param>
    public static void AttachFile(long boardId , int boardMasterId)
    {

        DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
        if (impersonater.ImpersonateValidUser())
        {
            string path = Common.GetFilePath(boardMasterId);

            if (!string.IsNullOrEmpty(HttpContext.Current.Request["_delete_file"]))
            {
                string[] deleted_fileArr = HttpContext.Current.Request["_delete_file"].Split(',');

                using (DID.Service.Community.Board obj = new DID.Service.Community.Board())
                {
                    for (int i = 0; i < deleted_fileArr.Length; i++)
                    {
                        obj.DeleteAttach(Convert.ToInt32(deleted_fileArr[i].Split('|')[0]));

                        string fileName = System.IO.Path.Combine(path, deleted_fileArr[i].Split('|')[1]);

                        if (System.IO.File.Exists(fileName))
                            System.IO.File.Delete(fileName);
                    }
                }
            }

            if (UploadModule.IsEnabled)
            {
                UploadedFileCollection allFiles = UploadModule.Files;

                for (int i = 0; allFiles != null && i < allFiles.Count; i++)
                {
                    if (allFiles[i].IsUploaded)
                    {
                        UploadedFile uploadedFile = allFiles[i];

                        string fileName = Common.GetUniqueFileName(path, uploadedFile.FileName);
                        uploadedFile.SaveAs(fileName);
                        fileName = System.IO.Path.GetFileName(fileName);

                        using (DID.Service.Community.Board obj = new DID.Service.Community.Board())
                        {
                            obj.InsertAttach(boardId
                                           , string.Empty
                                           , fileName
                                           , uploadedFile.ContentLength.ToString()
                                           );

                        }

                        uploadedFile.Dispose();

                    }
                }
            }
            else
            {
                throw new Exception(Resources.Resource.err_0001); //"UploadHttpModule 설정이 필요함."
            }

        impersonater.UndoImpersonation();
        }

    }

    /// <summary>
    /// 첨부 제한 파일 포함 여부 확인
    /// </summary>
    /// <param name="boardId"></param>
    public static Boolean CheckAttachFile()
    {
        Boolean bRet = true;
        DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
        if (impersonater.ImpersonateValidUser())
        {
            if (UploadModule.IsEnabled)
            {
                UploadedFileCollection allFiles = UploadModule.Files;
                String strExt;
                String[] limitExtension = new[] { ".html", ".htm", ".aspx", ".asp", ".cer", ".cdx", ".asa", ".exe", ".bat", ".cmd", ".com", ".htw", ".ida", ".idq", ".htr", ".idc", ".shtm", ".shtml", ".stm", ".printer", ".ini", ".log", ".pol", ".dat", ".config" };
                for (int i = 0; allFiles != null && i < allFiles.Count; i++)
                {
                    if (allFiles[i].IsUploaded)
                    {
                        UploadedFile uploadedFile = allFiles[i];
                        if (Path.HasExtension(uploadedFile.FileName))
                        {
                            strExt = Path.GetExtension(uploadedFile.FileName);
                            if (limitExtension.Contains(strExt.ToLower()))
                            {
                                bRet = false;
                                break;
                            }
                        }
                    }
                }
                if (!bRet)
                {
                    for (int i = 0; allFiles != null && i < allFiles.Count; i++)
                    {
                        if (allFiles[i].IsUploaded)
                        {
                            UploadedFile uploadedFile = allFiles[i];
                            uploadedFile.Dispose();

                        }
                    }
                }
            }
            else
            {
                throw new Exception(Resources.Resource.err_0001); //"UploadHttpModule 설정이 필요함."
            }

            impersonater.UndoImpersonation();
        }
        return bRet;
    }

    public static string GetFilePath(int boardMasterId)
    {
        string filePath = string.Empty;

        switch(boardMasterId)
        {
            case (int)Common.BoardMasterID.Notice:
                filePath = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_NOTICE"]);
                break;
            case (int)Common.BoardMasterID.Program:
                filePath = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_PROGRAM"]);
                break;
            case (int)Common.BoardMasterID.Manual:
                filePath = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_MANUAL"]);
                break;
            case (int)Common.BoardMasterID.Video:
                filePath = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_VIDEO"]);
                break;
            case (int)Common.BoardMasterID.Contents:
                filePath = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_CONTENTS"]);
                break;
            // 2011.06.15 임유석 시작 - Pop 게시판 화일 저장 PATH
            case (int)Common.BoardMasterID.PopNews:
                filePath = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_POP"]);
                break;
            case (int)Common.BoardMasterID.PopDisplay:
                filePath = HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["FILE_PATH_POP"]);
                break;
            // 2011.06.15 임유석 끝



        }
        return filePath;
    }

    public static string GetUniqueFileName(string path, string fileName)
    {

        string onlyFileName = System.IO.Path.GetFileNameWithoutExtension(fileName);
        string extention = System.IO.Path.GetExtension(fileName);

        bool exist = true;
        int fileCount = 0;

        while (exist)
        {
            if (File.Exists(Path.Combine(path, fileName)))
            {
                fileCount++;
                fileName = string.Format("{0}({1}){2}",onlyFileName,fileCount ,extention);
            }
            else
            {
                exist = false;
            }
        }

        return Path.Combine(path, fileName);
    }


    // 그리드 뷰의 셀병합하기
    // 2011-07-25 임유석 추가
    public static void GroupColumn(GridView gbMonitor, int ColumnIndex)
    {
        if (gbMonitor.Rows.Count > 0)
        {
            int startRow  = 0;
            int spanCount = 1;

            for (int i = 1; i < gbMonitor.Rows.Count; i++)
            {
                if (gbMonitor.Rows[i].Cells[ColumnIndex].Text == gbMonitor.Rows[startRow].Cells[ColumnIndex].Text)
                {
                    spanCount++;

                    gbMonitor.Rows[i].Cells[ColumnIndex].Visible = false;

                    gbMonitor.Rows[startRow].Cells[ColumnIndex].RowSpan = spanCount;

                }
                else
                {
                    startRow  = i;
                    spanCount = 1;
                }
            }

        }

    }

    // 그리드 뷰의 셀병합하기 오버로딩
    // 키값 컬럼으로 병합을한다 
    // 2011-07-28 임유석 추가
    public static void GroupColumn(GridView gbMonitor, int ColumnIndex, int KeyColumnIndex)
    {
        if (gbMonitor.Rows.Count > 0)
        {
            int startRow = 0;
            int spanCount = 1;

            for (int i = 1; i < gbMonitor.Rows.Count; i++)
            {
                if (gbMonitor.Rows[i].Cells[KeyColumnIndex].Text == gbMonitor.Rows[startRow].Cells[KeyColumnIndex].Text)
                {
                    spanCount++;

                    gbMonitor.Rows[i].Cells[ColumnIndex].Visible = false;

                    gbMonitor.Rows[startRow].Cells[ColumnIndex].RowSpan = spanCount;

                }
                else
                {
                    startRow = i;
                    spanCount = 1;
                }
            }

        }

    }

    /// <summary>
    /// 그리드뷰에 텍스트 존재여부 확인(대소문자 구분없음)
    /// </summary>
    /// <param name="gdv">대상 그리드뷰</param>
    /// <param name="columnIndex">찾을 열의 인덱스</param>
    /// <param name="text">찾을 문자열</param>
    /// <returns>찾는 문자열이 없으면 -1, 있으면 해당 행의 인덱스</returns>
    public static int findTextInGridview(GridView gdv, int columnIndex, string text)
    {
        return findTextInGridview(gdv, columnIndex, text, true);
    }

    /// <summary>
    /// 그리드뷰에 텍스트 존재여부 확인(ignoreCase 인자값에 따라서 대소문자 구분)
    /// </summary>
    /// <param name="gdv">대상 그리드뷰</param>
    /// <param name="columnIndex">찾을 열의 인덱스</param>
    /// <param name="text">찾을 문자열</param>
    /// <param name="ignoreCase">대소문자 구분</param>
    /// <returns>찾는 문자열이 없으면 -1, 있으면 해당 행의 인덱스</returns>
    public static int findTextInGridview(GridView gdv, int columnIndex, string text, bool ignoreCase)
    {
        foreach (GridViewRow row in gdv.Rows)
        {
            if (ignoreCase == true)
            {
                if (row.Cells[columnIndex].Text.ToLower().Equals(text.ToLower())) return row.RowIndex;
            }
            else
            {
                if (row.Cells[columnIndex].Text.Equals(text)) return row.RowIndex; 
            }
        }

        return -1;
    }

}

