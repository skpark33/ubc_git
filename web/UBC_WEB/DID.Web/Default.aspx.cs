﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Globalization;
using System.Threading;

using DID.Common.Framework;

public partial class _Default : BasePage 
{
    public string m_Domain;
    private bool IsSecureConnection; 

    protected void Page_Load(object sender, EventArgs e)
    {
        m_Domain = Request.Url.Host;
        //보안 통신이 아닌 경우 https로 redirection.
        IsSecureConnection = Request.IsSecureConnection;
        if (!Request.IsSecureConnection)
        {
        /*
            //SSL을 적용하는 경우 주석 제거
            string uri = "https://" + m_Domain + Request.RawUrl;
            this.Response.Redirect(uri);
            this.Response.End();
         * */
        }

        // GUBUN 인자가 있을경우 메니저에서 로그인 한것으로 판단
        if (Request.QueryString.Keys.Count > 0 )
        {          
            if( !Request.QueryString["GUBUN"].IsNullOrEmpty() )
            {
                if( urlDecode(Request.QueryString["GUBUN"]) == "UBC" )
                {
                    userLogin(
                          urlDecode(Request.QueryString["ID"].ToString())
                        , urlDecode(Request.QueryString["PW"].ToString())
                        , urlDecode(Request.QueryString["PROGRAM"].ToString())
                        , "S"
                    );

                }
            }
        }

        if (!this.IsPostBack){ txtUserID.Focus(); }         
    }
    
    private string urlDecode(string urlEncode)
    {
        //치환대상문자  치환문자
        //    =           .
        //    +           *
        //    /           ?

        string rtStr = "";
       
        rtStr = urlEncode.Replace('.', '=');
        rtStr = rtStr.Replace('*', '+');
        rtStr = rtStr.Replace('?', '/');

        
        // rtStr 복호화
        rtStr = System.Text.Encoding.GetEncoding("UTF-8").GetString(System.Convert.FromBase64String(rtStr));
         
        return rtStr; 
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        userLogin(txtUserID.Text, txtPwd.Text, null, "W");
    }


    public void userLogin(string userid, string userpassword, string program, string entryPoint)
    {

        DID.Service.Utility.FormAuthenticateUser objAuthUser = new DID.Service.Utility.FormAuthenticateUser();
        
        objAuthUser.UserID = userid;
        objAuthUser.UserPwd = userpassword;
        
        DataTable dt = objAuthUser.CheckUserValidation();
        
        if (dt.Rows.Count == 0)
        {
            ScriptExecute(Resources.Resource.msg_0042); // 아이디 혹은 비밀번호가 일치하지 않습니다
        }
        else
        {
             
            dt = objAuthUser.GetUserInfo();
            
            //프로파일을 설정하는 로직
            Profile.USER_ID = userid;
            Profile.USER_NAME = dt.Rows[0]["userName"].ToString();
            Profile.SITE_ID = dt.Rows[0]["siteId"].ToString();
            //Profile.LANG_FLAG = Thread.CurrentThread.CurrentCulture.Name;
            Profile.COMP_TYPE = dt.Rows[0]["franchizeType"].ToString();

            //USER_TYPE : 1-super admin    2-site admin  3-user
            //3  일때에가 일반 유저이다
            Profile.USER_TYPE = dt.Rows[0]["userType"].ToString();
            
            
            // 웹에 진입하는 부분 웹(W)인지 솔루션(S)인지
            Profile.ENTRY_POINT = entryPoint;

            objAuthUser.CreateAuthenticationTicket();
            //일반통신으로 변경
            string url = "http://" + m_Domain;
            if (program != null && program.Length > 0)
            {
                if (program.ToLower().StartsWith("http://") || program.ToLower().StartsWith("https://"))    //요청 URL에 http 혹은 https가 포함된 경우 변경 없음.
                {
                    url = program;
                }
                else if (program.StartsWith("/"))
                {
                    url += program;
                }
                else
                {
                    url += "/" + program;
                }
            }
            else
            {
                url += "/main.aspx";
            }

            this.Response.Redirect(url, false);
        }
    
    }

}
