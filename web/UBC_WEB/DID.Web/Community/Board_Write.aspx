﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Board_Write.aspx.cs" Inherits="Board_Write" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Innorix/InnoDS/InnoDS.js"></script>

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/default.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">

    var oEditors = [];
    var delArray = [];
    

    $(function() {
        if("<%=this.Profile.LANG_FLAG%>" == "ko-KR")
        {
            nhn.husky.EZCreator.createInIFrame({
                oAppRef: oEditors,
                elPlaceHolder: "<%=ir1.ClientID %>",
                sSkinURI: "/Asset/Editor/SEditorSkin_kr.html",
                fCreator: "createSEditorInIFrame"
            });
        }
        else
        {
            nhn.husky.EZCreator.createInIFrame({
                oAppRef: oEditors,
                elPlaceHolder: "<%=ir1.ClientID %>",
                sSkinURI: "/Asset/Editor/SEditorSkin_en.html",
                fCreator: "createSEditorInIFrame"
            });
        }
    });


    function ValidateForm() {
        oEditors.getById["<%=ir1.ClientID %>"].exec("UPDATE_IR_FIELD", []);        
        if (Trim($("#<%= txtTitle.ClientID%>").val()) == "") {
            alert("<%= Resources.Resource.msg_0044 %>"); // "제목을 입력하십시요
            $("#<%= txtTitle.ClientID%>").focus();
            return false;
        }
        if (oEditors.getById["<%=ir1.ClientID %>"].getIR().length == 0) {
            alert("<%= Resources.Resource.msg_0045 %>"); // 내용을 입력하십시요
            oEditors.getById["<%=ir1.ClientID %>"].exec("FOCUS", []); 
            return false;
        }
        return true;
    }

    function DoSave() {
        if (ValidateForm()) {

            $("#<%= hdnContents.ClientID%>").val($("#<%=ir1.ClientID %>").val());
            $("#<%= btnSave.ClientID%>").click();
        }
    }

    function DoList() {
        location.href = "Board_List.aspx";
    }


    function SetFileCnt() {
        var objFile = document.getElementById("selFileCnt");

        var cnt = objFile.options[objFile.selectedIndex].value;

        for (var i = 1; i <= 15; i++) {
            document.getElementById("divFile" + i).style.display = "none";
        }

        for (var i = 1; i <= cnt; i++) {
            document.getElementById("divFile" + i).style.display = "";
        }
    }


</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnContents" runat="server"  />
    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="display:none" />
    <input type="hidden" id="hndImagFileName" name="hndImagFileName" />
    <input type="button" id="hdnImageSave" name="hdnImageSave" style="display:none" value="ffff" onclick="PasteHtmlImage('<%= ir1.ClientID%>','hndImagFileName');" />
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><asp:Label ID="lblSubTitle" runat="server" Text=""></asp:Label></span>
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <span class='btnType btnRegister'><span><a href='javascript:void(0)' onclick='javascript:DoSave();'>저장</a></span></span>        
        <span class='btnType btnSearch'><span><a href='javascript:void(0)' onclick='javascript:DoList();'>목록</a></span></span>        
    </div>    
    
    <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
	    <colgroup>
	    <col width="15%" />
	    <col width="auto" /> 
    </colgroup>
    <tr>
        <th>제목</th>
        <td> 
            <asp:TextBox ID="txtTitle" runat="server"  CssClass="textType" Width="642px" ></asp:TextBox>
        </td>
    </tr> 
    <tr>
        <th>내용</th>
        <td align="right">
            <span onclick="PopUploader('image','hndImagFileName','hdnImageSave');" style="cursor:pointer">이미지 업로드</span>
            <textarea name="ir1" id="ir1" runat="server" style="width:100%;height:300px" ></textarea>
        </td>
    </tr> 
    <tr>
        <th>파일첨부</th>
        <td align="left">
            첨부파일수 : 
            <select id="selFileCnt" class="input_textfield_default" onchange="SetFileCnt();" >
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
            </select><br />
            <div id="divFile1"><input type="file" name="fileUp" size="45"  class="input_textfield" /></div>
            <div id="divFile2" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile3" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile4" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile5" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile6" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile7" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile8" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile9" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile10" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile11" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile12" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile13" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile14" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
            <div id="divFile15" style="display:none"><input type="file" name="fileUp" size="45" class="input_textfield" /></div>
        </td>
    </tr> 
    </table> 
</asp:Content>

