﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" EnableSessionState="False" ValidateRequest="false" CodeFile="Notice_Write.aspx.cs" Inherits="Notice_Write" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/default.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">

    var oEditors = [];
    var delArray = [];


    $(function() {
        if("<%=this.Profile.LANG_FLAG%>" == "ko-KR")
        {
            nhn.husky.EZCreator.createInIFrame({
                oAppRef: oEditors,
                elPlaceHolder: "<%=ir1.ClientID %>",
                sSkinURI: "/Asset/Editor/SEditorSkin_kr.html",
                fCreator: "createSEditorInIFrame"
            });
        }
        else
        {
            nhn.husky.EZCreator.createInIFrame({
                oAppRef: oEditors,
                elPlaceHolder: "<%=ir1.ClientID %>",
                sSkinURI: "/Asset/Editor/SEditorSkin_en.html",
                fCreator: "createSEditorInIFrame"
            });
        }

        var inlineProgressBar = NeatUploadPB.prototype.Bars["<%= inlineProgressBar.ClientID%>"];
        var origDisplay = inlineProgressBar.Display;
        inlineProgressBar.Display = function() {

            var elem = document.getElementById(this.ClientID);
            elem.parentNode.style.display = "block";
            origDisplay.call(this);
        }
        inlineProgressBar.EvalOnClose = "NeatUploadMainWindow.document.getElementById('" + inlineProgressBar.ClientID + "').parentNode.style.display = \"none\";";
    });


    function ValidateForm() {
        oEditors.getById["<%=ir1.ClientID %>"].exec("UPDATE_IR_FIELD", []);        
        if (Trim($("#<%= txtTitle.ClientID%>").val()) == "") {
            alert("<%= Resources.Resource.msg_0044 %>"); // "제목을 입력하십시요
            $("#<%= txtTitle.ClientID%>").focus();
            return false;
        }
        if (oEditors.getById["<%=ir1.ClientID %>"].getIR().length == 0) {
            alert("<%= Resources.Resource.msg_0045 %>"); // 내용을 입력하십시요
            oEditors.getById["<%=ir1.ClientID %>"].exec("FOCUS", []); 
            return false;
        }
        return true;
    }

    function DoSave() {
        if (ValidateForm()) {
            $("#<%= hdnContents.ClientID%>").val($("#<%=ir1.ClientID %>").val());
            $("#<%= btnSave.ClientID%>").click();
        }
    }

    function DoList() {
        location.href = "Notice_List.aspx";
    }

</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnContents" runat="server"  />
    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="display:none" />
    <input type="hidden" id="hndImagFileName" name="hndImagFileName" />
    <input type="button" id="hdnImageSave" name="hdnImageSave" style="display:none" value="ffff" onclick="PasteHtmlImage('<%= ir1.ClientID%>','<%=this.FILE_PATH_COMMON%>','hndImagFileName');" />
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn("SAVE#LIST") %>
    </div>    
    
    <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
    <colgroup>
	    <col width="13%" />
	    <col width="auto" /> 
	    <col width="13%" />
	    <col width="auto" /> 
	    <col width="13%" />
	    <col width="auto" /> 
    </colgroup>
    <tr>
        <th><%= Resources.Resource.txt_0093 %><!-- 제목 //--><font color="red"> *</font></th>
        <td colspan="5"> 
            <asp:TextBox ID="txtTitle" runat="server"  CssClass="textType" Width="660px" />
        </td>
    </tr> 
    <!--tr>
        <th>로그인 전후</th>
        <td>
            <asp:RadioButtonList ID="rdoLoginYn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                <asp:ListItem  Value="Y" Text="전" />
                <asp:ListItem  Value="N" Text="후" Selected="True" />
            </asp:RadioButtonList>
        </td>
        <th>팝업크기</th>
        <td>
            W : <asp:TextBox ID="txtWidth" runat="server" Width="30px" CssClass="textType" />
            H : <asp:TextBox ID="txtHeight" runat="server" Width="30px" CssClass="textType"/>
        </td>
        <th>팝업위치</th>
        <td>
            X : <asp:TextBox ID="txtPosX" runat="server" Width="30px" CssClass="textType"/>
            Y : <asp:TextBox ID="txtPosY" runat="server" Width="30px" CssClass="textType"/>
        </td>
    </tr> 
    <tr>
        <th>팝업사용 여부</th>
        <td>
            <asp:CheckBox ID="chkPopYn1" runat="server" />
        </td>
        <th>탑공지 여부</th>
        <td colspan="3">
            <asp:CheckBox ID="chkTopYn11" runat="server" />
        </td>
    </tr--> 
    <tr>
        <th><%= Resources.Resource.txt_0097 %><!-- 목록 상단 위치 //--></th>
        <td>
            <asp:RadioButtonList ID="rdoTopYn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radioType"  >
                <asp:ListItem Text="YES" Value="Y"  />
                <asp:ListItem Text="NO" Value="N" Selected="True" />
            </asp:RadioButtonList>
        </td>
        <th><%= Resources.Resource.txt_0098 %><!-- 팝업사용 여부 //--></th>
        <td colspan="3">
            <asp:RadioButtonList ID="rdoPopYn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radioType"  >
                <asp:ListItem Text="YES&nbsp;" Value="Y"  />
                <asp:ListItem Text="NO" Value="N" Selected="True" />
            </asp:RadioButtonList>
        </td>
    </tr> 
    <tr>
        <th><%= Resources.Resource.txt_0095 %><!-- 내용 //--><font color="red"> *</font></th>
        <td align="right" colspan="5">
            <span onclick="PopUploader('image','hndImagFileName','hdnImageSave');" style="cursor:pointer">
            <img src="/Asset/Images/body/img.gif" />
            <%= Resources.Resource.txt_0102 %><!-- 이미지 업로드 //--></span>
            <textarea name="ir1" id="ir1" runat="server" style="width:100%;height:300px" ></textarea>
        </td>
    </tr> 
    <tr id="trFileView" runat="server" visible="false">
        <th><%= Resources.Resource.txt_0100 %><!-- 첨부 화일 //--></th>
        <td style="text-align:left" colspan="5">
            <asp:Repeater ID="rptFile" runat="server">
            <ItemTemplate>
                <input type="checkbox" value="<%# Eval("attachId") %>|<%# Util.DB2HTML(Eval("fileName").ToString()) %>" name="_delete_file" /><%# Util.DB2HTML(Eval("fileName").ToString())%><br />        
            </ItemTemplate>
            </asp:Repeater><p></p>
            <span style="color:#8b0000"><%= Resources.Resource.txt_0101 %><!-- ◆ 삭제하려면 해당 파일을 체크하고 저장하면 됩니다. //--></span> 
        </td>
    </tr>
    <tr>
        <th><%= Resources.Resource.txt_0094 %><!-- 첨부 //--></th>
        <td align="left" colspan="5">
        
			<p>File(s) to upload:</P> 
			<Upload:MultiFile id="multiFile" runat="server" useFlashIfAvailable="false">			    
				<asp:Button id="multiFileButton" Text="<%$ Resources : Resource, txt_0099 %>" Enabled="<%# multiFile.Enabled %>" runat="server"/> <!-- 찾아보기 //-->
			</Upload:MultiFile>

			<div style="display:none;">
			<Upload:ProgressBar id="inlineProgressBar" runat="server" Inline="true" height="50px" Width="630px" Triggers="btnSave">
			<asp:Label id="label" runat="server" Text="Check Progress"/>
			</Upload:ProgressBar>
			</div>

        </td>
    </tr> 
    </table> 
    

</asp:Content>

