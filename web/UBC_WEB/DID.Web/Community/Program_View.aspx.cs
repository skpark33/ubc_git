﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

/// <summary>
/// 조회 페이지
/// </summary>
public partial class Program_View : BasePage
{
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string BoardID
    {
        get { return Request["boardId"] ?? ""; }
    }
    /// <summary>
    /// 게시물번호
    /// </summary>
    public string Thread
    {
        get;set;
    }
    /// <summary>
    /// 게시물 Depth
    /// </summary>
    public string Depth
    {
        get;set;
    }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }
    /// <summary>
    /// 모드 : write,reply,update
    /// </summary>
    public Common.BoardMode Mode
    {
        get
        {
            if (!string.IsNullOrEmpty(this.BoardID) && string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Update;
            else
                return Common.BoardMode.Write;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0035; // UBC 다운로드

        if (!this.IsPostBack)
        {
            if (this.Mode == Common.BoardMode.Update)
            {
                this.BindControl();
            }
        }
    }

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        DataTable dtList = null;
        DataTable dtFile = null;
        long boardId = Convert.ToInt64(this.BoardID);

        using (DID.Service.Community.Program obj = new DID.Service.Community.Program())
        {
            dtList = obj.GetDetail(boardId, this.Profile.SITE_ID);
            obj.UpdateReadCount(boardId);
            dtFile = obj.GetAttachList(boardId);
        }

        fvBoard.DataSource = dtList;
        fvBoard.DataBind();

        if (dtList.Rows.Count > 0)
        {
            this.Thread = dtList.ToDataRow()["thread"].ToString();
            this.Depth = dtList.ToDataRow()["depth"].ToString();
            this.hdnBoardId.Value = dtList.ToDataRow()["boardId"].ToString();
        }

        Repeater rpt = (Repeater)fvBoard.FindControl("rptFile");
        if (!rpt.IsNullOrEmpty())
        {
            rpt.DataSource = dtFile;
            rpt.DataBind();
        }        

    }
    #endregion

    #region 삭제버튼 클릭 - btnDelete_Click(object sender, EventArgs e)
    /// <summary>
    /// 삭제버튼 클릭
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        //삭제 권한 확인
        if (!Common.CheckAuth())
        {
            this.ScriptExecute(Resources.Resource.msg_0030);  //"권한이 없습니다.");
            return;
        }
        using (DID.Service.Community.Program obj = new DID.Service.Community.Program())
        {
            long boardId = Convert.ToInt64(this.hdnBoardId.Value);
            DataTable dt = obj.GetAttachList(boardId);
            Common.DeleteFile(dt, (int)Common.BoardMasterID.Program);
            obj.DeleteAttachByBoardId(boardId);
            obj.Delete(boardId);
        }

        this.ScriptExecuteUrl(Resources.Resource.msg_0047, "Program_List.aspx");// 삭제되었습니다
    }
    #endregion
}
