﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Manual_List.aspx.cs" Inherits="Manual_List" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoQuery(code) {
        $("#<%=txtSearchWord.ClientID %>").val(code);
        $("#<%= btnSearch.ClientID%>").click();
    }

    function DoReg() {
        location.href = "Manual_Write.aspx";
    }
    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Button id="btnSearch" runat="server" OnClick="btnSearch_click" style="display:none"  />

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
            </td>
        </tr>
    </table>
    
    <div class="pad_B10" >   
        <span class="button xLarge icon" onclick="DoQuery('');"><span class="refresh"></span><button type="button"><%= Resources.Resource.txt_0030 %></button></span> 
        <asp:Repeater ID="rptProgramType" runat="server">
        <ItemTemplate>
            <span class="button xLarge icon" onclick="DoQuery('<%# Eval("codeId")%>');"><span class="refresh"></span><button type="button"><%# Eval("codeNm")%></button></span>
        </ItemTemplate>
        </asp:Repeater>
    </div> 
        
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn("REG") %>
    </div>    
    
    <div>
                      
        <!-- 콤보박스 옵션
                전체 
                제목
                내용
                등록자
                구분
            -->                   
        <asp:DropDownList ID="ddlSearchKind" runat="server" CssClass="selectType" style="display:none">
            <asp:ListItem Text="<%$ Resources: Resource, txt_0030 %>" Value=""></asp:ListItem>
            <asp:ListItem Text="<%$ Resources: Resource, txt_0093 %>" Value="title"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources: Resource, txt_0095 %>" Value="contents"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources: Resource, txt_0048 %>" Value="userNm"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources: Resource, txt_0103 %>" Value="contentCd" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtSearchWord" runat="server" CssClass="textType" Width="300px" style="display:none"/>
    </div>
    
    <!--  GridView 컬럼명
        번호
        구분 
        제목
        첨부
        등록자
        등록일
        조회수
    //--> 
    <asp:GridView ID="gvList" runat="server" Width="100%" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false"  
    CssClass="boardListType">
    <Columns>
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0092 %>">
            <HeaderStyle Width="5%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#(this.TOTAL_ROW_COUNT - ((this.CURRENT_PAGE - 1) * this.PAGE_SIZE + Container.DataItemIndex))%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources: Resource, txt_0103 %>">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("contentNm")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0093 %>" HeaderStyle-CssClass="center">
            <HeaderStyle  />
            <ItemTemplate>
                <a href="Manual_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>"><%#Util.DB2HTML(Eval("title").ToString()) %></a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0094 %>" HeaderStyle-CssClass="center">
            <HeaderStyle Width="9%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField  DataField="userName" HeaderText="<%$ Resources : Resource, txt_0048 %>" HeaderStyle-Width="15%" ItemStyle-CssClass="center" />
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0096 %>">
            <HeaderStyle Width="10%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("createDate", "{0:yyyy-MM-dd}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0085 %>">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("readCount", "{0:n0}")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        <table style="width:100%;height:200px">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    </asp:GridView>   
                
    <div id="pagenum2" style="margin-top:15px">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>     
       	
</asp:Content>

