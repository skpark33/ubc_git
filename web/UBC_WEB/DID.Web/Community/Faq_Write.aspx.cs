﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;
using System.Web.UI.HtmlControls;

/// <summary>
/// 등록,수정,답변 페이지
/// </summary>
public partial class Faq_Write : BasePage
{
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string BoardID
    {
        get { return Request["boardId"] ?? ""; }
    }
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string Thread
    {
        get { return Request["Thread"] ?? ""; }
    }
    /// <summary>
    /// 모드 : write,reply,update
    /// </summary>
    public Common.BoardMode Mode
    {
        get
        {
            if (!string.IsNullOrEmpty(this.BoardID) && string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Update;
            else if (!string.IsNullOrEmpty(this.BoardID) && !string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Reply;
            else
                return Common.BoardMode.Write;
        }
    }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }
    /// <summary>
    /// 게시글 Depth
    /// </summary>
    public string Depth
    {
        get { return Request["Depth"] ?? ""; }
    }
    /// <summary>
    /// 첨부파일 데이터
    /// </summary>
    public DataTable AttachFileData { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        //쓰기 권한 확인
        if (!Common.CheckAuth())
        {
            //권한이 없는 경우 이전 페이지로 이동
            Response.Redirect(Util.URL_ERROR_ROLE);
            return;
        }

        ((HtmlForm)this.Master.FindControl("form1")).Enctype = "multipart/form-data";

        if (!this.IsPostBack)
        {
            this.InitControl();

            switch (this.Mode)
            {
                case Common.BoardMode.Write:
                    lblSubTitle.Text = "FAQ 등록";
                    break;
                case Common.BoardMode.Update:
                    lblSubTitle.Text = "FAQ 수정";
                    break;
            }

            if (this.Mode == Common.BoardMode.Update)
            {
                this.BindControl();
            }
        }
    }

    private void InitControl()
    {
        this.AttachFileData = new DataTable();
    }

    #region 저장버튼 클릭 - btnSave_Click(object sender, EventArgs e)
    /// <summary>
    /// 저장버튼 클릭
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        long boardId = 0;
        //쓰기 권한 확인
        if (!Common.CheckAuth())
        {
            //권한이 없는 경우 홈으로 이동
            this.ScriptExecuteGoHome(Resources.Resource.msg_0030);  //"권한이 없습니다.");
            return;
        }

        using (DID.Service.Community.Faq obj = new DID.Service.Community.Faq())
        {
            switch (this.Mode)
            {
                case Common.BoardMode.Write:
                    //boardId = obj.Insert((int)Common.BoardMasterID.Faq
                    //                    , Profile.USER_ID
                    //                    , hdnContents.Value
                    //                    , hdnContentsReply.Value
                    //                    , rdoFaqCd.SelectedValue
                    //                    , Request.UserHostAddress
                    //                    );
                    break;
                case Common.BoardMode.Update:
                    //boardId = Convert.ToInt64(this.BoardID);
                    //obj.Update(boardId
                    //            , Profile.USER_ID
                    //            , hdnContents.Value
                    //            , hdnContentsReply.Value
                    //            , rdoFaqCd.SelectedValue
                    //            , Request.UserHostAddress
                    //            );
                    break;

            }

            //Common.AttachFile(boardId);
        }

        this.ScriptExecuteUrl(Resources.Resource.msg_0046, string.Format("Faq_List.aspx?Pgn={0}", this.Pgn));   //"저장되었습니다."
    }
    #endregion

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        DataTable dt = null;
        long boardId = Convert.ToInt64(this.BoardID);
        using (DID.Service.Community.Faq obj = new DID.Service.Community.Faq())
        {
            dt = obj.GetDetail(boardId, this.Profile.SITE_ID);
            //this.AttachFileData = obj.GetAttachList(boardId);
        }

        if (dt.Rows.Count > 0)
        {
            DataRow dr = dt.Rows[0];

            rdoFaqCd.SelectedValue = dr["faqCd"].ToString();
            hdnContents.Value = dr["Contents"].ToString();
            ir1.Value = dr["Contents"].ToString();
            hdnContentsReply.Value = dr["Contents_Reply"].ToString();
            ir2.Value = dr["Contents_Reply"].ToString();
        }
    }
    #endregion


}
