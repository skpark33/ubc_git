﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/PopupMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Notice_Popup.aspx.cs" Inherits="Notice_Popup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
                
            </td>
        </tr>
    </table>
    
    <asp:FormView ID="fvBoard" runat="server" style="width:100%">
    <ItemTemplate>
        <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
        <colgroup>
	        <col width="15%" />
	        <col width="auto" />
	        <col width="15%" />
	        <col width="auto" />  
	        <col width="15%" />
	        <col width="auto" />  
        </colgroup>
        <tr>
        <th><%= Resources.Resource.txt_0093 %></th>
            <td colspan="5">
                <%# Util.DB2HTML(Eval("title").ToString()) %>
            </td> 
        </tr> 
        <tr>
            <th><%= Resources.Resource.txt_0095 %></th>
            <td colspan="5" >
                <div class="smartOutput">
                <%# Util.DB2HTML(Eval("contents").ToString())%>
                </div>
            </td>
        </tr> 
        </table>
    </ItemTemplate> 
    </asp:FormView>

</asp:Content>

