﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;
using System.Web.UI.HtmlControls;

/// <summary>
/// 등록,수정 페이지
/// </summary>
public partial class Contents_Write : BasePage
{
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string BoardID
    {
        get { return Request["boardId"] ?? ""; }
    }
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string Thread
    {
        get { return Request["Thread"] ?? ""; }
    }
    /// <summary>
    /// 모드 : write,reply,update
    /// </summary>
    public Common.BoardMode Mode
    {
        get
        {
            if (!string.IsNullOrEmpty(this.BoardID) && string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Update;
            else
                return Common.BoardMode.Write;
        }
    }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }
    /// <summary>
    /// 게시글 Depth
    /// </summary>
    public string Depth
    {
        get { return Request["Depth"] ?? ""; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //쓰기 권한 확인
        if (!Common.CheckAuth())
        {
            //권한이 없는 경우 이전 페이지로 이동
            Response.Redirect(Util.URL_ERROR_ROLE);
            return;
        }
        this.Title = Resources.Resource.ttl_0041; // 콘텐츠

        ((HtmlForm)this.Master.FindControl("form1")).Enctype = "multipart/form-data";

        if (!this.IsPostBack)
        {
            this.InitControl();

            switch (this.Mode)
            {
                case Common.BoardMode.Write:
                    this.Title += " - " + Resources.Resource.ttl_0042; // 콘텐츠 - 등록
                    break;
                case Common.BoardMode.Update:
                    this.Title += " - " + Resources.Resource.ttl_0043; // 콘텐츠 - 수정
                    break;
            }

            if (this.Mode == Common.BoardMode.Update)
            {
                this.BindControl();
            }
        }
    }

    private void InitControl()
    {
    }

    #region 저장버튼 클릭 - btnSave_Click(object sender, EventArgs e)
    /// <summary>
    /// 저장버튼 클릭
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        long boardId = 0;
        //쓰기 권한 확인
        if (!Common.CheckAuth())
        {
            //권한이 없는 경우 홈으로 이동
            this.ScriptExecuteGoHome(Resources.Resource.msg_0030);  //"권한이 없습니다.");
            return;
        }
        if (!Common.CheckAttachFile())
        {
            //업로드 제한 파일 포함됨.
            this.ScriptExecute(Resources.Resource.msg_0067);  //"업로드할 수 없는 파일이 포함되어 있습니다.");
            return;
        }

        using (DID.Service.Community.Contents obj = new DID.Service.Community.Contents())
        {
            switch (this.Mode)
            {
                case Common.BoardMode.Write:
                    boardId = obj.Insert((int)Common.BoardMasterID.Contents
                                        , Profile.USER_ID
                                        , txtTitle.Text
                                        , hdnContents.Value
                                        , rdoTopYn.SelectedValue.Equals("Y")
                                        , Request.UserHostAddress
                                        , Profile.COMP_TYPE
                                        );
                    break;
                case Common.BoardMode.Update:
                    boardId = Convert.ToInt64(this.BoardID);
                    obj.Update(boardId
                                , Profile.USER_ID
                                , txtTitle.Text
                                , hdnContents.Value
                                , rdoTopYn.SelectedValue.Equals("Y")
                                , Request.UserHostAddress
                                );
                    break;

            }

            Common.AttachFile(boardId, (int)Common.BoardMasterID.Contents);
        }

        this.ScriptExecuteUrl(Resources.Resource.msg_0046/* 저장되었습니다 */, string.Format("Contents_List.aspx?Pgn={0}", this.Pgn));
    }
    #endregion

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        DataTable dt = null;
        DataTable dtFile = null;

        long boardId = Convert.ToInt64(this.BoardID);
        using (DID.Service.Community.Contents obj = new DID.Service.Community.Contents())
        {
            dt = obj.GetDetail(boardId, this.Profile.SITE_ID);
            dtFile = obj.GetAttachList(boardId);
        }

        rptFile.DataSource = dtFile;
        rptFile.DataBind();

        if (dtFile.Rows.Count > 0)
        {
            trFileView.Visible = true;
        }

        if (dt.Rows.Count > 0)
        {
            DataRow dr = dt.ToDataRow();

            txtTitle.Text = dr["Title"].ToString();
            hdnContents.Value = dr["Contents"].ToString();
            ir1.Value = dr["Contents"].ToString();
            rdoTopYn.SelectedValue = Convert.ToBoolean(dr["topYn"]) ? "Y" : "N";
        }
    }
    #endregion
}
