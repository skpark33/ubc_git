﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Board_Write_InnoDS.aspx.cs" Inherits="Board_Write" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Innorix/InnoDS/InnoDS.js"></script>

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/default.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">

    var oEditors = [];
    var delArray = [];
    

    $(function() {
        if("<%=this.Profile.LANG_FLAG%>" == "ko-KR")
        {
            nhn.husky.EZCreator.createInIFrame({
                oAppRef: oEditors,
                elPlaceHolder: "<%=ir1.ClientID %>",
                sSkinURI: "/Asset/Editor/SEditorSkin_kr.html",
                fCreator: "createSEditorInIFrame"
            });
        }
        else
        {
            nhn.husky.EZCreator.createInIFrame({
                oAppRef: oEditors,
                elPlaceHolder: "<%=ir1.ClientID %>",
                sSkinURI: "/Asset/Editor/SEditorSkin_en.html",
                fCreator: "createSEditorInIFrame"
            });
        }
    });


    function ValidateForm() {
        oEditors.getById["<%=ir1.ClientID %>"].exec("UPDATE_IR_FIELD", []);        
        if (Trim($("#<%= txtTitle.ClientID%>").val()) == "") {
            alert("제목을 입력하십시요.");
            $("#<%= txtTitle.ClientID%>").focus();
            return false;
        }
        if (oEditors.getById["<%=ir1.ClientID %>"].getIR().length == 0) {
            alert("내용을 입력하십시요.");
            oEditors.getById["<%=ir1.ClientID %>"].exec("FOCUS", []); 
            return false;
        }
        return true;
    }

    function DoSave() {
        if (ValidateForm()) {

            $("#<%= hdnContents.ClientID%>").val($("#<%=ir1.ClientID %>").val());

            Upload();
        }
    }

    function DoList() {
        location.href = "Board_List.aspx";
    }

    function DoSubmit(){
        $("#<%= btnSave.ClientID%>").click();
    }

    // 컴포넌트 로드 완료시 자동 호출되는 콜백 함수
    function OnLoadComplete(objName) {
        SetInnoDSInit(objName);
        
        // 수정모드 가상파일 추가
        <% 
            if(this.AttachFileData != null){
                for(int i=0 ; i< this.AttachFileData.Rows.Count ; i++){
                System.Data.DataRow dr = this.AttachFileData.Rows[i]; 
        %>
                    InnoDS.AddVirtualFile('<%=dr["fileName"]%>', '<%= dr["size"]%>', '<%=dr["attachId"]%>');
        <% }} %>   
        
    }


</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnContents" runat="server"  />
    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="display:none" />
    <input type="hidden" id="hndImagFileName" name="hndImagFileName" />
    <input type="button" id="hdnImageSave" name="hdnImageSave" style="display:none" value="ffff" onclick="PasteHtmlImage('<%= ir1.ClientID%>','hndImagFileName');" />
    
    <table style="width:100%;height:100%" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><asp:Label ID="lblSubTitle" runat="server" Text=""></asp:Label></span>
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <span class='btnType btnRegister'><span><a href='javascript:void(0)' onclick='javascript:DoSave();'>저장</a></span></span>        
        <span class='btnType btnSearch'><span><a href='javascript:void(0)' onclick='javascript:DoList();'>목록</a></span></span>        
    </div>    
    
    <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
	    <colgroup>
	    <col width="15%" />
	    <col width="auto" /> 
    </colgroup>
    <tr>
        <th>제목</th>
        <td> 
            <asp:TextBox ID="txtTitle" runat="server"  CssClass="textType" Width="642px" ></asp:TextBox>
        </td>
    </tr> 
    <tr>
        <th>내용</th>
        <td align="right">
            <span onclick="PopUploader('image','hndImagFileName','hdnImageSave');" style="cursor:pointer">이미지 업로드</span>
            <textarea name="ir1" id="ir1" runat="server" style="width:100%;height:300px" ></textarea>
        </td>
    </tr> 
    <tr>
        <th>파일첨부</th>
        <td align="right">
            <input type="button" id="ff" value="폴더별" onclick="ViewDSFolder();" />
            <input type="button" id="Button1" value="파일별" onclick="ViewDSFile();" />
            <script type="text/javascript">
            
                var Enc = "Ouu4c/GrZ2WelN5djCy1oXQxaxFd5m9ItDD1x5jnpO/6JAMaXfvUwx6wdNoS8VaNGT7cgGOtPbNDp4R1lFes3yI+wRnpvGnWNy55mvmgcReBzCpVIzBP0mEFTfs0TL0a8Lyw8+ggMeY1bNxr";

                var InputType = "fixed";
                var UploadURL = "FileUploadProc.aspx";
                var SubDir = "";
                var InnoInstall = "./install.html";
                var ViewType = "2";

                InnoDSInit(-1, -1, -1, 650, 150);

            </script>
        </td>
    </tr> 
    </table> 
</asp:Content>

