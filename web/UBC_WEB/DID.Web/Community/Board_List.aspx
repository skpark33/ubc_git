﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Board_List.aspx.cs" Inherits="BoardList" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoSearch() {
        $("#<%= btnSearch.ClientID%>").click();
    }

    function DoReg() {
        location.href = "Board_Write.aspx";
    }
    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Button id="btnSearch" runat="server" OnClick="btnSearch_click" style="display:none"  />

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><span>게시판</span></span> <!--게시판-->
            </td>
        </tr>
    </table>
        
    <div class='topOptionBtns'>
        <span class='btnType btnSearch'><span><a href='javascript:void(0)' onclick='javascript:DoSearch();'>검색</a></span></span>        
        <span class='btnType btnRegister'><span><a href='javascript:void(0)' onclick='javascript:DoReg();'>등록</a></span></span>        
    </div>    
    
    <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
		<colgroup>
		<col width="100px" />
		<col width="auto" />
	</colgroup>
    <tr>
        <th><span>검색</span></th>
        <td>             
            <asp:DropDownList ID="ddlSearchKind" runat="server" CssClass="selectType">
                <asp:ListItem Text="전체" Value=""></asp:ListItem>
                <asp:ListItem Text="제목" Value="title"></asp:ListItem>
                <asp:ListItem Text="내용" Value="contents"></asp:ListItem>
                <asp:ListItem Text="글쓴이" Value="userNm"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearchWord" runat="server" CssClass="textType" />
        </td>
    </tr>
    </table>
    
    <asp:GridView ID="gvList" runat="server" Width="100%" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false"  
    CssClass="boardListType" style="margin-top:10px" >
    <Columns>
        <asp:TemplateField HeaderText="번호">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="center" />
            <ItemTemplate>
                <%#(this.TOTAL_ROW_COUNT - ((this.CURRENT_PAGE - 1) * this.PAGE_SIZE + Container.DataItemIndex))%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="제목" HeaderStyle-CssClass="center">
            <HeaderStyle Width="50%" />
            <ItemStyle CssClass="left" />
            <ItemTemplate>
                <a href="Board_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>"><%#Util.DB2HTML(Eval("title").ToString()) %></a>
            </ItemTemplate>
        </asp:TemplateField>
                
        <asp:BoundField  DataField="userName" HeaderText="글쓴이" HeaderStyle-Width="13%" ItemStyle-CssClass="center" />
        <asp:TemplateField HeaderText="등록일">
            <HeaderStyle Width="10%" />
            <ItemStyle CssClass="center" />
            <ItemTemplate>
                <%#Eval("createDate", "{0:yyyy-MM-dd}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="조회수">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="right" />
            <ItemTemplate>
                <%#Eval("readCount", "{0:n0}")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        <table style="width:100%;height:200px">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    </asp:GridView>   
                
    <div id="pagenum2" style="margin-top:15px">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>     
       	
</asp:Content>

