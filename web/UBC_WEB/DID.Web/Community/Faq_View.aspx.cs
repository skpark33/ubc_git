﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

/// <summary>
/// 조회 페이지
/// </summary>
public partial class Faq_View : BasePage
{
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string BoardID
    {
        get { return Request["boardId"] ?? ""; }
    }
    /// <summary>
    /// 게시물번호
    /// </summary>
    public string Thread
    {
        get;set;
    }
    /// <summary>
    /// 게시물 Depth
    /// </summary>
    public string Depth
    {
        get;set;
    }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }
    /// <summary>
    /// 모드 : write,reply,update
    /// </summary>
    public Common.BoardMode Mode
    {
        get
        {
            if (!string.IsNullOrEmpty(this.BoardID) && string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Update;
            else if (!string.IsNullOrEmpty(this.BoardID) && !string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Reply;
            else
                return Common.BoardMode.Write;
        }
    }
    /// <summary>
    /// 첨부파일 데이터
    /// </summary>
    public DataTable AttachFileData { get; set; }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (this.Mode == Common.BoardMode.Update)
            {
                this.BindControl();
            }
        }
    }

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        DataTable dt = null;
        long boardId = Convert.ToInt64(this.BoardID);

        using (DID.Service.Community.Board obj = new DID.Service.Community.Board())
        {
            dt = obj.GetDetail(boardId, this.Profile.SITE_ID);
            obj.UpdateReadCount(boardId);
            this.AttachFileData = obj.GetAttachList(boardId);
        }

        fvBoard.DataSource = dt;
        fvBoard.DataBind();

        if (dt.Rows.Count > 0)
        {
            this.Thread = dt.ToDataRow()["thread"].ToString();
            this.Depth = dt.ToDataRow()["depth"].ToString();
            this.hdnBoardId.Value = dt.ToDataRow()["boardId"].ToString();
        }
    }
    #endregion

    #region 삭제버튼 클릭 - btnDelete_Click(object sender, EventArgs e)
    /// <summary>
    /// 삭제버튼 클릭
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        //삭제 권한 확인
        if (!Common.CheckAuth())
        {
            this.ScriptExecute(Resources.Resource.msg_0030);  //"권한이 없습니다.");
            return;
        }
        using (DID.Service.Community.Board obj = new DID.Service.Community.Board())
        {
            obj.Delete(Convert.ToInt64(this.hdnBoardId.Value));
        }

        this.ScriptExecuteUrl(Resources.Resource.msg_0047, "Board_List.aspx");// 삭제되었습니다
    }
    #endregion
}
