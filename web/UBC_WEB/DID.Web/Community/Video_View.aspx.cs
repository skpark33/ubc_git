﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

/// <summary>
/// 조회 페이지
/// </summary>
public partial class Video_View : BasePage
{
    /// <summary>
    /// 게시글ID
    /// </summary>
    public string BoardID
    {
        get { return Request["boardId"] ?? ""; }
    }
    /// <summary>
    /// 게시물번호
    /// </summary>
    public string Thread
    {
        get;set;
    }
    /// <summary>
    /// 게시물 Depth
    /// </summary>
    public string Depth
    {
        get;set;
    }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }
    /// <summary>
    /// 모드 : write,reply,update
    /// </summary>
    public Common.BoardMode Mode
    {
        get
        {
            if (!string.IsNullOrEmpty(this.BoardID) && string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Update;
            else
                return Common.BoardMode.Write;
        }
    }

    protected string VIDEO_FILE_NAME { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0037; // 동영상 메뉴얼

        if (!this.IsPostBack)
        {
            if (this.Mode == Common.BoardMode.Update)
            {
                this.BindControl();
            }
        }
    }

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        DataTable dtList = null;
        DataTable dtFile = null;
        long boardId = Convert.ToInt64(this.BoardID);

        using (DID.Service.Community.Video obj = new DID.Service.Community.Video())
        {
            dtList = obj.GetDetail(boardId, this.Profile.SITE_ID);
            obj.UpdateReadCount(boardId);
            
            dtFile = obj.GetAttachList(boardId);

            string[] imgExtention = {".jpg",".png" ,".gif"};

            foreach (string ext in imgExtention)
            {
                dtFile.DefaultView.RowFilter = string.Format("fileName like '%{0}'", ext);
                if (dtFile.DefaultView.Count > 0)
                {
                    dtFile.Rows.Remove(dtFile.DefaultView[0].Row);
                }
            }

        }

        fvBoard.DataSource = dtList;
        fvBoard.DataBind();

        if(dtFile.Rows.Count > 0 )
        {
            this.VIDEO_FILE_NAME = this.FILE_PATH_VIDEO + "/" + dtFile.Rows[0]["fileName"].ToString();

            DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
            if (impersonater.ImpersonateValidUser())
            {
                if (!File.Exists(Server.MapPath(this.VIDEO_FILE_NAME)))
                {
                    this.VIDEO_FILE_NAME = "/";  //폴더를 지정하면 사용 불가 UI가 표출되어 효율적이므로 변경 함. by dykim. 2012.06.08.
                }
                impersonater.UndoImpersonation();
            }
        }
        if (dtList.Rows.Count > 0)
        {
            this.Thread = dtList.ToDataRow()["thread"].ToString();
            this.Depth = dtList.ToDataRow()["depth"].ToString();
            this.hdnBoardId.Value = dtList.ToDataRow()["boardId"].ToString();
        }

    }
    #endregion

    #region 삭제버튼 클릭 - btnDelete_Click(object sender, EventArgs e)
    /// <summary>
    /// 삭제버튼 클릭
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        //삭제 권한 확인
        if (!Common.CheckAuth())
        {
            this.ScriptExecute(Resources.Resource.msg_0030);  //"권한이 없습니다.");
            return;
        }
        using (DID.Service.Community.Video obj = new DID.Service.Community.Video())
        {
            long boardId = Convert.ToInt64(this.hdnBoardId.Value);
            DataTable dt = obj.GetAttachList(boardId);
            Common.DeleteFile(dt, (int)Common.BoardMasterID.Video);
            obj.DeleteAttachByBoardId(boardId);
            obj.Delete(boardId);
        }

        this.ScriptExecuteUrl(Resources.Resource.msg_0047, "Video_List.aspx");// 삭제되었습니다
    }
    #endregion
}
