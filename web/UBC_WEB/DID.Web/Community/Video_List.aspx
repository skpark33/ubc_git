﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Video_List.aspx.cs" Inherits="Video_List" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoQuery(code) {
        $("#<%=txtSearchWord.ClientID %>").val(code);
        $("#<%= btnSearch.ClientID%>").click();
    }


    function DoReg() {
        location.href = "Video_Write.aspx";
    }
    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Button id="btnSearch" runat="server" OnClick="btnSearch_click" style="display:none"  />

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
            </td>
        </tr>
    </table>
    
    <div class="pad_B10" >   
        <span class="button xLarge icon" onclick="DoQuery('');"><span class="refresh"></span><button type="button"><%= Resources.Resource.txt_0030 %></button></span> 
        <asp:Repeater ID="rptProgramType" runat="server">
        <ItemTemplate>
            <span class="button xLarge icon" onclick="DoQuery('<%# Eval("codeId")%>');"><span class="refresh"></span><button type="button"><%# Eval("codeNm")%></button></span>
        </ItemTemplate>
        </asp:Repeater>
    </div> 
    
    <div class='topOptionBtns' style="width:100%" >
        <%= Common.SetScriptBtn("REG") %>
    </div>    
    
    <div>       
        <!-- 콤보박스 옵션
                전체 
                제목
                내용
                등록자
                구분
            -->                   
        <asp:DropDownList ID="ddlSearchKind" runat="server" CssClass="selectType" style="display:none">
            <asp:ListItem Text="<%$ Resources: Resource, txt_0030 %>" Value=""></asp:ListItem>
            <asp:ListItem Text="<%$ Resources: Resource, txt_0093 %>" Value="title"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources: Resource, txt_0095 %>" Value="contents"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources: Resource, txt_0048 %>" Value="userNm"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources: Resource, txt_0103 %>" Value="contentCd" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtSearchWord" runat="server" CssClass="textType" Width="300px" style="display:none"/>
    </div>

    <asp:DataList ID="dList" runat="server" style="width:100%; margin-top:10px;" BorderWidth=0 RepeatColumns="5" CellPadding="5" CellSpacing="0" RepeatDirection="Horizontal" RepeatLayout="Table" 
    OnItemDataBound="dList_ItemDataBound" GridLines="Horizontal" ForeColor="#ffffff"  BorderColor="#ffffff" BackColor="#eeeeee" >
        
    <ItemTemplate>
    
        <table width="90%" style="text-align:center; padding:2px;">
        <tr>
            <td>
                <span style="cursor:pointer;" onclick="location.href='Video_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>'">
                    <asp:Image ID="img" Width="132px" Height="76px" runat="server" ImageUrl="/Asset/Images/body/no_img.gif" onerror="this.src='/Asset/Images/body/no_img.gif'"  />
                </span>
            </td>
        </tr>
        <tr>
            <td class="player_title" align="left" style="border-width: 0px 0px 1px 0px; border-bottom-style: solid;">
                <span style="cursor:pointer" onclick="location.href='Video_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>'"><%#Util.DB2HTML(Eval("title").ToString()) %></span>
            </td>
        </tr>
        <tr>
            <td class="player_writer" align="left">- <%= Resources.Resource.txt_0048 %><!-- 등록자명 //--> : <%#Util.DB2HTML(Eval("userName").ToString())%><br />
            - <%= Resources.Resource.txt_0096 %><!-- 등록일 //--> : <%#Eval("createDate", "{0:yyyy-MM-dd}")%></td>
        </tr>
        <tr>
            <td class="player_count" align="left">- <%= Resources.Resource.txt_0085 %><!-- 조회수 //--> : <%#Eval("readCount")%></td>
        </tr>
        </table>
        
    </ItemTemplate>
    
    </asp:DataList> 
    
    <asp:Panel ID="pnlMsg" runat="server">
        <table style="width:100%;height:200px;margin-top:10px" class="boardInputType">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>
    </asp:Panel>   
    
    <div id="pagenum2" style="margin-top:15px">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>     
       	
</asp:Content>

