﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Notice_List.aspx.cs" Inherits="Notice_List" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoSearch() {
        $("#<%= btnSearch.ClientID%>").click();
    }

    function DoReg() {
        location.href = "Notice_Write.aspx";
    }
    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Button id="btnSearch" runat="server" OnClick="btnSearch_click" style="display:none"  />

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
            </td>
        </tr>
    </table>
        
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn("SEARCH#REG") %>
    </div>    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pad_B10">
    <tr>
    <td width="7"><img src="/Asset/Images/body/box_left.gif" /></td>
    <td style="background:url(/Asset/Images/body/box_bg.gif) repeat-x">
        <div  class="pad_L10">
        
            <!-- 콤보박스 옵션
                전체 
                제목
                내용
                등록자
            -->
            <asp:DropDownList ID="ddlSearchKind" runat="server" CssClass="selectType">
                <asp:ListItem Text="<%$ Resources: Resource, txt_0030 %>" Value=""></asp:ListItem> 
                <asp:ListItem Text="<%$ Resources: Resource, txt_0093 %>" Value="title"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: Resource, txt_0095 %>" Value="contents"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: Resource, txt_0048 %>" Value="userNm"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearchWord" runat="server" CssClass="textType" Width="300px" />
        </div>
    </td>
    <td width="8"><img src="/Asset/Images/body/box_right.gif" /></td>
    </tr>
    </table>
    
    <!--  GridView 컬럼명
        번호 
        제목
        첨부
        등록자
        등록일
        조회수
    //-->        
    <asp:GridView ID="gvList" runat="server" rules="all" Width="100%" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false"  CssClass="boardListType">
    <Columns>
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0092 %>"> 
            <HeaderStyle Width="5%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#(this.TOTAL_ROW_COUNT - ((this.CURRENT_PAGE - 1) * this.PAGE_SIZE + Container.DataItemIndex))%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0093 %>" HeaderStyle-CssClass="center">
            <HeaderStyle  />
            <ItemTemplate>
                <a href="Notice_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>"><%#Util.DB2HTML(Eval("title").ToString())%></a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0094 %>" HeaderStyle-CssClass="center">
            <HeaderStyle Width="9%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField  DataField="userName" HeaderText="<%$ Resources : Resource, txt_0048 %>" HeaderStyle-Width="15%" ItemStyle-CssClass="center" />
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0096 %>">
            <HeaderStyle Width="10%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("createDate", "{0:yyyy-MM-dd}")%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0085 %>">
            <HeaderStyle Width="8%" />
            <ItemStyle CssClass="align_C" />
            <ItemTemplate>
                <%#Eval("readCount", "{0:n0}")%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        <table style="width:100%;height:200px">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    </asp:GridView>   
                
    <div id="pagenum2" style="margin-top:15px">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>     
       	
</asp:Content>

