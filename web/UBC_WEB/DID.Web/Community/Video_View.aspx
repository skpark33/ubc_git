﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Video_View.aspx.cs" Inherits="Video_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Innorix/InnoFD/InnoFD.js"></script>

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/Common/jwplayer.js"></script>
<script type="text/javascript" src="/Common/FLVPDF.js"></script>

<script language="javascript" type="text/javascript">
    function DoList() {
        location.href = "Video_List.aspx?pgn=<%=this.Pgn%>";
    }
    function DoUpdate() {
        location.href = "Video_Write.aspx?boardId=<%=this.BoardID%>&pgn=<%=this.Pgn%>";
    }
    function DoDelete() {
        if (confirm("<%= Resources.Resource.msg_0017 %>")) { // 삭제하시겠습니까?
            $("#<%=btnDelete.ClientID%>").click();
        }
    }

//    jwplayer('mediaspace').setup({
//        'flashplayer': 'player.swf',
//        'file': 'http://content.longtailvideo.com/videos/flvplayer.flv',
//        'controlbar': 'bottom',
//        'width': '470',
//        'height': '320'
//    });

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:HiddenField ID="hdnBoardId" runat="server" />
    <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" style="display:none" />
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
            </td>
        </tr>
    </table>
    
    <asp:FormView ID="fvBoard" runat="server" style="width:100%">
    <ItemTemplate>
        <div class='topOptionBtns'>
            <%= Common.SetScriptBtn("LIST#UPD#DEL")%>
        </div>
        <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
        <colgroup>
	        <col width="15%" />
	        <col width="auto" />
	        <col width="15%" />
	        <col width="auto" />  
	        <col width="15%" />
	        <col width="auto" />  
        </colgroup>
        <tr>
            <th><%= Resources.Resource.txt_0093 %><!-- 제목 //--></th>
            <td colspan="5">
                <%# Util.DB2HTML(Eval("title").ToString()) %>
            </td> 
        </tr> 
            <th><%= Resources.Resource.txt_0103 %><!-- 구분 //--></th>
            <td>
                <%# Util.DB2HTML(Eval("contentNm").ToString())%>
            </td> 
            <th><%= Resources.Resource.txt_0048 %><!-- 등록자명 //--></th>
            <td>
                <%# Util.DB2HTML(Eval("userName").ToString())%>
            </td>
            <th><%= Resources.Resource.txt_0096 %><!-- 등록일 //--></th>
            <td>
                <%# Eval("createDate" ,"{0:yyyy-MM-dd}") %>
            </td>
        </tr>
        <tr>
            <th><%= Resources.Resource.txt_0097 %><!-- 목록 상단 위치 //--></th>
            <td>
                <%# Convert.ToBoolean(Eval("topYn")) ? "YES" : "NO"%>
            </td>
            <th><%= Resources.Resource.txt_0085 %><!-- 조회수 //--></th>
            <td colspan="3">
                <%# Eval("readCount" ,"{0:n0}") %>
            </td>
        </tr> 
        <tr>
            <th><%= Resources.Resource.txt_0104 %><!-- 동영상 화일 //--></th>
            <td colspan="5">&nbsp;            
                <div class="smartOutput">
                
                    <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' width='660' height='350' id='single1' name='single1'>
                    <param name='movie' value='/Common/player.swf'>
                    <param name='allowfullscreen' value='true'>
                    <param name='allowscriptaccess' value='always'>
                    <param name='wmode' value='transparent'>
<param name='flashvars' value='file=<%= this.VIDEO_FILE_NAME %>'>
                    <embed
                      id='single2'
                      name='single2'
                      src='/Common/player.swf'
                      width='660'
                      height='350'
                      bgcolor='#000000'
                      allowscriptaccess='always'
                      allowfullscreen='true'
  flashvars='file=<%= this.VIDEO_FILE_NAME %>'
                    />
                    </object>

                </div>
            </td>
        </tr> 
        <tr>
            <th><%= Resources.Resource.txt_0095 %><!-- 내용 //--></th>
            <td colspan="5" >
                <div class="smartOutput">
                <%# Eval("contents") %>
                </div>
            </td>
        </tr> 
        </table>
    </ItemTemplate> 
    <EmptyDataTemplate>               
        <div class='topOptionBtns'>
            <%= Common.SetScriptBtn_nonAuth("LIST")%>        
        </div>  
         <table style="width:100%;height:200px;margin-top:10px" class="boardInputType">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>      
    </EmptyDataTemplate>
    </asp:FormView>
        
</asp:Content>

