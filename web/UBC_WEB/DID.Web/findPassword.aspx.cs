﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using DID.Common.Framework;

using System.Net.Mail;

public partial class findPassword : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.txt_0090; // 비밀번호 찾기      

        if (!this.IsPostBack)
        {
            //foreach( string temp in Request.ServerVariables )
            //    Response.Write(temp + "=" + Request.ServerVariables[temp] + "<br/>");

            if (!Request["npw"].IsNullOrEmpty())
                hiddenNewPassword.Value = Common.Base64Decode(Request["npw"]);
        }
    }

    // 암호변경 버튼 클릭이벤트 핸들러
    protected void BtnSaveClick_Click(object sender, EventArgs e)
    {
        hiddenResult.Value = "";
        lblResultMsgReg.Text = "";

        using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
        {
            // 새로운 비밀번호 생성
            DataTable dt = obj.UpdatePassword(  txtUserIdReg.Text
                                              , txtUserEmailReg.Text
                                              , hiddenNewPassword.Value
                                              , txtUserPw1.Text
                                             );

            if (dt.Rows.Count > 0)
            {
                hiddenResult.Value = "OK";
            }
            else
                lblResultMsgReg.Text = Resources.Resource.txt_0113; // 아이디 혹은 이메일 주소가 정확하지 않습니다.

            lblResultMsgReg.Text += "<br />" + Resources.Resource.txt_0116; // 다시 한번 시도해 보시고 계속 같은 오류가 나올경우 담당자에게 연락하시기 바랍니다.
        }
    }

    protected void ibtnFindPassword_Click(object sender, EventArgs e)
    {
        hiddenResult.Value = "";
        lblResultMsg.Text = "";

        using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
        {
            // 새로운 비밀번호 생성
            DataTable dt = obj.GetNewPassword(txtUserId.Text, txtUserEmail.Text);
        
            if( dt.Rows.Count > 0 )
            {
                // 새로운 비밀번호 이메일로 전송
                if( sendNewPasswordViaEmail(txtUserEmail.Text, dt.Rows[0][0].ToString()) )
                {
                    hiddenResult.Value = "OK";
                    return;
                }
                else
                {
                    lblResultMsg.Text = Resources.Resource.txt_0114; // 이메일 전송 중 오류가 발생했습니다.
                }
            }
            else
                lblResultMsg.Text = Resources.Resource.txt_0115; // 입력하신 이메일 주소가 처음 등록시의 이메일과 일치하지 않습니다.
        }

        lblResultMsg.Text += "<br />" + Resources.Resource.txt_0116; // 다시 한번 시도해 보시고 계속 같은 오류가 나올경우 담당자에게 연락하시기 바랍니다.
    }
    
    protected bool sendNewPasswordViaEmail(string receiverEmail, string newPassword)
    {
        string senderEmail = "ubc@sqisoft.com";
        string subject = Resources.Resource.txt_0090; // 비밀번호 찾기      

        StringBuilder sbContent = new StringBuilder();
        sbContent.AppendLine("<html><head><title>");
        sbContent.AppendLine(Resources.Resource.txt_0090); // 비밀번호 찾기
        sbContent.AppendLine("</title></head><body>");
        sbContent.AppendLine(Resources.Resource.txt_0118); // 안녕하세요 UBC 담당자 입니다. 아래 링크로 이동하셔서 비밀번호를 바로 변경해주시기 바랍니다. 감사합니다.
        sbContent.AppendLine("<br />");
        sbContent.AppendLine(string.Format("<a href='http://{0}/findpassword.aspx?npw={1}' target='_blank'>{2}</a>"
                                            , Request.ServerVariables["HTTP_HOST"]
                                            , Common.Base64Encode(newPassword)
                                            , Resources.Resource.txt_0117 // 비밀번호설정 페이지로 이동
                                          )
                           );
        sbContent.AppendLine("</body></html>");
        string content = sbContent.ToString();

        SmtpClient objSend = new SmtpClient("127.0.0.1");
        //SmtpClient objSend = new SmtpClient("211.232.57.219"); // 개발서버

        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(senderEmail);
        mail.To.Add(receiverEmail);

        mail.Subject = subject;

        // 줄바꿈은 <br> 태그로 
        mail.Body = content;

        mail.SubjectEncoding = System.Text.Encoding.UTF8;
        mail.BodyEncoding = System.Text.Encoding.UTF8;
        mail.IsBodyHtml = true; // HTML 형식 사용함 ==true

        //http://blog.naver.com/bamb99/100106464276


        string success_yn = "";
        try
        {
            objSend.Send(mail);
            success_yn = "Y";

        }
        catch
        {

            success_yn = "N";

        }

        // 로그 남기기
        using (DID.Service.Manager.Smtp obj = new DID.Service.Manager.Smtp())
        {
            obj.smtpLogInsert("CHANGE PASSWORD" // 원래는 company 구분(Profile.COMP_TYPE) 이 첫번째 인자인데 로그인을 한상태가 아니므로 그 값을 알수없다 "CHANGE PASSWORD" 로 셋팅
                            , senderEmail
                            , receiverEmail
                            , subject
                            , content
                            , success_yn);
        }


        return (success_yn=="Y")? true : false;
    }

}
