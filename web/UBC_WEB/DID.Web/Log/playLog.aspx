﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="playLog.aspx.cs" Inherits="playLog" Title="제목 없음" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoExcel() {
        $("#<%= btnExcel.ClientID%>").click();
    }

    function DoSetup() {
        window.open("./log_setup.aspx", "log_setup", "toolbar=0,scrollbars=0,location=0,directories=0,status=0,menubar=0,resizable=no,width=480,height=340");
    }

    function DoSelect() {

        //////////////////////
        // 날짜 유효성 검사 시작

        var startDate = $("#<%= fromDate.ClientID%>").val();
        var endDate = $("#<%= toDate.ClientID%>").val();

        // 시작날짜 문자열에서 년,월,일 을 뽑아 숫자로 변환
        var yearStart = parseInt(startDate.substr(0, 4), 10);
        var monthStart = parseInt(startDate.substr(5, 2), 10);
        var dayStart = parseInt(startDate.substr(8), 10);

        // 종료날짜 문자열에서 년,월,일 을 뽑아 숫자로 변환
        var yearEnd = parseInt(endDate.substr(0, 4), 10);
        var monthEnd = parseInt(endDate.substr(5, 2), 10);
        var dayEnd = parseInt(endDate.substr(8), 10);

        // 날짜 비교
        var dStart = new Date(yearStart, monthStart, dayStart);
        var dEnd = new Date(yearEnd, monthEnd, dayEnd);

        if (dStart > dEnd) {
            alert("<%= Resources.Resource.msg_0040 %>"); // 종료일자가 시작일자보다 앞설 수 없습니다
            return false;
        }

        // 날짜 유효성 검사 끝
        //////////////////////

        return true;
      
    }

    
     
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
 <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
  <colgroup>
  <col width="auto" />
  <col width="50%" />
  </colgroup>
  <tr>
    <td><span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span> </td>
  </tr>
</table>
    <div class='topOptionBtns'>
        <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources: Resource, btn_0007 %>" OnClick="btnSelect_Click" OnClientClick='return DoSelect()' /> <!-- 조회 -->
        <asp:Button ID="btnExcel" runat="server" Text="<%$ Resources: Resource, btn_0006 %>" OnClick="btnExcel_Click" Visible=false  /> <!-- 엑셀저장 -->
        <%--<asp:ImageButton ID="btnSetup"  runat="server" ToolTip="설정" OnClientClick="DoSetup()"  ImageUrl="~/Asset/Images/button/btn_setup.jpg"  /> --%>
        
    </div>    

    <table class="boardInputType mb10" cellpadding="0" cellspacing="0" border="0">
		<colgroup>
		<col width="120px" />
		<col width="auto" />
		<col width="150px" />
		<col width="auto" />		
	</colgroup>
    <tr>
        <th><span><%= Resources.Resource.txt_0044 %><!-- 단말ID --></span></th>
        <td>    
            <asp:TextBox ID="txtHostId" runat="server"  CssClass="textType"></asp:TextBox>
        </td>
        <th><span><%= Resources.Resource.txt_0049 %><!-- 조회기간 --></span></th>
        <td>        
          <asp:TextBox ID="fromDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);" onkeydown="return false;" />
            ~
          <asp:TextBox ID="toDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  onkeydown="return false;"  />                    
        </td>            
    </tr>
    </table>
    
    
    
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate> 
        <div class="container" >
        <!--   CssClass="boardListType" AllowSorting=true  onsorting="gvList_Sorting"   -->
        <asp:GridView ID="gvList" runat="server" OnPreRender="gvList_PreRender" AutoGenerateColumns="false" ShowHeader="true" CssClass="boardListType2"  >
        <Columns>  
        
            <asp:BoundField DataField="siteId"          HeaderText="<%$ Resources: Resource, txt_0002 %>" HeaderStyle-Width="120px" ItemStyle-HorizontalAlign=Center SortExpression="siteId" ItemStyle-VerticalAlign=Top />
            <asp:BoundField DataField="siteName"        HeaderText="<%$ Resources: Resource, txt_0003 %>"   HeaderStyle-Width="120px" ItemStyle-HorizontalAlign=Center SortExpression="siteName" ItemStyle-VerticalAlign=Top  />
            <asp:BoundField DataField="hostId"          HeaderText="<%$ Resources: Resource, txt_0044 %>"     HeaderStyle-Width="120px" ItemStyle-HorizontalAlign=Center SortExpression="hostId" ItemStyle-VerticalAlign=Top />
            <asp:BoundField DataField="playDate"        HeaderText="<%$ Resources: Resource, txt_0072 %>"     HeaderStyle-Width="120px" ItemStyle-HorizontalAlign=Center SortExpression="playDate" ItemStyle-VerticalAlign=Top />                       
            <asp:BoundField DataField="programId"       HeaderText="<%$ Resources: Resource, txt_0045 %>"   HeaderStyle-Width="150px" ItemStyle-HorizontalAlign=Left   SortExpression="programId" />
            <%--<asp:BoundField DataField="contentsId"      HeaderText="콘텐츠ID"                             ItemStyle-HorizontalAlign=Left   SortExpression="contentsId" />--%>
            <asp:BoundField DataField="contentsName"    HeaderText="<%$ Resources: Resource, txt_0069 %>"                             ItemStyle-HorizontalAlign=Left   SortExpression="contentsName" />
            <asp:BoundField DataField="playCount"       HeaderText="<%$ Resources: Resource, txt_0070 %>"   HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign=Right  SortExpression="playCount" />
            <asp:BoundField DataField="failCount"       HeaderText="<%$ Resources: Resource, txt_0071 %>" HeaderStyle-Width="80px"  ItemStyle-HorizontalAlign=Right  SortExpression="failCount" />
            
        </Columns>
        <EmptyDataTemplate>
            <table style="width:100%;height:290px;">
                <tr>
                    <td style="text-align:center">
                        <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        
        </asp:GridView>   
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

