﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using DID.Common.Framework;

public partial class bpLog : BasePage
{ 
   
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0007; // 방송계획 로그

        if (!this.IsPostBack)
        {
            txtSiteId.Focus();
            this.InitControl(); 
        }
    }

        //초기화
    private void InitControl()
    {
        // fromdate 를 -3일 로 세팅한다.
        fromDate.Text = DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd");

        // todate 를 오늘날짜로 세팅한다.
        toDate.Text   = DateTime.Now.ToString("yyyy-MM-dd");
    }
     

    //엑셀
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        string fileName = this.Title + ".xls";
        fileName = fileName.Replace(' ', '_');

        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
        Response.Charset = "";
        this.EnableViewState = false;

        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n");

        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
        gvList.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    // 엑셀 저장을 위해 재정의해야함 (지우면 에러남)
    public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    {
        // Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time. 
    }

    // 조회
    protected void btnSelect_Click(object sender, EventArgs e)
    {
        getDateList();
    }


    private void getDateList()
    {
        using (DID.Service.Log.Log obj = new DID.Service.Log.Log())
        {
            DataTable dt = null;
            string fromDateVar = fromDate.Text.Replace("-", "");
            string toDateVar = toDate.Text.Replace("-", "");

            dt = obj.GetBpLogList(txtSiteId.Text, fromDateVar, toDateVar);
            this.gvList.DataSource = dt;
            this.gvList.DataBind();

            // 데이타 여부에 따라 엑셀저장 버튼 활성/비활성
            if (dt.Rows.Count > 0)
            {
                btnExcel.Enabled = true;
                btnExcel.Visible = true;
            }
            else
            {
                btnExcel.Enabled = false;
                btnExcel.Visible = false;
            }
        }
    }

    // 그리드뷰 셀병합
    protected void gvList_PreRender(object sender, EventArgs e)
    {

        Common.GroupColumn(gvList, 0); //그리드뷰의 ID, 병합할 컬럼
        Common.GroupColumn(gvList, 1, 0); //그리드뷰의 ID, 병합할 컬럼

    }
}
