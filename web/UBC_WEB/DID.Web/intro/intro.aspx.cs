﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;

public partial class intro_intro : BasePage
{
    private int ROW_COUNT = 3;

    protected string UBC_PLAYER { get; set; }
    protected string UBC_MANAGER { get; set; }
    protected string UBC_STUDIO { get; set; }

    protected string UBC_PLAYER_MANUAL{ get; set; }
    protected string UBC_MANAGER_MANUAL { get; set; }
    protected string UBC_STUDIO_MANUAL{ get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.DataControlBind();
        }  
    }

     private void DataControlBind()
    {
        DataTable dt = this.GetBoardList();

        DataView dv = null;
        dv = dt.DefaultView;
        dv.RowFilter = "topYn = 0 ";
        dv.Sort = "createDate desc";
        rptNotice.DataSource = dv;
        rptNotice.DataBind();
        //pnlMsg1.Visible = dv.Count == 0;

        DataTable dtFile = this.GetIntroAttachList();

        if (dtFile.Rows.Count > 0)
        {
            DataView dvFile = dtFile.DefaultView;

            //UBC 플레이어
            dvFile.RowFilter = string.Format("boardMasterId = {0} AND contentCd = '{1}'", (int)Common.BoardMasterID.Program, "0");
            dvFile.Sort = "updateDate desc";
            if (dvFile.Count > 0) this.UBC_PLAYER = dvFile[0]["attachId"].ToString();
            //UBC 매니져
            dvFile.RowFilter = string.Format("boardMasterId = {0} AND contentCd = '{1}'", (int)Common.BoardMasterID.Program, "1");
            dvFile.Sort = "updateDate desc";
            if (dvFile.Count > 0) this.UBC_MANAGER = dvFile[0]["attachId"].ToString();
            //UBC 스튜디오
            dvFile.RowFilter = string.Format("boardMasterId = {0} AND contentCd = '{1}'", (int)Common.BoardMasterID.Program, "2");
            dvFile.Sort = "updateDate desc";
            if (dvFile.Count > 0) this.UBC_STUDIO = dvFile[0]["attachId"].ToString();

            //UBC 플레이어 매뉴얼
            dvFile.RowFilter = string.Format("boardMasterId = {0} AND contentCd = '{1}'", (int)Common.BoardMasterID.Manual, "0");
            dvFile.Sort = "updateDate desc";
            if (dvFile.Count > 0) this.UBC_PLAYER_MANUAL = dvFile[0]["attachId"].ToString();
            //UBC 매니져 매뉴얼
            dvFile.RowFilter = string.Format("boardMasterId = {0} AND contentCd = '{1}'", (int)Common.BoardMasterID.Manual, "1");
            dvFile.Sort = "updateDate desc";
            if (dvFile.Count > 0) this.UBC_MANAGER_MANUAL = dvFile[0]["attachId"].ToString();
            //UBC 스튜디오 매뉴얼
            dvFile.RowFilter = string.Format("boardMasterId = {0} AND contentCd = '{1}'", (int)Common.BoardMasterID.Manual, "2");
            dvFile.Sort = "updateDate desc";
            if (dvFile.Count > 0) this.UBC_STUDIO_MANUAL = dvFile[0]["attachId"].ToString();

        }

    }

    #region 게시물 데이터 조회 - GetBoardList()
    /// <summary>
    /// 게시물 데이터 조회
    /// </summary>
    /// <returns></returns>
    private DataTable GetBoardList()
    {
        int TotalRows = 0;

        using (DID.Service.Community.Board obj = new DID.Service.Community.Board())
        {
            //공지사항
            return obj.GetList((int)Common.BoardMasterID.Notice, this.Profile.SITE_ID, "", "", "", 1, this.ROW_COUNT, out TotalRows, Profile.COMP_TYPE);
        }
    }
    #endregion

    #region 게시물 첨부파일 조회 - GetBoardList()
    /// <summary>
    /// 게시물 첨부파일 조회
    /// </summary>
    /// <returns></returns>
    private DataTable GetIntroAttachList()
    {
        using (DID.Service.Community.Board obj = new DID.Service.Community.Board())
        {
            return obj.GetIntroAttachList();
        }
    }
    #endregion


}
