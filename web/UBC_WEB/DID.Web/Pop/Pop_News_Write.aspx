﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" EnableSessionState="False" ValidateRequest="false" CodeFile="Pop_News_Write.aspx.cs" Inherits="Pop_News_Write" %>
<%@ Register TagPrefix="Upload" Namespace="Brettle.Web.NeatUpload" Assembly="Brettle.Web.NeatUpload" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Asset/Editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<link href="/Asset/Editor/css/default.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">


    function ValidateForm() {
        if (Trim($("#<%= txtTitle.ClientID%>").val()) == "") {
            alert("<%= Resources.Resource.msg_0044 %>"); // "제목을 입력하십시요
            $("#<%= txtTitle.ClientID%>").focus();
            return false;
        }
        
        return true;
    }

    function DoSave() {
        if (ValidateForm()) {
             $("#<%= btnSave.ClientID%>").click();
        }
    }

    function DoList() {
        location.href = "Pop_News_List.aspx";
    }

</script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnContents" runat="server"  />
    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" style="display:none" />
    <input type="hidden" id="hndImagFileName" name="hndImagFileName" />
     
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
            </td>
        </tr>
    </table>
    
    <div class='topOptionBtns'>
        <%= Common.SetScriptBtn_nonAuth("SAVE#LIST")%>
    </div>    
    
    <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
    <colgroup>
	    <col width="18%" />
	    <col width="auto" /> 
    </colgroup>
    <tr>
        <th><%= Resources.Resource.txt_0093 %><!-- 제목 //--><font color="red"> *</font></th>
        <td> 
            <asp:TextBox ID="txtTitle" runat="server"  CssClass="textType" Width="660px" />
        </td>
    </tr> 
    <tr>
        <th><%= Resources.Resource.txt_0141%><!-- 관리자 POP 보이기 //--></th>
        <td > 
            <asp:RadioButtonList ID="rdoShowAdminYn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radioType"  >
                <asp:ListItem Text="YES" Value="Y" Selected="True" />
                <asp:ListItem Text="NO" Value="N"  />
            </asp:RadioButtonList>             
        </td>
    </tr>   
    <tr id="trFileView" runat="server" visible="false">
        <th><%= Resources.Resource.txt_0100 %><!-- 첨부 화일 //--></th>
        <td style="text-align:left" >
            <asp:Repeater ID="rptFile" runat="server">
            <ItemTemplate>
                <input type="checkbox" value="<%# Eval("attachId") %>|<%# Eval("fileName") %>" name="_delete_file" /><%# Eval("fileName")%><br />        
            </ItemTemplate>
            </asp:Repeater><p></p>
            <span style="color:#8b0000"><%= Resources.Resource.txt_0101 %><!-- ◆ 삭제하려면 해당 파일을 체크하고 저장하면 됩니다. //--></span> 
        </td>
    </tr>
    <tr>
        <th><%= Resources.Resource.txt_0094 %><!-- 첨부 //--></th>
        <td align="left" >
            <span style="color:#008b00; font-weight:bold;">Recommended Resolution: 770*1370<br />Digital Gallery file format: jpg,gif,png,swf,flv</span>
			<hr />
	        <Upload:MultiFile id="multiFile" runat="server">
		        <asp:Button id="multiFileButton" Text="<%$ Resources : Resource, txt_0099 %>" Enabled="<%# multiFile.Enabled %>" runat="server"/> <!-- 찾아보기 //-->		        
	        </Upload:MultiFile>	        
			<div style="display:none;">
			    <Upload:ProgressBar id="inlineProgressBar" runat="server" Inline="true" height="50px" Width="630px" Triggers="btnSave">
			    <asp:Label id="label" runat="server" Text="Check Progress"/>
			    </Upload:ProgressBar>
			</div>

        </td>
    </tr> 
    </table> 
    

</asp:Content>

