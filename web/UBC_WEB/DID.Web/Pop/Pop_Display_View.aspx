﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="Pop_Display_View.aspx.cs" Inherits="Pop_Display_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript" src="/Common/jwplayer.js"></script>

<script language="javascript" type="text/javascript">
    function DoList() {
        location.href = "Pop_Display_List.aspx?pgn=<%=this.Pgn%>";
    }
    function DoUpdate() {
        location.href = "Pop_Display_Write.aspx?boardId=<%=this.BoardID%>&pgn=<%=this.Pgn%>";
    }
    function DoDelete() {
        if (confirm("<%= Resources.Resource.msg_0017 %>")) { // 삭제하시겠습니까?
            $("#<%=btnDelete.ClientID%>").click();
        }
    }
    function GoUp(displayOrder) {
        if (displayOrder-1 < 0) return;
        
        $("#<%=hdnDisplayOrder.ClientID %>").val(displayOrder);
        $("#<%=btnUp.ClientID%>").click();
    }
    function GoDown(displayOrder) {
        var popCnt = $("#<%=hdnTotalPOPCount.ClientID %>").val();
        if (displayOrder+1 >= popCnt) return;
        
        $("#<%=hdnDisplayOrder.ClientID %>").val(displayOrder);
        $("#<%=btnDown.ClientID%>").click();
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

    <asp:HiddenField ID="hdnBoardId" runat="server" />
    <asp:HiddenField ID="hdnDisplayOrder" runat="server" />
    <asp:HiddenField ID="hdnTotalPOPCount" runat="server" />
    
    <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" style="display:none" />    
    <asp:Button ID="btnUp" runat="server" OnClick="btnUp_Click" style="display:none" />
    <asp:Button ID="btnDown" runat="server" OnClick="btnDown_Click" style="display:none" />
    
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
    <tr>
        <td>
            <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
        </td>
    </tr>
    </table>
    
    <asp:FormView ID="fvBoard" runat="server" style="width:100%">
    <ItemTemplate>
        <div class='topOptionBtns'>
            <%= Common.SetScriptBtn_nonAuth("LIST#UPD#DEL")%>        
        </div> 
        <table class="boardInputType" cellpadding="0" cellspacing="0" border="0">
        <colgroup>
	        <col width="25%" />
	        <col width="auto" />
	        <col width="18%" />
	        <col width="auto" />  	       
        </colgroup>
        <tr>            
            <th ><%= Resources.Resource.txt_0093 %><!-- 제목 //--></th>
            <td>
                <%# Eval("title") %>
            </td>  
            <th><%= Resources.Resource.txt_0141%><!-- 관리자 POP 보이기 //--></th>
            <td >
                <%# Convert.ToBoolean(Eval("popYn")) ? "Yes" : "No" %>
            </td> 
        </tr>       
        <tr>
            <th><%= Resources.Resource.txt_0048 %><!-- 등록자명 //--></th>
            <td>
                <%# Eval("userName")%>
            </td>
            <th><%= Resources.Resource.txt_0096 %><!-- 등록일 //--></th>
            <td>
                <%# Eval("createDate" ,"{0:yyyy-MM-dd}") %>
            </td>
        </tr>
        <tr>
            <th valign=top>
                <span style="color:#008b00;"><%= Resources.Resource.txt_0144 %></span><!-- 오른쪽 목록에서 보여지는 순서대로 디지털 갤러리에 표시됩니다. 화살표 버튼을 이용하여 순서를 변경하세요. //-->
                <hr style="width:90%" />
                <span style="color:#0000cc">Recommended Resolution: 770*1370</span>
                <hr style="width:90%" />
                <span style="color:#cc0000;">Digital Gallery file format: jpg,gif,png,swf,flv</span>
                <hr style="width:90%" />
            </th>
            <td colspan="3" align="left">
                <asp:Repeater ID="rptFile" runat="server" OnItemDataBound="RptFile_itemDataBound">
                <ItemTemplate>                         
                    <asp:HiddenField ID="hdnAttachId" runat="server" Value='<%# Eval("attachId") %>' />
                    
                    <table style="border-style: none; border-width: 0px">
                    <tr >
                        <td rowspan="2" style="border-style: solid; border-width: 0px 1px 0px 0px">
                            <span class="button large" onclick="GoUp('<%# Eval("displayOrder") %>')"><button type="button" class="up">▲</button></span> 
                            <br />                                
                            <span class="button large" onclick="GoDown('<%# Eval("displayOrder") %>')"><button type="button">▼</button></span>                                     
                        </td>
                        <td style="border-style: none; border-width: 0px">
                            File name: <span style="cursor:pointer" onclick="location.href='/Common/download.aspx?attachId=<%# Eval("attachId") %>&boardMasterId=<%=(int)Common.BoardMasterID.PopDisplay %>'"><%# Eval("fileName")%></span>
                            <br />
                            <asp:Label ID="Label_Image_size" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-style: none; border-width: 0px">
                        
                            <asp:Panel ID="pnlImage" runat="server">
                                <p style="background-color:black; vertical-align:top; width:<%= IMAGE_FRAME_WIDTH %>px; height:<%= IMAGE_FRAME_HEIGHT %>px;">
                                    <asp:Image ID="Image_pop" runat="server" BorderWidth="0" />                                        
                                </p>                                    
                            </asp:Panel>   
                                
                            <asp:Panel ID="pnlVideo" runat="server">
                                <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"  width=<%= IMAGE_FRAME_WIDTH %> height=<%= IMAGE_FRAME_HEIGHT %>">
                                    <param name="movie" value="/Common/player.swf">
                                    <param name=bgcolor value=#000000>
                                    <param name="allowfullscreen" value="true">
                                    <param name="allowscriptaccess" value="always">                                       
                                    <param name='flashvars' value='file=<asp:Literal ID="Literal1" runat="server"></asp:Literal>'>
                                    <embed
                                          src='/Common/player.swf'
                                          width="<%= IMAGE_FRAME_WIDTH %>" 
                                          height="<%= IMAGE_FRAME_HEIGHT %>"
                                          bgcolor='#000000'                                          
                                          allowfullscreen='true'
                                          allowscriptaccess='always'
                                          flashvars='file=<asp:Literal ID="Literal2" runat="server"></asp:Literal>'
                                    />                                        
                                </object> 
                            </asp:Panel> 
                            
                            <asp:Panel ID="pnlFlash" runat="server">                                    
                                <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="<%= IMAGE_FRAME_WIDTH %>" height="<%= IMAGE_FRAME_HEIGHT %>" >
                                    <param name="movie" value='<asp:Literal ID="Literal3" runat="server"></asp:Literal>'></param>
                                    <param name="quality" value="high"></param>
                                    <param name="bgcolor" value="#000000"></param>
                                    <embed 
                                        src='<asp:Literal ID="Literal4" runat="server"></asp:Literal>'
                                        width="<%= IMAGE_FRAME_WIDTH %>" 
                                        height="<%= IMAGE_FRAME_HEIGHT %>"
                                        quality="high"
                                        bgcolor="#000000"
                                        type="application/x-shockwave-flash" 
                                    />
                                </OBJECT>
                            </asp:Panel> 
                        </td>
                        </tr>                                                  
                    </table>  
                </ItemTemplate>
                <SeparatorTemplate>
                <hr />
                </SeparatorTemplate>    
                </asp:Repeater>
                &nbsp;
            </td>
        </tr>         
        </table>
    </ItemTemplate> 
    <EmptyDataTemplate>               
        <div class='topOptionBtns'>
            <%= Common.SetScriptBtn_nonAuth("LIST")%>        
        </div>  
         <table style="width:100%;height:200px;margin-top:10px" class="boardInputType">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>      
    </EmptyDataTemplate>
    </asp:FormView>

</ContentTemplate>
</asp:UpdatePanel>    
</asp:Content>

