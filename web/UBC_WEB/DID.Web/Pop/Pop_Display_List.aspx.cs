﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

/// <summary>
/// 목록 페이지
/// </summary>
public partial class Pop_Display_List : BasePage
{
    /// <summary>
    /// 페이지당 목록 수
    /// </summary>
    protected int PAGE_SIZE = 10;
    /// <summary>
    /// 현재 페이지
    /// </summary>
    protected int CURRENT_PAGE
    {
        get { return Pager.CurrentPageIndex; }
        set { Pager.CurrentPageIndex = value; }
    }
    /// <summary>
    /// 목록수
    /// </summary>
    protected int TOTAL_ROW_COUNT { get; set; }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }    

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0032; // POP 관리 - DISPLAY

        this.PAGE_SIZE = ddlPageSize.SelectedValue.ToInt();

        if (!this.IsPostBack)
        {
            this.InitControl();
            this.DataBindControl();
        }
    }

    #region 컨트롤 초기화 - InitControl()
    /// <summary>
    /// 컨트롤 초기화
    /// </summary>
    private void InitControl()
    {
        if (!string.IsNullOrEmpty(this.Pgn))
            this.CURRENT_PAGE = Convert.ToInt32(this.Pgn);
    }
    #endregion

    #region 페이징 이벤트 - PagerPaging(object sender, EventArgs e)
    /// <summary>
    /// 페이징 이벤트
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void PagerPaging(object sender, EventArgs e)
    {
        this.DataBindControl();
    }
    #endregion

    #region 데이터 바인딩 - DataBindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void DataBindControl()
    {
        DataTable dt = this.GetBoardList();

        this.gvList.DataSource = dt;
        this.gvList.DataBind();

        this.Pager.PageSize = this.PAGE_SIZE;
        this.Pager.TotalRowCount = this.TOTAL_ROW_COUNT;
        this.Pager.DataBind();

        
    }
    #endregion

    #region 게시물 데이터 조회 - GetBoardList()
    /// <summary>
    /// 게시물 데이터 조회
    /// </summary>
    /// <returns></returns>
    private DataTable GetBoardList()
    {
        int TotalRows = 0;
        DataTable dt = null;

        using (DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard())
        {

            dt = obj.GetList((int)Common.BoardMasterID.PopDisplay
                            , this.Profile.SITE_ID
                            , this.Profile.USER_ID
                            , ""
                            , ""
                            , this.CURRENT_PAGE
                            , this.PAGE_SIZE
                            , out TotalRows
                            , Profile.COMP_TYPE);
        }

        this.TOTAL_ROW_COUNT = TotalRows;

        return dt;
    }
    #endregion

    #region 그리드뷰 gvList_RowDataBound()
    /// <summary>
    /// 그리드뷰 gvList_RowDataBound
    /// </summary>
    protected void gvList_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        DataRowView drv = (DataRowView)e.Row.DataItem;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string attachYn = drv["attachYn"].ToString();

            if (!string.IsNullOrEmpty(attachYn))
            {
                System.Web.UI.WebControls.Image attchImg;
                attchImg = new System.Web.UI.WebControls.Image();
                attchImg.ImageUrl = "/Asset/Images/body/file.gif";

                e.Row.Cells[2].Controls.Add(attchImg);
            }
        }
       
    }
    #endregion

    #region 검색 버튼 클릭 - btnSearch_click(object sender, EventArgs e)
    /// <summary>
    /// 검색 버튼 클릭 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_click(object sender, EventArgs e)
    {
        this.DataBindControl();
    }
    #endregion

    protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.CURRENT_PAGE = 1;
        this.DataBindControl();
    }
}
