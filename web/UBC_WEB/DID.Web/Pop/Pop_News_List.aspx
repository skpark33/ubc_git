﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="Pop_News_List.aspx.cs" Inherits="Pop_News_List" %>
<%@ Register TagPrefix="DID" Namespace="DID.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script language="javascript" type="text/javascript">

    function DoSearch() {
        $("#<%= btnSearch.ClientID%>").click();
    }

    function DoReg() {
        location.href = "Pop_News_Write.aspx";
    }
    

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Button id="btnSearch" runat="server" OnClick="btnSearch_click" style="display:none"  />

    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
        <colgroup>
            <col width="auto" />
            <col width="50%" />
        </colgroup>
        <tr>
            <td>
                <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %></span>
            </td>
        </tr>
    </table>
    
   <div id='regBtn' class='topOptionBtns'>
       <%= Resources.Resource.txt_0143 %><!-- 페이지별 목록수 //-->
       <asp:DropDownList ID="ddlPageSize" runat="server" onselectedindexchanged="ddlPageSize_SelectedIndexChanged" AutoPostBack=true>
           <asp:ListItem>10</asp:ListItem>
           <asp:ListItem>50</asp:ListItem>
           <asp:ListItem>100</asp:ListItem>
       </asp:DropDownList>
        <%= Common.SetScriptBtn_nonAuth("REG") %>       
    </div>
    
    
    <!--  GridView 컬럼명
        번호 
        제목
        첨부
        등록자
        등록일
    //-->  
    <asp:GridView ID="gvList" runat="server" rules="all" Width="100%" OnRowDataBound="gvList_RowDataBound" AutoGenerateColumns="false"  
    CssClass="boardListType">
    <Columns>
    
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0092 %>" ItemStyle-HorizontalAlign=Center>
            <HeaderStyle Width="40px" />
            <ItemTemplate>
                <%#(this.TOTAL_ROW_COUNT - ((this.CURRENT_PAGE - 1) * this.PAGE_SIZE + Container.DataItemIndex))%>
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0093 %>" >
            <ItemTemplate>
                <a href="Pop_News_View.aspx?boardId=<%#Eval("boardId") %>&pgn=<%#this.CURRENT_PAGE%>"><%#Eval("title") %></a>
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0094 %>" ItemStyle-HorizontalAlign=Center >
            <HeaderStyle Width="80px" />
            <ItemTemplate>
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0141 %>" ItemStyle-HorizontalAlign=Center>
            <HeaderStyle Width="140px" />
            <ItemTemplate>
                 <%# Convert.ToBoolean(Eval("popYn")) ? "Yes" : "No" %> 
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:BoundField  DataField="userName" HeaderText="<%$ Resources : Resource, txt_0048 %>" HeaderStyle-Width="140px" ItemStyle-HorizontalAlign=Center />
        
        <asp:TemplateField HeaderText="<%$ Resources : Resource, txt_0096 %>" ItemStyle-HorizontalAlign=Center>
            <HeaderStyle Width="100px" />
            <ItemTemplate>
                <%#Eval("createDate", "{0:yyyy-MM-dd}")%>
            </ItemTemplate>
        </asp:TemplateField>
              
    </Columns>
    <EmptyDataTemplate>
        <table style="width:100%;height:200px">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
    </asp:GridView>   
                
   
     <div id="pagenum2" style="margin-top:15px">
       <DID:PagerControl ID="Pager" runat="server" OnPaging="PagerPaging"/>
    </div>  
       	
</asp:Content>

