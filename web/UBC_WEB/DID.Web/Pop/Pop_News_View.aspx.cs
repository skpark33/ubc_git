﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;
using System.Drawing;

/// <summary>
/// 조회 페이지
/// </summary>
public partial class Pop_News_View : BasePage
{
    protected const string REGEX_VALID_FILE_EXTENSION = @"(jpg|jpeg|gif|png|swf|flv)$";

    /// <summary>
    /// 게시글ID
    /// </summary>
    public string BoardID
    {
        get { return Request["boardId"] ?? ""; }
    }
    /// <summary>
    /// 게시물번호
    /// </summary>
    public string Thread
    {
        get;set;
    }
    /// <summary>
    /// 게시물 Depth
    /// </summary>
    public string Depth
    {
        get;set;
    }
    /// <summary>
    /// 현재 페이지링크
    /// </summary>
    public string Pgn
    {
        get { return Request["pgn"] ?? ""; }
    }
    /// <summary>
    /// 모드 : write,reply,update
    /// </summary>
    public Common.BoardMode Mode
    {
        get
        {
            if (!string.IsNullOrEmpty(this.BoardID) && string.IsNullOrEmpty(this.Thread))
                return Common.BoardMode.Update;
            else
                return Common.BoardMode.Write;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0031; // POP 관리 - NEWS

        if (!this.IsPostBack)
        {
            if (this.Mode == Common.BoardMode.Update)
            {
                this.BindControl();
            }
        }
    }

    #region 데이터 바인딩 - BindControl()
    /// <summary>
    /// 데이터 바인딩
    /// </summary>
    private void BindControl()
    {
        long boardId = Convert.ToInt64(this.BoardID);

        DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard();
        DataTable dtList = obj.GetDetail(boardId, this.Profile.COMP_TYPE);
        if (!this.IsPostBack) obj.UpdateReadCount(boardId);

        fvBoard.DataSource = dtList;
        fvBoard.DataBind();

        if (dtList.Rows.Count > 0)
        {
            this.Thread = dtList.ToDataRow()["thread"].ToString();
            this.Depth = dtList.ToDataRow()["depth"].ToString();
            this.hdnBoardId.Value = dtList.ToDataRow()["boardId"].ToString();
        }

        DataTable dtFile = obj.GetAttachList(boardId);

        if (dtFile.Rows.Count > 0)
        {
            Repeater rpt = (Repeater)(fvBoard.FindControl("rptFile"));

            if (!rpt.IsNullOrEmpty())
            {
                for (int i = dtFile.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = dtFile.Rows[i];

                    string fileName = dr["filename"].ToString().ToLower();

                    RegexStringValidator myRegexValidator = new RegexStringValidator(REGEX_VALID_FILE_EXTENSION);

                    try
                    {
                        // Attempt validation.
                        myRegexValidator.Validate(fileName);
                    }
                    catch (ArgumentException e)
                    {
                        // Validation failed.
                        dtFile.Rows.RemoveAt(i);
                    }
                }

                int displayOrder = 0;
                foreach (DataRow dr in dtFile.Rows)
                {
                    dr["displayOrder"] = displayOrder;
                    displayOrder++;
                }

                // 화살표 버튼을 이용하여 표시순서를 바꿀때 맨처음과 맨끝일 경우를 체크하기위해서 
                // 총 POP 수를 숨김 필드에 써놓는다
                hdnTotalPOPCount.Value = dtFile.Rows.Count.ToString();

                rpt.DataSource = dtFile;
                rpt.DataBind();
            }
        }

    }
    #endregion

    #region 삭제버튼 클릭 - btnDelete_Click(object sender, EventArgs e)
    /// <summary>
    /// 삭제버튼 클릭
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        using (DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard())
        {
            long boardId = Convert.ToInt64(this.hdnBoardId.Value);
            DataTable dt= obj.GetAttachList(boardId);
            Common.DeleteFile(dt, (int)Common.BoardMasterID.PopNews);
            obj.DeleteAttachByBoardId(boardId);
            obj.Delete(boardId);
        }

        this.ScriptExecuteUrl(Resources.Resource.msg_0047, "Pop_News_List.aspx"); // 삭제되었습니다
    }
    #endregion

    #region 이미지 미리보기에 쓰이는 상수

    public const int KIOSK_FRAME_WIDTH  = 770;
    public const int KIOSK_FRAME_HEIGHT = 1370;

    public const float IMAGE_REDUCE_PERCENTAGE = 27;
    public const int IMAGE_FRAME_WIDTH  = (int)(KIOSK_FRAME_WIDTH * IMAGE_REDUCE_PERCENTAGE / 100);
    public const int IMAGE_FRAME_HEIGHT = (int)(KIOSK_FRAME_HEIGHT * IMAGE_REDUCE_PERCENTAGE / 100);

    #endregion
    
    #region 이미지 사이즈 알아오기

    private Size getImageSize(string imageURL)
    {
        Size returnImageSize = new Size(0, 0);

        DID.Common.Framework.Impersonater impersonater = new DID.Common.Framework.Impersonater();
        if (impersonater.ImpersonateValidUser())
        {

            //서버 맵 패스로 이미지 경로 설정
            string path = Server.MapPath(imageURL);

            //이미지 존재여부 확인, 이미지가 없을경우 return false
            if (System.IO.File.Exists(path) == false)
            {
                return returnImageSize;
            }

            //해당 경로에서 이미지 파일을 비트맵으로 로딩
            Bitmap btmImage = new Bitmap(path, false);

            //불러 들인 이미지의 가로, 세로 크기 저장
            returnImageSize.Width = btmImage.Width;
            returnImageSize.Height = btmImage.Height;

            btmImage.Dispose();

        impersonater.UndoImpersonation();
        }

        return returnImageSize;
    }

    #endregion

    #region 리피터 바운드 이벤트

    protected void RptFile_itemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Web.UI.WebControls.Panel pnlImage = (System.Web.UI.WebControls.Panel)e.Item.FindControl("pnlImage");
            System.Web.UI.WebControls.Panel pnlVideo = (System.Web.UI.WebControls.Panel)e.Item.FindControl("pnlVideo");
            System.Web.UI.WebControls.Panel pnlFlash = (System.Web.UI.WebControls.Panel)e.Item.FindControl("pnlFlash");

            pnlImage.Visible = false;
            pnlVideo.Visible = false;
            pnlFlash.Visible = false;

            DataRowView dr = (DataRowView)e.Item.DataItem;
            string extension = Path.GetExtension(dr["filename"].ToString()).ToLower();

            if (extension == ".jpg" || extension == ".jpeg" ||
                extension == ".gif" || extension == ".png" || extension == ".bmp")
            {
                pnlImage.Visible = true;

                // 이미지 컨트롤 셋팅
                System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("Image_pop");
                string imgUrl = Path.Combine(this.FILE_PATH_POP, dr["filename"].ToString()).Replace('\\', '/');

                // 이미지 사이즈 가져오기
                Size imageSize = new Size();
                imageSize = getImageSize(imgUrl);

                // 화면에 뿌릴 메세지 작성
                StringBuilder sbMsg = new StringBuilder();
                sbMsg.Append(string.Format(Resources.Resource.txt_0138, imageSize.Width.ToString(), imageSize.Height.ToString())); // 업로드된 이미지 사이즈는 {0}*{1} 입니다.
                sbMsg.Append("<br />");

                if (imageSize.Width != KIOSK_FRAME_WIDTH || imageSize.Height != KIOSK_FRAME_HEIGHT)
                {
                    sbMsg.Append(string.Format(Resources.Resource.txt_0139, KIOSK_FRAME_WIDTH.ToString(), KIOSK_FRAME_HEIGHT.ToString())); // 디지털 갤러리에 들어가는 POP 권장사이즈는 {0}*{1} 입니다.

                    sbMsg.Append("<br />");

                    sbMsg.Append(Resources.Resource.txt_0140); // POP 사이즈를 확인해 주세요

                    sbMsg.Append("<br />");
                }


                img.ImageUrl = imgUrl;
                img.Height = (int)(imageSize.Height * IMAGE_REDUCE_PERCENTAGE / 100);
                img.Width = (int)(imageSize.Width * IMAGE_REDUCE_PERCENTAGE / 100);

                // 메세지를 레이블로 보여주기
                Label lb_img_size = (Label)e.Item.FindControl("Label_Image_size");
                lb_img_size.Text = sbMsg.ToString();
                lb_img_size.ForeColor = System.Drawing.Color.Salmon;

            }
            else if (extension == ".flv")
            {
                pnlVideo.Visible = true;

                System.Web.UI.WebControls.Literal ltl1 = (System.Web.UI.WebControls.Literal)e.Item.FindControl("Literal1");
                System.Web.UI.WebControls.Literal ltl2 = (System.Web.UI.WebControls.Literal)e.Item.FindControl("Literal2");

                string videoUrl = Path.Combine(this.FILE_PATH_POP, dr["filename"].ToString()).Replace('\\', '/');

                ltl1.Text = videoUrl;
                ltl2.Text = videoUrl;

            }
            else if (extension == ".swf")
            {
                pnlFlash.Visible = true;

                System.Web.UI.WebControls.Literal ltl3 = (System.Web.UI.WebControls.Literal)e.Item.FindControl("Literal3");
                System.Web.UI.WebControls.Literal ltl4 = (System.Web.UI.WebControls.Literal)e.Item.FindControl("Literal4");

                string flashUrl = Path.Combine(this.FILE_PATH_POP, dr["filename"].ToString()).Replace('\\', '/');

                ltl3.Text = flashUrl;
                ltl4.Text = flashUrl;

            }
        }
    }

    #endregion


    protected void btnUp_Click(object sender, EventArgs e)
    {
        Repeater rpt = (Repeater)(fvBoard.FindControl("rptFile"));
        if (rpt.IsNullOrEmpty()) return;

        int fromDisplayOrder = hdnDisplayOrder.Value.ToInt();
        int toDisplayOrder = fromDisplayOrder - 1;

        if (toDisplayOrder < 0) return;

        for (int i = 0; i < rpt.Items.Count; i++)
        {
            RepeaterItem rptItem = null;

            if (toDisplayOrder == i)
            {
                rptItem = rpt.Items[fromDisplayOrder];
            }
            else if (fromDisplayOrder == i)
            {
                rptItem = rpt.Items[toDisplayOrder];
            }
            else
                rptItem = rpt.Items[i];

            DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard();
            obj.UpdateAttach(((HiddenField)rptItem.FindControl("hdnAttachId")).Value.ToInt(), i);
        }

        BindControl();
    }

    protected void btnDown_Click(object sender, EventArgs e)
    {
        Repeater rpt = (Repeater)(fvBoard.FindControl("rptFile"));
        if (rpt.IsNullOrEmpty()) return;

        int fromDisplayOrder = hdnDisplayOrder.Value.ToInt();
        int toDisplayOrder = fromDisplayOrder + 1;

        if (toDisplayOrder >= rpt.Items.Count) return;

        for (int i = 0; i < rpt.Items.Count; i++)
        {
            RepeaterItem rptItem = null;

            if (toDisplayOrder == i)
            {
                rptItem = rpt.Items[fromDisplayOrder];
            }
            else if (fromDisplayOrder == i)
            {
                rptItem = rpt.Items[toDisplayOrder];
            }
            else
                rptItem = rpt.Items[i];

            DID.Service.Pop.POPBoard obj = new DID.Service.Pop.POPBoard();
            obj.UpdateAttach(((HiddenField)rptItem.FindControl("hdnAttachId")).Value.ToInt(), i);
        }

        BindControl();
    }

}
