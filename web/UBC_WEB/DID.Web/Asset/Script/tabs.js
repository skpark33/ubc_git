﻿
/*
    Sub Tabs
*/

// Tab 새로고침
function Tabs_Refresh() {
 
    var active = $('#hdnActiveId').val();
    var frame = $('#' + active).attr("title");
    $('#' + frame).attr("src", $('#' + frame).attr("src"));

} 

// Tab 삭제
function Tabs_Del(delUrl) {
 
    var frame = "";
    var frameCnt = 0;
    var strTabs = "";
    var strCookie = "";
    var active = $('#hdnActiveId').val();

    if (delUrl.indexOf("?") != -1)
        delUrl = delUrl.substring(0, delUrl.indexOf("?"));
        
    $('#TabBox li').each(function() {
        //탭삭제  if 문 변경
        if ($(this).attr("id") == delUrl)        //   if ($(this).attr("id") == active)    추가
        {
            frame = $(this).attr("title");
            $('#' + frame).attr("src", "").removeClass('DivShow').addClass('DivHide');
            $(this).remove();
            
        } else {
             
            if (strCookie == "") { 
  
                // 맨 처음 tab 활성화
                frame = $(this).attr("title");
                strCookie = $(this).attr("id") + '|' + $(this).text() + '|' + $('#' + frame).attr("src");
                $('#' + frame).removeClass('DivHide').addClass('DivShow');
                $(this).addClass('on');
                $('#hdnActiveId').val($(this).attr("id"));
                
            } else {
              
                frame = $(this).attr("title");
                strCookie += '#' + $(this).attr("id") + '|' + $(this).text() + '|' + $('#' + frame).attr("src");
                $('#' + frame).removeClass('DivShow').addClass('DivHide');
                $(this).removeClass('on');                 
            }

            frameCnt++;
        }        
    });

    $('#hdnFrameCount').val(frameCnt);
    $.cookie('TabsMenu', strCookie);
}


// 왼쪽 메뉴 클릭시 Sub Tab 생성
function Tabs_On(menuCode, menuName, menuUrl , reflash) {

 
    var tabs = "";
    var vals = "";
    var strTabs = "";
    var chkTabs = false;
    var cookie = $.cookie("TabsMenu");
    var active = $('#hdnActiveId').val();
    var frameCnt = $('#hdnFrameCount').val();

    if (frameCnt >= 7) {    // 최대 갯수 지정
        //alert('  메뉴가 7개 이상입니다.\n  메뉴는 최대 7개까지 열수 있습니다.    ');
        alert('Maximum tab count is 7.\nPlease close one of the open tab and try again.');
    } else {

        // 중복 여부 체크
        $('#TabBox li').each(function() {
            if ($(this).attr("id") == menuCode) {
                chkTabs = true;
                
            }
        });

        if (chkTabs) {
        
            // 중복되었을 경우
            $('#hdnActiveId').val(menuCode);
            $('#TabBox li').removeClass('on');
            $('li[id=' + menuCode + ']').addClass('on');
            var frame = $('li[id=' + menuCode+']').attr("title");

            $('.frame_box iframe').removeClass('DivShow').addClass('DivHide');
            $('#' + frame).removeClass('DivHide').addClass('DivShow');
            if(reflash)
                $('#' + frame).attr("src", menuUrl);

                
        } else {

            frameCnt++;
            cookie += "#" + menuCode + "|" + menuName + "|" + menuUrl;
            $('#TabBox li').removeClass('on');
            
            //탭 삭제 추가 시작  
            var delUrl;    
            //delUrl = menuUrl.substring(0,menuUrl.indexOf("."))  ;    // 주소의 뒤로 get 인자와 변수가 들어왔을때 .부터 뒤를 날려버린다.
            delUrl = menuCode; //delUrl.replace('.aspx', '') ;             // .aspx 가 있을때 날려버린다.
            
 
             
            //탭 삭제 추가 끝
            
            var j = 0;
            for (var i = 0; i <= 6; i++) {
                if ($('#frame' + i).attr("src") == '') {
                    if (j == 0) {
                        $('#hdnActiveId').val(menuCode);   // delString 의 span 에 class='DivShow' 가 있었었다.  크롬때문에 삭제 함
                        delString = "<span id='tab_delete_icon'  class='DivShow'  align='center'><img src='/Asset/Images/tab_delete_icon.gif'  id='tab_delete' onclick=Tabs_Del('" + delUrl + "')    alt='탭삭제'></span>";
                        tabs = '<li id="' + menuCode + '" class="on" title="frame' + i + '"><a href="#">' + menuName + '</a>'+delString+'</li>';
                        $('#frame' + i).attr("src", menuUrl);
                        $('.frame_box iframe').removeClass('DivShow').addClass('DivHide');
                        $('#frame' + i).removeClass('DivHide').addClass('DivShow');
                        j++;
                     }                    
                }
            }

            $.cookie("TabsMenu", cookie);
            $('#TabBox ul').prepend(tabs);
            $('#hdnFrameCount').val(frameCnt);

            $('#TabBox li').unbind('click');

            // 이벤트 설정
            $('#TabBox li').click(function() {

                $('#TabBox li').removeClass('on');
                $(this).addClass('on');
                $('#hdnActiveId').val($(this).attr("id"));

                $('.frame_box iframe').removeClass('DivShow').addClass('DivHide');
                $('#' + $(this).attr("title")).removeClass('DivHide').addClass('DivShow');
                 
            });   
        }        
    }     
}


// 초기 페이지 로딩시 Default Tab 생성 (새로고침 포함)
function Tabs_Onload(COMP_TYPE) {

     
    var frameCnt = $('#hdnFrameCount').val();
    if(frameCnt == 0)
    {
        Tabs_On("intro/intro", "HOME", "intro/intro.aspx");

    }    
     
     
//  
////    이곳이 풀려 있으면 F5 를 누르면 홈이외의 열린것은 없어짐.
//    var tabs = "";
//    var vals = "";
//    var strTabs = "";
//    var frameCnt = 0;
//    var cookie = $.cookie("TabsMenu");
//    var active = $('#hdnActiveId').val();   // 현재 활성화된 Tab
//    var delUrl;
//    
//    if ((cookie != "") || (cookie != null)) {
// 
//        if (cookie == null)   // 쿠키가 널 일때에 스크립 에러 안나게 한다. 추가
//        {
//           return;
//        }
// 
//        tabs = cookie.split('#'); 
//        for (var i = 0; i < tabs.length; i++) {            
//            if (tabs[i] != "") {
//                if (active == "") {
//                    frameCnt++;
//                    active = vals[0];
//                    vals = tabs[i].split('|');  
//                    delUrl = vals[0] ;      //추가
//                    $('#hdnActiveId').val(vals[0]);
//                    delString = "<div id=tab_delete_icon  class=DivShow  align=center><img src=/Asset/Images/tab_delete_icon.gif  id=tab_delete onclick=Tabs_Del('"+delUrl+"')    alt=탭삭제></div>";
//                    strTabs = "<li id='" + vals[0] + "' class='on' title='frame0'><a href='#'>" + vals[1] + "</a>"+delString+"</li>";
//                    $('#frame0').removeClass('DivHide').addClass('DivShow').attr('src', vals[2]);
//                } else {
//                    frameCnt++;
//                    vals = tabs[i].split('|');
//                    delUrl = vals[0] ;     //추가
//                    delString = "<div id=tab_delete_icon  class=DivShow  align=center><img src=/Asset/Images/tab_delete_icon.gif  id=tab_delete onclick=Tabs_Del('"+delUrl+"')    alt=탭삭제></div>";
//                    strTabs += "<li id='" + vals[0] + "' title='frame" + i + "'><a href='#'>" + vals[1] + "</a>"+delString+"</li>";
//                    $('#frame' + i).removeClass('DivShow').addClass('DivHide').attr('src', vals[2]);
//                }
//            }
//        } 
//        $('#hdnFrameCount').val(frameCnt);
//        $('#TabBox ul').append(strTabs); 
//        $('#TabBox li').unbind('click'); 
//        // 이벤트 설정
//        $('#TabBox li').click(function() {
// 
//            $('#TabBox li').removeClass('on');
//            $(this).addClass('on');
//            $('#hdnActiveId').val($(this).attr("id"));
// 
//            $('.frame_box iframe').removeClass('DivShow').addClass('DivHide');
//            $('#' + $(this).attr("title")).removeClass('DivHide').addClass('DivShow');
//        });  
//    } 
    
    
    
    
}