﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using System.Globalization;
using System.Threading;

using DID.Common.Framework;

/// <summary>
/// 메인페이지
/// </summary>
public partial class main : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {       
        // 랭귀지 콤보박스 초기화 
        initLanguageCombo();

        userName.Text = Profile.USER_NAME  ;

        if (Profile.USER_TYPE == "-1")
            userType.Text = "Developer";
        else if (Profile.USER_TYPE  == "1") 
            userType.Text = "Super Admin";
        else if (Profile.USER_TYPE  == "2") 
            userType.Text = "Site Admin";
        else if (Profile.USER_TYPE  == "3") 
            userType.Text = "User";
        else 
            userType.Text = "Etc";
        
        lblTopMenu.Text = "";

        using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
        {
            DataTable dt = obj.GetMenuLarge(this.Profile.COMP_TYPE, this.Profile.LANG_FLAG, this.Profile.USER_TYPE);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                if (i == 0)
                {
                    lblTopMenu.Text += string.Format("<li class='on' id='{0}' title='menubox_05'><a href='#'>{1}</a></li>"
                                       , dr["LARGE_CODE"]
                                       , Resources.Resource.ResourceManager.GetString(dr["MENU_NAME"].ToString()));

                }
                else
                {
                    lblTopMenu.Text += string.Format("<li id='{0}' title='menubox_05'><a href='#'>{1}</a></li>"
                                        , dr["LARGE_CODE"]
                                        , Resources.Resource.ResourceManager.GetString(dr["MENU_NAME"].ToString()));
                }
            }
        }
    }

    protected void initLanguageCombo()
    {
        ddlLanguage_main.Items.Clear();
        ddlLanguage_main.Items.Add(new ListItem(Resources.Resource.txt_0142, ""));

        // DB의 ubc_culture 테이블조회
        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        DataTable dt = obj.GetSiteLanguages(Profile.COMP_TYPE);
     
        // useYN 컬럼이 true 인 rows 조회
        DataRow[] rows = dt.Select("useYN = true");

        foreach (DataRow row in rows)
        {
            string culture = row["cultureName"].ToString();
            if(culture.IsNullOrEmpty()) continue;

            System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.GetCultureInfo(culture);

            ListItem addList = new ListItem();
            addList.Value = ci.Name;
            addList.Text = ci.NativeName;
            ddlLanguage_main.Items.Add(addList);
        } 
    }

    protected override void InitializeCulture()
    {        
        string Language = Request["ddlLanguage_main"];
        
        if (!Language.IsNullOrEmpty())
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Language);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Language);
        }

        Profile.LANG_FLAG = Thread.CurrentThread.CurrentCulture.Name;
    }
}
