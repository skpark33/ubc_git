using  System;
using  System.IO;
using  System.Text;


public partial class _Default : System.Web.UI.Page
{
	public void Page_Load(object sender, EventArgs e)
	{
		
		String FilePath = Server.MapPath("./data/");
		String FileName = Request["_filename"];
		FilePath += FileName;
		


		String szOffset = Request["_filesize"];
		if( szOffset == null )
		{
			szOffset = "0";
		}

		Int64 file_offset =  Int64.Parse(szOffset);

	
		// Length of the file:
		int length;

		// Total bytes to read:
		long dataToRead;


		System.IO.Stream iStream = null;
		byte[] buffer = new byte[10000];

		try
		{
			iStream = new System.IO.FileStream(FilePath, System.IO.FileMode.Open, 
					System.IO.FileAccess.Read,System.IO.FileShare.Read);

			iStream.Seek(file_offset, SeekOrigin.Begin);

			dataToRead = iStream.Length;
			dataToRead -= file_offset;
			Response.ContentType = "application/octet-stream";
			Response.AddHeader("Content-Length",  Convert.ToString(dataToRead)  );
			Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName);


			while (dataToRead > 0)
			{
				// Verify that the client is connected.
				if (Response.IsClientConnected) 
				{
					// Read the data in buffer.
					length = iStream.Read(buffer, 0, 10000);

					// Write the data to the current output stream.
					Response.OutputStream.Write(buffer, 0, length);

					// Flush the data to the HTML output.
					Response.Flush();

					buffer= new Byte[10000];
					dataToRead = dataToRead - length;
				}
				else
				{
					//prevent infinite loop if user disconnects
					dataToRead = -1;
				}
			}

		}
		catch ( Exception ex )
		{
			// Trap the error, if any.
			Response.Write("Error : " + ex.Message);

		}
		finally
		{
			if (iStream != null) 
			{
				//Close the file.
				iStream.Close();
			}
			Response.Close();

		}
		

	}
}