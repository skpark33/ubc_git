<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<head>
<title>Innorix Multi Platform Solution - InnoDS</title>
<script type="text/javascript">
	function upload()
	{
		// 첨부 파일이 있으면 컴포넌트로 파일전송
		if (InnoDS.GetFileCount() > 0)
		{
			// 서버에 저장될 서브 디렉토리 지정
			InnoDS.AddPostData("_SUB_DIR", SubDir);
			
			// 업로드 시작
			InnoDS.StartUpload();
		}
		else // 첨부 파일이 없으면 폼만 전송
		{
			InnoDSSubmit(document.f_write);
		}
	}
	
	// OnUploadComplete는 파일 업로드 완료 후 발생하는 이벤트 입니다.
	// 파일 업로드가 완료되면 f_write 폼 데이타와 업로드된 파일 정보를 함께 전송 합니다.
	function OnUploadComplete(objName)
	{
		InnoDSSubmit(document.f_write);
	}

	//서버오류, 회선 불안정 등으로 업로드를 실패 했을 때 발생되는 이벤트 입니다.
	function OnUploadError(objName)
	{
		OnUploadResume(objName);
	}

	// 업로드 중 사용자가 [x]를 클릭하여 전송을 중지했을 때 발생되는 이벤트 입니다.
	function OnUploadCancel(objName)
	{
		OnUploadResume(objName);	
	}
	
	function OnUploadResume(objName)
	{
		var UploadErrorResumeExists = false;
		var tStr = '';
			tStr += '<div style="width:490px; font-size:9pt">';
			tStr += '<center><font color="#224985">전송이 완료되지 않았습니다. <font color="#333333">이어올리기가 가능합니다.</font></center><br />';
			tStr += '<table bgcolor="#cccccc" width="490" border="0" cellspacing="1" cellpadding="3" style="font-size:9pt">';
			tStr += '<tr bgcolor="#e7e7e7" align="center" height="25">';
			tStr += '	<td>파일명</td>';
			tStr += '	<td>원본용량</td>';
			tStr += '	<td>업로드용량</td>';
			tStr += '	<td>상태</td>';
			tStr += '</tr>';

		var zStatus = '';
		var zSentBytes = '';
		
		var nUploadFileCount = InnoDS.UploadFileCount;
		for(var i = 0 ; i < nUploadFileCount ; i++)
		{
			var FullPath = InnoDS.GetUploadFullPath(i);
			var Folder = InnoDS.GetUploadFolder(i);
			var FileName = InnoDS.GetUploadFileName(i);
			var FileSize = InnoDS.GetUploadFileSize(i);
			var SentBytes = InnoDS.GetUploadBytes(i);
			var Status = InnoDS.GetUploadBytes(i);
			var IsTemp = InnoDS.IsTempFile(i);
			
			if (IsTemp)
			{
				continue;
			}		

			if (SentBytes == 0)
			{
				zStatus = '대기';
			} 
			else if (SentBytes >= parseInt(FileSize)) 
			{
				zStatus = '<font color=blue>완료</font>';
			} 
			else if (SentBytes > 0 && SentBytes < parseInt(FileSize)) 
			{
				UploadErrorResumeExists = true;
				zStatus = '<font color=red>전송중</font>';
			} 
			else 
			{
				zStatus = '-';
			}
			
			if(Folder != " ")
			{
				Folder += "\\";
			}

			tStr += '<tr bgcolor="#ffffff" height="22">';
			tStr += '	<td align="left">' + Folder + FileName + '</td>';
			tStr += '	<td align="right">' + fileSizeExp((FileSize/1024/1024), 2) + 'MB</td>';
			tStr += '	<td align="right">' + fileSizeExp((SentBytes/1024/1024), 2) + 'MB</td>';
			tStr += '	<td align="center">' + zStatus +'</td>';
			tStr += '</tr>';
		}
			tStr += '</table><br />';
			tStr += '<center><input type="button" value="이어올리기" onClick="InnoDS.StartUpload();"></center>';
			tStr += '</div>';
		
		if (UploadErrorResumeExists)
		{
			InnoDS.SetResume = true;
			var zObj = document.getElementById("UploadErrorDiv");
			zObj.innerHTML = tStr;
			zObj.style.display = 'block';

			var zObj2 = document.getElementById("UploadComponent");
			zObj2.style.visibility = 'hidden';
		}
	}
	
	// 파일 사이즈 표현 함수
	function fileSizeExp(val, precision)
	{
		var p = Math.pow(10, precision);
		return Math.round(val * p) / p;
	}
</script>
</head>
<body>
<div id="UploadErrorDiv" style="margin-left:15px; display:none"></div>
<div id="UploadComponent">
	<form action="resume_action.aspx" name="f_write" method="post" enctype="multipart/form-data">
	test1 : <input type="text" name="test1" />
	</form><br />

	<div style="border: 1px solid #c0c0c0; width:500px">
	<script type="text/javascript" src="InnoDS.js"></script>
	<script type="text/javascript">
		var Enc = "";

		var InputType = "fixed";
		var UploadURL = "action_ds.aspx";
		var SubDir = "2010";
		
		InnoDSInit( -1 , -1 , -1 , 500 , 200 );
	</script>
	</div>

	<br />
	<input type="button" value="파일추가" onClick="InnoDS.OpenFileDialog();" />
	<input type="button" value="전송하기" onClick="upload();" />
</div>
</body>
</html>