<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<head>
<title>Innorix Multi Platform Solution - InnoDS</title>

<script type="text/javascript">
	function upload()
	{
		// 첨부 파일이 있으면 컴포넌트로 파일전송
		if (InnoDS.GetFileCount() > 0)
		{
			// 서버에 저장될 서브 디렉토리 지정
			InnoDS.AddPostData("_SUB_DIR", SubDir);
			
			// 업로드 시작
			InnoDS.StartUpload();
		}
		else // 첨부 파일이 없으면 폼만 전송
		{
			InnoDSSubmit(document.f_write);
		}
	}
	
	// OnUploadComplete는 파일 업로드 완료 후 발생하는 이벤트 입니다.
	// 파일 업로드가 완료되면 f_write 폼 데이타와 업로드된 파일 정보를 함께 전송 합니다.
	function OnUploadComplete(objName)
	{
		InnoDSSubmit(document.f_write);
	}	
</script>

</head>
<body>

<form action="folder_action.aspx" name="f_write" method="post" enctype="multipart/form-data">
test1 : <input type="text" name="test1" />
</form><br />

<div style="border: 1px solid #c0c0c0; width:500px;">
<script type="text/javascript" src="InnoDS.js"></script>
<script type="text/javascript">
	var Enc = "";
	
	var InputType = "fixed";
	var UploadURL = "action_ds.aspx";
	var ViewType = 2;	
	var SubDir = "2010";

	InnoDSInit( -1 , -1 , -1 , 500 , 200 );
</script>
</div>

<br />
<input type="button" value="폴더추가" onClick="InnoDS.OpenFolderDialog();" />
<input type="button" value="전송하기" onClick="upload();" />

</body>
</html>