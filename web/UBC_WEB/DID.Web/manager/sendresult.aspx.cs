﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Reflection; 
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using DID.Common.Framework;



//예를들어)   고객이  KIA001 지점에서  2011년  06월  07일  밤8시  03분  03초에  견적을 냈다면,
//                 KIA001_110607200303.gif  로 보내주시면 됩니다.

//                 ( 시간은 24시간제입니다.)
//                 ( 확장자는 jpg 여도 상관없습니다.상관없습니다.)


//select comment2   from ubc_site  where franchizeType='KIA' and siteId = 'GB1'

//if(comment2 <= 0  )  
//{
//    // 남은 건수가 0이거나 음수이면 -1을 안해줌
//    // 남은 건수가 없습니다.
//} else {
//    //남은 건수가 0이상이면, MMS 모듈에 insert 해주고, ubc_site 에서   -1해줌
//    update   ubc_site  set comment2 = comment2 -1   where franchizeType='KIA' and siteId = 'GB1'
//}




public partial class manager_sendresult : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {     
            phone.Text    = Request["phone"];
            siteCode.Text = Request["siteCode"];

            HttpPostedFile mmsFile = Request.Files["priceImageFile"];

            if ((mmsFile != null) && (mmsFile.ContentLength > 0))
            {
                // this.ScriptExecute(Path.GetFileName(mmsFile.FileName));                  // 파일명
                // this.ScriptExecute(Path.GetFileName(mmsFile.ContentLength.ToString()));  // 파일 사이즈

                string SaveLocation = Server.MapPath("\\file_mms\\");    // c:/infobank/attach_file/smt 폴더를 file_mms 이란 이름으로 가상디렉토리 잡기
                
                string fn = System.IO.Path.GetFileName(mmsFile.FileName);

                string uniqueFullFileName = Common.GetUniqueFileName(SaveLocation, fn);
               
                try
                {
                    mmsFile.SaveAs(uniqueFullFileName);
                }
                catch (Exception ex)
                {
                    //this.ScriptExecute(ex.Message);
                }
                ImageName.Text = fn;
                Image1.ImageUrl = "/file_mms/"+fn;

                // MMS 날리기  시작
                using (DID.Service.Manager.MmsSms obj = new DID.Service.Manager.MmsSms())
                {
                     // ubc_site 테이블의 phoneNo1 컬럼에서 해당조직의 전화번호 가져오기
                     // phoneNo1 이 없거나 널이면 025109030 을 넣어준다.
                     DataTable dt =  obj.mmsSitePhone(Request["siteCode"].ToString().Trim());

                     string phoneSendVal = "025109030";

                     if (dt.Rows.Count != 0)
                     {
                         if (dt.Rows[0][0].ToString().Trim() == "")
                         {   //전화번호가 없는경우. 상황실전화번호

                         }
                         else
                         {
                             phoneSendVal = dt.Rows[0][0].ToString();
                         }
                     }
                     


                     
                     string titleVal = "기아자동차 견적내역 입니다.";
                     string contentsVal = " ";
                     string franchizeTypeVal = Profile.COMP_TYPE;
                     string siteIdVal        = Request["siteCode"];
                     string phoneVal         = Request["phone"].Replace("-","");
                     string fnVal            = fn;

                     int ret = obj.mmsSend(phoneSendVal ,titleVal, contentsVal, franchizeTypeVal, siteIdVal, phoneVal, fnVal);


                
                } 
                // MMS 날리기  끝



            }
            else {

               // this.ScriptExecute("화일을 선택 하세요");
            
            }


            


        }
    }

}
