﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="translator.aspx.cs" Inherits="translator"  EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript">

    $(document).ready(function() {
        $('#excel-upload').hide();
    });

    // 확장자가 xls  혹은 xlsx 인 파일만 업로드 가능 함.
    function checkFileExtension(file)
    {
        var filePath = file.value;

        if (filePath.indexOf('.') == -1) {
            file.value = "";
            return false;
        }
            
        var validExtensions = new Array("xls", "xlsx");
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1);

        for (var i = 0; i < validExtensions.length; i++)
        {
            if (ext.toLowerCase() == validExtensions[i].toLowerCase())
                return true;
        }

        alert("<%= Resources.Resource.msg_0051 %>" + "\n[ " + (validExtensions.join(",")) + " ]");

        file.value = "";        
        return false;
    }

    function checkUploadfile() 
    {
        if ($('#<%= FileUpload1.ClientID%>').val() == "")
            return false;
            
        return true;
    }

    function DoSave() {
        // 저장 하시겠습니까?
        if (!confirm("<%= Resources.Resource.msg_0050 %>" + "\n[language: " + $('#<%= ddlLngToTranslate.ClientID%>').val() + "]"))
            return false;

        return true;
    }

    function DoDelete() {
        // 삭제 하시겠습니까?
        if (!confirm("<%= Resources.Resource.msg_0017 %>" + "\n[language: " + $('#<%= ddlLngToTranslate.ClientID%>').val() + "]"))
            return false;

        return true;
    }
    
</script>

<style type="text/css">

#section 
{
	padding: 10px 10px;        
    margin: 0;     
    border: solid 1px #ddd;
    
    /*CSS3 properties*/
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    
    box-shadow: rgb(0,0,0) 4px 2px 6px;    
    -webkit-box-shadow: rgb(0,0,0) 4px 2px 6px;
    -moz-box-shadow: rgb(0,0,0) 4px 2px 6px;
}


#command 
{
	padding: 10px 20px 0px 0px; 
    background-color: #ff5522;
    color: #fff;
    margin: 0; 
    
    text-align:center;
    vertical-align: middle; 
    height: 30px;
	
    /*CSS3 properties*/
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
}

.excel
{
	margin: 4px 0 0 0;
}

#excel-command
{
	float:left;
	
	padding: 4px;
    background-color: #ffee11;
    margin: 0;

    /*CSS3 properties*/
    border-radius: 8px 8px 0px 0px;
    -webkit-border-radius: 8px 8px 0px 0px;
    -moz-border-radius: 8px 8px 0px 0px;
}

#excel-upload
{
	float:left;
	
	padding: 4px;
    background-color: #aadd00;
    margin: 0;

    
    /*CSS3 properties*/
    border-radius: 8px 8px 0px 0px;
    -webkit-border-radius: 8px 8px 0px 0px;
    -moz-border-radius: 8px 8px 0px 0px;    
}

#grid
{
	margin: 10px 0px 0px 0px;
	clear:both;
	
}

#grid table.translate
{
    width: 100%;
    border-style: none;
    border-collapse: collapse;
}
* html table.translate {width:auto; } /* ie6 */
#grid table.translate th
{
    color: #555555;
    background-color: #ECECEC;
    font-weight: bold;
    margin: 0px;
    padding: 6px 0 4px 0;
    border: 1px solid #c6c6c6;
}
#grid table.translate td
{
	color: #555555;	
    margin: 0px;
    padding: 4px;  
    border: 1px solid #c6c6c6;
    overflow:auto;
}

</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<div>
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
    <tr>
        <td>
            <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <a href="../generalSettings.aspx"><%= Resources.Resource.ttl_0044 %></a> >> <a href="./siteLanguage.aspx"><%= Resources.Resource.ttl_0046%></a> >> <%= this.Title %> <!-- 사이트 번역기 //--></span>
        </td>
    </tr>
    </table>
</div>

<div id="section">

<div id="command" >
    <asp:Label ID="Label1" runat="server" Text="<%$ Resources: Resource, txt_0091 %>" />  <!-- 번역할 언어 //-->     
    <asp:DropDownList ID="ddlLngToTranslate" runat="server" onselectedindexchanged="ddlLngToTranslate_SelectedIndexChanged" AutoPostBack=true />
    <asp:Button ID="btnApply" runat="server" Text="<%$ Resources: Resource, btn_0004 %>" OnClientClick="return DoSave();" onclick="btnApply_Click" ></asp:Button> <!-- 저장 //-->    
    <asp:Button ID="btnDelete" runat="server" Text="<%$ Resources: Resource, btn_0001 %>" OnClientClick="return DoDelete();" onclick="btnDelete_Click" ></asp:Button> <!-- 삭제 //-->       
</div>

<asp:Panel ID="Panel1" runat="server" CssClass="excel">
    
    <div id="excel-command">
        <asp:Button ID="btnDownload" runat="server" Text="<%$ Resources: Resource, btn_0009 %>" onclick="btnDownload_Click" ></asp:Button> <!-- 엑셀화일로 다운로드 //-->
        <asp:Button ID="btnUpload" runat="server" Text="<%$ Resources: Resource, btn_0010 %>" OnClientClick="$('#excel-upload').toggle('fast'); return false;"  ></asp:Button> <!-- 엑셀화일 불러오기 //-->
        
            <a onmouseover='this.style.cursor="pointer" ' onfocus='this.blur();' onclick="document.getElementById('PopUp').style.display = 'block' " ><span style="text-decoration: underline;"><%= Resources.Resource.txt_0145 %><!-- 사용법 //--></span></a>
            <div id='PopUp' style='display: none; position: absolute; left: 20%; top: 20%; border: solid black 1px; padding: 10px; background-color: rgb(255,255,225); text-align: justify; font-size: 12px; width: 618px;'>
                1. Download excel file : use [<%= Resources.Resource.btn_0009%>] button<br />
                2. Edit downloaded file following below example.<br />                
                <img src="instruction.gif" />
                3. Save as excel file. (.xls or .xlsx)<br />
                4. Upload excel file : use [<%= Resources.Resource.btn_0010%>] button
                <div style='text-align: right;'>
                    <a onmouseover='this.style.cursor="pointer" ' style='font-size: 12px;' onfocus='this.blur();' onclick="document.getElementById('PopUp').style.display = 'none' " ><span style="text-decoration: underline;">Close</span></a>
                </div>
            </div>
	
    </div>

    <div id="excel-upload" >
        <asp:Fileupload id="FileUpload1" runat="server" onchange="return checkFileExtension(this);" />
        <asp:Button ID="btnApplyExcel" runat="server" Text="<%$ Resources: Resource, btn_0011 %>" OnClientClick="return checkUploadfile();" onclick="btnApplyExcel_Click" /> <!-- 화일열기 //-->           
    </div>
    
</asp:Panel>

<div id="grid" >    

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CellPadding="3"  CssClass="translate">
    <Columns>                 
        <asp:BoundField DataField="name" HeaderText="ID" ItemStyle-HorizontalAlign=Center ItemStyle-Width="5%" Visible="true"/>
        <asp:BoundField DataField="value" HeaderText="From" HeaderStyle-HorizontalAlign=center ItemStyle-Width="45%" />
                
        <asp:TemplateField HeaderText="To" >
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle Width="45%" />
            <ItemTemplate>
                <asp:TextBox ID="txtValue"  runat="server" Width="98%"/> 
            </ItemTemplate>
        </asp:TemplateField>
        
    </Columns>
    </asp:GridView>    


    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" visible="false" onrowdatabound="GridView2_RowDataBound" >
    <Columns>                             
        <asp:BoundField DataField="name" HeaderText="ID" ItemStyle-HorizontalAlign=Center ItemStyle-Width="5%" />
        <asp:BoundField DataField="value" HeaderText="TO" HeaderStyle-HorizontalAlign=center ItemStyle-Width="47%" HtmlEncodeFormatString=false HtmlEncode=false  ConvertEmptyStringToNull="true" />
    </Columns>                            
    </asp:GridView> 
    
    
</div>

</div>

</asp:Content>

