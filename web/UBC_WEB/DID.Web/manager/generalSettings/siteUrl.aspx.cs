﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

public partial class generalSetting_siteUrl : BasePage
{
    protected const string DELIM = "\r\n";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0045; // URL 설정
    }

    /// <summary>
    /// DB의 ubc_customer 테이블의 URL 컬럼안에 UBC에서 사용하는 URL 정보들이 들어있다.
    /// 이 URL 정보들은 ini 화일 형식으로 저장된 하나의 문자열이다
    /// 이 함수에서 이 하나의 URL 문자열을 table 형태로 분리해서 gridview1 에 뿌려준다
    ///
    /// --------------- URL 정보 문자열 예제 -------------------------------------------------------------------
    /// [KIA](줄바꿈 \r\n) -- 섹션
    /// statistics_pkg_reg=Statistics/programReg.aspx(줄바꿈 \r\n) -- 엔트리 (name=value)
    /// --------------------------------------------------------------------------------------------------------
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_PreRender(object sender, EventArgs e)
    {
        refreshGridView1(); // Gridview1 새로고침
    }

    /// <summary>
    /// 그리드뷰 새로고침: DB에서 ubc_customer을 조회해서 Gridview1에 보여준다
    /// 
    /// DB의 ubc_customer 테이블의 URL 컬럼에는 UBC 에서 사용하는 URL 정보들이 들어있다.
    /// 이 URL 정보들은 ini 화일 형식으로 저장된 하나의 문자열이다.
    /// 이 하나의 URL 문자열을 table 형태로 분리해서 gridview1 에 뿌려준다
    ///
    /// --------------- URL 정보 문자열 예제 -------------------------------------------------------------------
    /// [KIA](줄바꿈 \r\n) -- 섹션
    /// statistics_pkg_reg=Statistics/programReg.aspx(줄바꿈 \r\n) -- 엔트리 (name=value)
    /// --------------------------------------------------------------------------------------------------------
    /// </summary>
    protected void refreshGridView1()
    {
        // DB의 ubc_customer 테이블조회
        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        DataTable dt = obj.GetCustomerURL(Profile.COMP_TYPE);

        // customer 정보가 없을경우 화면을 처리할 수 없도록 막는다
        if (dt.Rows.Count <= 0)
        {
            txtURLId.Enabled = false;
            txtURLValue.Enabled = false;
            btnAddURL.Enabled = false;

            ScriptExecute(Resources.Resource.msg_0055); //  고객정보를 불러올 수 없습니다.\n다시 한번 시도 하신 후 이 메시지가 또 나올경우 사이트 관리자에게 연락하세요.
            return;
        }

        string url = dt.Rows[0]["url"].ToString();
        
        // 사이트 섹션 값 (예) [KIA]
        string section = string.Format("[{0}]", Profile.COMP_TYPE);

        // URL 컬럼값에 사이트 섹션 값이 존재할 경우
        if (url.ToLower().Contains(section.ToLower()))
        {
            // 섹션값을 제외한 데이타 부분만을 잘라낸다
            string data = url.Substring(url.ToLower().IndexOf(section.ToLower()) + section.Length);            

            // 문자열 데이타를 DataTable 형태로 변형
            dt = stringToDataTable(data);
        }
        // URL 컬럼값에 사이트 섹션 값이 존재하지 않을 경우
        else
        {
            // Gridview1 초기화를 위해 null로 셋팅
            dt = null;
        }

        // Gridview1 refresh
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    /// <summary>
    /// DELIM(\r\n) 으로 구분된 문자열을 풀어서 DataTable 형태로 리턴한다.
    /// </summary>
    /// <param name="delimitedString">DELIM(\r\n) 으로 구분된 문자열</param>
    /// <returns>GridView1에 대입할 DataTable 객체</returns>
    protected DataTable stringToDataTable(string delimitedString)
    {
        // 앞뒤 공백 제거
        delimitedString = delimitedString.Trim();

        DataTable dt = new DataTable();
        dt.Columns.Add("seq", typeof(string));
        dt.Columns.Add("urlId", typeof(string));
        dt.Columns.Add("url", typeof(string));
        dt.Columns.Add("url_hidden", typeof(string));

        string[] properties = delimitedString.Split(DELIM.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

        int seq = 1;
        foreach (string property in properties)
        {
            // 혹시 다른 섹션이 존재하면 loop 종료 (원래는 없어야 정상임)
            if (property.StartsWith("[") == true) break;

            // 주석일경우 skip
            if (property.StartsWith(";") || property.StartsWith("#") ) continue;

            // name,value 구분값(주로 =이나 :를 쓰는 ini 화일도 있다고함)이 없으면 skip
            if (!property.Contains("=") && !property.Contains(":")) continue;

            // name,value 구분값 위치
            int delimIndex = (property.IndexOf("=")==-1)?property.IndexOf(":"):property.IndexOf("=");

            string name = property.Substring(0, delimIndex); // name 값
            string value = property.Substring(delimIndex + 1); // value 값

            DataRow dr      = dt.NewRow();
            dr["seq"]       = seq.ToString();
            dr["urlId"]     = name;
            dr["url"]       = value;
            dr["url_hidden"]= value;
            dt.Rows.Add(dr);

            seq++;
        }

        return dt;
    }

    /// <summary>
    /// DB의 ubc_customer 테이블의 URL 컬럼에는 UBC 에서 사용하는 URL 정보들이 들어있다.
    /// 이 URL 정보들은 ini 화일 형식으로 저장된 하나의 문자열이다.
    /// 이 함수에서 Gridview1의 데이타를 URL 컬럼 형식인 ini 화일 형식의 하나의 문자열로 만들어 리턴한다
    /// </summary>
    /// <param name="urlId">추가/변경/삭제 할 URL ID 값</param>
    /// <param name="urlValue">URL 값. "" 인 경우 삭제를 의미함.</param>
    /// <returns>ini 화일 형식의 하나의 문자열</returns>
    protected string gridToString(string urlId, string urlValue)
    {
        StringBuilder sb = new StringBuilder();

        // 섹션값 셋팅
        // 형식: [사이트명](줄바꿈\r\n)
        sb.Append(string.Format("[{0}]{1}", Profile.COMP_TYPE, DELIM));        

        foreach (GridViewRow row in GridView1.Rows)
        {
            // urlValue 값이 null 일경우 삭제이므로 제외시킴
            if (urlValue.IsNullOrEmpty() == true && row.Cells[1].Text.Equals(urlId) == true) 
                continue;

            // 엔트리 값셋팅
            // 형식: name=value(줄바꿈\r\n)
            sb.Append(string.Format("{0}={1}{2}"
                                    , row.Cells[1].Text
                                    , row.Cells[1].Text.Equals(urlId) ? urlValue : row.Cells[3].Text // 업데이트일 경우 urlValue 값을 넣어줌. 그외는 원래값(4번째 숨김필드)을 셋팅
                                    , DELIM)
                     );
        }

        // urlId 가 GridView1에 존재하지 않으면 추가를 의미한다. 문자열 끝에 새로운 엔트리 값을 추가해준다
        if (Common.findTextInGridview(GridView1, 1, urlId) == -1) 
            sb.Append(string.Format("{0}={1}", urlId, urlValue));

        return sb.ToString().Trim();

    }    

    // URL 신규등록 버튼 클릭 이벤트 핸들러
    protected void btnAddURL_Click(object sender, EventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        // 앞뒤 공백 제거
        string newURLId = txtURLId.Text.Replace(" ", "");

        if (Common.findTextInGridview(GridView1, 1, newURLId) != -1)
        {
            ScriptExecute(Resources.Resource.msg_0056); // 같은 이름의 URL ID가 존재합니다
            return;
        }

        // 앞뒤 공백 제거
        string newURLValue = txtURLValue.Text.Trim();

        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        obj.SetCustomerURL(Profile.COMP_TYPE, gridToString(newURLId, newURLValue));

        ScriptExecute(Resources.Resource.msg_0025); // 등록 되었습니다.
    }

    /// <summary>
    /// GridView1 RowDataBound 이벤트 핸들러
    /// 변경/삭제 버튼 클릭시 확인 메세지 창을 띄우는 자바스크립트를 추가함
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // 변경/삭제 버튼 클릭시 컨펌하기
            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                foreach (Control control in cell.Controls)
                {
                    Button button = control as Button;
                    if (button != null)
                    {
                        if (button.CommandName == "Delete")
                        {
                            button.OnClientClick = "if (!confirm('" + Resources.Resource.msg_0037 + "')) return false;"; // 코드 삭제 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다.\\n계속 하시겠습니까?
                        }
                        else if (button.CommandName == "Update")
                        {
                            button.OnClientClick = "if (!confirm('" + Resources.Resource.msg_0038 + "')) return false;"; // 코드 변경 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다.\\n계속 하시겠습니까?
                        }
                    }
                }
            }
        }
    }
  
    /// <summary>
    /// URL 업데이트 버튼 클릭 핸들러
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {        
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다
        
        // 앞뒤 공백 제거
        string newURLValue = ((TextBox)GridView1.Rows[e.RowIndex].Cells[2].Controls[0]).Text.Trim();

        if (newURLValue.IsNullOrEmpty())
        {
            ScriptExecute(Resources.Resource.msg_0054); // URL을 입력하세요.
            return;
        }
        
        // 변경할 URL ID 값 (현재행)
        string urlId = GridView1.Rows[e.RowIndex].Cells[1].Text;        

        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        obj.SetCustomerURL(Profile.COMP_TYPE, gridToString(urlId, newURLValue));

        GridView1.EditIndex = -1;

        ScriptExecute(Resources.Resource.msg_0024); // 수정 되었습니다.

    }
       

    /// <summary>
    /// URL 삭제 버튼 클릭 핸들러
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        // 삭제할 URL ID 값 (현재행)
        string urlId = GridView1.Rows[e.RowIndex].Cells[1].Text;
        
        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        obj.SetCustomerURL(Profile.COMP_TYPE, gridToString(urlId, ""));

        GridView1.EditIndex = -1;

        ScriptExecute(Resources.Resource.msg_0047); // 삭제 되었습니다
    }


    /// <summary>
    /// URL 수정 버튼 클릭 핸들러
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        GridView1.EditIndex = e.NewEditIndex;
    }

    // URL 취소 버튼 클릭시
    /// <summary>
    /// URL 변경 취소 버튼 클릭 핸들러
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
    }    
    

    
}
