﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="siteLanguage.aspx.cs" Inherits="generalSetting_siteLanguage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript">

</script>

<style type="text/css">

#section 
{
	padding: 10px 10px;        
    margin: 0;     
    border: solid 1px #ddd;
    
    /*CSS3 properties*/
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    
    box-shadow: rgb(0,0,0) 4px 2px 6px;    
    -webkit-box-shadow: rgb(0,0,0) 4px 2px 6px;
    -moz-box-shadow: rgb(0,0,0) 4px 2px 6px;
}

</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div>
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
    <tr>
    <td>
        <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <a href="../generalSettings.aspx"><%= Resources.Resource.ttl_0044 %></a> >> <%= this.Title %> <!-- 언어 설정 //--></span>
    </td>
    </tr>
    </table>
</div>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div id="section">

    <div class="edit_area">                    
        <img src="/Asset/Images/agronet/bul_arr01.gif" /> 
        <asp:Label ID="Label1" runat="server" Text="<%$ Resources: Resource, txt_0142 %>" CssClass="pad_R10" />
        <asp:DropDownList ID="ddlLanguage" runat="server"  />
        <asp:button ID="btnAddLanguage" runat="server" OnClick="btnAddLanguage_Click" Text="Add"/>        
    </div>
    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="customerId, cultureName" CssClass="boardListType2" 
                onprerender="GridView1_PreRender" 
                onrowediting="GridView1_RowEditing"
                onrowdeleting="GridView1_RowDeleting"
                onrowdatabound="GridView1_RowDataBound" 
                onrowupdating="GridView1_RowUpdating" 
                onrowcancelingedit="GridView1_RowCancelingEdit">
    <Columns>
        <asp:BoundField DataField="cultureName" HeaderText="Language Code" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="200px" ReadOnly="true" />
        <asp:TemplateField HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate></ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="englishName" HeaderText="English Name" ItemStyle-HorizontalAlign="Center"  ReadOnly="true"  />
        <asp:CheckBoxField DataField="useYN" HeaderText="Use" HeaderStyle-Width="60px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  Text="Yes"/>               
        <asp:CommandField 
            ButtonType="Button" 
            HeaderStyle-Width="150px" 
            ItemStyle-HorizontalAlign="Center"                 
            HeaderText="Action" 
            ShowEditButton="true" ShowDeleteButton="true"/>

    </Columns>
    
    <EmptyDataTemplate>
        <table style="width:100%;height:290px;">
            <tr>
                <td style="text-align:center">
                <%= Resources.Resource.txt_0025 %> <!-- 조회된 데이터가 없습니다. -->
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>

    </asp:GridView>    
    
</div>

</ContentTemplate>    
</asp:UpdatePanel>

</asp:Content>

