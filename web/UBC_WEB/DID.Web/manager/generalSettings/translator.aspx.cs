﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.IO;
using System.Text;

using System.Resources;
using System.Globalization;
using System.Xml;

using DID.Common.Framework;

public partial class translator : BasePage
{
    enum Gridview1_Columns { ID, FROM, TO };
    enum Gridview2_Columns { ID, TO };

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0020; // 사이트 번역기

        if (!IsPostBack)
        {
            initLanguageCombo();  

            btnApply.Enabled = false; // 적용번튼 비활성
            btnDelete.Enabled = false; // 삭제번튼 비활성
            Panel1.Visible = false; // 엑셀패널 숨기기            


            if (!Request["cultureName"].IsNullOrEmpty())
            {
                ddlLngToTranslate.SelectedValue = Request["cultureName"].ToString();
                ddlLngToTranslate_SelectedIndexChanged(null, null);
            }
        }

        
    }

    // culture 정보를 가져와서 콤보박스를 만든다
    protected void initLanguageCombo()
    {   
        List<string> list = new List<string>();
        foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.NeutralCultures))
        {
            if (ci.Name.IsNullOrEmpty()) continue;
            list.Add(String.Format("{0},{1}", ci.EnglishName, ci.Name));
        }

        list.Sort();  // sort by name

        ddlLngToTranslate.Items.Add("");

        foreach (string cultureText in list)
        {
            string[] temp = cultureText.Split(',');

            ListItem addList = new ListItem();
            addList.Text = temp[0];
            addList.Value = temp[1];

            ddlLngToTranslate.Items.Add(addList);
        }

        if (Profile.USER_TYPE != "-1" && Profile.USER_TYPE != "1" ) ddlLngToTranslate.Enabled = false;
    }

    // 번역할 랭귀지 콤보박스 변경 이벤트 핸들러
    protected void ddlLngToTranslate_SelectedIndexChanged(object sender, EventArgs e)
    {        
        // GridView2 는 숨겨져 있다. 
        // GridView1 은 영어 리소스를 GridView2 는 번역대상 리소스 로드한다
        // GridView2 가 바인드 되면서 GridView2의 [To] 컬럼의 값을 GridView1 의 [To] 컬럼으로 복사한다.

        // 2개의 그리드뷰 초기화
        GridView1.DataSource = null;
        GridView1.DataBind();
        GridView2.DataSource = null;
        GridView2.DataBind();

        // 선택된 랭귀지 유무에 따른 처리
        if (ddlLngToTranslate.SelectedValue.IsNullOrEmpty())
        {
            btnApply.Enabled = false;   
            btnDelete.Enabled = false;
            Panel1.Visible = false;

            return;
        }
        else
        {
            btnApply.Enabled = true;
            btnDelete.Enabled = true;
            Panel1.Visible = true;
        }

        // GridView1 에 Default(영어) 로드
        fillGridView1();

        /*
         * 선택된 랭귀지의 번역본이 이미 있을 경우 
         * 숨겨져 있는 GridView2 에 선택된 랭귀지를 로드한다 
         * GridView2 가 바인드 되면서 GridView1 의 세번째 [To] 컬럼 TextBox의 값을 GridView2 의 [To] 컬럼의 값으로 셋팅한다 
         * (바운드 이벤트핸들러 GridView2_RowDataBound 에서 작업)
         */

        // 선택된 랭귀지의 리소스 화일 전체경로
        string path = Server.MapPath("~/App_GlobalResources/Resource." + ddlLngToTranslate.SelectedValue + ".resx");
        if (File.Exists(path))
        {
            // GridView2 에 번역대상 언어 로드
            fillGridView2(path);
        }     

    }

    // GridView2 의 Row 데이타 바운드 핸들러
    // 숨겨놓은 GridView2 에 번역된 값이 바인딩 될때 그값을 GridView1 의 'To' 컬럼에 셋팅해준다
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // 1. 현재 Row 의 리소스ID 값을 
            string resId = e.Row.Cells[(int)Gridview2_Columns.ID].Text;

            // 2. GridView1 에서 찾아서 
            int gridview1_RowIndex = Common.findTextInGridview(GridView1, (int)Gridview1_Columns.ID, resId);
            
            // 3. 일치하는 리소스ID 가 있을 경우 
            if (gridview1_RowIndex != -1)
            {
                // 4. 현재 Row의 번역 문자열을 GridView1 의 세번째 컬럼인 텍스트 박스에 셋팅한다
                ((TextBox)GridView1.Rows[gridview1_RowIndex].Cells[(int)Gridview1_Columns.TO].FindControl("txtValue")).Text = (e.Row.Cells[(int)Gridview2_Columns.TO].Text == "&nbsp;") ? "" : e.Row.Cells[(int)Gridview2_Columns.TO].Text;

            }
        }
    }

    // Defalut(영어) GridView1 채우기
    protected void fillGridView1()
    {
        DataSet dsgrid = new DataSet();
        DataTable dt = new DataTable();

        dsgrid.ReadXml(Server.MapPath("~/App_GlobalResources/Resource.resx"));
        
        dt.Merge(dsgrid.Tables["data"]);
        dt.AcceptChanges();
       
        GridView1.DataSource = dt;
        GridView1.DataBind();      

    }

    // 번역대상 GridView2 채우기
    protected void fillGridView2(string filePath)
    {
        DataSet dsgrid = new DataSet();
        DataTable dt = new DataTable();

        dsgrid.ReadXml(filePath);

        dt.Merge(dsgrid.Tables["data"]);
        dt.AcceptChanges();

        GridView2.DataSource = dt;
        GridView2.DataBind();
    }

    // 저장버튼 이벤트 핸들러
    protected void btnApply_Click(object sender, EventArgs e)
    {
        /*
         * 번역대상 리소스가 존재하지 않을경우 (즉, 번역본을 처음 만드는 언어일경우)
         * Defalut(영어) 리소스를 카피하여 번역대상 리소스 화일을 만들어낸다
         */

        // 번역 대상 리소스 화일 전체경로
        string path = Server.MapPath("~/App_GlobalResources/Resource." + ddlLngToTranslate.SelectedValue + ".resx");

        if (!File.Exists(path)) 
        {
            // 한국어 리소스 화일 전체경로
            string sourceFile = Server.MapPath("~/App_GlobalResources/Resource.resx");

            File.Copy(sourceFile, path);
        }
        
        
        // 번역대상 리소스 화일을 사용자가 화면에서 입력한 번역내용으로 업데이트
        updateElement(path);

        ScriptExecute(Resources.Resource.msg_0046); // 저장 되었습니다

    }

    // 삭제버튼 이벤트 핸들러
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        // 번역 대상 리소스 화일 전체경로
        string path = Server.MapPath(string.Format("~/App_GlobalResources/Resource.{0}.resx", ddlLngToTranslate.SelectedValue));
        File.Delete(path);
        ddlLngToTranslate_SelectedIndexChanged(null, null);
        ScriptExecute(Resources.Resource.msg_0047); // 삭제 되었습니다
    }

    // 화면에서 입력된 번역내용을 리소스 화일에 적용하는 메서드
    public void updateElement(string FilePath)
    {
        // 인자로 받은 번역 대상 리소스화일을 XmlDocument 로 로드한다
        XmlDocument doc = new XmlDocument();
        doc.Load(FilePath);
        XmlNode rootnode = doc.SelectSingleNode("//root"); // XML Root node 설정
        
        /*
         * GridView1 첫번째 컬럼의 리소스ID 에 해당하는 XML 값을 찾아서 
         * GridView1 세번째 TextBox 컬럼에 입력된 번역 문자열을 셋팅한다
         * 번역 문자열 값이 없다면 해당 XML 노드를 삭제한다
         */
        bool isChanged = false;
        foreach (GridViewRow row in GridView1.Rows)
        {
            string translatedText = ((TextBox)row.Cells[(int)Gridview1_Columns.TO].FindControl("txtValue")).Text;
            //translatedText = translatedText.Replace(Environment.NewLine, "\\n");

            XmlNode node = rootnode.SelectSingleNode("//data[@name='" + row.Cells[0].Text + "']");

            if (translatedText.IsNullOrEmpty())
            {
                if (!node.IsNullOrEmpty())
                {                    
                    rootnode.RemoveChild(node);
                    isChanged = true;
                }
            }
            else
            {
                string innerXml = "<value>" + translatedText + "</value>";
                
                if (node.IsNullOrEmpty())
                {
                    XmlElement xElement = doc.CreateElement("data");
                    xElement.SetAttribute("name", row.Cells[0].Text);
                    xElement.SetAttribute("xml:space", "preserve");                    
                    xElement.InnerXml = innerXml;
                    doc.DocumentElement.AppendChild(xElement);

                    isChanged = true;
                }
                else
                {
                    if (!node.InnerXml.Trim().Equals(innerXml))
                    {
                        node.InnerXml = innerXml;
                        isChanged = true;
                    }
                }                
            }
        }
        
        if (isChanged) doc.Save(FilePath);
    }

    // 엑셀저장 버튼 클릭 핸들러
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        // 엑셀화일명
        string fileName = this.Title + "_" + ddlLngToTranslate.SelectedValue + ".xls";
        fileName = fileName.Replace(' ', '_');

        // 엑셀 포맷으로 저장
        ExportGridToExcel(GridView1, fileName);
    }

    // 엑셀 포맷으로 저장
    public void ExportGridToExcel(GridView gridView, string fileName)
    {
        //StringBuilder dataToExport = new StringBuilder();

        //dataToExport.Append(String.Format("{0}\t{1} (en)\t{2} ({3})\n"
        //                , gridView.HeaderRow.Cells[0].Text
        //                , gridView.HeaderRow.Cells[1].Text
        //                , gridView.HeaderRow.Cells[2].Text
        //                , ddlLngToTranslate.SelectedValue));


        //foreach (GridViewRow GridViewRow in gridView.Rows)
        //{
        //    dataToExport.Append(String.Format("{0}\t{1}\t{2}\n"
        //                    , GridViewRow.Cells[0].Text
        //                    , GridViewRow.Cells[1].Text.Replace("<br />", "\\n")
        //                    , ((TextBox)GridViewRow.Cells[2].FindControl("txtValue")).Text));
        //}

        //if (!string.IsNullOrEmpty(dataToExport.ToString()))
        //{
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8)));
        //    //Response.ContentType = "application/vnd.ms-excel";
        //    Response.ContentType = "text/csv";
        //    Response.ContentEncoding = System.Text.Encoding.Unicode;
        //    Response.Write(Encoding.Unicode.GetString(Encoding.Unicode.GetPreamble()));
        //    Response.Write(dataToExport.ToString());
        //    Response.Flush();
        //    Response.End();
        //}

        //DataSet DS = new DataSet();
        //DataTable DT = DS.Tables.Add();

        //DT.Columns.Add();
        //DT.Columns.Add();
        //DT.Columns.Add();

        //DT.Rows.Add(gridView.HeaderRow.Cells[0].Text
        //           , gridView.HeaderRow.Cells[1].Text
        //           , gridView.HeaderRow.Cells[2].Text);

        //foreach (GridViewRow GridViewRow in gridView.Rows)
        //{
        //    DT.Rows.Add(GridViewRow.Cells[0].Text
        //              , GridViewRow.Cells[1].Text.Replace("<br />", "\\n")
        //              , ((TextBox)GridViewRow.Cells[2].FindControl("txtValue")).Text
        //              );
        //}

        //if (DT.Rows.Count > 0)
        //{
        //    string strFile = HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8);
        //    if (LiAsExcelDatabase.LiAsExcelDB.SaveExcel2("temp/" + strFile, DS, true))
        //    {
        //        DownloadFile("temp/" + strFile);
        //    }
        //}

        StringBuilder dataToExport = new StringBuilder();

        dataToExport.Append("<div><table class='translate' cellspacing='0' cellpadding='3' rules='all' border='1' id='' width='1000'style='border-collapse:collapse;'>\n");
        dataToExport.AppendFormat("<tr><th scope='col'>{0}</th><th align='center' scope='col'>{1}</th><th align='center' scope='col'>{2}</th></tr>\n"
                        , gridView.HeaderRow.Cells[0].Text
                        , gridView.HeaderRow.Cells[1].Text
                        , gridView.HeaderRow.Cells[2].Text
                        , ddlLngToTranslate.SelectedValue);


        foreach (GridViewRow GridViewRow in gridView.Rows)
        {
            dataToExport.AppendFormat("<tr><td align='center' style='width:5%;'>{0}</td><td style='width:45%;'>{1}</td><td style='width:45%;'>{2}</td></tr>\n"
                            , GridViewRow.Cells[0].Text
                            , GridViewRow.Cells[1].Text.Replace("<br />", "\\n")
                            , ((TextBox)GridViewRow.Cells[2].FindControl("txtValue")).Text);
        }

        dataToExport.Append("</table></div>");

        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/vnd.ms-excel";
        //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8)));
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
        Response.Charset = "";
        this.EnableViewState = false;
        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n");
        Response.Write(dataToExport.ToString());
        Response.Flush();
        Response.End();
    }

    //public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
    //{
    //    // Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
    //}

    public bool DownloadFile(string url)
    {
        string URL = url;
        System.IO.FileInfo fileInfo = new System.IO.FileInfo(URL);

        if (fileInfo.Exists)
        {
            Response.ContentType = "application/x-download";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(fileInfo.Name));
            Response.Clear();
            Response.WriteFile(fileInfo.FullName);
            Response.End();
        }

        return true;
    }

    // 엑셀 화일 열기 버튼 핸들러
    // 업로드된 화일을 서버에 저장한 후 저장된 서버의 엑셀화일을 열어 번역내용을 화면 그리드뷰에 써준다.
    // 인터넷에서 찾은 LiAsExcelDB 클래스의 OpenExcelDB 함수를 이용하여 엑셀내용을 DataSet으로 변환하여 그리드뷰로 바인딩한다.
    // 그리드뷰 바인딩 후 서버에 저장된 화일은 삭제한다.
    protected void btnApplyExcel_Click(object sender, EventArgs e)
    {
        if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
        {            
            string folderPath = Server.MapPath("./");
            string fileName = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
            
            string uniqueFullFileName = Common.GetUniqueFileName(folderPath, fileName);

            FileUpload1.PostedFile.SaveAs(uniqueFullFileName);

            try
            {
                // 엑셀 화일을 읽고 쓰는 클래스임. 인터넷에서 찾은 소스임(DID.COMMON.FRAMEWORK 에 속해있음)
                DataSet ds = LiAsExcelDatabase.LiAsExcelDB.OpenExcelDB(uniqueFullFileName);

                // 데이터 바인딩을 위해 컬럼명을 GridView2에 설정된 DataField 명으로 지정
                ds.Tables[0].Columns[0].ColumnName = "name"; 
                ds.Tables[0].Columns[2].ColumnName = "value";
                
                // 숨겨져있는 gridview2 에 데이터 바인딩
                GridView2.DataSource = ds;
                GridView2.DataBind();
            }
            catch
            {
                ScriptExecute(Resources.Resource.msg_0043); // 온전한 엑셀 화일형식이 아닙니다.\n엑셀을 이용하여 엑셀 형식으로 저장한 후 다시 시도해 주십시요
            }
            finally
            {
                File.Delete(uniqueFullFileName);
            }           
        }
    }

}


