﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using System.Globalization;

using DID.Common.Framework;

public partial class generalSetting_siteLanguage : BasePage
{
    enum Gridview1_Columns { LANGUAGE_CODE, IS_TRANSLATED, ENGLISH_NAME, IN_USE, ACTION };

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0046; // 언어 설정

        initLanguageCombo();
    }

    // culture 정보를 가져와서 콤보박스를 만든다
    protected void initLanguageCombo()
    {
        List<string> list = new List<string>();
        foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.NeutralCultures))
        {
            if (ci.Name.IsNullOrEmpty()) continue;
            list.Add(String.Format("{0},{1}", ci.EnglishName, ci.Name));
        }

        list.Sort();  // sort by name

        foreach (string cultureText in list)
        {
            string[] temp = cultureText.Split(',');

            ListItem addList = new ListItem();
            addList.Text = temp[0];
            addList.Value = temp[1];

            ddlLanguage.Items.Add(addList);
        }
    }

    protected void GridView1_PreRender(object sender, EventArgs e)
    {
        refreshGridView1(); // Gridview1 새로고침
    }

    protected void refreshGridView1()
    {
        // DB의 ubc_culture 테이블조회
        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        DataTable dt = obj.GetSiteLanguages(Profile.COMP_TYPE);
     
        // Gridview1 refresh
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
       

    // Language 등록 버튼 클릭 이벤트 핸들러
    protected void btnAddLanguage_Click(object sender, EventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        // 이미 등록된 cultureName 일경우 return
        if (Common.findTextInGridview(GridView1, (int)Gridview1_Columns.LANGUAGE_CODE, ddlLanguage.SelectedValue) != -1)
        {
            ScriptExecute(Resources.Resource.msg_0057); // 이미 등록된 언어입니다.
            return;
        }

        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        obj.InsertSiteLanguage(Profile.COMP_TYPE, ddlLanguage.SelectedValue, ddlLanguage.SelectedItem.Text, false);

        ScriptExecute(Resources.Resource.msg_0025); // 등록 되었습니다.
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // 변경/삭제 버튼 클릭시 컨펌하기
            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                foreach (Control control in cell.Controls)
                {
                    Button button = control as Button;
                    if (button != null)
                    {
                        if (button.CommandName == "Delete")
                        {
                            button.OnClientClick = "if (!confirm('" + Resources.Resource.msg_0017 + "')) return false;"; // 삭제 하시겠습니까?
                        }
                    }
                }
            }

           // 현재행의 cultureName
            string cultureName = GridView1.DataKeys[e.Row.RowIndex].Values[1].ToString();

            string resxFileName = string.Format("Resource.{0}.resx", cultureName);

            // 글로벌 리소스 폴더 전체경로
            string path = Server.MapPath("~/App_GlobalResources/");

            // 리소스 화일 조회(전체 경로)
            if( File.Exists( Path.Combine(path, resxFileName) ) )
            {
                e.Row.Cells[(int)Gridview1_Columns.IS_TRANSLATED].Text = string.Format("<span class=\"button small icon\" onclick=\"location.href='translator.aspx?cultureName={0}'\"><span class=\"check\"></span><button type=\"button\">View Translation</button></span>", cultureName);                
                
            }
            else
            {
                e.Row.Cells[(int)Gridview1_Columns.IS_TRANSLATED].Text = string.Format("<span class=\"button small icon\" onclick=\"location.href='translator.aspx?cultureName={0}'\"><span class=\"add\"></span><button type=\"button\">Add Translation</button></span>", cultureName);                
            }           
        }        
    }

    // GridView1 업데이트 버튼 클릭 핸들러    
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        // 삭제할 현재행 키값
        string customerId = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string cultureName = GridView1.DataKeys[e.RowIndex].Values[1].ToString();

        // 사용여부
        bool useYN = ((CheckBox)GridView1.Rows[e.RowIndex].Cells[(int)Gridview1_Columns.IN_USE].Controls[0]).Checked;

        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        obj.UpdateSiteLanguage(customerId, cultureName, useYN);

        GridView1.EditIndex = -1;

        ScriptExecute(Resources.Resource.msg_0024); // 수정 되었습니다.
    }

    // GridView1 삭제 버튼 클릭 핸들러
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        // 삭제할 현재행 키값
        string customerId = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string cultureName = GridView1.DataKeys[e.RowIndex].Values[1].ToString();

        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        obj.DeleteSiteLanguage(customerId, cultureName);

        GridView1.EditIndex = -1;

        ScriptExecute(Resources.Resource.msg_0047); // 삭제 되었습니다
    }

    // GridView1 수정 버튼 클릭 핸들러
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {        
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        GridView1.EditIndex = e.NewEditIndex;
    }

    // GridView1 취소 버튼 클릭시    
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
    }   
}
