﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="generalSettings.aspx.cs" Inherits="manager_generalSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<style type="text/css">

#menu_button {
	padding: 1em 0.5em 1em 0.5em;
	margin-bottom: 1em;
	background-color: #90bade;
	color: #333;
       
	/*CSS3 properties*/
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    
    box-shadow: rgb(0,0,0) 4px 2px 6px;    
    -webkit-box-shadow: rgb(0,0,0) 4px 2px 6px;
    -moz-box-shadow: rgb(0,0,0) 4px 2px 6px;
    
	}
	
#menu_button ul 
{
	width: 100%;
	
	list-style: none;
	margin: 0;
	padding: 0;
	border: none;
	
	/*CSS3 properties*/
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
	}
		
#menu_button li {
	border-bottom: 1px solid #90bade;
	margin: 0;
	
	/*CSS3 properties*/
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
	}
		
#menu_button li a {
	display: block;
	padding: 5px 5px 5px 0.5em;
	border-left: 10px solid #1958b7;
	border-right: 10px solid #508fc4;
	background-color: #2175bc;
	color: #fff;
	text-decoration: none;
	width: 100%;
	
	/*CSS3 properties*/
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
	}

html>body #menu_button li a {
	width: auto;
	}

#menu_button li a:hover {
	border-left: 10px solid #1c64d1;
	border-right: 10px solid #5ba3e0;
	background-color: #2586d7;
	color: #fff;
	}
				
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div>
    <table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
    <tr>
    <td>
        <span class="subTitle"><img src="/Asset/Images/body/bullet01.gif" /> <%= this.Title %> <!-- 사이트 설정 //--></span>
    </td>
    </tr>
    </table>
</div>



<div id="menu_button">
    <ul >
        <li >
            <a href="./generalSettings/siteUrl.aspx"> <%= Resources.Resource.ttl_0045 %> <!-- URL 설정 //--></a>
        </li>
        <li >
            <a href="./generalSettings/siteCopyUrl.aspx"> <%= Resources.Resource.ttl_0048 %> <!-- URL 설정 //--></a>
        </li>
        <li>
            <a href="./generalSettings/siteLanguage.aspx"> <%= Resources.Resource.ttl_0046 %> <!-- 언어 설정 //--></a>
        </li>
        
        
    </ul>
</div>        
    
</asp:Content>

