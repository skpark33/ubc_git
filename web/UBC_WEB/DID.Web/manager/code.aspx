﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="code.aspx.cs" Inherits="manager_code" Title=""  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript">

    function NewCodeSave()
    {
       
        if( $("#<%=NewCodeName.ClientID %>").val() == "" )
        {
            alert("<%= Resources.Resource.msg_0031 %>"); // 카테고리명을 입력하세요
            $("#<%=NewCodeName.ClientID %>").focus();
            return false ;
        }

        return true;        
    }
     
    function NewCodeDetailSave() {


        if ($("#<%=hiddenCategoryName.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0032 %>"); // 코드를 추가할 카테고리를 상위 목록에서 선택해 주세요
            return false;
        }
        
        if( $("#<%=txtEnumString.ClientID %>").val() == "" )
        {
            alert("<%= Resources.Resource.msg_0033 %>"); // 코드명을 입력하세요
            $("#<%=txtEnumString.ClientID %>").focus();
            return false;
        }
                
        return true;
    }

    
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    
<asp:TextBox ID="hiddenCategoryName" runat="server"   style="display:none"></asp:TextBox>     
 
  
 <!------------------------------------------------------------------------->
 
<table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
<colgroup>
<col width="auto" />
<col width="50%" />
</colgroup>
<tr>
    <td><span class="subTitle"><span id=""><img src="/Asset/Images/body/bullet01.gif" /> 
    <%= Resources.Resource.txt_0032 %> <!-- 코드 관리 //--></span></span> </td>
</tr>
</table>

<table style="width:810px;height:100%" class="newContentsWrap" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td class="contentsOverflow"><!-- new contents -->
        <div class="newContents">
            <!-- new contents inner -->
            <table style="width:100%;height:100%" class="newContentsInner" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td style="vertical-align:top" >
                    <!--테이블-->
                    <%= Resources.Resource.txt_0033 %> <!-- 카테고리 관리 //-->
                    <asp:Panel ID="Panel1" runat="server" >                 
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="codeId"
                            CssClass="boardListType2"
                            CellPadding="4" GridLines="Vertical"                             
                             
                            onprerender="GridView1_PreRender" 
                            onrowediting="GridView1_RowEditing" 
                            onrowcancelingedit="GridView1_RowCancelingEdit" 
                            onrowupdating="GridView1_RowUpdating" 
                            onrowdeleting="GridView1_RowDeleting" 
                            onselectedindexchanging="GridView1_SelectedIndexChanging" 
                            onrowdatabound="GridView1_RowDataBound"
                        >
                        <Columns>                             
                            <asp:BoundField DataField="categoryName" HeaderText="<%$ Resources: Resource, txt_0036 %>" ItemStyle-HorizontalAlign=Center SortExpression="categoryName" />
                            <asp:BoundField DataField="codeId" HeaderText="" Visible="false" />
                            <asp:CommandField ButtonType="Button" HeaderText="Action" ItemStyle-HorizontalAlign=Center
                                ShowEditButton="true" ShowDeleteButton="true" ShowSelectButton="true" />
                                
                        </Columns>
                        </asp:GridView>
                    </asp:Panel>
                    <!--//테이블-->
                    <!--등록-->
                    <div class="edit_area">                    
                        <img src="/Asset/Images/agronet/bul_arr01.gif" alt="" /> <%= Resources.Resource.txt_0035 %> <!-- New 카테고리 //-->
                        <asp:TextBox ID="NewCodeName" CssClass="textType"  runat="server" />
                        <asp:Button ID="NewCodeSave" runat="server" Text="<%$ Resources: Resource, btn_0003 %>" OnClientClick="return NewCodeSave();" OnClick="btnNewCodeName_Click"/>
                    </div>
                    <!--//등록-->
                    <br />
                    <%= Resources.Resource.txt_0034 %> <!-- 카테고리 코드 관리 //--><asp:Label ID="lblcategoryName" runat="server" Font-Size="Large" Font-Bold="true" ForeColor="Red" />
                     <!--테이블-->
                    <asp:Panel ID="Panel2" runat="server" > 
                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                            DataKeyNames="codeId"
                            CssClass="boardListType2"

                            onrowediting="GridView2_RowEditing" 
                            onprerender="GridView2_PreRender" 
                            onrowcancelingedit="GridView2_RowCancelingEdit" 
                            onrowdeleting="GridView2_RowDeleting"   
                            onrowdatabound="GridView2_RowDataBound" 
                            onrowupdating="GridView2_RowUpdating" 
                        >
                        <Columns>

                            <asp:BoundField DataField="categoryName" HeaderText="<%$ Resources: Resource, txt_0036 %>" HeaderStyle-Width="120px" ReadOnly="true" ItemStyle-HorizontalAlign=Center SortExpression="categoryName" />
                            <asp:BoundField DataField="enumString" HeaderText="<%$ Resources: Resource, txt_0037 %>"  ItemStyle-HorizontalAlign=Center SortExpression="enumString" />
                            <asp:BoundField DataField="enumNumber" HeaderText="<%$ Resources: Resource, txt_0038 %>" ReadOnly=true HeaderStyle-Width="100px" ItemStyle-HorizontalAlign=Center SortExpression="enumNumber" />
                            <asp:TemplateField HeaderText="<%$ Resources: Resource, txt_0039 %>" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign=Center>                                  
                            <ItemTemplate>        
                                <asp:DropDownList ID="ddlDorder" runat="server" Enabled=false >
                                    <asp:ListItem Value="0">0</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                    <asp:ListItem Value="13">13</asp:ListItem>
                                    <asp:ListItem Value="14">14</asp:ListItem>
                                    <asp:ListItem Value="15">15</asp:ListItem>
                                    <asp:ListItem Value="16">16</asp:ListItem>
                                    <asp:ListItem Value="17">17</asp:ListItem>
                                    <asp:ListItem Value="18">18</asp:ListItem>
                                    <asp:ListItem Value="19">19</asp:ListItem>
                                    <asp:ListItem Value="20">20</asp:ListItem>
                                    <asp:ListItem Value="21">21</asp:ListItem>
                                    <asp:ListItem Value="22">22</asp:ListItem>
                                    <asp:ListItem Value="23">23</asp:ListItem>
                                    <asp:ListItem Value="24">24</asp:ListItem>
                                    <asp:ListItem Value="25">25</asp:ListItem>
                                    <asp:ListItem Value="26">26</asp:ListItem>
                                    <asp:ListItem Value="27">27</asp:ListItem>
                                    <asp:ListItem Value="28">28</asp:ListItem>
                                    <asp:ListItem Value="29">29</asp:ListItem>
                                    <asp:ListItem Value="30">30</asp:ListItem>
                                    <asp:ListItem Value="31">31</asp:ListItem>
                                    <asp:ListItem Value="32">32</asp:ListItem>
                                    <asp:ListItem Value="33">33</asp:ListItem>
                                    <asp:ListItem Value="34">34</asp:ListItem>
                                    <asp:ListItem Value="35">35</asp:ListItem>
                                    <asp:ListItem Value="36">36</asp:ListItem>
                                    <asp:ListItem Value="37">37</asp:ListItem>
                                    <asp:ListItem Value="38">38</asp:ListItem>
                                    <asp:ListItem Value="39">39</asp:ListItem>
                                    <asp:ListItem Value="40">40</asp:ListItem>
                                    <asp:ListItem Value="41">41</asp:ListItem>
                                    <asp:ListItem Value="42">42</asp:ListItem>
                                    <asp:ListItem Value="43">43</asp:ListItem>
                                    <asp:ListItem Value="44">44</asp:ListItem>
                                    <asp:ListItem Value="45">45</asp:ListItem>
                                    <asp:ListItem Value="46">46</asp:ListItem>
                                    <asp:ListItem Value="47">47</asp:ListItem>
                                    <asp:ListItem Value="48">48</asp:ListItem>
                                    <asp:ListItem Value="49">49</asp:ListItem>
                                    <asp:ListItem Value="50">50</asp:ListItem>
                                    <asp:ListItem Value="51">51</asp:ListItem>
                                    <asp:ListItem Value="52">52</asp:ListItem>
                                    <asp:ListItem Value="53">53</asp:ListItem>
                                    <asp:ListItem Value="54">54</asp:ListItem>
                                    <asp:ListItem Value="55">55</asp:ListItem>
                                    <asp:ListItem Value="56">56</asp:ListItem>
                                    <asp:ListItem Value="57">57</asp:ListItem>
                                    <asp:ListItem Value="58">58</asp:ListItem>
                                    <asp:ListItem Value="59">59</asp:ListItem>
                                    <asp:ListItem Value="60">60</asp:ListItem>
                                    <asp:ListItem Value="61">61</asp:ListItem>
                                    <asp:ListItem Value="62">62</asp:ListItem>
                                    <asp:ListItem Value="63">63</asp:ListItem>
                                    <asp:ListItem Value="64">64</asp:ListItem>
                                    <asp:ListItem Value="65">65</asp:ListItem>
                                    <asp:ListItem Value="66">66</asp:ListItem>
                                    <asp:ListItem Value="67">67</asp:ListItem>
                                    <asp:ListItem Value="68">68</asp:ListItem>
                                    <asp:ListItem Value="69">69</asp:ListItem>
                                    <asp:ListItem Value="70">70</asp:ListItem>
                                    <asp:ListItem Value="71">71</asp:ListItem>
                                    <asp:ListItem Value="72">72</asp:ListItem>
                                    <asp:ListItem Value="73">73</asp:ListItem>
                                    <asp:ListItem Value="74">74</asp:ListItem>
                                    <asp:ListItem Value="75">75</asp:ListItem>
                                    <asp:ListItem Value="76">76</asp:ListItem>
                                    <asp:ListItem Value="77">77</asp:ListItem>
                                    <asp:ListItem Value="78">78</asp:ListItem>
                                    <asp:ListItem Value="79">79</asp:ListItem>
                                    <asp:ListItem Value="80">80</asp:ListItem>
                                    <asp:ListItem Value="81">81</asp:ListItem>
                                    <asp:ListItem Value="82">82</asp:ListItem>
                                    <asp:ListItem Value="83">83</asp:ListItem>
                                    <asp:ListItem Value="84">84</asp:ListItem>
                                    <asp:ListItem Value="85">85</asp:ListItem>
                                    <asp:ListItem Value="86">86</asp:ListItem>
                                    <asp:ListItem Value="87">87</asp:ListItem>
                                    <asp:ListItem Value="88">88</asp:ListItem>
                                    <asp:ListItem Value="89">89</asp:ListItem>
                                    <asp:ListItem Value="90">90</asp:ListItem>
                                    <asp:ListItem Value="91">91</asp:ListItem>
                                    <asp:ListItem Value="92">92</asp:ListItem>
                                    <asp:ListItem Value="93">93</asp:ListItem>
                                    <asp:ListItem Value="94">94</asp:ListItem>
                                    <asp:ListItem Value="95">95</asp:ListItem>
                                    <asp:ListItem Value="96">96</asp:ListItem>
                                    <asp:ListItem Value="97">97</asp:ListItem>
                                    <asp:ListItem Value="98">98</asp:ListItem>
                                    <asp:ListItem Value="99">99</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="<%$ Resources: Resource, txt_0040 %>" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign=Center>      
                            <ItemTemplate>        
                                <asp:DropDownList ID="ddlVisible"  runat="server" Enabled=false>        
                                    <asp:ListItem Value="1" Text="<%$ Resources: Resource, txt_0041 %>" />
                                    <asp:ListItem Value="0" Text="<%$ Resources: Resource, txt_0042 %>" />
                                </asp:DropDownList>      
                            </ItemTemplate>
                            </asp:TemplateField>                               
                            
                            <asp:BoundField DataField="dorder"  ItemStyle-Width="0" HeaderStyle-Width="0"  ShowHeader="False"  Visible=false />
                            <asp:BoundField DataField="visible" ItemStyle-Width="0" HeaderStyle-Width="0"   ShowHeader="False" Visible=false  />

                            <asp:CommandField ButtonType="button" HeaderStyle-Width="150px"
                                ItemStyle-HorizontalAlign="Center" HeaderText="Action" 
                                ShowEditButton="true" ShowDeleteButton="true" />

                        </Columns>

                        </asp:GridView>
                      
                    </asp:Panel>
                    <!--//테이블-->
                    <!--등록-->
                    <div class="edit_area">
                        <asp:Table ID="newCodeDetailTable" Bo  rderWidth="0" BorderStyle="Solid"  CellPadding="4" runat="server"  >
                            <asp:TableRow >    
                                
                                <asp:TableCell> 
                                    <img src="/Asset/Images/agronet/bul_arr01.gif" alt="" />
                                </asp:TableCell>
                                <asp:TableCell><%= Resources.Resource.txt_0043 %><!-- 코드명 //--></asp:TableCell>                                
                                <asp:TableCell> 
                                    <asp:TextBox ID="txtEnumString" CssClass="textType" width="70" runat="server" />
                                </asp:TableCell>                                
                                <asp:TableCell> 
                                    <img src="/Asset/Images/agronet/bul_arr01.gif" />
                                </asp:TableCell>
                                <asp:TableCell><%= Resources.Resource.txt_0039 %><!-- 표시 순서 //--></asp:TableCell>
                                <asp:TableCell> 
                                    <asp:DropDownList ID="ddlDorderReg" runat="server" >
                                        <asp:ListItem Value="0">0</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                        <asp:ListItem Value="13">13</asp:ListItem>
                                        <asp:ListItem Value="14">14</asp:ListItem>
                                        <asp:ListItem Value="15">15</asp:ListItem>
                                        <asp:ListItem Value="16">16</asp:ListItem>
                                        <asp:ListItem Value="17">17</asp:ListItem>
                                        <asp:ListItem Value="18">18</asp:ListItem>
                                        <asp:ListItem Value="19">19</asp:ListItem>
                                        <asp:ListItem Value="20">20</asp:ListItem>
                                        <asp:ListItem Value="21">21</asp:ListItem>
                                        <asp:ListItem Value="22">22</asp:ListItem>
                                        <asp:ListItem Value="23">23</asp:ListItem>
                                        <asp:ListItem Value="24">24</asp:ListItem>
                                        <asp:ListItem Value="25">25</asp:ListItem>
                                        <asp:ListItem Value="26">26</asp:ListItem>
                                        <asp:ListItem Value="27">27</asp:ListItem>
                                        <asp:ListItem Value="28">28</asp:ListItem>
                                        <asp:ListItem Value="29">29</asp:ListItem>
                                        <asp:ListItem Value="30">30</asp:ListItem>
                                        <asp:ListItem Value="31">31</asp:ListItem>
                                        <asp:ListItem Value="32">32</asp:ListItem>
                                        <asp:ListItem Value="33">33</asp:ListItem>
                                        <asp:ListItem Value="34">34</asp:ListItem>
                                        <asp:ListItem Value="35">35</asp:ListItem>
                                        <asp:ListItem Value="36">36</asp:ListItem>
                                        <asp:ListItem Value="37">37</asp:ListItem>
                                        <asp:ListItem Value="38">38</asp:ListItem>
                                        <asp:ListItem Value="39">39</asp:ListItem>
                                        <asp:ListItem Value="40">40</asp:ListItem>
                                        <asp:ListItem Value="41">41</asp:ListItem>
                                        <asp:ListItem Value="42">42</asp:ListItem>
                                        <asp:ListItem Value="43">43</asp:ListItem>
                                        <asp:ListItem Value="44">44</asp:ListItem>
                                        <asp:ListItem Value="45">45</asp:ListItem>
                                        <asp:ListItem Value="46">46</asp:ListItem>
                                        <asp:ListItem Value="47">47</asp:ListItem>
                                        <asp:ListItem Value="48">48</asp:ListItem>
                                        <asp:ListItem Value="49">49</asp:ListItem>
                                        <asp:ListItem Value="50">50</asp:ListItem>
                                        <asp:ListItem Value="51">51</asp:ListItem>
                                        <asp:ListItem Value="52">52</asp:ListItem>
                                        <asp:ListItem Value="53">53</asp:ListItem>
                                        <asp:ListItem Value="54">54</asp:ListItem>
                                        <asp:ListItem Value="55">55</asp:ListItem>
                                        <asp:ListItem Value="56">56</asp:ListItem>
                                        <asp:ListItem Value="57">57</asp:ListItem>
                                        <asp:ListItem Value="58">58</asp:ListItem>
                                        <asp:ListItem Value="59">59</asp:ListItem>
                                        <asp:ListItem Value="60">60</asp:ListItem>
                                        <asp:ListItem Value="61">61</asp:ListItem>
                                        <asp:ListItem Value="62">62</asp:ListItem>
                                        <asp:ListItem Value="63">63</asp:ListItem>
                                        <asp:ListItem Value="64">64</asp:ListItem>
                                        <asp:ListItem Value="65">65</asp:ListItem>
                                        <asp:ListItem Value="66">66</asp:ListItem>
                                        <asp:ListItem Value="67">67</asp:ListItem>
                                        <asp:ListItem Value="68">68</asp:ListItem>
                                        <asp:ListItem Value="69">69</asp:ListItem>
                                        <asp:ListItem Value="70">70</asp:ListItem>
                                        <asp:ListItem Value="71">71</asp:ListItem>
                                        <asp:ListItem Value="72">72</asp:ListItem>
                                        <asp:ListItem Value="73">73</asp:ListItem>
                                        <asp:ListItem Value="74">74</asp:ListItem>
                                        <asp:ListItem Value="75">75</asp:ListItem>
                                        <asp:ListItem Value="76">76</asp:ListItem>
                                        <asp:ListItem Value="77">77</asp:ListItem>
                                        <asp:ListItem Value="78">78</asp:ListItem>
                                        <asp:ListItem Value="79">79</asp:ListItem>
                                        <asp:ListItem Value="80">80</asp:ListItem>
                                        <asp:ListItem Value="81">81</asp:ListItem>
                                        <asp:ListItem Value="82">82</asp:ListItem>
                                        <asp:ListItem Value="83">83</asp:ListItem>
                                        <asp:ListItem Value="84">84</asp:ListItem>
                                        <asp:ListItem Value="85">85</asp:ListItem>
                                        <asp:ListItem Value="86">86</asp:ListItem>
                                        <asp:ListItem Value="87">87</asp:ListItem>
                                        <asp:ListItem Value="88">88</asp:ListItem>
                                        <asp:ListItem Value="89">89</asp:ListItem>
                                        <asp:ListItem Value="90">90</asp:ListItem>
                                        <asp:ListItem Value="91">91</asp:ListItem>
                                        <asp:ListItem Value="92">92</asp:ListItem>
                                        <asp:ListItem Value="93">93</asp:ListItem>
                                        <asp:ListItem Value="94">94</asp:ListItem>
                                        <asp:ListItem Value="95">95</asp:ListItem>
                                        <asp:ListItem Value="96">96</asp:ListItem>
                                        <asp:ListItem Value="97">97</asp:ListItem>
                                        <asp:ListItem Value="98">98</asp:ListItem>
                                        <asp:ListItem Value="99">99</asp:ListItem>
                                    </asp:DropDownList> 
                                </asp:TableCell>                                                                    
                                <asp:TableCell> 
                                    <img src="/Asset/Images/agronet/bul_arr01.gif" />
                                </asp:TableCell>
                                <asp:TableCell><%= Resources.Resource.txt_0040 %><!-- 사용여부 //--></asp:TableCell>    
                                <asp:TableCell> 
                                    <asp:DropDownList ID="ddlVisibleReg" runat="server">
                                        <asp:ListItem Value="1" Text="<%$ Resources: Resource, txt_0041 %>" />
                                        <asp:ListItem Value="0" Text="<%$ Resources: Resource, txt_0042 %>" />
                                    </asp:DropDownList> 
                                </asp:TableCell>                                
                                <asp:TableCell>                                    
                                    <asp:Button ID="newCodeDetailAdd" runat="server" Text="<%$ Resources: Resource, btn_0003 %>" OnClientClick="return NewCodeDetailSave();" OnClick="newCodeDetailAdd_Click"/>
                                </asp:TableCell>                                
                                
                            </asp:TableRow>
                            
                        </asp:Table>
                        
                    </div>
                    <!--//등록-->
                </td>
            </tr>
            </table>
        </div>
    </td>
</tr>
</table>


</ContentTemplate></asp:UpdatePanel>
</asp:Content>

