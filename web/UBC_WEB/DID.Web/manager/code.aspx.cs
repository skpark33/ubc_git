﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using DID.Common.Framework;
 
public partial class manager_code : BasePage
{ 
    protected void Page_Load(object sender, EventArgs e)
    {       
        if (!this.IsPostBack)
        {
            // 등록 : 순서 콤보 초기화
            ddlDorderReg.SelectedIndex = ddlDorderReg.Items.Count - 1;            
        }    
    }

    // 카테고리  불러오기
    protected void CodeList()
    {
        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            DataTable dt = obj.GetCodeList(Profile.COMP_TYPE);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }

    /////////////////////////// 카테고리 관리 ////////////////////////        


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    { 

        //삭제버튼 클릭시 컨펌하기
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            foreach (DataControlFieldCell cell in e.Row.Cells)
            {
                foreach (Control control in cell.Controls)
                {
                    //ImageButton button = control as ImageButton;
                    Button button = control as Button;

                    if (button != null)
                    {
                        if (button.CommandName == "Delete")
                            button.OnClientClick = "if (!confirm('" + Resources.Resource.msg_0035 + "')) return false;"; // 카테고리 삭제 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다\\n카테고리에 속해있는 모든 코드값도 삭제됩니다\\n계속 하시겠습니까?
                        else if (button.CommandName == "Update")
                            button.OnClientClick = "if (!confirm('" + Resources.Resource.msg_0036 + "')) return false;"; // 카테고리 변경 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다\\n계속 하시겠습니까?
                    }


                }
            }
        } 
    }

    protected void GridView1_PreRender(object sender, EventArgs e)
    {
        this.CodeList();    
    }

    // 카테고리 수정 버튼 클릭시 쓰기모드 활성화.
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        GridView1.SelectedIndex = -1;
        GridView1.EditIndex = e.NewEditIndex;
    }

    // 카테고리 취소 버튼 클릭시
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        GridView1.EditIndex = -1;
    }

    // 카테고리 신규등록
    protected void btnNewCodeName_Click(object sender, EventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string codeNameNew = NewCodeName.Text.Trim();

            if (Common.findTextInGridview(GridView1, 0, codeNameNew) != -1)
            {
                ScriptExecute(Resources.Resource.msg_0034); // 같은 이름의 카테고리명이 존재합니다.\\n다른이름으로 등록하세요
                return;
            }

            obj.GetCodeInsert(Profile.COMP_TYPE, codeNameNew);
        }
    }

    // 카테고리 업데이트 버튼 클릭시
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string codeId = GridView1.DataKeys[e.RowIndex].Value.ToString();
            string categoryName = ((TextBox)GridView1.Rows[e.RowIndex].Cells[0].Controls[0]).Text;

            if (Common.findTextInGridview(GridView1, 0, categoryName) != -1)
            {
                ScriptExecute(Resources.Resource.msg_0034); // 같은 이름의 카테고리명이 존재합니다.\\n다른이름으로 등록하세요
                return;
            }

            obj.GetCodeUpdate(Profile.COMP_TYPE, codeId, categoryName);
        }

        GridView1.EditIndex  = -1;
    }

    // 카테고리 삭제 클릭시
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            
            string codeId = GridView1.DataKeys[e.RowIndex].Value.ToString();
            
            obj.GetCodeDelete(Profile.COMP_TYPE, codeId);
        }

        // 현재 선택되어 있는 카테고리가 삭제되었을 경우 카테고리 선택값 초기화
        if( GridView1.Rows[e.RowIndex].Cells[0].Text == hiddenCategoryName.Text )
        {
            hiddenCategoryName.Text = "";
            lblcategoryName.Text = "";
        }
    }
    
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        hiddenCategoryName.Text = GridView1.Rows[e.NewSelectedIndex].Cells[0].Text;
        lblcategoryName.Text = "[" + GridView1.Rows[e.NewSelectedIndex].Cells[0].Text + "]"  ;
    }     
 
    /////////////////////////// 코드 관리 ////////////////////////        

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            // 숨겨진 컬럼값으로 콤보를 셋팅한다 (표시순서)
            DropDownList ddl = (DropDownList)e.Row.Cells[3].FindControl("ddlDorder");
            ddl.SelectedValue = drv["dorder"].ToString();

            // 숨겨진 컬럼값으로 콤보를 셋팅한다 (사용여부)
            ddl = (DropDownList)e.Row.Cells[4].FindControl("ddlVisible");
            ddl.SelectedValue = drv["visible"].ToString();

            //삭제버튼 클릭시 컨펌하기
            foreach (DataControlFieldCell cell in e.Row.Cells)
            {                
                foreach (Control control in cell.Controls)
                {
                    Button button = control as Button;

                    if (button != null)
                    {
                        if (button.CommandName == "Delete")
                        {
                            button.OnClientClick = "if (!confirm('" +  Resources.Resource.msg_0037 + "')) return false;"; // 코드 삭제 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다.\\n계속 하시겠습니까?
                        }
                        else if (button.CommandName == "Update")
                        {
                            button.OnClientClick = "if (!confirm('" + Resources.Resource.msg_0038 + "')) return false;"; // 코드 변경 시 \\n시스템에 심각한 문제가 발생할 수도 있습니다.\\n계속 하시겠습니까?
                        }
                    }

                   
                }

            }

            // 변경시 순서와 사용여부 드롭다운리스트 활성화
            if ((e.Row.RowState & DataControlRowState.Edit) > 0 )
            {
                (e.Row.FindControl("ddlDorder") as DropDownList).Enabled = true;
                (e.Row.FindControl("ddlVisible") as DropDownList).Enabled = true;
                
            }

                      
        }


    }


    protected void GridView2_PreRender(object sender, EventArgs e)
    {
        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            DataTable dt = obj.GetDetailCodeList( Profile.COMP_TYPE, hiddenCategoryName.Text );
            GridView2.DataSource = dt;             
            GridView2.DataBind();
             
        }
    }
  
    // 코드 신규등록
    protected void newCodeDetailAdd_Click(object sender, EventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string enumString = txtEnumString.Text.Trim();
            string dorder = ddlDorderReg.SelectedValue;
            string visible = ddlVisibleReg.SelectedValue;

            if (Common.findTextInGridview(GridView2, 1, enumString) != -1)
            {
                ScriptExecute(Resources.Resource.msg_0039); // 같은 이름의 코드명이 존재합니다.\\n다른 코드명으로 등록하세요
                return;
            }

            obj.GetCodeDetailInsert(  Profile.COMP_TYPE
                                    , hiddenCategoryName.Text
                                    , enumString
                                    , dorder.ToInt()
                                    , visible.ToInt());

        }
    }

    // 코드 업데이트
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string codeId = GridView2.DataKeys[e.RowIndex].Value.ToString();

            string enumString = ((TextBox)GridView2.Rows[e.RowIndex].Cells[1].Controls[0]).Text.Trim();
                        
            DropDownList ddlDorder = (DropDownList)GridView2.Rows[e.RowIndex].Cells[3].FindControl("ddlDorder");
            string dorder = ddlDorder.SelectedValue;

            DropDownList ddlVisible = (DropDownList)GridView2.Rows[e.RowIndex].Cells[4].FindControl("ddlVisible");
            string visible = ddlVisible.SelectedValue;

            if (enumString.IsNullOrEmpty())
            {
                //((TextBox)GridView2.Rows[e.RowIndex].Cells[1].Controls[0]).Focus();
                ScriptExecute(Resources.Resource.msg_0033); // 코드명을 입력하세요
                return;
            }

            if (Common.findTextInGridview(GridView2, 1, enumString) != -1)
            {
                ScriptExecute(Resources.Resource.msg_0039); // 같은 이름의 코드명이 존재합니다.\\n다른 코드명으로 등록하세요
                return;
            }
            
            obj.GetCodeDetailUpdate(  Profile.COMP_TYPE
                                    , codeId
                                    , enumString
                                    , dorder.ToInt()
                                    , visible.ToInt());

        }

        GridView2.EditIndex = -1;         

    }

    // 코드 수정 버튼 클릭시 쓰기모드 활성화.
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        GridView2.SelectedIndex = -1;
        GridView2.EditIndex = e.NewEditIndex;

        
    }

    // 코드 삭제 버튼 클릭시
    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        using (DID.Service.Manager.Code obj = new DID.Service.Manager.Code())
        {
            string codeId = GridView2.DataKeys[e.RowIndex].Value.ToString();
            obj.GetCodeDetailDelete(Profile.COMP_TYPE, codeId);
        }
    }

    // 코드 취소 버튼 클릭시
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        if (Profile.USER_TYPE != "-1") { ScriptExecute(Resources.Resource.msg_0030); return; } // 권한이 없습니다

        GridView2.EditIndex = -1;
    }    
    
}
