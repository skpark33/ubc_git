﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Reflection;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Data.OleDb; 

using DID.Common.Framework; 
public partial class manager_popUploadUser : BasePage
{
    enum Columns { SiteID, UserID, UserPW, UserName, MobileNo, Email, UserType, RoleId };

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            File1.Attributes.Add("onchange", "return checkFileExtension(this);");
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if ((File1.PostedFile != null) && (File1.PostedFile.ContentLength > 0))
        {
            string SaveLocation = Server.MapPath("\\manager\\userExcelFile\\");
            string fn = System.IO.Path.GetFileName(File1.PostedFile.FileName);            
            string uniqueFullFileName = Common.GetUniqueFileName(SaveLocation, fn);

            try
            {

                File1.PostedFile.SaveAs(uniqueFullFileName);

                //아래코드에서 HDR=YES 라고 하면 첫줄은 건너띄고 처리한다. 즉, 두번째 줄부터 데이타테이블에 들어감
                //string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=D:\PROJECT_SOURCE\UBC_WEB_K\DIDSolution.root\DIDSolution\DID.Web\manager\userExcelFile\userFormat.xls;Extended Properties=""Excel 8.0;HDR=YES;IMEX=1""";
                string connectionString =  @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + uniqueFullFileName + ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";
                OleDbConnection   conn = new OleDbConnection(connectionString) ; 
                OleDbCommand      comm = new OleDbCommand();
                OleDbDataAdapter  adap = new OleDbDataAdapter();
                comm.Connection    = conn;
                comm.CommandType   = CommandType.Text;
                comm.CommandText   = "select * from [Sheet1$]";
                adap.SelectCommand = comm;
                System.Data.DataTable   dtXls = new System.Data.DataTable("");
                adap.Fill(dtXls);


                // 조직 = 0 , 아이디 = 1 , 암호 = 2 , 사용자명 = 3

                // 제목을 제외하고, 작성한 건수가 없을때에 체크
                if (dtXls.Rows.Count == 0)
                {
                    if (System.IO.File.Exists(uniqueFullFileName))
                    {  System.IO.File.Delete(uniqueFullFileName); }
                    this.ScriptExecute("엑셀에 사용자 정보가 한 건도 없습니다.");
                    return;
                
                }

                // 이메일 검사 2011-08-10 임유석 추가
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");

                //공백 오류 체크 및 조직/사용자 아이디 중복 체크
                for (int i = 0; i < dtXls.Rows.Count; i++)
                {
                    // 공백오류
                    if (   dtXls.Rows[i][(int)Columns.SiteID]  .ToString().Trim() == "" || dtXls.Rows[i][(int)Columns.UserID]  .ToString().Trim() == ""
                        || dtXls.Rows[i][(int)Columns.UserPW]  .ToString().Trim() == "" || dtXls.Rows[i][(int)Columns.UserName].ToString().Trim() == ""
                        || dtXls.Rows[i][(int)Columns.MobileNo].ToString().Trim() == "" || dtXls.Rows[i][(int)Columns.Email]   .ToString().Trim() == ""
                        || dtXls.Rows[i][(int)Columns.UserType].ToString().Trim() == "" || dtXls.Rows[i][(int)Columns.RoleId]  .ToString().Trim() == "")
                    {
                        int j = i + 1;  // 맨 위에줄은 제목이므로, 오류줄수를 보이기위해서는 두개를 더한다.
                        if (System.IO.File.Exists(uniqueFullFileName))
                        { System.IO.File.Delete(uniqueFullFileName); }
                        this.ScriptExecute(j.ToString() + " 번째 사용자의 정보중에 값이 없는 항목이 있습니다.");
                        return;
                    }

                    // 이메일 검사 2011-08-10 임유석 추가
                    if( !regex.IsMatch(dtXls.Rows[i][(int)Columns.Email].ToString().Trim()) )
                    {
                        int j = i + 1;  // 맨 위에줄은 제목이므로, 오류줄수를 보이기위해서는 두개를 더한다.
                        if (System.IO.File.Exists(uniqueFullFileName))
                        { System.IO.File.Delete(uniqueFullFileName); }
                        this.ScriptExecute(j.ToString() + " 번째 사용자의 E-mail 값이 유효하지 않습니다.");
                        return;
                    }

                    //엑셀내의 아이디중복체크
                    for (int k = 0; k < dtXls.Rows.Count; k++)
                    {
                        // 자기자신은 제외
                        if (i == k) continue; 

                        if (   dtXls.Rows[i][(int)Columns.SiteID].ToString().Trim() == dtXls.Rows[k][(int)Columns.SiteID].ToString().Trim()
                            && dtXls.Rows[i][(int)Columns.UserID].ToString().Trim() == dtXls.Rows[k][(int)Columns.UserID].ToString().Trim())
                        {
                            if (System.IO.File.Exists(uniqueFullFileName))
                            { System.IO.File.Delete(uniqueFullFileName); }
                            int m = k + 1;  // 맨 위에줄은 제목이므로, 오류줄수를 보이기위해서는 두개를 더한다.
                            this.ScriptExecute(m.ToString() + " 번째 사용자의 조직/아이디 중복값이 있습니다.");
                            return;
                        } 
                    } 
                }


                //조직 오류 체크
                for (int i = 0; i < dtXls.Rows.Count; i++)
                {
                    using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
                    {
                        DataTable dt = obj.GetUserSite(dtXls.Rows[i][(int)Columns.SiteID].ToString().Trim(), Profile.COMP_TYPE);
                        
                        if (dt.IsNullOrEmpty() || dt.Rows.Count <= 0)
                        {
                            if (System.IO.File.Exists(uniqueFullFileName))
                            { System.IO.File.Delete(uniqueFullFileName); }
                            int m = i + 1;  // 맨 위에줄은 제목이므로, 오류줄수를 보이기위해서는 두개를 더한다.
                            this.ScriptExecute(m.ToString() + " 번째 사용자의 조직코드가 존재 하지 않습니다. 조직코드를 확인 하시고 만약 등록 되지 않은 조직일 경우 조직 관리 화면을 통해 조직을 먼저 등록한 후 사용자를 등록하셔야 합니다.");
                            return;
                        }
                    } 
                }

                // 기존 디비안의 사용자 아이디와 엑셀내의 사용자아이디   중복 체크
                for (int i = 0; i < dtXls.Rows.Count; i++)
                {
                    using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
                    { 
                        // 조직과 아이디의 중복을 검색한다.
                        DataTable dt = obj.GetUserSiteDup(dtXls.Rows[i][(int)Columns.SiteID].ToString().Trim(), dtXls.Rows[i][(int)Columns.UserID].ToString().Trim());
                        if (dt.Rows.Count >= 1)
                        {
                            if (System.IO.File.Exists(uniqueFullFileName))
                            { System.IO.File.Delete(uniqueFullFileName); }
                            int m = i + 1;  // 맨 위에줄은 제목이므로, 오류줄수를 보이기위해서는 두개를 더한다.
                            this.ScriptExecute(m.ToString() + " 번째 사용자의 조직코드와 아이디가 기존디비에 존재합니다.");
                            return;
                        }
                    }
                }
                
                // 사용자 입력
                for (int i = 0; i < dtXls.Rows.Count; i++)
                {
                    using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
                    {
                        // 입력 시작
                          obj.UserSiteExcelInsert(   dtXls.Rows[i][(int)Columns.SiteID  ].ToString().Trim()
                                                   , dtXls.Rows[i][(int)Columns.UserID  ].ToString().Trim()
                                                   , dtXls.Rows[i][(int)Columns.UserPW  ].ToString().Trim()
                                                   , dtXls.Rows[i][(int)Columns.UserName].ToString().Trim()
                                                   , dtXls.Rows[i][(int)Columns.MobileNo].ToString().Trim()
                                                   , dtXls.Rows[i][(int)Columns.Email   ].ToString().Trim()
                                                   , dtXls.Rows[i][(int)Columns.UserType].ToString().Trim()
                                                   , dtXls.Rows[i][(int)Columns.RoleId  ].ToString().Trim());
                          
                    }
                }

                // 입력되었으면 화일을 삭제한다.

                if (System.IO.File.Exists(uniqueFullFileName))
                {
                    System.IO.File.Delete(uniqueFullFileName);
                    this.ScriptExecute("사용자가 추가 되었습니다.  업로드된 화일은 보안상 삭제 합니다.");
                }

                 
            }
            catch (Exception ex)
            {

                //this.ScriptExecute(ex.Message.ToString());
                //this.ScriptExecute(ex.Message);

            }
             
        }
        else
        {

            this.ScriptExecute("화일을 선택 하세요");

        }

    }


    
}
