﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using System.Reflection; 
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using DID.Common.Framework;


 

public partial class manager_smsresult : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {

            send_phone.Text   = Request["send_phone"];
            recive_phone.Text = Request["recive_phone"];
            contents.Text     = Request["contents"];

            // SMS 날리기  시작
            using (DID.Service.Manager.MmsSms obj = new DID.Service.Manager.MmsSms())
            {
                string send_phone_val = Request["send_phone"].Replace("-","");
                string recive_phone_val = Request["recive_phone"].Replace("-", "");
                string contents_val = Request["contents"];
              

                int ret = obj.smsSend(send_phone_val, recive_phone_val, contents_val   );
            } 

        }
    }

}
