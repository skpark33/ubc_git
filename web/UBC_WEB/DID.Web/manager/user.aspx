﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="user.aspx.cs" Inherits="manager_user" Title="" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
<script type="text/javascript"> 

    function fn_siteListAllSearch() {

        var textSearch = document.aspnetForm.siteListAllSearch.value;
        $("#<%=siteListAll.ClientID %>   option[text=" + textSearch + "]").attr("selected", "true");
        
    }

    // 장애 더블클릭
    function probableCauseListDblClickToRight()
    {
          $("#<%=btnProbableCauseListDblClickToRight.ClientID %>").click();       
    }        
    function probableCauseListDblClickToLeft()
    {
          $("#<%=btnProbableCauseListDblClickToLeft.ClientID %>").click();    
    }        
    
    // 관리대상그룹 더블클릭
    function siteListDblClickToRight()
    {
          $("#<%=btnSiteListDblClickToRight.ClientID %>").click();       
    }        
    function siteListDblClickToLeft()
    {
          $("#<%=btnSiteListDblClickToLeft.ClientID %>").click();    
    }     
    
    // 관리대상단말 더블클릭
    function hostListDblClickToRight()
    {
          $("#<%=btnHostListDblClickToRight.ClientID %>").click();       
    }        
    function hostListDblClickToLeft()
    {
          $("#<%=btnHostListDblClickToLeft.ClientID %>").click();    
    }

    // 비밀번호 강화 정책
    function fnCheckPassword() {

        var upw = $("#<%=txtUserPw1.ClientID %>").val();

        var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
        if (false == strongRegex.test(upw)) {
            // 비밀번호는 알파벳 소문자, 알파벳 대문자, 숫자, 특수문자를 포함한 8자리 이상을 사용해야 합니다.

            alert("<%= Resources.Resource.msg_0000 %>");
            return false;
        }

        return true;

    }    
        
    function userValidation() {

        if ($("#<%=hiddenTxtTreeSiteId.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0004 %>"); // 왼쪽 트리에서 조직을 선택하세요
            return false;
        }

        // 신규일경우
        if ($("#<%=hiddenTxtNewOrUpadte.ClientID %>").val() == "NEW")
        {      

            if ($("#<%=txtUserId.ClientID %>").val() == "") {
                alert("<%= Resources.Resource.msg_0005 %>"); // 사용자 아이디를 입력하세요
                $("#<%=txtUserId.ClientID %>").focus();
                return false;
            }

            if ($("#<%=hiddenTxtUserDupCheck.ClientID %>").val() == "" )
            {
                alert("<%= Resources.Resource.msg_0006 %>"); // 아이디 중복체크를 하셔야 합니다
                return false;
            }
            else
            {
                if ($("#<%=hiddenTxtUserDupCheck.ClientID %>").val() != $("#<%=txtUserId.ClientID %>").val()) {
                    alert("<%= Resources.Resource.msg_0006 %>"); // 아이디 중복체크를 하셔야 합니다
                    return false;
                }
            }

            if ($("#<%=txtUserPw1.ClientID %>").val() == "") {
                alert("<%= Resources.Resource.msg_0007 %>"); // 비밀번호를 입력하세요
                $("#<%=txtUserPw1.ClientID %>").focus()
                return false;
            }

            // 비밀번호 유효성검사
            if (!fnCheckPassword()) {
                $("#<%=txtUserPw1.ClientID %>").focus();
                return false;
            }

            if ($("#<%=txtUserPw2.ClientID %>").val() == "") {
                alert("<%= Resources.Resource.msg_0008 %>"); // 확인 비밀번호를 입력하세요
                $("#<%=txtUserPw2.ClientID %>").focus();
                return false;
            }

            if ($("#<%=txtUserPw1.ClientID %>").val() != $("#<%=txtUserPw2.ClientID %>").val()) {
                alert("<%= Resources.Resource.msg_0009 %>"); // 비밀번호가 불일치합니다
                $("#<%=txtUserPw2.ClientID %>").focus();
                return false;
            }

        }
        //업데이트일경우
        else 
        {
            // 비밀번호를 입력했을 경우만 체크
            if ($("#<%=txtUserPw1.ClientID %>").val() != "" ||
                $("#<%=txtUserPw2.ClientID %>").val() != "" ) 
            {
                // 비밀번호 유효성검사
                if (!fnCheckPassword()) {
                    $("#<%=txtUserPw1.ClientID %>").focus();
                    return false;
                }

                if ($("#<%=txtUserPw2.ClientID %>").val() == "") {
                    alert("<%= Resources.Resource.msg_0008 %>"); // 확인 비밀번호를 입력하세요
                    $("#<%=txtUserPw2.ClientID %>").focus();
                    return false;
                }

                if ($("#<%=txtUserPw1.ClientID %>").val() != $("#<%=txtUserPw2.ClientID %>").val()) {
                    alert("<%= Resources.Resource.msg_0009 %>"); // 비밀번호가 불일치합니다
                    $("#<%=txtUserPw2.ClientID %>").focus();
                    return false;
                }
            }

        }
        
        if( $("#<%=txtUserName.ClientID %>").val() == "" )
        {
            alert("<%= Resources.Resource.msg_0010 %>"); // 사용자명을 입력하세요
            $("#<%=txtUserName.ClientID %>").focus();
            return false ;
        }
        
//        if( $("#<%=txtMobileNo.ClientID %>").val() == "" )
//        {        
//            alert("핸드폰번호를 입력하세요.");
//            $("#<%=txtMobileNo.ClientID %>").focus()
//             return false ;
//        }

        if ($("#<%=txtEmail.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0011 %>"); // 이메일을 입력하세요
            $("#<%=txtEmail.ClientID %>").focus();
            return false ;
        }

        if (checkEmail($("#<%=txtEmail.ClientID %>").val(), "<%= Resources.Resource.msg_0012 %>") == false) { // 유효한 E-mail 주소 형식이 아닙니다
            $("#<%=txtEmail.ClientID %>").focus();
            return false ;
        }        
        
        if( $("#<%=userType.ClientID %>").val() == "" )
        {
            alert("<%= Resources.Resource.msg_0013 %>"); // 사용자 그룹을 선택하세요
            $("#<%=userType.ClientID %>").focus();
             return false ;
        }

        
        if( $("#<%=roleId.ClientID %>").val() == "" )
        {
            alert("<%= Resources.Resource.msg_0014 %>"); // 롤을 선택하세요
            $("#<%=roleId.ClientID %>").focus();
            return false ;
        }

         
        if( $("#<%=validationDate.ClientID %>").val() == "" )
        {
            alert("<%= Resources.Resource.msg_0015 %>"); // 사용자 만료일자를 선택하세요
            $("#<%=validationDate.ClientID %>").focus();
            return false ;
        }

        return true;
           
    }
 
    function userDup()
    {
        if( $("#<%=txtUserId.ClientID %>").val() == "" )
        {
            alert("<%= Resources.Resource.msg_0005 %>"); // 사용자 아이디를 입력하세요
            $("#<%=txtUserId.ClientID %>").focus();
            return false ;
        }

        return true;        
        
    }
     
    function ViewDetail(siteId , userId)
    {
        $("#<%=hiddenTxtSiteId.ClientID %>").val(siteId);
        $("#<%=txtUserId.ClientID %>").val(userId);       
        $("#<%=btnDetailCode.ClientID %>").click();   
    }
 
    
    function userDelete() 
    {
        if ($("#<%=txtUserId.ClientID %>").val() == "" || 
            $("#<%=hiddenTxtSiteId.ClientID %>").val() == "" )
        {
            alert("<%= Resources.Resource.msg_0016 %>"); // 상단의 리스트에서 사용자아이디를 선택하세요
            return false;
        }
        else
        {
            if (!confirm("<%= Resources.Resource.msg_0017 %>")) return false; // 삭제 하시겠습니까?
        }

        return true;
    }

    function popUploadUser() {
        window.open("./popUploadUser.aspx", "popUploadUser", "toolbar=no,scrollbars=no,location=no,directories=no,status=no,menubar=no,resizable=no,width=390,height=430");
    }  
         
</script>

 
 
</asp:Content>
 



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" >

<asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional"  >
<ContentTemplate>


<!----- 숨겨진 변수들 ------------------------------------------------------------------------------->

<asp:TextBox ID="hiddenTxtTreeSiteId" runat="server" style="display:none;"></asp:TextBox> 
<asp:TextBox ID="hiddenTxtTreeSiteName" runat="server" style="display:none;"></asp:TextBox> 
<asp:TextBox ID="hiddenTxtNewOrUpadte" runat="server" style="display:none;"></asp:TextBox> 

<asp:TextBox ID="hiddenTxtSiteId" runat="server" style="display:none;" />
<asp:TextBox ID="hiddenTxtUserDupCheck" runat="server" style="display:none;" />

<asp:Button id="btnProbableCauseListDblClickToRight" runat="server" OnClick="probableToRight_Click" style="display:none"  />
<asp:Button id="btnProbableCauseListDblClickToLeft"  runat="server"  OnClick="probableToLeft_Click"  style="display:none"  />
<asp:Button id="btnSiteListDblClickToRight" runat="server"  OnClick="siteToRight_Click" style="display:none"  />
<asp:Button id="btnSiteListDblClickToLeft"  runat="server"  OnClick="siteToLeft_Click"  style="display:none"  />
<asp:Button id="btnHostListDblClickToRight" runat="server"  OnClick="hostToRight_Click" style="display:none"  />
<asp:Button id="btnHostListDblClickToLeft"  runat="server"  OnClick="hostToLeft_Click"  style="display:none"  /> 
<asp:Button id="btnDetailCode" runat="server" OnClick="btnDetailCode_Click" style="display:none"  />

<!---------------------------------------------------------------------------------------------->

<table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
<colgroup>
<col width="auto" />
<col width="50%" />
</colgroup>
<tr>
    <td>
    <span class="subTitle"><span id="Span1"><img src="/Asset/Images/body/bullet01.gif" /> <%= Resources.Resource.txt_0000 %></span></span>
    </td>
</tr> 
</table> 
<%--
<table>
<tr>
    <td>

    [ <a href="javascript:popUploadUser()"  ><%= Resources.Resource.txt_0001 %></a> ]
     
    </td>
</tr>
</table>
  --%>                          
<!--  930px -->
<table style="width:1030px;height:100%" class="newContentsWrap" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td class="contentsOverflow"><!-- new contents -->
    <div class="newContents">
        
        <!-- new contents inner -->
        <table style="width:100%;height:100%" class="newContentsInner" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td style="vertical-align:top" >
            
             <table border=0  height=100% >
             <tr>
                  <td width=150 valign=top>
                      <table border=0  width=100% style="border:2px solid #CCCCCC">
                      <tr>
                            <td align=left>
                            
                            <!-- 트리  -->
                            <div style="height: 523px; width: 180px; overflow: scroll;"> 
                         
                            <asp:TreeView ID="TreeView1" runat="server"    ImageSet="Arrows"    AutoPostBack="true" 
                                ontreenodeexpanded="TreeView1_TreeNodeExpanded" 
                                onselectednodechanged="TreeView1_SelectedNodeChanged"        >
                                <ParentNodeStyle Font-Bold="False" />
                                <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                                <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" 
                                    VerticalPadding="0px" ForeColor="#5555DD" />
                                
                                <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" 
                                    HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                            </asp:TreeView>  
                                  
                              
                            </div>
                            <!-- 트리 -->
                            
                            </td>
                       </tr>
                       </table>
                  </td>
                  <td valign=top width=100%  >
                 
                    <!--  사용자 리스트 시작 -->
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"    DataKeyNames="userId"
                    CssClass="boardListType2" OnRowDataBound="GridView1_RowDataBound" >
                    <RowStyle BackColor="#F7F7DE" />
                    <Columns> 
                        <asp:BoundField DataField="siteId"  HeaderText="<%$ Resources: Resource, txt_0002 %>" ItemStyle-HorizontalAlign=Center SortExpression="siteId"   HeaderStyle-Width="90"   >
                        </asp:BoundField>
                        <asp:BoundField DataField="siteName" HeaderText="<%$ Resources: Resource, txt_0003 %>" ItemStyle-HorizontalAlign=Center SortExpression="siteName"  HeaderStyle-Width="90" >
                        </asp:BoundField>
                        <asp:BoundField DataField="userId" HeaderText="<%$ Resources: Resource, txt_0004 %>"    ItemStyle-Font-Bold=true  ItemStyle-Font-Underline=true ItemStyle-HorizontalAlign=Center   >
                        </asp:BoundField> 
                        <asp:BoundField DataField="userName" HeaderText="<%$ Resources: Resource, txt_0005 %>"   ItemStyle-Wrap=false   ItemStyle-HorizontalAlign=Left SortExpression="userName"    >
                        </asp:BoundField>
                        <asp:BoundField DataField="mobileNo" HeaderText="<%$ Resources: Resource, txt_0006 %>" ItemStyle-HorizontalAlign=Center    HeaderStyle-Width=100 >
                        </asp:BoundField>
                        <asp:BoundField DataField="email" HeaderText="<%$ Resources: Resource, txt_0007 %>" ItemStyle-HorizontalAlign=Center    ItemStyle-Width=400 >
                        </asp:BoundField> 
                        <asp:BoundField DataField="useEmail" HeaderText="<%$ Resources: Resource, txt_0008 %>"  HtmlEncode=false  ItemStyle-HorizontalAlign=Center SortExpression="useEmail"  HeaderStyle-Width=100  >
                        </asp:BoundField>
                        <asp:BoundField DataField="useSms" HeaderText="<%$ Resources: Resource, txt_0009 %>"  HtmlEncode=false ItemStyle-HorizontalAlign=Center SortExpression="useSms"  HeaderStyle-Width=100 >
                        </asp:BoundField>
             
                        <asp:BoundField DataField="password" HeaderText="암호" Visible="false"  ItemStyle-HorizontalAlign=Center   ItemStyle-Width=400 >
                        </asp:BoundField>
                        <asp:BoundField DataField="sid"  Visible="false"  HeaderText="SID" ItemStyle-HorizontalAlign=Center SortExpression="sid"  ItemStyle-Width=400 >
                        </asp:BoundField>
                        <asp:BoundField DataField="phoneNo" HeaderText="전화번호"  Visible="false"  ItemStyle-HorizontalAlign=Center   ItemStyle-Width=400 >
                        </asp:BoundField>  
                        <asp:BoundField DataField="zipCode" HeaderText="우편번호"  Visible="false"   ItemStyle-HorizontalAlign=Center    ItemStyle-Width=400 >
                        </asp:BoundField>   
                        <asp:BoundField DataField="addr1" HeaderText="주소"  Visible="false"  ItemStyle-HorizontalAlign=Center   ItemStyle-Width=400 >
                        </asp:BoundField>                                                                                    
                        <asp:BoundField DataField="addr2" HeaderText="상세주소"  Visible="false"  ItemStyle-HorizontalAlign=Center    ItemStyle-Width=400 >
                        </asp:BoundField> 
                        <asp:BoundField DataField="registerTime" HeaderText="등록일"  Visible="false"  ItemStyle-HorizontalAlign=Center    ItemStyle-Width=400 >
                        </asp:BoundField> 
                        <asp:BoundField DataField="userType" HeaderText="사용자타입" Visible="false"   ItemStyle-HorizontalAlign=Center SortExpression="userType"  ItemStyle-Width=400 >
                        </asp:BoundField>   
                        <asp:BoundField DataField="siteList" HeaderText="SITEList"  Visible="false"  ItemStyle-HorizontalAlign=Center    ItemStyle-Width=400 >
                        </asp:BoundField>  
                        <asp:BoundField DataField="hostList" HeaderText="HOSTList"  Visible="false"  ItemStyle-HorizontalAlign=Center    ItemStyle-Width=400 >
                        </asp:BoundField>
                        <asp:BoundField DataField="probableCauseList" HeaderText="probableCauseList"  Visible="false"  ItemStyle-HorizontalAlign=Center    ItemStyle-Width=400 >
                        </asp:BoundField>
                        <asp:BoundField DataField="roleId" HeaderText="롤ID"  Visible="false"  ItemStyle-HorizontalAlign=Center SortExpression="roleId"  ItemStyle-Width=400 >
                        </asp:BoundField>   
                        <asp:BoundField DataField="validationDate" HeaderText="validationDate"  Visible="false"  ItemStyle-HorizontalAlign=Center   ItemStyle-Width=400 >
                        </asp:BoundField>   
                        
                    </Columns>        
                    
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />

                    <EmptyDataTemplate>
                        <table class="boardListType2" cellspacing="0" cellpadding="4" width=100% rules="cols" border="1"   style="color:Black;background-color:White;border-color:#DEDFDE;border-width:1px;border-style:None;border-collapse:collapse;">
                        <tr style="color:White;background-color:#6B696B;font-weight:bold;"  >
	                        <th scope="col" style="width:90px;"><%= Resources.Resource.txt_0002 %></th>
	                        <th scope="col" style="width:90px;"><%= Resources.Resource.txt_0003 %></th>
	                        <th scope="col"><%= Resources.Resource.txt_0004 %></th>
	                        <th scope="col"><%= Resources.Resource.txt_0005 %></th>
	                        <th scope="col" style="width:100px;"><%= Resources.Resource.txt_0006 %></th>
	                        <th scope="col"><%= Resources.Resource.txt_0007 %></th>
	                        <th scope="col" style="width:100px;"><%= Resources.Resource.txt_0008 %></th>
	                        <th scope="col" style="width:100px;"><%= Resources.Resource.txt_0009 %></th>
                        </tr>
                        <tr style="background-color:#F7F7DE;">
                            <td align="center" style="width:100%;"   colspan=8>
                                <span class="txt_orange"><%= Resources.Resource.txt_0018 %></span> <!-- * 조회된 사용자가 없습니다. 왼쪽 트리에서 조회하고자 하는 조직을 선택하세요 //-->
                            </td> 
                        </tr>
                        </table>
                    </EmptyDataTemplate>        
                    
                    </asp:GridView>
                    <!--  사용자 리스트 끝 -->  
                            
                    <!-- 등록 화면 시작 -->
                          
                    <table border=0 width=100% >
                    <tr>
                        <td>
                            <asp:Button ID="BtnNew" runat="server" Text="<%$ Resources: Resource, btn_0002 %>" OnClick="BtnNew_Click"  />
                            <asp:Button ID="BtnSave" runat="server" Text="<%$ Resources: Resource, btn_0004 %>" OnClick="BtnSaveClick_Click" OnClientClick="return userValidation()" />
                            <asp:Button ID="BtnDel" runat="server" Text="<%$ Resources: Resource, btn_0001 %>" OnClick="BtnDel_Click" OnClientClick="return userDelete()" Visible=false />
                            <asp:Label ID="lbHowtoMsg" runat="server" CssClass="txt_orange" Text="<%$ Resources : Resource, txt_0019 %>" Visible=false /> <!--   * 상위 리스트의 사용자ID 를 클릭하신 후 변경,삭제 처리를 하실 수 있습니다. //-->
                        </td>
                    </tr>
                    <tr>
                        <td align=left>

                        </td>
                    </tr>
                    <tr>
                        <td>

                        <!--등록 테이블 시작 -->
                        
                        <table class="boardListType" cellspacing="0" rules="all" border="1"   >
                        <caption>
                        </caption>
                        <colgroup>
                        <col width="15%"/>
                        <col width=""/>
                        <col width="15%"/>
                        <col width=""/>
                        </colgroup>
                        <tr>
                            <th><%= Resources.Resource.txt_0002 %></th>
                            <td>
                                <asp:TextBox ID="siteName" runat="server" CssClass="textType"  readonly=true />
                            </td>
                            <th><%= Resources.Resource.txt_0004 %></th>
                            <td>
                                <asp:TextBox ID="txtUserId" runat="server"  CssClass="textType" Width="80px" />
                                <asp:Button ID="btnUserDup" runat="server" Text="<%$ Resources: Resource, btn_0008 %>" OnClick="btnUserDup_Click" OnClientClick="return userDup()" />
                            </td>
                        </tr>
                        <tr>
                            <th><%= Resources.Resource.txt_0010 %></th>
                            <td>
                                <!-- 브라우저에 저장되어 있는 아이디,패스워드가 자동으로 id, password 입력박스에 셋팅되는것을 막기위해 만들어놓은 것임. //-->
                                <!-- 여기부터 시작 //-->
                                <asp:TextBox ID="txtDummyID" runat="server" style="display:none;" />
                                <asp:TextBox ID="txtDummyPW" runat="server" TextMode=Password style="display:none;" />
                                <!-- 여기까지 끝//-->

                                <asp:TextBox ID="txtUserPw1" runat="server"  CssClass="textType"  TextMode=Password />
                            </td>
                            <th><%= Resources.Resource.txt_0011 %></th>
                            <td>
                                <asp:TextBox ID="txtUserPw2" runat="server"  CssClass="textType"  TextMode=Password />
                            </td>
                        </tr>                        
                        <%--<tr>
                            <td colspan=4>
                                <b>1.</b> 아이디가 포함된 암호금지.  &nbsp;&nbsp;&nbsp;&nbsp; 
                                <b>2.</b> 6자리 이상 숫자와 영문자의 조합.  (특수문자가능) &nbsp;&nbsp;&nbsp;&nbsp;
                                <b>3.</b> 같은문자 연속4회 이상 금지.
                            </td>                            
                        </tr>--%>
                        <tr>
                            <th><%= Resources.Resource.txt_0005 %></th>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server" CssClass="textType" />
                            </td>
                            <th><%= Resources.Resource.txt_0006 %></th>
                            <td>
                                <asp:TextBox ID="txtMobileNo" runat="server" CssClass="textType" />
                            </td>
                        </tr>
                        <tr>
                            <th><%= Resources.Resource.txt_0007 %></th>
                            <td> 
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="textType" />
                            </td>
                            <th><%= Resources.Resource.txt_0012 %></th>
                            <td>                               
                                <asp:TextBox ID="validationDate" runat="server" CssClass="calender" onclick="new CalendarFrame.Calendar(this);"  onkeydown="return false;" />
                            </td>
                        </tr>                            
                         
                        <tr>
                            <th><%= Resources.Resource.txt_0013 %></th>
                            <td colspan=3>                          
                                <div class="float_L">
                                    <asp:ListBox ID="probableCauseListAll" runat="server"   Width="200px"       Rows="5" 
                                        SelectionMode="Multiple"      onDblClick="probableCauseListDblClickToRight();" >
                                        <asp:ListItem Value="server_process_down"              Text="server_process_down"             ></asp:ListItem>
                                        <asp:ListItem Value="server_communication_fail"        Text="server_communication_fail"       ></asp:ListItem>
                                        <asp:ListItem Value="server_vnc_fail"                  Text="server_vnc_fail"                 ></asp:ListItem>
                                        <asp:ListItem Value="server_hdd_overload"              Text="server_hdd_overload"             ></asp:ListItem> 
                                        <asp:ListItem Value="server_mem_overload"              Text="server_mem_overload"             ></asp:ListItem>
                                        <asp:ListItem Value="server_cpu_overload"              Text="server_cpu_overload"             ></asp:ListItem>
                                        <asp:ListItem Value="server_net_overload"              Text="server_net_overload"             ></asp:ListItem>
                                        <asp:ListItem Value="server_db_overload"               Text="server_db_overload"              ></asp:ListItem>
                                        <asp:ListItem Value="host_process_down"                Text="host_process_down"               ></asp:ListItem>
                                        <asp:ListItem Value="host_communication_fail"          Text="host_communication_fail"         ></asp:ListItem>
                                        <asp:ListItem Value="host_vnc_fail"                    Text="host_vnc_fail"                   ></asp:ListItem>
                                        <asp:ListItem Value="host_ftp_fail"                    Text="host_ftp_fail"                   ></asp:ListItem>
                                        <asp:ListItem Value="host_update_fail"                 Text="host_update_fail"                ></asp:ListItem>
                                        <asp:ListItem Value="host_play_contents_fail"          Text="host_play_contents_fail"         ></asp:ListItem>
                                        <asp:ListItem Value="host_change_schedule_fail"        Text="host_change_schedule_fail"       ></asp:ListItem>
                                        <asp:ListItem Value="host_screenshot_fail"             Text="host_screenshot_fail"            ></asp:ListItem>
                                        <asp:ListItem Value="host_hdd_overload"                Text="host_hdd_overload"               ></asp:ListItem>
                                        <asp:ListItem Value="host_mem_overload"                Text="host_mem_overload"               ></asp:ListItem>
                                        <asp:ListItem Value="host_cpu_overload"                Text="host_cpu_overload"               ></asp:ListItem>
                                        <asp:ListItem Value="host_net_overload"                Text="host_net_overload"               ></asp:ListItem>
                                    </asp:ListBox> 
                                </div>
                                
                                <div class="float_L pad_T20 pad_L2R2">
                                    <asp:ImageButton ID="probableToRight" runat="server" ImageUrl="/Asset/Images/body/bt_right.gif" OnClick="probableToRight_Click" />                                    
                                    <br /><br />
                                    <asp:ImageButton ID="probableToLeft" runat="server" ImageUrl="/Asset/Images/body/bt_left.gif" OnClick="probableToLeft_Click" />
                                </div>
                                
                                <div class="float_L">
                                    <asp:ListBox ID="probableCauseList"  Width="200" runat="server"  Rows="5"  SelectionMode="Multiple" onDblClick="probableCauseListDblClickToLeft();" />
                                </div>
                                
                                <div style="clear:both">
                                    <asp:CheckBox ID="chkUseEmail" runat="server" Text="<%$ Resources : Resource, txt_0008 %>" Font-Bold="true" AutoPostBack=false /> <!-- 알림사용(Email) //-->
                                    <asp:CheckBox ID="chkUseSms"   runat="server" Text="<%$ Resources : Resource, txt_0009 %>" Font-Bold="true" AutoPostBack=false/>   <!-- 알림사용(SMS) //-->                              
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <th><%= Resources.Resource.txt_0014 %></th>
                            <td colspan=3>
                                <input id="siteListAllSearch" type="text" size="27"   onkeyup="fn_siteListAllSearch();" />
                                <br />
                                <div class="float_L">
                                    <asp:ListBox ID="siteListAll" runat="server" Rows="5" Width="200px"   SelectionMode="Multiple" onDblClick="siteListDblClickToRight();" />
                                </div>
                            
                                <div class="float_L pad_T20 pad_L2R2"> 
                                    <asp:ImageButton ID="ButtonToRight" runat="server" ImageUrl="/Asset/Images/body/bt_right.gif" OnClick="siteToRight_Click" />
                                    <br /><br />
                                    <asp:ImageButton ID="ButtonToLeft" runat="server" ImageUrl="/Asset/Images/body/bt_left.gif" OnClick="siteToLeft_Click" />                                                                    
                                </div>
                            
                                <div class="float_L">
                                    <asp:ListBox ID="siteList" runat="server"  Width="200px"   Rows="5"  SelectionMode="Multiple" onDblClick="siteListDblClickToLeft();" />
                                </div>
                            </td>                               
                        </tr>
                        <tr>
                            <th><%= Resources.Resource.txt_0015 %></th>
                            <td colspan=3>
                                <div class="float_L">
                                    <asp:ListBox ID="hostListAll" runat="server" Rows="5" Width="200px"   SelectionMode="Multiple" onDblClick="hostListDblClickToRight();" />                                
                                </div>
                                
                                <div class="float_L pad_T20 pad_L2R2"> 
                                    <asp:ImageButton ID="hostButtonToRight" runat="server" ImageUrl="/Asset/Images/body/bt_right.gif" OnClick="hostToRight_Click" />
                                    <br /><br />
                                    <asp:ImageButton ID="hostButtonToLeft" runat="server" ImageUrl="/Asset/Images/body/bt_left.gif" OnClick="hostToLeft_Click" />
                                </div>
                                
                                <div class="float_L">
                                    <asp:ListBox ID="hostList" runat="server"   Width="200px"   Rows="5"  SelectionMode="Multiple" onDblClick="hostListDblClickToLeft();" />
                                </div>                            
                            </td>                                                                              
                        </tr>                         
                        <tr>
                            <th><%= Resources.Resource.txt_0016 %></th>
                            <td>
                                <asp:DropDownList ID="userType" runat="server" AutoPostBack=false>
                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Super Admin"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Site Admin"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="User"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <th><%= Resources.Resource.txt_0017 %></th>
                            <td>  
                                <asp:DropDownList ID="roleId" runat="server" AutoPostBack=false>
                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>

                        </table>
                        
                        <!--//등록 테이블 끝-->
                          
                    </td>
                    </tr>
                    </table>
                    <!-- 등록 화면 끝 -->
                    
                    
                  </td>
                </tr>
              </table>
              
            </td>
        </tr>
        </table>
    
    </div>
    </td>
</tr>
</table>

 
<!---------------------------------------------------------------------------------------------->

</ContentTemplate>
</asp:UpdatePanel>
 
</asp:Content>

