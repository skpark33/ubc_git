﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/ContentMasterPage.master" AutoEventWireup="true" CodeFile="ubcCenter.aspx.cs" Inherits="ubcCenter" Title="ubcCenter" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
<script type="text/javascript">

    function goRelease_Client() {
        $("#<%=btnClient.ClientID %>").attr('disabled', 'disabled');
        $("#<%=btnServer.ClientID %>").attr('disabled', 'disabled');

        $("#<%=Button1.ClientID %>").click();

        return true;

    }
    function goRelease_Server() {

        $("#<%=btnClient.ClientID %>").attr('disabled', 'disabled');
        $("#<%=btnServer.ClientID %>").attr('disabled', 'disabled');

        $("#<%=Button2.ClientID %>").click();

        return true;
    }

</script>

 
 <style type="text/css">
    body {
        height: 100%;  /*  반드시 높이 값을 body 에 설정을 해야만 한다. */
        width: 100%;
    }
     
    #layout-container {
        position: absolute; /* 레이어 위치는 반드시 절대값으로 설정해야 한다. 다른 것들은 안 된다. */
        height: 98%; /* 반드시 높이를 설정한다. 100% 하게 되면 브라우저에 스크롤바가 생긴다. */
        width: 99%; 
        background-image: url(../Asset/images/bg_ubcCenter.jpg);
        /* background-position: right bottom; */
        background-repeat: no-repeat;
    }
     
    #layout-content {
        position: absolute; /* 위치 고정을 하면 된다. */
        top: 70px;  /* 50% 로 하면 중앙부터 출력이 된다. 적당한 비율을 입력하면 된다. */
        left: 50px;
        /*width: 100%;*/
    }
</style>


</asp:Content>
 

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" >


<table style="width:100%;" cellpadding="0" cellspacing="0" border="0" class="subTitleArea">
<tr>
    <td>
        <span class="subTitle"><span id="Span1"><img src="/Asset/Images/body/bullet01.gif" /> <%= Resources.Resource.ttl_0033 %> <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="#990033" /></span></span>
    </td>
</tr>
</table>



        
<%
    if (isExcutable == false)
    {                     
%>

<div id="layout-container">
    <div id="layout-content">    
                
            <%= Resources.Resource.txt_0153 %><br/>           
            <a href='http://<%= EXCUTABLE_SERVER %>' target='_blank'>>> <%= Resources.Resource.txt_0154 %></a>      
        
    </div>
</div>    

<%        
    }
    else
    {
%>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" >
<ContentTemplate>
    
<asp:Panel ID="divCmd" runat="server">
           
    <asp:Button ID="Button1" runat="server" Text="Button" OnClick='btnClient_Click' style="display:none;" />
    <asp:Button ID="Button2" runat="server" Text="Button" OnClick='btnServer_Click' style="display:none;" />
    
    <asp:Button ID="btnServer" runat="server" Text="<%$ Resources: Resource, txt_0148 %>" Width="150" OnClientClick="return goRelease_Server();" />       
    <asp:Button ID="btnClient" runat="server" Text="<%$ Resources: Resource, txt_0149 %>" Width="150" OnClientClick="return goRelease_Client();" />
        
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources: Resource, txt_0147 %>" Width="150" OnClick='btnCancel_Click' />    
    <asp:Image ID="imgUpdate" runat="server" ImageUrl="~/Asset/Images/update.gif" ImageAlign=AbsMiddle/>
        
</asp:Panel>
    

<div ID="divResult"><img src="/Asset/Images/agronet/ico_sqr_light_green.gif" /> <%= Resources.Resource.txt_0150%>
    <asp:TextBox ID="txtWorkLog" runat="server" TextMode="MultiLine" Rows="5" Width="100%" Wrap="False" ReadOnly="true" ></asp:TextBox>
</div>  

<div ID="divLog"><img src="/Asset/Images/agronet/ico_sqr_blue_green.gif" /> <%= Resources.Resource.txt_0151%>
    <asp:TextBox ID="txtTransLog" runat="server" TextMode="MultiLine" Width="100%" Rows="15" BackColor="Black" ForeColor="White" Wrap="False"  ReadOnly="true" ></asp:TextBox>
    <asp:HiddenField ID="HiddenLogFileName" runat="server" Value="" />
    <asp:HiddenField ID="HiddenLogFileRowCount" runat="server" Value="0"/>
</div>  

<div ID="divError"><img src="/Asset/Images/agronet/ico_sqr_dark_red.gif" /> <%= Resources.Resource.txt_0152%>
    <asp:TextBox ID="txtErrorLog" runat="server" TextMode="MultiLine" Width="100%"  Rows="10" BackColor="Black" ForeColor="White" Wrap="False"   ReadOnly="true" ></asp:TextBox>
</div>  

<asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="1000" Enabled="false" >
</asp:Timer>
 
</ContentTemplate>    
</asp:UpdatePanel>


<%
    }
%>

</asp:Content>

