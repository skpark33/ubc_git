﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DID.Common.Framework;
using System.IO;

public partial class KH_List_Service : BasePage
{
    protected const string DELIM = "\r\n";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.ContentType = "text/xml";    
        Response.Write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        Response.Write("<kh>\n");
        

        DID.Service.Manager.GeneralSettings obj = new DID.Service.Manager.GeneralSettings();
        DataTable dt = obj.GetCustomerURL(Profile.COMP_TYPE);

        if( dt.Rows.Count > 0 )
        {
            string url = dt.Rows[0]["url"].ToString();
            
            // 사이트 섹션 값 (예) [KIA]
            string section = string.Format("[{0}]", Profile.COMP_TYPE);

            // URL 컬럼값에 사이트 섹션 값이 존재할 경우
            if (url.ToLower().Contains(section.ToLower()))
            {
                // 섹션값을 제외한 데이타 부분만을 잘라낸다
                string data = url.Substring(url.ToLower().IndexOf(section.ToLower()) + section.Length);

                string[] properties = data.Split(DELIM.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                foreach (string property in properties)
                {
                    // 혹시 다른 섹션이 존재하면 loop 종료 (원래는 없어야 정상임)
                    if (property.StartsWith("[") == true) break;

                    // 주석일경우 skip
                    if (property.StartsWith(";") || property.StartsWith("#")) continue;

                    // name,value 구분값(주로 =이나 :를 쓰는 ini 화일도 있다고함)이 없으면 skip
                    if (!property.Contains("=") && !property.Contains(":")) continue;

                    // name,value 구분값 위치
                    int delimIndex = (property.IndexOf("=") == -1) ? property.IndexOf(":") : property.IndexOf("=");

                    string name = property.Substring(0, delimIndex); // name 값
                    string value = property.Substring(delimIndex + 1); // value 값

                    if (name.ToLower().Equals("kh_web".ToLower()))
                        Response.Write(string.Format("<web>{0}</web>\n", value));

                    if (name.ToLower().Equals("kh_sims".ToLower()))
                        Response.Write(string.Format("<sims>{0}</sims>\n", value));
                   
                }
            }            
        }        

        Response.Write("</kh>\n");
        Response.End();        
    }   
    

}
