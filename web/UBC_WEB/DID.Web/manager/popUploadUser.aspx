﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/PopupMasterPage.master" AutoEventWireup="true" CodeFile="popUploadUser.aspx.cs" Inherits="manager_popUploadUser" Title="::: KIA ::: 사용자 엑셀 업로드" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <script type="text/javascript">
       //확장자가 xls  혹은 xlsx 인 파일만 업로드 가능 함.
        function checkFileExtension(elem) {  
            var filePath = elem.value;  
            if(filePath.indexOf('.') == -1)  
                return false;  
            var validExtensions = new Array();  
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();  
            validExtensions[0] = 'xls';    
            validExtensions[1] = 'xlsx';     
            for(var i = 0; i < validExtensions.length; i++) {  
                if(ext.toLowerCase()  == validExtensions[i].toLowerCase()  )  
                    return true;  
            }   
            alert('확장자가 xls 혹은 xlsx 인 화일만 첨부할 수 있습니다.');  
            return false;
        }



        
//        if (window == top) {
//            window.onload = function() {

//                window.moveTo(300, 300);
//                window.resizeTo(390, 430);
//                document.body.onresize = function() {
//                    window.resizeTo(390, 430);
//                }
//            }
//        }

        
   </script>
    
    
<style type="text/css">
    body {
        height: 100%;  /*  반드시 높이 값을 body 에 설정을 해야만 한다. */
        width: 100%;
    }
     
    #layout-container {
        position: absolute; /* 레이어 위치는 반드시 절대값으로 설정해야 한다. 다른 것들은 안 된다. */
        height: 98%; /* 반드시 높이를 설정한다. 100% 하게 되면 브라우저에 스크롤바가 생긴다. */
        width: 99%; 
        background-image: url(../Asset/images/bg_userUpload.jpg);
        /* background-position: right bottom; */
        background-repeat: no-repeat;
    }
     
    #layout-content {
        position: absolute; /* 위치 고정을 하면 된다. */
        top: 95px;  /* 50% 로 하면 중앙부터 출력이 된다. 적당한 비율을 입력하면 된다. */
        left: 50px;
        /*width: 100%;*/
    }
</style>


    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    <div id="layout-container">
        <div id="layout-content">
            
            
            <table >
            <tr>
                <td>1. 사용자 입력 폼 다운로드</td>
            </tr>
            <tr>
                <td><a href="./userFormat.xls"><asp:Button ID="Button2" runat="server" Text="엑셀화일 다운로드" /></a></td>
            </tr>
            <tr>
                <td>2. 엑셀 화일에 사용자 정보 입력 후 저장</td>
            </tr>
            <tr>
                <td>3. 업로드할 엑셀 화일 선택</td>
            </tr>
            <tr>
                <td><asp:FileUpload ID="File1" runat="server" /></td>
            </tr>
            <tr>
                <td>4. 엑셀 화일 업로드</td>
            </tr> 
            <tr>
                <td><asp:Button ID="Button1" runat="server" Text="엑셀화일 업로드" onclick="Button1_Click" /></td>
            </tr>
            
            </table>       
        </div>
    </div>



</asp:Content>

