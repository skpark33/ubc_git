﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

using DID.Common.Framework;

public partial class manager_generalSettings : BasePage
{   
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.Resource.ttl_0044; // 사이트 설정
    }
}
