﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;
using DID.Common.Framework;
 

public partial class manager_user : BasePage
{
        
    protected void Page_Load(object sender, EventArgs e)
    { 
        if (!this.IsPostBack)
        { 
            this.InitControl();        
        }

        // postback 시 패스워드 필드 값이 없어지지 않게 셋팅해줌
        txtUserPw1.Attributes["value"] = txtUserPw1.Text;
        txtUserPw2.Attributes["value"] = txtUserPw2.Text;
    }    

    //초기화
    private void InitControl()
    { 
        //관리대상그룹(단말그룹)  : ubc_site 전체를 불러온다.
        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {
            DataTable dt = obj.GetManageSiteList(Profile.COMP_TYPE);
            for( int i=0 ; i < dt.Rows.Count ; i++ )
            { 
                ListItem addList = new ListItem();
                addList.Value    = dt.Rows[i]["siteId"].ToString();
                addList.Text     = dt.Rows[i]["siteName"].ToString();

                siteListAll.Items.Add(addList);
            } 
        } 
        //관리대상단말선택
        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {
            DataTable dt = obj.GetManageHostList();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ListItem addList = new ListItem();
                addList.Value = dt.Rows[i]["hostId"].ToString();
                addList.Text = dt.Rows[i]["hostName"].ToString();

                hostListAll.Items.Add(addList);
            }
        } 
        //롤 선택 : roleId
        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {

            DataTable dt = obj.GetRoleList(Profile.COMP_TYPE);
             
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ListItem addList = new ListItem();
                addList.Value = dt.Rows[i]["roleId"].ToString();
                addList.Text  = dt.Rows[i]["roleName"].ToString();

                roleId.Items.Add(addList);                 
            }
        } 
        // 만료일자를 +10년 로 세팅한다.
        validationDate.Text = DateTime.Now.AddYears(+10).ToString("yyyy-MM-dd");

        // 트리 초기화
        FindAndAttachChildSite(null);

        // 사용자 정보 화면 초기화
        clearUserInfo();

        // 초기화면에 그리드뷰를 보여주시위해서 데이터 소스없이 아무값도 없는 모습으로 바인드해줌
        GridView1.DataBind();
    }

    // 트리 자식 노드 가져오기
    // 2011-08-01 임유석 추가
    private void FindAndAttachChildSite(TreeNode parentNode)
    {
        using (DID.Service.Manager.Org obj = new DID.Service.Manager.Org())
        {

            DataTable dt = obj.GeChildSiteListByParentID(parentNode.IsNullOrEmpty() ? "" : parentNode.Value, Profile.COMP_TYPE);

            if (!parentNode.IsNullOrEmpty()) parentNode.ChildNodes.Clear();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TreeNode node = new TreeNode();
                node.Text = dt.Rows[i]["siteName"].ToString().Trim();
                node.Value = dt.Rows[i]["siteId"].ToString().Trim();
                node.PopulateOnDemand = true;
                node.Expanded = false;
                node.SelectAction = TreeNodeSelectAction.Select;

                if (parentNode.IsNullOrEmpty())
                    TreeView1.Nodes.Add(node);
                else
                    parentNode.ChildNodes.Add(node);
            }
        }
    }

    //사용자  불러오기
    protected void UserList(string siteId )
    { 
        using (DID.Service.Manager.User  obj = new DID.Service.Manager.User ())
        {
            DataTable dt = obj.GetUserList(siteId, Profile.COMP_TYPE);
            GridView1.DataSource = dt;
            GridView1.DataBind();

            // 데이타가 있으며 사용법 메세지 보여줌
            if (dt.Rows.Count > 0)
                lbHowtoMsg.Visible = true;
            else
                lbHowtoMsg.Visible = false;
    
        } 
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[2].Attributes["onclick"] = string.Format("ViewDetail('{0}','{1}');", e.Row.Cells[0].Text, e.Row.Cells[2].Text);
            
            if( e.Row.Cells[6].Text == "0" )
                e.Row.Cells[6].Text = Resources.Resource.txt_0042; // 미사용
            else
                e.Row.Cells[6].Text = Resources.Resource.txt_0041; // 사용

            if( e.Row.Cells[7].Text == "0" )
                e.Row.Cells[7].Text = Resources.Resource.txt_0042; // 미사용
            else
                e.Row.Cells[7].Text = Resources.Resource.txt_0041; // 사용
        
        }
    }

    // 사용자 상세보기 이벤트처리 : 히든버튼이 클릭됨
    protected void btnDetailCode_Click(object sender, EventArgs e)
    {
        // 사용자 정보 초기화
        initUserInfo(hiddenTxtSiteId.Text, txtUserId.Text);
    }

    // 사용자 정보 초기화
    protected void initUserInfo(string siteId, string userId)
    {
        // 화면초기화
        clearUserInfo();

        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {
            DataTable dt = obj.GetUserDetail(siteId, userId);    
            
            if(dt.Rows.Count > 0) 
            {                
                // 업데이트로 셋팅              
                hiddenTxtNewOrUpadte.Text = "UPDATE";

                txtUserId.Enabled  = false;
                BtnDel.Visible     = true; // 삭제버튼 보이기              
                btnUserDup.Visible = false; // 아이디 중복검사 보이기

                // 아이디 중복검사 이미 되었음으로 현재 아이디를 넣어줌
                hiddenTxtUserDupCheck.Text = dt.Rows[0]["userId"].ToString();

                hiddenTxtSiteId.Text = dt.Rows[0]["siteId"].ToString();
                txtUserId.Text = dt.Rows[0]["userId"].ToString();
                //txtUserPw1.Text = dt.Rows[0]["password"].ToString();
                txtUserName.Text  = dt.Rows[0]["userName"].ToString();
                txtMobileNo.Text  = dt.Rows[0]["mobileNo"].ToString();
                txtEmail.Text     = dt.Rows[0]["email"].ToString();

                validationDate.Text = (dt.Rows[0]["validationDate"].ToString().IsNullOrEmpty()) 
                                    ? ""
                                    //: dt.Rows[0]["validationDate"].ToString().Substring(0, 10)
                                    : DateTime.Parse(dt.Rows[0]["validationDate"].ToString()).ToString("yyyy-MM-dd")
                                    ;

                // email 수신 체크박스
                chkUseEmail.Checked = (dt.Rows[0]["useEmail"].ToString() == "1") 
                                    ? true 
                                    : false
                                    ;

                // sms 수신 체크박스
                chkUseSms.Checked = (dt.Rows[0]["useSms"].ToString() == "1") 
                                  ? true 
                                  : false
                                  ;

                //등급
                userType.SelectedValue = dt.Rows[0]["userType"].ToString();

                //롤
                roleId.SelectedValue   = dt.Rows[0]["roleId"].ToString();     

                //장애리스트 세팅
                string reStr = dt.Rows[0]["probableCauseList"].ToString();
                reStr = reStr.Replace(" "  , "");
                reStr = reStr.Replace("\"" , "");
                reStr = reStr.Replace("["  , "");
                reStr = reStr.Replace("]"  , "");
                reStr = reStr.Trim(','); 

                foreach( string strTmp in reStr.Split(',') )
                {
                    ListItem addList = new ListItem();
                    addList.Text  = strTmp;
                    addList.Value = strTmp;

                    //중복금지
                    if (probableCauseList.Items.IndexOf(addList) == -1)
                        probableCauseList.Items.Add(addList); 
                  
                }

                //관리대상그룹세팅 (단말그룹)
                reStr = dt.Rows[0]["siteList"].ToString();
                reStr = reStr.Replace(" ", "");
                reStr = reStr.Replace("\"", "");
                reStr = reStr.Replace("[", "");
                reStr = reStr.Replace("]", "");
                reStr = reStr.Trim(','); 

                foreach (string strTmp in reStr.Split(','))
                {
                    ListItem addList = siteListAll.Items.FindByValue(strTmp);

                    //중복금지
                    if (addList!=null && siteList.Items.IndexOf(addList) == -1)
                        siteList.Items.Add(addList);
                }

                //관리대상단말    세팅
                reStr = dt.Rows[0]["hostList"].ToString();
                reStr = reStr.Replace(" ", "");
                reStr = reStr.Replace("\"", "");
                reStr = reStr.Replace("[", "");
                reStr = reStr.Replace("]", "");
                reStr = reStr.Trim(','); 

                foreach (string strTmp in reStr.Split(','))
                {
                    ListItem addList = hostListAll.Items.FindByValue(strTmp);

                    //중복금지
                    if (addList != null && hostList.Items.IndexOf(addList) == -1)
                        hostList.Items.Add(addList);
                } 
            }  
        } 
    }
        
    // 관리대상그룹(소속그룹) : 전체 항목에서 선택해서 오른쪽으로 옮긴다.
    protected void siteToRight_Click(object sender, EventArgs e)
    {
        if (siteListAll.SelectedIndex != -1 )
        {
            //멀티 입력
            for (int i = 0; i < siteListAll.Items.Count; i++)
            {
                if(siteListAll.Items[i].Selected )
                {
                    ListItem addList = new ListItem();                
                    addList.Text  = siteListAll.Items[i].Text ;
                    addList.Value = siteListAll.Items[i].Value ;

                    //중복금지
                    if (siteList.Items.IndexOf(addList) == -1)
                        siteList.Items.Add(addList);              
                     
                } 
            } 
        } 
    }

    //삭제 : 관리대상그룹(소속그룹) : 선택항목에서 선택해서 왼쪽으로 옮긴다.
    protected void siteToLeft_Click(object sender, EventArgs e)
    {
        if (siteList.SelectedIndex != -1)
        {
            for (int i=siteList.Items.Count-1 ; i>=0 ; i--)
            {
                if (siteList.Items[i].Selected)
                {
                    siteList.Items[i].Selected = false;
                    siteList.Items.RemoveAt(i);
                }
            }
        }
    }
        
    //관리대상단말선택 : 전체 항목에서 선택해서 오른쪽으로 옮긴다.
    protected void hostToRight_Click(object sender, EventArgs e)
    {
        if (hostListAll.SelectedIndex != -1)
        { 
            //멀티 입력
            for (int i = 0; i < hostListAll.Items.Count; i++)
            {
                if (hostListAll.Items[i].Selected)
                {
                    ListItem addList = new ListItem();
                    addList.Text = hostListAll.Items[i].Text;
                    addList.Value = hostListAll.Items[i].Value;

                    //중복금지
                    if (hostList.Items.IndexOf(addList) == -1)
                        hostList.Items.Add(addList);

                }                 
            }  
        }
    }

    //삭제 : 관리대상단말선택 : 선택항목에서 선택해서 왼쪽으로 옮긴다.
    protected void hostToLeft_Click(object sender, EventArgs e)
    {
        if (hostList.SelectedIndex != -1)
        {
            for (int i=hostList.Items.Count-1 ; i>=0 ; i--)
            {
                if (hostList.Items[i].Selected)
                {
                    hostList.Items[i].Selected = false;
                    hostList.Items.RemoveAt(i);
                }
            }
        }
    }
        
    //장애리스트 : 선택항목에서 선택해서 오른쪽으로 옮긴다.
    protected void probableToRight_Click(object sender, EventArgs e)
    {    
        if (probableCauseListAll.SelectedIndex != -1)
        {
            //멀티 입력
            for (int i = 0; i < probableCauseListAll.Items.Count; i++)
            {
                if (probableCauseListAll.Items[i].Selected)
                {
                    
                    ListItem addList = new ListItem();
                    addList.Text  = probableCauseListAll.Items[i].Text;
                    addList.Value = probableCauseListAll.Items[i].Value;

                    //중복금지
                    if (probableCauseList.Items.IndexOf(addList) == -1)
                        probableCauseList.Items.Add(addList);
                    
                }
            }
        }
    }

    //삭제 : 장애리스트 : 선택항목에서 선택해서 왼쪽으로 옮긴다.
    protected void probableToLeft_Click(object sender, EventArgs e)
    {
        if (probableCauseList.SelectedIndex != -1)
        {
            for (int i=probableCauseList.Items.Count-1 ; i>=0 ; i--)
            {
                if (probableCauseList.Items[i].Selected)
                {
                    probableCauseList.Items[i].Selected = false;
                    probableCauseList.Items.RemoveAt(i);
                }
            }
        }
        
    }

    // 신규등록 버튼
    protected void BtnNew_Click(object sender, EventArgs e)
    {
        // 사용자 정보 화면 초기화
        clearUserInfo();

        //////////////////////////////
        // 숨김필드 초기화 시작

        hiddenTxtNewOrUpadte.Text = "NEW"; // 신규로 셋팅
        siteName            .Text = hiddenTxtTreeSiteName.Text; // 조직명 (선택된 트리값으로 초기화)
        hiddenTxtSiteId     .Text = hiddenTxtTreeSiteId.Text; // 조직ID (선택된 트리값으로 초기화)               
                
        // 숨김필드 초기화 끝
        //////////////////////////////


        //////////////////////////////
        // 컨트롤 활성/비활성 시작

        txtUserId .Enabled = true;  // 유저 ID 입력가능하게 설정        
        btnUserDup.Visible = true;  // 아이디 중복검사 숨기기
        BtnDel    .Visible = false; // 삭제버튼 숨기기        

        // 컨트롤 활성/비활성 끝
        //////////////////////////////       
    } 
    
    // 화면 초기화
    public void clearUserInfo()
    {
        //
        // 여기부터 숨김 필드 초기화
        //

        hiddenTxtUserDupCheck.Text = "";    // 아이디 중복검사 여부        
        hiddenTxtSiteId      .Text = "";    // 유저 SITE ID
        hiddenTxtNewOrUpadte .Text = "NEW"; // Default 로 NEW 셋팅

        //
        // 여기부터 UI 필드 초기화 
        //

        txtUserId .Text = "";
        txtUserPw1.Text = "";
        txtUserPw2.Text = "";
        txtUserPw1.Attributes["value"] = "";
        txtUserPw2.Attributes["value"] = "";
        txtUserName.Text = "";
        txtMobileNo.Text = "";
        txtEmail   .Text = "";

        //장애처리 초기화: 클리어        
        probableCauseList.SelectedIndex = -1;
        probableCauseList.Items.Clear();

        // 이메일, SMS 송수신 초기화:         
        chkUseEmail.Checked = false;
        chkUseSms  .Checked = false;

        //관리대상그룹 초기화: 클리어
        siteList.SelectedIndex = -1; 
        siteList.Items.Clear();

        //관리대상단말 초기화: 클리어
        hostList.SelectedIndex = -1; 
        hostList.Items.Clear();

        // 등급 초기화 : 첫번째 아이템으로 선택
        userType.SelectedIndex = -1;

        // 롤 초기화  : 첫번째 아이템으로 선택
        roleId.SelectedIndex = -1;
        
        // 사용자 만료일 초기화: 10년 후로 셋팅
        validationDate.Text = DateTime.Now.AddYears(+10).ToString("yyyy-MM-dd");       

    }

    //삭제 버튼클릭 :  사용자 삭제
    protected void BtnDel_Click(object sender, EventArgs e)
    {    
        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {
            obj.GetUserDelete(hiddenTxtSiteId.Text, txtUserId.Text);
        }       

        // 사용자 목록 조회
        UserList(hiddenTxtTreeSiteId.Text);

        // 신규입력상태로 변경
        BtnNew_Click(null, null);

        this.ScriptExecute(Resources.Resource.msg_0047); // 삭제 되었습니다

    }

    // 사용자 저장btnSave
    protected void BtnSaveClick_Click(object sender, EventArgs e)
    {
        if (hiddenTxtNewOrUpadte.Text == "NEW")
        { 
            insertUser();

            // UPDATE 로 셋팅
            hiddenTxtNewOrUpadte.Text = "UPDATE";
        }
        else if (hiddenTxtNewOrUpadte.Text == "UPDATE")
        { 
            updateUser();
        }

        // 사용자 목록 조회
        UserList(hiddenTxtTreeSiteId.Text);

        // 사용자 정보 초기화 
        initUserInfo(hiddenTxtTreeSiteId.Text, txtUserId.Text);
    }
    
    // 사용자 정보 등록
    public void insertUser()
    { 
        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {
            string userIdVAL = txtUserId.Text;
            string siteIdVAL = hiddenTxtSiteId.Text;
            string passwordVAL = txtUserPw1.Text;
            string userNameVAL = txtUserName.Text;
            string mobileNoVAL = txtMobileNo.Text;
            string emailVAL = txtEmail.Text;
            string userTypeVAL = userType.SelectedValue;
            string roleIdVAL = roleId.SelectedValue;
            string validationDateVAL = validationDate.Text;

            string useEmailVAL = (chkUseEmail.Checked) ? "1" : "0";            
            string useSmsVAL = (chkUseSms.Checked) ? "1" : "0";

            string probableCauseListVAL = ListboxItemsToFormatedString(probableCauseList);
            string siteListVAL = ListboxItemsToFormatedString(siteList);
            string hostListVAL = ListboxItemsToFormatedString(hostList);

            obj.GetUserInsert(    userIdVAL.Trim()
                                , siteIdVAL
                                , passwordVAL
                                , userNameVAL.Trim()
                                , mobileNoVAL.Trim()
                                , emailVAL.Trim()
                                , userTypeVAL
                                , roleIdVAL
                                , validationDateVAL
                                , useEmailVAL
                                , useSmsVAL
                                , probableCauseListVAL
                                , siteListVAL
                                , hostListVAL
                              );

            this.ScriptExecute(Resources.Resource.msg_0025); // 등록 되었습니다
        }        
    }

    // 사용자 정보 수정
    public void updateUser()
    { 
        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {
            string userIdVAL = txtUserId.Text;
            string siteIdVAL = hiddenTxtSiteId.Text;
            string passwordVAL = txtUserPw1.Text;
            string userNameVAL = txtUserName.Text;
            string mobileNoVAL = txtMobileNo.Text;
            string emailVAL    = txtEmail.Text;
            string userTypeVAL = userType.SelectedValue;
            string roleIdVAL   = roleId.SelectedValue;
            string validationDateVAL = validationDate.Text;
            
            string useEmailVAL = (chkUseEmail.Checked) ? "1" : "0";            
            string useSmsVAL = (chkUseSms.Checked) ? "1" : "0";

            string probableCauseListVAL = ListboxItemsToFormatedString(probableCauseList);
            string siteListVAL = ListboxItemsToFormatedString(siteList);
            string hostListVAL = ListboxItemsToFormatedString(hostList);

            // 암호를 넣지 않았을 때에의 업데이트 처리
            if (txtUserPw1.Text.Trim() == "" && txtUserPw2.Text.Trim() == "")
            {
                obj.GetUserUpdateNotPass(     userIdVAL.Trim()
                                            , siteIdVAL
                                            , userNameVAL.Trim()
                                            , mobileNoVAL.Trim()
                                            , emailVAL.Trim()
                                            , userTypeVAL
                                            , roleIdVAL
                                            , validationDateVAL
                                            , useEmailVAL
                                            , useSmsVAL
                                            , probableCauseListVAL
                                            , siteListVAL
                                            , hostListVAL
                                         );
            }
            else
            {
                obj.GetUserUpdate(userIdVAL.Trim()
                                    , siteIdVAL
                                    , passwordVAL
                                    , userNameVAL.Trim()
                                    , mobileNoVAL.Trim()
                                    , emailVAL.Trim()
                                    , userTypeVAL
                                    , roleIdVAL
                                    , validationDateVAL
                                    , useEmailVAL
                                    , useSmsVAL
                                    , probableCauseListVAL
                                    , siteListVAL
                                    , hostListVAL
                                  );
            }

            this.ScriptExecute(Resources.Resource.msg_0024); // 수정 되었습니다
        }       
    
    }

    // 리스트 박스의 항목들을 하나의 문자열로 만들기
    // 리턴값 (예) ["값1","값2","값2"]
    protected string ListboxItemsToFormatedString(ListBox listBox)
    {
        string returnString = "";

        // 리스트의 값들을 하나의 문자열로 만든다 예) "값1","값2","값2", 
        foreach (ListItem item in listBox.Items)
        {
            if (item.Value.IsNullOrEmpty()) continue;

            returnString += string.Format("\"{0}\",", item.Value);
        }

        // 마지막 콤마 짜르고~
        returnString = returnString.TrimEnd(',');


        // 앞뒤로 [,] 붙여줌!!
        if (!returnString.IsNullOrEmpty())
            returnString = "[" + returnString + "]";

        return returnString;
    }

    //사용자 아이디 중복체크
    public void btnUserDup_Click(object sender, EventArgs e)
    {
        hiddenTxtUserDupCheck.Text = ""; // 초기화

        using (DID.Service.Manager.User obj = new DID.Service.Manager.User())
        {
            DataTable dt = obj.GetUserIdDup(txtUserId.Text);

            if (dt.Rows[0]["userId"].ToString() == "0")
            {
                hiddenTxtUserDupCheck.Text = txtUserId.Text;

                this.ScriptExecute(Resources.Resource.msg_0048); // 사용 가능한 아이디 입니다
            }
            else
            {
                hiddenTxtUserDupCheck.Text = "";

                //아이디 사용 불가능
                txtUserId.Text = "";
                txtUserId.Focus();

                this.ScriptExecute(Resources.Resource.msg_0049); // 이미 등록된 아이디 입니다
            }
        }
    }
   
    //트리 ajax
    protected void TreeView1_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
        FindAndAttachChildSite(e.Node);      
    }

    //조직노드 클릭시 발생 : 사용자 그리드를 조직으로 리스팅한다.
    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    { 
        hiddenTxtTreeSiteId  .Text = TreeView1.SelectedValue;
        hiddenTxtTreeSiteName.Text = TreeView1.SelectedNode.Text;

        // 사용자 목록 조회
        UserList(TreeView1.SelectedValue);

        // 신규입력상태로 변경
        BtnNew_Click(null, null);

    }  
    
}
