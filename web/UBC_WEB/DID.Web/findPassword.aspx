﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/PopupMasterPage.master" AutoEventWireup="true" CodeFile="findPassword.aspx.cs" Inherits="findPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript">

    function userValidation() {
        
        if ($("#<%=txtUserId.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0005 %>"); // 사용자 아이디를 입력하세요
            $("#<%=txtUserId.ClientID %>").focus();
            return false;
        }

        if ($("#<%=txtUserEmail.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0011 %>"); // 이메일을 입력하세요
            $("#<%=txtUserEmail.ClientID %>").focus();
            return false;
        }
    }
    
    function userValidationReg() {

        if ($("#<%=txtUserIdReg.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0005 %>"); // 사용자 아이디를 입력하세요
            $("#<%=txtUserIdReg.ClientID %>").focus();
            return false;
        }

        if ($("#<%=txtUserEmailReg.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0011 %>"); // 이메일을 입력하세요
            $("#<%=txtUserEmailReg.ClientID %>").focus();
            return false;
        }
        
        if ($("#<%=txtUserPw1.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0007 %>"); // 비밀번호를 입력하세요
                $("#<%=txtUserPw1.ClientID %>").focus();
                return false;
        }

        // 비밀번호 유효성검사
        if (!fnCheckPassword()) {
            $("#<%=txtUserPw1.ClientID %>").focus();
            return false;
        }

        if ($("#<%=txtUserPw2.ClientID %>").val() == "") {
            alert("<%= Resources.Resource.msg_0008 %>"); // 확인 비밀번호를 입력하세요
            $("#<%=txtUserPw2.ClientID %>").focus();
            return false;
        }

        if ($("#<%=txtUserPw1.ClientID %>").val() != $("#<%=txtUserPw2.ClientID %>").val()) {
            alert("<%= Resources.Resource.msg_0009 %>"); // 비밀번호가 불일치합니다
            $("#<%=txtUserPw2.ClientID %>").focus();
            return false;
        }
    }

    // 비밀번호 강화 정책
    function fnCheckPassword() {

        var upw = $("#<%=txtUserPw1.ClientID %>").val();

        var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
        if (false == strongRegex.test(upw)) {
            // 비밀번호는 알파벳 소문자, 알파벳 대문자, 숫자, 특수문자를 포함한 8자리 이상을 사용해야 합니다.

            alert("<%= Resources.Resource.msg_0000 %>");
            return false;
        }
        
        return true;

    }
    
</script>
    
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:HiddenField ID="hiddenResult" runat="server"  />    
<asp:HiddenField ID="hiddenNewPassword" runat="server" />

<div class="wrap">
 
    <div id="Header">
        <div id="TopMenuBox">
            <div class="logo"><img src="./Asset/Images/logo.gif" /></div>
        </div>
    </div>
  
    <div class="login_body"> 
        <%--<p class="login_title"><img src="./Asset/Images/login/title.png" /></p>--%>
<% 
    // 이메일 인증 후
    if (hiddenNewPassword.Value != "")
    {
        // 비밀번호 변경 성공
        if (hiddenResult.Value == "OK")
        {
%>
            <div class="login_input">
                <div class="line_bottom pad_B10" style="background:url(./asset/images/login/change_password.png) no-repeat left top ; height:30px; margin: 0; padding:10px 0 0 50px">
                    <span style="font-weight:bold; font-size:25px; color:#4488ff"><%= Resources.Resource.txt_0106 %><!-- 비밀번호 변경 --></span>
                </div>

                 <div  class="pad_T20 pad_B30 align_C">
                     <%= Resources.Resource.txt_0112 %><!-- 비밀번호 변경에 성공하였습니다. --><br />
                     <a href="http://<%=Request.ServerVariables["HTTP_HOST"] %>"> <%= Resources.Resource.txt_0110 %><!-- 홈페이지 가기 --> </a>   
                 </div>
                 <div class="pad_T20 line_top">                     
                </div>
            </div>           
<%
        }
        // 비밀번호 변경 폼   
        else
        {
%>          
            <div class="login_input">
                <div class="line_bottom pad_B10" style="background:url(./asset/images/login/change_password.png) no-repeat left top ; height:30px; margin: 0; padding:10px 0 0 50px">
                    <span style="font-weight:bold; font-size:25px; color:#4488ff"><%= Resources.Resource.txt_0106 %><!-- 비밀번호 변경 --></span>
                </div>

                 <div  class="pad_T20 pad_B30 align_C">
                    <table >
                    <colgroup>
                    <col width="62">
                    <col width="" align=left>
                    </colgroup>

                    <tr>
                        <th><%= Resources.Resource.txt_0004 %><!-- 사용자ID //--></th>
                        <td>
                            <asp:TextBox ID="txtUserIdReg" runat="server"  CssClass="textType"  />                        
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th><%= Resources.Resource.txt_0007 %><!-- 이메일 //--></th>
                        <td>
                            <asp:TextBox ID="txtUserEmailReg" runat="server"  CssClass="textType"  />                        
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <!-- 브라우저에 저장되어 있는 아이디,패스워드가 자동으로 id, password 입력박스에 셋팅되는것을 막기위해 만들어놓은 것임. //-->
                        <!-- 여기부터 시작 //-->
                        <asp:TextBox ID="txtDummyID" runat="server" style="display:none;" />
                        <asp:TextBox ID="txtDummyPW" runat="server" TextMode=Password style="display:none;" />
                        <!-- 여기까지 끝//-->
                        <th><%= Resources.Resource.txt_0010 %><!-- 비밀번호 //--></th>
                        <td>
                            <asp:TextBox ID="txtUserPw1" runat="server"  CssClass="textType"  TextMode=Password />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th><%= Resources.Resource.txt_0011 %><!-- 비밀번호확인 //--></th>
                        <td>
                            <asp:TextBox ID="txtUserPw2" runat="server"  CssClass="textType"  TextMode=Password />                            
                        </td>
                        <td>
                            <asp:Button ID="BtnSave" runat="server" Text="<%$ Resources: Resource, btn_0004 %>" OnClientClick="return userValidationReg();" OnClick="BtnSaveClick_Click"  />
                        </td>
                    </tr>
                   <%-- <tr>   
                        <td colspan=2>
                            <b>1.</b> 아이디가 포함된 암호금지.<br />  
                            <b>2.</b> 6자리 이상 숫자와 영문자의 조합.(특수문자가능)<br /> 
                            <b>3.</b> 같은문자 연속4회 이상 금지.<br />
                            
                        </td>
                    </tr>        --%>    
                    </table>
                </div>
                 <div class="pad_T20 line_top"> 
                    <asp:Label ID="lblResultMsgReg" runat="server" Text="" ForeColor=PaleVioletRed></asp:Label>
                </div>
            </div>
<%        
        }
    }
    // 이메일 인증 전 비밀번호 찾기 폼
    else
    {
        // 비밀번호 전송 성공
        if (hiddenResult.Value == "OK")
        { 
%>
            <div class="login_input">
            
                <div class="line_bottom pad_B10" style="background:url(./asset/images/login/change_password.png) no-repeat left top ; height:30px; margin: 0; padding:10px 0 0 50px">
                    <span style="font-weight:bold; font-size:25px; color:#bb2200"><%= Resources.Resource.txt_0090 %><!-- 비밀번호 찾기 --></span>
                </div>
                                
                <div  class="pad_T20 pad_B30 align_C">
                <%= Resources.Resource.txt_0111 %><!-- 새로운 비밀번호를 이메일로 전송하였습니다. --><br />
                <a href="http://<%=Request.ServerVariables["HTTP_HOST"] %>"> <%= Resources.Resource.txt_0110 %><!-- 홈페이지 가기 --> </a>   
                </div>
                <div class="pad_T20 line_top">                     
                </div>
            </div>   
<% 
        }
        // 비밀번호 찾기 폼
        else
        {        
%>
            <div class="login_input">
                
                <div class="line_bottom pad_B10" style="background:url(./asset/images/login/change_password.png) no-repeat left top ; height:30px; margin: 0; padding:10px 0 0 50px">
                    <span style="font-weight:bold; font-size:25px; color:#bb2200"><%= Resources.Resource.txt_0090 %><!-- 비밀번호 찾기 --></span>
                </div>
                
                <div  class="pad_T20 pad_B30 align_C">
                                 
                    <table>                    
                    <tr>
                        <td>
                            <asp:Label ID="lblUserId" runat="server" Text="<%$ Resources: Resource, txt_0004 %>"></asp:Label> <!-- 사용자ID //-->
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserId" runat="server" Width=150 TabIndex="1" CssClass="i_text"></asp:TextBox>
                        </td>
                        <td rowspan="2">     
                            <asp:ImageButton runat="server" ID="ibtnFindPassword"   ImageUrl="./Asset/Images/login/btn_search_pw.gif" OnClientClick="return userValidation();" onclick="ibtnFindPassword_Click"   >
                            </asp:ImageButton>                            
                       </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblUserEmail" runat="server" Text="<%$ Resources: Resource, txt_0007 %>"></asp:Label> <!-- 이메일 //-->
                        </td>
                        <td>
                            <asp:TextBox ID="txtUserEmail" runat="server" Width=150 TabIndex="2" CssClass="i_text"></asp:TextBox>
                        </td>
                    </tr>
                    </table>  
                                      
                </div>                 
                <div class="pad_T20 line_top">
                    <a href="default.aspx"><%= Resources.Resource.txt_0108 %><!-- 로그인 화면가기 //--></a>                          
                </div>
                <div>     
                    <!-- 아이디와 등록 시에 입력하신 이메일 주소를 입력해주세요 //-->
                    <asp:Label ID="lblResultMsg" runat="server" Text="<%$ Resources: Resource, txt_0109 %>" ForeColor=PaleVioletRed></asp:Label>
                </div>
                
            </div>
<%      
        }
    }
%> 
    </div>        
</div>
           
</asp:Content>

