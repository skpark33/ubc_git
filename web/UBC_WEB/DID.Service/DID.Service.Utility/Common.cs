﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Utility
{
    public class Common : IDisposable
    {
        private Database db = null;

        public Common()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        public void Dispose(){ }

        #region 페이지 메뉴명 조회- GetMenuName(string PROG_ID)
        /// <summary>
        /// 페이지 메뉴명 조회
        /// </summary>
        public string GetMenuName(string PROG_ID, string LANG_FLAG)
        {
            DbCommand Cmd = db.GetSqlStringCommand(@"
                            SELECT MENU_NAME FROM dbo.TB_POT_MENU_MIDDLE WHERE USE_FLAG='Y' AND PROG_ID = @PROG_ID AND LANG_FLAG=@LANG_FLAG
                            UNION ALL
                            SELECT MENU_NAME FROM dbo.TB_POT_MENU_SMALL WHERE USE_FLAG='Y' AND PROG_ID = @PROG_ID AND LANG_FLAG=@LANG_FLAG
                            ");

            db.AddInParameter(Cmd, "@PROG_ID", DbType.String, PROG_ID);
            db.AddInParameter(Cmd, "@LANG_FLAG", DbType.String, LANG_FLAG);

            DataTable dt = db.ExecuteDataSet(Cmd).Tables[0];

            return dt.Rows.Count > 0 ?  dt.Rows[0][0].ToString() : string.Empty;
        }
        #endregion

        #region 공통코드 조회 - GetCommonCode(string ITEM_CODE)
        /// <summary>
        /// 공통코드 조회
        /// </summary>
        /// <returns></returns>
        public DataTable GetCommonCode(string mgrId, string franchizeType, string categoryName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_CommonCode_Sel");

            db.AddInParameter(Cmd, "@mgrId", DbType.String, mgrId);
            db.AddInParameter(Cmd, "@categoryName", DbType.String, categoryName);
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion

        

    }
}
