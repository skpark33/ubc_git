﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Data;

using DID.Common.Framework;

namespace DID.Service.Utility
{
    public class FormAuthenticateUser
    {
        //UserInfoContext userInfoContext = new UserInfoContext();

        public string UserID { get; set; }
        public string UserPwd { get; set; }
        //public string CompType { get; set; }
        //public string LangFlag { get; set; }
        //public string MgrId { get; set; }
        //public string SiteId { get; set; }

        public FormAuthenticateUser() { }


        public void CreateAuthenticationTicket()
        {
            try
            {
                FormsAuthenticationTicket AuthenticationTicket = new FormsAuthenticationTicket(this.UserID, false, 720);
                string EncryptedTicket = FormsAuthentication.Encrypt(AuthenticationTicket);

                HttpCookie AuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, EncryptedTicket);
                HttpContext.Current.Response.Cookies.Add(AuthenticationCookie);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable CheckUserValidation()
        {
            using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
            {
                return obj.ValidateUser(this.UserID, this.UserPwd);
            }
        }

        public DataTable GetUserInfo()
        {
            using (DID.Service.Utility.Member obj = new DID.Service.Utility.Member())
            {
                return obj.GetUserInfo(this.UserID);
            }
        }      
       
    }
}
