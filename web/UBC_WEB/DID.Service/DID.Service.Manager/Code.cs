﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Manager
{ 
    public class Code : IDisposable 
    {
        private Database db = null;

        public Code()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() { }


        /// <summary>
        /// 공통코드관리
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <param name="PROG_ID"></param>
        /// <returns></returns>
        public DataTable GetCodeList(string customer)   
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_List");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public int GetCodeInsert(string customer, string categoryName) 
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Insert");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer);
            db.AddInParameter(Cmd, "@categoryName", DbType.String, categoryName);
            
            return db.ExecuteNonQuery(Cmd);
        }

        public int GetCodeDelete(string customer, string codeId)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Delete");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer);
            db.AddInParameter(Cmd, "@codeId", DbType.String, codeId);
            
            return db.ExecuteNonQuery(Cmd);
        }

        public int GetCodeUpdate(string customer, string codeId, string categoryName)   //string USER_ID, string PROG_ID
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Update");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer);
            db.AddInParameter(Cmd, "@codeId", DbType.String, codeId);
            db.AddInParameter(Cmd, "@categoryName", DbType.String, categoryName);

            return db.ExecuteNonQuery(Cmd);
        }

        public DataTable GetDetailCodeList(string customer, string categoryName)   //string USER_ID, string PROG_ID
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Detail_List");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer);
            db.AddInParameter(Cmd, "@categoryName", DbType.String, categoryName);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public int GetCodeDetailInsert(string customer, string categoryName, string enumString, int dorder, int visible)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Detail_Insert");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer);
            db.AddInParameter(Cmd, "@categoryName", DbType.String, categoryName);
            db.AddInParameter(Cmd, "@enumString", DbType.String, enumString);
            db.AddInParameter(Cmd, "@dorder", DbType.Int16, dorder);
            db.AddInParameter(Cmd, "@visible", DbType.Int16, visible);
            
            return db.ExecuteNonQuery(Cmd);
        }


        public int GetCodeDetailUpdate(string customer, string codeId, string enumString, int dorder, int visible)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Detail_Update");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer);
            db.AddInParameter(Cmd, "@codeId", DbType.String, codeId);
            db.AddInParameter(Cmd, "@enumString", DbType.String, enumString);
            db.AddInParameter(Cmd, "@dorder", DbType.Int16, dorder);
            db.AddInParameter(Cmd, "@visible", DbType.Int16, visible);

            return db.ExecuteNonQuery(Cmd);
        }


        public int GetCodeDetailDelete(string customer, string codeId)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Code_Detail_Delete");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer);
            db.AddInParameter(Cmd, "@codeId", DbType.String, codeId);

            return db.ExecuteNonQuery(Cmd);
        }

        

    }
}
