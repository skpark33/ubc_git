﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Manager
{ 
    public class GeneralSettings : IDisposable 
    {
        private Database db = null;

        public GeneralSettings()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() { }
                
        public DataTable GetCustomerURL(string customerId)   
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_generalSettings_Url_Sel");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customerId);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public int SetCustomerURL(string customerId, string url)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_generalSettings_Url_Upd");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customerId);
            db.AddInParameter(Cmd, "@url", DbType.String, url);

            return db.ExecuteNonQuery(Cmd);
        }

        public int SetCustomerCopyURL(string customerId, string copyUrl)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_generalSettings_CopyUrl_Upd");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customerId);
            db.AddInParameter(Cmd, "@copyUrl", DbType.String, copyUrl);

            return db.ExecuteNonQuery(Cmd);
        }

        public DataTable GetSiteLanguages(string customerId)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_generalSettings_Language_Sel");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customerId);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public int DeleteSiteLanguage(string customerId, string cultureName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_generalSettings_Language_Del");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customerId);
            db.AddInParameter(Cmd, "@cultureName", DbType.String, cultureName);

            return db.ExecuteNonQuery(Cmd);
        }

        public int InsertSiteLanguage(string customerId, string cultureName, string englishName, bool useYN)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_generalSettings_Language_Reg");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customerId);
            db.AddInParameter(Cmd, "@cultureName", DbType.String, cultureName);
            db.AddInParameter(Cmd, "@englishName", DbType.String, englishName);
            db.AddInParameter(Cmd, "@useYN", DbType.Boolean, useYN);

            return db.ExecuteNonQuery(Cmd);
        }

        public int UpdateSiteLanguage(string customerId, string cultureName, bool useYN)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_generalSettings_Language_Upd");
            db.AddInParameter(Cmd, "@customerId", DbType.String, customerId);
            db.AddInParameter(Cmd, "@cultureName", DbType.String, cultureName);
            db.AddInParameter(Cmd, "@useYN", DbType.Boolean, useYN);

            return db.ExecuteNonQuery(Cmd);
        }

    }
}
