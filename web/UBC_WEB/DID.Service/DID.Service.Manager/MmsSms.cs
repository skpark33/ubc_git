﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Manager
{ 
    public class MmsSms : IDisposable 
    {
        private Database db = null;

        public MmsSms()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() { }


        /// <summary>
        /// MmsSms 
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <param name="PROG_ID"></param>
        /// <returns></returns>

         
        public int mmsSend(string  phoneSend  , string title , string contents  , string franchizeType, string siteId , string phone , string fn    )    //MMS
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Mms_Send");

            db.AddInParameter(Cmd, "@phoneSend", DbType.String, phoneSend);
            db.AddInParameter(Cmd, "@title", DbType.String, title);
            db.AddInParameter(Cmd, "@contents", DbType.String, contents);

            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType );
            db.AddInParameter(Cmd, "@siteId",        DbType.String, siteId );
            db.AddInParameter(Cmd, "@phone",         DbType.String, phone );
            db.AddInParameter(Cmd, "@fn",            DbType.String, fn ); 
            return db.ExecuteNonQuery(Cmd);
        }

        public DataTable mmsSitePhone(string siteId)    //사이트의 전화번호 가져오기
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Mms_Site_Phone");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            DataTable dt = db.ExecuteDataSet(Cmd).Tables[0];

            return dt;
        }



        public int smsSend(string send_phone , string recive_phone , string contents  )    //SMS
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Sms_Send");

            db.AddInParameter(Cmd, "@send_phone", DbType.String, send_phone );
            db.AddInParameter(Cmd, "@recive_phone", DbType.String, recive_phone );
            db.AddInParameter(Cmd, "@contents", DbType.String, contents );
             
            return db.ExecuteNonQuery(Cmd);
        }
         

    }
}
