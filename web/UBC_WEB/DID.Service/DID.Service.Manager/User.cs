﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;

namespace DID.Service.Manager
{ 
    public class User : IDisposable 
    {
        private Database db = null;

        public User()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() { }


        /// <summary>
        /// 사용자 관리
        /// </summary>
        /// <param name="USER_ID"></param>
        /// <param name="PROG_ID"></param>
        /// <returns></returns>
        
        public DataTable GetManageSiteList(string ROOT_SITE_ID)   // 관리 사이트 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manage_Site_List");
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        public DataTable GetManageHostList()   // 관리 단말 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Manage_Host_List");
            //db.AddInParameter(Cmd, "@USER_ID", DbType.String, USER_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        public DataTable GetUserList(string siteId, string ROOT_SITE_ID)   // 사용자 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_List");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@ROOT_SITE_ID", DbType.String, ROOT_SITE_ID); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public DataTable GetUserDetail(string siteId, string userId)   // 사용자 디테일
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_Detail");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId); 
            db.AddInParameter(Cmd, "@userId", DbType.String, userId); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        public int GetUserDelete(string siteId, string userId)   // 사용자 삭제
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_Delete");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);

            return db.ExecuteNonQuery(Cmd);
        }

        // 사용자 등록 
        //siteId , password , userName , mobileNo , email , userType , roleId , idationDate , useEmail , useSms , probableCauseList , siteList , hostList
        public int GetUserInsert(
                                  string userId
                                , string siteId
                                , string password
                                , string userName
                                , string mobileNo
                                , string email
                                , string userType
                                , string roleId
                                , string validationDate
                                , string useEmail
                                , string useSms
                                , string probableCauseList
                                , string siteList
                                , string hostList
                                )
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_insert");
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@password", DbType.String, password);
            db.AddInParameter(Cmd, "@userName", DbType.String, userName);
            db.AddInParameter(Cmd, "@mobileNo", DbType.String, mobileNo);
            db.AddInParameter(Cmd, "@email", DbType.String, email);
            db.AddInParameter(Cmd, "@userType", DbType.String, userType);
            db.AddInParameter(Cmd, "@roleId", DbType.String, roleId);
            db.AddInParameter(Cmd, "@validationDate", DbType.String, validationDate);
            db.AddInParameter(Cmd, "@useEmail", DbType.String, useEmail);
            db.AddInParameter(Cmd, "@useSms", DbType.String, useSms);
            db.AddInParameter(Cmd, "@probableCauseList", DbType.String, probableCauseList);
            db.AddInParameter(Cmd, "@siteList", DbType.String, siteList);
            db.AddInParameter(Cmd, "@hostList", DbType.String, hostList);

            return db.ExecuteNonQuery(Cmd);
        }

         
        // 사용자 수정 
        //siteId , password , userName , mobileNo , email , userType , roleId , idationDate , useEmail , useSms , probableCauseList , siteList , hostList
        public int GetUserUpdate(
                                  string userId
                                , string siteId
                                , string password
                                , string userName
                                , string mobileNo
                                , string email
                                , string userType
                                , string roleId
                                , string validationDate
                                , string useEmail
                                , string useSms
                                , string probableCauseList
                                , string siteList
                                , string hostList
                                )   
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_update");
            db.AddInParameter(Cmd, "@userId",            DbType.String, userId);
            db.AddInParameter(Cmd, "@siteId",            DbType.String, siteId);
            db.AddInParameter(Cmd, "@password",          DbType.String, password);
            db.AddInParameter(Cmd, "@userName",          DbType.String, userName);
            db.AddInParameter(Cmd, "@mobileNo",          DbType.String, mobileNo);
            db.AddInParameter(Cmd, "@email",             DbType.String, email);
            db.AddInParameter(Cmd, "@userType",          DbType.String, userType);
            db.AddInParameter(Cmd, "@roleId",            DbType.String, roleId);
            db.AddInParameter(Cmd, "@validationDate",    DbType.String, validationDate);
            db.AddInParameter(Cmd, "@useEmail",          DbType.String, useEmail);
            db.AddInParameter(Cmd, "@useSms",            DbType.String, useSms);
            db.AddInParameter(Cmd, "@probableCauseList", DbType.String, probableCauseList);
            db.AddInParameter(Cmd, "@siteList",          DbType.String, siteList);
            db.AddInParameter(Cmd, "@hostList",          DbType.String, hostList);

            return db.ExecuteNonQuery(Cmd);
        }


        // 사용자 수정 암호를 낭넣었을때에 
        //siteId , password , userName , mobileNo , email , userType , roleId , idationDate , useEmail , useSms , probableCauseList , siteList , hostList
        public int GetUserUpdateNotPass(
                                  string userId
                                , string siteId 
                                , string userName
                                , string mobileNo
                                , string email
                                , string userType
                                , string roleId
                                , string validationDate
                                , string useEmail
                                , string useSms
                                , string probableCauseList
                                , string siteList
                                , string hostList
                                )
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_update_notPass");
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId); 
            db.AddInParameter(Cmd, "@userName", DbType.String, userName);
            db.AddInParameter(Cmd, "@mobileNo", DbType.String, mobileNo);
            db.AddInParameter(Cmd, "@email", DbType.String, email);
            db.AddInParameter(Cmd, "@userType", DbType.String, userType);
            db.AddInParameter(Cmd, "@roleId", DbType.String, roleId);
            db.AddInParameter(Cmd, "@validationDate", DbType.String, validationDate);
            db.AddInParameter(Cmd, "@useEmail", DbType.String, useEmail);
            db.AddInParameter(Cmd, "@useSms", DbType.String, useSms);
            db.AddInParameter(Cmd, "@probableCauseList", DbType.String, probableCauseList);
            db.AddInParameter(Cmd, "@siteList", DbType.String, siteList);
            db.AddInParameter(Cmd, "@hostList", DbType.String, hostList);

            return db.ExecuteNonQuery(Cmd);
        }



        public DataTable GetRoleList(string customer)   // 롤 리스트
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Role_List");
            db.AddInParameter(Cmd, "@customer", DbType.String, customer); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }



        public DataTable GetUserIdDup(string userId )   // 사용자중복체크
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_User_Dup");
            db.AddInParameter(Cmd, "@userId", DbType.String, userId); 

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        
        // 사용자 엑셀 업로드- 조직 오류 체크
        public DataTable GetUserSite(string siteId, string franchizeType)    
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_User_Excel");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
         
        // 사용자 엑셀 업로드- 조직/사용자 중복  오류 체크
        public DataTable GetUserSiteDup(string siteId, string userId)    
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_User_Excel_Dup");
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }



        // 사용자 엑셀 업로드-  아이디 저장
        public int UserSiteExcelInsert(   string siteId
                                        , string userId
                                        , string password 
                                        , string userName
                                        , string mobileNo
                                        , string email
                                        , string userType
                                        , string roleId
                                        )   
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Site_User_Excel_Insert");
            db.AddInParameter(Cmd, "@siteId",   DbType.String, siteId);
            db.AddInParameter(Cmd, "@userId",   DbType.String, userId);
            db.AddInParameter(Cmd, "@password", DbType.String, password);
            db.AddInParameter(Cmd, "@userName", DbType.String, userName);
            db.AddInParameter(Cmd, "@mobileNo", DbType.String, mobileNo);
            db.AddInParameter(Cmd, "@email", DbType.String, email);
            db.AddInParameter(Cmd, "@userType", DbType.String, userType);
            db.AddInParameter(Cmd, "@roleId", DbType.String, roleId);
            

            return db.ExecuteNonQuery(Cmd);
        }


    }
}
