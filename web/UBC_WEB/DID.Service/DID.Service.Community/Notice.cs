﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DID.Service.Community
{
    /// <summary>
    /// 공지사항
    /// </summary>
    public class Notice : BoardBase
    {
        private Database db = null;

        public Notice()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        #region 게시물 수정 - Update
        /// <summary>
        /// 게시물 수정
        /// </summary>
        public DataTable GetDetailPop(string compType)
        {

            DbCommand Cmd = db.GetStoredProcCommand(SP_DETAIL_POPUP);

            db.AddInParameter(Cmd, "@compType", DbType.String, compType);

            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        #endregion


        #region 게시물 등록 - Insert
        /// <summary>
        /// 게시물 등록
        /// </summary>
        public long Insert(int boardMasterId
                            , string userId
                            , string title
                            , string contents
                            , bool topYn
                            , bool popYn
                            , string ip
                            , string compType)
        {
            DbCommand Cmd = db.GetStoredProcCommand(SP_INSERT);

            db.AddInParameter(Cmd,"@BoardMasterID" , DbType.Int32, boardMasterId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@title", DbType.String, title);
            db.AddInParameter(Cmd, "@contents", DbType.String, contents);
            db.AddInParameter(Cmd, "@topYn", DbType.Boolean, topYn);
            db.AddInParameter(Cmd, "@popYn", DbType.Boolean, popYn);
            db.AddInParameter(Cmd, "@ip", DbType.String, ip);
            db.AddInParameter(Cmd, "@compType", DbType.String, compType);

            db.AddOutParameter(Cmd, "@boardId", DbType.Int32, 4);

            db.ExecuteNonQuery(Cmd);

            return Convert.ToInt64(db.GetParameterValue(Cmd, "@boardId"));
        }
        #endregion

        #region 게시물 수정 - Update
        /// <summary>
        /// 게시물 수정
        /// </summary>
        public int Update(long boardId
                        , string userId
                        , string title
                        , string contents
                        , bool topYn
                        , bool popYn
                        , string ip)
        {

            DbCommand Cmd = db.GetStoredProcCommand(SP_UPDATE);

            db.AddInParameter(Cmd, "@boardId", DbType.Int64, boardId);
            db.AddInParameter(Cmd, "@userId", DbType.String, userId);
            db.AddInParameter(Cmd, "@Title", DbType.String, title);
            db.AddInParameter(Cmd, "@Contents", DbType.String, contents);
            db.AddInParameter(Cmd, "@topYn", DbType.Boolean, topYn);
            db.AddInParameter(Cmd, "@popYn", DbType.Boolean, popYn);
            db.AddInParameter(Cmd, "@ip", DbType.String, ip);

            return db.ExecuteNonQuery(Cmd);
        }
        #endregion
    }
}
