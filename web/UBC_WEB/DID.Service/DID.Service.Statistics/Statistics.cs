﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using DID.Common.Framework;


namespace DID.Service.Statistics
{
    public class Statistics : IDisposable
    {
        private Database db = null;

        public Statistics()
        {
            db = DatabaseFactory.CreateDatabase();
        }
        public void Dispose() { }


        /// <summary>
        /// 통계관리
        /// </summary>  
        /// <returns></returns>


        // 장애통계  - 재훈
        // 2011-07-20 임유석 수정
        public DataTable GetFaultStatisticsList(string franchizeType, string fromDate, string toDate, string siteId, string probableCause)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_FaultStatistics_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@probableCause", DbType.String, probableCause);
            
            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        
        // 장애통계 - 그래프 - 재훈
        // 2011-07-20 임유석 수정
        public DataTable GetFaultStatisticsGraph(string franchizeType, string fromDate, string toDate, string siteId, string probableCause)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_FaultStatistics_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId); 
            db.AddInParameter(Cmd, "@probableCause", DbType.String, probableCause);            
            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        // 전송이용 통계 리스트 - 재훈
        // 2011-07-20 임유석 수정
        public DataTable GetMessageUsageStatisticsList(string franchizeType, string fromDate, string toDate, string siteId, string mobileSend)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_MessageUsage_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@mobileSend", DbType.String, mobileSend);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        // 전송이용 통계 - 그래프
        // 2011-07-20 임유석 추가
        public DataTable GetMessageUsageStatisticsGraph(string franchizeType, string fromDate, string toDate, string siteId, string mobileSend)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_MessageUsage_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@mobileSend", DbType.String, mobileSend);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }
                
        // 컨텐츠 방송통계
        // 2011-07-16 임유석 추가
        public DataTable GetContentsBroadcastList(string franchizeType, string fromDate, string toDate, string siteId, string hostId, string contentsName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ContentsBroadcast_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@hostId", DbType.String, hostId);
            db.AddInParameter(Cmd, "@contentsName", DbType.String, contentsName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 컨텐츠 방송통계 - 그래프
        // 2011-07-16 임유석 추가
        public DataTable GetContentsBroadcastGraph(string franchizeType, string fromDate, string toDate, string siteId, string hostId, string contentsName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ContentsBroadcast_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@hostId", DbType.String, hostId);
            db.AddInParameter(Cmd, "@contentsName", DbType.String, contentsName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        // 컨텐츠 등록 통계
        // 2011-07-16 임유석 추가
        public DataTable GetContentsRegList(string franchizeType, string fromDate, string toDate, string siteId, string contentsName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ContentsReg_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@contentsName", DbType.String, contentsName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 컨텐츠 등록 통계 - 그래프
        // 2011-07-16 임유석 추가
        public DataTable GetContentsRegGraph(string franchizeType, string fromDate, string toDate, string siteId, string contentsName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ContentsReg_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@contentsName", DbType.String, contentsName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        // 컨텐츠 사용 통계
        // 2011-07-17 임유석 추가
        public DataTable GetContentsUsedList(string franchizeType, string fromDate, string toDate, string siteId, string contentsName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ContentsUsed_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@contentsName", DbType.String, contentsName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 컨텐츠 사용 통계 - 그래프
        // 2011-07-17 임유석 추가
        public DataTable GetContentsUsedGraph(string franchizeType, string fromDate, string toDate, string siteId, string contentsName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ContentsUsed_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@contentsName", DbType.String, contentsName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 패키지 등록 통계
        // 2011-07-18 임유석 추가
        public DataTable GetProgramRegList(string franchizeType, string fromDate, string toDate, string siteId, string programName, string hostType)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ProgramReg_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@programName", DbType.String, programName);
            db.AddInParameter(Cmd, "@hostType", DbType.String, hostType);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 패키지 등록 통계 - 그래프
        // 2011-07-18 임유석 추가
        public DataTable GetProgramRegGraph(string franchizeType, string fromDate, string toDate, string siteId, string programName, string hostType)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ProgramReg_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@programName", DbType.String, programName);
            db.AddInParameter(Cmd, "@hostType", DbType.String, hostType);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        // 패키지 사용 통계
        // 2011-07-18 임유석 추가
        public DataTable GetProgramUsedList(string franchizeType, string fromDate, string toDate, string siteId, string programName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ProgramUsed_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@programName", DbType.String, programName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 패키지 사용 통계 - 그래프
        // 2011-07-18 임유석 추가
        public DataTable GetProgramUsedGraph(string franchizeType, string fromDate, string toDate, string siteId, string programName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ProgramUsed_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@programName", DbType.String, programName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        // 배포 통계
        // 2011-07-18 임유석 추가
        public DataTable GetApplyStatisticsList(string franchizeType, string fromDate, string toDate, string siteId, string programName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ApplyStatistics_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@programName", DbType.String, programName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 배포 통계 - 그래프
        // 2011-07-18 임유석 추가
        public DataTable GetApplyStatisticsGraph(string franchizeType, string fromDate, string toDate, string siteId, string programName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ApplyStatistics_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@programName", DbType.String, programName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 스캐쥴 생성/변경 통계
        // 2011-07-18 임유석 추가
        public DataTable GetBpStatisticsList(string franchizeType, string fromDate, string toDate, string siteId, string bpName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_BpStatistics_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@bpName", DbType.String, bpName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 스캐쥴 생성/변경 통계 - 그래프
        // 2011-07-18 임유석 추가
        public DataTable GetBpStatisticsGraph(string franchizeType, string fromDate, string toDate, string siteId, string bpName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_BpStatistics_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@bpName", DbType.String, bpName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 상호작용 사용 통계
        // 2011-07-21 임유석 추가
        // 2011-12-09 임유석 수정
        public DataTable GetInterActiveList(string franchizeType, string fromDate, string toDate, string siteIdPath, string parentSiteId, string searchType)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_InterActive_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteIdPath", DbType.String, siteIdPath);
            db.AddInParameter(Cmd, "@parentSiteId", DbType.String, parentSiteId);
            db.AddInParameter(Cmd, "@searchType", DbType.String, searchType);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 상호작용 사용 키워드 조회
        // 2011-12-08 임유석 추가
        public DataTable GetInterActiveKeyword(string franchizeType, string totalVisitorString, string keyword1, string keyword2, string keyword3, string keyword4)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_InterActive_Keyword");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@totalVisitorString", DbType.String, totalVisitorString);
            db.AddInParameter(Cmd, "@keyword1", DbType.String, keyword1);
            db.AddInParameter(Cmd, "@keyword2", DbType.String, keyword2);
            db.AddInParameter(Cmd, "@keyword3", DbType.String, keyword3);
            db.AddInParameter(Cmd, "@keyword4", DbType.String, keyword4);
            return db.ExecuteDataSet(Cmd).Tables[0];

        }

        // 상호작용 사용 통계 - 그래프
        // 2011-07-21 임유석 추가
        public DataTable GetInterActiveGraph(string franchizeType, string fromDate, string toDate, string siteId, string programName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_InterActive_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@programName", DbType.String, programName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }

        // 콘텐츠 접근 통계
        // 2011-07-21 임유석 추가
        public DataTable GetContentsAccessList(string franchizeType, string fromDate, string toDate, string siteId, string selectedSiteId, string searchType)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ContentsAccess_List");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@selectedSiteId", DbType.String, selectedSiteId);
            db.AddInParameter(Cmd, "@searchType", DbType.String, searchType);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }


        // 콘텐츠 접근  - 그래프
        // 2011-07-21 임유석 추가
        public DataTable GetContentsAccessGraph(string franchizeType, string fromDate, string toDate, string siteId, string programName)
        {
            DbCommand Cmd = db.GetStoredProcCommand("dbo.usp_Statistics_ContentsAccess_Graph");
            Cmd.CommandTimeout = 180;
            db.AddInParameter(Cmd, "@franchizeType", DbType.String, franchizeType);
            db.AddInParameter(Cmd, "@fromDate", DbType.String, fromDate);
            db.AddInParameter(Cmd, "@toDate", DbType.String, toDate);
            db.AddInParameter(Cmd, "@siteId", DbType.String, siteId);
            db.AddInParameter(Cmd, "@programName", DbType.String, programName);
            return db.ExecuteDataSet(Cmd).Tables[0];
        }
        
    }

}
