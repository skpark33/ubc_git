﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DID.Common.Framework
{
    public static class Extensions
    {
        public static int ToInt(this object value)
        {
            return Convert.ToInt32(value);
        }

        public static double ToDouble(this object value)
        {
            double num = 0;

            if (value == null || value == DBNull.Value)
                return num;

            num = Convert.ToDouble(value);

            return num;
        }

        public static bool IsNullOrEmpty(this object value)
        {
            return ((value == null) || string.IsNullOrEmpty(value.ToString()));
        }

        public static string IsNullOrEmpty(this object value, string text)
        {
            string str = value.ToString();

            if (value == null)
            {
                return text;
            }
            return (string.IsNullOrEmpty(str) ? text : str);
        }

        public static DataRow ToDataRow(this object value)
        {
            DataTable dt = value as DataTable;

            if (dt == null || dt.Rows.Count == 0)
                return null;
            else
                return dt.Rows[0];
        }

        public static string ToZero(this object value)
        {
            return ((value == null) || string.IsNullOrEmpty(value.ToString()) || value == DBNull.Value ? "0" : value.ToString());
        }

        public static string ToPadZero(this object value)
        {
            return value.ToString().Length == 1 ? "0"+value.ToString() : value.ToString();
        }

        public static string ToDateString(this object value)
        {
            string date = string.Empty;

            if (value.GetType() == typeof(DateTime))
                date = ((DateTime)value).ToString("yyyyMMdd");
            else if (value.GetType() == typeof(string))
                date = value.ToString();
            else
                return string.Empty;

            if (date.Equals(string.Empty) || date.Length != 8)
                return string.Empty;


            return string.Format("{0}-{1}-{2}", date.Substring(0, 4), date.Substring(4, 2), date.Substring(6));

        }

        public static string ToYmd(this object value)
        {
            return value.ToString().Replace("-","");
        }

        public static void ToRemoveItem(this object value)
        {

            System.Web.UI.WebControls.ListItemCollection item = value as System.Web.UI.WebControls.ListItemCollection;

            System.Web.UI.WebControls.ListItem itemTemp = new System.Web.UI.WebControls.ListItem();
            itemTemp= item[0];

            item.Clear();

            item.Add(itemTemp);
        }

        public static void ToRemoveItemAll(this object value)
        {

            System.Web.UI.WebControls.ListItemCollection item = value as System.Web.UI.WebControls.ListItemCollection;

            item.Clear();
        }

    }
}
