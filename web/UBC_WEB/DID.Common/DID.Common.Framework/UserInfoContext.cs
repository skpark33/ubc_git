﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace DID.Common.Framework
{
    public class UserInfoContext
    {
        public string USER_ID
        {
            get { return !GetUserInfo("USER_ID").IsNullOrEmpty() ? GetUserInfo("USER_ID").ToString() : ""; }
        }

        public string USER_NAME
        {
            get { return !GetUserInfo("USER_NAME").IsNullOrEmpty() ? GetUserInfo("USER_NAME").ToString() : ""; }
        }

        public string COMP_CODE
        {
            get { return !GetUserInfo("COMP_CODE").IsNullOrEmpty() ? GetUserInfo("COMP_CODE").ToString() : ""; }
        }

        public string COMP_TYPE 
        {
            get { return !GetUserInfo("COMP_TYPE").IsNullOrEmpty() ? GetUserInfo("COMP_TYPE").ToString() : ""; }
        }

        public string USER_TYPE
        {
            get { return !GetUserInfo("USER_TYPE").IsNullOrEmpty() ? GetUserInfo("USER_TYPE").ToString() : ""; }
        }

        public string LANG_FLAG
        {
            get { return !GetUserInfo("LANG_FLAG").IsNullOrEmpty() ? GetUserInfo("LANG_FLAG").ToString() : ""; }
        }

        public string LOGIN_USER_ID
        {
            get { return !GetUserInfo("LOGIN_USER_ID").IsNullOrEmpty() ? GetUserInfo("LOGIN_USER_ID").ToString() : ""; }
        }

        public string LOGIN_USER_PWD
        {
            get { return !GetUserInfo("LOGIN_USER_ID").IsNullOrEmpty() ? GetUserInfo("LOGIN_USER_ID").ToString() : ""; }
        }

        public UserInfoContext()
        {
        }

        public UserInfoContext(string userID)
        {
        }   

        private object GetUserInfo(string key)
        {
            return (string.IsNullOrEmpty(key)) ? string.Empty : HttpContext.Current.Request.Cookies["UserInfo"] != null ? HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request.Cookies["UserInfo"][key.ToUpper()]) : string.Empty;
        }

        public void AbandonUserInfo()
        {
            //FormsAuthentication.SignOut();

            HttpContext.Current.Response.Cookies["UserInfo"].Value = null;
            HttpContext.Current.Response.Cookies["UserInfo"].Expires = new System.DateTime(1999, 1, 1);
        }

    }
}
